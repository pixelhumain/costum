<div class="modal fade modal-boostrap-help-and-feedback" id="openModalHelpAndFeedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <a href="#tabHelp" class="tab-item" data-toggle="tab"><?= Yii::t("cms", "Quick help") ?></a>
        <a href="#tabQuestion" class="tab-item" data-toggle="tab"><?= Yii::t("cms", "Ask a question") ?></a>
        <a href="#tabFeedback" class="tab-item" data-toggle="tab"><?= Yii::t("cms", "Giving feedback") ?></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="tab-content">
            <div class="tab-pane fade" id="tabHelp" role="tablist" aria-multiselectable="true">
                <div class="link-to-doc-content">
                    <a class="link-to-doc" target="_blank" href="https://codimd.communecter.org/wPwWi-n0SLGKeprbX8ld5w?view"><i class="fa fa-link"></i> <?= Yii::t("cms", "Access to documentation") ?></a>
                </div>

                <!-- Configurer son costum -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="helpConfigCostum">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#tabHelp" href="#configcostum" aria-expanded="false" aria-controls="configcostum">I - <?= Yii::t("cms", "Configuring your costum") ?></a>
                        </h5>
                    </div>
                    <div id="configcostum" class="panel-collapse collapse" role="tabpanel" aria-labelledby="helpConfigCostum">
                        <div class="panel-body">
                            <div id="allConfigCostum" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="infoGConfig">
                                  <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#allConfigCostum" href="#infoGCostum" aria-expanded="false" aria-controls="infoGCostum">A - <?= Yii::t("cms", "General information about your costum") ?></a>
                                  </h5>
                                </div>
                                <div id="infoGCostum" class="panel-collapse collapse" role="tabpanel" aria-labelledby="infoGConfig">
                                  <div class="panel-body">
                                    <p><?= Yii::t("cms", "A costum is created by an organization, project or event wishing to develop its own site, also known as a CMS. By default, a costum contains the following information: Logo, title, info for meta referencing, costum languages with its various registered versions (registered costum version).") ?></p>
                                    <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_de4bcf5ae80683a14bfc2dbfd9c608bc.png" class="thumb-info" data-title="<?= Yii::t("cms", "General costum info") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="constructionSiteConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#allConfigCostum" href="#constructionGenerale" aria-expanded="false" aria-controls="constructionGenerale">B - <?= Yii::t("cms", "General site construction") ?></a>
                                    </h5>
                                </div>
                                <div id="constructionGenerale" class="panel-collapse collapse" role="tabpanel" aria-labelledby="constructionSiteConfig">
                                    <div class="panel-body">
                                        <p classs="img-view"><?= Yii::t("cms", "Menu") ?> : <a href="https://codimd.communecter.org/uploads/upload_50e2febefcf31e68e4def9a4c65f09ad.png" class="thumb-info" data-title="<?= Yii::t("cms", "General site construction") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        <ul style="list-style: none;">
                                            <li>
                                                <h5>B.1 - <?= Yii::t("cms", "Pages") ?> :</h5>
                                                <p><?= Yii::t("cms", "Definition and management of all site pages, from home page to legal notices") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_05ce306fef23c14818b7891128fb009f.png" alt="page">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_4f04a29e8bdf70ef2b1464b24b3e379a.png" class="thumb-info" data-title="<?= Yii::t("cms", "Menu contents page") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>B.2 - <?= Yii::t("cms", "Menus & header & footer") ?> :</h5>
                                                <p><?= Yii::t("cms", 'complete view of site layout and menus to build a menu, go directly to section II "Menus & header & footer".') ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_887f33ee7a9a002cd312961df3ea47d6.png" alt="menu-entete">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_10e403ca85caa9ede8b0c2a5e9fb5778.png" class="thumb-info" data-title="<?= Yii::t("cms", "Menus & header & footer") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>B.3 - <?= Yii::t("cms", "Add-on element") ?> :</h5>
                                                <p><?= Yii::t("cms", "All the elements you can activate and define to add to your costum: projects, events, local players and much more...") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_9d4cf90487cd2f589035c1b75bfd4056.png" alt="elementAjoutable">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_e53d66437fa27e2b72e9234290b3bde7.png" class="thumb-info" data-title="<?= Yii::t("cms", "Add-on element") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="designGlobalConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#allConfigCostum" href="#designGlobal" aria-expanded="false" aria-controls="designGlobal">C - <?= Yii::t("cms", "Global site design") ?></a>
                                    </h5>
                                </div>
                                <div id="designGlobal" class="panel-collapse collapse" role="tabpanel" aria-labelledby="designGlobalConfig">
                                    <div class="panel-body">
                                        <p classs="img-view"><?= Yii::t("cms", "Menu") ?> : <a href="https://codimd.communecter.org/uploads/upload_85c7fdadfb62498c73696e9f63973652.png" class="thumb-info" data-title="<?= Yii::t("cms", "Global site design") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        <ul style="list-style: none;">
                                            <li>
                                                <h5>C.1 <?= Yii::t("cms", "Dressing") ?> : </h5>
                                                <p><?= Yii::t("cms", "Style and configuration of loader, progress, menu (top or left), general site color, font and default css for site title, text and links.") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_a2b047dfc9ce970a15926e023f83e5fd.png" alt="<?= Yii::t("cms", "Dressing") ?>">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_9d53fd8ed52fd29f83bb129797687d90.png" class="thumb-info" data-title="<?= Yii::t("cms", "Dressing") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>C.2 <?= Yii::t("cms", "Loader") ?> : </h5>
                                                <p><?= Yii::t("cms", "Types of animation during page loading") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_1d6b7b74403137216914c6f9aa8d18f6.png" alt=" <?= Yii::t("cms", "Loader") ?>">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_ad5d8a7158fd4240279202a80bb53d86.png" class="thumb-info" data-title="<?= Yii::t("cms", "Overview") ?>" data-lightbox="helpAndFeedback"> <?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>C.3 <?= Yii::t("cms", "Customized css style") ?> :</h5>
                                                <p><?= Yii::t("cms", "Specific css code for the site") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_5ff74721175342ba3e2a99fa935a45ad.png" alt="css">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_8a9fdb785d1cad21956aa36942ae418a.png" class="thumb-info" data-title="<?= Yii::t("cms", "Customized css style") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="reglageCommunauteConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#allConfigCostum" href="#reglageCommunaute" aria-expanded="false" aria-controls="reglageCommunaute">D - <?= Yii::t("cms", "Community settings") ?></a>
                                    </h5>
                                </div>
                                <div id="reglageCommunaute" class="panel-collapse collapse" role="tabpanel" aria-labelledby="reglageCommunaute">
                                    <div class="panel-body">
                                        <p classs="img-view"><?= Yii::t("cms", "Menu") ?> : <a href="https://codimd.communecter.org/uploads/upload_995a2194d184f673bc69baef439d2ea1.png" class="thumb-info" data-title="<?= Yii::t("cms", "Community settings") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        <ul style="list-style: none;">
                                            <li>
                                                <h5>D.1 <?= Yii::t("cms", "Administrators") ?> :</h5>
                                                <p><?= Yii::t("cms", "Site community management with lists of costum members and their respective roles") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_ce17b0d4fcc3cf27ac35f0ac22a03d2b.png" alt="<?= Yii::t("cms", "Administrators") ?>">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_da4a96ce071781500aba706b081cf018.png" class="thumb-info" data-title="<?= Yii::t("cms", "Administrators") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>D.2 <?= Yii::t("cms", "Media manager") ?> :</h5>
                                                <p><?= Yii::t("cms", "A gallery with all the photos of the site") ?></p>
                                                <img src="https://codimd.communecter.org/uploads/upload_66a2b62f0b521ba493e9c833fbe9143b.png" alt="<?= Yii::t("cms", "Media manager") ?>">
                                                <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_9ec8cb8ecaa60793b59424e24645af71.png" class="thumb-info" data-title="<?= Yii::t("cms", "Media manager") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end configurer son costum -->

                <!-- Creation d'un menu -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="createMenuConfig">
                        <h5 class="panel-title">    
                            <a data-toggle="collapse" data-parent="#tabHelp" href="#createMenu" aria-expanded="false" aria-controls="createMenu">II - <?= Yii::t("cms", "Creating a menu") ?></a>
                        </h5>
                    </div>
                    <div id="createMenu" class="panel-collapse collapse" role="tabpanel" aria-labelledby="createMenuConfig">
                        <div class="panel-body">
                            <p><?= Yii::t("cms", 'Menu creation can be found in the "Menus & header & footer" menu') ?> : <a href="https://codimd.communecter.org/uploads/upload_887f33ee7a9a002cd312961df3ea47d6.png" class="thumb-info" data-title="<?= Yii::t("cms", "Creating a menu") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                            <ol class="sommaire">
                                <li class="sommaire-item"><?= Yii::t("cms", "Activate the menus you want to create: top menu, left menu, submenu, footer") ?> : <a href="https://codimd.communecter.org/uploads/upload_23c66f7fd35b95d98cfe8025b220d7f6.png" class="thumb-info" data-title="<?= Yii::t("cms", "Activate menus") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                <li class="sommaire-item"><?= Yii::t("cms", "Add a button to the menu") ?> : <a href="https://codimd.communecter.org/uploads/upload_0deaaf82394e24da5c83d3ca937cc032.png" class="thumb-info" data-title="<?= Yii::t("cms", "Add a button to the menu") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                <li class="sommaire-item"><?= Yii::t("cms", "Drag and drop button into content (button style can be edited after dragging)") ?> : <a href="https://codimd.communecter.org/uploads/upload_3659820f37599be8df9d3841ccbd0e9c.png" class="thumb-info" data-title="<?= Yii::t("cms", "Drag and drop button into contents") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- end creation d'un menu -->

                <!-- Création d'une page  --->                
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="createPageConfig">
                        <h5 class="panel-title">    
                            <a data-toggle="collapse" data-parent="#tabHelp" href="#createPage" aria-expanded="false" aria-controls="createPage">III - <?= Yii::t("cms", "Creating a page") ?></a>
                        </h5>
                    </div>
                    <div id="createPage" class="panel-collapse collapse" role="tabpanel" aria-labelledby="createPageConfig">
                        <div class="panel-body">
                            <h5><?= Yii::t("cms", "CONTENTS") ?></h5>
                            <p><?= Yii::t("cms", "Create a page and add it to the menu, so you can add your blocks, and even use a page template to generate a set of blocks") ?></p>
                            <div id="initialisationPage" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="initPageConfig">
                                  <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#initialisationPage" href="#initPage" aria-expanded="false" aria-controls="initPage">A - <?= Yii::t("cms", "Initializing a page") ?></a>
                                  </h5>
                                </div>
                                <div id="initPage" class="panel-collapse collapse" role="tabpanel" aria-labelledby="initPageConfig">
                                  <div class="panel-body">
                                    <ul class="sommaire" style="list-style: none;">
                                        <li class="sommaire-item">
                                            <h5>A1 - <?= Yii::t("cms", "Create a page") ?></h5>
                                            <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_1485d577eb6a5704ec19cac6eda6066c.png" class="thumb-info" data-title="<?= Yii::t("cms", "Create a page") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        </li>
                                        <li class="sommaire-item">
                                            <h5>A2 - <?= Yii::t("cms", "Edit a page") ?></h5>
                                            <p><?= Yii::t("cms", "There are two ways to modify a page") ?> :</p>
                                            <ul style="list-style: none;">
                                                <li>1. <?= Yii::t("cms", "Modify a page in the page list") ?> : <a href="https://codimd.communecter.org/uploads/upload_9cd516165a493bbf000648c0f9b56f14.png" class="thumb-info" data-title="<?= Yii::t("cms", "Modify a page in the page list") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                                <li>2. <?= Yii::t("cms", "Modify a page from the menu") ?> : <a href="https://codimd.communecter.org/uploads/upload_9be548362dbecb427afac29e1c46e381.png" class="thumb-info" data-title="<?= Yii::t("cms", "Modify a page from the menu") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                            </ul>
                                        </li>
                                        <li class="sommaire-item">
                                            <h5>A3 - <?= Yii::t("cms", "Add page to the menu") ?></h5>
                                            <p><?= Yii::t("cms", "There are two ways to add a page to the menu") ?> :</p>
                                            <ul style="list-style: none;">
                                                <li>1. <?= Yii::t("cms", 'Add from "Menus & Header" menu') ?> : <a href="https://codimd.communecter.org/uploads/upload_b3cdd0b41ff6e9ef2eebba1335e641b1.png" class="thumb-info" data-title="<?= Yii::t("cms", 'Add from "Menus & Header" menu') ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                                <li>2. <?= Yii::t("cms", "Add from menu if one or more menus already exist in the section (Example: Top menu, Right menu...)") ?> : <a href="https://codimd.communecter.org/uploads/upload_ac07a32b07404e7a9b004966389b692d.png" class="thumb-info" data-title="<?= Yii::t("cms", "Add from menu if one or more menus already exist in the section (Example: Top menu, Right menu...)") ?> " data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                            </ul>
                                        </li>
                                        <li class="sommaire-item">
                                            <h5>A4 - <?= Yii::t("cms", "Restrict access to a page of my costum") ?></h5>
                                            <p><?= Yii::t("cms", "Access to a page can be restricted to my community, depending on the user's link to my organization or project. A page can be made accessible only to") ?></p>
                                            <ul>
                                                <li><?= Yii::t("cms", "Members of the community") ?></li>
                                                <li><?= Yii::t("cms", "Administrators of the community") ?></li>
                                                <li><?= Yii::t("cms", "Or to members with a specific role") ?></li>
                                            </ul>
                                            <p class="img-view"><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_1cd420cac36e491ef5894f2cf381e1a0.png" class="thumb-info" data-title="<?= Yii::t("cms", "Restrict access to a page of my costum") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="constructionBlocConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#initialisationPage" href="#constructionBloc" aria-expanded="false" aria-controls="constructionBloc">B - <?= Yii::t("cms", "Building the page with blocks") ?></a>
                                    </h5>
                                </div>
                                <div id="constructionBloc" class="panel-collapse collapse" role="tabpanel" aria-labelledby="constructionBlocConfig">
                                    <div class="panel-body">
                                        <ul style="list-style: none;">
                                            <li>
                                                <h5>B1 - <?= Yii::t("cms", "Create a section") ?></h5>
                                                <p><?= Yii::t("cms", "To use CMS block elements. you need") ?> :</p>
                                                <ul>
                                                    <li><?= Yii::t("cms", "Click on the plus button") ?> <img src="https://codimd.communecter.org/uploads/upload_4b39ec56d4bc1353b3e15125e86c099b.png" alt="btn-plus"> <?= Yii::t("cms", "on a section or on the button") ?> <b><?= Yii::t("cms", "Add a block") ?></b> <?= Yii::t("cms", "on the left") ?></li>
                                                    <li><?= Yii::t("cms", "Select and drag to page.") ?></li>
                                                    <li><?= Yii::t("cms", "Click on an item to open its configuration menu,") ?></li>
                                                    <li><?= Yii::t("cms", "Configure to suit your needs.") ?></li>
                                                </ul>
                                                <p><?= Yii::t("cms", "Here's an example of a section containing a title and text with a background photo.") ?> <a href="https://codimd.communecter.org/uploads/upload_c72d3a1788d2e78bc38c117895aebcf4.png" class="thumb-info" data-title="<?= Yii::t("cms", "Here's an example of a section containing a title and text with a background photo") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                                <p><?= Yii::t("cms", "Watch a video tutorial") ?> : <a href="https://peertube.communecter.org/videos/watch/57668e80-a642-40c9-bc20-65c655891cff" class="tutoriel" target="_blank"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>B2 - <?= Yii::t("cms", "Select section structure.") ?></h5>
                                                <p><?= Yii::t("cms", "On a section, there is a list of structures") ?> : <a href="https://codimd.communecter.org/uploads/upload_93f34568c47a09ecc14537eddd49ad57.png" class="thumb-info" data-title="<?= Yii::t("cms", "list of structures") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                                <ul>
                                                    <li><?= Yii::t("cms", "Select and click for structured column") ?></li>
                                                    <li><?= Yii::t("cms", "Fill them according to your requirements.") ?></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <h5>B3 - <?= Yii::t("cms", "Copy and paste from block to block.") ?></h5>
                                                <ul>
                                                    <li><?= Yii::t("cms", "In case of repetitive content,") ?></li>
                                                    <li><?= Yii::t("cms", "Click on button") ?> <b><?= Yii::t("cms", "Copy") ?></b> <?= Yii::t("cms", "below the items in question") ?> : <a href="https://codimd.communecter.org/uploads/upload_c2b44da6392e23de4447391baf263403.png" class="thumb-info" data-title="<?= Yii::t("cms", "Click on a copy button to copy/paste") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li>
                                                    <li><?= Yii::t("cms", "Go to the place where you want to paste it,") ?></li>
                                                    <li><?= Yii::t("cms", "Click on button") ?>  <b><?= Yii::t("cms", "Paste") ?></b> <?= Yii::t("cms", "at the bottom of the elements where you want to place") ?></li>
                                                </ul>
                                                <p><?= Yii::t("cms", "Here's an example of a section created using 4 columns with text.") ?> <a href="https://codimd.communecter.org/uploads/upload_b3f1ce493678664ef5e4c71863940f0d.png" class="thumb-info" data-title="<?= Yii::t("cms", "Example of a section") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                                <p><?= Yii::t("cms", "Watch a video tutorial") ?> : <a href="https://peertube.communecter.org/videos/watch/534ea756-3757-4b24-8f6e-de86fc88f1e8" class="tutoriel" target="_blank"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>B4 - <?= Yii::t("cms", "How to personalize your section with text and images") ?>?</h5>
                                                <p><?= Yii::t("cms", "To get started") ?> :</p>
                                                <ul>
                                                    <li><?= Yii::t("cms", "Add a section") ?>,</li>
                                                    <li><?= Yii::t("cms", "Choose the section structure") ?>,</li>
                                                    <li><?= Yii::t("cms", "Drag text and image to column. Depending on the design you want") ?>,</li>
                                                    <li><?= Yii::t("cms", "Click on an item to open its configuration menu") ?>,</li>
                                                    <li><?= Yii::t("cms", "Configure to suit your needs") ?>.</li>
                                                </ul>
                                                <p><?= Yii::t("cms", "Here's an example of a 2-column section containing text and title with background color on the left and a round image with border on the right") ?>. <a href="https://codimd.communecter.org/uploads/upload_465df9419bd79029606b1531c7a20a94.png" class="thumb-info" data-title="<?= Yii::t("cms", "Here's an example of a 2-column section") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                                <p><?= Yii::t("cms", "Watch a video tutorial") ?> : <a href="https://peertube.communecter.org/videos/watch/55b6420b-8b9b-4036-9451-9bda4a963a02" class="tutoriel" target="_blank"><?= Yii::t("cms", "click here") ?></a></p>
                                            </li>
                                            <li>
                                                <h5>B5 - <?= Yii::t("cms", "Add a button to a section") ?></h5>
                                                <p><?= Yii::t("cms", "Two types of buttons: the simple button and the button with prefigured actions (popup) - to describe") ?></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end création d'une page --->

                <!-- Editer un block --->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="editBlockConfig">
                        <h5 class="panel-title">    
                            <a data-toggle="collapse" data-parent="#tabHelp" href="#editBlock" aria-expanded="false" aria-controls="editBlock">IV - <?= Yii::t("cms", "Editer un bloc") ?></a>
                        </h5>
                    </div>
                    <div id="editBlock" class="panel-collapse collapse" role="tabpanel" aria-labelledby="editBlockConfig">
                        <div class="panel-body">
                            <div id="selectionBlockContent" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="selectBlockConfig">
                                  <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#selectionBlockContent" href="#selectBlock" aria-expanded="false" aria-controls="selectBlock">A - <?= Yii::t("cms", "Select a block to edit") ?></a>
                                  </h5>
                                </div>
                                <div id="selectBlock" class="panel-collapse collapse" role="tabpanel" aria-labelledby="selectBlockConfig">
                                  <div class="panel-body">
                                    <ul style="list-style: none;">
                                        <li>
                                            <h5>A1 - <?= Yii::t("cms", "By clicking on the block in question") ?></h5>
                                            <p><?= Yii::t("cms", "When you hover over a block, it will have a blue border, and you just need to click on it to switch to editing") ?> : <a href="https://codimd.communecter.org/uploads/upload_814a5fbc158ca332e6f9528059a69c8f.png" class="thumb-info" data-title="<?= Yii::t("cms", "Select a block for editing: click on the block in question") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        </li>
                                        <li>
                                            <h5>A2 - <?= Yii::t("cms", "Click to use the block layer") ?></h5>
                                            <p><?= Yii::t("cms", "It's a three-step process") ?> : <a href="https://codimd.communecter.org/uploads/upload_6d3a2940a977e19dd935c17eba4b79ae.png" class="thumb-info" data-title="<?= Yii::t("cms", "Select a block for editing: click to use block layer") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                            <ul>
                                                <li><?= Yii::t("cms", 'Click on the "Add a block" button to open the left-hand panel') ?>.</li>
                                                <li><?= Yii::t("cms", "Go to the layer tab") ?>.</li>
                                                <li><?= Yii::t("cms", "Select the block to be modified in the layer") ?>.</li>
                                                <li><?= Yii::t("cms", "You can also press the") ?> <b>Tab</b> <img src="https://codimd.communecter.org/uploads/upload_522152a5aa2950f55eff1ce90a79bc23.png" alt="tab"> <?= Yii::t("cms", "to go directly to the layer tab") ?></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h5>A3 - <?= Yii::t("cms", "The select parent button also opens the parent block editor.") ?></h5>
                                            <p><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_c1d08a6809f1ed9de4acc50a49d50139.png" class="thumb-info" data-title="<?= Yii::t("cms", "Selecting a block for editing: by selecting the parent select button") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                        </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="panelEditionBlocConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#selectionBlockContent" href="#panelEditionBloc" aria-expanded="false" aria-controls="panelEditionBloc">B - <?= Yii::t("cms", "The block editing panel") ?></a>
                                    </h5>
                                </div>
                                <div id="panelEditionBloc" class="panel-collapse collapse" role="tabpanel" aria-labelledby="panelEditionBlocConfig">
                                    <div class="panel-body">
                                        <p><?= Yii::t("cms", "Once you've selected a block to edit, the panel on the right will open, containing all existing parameters for the block in question.") ?></p>
                                        <p><?= Yii::t("cms", "Overview") ?> : <a href="https://codimd.communecter.org/uploads/upload_1a489bf8158aba2c320559549221ddb2.png" class="thumb-info" data-title="<?= Yii::t("cms", "The block editing panel") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></p>
                                    </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="optionsPossibleConfig">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#selectionBlockContent" href="#optionsPossible" aria-expanded="false" aria-controls="optionsPossible">C - <?= Yii::t("cms", "Possible options for a block") ?></a>
                                    </h5>
                                </div>
                                <div id="optionsPossible" class="panel-collapse collapse" role="tabpanel" aria-labelledby="optionsPossibleConfig">
                                    <div class="panel-body">
                                        <p><?= Yii::t("cms", "Once you've selected a block to edit, there are a few things you can do to manipulate it.") ?></p>
                                        <img src="https://codimd.communecter.org/uploads/upload_7cd1cd68f065b0763f3fd4f643b37f29.png" alt="btn-bloc">
                                        <ul style="list-style: none; margin-top: 30px">
                                            <li>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_8ea71d242a4b7f0c74d8469cf834f1d3.png" style="margin-right: 15px" alt="btn-top"> <?= Yii::t("cms", "Move up: To move a block upwards") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_b5ce8dfe1d00e6de2380621b0eea135d.png" style="margin-right: 15px" alt="btn-bottom"> <?= Yii::t("cms", "Move down: To move a block downwards") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_f244747bff72f7bd9546ec9a129bcb8c.png" style="margin-right: 15px" alt="btn-edit"> <?= Yii::t("cms", "Edit: to access block editing") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_c228a5648a3389d20388c54109af5aeb.png" style="margin-right: 15px" alt="btn-to-parent"> <?= Yii::t("cms", "Select parent : To select the parent of the block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_13eaddc276dc5927771dee5ce1ef1828.png" style="margin-right: 15px" alt="btn-duplicate"> <?= Yii::t("cms", "Duplicate : To duplicate a block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_984a0b6df4a2b27fb272e6e2ba9800af.png" style="margin-right: 15px" alt="btn-copy"> <?= Yii::t("cms", "Copy: To copy a block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_defed7075f0464d3c63b390761d6e5f4.png" style="margin-right: 15px" alt="btn-paste"> <?= Yii::t("cms", "Paste: To duplicate a block copy") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_4bdc540aab8a411f138e8ba47325d512.png" style="margin-right: 15px" alt="btn-add-section"> <?= Yii::t("cms", "Add a section: To insert a new section at the bottom of the section") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_b7ad915cfe72a4c838a1065584cdeb74.png" style="margin-right: 15px" alt="btn-save"> <?= Yii::t("cms", "Save : To save a section as a reusable blocCms") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_eddf417ed6dab47eeb93e24f4c6b0268.png" style="margin-right: 15px" alt="btn-delete"> <?= Yii::t("cms", "Delete: To delete a block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_dd68eb7e4013e6c1a2bcdd186ec30e20.png" style="margin-right: 15px" alt="btn-deselect"> <?= Yii::t("cms", "Deselect: To deselect a block and exit its editing mode") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_75388bbe115abc141620116973981a4a.png" style="margin-right: 15px" alt="btn-deselect"> <?= Yii::t("cms", "Paste as mirror block : Paste a copy block as a mirror block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_2cde2bdfd0f99506a615db740e761ff6.png" style="margin-right: 15px" alt="btn-deselect"> <?= Yii::t("cms", "Block log : To trace editions applied to the block") ?></p>
                                                <p><img src="https://codimd.communecter.org/uploads/upload_d82bbdaba5fca01d321a104f43a9919c.png" style="margin-right: 15px" alt="btn-deselect"> <?= Yii::t("cms", "AI : To generate text or image") ?></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end editer un block --->

                <!-- responsive design --->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="responsiveEditionConfig">
                        <h5 class="panel-title">    
                            <a data-toggle="collapse" data-parent="#tabHelp" href="#responsiveEdition" aria-expanded="false" aria-controls="responsiveEdition">V - <?= Yii::t("cms", "Responsive Edition") ?></a>
                        </h5>
                    </div>
                    <div id="responsiveEdition" class="panel-collapse collapse" role="tabpanel" aria-labelledby="responsiveEditionConfig">
                        <div class="panel-body">
                            <p><?= Yii::t("cms", "It is possible to have responsive configurations for certain block settings") ?></p>
                            <p><?= Yii::t("cms", "Watch a video tutorial") ?> : <a href="https://peertube.communecter.org/videos/watch/6024b1aa-ceea-4875-81a5-eb37c9330912" class="tutoriel" target="_blank"><?= Yii::t("cms", "click here") ?></a></p>
                            <p><?= Yii::t("cms", "To do this, change the view mode and make the following changes") ?> :</p>
                            <ul>
                               <li><?= Yii::t("cms", "By using the change view button next to each Css property with a responsive configuration") ?> : <a href="https://codimd.communecter.org/uploads/upload_b4b95aaaa8dd84f248228d1632313e99.png" class="thumb-info" data-title="<?= Yii::t("cms", "Responsive design: using the change view button next to each Css property with a responsive configuration") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li> 
                               <li><?= Yii::t("cms", "Or by using the top button") ?> : <a href="https://codimd.communecter.org/uploads/upload_9ab3dd04c5ddf6330e1bc8f1c641cbef.png" class="thumb-info" data-title="<?= Yii::t("cms", "Responsive design: using the button at the top") ?>" data-lightbox="helpAndFeedback"><?= Yii::t("cms", "click here") ?></a></li> 
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end responsive design --->
            </div>
            <div class="tab-pane fade" id="tabQuestion">
                <h5><?= Yii::t("cms", "Have you checked whether the answer to your question can be found in the") ?> <a href="#tabHelp" data-toggle="tab" class="link-to-help"><?= Yii::t("cms", "Quick help") ?>?</a></h5>
                <form id="question_form" action="#" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputSubject" class="col-form-label"><?= Yii::t("cms", "Subject") ?></label>
                        <input type="text" class="form-control" id="inputSubject" placeholder="<?= Yii::t("cms", "Subject of your question") ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="inputQuestion" class="col-form-label"><?= Yii::t("cms", "Question") ?></label>
                        <textarea class="form-control" id="inputQuestion" rows="3" placeholder="<?= Yii::t("cms", "Write your question here...") ?>" required></textarea>
                    </div>
                    <hr class="separator">
                    <div class="btn-submit">
                        <p><?= Yii::t("cms", "Your question will be redirected to the communication platform") ?> <b>Rocket.Chat</b> <?= Yii::t("cms", "in the channel") ?> <b>cmsopen</b> : <a href="https://chat.communecter.org/channel/cmsopen" target="_blank"><?= Yii::t("cms", "Go to channel") ?></a></p>
                        <button class="btn btn-success" type="submit"><?= Yii::t("cms", "Send") ?></button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="tabFeedback">
                <form id="feedback_form" action="#" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="form-label"><?= Yii::t("cms", "Type") ?></label>
                        <div class="check-value">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typeFeedback" id="typeFeedback1" value="<?= Yii::t("cms", "Bug report") ?>" checked>
                                <label class="form-check-label" for="typeFeedback1">
                                    <?= Yii::t("cms", "Bug report") ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typeFeedback" id="typeFeedback2" value="<?= Yii::t("cms", "Feature report") ?>">
                                <label class="form-check-label" for="typeFeedback2">
                                    <?= Yii::t("cms", "Feature report") ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="typeFeedback" id="typeFeedback3" value="<?= Yii::t("cms", "Praise") ?>">
                                <label class="form-check-label" for="typeFeedback3">
                                    <?= Yii::t("cms", "Praise") ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSubjectFeedback" class="col-form-label"><?= Yii::t("cms", "Subject") ?></label>
                        <input type="text" class="form-control" id="inputSubjectFeedback" placeholder="<?= Yii::t("cms", "Subject of your feedback") ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="inputFeedBackDescription" class="col-form-label"><?= Yii::t("cms", "Feedback") ?></label>
                        <textarea class="form-control" id="inputFeedBackDescription" rows="3" placeholder="<?= Yii::t("cms", "Write your feedback here...") ?>" required></textarea>
                    </div>
                    <hr class="separator">
                    <div class="btn-submit">
                        <p><?= Yii::t("cms", "Your feedback will be redirected to the communication platform") ?> <b>Rocket.Chat</b> <?= Yii::t("cms", "in the channel") ?> <b>cmsopen</b> : <a href="https://chat.communecter.org/channel/cmsopen" target="_blank"><?= Yii::t("cms", "Go to channel") ?></a></p>
                        <button class="btn btn-success" type="submit"><?= Yii::t("cms", "Send") ?></button>
                    </div>
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".modal-boostrap-help-and-feedback .tab-item").click(function() {
            var value = $(this).attr("href");
            $(".modal-boostrap-help-and-feedback .tab-item").removeClass("active");
            $(this).addClass("active");
            $(".modal-boostrap-help-and-feedback "+value+"").addClass("in active");
			$("#openModalHelpAndFeedback #question_form, #openModalHelpAndFeedback #feedback_form").trigger('reset');
        })

        $(".modal-boostrap-help-and-feedback .link-to-help").click(function() {
            $(".modal-boostrap-help-and-feedback .tab-item").removeClass("active");
            $(".modal-boostrap-help-and-feedback a[href='#tabHelp'].tab-item").addClass("active");
        })

        $("#question_form").submit(function(e) {
            e.preventDefault();
            var sujet = $("#question_form #inputSubject").val();
            var message = $("#question_form #inputQuestion").val();
            ajaxPost(null, baseUrl + '/survey/answer/rcnotification/action/questioncms', {
                sujet : sujet,
                message : message,
                costumSlug: costum.contextSlug,
                channelChat: "cmsopen"
            }, function(){
                toastr.success("<?= Yii::t("cms", "The message has been sent successfully") ?>");
                $("#openModalHelpAndFeedback").modal("hide");
            });
        });

        $("#feedback_form").submit(function(e) {
            e.preventDefault();
            var type = $("#feedback_form input[name='typeFeedback']").val();
            var sujet = $("#feedback_form #inputSubjectFeedback").val();
            var feedback = $("#feedback_form #inputFeedBackDescription").val();
            ajaxPost(null, baseUrl + '/survey/answer/rcnotification/action/feedbackcms', {
                type: type,
                sujet : sujet,
                feedback : feedback,
                channelChat: "cmsopen"
            }, function(){
                toastr.success("<?= Yii::t("cms", "The message has been sent successfully") ?>");
                $("#openModalHelpAndFeedback").modal("hide");
            });
        });
    });
</script>