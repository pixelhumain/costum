<!-- ******************** Proposer des thÃ¨mes suivant catÃ©gorie ****************
Stepper THEMATIC
1- Votre costum est-il axÃ© sur une territoire ? 
    Si oui -> montrer la liste de costum taggÃ© smarterre
2- Voulez-vous mettre en place un site pour votre communautÃ© ? 
    Si oui -> montrer des exemples de sites de communautÃ©s
3 - Quel est la thÃ©matique de votre costum ? 
    Proposer une liste : TiersLieux, Commons, Smarterre, Observatory, Cocity, citizenPower, AppelAProjet, technologicalGeek, socialMapping, EnvironmentChange, Institution, ArtsAndActivity, CompagnyPrez
    => Suivant les thÃ©matiques sÃ©lectionner affichÃ©es les templates correspondant + Ajouter des metatags 
    ********************************************************************************
-->
<style type="text/css">
    #modalFirstStepCmsCostum{
        background-color: white;
        z-index:999999;
        font-size: 18px;
        top: 40px;
    }
    #modalFirstStepCmsCostum .startCostumizer{
        color: white;
        background-color: #086784;
        font-size: 24px;
        border-color: #086784;
        font-weight: 700;
        border-radius: 10px;
        padding: 7px 25px;
    }
    #modalFirstStepCmsCostum .startCostumizer:hover{
        color: #086784;
        background-color: white;
        font-size: 24px;
        font-weight: 700;
        border-radius: 10px;
        padding: 7px 25px;
    }
    #modalFirstStepCmsCostum .modal-dialog{
        width: 100%;
        height: 100%;
        margin: 0px;
    }
    #modalFirstStepCmsCostum  .modal-content{
        padding:0;
    }
    #modalFirstStepCmsCostum .modal-menu{
        position : relative;
        float: left;
        width: 25%;
        padding : 5px;
        height: calc(100vh - 40px);
        display: inline-grid;
        align-content: center;
        background-color:#086784;
    }
    #modalFirstStepCmsCostum .modal-body{
        width: 75%;
        height: calc(100vh - 40px);
        float: left;
        overflow: auto;
    }
    #modalFirstStepCmsCostum .modal-dialog .firstStepSwipping{
        height: 600px;
        width: 100%;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide{
        text-align: center;
        display: grid;
        align-items: center;

    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .content-top{
        align-self: end;
        padding: 20px;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .content-top h1{
        font-size: 18px;
        color:  #086784;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .content-main{
        display: inline-flex;
        align-self: baseline;
        padding: 20px;
        justify-content: center;
        width: 100%;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum{
        border: 1px solid #6d7a86;
        border-radius: 8px;
        padding: 20px 40px;
        font-size: 18px;
        width: 30%;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum:hover{
        border: 1px solid #6d7a86;
        background: 1px solid #6d7a86;
        color: white;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum.btn-white-to-default:hover{
        border: 1px solid #6d7a86;
        background: #6d7a86;
        color: white;
    }

    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum.btn-white-to-red:hover{
        border: 1px solid #086784;
        background: #086784;
        color: white;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum.btn-red-to-white{
        border: 1px solid #086784;
        background: #086784;
        color: white;
    }
    #modalFirstStepCmsCostum .firstStepSwipping .swiper-slide .btn-step-costum.btn-red-to-white:hover{
        border: 1px solid #086784;
        background: white;
        color: #086784;
    }
    .firstStepSwipping .swiper-control .button-swipper{
        border: 1px solid #324553;
        border-radius: 6px;
        padding: 10px 20px;
        font-size: 14px;
        width: fit-content;
    }
    .firstStepSwipping .swiper-control .button-swipper:hover{
        text-decoration: none;
        box-shadow: 1px 1px 1px 0px #b1adad;
    }
    .firstStepSwipping .swiper-control .button-swipper.swiper-to-next{
        border: 1px solid #086784;
        background: #086784;
        color: white;
    }
    .firstStepSwipping .swiper-control .button-swipper.swiper-to-next.disable{
        border: 1px solid #d47e8a;
        background: #d47e8a;
        cursor: not-allowed;
    }
    .swiper-pagination-progressbar .swiper-pagination-progressbar-fill{
        background: #086784;
    }
    .swiper-container-horizontal>.swiper-pagination-progressbar{
        height: 5px;
    }
    .firstStepSwipping .parentSitemapOptions {
        padding: 6px;
        width: 33%;
        display: inline-flex;
    }
    .firstStepSwipping .parentSitemapOptions .box-mockup{
        margin-bottom: 0px;
        margin-top: 0px;
    }
    .firstStepSwipping .parentSitemapOptions.active .box-mockup{
        border-style: solid;
        border-color: #086784;
        box-shadow: 0 0 5px #086784;
    }
    .firstStepSwipping .parentSitemapOptions.active .box-mockup .title{
        border: 2px solid #086784;
        color: #086784;
    }

    .firstStepSwipping .pages-genarator{
        width: 80%;
    }
    .firstStepSwipping .page-to-cms.badge {
        background-color: #086784;
        box-shadow: none;
        padding: 8px 8px 8px 22px;
        margin: 8px 0px 0px 8px;
        font-size: 15px;
        color: white;
        border: 1px #086784 solid;
    }
    .firstStepSwipping .page-to-cms.badge i{
        cursor: pointer;
        margin-right:5px;
    }
    .firstStepSwipping .form-input-page{
        position: relative;
        margin-top: 30px;
    }
    .firstStepSwipping .form-input-page .addPage{
        caret-color :#086784 ;
        color :#086784;
        font-size: 20px !important;
        box-shadow: 0px 1px 1px 0px gray;
        border: transparent;
        width: 100%;
    }
    .firstStepSwipping .form-input-page .placeholder{
        position: absolute;
        left: 10px;
        top:0px;
        color :#086784;
        font-size: 20px !important;
    }
    .firstStepSwipping .typeObjActivation, .firstStepSwipping .appHelpersActivation{
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        margin-left: auto;
        margin-right: auto;
    }
    .firstStepSwipping .typeObjActivation > *, .firstStepSwipping .appHelpersActivation > *{margin: .5rem 0.5rem;}
    .firstStepSwipping .checkbox-input {
        clip: rect(0 0 0 0);
        clip-path: inset(100%);
        height: 1px;
        overflow: hidden;
        position: absolute;
        white-space: nowrap;
        width: 1px;
    }
    .firstStepSwipping .checkbox-input:checked + .checkbox-tile {
        border-color: #086784;
        box-shadow: 0 5px 10px rgba(#000, 0.1);
        color: #2260ff;
    }
    .firstStepSwipping .checkbox-input:checked + .checkbox-tile:before {
        transform: scale(1);
        opacity: 1;
        background-color: #086784;
        border-color: #086784;
    }
    .firstStepSwipping .checkbox-input:checked + .checkbox-tile .checkbox-icon,
    .firstStepSwipping .checkbox-input:checked + .checkbox-tile .checkbox-label {
        color: #086784;
    }

    .firstStepSwipping .checkbox-tile {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: 100%;
        min-height: 100%;
        padding: 20px;
        border-radius: 11px;
        border: 2px solid #b5bfd9;
        background-color: #fff;
        box-shadow: 0 5px 10px rgba(#000, 0.1);
        transition: 0.15s ease;
        cursor: pointer;
        position: relative;
    }

    .firstStepSwipping .checkbox-tile:before {
        content: "";
        position: absolute;
        display: block;
        width: 15px;
        height: 15px;
        border: 2px solid #b5bfd9;
        background-color: #fff;
        border-radius: 50%;
        top: 6px;
        left:8px;
        opacity: 0;
        transform: scale(0);
        transition: 0.25s ease;
    }
    .firstStepSwipping .checkbox-tile:hover:before {
        transform: scale(1);
        opacity: 1;
    }

    .firstStepSwipping .checkbox-icon {
        transition: .375s ease;
        color: #494949;
        height: 40px;
    }
    .firstStepSwipping .checkbox-icon i{
        font-size: 30px;
    }
    .firstStepSwipping .checkbox-label{
        color: #707070;
        transition: .375s ease;
        text-align: center;
        width: 120px;
    }
    .firstStepSwipping .card_loader{
        width:20%;
        margin-top: 0px;
    }
    .firstStepSwipping .card_loader .box-loader{
        height:150px;
    }
    .firstStepSwipping .card_loader .newtons-cradle-img {
        width: 75px;
    }
    .firstStepSwipping .horizontal_bar .horizontal_bar_logo img {
        width: 80px;
    }
    .firstStepSwipping .card_loader .horizontal_bar .horizontal_bar_loader{
        height:5px;
    }
    .firstStep-loader-choose > div{
        width: 100%;
        margin: 0px !important;
        padding: 0px;
    }
    .firstStep-loader-choose .card_loader.active .loader_section {
        box-shadow: 0 0 5px #086784;
        border-bottom: 5px #086784 solid;
        border-radius: 10px;
        border-right: 2px dotted #086784;
        border-left: 2px #086784 dotted;
        border-top: 2px #086784 dotted;
        background-color: #086784;
    }
    .tree-structure{
        list-style: none;
        clear: both;
        padding-left: 15px;
        margin-top: -70px;
    }
    .tree-structure li {
        position: relative;
        height: 70px;
    }
    .tree-structure li a{
        font-weight: normal;
        color: #ffffffc4;
        text-decoration: none;
        font-weight: 700;
        vertical-align: middle;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.2s ease-in-out;
        display: inline-block;
        max-width: calc(100% - 50px);
        vertical-align: top;
    }
    .tree-structure li a:hover{
        padding-left: 5px;
    }


    .tree-structure > li:last-child > .bullet:after{
        height: 0px;
    }
    .tree-structure ol{
        padding: 20px 0 20px 45px;
    }
    .tree-structure ol li{
        list-style-type: none;
        padding: 8px 0
    }
    .tree-structure ol li .bullet{
        position: relative;
    }
    .tree-structure ol li a{
        color: #000;
        font-weight: normal;
    }
    .tree-structure .bullet{
        background-color: #f9d0d6;
        width: 25px;
        height: 25px;
        border-radius: 100%;
        padding-left: 0px;
        padding-right: 0px;
        padding: 3px 9px;
        color: #fff;
        float: left;
        font-weight: 700;
        font-size: 12px;
        display: inline-block;
        vertical-align: middle;
    }
    .tree-structure li.active .bullet{
        width: 27px;
        height: 27px;
        border: 4px solid white;
        background-color: #086784;
    }
    .tree-structure li.active a{
        color: white;
    }
    .tree-structure > li > .bullet:after{
        position: absolute;
        content: "";
        width: 1px;
        height: 45px;
        /* bottom: 25px; */
        background-color: #f9d0d6;
        top: 25px;
        z-index: 2;
    }
    .tree-structure  ol  li .bullet:before{
        position: absolute;
        content: "";
        top: 0;
        bottom: 0;
        right: 100%;
        margin: auto;
        width: 1px;
        height: 100%;
        background-color: #ffffffc4;
    }
    .img-construct {
        width: 70%;
        margin-left: 15%;
        bottom: 20px;
        position: absolute;
    }
    .contain-info-costum {
        top: 0px;
        position: absolute;
        margin-top: 30px;
        margin-bottom: 30px;
        width: 100%;
    }
    .contain-info-costum img.logo-info {
        width: 75px;
        height: 75px;
        border: solid 2px rgba(255, 255, 255);
        border-radius: 50%;
    }
    .contain-info-costum .title-info {
        font-size: 18px;
        color: #fff;
        margin-left: 10px;
    }
</style>
<div class="modal fade portfolio-modal" role="dialog" id="modalFirstStepCmsCostum">
    <div class="modal-dialog">
        <div class="modal-content">
            <div onclick='costumizer.actions.changeAndSave(`firstStepper`, `$unset`);' class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-menu">
                <div class="contain-info-costum">
                    <?php
                        $themeParams=$this->costum;
                        $logoCostum = isset($themeParams["logo"]) ? $themeParams["logo"] : Yii::app()->getModule("costum")->assetsUrl."/cmsBuilder/img/costum.png";
                    ?>
                    <img class="logo-info" src="<?php echo $logoCostum ?>">
                    <span class="title-info"><?php echo $themeParams["title"] ?></span>
                </div>
                <ol class="tree-structure">
                </ol>
                <img class="img-construct" src="<?= Yii::app()->getModule("costum")->assetsUrl ?>/cmsBuilder/img/construction_costum.png">
            </div>
            <div class="modal-body center text-dark">
                <!-- Slider main container -->
                <div class="firstStepSwipping">
                    <!-- Additional required wrapper -->

                    <!-- <div class="swiper-pagination-progressbar-fill"></div> -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide" data-menu="<?= Yii::t("cms","Welcome on board") ?>" data-control="prev:false;next:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms","Welcome on board") ?></h1>
                                <span><?= Yii::t("cms","Let's introduce you potential of this Costum tool with images") ?></span>
                            </div>
                            <div class="content-main">
                                <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Le Pixel Humain réseau social citoyen libre (court)" src="https://peertube.communecter.org/videos/embed/ed56cafe-7f12-452a-a8e8-1324b5faef1b" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="swiper-slide" data-key="langChooser" data-menu="<?= Yii::t("cms", "General language of the site") ?>" data-control="prev:true;next:true">
                            <div class="title content-top">
                                <h1></h1>
                            </div>
                            <div class="content-main">
                                <div class="siteLangOnCostumizer" style="font-size: 25px;color: #086784;"></div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-key="siteMapOptions" data-menu="<?= Yii::t("cms", "Choose your site layout") ?>" data-control="prev:true;next:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms", "Choose your site layout") ?></h1>
                            </div>
                            <div class="content-main">
                                <div class="sitemapOnCostumizer"></div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-key="pageSelecteur" data-menu="<?= Yii::t("cms", "Add page you need") ?>" data-control="prev:true;next:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms", "Add page you need") ?></h1>
                            </div>
                            <div class="content-main">
                                <div class="pages-genarator"></div>
                            </div>
                        </div>
                       <!--  <div class="swiper-slide" data-key="loaderChooser" data-menu="<?= Yii::t("cms","Activate the apps you want on your costum") ?>" data-control="prev:true;next:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms","Activate the apps you want on your costum") ?></h1>
                            </div>
                            <div class="content-main">
                                <div class="firstStep-loader-choose"></div>
                            </div>
                        </div> -->
                        <div class="swiper-slide" data-key="typeObj" data-menu="<?= Yii::t("cms", "What actors, initiatives or resources will your costum map ?") ?>" data-control="prev:true;next:true">
                                <div class="title content-top">
                                    <h1><?= Yii::t("cms", "What actors, initiatives or resources will your costum map ?") ?></h1>
                                    <span><?= Yii::t("cms","Activate the elements you want to reference to show the activity of your territory, theme or community ?") ?></span>
                                </div>
                                <div class="content-main">
                                    <div class="typeObjActivation"></div>
                                </div>
                        </div>
                        <div class="swiper-slide" data-key="userSubscription" data-menu="<?= Yii::t("cms","Will your costum allow user registration?") ?>" data-control="prev:true;next:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms","Will your costum allow user registration?") ?></h1>
                                <span><?= Yii::t("cms","Users will be able to register and take part in referencing the life of a territory. They can create communities, take part in calls for projects and act on the many tools at your disposal.") ?></span>
                            </div>
                            <div class="content-main">
                                <a href="javascript:;" data-value="1"  data-path="typeObj.citoyens" class="btn btn-add-citoyen btn-step-costum btn-red-to-white"><?= Yii::t("common","Yes"); ?></a>
                                <a href="javascript:;" data-value="0"  data-path="typeObj.citoyens" class="btn btn-add-citoyen btn-step-costum btn-white-to-default"><?= Yii::t("common","No"); ?></a>
                            </div>
                        </div>

                        <div class="swiper-slide" data-key="loaderConfig" data-menu="<?= Yii::t("cms", "Select your loader") ?>" data-control="prev:true;next:true;finish:false">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms", "Select your loader") ?></h1>
                            </div>
                            <div class="content-main sp-cms-std">                                                   
                                <div class="chooserLoaderCostum sp-cms-100 can-reset"></div>
                            </div>
                        </div>
                        <div class="swiper-slide" data-key="colorOptions" data-menu="<?= Yii::t("cms", "Select your main colors") ?>" data-control="prev:true;end:true">
                            <div class="title content-top">
                                <h1><?= Yii::t("cms", "Select your main colors") ?></h1>
                            </div>
                            <div class="content-main">
                                <div class="chooserColerCostum"></div>
                            </div>
                        </div>
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="swiper-control">
                        <a href="javascript:;" class="button-swipper swiper-to-prev disable"><?= Yii::t("common", "Back"); ?></a>
                        <a href="javascript:;" class="button-swipper swiper-to-next disable"><?= Yii::t("common", "Next"); ?> <i class="fa fa-arrow-circle-o-right"></i></a>
                        <a href="javascript:;" class="button-swipper swiper-to-end disable" style="display:none;"><?= Yii::t("common", "End"); ?> <i class="fa fa-arrow-circle-o-right"></i></a>

                    </div>
                    <!-- If we need scrollbar -->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var firstStepCostum = {
        swipper: {},
        swipper2: {},
        options : {
            step : 0,
            pathToApp:''
        },
        init : function(){
            if(typeof costum.firstStepper == "object"){
                this.options=costum.firstStepper;
            }
            this.actions.init();
            this.events.init();
            this.views.init();
        },
        events : {
            init : function(){
                this.initSwipper();
                this.changeView();
            },
            initSwipper : function(){
                firstStepCostum.swipper = new Swiper('.firstStepSwipping', {
                    // Navigation arrows
                    /*navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    },*/
                    theme : {
                        color : "#086784"
                    },
                    allowTouchMove : false,
                    parallax:true,
                    // pagination : {
                    //     el : ".swiper-pagination-progressbar-fill",
                    //     type:"progressbar",
                    //     progressbarFillClass: "swiper-pagination-progressbar-fill",
                    //     renderProgressbar: function (progressbarFillClass) {
                    //         return '<span class="' + progressbarFillClass + '"></span>';
                    //     }
                    // }
                });
                firstStepCostum.swipper2 = new Swiper('.swipper2', {
                    // Navigation arrows
                    /*navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    },*/
                    theme : {
                        color : "#086784"
                    },
                    direction: "vertical",

                    allowTouchMove : true,
                    parallax:true
                    /*pagination : {
                        el : ".swiper-pagination-progressbar-fill",
                        type:"progressbar",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        renderProgressbar: function (progressbarFillClass) {
                            return '<span class="' + progressbarFillClass + '"></span>';
                        }
                    }*/
                });
                this.slideToNextAndPrev();

            },
            slideToNextAndPrev : function(){
                $(".swiper-to-prev").off().on("click", function(){
                    firstStepCostum.swipper.slidePrev(500, function(){});
                });
                $(".swiper-to-next").off().on("click", function(){
                    firstStepCostum.swipper.slideNext(500, function(){});

                });
                $(".swiper-to-end").off().on("click", function(){
                    costumizer.actions.changeAndSave("firstStepper", "$unset");
                    $(".close-modal").trigger("click");
                });
            },
            siteMapOptions: function(){
                costumizer.space="htmlConstruct";
                costumizer.htmlConstruct.events.changeSitemap(function(){
                    $(".swiper-control .swiper-to-next").removeClass("disable");
                });
            },
            changeView: function(){
                firstStepCostum.swipper.on('slideChange', function(e) {
                    if(e.activeIndex > firstStepCostum.options.step){
                        costumizer.space="generalInfos";
                        costumizer.actions.changeAndSave("firstStepper.step", e.activeIndex);
                    }
                    firstStepCostum.actions.setControlOptions(e.activeIndex);
                    firstStepCostum.actions.activeMenu(e.activeIndex);
                    if(typeof $(".swiper-slide-"+e.activeIndex).data("key") != "undefined"){
                        var keySlide=$(".swiper-slide-"+e.activeIndex).data("key");
                        if(typeof firstStepCostum.views[keySlide] == "function")
                            firstStepCostum.views[keySlide]();
                        if(typeof firstStepCostum.events[keySlide] == "function")
                            firstStepCostum.events[keySlide]();
                        if(typeof firstStepCostum.actions[keySlide] == "function")
                            firstStepCostum.actions[keySlide]();
                    }
                });
            },
            typeObj : function(){
                $(".typeObjActivation .checkbox-input").off().on("change", function(e){
                    valCheck = ($(this).is(":checked")) ? true : false;
                    costumizer.actions.changeAndSave("typeObj."+$(this).data("key")+".add", valCheck);
                });
            },
            appHelpers : function(){
                $(".appHelpersActivation .checkbox-input").off().on("change", function(e){
                    valCheck = ($(this).is(":checked")) ? true : '$unset';
                    costumizer.actions.changeAndSave("app."+$(this).data("key"), valCheck);
                });
            },
            pageSelecteur : function(){
                //$(".form-input-page .addPage").focus();
                var pathCheckIfisArrayEmpty = (costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "header.menuTop.left.buttonList.app.buttonList" : "menuLeft.buttonList.app.buttonList";
                if (!notEmpty(jsonHelper.getValueByPath(costumizer.obj, "htmlConstruct."+pathCheckIfisArrayEmpty))) {
                    jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathCheckIfisArrayEmpty, {});
                }
                $(".firstStepSwipping .form-input-page .addPage").off().on('keyup', function (event) {
                    if ((event.key === 'Enter' || event.keyCode === 13) && notEmpty($(this).val())) {
                        var newPage   = $(this).val();
                        var keyPage   = "#"+slugify(newPage).toLowerCase();
                        var labelLang = costum.language ? costum.language : "fr";
                        var formData  = {
                            "urlExtra" : "/page/"+slugify(newPage).toLowerCase(),
                            "hash":"#app.view"
                        }
                        formData.name = {}
                        formData.name[labelLang] = newPage

                        var pathToMenu=(costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "header.menuTop.left.buttonList.app.buttonList" : "menuLeft.buttonList.app.buttonList";
                        jsonHelper.setValueByPath(themeParams.pages, keyPage, formData);
                        jsonHelper.setValueByPath(costum, "appConfig.pages." + keyPage, formData);
                        jsonHelper.setValueByPath(urlCtrl.loadableUrls, keyPage, formData);
                        costumizer.actions.changeAndSave("app."+keyPage, formData);
                        costumizer.actions.changeAndSave("htmlConstruct."+pathToMenu+"."+keyPage, true);
                        var objToRefresh={}
                        objToRefresh["htmlConstruct."+pathToMenu]=true;
                        costumizer.htmlConstruct.actions.refreshmenu(objToRefresh, costumizer.obj.htmlConstruct);
                        $(".page-already-generated").append(firstStepCostum.views.addSimplePage(keyPage, newPage));
                        firstStepCostum.events.deleteSimplePage();
                        $(this).val("");
                        $(".form-input-page .placeholder").show();
                    }else{
                        if(notEmpty($(this).val()))
                            $(".form-input-page .placeholder").hide();
                        else
                            $(".form-input-page .placeholder").show();
                    }
                });
                firstStepCostum.events.deleteSimplePage();
            },
            deleteSimplePage : function(){
                $(".page-already-generated .page-to-cms > i").off().on("click", function(){
                    costumizer.pages.actions.deletePage([$(this).parent().data("key")]);
                    $(this).parent().fadeOut(300).remove();
                });
            },
            userSubscription : function(){
                $(".firstStepSwipping .btn-add-citoyen").off().on("click", function(){
                    if($(this).data("value")){
                        var pathToMenu=(costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "header.menuTop.right.buttonList" : "menuLeft.buttonList";
                        var objToImplement = {
                            "login": true,
                            "userProfil" : {
                                "img" : true,
                                "name" : true,
                                "dashboard":true
                            }
                        };
                        costumizer.actions.changeAndSave("htmlConstruct."+pathToMenu, objToImplement);
                        var objToRefresh={}
                        objToRefresh["htmlConstruct."+pathToMenu]=objToImplement;
                        costumizer.htmlConstruct.actions.refreshmenu(objToRefresh, costumizer.obj.htmlConstruct);
                        //$(".page-already-generated").append(firstStepCostum.views.addSimplePage(keyPage, newPage));
                    }else{
                        var pathToMenu=(costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "header.menuTop.right.buttonList" : "menuLeft.buttonList";
                        var objToImplement = {
                            "login": false,
                        };
                        costumizer.actions.changeAndSave("htmlConstruct."+pathToMenu, objToImplement);
                        var objToRefresh={}
                        objToRefresh["htmlConstruct."+pathToMenu]=objToImplement;
                        costumizer.htmlConstruct.actions.refreshmenu(objToRefresh, costumizer.obj.htmlConstruct);
                    }
                    firstStepCostum.swipper.slideNext(500, function(){});
                });
            },
            // loaderChooser : function(){
            //     costumizer.loader.events.bind();
            // },
            loaderInit : function(){
                $(".activeLoader").off().on("click", function (e) {
                    valueLoader = $(this).data("name");
                    var finalPath = costumizer.obj;
                    var path = "css.loader.loaderUrl";
                    jsonHelper.setValueByPath(costumizer.obj, path, valueLoader);
                    costumizer.actions.change(path);
                    costumizer.actions.update();
                    firstStepCostum.views.loaderOptions()
                });
            }
        },
        views:{
            init: function(){
                firstStepCostum.actions.setControlOptions(firstStepCostum.options.step);
                for (var i = 0; i <=firstStepCostum.options.step ; i++) {
                    $(".menu-inc-"+i).show();
                }
                firstStepCostum.actions.activeMenu(firstStepCostum.options.step);
                if(firstStepCostum.options.step > 0)
                    firstStepCostum.actions.changeView(firstStepCostum.options.step);
            },
            siteMapOptions: function(){
                costumizer.htmlConstruct.init();
                $(".sitemapOnCostumizer").html(costumizer.htmlConstruct.views.configOptions(true));
                if($(".sitemapOnCostumizer .parentSitemapOptions.active").length >=0)
                    $(".swiper-control .swiper-to-next").removeClass("disable");
            },
            addSimplePage : function(key, name){
                return "<span class='page-to-cms badge' data-key='"+key+"'><i class='fa fa-times-circle'></i> "+name+"</span>";
            },
            pageSelecteur : function(){
                costumizer.space="htmlConstruct";
                var pageInitHtml="";
                var labelLang = costum.language ? costum.language : "fr";
                if(typeof costum.app != "undefined"){
                    $.each(costum.app, function(e, v){
                        if(typeof v.name != "undefined"){
                            if (typeof v.name[labelLang] == "undefined") 
                                pageInitHtml+=firstStepCostum.views.addSimplePage(e, Object.values(v.name)[0]);
                            else
                                pageInitHtml+=firstStepCostum.views.addSimplePage(e, v.name[labelLang]);
                        }
                    });
                }
                var str="<div class='page-already-generated'>"+pageInitHtml+"</div>"+
                        "<div class='form-input-page'>"+
                            "<input type='text' class='addPage'>"+
                            "<span class='placeholder'> "+tradCms.writeNameOfPage+"</span>"+
                        "</div>";
                    $(".pages-genarator").html(str);
                    $(".form-input-page .placeholder").click(function(){$(".addPage").focus()})
            },
            // loaderChooser : function(){
            //     $(".firstStep-loader-choose").html(costumizer.loader.views.init());
            // },
            langChooser : function() {
                var defaultValues = costumizer.obj;
                new CoInput({
                    container : ".siteLangOnCostumizer",
                    inputs:[
                    {
                        type: "select",
                        options: {
                            name: "language",
                            label: tradCms.language,
                            isSelect2: true,
                            options: $.map(themeParams.DeeplLanguages, function (value, key) {
                                return {
                                    label: tradCms[value["label"]],
                                    value: key.toLowerCase()
                                };
                            }),
                            icon : "language",
                            class : "new-feature",
                            defaultValue : (typeof costum.language != "undefined") ? costum.language : ""
                        },
                    },
                    {
                        type: "selectMultiple",
                        options: {
                            name: "otherLanguages",
                            label: "Autre language",
                            options: $.map(themeParams.DeeplLanguages, function (value, key) {
                                return {
                                    label: tradCms[value["label"]],
                                    value: key.toLowerCase()
                                };
                            }),
                            icon : "globe",
                            class : "new-feature",
                            defaultValue : (typeof defaultValues["otherLanguages"] != "undefined") ? defaultValues["otherLanguages"] : []
                        }
                    }

                    ],
                    onchange:function(name, value, payload){
                        var path = (typeof payload != "undefined" && typeof payload.path != "undefined") ? payload.path : name;
                        ajaxPost(
                            null,
                            baseUrl+"/"+moduleId+"/cms/updatecostum",
                            {
                                params : {[name]: value}
                            },
                            function(data){
                                toastr.success(tradCms.updateConfig);
                                costum[name] = value
                                
                            },
                            null, 
                            "json"      
                            );
                        
                    }
                })
            },
            colorOptions : function(){
                new CoInput({
                    container : ".chooserColerCostum",
                    inputs:[
                        {
                            type : "colorPicker",
                            options : {
                                label : "Couleur principale",
                                name : "color1",
                                defaultValue : ((typeof costum.css.color != "undefined" && typeof costum.css.color.color1 != "undefined") ?  costum.css.color.color1 : "")
                            }
                        },
                        {
                            type : "colorPicker",
                            options : {
                                label : "Couleur secondaire",
                                name : "color2",
                                defaultValue : ((typeof costum.css.color != "undefined" && typeof costum.css.color.color2 != "undefined") ?  costum.css.color.color2 : "")
                            }
                        },
                        {
                            type : "colorPicker",
                            options : {
                                label : "Couleur tertiaire",
                                name : "color3",
                                defaultValue : ((typeof costum.css.color != "undefined" && typeof costum.css.color.color3 != "undefined") ?  costum.css.color.color3 : "")
                            }
                        }
                    ],
                    onchange:function(name, value, payload){

                        costumizer.space="design";
                        if(name=="color1"){
                            if(costumizer.htmlConstruct.actions.isActivated("header.menuTop")){
                                costumizer.actions.changeAndSave("css.menuTop.background", value);
                                cssHelpers.render.addStyleUI("css.menuTop.background", value, "cosDyn");
                                costumizer.actions.changeAndSave("css.title.h1.color", value);
                            }
                            if(costumizer.htmlConstruct.actions.isActivated("menuLeft")){
                                costumizer.actions.changeAndSave("css.menuLeft.background", value);
                                cssHelpers.render.addStyleUI("css.menuLeft.background", value, "cosDyn");
                            }
                            costumizer.actions.changeAndSave("css.loader.ring1.color", value);
                        }
                        if(name=="color2"){
                            costumizer.actions.changeAndSave("css.title.h2.color", value);
                            costumizer.actions.changeAndSave("css.loader.ring2.color", value);
                        }
                        costumizer.actions.changeAndSave("css.color."+name, value);

                    }
                })
            },
             loaderConfig : function() {
                costumizer.design.views.loaderView()
                firstStepCostum.views.loaderOptions()
            },
            loaderOptions : function(){
                var loaderHtml = costumizer.loader.views.init()
                $(".chooserLoaderCostum").html(loaderHtml);  
                firstStepCostum.events.loaderInit()              
            },
            typeObj: function(){
                costumizer.space="typeObj";
                var str="";
                $.each(["organization", "project", "event", "poi"], function(e, v){
                    var checkedAttr=(typeof costum.typeObj[typeObj[v].col] != "undefined" && notNull(costum.typeObj[typeObj[v].col])) ? "checked": "";
                    str+="<div class='clickAndActive' data-key='"+typeObj[v].col+"' >"+
                        '<label class="checkbox-wrapper">'+
                        '<input type="checkbox" data-key="'+typeObj[v].col+'" class="checkbox-input" '+checkedAttr+'/>'+
                        '<span class="checkbox-tile">'+
                        '<span class="checkbox-icon">'+
                        '<i class="fa fa-'+typeObj[v].icon+'"></i>'+
                        '</span>'+
                        '<span class="checkbox-label">'+ucfirst(typeObj[v].name)+'</span>'+
                        '</span>'+
                        '</label>'+
                        '</div>';

                });
                $(".typeObjActivation").html(str);
            },
            appHelpers: function(){
                costumizer.space="htmlConstruct";
                var str="";
                $.each(["#search", "#live", "#map", "#annonces", "#agenda"], function(e, v){
                    var checkedAttr=(typeof costum.app[v] != "undefined" && notNull(costum.app[v])) ? "checked": "";
                    str+="<div class='clickAndActive' data-key='"+v+"' >"+
                        '<label class="checkbox-wrapper">'+
                        '<input type="checkbox" data-key="'+v+'" class="checkbox-input" '+checkedAttr+'/>'+
                        '<span class="checkbox-tile">'+
                        '<span class="checkbox-icon">'+
                        '<i class="fa fa-'+themeParams.pages[v].icon+'"></i>'+
                        '</span>'+
                        '<span class="checkbox-label">'+ucfirst(themeParams.pages[v].subdomainName)+'</span>'+
                        '</span>'+
                        '</label>'+
                        '</div>';

                });
                $(".appHelpersActivation").html(str);
            },
            selectOptionsConfig : function(){
                /*firstStepCostum.swipper2 = new Swiper('.swipper2', {
                    // Navigation arrows

                    theme : {
                        color : "#086784"
                    },
                    direction: "vertical",

                    allowTouchMove : true,
                    parallax:true
                    /*pagination : {
                        el : ".swiper-pagination-progressbar-fill",
                        type:"progressbar",
                        progressbarFillClass: "swiper-pagination-progressbar-fill",
                        renderProgressbar: function (progressbarFillClass) {
                            return '<span class="' + progressbarFillClass + '"></span>';
                        }
                    }*/
                //  });*/
            },
            thematicTemplate : function(){
                var paramsTemplateList = {
                    urlData: baseUrl + "/co2/search/globalautocomplete",
                    container: "#getTheTemplate",
                    interface : {
                        events : {
                            pagination : true
                        }
                    },
                    /*  header: {
                          dom: ".headerSearchTemplate",
                          options: {
                              left: {
                                  classes: 'col-xs-8 elipsis no-padding',
                                  group: {
                                      count: true,
                                      types: true
                                  }
                              }
                          },
                      },*/
                    defaults: {
                        notSourceKey: true,
                        types: ["templates"],
                        filters: {
                            type: "template",
                            shared: true
                        },
                        fields : ["userCounter","shared", "source","screenshot","cmsList"]

                    },
                    results: {
                        dom: ".bodySearchContainer",
                        renderView: "costumizer.template.views.templateListHtml",
                        map: {
                            active: false
                        }
                    },
                    filters: {
                        category: {
                            view: "dropdownList",
                            type: "filters",
                            name: tradCms.choooseCategory,
                            field: "category",
                            action: "filters",
                            event: "filters",
                            keyValue: false,
                            list: costumizer.template.category
                        }
                    }
                }
                var filterGroupTemplate = searchObj.init(paramsTemplateList);
                filterGroupTemplate.search.init(filterGroupTemplate);
            },
            addMenu : function(title, inc){
                var menuStr="<li class='menu-inc-"+inc+"' style='display:none;'>"+
                    "<span class='bullet'></span>"+
                    "<a href='javascript:firstStepCostum.actions.changeView("+inc+");'>"+title+"</a>"+
                    "</li>";
                $(".modal-menu .tree-structure").append(menuStr);

            }
        },
        actions : {
            init : function(){
                var inc=0;
                $(".swiper-slide").each(function(){
                    $(this).addClass("swiper-slide-"+inc);
                    firstStepCostum.views.addMenu($(this).data('menu'), inc);
                    inc++;
                });
            },
            setControlOptions : function(index){
                $(".swiper-control .button-swipper").hide();
                if(typeof $(".swiper-slide-"+index).data("control") != "undefined"){
                    var controlOptions=$(".swiper-slide-"+index).data("control").split(";");
                    $.each(controlOptions, function(e,v){
                        optDetail=v.split(":");
                        if(optDetail[1]==="false")
                            $(".swiper-control .swiper-to-"+optDetail[0]).hide();
                        else if(optDetail[1]==="true")
                            $(".swiper-control .swiper-to-"+optDetail[0]).show().removeClass("disable");
                        else if(optDetail[1]==="disable")
                            $(".swiper-control .swiper-to-"+optDetail[0]).show().addClass("disable");
                    });
                }
            },
            activeMenu: function(index){
                $(".tree-structure li").removeClass("active");
                if(!$(".menu-inc-"+index).is(":visible")) $(".menu-inc-"+index).fadeIn(500);
                $(".menu-inc-"+index).addClass("active");
            },
            changeView : function(index){
                // firstStepCostum.actions.activeMenu(index);
                firstStepCostum.swipper.slideTo(index);
            },
            changeVerticalNext : function() {
                firstStepCostum.swipper2.slideNext(500, function(){});
            }
        }
    }
</script>