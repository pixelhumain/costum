
<!-- assets -->
<?php $assetsUrl = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl(); ?>
<script src="<?= Yii::app()->getModule(Costum::MODULE)->getParentAssetsUrl() ?>/js/admin/admin_directory.js"></script>
<script src="<?= Yii::app()->getModule(Costum::MODULE)->getParentAssetsUrl() ?>/js/admin/panel.js"></script>
<script src="/plugins/spectrum-colorpicker2/spectrum.min.js"></script>
<link rel="stylesheet" href="/plugins/spectrum-colorpicker2/spectrum.min.css">
<link rel="stylesheet" href="<?= $assetsUrl ?>/cmsBuilder/css/costumizerV2.css">

<style>
    .d-none {
        display: none !important;
    }


    .clearfix:before,
    .clearfix:after {
        content: " ";
        display: table;
    }

    .clearfix:after {
        clear: both;
    }


    .tab_container_version h1 {
        color: #ccc;
        text-align: center;
    }

    .tab_container_version a {
      color: #ccc;
      text-decoration: none;
      outline: none;
    }

    /*Fun begins*/
    .tab_container_version {
        width: 100%;
        margin: 0 auto;
        position: relative;
    }

    .tab_container_version input, .tab_container_version section {
      clear: both;
      padding-top: 10px;
      display: none;
    }

    .tab_container_version label {
      font-weight: 700;
      font-size: 18px;
      display: block;
      float: left;
      width: 25%;
      padding: 1.5em;
      color: #757575;
      cursor: pointer;
      text-decoration: none;
      text-align: center;
      background: #f0f0f0;
    }

    .tab_container_version #tab1:checked ~ #content1,
    .tab_container_version #tab2:checked ~ #content2,
    .tab_container_version #tab3:checked ~ #content3,
    .tab_container_version #tab4:checked ~ #content4,
    .tab_container_version #tab5:checked ~ #content5 {
      display: block;
      padding: 20px;
      background: #fff;
      color: #999;
      border-bottom: 2px solid #f0f0f0;
    }

    .tab_container_version .tab-content .panel-default,
    .tab_container_version .tab-content h3 {
      -webkit-animation: fadeInScale 0.7s ease-in-out;
      -moz-animation: fadeInScale 0.7s ease-in-out;
      animation: fadeInScale 0.7s ease-in-out;
    }
    .tab_container_version .tab-content h3  {
      text-align: center;
    }

    .tab_container_version [id^="tab"]:checked + label {
      background: #fff;
      box-shadow: inset 0 3px #0CE;
    }

    .tab_container_version [id^="tab"]:checked + label .fa {
      color: #0CE;
    }

    .tab_container_version label .fa {
      font-size: 1.3em;
      margin: 0 0.4em 0 0;
    }

    /*Media query*/
    @media only screen and (max-width: 930px) {
      .tab_container_version label span {
        font-size: 14px;
      }
      .tab_container_version label .fa {
        font-size: 14px;
      }
    }

    @media only screen and (max-width: 768px) {
      .tab_container_version label span {
        display: none;
      }

      .tab_container_version label .fa {
        font-size: 16px;
      }

      .tab_container_version {
        width: 98%;
      }
    }

    /*Content Animation*/
    @keyframes fadeInScale {
      0% {
        transform: scale(0.9);
        opacity: 0;
      }

      100% {
        transform: scale(1);
        opacity: 1;
      }
    }

    /* Colapse */
    .tab_container_version .panel-group .panel+.panel {
        margin-top: 15px;
    }
    .panel-group .panel+.panel {
        margin-top: 5px;
    }
    .tab_container_version .panel-default {
        border-radius: 0;
        border: none;
        background: none;
        margin-bottom: 0;
        padding-bottom: 14px;
    }
    .tab_container_version .panel {
        -webkit-box-shadow: 0 1px 0px rgb(0 0 0 / 25%);
        border-color: #3f4e58;
    }
    .tab_container_version .panel-default>.panel-heading {
        border: none;
        background: none;
        padding: 0;
    }
    .tab_container_version .panel-default h5 {
        font-size: 18px;
        font-weight: 300;
        padding: 0;
        margin: 0 0 5px;
    }
    .tab_container_version h5 {
        text-transform: none;
    }
    .tab_container_version .panel-default .collapsed {
        color: #3f4e58;
    }
    .tab_container_version .panel-default .accordion-toggle, .tab_container_version .panel-default a.accordion-toggle:focus, .tab_container_version .panel-default a.accordion-toggle:hover, .tab_container_version .panel-default a.accordion-toggle:active {
        color: #3f4e58;
        text-decoration: none;
    }
    .tab_container_version .panel-default>.panel-heading+.panel-collapse .panel-body {
        border: none;
        padding: 0 0 0 10px;
    }
    .tab_container_version li {
        font-size: 16px;
    }
    .tab_container_version .panel-default .accordion-toggle:before {
        content: "";
        width: 15px;
        height: 15px;
        display: inline-block;
        background: #3f4e58;
        border-radius: 50%;
        margin-right: 10px;
        position: relative;
        top: 4px;
    }
    .tab_container_version section .panel+.panel {
        margin-top: 15px;
    }

    .modal-blockcms-header li .menu-button{
        color: white ;
    }

    .modal-blockcms-header li .arrow_box {
        position: absolute;
        background-color: #545454;
        padding: 1px;
    }
    .modal-blockcms-header li .arrow_box .list-filters .btn-filters-select{
        width: 100%;
        border-radius: unset;
    }

    .modal-blockcms-header .searchBar-filters {
        margin-bottom: 10px;
    }

    .modal-blockcms-header li::before {
        font-family: 'FontAwesome';
        content: "\f023"; 
        margin-right: 5px;
    } 
    .modal-blockcms-header li {
        list-style-type: none;
    }

    .modal-blockcms-header .searchBar-filters {
        position: relative;
        z-index: 1;
    }

</style>

<?php  
    $mode =  $costum["responsive"];
    if ( $mode != "md" ) {   
?>
    <style>
        .cmsbuilder-center-content {
            height: auto !important;
            padding: inherit !important;
        }

        .cmsbuilder-body {
            height: auto !important;
        }

        .cmsbuilder-left-content{
            top : 210px;
        }


        
    </style>
    
        <div class="cmsbuilder-body">
            <div class="cmsbuilder-center-content">
                    <?= $content ?>
            </div>
        </div>
    <script>
        $(function() {
            costumizer.init("<?= $mode ?>")
            // cmsConstructor.init()
            // cssHelpers.form.extendCoInput();
        })
    </script>
    
<?php }else { ?>

<style>
    #mainNav {
        position: absolute !important;
    }
</style>

<div class="cmsbuilder-container">
    <div class="cmsbuilder-header">
        <nav id="menu-top-costumizer">
            <div class="cmsbuilder-header-left">
                <ul class="costum-title">
                    <!-- <li>
                        <a href="javascript:;" class="btn-menu-tooltips">
                            CO<span class="tooltips-menu-btn"><?php echo Yii::t("cms", "tutoriel"); ?></span>
                        </a>
                    </li> -->
                    <li><span class="pull-left text-white elipsis" style="padding-left:10px;max-width:150px;"><?= $costum["title"] ?> </span></li>
                </ul>
                <ul class="cmsbuilder-toolbar">
                    <li class="cmsbuilder-toolbar-item lbh-costumizer  btn-menu-tooltips lbh-menu-no-dropdown" data-space="generalInfos" data-title="<?php echo Yii::t("cms", "General settings of costum"); ?>">
                        <span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "General Informations"); ?></span>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-file-o" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Sitemap"); ?> > <?php echo Yii::t("cms", "Settings of pages"); ?>" data-space="pages" data-savebutton="false">
                                <span><i class="fa fa-file-o" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Pages"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Edition of menu"); ?> " data-space="htmlConstruct">
                                <span><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Menus & header & footer"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Last modification"); ?>" data-space="lastChange" data-savebutton="false">
                                <span><i class="fa fa-dashboard" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Last modification"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="<?php echo Yii::t("cms", "Activated elements of the costums"); ?>" data-space="typeObj">
                                <span><i class="fa fa-connectdevelop" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Add-on element"); ?></span>
                            </li>
                        </ul>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-paint-brush" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-dom="#menu-right-options" data-space="options" data-title="<?php echo Yii::t("cms", "Options"); ?>">
                                <span><i class="fa fa-sliders"></i></i></span>
                                <span><?php echo Yii::t("cms", "Options"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#menu-right-costumizer" data-space="design" data-title="<?php echo Yii::t("cms", "Design"); ?>">
                                <span><i class="fa fa-paint-brush" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Dressing"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-dom="#modal-content-costumizer-administration" data-title="Loader" data-link="loader" data-space="loader">
                                <span><i class="fa fa-spinner" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Loader"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-link="css" data-space="css">
                                <span><i class="fa fa-css3" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Customized css style"); ?></span>
                            </li>
                        </ul>
                    </li>
                    <li class="cmsbuilder-toolbar-item">
                        <span><i class="fa fa-cog" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="lbh-costumizer" data-space="communityAdmin" data-savebutton="false" data-title="<?php echo Yii::t("common", "Community"); ?>">
                                <span><i class="fa fa-users" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("common", "Community"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-space="medias" data-title="<?php echo Yii::t("cms", "Media manager"); ?>">
                                <span><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Media manager"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-space="files" data-title="<?php echo Yii::t("cms", "File manager"); ?>">
                                <span><i class="fa fa-folder-open" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "File manager"); ?></span>
                            </li>
                            <li class="lbh-costumizer" data-space="fonts" data-title="<?php echo Yii::t("cms", "Font manager"); ?>">
                                <span><i class="fa fa-font" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Font manager"); ?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div>
                <ul class="screen-size-nav">
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips active" data-mode="md" data-title="<?php echo Yii::t("cms", "Computer view-mode"); ?>">
                            <i class="fa fa-desktop" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Computer view-mode"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips" data-mode="sm" data-title="<?php echo Yii::t("cms", "Tablet view-mode"); ?>">
                            <i class="fa fa-tablet" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Tablet view-mode"); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" class="btn-costumizer-viewMode btn-menu-tooltips" data-mode="xs" data-title="<?php echo Yii::t("cms", "Mobile view-mode"); ?>">
                            <i class="fa fa-mobile" aria-hidden="true"></i>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Mobile view-mode"); ?></span>
                        </a>
                    </li>
                </ul>
                <ul class="cmsbuilder-toolbar editModeUserSwitcher">
                    <li class="cmsbuilder-toolbar-item">
                        <span class="activeModeConnected">
                            <i class="fa fa-user"></i>
                            <i class="statusConnected fa fa-sign-in" aria-hidden="true"></i>
                        </span>
                        <span class="arrow-down"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown">
                            <li class="costumizer-edit-mode-connected" data-value="true">
                                <span><i class="fa fa-sign-in" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Loggued mode"); ?></span>
                                 <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                            </li>
                             <li class="costumizer-edit-mode-connected" data-value="false">
                                <span><i class="fa fa-sign-out" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Unloggued mode"); ?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div>
                <?php
                    $newUrl = "";
                    $toExplode = "edit";

                    if ( str_contains($_SERVER['REQUEST_URI'], "?edit")){
                        $toExplode = "?edit";
                    }

                    $url =  explode( $toExplode, $_SERVER['REQUEST_URI']);
                    if (isset($url[1])) {
                        $newUrl = $url[0];
                    }
                 ?>
                <ul class="action-nav cmsbuilder-toolbar" id="cmsBuilder-right-actions">
                    <li class="cmsbuilder-toolbar-item save-version">
                        <span class="activeModeConnected">
                            <i class="statusConnected fa fa-floppy-o" aria-hidden="true"></i>
                        </span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown rightAlign">
                        <?php if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])) || Authorisation::isUserSuperAdminCms()){ ?>
                            <li onclick="costumizer.template.actions.save('costum');">
                                <!-- <a href="javascript:;" onclick="costumizer.template.actions.saveThis();"> -->
                                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                                    <span><?php echo Yii::t("cms", "Save this COSTUM as a template"); ?></span>
                                <!-- </a> -->
                            </li>
                            <li onclick="costumizer.template.actions.save('page');">
                                <!-- <a href="javascript:;" onclick="costumizer.template.actions.saveThis();"> -->
                                    <span><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                                    <span><?php echo Yii::t("cms", "Save this page"); ?></span>
                                <!-- </a> -->
                            </li>
                        <?php } ?>
                            <li class="save-costum-version">
                                <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("cms", "Save this costum version"); ?></span>
                            </li>
                        </ul>
                    </li>

                    <li class="cmsbuilder-toolbar-item">
                        <span>
                            <img src="<?= $assetsUrl ?>/cmsBuilder/img/tplIcon.png" alt="" draggable="false" style="height: 24px;padding-bottom: 4px;">
                        </span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown rightAlign">
                            <li class="lbh-costumizer nav-costumizer" data-dom="#modal-blockcms" data-space="template" data-key="costum" data-savebutton="false">
                                <!-- <a href="javascript:;" onclick="costumizer.template.actions.saveThis();"> -->
                                    <span><i class="fa fa-credit-card" aria-hidden="true"></i></span>
                                    <span><?php echo Yii::t("cms", "Choose a Template for whole site"); ?> </span>
                                <!-- </a> -->
                            </li>
                            <li class="lbh-costumizer nav-costumizer" data-dom="#modal-blockcms" data-space="template" data-key="page" data-savebutton="false">
                                <!-- <a href="javascript:;" onclick="costumizer.template.actions.saveThis();"> -->
                                    <span><i class="fa fa-file-o" aria-hidden="true"></i></span>
                                    <span><?php echo Yii::t("cms", "Get Template for this page"); ?></span>
                                <!-- </a> -->
                            </li>
                        </ul>
                    </li>
                                            
                   <!--  <li class="btn-menu-tooltips">
                        <a href="javascript:;" class="open-preview-costum" data-url="<?= Yii::app()->baseUrl.$newUrl ?>">
                            <span><i class="fa fa-eye" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "Preview"); ?></span>
                        </a>
                    </li> -->
                    <li class="cmsbuilder-toolbar-item">
                        <span class="icon-menu-version-and-info"><i class="fa fa-eye" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown rightAlign">
                            <li class="open-preview-costum" data-url="<?= Yii::app()->baseUrl.$newUrl ?>">
                                <a href="javascript:;" style="justify-content: flex-start;"  class="text-white">                                    
                                <span><i class="fa fa-expand" aria-hidden="true" style="font-size: 20px;"></i></span> <span> <?php echo Yii::t("cms", "Preview"); ?></span>
                                </a>
                            </li>
                            <li>
                                <a style="justify-content: flex-start;" href="<?= Yii::app()->getRequest()->getBaseUrl(true)."/#@".$costum["slug"] ?>" target="_blank" class="text-white">
                                    <i class="fa fa-external-link" aria-hidden="true" style="font-size: 20px;"></i> 
                                    <span><?php echo Yii::t("cms", "Go to the parent element"); ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="cmsbuilder-toolbar-item menu-version-and-info">
                        <span class="icon-menu-version-and-info"><i class="fa fa-info" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown rightAlign" >
                            <li class="menu-item-version-and-info" class="" data-value="#openEditor">
                                <span><i class="fa fa-info" aria-hidden="true"></i></span><span><?php echo Yii::t("cms", "List of New Features"); ?></span>
                            </li>
                            <li class="menu-item-version-and-info" data-value="#observatory">
                                <span><i class="fa fa-binoculars" aria-hidden="true"></i></span>
                                <span><?php echo Yii::t("survey","Observatory"); ?></span>
                            </li>
                        </ul>
                    </li>

                    <!-- <li class="btn-menu-tooltips">
                        <a href="javascript:;" class="open-version-modal" data-toggle="modal" data-target="#openEditor">
                            <span><i class="fa fa-info" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"> Version </span>
                        </a>
                    </li> -->

                    <li class="cmsbuilder-toolbar-item menu-help-and-feedback">
                        <span class="icon-menu-help-and-feedback"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        <ul class="cmsbuilder-toolbar-dropdown rightAlign" >
                            <li class="menu-item-help-and-feedback" data-value="tabHelp">
                                <span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                                <span><?= Yii::t("cms", "Quick help") ?></span>
                            </li>
                            <li class="menu-item-help-and-feedback" data-value="tabQuestion">
                                <span><i class="fa fa-question" aria-hidden="true"></i></span>
                                <span><?= Yii::t("cms", "Ask a question") ?></span>
                            </li>
                            <li class="menu-item-help-and-feedback" data-value="tabFeedback">
                                <span><i class="fa fa-comment" aria-hidden="true"></i></span>
                                <span><?= Yii::t("cms", "Giving feedback") ?></span>
                            </li>
                        </ul>
                    </li>
                    <li class="btn-menu-tooltips">
                        <a href="javascript:;" class="lbh-costumizer" data-dom="#menu-right-history" data-space="history" data-title="<?php echo Yii::t("cms", "history"); ?>">
                            <span><i class="fa fa-history" aria-hidden="true"></i></span>
                            <span class="tooltips-menu-btn tooltips-top-menu-btn"><?php echo Yii::t("cms", "History"); ?></span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="cmsbuilder-body">
        <div class="cmsbuilder-side-content cmsbuilder-left-content">
            <button class="btn-toggle-cmsbuilder-sidepanel-page" data-target="left" style="top: 68px;padding-top: 5px;padding-bottom: 5px;">
                <i class="fa fa-file" aria-hidden="true"></i>
            </button>
            <button class="btn-toggle-cmsbuilder-sidepanel" data-target="left">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                <span><?php echo Yii::t("cms", "Add a block"); ?></span>
            </button>
            <button class="btn-import-export-cmsbuilder-sidepanel" data-target="left" onclick="cmsConstructor.importExport.getPageContentJson()"><i class="fa fa-upload"></i><!-- <span>Import/export</span> --></button>
            <div id="left-panel" style="width: 100%; height:100%;">

            </div>
        </div>
        <div class="cmsbuilder-center-content">
            <div onclick="$(`.lbh-costumizer[data-space='pages']`).click()" class="costum-info text-center padding-10 bg-turq hidden cursor-hand" style="position: absolute;top: 0px;width: 100%;"></div>
            <div id="cmsbuilder-content-md" class="cmsbuilder-content-wrapper">
                <?= $content ?>
            </div>
            <div id="cmsbuilder-content-sm" class="d-none cmsbuilder-content-wrapper">
                <div class="device-mockup" style="background-image: url('<?= $assetsUrl ?>/cmsBuilder/img/device-sm.png');"></div>
                <div class="device-screen">
                    <div class="device-status-bar">
                        <div>
                            <span class="device-time">07:58</span>
                        </div>
                        <div>
                            <span><i class="fa fa-signal" aria-hidden="true"></i></span>
                            <span>5G</span>
                            <span><i class="fa fa-battery-three-quarters" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="device-search-bar">
                        <div class="devise-search-bar-url">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <span><?= $costum["title"] ?></span>
                        </div>
                        <div><i class="fa fa-repeat" aria-hidden="true"></i></div>
                    </div>
                    <div class="device-page">

                    </div>
                </div>
            </div>
            <div id="cmsbuilder-content-xs" class="d-none cmsbuilder-content-wrapper">
                <div class="device-mockup" style="background-image: url('<?= $assetsUrl ?>/cmsBuilder/img/device-xs.png');"></div>
                <div class="device-screen">
                    <div class="device-status-bar">
                        <div>
                            <span class="device-time">07:58</span>
                        </div>
                        <div>
                            <span><i class="fa fa-signal" aria-hidden="true"></i></span>
                            <span>5G</span>
                            <span><i class="fa fa-battery-three-quarters" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div class="device-search-bar">
                        <div class="devise-search-bar-url">
                            <span><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <span><?= $costum["title"] ?></span>
                        </div>
                        <div><i class="fa fa-repeat" aria-hidden="true"></i></div>
                    </div>
                    <div class="device-page">

                    </div>
                </div>
            </div>
        </div>
        <div class="cmsbuilder-side-content cmsbuilder-right-content">
            <button class="btn-toggle-cmsbuilder-sidepanel" data-target="right"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
            <!-- <div id="id-palette-color" class="draggabled-co kl-palette-drag-drop"></div> -->
            <div id="id-palette-color" style="display: block;position: absolute; top: 150px; border: none; padding: 5px; width: 30px; left: -25px; z-index: 999; background-color: #3A3A3A; color: white; line-height: 20px; font-size: 14px;">
            </div>
            <div id="right-panel" style="width: 100%; height:100%;">Select an element</div>
        </div>
    </div>
</div>
<?php if(@$costum["firstStepper"]){ 
    echo $this->renderPartial("firstStepper", array("element"=>$costum));
}
?> 
<div id="modal-content-costumizer-administration">
    <div class="modal-tools-costumizer">
        <div class="costumizer-title">
            <span class="bold uppercase"></span>
        </div>
        <div class="pull-right">
            <button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> <?php echo Yii::t("common", "Save") ?></button>

            <button class="btn btn-default close-dash-costumizer" data-dom="#modal-content-costumizer-administration"><i class="fa fa-close"></i> <?php echo Yii::t("common", "Close") ?></button>
        </div>
    </div>
    <div data-key="" class=" contains-view-admin col-xs-12 no-padding">

    </div>
</div>
<div id="modal-blockcms">
    <div class="sp-cms-100 text-center tpl-info-container" style="padding-bottom: 3px;background: linear-gradient(to right, #333333, #ffa500, #333333);">
        <div style="width: 100%;background-color: #3a3a3a;">
            <div class="information" style="width:100%;"></div>
        </div>        
    </div>
    <div class="modal-blockcms-header">     
        <div class="modal-filter-template"></div>
        <button id="btn-hide-modal-blockcms"><i class="fa fa-times" aria-hidden="true"></i></button>
    </div>
    <div class="modal-blockcms-body">
        <div id="category-template-filter" class="col-md-2 col-sm-2 co-scroll"></div>
        <ul id="shared-template-filter"></ul>
        <div class="modal-blockcms-list col-md-10 col-sm-10">
            <div id="modal-template-results"></div>
        </div>
    </div>
</div>
<div id="modal-preview-template" class="modal">
    <div class="content-shadow">
        <div class="modal-blockcms-header">
            <div class="topsection">
                <h3 class="title-template"></h3>
                <button class="close-modal" data-target="#modal-preview-template"><i class="fa fa-times" aria-hidden="true"></i></button>
            </div>
            <div class="action-template text-center col-xs-12">
                <a href="javascript:;" class="btn btn-default use-this-template" data-id="" data-type=""><i class="fa fa-plus"></i> <?php echo Yii::t("common", "Choose") ?></a>
                <?php if(Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) ){ ?>
                    <a href="javascript:;" class="btn btn-primary edit-template" data-id="" data-type=""><i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit") ?></a>
                    <a href="javascript:;" class="btn btn-danger delete-template" data-id="" data-type=""><i class="fa fa-trash"></i> <?php echo Yii::t("common", "Delete") ?></a>
                    <a href="javascript:;" class="btn btn-success share-this-template locked" data-id="" data-type=""><i class="fa fa-unlock"></i> <?php echo Yii::t("cms", "Make Public"); ?></a>
                <?php } ?>
            </div>
            
        </div>
        <div class="preview-body">
     
        </div>
    </div>
</div>

<?php 
    echo $this->renderPartial("features", array("assetsUrl" => $assetsUrl)); 
    echo $this->renderPartial("helpAndFeedback", array("assetsUrl" => $assetsUrl)); 
    echo $this->renderPartial("observatory", array("assetsUrl" => $assetsUrl)); 
?>
<div id="right-sub-panel-container">
    <!-- <button class="btn-close-sub-right-costumizer" onclick="costumizer.actions.closeMenuSubRight()"><span>Glisser et deposer un élément dans le contenu</span> <span><i class="fa fa-chevron-up" aria-hidden="true"></i></span></button> -->
</div>
<div class="sp-cms-std user-state" style="flex-direction: column">    
    <div class="sp-cms-std useractive pannelCommunity ">
    </div>

    <div class="sp-cms-std pannelCommunity loadKambanCostum">
        <i class="fa fa-trello fa-lg" data-toggle="tooltip" title="kamban" style="cursor:pointer;font-size:20x;margin:5px;"></i>
    </div>
    <div class="sp-cms-std pannelCommunity ">
        <a class='text-center show-admin-active' style='display:block !important;font-size:20px;color:white;'><i class='fa fa-users'></i></a>
    </div>   
    <div class="sp-cms-std useradmin hidden">
        <span> <?php echo Yii::t("cms", "Online Admins"); ?></span>
        <input type="text" name="">
    </div>
    <div class="sp-cms-std useradmin hidden">
        <audio class="hidden" id="connectedAudio">
            <source src="<?= $assetsUrl ?>/cmsBuilder/sound/costum-user-connected.mp3" type="audio/mpeg">
        </audio>
        <audio class="hidden" id="leavedAudio">
            <source src="<?= $assetsUrl ?>/cmsBuilder/sound/costum-user-leave.mp3" type="audio/mpeg">
        </audio>
    </div>
    <div class="sp-cms-std pannelCommunity ">
        <button class="ssmla" data-action="chatmanager" style="border:none; font-size:20px ;background-color: transparent;"><i class="fa fa-comments"></i></button>
    </div>

</div>

<div class="persist-loader hidden text-center" style="position: fixed;top: 50%;width: 100%; z-index: 999999;"></div>
<style id="cssLoader"></style>
<style type="text/css">
      .popover {
        z-index: 999999 !important;
        width: 450px;
  }
</style>
<script>
    localStorage.removeItem("ctrlZmd"+costum.slug);
    localStorage.removeItem("ctrlYmd"+costum.slug);
    localStorage.removeItem("ctrlZsm"+costum.slug);
    localStorage.removeItem("ctrlYsm"+costum.slug);
    localStorage.removeItem("ctrlZxs"+costum.slug);
    localStorage.removeItem("ctrlYxs"+costum.slug);

    $(".user-state").on("click",".show-admin-active",function () {
        $('.lbh-costumizer[data-space="communityAdmin"').click()
    })

    $(".useractive").off().on('mousedown', '.user-trigger', function () {

            $(this).popover({
                html: true,
                content: $(this).data('content')
            }).on('mouseup', function () {
                $('.user-trigger').not(this).popover('hide');
            });
        // }
    });

    $(document) .on('click', function (e) {
        if (!$(e.target).hasClass("costum-user-img-avatar") && $(e.target).parents('.popover.in').length === 0) { 
            $('.user-trigger').popover('hide');
        }
    });

    $(function() {
        $(".cmsbuilder-container").css({"pointer-events": "none"});
        var checkCostumSocketInterval = setInterval(function(){
            if (window.costumSocket != undefined) {
                clearInterval(checkCostumSocketInterval);
                $(".cmsbuilder-container").css({"pointer-events": ""});
                costumizer.init("<?= $mode ?>")
                if(costum.firstStepper && costum.editMode===true){
                    $("#modalFirstStepCmsCostum").modal("show");   
                    setTimeout(function(){
                        firstStepCostum.init();
                    }, 500);
                }
            }
        }, 500)

        $(".changetheme").change(function() {
            var valueCheckbox = ($(this).is(":checked")) ? true : false;
            $("#modalKanbanAction").attr("data-kanbantheme", `${(valueCheckbox) ? "light" : "dark"}`)
		})

        $(".loadKambanCostum").on('click', function(e){
            e.stopPropagation();
            window.loadKanbanAction("all", costum.contextSlug, { id : costum.contextId , type : costum.contextType , slug : costum.slug });
            $("#modalKanbanAction").modal("show");
		});
    })


</script>

<?php } ?>