<style>
    .obs .row-observatory{
        display: flex;
        justify-content: space-around;
        align-items: center;
        margin-bottom: 5%;
    }
    .card-obs {
        padding: 20px;
        background: #333333;
        color: white;
        width : 200px;
        border-radius: 14px;
        box-shadow: 0px 0px 4px 1px #9fbd38, 4px 3px 4px 1px #c33c66;
    }
    .card-obs:hover{
        transform: translateY(-5px);
        box-shadow: 4px 3px 20px 2px  #9fbd38, 0px 0px 20px 2px #c33c66;
    }
    .card-size {
        width: 60%;
        font-size: 20px;
        justify-content: center;
        align-items: center;
        text-align: center;
        display: flex;
        padding: 10px;
        box-shadow: 1px 0px 1px 1px #5e5a5a;
    }
    .card-icon .fa-users{ 
        font-size : 15px;
    }       
    .card-icon {        
        font-size: 10px;
        width: 40%;
        height: 66px;
        padding: 1px 20px 0px 0px;

    }
    .card-text p{
        font-size : 20px;
        color : white;
    }
    .card-header{
        display : flex;
    }
    .card-text{
        margin-top : 5px;
    }
    #observatory button.close{
        color : white;
        opacity : 1;
    }
    #observatory{
        color : white;
    }
    #observatory .modal-content{
        background : #333333;
    }
    .tab_container_observatory{
        min-height : 400px;
    }
    #observatory .modal-header{
        background-color : transparent;
    }
    .obs .lines .tr-obs{
        background-color : transparent;
    }
    .table-actions:hover{
        transform: translateY(-5px);
        box-shadow: 4px 3px 20px 2px  #9fbd38, 0px 0px 20px 2px #c33c66;
    }
    .table-actions{
        box-shadow: 0px 0px 4px 1px #9fbd38, 4px 3px 4px 1px #c33c66;
        padding : 5px;
        border-radius: 14px;
    }
    .tableActions a{
        color: white;
        margin-bottom: 2px;
        /* border: 1px solid white; */
        padding: 2px;
    }
    a.close-detail{
        margin-left: 6px;
        text-shadow: 0 1px 0 #fff;
        font-weight: bold;
        padding: 1px 5px 1px 5px;
    }
    .header-table-actions{
        margin-bottom : 5px;
    }
</style>
<div class="modal fade" id="observatory" role="dialog" style="z-index: 999999;">
    <div class="modal-dialog modal-lg" style = "margin-top: 50px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    <span><?php echo Yii::t("survey","Observatory") ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="tab_container_observatory">
                    
                </div>
            </div>

        </div>

    </div>
</div>
<script>
    var obsCostum = {
        getInfo : function(){
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/cms/getinfo",
                {
                    id : costum.contextId,
                    type : costum.contextType,
                },
                function(data){
                    $(".tab_container_observatory").html(obsCostum.views(data));
                    costumizer.events.startView();
			        coInterface.bindLBHLinks();    
                    obsCostum.actions.bindEvent();
                    var maxHeight = Math.max.apply(null, $(".card-obs").map(function() {
                        return $(this).height();
                    }).get());
                    $(".card-obs").css("height", maxHeight+20);
                },
                null

            );
        },
        views : function(data){
            var str = "";
            str += `<div class="obs">
                        <div class="row-observatory">
                            <div class="card-obs lbh-costumizer" data-space="medias" data-dismiss="modal">
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                            <i class="fa fa-image fa-2x"></i>
                                        </div>
                                        <div class="card-size" >
                                            ${data.sizeImg}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p> ${trad.image} </p>
                                    </div> 
                                </div> 
                            </div>
                            <div class="card-obs lbh-costumizer" data-space="files" data-dismiss="modal">
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                        <i class="fa fa-file-text-o fa-2x"></i>
                                        </div>
                                        <div class="card-size">
                                            ${data.sizeFile}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p> ${trad.file} </p>
                                    </div> 
                                </div> 
                            </div>  
                            <div href = "#admin.view.statistic" class="card-obs lbh" data-dismiss="modal" >
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                            <i class="fa fa-eye fa-2x"></i>
                                        </div>
                                        <div class="card-size">
                                            ${data.visit}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p> ${tradCms.visitor} </p>
                                    </div> 
                                </div>
                            </div> 
                            <div href = "#admin.view.statistic" class="card-obs lbh" data-dismiss="modal">
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                            <i class="fa fa-map-marker fa-2x"></i>
                                        </div>
                                        <div class="card-size">
                                            ${data.visitCarte}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p> Click carte </p>
                                    </div> 
                                </div>
                            </div> 
                        </div> 
                        <div class="row-observatory">`
                            if(typeof data.members != "undefined"){
                                str +=`
                                <div href="#@${costum.contextSlug}.view.directory.dir.members" class=" card-obs lbh" data-dismiss="modal">
                                    <div class="card-body">
                                        <div class="card-header">
                                            <div class="card-icon">
                                                    <i class="fa fa-users"></i>
                                            </div>
                                            <div class="card-size">
                                                ${data.members}
                                            </div>
                                        </div>
                                        <div class="card-text"> 
                                            <p> ${trad.Member} </p>
                                        </div> 
                                    </div>
                               </div>`;
                            }
                            if(typeof data.contributors != "undefined"){
                                str +=`
                                <div href="#@${costum.contextSlug}.view.directory.dir.contributors" class=" card-obs lbh" data-dismiss="modal">
                                    <div class="card-body">
                                        <div class="card-header">
                                            <div class="card-icon">
                                                    <i class="fa fa-users"></i>
                                            </div>
                                            <div class="card-size">
                                                ${data.contributors}
                                            </div>
                                        </div>
                                        <div class="card-text"> 
                                            <p> ${trad.contributor} </p>
                                        </div> 
                                    </div>
                                </div>`;
                            } 
                            if(typeof data.attendees != "undefined"){
                                str +=`
                                <div href="#@${costum.contextSlug}.view.directory.dir.attendees" class=" card-obs lbh" data-dismiss="modal">
                                    <div class="card-body">
                                        <div class="card-header">
                                            <div class="card-icon">
                                                    <i class="fa fa-users"></i>
                                            </div>
                                            <div class="card-size">
                                                ${data.attendees}
                                            </div>
                                        </div>
                                        <div class="card-text"> 
                                            <p> ${trad.attendees} </p>
                                        </div> 
                                    </div>
                                    </div>
                                </div>`;
                            }
                            str += `<div class="card-obs" data-dismiss="modal">
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                            <i class="fa fa-map-marker fa-2x"></i>
                                        </div>
                                        <div class="card-size" >
                                            ${data.memberGeolo}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p> ${tradCms.geolocatedMembers} </p>
                                    </div> 
                                </div> 
                            </div>
                            <div href="#projects" class=" card-obs lbh"data-dismiss="modal">
                                <div class="card-body">
                                    <div class="card-header">
                                        <div class="card-icon">
                                            <i class="fa fa-lightbulb-o  fa-2x"></i>
                                        </div>
                                        <div class="card-size" >
                                            ${data.nbProject}
                                        </div>
                                    </div>
                                    <div class="card-text"> 
                                        <p>${trad.projects} </p>
                                    </div> 
                                </div> 
                            </div>`
                            if(costum.contextType == "projects" || costum.contextType == "organizations"){
                                str += `<div class="card-obs actionsclick" ">
                                    <div class="card-body">
                                        <div class="card-header">
                                            <div class="card-icon">
                                                <i class="fa fa-list fa-2x"></i>
                                            </div>`
                                                if(costum.contextType == "projects"){
                                                    str += `<div class="card-size" id="nbActions" >${obsCostum.actions.getNbActions(costum.contextId)} </div>`
                                                }
                                        str += `</div>
                                        <div class="card-text"> 
                                            <p> `;
                                                if(costum.contextType == "projects"){
                                                    str += trad.actions;
                                                }else if( costum.contextType == "organizations"){
                                                    str += trad.projectsWithAction
                                                }
                                        str += `</p>
                                        </div> 
                                    </div> 
                                </div>`;
                            }
                        str += `</div>         
                    </div>
                    <div class="obs">
                        <div class="tableActions">
                        </div>
                    </div>`;
            
            return str; 
        },
        actions : {
            nbAllActions : 0,
            nbActionsProjects : {},
            getNbActions : function(id){
                let paramsKanbanAction = {     
                    "filters[parentId]" : id
                };
                var nbActions = 0;
                ajaxPost(
                    null,
                    baseUrl+"/costum/project/action/request/actions/scope/all",
                    paramsKanbanAction,
                    function(data){
                        obsCostum.actions.nbActionsProjects[id] = {};
                        obsCostum.actions.nbActionsProjects[id].nbActionsTodo = 0;
                        obsCostum.actions.nbActionsProjects[id].nbActionsDone = 0;
                        obsCostum.actions.nbActionsProjects[id].nbActionsTracking = 0;
                        obsCostum.actions.nbActionsProjects[id].nbActionsTotest = 0;
                        $.each(data,function(k,v){
                            if(v.status == "todo"){
                                obsCostum.actions.nbActionsProjects[id].nbActionsTodo++  
                            }
                            if(v.status == "todo" && typeof v.tracking !="undefined" && v.tracking){
                                obsCostum.actions.nbActionsProjects[id].nbActionsTracking++  
                            }
                            if(v.status == "todo" && typeof v.tags !="undefined" && $.inArray("totest",v.tags) >=0 ){
                                obsCostum.actions.nbActionsProjects[id].nbActionsTotest++  
                            }
                            if(v.status == "done"){
                                obsCostum.actions.nbActionsProjects[id].nbActionsDone++  
                            }
                        })
                        nbActions += data.length;
                    },null,null, {async:false}
                )
                return nbActions;
            },   
            bindEvent : function(){   
                $(".actionsclick").click(function(){
                    if($(".tab_container_observatory .obs .tableActions .table-actions").height() == null){
                        $(".tab_container_observatory").css("min-height","600px")
                        coInterface.showLoader(".tab_container_observatory .obs .tableActions");
                        var str = "";              
                        if(costum.contextType == "projects"){
                            str += `<div class="table-actions"> 
                                <div class="header-table-actions">
                                    <div class="text-center">
                                        <p> Actions </p> 
                                    </div>
                                    <div class="text-right ">
                                        <a href="javascript:;" class="view-detail"><i class="fa fa-eye"></i> Voir le détail </a>
                                        <a href="javascript:;" class="close-detail">x</a>
                                    </div>
                                </div> 
                                <div class="">  
                                    <table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover text-center" >
                                        <thead>
                                            <tr id="headerTable">
                                                <th class="text-center">A faire</th>
                                                <th class="text-center">En cours</th>
                                                <th class="text-center">A tester</th>
                                                <th class="text-center">Terminé</th>
                                            </tr>
                                        </thead>
                                        <tbody class="lines">
                                            <tr class="tr-obs">
                                                <td>${obsCostum.actions.nbActionsProjects[costum.contextId].nbActionsTodo}</td>
                                                <td>${obsCostum.actions.nbActionsProjects[costum.contextId].nbActionsTracking}</td>
                                                <td>${obsCostum.actions.nbActionsProjects[costum.contextId].nbActionsTotest}</td>
                                                <td>${obsCostum.actions.nbActionsProjects[costum.contextId].nbActionsDone} </td>
                                            </tr>                                        
                                        </tbody>
                                    </table> 
                                </div>                        
                            </div>`;
                            $(".tab_container_observatory .obs .tableActions").html(str);
                            $(".tab_container_observatory").css("min-height","400px")
                            $(".view-detail").click(function(){
                                urlCtrl.openPreview("/view/url/costum.views.custom.kanban.kanban-oceco", {"context":contextData, "options":""});
                            }) 
                            $(".close-detail").click(function(){
                                $(".table-actions").hide()
                            })
                        }else if(costum.contextType == "organizations"){
                            var url = baseUrl + '/co2/search/globalautocompleteadmin/type/'+costum.contextType+'/id/'+costum.contextId+'/canSee/true';
                            var post = {
                                searchType : ['projects'],
                                notSourceKey : true,
                                indexStep : 0,
                                count : true,
                                countType : ['projects'],
                                filters : {
                                    '$or' : {

                                    }
                                },
                                'fediverse' : false
                            };
                            post.filters['preferences.toBeValidated.'+costum.slug] = { '$exists' : false }
                            post.filters['$or']['links.contributors.'+costum.contextId] = { '$exists' : true }
                            post.filters['$or']['parent.'+costum.contextId] = { '$exists' : true }
                            post.filters['links.contributors.'+costum.contextId] = { '$exists' : true }
                            ajaxPost(
                                null,
                                url,
                                post,
                                function(data){
                                    $.each(data.results,function(k,v){
                                        obsCostum.actions.getNbActions(k);
                                    })
                                    str += `<div class="table-actions"> 
                                        <div>
                                            <div class="text-center">
                                                <p> Liste projets </p> 
                                            </div>
                                            <div class="text-right ">
                                                <a href="javascript:;" class="view-detail"><i class="fa fa-eye"></i> Voir le détail </a>
                                                <a href="javascript:;" class="close-detail">x</a>
                                            </div>
                                        </div>
                                        <div class="">  
                                            <table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover text-center" >
                                                <thead>
                                                    <tr id="headerTable">
                                                        <th class="text-center"> Projets </th>
                                                        <th class="text-center">A faire</th>
                                                        <th class="text-center">En cours</th>
                                                        <th class="text-center">A tester</th>
                                                        <th class="text-center">Terminé</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="lines">`
                                                    $.each(data.results,function(kp, vProject){    
                                                        if(obsCostum.actions.nbActionsProjects[kp].nbActionsTodo > 0){                                    
                                                            str += `<tr class="tr-obs">
                                                                <td>${vProject.name}</td>
                                                                <td>${obsCostum.actions.nbActionsProjects[kp].nbActionsTodo}</td>
                                                                <td>${obsCostum.actions.nbActionsProjects[kp].nbActionsTracking}</td>
                                                                <td>${obsCostum.actions.nbActionsProjects[kp].nbActionsTotest}</td>
                                                                <td>${obsCostum.actions.nbActionsProjects[kp].nbActionsDone} </td>
                                                            </tr> `;
                                                        }                                                
                                                    })
                                            str += `</tbody> 
                                            </table> 
                                        </div>         
                                    </div>`; 
                                    $(".tab_container_observatory .obs .tableActions").html(str);
                                    $(".tab_container_observatory").css("min-height","400px")
                                    $(".view-detail").click(function(){
                                        urlCtrl.openPreview("/view/url/costum.views.custom.kanban.kanban-gestion-orga", {"context":contextData, "options":''});
                                    })
                                    $(".close-detail").click(function(){
                                        $(".table-actions").hide()
                                    })
                                },null,null, {}
                            )
                        }
                    }else{
                        $(".table-actions").show()
                    }
                })
            }         
        }
    }
</script>