<?php
$dataAttributes = "";
foreach ($blockAttributes as $key => $value) {
    $dataAttributes .= "data-" . $key . "='" . $value . "' ";
}
?>

<div class="block-actions-wrapper block-actions-wrapper-<?= $kunik ?>">
    <div class="cmsbuilder-block-action">
        <div class="cmsbuilder-block-action-label"><?= $label ?></div>
        <div class="cmsbuilder-block-action-list">
            <?php if (is_array($actions)) { ?>
                <?php if (isset($actions[0]["label"])) { ?>
                    <ul>
                        <?php foreach ($actions as $action) { ?>
                            <li <?= $dataAttributes ?> data-action="<?= $action["name"] ?>" class="<?= isset($action["className"]) ? $action["className"] : "" ?>">
                                <span><i class="fa fa-<?= $action["icon"] ?>" aria-hidden="true"></i></span>
                                <span><?= $action["label"] ?></span>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } else if (isset($actions[0][0]["label"])) { ?>
                    <?php foreach ($actions as $groupActions) { ?>
                        <ul>
                            <?php foreach ($groupActions as $action) { ?>
                                <li <?= $dataAttributes ?> data-action="<?= $action["name"] ?>" class="<?= isset($action["className"]) ? $action["className"] : "" ?>">
                                    <span><i class="fa fa-<?= $action["icon"] ?>" aria-hidden="true"></i></span>
                                    <span><?= $action["label"] ?></span>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>