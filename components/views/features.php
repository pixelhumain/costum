<div class="modal fade" id="openEditor" role="dialog" style="z-index: 999999;">
    <div class="modal-dialog modal-lg" style = "margin-top: 200px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">
                    <span>Liste des nouveauté</span>
                <?php if(Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) ){ ?>        
                    <button class="btn editVersionInfo pull-right margin-right-20"><?php echo Yii::t("common", "Edit") ?></button>            
                    <button class="btn btn-success saveVersionInfo hidden pull-right margin-right-20"><?php echo Yii::t("common", "save") ?></button>
                <?php } ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="tab_container_version">
                    
                </div>
            </div>

        </div>

    </div>
</div>
<script type="text/javascript">
    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/cms/featureinfo",
        null,
        function(data){
            mylog.log("tab_container_version",data.doc.description,data.doc._id.$id)
            // $(".tab_container_version .markdown").html(data.doc.description)
            $(".tab_container_version").html(`<div class="markdown">${dataHelper.markdownToHtml(data.doc.description)}</div>`);
        },
        null

        );

    $(".editVersionInfo").click(function(){   
        $(this).addClass("hidden")      
        $(".saveVersionInfo").removeClass("hidden")     
        dataHelper.activateMarkdown(".tab_container_version .markdown")
    })

    $(".saveVersionInfo").click(function(){   
        $(this).addClass("hidden")      
        $(".editVersionInfo").removeClass("hidden") 
        var featureInfo = {
            insert : true,
            description: $(".tab_container_version .md-input").val()
        }
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/cms/featureinfo",
            featureInfo,
            function(data){
                mylog.log("tab_container_version",data)
                $(".tab_container_version").html(`<div class="markdown">${dataHelper.markdownToHtml(data.doc.description)}</div>`);
            },
            null

            );
    })

</script>