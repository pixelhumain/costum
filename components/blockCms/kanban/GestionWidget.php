<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\kanban;

    use yii\base\Widget;

    class GestionWidget extends Widget {
        public $defaultData = [
            "text" => "text",
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>