<?php 

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\elements;

    use Element;
    use yii\base\Widget;

    class AtmotrackWidget extends Widget {
        public $path = "";
        public $config = [];
        public $paramsData = [];
        public $defaultData = [];

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
            }

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);

            $this->config['$el'] = $el;
        }

        public function run(){
            return $this->render($this->path, $this->config);
        }
    }
?>