<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\elements;

    use Element;
    use yii\base\Widget;

    class DataGouveWidget extends Widget {
        public $path = "";
        public $config = [];
        public $paramsData = [];
        public $defaultData = [];

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
            }

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            $typeCocity = isset($this->config["costum"]["typeCocity"]) ? $this->config["costum"]["typeCocity"] : "";
            $address = isset($el['address']) ? $el['address'] : "";
            $dataApi = [];
            if($typeCocity != "" && $address != "") {
                $dataApi = $this->getDataFromAPI($address['level3Name']);
            }

            $this->config['dataApi'] = $dataApi;
        }

        function getDataFromAPI($region) {
            $region = urlencode($region);
            
            $url = "https://www.data.gouv.fr/api/2/datasets/search/?granularity=fr:region&q=$region";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
        
            if ($httpCode !== 200) {
                return ['error' => "Erreur API (Code: $httpCode)"];
            }
        
            $data = json_decode($response, true);
        
            if (empty($data['data'])) {
                return ['error' => "Aucun résultat trouvé pour la région '$region'."];
            }
        
            return $data;
        }

        public function run(){
            return $this->render($this->path, $this->config);
        }
    }


?>