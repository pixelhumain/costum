<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use yii\base\Widget;
use Yii;
use CO2;
use Form;
use HtmlHelper;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;

class SearchObjWidget extends Widget{
    private $assetsUrl = "";
    public $defaultsData = [
        "query" => [
            "types" => ["projects"],
            "indexStep" => "30",
            "orderBy" => "",
            "orderType" => "1",
            "notSourceKey" => false,
            "community" => false,
            "fields" => ["profilRealBannerUrl"],
            "loadEvent" => "pagination",
            "displayResults" => "directory"
        ],
        "displayConfig" => [
            "designType" => "elementPanelHtml"
        ],
        "headers" => [
            "left" => [
                "classe" => "col-xs-8 elipsis no-padding"
            ],
            "right" => [
                "classe" => "col-xs-8 elipsis no-padding"
            ]
        ],
        "style" => [

        ]

    ];

    public $path = "";
    public $config = []; 
    private $filliaireCategories = null;
    public function init(){
        parent::init();
        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
        $this->filliaireCategories =  CO2::getContextList("filliaireCategories");
        $this->config["filliaireCategories"] = $this->filliaireCategories;
        if (!empty($this->config["costum"]["contextId"]))
            $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultsData, $this->config["blockCms"]);
            if (isset($this->config["blockCms"]["filters"]["initLists"]["lists"])) {
                if (in_array('aac', $this->config["blockCms"]["filters"]["initLists"]["lists"])) {
                    $allAac = Aap::getAacElements();
                    if (!empty($allAac["data"])) {
                        foreach ($allAac["data"] as $aacKey => $aacValue) {
                            $this->config["blockCms"]["filters"]["listsData"]["aac"][$aacKey] = $aacValue["name"];
                        }
                    } else {
                        $this->config["blockCms"]["filters"]["listsData"]["aac"] = [];
                    }
                }
                if (in_array('aacDefi', $this->config["blockCms"]["filters"]["initLists"]["lists"])) {
                    $allAacDefi = Aap::getAacDefi("checkboxNewaapStep1m03ot9qymmfgashp7l.list");
                    if (!empty($allAacDefi["data"])) {
                        $this->config["blockCms"]["filters"]["listsData"]["aacDefi"] = $allAacDefi["data"];
                    } else {
                        $this->config["blockCms"]["filters"]["listsData"]["aacDefi"] = [];
                    }
                }
            }
        }
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}