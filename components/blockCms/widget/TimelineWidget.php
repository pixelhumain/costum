<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

    use Element;
    use yii\base\Widget;

    class TimelineWidget extends Widget {
        public $defaultData = [
            "titleBlock" => ["fr" => "TIMELINE"],
            "panel"=> "transparent",
            "iconColor" => "#49cbe3",
            "iconBgColor" => "#1A2660",
            "timelineData" => [
                "line1" => [
                    "icon" => "hand-o-up",
                    "nom" => ["fr" => "<font face=\"NotoSans-Regular\">Détecter, accompagner et promouvoir la création de Jeunes Entreprises Innovantes et accélérer le développement des jeunes pousses</font>"],
                    "key" => "line1"
                ], 
                "line2" => [
                    "icon" => "share-alt",
                    "nom" => ["fr" => "<font face=\"NotoSans-Regular\">Animer le réseau des acteurs de l'Enseignement Supérieur, de la Recherche et de l'Innovation et de l'Entreprise sur le principe de la fertilisation croisée</font>"],
                    "key" => "line2"
                ], 
                "line3" => [
                    "icon" => "lightbulb-o",
                    "nom" => ["fr" => "<font face=\"NotoSans-Regular\">Communiquer et promouvoir La Réunion technologique et innovante en valorisant les savoir-faire réunionnais dans tous les secteurs d'activités économiques</font>"],
                    "key" => "line3"
                ]
            ]
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                if (isset($this->config["blockCms"]["timelineData"])) {
                    $this->defaultData["timelineData"] = $this->config["blockCms"]["timelineData"];
                }   
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>