<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use yii\base\Widget;

class BannerTwoBtnWidget extends Widget
{
    public $defaultData = [
        "title" => "<div><font color=\"#ffffff\" face=\"NotoSans-Regular\" style=\"font-size: 31px;line-height: 50px;\">connecter vous au mapping by Technopole</font></div>",
        "buttonLink" => "",
        
        "button1Label"       => "Qu'est ce que l'innovation ?",
        "formShow" => false,


        "contenuePremierInput" => "",
        "contenueDeuxiemeInput" => "",
        "contenueTroisiemeInput" => "",
        "contenueQuatriemeInput" => "",
        "proprieteBouton2" => [
            "buttonLabel2"       => "Je souhaite rejoindre l'écosystème",
        ],
        "proprieteBouton1" => [
            "buttonLabel1"       => "Qu'est ce que l'innovation ?",
            "button1Class"       => "lbh",
            "button1Link"        => "#innovation"

        ],
        "linkProperty" => [
            "videoLink" => "https://www.youtube.com/embed/GGEr6in6G1w?rel=0&enablejsapi=1&widgetid=1&mute=1&loop=1&autoplay=1&controls=0&showinfo=0&autohide=1&playlist=GGEr6in6G1w"
        ],
        "linkProperty" => [
            "linkLabel" => "Le Mapping de l'Ecosystème Innovation de La Réunion",
            "linkSize" => "40",
            "linkColor" => "#ffffff",
            // "linkLink" => "mapping"

        ],
        "searchForm" => [
            "firstText" => [
                "premierLabel" => "Je suis",
                "contenuePremierInput" => [],
                "paramCont1" => [],
                "show" => '',
            ],
            "secondoText" => [
                "deuxiemeLabel" => "Je cherche",
                "contenueDeuxiemeInput" => [],
                "paramCont2" => [],
                "show" => '',
            ],
            "thirdText" => [
                "troisiemeLabel" => "Secteurs",
                "contenueTroisiemeInput" => [],
                "paramCont3" => [],
                "show" => '',
            ],
            "fourthText" => [
                "quatriemeLabel" => "Types de financements",
                "contenueQuatriemeInput" => [],
                "paramCont4" => [],
                "show" => '',
            ],
        ]
    ];
    public $config = [];
    public $path = "";

    public function init()
    {
        parent::init();

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
