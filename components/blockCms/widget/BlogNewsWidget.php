<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew;
use HtmlHelper;

class BlogNewsWidget extends Widget{
    public $paramsData = [
        "query" => [
            "types" => ["news"],
            "indexStep" => 6,
            "orderBy" => "created",
            "orderType" => "-1",
            "notSourceKey" => true,
            "displayResults" => "directory",
            "fieldShow" => [
                "text","mediaImg","sharedBy", "tags","vote","voteCount","comment","commentCount","created","markdownActive","media","type","listImage"
            ]

        ],
        "filters" => [
            "filtersPersonal" => [
                "filterPersonal" => [
                    [
                        "view" => "horizontalList",
                        "label" => "Filtre par Tags",
                        "type" => "filters",
                        "field" =>"tags",
                        /*"typeList" => "object",*/
                        "events" => "filters",
                        "lists" => "manuel",
                        /*"listObject" =>  [
                            [
                                "objectkey"=> "kaiza",
                                "objectvalue"=>  "kaiza"
                            ],
                            [
                                "objectkey"=> "okay",
                                "objectvalue"=> "okay"
                            ]
                        ]*/
                        "listTags" => "Nouveauté,Communecter,OCECO,Formation"
                    ]
                 ]
            ]
        ],

        "footerDom" => ".footerSearchNewsContainer",
        "displayConfig" => [
            "designType" => "newsPanelFullWidth",
            "smartGrid" => false
        ],

        "css" => [
            "commonConfigCard" => [
                "background" => "#00997c",
                "borderBottomLeftRadius" => "24px",
                "borderBottomRightRadius" => "24px",
                "borderTopLeftRadius" => "24px",
                "borderTopRightRadius" => "24px",

            ]
        ],
    ];
    public $path = "";
    public $config = [];
    private $assetsUrl = "";

    public function init(){
        parent::init();


        if (isset($this->config["blockCms"])) {
            if  (isset($this->config["blockCms"]["query"]["fieldShow"])) {
                $this->paramsData["query"]["fieldShow"] = $this->config["blockCms"]["query"]["fieldShow"];
            }
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }

        $this->config["blockCms"]["query"]["filters"]["type"] = "news";
        if(isset($this->config["costum"]["communityLinks"]["projects"])){
            $this->config["blockCms"]["query"]["filters"]["target.id"] = [
                '$in' => array_merge(array_keys($this->config["costum"]["communityLinks"]["projects"]),
                    [$this->config["costum"]["contextId"]]
                )
            ];
        }else{
            $this->config["blockCms"]["query"]["filters"]["target.id"] = $this->config["costum"]["contextId"];
        }

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }

}
