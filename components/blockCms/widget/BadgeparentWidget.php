<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew,HtmlHelper,MongoId;

class BadgeparentWidget extends Widget
{
    public $paramsData = [
        "css" => [
            "image-badge-parent" => [
                "height" => "100%",
                "border-radius" => "20px" 
            ],
            "container-badge-parent-item" => [
                "width" => "100%",
                "padding" => "10px",
                "margin" => "10px",
            ],
            "description-badge-parent" => [
                "font-size" => "15px", 
            ],
            "titre-badge-parent" => [
                "color" =>  "#333",
            ],
        ],
    ];
    public $path = "";
    public $config = [];
    public $assetsUrl = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]); 
        }

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config );
    }
}