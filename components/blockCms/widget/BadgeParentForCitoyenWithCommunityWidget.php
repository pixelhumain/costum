<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew,HtmlHelper,MongoId;

class BadgeParentForCitoyenWithCommunityWidget extends Widget
{
    public $paramsData = [
        "css" => [
            "image-citoyen-possess-badge" => [
                "width" => "100%",
                "border-radius" => "5px",
                "height" => "50%"
            ],
            "container-citoyen-possess-badge" => [
                "height" => "300px",
            ],
            "name-citoyen-possess-badge" => [
                "text-align" => "center",
            ]
        ],
    ];
    public $path = "";
    public $config = [];
    public $assetsUrl = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]); 
        }

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config );
    }
}