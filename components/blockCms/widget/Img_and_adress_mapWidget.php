<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

    use Element;
    use yii\base\Widget;

    class Img_and_adress_mapWidget extends Widget {
        public $defaultData = [
            "title" => "Lorem Ipsum", 
            "subtitle"=> "simply text",
            "bgCard"=> "#000000",
            "colorDecor" => "#f0ad16",
            "logo"=> "",
            "content"=> [], 
            "serviceSize" => "18",
            "serviceFont" => "Arial",
            "descriptionSize" => "18",
            "descriptionFont" => "Arial",
        ];
        
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            if(isset($this->config["costum"]["contextType"]) && isset($this->config["costum"]["contextId"])){
                $this->config['el'] = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>