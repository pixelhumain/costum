<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use yii\base\Widget;

class ShowingPdfInPopupModalWidget extends Widget
{
    public $defaultData = [
        "title"             => "Lorem Ipsum",
        "content"           => "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
        "pdf"              => "",
        "buttonlabel"       => "Boutton",
        "buttoncolorlabel"  => "#2b7849",
        "buttoncolor"       => "#e7e4e4",
        "buttoncolorBorder" => "#b8eeb4",
        "showType"          => "textbtn",
        "logo"              => "",
    ];
    public $config = [];
    public $path = "";

    public function init()
    {
        parent::init();

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
