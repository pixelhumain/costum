<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew;
use HtmlHelper;

class ElementStandardWidget extends Widget{
    public $paramsData = [
        "query" => [
            "types" => ["events"],
            "indexStep" => "6",
            "orderBy" => "",
            "orderType" => "1",
            "notSourceKey" => false,
            "fields" => ["profilRealBannerUrl"],
            "displayResults" => "directory",
            "fieldShow" => [
                "name","address","tags", "shortDescription","description","role","collection","type","created","startDate","endDate"
            ]
     
 
         ],
        "displayConfig" => [
            "designType" => "elementPanelHtml",
            "carousel" => false,
            "smartGrid" => false,
            /*"showData" => [
                "socialTools", "locality", "tags", "shortDescription"
            ],*/
        ],
        "swiper" => [
            "slideDirection" => "horizontal",
            "breakpoints" => [
                "slidesPerView" => [
                    "xs" => 1,
                    "sm" => 2,
                    "md" => 3
                ],
                "spaceBetween" => [
                    "xs" => 20,
                    "sm" => 30,
                    "md" => 40
                ]
            ],
            "initialSlide" => 0,
            "speed" => 1000,
            "loop" => false,
            "autoplay" => false,
            "autoHeight" => false,
            "effect" => "slide",
            "pagination" => [
                "clickable" => false,
                "type" => "bullets"
            ],
            "scrollbar" => [
                "hide" => false
            ],
            "grabCursor" => false,
        ],
        "css" => [
            "card" => [
                "background" => "tranparent"
            ]
        ],
    ];
    public $path = "";
    public $config = [];
    private $assetsUrl = "";

    public function init(){
        parent::init();

        $this->assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
        HtmlHelper::registerCssAndScriptsFiles(["/cmsBuilder/js/globalSearchObj.js"], $this->assetsUrl);


        if (isset($this->config["blockCms"])) {
            if  (isset($this->config["blockCms"]["query"]["fieldShow"])) {
                $this->paramsData["query"]["fieldShow"] = $this->config["blockCms"]["query"]["fieldShow"];
            }
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);

        }
        

        if (!empty($this->config["blockCms"]["query"]["orderBy"])) {
            $this->config["blockCms"]["query"]["sortBy"][$this->config["blockCms"]["query"]["orderBy"]] = $this->config["blockCms"]["query"]["orderType"];

        }
        $contextParent = "parent.".$this->config["costum"]["contextId"];
        $this->config["blockCms"]["query"]["filters"]['$or'] = [];
        $this->config["blockCms"]["query"]["filters"]['$or'][$contextParent] = ['$exists'=>true];
        $this->config["blockCms"]["query"]["filters"]['$or']["source.keys"] = $this->config["costum"]["contextSlug"];

        $list= searchNew::globalautocomplete($this->config["blockCms"]["query"]);

        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
            "assetsUrl" => $this->assetsUrl,
            "dataResult" => $list["results"]
        ]);
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }

}