<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

    use yii\base\Widget;

    class ModalWithAgendaCalendarWidget extends Widget {
        public $defaultData = [
            "labelButton"       => "Boutton",
            "colorlabelButton"  => "#000000",
            "colorBorderButton" => "#000000",
            "colorButton"       => "#ffffff",
            "colorButtonHover" => "#000",
            "colorlabelButtonHover" => "#fff",
            "eventBackgroundColor" => "#2d328d",
            "eventToday" => "#e16d38",
            "showType" => "calendar",
            "image" => "",
            "titreAgenda" => "Votre titre",
            "iconAgenda" => "calendar"
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>