<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Document;
use Yii;
use yii\base\Widget;
use SearchNew;

class CardimageWidget extends Widget
{
    public  $paramsData = [
        "title" => "h1"
    ];
    public $path = "";
    public $config = [];
    private $assetsUrl = "";
    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
        $this->config = array_merge($this->config, [
            "paramsData" => $this->paramsData,
            "assetsUrl" => $this->assetsUrl
        ]);
    }
    public function run()
    {
        return $this->render($this->path, $this->config);
    }

}
