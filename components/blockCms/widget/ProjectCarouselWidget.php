<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

    use Element;
    use yii\base\Widget;

    class ProjectCarouselWidget extends Widget {
        public $defaultData = [
                "title"         => "Block caroussel d'éléments",
                "description"   => "",
                "elementType"   => "projects",
                "background"    => "#FFFFFF",
                "icon"          => [
                    "iconStyle" => "", 
                    "iconColor" => "#000000"
                ],
                "color"         => "",
                "limit"         => 3,
                "iconActionColor" =>  "#ABB76B",
                "imageForm" => "rounded",
                "textPreview" => "En savoir plus",
                "category"      => "",
            
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            if(isset($this->config["costum"]["contextType"]) && isset($this->config["costum"]["contextId"])){
                $this->config['el'] = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>