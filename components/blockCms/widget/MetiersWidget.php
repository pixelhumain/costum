<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use PHDB;
use yii\base\Widget;

class MetiersWidget extends Widget
{
    public $defaultData = [
        "filters" => [
            [
                "type" => "text",
                "value" => "title",
                "description" => "Titre du metier..."
            ],
            [
                "type" => "text",
                "value" => "code",
                "description" => "Code du metier..."
            ],
            [
                "type" => "select",
                "value" => "contractTypes",
                "description" => "Type de contrat..",
                "options" => [
                    [
                        "label" => "CDI",
                        "value" => "cdi",
                    ],
                    [
                        "label" => "CDD",
                        "value" => "cdd"
                    ],
                    [
                        "label" => "Temps plein",
                        "value" => "full-time"
                    ],
                    [
                        "label" => "Temps partiel",
                        "value" => "part-time"
                    ]
                ]
            ],
            [
                "type" => "text",
                "value" => "jobTrends",
                "description" => "Les tendances de l'emploi..."
            ]
        ]
    ];

    public $config = [];
    public $path = "";

    public function init() {
        parent::init();

        $metiers = PHDB::find("metiers");

        if (isset($metiers)) {
            $this->defaultData["metiers"] = $metiers;
        }

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run() {
        return $this->render($this->path, $this->config);
    }
}

?>