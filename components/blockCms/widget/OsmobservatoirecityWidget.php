<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Element;
use yii\base\Widget;
use Osm,HtmlHelper,MongoId;

class OsmobservatoirecityWidget extends Widget
{
    public $paramsData = [
        "css" => [],
    ];
    public $path = "";
    public $config = [];
    public $assetsUrlCitizen = "";

    public $defaultData = [
        "location" => [],
        "typeTag" => [],
        "selectedElement" => [],
        "elts" => [],
    ];

    public function init() {
        parent::init();

        if (isset($this->config["blockCms"])) {
            if(isset($this->config["blockCms"]["selectedElement"]) && $this->config["blockCms"]["selectedElement"] != null)
                $this->defaultData['selectedElement'] =  $this->config["blockCms"]["selectedElement"];
            
            if(isset($this->config["blockCms"]["parent"])){
                foreach($this->config["blockCms"]["parent"] as $id => $parent) {
                    if($parent != null && isset($parent["type"])) {
                        $element = Element::getByTypeAndId($parent["type"], $id);
                        if(count($this->defaultData['location']) == 0 && !empty($element) && isset($element["costum"]) && $element["costum"] != null && isset($element["costum"]["osm"]) && $element["costum"]["osm"] != null && isset($element["costum"]["osm"]["location"])  && $element["costum"]["osm"]["location"] != null )
                            $this->defaultData['location'] = $element["costum"]["osm"]["location"];

                        else if(count($this->defaultData['location']) == 0 && !empty($element) && isset($element["address"]) && $element["address"] != null && isset($element["address"]["addressLocality"]))
                            $this->defaultData['location'] = [$element["address"]["addressLocality"]];
                        
                        if(!empty($element) && isset($element["costum"]) && $element["costum"] != null && isset($element["costum"]["osm"]) && $element["costum"]["osm"] != null && isset($element["costum"]["osm"]["elts"])  && $element["costum"]["osm"]["elts"] != null )
                            $this->defaultData['elts'] = $element["costum"]["osm"]["elts"];
                        
                        
                            
                        if(!empty($element) && isset($element["costum"]) && $element["costum"] != null && isset($element["costum"]["osm"]) && $element["costum"]["osm"] != null && isset($element["costum"]["osm"]["typeTag"])  && $element["costum"]["osm"]["typeTag"] != null )
                            $this->defaultData['typeTag'] = $element["costum"]["osm"]["typeTag"];
                                
                        if(!empty($element) && isset($element["costum"]) && $element["costum"] != null && isset($element["costum"]["osm"]) && $element["costum"]["osm"] != null && isset($element["costum"]["osm"]["selectedElement"])  && $element["costum"]["osm"]["selectedElement"] != null )
                            $this->defaultData['selectedElement'] = $element["costum"]["osm"]["selectedElement"];
                    }
                }
            }

            if(count($this->defaultData['location']) == 0 )
                array_push($this->defaultData['location'], "Fianarantsoa");

            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run(){
        return $this->render($this->path, $this->config );
    }
}