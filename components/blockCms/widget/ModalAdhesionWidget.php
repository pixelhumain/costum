<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\widget;

use Answer;
use Form, Element, PHDB, Yii;
    use yii\base\Widget;

    class ModalAdhesionWidget extends Widget {
        public $defaultData = [
            "btnLabel" => "Je dépose un commun",
            "coformId" => "672db9b36e1df11cb82f3353",
            "modalLabel" => "Formulaire d'Adhesion",
            "textAdhesionSuccess" => "Votre adhésion a bien été enregistrée et le paiement a été reçu avec succès. Vous allez pouvoir accéder aux ressources et évenements de la plateforme."
        ];
        public $config = [];
        public $path = "";
        public $costum = [];
        public function init() {
            parent::init();
            if (!empty($this->config["costum"]["contextId"])) {
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 0);
            }
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            $this->costum = $this->config["costum"];
            $this->config["answer"] = [];
            $this->config["subForms"] = [];

            $this->config["form"] = PHDB::findOneById("forms", $this->config["blockCms"]["coformId"]);
            //$this->config["form"] = null;
            if(!empty($this->config["form"]["subForms"])) {
                $this->config["subForms"] = PHDB::find("forms", ["id" => ['$in' => $this->config["form"]["subForms"]]]);
            }
            if(!empty($this->config["blockCms"]["coformId"]) && isset(Yii::app()->session["userId"])) {
                $this->config["answer"] = PHDB::findOne("answers", ["form" => $this->config["blockCms"]["coformId"], "user" => Yii::app()->session["userId"]]);
                if(empty($this->config["answer"])) {
                    $this->config["answer"] = Answer::generateAnswer($this->config["form"], false, null);
                }
            }
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>