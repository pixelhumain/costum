<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms;

use yii\base\Widget;

class BlockCmsWidget extends Widget{
    private $target_classname = "";

    public $config = [];
    public $path = "";
    public $module = "";
    public $costum = "";
    public $notFoundViewPath = "costum.views.tpls.blockNotFound";

    public function init(){
        parent::init();

        $path_parts = array_reverse(explode(".", $this->path));
        $name = isset($path_parts[0]) ? $path_parts[0]:"";
        $group = isset($path_parts[1]) ? $path_parts[1]:"";

        $this->target_classname = __NAMESPACE__."\\".$group."\\".ucfirst($name)."Widget";
    }

    public function run(){
        if(class_exists($this->target_classname))
            return $this->target_classname::Widget([
                "path" => "costum.views.".$this->path,
                "config" => $this->config
            ]);
        else
            return $this->render($this->notFoundViewPath, $this->config);
    }
}