<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\openatlas;

    use Costum;
    use yii\base\Widget;
    use Element;    

    class BanslideWidget extends Widget {
        public $defaultData = [
            "slideData" => [
                "slide1" => [
                    "title" => [ "fr" =>  "Gestion de contenu <strong>simplifiée</strong>" ],
                    "description" => [ "fr" => "L'outils CMS permet de créer, modifier et gérer votre COstum sans connaissances techniques avancées. Il offre une interface conviviale pour organiser des contenues." ], 
                    "slideBackground" => "images/OpenAtlas/banniere/hero-bg.jpg",
                    "button" => [
                        "text" => [ "fr" => "En savoir plus" ]
                    ],
                    "key" => "slide1"
                ],
                "slide2" => [
                    "title" => [ "fr" => "Personnalisation et <strong>évolutivité</strong>" ],
                    "description" => [ "fr" => "Ils offrent des templates et des blocks CMS permettant de personnaliser l'apparence et les fonctionnalités du COstum pour s'adapter à votre besoins." ],
                    "slideBackground" => "images/OpenAtlas/banniere/hero-bg2.jpg",
                    "button" => [
                        "text" => [ "fr" => "Découvrir" ]
                    ],
                    "key" => "slide2",
                ],
                "slide3" => [
                    "title" => [ "fr" => "Collaboration et gestion des <strong>accès</strong>" ],
                    "description" => [ "fr" => "Ils permettent de collaborer facilement en attribuant différents rôles et permissions aux utilisateurs pour garantir une gestion efficace du contenu tout en maintenant la sécurité." ],
                    "slideBackground" => "images/OpenAtlas/banniere/hero-bg3.jpg",
                    "button" => [
                        "text" => [ "fr" => "Utiliser CMS" ]
                    ],
                    "key" => "slide3"
                ]
            ],
            "imageHero" => [
                "url" => "images/OpenAtlas/banniere/banner_illustration.png"
            ]
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                if (isset($this->config["blockCms"]["slideData"])) {
                    $this->defaultData["slideData"] = $this->config["blockCms"]["slideData"];
                }
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>