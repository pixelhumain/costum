<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;
    use yii\base\Widget;

    class ReseauxSociauxWidget extends Widget
    {
        public $defaultData = [
            "iconRotation" => false,
            "css" => array(
                "social" => array(
                    "color" => "#ffffff",
                    "backgroundColor" => "#D3D3D3",
                    "fontSize" => "24px",
                    "borderBottomLeftRadius" => "50%",
                    "borderBottomRightRadius" => "50%",
                    "borderTopLeftRadius" => "50%",
                    "borderTopRightRadius" => "50%",
                    "width" => "50px",
                    "height" => "50px"
                ),
                "hover" => array(
                    "social" => array(
                        "color" => "#D3D3D3",
                        "backgroundColor" => "#ffffff",
                        "fontSize" => "24px",
                        "borderBottomLeftRadius" => "50%",
                        "borderBottomRightRadius" => "50%",
                        "borderTopLeftRadius" => "50%",
                        "borderTopRightRadius" => "50%",
                        "width" => "50px",
                        "height" => "50px"
                    ),
                )
                
            ),
            "socialNetwork" => null
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>