<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;

    use yii\base\Widget;

    class FooterWithCollaboWidget extends Widget {
        public $defaultData = [
            "titleColumn1" => "Porteur du projet",
            "titleColumn2" => "Supporteur du projet",
            "titleSocialMedia" => "Suivez-nous sur :",
            "apropos"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Nam convallis enim a finibus congue. Phasellus et tristique dolor. Integer volutpat viverra ornare.",
            "css" => array(
                "logo" => array(
                    "height" => "50px"
                ),
                "icone" => array(
                    "color" => "#dddddd",
                    "backgroundColor" => "white",
                    "borderColor" => "black"
                ),
                "hover" => array(
                    "icone" => array(
                        "color" => "white",
                        "backgroundColor" => "#dddddd",
                        "borderColor" => "black"
                    )
                )
            ), 
            "logoPorteur"=> [],  
            "logoSupporteur" =>[],  
            "socialLinks" => array(
                "links"=> array(
                    [
                        "link" => "",
                        "inputIcon" => "facebook"
                    ],
                    [
                        "link" => "",
                        "inputIcon" => "youtube-play"
                    ],
                    [
                        "link" => "",
                        "inputIcon" => "instagram"
                    ],
                    [
                        "link" => "",
                        "inputIcon" => "linkedin"
                    ]
                )
            ), 
            "colorButton"=>"#005E6F",
            "backgroundBoutton"=>"",
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>