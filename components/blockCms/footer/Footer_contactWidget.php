<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;
    use yii\base\Widget;

    class Footer_contactWidget extends Widget
    {
        public $defaultData = [
            "titleBlock"    => "CONTACTS",
            "subtitleBlock" => "Sous titre",
            "contact"       => "+0000",
            "mail"          => "mail@mail.com",
            "css" => array(
                "social" => array(
                    "color" => "#ffffff",
                    "backgroundColor" => "#D3D3D3"
                ),
                "hover" => array(
                    "social" => array(
                        "color" => "#D3D3D3",
                        "backgroundColor" => "#ffffff"
                    ),
                )
                
            ),
            "socialNetwork" => null
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>