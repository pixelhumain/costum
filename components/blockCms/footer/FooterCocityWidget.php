<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;
    use yii\base\Widget;

    class FooterCocityWidget extends Widget {
        public $defaultData = [
            'firstColumn' =>
            "<div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\"><br></font></div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\"> ENSEMBLE\n\t\tTROUVER&nbsp;</font><div><font size=\"6\" color=\"#ffffff\" face=\"SharpSans-No1-Medium\">et AJOUTER DES ACTIONS LOCALES POSITIVES</font></div>",
            'secondColumn' =>
            "<div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\"><br></font></div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\"> Se rassembler\n\t\tpour regler des problème mondiaux</font><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;</font><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #covidFreedom&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #deforestation&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #climateChange&nbsp;</font></div><div><font color=\"#ffffff\" size=\"5\" face=\"SharpSans-No1-Medium\">&nbsp;- #LocalPositif</font></div></div>",
            'thirdColumn' =>
            "<div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\"><br></font></div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">CONNECTER LE LOCAL&nbsp;</font><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\"><br></font><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Producteur</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Artisans</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Ingenieurs</font><span style=\"font-size: x-large; color: rgb(255, 255, 255); font-family: SharpSans-No1-Medium;\">&nbsp;</span></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Contributeur&nbsp;</font></div><div><font color=\"#ffffff\" face=\"SharpSans-No1-Medium\" size=\"5\">- Etc...</font></div></div>",
            'blockCmsBgColor' => '#052434',
            'tpl' => ""
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>