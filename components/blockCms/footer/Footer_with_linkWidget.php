<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;

    use yii\base\Widget;

    class Footer_with_linkWidget extends Widget {
        public $defaultData = [
            "text1" => "     <u><font face=\"OpenSans-Bold\" size=\"3\">         CONTACT</font></u><div><font face=\"OpenSans-Bold\"><u><br></u></font><div><font face=\"OpenSans-Bold\" size=\"5\">Technopole de la réunion&nbsp;</font></div><div><font face=\"OpenSans-Bold\" size=\"5\"><br></font></div><div><font face=\"OpenSans-SemiBold\">14 rue Henri Cornu&nbsp;</font></div><div><font face=\"OpenSans-SemiBold\">Immeuble Darwin</font></div><div><font face=\"OpenSans-SemiBold\">97490 Sainte-Clotilde</font></div><div><font face=\"OpenSans-SemiBold\">Tél : 0262 90 71 80 – Fax : 0262 90 71 81</font></div><div><font face=\"OpenSans-SemiBold\">Email :&nbsp;<a target=\"_blank\" href=\"<span style=\" color:=\"\" rgb(32,=\"\" 33,=\"\" 36\"=\"\" style=\"\"><font color=\"#bdd424\" style=\"\">courrier@technopole-reunion.com</font></a></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\">© 2021 – Copyright Technopole de La Réunion<br style=\"\">Tous droits réservés –</font><font face=\"-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif\">&nbsp;</font><a target=\"_blank\" href=\"https://technopole-reunion.com/mentions-legales/\" style=\"\"><font color=\"#bdd424\" face=\"OpenSans-SemiBold\">Mentions légales</font></a></div><div><br></div></div>",
            "text2" => "        <div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/la-technopole/\"><font color=\"#ffffff\">LA TECHNOPOLE</font></a></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/nos-missions/\"><font color=\"#ffffff\">NOS MISSIONS</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/lincubateur/\"><font color=\"#ffffff\">L'INCUBATEUR</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/la-reunion-innovante/\"><font color=\"#ffffff\">LA RÉUNION INNOVANTE</font></a><br></font></div><div><br></div><div><font face=\"OpenSans-SemiBold\" size=\"3\" style=\"<br>    line-height: 35px;<br>\"><a target=\"_blank\" href=\"https://technopole-reunion.com/contact/\"><font color=\"#ffffff\">CONTACTEZ-NOUS</font></a><br></font></div><div><br></div><div><a target=\"_blank\" href=\"https://technopole-reunion.com/adherer/\"><font face=\"OpenSans-SemiBold\" size=\"3\" color=\"#ffffff\" style=\"<br>    line-height: 35px;<br>\">ADHÉRER</font></a><br></div>",
            "text3" => "        <a target=\"_blank\" href=\"https://technopole-reunion.com/politique-de-confidentialite/\"><font color=\"#ffffff\" size=\"3\" face=\"OpenSans-SemiBold\" style=\"<br>    line-height: 35px;<br>\">POLITIQUE DE CONFIDENTIALITÉ</font></a><div><br><div><a target=\"_blank\" href=\"https://technopole-reunion.com/politique-des-cookies/\"><font color=\"#ffffff\" size=\"3\" face=\"OpenSans-SemiBold\">POLITIQUE DES COUKIES</font></a><br></div></div>",
            "css" => array(
                "icone" => array(
                    "color" => "white",
                    "backgroundColor" => "#2C3E50"
                ),
                "hover" => array(
                    "icone" => array(
                        "color" => "white",
                        "backgroundColor" => "#788713"
                    ),
                )
            ),
            "socialNetwork" => null,
            "financeur" => ""
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>