<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;
    use yii\base\Widget;

    class NewCocityWidget extends Widget {
        public $defaultData = [
            "css" => array(
                "bouton" => array(
                    "color" => "#092434",
                    "backgroundColor" => "white",
                    "border" => "1px solid #092434",
                    "fontSize" => "24px"
                ),
                "hover" => array(
                    "bouton" => array(
                        "color" => "white",
                        "backgroundColor" => "#092434",
                        "border" => "1px solid #092434",
                        "fontSize" => "24px"
                    )
                )
            )
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>