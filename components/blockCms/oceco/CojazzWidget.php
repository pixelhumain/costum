<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\oceco;
    use yii\base\Widget;
    use Element;
    use PHDB;
    use Project;
    use Event;
    use Badge;
    use DateTime;
    use Action;
    use Actions;
    use MongoDB\BSON;
    use MongoDB\BSON\UTCDateTime;
    use MongoId;

    class CoJazzWidget extends Widget {
        public $defaultData = [
            "roleToShow" => ["Développeur","CDP","Pixelhumain","Design"],
            "range" => "150",
        ];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();
            ini_set('memory_limit', '20M');
            set_time_limit(5000);  

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }

            $this->config["countAllProjectChild"] = PHDB::count(Project::COLLECTION, 
                    [
                        '$or' => [
                            ["parentId" => $this->config["costum"]["contextId"]],
                            ["parent.". $this->config["costum"]["contextId"] => ['$exists' => true]]
                        ]
                    ]
            );
            $this->config["countAllEventChild"] = PHDB::count(Event::COLLECTION,
                    [ 
                        '$or'	=> [
                            ["parent." . $this->config["costum"]["contextId"] => ['$exists' => true]], 
                            ["organizer." . $this->config["costum"]["contextId"] => ['$exists' => true]]
					    ]
                    ]
            );
            $this->config['allProjectAndEventChild'] = self::getAllChild($this->config["costum"]["contextId"]);
            $this->config["statPerStatus"] = self::countAllActionsByStatus($this->config["costum"]["contextId"],$this->config['allProjectAndEventChild']);
            $this->config["allCommingEvent"] = self::getAllComminEvent($this->config["costum"]["contextId"],$this->config["blockCms"]["range"]);
            $this->config["allProjectActive"] = self::getAllActiveProject($this->config["costum"]["contextId"],$this->config["blockCms"]["range"]);
            $oceco = PHDB::findOneById($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] , ["oceco"] );
            
            $ocecoTags = $oceco["oceco"]["tags"] ?? [] ;
            $this->config["oceco"] = $oceco["oceco"] ?? [];
            $poleProject = self::getAllPole($this->config["costum"]["contextId"]);
            $this->config["poleProject"] = $poleProject;
            $this->config["pole"] = array_unique(array_merge($poleProject,$ocecoTags));
            $this->config["activityThisYear"] = self::activityThisYear($this->config['allProjectAndEventChild']);
            // echo "<pre>";
            // var_dump($this->config["activityThisYear"]);die();

            $this->config["allRoles"] = self::getAllRoles($this->config["costum"]["contextId"],$this->config["costum"]["contextType"]);
            $this->config["allBadges"] = PHDB::find(Badge::COLLECTION, ["parent.". $this->config["costum"]["contextId"] => ['$exists' => true]],["name","profilImageUrl"]);
        }
        /**
         * Run the widget
         * 
         * @return string
         */
        public function run() {
            return $this->render($this->path, $this->config);
        }

        public function getAllActiveProject($contextId,$range) {
            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                        ],
                        [
                            '$project' => [
                                'ids' => [
                                    '$map' => [
                                        'input' => '$ids',
                                        'as' => 'id', 
                                        'in' => [
                                            '$toString' => '$$id' 
                                        ]
                                    ]
                                ]
                            ]
                        ]
                ]
            );

            $currentDate = new DateTime();
            $daysAgo = (clone $currentDate)->modify('-' . $range . ' days');
            $daysPeriode = new UTCDateTime($daysAgo->getTimestamp() * 1000);

            if (isset($projectsChild["result"][0]["ids"])) {
                
                $activities = PHDB::aggregate(
                    Actions::COLLECTION,
                    [
                        // Stage 1: Filtrer les documents selon les critères
                        [
                            '$match' => [
                                'parentId' => ['$in' => $projectsChild["result"][0]["ids"] ],
                                '$or' => [
                                    ['startDate' => ['$gte' => $daysPeriode]],
                                    ['endDate' => ['$gte' => $daysPeriode]]
                                ]
                            ]
                        ],
                        [
                            '$group' => [
                                '_id' => '$idUserAuthor',
                                'actions' => [
                                    '$push' => [
                                        '_id' => '$_id',
                                        'status' => '$status',
                                        'name' => '$name',
                                        'parentId' => '$parentId',
                                        'authorId' => '$idUserAuthor',
                                        'parentType' => '$parentType',
                                        'tracking' => '$tracking',
                                        'tags' => '$tags',  
                                        'tasks' => '$tasks'
                                    ]
                                ],
                                'todoCount' => [
                                    '$sum' => ['$cond' => [['$eq' => ['$status', 'todo']], 1, 0]]
                                ],
                                'doneCount' => [
                                    '$sum' => ['$cond' => [['$eq' => ['$status', 'done']], 1, 0]]
                                ],
                                'distinctParentIds' => [
                                    '$addToSet' => [
                                        'parentId' => '$parentId',
                                        'parentType' => '$parentType'
                                    ]
                                ],
                            ]
            
                        ],
                        // Stage 3: Projeter les résultats dans un format utilisable
                        [
                            '$project' => [
                                '_id' => 0,  // Supprimer _id
                                'userId' => '$_id',  // Renommer _id en userId
                                'actions' => 1,  // Garder les actions
                                'todoCount' => 1, // Ajouter le nombre de "todo"
                                'doneCount' => 1, // Ajouter le nombre de "done"
                                'distinctParentIds' => 1 // Garder les parentId distincts
                            ]
                        ]
                    ]
                );


                if (empty($activities['result'])) {
                    return [];
                } else {
                    $result = [];
                    $allProject = [];
                    foreach ($activities['result'] as $activity) {
                        $parentDetails = [];
                        $activityPerProject = [];
                        $parentIds = array_column($activity['distinctParentIds'], 'parentId', 'parentType');
                        
                        foreach ($parentIds as $parentType => $parentId) {
                            $parent = PHDB::findOne($parentType, ['_id' => new MongoId($parentId)]);
                            if ($parent) {
                                $parentData = [
                                    'id' => $parentId,
                                    'parentId' => $parentId,
                                    'parentType' => $parentType,
                                    'name' => $parent['name'],
                                    'profilImageUrl' => $parent['profilImageUrl'] ?? null,
                                    'slug' => $parent['slug']
                                ];
                    
                                if (!isset($allProject[$parentId])) {
                                    $allProject[$parentId] = $parentData;
                                }
                    
                                $parentDetails[] = $parentData;
                                $activityPerProject[$parentId] = [];
                            }
                        }
                    }

                    return array_values($allProject); 
                }
            } else {
                return [];
            }
        }


        public function activityThisYear($allChild) {
            $dateAujourdhui = new DateTime();

            $dateDebutAnnee = new DateTime('first day of January this year');
            $dateDebutAnnee->setTime(0, 0, 0);

            $timestampDebutAnnee = $dateDebutAnnee->getTimestamp();
            $timestampAujourdhui = $dateAujourdhui->getTimestamp();

        
            $activities = PHDB::aggregate(
                Actions::COLLECTION,
                [
                    [
                        '$match' => [
                            'parentId' => ['$in' => $allChild ],
                            '$or' => [
                                [
                                    'created' => [
                                        '$gte' => $timestampDebutAnnee,
                                        '$lte' => $timestampAujourdhui
                                    ],
                                    'updated' => ['$exists' => false]
                                ],
                                [
                                    'updated' => [
                                        '$gte' => $timestampDebutAnnee,
                                        '$lte' => $timestampAujourdhui
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        '$project' => [
                            'dates' => [
                                '$cond' => [
                                    'if' => [
                                        '$and' => [
                                            ['$ne' => ['$created', '$updated']],
                                            ['$gt' => ['$updated', null]]
                                        ]
                                    ],
                                    'then' => ['$created', '$updated'],
                                    'else' => ['$created']
                                ]
                            ]
                        ]
                    ],
                    [
                        '$unwind' => '$dates'
                    ],
                    [
                        '$group' => [
                            '_id' => [
                                'date' => [
                                    '$dateToString' => [
                                        'format' => "%Y-%m-%d",
                                        'date' => [
                                            '$toDate' => [
                                                '$multiply' => ['$dates', 1000]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'actionCount' => ['$sum' => 1]
                        ]
                    ],
                    [
                        '$sort' => [
                            '_id.date' => 1
                        ]
                    ]
                ]
            );
        
            $filteredResults = [];
            foreach ($activities['result'] as $activity) {
                if ( isset($activity['_id']['date']) ){
                    $date = new DateTime($activity['_id']['date']);
                    if ($date >= $dateDebutAnnee && $date <= $dateAujourdhui) {
                        $filteredResults[$activity['_id']['date']] = [ 'nombre' => $activity['actionCount'] ];
                    }
                }
            }
        
            return $filteredResults;
        }

        public function getAllComminEvent($contextId,$range) {

            $currentDate = new DateTime(); 
            $endOfPeriod = (clone $currentDate)->modify('+' . $range . ' days');
            $currentDateUTC = new UTCDateTime($currentDate->getTimestamp() * 1000);
            $endOfPeriodUTC = new UTCDateTime($endOfPeriod->getTimestamp() * 1000);
            

            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );

            $idsProject = $projectsChild["result"][0]["ids"] ?? [];
            $possibleParent = array_merge([$contextId], $idsProject);
            $eventChild = PHDB::aggregate(
                Event::COLLECTION,
                [
                    [
                        '$match' => [
                            '$and' => [
                                [
                                    '$or' => [
                                        [ "parent.$contextId" => ['$exists' => true] ],
                                        ['parentId' => ['$in' => $possibleParent]],
                                        [
                                            '$or' => array_map(
                                                fn($childId) => ["organizer.$childId" => ['$exists' => true]], 
                                                $possibleParent
                                            )
                                        ]
                                    ]
                                ],
                                [
                                    '$or' => [
                                        [
                                            '$and' => [
                                                ['startDate' => ['$gte' => $currentDateUTC]],
                                                ['startDate' => ['$lte' => $endOfPeriodUTC]],
                                                ['endDate' => ['$gte' => $currentDateUTC]]
                                            ]
                                        ],
                                        [
                                            'endDate' => ['$gte' => $currentDateUTC]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        '$sort' => [
                            'startDate' => 1
                        ]
                    ],
                    [
                        '$project' => [
                            'name' => 1,
                            'profilImageUrl' => 1,
                            'slug' => 1,
                            'startDate' => 1,
                            'endDate' => 1
                        ]
                    ]
                ]
            );
        
            return empty($eventChild['result']) ? [] : $eventChild['result'];
        }


        public function getAllChild($contextId){

            $projectsAndEventChild = [];

            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );
        
            $idsProject = $projectsChild["result"][0]["ids"] ?? [];
        
            $eventsChild = PHDB::aggregate(
                Event::COLLECTION,
                [
                    [
                        '$match' => [
                            "organizerId" => ['$in' => $idsProject]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );
        
            $idsEvents = $eventsChild["result"][0]["ids"] ?? [];
        
            $projectsAndEventChild = array_merge($idsProject, $idsEvents);
            array_unshift($projectsAndEventChild, $contextId);
            
            return $projectsAndEventChild;
        }

        public function getAllPole($contextId) {
            $tagsProject = [];
            $tagsProject = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            'tags' => 1
                        ]
                    ],
                    [
                        '$unwind' => '$tags'
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'tags' => ['$addToSet' => '$tags']
                        ]
                    ]
                ]
            );
            
            $tagsProject = $tagsProject["result"][0]["tags"] ?? [];
            
            return $tagsProject;
            
            
        }

        public function countAllActionsByStatus($contextId,$projectsAndEventChild) {
        
            $activities = PHDB::aggregate(
                Actions::COLLECTION,
                [
                    [
                        '$match' => [
                            'parentId' => ['$in' => $projectsAndEventChild]
                        ]
                    ],
                    [
                        '$project' => [
                            'tags' => 1,
                            'status' => 1,
                            'tracking' => 1
                        ]
                    ]
                ]
            );
        
            $actions = $activities["result"] ?? [];
        
            $statusesCount = [
                "done" => 0,
                "totest" => 0,
                "tracking" => 0,
                "next" => 0,
                "discuter" => 0,
                "todo" => 0,
                "total" => count($actions)
            ];
        
            foreach ($actions as $action) {
                $status = self::get_action_status($action);
                if (isset($statusesCount[$status])) {
                    $statusesCount[$status]++;
                }
            }
        
            return $statusesCount;
        }

        public function getAllRoles($contextId,$contextType){
            $linksData = PHDB::findOne($contextType, ['_id' => new MongoId($contextId)],["links","roles"]);
            $allRoles = [];

            foreach ($linksData['links']['members'] as $link) {
                    if (isset($link['type']) && $link['type'] === 'citoyens' && !empty($link['roles'])) {
                        $allRoles = array_merge($allRoles, $link['roles']);
                    }
            } 
            $uniqueRoles = [];
            if(isset($linksData['roles'])){
                $uniqueRoles = array_unique(array_merge($linksData['roles'],$allRoles));
            } else {
                $uniqueRoles = array_unique($allRoles);
            }

            return [ 'uniqueRoles' => $uniqueRoles , 'usersRoles' => $allRoles];
        }
        
        public static function get_action_status($action) {
            $has_tags = !empty($action["tags"]) && is_array($action["tags"]);
            $status = $action["status"] ?? "";
            $is_tracking = !empty($action["tracking"]) && filter_var($action["tracking"], FILTER_VALIDATE_BOOL);
            $lower_tags = $has_tags ? array_map(fn($t) => strtolower($t), $action["tags"]) : [];
            if ($status === "done") return "done";
            if (in_array("totest", $lower_tags)) return "totest";
            if ($is_tracking) return "tracking";
            if (in_array("next", $lower_tags)) return "next";
            if (in_array("discuter", $lower_tags)) return "discuter";
            return "todo";
        }
        
        
    }
?>