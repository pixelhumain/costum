<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\oceco;

    use yii\base\Widget;
    use Element;
    use PHDB;
    use Project;
    use Organization;
    use Event;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;


    class ActionsByPoleWidget extends Widget {
        public $defaultData = [
            "range" => "7",
        ];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
            
            $oceco = PHDB::findOneById($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] , ["oceco"] );
            $this->config["configOceco"] = $oceco["oceco"] ?? [];
            if (isset($oceco["oceco"]["pole"]) && $oceco["oceco"]["pole"]) {
                $ocecoTags = $oceco["oceco"]["tags"] ?? [] ;
                $this->config["pole"] = array_unique(array_merge(self::getAllPole($this->config["costum"]["contextId"]),$ocecoTags));
            }
        }
        /**
         * Run the widget
         * 
         * @return string
         */
        public function run() {
            if (isset($this->config["configOceco"]["pole"]) && $this->config["configOceco"]["pole"]) {
                return $this->render($this->path, $this->config);
            } else {
                return null ;
            }
        }

        public function getAllPole($contextId) {
            $tagsProject = [];
            $tagsProject = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            'tags' => 1
                        ]
                    ],
                    [
                        '$unwind' => '$tags' // Séparation des tags en éléments uniques
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'tags' => ['$addToSet' => '$tags'] // Récupération des tags uniques
                        ]
                    ]
                ]
            );
            
            $tagsProject = $tagsProject["result"][0]["tags"] ?? [];
            
            return $tagsProject;
            
            
        }
    }
?>