<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\oceco;

    use yii\base\Widget;
    use Element;
    use PHDB;
    use Project;
    use Event;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;


    class CommunityScrumWidget extends Widget {
        public $defaultData = [
            "range" => "7",
        ];
        public $defaultparentType = ['projects', 'organizations' , 'events'];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }

            if ( !isset($this->config["blockCms"]["parentType"]) ) {
                $this->config["blockCms"]["parentType"] = $this->defaultparentType;
            }

            $oceco = PHDB::findOneById($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] , ["oceco"] );
            $this->config["configOceco"] = $oceco["oceco"] ?? [];

        }
        /**
         * Run the widget
         * 
         * @return string
         */
        public function run() {
            if (!isset($this->config["configOceco"]["pole"]) || !$this->config["configOceco"]["pole"]) {
                return $this->render($this->path, $this->config);
            } else {
                return null ;
            }
        }
    }
?>