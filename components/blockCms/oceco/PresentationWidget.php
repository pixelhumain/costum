<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\oceco;
    use yii\base\Widget;
    use Element;
    use PHDB;
    use Project;
    use Event;
    use Badge;
    use DateTime;
    use Action;
    use Actions;
    use MongoDB\BSON;
    use MongoDB\BSON\UTCDateTime;
    use MongoId;

    class PresentationWidget extends Widget {
        public $defaultData = [
            "roleToShow" => []
        ];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();
            ini_set('memory_limit', '200M');
            set_time_limit(5000);  

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
            $ocecoTags = $oceco["oceco"]["tags"] ?? [] ;
            $this->config["oceco"] = $oceco["oceco"] ?? [];
            $poleProject = self::getAllPole($this->config["costum"]["contextId"]);
            $this->config["pole"] = array_unique(array_merge($poleProject,$ocecoTags));

            $this->config['allEventChild'] = self::getAllRecentEventChild($this->config["costum"]["contextId"]);
            $this->config['allOldEventChild'] = self::getAllOldEventChild($this->config["costum"]["contextId"]);
            $this->config['allProjectChild'] = PHDB::findAndSort(Project::COLLECTION,["parent.{$this->config["costum"]["contextId"]}" => ['$exists' => true]], array("created" => 1), 40 ,["name","slug","profilImageUrl","tags","shortDescription"]);
            $this->config['allActiveProjectChild'] = self::getAllActiveProject($this->config["costum"]["contextId"]);
        }
        /**
         * Run the widget
         * 
         * @return string
         */
        public function run() {
            return $this->render($this->path, $this->config);
        }

        public function getAllRecentEventChild($contextId) {

            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );

            $currentDate = new DateTime();
            $actualDate = new UTCDateTime($currentDate->getTimestamp() * 1000);

            $idsProject = $projectsChild["result"][0]["ids"] ?? [];
            $possibleParent = array_merge([$contextId], $idsProject);
            $eventChild = PHDB::aggregate(
                Event::COLLECTION,
                [
                    [
                        '$match' => [
                            '$and' => [
                                [
                                    '$or' => [
                                        [ "parent.$contextId" => ['$exists' => true] ],
                                        ['parentId' => ['$in' => $possibleParent]],
                                        [
                                            '$or' => array_map(
                                                fn($childId) => ["organizer.$childId" => ['$exists' => true]], 
                                                $possibleParent
                                            )
                                        ]
                                    ]
                                ],
                                // Condition pour récupérer les événements futurs ou en cours
                                [
                                    '$or' => [
                                        ['startDate' => ['$gt' =>  $actualDate]], // Événements à venir
                                        [
                                            '$and' => [
                                                ['startDate' => ['$lte' =>  $actualDate]], // Déjà commencé
                                                ['endDate' => ['$gt' =>  $actualDate]] // Pas encore terminé
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        '$sort' => [
                            'startDate' => 1 // Tri par date croissante (les plus proches en premier)
                        ]
                    ],
                    [
                        '$project' => [
                            'id' => ['$toString' => '$_id'], 
                            'name' => 1,
                            'profilImageUrl' => 1,
                            'profile' => 1,
                            'slug' => 1,
                            'organizer' => 1,
                            'links' => 1,
                            'short_description' => 1,
                            'address' => 1,
                            'startDate' => [
                                '$dateToString' => [
                                    'format' => "%d/%m/%Y %H:%M",
                                    'date' => '$startDate'
                                ]
                            ],
                            'endDate' => [
                                '$dateToString' => [
                                    'format' => "%d/%m/%Y %H:%M",
                                    'date' => '$endDate'
                                ]
                            ]
                        ]
                    ]
                ]
            );

            
            $eventChildAssoc = [];
            foreach ($eventChild['result'] as $event) {
                $eventChildAssoc[$event['id']] = $event;
            }

            return empty($eventChildAssoc) ? [] : $eventChildAssoc;

        }

        public function getAllOldEventChild($contextId) {
            // Récupérer les sous-projets du contexte donné
            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );
        
            $currentDate = new DateTime();
            $actualDate = new UTCDateTime($currentDate->getTimestamp() * 1000);
        
            $idsProject = $projectsChild["result"][0]["ids"] ?? [];
            $possibleParent = array_merge([$contextId], $idsProject);
        
            $eventChild = PHDB::aggregate(
                Event::COLLECTION,
                [
                    [
                        '$match' => [
                            '$and' => [
                                [
                                    '$or' => [
                                        [ "parent.$contextId" => ['$exists' => true] ],
                                        ['parentId' => ['$in' => $possibleParent]],
                                        [
                                            '$or' => array_map(
                                                fn($childId) => ["organizer.$childId" => ['$exists' => true]], 
                                                $possibleParent
                                            )
                                        ]
                                    ]
                                ],
                                // 🚀 Condition pour récupérer uniquement les événements passés
                                [
                                    'endDate' => ['$lt' => $actualDate] // Finie avant la date actuelle
                                ]
                            ]
                        ]
                    ],
                    [
                        '$sort' => [
                            'startDate' => -1 // Tri par date décroissante (les plus récents en premier)
                        ]
                    ],
                    [
                        '$limit' => 30 // 🚀 Limite à 20 résultats
                    ],
                    [
                        '$project' => [
                            'id' => ['$toString' => '$_id'], 
                            'name' => 1,
                            'profilImageUrl' => 1,
                            'profile' => 1,
                            'slug' => 1,
                            'organizer' => 1,
                            'links' => 1,
                            'short_description' => 1,
                            'address' => 1,
                            'startDate'=> [
                                '$dateToString' => [
                                    'format' => "%d/%m/%Y %H:%M",
                                    'date' => '$startDate'
                                ]
                            ],
                            'endDate'=> [
                                '$dateToString' => [
                                    'format' => "%d/%m/%Y %H:%M",
                                    'date' => '$endDate'
                                ]
                            ],
                        ]
                    ]
                ]
            );
        
            $eventChildAssoc = [];
            foreach ($eventChild['result'] as $event) {
                $eventChildAssoc[$event['id']] = $event;
            }
        
            return empty($eventChildAssoc) ? [] : $eventChildAssoc;
        }

        public function getAllActiveProject($contextId) {
            // Récupérer les IDs des projets actifs
            $projectsChild = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            '_id' => 1
                        ]
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'ids' => ['$push' => '$_id']
                        ]
                    ],
                    [
                        '$project' => [
                            'ids' => [
                                '$map' => [
                                    'input' => '$ids',
                                    'as' => 'id',
                                    'in' => ['$toString' => '$$id']
                                ]
                            ]
                        ]
                    ]
                ]
            );
        
            $currentDate = new DateTime();
            $daysAgo = (clone $currentDate)->modify('-300 days');
            $daysPeriode = new UTCDateTime($daysAgo->getTimestamp() * 1000);
        
            if (isset($projectsChild["result"][0]["ids"])) {
                // Récupérer les actions et calculer la dernière action par projet
                $activities = PHDB::aggregate(
                    Actions::COLLECTION,
                    [
                        [
                            '$match' => [
                                'parentId' => ['$in' => $projectsChild["result"][0]["ids"]],
                                'parentType' => "projects",
                                '$or' => [
                                    ['startDate' => ['$gte' => $daysPeriode]],
                                    ['endDate' => ['$gte' => $daysPeriode]]
                                ]
                            ]
                        ],
                        [
                            '$addFields' => [
                                'created' => [
                                    '$toDate' => [
                                        '$multiply' => ['$created', 1000] 
                                    ]
                                ]
                            ]
                        ],
                        [
                            '$group' => [
                                '_id' => '$parentId',
                                'parentType' => ['$first' => '$parentType'],
                                'maxCreated' => ['$max' => '$created']
                            ]
                        ],
                        [
                            '$sort' => ['maxCreated' => -1]
                        ],
                        [
                            '$project' => [
                                '_id' => 0,
                                'parentId' => '$_id',
                                'parentType' => 1,
                                'maxCreated' => 1
                            ]
                        ]
                    ]
                );
        
                if (empty($activities['result'])) {
                    return [];
                } else {
                    $allProject = [];
        
                    foreach ($activities['result'] as $activity) {
                        $parent = PHDB::findOne($activity['parentType'], ['_id' => new MongoId($activity['parentId'])]);
        
                        if ($parent) {
                            $parentData = [
                                'id' => $activity['parentId'],
                                'parentId' => $activity['parentId'],
                                'parentType' => $activity['parentType'],
                                'name' => $parent['name'],
                                'profilImageUrl' => $parent['profilImageUrl'] ?? null,
                                'slug' => $parent['slug'],
                                'tags' => $parent['tags'] ?? [],
                                'shortDescription' => $parent['shortDescription'] ?? "",
                                'lastActionDate' => $activity['maxCreated'] // Ajout de la dernière action
                            ];
        
                            $allProject[] = $parentData;
                        }
                    }
        
                    return $allProject; // Les projets sont déjà triés par activité récente
                }
            } else {
                return [];
            }
        }

        public function getAllPole($contextId) {
            $tagsProject = [];
            $tagsProject = PHDB::aggregate(
                Project::COLLECTION,
                [
                    [
                        '$match' => [
                            "parent.{$contextId}" => ['$exists' => true]
                        ]
                    ],
                    [
                        '$project' => [
                            'tags' => 1
                        ]
                    ],
                    [
                        '$unwind' => '$tags' // Séparation des tags en éléments uniques
                    ],
                    [
                        '$group' => [
                            '_id' => null,
                            'tags' => ['$addToSet' => '$tags'] // Récupération des tags uniques
                        ]
                    ]
                ]
            );
            
            $tagsProject = $tagsProject["result"][0]["tags"] ?? [];
            
            return $tagsProject;
            
            
        }
        
        
    }
?>