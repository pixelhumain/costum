<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\contact;

    use yii\base\Widget;

    class ContactFormWidget extends Widget {
        public $defaultData = [
            "btnBgColor"        => "#bca87d",
            "btnLabelColor"     => "white",
            "btnBorderColor"    => "#eeeeee",
            "btnRadius"         => "4",
            "btnLabel"          => "ME CONTACTER",
            "btnPosition"       => "center",
            "titleBlock"        => "",
            "subTitleBlock"     => "Envoyer un e-mail à :",
            "showForm"          => "show",
            "labelAlign"        => "left",
            "sendBtnLabel"      => "Envoyer le message",
            "sendBtnAlign"      => "left",
            "showCharte"        => true,
            "agreeMsgInfo"      => "J’accepte que la Technopole de la Réunion utilise les informations que j’ai fournies à des fins de prospection commerciale.",		
            "agreeMsgAgree"     => "En envoyant ce formulaire , j'accepte que les informations saisies soit utilisées par la Technopole de la Réunion dans le cadre strict de ma demande et j'accepte également la politique de confidentialité *",	
            "mailFeedBack"      => false,
            "css" => array(
                "label" => array(
                    "color"        => "#000000",
                    "fontSize"         => "15",
                ),
                "champ" => array(
                    "fontSize" => "16",
                    "height" => "38",
                    "borderType" => "border-bottom",
                    "backgroundColor" => "#ffffff",
                    "color" => "#555555",
                    "border" => "2px solid black",
                    "borderBottomLeftRadius" => "5px",
                    "borderBottomRightRadius" => "5px",
                    "borderTopLeftRadius" => "5px",
                    "borderTopRightRadius" => "5px",
                ),
                "sendBouton" => array(
                    "fontSize" => "15",
                    "backgroundColor" => "#18BC9C",
                    "color" => "white",
                    "borderColor" => "#18BC9C",
                    "borderBottomLeftRadius" => "15px",
                    "borderBottomRightRadius" => "15px",
                    "borderTopLeftRadius" => "15px",
                    "borderTopRightRadius" => "15px",
                ),
                "subTitleBlockTextSize" => array(
                    "fontSize" => "25"
                ),
                "hover" => array(
                    "sendBouton" => array(
                        "backgroundColor" => "#18BC9C",
                        "color" => "white",
                        "borderColor" => "#18BC9C",
                    ),
                    "openModBouton" => array(
                        "backgroundColor" => "white",
                        "color" => "#bca87d",
                        "borderColor" => "#bca87d",
                    )
                )
            ),
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>