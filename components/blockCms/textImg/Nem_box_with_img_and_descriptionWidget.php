<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\textImg;

    use Organization;
    use Person;
    use PHDB;
    use yii\base\Widget;

    class Nem_box_with_img_and_descriptionWidget extends Widget {
        public $defaultData = [
            // "colorDecor" => "#0dab76",
            // "titleColor" => "white",
            // "textColor" => "white",
            
            "title" => "OUR TEAM",
            "css" => array(
                "boxCss" => array(
                    "color" => "white",
                    "backgroundColor" => "#0dab76"
                ),
                "cardCss" => array(
                    "color" => "white"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>