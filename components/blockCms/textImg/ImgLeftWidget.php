<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\textImg;

    use Organization;
    use Person;
    use PHDB;
    use yii\base\Widget;

    class ImgLeftWidget extends Widget {
        public $defaultData = [
            "borderColor" => "#008037",
            "buttonBackground" => "#008037",
            "buttonfontSize" => "",

            "title"=>"Art",
            "description" => "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "imagePosition" => "left",
            "btnReadMore" => false,
            "buttonShow" => false,
            "buttonUrl" => "",
            "buttonLabel" => "",
            "image" => "",
            "css" => array(
                "btnCss" => array(
                    "backgroundColor" => "#008037",
                    "borderColor" => "#008037",
                    "fontSize" => "14px"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>