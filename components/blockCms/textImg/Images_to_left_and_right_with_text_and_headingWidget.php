<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\textImg;

    use Organization;
    use Person;
    use PHDB;
    use yii\base\Widget;

    class Images_to_left_and_right_with_text_and_headingWidget extends Widget {
        public $defaultData = [
            "bgcolorTitle"=>"#9fbd38",
            "widthImg"=>"50",
            
            "bgcolor"=>"#ffffff",
            "sizeTitle"=>"30",  
            "colorTitle"=>"#fff",
            "sizeContent"=>"20",
            "title1" => "Lorem Ipsum",
            "title2" => "Lorem Ipsum",
            "content1"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
            "content2"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
            "photo1"=>"",
            "logo1"=>"",
            "photo2"=>"",
            "logo2"=>"",
            "css" => array(
                "imgCss" => array(
                    "backgroundColor" => "#9fbd38",
                    "width" => "50%"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>