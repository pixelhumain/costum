<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\textImg;

    use Element;
    use yii\base\Widget;

    class GalleryWithPreviewsWidget extends Widget {

        public $defaultData = [
            "title" => "Lorem Ipsum",
            "bgMenu"=> "#9fbd38",
            "colorMenu" => "#04155c",
            "content" => array(
                            array(
                                "keyImg" => "https://www.w3schools.com/howto/img_fjords.jpg",
                                "keyDescr" => "Lorem Ipsum",
                            ),
                            array(
                                "keyImg" => "https://www.w3schools.com/howto/img_fjords.jpg",
                                "keyDescr" => "Lorem Ipsum andrana",
                            ),
                        ),
        ];


        public $config = [];
        public $path = "";

        public function init() {
            parent::init();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>