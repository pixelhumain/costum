<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\textImg;

    use Organization;
    use Person;
    use PHDB;
    use Yii;
    use yii\base\Widget;

    class LinkDocsWidget extends Widget {
        public $defaultData = [
            "btnColor" => "#FFFFFF",
            "btnBgColor" => "#000091",
            "btnBorderColor"   => "#000091",
            
            "title" => "TUTORIELS",
            "css" => array(
                "btnCss" => array(
                    "color" => "#FFFFFF",
                    "backgroundColor" => "#000091",
                    "borderColor" => "#000091"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>