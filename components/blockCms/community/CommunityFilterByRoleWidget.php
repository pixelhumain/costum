<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\community;

    use yii\base\Widget;
    use Element;
    use PHDB;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Yii;

    class CommunityFilterByRoleWidget extends Widget {
        public $defaultData = [
                "title"        => [ "fr" => "<div style=\"text-align:center;\">QUI SOMMES NOUS ?</div>" ],
                "title2"        => [ "fr" => "<div style=\"text-align:center;\">Nous rejoindre</div>" ],
                "roleToShow" => []
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }

            $invitelink = PHDB::findOne(InvitationLink::COLLECTION, ["targetId" => $this->config["costum"]["contextType"], "targetId" => $this->config["costum"]["contextId"], "isAdmin" => "false", "roles" => ""]);
            if(!$invitelink){
                $invitelink = [
                    "targetId" => $this->config["costum"]["contextId"],
                    "targetType" => $this->config["costum"]["contextType"],
                    "ref" => uniqid(),
                    "createOn" => time(),
                    "isAdmin" => "false",
                    "roles" => "",
                    "urlRedirection" => "",
                    "invitorId" => Yii::app()->session["userId"],
                ];
				Yii::app()->mongodb
					->selectCollection(InvitationLink::COLLECTION)
					->insert($invitelink);
            }
            $this->config["invitelink"] = $invitelink;
            $infoData = Element::getInfoDetail([], Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]), $this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            $this->config["linksBtn"] = @$infoData["linksBtn"];
        }

        public function run() {
            // var_dump($this->config);die();
            return $this->render($this->path, $this->config);
        }
    }
?>