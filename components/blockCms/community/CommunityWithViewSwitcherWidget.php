<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\community;

use yii\base\Widget;
use Element;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Yii;
use Event,Organization,Project;


class CommunityWithViewSwitcherWidget extends Widget
{
    public $defaultData = [
        'data' => [],
        "groupped" => false,
        "roleToShow" => []
    ];
    
    public $config = [];
    public $path = "";

    public function init() {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }

        $invitelink = PHDB::findOne(InvitationLink::COLLECTION, ["targetId" => $this->config["costum"]["contextType"], "targetId" => $this->config["costum"]["contextId"], "isAdmin" => "false", "roles" => ""]);
            if(!$invitelink){
                $invitelink = [
                    "targetId" => $this->config["costum"]["contextId"],
                    "targetType" => $this->config["costum"]["contextType"],
                    "ref" => uniqid(),
                    "createOn" => time(),
                    "isAdmin" => "false",
                    "roles" => "",
                    "urlRedirection" => "",
                    "invitorId" => Yii::app()->session["userId"],
                ];
				Yii::app()->mongodb
					->selectCollection(InvitationLink::COLLECTION)
					->insert($invitelink);
        }
        $this->config["invitelink"] = $invitelink;
        $infoData = Element::getInfoDetail([], Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]), $this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
        $this->config["linksBtn"] = @$infoData["linksBtn"];

        $element = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
        if($element["type"] && in_array($element["collection"], [Project::COLLECTION, Event::COLLECTION, Organization::COLLECTION]))
            unset($element["type"]);

        $this->config["element"] = $element;
        
    }

    public function run() {
        return $this->render($this->path, $this->config);
    }
}