<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\community;

    use yii\base\Widget;

    class CommunityCarouselV2Widget extends Widget {
        public $defaultData = [
            "title"        => [ "fr" => "<div style=\"text-align:center;\">QUI SOMMES NOUS ?</div>" ]
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            // var_dump($this->config);die();
            return $this->render($this->path, $this->config);
        }
    }
?>