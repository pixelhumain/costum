<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\button;
    use yii\base\Widget;
    use CAction, CO2Stat;
    class CreateCostumWidget extends Widget {
        public $defaultData = [  
            "label"     => "Initialiser vos costum",   
            "css" => [ 
                "bouton" => array(
                    "borderColor" => "#0091c6",
                    "color" =>"#fff",
                    "backgroundColor" => "#0091c6"
                )

            ]
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>