<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\button;

    use yii\base\Widget;

    class BtnCoformWidget extends Widget {
        public $defaultData = [
            "buttonCoform"=>"bouton coform",
            "buttonInfos"=>"",
            "linkInfos"=>"",
            "formId"=>"",
            "data" => [],
            "css" => array(
                "textCoformCss" => array(
                    "color" => "#ffb153"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>