<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\slide;

use PHDB;
use yii\base\Widget;
use Zone;

class SlidemapscenarioconfigurableWidget extends Widget
{
    public $paramsData = [
        "progressColor" => "#66b00b",
        "title" => "CARTE DES TIERS LIEUX",
        "config" => array(
            array(
                "marker" => "pie",
                "legende" => array(
                    "cible" => "",
                    "label" => ""
                ),
                "zone" => array(
                    "zones" => [],
                    "country" => '',
                    "level" => ''
                )
            )
        )
    ];
    public $path = "";
    public $config = [];
    public function init()
    {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $this->config["country"] = PHDB::find(Zone::COLLECTION, array("level" => "1"), array("geoShape" => 0));
        foreach ($this->config["blockCms"]["config"] as $key => $value) {
            $params = array();
            if($value["zone"]["country"] != ''){
                $params["countryCode"] = $value["zone"]["country"];
            }
            if($value["zone"]["level"] != ''){
                $params["level"][] = $value["zone"]["level"];
            }
            if(count($params) > 0){
                $this->config['blockCms']["zoneData"][] = PHDB::find(Zone::COLLECTION, $params, array("geoShape" => 0));
            }else{
                $this->config['blockCms']["zoneData"][] = [];
            }
            $this->config['blockCms']["config"][$key] = $value;
        }
    }

    public function run() {
        return $this->render($this->path, $this->config);
    }
}
