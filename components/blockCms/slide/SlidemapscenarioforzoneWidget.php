<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\slide;

use PHDB;
use yii\base\Widget;
use Zone;

class SlidemapscenarioforzoneWidget extends Widget
{
    public $paramsData = [
        "progressColor" => "#66b00b",
        "title" => "CARTE DES TIERS LIEUX",
        "element" => array(
            "map" => ["pie", "bar", "heatmap"],
            "graph" => [],
        ),
        "legende" => [],
        "zone" => array(
            "region" => "",
            "country" => "",
            "level" => "",
            "zones" => []
        )
    ];
    public $path = "";
    public $config = [];
    public function init()
    {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $this->config["country"] = PHDB::find(Zone::COLLECTION, array("level" => "1"), array("geoShape" => 0));
        $this->config["region"] = [];
        $this->config["zones"] = [];
        if($this->config['blockCms']["zone"]["level"] == "4"){
            $this->config["region"] = PHDB::find(Zone::COLLECTION, array("level" => "3", "countryCode" => $this->config["blockCms"]["zone"]["country"]), array("geoShape" => 0));
        }
        $params = array();
        if($this->config["blockCms"]["zone"]["country"] != ''){
            $params["countryCode"] = $this->config["blockCms"]["zone"]["country"];
        }
        if($this->config["blockCms"]["zone"]["level"] != ''){
            $params["level"][] = $this->config["blockCms"]["zone"]["level"];
        }
        if($this->config["blockCms"]["zone"]["region"] != ''){
            $params["level".((int) $this->config["blockCms"]["zone"]["level"] - 1)] = $this->config["blockCms"]["zone"]["region"];
        }
        if(count($params) > 0){
            $this->config["zones"] = PHDB::find(Zone::COLLECTION, $params, array("geoShape" => 0));
        }
    }

    public function run() {
        return $this->render($this->path, $this->config);
    }
}
