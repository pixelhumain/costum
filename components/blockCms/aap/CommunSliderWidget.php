<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

use Form;
use yii\base\Widget;

class CommunSliderWidget extends Widget
{
    public $defaultData = [
        "title10" => array("fr" => "Découvrez les communs"),
        "btnLabel0" => array("fr" => "Voir tous les communs"),
        "redirectPage" => "",
        "typeSlide" => "randomCommun",
        "btnLabel" => "Voir tous les communs",
    ];
    public $config = [];
    public $path = "";
    public function init()
    {
        parent::init();
        if (!empty($this->config["costum"]["contextId"]))
            $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
        if (!empty($this->config["blockCms"]))
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
    }
    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
