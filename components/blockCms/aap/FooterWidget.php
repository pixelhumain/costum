<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

    use Form;
    use yii\base\Widget;

    class FooterWidget extends Widget {
        public $defaultData = [
            "title" => "Commun du jour",
            "subTitle" => "Nom du commun",
            "communDesc" => "Lorem ipsum dolor sit amet,  Nulla nec tortor. Donec id elit quis purus consectetur consequat. Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. Aliquam ante. Suspendisse scelerisque dui nec velit. Duis augue augue, gravida euismod, vulputate ac, facilisis id, sem. Morbi in orci.",
            "btnName" => "En savoir plus",
            "image" => "",
            "link1" => "",
            "link2" => "",
            "link3" => "",
            "link4" => "",
            "link5" => "",
            "link6" => "",
            "link7" => "",
            "showLogo" => false,
            "linkLabelContent" => array(
                "linkLabel1" => "Accueil",
                "linkLabel2" => "Les communs",
                "linkLabel3" => "Déposer un commun",
                "linkLabel4" => "Contribuer aux communs",
                "linkLabel5" => "Campagane de cofinancement",
                "linkLabel6" => "Qui sommes-nous ?",
                "linkLabel7" => "FAQ"
            ),
            "imageContent" => array(
                "image1" => "",
                "image2" => "",
                "image3" => "",
                "image4" => "",
                "image5" => ""
            ),
            "css" => array(
                "fondImage" => array(
                    "backgroundColor" => "#D8F4F0",
                    "partBorder" => "2px solid #43c9b7"
                ),
                "fondLink" => array(
                    "backgroundColor" => "#D8F4F0"
                ),
            )
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            if (!empty($this->config["costum"]["contextId"])) {
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
            }
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>