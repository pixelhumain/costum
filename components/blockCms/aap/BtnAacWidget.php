<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;
    
    use Form;
    use yii\base\Widget;

    class BtnAacWidget extends Widget {
        public $defaultData = [
            "link" => "",
            "btnLabel0" => array("fr" => "Je dépose un commun")
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            if (!empty($this->config["costum"]["contextId"])) {
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], !empty($this->config["blockCms"]["getAllForm"]) ? 0 : 1);
            }
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>