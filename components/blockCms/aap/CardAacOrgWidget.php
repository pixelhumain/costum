<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

    use Form;
    use yii\base\Widget;

    class CardAacOrgWidget extends Widget {
        public $defaultData = [
            "title" => "Contributeurs aux communs",
            "nombre" => "0",
            "css" => array(
                "colorTheme" => array(
                    "color" => "#43C9B7"
                )
            ),
            "texta0" => array("fr" => "Nombres des appels à communs"),
            "texta1" => array("fr" => "Nombres des communs"),
            "texta2" => array("fr" => "Total financements collecter"),
            "texta3" => array("fr" => "Total financements neccessaire")
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }     
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>