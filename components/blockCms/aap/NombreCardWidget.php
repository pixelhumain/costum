<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

    use Form;
    use yii\base\Widget;

    class NombreCardWidget extends Widget {
        public $defaultData = [
            "title" => "Contributeurs aux communs",
            "nombre" => "0",
            "css" => array(
                "colorTheme" => array(
                    "color" => "#43C9B7"
                )
            ),
            "texta" => array("fr" => "Nombres des communs sur la plateforme")
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (!empty($this->config["costum"]["contextId"]))
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }     
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>