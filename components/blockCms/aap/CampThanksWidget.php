<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;
    use yii\base\Widget;

    class CampThanksWidget extends Widget {
        public $defaultData = [
            "title" => "votre titre",
            "desc" => "Nous vous exprimons notre profonde gratitude pour votre précieuse contribution à cette campagne de cofinancement des communs, qui ne saurait réussir sans l'engagement généreux de personnes telles que vous. Votre soutien est essentiel pour bâtir ensemble un avenir plus équitable et collaboratif.",
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>