<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;
    use yii\base\Widget;

    class CampCoFinancBannerWidget extends Widget {
        public $defaultData = [
            "title" => "Co-ﬁnancons les communs des tiers-lieux !",
            "campDesc" => array(
                "fr" => "Pour expérimenter et initier le coﬁnancement des communs des tiers-lieux, une campagne de coﬁnancement, abondée par France Tiers-Lieux, sera lancée en octobre 2024, ciblant spéciﬁquement ces besoins prioritaires des tiers-lieux. Début octobre, lors de Faire Tiers-lieux, un temps fort sera dédié au lancement de cette campagne de coﬁnancement des communs par les tiers-lieux."
            ),
            "showBtn" => true,
            "btnLabel" => "Participez à la campagne de co-ﬁnacement",
            "link" => ""
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>