<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

use Form;
use yii\base\Widget;

    class CampCoFinancFormWidget extends Widget {
        public $defaultData = [
            "title" => "Vous souhaitez participer à la campagne de coﬁnancement ?",
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            if (!empty($this->config["costum"]["contextId"]))
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"]);
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>