<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

use Form;
use yii\base\Widget;

class CampFinancDescWidget extends Widget
{
    public $defaultData = [
        "title0" => array("fr" => "Commun du jour"),
        "btnTxt0" => array("fr" => "Consulter"),
        "subTitle" => "Nom du commun",
        "communDesc" => array("fr" => "Lorem ipsum dolor sit amet,  Nulla nec tortor. Donec id elit quis purus consectetur consequat. Nam congue semper tellus. Sed erat dolor, dapibus sit amet, venenatis ornare, ultrices ut, nisi. Aliquam ante. Suspendisse scelerisque dui nec velit. Duis augue augue, gravida euismod, vulputate ac, facilisis id, sem. Morbi in orci."),
        "btnName" => "En savoir plus",
        "btnLink" => "",
        "image" => "",
    ];
    public $config = [];
    public $path = "";
    public function init()
    {
        parent::init();
        if (!empty($this->config["costum"]["contextId"]))
            $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
        if (!empty($this->config["blockCms"]))
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
    }
    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
