<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;

    use Form;
    use yii\base\Widget;

    class PartCommunWidget extends Widget {
        public $defaultData = [
            "title0" => array("fr" => "Contributeurs aux communs"),
            "partDesc" => array("fr" => "Vous cherchez une ressource ouverte et partagée pour répondre à vos besoins ? Vous avez envie de contribuer aux communs des tiers-lieux aﬁn de mutualiser les efforts ?"),
            "btnLabel0" => array("fr" => "Contribuez aux communs"),
            "image" => "",
            "link" => "",
            "nombre" => "0",
            "text0" => array("fr" =>"Commun déposés")
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (!empty($this->config["costum"]["contextId"]))
                $this->config["allFormsContext"] = Form::getAllFormContext($this->config["costum"]["contextId"], 1);
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>