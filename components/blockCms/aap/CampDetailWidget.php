<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\aap;
    use yii\base\Widget;

    class CampDetailWidget extends Widget {
        public $defaultData = [
            "title" => "Nom du commun",
            "campDesc" => array("fr" => "Lorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, consetetur"),
            "porteurDesc" => array("fr" => "Lorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet,conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet,conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, conseteturLorem ipsum dolor sit amet, consetetur")
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>