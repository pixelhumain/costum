<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

    use Organization;
    use PHDB;
    use yii\base\Widget;

    class ThematiquePrezWidget extends Widget {
        public $defaultData = [
            "tagsToShow" => []
        ];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $_where = array(
                "thematic" => array(
                    '$exists' => true
                ),
                "costum.typeCocity" => array(
                    '$exists' => true
                ),
                "costum.cocity" => array(
                    '$exists' => false
                )
            );

            $pageFilieres = PHDB::find(Organization::COLLECTION, $_where);
            $allThematics = [];
            foreach ($pageFilieres as $id => $details) {
                if (isset($details['thematic'])) {
                    $thematic = strtolower($details['thematic']);
                    if (isset($allThematics[$thematic])) {
                        $allThematics[$thematic]['count']++;
                    } else {
                        $allThematics[$thematic] = [
                            'thematic' => $details['thematic'],
                            'count' => 1
                        ];
                    }
                }
            }

            $this->config['pagesFilies'] = $pageFilieres;
            $this->config['allThematics'] = $allThematics;
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>