<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

    use Element;
    use yii\base\Widget;

    class GridMenuFilterWidget extends Widget {
        public $defaultData = [
            "field" => "objectiveOdd",
            "elementsType" => "",
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $elementOrga = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            $this->config['elementOrga'] = $elementOrga;

        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>