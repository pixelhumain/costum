<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

use yii\base\Widget;

class JoinsWidget extends Widget{
    public $config = [];
    public $path = "";
    public function init() {
        parent::init();
    }
    
    public function run() {
        return $this->render($this->path, $this->config);
    }
}