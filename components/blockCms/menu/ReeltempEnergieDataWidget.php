<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

use City;
use Element;
use PHDB;
use yii\base\Widget;

    class ReeltempEnergieDataWidget extends Widget {
        public $defaultData = [];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            $this->config['el'] = $el;

            $cities = PHDB::find(City::COLLECTION, array(
                "level3" => "58be4af494ef47df1d0ddbcc" 
            ), array('name','geoShape'));

            $this->config['cities'] = $cities;
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>