<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\footer;
    use yii\base\Widget;

    class MenuWidget extends Widget {
        public $defaultData = [
            "filiere" => [
                "food"=> array(
                    "name" => "Food",
                    "icon" => "fa-cutlery",
                    "tags" => []
                ),
                "health"=> array(
                    "name" => "Health",
                    "icon" => "fa-heart-o",
                    "tags" => []
                ),  
                "waste"=> array(
                    "name" => "Waste",
                    "icon" => "fa-trash-o ",
                    "tags" => []
                ),  
                "transport"=> array(
                    "name" => "Transport",
                    "icon" => "fa-bus",
                    "tags" => []
                ),  
                "education"=> array(
                    "name" => "Education",
                    "icon" => "fa-book",
                    "tags" => []
                ),  
                "citizen"=> array(
                    "name" => "Citizenship",
                    "icon" => "fa-user-circle-o",
                    "tags" => []
                ),  
            ]  
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>