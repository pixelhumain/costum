<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;
    use yii\base\Widget;

    class MenuFiliereWidget extends Widget {
        public $defaultData = [
            "filiere" => []  
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>