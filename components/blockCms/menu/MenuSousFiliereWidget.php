<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

    use Element;
    use PHDB;
    use SearchNew;
    use yii\base\Widget;
    use Zone;

    class MenuSousFiliereWidget extends Widget {
        public $defaultData = [
            "tagsToShow" => []
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $parent = Element::getElementSimpleById(
                $this->config["costum"]["contextId"],
                $this->config["costum"]["contextType"],
                null,
                ["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere","thematic","tags","address"]
            );

            $costum = $this->config["costum"];

            $params = [
                'searchType' => ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
                'notSourceKey' => true,
                'filters' => [
                    '$or' => [
                        'source.keys' => $parent['slug'],
                        'links.memberOf.' .$this->config["costum"]["contextId"] => array('$exists' => true),
                    ]
                ],
                'fields' => ['tags'],
                'indexStep' => 0
            ];

            if(isset($parent['thematic'])) {
                $thematic = $parent['thematic'];
                $params['filters']['$or']['$and'] = $this->setFiltersParams($costum, $parent, $thematic);
            }

            $result = SearchNew::globalAutoComplete($params, null, true);

            $datas = $result['results'];
            $tags = [];

            foreach ($datas as $key => $value) {
                if (isset($value['tags']) && is_array($value['tags'])) {
                    foreach ($value['tags'] as $tag) {
                        $tagLowerCase = strtolower($tag);
                        if (isset($tags[$tagLowerCase])) {
                            $tags[$tagLowerCase]++;
                        } else {
                            $tags[$tagLowerCase] = 1;
                        }
                    }
                }
            }

            uasort($tags, function ($a, $b) {
                return $b - $a;
            });

            $tagArray = array_keys($tags);
            $this->config['tagArray'] = $tagArray;
            $this->config['elCostum'] = $parent;
        } 


        function setFiltersParams($costum, $dataCostum, $thematic = "") {
            $dataAnd = [];
            $idAddress = [];
        
            if (isset($costum['typeCocity'])) {
                switch ($costum['typeCocity']) {
                    case "region":
                        $idAddress = ["address.level3" => $dataCostum['address']['level3']];
                        break;
                    case "ville":
                        $idAddress = ["address.localityId" => $dataCostum['address']['localityId']];
                        break;
                    case "departement":
                    case "district":
                        $idAddress = ["address.level4" => $dataCostum['address']['level4']];
                        break;
                    case "epci":
                        $citiesArray = PHDB::findOne(Zone::COLLECTION, [
                            "name" => $dataCostum["address"]["level5Name"]
                        ]);
                        $idAddress = ["address.localityId" => ['$in' => $citiesArray['cities']]];
                        break;
                }
            } else {
                if(isset($dataCostum['address']['localityId']) && $dataCostum['address']['localityId'] != "") {
                    $idAddress = ["address.localityId" => $dataCostum['address']['localityId']];
                } else if(isset($dataCostum['address']['level3']) && $dataCostum['address']['level3'] != "") {
                    $idAddress = ["address.level3" => $dataCostum['address']['level3']];
                }
            }
        
            $addressParams = [];
        
            if ($thematic === "Tiers lieux" || $thematic === "Pacte") {
                $sourcekey = $thematic === "Tiers lieux" ? "franceTierslieux" : "siteDuPactePourLaTransition";
                $tags = $thematic === "Tiers lieux" ? "TiersLieux" : strtolower($thematic);
                
                $source = [
                    '$or' => [
                        ["source.keys" => $sourcekey],
                        ["reference.costum" => $sourcekey],
                        ["tags" => $tags]
                    ]
                ];
                $addressParams[] = $source;
            } else {
                $addressParams[] = ["tags" => strtolower($thematic)];
            }
        
            if (!empty($idAddress)) {
                array_unshift($addressParams, $idAddress);
            }
            $dataAnd = $addressParams;
        
            return $dataAnd;
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>