<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\menu;

    use Element;
    use SearchNew;
    use yii\base\Widget;

    class CatalogueWidget extends Widget {
        public $defaultData = [];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $elementOrga = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            $this->config['elementOrga'] = $elementOrga;    

            $fields = ["_id", "address", "category", "collection", "description", "geo", "geoPosition", "name", "section", "subtype", "tags", "type", "profilImageUrl", "contactInfo", "price", "devise", "creator"];

            $paramsFilters = [
                "notSourceKey" => true,
                "searchType" => ['ressources', 'classifieds'],
                "filters" => [
                    "address.level1" => $elementOrga['address']['level1'],
                ],
                "fields" => $fields
            ];
            
            $data = SearchNew::globalAutoComplete($paramsFilters);
            if (isset($data['results'])) {
                $this->config['ressources'] = $data['results'];
            }

        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>