<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\annimated;

    use yii\base\Widget;

    class TextTyppingWidget extends Widget {
        public $defaultData = [
            "text" => [ "Fr" => "Hi i like HTML,I also like css,Lorem ipsum dolor sit amet,consectetur adipiscing elit,sed do eiusmod tempor incididunt" ],
            "speed" => 70
        ];

        public $config = [];
        public $path = "";
        
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>