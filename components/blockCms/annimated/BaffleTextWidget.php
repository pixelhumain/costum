<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\annimated;

    use yii\base\Widget;

    class BaffleTextWidget extends Widget {
        public $defaultData = [
            "text1"        => [ "fr" => "G E N I U S E "],
            "text2"     =>  [ "fr" => "Be fast then what you can do!"],
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>