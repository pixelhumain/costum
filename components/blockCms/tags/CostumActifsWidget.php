<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\tags;

    use City;
    use Element;
    use PHDB;
    use yii\base\Widget;
    use Zone;

    class CostumActifsWidget extends Widget {
        public $defaultData = [
            "activeFilter" => true,
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            
            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            $cities = [];
            if($el != "" ) {
                if($el["costum"]["typeCocity"] == "epci") {
                    $cities = PHDB::findOne(Zone::COLLECTION,array(
                        "name" => $el["address"]["level5Name"]
                    ));
                } else if($el["costum"]["typeCocity"] == "district"){
                    $cities['cities'] = [];
                    $allCities = PHDB::find(City::COLLECTION,array(
                        "level4" => $el["address"]["level4"]
                    ));
                    foreach($allCities as $citie => $data) {
                        array_push($cities['cities'], $citie);
                    }
                }
            }
            $this->config["cities"] = $cities;
            $this->config['el'] = $el;
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>