<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\observatoire;
    use yii\base\Widget;
    use CAction, CO2Stat;
    class NumberVisitWidget extends Widget {
        public $defaultData = [            
            "css" => [

            ]
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            $all = co2Stat::getStatsCustomAll(); 
            $tot = 0;
            foreach($all as $e=>$v){
                $tot = ($tot+$v);
            }
            if (isset($this->config["blockCms"])) {
                $this->config["visit"] = $tot;
            }   
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>