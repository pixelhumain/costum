<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\observatoire;

use Citoyen;
use Event;
use Organization;
use PHDB;
use Poi;
use Project;
use yii\base\Widget;
    use Zone;

    class ObservaCityWidget extends Widget {
        public $defaultData = [];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $this->config["country"] = PHDB::find(Zone::COLLECTION, array("level" => "1"), array("geoShape" => 0));
            $this->config["momberElements"] = PHDB::count(Organization::COLLECTION);
            $this->config["momberElements"] += PHDB::count(Project::COLLECTION);
            $this->config["momberElements"] += PHDB::count(Event::COLLECTION);
            $this->config["momberElements"] += PHDB::count(Poi::COLLECTION);
            $this->config["momberCitoyen"] = PHDB::count(Citoyen::COLLECTION);
            $this->config["nomberCity"] = PHDB::count(Zone::COLLECTION, array("level" => "1"));
            $where = [
                "costum.cocity" => array(
                    '$exists'=> true
                )
            ];
            $this->config["nomberCocity"] = PHDB::count(Organization::COLLECTION,$where);
            
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>