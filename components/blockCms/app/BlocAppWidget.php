<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\app;
    use yii\base\Widget;

    class BlocAppWidget extends Widget {
        public $defaultData = [
            "title" => "Rejoignez-nous",
            "css" => array(
                "titre" => array(
                    "color" => "white",
                    "fontSize" => "24px"
                ),
                "texte" => array(
                    "color" => "white",
                    "fontSize" => "16px"
                ),
                "boutton" => array(
                    "color" => "black",
                    "fontSize" => "16px",
                    "backgroundColor" => "#ffffff"
                )
            )
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>