<?php 

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\step;
    use yii\base\Widget;
    use Element,PHDB,Poi;

    class Step_wizardWidget extends Widget
    {
        public $defaultData = [
            "title" => "Explication des etape d'un costume ",
            "subtitle"=> " « Contribuer aux communs avec un contexte personnalisé. » ",
            "stepNumber" => 4,
            "imgShow" => "yes",
            "logo" => ""
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            
            $this->config["poiList"] = array();
            if(isset($this->config["costum"]["contextType"]) && isset($this->config["costum"]["contextId"])){
                $this->config["el"] = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
                $this->config["poiList"] = PHDB::find(Poi::COLLECTION, array( "source.key" => $this->config["costum"]["slug"], "type"=>"cms") );
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>