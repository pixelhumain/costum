<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\multimedia;

    use yii\base\Widget;

    class ImacWidget extends Widget {
        
        public $defaultData = [
            "link" => [ "fr" => "https://peertube.communecter.org/videos/embed/6024b1aa-ceea-4875-81a5-eb37c9330912"]
        ];

        public $config = [];
        public $path = "";
        
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>