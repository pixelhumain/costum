<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\text;
    use yii\base\Widget;

    class Text4ColumnsWidget extends Widget {
        public $defaultData = [
            "title" => "\n        \n        <span style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\" style=\"\"><u>\n        Le Processus      </u></font></span><div><u style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\"><br></font></u></div><div><u style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\"><br></font></u></div>",
            "title1"=>"<font color=\"#63aabc\" size=\"5\">Connecter</font>",
            "content1" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.</font>",
            "content2" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula.&nbsp;<span style=\"background-color: transparent;\">Ut et purus lorem.</span></font>",
            "content3" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.  </font>",
            "content4" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.  </font>",
            "title2" => "<font color=\"#63aabc\" size=\"5\">Observer</font>",
            "title3" => "<font color=\"#63aabc\" size=\"5\">Cultiver</font>", 
            "title4" => "<font color=\"#63aabc\" size=\"5\">Service</font>"
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>