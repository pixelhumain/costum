<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\text;

    use Organization;
    use Person;
    use PHDB;
    use Yii;
    use yii\base\Widget;

    class TextCollapseonHoverWidget extends Widget {
        public $defaultData = [
            "titleWithColor1" => "",
            "titleWithColor2" => "",
            "content" => "Ariane est une méthodologie d’accompagnement personnel et professionnel solide et éprouvée depuis 30 ans.
            Créée et développée par Olivier Ronceray et Maryse Tournon-Ronceray, elle est adaptée :
            - À tout contexte (professionnel ou personnel)
            - À toute situation (des questions quotidiennes, en passant par les problématiques organisationnelles jusqu’à la stratégie d’entreprises)
            - À tout format (individuel ou collectif)
            Grâce à ses outils dynamiques d’aide à l’introspection, à la décision et à la résolution de problème, Ariane permet l’élaboration de solutions et le passage à l’action.
            Ariane possède également une dimension philosophique qui s’appuie sur les théories de la connaissance, des systèmes et de la complexité, tout en restant très pratique, favorisant la mise en œuvre de la créativité collective au service d’objectifs concrets, sans oublier de prendre soin de la richesse humaine.",
            "iconColor"=> "black",
            "iconSize"=> "150",
            "css" => array(
                "iconCss" => array(
                    "color" => "black",
                    "fontSize" => "150px"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>