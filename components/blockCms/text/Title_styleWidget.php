<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\text;

use yii\base\Widget;

class Title_styleWidget extends Widget
{
    public $defaultData = [
        // "colordecor" =>"#5f9f0d",
        // "bgcolor" => "#e8e8e8",
        // "iconLeftColor"=>"#ffffff",
        // "textLeftColor" => "#ffffff",
        // "colorLabelButton" =>"#ffffff",
        "typeInLeft" => "text",
        "textLeft" => "?",
        "iconLeft" => "share",
        "css" => array(
            "bgcolor" => array(
                "backgroundColor" => "#e8e8e8"
            ),
            "colordecor" => array(
                "backgroundColor" => "#5f9f0d"
            ),
            "iconLeftColor" => array(
                "color" => "#ffffff"
            ),
            "textLeftColor" => array(
                "color" => "#ffffff"
            )
        )

    ];
    public $config = [];
    public $path = "";

    public function init()
    {
        parent::init();

        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
