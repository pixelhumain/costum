<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\events;

    use yii\base\Widget;

    class Modal_with_agenda_calendarWidget extends Widget {
        public $defaultData = [
            // "colorlabelButton"  => "#000000",
            // "colorBorderButton" => "#000000",
            // "colorButton"       => "#ffffff",
            // "colorButtonHover" => "#000",
            // "colorlabelButtonHover" => "#fff",
            // "eventBackgroundColor" => "#2d328d",
            // "eventToday" => "#e16d38",

            "labelButton" => "Boutton",
            "showType" => "calendar",
            "image" => "",
            "titreAgenda" => "Votre titre",
            "iconAgenda" => "",
            "css" => array(
                "buttonCss" => array(
                    "color" => "#000000",
                    "backgroundColor" => "transparent",
                    "borderColor" => "#000000"
                ),
                "eventCss" => array(
                    "backgroundColor" => "#2d328d"
                ),
                "todayCss" => array(
                    "backgroundColor" => "#e16d38"
                ),
                "hover" => array(
                    "buttonCss" => array(
                        "color" => "#ffffff",
                        "backgroundColor" => "#000000"
                    )  
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>