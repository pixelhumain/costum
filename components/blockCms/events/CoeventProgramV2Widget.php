<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\events;

    use yii\base\Widget;
    use Element;
use Event;
use PHDB;
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;


    class CoeventProgramV2Widget extends Widget {
        public $defaultData = [];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();
			$context = $this->config['costum']['contextId'];
            if (isset($this->config["blockCms"])) {
               
				if ($this->config["costum"]["contextType"] !== Event::COLLECTION) {
					$context = $this->config["costum"]["app"];
					$context = $context["#" . $this->config["blockCms"]["page"]] ?? [];
					if (!empty($context["event"]))
						$context = [
							"id"	=> $context["event"],
							"type"	=> Event::COLLECTION
						];
					else
						$context = [
							"id"	=> $this->config["costum"]["contextId"],
							"type"	=> $this->config["costum"]["contextType"]
						];
				} else
					$context = [
						'id'	=> $context,
						'type'	=> Event::COLLECTION
					];
				$this->config["parent"] = $context;
				$this->config["blockCms"] = array_replace_recursive($this->defaultData,$this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>