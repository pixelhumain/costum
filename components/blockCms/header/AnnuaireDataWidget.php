<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\header;

use Element;
use PHDB;
use yii\base\Widget;
use Zone;

    class AnnuaireDataWidget extends Widget {
        public $defaultData = [
            "css" => array(
                "fondContainer" => array(
                    "backgroundColor" => "#00b5c0"
                ),
                "text" => array(
                    "fontSize" => "12",
                    "color" => "#84d70f"
                ),
                "icone" => array(
                    "fontSize" => "15",
                    "color" => "#84d70f"
                )
            )
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $el = "";
            $cities = "";
            
            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"] );
            if($el != "" &&  $el["costum"]["typeCocity"] == "epci") {
                $cities = PHDB::findOne(Zone::COLLECTION,array(
                    "name" => $el["address"]["level5Name"]
                ));
            }
            $this->config["cities"] = $cities;
            $this->config['el'] = $el;
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>