<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\header;

use Element;
use yii\base\Widget;

    class InvitandrejoindrecostumeWidget extends Widget {
        public $defaultData = [
            "css" => array(
                "botton" => array(
                    "color" => 'white',
                    "backgroundColor" => '#00b7c5',
                    "width" => "170px",
                    "fontSize" => "20px",
                    "borderColor" => "#00b7c5",
                    "borderBottomLeftRadius" => "25px",
                    "borderBottomRightRadius" => "25px",
                    "borderTopLeftRadius" => "25px",
                    "borderTopRightRadius" => "25px",
                    "paddingTop" => "10px",
                    "paddingLeft" => "10px",
                    "paddingRight" => "10px",
                    "paddingBottom" => "10px",
                    "marginTop" =>"5px",
                    "marginBottom" =>"5px",
                    "marginLeft" =>"5px",
                    "marginRight" =>"5px",
                ),
                "hover" => array(
                    "botton" => array(
                        "backgroundColor" => "#66f105",
                        "color" => "white",
                        "borderColor" => "#66f105",
                    )
                ),
                "position" => 'left'
            )
        ];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);

            $this->config['elements'] = $el;

            $infoData = Element::getInfoDetail([], Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]), $this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            
            $this->config['infoData'] = $infoData;
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>