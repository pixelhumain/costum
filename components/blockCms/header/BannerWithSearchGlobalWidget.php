<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\header;
    use yii\base\Widget;

    //use Element

    class BannerWithSearchGlobalWidget extends Widget {
        public $defaultData = [
            "descriptionVille" => " <font color=\"#ffffff\" style=\"font-size: 25px;\"> Votre territoire est un espace d'interaction Locale (citoyens, la biodiversité, acteurs socio-économiques)\nLa collectivité constitue le liant de tous ces éléments et se positionne en tant que facilitateur des interactions.\nC'est un espace où on peut (se) poser des questions librement et construire une vision partagée afin d'apporter des solutions en commun.\nSoutenir les élu.e.s dans leur rôle de facilitateur.rice des dynamiques locales qui font converger les acteurs vers des objectifs communs\nAccompagner les grands enjeux de résilience des collectivités en facilitant l'adhésion et l'action de chacun à travers la visibilité de l'impact local de ses activités.\nun système vivant où toute vie y est pleinenemnt respecter </font> "
        ];
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>