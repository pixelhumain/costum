<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\header;

use Element;
use yii\base\Widget;

    class NameParentWidget extends Widget {
        public $defaultData = [
            "titre" => "",
            "showVille" => true,
            "css" => array(
                "styleText" => array(
                    "backgroundColor" => "transparent",
                    "inputNumberRange" => 1,
                    "boxShadow" => "0px 0px 0px 0px transparent",
                    "marginTop"=>"",
                    "marginLeft"=> "9%",
                    "marginRight"=> "",
                    "marginBottom"=> "",
                    "paddingTop"=>"",
                    "paddingLeft"=> "",
                    "paddingRight"=> "",
                    "paddingBottom"=> "",
                    "border" => "2px solid black",
                    "borderBottomLeftRadius" => "5px",
                    "borderBottomRightRadius" => "5px",
                    "borderTopLeftRadius" => "5px",
                    "borderTopRightRadius" => "5px",
                ),
                "position" => "left",
                "opacity" => "0.7"
            )
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();   

            $parent = [];

            $parent = Element::getElementSimpleById($this->config["costum"]["contextId"],$this->config["costum"]["contextType"],null,["name", "description","descriptionVille","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","address","source","cocity"]);

            $laville = "";
            if(isset($this->config["costum"]["typeCocity"]) || isset($parent["address"])) {
                if(isset($parent["address"]["addressLocality"])){
                    $laville =  "Commune d'(e) ".$parent["address"]["addressLocality"].", ".$parent["address"]["level4Name"];
                }else if(isset($parent["address"]["level5Name"]) && $parent["address"]["level4Name"]){
                    $laville =  "EPCI d'(e) ".$parent["address"]["level5Name"].", ".$parent["address"]["level4Name"].", ".$parent["address"]["level3Name"];
                } else if(isset($parent["address"]["level4Name"])){
                    $teritor = "Département d'(e) ";
                    if(isset($this->config["costum"]["typeCocity"]))
                        $teritor = $this->config["costum"]["typeCocity"] == "district" ?? "District d'(e) ";

                    $laville =  $teritor.$parent["address"]["level4Name"].", ".$parent["address"]["level3Name"].", ".$parent["address"]["level1Name"];
                } else if(isset($parent["address"]["level3Name"])) {	
                    $laville =  "Region d'(e) ".$parent["address"]["level3Name"].", ".$parent["address"]["level1Name"];
                }
            }

            if (!isset($this->config["laville"])){
                $this->config["laville"] = [ "fr" => $laville ];
            }

            $this->defaultData["titre"] = [ "fr" => $parent["name"] ];
            if ($laville == "") {
                $laville = "Pas d'adresse";
            }
            $this->defaultData["laville"] = [ "fr" => $laville ];
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $this->config["parent"] = $parent;
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>