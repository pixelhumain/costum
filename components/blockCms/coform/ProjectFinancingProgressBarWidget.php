<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\coform;

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use CacheHelper;
use DashboardData;
use Element;
use Form;
use Link;
use MongoId;
use Organization;
use PHDB;
use PhpParser\Node\Expr\Cast\Object_;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Yii;
use yii\base\Widget;
class ProjectFinancingProgressBarWidget extends Widget
{
    public $paramsData = [
        "originalColor" => '#9fbd38',
        "showbtn" => true,
        "showtitle" => true,
        "showdesc" => true,
        "ftl" => false,
        "css" => array(
            'labelColor' => array(
                'color' => "black"
            ),
            'percentColor' => array(
                'color' => "white"
            ),
            'emptyColor' => array(
                'backgroundColor' => "#FF286B"
            ),
            'completeColor' => array(
                'backgroundColor' => "#9B6FAC"
            ),
        )
    ];
    public $path = "";
    public $config = [];
    public function init(){
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }

        $id = $this->config["costum"]["contextId"];
        $type = $this->config["costum"]["contextType"];
        $slug = $this->config["costum"]["contextSlug"];

        $this->config["blockCms"]["dataAnswers"] = null;


        $answerQuery = array("project.id" => $id);
        $answer = PHDB::findOne(Form::ANSWER_COLLECTION, $answerQuery);
        if(empty($answer) & in_array("communId",explode(".",$this->config["clienturi"]))){
            $clienturiarray = explode(".",$this->config["clienturi"]);
            $answerid = $clienturiarray[array_search("communId", $clienturiarray)+1];
            //$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerid);
            $answer = Aap::filterdepense($answerid,null);
        }
		$financementData["arraylines"] = [];
        if(!empty($answer["answers"]["aapStep1"]["depense"]))
            $financementData["arraylines"] = $answer["answers"]["aapStep1"]["depense"];
        foreach ($financementData["arraylines"] as $in => $item){
            if(isset($item["financer"])) {
                $financementData["financement"][$in] =  $item["financer"];
            } else{
                $financementData["financement"][$in] =  [];
            }
        }
        $parentForm = PHDB::findOneById(Form::COLLECTION, $answer["form"]);
        $parentFormConfig = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
        $groupCommunity = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
        $parentCommunity = Link::groupFindByType(Organization::COLLECTION, $groupCommunity);
        //if($this->config["blockCms"]["ftl"]) {
            $tiersLieux = PHDB::find(Organization::COLLECTION,
                array(
                    '$and'	=> [
                        [
                            '$or'	=> [
                                ['source.keys'		=> 'franceTierslieux'],
                                ['reference.costum'	=> 'franceTierslieux']
                            ]
                        ]
                    ]
                )
            );
            $parentCommunity = array_merge($parentCommunity, $tiersLieux);
        //}
        $financementData["lines"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_merge_recursive(...$financementData["financement"]) : 0;
        $financementData["amountearn"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($financementData["lines"], "amount")) : 0;
        $financementData["total"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($answer["answers"]["aapStep1"]["depense"], 'price')) : 0;
        $context = $this->config["blockCms"]["ftl"] ? "FTL" : "(auto-financement)";
        $financerData = Form::getFinancerAmount($financementData["lines"], $context , $parentCommunity , true );
        $financerData = array_map(function ($name, $value){
            if(!empty($value["id"])){
                return [
                    "id" => $value["id"],
                    "name" => $name,
                    "amount" => $value["amount"],
                    "profilImg" => $value["profilImageUrl"]
                ];
            }
            return [
                "name" => $name,
                "amount" => $value
            ];
        }, array_keys($financerData) , $financerData);

        usort($financerData, function ($object1 , $object2){
           return $object1["amount"] < $object2["amount"];
        });

        $checkDate = "include";
        if(isset($parentFormConfig["campagne"]) && isset($parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["cofinancedate"])){
            $now = date('d-m-Y H:i', time());
            $checkDate = Api::checkDateStatus($now,$parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["cofinancedate"],null);
        }
        $campColor = "#4623c9";
        $campPorteur = [];
        $porteurImg = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
        if(isset($parentFormConfig["campagne"]) && isset($parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campColor"])){
            $campColor = $parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campColor"];
        }
        if(isset($parentFormConfig["campagne"]) && isset($parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campPorteur"])){
            $campPorteur = $parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campPorteur"];
            $porteurImg = PHDB::findOneById(Organization::COLLECTION, $campPorteur["id"], ["profilImageUrl"])["profilImageUrl"];
        }

        $this->config["blockCms"]["answer"] = $answer;
        $this->config["blockCms"]["financerData"] = $this->addPalette($financerData , $this->config["blockCms"]["originalColor"]);
        $this->config["blockCms"]["financementData"] = $financementData;
        $this->config["blockCms"]["parentForm"] = $parentForm;
        $this->config["blockCms"]["checkDate"] = $checkDate;
        $this->config["blockCms"]["campColor"] = $campColor;
        $this->config["blockCms"]["campPorteur"] = $campPorteur;
        $this->config["blockCms"]["porteurImg"] = $porteurImg;
    }
    public function run () {
        return $this->render($this->path, $this->config);
    }

    public function getSimilarRandomColor($color){
        $p = 1;
        $temp = null;
        $random = mt_rand() / mt_getrandmax();
        $result = '#';

        while($p < strlen($color)){
            $temp = intval(substr($color, $p, 2), 16);
            $temp += floor((255 - $temp) * $random);
            $result .= str_pad(dechex($temp), 2, '0', STR_PAD_LEFT);
            $p += 2;
        }
        return $result;
    }

    public function addPalette($financerData , $color)
    {
        $mixedSimilarColor = [];
        for ($i = 0; $i < count((array)$financerData); $i++){
            $similarcolor  = $this->getSimilarRandomColor($color);
            array_push($mixedSimilarColor,  [
                'color' => $similarcolor,
                'hsl' => $this->hexToHsl($similarcolor)
            ]);
        }
        usort($mixedSimilarColor, function ($a, $b) {
            if(!$this->huesAreinSameInterval($a['hsl']['h'], $b['hsl']['h'])){
                if ($a['hsl']['h'] < $b['hsl']['h'])
                    return -1;
                if ($a['hsl']['h'] > $b['hsl']['h'])
                    return 1;
            }
            if ($a['hsl']['l'] < $b['hsl']['l'])
                return -1;
            if ($a['hsl']['l'] > $b['hsl']['l'])
                return 1;
            if ($a['hsl']['s'] < $b['hsl']['s'])
                return -1;
            if ($a['hsl']['s'] > $b['hsl']['s'])
                return 1;
            return 0;
        });
        $return = array_map(function ($fin, $color){
            $fin['color'] = $color['color'];
            return $fin;
        }, $financerData , $mixedSimilarColor);
        return $return;
    }

    public function hexToHsl($hex){
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return $this->rgbToHsl($r, $g, $b);
    }

    public function huesAreinSameInterval($hue1, $hue2, $interval = 30){
        return (round(($hue1 / $interval), 0, PHP_ROUND_HALF_DOWN) === round(($hue2 / $interval), 0, PHP_ROUND_HALF_DOWN));
    }

    public function rgbToHsl($r, $g, $b)
    {
        $r /= 255;
        $g /= 255;
        $b /= 255;

        $max = max($r, $g, $b);
        $min = min($r, $g, $b);

        $h = 0;
        $l = ($max + $min) / 2;
        $d = $max- $min;

        if ($d == 0){
            $h = $s = 0;
        } else {
            $s = $d / (1 - abs(2 * $l - 1));

            switch ($max) {
                case $r:
                    $h = 60 * fmod((($g - $b) / $d), 6);
                    if ($b > $g) {
                        $h += 360;
                    }
                    break;

                case $g:
                    $h = 60 * (($b - $r) / $d + 2);
                    break;

                case $b:
                    $h = 60 * (($r - $g) / $d + 4);
                    break;
            }
        }
        return array('h' => round($h, 2), 's' => round($s, 2), 'l' => round($l, 2));
    }
}
