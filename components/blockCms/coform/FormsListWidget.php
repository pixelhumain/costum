<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\coform;

    use yii\base\Widget;

    class FormsListWidget extends Widget {
        public $defaultData = [
            // "colorTheme"  => "#8fcf1b",
            // "titleColor" => "#8fcf1b",
            // "sourceKey" => "",
            // "btnEditText" => "Modifier",

            "title" => "Liste des formulaires",
            "btnNewFormText" => "Nouvelle formaulaire",
            "btnTemplateText" => "Choisir un template",
            "btnAnswerText" => "Répondre",
            "btnListAnswersText" => "Réponses",
            "btnConfigText" => "Configurer",
            "coformSlug" => '',
            "coformDisplay" => [],
            "activeOnly" => false,
            "css" => array(
                "formListCss" => array(
                    "color" => "#8fcf1b"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>