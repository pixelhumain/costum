<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\coform;

use CacheHelper;
use Citoyen;
use DashboardData;
use Element;
use Form;
use Link;
use MongoId;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\extended\actor\Person;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use Yii;
use yii\base\Widget;
class OrgaActionSlideWidget extends Widget
{
    public $paramsData = [
        "title" => "Financement des projets et propositions",
        "data" => [],
        "form" => "",
        "primaryColor" => "#9fbd38",
        "secondaryColor" => "#7193c2",
        "fondNiv1" => "#eceff1",
        "fondNiv2" => "#edefdf",
        "progressNiv1" => "#bceab5",
        "progressNiv2" => "#bceab5"

    ];
    public $path = "";
    public $config = [];
    public function init(){
        parent::init();
        $id = $this->config["costum"]["contextId"];
        $type = $this->config["costum"]["contextType"];
        $slug = $this->config["costum"]["contextSlug"];

        if(isset(Yii::app()->session["userId"])) {
            $result = PHDB::find( Form::COLLECTION , array("type" => "aap") , array("name"));
            $this->paramsData["data"] = array_map(function ($n){
                                return $n["name"];
                            } ,
                            array_filter($result,function ($n){
                                return Form::canAdmin((string)$n["_id"]);
                            })
                        );
        }
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }

        $form = $this->config["blockCms"]["form"];
        $answers = [];
        $totalfinancement = [];
        if($form != "") {
            $answerQuery = array("form" => $form);
            $answers = PHDB::find(Form::ANSWER_COLLECTION, $answerQuery);
            $totalfinancement = Form::getFormTotalFinancement($form);
        }
       /* $groupCommunity = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
        $parentCommunity = Link::groupFindByType(Organization::COLLECTION, $groupCommunity);*/

        $this->config["blockCms"]["answers"] = $answers;
        $this->config["blockCms"]["totalfinancement"] = $totalfinancement;
    }
    public function run () {
        return $this->render($this->path, $this->config);
    }
}
