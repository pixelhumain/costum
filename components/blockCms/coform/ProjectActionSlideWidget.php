<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\coform;

use Authorisation;
use Element;
use Form;
use Link;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Yii;
use yii\base\Widget;

class ProjectActionSlideWidget extends Widget {
	public $paramsData = [
		"dataAnswer" => [],
		"showbtn" => true,
		"originalColor" => '#9fbd38',
		"css" => array(
			'labelColor' => array(
				'color' => "black"
			),
			'percentColor' => array(
				'color' => "white"
			),
			'emptyColor' => array(
				'backgroundColor' => "#FF286B"
			),
			'completeColor' => array(
				'backgroundColor' => "#9B6FAC"
			),
		)
	];
	public $path = "";
	public $config = [];
	public function init() {
		parent::init();
		$id = $this->config["costum"]["contextId"];
		$type = $this->config["costum"]["contextType"];
		$slug = $this->config["costum"]["contextSlug"];

		$this->config["blockCms"]["dataAnswers"] = null;

		$this->config["blockCms"]["dataAnswers"] = null;

		$answerQuery = array("project.id" => $id);
		$answer = PHDB::findOne(Form::ANSWER_COLLECTION, $answerQuery);
		if (empty($answer) && in_array("communId", explode(".", $this->config["clienturi"]))) {
			$clienturiarray = explode(".", $this->config["clienturi"]);
			$answerid = $clienturiarray[array_search("communId", $clienturiarray) + 1];
			//$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerid);
            $answer = Aap::filterdepense($answerid,null);
            $archivedAnswer = Aap::filterdepense($answerid,null,true);
		}
		$parentForm = PHDB::findOneById(Form::COLLECTION, $answer["form"]);
        $parent = PHDB::findOneById($parentForm["parent"][array_keys($parentForm["parent"])[0]]["type"] , array_keys($parentForm["parent"])[0]);
        $parentFormConfig = PHDB::findOneById(Form::COLLECTION, $parentForm["config"]);
		$groupCommunity = Element::getCommunityByParentTypeAndId($parentForm["parent"]);
		$parentCommunity = Link::groupFindByType(Organization::COLLECTION, $groupCommunity);
		$tiersLieux = Organization::get_tls();
		$parentCommunity = array_merge($parentCommunity, $tiersLieux);
		$financementData["arraylines"] = [];
		if (!empty($answer["answers"]["aapStep1"]["depense"]))
			$financementData["arraylines"] = $answer["answers"]["aapStep1"]["depense"];
		foreach ($financementData["arraylines"] as $in => $item) {
			if (isset($item["financer"]))
				$financementData["arraylines"][$in]["totalfinancement"] =  array_sum(array_column($item["financer"], "amount"));
			else
				$financementData["arraylines"][$in]["totalfinancement"] =  [];
		}

        /* financerlist */
        $financerlist = [];
        $financerlist["arraylines"] = [];
        if(!empty($answer["answers"]["aapStep1"]["depense"]))
            $financerlist["arraylines"] = $answer["answers"]["aapStep1"]["depense"];
        foreach ($financerlist["arraylines"] as $in => $item){
            if(isset($item["financer"])) {
                $financerlist["financement"][$in] =  $item["financer"];
            } else{
                $financerlist["financement"][$in] =  [];
            }
        }
        $financerlist["lines"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_merge_recursive(...$financerlist["financement"]) : 0;
        $financerlist["amountearn"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($financerlist["lines"], "amount")) : 0;
        $financerlist["total"] = isset($answer["answers"]["aapStep1"]["depense"]) ? array_sum(array_column($answer["answers"]["aapStep1"]["depense"], 'price')) : 0;
        $context = "FTL";
        $financerData = Form::getFinancerAmount($financerlist["lines"], $context , $parentCommunity , true );
        $financerData = array_map(function ($name, $value){
            if(!empty($value["id"])){
                return [
                    "id" => $value["id"],
                    "name" => $name,
                    "amount" => $value["amount"],
                    "profilImg" => $value["profilImageUrl"],
                    "type" => $value["type"]
                ];
            }
            return [
                "name" => $name,
                "amount" => $value
            ];
        }, array_keys($financerData) , $financerData);

        usort($financerData, function ($object1 , $object2){
            return $object1["amount"] < $object2["amount"];
        });


        $campColor = "#4623c9";
		$campPorteur = [];
        $porteurImg = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
        if(isset($parentFormConfig["campagne"]) && isset($parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campColor"])){
            $campColor = $parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campColor"];
        }
		if(isset($parentFormConfig["campagne"]) && isset($parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campPorteur"])){
            $campPorteur = $parentFormConfig["campagne"][array_key_first($parentFormConfig["campagne"])]["campPorteur"];
            $porteurImg = PHDB::findOneById(Organization::COLLECTION, $campPorteur["id"], ["profilImageUrl"])["profilImageUrl"];
        }

        $canEdit = false;
        if(
            isset(Yii::app()->session['userId'])
            && (
                Form::canAdmin(Yii::app()->session['userId'], (string)$parentForm["_id"])
                || Yii::app()->session['userId'] == $answer["user"]
                || Authorisation::isUserSuperAdmin(Yii::app()->session['userId'])
                || !empty($answer["links"]["canEdit"][Yii::app()->session['userId']])
				|| (!empty($parentForm["contributorsCanEdit"]) && !empty($answer["links"]["contributors"][Yii::app()->session['userId']]))
            )
        ){
            $canEdit = true;
        }

		$this->config["blockCms"]["answer"] = $answer;
		$this->config["blockCms"]["lines"] = $financementData["arraylines"];
		$this->config["blockCms"]["parentCommunity"] = $parentCommunity;
        $this->config["blockCms"]["campColor"] = $campColor;
        $this->config["blockCms"]["canEdit"] = $canEdit;
        $this->config["blockCms"]["archivedAnswer"] = $archivedAnswer;
		$this->config["blockCms"]["campPorteur"] = $campPorteur;
        $this->config["blockCms"]["porteurImg"] = $porteurImg;
        $this->config["blockCms"]["financerData"] = $financerData;
        $this->config["blockCms"]["parentForm"] = $parentForm;
        $this->config["blockCms"]["parent"] = $parent;

	}
	public function run() {
		return $this->render($this->path, $this->config);
	}
}
