<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\faq;

    use yii\base\Widget;

    class QuestionLocalWidget extends Widget {
        public $defaultData = [
            "css" => array(
                "question" => array(
                    "fontSize" => "18",
                    "color" => "#333333",
                    "backgroundColor" =>"#8ABF32"
                ),
                "text" => array(
                    "fontSize" => "14",
                    "color" => "#333333"
                ),
                "bouton" => array(
                    "borderColor" => "#8A4F32",
                    "color" =>"#fff",
                    "backgroundColor" => "#8A4F32",
                    "borderBottomLeftRadius" => "2px",
                    "borderBottomRightRadius" => "2px",
                    "borderTopLeftRadius" => "2px",
                    "borderTopRightRadius" => "2px",
                )
            ),
            // "btnBorderRadius" => 2,
            "btnPosition" => "right",
            "background_color2" => "#f5f5f5",
            "questionAccordion" => false,
            "faqDisplay" => []
        ];

        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>
