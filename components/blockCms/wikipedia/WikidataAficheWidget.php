<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\wikipedia;
    use yii\base\Widget;

    class WikidataAficheWidget extends Widget {
        public $defaultData = [];
        
        public $config = [];
        public $path = "";

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>