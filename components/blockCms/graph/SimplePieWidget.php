<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;
    use DashboardData;
    use Form;
    use PHDB;
    use yii\base\Widget;

    class SimplePieWidget extends Widget{
        public $defaultData = [
            "coform" => "",
            "answerPath" => "",
            "answerValue" => "",
            "css" => array(
                "percentColor" => array(
                    "color" => "#413F99"
                ) 
            ),
            //"percentColor" => "#413F99",
            "nextTo" => "",
            // "emptyColor" => "#DDD",
            // "completeColor" => "#F0506A",
        ];

        public $config = [];
        public $path = "";

        public function init()
        {
            parent::init();
            
            $id = $this->config["costum"]["contextId"];
            $type = $this->config["costum"]["contextType"];
            $slug = $this->config["costum"]["contextSlug"];

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $this->config["blockCms"]["dataAnswers"] = null;
            if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
            
                $aCountQuery = array('form' => $this->config['blockCms']["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                $answersCount = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                if($answersCount==0){
                    $answersCount = 1;
                }
                $this->config["blockCms"]["dataAnswers"] = DashboardData::simplePie($id, $answersCount, $this->config['blockCms'], null);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>