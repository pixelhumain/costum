<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;
    use DashboardData;
    use Form;
use MongoId;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\controllers\element\GetAction;
use Yii;
use yii\base\Widget;

    class ProgressCircleWidget extends Widget
    {
        public $defaultData = [
            "css" => array(
                'percentColor' => array(
                    "fill" => "#F0506A"
                ),
                'emptyColor' => array(
                    "fill" => "#dddddd"
                ),
                'completeColor' => array(
                    "fill" => "green"
                )
            ),
            "titleTop" => "",
            "titleBottom" => "",
            "textValueRight" => "",
            "coform" => "",
            "answerPath" => "",
            "answerValue" => "",
            "textRight" => false
        ];
        public $config = [];
        public $path = "";
        public function init() {
            parent::init();
            $id = $this->config["costum"]["contextId"];
            $type = $this->config["costum"]["contextType"];
            $slug = $this->config["costum"]["contextSlug"];
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            if(($this->config['costum']["assetsSlug"] == "franceTierslieux") || ($this->config['costum']["assetsSlug"] == "reseauTierslieux")){
                $extraFilter = [
                    "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s" => [
                        '$exists' => true
                    ]
                ];
            }
            if($id){
                $where = array(
                    "_id" => new MongoId($id)
                );
            }else if($slug){
                $where = array(
                    "slug" => $slug
                );
            }
            $dataCostum = Yii::app()->cache->get("costum".$slug);
            if(!$dataCostum){
                $dataCostum = PHDB::findOne($type, $where);
                Yii::app()->cache->set('costum'.$slug, $dataCostum, 3600);
            }
            $scopeQuery = array("coform" => array("shareToChildren" => true));
            if(isset($extraFilter) && is_array($extraFilter)){
                $scopeQuery["answers"] = $extraFilter;
            }
            if(isset($dataCostum["level"])){
                $level = $dataCostum["address"][$dataCostum["level"]];
                $levelNb = substr($dataCostum["level"], 5);
                if (!$level && $levelNb=="5"){
					$levelNb=$levelNb-1;
					$level=$dataCostum["address"]["level$levelNb"];
				}
            }
            if(isset($level) && $level!=null && $level!=""){
                $orga = Yii::app()->cache->get('zonesId'.$slug);
                if(!$orga){
                    $inScope = PHDB::find(Organization::COLLECTION , array(
                        "address.level$levelNb" => $level,
                        '$or' => array(
                            array("reference.costum"=>"franceTierslieux"),
                            array("source.keys"=>"franceTierslieux")
                        ) ), ["name", "collection"] );

                    $orga = array_map(function($item, $key){
                        unset($item["_id"]);
                        $item["type"] = $item["collection"];
                        unset($item["collection"]);
                        return array($key => $item);
                    }, $inScope, array_keys($inScope));
                    Yii::app()->cache->set('zonesId'.$slug, $orga, 3600);
                }
                if(isset($scopeQuery["answers"])){
                    $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
                }
            }
            $this->config["blockCms"]["dataAnswers"] = null;
            if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
                $cachedData = Yii::app()->cache->get('dashboardData'.$slug);
                $aCountQuery = array('form' => $this->config['blockCms']["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                if(isset($scopeQuery["answers"])){
                    $aCountQuery = array_merge($aCountQuery, $scopeQuery["answers"]);
                }
                $answersCount = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                if($answersCount==0){
                    $answersCount = 1;
                }
                $this->config["blockCms"]["dataAnswers"] = DashboardData::progressCircle($id, $answersCount, $this->config['blockCms'],  $scopeQuery["answers"] ?? null);
            }
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>