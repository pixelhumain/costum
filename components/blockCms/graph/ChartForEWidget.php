<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

    use Organization;
    use Person;
    use PHDB;
    use Yii;
    use yii\base\Widget;

    class ChartForEWidget extends Widget {
        public $defaultData = [
            "chartTitleColor" => "black",
            "chartTitleSize" => 20,
            "chartLegendSize" => 20,
            "chartLegendColor" => "black",
            "chartBlockHeight" => 250,
            "chartBlockWidth" => 100,
            "chartBlockBackgroundColor" => "transparent",
            "chartBlockBorderRadius" => 2,
            "chartAxeColor" => "transparent",
            "chartDataColorBg" => "transparent",

            "chartTitleText" => "Here Title",
            "chartDataLabel" => "Lorem Ipsum",
            "chartDataToShow" => [],
            "chartBlockType" => "doughnut",
            "chartAxeX" => true,
            "chartAxeY" => true,
            "chartDataColorGradient" => false,
            "chartLegendDisplay" => true,
            "chartLegendPosition" => "bottom",
            "chartDataElementTypes" => "organizations",
            "chartDataFields" => "type",
            "chartDesignImage" => false,
            "chartDataManual" => [
                [
                    "label" => "",
                    "value" => 0,
                    "color" => "blue"
                ]
            ],
            "chartDataFromFile" => [],
            "chartDataImportOrType" => "manuel",
            "chartDataUse" => false,
            "css" => array(
                "chartTitleCss" => array(
                    "color" => "black",
                    "fontSize" => "20px"
                ),
                "chartLegendCss" => array(
                    "color" => "black",
                    "fontSize" => "20px"
                ),
                "chartBlockCss" => array(
                    "width" => "100%",
                    "height" => "150px",
                    "backgroundColor" => "transparent",
                    "borderBottomLeftRadius" => "2px",
                    "borderBottomRightRadius" => "2px",
                    "borderTopLeftRadius" => "2px",
                    "borderTopRightRadius" => "2px"
                ),
                "chartAxeCss" => array(
                    "color" => "black"
                )

            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>