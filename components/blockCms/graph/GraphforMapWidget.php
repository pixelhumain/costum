<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use PHDB;
use SearchNew;
use Yii;
use yii\base\Widget;
use yii\web\Request;
use Zone;

class GraphforMapWidget extends Widget
{
    public $paramsData = [
        "marker" => array(
            "cluster" => "pie",
            "marker" => "default",
        ),
        "legende" => array(
            "show" =>  false,
            "dynamic" => false,
            "config" => [],
            "cible" => "",
            "var" => "type",
            "label" => "Type"
        ),
        "filters" => array(
            "activate" => false,
            "config" => "",
        ),
        "zone" => array(
            "zones" => [],
            "country" => '',
            "level" => ''
        ),
        "linkData" => "co2/search/globalautocomplete"
    ];
    public $path = "";
    public $config = [];

    public function init(){
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $this->config["country"] = PHDB::find(Zone::COLLECTION, array("level" => "1"), array("geoShape" => 0));
        $params = array();
        if($this->config["blockCms"]["zone"]["country"] != ''){
            $params["countryCode"] = $this->config["blockCms"]["zone"]["country"];
        }
        if($this->config["blockCms"]["zone"]["level"] != ''){
            $params["level"][] = $this->config["blockCms"]["zone"]["level"];
        }
        if(count($params) > 0){
            $this->config["zones"] = PHDB::find(Zone::COLLECTION, $params, array("geoShape" => 0));
        }else{
            $this->config["zones"] = [];
        }
    }
    public function run () {
        return $this->render($this->path, $this->config);
    }
}
