<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use SearchNew;
use yii\base\Widget;

class HexagoneBadgesWidget extends Widget
{
    public $defaultData = [
        'data' => [],
        'circle' => 50,
        "navigate" => "normal",
    ];
    public $config = [];
    public $path = "";


    public function init() {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
        $params = [
            "indexStep" => 0,
            "searchType" => ["badges"],
            "notSourceKey" => true,
            "filters" => [
                'preferences.private' => "false",
                '$or' => [
                    "issuer.".$this->config["costum"]["contextId"] => ['$exists' => true],
                    "source.keys" => $this->config["costum"]["contextSlug"],
                ]
            ]
        ];
        $results = SearchNew::globalAutoComplete($params)["results"];
        
        $searchAssigned = [
            "indexStep" => 0,
            "searchType" => ["organizations", "projects", "events", "citoyens"],
            "notSourceKey" => true,
            "filters" => [
                '$or' => []
            ],
            "fields" => ['badges']
        ];
        $ids = array_keys($results);
        $searchAssigned["filters"]['$or'] = array_map(fn($id) => ['$exists' => true], array_combine(array_map(fn($id) => "badges.$id", $ids), $ids));
        $assignedElements = SearchNew::globalAutoComplete($searchAssigned)["results"];
        $this->config["blockCms"]["data"] = $results;
        $this->config["blockCms"]["members"] = $assignedElements;
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}
