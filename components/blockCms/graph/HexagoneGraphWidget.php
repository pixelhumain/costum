<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use yii\base\Widget;

class HexagoneGraphWidget extends Widget{
    public $defaultData = [
        'data' => [],
        'circle' => 50,
        'navigate' => "normal",
        "groupped" => false,
        "css" => [
            "fillColor" => "black",
            "fillWidth" => 1
        ]
    ];
    
    public $config = [];
    public $path = "";

    public function init() {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
    }

    public function run() {
        return $this->render($this->path, $this->config);
    }
}