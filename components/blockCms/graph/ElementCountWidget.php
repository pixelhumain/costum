<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

    use Organization;
    use Person;
    use PHDB;
    use yii\base\Widget;

    class ElementCountWidget extends Widget {
        public $defaultData = [
            // "countTitleColor" => "#4AB5A1",
            // "elementIconBackground" => "#eee",
            
            "countTitle" => "",
            "elementIcon" => "group",
            "elementType" => Organization::COLLECTION,
            "design" => "",
            "count",
            "css" => array(
                "elementCss" => array(
                    "color" => "#4AB5A1",
                    "backgroundColor" => "#eee"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            $id = $this->config["costum"]["contextId"];
            $type = $this->config["costum"]["contextType"];
            $slug = $this->config["costum"]["contextSlug"];
            
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }

            $queryFilter = [
                array("source.keys" => $slug),
                array("parent.".$id => ['$exists'=>true])
            ];
        
            if($this->config["blockCms"]["elementType"]==Person::COLLECTION){
                array_push($queryFilter, array("links.memberOf.".$id => ['$exists'=>true]));
                array_push($queryFilter, array("links.projects.".$id => ['$exists'=>true]));
            }
        
            if($this->config["blockCms"]["elementType"]==Organization::COLLECTION){
                array_push($queryFilter, array("links.memberOf.".$id => ['$exists'=>true]));
            }
        
            $this->config["blockCms"]["count"] = PHDB::count($this->config["blockCms"]["elementType"], array('$or' => $queryFilter));

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>