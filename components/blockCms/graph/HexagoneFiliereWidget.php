<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use MongoId;
use PHDB;
use SearchNew;
use yii\base\Widget;

class HexagoneFiliereWidget extends Widget
{
    public $defaultData = [
        'data' => [],
        'circle' => 50,
        "navigate" => "depth",
        'thematic' => [
            'alimentation',
            'santé',
            'déchets',
            'transport',
            "éducation",
            "citoyenneté",
            'énergie',
            "culture",
            "Économie",
            "environnement",
            "numérique",
            "sport",
            "associations",
            "tiers lieux",
            "pacte"
        ]
    ];
    private $thematicAssociative = [
        "alimentation" => [
            "name" => "Food",
            "icon" => "fa-cutlery",
            "tags" => [
                "agriculture",
                "alimentation",
                "nourriture",
                "AMAP"
            ]
        ],
        "santé" => [
            "name" => "Health",
            "icon" => "fa-heart-o",
            "tags" => [
                "santé"
            ]
        ],
        "déchets" => [
            "name" => "Waste",
            "icon" => "fa-trash-o ",
            "tags" => [
                "déchets"
            ]
        ],
        "transport" => [
            "name" => "Transport",
            "icon" => "fa-bus",
            "tags" => [
                "Urbanisme",
                "transport",
                "construction"
            ]
        ],
        "éducation" => [
            "name" => "Education",
            "icon" => "fa-book",
            "tags" => [
                "éducation",
                "petite enfance"
            ]
        ],
        "citoyenneté" => [
            "name" => "Citizenship",
            "icon" => "fa-user-circle-o",
            "tags" => [
                "citoyen",
                "society"
            ]
        ],
        "Économie" => [
            "name" => "Economy",
            "icon" => "fa-money",
            "tags" => [
                "ess",
                "économie social solidaire"
            ]
        ],
        "énergie" => [
            "name" => "Energy",
            "icon" => "fa-sun-o",
            "tags" => [
                "énergie",
                "climat"
            ]
        ],
        "culture" => [
            "name" => "Culture",
            "icon" => "fa-universal-access",
            "tags" => [
                "culture",
                "animation"
            ]
        ],
        "environnement" => [
            "name" => "Environnement",
            "icon" => "fa-tree",
            "tags" => [
                "environnement",
                "biodiversité",
                "écologie"
            ]
        ],
        "numérique" => [
            "name" => "Numeric",
            "icon" => "fa-laptop",
            "tags" => [
                "informatique",
                "tic",
                "internet",
                "web",
                "numérique"
            ]
        ],
        "sport" => [
            "name" => "Sport",
            "icon" => "fa-futbol-o",
            "tags" => [
                "sport"
            ]
        ],
        "tiers lieux" => [
            "name" => "Third places",
            "icon" => "fa-globe",
            "tags" => [
                "TiersLieux"
            ]
        ],
        "pacte" => [
            "name" => "Pact",
            "icon" => "fa-hand-o-up",
            "tags" => [
                "pacte"
            ]
        ],
        "associations" => [
            "name" => "Associations",
            "icon" => "fa-chain",
            "tags" => [
                "associations"
            ]
        ]
    ];

    public $config = [];
    public $path = "";


    public function init()
    {
        parent::init();
        $typeCocity = @$this->config["costum"]["typeCocity"];
        $element = PHDB::findOne($this->config["costum"]["contextType"], ["_id" => new MongoId($this->config["costum"]["contextId"])]);
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
        }
        $searchTags = [];
        foreach ($this->config["blockCms"]['thematic'] as $key => $value) {
            $searchTags = array_merge($searchTags, $this->thematicAssociative[$value]["tags"]); 
        }
        $paramsSearch = [
            "indexStep" => 0,
            "searchType" => ["organizations", "projects", "events", "citoyens"],
            "notSourceKey" => true,
            "searchTags" => $searchTags,
            "filters" => []
        ];
        if($typeCocity == "region"){
            $paramsSearch["filters"] = [
                "address.level3" => $element["address"]["level3"]
            ];
        }else if($typeCocity == "departement" && $typeCocity == "district"){
            $paramsSearch["filters"] = [
                "address.level4" => $element["address"]["level4"]
            ];
        }else if($typeCocity == "epci"){

        }else if($typeCocity == "ville"){
            $paramsSearch["filters"] = [
                "address.localityId" => $element["address"]["localityId"]
            ];
        }else{
            $paramsSearch["filters"] = array(
                "thematic" => array(
                    '$exists' => 1
                ),
                "costum.typeCocity" => array(
                    '$exists' => 1
                ),
                "costum.cocity" => array(
                    '$exists' => 0
                )
            );
        }
        $data = SearchNew::globalAutoComplete($paramsSearch)["results"];
        $this->config["blockCms"]["data"] = $data;
    }

    public function run()
    {
        return $this->render($this->path, $this->config);
    }
}
