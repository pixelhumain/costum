<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use DashboardData;
use Form;
use MongoId;
use Organization;
    use Person;
    use PHDB;
use Yii;
use yii\base\Widget;

    class TextWithValueAndIconWidget extends Widget {
        public $defaultData = [
            // "percentColor" => "#9B6FAC",
            // "color01" => "#9B6FAC",
            // "color02" => "#ddd",
            // "progressBarHeight" => 35,
            // "labelSize" => 16,
            "withStaticTextBottom" => true,
            "label" => "Ajouter label ici",

            "coform" => "",
            "answerPath" => "",
            "answerValue" => "",
            "text" => 'Personnes ont assisté <br><span class="accentuated">à un événement artistico-culturel</span>',
            "profilImageUrl" => "",
            "design" => false,
            "css" => array(
                "percentColor" => array(
                    "color" => "#9B6FAC"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            $id = $this->config["costum"]["contextId"];
            $type = $this->config["costum"]["contextType"];
            $slug = $this->config["costum"]["contextSlug"];
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            if(($this->config['costum']["assetsSlug"] == "franceTierslieux") || ($this->config['costum']["assetsSlug"] == "reseauTierslieux")){
                $extraFilter = [
                    "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s" => [
                        '$exists' => true
                    ]
                ];
            }
            if($id){
                $where = array(
                    "_id" => new MongoId($id)
                );
            }else if($slug){
                $where = array(
                    "slug" => $slug
                );
            }
            $dataCostum = Yii::app()->cache->get("costum".$slug);
            if(!$dataCostum){
                $dataCostum = PHDB::findOne($type, $where, ['_id', "name", "address"]);
                Yii::app()->cache->set('costum'.$slug, $dataCostum, 3600);
            }
            $scopeQuery = array("coform" => array("shareToChildren" => true));
            if(isset($extraFilter) && is_array($extraFilter)){
                $scopeQuery["answers"] = $extraFilter;
            }
            if(isset($dataCostum["level"])){
                $level = $dataCostum["address"][$dataCostum["level"]];
                $levelNb = substr($dataCostum["level"], 5);
                if (!$level && $levelNb=="5"){
					$levelNb=$levelNb-1;
					$level=$dataCostum["address"]["level$levelNb"];
				}
            }
            if(isset($level) && $level!=null && $level!=""){
                $inScope = PHDB::find(Organization::COLLECTION , array(
                    "address.level$levelNb" => $level,
                    '$or' => array(
                        array("reference.costum"=>"franceTierslieux"),
                        array("source.keys"=>"franceTierslieux")
                    ) ), ["name", "collection"] );
                    
                $orga = array();
                foreach ($inScope as $key => $item) {
                    unset($item["_id"]);
                    array_push($communityIds, $key);
                    $item["type"] = $item["collection"];
                    unset($item["collection"]);
                    array_push($orga, [$key => $item]);
                }
                if(isset($scopeQuery["answers"])){
                    $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
                }
            }
            $this->config["blockCms"]["dataAnswers"] = null;
            if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
            
                $aCountQuery = array('form' => $this->config['blockCms']["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                if(isset($scopeQuery["answers"])){
                    $aCountQuery = array_merge($aCountQuery, $scopeQuery["answers"]);
                }
                $answersCount = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                $answersCountCoform = $answersCount;
                if($answersCount==0){
                    $answersCount = 1;
                }
                $this->config["blockCms"]["dataAnswers"] = DashboardData::simpleValue($id, $answersCount, $answersCountCoform,$this->config['blockCms'],  $scopeQuery["answers"] ?? null);
            }

        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>