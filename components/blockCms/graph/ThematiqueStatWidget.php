<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use Event;
use Organization;
use PHDB;
use Poi;
use Project;
use SearchNew;
use yii\base\Widget;

    class ThematiqueStatWidget extends Widget {
        public $path = "";
        public $config = [];
        public $paramsData = [];
        public $defaultData = [];


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
            }

            $allThem = [
                "alimentation" => [
                    "name" => "Food",
                    "icon" => "fa-cutlery",
                    "thematique" => "Alimentation",
                    "tags" => [
                        "agriculture",
                        "alimentation",
                        "nourriture",
                        "AMAP"
                    ]
                ],
                "santé" => [
                    "name" => "Health",
                    "icon" => "fa-heart-o",
                    "thematique" => "Santé",
                    "tags" => [
                        "santé"
                    ]
                ],
                "déchets" => [
                    "name" => "Waste",
                    "icon" => "fa-trash-o",
                    "thematique" => "Déchets",
                    "tags" => [
                        "déchets"
                    ]
                ],
                "transport" => [
                    "name" => "Transport",
                    "icon" => "fa-bus",
                    "thematique" => "Transport",
                    "tags" => [
                        "Urbanisme",
                        "transport",
                        "construction"
                    ]
                ],
                "éducation" => [
                    "name" => "Education",
                    "icon" => "fa-book",
                    "thematique" => "Éducation",
                    "tags" => [
                        "éducation",
                        "petite enfance"
                    ]
                ],
                "citoyenneté" => [
                    "name" => "Citizenship",
                    "icon" => "fa-user-circle-o",
                    "thematique" => "Citoyenneté",
                    "tags" => [
                        "citoyen",
                        "society"
                    ]
                ],
                "Économie" => [
                    "name" => "Economy",
                    "icon" => "fa-money",
                    "thematique" => "Économie",
                    "tags" => [
                        "ess",
                        "économie social solidaire"
                    ]
                ],
                "énergie" => [
                    "name" => "Energy",
                    "icon" => "fa-sun-o",
                    "thematique" => "Énérgie",
                    "tags" => [
                        "énergie",
                        "climat"
                    ]
                ],
                "culture" => [
                    "name" => "Culture",
                    "icon" => "fa-universal-access",
                    "thematique" => "Culture",
                    "tags" => [
                        "culture",
                        "animation"
                    ]
                ],
                "environnement" => [
                    "name" => "Environnement",
                    "icon" => "fa-tree",
                    "thematique" => "Environnement",
                    "tags" => [
                        "environnement",
                        "biodiversité",
                        "écologie"
                    ]
                ],
                "numérique" => [
                    "name" => "Numeric",
                    "icon" => "fa-laptop",
                    "thematique" => "Numérique",
                    "tags" => [
                        "informatique",
                        "tic",
                        "internet",
                        "web"
                    ]
                ],
                "sport" => [
                    "name" => "Sport",
                    "icon" => "fa-futbol-o",
                    "thematique" => "Sport",
                    "tags" => [
                        "sport"
                    ]
                ],
                "tiers lieux" => [
                    "name" => "Third places",
                    "icon" => "fa-globe",
                    "thematique" => "Tiers lieux",
                    "tags" => [
                        "TiersLieux"
                    ]
                ],
                "pacte" => [
                    "name" => "Pact",
                    "icon" => "fa-hand-o-up",
                    "thematique" => "Pacte",
                    "tags" => [
                        "pacte"
                    ]
                ],
                "associations" => [
                    "name" => "Associations",
                    "icon" => "fa-chain",
                    "thematique" => "Associations",
                    "tags" => [
                        "associations"
                    ]
                ]
            ];

            $_countResults = array();

            foreach ($allThem as $key => $value) {
                $_where = array(
                    "thematic" => $value['thematique']
                );
                $_countTemp = 0;
                $_countTemp += PHDB::count(Organization::COLLECTION, $_where);
                $_countTemp += PHDB::count(Project::COLLECTION, $_where);
                $_countTemp += PHDB::count(Poi::COLLECTION, $_where);
                $_countTemp += PHDB::count(Event::COLLECTION, $_where);
                
                $_countResults[$key] = $_countTemp;
                
            }
            arsort($_countResults);

            $_top10Results = array_slice($_countResults, 0, 10, true);

            $this->config['allDatas'] = $_top10Results;
            $this->config['thematic'] = $allThem;
        }

        public function run(){
            return $this->render($this->path, $this->config);
        }
    }
?>