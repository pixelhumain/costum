<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use DashboardData;
use Form;
use MongoId;
use Organization;
    use Person;
    use PHDB;
use Yii;
use yii\base\Widget;

    class TextWithValueWidget extends Widget {
        public $defaultData = [
            "textWithValue" => "",
            "textRight" => "",
            "textLeft" => "",
            "coform" => "",
            "answerPath" => "",
            "answerValue" => "",
            "withStaticTextRight"=>false,
            "withStaticTextLeft"=>false,
            "roundMillionValue" => false,
            "isAverage" => false,
            "applyFormForAll" => false,
            "countage" => false,
            "textAlign" => "center",
            "css" => array(
                "textValue" => array(
                    "fontSize" => "60px",
                    "color" => "#9B6FAC",
                    "backgroundColor" => "transparent"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();
            $id = $this->config["costum"]["contextId"];
            $type = $this->config["costum"]["contextType"];
            $slug = $this->config["costum"]["contextSlug"];
            $inScope = array();
            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
            if(($this->config['costum']["assetsSlug"] == "franceTierslieux") || ($this->config['costum']["assetsSlug"] == "reseauTierslieux")){
                $extraFilter = [
                    "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s" => [
                        '$exists' => true
                    ]
                ];
            }

            if(isset($this->config["extraParams"]["reseauId"]) || isset($_GET["reseauId"])){
                $id = isset($this->config["extraParams"]["reseauId"]) ? $this->config["extraParams"]["reseauId"] : $_GET["reseauId"];
                $type = "organizations";
            }
            if(isset($this->config["extraParams"]["slug"]) || isset($_GET["slug"])){
                $slug = isset($this->config["extraParams"]["slug"]) ? $this->config["extraParams"]["slug"] : $_GET["slug"];
                $id = null;
                $type = "organizations";
            }
            $scopeQuery = array("coform" => array("shareToChildren" => true));
            if(isset($extraFilter) && is_array($extraFilter)){
                $scopeQuery["answers"] = $extraFilter;
            }
            if($id){
                $where = array(
                    "_id" => new MongoId($id)
                );
            }else if($slug){
                $where = array(
                    "slug" => $slug
                );
            }
            $dataCostum = Yii::app()->cache->get("costum".$slug);
            if(!$dataCostum){
                $dataCostum = PHDB::findOne($type, $where, ['_id', "name", "address"]);
                Yii::app()->cache->set('costum'.$slug, $dataCostum, 3600);
            }
            if(isset($dataCostum["level"]) || isset($dataCostum["address"]["level3"])){
                $dataCostum["level"] = isset($dataCostum["level"]) ? $dataCostum["level"] : "level3";
                $level = $dataCostum["address"][$dataCostum["level"]];
                $id = (string)$dataCostum["_id"];
                $levelNb = substr($dataCostum["level"], 5);
                if (!$level && $levelNb=="5"){
                    $levelNb=$levelNb-1;
                    $level=$dataCostum["address"]["level$levelNb"];
                }
            }
            if($slug != "franceTierslieux" && (isset($level) && $level!=null && $level!="")){
                $orga = Yii::app()->cache->get('zonesId'.$slug);
                if(!$orga){
                    $inScope = PHDB::find(Organization::COLLECTION , array(
                        "address.level$levelNb" => $level,
                        '$or' => array(
                            array("reference.costum"=>"franceTierslieux"),
                            array("source.keys"=>"franceTierslieux")
                        ) ), ["name", "collection"] );
                        
                    $orga = array_map(function($item, $key){
                        unset($item["_id"]);
                        $item["type"] = $item["collection"];
                        unset($item["collection"]);
                        return array($key => $item);
                    }, $inScope, array_keys($inScope));
                    Yii::app()->cache->set('zonesId'.$slug, $orga, 3600);
                }
                if(isset($scopeQuery["answers"])){
                    $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
                }
            }else{
                $level = "";
            }
            $this->config["blockCms"]["dataAnswers"] = null;
            if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
                $cachedData = Yii::app()->cache->get('dashboardData'.$slug);
                if(isset($cachedData["global"]['formTL']) && !isset($cachedData["global"]['formTL'][$this->config['blockCms']['coform']]["shareToChildren"])){
                    unset($scopeQuery["answers"]);
                }
                $cachedFomrs = Yii::app()->cache->get('coform'.$this->config['blockCms']['coform'].$slug);
                if(!$cachedFomrs){
                    $aCountQuery = array('form' => $this->config['blockCms']['coform'], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                    if(isset($scopeQuery["answers"])){
                        $aCountQuery = array_merge($aCountQuery, $scopeQuery["answers"]);
                    }
                    $cachedFomrs = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                    Yii::app()->cache->set('coform'.$this->config['blockCms']['coform'].$slug,  $cachedFomrs);
                }
                $answersCount = $cachedFomrs;
                $answersCountCoform = $answersCount;
                if($answersCount==0){
                    $answersCount = 1;
                }
                if(isset($this->config['blockCms']["answerPath"]) && strpos($this->config['blockCms']["answerPath"], "nbTLgeomapped")!==false){
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::nbTLgeomapped($id, count($inScope));
                }else if(isset($this->config['blockCms']["answerPath"]) && strpos($this->config['blockCms']["answerPath"], "countByElementTags")!==false){
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::countByElementTags($id, $this->config['blockCms'], $level);
                }else if(isset($this->config['blockCms']["answerPath"]) && strpos($this->config['blockCms']["answerPath"], "percentageByRegion")!==false){
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::percentageByRegion($this->config['blockCms'], (string) $this->config['blockCms']["_id"], $level);
                }else if(isset($this->config['blockCms']["answerPath"]) && strpos($this->config['blockCms']["answerPath"], "countQPV")!==false){
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::countQPV($id, $this->config['blockCms'], $level);
                }else if(isset($this->config['blockCms']["answerPath"]) && strpos($this->config['blockCms']["answerPath"], "bail36mois")!==false){
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::bail36mois($this->config['blockCms']["coform"], $scopeQuery["answers"] ?? null);
                }else{
                    $this->config["blockCms"]["dataAnswers"] = DashboardData::simpleValue($id, $answersCount, $answersCountCoform,$this->config['blockCms'],  $scopeQuery["answers"] ?? null);
                }
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>