<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use CacheHelper;
use DashboardData;
use Form;
use MongoId;
use Organization;
use PHDB;
use Yii;
use yii\base\Widget;
class ProgressBarMultipleWidget extends Widget
{
    public $paramsData = [
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "css" => array(
            'labelColor' => array(
                'color' => "black"
            ),
            'percentColor' => array(
                'color' => "white"
            ),
            'emptyColor' => array(
                'backgroundColor' => "#FF286B"
            ),
            'completeColor' => array(
                'backgroundColor' => "#9B6FAC"
            ),
        ),
        "withStaticTextBottom" => false,
        "showValue" => false,
    ];
    public $path = "";
    public $config = [];
    public function init(){
        parent::init();
        $id = $this->config["costum"]["contextId"];
        $type = $this->config["costum"]["contextType"];
        $slug = $this->config["costum"]["contextSlug"];
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        if(($this->config['costum']["assetsSlug"] == "franceTierslieux") || ($this->config['costum']["assetsSlug"] == "reseauTierslieux")){
            $extraFilter = [
                "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s" => [
                    '$exists' => true
                ]
            ];
        }
        if(isset($this->config["extraParams"]["reseauId"]) || isset($_GET["reseauId"])){
            $id = isset($this->config["extraParams"]["reseauId"]) ? $this->config["extraParams"]["reseauId"] : $_GET["reseauId"];
            $type = "organizations";
        }
        if(isset($this->config["extraParams"]["slug"]) || isset($_GET["slug"])){
            $slug = isset($this->config["extraParams"]["slug"]) ? $this->config["extraParams"]["slug"] : $_GET["slug"];
            $id = null;
            $type = "organizations";
        }
        $scopeQuery = array("coform" => array("shareToChildren" => true));
        if(isset($extraFilter) && is_array($extraFilter)){
            $scopeQuery["answers"] = $extraFilter;
        }
        if($id){
            $where = array(
                "_id" => new MongoId($id)
            );
        }else if($slug){
            $where = array(
                "slug" => $slug
            );
        }
        $dataCostum = Yii::app()->cache->get("costum".$slug);
        if(!$dataCostum){
            $dataCostum = PHDB::findOne($type, $where, ['_id', "name", "address"]);
            Yii::app()->cache->set('costum'.$slug, $dataCostum, 3600);
        }
        if(isset($dataCostum["level"]) || isset($dataCostum["address"])){
            $dataCostum["level"] = isset($dataCostum["level"]) ? $dataCostum["level"] : "level3";
            $level = $dataCostum["address"][$dataCostum["level"]];
            $id = (string)$dataCostum["_id"];
            $levelNb = substr($dataCostum["level"], 5);
            if (!$level && $levelNb=="5"){
                $levelNb=$levelNb-1;
                $level=$dataCostum["address"]["level$levelNb"];
            }
        }
        if($slug != "franceTierslieux" && (isset($level) && $level!=null && $level!="")){
            $orga = Yii::app()->cache->get('zonesId'.$slug);
            if(!$orga){
                $inScope = PHDB::find(Organization::COLLECTION , array(
                    "address.level$levelNb" => $level,
                    '$or' => array(
                        array("reference.costum"=>"franceTierslieux"),
                        array("source.keys"=>"franceTierslieux")
                    ) ), ["name", "collection"] );
                    
                $orga = array_map(function($item, $key){
                    unset($item["_id"]);
                    $item["type"] = $item["collection"];
                    unset($item["collection"]);
                    return array($key => $item);
                }, $inScope, array_keys($inScope));
                Yii::app()->cache->set('zonesId'.$slug, $orga, 3600);
            }
            if(isset($scopeQuery["answers"])){
                $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
            }
        }
        $this->config["blockCms"]["dataAnswers"] = null;
        if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
        
            $aCountQuery = array('form' => $this->config['blockCms']["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
            if(isset($scopeQuery["answers"])){
                $aCountQuery = array_merge($aCountQuery, $scopeQuery["answers"]);
            }
            $answersCount = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
            if($answersCount==0){
                $answersCount = 1;
            }
            $this->config["blockCms"]["dataAnswers"] = DashboardData::horizontalBar($id, $answersCount, $this->config['blockCms'], $scopeQuery["answers"] ?? null);
        }      
    }
    public function run () {
        return $this->render($this->path, $this->config);
    }
}
