<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\graph;

use CacheHelper;
use DashboardData;
use Form;
use MongoId;
use Organization;
use PHDB;
use SearchNew;
use Yii;
use yii\base\Widget;
class GroupElementWidget extends Widget
{
    public $paramsData = [
        "choice" => "elements",
        "type" => ["organizations"],
        "design" => "pourcentageMultiple",
        "cible" => "",
        "config" => [],
        "nombre" => 4,
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "css" => array(
            "paddingBottom" => "10px",
            "paddingLeft" =>"0",
            "paddingRight" =>"0",
            "paddingTop" => "10px",
            "empty" => array(
                "fill" => "white"
            ),
            "percent" => array(
                "fill" => "#4524c9"
            ),
            "text" => array(
                "fill" => "#4524c9"
            ),
        ),
        "advanced" => [
            "target" => "tags"
        ]
    ];
    public $path = "";
    public $config = [];

    public function init(){
        parent::init();
        $id = $this->config["costum"]["contextId"];
        $type = $this->config["costum"]["contextType"];
        $slug = $this->config["costum"]["contextSlug"];
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        if($this->config["blockCms"]["advanced"]['target'] == ""){
            $this->config["blockCms"]["advanced"]['target'] = "tags";
        }
        if(isset($this->config["extraParams"]["reseauId"]) || isset($_GET["reseauId"])){
            $id = isset($this->config["extraParams"]["reseauId"]) ? $this->config["extraParams"]["reseauId"] : $_GET["reseauId"];
            $type = "organizations";
        }
        if(isset($this->config["extraParams"]["slug"]) || isset($_GET["slug"])){
            $slug = isset($this->config["extraParams"]["slug"]) ? $this->config["extraParams"]["slug"] : $_GET["slug"];
            $id = null;
            $type = "organizations";
        }
        $scopeQuery = array("coform" => array("shareToChildren" => true));
        if(isset($extraFilter) && is_array($extraFilter)){
            $scopeQuery["answers"] = $extraFilter;
        }
        if($id){
            $where = array(
                "_id" => new MongoId($id)
            );
        }else if($slug){
            $where = array(
                "slug" => $slug
            );
        }
        $dataCostum = Yii::app()->cache->get("costum".$slug);
        if(!$dataCostum){
            $dataCostum = PHDB::findOne($type, $where, ['_id', "name", "address"]);
            Yii::app()->cache->set('costum'.$slug, $dataCostum, 3600);
        }
        $id = (string)$dataCostum["_id"];
        $this->config["blockCms"]["dataCount"] = array();
        if($this->config["blockCms"]["choice"] == "elements"){
            if($this->config["blockCms"]["cible"] != ""){
                if(isset($this->config["costum"]["lists"][$this->config["blockCms"]["cible"]])){
                    $data = CacheHelper::get("globalAutoComplete$slug");
                    if(!$data){
                        $defaultFilters = array('$or' => array());
                        $defaultFilters['$or']["parent.$id"] = array('$exists' => true);
                        $defaultFilters['$or']["source.keys"] = $slug;
                        $defaultFilters['$or']["reference.costum"] = $slug;
                        // $defaultFilters['$or']['links.memberOf.'.$id] = array('$exists' => true);
                        $paramsFilters = array(
                            "notSourceKey" => true,
                            "searchType" => $this->config['blockCms']['type'],
                            "fields" => isset($this->config["costum"]["map"]["searchFields"]) ? $this->config["costum"]["map"]["searchFields"] : ["urls","address","geo","geoPosition", "tags", "type", "zone"],
                            "indexStep" => 0,
                            "filters" => $defaultFilters,
                            "count" => true,
                            "countType" => $this->config['blockCms']['type']
                        );
                        $data = SearchNew::globalAutoComplete($paramsFilters);
                        CacheHelper::set("globalAutoComplete$slug", $data);
                    }
                    $cibles = $this->config["costum"]["lists"][$this->config["blockCms"]["cible"]];
                    if($this->config["blockCms"]["design"] == "pourcentageMultiple"){
                        if (($key = array_search("Autre", $cibles)) !== false) {
                            unset($cibles[$key]);
                        }
                    }
                    foreach ($cibles as $element) {
                        $this->config["blockCms"]["dataCount"][$element] = 0;
                        foreach ($data["results"] as $key => $value) {
                            if(isset($value[$this->config["blockCms"]["advanced"]["target"]]) && array_search($element, $value[$this->config["blockCms"]["advanced"]["target"]])){
                                $this->config["blockCms"]["dataCount"][$element]++;
                            }
                        }
                        $this->config["blockCms"]["dataCount"]["percentageArray"][] = array("label" => $element, "value" => number_format(($this->config["blockCms"]["dataCount"][$element]/array_sum(array_values($data["count"])))*100, 1, ".", ""));
                    }
                }
                usort($this->config["blockCms"]["dataCount"]["percentageArray"], function($a, $b){
                    return $a["value"] < $b["value"];
                });
            }
        }else{
            if(($this->config['costum']["assetsSlug"] == "franceTierslieux") || ($this->config['costum']["assetsSlug"] == "reseauTierslieux")){
                $extraFilter = [
                    "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s" => [
                        '$exists' => true
                    ]
                ];
            }
            if(isset($dataCostum["level"]) || isset($dataCostum["address"]["level3"])){
                $dataCostum["level"] = isset($dataCostum["level"]) ? $dataCostum["level"] : "level3";
                $level = $dataCostum["address"][$dataCostum["level"]];
                $levelNb = substr($dataCostum["level"], 5);
                $id = (string)$dataCostum["_id"];
                if (!$level && $levelNb=="5"){
                    $levelNb=$levelNb-1;
                    $level=$dataCostum["address"]["level$levelNb"];
                }
            }
            if($slug != "franceTierslieux" && (isset($level) && $level!=null && $level!="")){
                $orga = Yii::app()->cache->get('zonesId'.$slug);
                if(!$orga){
                    $inScope = PHDB::find(Organization::COLLECTION , array(
                        "address.level$levelNb" => $level,
                        '$or' => array(
                            array("reference.costum"=>"franceTierslieux"),
                            array("source.keys"=>"franceTierslieux")
                        ) ), ["name", "collection"] );

                    $orga = array_map(function($item, $key){
                        unset($item["_id"]);
                        $item["type"] = $item["collection"];
                        unset($item["collection"]);
                        return array($key => $item);
                    }, $inScope, array_keys($inScope));
                    Yii::app()->cache->set('zonesId'.$slug, $orga, 3600);
                }
                if(isset($scopeQuery["answers"])){
                    $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
                }
            }
            if(($this->config['blockCms']["coform"] != "") && ($this->config['blockCms']["answerPath"] != "")){
            
                $aCountQuery = array('form' => $this->config['blockCms']["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                if(isset($scopeQuery["answers"])){
                    $aCountQuery = array_merge($aCountQuery, $scopeQuery["answers"]);
                }
                $answersCount = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                if($answersCount==0){
                    $answersCount = 1;
                }
                $this->config["blockCms"]["dataCount"] = DashboardData::horizontalBar($id, $answersCount, $this->config['blockCms'], $scopeQuery["answers"] ?? null);
            }
        }
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
    
}