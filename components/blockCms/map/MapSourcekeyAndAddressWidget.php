<?php 
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use Element;
use yii\base\Widget;

    class MapSourcekeyAndAddressWidget extends Widget {
        public $defaultData = [
            "title"      => " <font color=\"#005e6f\">Les Existants</font><div><br></div>"
        ];

        public $config = [];
        public $path = "";
        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }   

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            $this->config['el'] = $el;
        }
        public function run() {
            return $this->render($this->path, $this->config);
        }
    }
?>