<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use City;
use Element;
use MongoId;
use Organization;
use PHDB;
use SearchNew;
use Template;
use yii\base\Widget;
use Zone;

class CocityMapWidget extends Widget
{
    
    public $path = "";
    public $config = [];
    public $paramsData = [
        "click" => "answers",
        "css" => array(
            "titres" => array(
                "color" => "#84d70f",
                "fontSize" => "30"
            )
        )
    ];
    public $defaultData = [];

    public function init() {
        parent::init();
        
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
    
        $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
        $typeCocity = isset($this->config["costum"]["typeCocity"]) ? $this->config["costum"]["typeCocity"] : "";
        $address = isset($el['address']) ? $el['address'] : "";
    
        $filters = [];
        $fields = [];
    
        if ($address != "" && $typeCocity != "ville" && $typeCocity != "") {
            if ($typeCocity == "epci") {
                $_zone = PHDB::findOne(Zone::COLLECTION, ["name" => $address['level5Name']], ['cities']);
                $_cities = $_zone['cities'] ?? [];
                $filters = ["_id" => ['$in' => array_map(fn($id) => new MongoId($id), $_cities)]];
            } else if ($typeCocity == "district") {
                $filters = ["level4" => $address['level4'], "country" => "MG"];
            } else if ($typeCocity == "departement") {
                $filters = [
                    "level4" => $address['level4'],
                    "countryCode" => $address['addressCountry'] ?? "FR",
                    "level" => ["5"]
                ];
            } else if ($typeCocity == "region") {
                $filters = [
                    "level3" => $address['level3'],
                    "countryCode" => $address['addressCountry'] ?? "FR",
                    "level" => "4"
                ];
            }
    
            $fields = ["geoShape", "name", "level1", "level1Name", "level3", "level3Name", "level4Name", "level4", "cocityId", "country", "countryCode", "level","cities"];
            if (!empty($filters)) {
                // $paramsFilters = [
                //     "notSourceKey" => true,
                //     "searchType" => $typeCocity == "epci" || $typeCocity == "district" ? ["cities"] : ["zones"],
                //     "fields" => $fields,
                //     "filters" => $filters
                // ];

                // $data = SearchNew::globalAutoComplete($paramsFilters);

                $_searchType = $typeCocity == "epci" || $typeCocity == "district" ? "City" : "Zone";

                $_zones = PHDB::find($_searchType::COLLECTION, $filters, $fields);

                if(count($_zones) > 0) {
                    if($_searchType == "Zone") {
                        $levelAdrress = "";
                        foreach ($_zones as $subKey => $subValue) {
                            $levelAdrress = $subValue['level'];
                        }
                        if(count($levelAdrress) > 1) {
                            $_whereCitis = array(
                                "level3" => $address['level3'],
                                "countryCode" => $address['addressCountry'] ?? "FR",
                                "level" => ["5"]
                            );
                            $this->config['blockCms']['zones'] = PHDB::find(Zone::COLLECTION, $_whereCitis, $fields);
                            $this->config['initTypeCreate'] = 'departement';
                        } else {
                            $this->config['blockCms']['zones'] = $_zones;
                        }
                    } else {
                        $this->config['blockCms']['zones'] = $_zones;
                    }
                }
                // if (isset($data['results'])) {
                //     if($paramsFilters['searchType'] == "zones") {
                //         $levelAdrress = "";
                //         foreach ($data['results'] as $subKey => $subValue) {
                //             $levelAdrress = $subValue['level'];
                //         }
                //         if(count($levelAdrress) > 1) {
                //             $_whereCitis = array(
                //                 "level3" => $address['level3'],
                //                 "countryCode" => $address['addressCountry'] ?? "FR",
                //                 "level" => ["5"]
                //             );
                //             $this->config['blockCms']['zones'] = PHDB::find(Zone::COLLECTION, $_whereCitis, $fields);
                //             $this->config['initTypeCreate'] = 'departement';
                //         } else {
                //             $this->config['blockCms']['zones'] = $data['results'];
                //         }
                //     } else {
                //         $this->config['blockCms']['zones'] = $data['results'];
                //     }
                // }
            }
    
            $this->handleTemplateAndSlug($el, $typeCocity, $address);
        }
    
        $this->config['el'] = $el;
    }
    
    private function handleTemplateAndSlug($el, $typeCocity, $address) {
        $templateCocity = PHDB::findOne(Template::COLLECTION, ["name" => "Template cocity", "source.key" => "templateCocity"]);
        $templateFiliere = PHDB::findOne(Template::COLLECTION, ["name" => "Template Filiere", "source.key" => "templateFiliere"]);
        
        $_sousCocity = [];
        $_slugRegion = "";
        $_slugDepartement = "";
        $_allSlugThematiques = [];
        
        if (isset($typeCocity)) {
            $_where = [];
            $_typeZone = $address["addressCountry"] == "MG" ? "district" : "departement";
            
            if ($typeCocity == "region") {
                $_where = [
                    "address.level3" => $address['level3'],
                    "costum.cocity" => ['$exists' => true],
                    "costum.typeCocity" => $_typeZone
                ];
            } else if ($typeCocity == "departement") {
                $_where = [
                    "address.level3" => $address['level3'],
                    "address.level4" => $address['level4'],
                    "costum.cocity" => ['$exists' => true],
                    "costum.typeCocity" => 'epci'
                ];
                $_slugRegion = $this->fetchSlug($address, 'region');
            } else if ($typeCocity == "epci") {
                $_cities = $this->fetchCities($address['level5Name']);
                $_where = [
                    "address.localityId" => ['$in' => $_cities],
                    "costum.cocity" => ['$exists' => true],
                    "costum.typeCocity" => 'ville'
                ];
                $_slugDepartement = $this->fetchSlug($address, 'departement');
                $_slugRegion = $this->fetchSlug($address, 'region');
            } else if ($typeCocity == "district") {
                $_where = [
                    "address.level4" => $address['level4'],
                    "costum.cocity" => ['$exists' => true],
                    "costum.typeCocity" => 'ville'
                ];
                $_slugRegion = $this->fetchSlug($address, 'region');
            }
            $_sousCocity = PHDB::find(Organization::COLLECTION, $_where);
            foreach ($_sousCocity as $id => $details) {
                if (isset($details['thematic'])) {
                    $_allSlugThematiques[$details['slug']] = [];
                    foreach ($details['thematic'] as $thematique) {
                        $thematiqueFormate = mb_convert_case($thematique, MB_CASE_TITLE, "UTF-8");
                        $slug = PHDB::find(Organization::COLLECTION, array("thematic" => $thematiqueFormate, "source.key" => $details['slug'] ), array('slug'));
                        if (!empty($slug)) {
                            $slugValue = reset($slug);
                            if (isset($slugValue['slug'])) {
                                $_allSlugThematiques[$details['slug']][$thematique] = $slugValue['slug'];
                            }
                        }
                    }
                } 
            }
        }
        $this->config['allSlugThematic'] = $_allSlugThematiques;
        $this->config['slugRegion'] = $_slugRegion;
        $this->config['slugDepartement'] = $_slugDepartement;
        $this->config['_idTemplatecocity'] = isset($templateCocity["_id"]) ? (string)$templateCocity["_id"] : "";
        $this->config['_idTemplatefiliere'] = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : "";
        $this->config['sousCocity'] = $_sousCocity;
    }
    
    private function fetchSlug($address, $typeCocity) {
        $_whereSlug = [
            "address.level3" => $address['level3'],
            "costum.cocity" => ['$exists' => true],
            "costum.typeCocity" => $typeCocity
        ];
        $slug = PHDB::findOne(Organization::COLLECTION, $_whereSlug, ['slug']);
        return $slug['slug'] ?? "";
    }
    
    private function fetchCities($level5Name) {
        $_zone = PHDB::findOne(Zone::COLLECTION, ["name" => $level5Name], ['cities']);
        return $_zone['cities'] ?? [];
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}
