<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use Yii;
use yii\base\Widget;

class MapVisualisationWidget extends Widget
{
    public $path = "";
    public $config = [];
    public $paramsData = [
        "marker" => "mixte",
        "url" => "",
        "click" => "answers"
    ];
    public function init(){
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $this->config["blockCms"]["dataZone"] = null;
        if($this->config["blockCms"]["url"] != ""){
            $this->config["blockCms"]["dataZone"] = Yii::$app->runAction($this->config["blockCms"]["url"]);
        }
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}

