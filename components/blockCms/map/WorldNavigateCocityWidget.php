<?php

    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use Element;
use MongoId;
use Organization;
use PHDB;
use SearchNew;
use Template;
use yii\base\Widget;
use Zone;

    class WorldNavigateCocityWidget extends Widget {
        public $path = "";
        public $config = [];
        public $paramsData = [
            "click" => "answers",
        ];
        public $defaultData = [];

        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
            }

            $templateCocity = PHDB::findOne(Template::COLLECTION,array(
                "name" => "Template cocity",
                "source.key" => "templateCocity"
            ));
        
            $templateFiliere = PHDB::findOne(Template::COLLECTION,array(
                "name" => "Template Filiere",
                "source.key" => "templateFiliere"
            ));

            $fields = ["geoShape", "name", "level1", "level1Name", "level3", "level3Name", "level4", "level4Name", "level5", "level5Name", "cocityId", "country", "countryCode", "level"];
            $fieldsgeo = ["geo", "geoPosition", "geoShape", "name", "level1", "level1Name", "level3", "level3Name", "level4", "level4Name", "level5", "level5Name", "cocityId", "country", "countryCode", "level"];

            $where = array(
                '$or' => array(
                    ["level" => ['3']],
                    ["level" => ['1', '3']],
                    ["level" => ['1', '3', '4']]
                )
            );
            $sousCocitys = PHDB::find(Organization::COLLECTION, array("costum.cocity" => ['$exists' => true]));
            $allSlugThematiques = [];
            foreach ($sousCocitys as $id => $details) {
                if (isset($details['thematic'])) {
                    $allSlugThematiques[$details['slug']] = [];
                    foreach ($details['thematic'] as $thematique) {
                        // $thematiqueFormate = mb_convert_case($thematique, MB_CASE_TITLE, "UTF-8");
                        $thematiqueFormate = mb_strtoupper(mb_substr($thematique, 0, 1), "UTF-8") . mb_substr($thematique, 1, null, "UTF-8");
                        $slug = PHDB::find(Organization::COLLECTION, array("thematic" => $thematiqueFormate, "source.key" => $details['slug'] ), array('slug'));
                        if (!empty($slug)) {
                            $slugValue = reset($slug);
                            if (isset($slugValue['slug'])) {
                                $allSlugThematiques[$details['slug']][$thematique] = $slugValue['slug'];
                            }
                        }
                    }
                } 
            }

            $el = Element::getByTypeAndId($this->config["costum"]["contextType"], $this->config["costum"]["contextId"]);
            $collection = "Zone";
            $this->config['el'] = $el;
            if($el['slug'] != "cocityPrez") {
                $typeCocity = isset($this->config["costum"]["typeCocity"]) ? $this->config["costum"]["typeCocity"] : "";
                $address = isset($el['address']) ? $el['address'] : "";
                if($typeCocity != "" && $address != "") {
                    switch ($typeCocity) {
                        case "epci":
                            $_zone = PHDB::findOne(Zone::COLLECTION, ["name" => $address['level5Name']], ['cities']);
                            $_cities = $_zone['cities'] ?? [];
                            $where = ["_id" => ['$in' => array_map(fn($id) => new MongoId($id), $_cities)]];
                            $collection = "City";
                            break;
                    
                        case "district":
                            $where = [
                                "level4" => $address['level4'],
                                "country" => "MG"
                            ];
                            $collection = "City";
                            break;
                    
                        case "departement":
                            $where = [
                                "level4" => $address['level4'],
                                "countryCode" => $address['addressCountry'] ?? "FR",
                                "level" => ["5"]
                            ];
                            break;
                    
                        case "region":
                            $where = [
                                "level3" => $address['level3'],
                                "countryCode" => $address['addressCountry'] ?? "FR",
                                "level" => "4"
                            ];
                            break;
                        
                        default:
                            $where = [
                                "level3" => $address['level3'],
                                "countryCode" => $address['addressCountry'] ?? "FR",
                                "level" => "4"
                            ];
                            break;
                    }
                }

                if(isset($address['level3']) && isset($this->config["costum"]["typeCocity"]) && $this->config["costum"]["typeCocity"] != "region") {
                    $_where_cocity_region = array(
                        "costum.cocity" => array('$exists' => true),
                        "costum.typeCocity" => 'region',
                        "address.level3" => $address['level3']
                    );

                    $cocityParent = PHDB::findOne(Organization::COLLECTION, $_where_cocity_region, ['slug']);

                    $this->config['cocityParent'] = $cocityParent;

                    // var_dump($cocityParent['slug']); die;
                }
            }

            $data = PHDB::find($collection::COLLECTION, $where, $fields);
            $dataGeo = PHDB::find($collection::COLLECTION, $where, $fieldsgeo);
            if (isset($data)) {
                $this->config['zones'] = $data;
            }

            if (isset($dataGeo)) {
                $this->config['zonesgeo'] = $dataGeo;
            }

            $_allCocitys = [];
            $_whr = array(
                "costum.cocity" => array('$exists' => true)
            );

            if(isset($el['slug']) && $el['slug'] != "cocityPrez" && isset($el['address']) && isset($el['costum']['typeCocity'])) {
                switch ($typeCocity) {
                    case "epci":
                        $_whr = array(
                            "costum.cocity" => array('$exists' => true),
                            "address.level5" => $el['address']['level5']
                        );
                        break;
                
                    case "district":
                    case "departement":
                        $_whr = array(
                            "costum.cocity" => array('$exists' => true),
                            "address.level4" => $el['address']['level4']
                        );
                        break;
                
                    case "region":
                        $_whr = array(
                            "costum.cocity" => array('$exists' => true),
                            "address.level3" => $el['address']['level3']
                        );
                        break;
                    
                    default:
                        $_whr = array(
                            "costum.cocity" => array('$exists' => true),
                            "address.level3" => $el['address']['level3']
                        );
                        break;
                }
            }

            $_allCocitys = PHDB::find(Organization::COLLECTION, $_whr);
            $this->config['allCocitys'] = $_allCocitys;
            
            $_idTemplatecocity = isset($templateCocity["_id"]) ? (string)$templateCocity["_id"] : "";
            $_idTemplatefiliere = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : "";
            $this->config['_idTemplatecocity'] = $_idTemplatecocity;
            $this->config['_idTemplatefiliere'] = $_idTemplatefiliere;
            $this->config['sousCocity'] = $sousCocitys;
            $this->config['allSlugThematic'] = $allSlugThematiques;
        }

        public function run(){
            return $this->render($this->path, $this->config);
        }
    }

?>