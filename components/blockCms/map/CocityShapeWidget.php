<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use Organization;
use PHDB;
use Template;
use yii\base\Widget;

class CocityShapeWidget extends Widget
{
    
    public $path = "";
    public $config = [];
    public $paramsData = [
        "click" => "answers"
    ];

    public function init() {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $templateCocity = PHDB::findOne(Template::COLLECTION,array(
            "name" => "Template cocity",
            "source.key" => "templateCocity"
        ));
    
        $templateFiliere = PHDB::findOne(Template::COLLECTION,array(
            "name" => "Template Filiere",
            "source.key" => "templateFiliere"
        ));

        $level3Cocity = PHDB::find(
            Organization::COLLECTION, 
            array(
                "costum.cocity" => array(
                    '$exists'=> true
                ),
                "costum.typeCocity" => "region"
            ),
            array(
                "address.level3"
            )
        );

        $level3CocityExist = [];

        if (count($level3Cocity) > 0) {
            foreach ($level3Cocity as $key => $value) {
                $level3 = $value['address']['level3'];
                $level3CocityExist[] = $level3;
            }
        }
    
        $_idTemplatecocity = isset($templateCocity["_id"]) ? (string)$templateCocity["_id"] : "";
        $_idTemplatefiliere = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : "";
        $this->config['_idTemplatecocity'] = $_idTemplatecocity;
        $this->config['_idTemplatefiliere'] = $_idTemplatefiliere;
        $this->config['_level3Cocity'] = $level3CocityExist;
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}
