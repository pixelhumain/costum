<?php
namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use DashboardData;
use Form;
use PHDB;
use SearchNew;
use yii\base\Widget;

class BasicmapsWidget extends Widget
{
    public $paramsData = [
        "activeFilter" => "false",
        "elementsType" => ["projects","organizations","events", "citoyens","poi"],
        "btnAddText" =>  "",
        "activeContour" => false,
        "legende" => array(
            "show" =>  false,
            "config" => [],
            "cible" => "",
        ),
        "css" => array(
            'styleAnnuaire' => array(
                "backgroundColor" => "#AFCB21",
                "borderColor" => "#AFCB21",
                "color" => "white",
                "borderBottomLeftRadius" => "2px",
                "borderBottomRightRadius" => "2px",
                "borderTopLeftRadius" => "2px",
                "borderTopRightRadius" => "2px",
            ),
            'mapLegendeContainer' => array(
                "backgroundColor" => "white",
                "paddingTop" => "10px",
                "paddingRight" => 0,
                "paddingBottom" => 0,
                "paddingLeft" => "10px",
            ),
            "map" => array(
                'height' => "550px"
            )
        ),
        "styleFilter" => array(
            "headerColor" => "#AFCB21"
        ),
        "btnAnnuaire" => array(
            "text" => "Voir dans l'annuaire",
            "link" => "search",
            "isLbh" => true
        ),
        "advanced" => array(
            "legendevar" => "tags",
            "thematic" => array(
                "themeshow" => false,
                "themecible" => ""
            ),
            "linked" => array(
                "linkshow" => false,
                "linkcible" => "",
                "linkconfig" => [],
            )
        )
    ];
    public $path = "";
    public $config = [];
    public function init(){
        parent::init();
        if (isset($this->config["blockCms"]["elementsType"])) {
            unset($this->paramsData["elementsType"]);
        }
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        if($this->config["blockCms"]["elementsType"] == ""){
            $this->config['blockCms']['elementsType'] = ["projects","organizations","events", "citoyens","poi"];
        }
        if($this->config["blockCms"]["advanced"]['legendevar'] == ""){
            $this->config["blockCms"]["advanced"]['legendevar'] = "tags";
        }
        $defaultFilters = array('$or' => array());
        $defaultFilters['$or']['parent'.$this->config["costum"]["contextId"]] = array('$exists' => true);
        $defaultFilters['$or']["source.keys"] = $this->config["costum"]["contextSlug"];
        $defaultFilters['$or']["reference.costum"] = $this->config["costum"]["contextSlug"];
        $defaultFilters['$or']['links.memberOf.'.$this->config["costum"]["contextId"]] = array('$exists' => true);
        $paramsFilters = array(
            "notSourceKey" => true,
            "searchType" => $this->config['blockCms']['elementsType'],
            "fieatlds" => isset($this->config["costum"]["map"]["searchFields"]) ? $this->config["costum"]["map"]["searchFields"] : ["urls","address","geo","geoPosition", "tags", "type", "zone"],
            "activeContour" => $this->config['blockCms']['activeContour'],
            "indexStep" => 0,
            "filters" => $defaultFilters
        );
        $data = SearchNew::globalAutoComplete($paramsFilters);
        if(isset($data['zones'])){
            $this->config['blockCms']["elts"] = array_merge($data['results'], $data['zones']);
        }else{
            $this->config['blockCms']["elts"] = $data['results'];
        }
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}
