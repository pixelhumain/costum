<?php

namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\map;

use Organization;
use PHDB;
use yii\base\Widget;

class ShapeVisualisationWidget extends Widget
{
    
    public $path = "";
    public $config = [];
    public $paramsData = [
        "click" => "answers"
    ];

    public function init() {
        parent::init();
        if (isset($this->config["blockCms"])) {
            $this->config["blockCms"] = array_replace_recursive($this->paramsData, $this->config["blockCms"]);
        }
        $reseaux = PHDB::find( Organization::COLLECTION,["tags" => "RéseauTiersLieux" ], ['name',"profilMediumImageUrl", "slug", "costum.host"]) ?? [];
        foreach ($reseaux as $key => $reseau) {
            if(isset($reseau["costum"]["host"])){
                $reseau["host"] = $reseau["costum"]["host"];
            }
            unset($reseau["costum"]);
            $reseaux[$reseau["slug"]] = $reseau;
            unset($reseaux[$key]);
        }
        $this->config["blockCms"]["reseaux"] = $reseaux;
    }

    public function run(){
        return $this->render($this->path, $this->config);
    }
}
