<?php
    namespace PixelHumain\PixelHumain\modules\costum\components\blockCms\annuaire;

    use yii\base\Widget;

    class AnnuaireDropdownWidget extends Widget {
        public $defaultData = [
            // "blockCmsColorTitle1" => "",
            // "blockCmsTextSizeTitle1" => "",

            "textSlogan" => "Lorem ipsum dolor sit amet...",
            "iconTitre" => "",
            "sectionTitle" => "CARTO/ANNUAIRE",
            "imageCarte" => "",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas explicabo nesciunt exercitationem placeat? Cupiditate harum debitis nesciunt deleniti hic? Similique obcaecati dicta hic aspernatur officia dolor eum ut. Sit.",
            "typeActeurs" => "",
            "lien" => "#search",

            "css" => array(
                "titreCss" => array(
                    "color" => "#1a242f",
                    "fontSize" => "36px"
                )
            )
        ];
        
        public $config = [];
        public $path = "";


        public function init() {
            parent::init();

            if (isset($this->config["blockCms"])) {
                $this->config["blockCms"] = array_replace_recursive($this->defaultData, $this->config["blockCms"]);
            }
        }

        public function run() {
            return $this->render($this->path, $this->config);
        }
    }

?>