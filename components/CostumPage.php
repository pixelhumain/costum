<?php
namespace PixelHumain\PixelHumain\modules\costum\components;

use CacheHelper;
use CO2Stat;
use Costum;
use Menu;
use Yii;

class CostumPage {
    const DEFAULT_COSTUM_LAYOUT = "costum.views.cmsBuilder.tplEngine.index";
    private $controller;
    private $costum=null;

    public function __construct($controller, $costum){
        $this->controller = $controller;
        $this->controller->layout = "//layouts/mainSearch";
        $this->costum = $costum;
    }

    private function getView($page=null, $url=null, $extra=null, $renderPartial=false){
        CO2Stat::incNbLoad("co2-info");


        if($page != "" ) {
            if (isset($this->costum["app"]["#".$page]["linked"])) {
                $this->costum["contextId"] = $this->costum["app"]["#".$page]["linked"]["costumId"];
                $page = $this->costum["app"]["#".$page]["linked"]["page"];
            }
            
        }

        $params=array("page"=>@$page);
        $params=array_merge($params, $_POST);

        if(!empty($extra)){
        	$extraP=explode(".", $extra);
        	$i=0;
        	for ($i=0; $i < $extraP;) { 
        		$params[$extraP[$i]]=$extraP[$i+1];
        		$i+=2;
        	}
        } 

        // $this->costum["contextId"] = "656987912c24bf755355be64";
        // echo "<pre>";
        // var_dump($page);
        // echo "</pre>";
        // $page = "mah";
        // $this->costum["editMode"] = false;
        if(isset($page) && empty($url)){
            $url = CostumPage::DEFAULT_COSTUM_LAYOUT;
            $params = Costum::getPageParams($this->costum, $page);
        }

        $pageDetail = !empty($this->costum["app"]["#".$params["page"]]) ? $this->costum["app"]["#".$params["page"]] : [];
        $msgNotAuthorized = "";
        if(!isset($this->costum["msgNotAuthorized"])){
            $msgNotAuthorized = '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
                                    <i class="fa fa-lock fa-4x "></i><br/>
                                    <h1 class="">'.Yii::t("organization", "Unauthorized Access.").'</h1>
                                </div>';
        }else{
            $msgNotAuthorized = $this->costum["msgNotAuthorized"];
        }

        if($this->costum != false && !Menu::showButton($pageDetail))
            return $msgNotAuthorized;

        if($renderPartial)
            return $this->controller->renderPartial($url, $params);

        return $this->controller->render($url, $params);
    }

    public function render($page=null, $url=null, $extra=null, $pageParams=null){
        $this->setMetadata($page, $pageParams);

        return $this->getView($page, $url, $extra, false);
    }

    public function renderPartial($page=null, $url=null, $extra=null){
        return $this->getView($page, $url, $extra, true);
    }

    public function setMetadata($hash, $hashParams){
        $costumPageInspector = new CostumPageInspector($hash, $hashParams);
        $metadata = $costumPageInspector->getMetadata();

        if(isset($metadata) && property_exists($this->controller, "module")){
            if(isset($this->controller->module->description) && ($metadata["shortDescription"] !== "" || $metadata["description"] !== ""))
                $description = !empty($metadata["shortDescription"]) ? $metadata["shortDescription"]:$metadata["description"];
				$description = $description ?? '';
                $description = htmlspecialchars($description, ENT_QUOTES, 'UTF-8');
                $description = mb_substr($description, 0, 400);

                $this->controller->module->description = $description;

            if(isset($this->controller->module->pageTitle) && $metadata["title"] !== "")
                $this->controller->module->pageTitle .= " - ".$metadata["title"];

            if(isset($this->controller->module->image) && isset($metadata["image"]))
                $this->controller->module->image = $metadata["image"];

            if(isset($this->controller->module->relCanonical) && isset($metadata["relCanonical"]))
                $this->controller->module->relCanonical = $metadata["relCanonical"];
        }
    }
}

