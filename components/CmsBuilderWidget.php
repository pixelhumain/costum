<?php
namespace PixelHumain\PixelHumain\modules\costum\components;

use Cms;
use yii\base\Widget;

class CmsBuilderWidget extends Widget{
    const BLOCKS_CONTAINERS = [
        [
            "name" => "section",
            "label" => "Section",
            "image" => "section.png"
        ],
        [
            "name" => "column",
            "label" => "Colonne",
            "image" => "column.png"
        ]
    ];
    const BLOCKS_CONTENTS = [
        [
            "name" => "text",
            "label" => "Texte",
            "image" => "text.png"
        ],
        [
            "name" => "button",
            "label" => "Bouton",
            "image" => "button.png"
        ],
        [
            "name" => "image",
            "label" => "Image",
            "image" => "image.png"
        ],
        [
            "name" => "separator",
            "label" => "Separatrice",
            "image" => "separator.png"
        ],
        [
            "name" => "chart",
            "label" => "Graph",
            "image" => "chart.png"
        ],
        [
            "name" => "icon",
            "label" => "Icon",
            "image" => "icon.png"
        ],
        [
            "name" => "blockcms",
            "label" => "Block cms",
            "image" => "cms.png"
        ]
    ];

    public $costum;

    public function init(){
        parent::init();
        ob_start();
    }

    public function run(){
        $content = ob_get_clean();
        if(isset($this->costum["editMode"]) && $this->costum["editMode"]){
          /*  $customBlocks = Cms::getCmsByWhere(array("type" => "blockCms"),array( 'name' => 1 ));
            $customBlockTypes = [];

            foreach($customBlocks as $block){
                if (isset($block["path"])) {
                    $pathParts = explode(".", $block["path"]);
                    if(isset($pathParts[2]) && !in_array($pathParts[2], $customBlockTypes))
                        $customBlockTypes[] = $pathParts[2];
                }else{                    
                    if (!in_array("Multi-section" , $customBlockTypes)) {                       
                       $customBlockTypes[]  = "Multi-section";
                   }
                }

            }
            sort($customBlockTypes);*/

            $content = $this->render("cmsBuilderLayout", [
                "content"=>$content,
                "costum" => $this->costum,
                "blocks" => [
                    "containers" => self::BLOCKS_CONTAINERS,
                    "contents" => self::BLOCKS_CONTENTS
                ],
               // "customBlocks" => $customBlocks,
                //"customBlockTypes" => $customBlockTypes
            ]);
        }

        return $content;
    }
}