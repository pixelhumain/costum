<?php
namespace PixelHumain\PixelHumain\modules\costum\components;

use MongoId;
use PHDB;
use Poi;
use Project;

class CostumPageInspector{
    private $hash = "";
    private $hashParams;

    public function __construct($hash, $params){
        $this->hash = $hash;
        $this->hashParams = trim(base64_decode($params));    
    }

    public function getMetadata(){
        $metadata = null;
        
        if($params = $this->getParsedParams()){
            switch($params["name"]){
                case "preview":
                    $metadata = $this->getMetadataPreview($params["value"]);
                break;
            }
        }
        
        if(!isset($metadata) && isset($this->hash) && $this->hash !== ""){
            //return metatada of hash page
            return;
        }

        return $metadata;
    }

    private function getMetadataPreview($path){
        $path_parts = explode(".", $path);
        if(sizeof($path_parts) < 2)
            return null;

        $collection = $path_parts[0];
        $id = new MongoId($path_parts[1]);
        
        switch(strtolower($collection)){
            case "poi":
                $data = PHDB::findOne(Poi::COLLECTION, ["_id" => $id]);
                if(!isset($data))
                    return null;
                    
                $meta = [
                    "title" => isset($data["name"]) ? $data["name"] : "",
                    "description" => isset($data["description"]) ? $data["description"] : "",
                    "shortDescription" => isset($data["shortDescription"]) ? $data["shortDescription"] : "",
                    "relCanonical" => (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
                ];
                if(isset($data["profilMediumImageUrl"]) && $data["profilMediumImageUrl"] !== "")
                    $meta["image"] = $data["profilMediumImageUrl"];
                
                return $meta;
            break;
            case "projects":
                $data = PHDB::findOne(Project::COLLECTION, ["_id" => $id]);
                if(!isset($data))
                    return null;
                
                return [
                    "title" => isset($data["name"]) ? $data["name"] : "",
                    "description" => isset($data["description"]) ? $data["description"] : "",
                    "shortDescription" => isset($data["shortDescription"]) ? $data["shortDescription"] : "",
                    "relCanonical" => (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
                ];
            break;
            default:
                return null;
            break;
        }
    }

    private function getParsedParams(){
        if(!isset($this->hashParams) || $this->hashParams == "")
            return;

        $params_parts = explode("=", $this->hashParams);
        if(sizeof($params_parts) < 2)
            return;

        return [
            "name" => $params_parts[0],
            "value" => $params_parts[1]
        ];
    }
}