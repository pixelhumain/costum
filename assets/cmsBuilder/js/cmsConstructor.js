var cmsConstructor = {
    /* PROPERTIES */

    sp_params: {},
    values: {},
    pathConfig: {},
    kunik: "",
    spId: "",
    textFieldChanged : "",
    textFieldError : [],
    placeToAppend : "",
    textOnEdit: "",
    configView:{},
    dragstart:false,
    ctrlzPosition: "0",
    tabKeyLeftActive : "blocks",
    /* UTILITIES */
    helpers: {
        hexDigits : new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"),
        getTypeObjOptions : function(){
            var eltOptions = {}            
            for (const [key, value] of Object.entries(typeObj)) {
                if(typeof value != "function" && typeof value.add != "undefined"){ 
                    if (value.add == true) {          
                        eltOptions[key] = (value.name).toUpperCase();                  
                    }               
                }
                typeObjOptions = $.map(eltOptions, function(key, value) {
                    return {
                        label: key,
                        value: value
                    }})
            }
            return typeObjOptions;
        },
        isValidColorName : function(color){
            var colorList = ["aliceblue","antiquewhite","aqua","aquamarine","azure","beige","bisque","black","blanchedalmond","blue","blueviolet","brown","burlywood","cadetblue","chartreuse","chocolate","coral","cornflowerblue","cornsilk","crimson","cyan","darkblue","darkcyan","darkgoldenrod","darkgray","darkgreen","darkgrey","darkkhaki","darkmagenta","darkolivegreen","darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkslategray","darkslategrey","darkturquoise","darkviolet","deeppink","deepskyblue","dimgray","dimgrey","dodgerblue","firebrick","floralwhite","forestgreen","fuchsia","gainsboro","ghostwhite","gold","goldenrod","gray","green","greenyellow","grey","honeydew","hotpink","indianred","indigo","ivory","khaki","lavender","lavenderblush","lawngreen","lemonchiffon","lightblue","lightcoral","lightcyan","lightgoldenrodyellow","lightgray","lightgreen","lightgrey","lightpink","lightsalmon","lightseagreen","lightskyblue","lightslategray","lightslategrey","lightsteelblue","lightyellow","lime","limegreen","linen","magenta","maroon","mediumaquamarine","mediumblue","mediumorchid","mediumpurple","mediumseagreen","mediumslateblue","mediumspringgreen","mediumturquoise","mediumvioletred","midnightblue","mintcream","mistyrose","moccasin","navajowhite","navy","oldlace","olive","olivedrab","orange","orangered","orchid","palegoldenrod","palegreen","paleturquoise","palevioletred","papayawhip","peachpuff","peru","pink","plum","powderblue","purple","red","rosybrown","royalblue","saddlebrown","salmon","sandybrown","seagreen","seashell","sienna","silver","skyblue","slateblue","slategray","slategrey","snow","springgreen","steelblue","tan","teal","thistle","tomato","turquoise","violet","wheat","white","whitesmoke","yellow","yellowgreen", "transparent"];
            return colorList.indexOf(color.toLowerCase()) !== -1;     

        },
        isValidColor : function(color){
            var hexColor = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/,
            hslColor = /^hsl\(\s*\d{1,3}(\.\d+)?\s*,\s*\d{1,3}%\s*,\s*\d{1,3}%\s*\)$/,
            rgbColor = /^rgb\(\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*\)$/,
            rgbaColor = /^rgba\(\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*\d{1,3}%?\s*,\s*(0\.\d+|1|0)\s*\)$/;
            return cmsConstructor.helpers.isValidColorName(color) ||  hexColor.test(color) || hslColor.test(color) || rgbColor.test(color) || rgbaColor.test(color);       

        },
        getComputedBg : function(sptextField = null){

            let bgc = "";
            if(sptextField){
                bgc = sptextField.css('background-color')

                if(sptextField.hasClass("block-container-html")!= true && bgc == "rgba(0, 0, 0, 0)") {  
                    cmsConstructor.helpers.getComputedBg(sptextField.parent());
                }

                if ( bgc != "rgba(0, 0, 0, 0)") {
                    cmsConstructor["bgTextColor"] = bgc
                }   
            }        

        },
        getComputedColor : function(sptextField = null){
            if (sptextField) {
                var spColor =  sptextField.css('color')
                if(spColor == "rgba(0, 0, 0, 0)" && sptextField.hasClass("block-container-html") != true) {  
                    cmsConstructor.helpers.getComputedColor(sptextField.parent());
                }
                cmsConstructor["textColor"] = spColor
            }
        },
        InheritedBackgroundColor : function(elt){
            delete cmsConstructor.bgTextColor;
            delete cmsConstructor.textColor;
            cmsConstructor.helpers.getComputedBg(elt)
            cmsConstructor.helpers.getComputedColor(elt)
            if (typeof cmsConstructor.bgTextColor == "undefined"|| cmsConstructor.bgTextColor == "rgb(255, 255, 255)"){
                cmsConstructor.bgTextColor = "rgb(255, 255, 255)";
            }
            if (typeof cmsConstructor.textColor == "undefined" || cmsConstructor.bgTextColor == "rgb(0, 0, 0)"){
                cmsConstructor.textColor = "rgba(0, 0, 0, 0)"
            }
            return { bgTextColor : cmsConstructor.bgTextColor, textColor : cmsConstructor.textColor}
        },

        rgb2hex : function(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            return "#" + cmsConstructor.helpers.hex(rgb[1]) + cmsConstructor.helpers.hex(rgb[2]) + cmsConstructor.helpers.hex(rgb[3]);
        },

        hex : function(x) {
            return isNaN(x) ? "00" : cmsConstructor.helpers.hexDigits[(x - x % 16) / 16] + cmsConstructor.helpers.hexDigits[x % 16];
        },

        validateScript: function (element, sp_message) {
            var strDev = "";
            if (element.indexOf('script') > -1 || element.indexOf(')') > -1 || element.indexOf('}') > -1 || element.indexOf(']') > -1) {
                $(".sp-message").html(sp_message);
                return false;
            } else {
                strDev = element;
                $(".sp-message").html("");
                return strDev;
            }
        },
        validateOtherCss: function (cssValue,message) { 
            // let validate = cssValue.replace(/[^\S+]/g,"");
            //  validate = validate.replace(/\/\*[\s\S]*?\*\//g,"");
            //  validate = validate.replace(/http:\/\//gm,"");
            //  validate = validate.replace(/((#|[.]|[*]){0,1}[\w\-:>~+,()])+\s*\{((?:[A-Za-z\- \s]+[:]\s*([#'"0-9\w .,\/()\-!%]+\;)))+\}(?:\s*)/mg,"");
            //  validate = validate.replace(/@importurl\([\"']([^)]*)[\"']\);|@import(\"|\')([^)]*)(\"|\');/g,"");
            //  validate = validate.replace(/(@.*?:?[^}{@]+(\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}))/g,"")
            
            const validation = csstreeValidator.validate(cssValue);

            if(validation.length > 0){ 
                if (message == true){
                    toastr.error(validation[0]["message"]+": line "+validation[0]["line"]+",column "+validation[0]["column"]);
                }
                return false;
            }
            else {
                return true;
            }
        },
        getBlockTitle:function($block){
            var type  = $block.data("blocktype"),
                name  = $block.data("name"),
                path  = $block.data("path")

            var title = name;

            if(type == "section" && path == "tpls.blockCms.superCms.container"){
                title = "Section"                
                if(typeof cmsConstructor.sp_params[$block.data("id")] != "undefined" && typeof cmsConstructor.sp_params[$block.data("id")].advanced != "undefined"){
                    if(typeof cmsConstructor.sp_params[$block.data("id")].advanced.persistent != "undefined" && cmsConstructor.sp_params[$block.data("id")].advanced.persistent != "")
                        title = tradCms[cmsConstructor.sp_params[$block.data("id")].advanced.persistent] 
                    else
                        title = "Section"
                }
            }
            else if(type == "column" && title.indexOf("blockCms.superCms") > 0 )
                title = "Colonne"

            if (typeof cmsConstructor.sp_params[$block.data("id")].parent == "undefined") {
                cmsConstructor.sp_params[$block.data("id")].parent = {
                    [costum.contextId] : {
                        "type" : costum.contextType,
                        "name" : costum.contextSlug
                    }
                }
            }

            if (Object.keys(cmsConstructor.sp_params[$block.data("id")].parent)[0] != costum.contextId) { 
                title = "Alias depuis "+cmsConstructor.sp_params[$block.data("id")].source.key+", "+ cmsConstructor.sp_params[$block.data("id")].page
            }

            return title;
        },
        scrollTo: function($container, $target, duration=500){
            $container.animate({
                scrollTop: $target.offset().top -  $container.offset().top +  $container.scrollTop() - 200
            }, duration)
        },
        string2Json: function (element, json) {
            var treeObject = {};

            if (typeof element === "string") {
                if (window.DOMParser) {
                    parser = new DOMParser();
                    docNode = parser.parseFromString(element, "text/xml");
                } else {
                    docNode = new ActiveXObject("Microsoft.XMLDOM");
                    docNode.async = false;
                    docNode.loadXML(element);
                }
                element = docNode.firstChild;
            }

            function treeHTML(element, object) {
                object["type"] = element.nodeName;
                var nodeList = element.childNodes;
                if (nodeList != null) {
                    if (nodeList.length) {
                        object["content"] = [];
                        for (var i = 0; i < nodeList.length; i++) {
                            if (nodeList[i].nodeType == 3) {
                                object["content"].push(nodeList[i].nodeValue);
                            } else {
                                object["content"].push({});
                                treeHTML(nodeList[i], object["content"][object["content"].length - 1]);
                            }
                        }
                    }
                }
                if (element.attributes != null) {
                    if (element.attributes.length) {
                        object["attributes"] = {};
                        for (var i = 0; i < element.attributes.length; i++) {
                            object["attributes"][element.attributes[i].nodeName] = element.attributes[i].nodeValue;
                        }
                    }
                }
            }
            treeHTML(element, treeObject);

            return (json) ? treeObject : treeObject;
        },
        isContains: function (json, value) {
            let contains = false;
            Object.keys(json).some(key => {
                contains = typeof json[key] === 'object' ? this.isContains(json[key], value) : json[key] === value;
                return contains;
            });
            return contains;
        },
        selectNode: function(node){
            var selection = window.getSelection()
            selection.removeAllRanges()
            var range = document.createRange()
            range.selectNode(node)
            selection.addRange(range)
        },

        getStyleOfSelection:function(){
            if(!window.getSelection || !window.getSelection().rangeCount)
                return {}

            var range = window.getSelection().getRangeAt(0),
                parentNode = range.startContainer.parentNode,
                textContent = range.toString();

            window.parentNode = parentNode
            if(parentNode.getAttribute("contenteditable") == null && parentNode.textContent == textContent){
                var res = {};
                if (parentNode.getAttribute("style")) {
                    var styles = parentNode.getAttribute("style").trim().split(";");
                    styles.forEach(function(style){
                        style = style.split(":")
                        if(style.length > 1)
                            res[style[0].trim()] = style[1].trim()
                    })
                }
                return res;
            }

            return {}
        },
        colorRGBtoHex: function(colorRGB, defaultValue="#000000"){
            if(/^#.*/.test(colorRGB))
                return colorRGB
            else if(/rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)/.test(colorRGB)){
                var rgb = colorRGB.substring(4, colorRGB.length-1).replace(/ /g, '').split(',');
                
                return "#" + (1 << 24 | rgb[0] << 16 | rgb[1] << 8 | rgb[2]).toString(16).slice(1);
            }else{
                return defaultValue
            }
        },
        colorHexToRGB: function(colorHex){    
            var red = parseInt(colorHex.substr(1, 2), 16);
            var green = parseInt(colorHex.substr(3, 2), 16);
            var blue = parseInt(colorHex.substr(5, 2), 16);
            var alpha = parseInt(colorHex.substr(7, 2), 16) / 255; 
            return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")"
        },

        onchangeAdvanced: function(name,kunik,value,spId){
            if(name == "otherClass") {
                    var valValid = cmsConstructor.helpers.validateScript(value, "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
                    if (valValid !== false) {
                        if(kunik.startsWith('button')){ 
                            $(".btn-"+kunik).removeClassExcept("cmsbuilder-block super-cms "+kunik+"-css btn-functionLink bs btn-"+kunik+" other-css-"+kunik+" newCss-"+kunik);
                            $(".btn-"+kunik).addClass(valValid);
                        }else{
                            $("." +kunik).removeClassExcept("super-img-"+spId+" "+kunik+"-css this-content-"+kunik+" unselectable sp-cms-container cmsbuilder-block-container text-left has-children " + kunik + " whole-" + kunik + " super-cms spCustomBtn other-css-" + kunik + " newCss-" + kunik + " sp-cms-10 sp-cms-20 sp-cms-30 sp-cms-40 sp-cms-50 sp-cms-120 sp-cms-120 sp-cms-80 sp-cms-90 sp-cms-100 super-container sp-cms-std");
                            $("." +kunik).addClass(valValid);
                        }
                    }
            }

            if(name == "otherCss") {
                    $("#myStyleId" + kunik).remove();
                    cmsConstructor.sp_params[spId].css.other = value;
                    var element = document.createElement("style");
                    element.id = "myStyleId" + kunik;
                    element.innerHTML = ".newCss-" + kunik + " {" + cmsConstructor.sp_params[spId].css.other + "}";
                    if(cmsConstructor.helpers.validateOtherCss(element.innerHTML,false)){
                        var header = document.getElementsByTagName("HEAD")[0];
                        header.appendChild(element);
                        if(kunik.startsWith('button')){
                            $(".btn-" +kunik).removeClass("newCss-" + kunik);
                            $(".btn-" +kunik).removeClass("other-css-" + kunik);
                            $(".btn-" +kunik).addClass("newCss-" + kunik);
                        }else{
                            $("." +kunik).removeClass("newCss-" + kunik);
                            $("." +kunik).removeClass("other-css-" + kunik);
                            $("." +kunik).addClass("newCss-" + kunik);
                        }
                    }  
            }


            if( name == "hideOnDesktop" || name == "hideOnTablet" || name == "hideOnMobil" ){
                var dynamicObject = {};
                dynamicObject[name] = value;
                cssHelpers.render.addClassDomByPath(dynamicObject,kunik);
            }
        },
        getAutoScroller:function(container, params={}){
            var $container = $(container);
        
            var config = Object.assign({}, {
                distance:100, timer:80, step:50
            }, params)
            var offset = $container.offset(),
                offsetHeight = offset.top + $container.height()

            var handlers = { 
                top:null, 
                bottom:null,
                clear:function(){
                    clearInterval(this.top),
                    clearInterval(this.bottom)
                }
            }

            return {
                start:function(event){
                    var isMoving = false
    
                    if((event.pageY - offset.top) <= config.distance){
                        isMoving = true
                        handlers.clear()
                        handlers.top = setInterval(function(){
                            $container.scrollTop($container.scrollTop() - config.step)
                        }, config.timer)
                    }
                    //bottom
                    if(event.pageY >= (offsetHeight - config.distance)){
                        isMoving = true
                        handlers.clear()
                        handlers.bottom = setInterval(function(){
                            $container.scrollTop($container.scrollTop() + config.step)
                        }, config.timer)
                    }
                    if(!isMoving)
                        handlers.clear()
                },
                stop:function(){
                    handlers.clear();
                }
            }
        },
        isInViewport:function(el,marge) {
            const rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom-marge <= (window.innerHeight || document.documentElement.clientHeight ) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth )
            );
        },
        getBlockType:function(kunik,id){
            var blockType = kunik.replace(id,"");
            if(typeof cmsConstructor.blocks[kunik] != "undefined"){
                blockType = kunik;
            }
            if(blockType == 'container'){
                blockType = 'section';
            }
            return blockType;
        },
        refreshBlock: function(id, dom, updateOrder=false) {
            ajaxPost(
                null,
                baseUrl+"/co2/cms/refreshblock",
                {
                    idBlock: id,
                    clienturi: window.location.href,
                },
                function(data) {
                    if(data.result){
                        $(`${dom}`).html(data["view"]);
                        cmsBuilder.block.initEvent();
                        if (updateOrder)
                            cmsBuilder.block.updateOrder();
                    } else {
                        toastr.error(tradCms.somethingWrong);
                    }
                },
                null,
                "json",
                {
                    async: false
                }
            )
        },
        replaceBlock: function(id, dom, updateOrder= false) {
            ajaxPost(
                null,
                baseUrl+"/co2/cms/refreshblock",
                {
                    idBlock: id,
                    clienturi: window.location.href,
                },
                function(data) {
                    if(data.result){
                        var prevElement = $(`${dom}`).prev();
                        var nextElement = $(`${dom}`).nextAll('script:first');
                        prevElement.remove();
                        nextElement.remove();
                        $(`${dom}`).replaceWith(data["view"]);
                        cmsBuilder.block.initEvent();
                        if (updateOrder)
                            cmsBuilder.block.updateOrder();
                    } else {
                        toastr.error(tradCms.somethingWrong);
                    }
                },
                null,
                "json",
                {
                    async: false
                }
            )
        },
        getCoInputName: function(config) {
            var valueKey = "";

            if ( typeof config == "string" )
                valueKey =  config;
            else if ( typeof config === "object" && config) {
				if (config.options && config.options.name)
					valueKey = config.options.name;
				else if (config.type)
					valueKey = config.type;
            }

            return valueKey;
        },
        videoLinkProcess : function(value) {
            var videoId = "",
                srcVideo = "";
            if (value.includes("=") == true) {
                videoId = value.split("=").pop();
            } else {
                videoId = value.split("/").pop();
            }

            if (value.includes("vimeo") == true) {
                srcVideo = "https://player.vimeo.com/video/"+videoId;
            } else if (value.includes("youtu") == true) {
                srcVideo = "https://www.youtube.com/embed/"+videoId;
            } else if (value.includes("dailymotion") == true) {
                srcVideo = "https://www.dailymotion.com/embed/video/"+videoId;
            } else if (value.includes("indymotion") == true) {
                srcVideo = "https://indymotion.fr/videos/embed/w/"+videoId;
            }
            return  srcVideo;
        },
        removeAllBaliseHtml: function(string) {
            return string.replace(/<[^>]*>/g, '');
        },
        autoTranslate : function(textTotranslate, langTotranslate, labelInput = null, callback) {
            var ring = costumizer.obj.css.loader;
            var loaderStr = '<div class="loader-translate" style ="width: 140px;margin-left: auto;margin-right: auto;">'+
                '   <div class="horizontal_bar_loader" style="background-color:'+ring.ring2.color+';height:3px;">' +
                '       <div class="horizontal_bar_loading" style="background-color:'+ring.ring1.color+';height:3px;"></div>' +
                '   </div>'+
                '</div>';
            $(".cmsbuilder-tabs-body .cmsbuilder-tab-section #general").prepend(loaderStr);
            var attribInput = [labelInput , ".cmsbuilder-tab-section[data-key='general'] select[name='languages']", ".cmsbuilder-tab-section[data-key='general'] .select-language-props"];
            $.each(attribInput, function (index, el) {
                $(el).prop('disabled', true);
            });
            if (textTotranslate != "" && langTotranslate != "") {
                dataHelper.autoTranslateDeepl(textTotranslate, langTotranslate, function(error, translatedText) {
                    if (error) {
                        callback("Une erreur s'est produite !! Veuillez réessayer.", null);
                    } else {
                        if (translatedText != "") {
                            $(".cmsbuilder-tabs-body .cmsbuilder-tab-section #general .loader-translate").remove();
                            $.each(attribInput, function (index, el) {
                                $(el).prop('disabled', false);
                            });
                            callback(null, translatedText); 
                        }
                    }
                })
            }
        },
        SaveModifLogs : function(params,blockPath,blockName) {
            // Mettre à jour le Logs
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/cms/savelog",
                {
                    costumId    : costum.contextId,
                    costumType  : costum.contextType,
                    cmsId       : params.id,
                    blockPath   : blockPath,
                    blockName   : blockName,
                    path        : params.path,
                    value       : params.value,
                    collection  : params.collection,
                    page        : page
                },
                function(data) {
                    if ( data && data.result ) {
                       // toastr.success(data.msg);
                    }
                })
        },
        saveBlockAsBlockCms : function (id){
            dyFObj.setMongoId('templates', function(){
                costumizer.template.actions.save("blockCms", id, dyFObj.currentElement.id);
            });
               /* sectionDyfaSaveBlockCms = {
                    beforeBuild : function(){	
                        dyFObj.setMongoId('cms', function(){});
                    },
                    saveUrl : baseUrl+"/co2/cms/saveasblockcms",
                    dynForm : {
                        "jsonSchema": {
                            "title": tradCms.saveBlockAsBlockCms,
                            "icon": "save",
                            "text": tradCms.saveBlockCms,
                            "properties": {
                                idCmsToDuplic : {
                                    inputType: "hidden",
                                    value: [id]
                                },
                                type : {
                                    inputType: "hidden",
                                    value: "blockCms"
                                },
                                costumId : {
                                    inputType: "hidden",
                                    value: costum.contextId
                                },
                                costumSlug : {
                                    inputType: "hidden",
                                    value: costum.contextSlug
                                },
                                costumType : {
                                    inputType: "hidden",
                                    value: costum.contextType
                                },
                                name : {
                                    inputType: "text",
                                    label: tradCms.blockName,
                                    placeholder: tradCms.blockName,
                                    rules: {
                                        required: true
                                    },
                                    value: ""
                                },
                                category : {
                                    inputType: "select",
                                    label: tradCms.category,
                                    placeholder: tradCms.category,
                                    inputType: "select",
                                    options: {
                                        "presentation": "Presentation",
                                        "directory": "Directory",
                                        "media": "Media",
                                        "formulaire" : "Formulaire",
                                        "observatory": "Observatory"
                                    },
                                    value: ""
                                },
                                image: {
                                    inputType: "uploader",
                                    label: "image",
                                    docType: "image",
                                    contentKey: "profil",
                                    itemLimit: 5,
                                    domElement: "image",
                                    filetypes: ["jpeg", "jpg", "gif", "png"],
                                    label: "Image :",
                                    showUploadBtn: false,
                                },
                                image2: {
                                    inputType: "uploader",
                                    label: "image2",
                                    docType: "image2",
                                    contentKey: "profil",
                                    itemLimit: 5,
                                    domElement: "image2",
                                    filetypes: ["jpeg", "jpg", "gif", "png"],
                                    label: "Image :",
                                    showUploadBtn: false,
                                },
                            },
                            beforeBuild: function(){
                                dyFObj.setMongoId('cms', function(){
                                    sectionDyfaSaveBlockCms.dynForm.jsonSchema.properties.newId = {
                                        inputType: "hidden",
                                        rules: {
                                            required: true
                                        },
                                        value: dyFObj.currentElement.id
                                    };
                                    uploadObj.set("cms",dyFObj.currentElement.id);
                                });

                            },
                            afterSave: function(){
                                dyFObj.commonAfterSave(null);
                            }
                        }
                    }
                };

                dyFObj.openForm(sectionDyfaSaveBlockCms, "cms");
            */
        },
        // editBlockCms : function(savedBlock) {
        //     var tplId = savedBlock.data("id");
        //             var tplCtx = {};

        
        //             var dynFormCostum = {
        //                 "beforeBuild": {
        //                     "properties": {
        //                         image: dyFInputs.image(),
        //                         path: {
        //                             label: "Chemin",
        //                             inputType: "text",
        //                             order: 2
        //                         }
        //                     }
        //                 },
        //                 "onload": {
        //                     "Title": "Modifier le cms",
        //                     "actions": {
        //                         "html": {
        //                             "nametext>label": "Nom du bloc",
        //                             "infocustom": ""
        //                         },

        //                         "hide": {
        //                             "documentationuploader": 1,
        //                             "structagstags": 1
        //                         }
        //                     }
        //                 },
        //                 afterSave: function() {
        //                     dyFObj.commonAfterSave(params, function() {
        //                         window.location.reload();
        //                     });

        //                 }
        //             };
        //             dyFObj.editElement("cms", tplId, null, dynFormCostum);
        // }
        upCmsPersistence : function(fromParent=true,blockID,persistenceValue,blockToRefresh=null){
            var data = {
                idCms  : blockID ,
                value  : persistenceValue,
                fromParent : fromParent
            }
            ajaxPost(
                null,
                baseUrl+"/co2/cms/updatepersistentcms",
                data, 
                function (response) {  
                    if (response.result){
                        if ( blockToRefresh !== null){
                            cmsConstructor.helpers.refreshBlock(blockToRefresh, ".cmsbuilder-block[data-id='"+blockToRefresh+"']",true);
                        } else {
                            cmsBuilder.block.updateOrder();
                        }
                    }                  
                });
        },
        hanldePersistence: function(id,value){
            if (value == "footer") {
                costum.htmlConstruct.footer["blockID"] = id;
                if (typeof costum.htmlConstruct.footer == "undefined") {
                    costum.htmlConstruct["footer"]["activated"] = true;
                }
                $(".sortable-container"+id).appendTo($("#all-block-container"));                 

            }else if(value == "banner"){
                $(".sortable-container"+id).prependTo($("#all-block-container"));
            }
        },
        findElement : function (value){
            $element = "";
            if ( costumizer.mode == "md") {
                $element = $(value)
            } else {
                $element = $("#responsiveScreen").contents().find(value)
            }

            return $element;
        },
        chooseBlockCms : function(blockChoosed) {
            var idChoosed = blockChoosed.data("id")
            if (typeof idChoosed == "string") {
                cmsConstructor.builder.actions.duplicateBlock(idChoosed, cmsConstructor["blockParent"] , "chooseBlockCms" , cmsConstructor["blockParent"],"templates","cms"); 
                //     // If persistent section, Update the added block into persistent
                // var sectionId = $(".whole-container"+cmsConstructor["blockParent"]).parents(".custom-block-cms").data("id")
                // if(typeof sectionId != "undefined" && typeof cmsConstructor.sp_params[sectionId].advanced != "undefined"){
                //     if(typeof cmsConstructor.sp_params[sectionId].advanced.persistent != "undefined")
                //         cmsConstructor.helpers.upCmsPersistence(sectionId,cmsConstructor.sp_params[sectionId].advanced.persistent)
                // }
            }else{
                delete cmsConstructor["blockParent"];
            }    

        },
        removeEditableSpText: function(){
            //remove editable text 
            var editableElements = $(".sp-text[contenteditable=true]");
            editableElements.each(function() {
                $(this).removeAttr('contenteditable');
            });
        },
        // Function to update the left position based on visibility
        updateLeftPosition: function ($element, $parentElement) {
            if ( $element.length > 0 ) {
                if (!cmsConstructor.helpers.isInViewport($element[0], 0)) {
                    var elementOffset = $element.offset().left;
                    var left = $element.css('left');
                    var finalValue = parseInt(left) - elementOffset;
                    $parentElement.css({
                        "cssText": "left: " + finalValue + "px !important;"
                    });
                }
            }
        },
        // Determinate if an element is under another element 
        elementIsUnder: function($elemetToDeterminate,$elementAbove){

            // Obtenez les coordonnées des deux éléments
            var rect1 = $elemetToDeterminate.getBoundingClientRect();
            var rect2 = $elementAbove.getBoundingClientRect();

            // Vérifiez si le rectangle de element1 est en dessous de element2
            if (rect1.top >= rect2.bottom || rect1.bottom <= rect2.top || rect1.right <= rect2.left || rect1.left >= rect2.right) {
                return false;
            } else {
                return true;
            }
        },
        deleteRemoveElement: function(id){
            var idparent = $("[data-id="+id+"]").parents('.custom-block-cms').data('id');
            var kunik = $("[data-id="+id+"]").parents(".custom-block-cms").data("kunik");
            if(idparent == id){
                $('.block-container-'+kunik).remove();
            }else{
                var parents = $("[data-id="+id+"]").parent();
                var kunikparents = parents.data("kunik");
                var imageSrc = $('.'+kunikparents).css('background-image');
                var backgroundColor = $('.'+kunikparents).css('background-color');
                var height = (typeof cmsConstructor.sp_params[idparent] != "undefined" && typeof cmsConstructor.sp_params[idparent].css != "undefined" && typeof cmsConstructor.sp_params[idparent].css.height != "undefined") ? cmsConstructor.sp_params[idparent].css.height : "";
                $("[data-id="+id+"]").remove();
                if( (imageSrc == "none" || backgroundColor == "none") && parents.children(".cmsbuilder-block").length == 0 ){
                    $(".empty-sp-element"+kunikparents).removeClass("d-none");
                }else{
                    if(imageSrc != "none" && parents.children(".cmsbuilder-block").length == 0 ){
                        if( height == ""){
                            parents.css("height","250px");
                        }
                    }
                }   
            }
            $(".cmsbuilder-right-content").removeClass("active")
            costumizer.actions.closeRightSubPanel()
            $("#right-panel").html("")
            $(".block-actions-wrapper").removeClass("selected")
            cmsConstructor.layer.init()
            coInterface.initHtmlPosition();
        },
        insertBlockView: function(action,owner,view,refresh,parent,newDiv){
            switch (action) {
                case "chooseBlockCms":
                    if( refresh ){
                        cmsConstructor.helpers.refreshBlock(owner, ".cmsbuilder-block[data-id='"+owner+"']");
                    } else {
                        $(`.empty-sp-elementcontainer${owner}`).remove()
                        $(`.whole-container${owner}`).append(view)
                        cmsBuilder.block.initEvent();
                    }
                    break;
                case "duplicate":
                    if( refresh ){
                        if ( parent != ""){
                            $(".cmsbuilder-block[data-id="+owner+"]").after(newDiv);
                            cmsConstructor.helpers.refreshBlock(parent, ".cmsbuilder-block[data-id='"+parent+"']");
                        } else {
                            cmsConstructor.helpers.refreshBlock(parent, ".cmsbuilder-block[data-id='"+parent+"']");
                        }
                    } else {
                        $(".cmsbuilder-block[data-id="+owner+"]").after(view);
                        cmsBuilder.block.initEvent();
                    }

                    break;
                case "paste":
                    if ($(".cmsbuilder-block[data-id="+owner+"]").data("blocktype") == "section") {
                        if(refresh){
                            cmsConstructor.helpers.refreshBlock(owner, ".cmsbuilder-block[data-id='"+owner+"']");
                        } else {
                            $(`.empty-sp-elementcontainer${owner}`).remove()
                            $(".cmsbuilder-block[data-id="+owner+"]").append(view);
                            cmsBuilder.block.initEvent();    
                        }
                    }else{
                        if(refresh){
                            cmsConstructor.helpers.refreshBlock(owner, ".cmsbuilder-block[data-id='"+owner+"']");
                        } else {
                            $(".cmsbuilder-block[data-id="+owner+"]").append(view);
                            cmsBuilder.block.initEvent();
                        }
                    }      
                break;
            }

        },
        moveBlockView: function(action,idBlock,socket){
            $el = $(".cmsbuilder-block[data-id="+idBlock+"]");
            var $cloneBlock = $el.clone(true);
            var params = {};
            if($el.hasClass("cmsbuilder-block")){
                    if((action == "move_up" && $el.prevAll().filter(".cmsbuilder-block").length != 0) || (action == "move_down" && $el.nextAll().filter(".cmsbuilder-block").length != 0)){
                        var parents = $el.parent();
                        if(action == "move_up")
                            $el.prevAll().filter(".cmsbuilder-block").first().before($cloneBlock)
                        else
                            $el.nextAll().filter(".cmsbuilder-block").first().after($cloneBlock)

                        cmsConstructor.block.events.actions.bindOnclickActionItem()

                        $el.remove()

                        coInterface.scrollTo($el.attr("id"))


                        var params = {
                            id : costum.contextId,
                            collection : costum.contextType,
                            idAndPosition : []
                        };

                        parents.children(".cmsbuilder-block").each(function(){
                            params.idAndPosition.push($(this).data("id"));
                        });
                    }
            }else{
                if((action == "move_up" && $el.prev(".custom-block-cms").length != 0) || (action == "move_down" && $el.next(".custom-block-cms").length != 0)){
                        if(action == "move_up")
                            $el.prev(".custom-block-cms").before($cloneBlock)
                        else
                            $el.next(".custom-block-cms").after($cloneBlock)

                        cmsConstructor.block.events.actions.bindOnclickActionItem()

                        $el.remove()

                        coInterface.scrollTo($el.attr("id"))

                        var params = {
                            id : costum.contextId,
                            collection : costum.contextType,
                            idAndPosition : []
                        };

                        $(".custom-block-cms").each(function(){
                            params.idAndPosition.push($(this).data("id"));
                        });
                }
            }

            if(!socket)
                return params 
        },
        giveModeValue: function(str){
            switch (str) {
                case "md":
                    return 3;
                case "sm":
                    return 2;
                case "xs":
                    return 1;
                default:
                    return 0; 
            }
        },
        deepCopy: function(obj){
            if (typeof obj !== "object" || obj === null) {
                return obj;
            }
            
            var copy = Array.isArray(obj) ? [] : {};
            
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                copy[key] = cmsConstructor.helpers.deepCopy(obj[key]);
                }
            }
            
            return copy;
        },
        compareObjects: function(newData,ancientData){
            var result = {};

            for (var key in newData) {
                if (ancientData.hasOwnProperty(key)) {
                    if (typeof newData[key] === "object" && typeof ancientData[key] === "object") {
                        var nestedResult = cmsConstructor.helpers.compareObjects(newData[key], ancientData[key]);
                        if (Object.keys(nestedResult).length > 0) {
                            result[key] = nestedResult;
                        }
                    } else if (newData[key] !== ancientData[key]) {
                        result[key] = ancientData[key];
                    } else if (typeof newData[key] === "object" && typeof ancientData[key] === "undefined") {
                        ancientData[key] = {};
                        if (Object.keys(newData[key]).length > 0) {
                            result[key] = "deleted";
                        }
                    }
                } else {
                    result[key] = "deleted";
                }
            }

            // Ajouter la structure de l'objet d'origine dans le résultat
            for (var key in newData) {
                if (typeof newData[key] === "object" && typeof ancientData[key] === "undefined" && !result.hasOwnProperty(key)) {
                    result[key] = newData[key];
                }
            }

            return result;
        },
        appendBusy: function(userId, panel=false){
            if (!panel) {
                str = `<p class="padding-20 text-center">${costum.members.lists[userId].avatar} ${costum.members.lists[userId].name} ${tradCms.elementBusy}</p>`
                $("#right-panel").html("");
                $("#right-panel").append(str);
                $(".cmsbuilder-right-content").addClass("active");
            }else{
                // bootbox.alert({
                //     message: `<p class="padding-20 text-center">${costum.members.lists[userId].avatar} ${costum.members.lists[userId].name} ${tradCms.elementBusy}</p>`,
                //     title: `On edit`
                // });
                toastr.options = {
                    "closeButton": true,
                    "progressBar": true,
                    "positionClass": "toast-top-center"
                }

                toastr.info(`<br><p class="padding-20">${costum.members.lists[userId].avatar} ${costum.members.lists[userId].name} ${tradCms.elementBusy}</p>`, '', {
                    onShown: function() {
                        $('.toast').css('opacity', 10);
                    }
                });
            }
        },
        generateUniqueId: function () {
            return Math.random().toString(36).slice(-6)
        },
        createObject: function(objet,cle, valeur){
            var parties = cle.split(".");
            var tempObjet = objet;

            for (var i = 0; i < parties.length - 1; i++) {
                var partie = parties[i];
                if (!tempObjet.hasOwnProperty(partie)) {
                    tempObjet[partie] = {};
                }   
                tempObjet = tempObjet[partie];
            }

            tempObjet[parties[parties.length - 1]] = valeur;

            return objet;
        },
        mergeObjectsStructure2: function(obj1,obj2){
            for (let key in obj1) {
                if (obj1.hasOwnProperty(key)) {
                  if (Array.isArray(obj1[key]) && Array.isArray(obj2[key])) {
                    obj2[key] = obj1[key];
                  } else if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
                    obj2[key] = cmsConstructor.helpers.mergeObjectsStructure2(obj1[key], obj2[key]);
                  } else {
                    obj2[key] = obj1[key];
                  }
                }
            }

            return obj2;
        },
        mergeObjectsStructure1: function(obj1, obj2) {
            for (let key in obj1) {
                if (obj1.hasOwnProperty(key)) {
                  if (Array.isArray(obj1[key]) && Array.isArray(obj2[key])) {
                    obj1[key] = obj2[key];
                  } else if (typeof obj1[key] === 'object' && typeof obj2[key] === 'object') {
                    obj1[key] = this.mergeObjectsStructure1(obj1[key], obj2[key]);
                  } else {
                    obj1[key] = obj2[key];
                  }
                }
            }

            return obj1;
        },
        insertValueBeforeFirstElement : function(arr, value) {
            if (arr.length === 0) {
                arr.push(value);
            } else {
                arr.splice(0, 0, value);
            }
        },
        insertObjectToLocalStorage: function(item,id=null,kunik=null,type,oldValue,actualPage=location.hash == "" ? "#welcome" : location.hash) {
            if (typeof(Storage) !== "undefined") {
                var data = JSON.parse(localStorage.getItem(item)) || {} ;
                var dataPage = typeof data[actualPage] !== "undefined" ? data[actualPage] : [] ;
                var valueToInsert = {};
                
                if (type === "block") {

                    var blockType = cmsConstructor.helpers.getBlockType(kunik,id);
                    valueToInsert = { "id" : id , "kunik" : kunik, "page": actualPage , "type":type ,"data" : oldValue , "blockPath" : cmsConstructor.sp_params[id].path , "blockType" : blockType}
                    if ( typeof cmsConstructor.sp_params[id].blockParent !== "undefined" )
                        valueToInsert["blockParent"] = cmsConstructor.sp_params[id].blockParent;
                } else if ( type === "menu" ) {
                    valueToInsert = { "page": actualPage , "type":type ,"data" : oldValue.olderData , "pathChanged" : oldValue.pathChanged }
                }

                dataPage.unshift(valueToInsert);
                data[actualPage] = dataPage;
                localStorage.setItem(item, JSON.stringify(data));
            } else {
                mylog.log("LocalStorage n'est pas pris en charge par votre navigateur.");
            }
        },
        removeUnnecessaryEntriesBeforeSave: function(obj){
            for (let key in obj) {
                if (key === "languageSelected") {
                  delete obj[key];
                } else if ( obj[key] === "") {
                    obj[key] = null;
                } else if (typeof obj[key] === 'object') {
                   this.removeUnnecessaryEntriesBeforeSave(obj[key]);
                }
            }
            return obj;
        },
        scrollToAndRefresh : function(idBlock){
            var timeOut = 0;
            var blockTarget = idBlock;
            if ( !(cmsConstructor.sp_params[blockTarget].path.includes("superCms")) ){
                blockTarget = cmsConstructor.sp_params[blockTarget].blockParent;
            }

            if (!cmsConstructor.helpers.isInViewport($(".cmsbuilder-block[data-id='" + blockTarget + "']")[0], 0)) {
                cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(".cmsbuilder-block[data-id='" + blockTarget + "']").first(), function() {
                    setTimeout(function() {
                        if (cmsConstructor.sp_params[blockTarget].path === "tpls.blockCms.superCms.container" && typeof cmsConstructor.sp_params[blockTarget].blockParent === "undefined") {
                            cmsConstructor.helpers.refreshBlock(blockTarget, ".cmsbuilder-block[data-id='" + blockTarget + "']");
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "refreshBlock", 
                                data:  { id: blockTarget }
                            })
                        } else {
                            cmsConstructor.helpers.replaceBlock(blockTarget, ".cmsbuilder-block[data-id='" + blockTarget + "']");
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: blockTarget }
                            })
                        }
                    }, 200);
                });
            } else {
                if (cmsConstructor.sp_params[blockTarget].path === "tpls.blockCms.superCms.container" && typeof cmsConstructor.sp_params[blockTarget].blockParent === "undefined") {
                    cmsConstructor.helpers.refreshBlock(blockTarget, ".cmsbuilder-block[data-id='" + blockTarget + "']");
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "refreshBlock", 
                        data:  { id: blockTarget }
                    })
                } else {
                    cmsConstructor.helpers.replaceBlock(blockTarget, ".cmsbuilder-block[data-id='" + blockTarget + "']");
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "replaceBlock", 
                        data:  { id: blockTarget }
                    })
                }
            }
        },
        deleteCtrlBlock: function(item, allBlock) {
            var actualPage = location.hash == "" ? "#welcome" : location.hash;
            var data = JSON.parse(localStorage.getItem(item)) || {};
            var dataPage = typeof data[actualPage] !== "undefined" ? data[actualPage] : [];
        
            allBlock.forEach(element => {
                for (let i = dataPage.length - 1; i >= 0; i--) {
                    if (dataPage[i].id === element) {
                        dataPage.splice(i, 1);
                    }
                }
            });
        
            data[actualPage] = dataPage;
            localStorage.setItem(item, JSON.stringify(data));
        },
        ctrlChangeAction: function(obj,kunik,spId){
            var action = false;
            var blockType = cmsConstructor.helpers.getBlockType(kunik,spId);

            if (!cmsConstructor.helpers.isInViewport($(".cmsbuilder-block[data-id='" + spId + "']")[0], 0)) {
                cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(".cmsbuilder-block[data-id='" + spId + "']").first())
            }

            for (let key in obj) {
                if ( key == "title" || key == "text"){
                    const keys = Object.keys(obj[key]);
                    const firstKey = keys[0];
                    const firstValue = obj[key][firstKey];
                    appendTextLangBased(".sp-text-"+spId,firstKey,obj[key],spId);
                    action = true 
                } else if ( key == "image" && blockType == "image" ){
                    const keys = Object.keys(obj[key]);
                    const firstKey = keys[0];
                    const firstValue = obj[key][firstKey];
                    $(".this-content-"+kunik).attr("src", firstValue);
                    cmsConstructor.sp_params[spId].image.languageSelected = firstKey;
                    action = true 
                } else if ( key == "css" ){
                    var css = cmsConstructor.sp_params[spId].css;
                    $.each(obj[key], function(cssKey,cssVal){
                        if (typeof cssHelpers.json[cssKey] != "undefined" && cssHelpers.rejectFromCss.includes(cssKey) == false) {
                            if (typeof cssVal == "object") {
                                $.each(cssVal, function(keyChange, valueChange) {
                                    cssHelpers.render.addStyleUI(kunik+"."+keyChange,valueChange)
                                })
                            } else {
                                cssHelpers.render.addStyleUI(kunik+"."+cssKey,cssVal)
                            }
            
                            if ( costumizer.mode != "md"){
                                $element = $("#responsiveScreen").contents().find("."+kunik);
                            } else {
                                $element = $("."+kunik);
                            }
            
                            if ( blockType == "section" ) {
                                if ( cssKey == "backgroundType"){
                                    if ( cssVal == "backgroundColor"){
                                        $element.css('background-image' , 'none');
                                        if ( typeof css.backgroundColor != "undefined" ){
                                            $element.css('background-color' , css.backgroundColor);
                                        } 
                                    }else {
                                        if ( typeof css.backgroundImage != "undefined"){
                                            cssHelpers.json["backgroundUpload"].input.options.forEach(element => {
                                                if ( typeof css[element] != "undefined" && cssHelpers.rejectFromCss.includes(cssVal) == false){
                                                    $element.css(cssHelpers.json[element].property , css[element]);
                                                }
                                            });
                                        }
                                    }
                                    
                                } 
            
            
                                if ( cssKey == "backgroundColor" || cssKey == "backgroundImage" ){
                                    if (!$('.empty-sp-element'+kunik).hasClass("d-none")){
                                            $('.empty-sp-element'+kunik).addClass("d-none");
                                            if (typeof cmsConstructor.sp_params[spId].blockParent == "undefined" && (typeof height != "undefined" || $element.css('height') != "max-content")){
                                                $element.css('height' , '250px');
                                            }
                                    }
                                
                                } 


                                action = true;
                            }
                        }
                    });
                } else if ( key == "advanced"){
                    $.each(obj[key], function(advancedKey,advancedVal){
                        cmsConstructor.helpers.onchangeAdvanced(advancedKey,cmsConstructor.kunik,advancedVal,cmsConstructor.spId);
                        // costumSocket.emitEvent(wsCO, "update_component", {
                        //     functionName: "advancedChange", 
                        //     data: {
                        //         kunik: kunik,
                        //         spId: spId,
                        //         value: advancedVal,
                        //         name: advancedKey
                        //     }
                        // })
                    })
                    action = true 
                }  

                if( cmsConstructor.blocks != null && typeof cmsConstructor.blocks[blockType] != "undefined" && cmsConstructor.blocks[blockType] != null &&  typeof cmsConstructor.blocks[blockType].onchange == "function" ) 
                    action = cmsConstructor.blocks[blockType].onchange(path, valueToSet, key, payload, obj[key]);
            }
             
            // if ( actions){
            //     if ( blockType === "section" && typeof cmsConstructor.sp_params[spId].blockParent === "undefined" ){
            //         costumSocket.emitEvent(wsCO, "update_component", {
            //             functionName: "refreshBlock", 
            //             data:{ id: spId }
            //         })
            //     } else {
            //         costumSocket.emitEvent(wsCO, "update_component", {
            //             functionName: "replaceBlock", 
            //             data:{ id: spId }
            //         })
            //     }
            // }
            
            return action;
        },
        uploadFilesImage: async function(blob) {
            var fileExtension = '.png';
            if (blob.type === 'image/jpeg') {
                fileExtension = '.jpg';
            } else if (blob.type === 'image/gif') {
                fileExtension = '.gif';
            }

            const fileName = `clipboard_image_${Date.now()}${fileExtension}`;

            // Create a File object from the Blob with dynamically obtained filename
            const file = new File([blob], fileName, { type: blob.type });

            var fd = new FormData();
            fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
            fd.append('qqfilename',file.name);
            fd.append('qqtotalfilesize',file.size);
            fd.append('qqfile',file);
            fd.append('costumSlug', costumizer.obj.contextSlug);
            fd.append('costumEditMode', costum.editMode);

            return $.ajax({
                type: "POST",
                url: baseUrl + "/co2/document/upload-save/dir/communecter/folder/" + costum.contextType + "/ownerId/" + costum.contextId + "/input/qqfile/docType/image/contentKey/slider/subKey/coInput",
                data: fd,
                contentType: false,
                processData: false
            });
        },
        imageBase64ToBlob: async function(base64, fileName, mimeType) {
            const byteCharacters = atob(base64);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
                byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray], { type: mimeType });
            return new File([blob], fileName, { type: mimeType });
        },
        generateImageIA: function (btnClicked, dom) {
            bootbox.prompt({
	            title: tradCms.imgToGenerate, 
	            value : "", 
                className : "modal-to-generate-image",
                buttons: {
                    confirm: {
                        label: tradCms.generate,
                    },
                    cancel: {
                        label: trad.cancel
                    }
                },
	            callback : function(prompt){
                    if ($.inArray(prompt, [null, ""]) < 0) {
                        var kunik = dom.data("kunik");
                        var id = dom.data("id");
                        btnClicked.find("i").removeClass("fa-android").addClass("fa-spin fa-spinner");
                        $(`.content-loader-on-generateImage.${kunik}-loader`).addClass("isLoaderShow");
                        coInterface.showLoader(`.content-loader-on-generateImage.${kunik}-loader`);
                        $(".cmsbuilder-container").css({"pointer-events": "none"});
                        dataHelper.generateImageOpenai(prompt, async function(error, response) {
                            if (notNull(response)) {
                                const date = new Date();
                                const dateString = `${date.getFullYear()}${String(date.getMonth() + 1).padStart(2, '0')}${String(date.getDate()).padStart(2, '0')}_${String(date.getHours()).padStart(2, '0')}${String(date.getMinutes()).padStart(2, '0')}${String(date.getSeconds()).padStart(2, '0')}`;
                                const newName = `clipboardImage${dateString}`;
                                cmsConstructor.helpers.imageBase64ToBlob(response, newName, "image/png").then(newFile => {
                                    cmsConstructor.helpers.uploadFilesImage(newFile).then((result) => {
                                        btnClicked.find("i").removeClass("fa-spin fa-spinner").addClass("fa-android");
                                        $(".cmsbuilder-container").css({"pointer-events": ""});
                                        $(`.content-loader-on-generateImage.${kunik}-loader`).removeClass("isLoaderShow").html("");
                                        var lang = typeof cmsConstructor.sp_params[id].image.languageSelected !== "undefined" ? cmsConstructor.sp_params[id].image.languageSelected : costum.langCostumActive;
                                        var blockType = cmsConstructor.helpers.getBlockType(kunik,id)
                                        cmsConstructor.editor.actions.updateBlock(blockType, "image."+lang, "valueToSet", "image", {}, result.docPath, "", "updateImage");
                                    });
                                }).catch(error => {
                                    mylog.log('Erreur:', error);
                                });
                            } else {    
                                btnClicked.find("i").removeClass("fa-spin fa-spinner").addClass("fa-android");
                                $(".cmsbuilder-container").css({"pointer-events": ""});
                                $(`.content-loader-on-generateImage.${kunik}-loader`).removeClass("isLoaderShow").html("");
                                toastr.error(tradCms.errorKeyAPIorConnexion);
                            }
                        });
                    }
	            }
	        });
        }
    },

    /* ENTRY SPACE */
    init: function () {
        if(costum && costum.editMode){
            cmsConstructor.alreadyInitialized = true;
            var topCss = ($("#headerBand").is(":visible")) ? $("#headerBand").outerHeight() : 0;
            $('.cmsbuilder-container').append(`<style>.top-${topCss}{top: ${topCss}px !important;}</style>`)
            $(".main-container #mainNav").addClass(`top-${topCss}`)
            $(".sp-text").attr("placeholder",trad.writeatexthere)
        }

        if(window.costumizer && costum && costum.editMode){
          //  this.initViews()
            this.initEvents()
            this.builder.init()
            this.layer.init()
            this.page.init();
        }
    },
    initViews: function () {
        
    },
    initEvents: function () {
        cmsConstructor.block.events.init();

        $(".cmsbuilder-center-content").off("scroll").on('scroll', function() {
            $('.sp-is-loading').each(function() {
              if ($(this).isInViewport()) {
                cmsBuilder.block.loadIntoPage($(this).data("id"),$(this).data('page'),$(this).data("path"),$(this).data("kunik"))
              }
            });
        });

        $(".cmsbuilder-center-content").off("mouseleave").on("mouseleave", function(){
            $(".block-actions-wrapper:not(.selected)").remove()
        })

        $(".main-container").off().on("click", ".editFooter" , function (e) {
            let id = $(this).data("id") 
            let ctxPage = page;           
            if ($(this).hasClass("editing-footer")) {
                cmsBuilder.config.page = ctxPage;
                // page = ctxPage;
                $(this).removeClass("editing-footer")
                $(".focus-on-footer").addClass("hidden")
                $(this).text(trad.edit+" footer")
                $(".better-footer").remove()
                let required = {
                    displayFooter : true
                }
                ajaxPost(
                    null, 
                    baseUrl+"/costum/blockcms/loadfootercms",
                    required,
                    function(data){
                        $(".footer-cms").html(data.html) 
                        var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
                        if($(".footer-cms").length > 0){
                            $(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
                        }
                    // cmsConstructor.initEvents()
                    }
                    );
            }else{
                $(this).addClass("editing-footer")
                $(this).text(trad.done)
                  var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms" data-id="undefined">'+
            '<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
            '</div>';
            $(".sp-footer").hide()            
            $(".focus-on-footer").css("height",$("#all-block-container").height()+"px")
            $(".focus-on-footer").removeClass("hidden")
            $("#all-block-container").append(strNewBlc)
             var required = {
                "orderBlock" : "10000",
                "idblock" : id,
                "contextId" : costum.contextId,
                "contextType" : costum.contextType,
                "contextSlug" : costum.contextSlug,
                "page" : "allPages",
                "clienturi" : location.href,
                "path" : "",
            };
            ajaxPost(
                null, 
                baseUrl+"/costum/blockcms/loadbloccms",
                required,
                function(data){
                    let footerSection = `
                    <div 
                    id="${id}-10000"
                    class="block-footer better-footer cmsbuilder-block cmsbuilder-block-droppable sortable-container${id} block-container-container${id} custom-block-cms col-xs-12 no-padding col-lg-12 col-md-12 col-sm-12 col-xs-12 block-container-container${id}" data-blocktype="section" data-path="tpls.blockCms.superCms.container" data-id="${id}" data-kunik="container${id}" data-name="Section">
                        <div class="block-container-html">
                        ${data.html}
                        </div>
                    </div>                    `
                    $(".sample-cms").replaceWith(footerSection) 
                    $(".sp-text").attr("placeholder",trad.writeatexthere)

                    var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
                    if($(".block-footer").length > 0){
                        $(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
                        // cmsConstructor.init()
                        cmsBuilder.config.page = "allPages";
                        // page = "allPages";
                    }
                    },
                {async:true}
            );
            }
        })
    },
    blocks : {
        section : {
            name: "section",
            label: tradCms.oneSection,
            image: "section.png",
            path:"tpls.blockCms.superCms.container",
            configTabs : {
                general : {
                    inputsConfig : [
                        "backgroundType",
                        "height",
                        "width",
                        "justifyContent",
                        "alignContent",
                    ], 
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "inputSimple",
                            options: {
                                inputType : "text",
                                name : "backgroundImage",
                                payload : {
                                    path : "css.backgroundImage",
                                }
                            }
                        },
                    ]
                },
                animation: {
                    key: "animation",
                    keyPath: "animation",
                    icon: "film",
                    label: "Animation",
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
                hover: {
                    inputsConfig : [
                        "addCommonConfig"
                    ],
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "select",
                            options: {
                                name : "balise",
                                label : tradCms.selectSectionBalise,
                                options : ["div", "section"]
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "persistent",
                                class : "new-feature",
                                label : tradCms.useas,
                                options : $.map({"banner" : tradCms.banner,"footer" : tradCms.footer}, function(key, value) {
                                  return {
                                    label: key.toUpperCase(),
                                    value: value
                                }})
                            }
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeUrl",
                                label : tradCms.link,
                                tabs: [
                                    {
                                        value:"internalLink",  
                                        label: tradCms.internalLink,
                                        inputs: [
                                            {
                                                type: "inputWithSelect",
                                                options: {
                                                    name: "link",
                                                    viewPreview : true,
                                                    configForSelect2: {
                                                        maximumSelectionSize: 1,
                                                        tags: Object.keys(costum.app)
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value:"externalLink"  , 
                                        label: tradCms.externalLink,   
                                        inputs: [
                                            {
                                                type : "inputSimple",
                                                options: {
                                                    name : "link",
                                                    viewPreview : true
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "targetBlank",
                                label: tradCms.openInNewTab,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "downloadFile",
                                label: tradCms.downloadFile,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                        {
                            type: "inputWithSelect",
                            options: {
                                name: "anchorTarget",
                                label: "Anchor Target",
                                configForSelect2: {
                                    maximumSelectionSize: 1,
                                    tags: Object.keys(costum.app)
                                }
                            }
                        },
                    ],
                    processValue : {
                        anchorTarget : function (value){
                            if(value && value.charAt(0) != "#"){
                                value = `#${value}`;
                            }
                            return value ;
                        }
                    }
                } 
            },
            beforeLoad : function (){

                // if (typeof costum.htmlConstruct.footer.blockID != "undefined") {
                //     if (costum.htmlConstruct.footer.blockID != cmsConstructor.spId)
                //         delete cmsConstructor.blocks.section.configTabs.advanced.inputsConfig[1].options.options[1]
                //     else
                //         if(jQuery.inArray("footer", cmsConstructor.blocks.section.configTabs.advanced.inputsConfig[1].options.options) == -1)
                //             cmsConstructor.blocks.section.configTabs.advanced.inputsConfig[1].options.options[1] = "footer"
                // }
                // var persistenceOptions = {
                //     "banner" : tradCms.banner,
                //     "footer" : tradCms.footer
                // }
                // mylog.log("cmsConstructor.blocks.section.configTabs.advanced.inputsConfig",cmsConstructor.blocks.section.configTabs.advanced.inputsConfig)
                // cmsConstructor.blocks.section.configTabs.advanced.inputsConfig[2].options.options = $.map(persistenceOptions, function(key, value) {
                //   return {
                //     label: key.toUpperCase(),
                //     value: value
                // }})
            },

            onChange : function(path,valueToSet,name,payload,value){
                if ( name == "persistent"){

                    cmsConstructor.helpers.hanldePersistence(cmsConstructor.spId,value);
                    $target = $(`.cmsbuilder-block[data-kunik="${cmsConstructor.kunik}"]`).first();              
                    cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $target);
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "handlePersistent", 
                        data: { spId : cmsConstructor.spId , value : value }
                    })

                    cmsConstructor.helpers.upCmsPersistence(true,cmsConstructor.spId,value,cmsConstructor.spId)
                    $(".cmsbuilder-right-content").removeClass("active")
                    $("#right-panel").html("")
                    $(".block-actions-wrapper").removeClass("selected")
                    delete costum.members.focused[userId]
                    return true;
                }
                
                return false ;
            }
        },
        column : {
            label: tradCms.column,
            image: "column.png",
            name:"Colonne",
           // subtype:"supercms",
            path:"tpls.blockCms.superCms.container",
            configTabs : {
                general : {
                },
                style: {
                    common:true,
                    columnWidth : true
                },
                hover: {
                    common:true,
                    columnWidth : true
                },
                advanced : {
                    common:true,
                    targetAnchor:true
                }
            }
        },
        title:{
            label: tradCms.title,
            image : "title.png",
            name : "Titre",
            path : "tpls.blockCms.superCms.elements.title",
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "select",
                            options: {
                                name : "balise",
                                label : tradCms.selectTitleBalise,
                                options : ["h1", "h2", "h3", "h4", "h5","h6"]
                            }
                        },
                        "backgroundColor",
                        "width"
                        // "fontSize"
                    ]
                },
                style: {
                    inputsConfig : [
                        // "fontStyle",
                        // "textTransform",
                        // "fontWeight",
                        // "textAlign",
                        "textShadow",
                        "addCommonConfig"
                    ],
                },
                hover: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange : function(path,valueToSet,name,payload,value){
                // cmsConstructor.helpers.onchangeAdvanced(name,cmsConstructor.kunik,value,cmsConstructor.spId);
                if(name=="balise"){
                    baliseToReplace=(typeof cmsConstructor.sp_params[cmsConstructor.spId].balise != "undefined" && notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].balise)) ? cmsConstructor.sp_params[cmsConstructor.spId].balise : "h1";
                    newH=document.querySelector("."+cmsConstructor.kunik).outerHTML.replaceAll(baliseToReplace, (value != "") ? value : "h1");
                    $("."+cmsConstructor.kunik).replaceWith(newH);
                    cmsConstructor.block.events.actions.bindOnclickActionItem();
                    cmsConstructor.block.events.actions.bindOnMouseover();

                    return true;
                }
                return false;
            },
            afterSave: function(path,valueToSet,name,payload,value,id,tplCtxPath){
                if ( tplCtxPath !== "css" || ( typeof cssHelpers.json[name] !== "undefined" && cssHelpers.rejectFromCss.includes(name) == false ) ){
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "replaceBlock", 
                        data:{ id: id }
                    })
                }
            }
        },
        text : {
            label: tradCms.text,
            image: "text.png",
            name:"Texte",
            path:"tpls.blockCms.superCms.elements.supertext",
            configTabs : {
                general : {
                    inputsConfig : []
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            beforeLoad : function (){
                if (costumizer.mode != "md") {
                    $(".cmsbuilder-tabs-header").find("[data-key='general']").addClass("hidden")
                    $(".cmsbuilder-tabs-header").find("[data-key='style']").click()
                }else{                  
                    $(".cmsbuilder-tabs-header").find("[data-key='general']").removeClass("hidden")
                }
            }
        },
        image :{
            label: tradCms.image,
            image: "image.png",
            name:"Image",
            path:"tpls.blockCms.superCms.elements.image",
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type : "inputFileImage",
                            options : {
                                name : "image",
                                label : tradCms.uploadeImage,
                                collection : "cms",
                                translate : {auto: false}
                            }
                        },
                        "objectFit",
                        "height",
                        "width"
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                hover: {
                    inputsConfig : [
                        "width",
                        {
                            type: "inputNumberRange",
                            options: {
                                name: "height",
                                label : tradCms.height,
                                property: "height",
                                defaultZero: "auto",
                                units: ["px", "%", "vh"],
                                views: ["desktop", "tablet", "mobile"],
                                filterValue: cssHelpers.form.rules.checkLengthProperties, 
                            }
                        },
                        "margin",
                        "padding",
                        "boxShadow",
                        "border",
                        "borderRadius"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeUrl",
                                label : tradCms.link,
                                tabs: [
                                    {
                                        value:"internalLink",  
                                        label: tradCms.internalLink,
                                        inputs: [
                                            {
                                                type: "inputWithSelect",
                                                options: {
                                                    name: "link",
                                                    viewPreview : true,
                                                    configForSelect2: {
                                                        maximumSelectionSize: 1,
                                                        tags: Object.keys(costum.app)
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value:"externalLink"  , 
                                        label: tradCms.externalLink,   
                                        inputs: [
                                            {
                                                type : "inputSimple",
                                                options: {
                                                    name : "link",
                                                    viewPreview : true
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "targetBlank",
                                label: tradCms.openInNewTab,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "downloadFile",
                                label: tradCms.downloadFile,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                    ]
                }
            },
            onChange : function(path,valueToSet,name,payload,value){
                if (name == "image"){
                    $(".this-content-"+cmsConstructor.kunik).attr("src", value["pathImage"]);
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "changeImage", 
                        data:{ kunik: cmsConstructor.kunik , value:  value["pathImage"]}
                    })
                    return true;
                }
                return false;
            }
        },
        button :{
            label: tradCms.button,
            image: "button.png",
            name:"Button",
            path:"tpls.blockCms.superCms.elements.button",
            configTabs : {
                general : {},
                style: {
                    inputsConfig : [
                        // "fontSize",
                        "addCommonConfig"
                        ]
                },
                hover: {
                    inputsConfig : [
                        "background",
                        "color",
                        "fontSize",
                        "addCommonConfig"
                        ],
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "select",
                            options : {
                                name : "restricted",
                                label : tradCms.accessRestriction,
                                options : [
                                    {"label": tradCms.visibleToAdminOnly ,"value" : "admin"},
                                    {"label": tradCms.visibleMembers ,"value" : "members"},
                                    {"label": tradCms.visibleRoles ,"value" : "role"}
                                ]
                            }
                        }
                        ]
                }
            },
            beforeLoad : function (){
                var btnOptions = [
                    // {
                    //     type : "inputSimple",
                    //     options : {
                    //         name : "text",
                    //         label : tradCms.text,
                    //         translate : true
                    //     }
                    // },
                    {
                        type : "select",
                        options : {
                            name : "btnOpenModal",
                            label : "Action du button",
                            options : [
                                {label : tradCms.AccessAlink, value : "default"} ,
                                {label : tradCms.OpenFormCreatingElement, value : "createElement"},
                                {label : tradCms.DisplayContactForm, value : "contact"},
                                {label : tradCms.DisplayRegistrationForm, value : "register"},
                                {label : tradCms.DisplayLoginForm, value : "login"}
                                ]
                        }
                    },
                    {
                        type : "inputSwitcher",
                        options : {
                            name : "typeUrl",
                            label : tradCms.link,
                            class : "urlOptions "+(cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "default" ? "":"hidden"),
                            tabs: [
                            {
                                value:"internalLink",  
                                label: tradCms.internalLink,
                                inputs: [
                                {
                                    type: "inputWithSelect",
                                    options: {
                                        viewPreview : true,
                                        name: "link",
                                        configForSelect2: {
                                            maximumSelectionSize: 1,
                                            tags: Object.keys(costum.app)
                                        }
                                    }
                                }
                                ]
                            },
                            {
                                value:"externalLink"  , 
                                label: tradCms.externalLink,   
                                inputs: [
                                {
                                    type : "inputSimple",
                                    options: {
                                        name : "link",
                                        viewPreview : true
                                    }
                                }
                                ]
                            }
                            ]
                        }
                    },
                    {
                        type : "inputSimple",
                        options : {
                            type : "checkbox",
                            name : "targetBlank",
                            label : tradCms.openInNewTab,
                            class : "switch targetMode "+((cmsConstructor.sp_params[cmsConstructor.spId].typeUrl == "externalLink") && (cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "default") ? "":"hidden")
                        }
                    },
                    {
                        type : "select",
                        options : {
                            name : "elementsType",
                            class : cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "createElement" ? "addEltType":"addEltType hidden",
                            label : tradCms.elToCreate,
                            options : cmsConstructor.helpers.getTypeObjOptions()
                        }
                    },
                  /*  {
                        type : "inputWithSelect",
                        options : {
                            name : "addExistingPage",
                            label : tradCms.pageSlugToOpen,
                            class : cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "addExistingPage" ? "addExistingPage" : "addExistingPage hidden",
                            configForSelect2: {
                                maximumSelectionSize: 1,
                                tags: Object.keys(costum.app)
                            }
                        }
                    },*/
                    {
                        type: "inputWithSelect",
                        options: {
                            name: "urlToRedirect",
                            class : cmsConstructor.sp_params[cmsConstructor.spId].btnOpenModal == "register" ? "urlToRedirect" : "urlToRedirect hidden",
                            label : tradCms.urlOfTheRedirection,
                            configForSelect2: {
                                maximumSelectionSize: 1,
                                tags: Object.keys(costum.app)
                            }
                        }
                    },
                    "width",
                    "height",
                    "background"
                    ]
                cmsConstructor.blocks.button.configTabs.general["inputsConfig"] = btnOptions; 
            },
            onChange: function(path,valueToSet,name,payload,value){
                // if(name == "text") {
                //     $("."+cmsConstructor.kunik).text(value["text"]);
                // }

                var status = false;
                if (value == "externalLink"){
                    $(".targetMode").removeClass("hidden")
                    status = true;
                } else if (name != "targetBlank") {
                    $(".targetMode").addClass("hidden")
                    status = true;
                }
                    

                if (name == "btnOpenModal") {
                    if (value == "createElement") 
                        $(".addEltType").removeClass("hidden")
                    else
                        $(".addEltType").addClass("hidden")  

                    if (value == "register") 
                        $(".urlToRedirect").removeClass("hidden")
                    else
                        $(".urlToRedirect").addClass("hidden")

                    if (value == "default") {
                        $(".urlOptions").removeClass("hidden")
                        if (cmsConstructor.sp_params[cmsConstructor.spId].typeUrl == "externalLink") {                            
                            $(".targetMode").removeClass("hidden")
                        }
                    }
                    else
                        $(".urlOptions").addClass("hidden")

                    status = true;
                }

                return status;
            },
            afterSave: function(path,valueToSet,name,payload,value,id,tplCtxPath){
                if ( tplCtxPath !== "css" || ( typeof cssHelpers.json[name] !== "undefined" && cssHelpers.rejectFromCss.includes(name) == false ) ){
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "replaceBlock", 
                        data:{ id: id }
                    })
                }
            }
        },
        video : {
            label: tradCms.video,
            image: "video.png",
            name:"Video",
            path:"tpls.blockCms.superCms.elements.video",
            configTabs : {
                general : {
                    inputsConfig :[
                        {
                            type : "inputSimple",
                            options : {
                                type : "text",
                                name : "link",
                                label : tradCms.link,
                                translate : {auto: false},
                            },
                        },
                        "width",
                        "height",
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "autoplay",
                                label : tradCms.autoplay,
                                class : "switch"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                type : "checkbox",
                                name : "repeat",
                                label : tradCms.repeat,
                                class : "switch"
                            },
                        }
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                }
            },
            onChange: function(path,valueToSet,name,payload,value){
                if (name == "link" && typeof value["text"] != "undefined"){
                    $("."+cmsConstructor.kunik).attr("src",cmsConstructor.helpers.videoLinkProcess(value["text"]));
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "videoLinkChange", 
                        data: {
                            kunik: cmsConstructor.kunik,
                            value: value["text"]
                        }
                    })
                    return true;
                }
                return false;
            }
        },
        icon : {
            label: tradCms.icon,
            image: "icon.png",
            name:"icon",
            path:"tpls.blockCms.superCms.elements.icon",
            configTabs : {
                    general : {
                        inputsConfig : [
                            "color", 
                            "background",
                            "fontSize", 
                            {
                             type: "inputIcon",
                             options: {
                                name: "iconStyle",
                                label: tradCms.icon,
                             }
                            }
                        ]
                    },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                hover: {
                    inputsConfig : [
                        "background",
                        "color",
                        "addCommonConfig"
                    ]
                },
                advanced :{
                    inputsConfig : [
                        "addCommonConfig",
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeUrl",
                                label : tradCms.link,
                                tabs: [
                                    {
                                        value:"internalLink",  
                                        label: tradCms.internalLink,
                                        inputs: [
                                            {
                                                type: "inputWithSelect",
                                                options: {
                                                    name: "link",
                                                    viewPreview : true,
                                                    viewPreview : true,
                                                    configForSelect2: {
                                                        maximumSelectionSize: 1,
                                                        tags: Object.keys(costum.app)
                                                    }
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        value:"externalLink"  , 
                                        label: tradCms.externalLink,   
                                        inputs: [
                                            {
                                                type : "inputSimple",
                                                options: {
                                                    name : "link",
                                                    viewPreview : true
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "targetBlank",
                                label: tradCms.openInNewTab,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name: "downloadFile",
                                label: tradCms.downloadFile,
                                inputType: "checkbox",
                                class: "switch"
                            }
                        },
                    ]
                }
            },
            onChange: function(path,valueToSet,name,payload,value){
                    if (name == "iconStyle"){
                        $('.' + cmsConstructor.kunik).removeClass(function (index, css) {
                        return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
                        });
                        
                        $('.' + cmsConstructor.kunik).addClass("fa fa-" + value)
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "changeIcon", 
                            data: { kunik: cmsConstructor.kunik , value : value }
                        })
                        
                        return true
                    }
                    return false
            },
        },
        lineSeparator : {
            label: tradCms.separator,
            image: "separator.png",
            name:"Separator",
            path:"tpls.blockCms.superCms.elements.lineSeparator",
            configTabs : {
                general : {
                        inputsConfig : [
                            {
                                type : "inputSimple",
                                options : {
                                    type : "checkbox",
                                    name : "positionTop",
                                    label : tradCms.lineSeparatorPositionTop,
                                    class : "switch"
                                }
                            },
                            "backgroundColor",
                            "height",
                            "width",
                            {
                                type : "section",
                                options : {
                                    name : "icon",
                                    label: tradCms.icon,
                                    showInDefault : true,
                                    inputs : [
                                        {
                                            type : "inputSimple",
                                            options : {
                                                type : "checkbox",
                                                name : "display",
                                                label : tradCms.icon+" "+tradCms.display,
                                                class : "switch",
                                                payload : {
                                                    path : "displayPath",
                                                }
                                            }
                                        },
                                        {
                                            type: "inputIcon",
                                            options: {
                                               name: "iconStyle",
                                               label: tradCms.icon,
                                            },
                                        },
                                        {
                                            type: "fontSize",
                                            options: {
                                               name: "fontSize",
                                               label: tradCms.size,
                                            },
                                        },
                                        {
                                            type: "color",
                                            options: {
                                               name: "color",
                                               label: tradCms.color,
                                            },
                                        },
                                        {
                                            type: "background",
                                            options: {
                                               name: "background",
                                               label: tradCms.background,
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "radius",
                                                label: tradCms.radius,
                                                minRange: 0,
                                                maxRange: 100,
                                                units: ["px"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,                            
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "marginTop",
                                                label: tradCms.vertical,
                                                minRange: -100,
                                                maxRange: 100,
                                                units: ["px"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties
                                            }
                                        },
                                        {
                                            type: "inputNumberRange",
                                            options: {
                                                name: "marginLeft",
                                                label: tradCms.horizontal,
                                                minRange: -100,
                                                maxRange: 100,
                                                units: ["px","%"],
                                                filterValue: cssHelpers.form.rules.checkLengthProperties,                                    
                                            }
                                        },
    
                                    ],
                                }
                            },

                        ],
                    },
                style: {
                        inputsConfig :["addCommonConfig"],
                        
                    },
                advanced :{
                        inputsConfig :["addCommonConfig"]
                    }
            },
            onChange: function(path,valueToSet,name,payload,value){

                var status = false;
                if (name == "positionTop") {
                    $("#wrapperforLineSeparator"+cmsConstructor.kunik).toggleClass("bottom-lineSeparator"+cmsConstructor.kunik);  
                    $("#wrapperforLineSeparator"+cmsConstructor.kunik).toggleClass("top-lineSeparator"+cmsConstructor.kunik); 
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "changePositionLineSeparator", 
                        data: { kunik: cmsConstructor.kunik }
                    })

                    status = true;
                }

                if (name == "iconStyle"){
                    $('.' + cmsConstructor.kunik +"-icon").removeClass(function (index, css) {
                        return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
                    });
                    
                    $('.' + cmsConstructor.kunik +"-icon").addClass("fa fa-" + value)
                    status = true;
                }
                
                return status;
            }
        },
        blockCms : {
            name: "blockcms",
            label: tradCms.blockcms,
            image: "cms.png"
        },
        blockGraph : {
            name: "blockgraph",
            label: "Graph",
            image: "chart.png"
        },
    },
    commonEditor:{
        general : [
        ],
        animation: [
            {
                type: "groupButtons",
                options: {
                    "name": "activeAnimate",
                    "label" : tradCms.activateAnimation,
                    options: [
                        {
                            value : false,
                            label : trad.no
                        },
                        {
                            value : true,
                            label : trad.yes
                        }
                    ]
                }
            },
            {
                type: "select",
                options: {
                    name: "animateType",
                    label: tradCms.animationName,
                    options: [
                        {
                            label: "Haut vers le bas",
                            value: "from { top: -150px; opacity: 0;} to {top: 0px; opacity: 1; }"
                        },
                        {
                            label: "En bas vers le haut",
                            value: "from { bottom: -150px; opacity: 0;} to {bottom: 0px; opacity: 1; }"
                        },
                        {
                            label: "A gauche vers droit",
                            value: "from { left: -150px; opacity: 0;} to {left: 0px; opacity: 1; }"
                        },
                        {
                            label: "A droit vers gauche",
                            value: "from { right: -150px; opacity: 0;} to {right: 0px; opacity: 1; }"
                        },
                        {
                            label: "Flou en apparition",
                            value: "from { filter: blur(10px); opacity: 0;} to {filter: blur(0); opacity: 1; }"
                        },
                        {
                            label: "Pulsation",
                            value: "0% {transform: scale(1);} 50% {transform: scale(1.1);}100% {transform: scale(1);}"
                        },
                        {
                            label: "Rotation",
                            value: "from { transform: rotateY(0deg); }to {transform: rotateY(360deg); }"
                        },
                        {
                            label: "Rebondissement",
                            value: "0%, 20%, 50%, 80%, 100% {transform: translateY(0); }40% {transform: translateY(-30px);}60% {transform: translateY(-15px);}"
                        },
                        // {
                        //     label: "Rétrécissement",
                        //     value: " from {transform: scale(2);}to {transform: scale(1);}"
                        // },
                        {
                            label: "Agrandissement",
                            value: "from {transform: scale(0);}to {transform: scale(1);}"
                        }

                    ]
                }
            },
            {
                type: "inputSimple",
                options: {
                    name: "tempsAnimate",
                    label: tradCms.animationDuration+" (s)"
                }
            },
            {
                type: "groupButtons",
                options: {
                    "name": "repeatAnimate",
                    "label" : tradCms.repetition,
                    options: [
                        {
                            value : false,
                            label : trad.no
                        },
                        {
                            value : true,
                            label : trad.yes
                        }
                    ]
                }
            },
        ],
        style : [
            "margin", 
            "padding",
            "boxShadow",
            "border",
            "borderRadius"
        ],
        hover : [
            "width",
            "height",
            "margin",
            "padding",
            "boxShadow",
            "border",
            "borderRadius"
        ],
        advanced : [
            "otherClass",
            "otherCss",
            "hideOnDesktop", 
            "hideOnTablet",
            "hideOnMobil",
        ],
        configCard : [
            "background",
            "border",
            "borderRadius",
            "boxShadow",
            {
                type: "section",
                    options: {
                    name: "commonClassName",
                        label: trad.Name,
                        showInDefault: true,
                        inputs: [
                        "color"
                    ]
                }
            },
            {
                type: "section",
                options: {
                    name: "commonClassTags",
                        label: trad.tags,
                        showInDefault: true,
                        inputs: [
                        "color",
                        "background",
                        "border",
                        "borderRadius",
                    ]
                }
            },
            {
                type: "section",
                options: {
                    name: "commonClassLocality",
                        label: "Locality",
                        showInDefault: true,
                        inputs: [
                        "color",
                    ]
                }
            },
            {
                type: "section",
                options: {
                    name: "commonClassDesc",
                        label: tradCms.shortDescription,
                        showInDefault: true,
                        inputs: [
                        "color",
                    ]
                }
            }
        ],
        query : {
            type: "section",
            options: {
                name: "query",
                label: tradCms.query,
                showInDefault: true,
                inputs: [
                    {
                        type: "selectMultiple",
                        options: {
                            name: "types",
                            label: tradCms.typeOfElement,
                            options: $.map(elTypeOptions, function (key, val) {
                                return {value: val, label: key}
                            })
                        },
                    },
                    {
                        type: "inputSimple",
                        options: {
                            name: "indexStep",
                            label: tradCms.numberofResultsDisplayed,
                        },
                    },
                    {
                        type: "select",
                        options: {
                            name: "orderBy",
                            label: trad.sortby,
                            options: $.map({
                                name: trad.name, 
                                created: tradCms.dateOfCreation, 
                                updated: tradCms.dateOfModification, 
                                startDate: tradDynForm.startDate
                            }, function (key, val) {
                                return {value: val, label: key}
                            })
                        },
                    },
                    {
                        type: "select",
                        options: {
                            name: "orderType",
                            label: tradCms.orderType,
                            options: $.map({
                                "1" : tradCms.ascending, 
                                "-1" : tradCms.descending
                            }, function (key, val) {
                                return {value: val, label: key}
                            })
                        },
                    },
                    {
                        type: "selectMultiple",
                        options: {
                            name: "fieldShow",
                            label: tradCms.dataDisplay,
                            maximumSelectionLength: 1 ,
                            options: $.map({ 
                                "name" : trad.Name,
                                "description" : tradCms.description,
                                "address" : trad.locality,
                                "tags" : trad.tags,
                                "shortDescription" : tradCms.shortDescription,
                                "role" : tradDynForm.roles,
                                "collection" : trad.collection,
                                "type" : trad.type,
                                "created" : trad.created
                            }, function (key, val) {
                                return {value: val, label: key}
                            })
                        }
                    },
                    {
                        type : "groupButtons",
                        options: {
                            name : "notSourceKey",
                            label :  tradCms.notSourceKey,
                            options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
                        }
                    },
                    {
                        type : "select",
                        options : {
                            name : "loadEvent",
                            label : tradCms.behavior,
                            class : "loadEvent",
                            options : ["scroll","pagination"]  
                        }
                    }
                ]
            }
        }
    }, 
    keyEvent:{
        Escape : function(){
            $("#right-panel").html("")
            $(".cmsbuilder-right-content").removeClass("active")
            $(".cmsbuilder-left-content").removeClass("active")
            $(".block-actions-wrapper").remove()
            $('[contenteditable]').removeAttr('contenteditable')
        },
        Delete : function() {
            var blockSelectedExist =  $("#all-block-container").find(".block-actions-wrapper.selected").length > 0;
            
            if (blockSelectedExist){
                var blockVisible =  !cmsConstructor.helpers.elementIsUnder($(`.cmsbuilder-block[data-kunik="${cmsConstructor.kunik}"]`)[0],$("#ajax-modal")[0]) && (cmsConstructor.helpers.isInViewport($(`.cmsbuilder-block-action-label[data-kunik="${cmsConstructor.kunik}"]`)[0],0) || cmsConstructor.helpers.isInViewport($(`.cmsbuilder-block-action-list[data-kunik="${cmsConstructor.kunik}"]`)[0],0) );
            
                if (blockVisible && typeof cmsConstructor.kunik != "undefined" && typeof cmsConstructor.spId != "undefined"){
                    $owner = $(`.cmsbuilder-block[data-kunik="${cmsConstructor.kunik}"]`);
                    name = $owner.data("name");
                    
                    cmsConstructor.builder.actions.deleteBlock(null,null,$owner,name)
                }
            }
                
        },
        ArrowUp : function(){
            cmsConstructor.builder.actions.sortBlock($($("#all-block-container").find(".block-actions-wrapper.selected").parents(".cmsbuilder-block")[0]), "move_up")
        },
        ArrowDown : function(){
            cmsConstructor.builder.actions.sortBlock($($("#all-block-container").find(".block-actions-wrapper.selected").parents(".cmsbuilder-block")[0]), "move_down")
        },
        Tab : function(){
            $(".btn-toggle-cmsbuilder-sidepanel").click();
            $(".cmsbuilder-tabs-header li[data-key='layers']").click();
        },
    },
    /* SPACES TO ADD*/
    builder: {
        init:function(){
            costumizer.actions.tabs.init(
                "#left-panel",
                [
                    {
                        key: "blocks",
                        icon: "object-group",
                        label: tradCms.blocks,
                        content: this.views.init()
                    },
                    {
                        key: "layers",
                        icon: "list-ul",
                        label: tradCms.layers,
                        content: cmsConstructor.layer.views.getContainer()
                    },
                    {
                        key: "page",
                        icon: "file",
                        label: "Page",
                        content: cmsConstructor.page.views.getContainerPage()
                    }
                ]
                
            );

            this.events.init()
        },
        views: {
            init: function () {
                var $html = $(`<div class="cmsbuilder-block-section"></div>`)

                var $blocksContainerSection = $(`
                    <div class="cmsbuilder-block-section-item">
                        <div class="cmsbuilder-block-section-label">${tradCms.container}</div>
                        <div class="cmsbuilder-block-list"></div>
                    </div>
                `);
                var containerArray=["section", "column"]
                containerArray.forEach(function (k) {
                    block=cmsConstructor.blocks[k];
                    $blocksContainerSection.find(".cmsbuilder-block-list").append(`
                        <div class="cmsbuilder-block-list-item" data-name="${block.name}" data-path="${block.path}" data-subtype="${block.subtype}" draggable="true">
                            <img src="${assetPath}/cmsBuilder/img/blocks/${block.image}" alt="" draggable="false">
                            <span>${block.label}</span>
                        </div>
                    `)
                })
                $html.append($blocksContainerSection)

                var $blocksContentsSection = $(`
					<div class="cmsbuilder-block-section-item">
						<div class="cmsbuilder-block-section-label">${tradCms.components}</div>
						<div class="cmsbuilder-block-list"></div>
					</div>
				`)
                var contentsArray=["title", "text", "image", "video", "button", "icon", "lineSeparator", "blockCms", "blockGraph"]
                contentsArray.forEach(function (k) {
                     block=cmsConstructor.blocks[k];
                    $blocksContentsSection.find(".cmsbuilder-block-list").append(`
                        <div class="cmsbuilder-block-list-item" data-name="${block.name}" data-path="${block.path}" data-subtype="${block.subtype}" draggable="true">
                            <img src="${assetPath}/cmsBuilder/img/blocks/${block.image}" alt="" draggable="false">
                            <span>${block.label}</span>
                        </div>
                    `)
                })
                $html.append($blocksContentsSection)

                return $html;
            },
            dropzone : {
                addContainer:function(){
                    // $(".block-cms-dropzone").remove()
                    $(".block-cms-dropzone").remove();
                    //add column dropzone
                    $(".cmsbuilder-block:not([data-blockType='element'])").each(function(){
                        var dropzoneContainer = $(`
                            <div class="block-cms-dropzone block-cms-dropzone-container" data-kunik="${$(this).data("kunik")}">
                                <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                            </div>
                        `)

                        $(this).prepend(dropzoneContainer)
                    })

                },
                addPage:function(){

                    $(".block-cms-dropzone").remove();

                    if ($(".pageContent #all-block-container .cmsbuilder-block[data-blockType='section']").length < 1) {
                        // No 'section' blocks available, add dropzone
                        $(".pageContent").append(`
                            <div class="block-cms-dropzone block-cms-dropzone-page" data-position="in" style="height:${$(".pageContent").height()}px !important;">
                                <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                            </div>
                        `);
                    } else {
                        //add section dropzone
                        $(".pageContent #all-block-container > .cmsbuilder-block[data-blockType='section']").each(function(index){
                            var getDropzone = function(id, kunik, position){
                                return (`
                                    <div class="block-cms-dropzone block-cms-dropzone-page" data-position="${position}" data-id="${id}" data-kunik="${kunik}">
                                        <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                                    </div>
                                `)
                            }

                            if (cmsConstructor.helpers.isInViewport(this,1000)){
                                $(this).before(getDropzone($(this).data("id"), $(this).data("kunik"), "before"))
                                if((index+1)=== $(".cmsbuilder-block[data-blockType='section']").length)
                                    $(this).after(getDropzone($(this).data("id"), $(this).data("kunik"), "after"))
                            }
                        })
                    }
                }
               
            }
        },
        actions : {
            /**
                * 
             * @param {object} parent
             * @param {string} parent.id
             * @param {string} parent.kunik 
             * @param {object} block 
             * @param {string} block.path
             * @param {string} block.name
             * @param {string} block.subtype 
             */
            addBlock: function(parent, block){
                var order = 0,
                    id = parent.id,
                    kunik = parent.kunik,
                    path = block.path,
                    name = block.name,
                    subtype = block.subtype,
                    generateID = cmsConstructor.helpers.generateUniqueId();
                $(".empty-sp-element" + kunik).addClass("d-none");
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms '+generateID+' " data-id="undefined">' +
                    '<div class="selected cms-area-selected blink-info">' + trad.waitendofloading + '</div>' +
                    '</div>';
                $(".cmsbuilder-block-container[data-id=" + id + "]").append(strNewBlc);
                coInterface.initHtmlPosition();
                var newCmsElement = {
                    collection: "cms",
                    value: {
                        path: path,
                        page: page,
                        order: order,
                        blockParent: id,
                        css: {
                            "paddingTop": "20px",
                            "paddingLeft": "40px",
                            "paddingRight": "40px",
                            "paddingBottom": "20px"
                        },
                        parent: {}
                    },
                    saveLog: {
                        costumId: costum.contextId,
                        action: "costum/addBlock",
                        blockName: path.split(".").pop(),
                        blockPath: path,
                        page: page,
                    }
                }
                dyFObj.setMongoId('cms', function (data) {
                    newCmsElement.id = dyFObj.currentElement.id;
                    newCmsElement.value.parent[costum.contextId] = {
                        type: costum.contextType,
                        name: costum.contextName
                    };
                    if (path == "tpls.blockCms.superCms.elements.button") {
                        newCmsElement.value.css.paddingTop = "10px"
                        newCmsElement.value.css.paddingBottom = "10px"
                        delete newCmsElement.value.css.paddingLeft
                        delete newCmsElement.value.css.paddingRight
                        newCmsElement.value.css["borderBottomLeftRadius"] = "25px",
                            newCmsElement.value.css["borderBottomRightRadius"] = "25px",
                            newCmsElement.value.css["borderTopLeftRadius"] = "25px",
                            newCmsElement.value.css["borderTopRightRadius"] = "25px"
                    };
                    if (path == "tpls.blockCms.superCms.container") {
                        newCmsElement.value.css["justifyContent"] = "space-evenly"
                    }
                    if (path == "tpls.blockCms.superCms.elements.image") {
                        delete newCmsElement.value.css
                    }
                    if (path == "tpls.blockCms.superCms.elements.title") {
                        newCmsElement.value.css["textAlign"] = "center"
                    }
                    dataHelper.path2Value(newCmsElement, function (params) {
                        var newChildId = params.saved.id;

                        // If persistent section, Update the added block into persistent
                        var sectionId = $(".whole-container" + id).parents(".custom-block-cms").data("id")
                        if (typeof cmsConstructor.sp_params[sectionId].advanced != "undefined" && typeof cmsConstructor.sp_params[sectionId].advanced.persistent != "undefined") {
                            cmsConstructor.helpers.upCmsPersistence(true,newChildId, cmsConstructor.sp_params[sectionId].advanced.persistent,sectionId)
                        } else {
                            ajaxPost(
                                null,
                                baseUrl + '/co2/cms/refreshblock',
                                {
                                    idBlock: newChildId,
                                    clienturi: window.location.href
                                },
                                function (response) {
                                    $("."+generateID).replaceWith(response.view)
                                    cmsBuilder.block.initEvent();
                                    cmsBuilder.block.updateOrder();
                                    costumSocket.emitEvent(wsCO, "update_component", {
                                        functionName: "insertBlockCms",
                                        data: {
                                            id: id,
                                            kunik: kunik,
                                            view: response.view,
                                            page: location.hash
                                        }
                                    })

                                }
                            )
                        }

                        // Desable json injection (Add block means, it's no longer identic as the template origin)
                        // cmsConstructor.helpers.unsetTemplateOrigin()

                        toastr.success(tradCms.elementwelladded);
                        delete newCmsElement.saveLog;
                  })
                });
            },
            insertBlock: function(position="in", kunik=null,reloadByHash=false){
                localStorage.removeItem("parentCmsIdForChild");
                localStorage.setItem("addCMS", true);
                // $(".sample-cms").remove();
                var generateID = cmsConstructor.helpers.generateUniqueId();
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms '+generateID+' " data-id="undefined">' +
                    '<div class="selected cms-area-selected blink-info">' + trad.waitendofloading + '</div>' +
                    '</div>';
                switch (position) {
                    case "in":
                        $("#all-block-container").append(strNewBlc)
                        break;
                    case "before":
                        $(strNewBlc).insertBefore($(`.block-container-${kunik}`))
                        break;
                    case "after":
                        $(strNewBlc).insertAfter($(`.block-container-${kunik}`))
                        break;
                };
                ajaxPost(
                    null,
                    baseUrl + "/co2/cms/insertblocksection",
                    {
                        "page": page
                    },
                    function (response) {
                        $("."+generateID).replaceWith(response.html);
                        $("."+generateID).parent().css("height", "max-content");
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "insertSection",
                            data: {
                                page: location.hash,
                                parentKunik: kunik,
                                position: position,
                                view: response.html,
                                idBlock: response.params._id.$id,
                                reloadByHash: reloadByHash,
                                generatedID: generateID
                            }
                        })
                        cmsBuilder.block.initEvent(response.params._id.$id, response.params._id.$id);
                        // cmsConstructor.helpers.unsetTemplateOrigin()
                        cmsBuilder.block.updateOrder();
                        coInterface.initHtmlPosition();
                        toastr.success(tradCms.elementwelladded);
                        if (reloadByHash) {
                            urlCtrl.loadByHash(location.hash);
                        }
                    }
                );
            },
            sortBlock:function($el, action){
                var kunik = $el.attr("data-kunik");
                var id = $el.attr("data-id");
                var params = cmsConstructor.helpers.moveBlockView(action,id,false);
                ajaxPost(
                    null,
                    baseUrl+"/costum/blockcms/dragblock",
                    params,
                    function(response){
                            cmsConstructor.layer.init()
                            if ( $(".cmsbuilder-left-content").hasClass("active")){
                                cmsConstructor.layer.actions.selectPath(kunik)
                            };
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "moveBlock", 
                                data: { 
                                    idBlock: id ,
                                    action : action
                                }
                            })
                    }
                );
            },
            deleteBlock: function(callback=null,children=null,owner=null,name=null){

                var childElements = owner.find('.cmsbuilder-block');
                var hasElementBusy = false;
                childElements.each(function() {
                    if ($(this).hasClass('element-busy')) {
                        hasElementBusy = true;
                        return false;
                    }
                });
                if ( owner.hasClass('element-busy') || hasElementBusy ) {
                    var message = owner.hasClass('element-busy') ? `<p class="padding-20 text-center">${costum.members.lists[owner.data('userid')].avatar} ${costum.members.lists[owner.data('userid')].name} ${tradCms.elementBusy}</p>` : `<p class="padding-20 text-center">${tradCms.userEditingSub}</p>`;
                    bootbox.dialog({
                        message: message,
                        title: tradCms.deletionRefused,
                        show: true,
                        backdrop: true,
                        closeButton: false,
                        animate: true,
                        className: "my-modal",
                        backdrop: 'static',
                        buttons: { 
                            cancel: {
                                label: "OK",
                                className: 'btn-default'
                            }
                        }
                    });
                } else {
                    var children = cmsConstructor.layer.views.getArbre(owner);
                    var allBlockToDelete = [cmsConstructor.spId];
                    if (children.find("li").length !== 0) {
                        children.find("li").each(function() {
                            allBlockToDelete.push($(this).data("id"));
                        });
                    }

                    var nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name == "une section" ? tradCms.oneSection : name;
                    bootbox.confirm(
                        "<font size='5' class='text-red'>" + trad.delete + "  " + nameTrad + ":</font><br>" + trad.areyousuretodelete + "<br>" + ((children.find("li").length != 0 ? tradCms.deleteWithElement : "")) + "<br>" + children.html(),
                        function (result) {
                            if (result) {
                                $("#firstLoader").show();
                                $("#firstLoader #loadingModal").css({
                                    'opacity': '0.5'
                                });

                                var id = cmsConstructor.spId;
                                var paramsBlock = cmsConstructor.sp_params[id];
                                var params = {
                                    costumId: costum.contextId,
                                    costumType: costum.contextType,
                                    cmsId: id,
                                    blockPath: paramsBlock.path,
                                    blockName: paramsBlock.name,
                                    collection: paramsBlock.collection,
                                    page: paramsBlock.page,
                                    allBlockToDelete: allBlockToDelete
                                };

                                if (children != null && children[0]["childElementCount"] > 0)
                                    params.children = children[0]["outerHTML"];

                                ajaxPost(   
                                    null,
                                    `${baseUrl}/co2/cms/delete`,
                                    params,
                                    function (data) {
                                        if (data && data.result) {
                                            cmsConstructor.helpers.deleteRemoveElement(id);
                                            costumSocket.emitEvent(wsCO, "update_component", {
                                                functionName: "deleteBlock",
                                                data: { idBlock: id }
                                            })
                                            $("#firstLoader").hide();
                                            var params = { functionName: "elementUnfocused", blockId: {}, userId: userId }
                                            costumSocket.emitEvent(wsCO, "edit_component", params)
                                        } else {
                                            toastr.error(tradCms.somethingWrong);
                                        }

                                        if (callback && typeof callback === "function")
                                            callback(data)
                                        // cmsConstructor.helpers.unsetTemplateOrigin()
                                        cmsConstructor.helpers.deleteCtrlBlock("ctrlZ"+costum.slug,allBlockToDelete);
                                        cmsConstructor.helpers.deleteCtrlBlock("ctrlY"+costum.slug,allBlockToDelete);
                                    },
                                    null,
                                    "json"
                                );
                            }

                        }
                    )
                }

            }, 
            unlinkBlock : function ($owner) {
                if($owner.parents(".cmsbuilder-block[data-blockType='column']:not(.sp-alias)").length > 0)
                    $target = $($owner.parents(".cmsbuilder-block[data-blockType='column']")[0])
                else if($owner.parents(".cmsbuilder-block[data-blockType='section']:not(.sp-alias)").length > 0)
                    $target = $($owner.parents(".cmsbuilder-block[data-blockType='section']")[0])

                if($target && $target.find("> .block-actions-wrapper").length < 1){
                    var targetType = $target.data("blocktype")
                    cmsConstructor.spId = $target.data("id")
                }

                $.each(cmsConstructor.sp_params[cmsConstructor.spId].alias, function(costumParent, valueAlias){ 
                    $.each(valueAlias.container, function(key, idCMSAlias){                                          
                        if (idCMSAlias == $owner.data("id")) { 
                            var arrayAliasCms = cmsConstructor.sp_params[cmsConstructor.spId].alias[costumParent].container
                            cmsConstructor.sp_params[cmsConstructor.spId].alias[costumParent].container = arrayAliasCms.filter(item => item !== $owner.data("id"));                                           

                            if (!notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].alias[costumParent].container)) {
                                delete cmsConstructor.sp_params[cmsConstructor.spId].alias[costumParent]
                                if (!notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].alias)) {
                                    delete cmsConstructor.sp_params[cmsConstructor.spId].alias
                                }
                            }

                            if (typeof cmsConstructor.sp_params[cmsConstructor.spId].alias == "undefined") {
                                removeAlias = {
                                    id : cmsConstructor.spId,
                                    collection: "cms",
                                    path : "alias"
                                }
                                dataHelper.unsetPath( removeAlias, function(params) {
                                    cmsConstructor.helpers.deleteRemoveElement(idCMSAlias);
                                    costumSocket.emitEvent(wsCO, "update_component", {
                                        functionName: "deleteBlock",
                                        data: { idBlock: idCMSAlias }
                                    })
                                    toastr.success(trad.finish);
                                } );
                            }else{
                                var aliasParent = {
                                    collection : "cms",
                                    id : cmsConstructor.spId,
                                    path : "alias",
                                    value : cmsConstructor.sp_params[cmsConstructor.spId].alias                                                           
                                }
                                dataHelper.path2Value(aliasParent, function (params) {
                                    cmsConstructor.helpers.deleteRemoveElement(idCMSAlias);
                                    costumSocket.emitEvent(wsCO, "update_component", {
                                        functionName: "deleteBlock",
                                        data: { idBlock: cmsConstructor.spId }
                                    })
                                    toastr.success(trad.finish);
                                });
                            }
                        }
                    })

                })
            },
            duplicateBlock: function (ide, blockParent=null,action,owner,from=null,to=null){ 
                if (typeof ide == "string"){
                    ide = [ide]
                    let parentKunik = cmsConstructor.helpers.findElement(".cmsbuilder-block [data-id="+ide+"]").parents(".custom-block-cms").data("kunik");
                    var intervalBlocAdded = setInterval(function () {
                        clearInterval(intervalBlocAdded);
                        if ($(".load"+parentKunik).length > 0) {
                            let strTimeOut = `<p class="padding-top-20">Probleme de chargement!</p><i onclick="window.location.reload()" class="fa fa-repeat fa-2x cursor-pointer padding-top-10"></i>`
                            $(".load"+parentKunik).html(strTimeOut)
                        }
                    }, 30000)
                }
                
                var data = {   
                    "idCmsToDuplic" : ide,
                    "page"          : cmsBuilder.config.page,
                    "costumId"      : costum.contextId,
                    "costumSlug"    : costum.contextSlug,
                    "costumType"    : costum.contextType,
                    "clienturi"     : window.location.href
                }
                if( notNull(from)){
                    data.from = from
                }
                if( notNull(to)){
                    data.to = to
                }
                if(notNull(blockParent)){
                    data.blockParent=blockParent;
                    owner = blockParent;
                }
                ajaxPost(
                    null,
                    baseUrl+"/co2/cms/duplicateblock",
                    data, 
                    function (response) {
                        if ( response.result ) {
                            cmsConstructor.helpers.insertBlockView(action,owner,response.view,false,null,null);
                            newSection = "";
                            newBlockParent = "";

                            if (notNull(blockParent) && typeof response.blockCms.advanced != "undefined" && typeof response.blockCms.advanced.persistent != "undefined") {
                                cmsConstructor.helpers.upCmsPersistence(false,blockParent, response.blockCms.advanced.persistent,blockParent)
                            } else {
                                if( typeof response.blockCms.blockParent === "undefined") {
                                    var $div = $(".cmsbuilder-block[data-id="+response.blockCms["_id"]["$id"]+"]").clone();
                                    $div.empty();
                                    var newSection = $div[0].outerHTML;
                                    newBlockParent = response.blockCms["_id"]["$id"];
                                } else {
                                    newBlockParent = response.blockCms.blockParent;
                                }
                                cmsBuilder.block.updateOrder();
                            }
                            
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "duplicateBlock", 
                                data: { actionName : action , ownerBlock : owner , newBlockParent : newBlockParent , newDiv : newSection }
                            })
                            toastr.success(trad.finish);
                        } else {
                            toastr.error(tradCms.somethingWrong);
                        }
                    }
                );
            }
        },
        events:{
            init:function(){
                this.dragAndDropBlock();
                this.dragAndDropColor();
                this.modalBlockCms()
            },
            dropzone :{ 
                launch : function(){
                    $(".cmsbuilder-block-droppable").droppable({
                        greedy:true,
                        over:function(e){
                            $(".block-cms-dropzone").removeClass("dragover")
                            $(this).find("> .block-cms-dropzone").addClass("dragover")
                        },
                        out:function(){
                            $(this).find("> .block-cms-dropzone").removeClass("dragover")
                        },
                        drop:function(e, ui){
                            e.stopPropagation()
                            
                            var blockName = $(ui.draggable).data("name"),
                                parentType = $(e.target).data("blocktype");
                            if(blockName == "blockcms" || blockName == "blockgraph"){
                                var params = {
                                    space:"template",
                                   // title:"Model CMS",
                                    showSaveButton:false,
                                    key:"blockCms"
                                }
                               // delete costumizer.cms["list"]
                                // costumizer.template.paramsTemplateList.defaults.filters["category"] = "community";
                               // $("#blocklist-search-text").parent().show()
                               // $(".cmsbuilder-right-content").removeClass("active")
                               // $("#blocklist-dropdown-toggle").parent().toggle(true);
                                if(blockName == "blockgraph"){
                                    params["category"] = ["graph"];
                                    costumizer.template.views.initAjax(params)
                                    if (!$(".category[data-key='graph']").hasClass("active")) {
                                        $(".category[data-key='graph']").click();
                                    }
                                }else{
                                    costumizer.template.views.initAjax(params)
                                }
                                cmsConstructor["blockParent"] = $(e.target).data("id")
                                localStorage.setItem("parentCmsIdForChild", $(e.target).data("id"));
                            }else if(parentType == "section" && blockName == "section"){
                                bootbox.alert({
                                    message: `<br>
                                    <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                    <p class="padding-left-20">${tradCms.alertSecinSec}</p>
                                    </div>`
                                    ,
                                    size: 'medium'
                                });
                            }else if(parentType == "column" && blockName == "section"){
                                bootbox.alert({
                                    message: `<br>
                                    <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                    <p class="padding-left-20">${tradCms.alertSecinColumn}</p>
                                    </div>`
                                    ,
                                    size: 'medium'
                                });
                            }else{
                                var parent = {
                                    id:$(e.target).data("id"),
                                    kunik:$(e.target).data("kunik")
                                }
                                var block = {
                                    name: blockName,
                                    subtype: $(ui.draggable).data("subtype"),
                                    path: $(ui.draggable).data("path")
                                }
                                
                                cmsConstructor.builder.actions.addBlock(parent, block)
                            }
                        }
                    })
                    $(".block-cms-dropzone-page").droppable({
                        greedy:true,
                        hoverClass:"dragover",
                        drop:function(){
                            cmsConstructor.builder.actions.insertBlock($(this).data("position"), $(this).data("kunik"))
                        }
                    })
                }
            },
            dragAndDropBlock: function(){
                var autoScroller = cmsConstructor.helpers.getAutoScroller(".cmsbuilder-center-content")
                $(".cmsbuilder-block-list-item").draggable({
                    containment:"document",
                    scroll:true,
                    zIndex:999999999,
                    helper:"clone",
                    start:function(){
                        cmsConstructor.dragstart = true;

                        $(".block-actions-wrapper").remove();
                        $(".cmsbuilder-right-content").removeClass("active")

                        var name = $(this).data("name")

                        if(name === "section"){
                            cmsConstructor.builder.views.dropzone.addPage()
                        }else{
                            //add space and border to column block on drag start
                            $(".cmsbuilder-block-droppable").each(function() {
                                var $element = $(this);
                                if (cmsConstructor.helpers.isInViewport($element[0],0)) {
                                  $element.addClass("dragstart");
                                }
                              });
                              
                            cmsConstructor.builder.views.dropzone.addContainer()
                        }
                    
                        cmsConstructor.builder.events.dropzone.launch()
                    },
                    stop:function(){
                        cmsConstructor.dragstart = false
                        //remove space and border to column block on drag end
                        $(".cmsbuilder-block-droppable").removeClass("dragstart")
                        $(".block-cms-dropzone").remove()
                  
                        //cmsConstructor.builder.events.dropzone.remove()
                        autoScroller.stop()
                    },
                    drag: function(e){
                        autoScroller.start(e)
                    }
                })
            },
            dragAndDropColor: function(){
                $('.draggabled-co').draggable({
                    helper: "clone",
                    start: function (event, ui) {
                        $(".block-actions-wrapper").remove();
                        $(".cmsbuilder-right-content").removeClass("active");
                        const color = $(this).data("color");
                        if (color) {
                            ui.helper.data("color", color);
                            mylog.log('colorsss', color);
                        } else {
                            console.warn("Missing data-color attribute on draggable element.");
                        }
                        $(".colorDroppable").droppable({
                            accept: ".draggabled-co",
                            hoverClass: "ui-active",
                            greedy: true,
                            over: function(e) {
                                $(".colorDroppable").removeClass("ui-active");
                                $(this).addClass("ui-active");
                            },
                            out: function() {
                                $(this).removeClass("ui-active");
                            },
                            drop: function (e, ui) {
                                $(this).removeClass("ui-active");
            
                                const draggedColor = ui.helper.data("color");
                                const droppableId = $(this).data("id");
                                const blockType = $(e.target).data("blocktype");
                                var kunik = $(this).data("kunik") ? $(this).data("kunik"):"";
            
                                if (!droppableId) {
                                    toastr.error("Il est impossible de glisser ou déposer une couleur ici.");
                                    return;
                                }
                                if (
                                    $(this).is('.sp-text.colorDroppable[data-name="text"]') && 
                                    $(this).children('[style*="color:"]').length > 0
                                ) {
                                    toastr.error("Il est impossible de glisser ou déposer une couleur ici.");
                                    return;
                                }
                                if (
                                    $(this).is('.sp-text.colorDroppable[data-field="title"]') && 
                                    ($(this).children('[style*="color:"]').length > 0 || $(this).children('[color]').length > 0)
                                ) {
                                    toastr.error("Il est impossible de glisser ou déposer une couleur ici.");
                                    return;
                                }
                                
            
                                if (draggedColor) {
                                    const target = $(this);
                                    if (target.length) {
                                        if (!target.hasClass("sp-text") && !target.hasClass("superIcon")) {
                                            target.css("background-color", draggedColor);
                                            cmsConstructor.spId = droppableId;
                                            cmsConstructor.kunik = kunik;
                                            cmsConstructor.editor.actions.updateBlock(
                                                blockType,
                                                "css.backgroundColor",
                                                draggedColor,
                                                "backgroundColor",
                                                {},
                                                draggedColor
                                            );
                                        } else {
                                            target.css("color", draggedColor);
                                            cmsConstructor.spId = droppableId;
                                            cmsConstructor.kunik = kunik;
                                            cmsConstructor.editor.actions.updateBlock(
                                                blockType,
                                                "css.color",
                                                draggedColor,
                                                "color",
                                                {},
                                                // draggedColor+'! important'
                                                draggedColor
                                            );
                                        }
                                    } else {
                                        // mylog.log("Target element not found for ID:", droppableId);
                                        toastr.error("Il est impossible de glisser ou déposer une couleur ici.");
                                    }
                                } else {
                                    // mylog.log("Missing draggedColor.");
                                    toastr.error("Il est impossible de glisser ou déposer une couleur ici.");
                                }
                            }
                        }); 
                    }
                });
            },            
            modalBlockCms: function(){
                var filterListBlocks = function(){
                    var type = $("#blocklist-dropdown-menu li.selected").data("type"),
                        text = $("#blocklist-search-text").val().toLowerCase()

                    $(".modal-blockcms-list-item").filter(function(){
                        $(this).toggle(
                            (($(this).data("type") == type) || (type=="all")) &&
                            ((text.replaceAll(" ", "") === "") || ($(this).data("name").toLowerCase().indexOf(text) > -1))
                        )
                    })
                }

                $("#blocklist-dropdown-menu li").off("click").on("click", function(){
                    var type = $(this).data("type");
                    $("#blocklist-dropdown-menu li").removeClass("selected")
                    $(this).addClass("selected")

                    $("#blocklist-dropdown-toggle .btn-label").text((type == "all") ? "Tout les blocks":type)

                    filterListBlocks()
                })

                var typingTimer = null
                $("#blocklist-search-text").off("keyup").on("keyup", function(){
                    clearTimeout(typingTimer)
                    setTimeout(function(){  
                        filterListBlocks()
                    }, 100)
                })

                $("#btn-hide-modal-blockcms").off("click").on("click", function(){
                    $("#modal-blockcms").removeClass("open")
                })

                $(".modal-blockcms-list-item .deleteCms").off().on("click", function(e) {
                    e.stopImmediatePropagation()
                    e.stopPropagation()
                    var id = $(this).data("id");
                    $("#modal-blockcms").removeClass("open");
                    bootbox.confirm(`<div role="alert">
                        <p><b class="">Supprimer le bloc</b> <b class="text-red">`+$(this).data("name")+`</b> <b class="">pour toute l'éternité?</b><br><br>
                        Il ne peut pas être récupéré une fois supprimé.<br>
                        </p> 
                        </div> `,
                        function(result) {
                            if (!result) {
                                $("#modal-blockcms").addClass("open")
                                return;
                            } else {
                                var type = "cms";
                                var urlToSend = baseUrl + "/co2/cms/delete/id/" + id;
                                ajaxPost(
                                    null,
                                    urlToSend,
                                    null,
                                    function(data) {
                                        if (data && data.result) {
                                            toastr.success("Elément effacé");
                                            window.location.reload()
                                        } else {
                                            toastr.error(tradCms.somethingWrong);
                                        }
                                    },
                                    null,
                                    "json"
                                    );
                            }
                        }
                        );
                });
            }
        }
    },
    layer: {  
        actions:{
            selectPath:function(kunik){
                parent.$(".page-layer-item-btn").removeClass("selected")
                parent.$(`.page-layer-item-btn[data-kunik="${kunik}"]`).addClass("selected")
                parent.$(".page-layer-item").removeClass("active")
                parent.$(`.page-layer-item-btn[data-kunik="${kunik}"]`).parents(".page-layer-item").addClass("active")
            }
        },
        init:function(){
            this.views.init()
            this.events.init()
        },
        views: {
            init:function(){
                parent.$("#page-layer-container").html(cmsConstructor.layer.views.getLayer($("#all-block-container")))
            },
            getLayer : function($el){
                var $layer = $(`<ul class="page-layer-item-children"></ul>`)
                if($el.children(".cmsbuilder-block").length > 0){
                    $el.children(".cmsbuilder-block").each(function(){
                        var kunik = $(this).data("kunik"),
                            id = $(this).data("id");
                            blocktype = $(this).data("blocktype"),
                            name = $(this).data("name"),
                            nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name ,
                            path = $(this).data("path")
                            isBlockContinainer = ((blocktype=="section" && path=="tpls.blockCms.superCms.container") || blocktype=="column");

                        var $li = $(`<li class="page-layer-item"></li>`)

                        $li.append(`
                            <div class="page-layer-item-btn" data-blocktype="${blocktype}" data-kunik="${kunik}" data-label="${name}">
                                <div>
                                    <span><i class="fa fa-${ blocktype=='section'?'object-group':'columns' }" aria-hidden="true"></i></span>
                                    ${nameTrad}
                                </div> 
                                <span>
                                ${ isBlockContinainer ? `<i class="fa fa-caret-down" aria-hidden="true"></i>`:""}
                                <i class="fa fa-trash page-layer-item-btn-delete" data-kunik="${kunik}" data-id="${id}" data-name="${name}" aria-hidden="true" 
                                style="margin-left: 5px;">
                                </i>
                                </span>
                                
                            </div>
                        `)

                        var $children = cmsConstructor.layer.views.getLayer($(this));
                        if($children.find("li").length > 0)
                            $li.append($children)

                        $layer.append($li)
                    })
                }else if($el.children(".block-container-html").length > 0)
                    $layer = cmsConstructor.layer.views.getLayer($($el.children(".block-container-html")[0]))
                else if($el.children(".cmsbuilder-block-container").length > 0)
                    $layer = cmsConstructor.layer.views.getLayer($($el.children(".cmsbuilder-block-container")[0]))
    
                return $layer;
            },
            getArbre : function($el){
                var $layer = $(`<ol></ol>`);
                if($el.children(".cmsbuilder-block").length > 0){
                    $el.children(".cmsbuilder-block").each(function(){
                        if (Object.keys(cmsConstructor.sp_params[$(this).data("id")].parent)[0] == costum.contextId) {
                            var id = $(this).data("id"),
                            blocktype = $(this).data("blocktype"),
                            name = $(this).data("name"),
                            nameTrad = name == "image" ? tradCms.image : name == "button" ? tradCms.button : name == "text" ? tradCms.text : name == "separator" ? tradCms.separator : name == "une section" ? tradCms.oneSection : name,
                            path = $(this).data("path")
                            isBlockContinainer = ((blocktype=="section" && path=="tpls.blockCms.superCms.container") || blocktype=="column");
                            var $li = $(`<li data-id=${id} >${nameTrad}</li>`)

                            var $children = cmsConstructor.layer.views.getArbre($(this));
                            if($children.find("li").length > 0)
                                $li.append($children)
                            $layer.append($li)
                        }
                    })
                }else if($el.children(".block-container-html").length > 0)
                    $layer = cmsConstructor.layer.views.getArbre($($el.children(".block-container-html")[0]))
                else if($el.children(".cmsbuilder-block-container").length > 0)
                    $layer = cmsConstructor.layer.views.getArbre($($el.children(".cmsbuilder-block-container")[0]))
    
                return $layer;
            },
            getContainer: function(){
                return `<div id="page-layer-container" style="width:100%; height:100%; color:white;"></div>`
            }
        },
        events:{
            init:function(){
                parent.$(".page-layer-item-btn").off("click").on("click", function(e){
                    e.stopPropagation();
                    var kunik = $(this).data("kunik");
                    if ( $(`.cmsbuilder-block[data-kunik="${kunik}"]`).hasClass('element-busy') ){
                        userId = $(`.cmsbuilder-block[data-kunik="${kunik}"]`).data('userid');
                        bootbox.dialog({
                            message: `<p class="padding-20 text-center">${costum.members.lists[userId].avatar} ${costum.members.lists[userId].name} ${tradCms.elementBusy}</p>`,
                            title: tradCms.editionRefused,
                            show: true,
                            backdrop: true,
                            closeButton: false,
                            animate: true,
                            className: "my-modal",
                            backdrop: 'static',
                            buttons: { 
                                cancel: {
                                    label: "OK",
                                    className: 'btn-default'
                                }
                            }
                        });
                    } else {
                        $(this).closest(".page-layer-item").toggleClass("active")
                        $(".page-layer-item-btn").removeClass("selected")
                        $(".block-actions-wrapper").removeClass("selected")
                        $(this).addClass("selected")
                        costumizer.actions.closeRightSubPanel()
                        $target = $(`.cmsbuilder-block[data-kunik="${kunik}"]`).first();              
                        cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $target);
                        cmsConstructor.block.actions.addActions($target, true)
                        $(`.cmsbuilder-block[data-kunik="${kunik}"]`).click();
                    }
                })

                parent.$(".page-layer-item-btn-delete").off("click").on("click", function(e){
                    e.stopPropagation();

                    kunik = $(this).data("kunik");
                    cmsConstructor.spId =  $(this).data("id");
                    name = $(this).data("name");
                    $owner = $(`.cmsbuilder-block[data-kunik="${kunik}"]`);
                    cmsConstructor.builder.actions.deleteBlock(null,null,$owner,name)
                })


            }
        }
    },
    page: {
        init:function(){
            cmsConstructor.page.views.init();
            cmsConstructor.page.events.bind();
        },
        views : {
            init:function(){
                $("#list-page-container").html(cmsConstructor.page.views.getPage());
                costumizer.pages.events.bind()
            },
            getPage : function(){

                var positionMenu = ["left","center","right"],
                    typeMenu = ["menuTop","subMenu","menuLeft"],
                    $pageList = "";

                costumizer.liveChange = false;

                if (typeof costumizer.obj.app != "undefined") {
                    $pageList += `<button class="btn create-page btn"><i class="fa fa-plus"></i> ${tradCms.createNewPage}</button>
                                    <ul id="menu-content-page" class="menu-content-page">`;
                        $.each(costumizer.obj.app, function(key, value) {
                            var pageName = key.replace("#", ""),
                                costumLang = costum.langCostumActive || "fr",
                                typePage="pageCMS",
                                status = ["#575a5a", tradCms.published],
                                btnPublie = " <a href='javascript:;' class='brouillon-page tooltips' data-key='"+key+"''><i class='fa fa-share-square-o'></i> "+tradCms.draftPage+"</a>",
                                defaultValueRestricted = "all",
                                iconeRestricted = '<i class="fa fa-unlock"></i>';

                            if((typeof value.appTypeName != "undefined" && value.appTypeName != "") || typeof value.useFilter != "undefined"){
                                typePage="advancedApp";
                            }

                            if(typeof value.lbhAnchor != "undefined" && value.lbhAnchor == true)
                                typePage="anchorMenu";

                            if(typeof value.target != "undefined" && value.target == true && typeof value.urlExtern != "undefined")
                                typePage="externalLink";

                            if(typeof value.hash != "undefined" && value.hash == "#app.aap")
                                typePage="aap";

                            if(typeof value["name"] !='undefined' && typeof value["name"] === "object" && notEmpty(value["name"])) {
                                pageName = (typeof value["name"][costumLang] != "undefined") ? value["name"][costumLang] : value["name"][Object.keys(value["name"])[0]];
                            }
                            if(typeof value.restricted != "undefined" && value.restricted != null){
                                if(typeof value.restricted.admins != "undefined"){
                                    if( value.restricted.admins == true || value.restricted.admins == "true"){
                                        defaultValueRestricted = "admins";
                                        forAll = ["red", tradCms.visibleToAdminOnly];
                                    }
                                }

                                if(typeof value.restricted.members != "undefined"){
                                    if( value.restricted.members == true || value.restricted.members == "true"){
                                        defaultValueRestricted = "members";
                                    }
                                }

                                if(typeof value.restricted.draft != "undefined" && value.restricted.draft == true){
                                    status = ["#63333f", tradCms.draft];
                                    btnPublie = " <a href='javascript:;' class='publie-page tooltips' data-key='"+key+"''><i class='fa fa-share-square-o'></i> "+tradCms.publishPage+"</a>";
                                }
                            }
                            if (defaultValueRestricted == "admins")
                                iconeRestricted = '<i class="fa fa-lock"></i>';
                            else if (defaultValueRestricted == "members")
                                iconeRestricted = '<i class="fa fa-user"></i>';

                            $pageList += ` <li  style="background-color:${status[0]}" data-toggle="tooltip"  title="${status[1]}">
                                                <div class="dropdown dropdown-visibility">
                                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">${iconeRestricted}
                                                    <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                      <li><a href="javascript:;" class="restricted-value btn ${defaultValueRestricted == 'all' ? 'disabled' : ''}" data-valu="all" data-key="${key}">${tradCms.forAll} <i class="fa fa-unlock"></i></a></li>
                                                      <li><a href="javascript:;" class="restricted-value btn ${defaultValueRestricted == 'admins' ? 'disabled' : ''}" data-valu="admins" data-key="${key}">${tradCms.forAdmin} <i class="fa fa-lock"></i></a></li>
                                                      <li><a href="javascript:;" class="restricted-value btn ${defaultValueRestricted == 'members' ? 'disabled' : ''}" data-valu="members" data-key="${key}">${tradCms.forMembers} <i class="fa fa-user"></i></a></li>
                                                    </ul>
                                                </div> 
                                                <span data-key="${key}" data-switch="page" data-type='${typePage}' class="editPageLeftPanel namePage">${pageName}</span> 
                                            <div class="dropdown dropdown-menu-action-page pull-right" id="dropdown-page-${key.replace("#", "")}" >
                                               <button class="btn dropdown-toggle btn-action-page" type="button" data-id="${key.replace("#", "")}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                               <ul class="dropdown-menu pull-right" aria-labelledby="${key}">`;
                            if(typePage != "anchorMenu"){
                                $pageList+= "<li><a href='"+((typePage == "externalLink") ? ""+value.urlExtern+"" : ""+key+"")+"' class='lbh-costumizer-edit-page show-page-btn tooltips' data-type='"+typePage+"' data-switch='page'><i class='fa fa-eye'></i> "+tradCms.showPage+"</a></li>";
                               }
                            $pageList += `       <li><a href="javascript:;" class="editPage " data-key="${key}" ><i class="fa fa-pencil "></i> ${trad.edit}</a></li>
                                                  <li><a href="javascript:;" class="duplicate-page tooltips" data-key="${key}"><i class="fa fa-copy"></i> Duplicate</a></li>`;
                            if (key!="#welcome" && typePage != "externalLink"){
                                $pageList+= 				"<li>"+btnPublie+"</li>";
                            }
                            if (value.hash == "#app.search"){
                                $pageList+= 				"<li><a class='tooltips' href=javascript:costumizer.actions.configFilterAnswers(`"+key+"`)><i class='fa fa-filter'></i> Configurer le filtre par réponse</a></li>";
                            }
                            if (key!="#welcome" && typePage != "anchorMenu"){
                                $pageList+=				"<li><a href='javascript:;' class='delete-page text-red' data-id='delete_one' id='"+key+"' ><i class='fa fa-trash '></i> "+trad.delete+"</a></li>";
                            }
                            $pageList += `    </ul>
                                            </div>
                                        </li>`;
                        })
                    $pageList +=  `</ul>`;

                }


                return  $pageList;
            },
            getArbre : function (object,type) {
                var $str = "";
                $.each(object, function(k, v) {
                    if (typeof v== "object" && typeof v["buttonList"] != "undefined") {
                        $str += ` <li data-toggle="collapse" data-target="${k}-${type}" class="collapsed"><a href="#"> ${k} <span class="arrow"></span></a> </li> 
                                  <ul class="sub-menu collapse" id="${k.replace("#", "")}-${type}">`;
                                $.each(v["buttonList"], function(e, val) {
                                    $str += `<li><a href="${e}" class="lbh"> ${e}</a></li>`;
                                });
                        $str += ` </ul>`;
                    }else  {
                        $str += ` <li><a href="${k}" class="lbh"> ${k}</a> </li>`;
                    }
                });
                return  $str;
            },
            getContainerPage: function(){
                return `<div id="list-page-container" style="width:100%; height:100%;"></div>`;
            }
        },
        events : {
            bind :  function(){
                $(".restricted-value").click(function(e) {
                    e.stopImmediatePropagation()
                    costumizer.pages.actions.visibility($(this).data("valu"), $(this).data("key"));
                });
                const maxLength = 18;
                $('#list-page-container .menu-content-page .namePage').each(function() {
                    const text = $(this).text();
                    if (text.length > maxLength) {
                        $(this).text(text.slice(0, maxLength) + '...');
                    }
                });
            },
            loadListPage : function () {
                //costumizer.pages.callbackPage = true;
                cmsConstructor.page.init();
                if (/*costumizer.pages.callbackPage && */$(".cmsbuilder-left-content").hasClass("active")) {
                    $(".btn-toggle-cmsbuilder-sidepanel[data-target='left']").trigger("click");
                    $(".btn-toggle-cmsbuilder-sidepanel-page").trigger("click");
                    //costumizer.pages.callbackPage = false;
                }

            }

        }
    },
    editor: {
        init:function(dom=""){
            this.views.init(dom)
            this.events.init()
        },
        actions:{
            generateCoInputs: function (kunik, spId, values, blockType, tabConfig) {

                var tabKey = tabConfig.key;
                var tabKeyPath = tabConfig["keyPath"] != "" ? tabConfig["keyPath"]+"." : "";
                var inputsForm = [];
                var blockConfigs = cmsConstructor.blocks[blockType];
                
                if (values == null) {
                    values = {}
                    values['icon'] = {}
                    values['css'] = {}
                    values['css']['hover'] = {}  
                } 

                if (typeof blockConfigs.beforeLoad == "function") {
                    blockConfigs.beforeLoad();
                }

                if (blockConfigs.configTabs[tabKey]["inputsConfig"].indexOf("addCommonConfig") >= 0) {
                    sliceIndex = blockConfigs.configTabs[tabKey]["inputsConfig"].indexOf("addCommonConfig");
                    inputsForm = blockConfigs.configTabs[tabKey]["inputsConfig"].slice(0, sliceIndex).concat(cmsConstructor.commonEditor[tabKey], blockConfigs.configTabs[tabKey]["inputsConfig"].slice(sliceIndex));
                } else {
                    inputsForm = blockConfigs.configTabs[tabKey]["inputsConfig"];
                }


                var onchange = function (path, valueToSet, name, payload, value) {
                    if (typeof value["translate"] != "undefined") {
                        path = name+"."+value["lang"];
                        var dataBlock = cmsConstructor.sp_params[cmsConstructor.spId];
                        if (typeof dataBlock[name] != "undefined") {
                            dataBlock[name]["languageSelected"] = value["lang"];
                        }
                    }

                    if (typeof cssHelpers.json[name] != "undefined" && cssHelpers.rejectFromCss.includes(name) == false) {
                        cmsConstructor.editor.actions.onChangeCss(cmsConstructor.kunik,cmsConstructor.spId,path, valueToSet, name, payload, value, tabKey ,cmsConstructor.sp_params[cmsConstructor.spId].css);
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "cssChange", 
                            data:{
                                kunik: cmsConstructor.kunik,
                                spId: cmsConstructor.spId,
                                path: path,
                                valueToSet: valueToSet,
                                name: name,
                                payload: payload,
                                value: value,
                                tabKey: tabKey,
                                mode: costumizer.mode,
                                css: cmsConstructor.sp_params[cmsConstructor.spId].css
                            } 
                        })
                    } else if (typeof blockConfigs.onChange == "function") {
                        blockConfigs.onChange(path, valueToSet, name, payload, value);
                    }

                    if (tabKey == "advanced" && ( name !=="otherClass" || name !=="otherCss")) {
                        cmsConstructor.helpers.onchangeAdvanced(name, cmsConstructor.kunik, value, cmsConstructor.spId);
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "advancedChange", 
                            data: {
                                kunik: cmsConstructor.kunik,
                                spId: cmsConstructor.spId,
                                value: value,
                                name: name
                            }
                        })
                    }

                    if( cmsConstructor.blocks != null && typeof cmsConstructor.blocks[blockType] != "undefined" && cmsConstructor.blocks[blockType] != null &&  typeof cmsConstructor.blocks[blockType].onchange == "function" ) 
                        cmsConstructor.blocks[blockType].onchange(path, valueToSet, name, payload, value);
                };
                
                var onblur = function (path, valueToSet, name, payload, value){
                    var newKey = jsonHelper.getValueByPath(payload[tabKey], name + ".path");
                    var sectionPath = typeof payload.sectionPath != "undefined" ? payload.sectionPath + "." : "";
                    var mode = (costumizer.mode != "md" && (typeof cssHelpers.json[name] != "undefined" && typeof cssHelpers.json[name].input != "undefined" && typeof cssHelpers.json[name].input.responsive != "undefined" && cssHelpers.json[name].input.responsive === true)) ? costumizer.mode + "." : "" ;
                    if (typeof newKey != "undefined" && newKey != "") {
                        path = newKey;
                    } else {
                        if ( tabKey == "general"){
                            if (typeof cssHelpers.json[name] != "undefined" && cssHelpers.rejectFromCss.includes(name) == false) {
                                    path = "css." + mode + sectionPath + name;
                            } else {
                                path = tabKeyPath + sectionPath + name;
                            } 
                        } else {
                            path = tabKeyPath + mode + sectionPath + name;
                        }
                    }

                    // up new way for transforming value 
                    if (typeof blockConfigs.configTabs[tabKey].processValue != "undefined" && typeof blockConfigs.configTabs[tabKey].processValue[name] == "function") {
                        value = blockConfigs.configTabs[tabKey].processValue[name](value);
                    }

                    if (typeof value["translate"] != "undefined") {
                        path = name+"."+value["lang"];
                        if (typeof value["text"] != "undefined") {
                            value = value["text"]; 
                        } else if (typeof value["pathImage"] != "undefined") {
                            value = value["pathImage"];
                        }
                    }

                    if (path != "advanced.otherCss") {
                        cmsConstructor.editor.actions.updateBlock(blockType, path, valueToSet, name, payload, value, tabKey);
                    } else if (path === "advanced.otherCss" && cmsConstructor.helpers.validateOtherCss(".newCss-" + kunik + " {" + value + "}", true) == true) {
                        cmsConstructor.editor.actions.updateBlock(blockType, path, valueToSet, name, payload, value, tabKey);
                    }
                    
                    if (tabKey == "advanced" && ( name ==="otherClass" || name ==="otherCss")) {
                        cmsConstructor.helpers.onchangeAdvanced(name, cmsConstructor.kunik, value, cmsConstructor.spId);
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "advancedChange", 
                            data: {
                                kunik: cmsConstructor.kunik,
                                spId: cmsConstructor.spId,
                                value: value,
                                name: name
                            }
                        })
                    }
                    
                }
                
                
                var payload = this.setPayload(inputsForm, tabKey, {})
                var defaultValue = this.setDefaultValueCoInput(inputsForm, values, tabKey, payload , tabConfig["keyPath"])
                cssHelpers.form.launch(parent.$("#" + tabKey), inputsForm, payload, defaultValue, {}, onchange , onblur);
            },
            onChangeCss: function(kunik,spId,path,valueToSet,name,payload,value,tabKey,css){
                var newKey = jsonHelper.getValueByPath(payload[tabKey], name + ".path");
                var sectionPath = typeof payload.sectionPath !== "undefined" ? payload.sectionPath : "";
                var childSelector = "";

                if (sectionPath !== "") {
                } else if (typeof newKey !== "undefined" && newKey !== "") {
                    sectionPath = newKey;
                }

                if (sectionPath !== "") {
                    childSelector = " ." + sectionPath.replace(/^css./, '').replace(/\./g, ' .');
                }
               
                var blockType = cmsConstructor.helpers.getBlockType(kunik,spId);

                if ( tabKey != "hover"){
                    if (typeof value == "object") {
                        $.each(value, function(keyChange, valueChange) {
                            cssHelpers.render.addStyleUI(kunik+"."+keyChange,valueChange,"",childSelector)
                        })
                    } else {
                        cssHelpers.render.addStyleUI(kunik+"."+name,value,"",childSelector)
                    }
                }

                if ( costumizer.mode != "md"){
                    $element = $("#responsiveScreen").contents().find("."+kunik+" "+childSelector);
                } else {
                    $element = $("."+kunik+" "+childSelector);
                }

                if ( blockType == "section" ) {
                    if ( name == "backgroundType"){
                        if ( value == "backgroundColor"){
                            $element.css('background-image' , 'none');
                            if ( typeof css.backgroundColor != "undefined" ){
                                $element.css('background-color' , cmsConstructor.sp_params[spId].css.backgroundColor);
                            } 
                        }else {
                            if ( typeof css.backgroundImage != "undefined"){
                                cssHelpers.json["backgroundUpload"].input.options.forEach(element => {
                                    if ( typeof css[element] != "undefined" && cssHelpers.rejectFromCss.includes(value) == false){
                                        $element.css(cssHelpers.json[element].property , css[element]);
                                    }
                                });
                            }
                        }
                        
                    } 


                    if ( name == "backgroundColor" || name == "backgroundImage" ){
                        if (!$('.empty-sp-element'+kunik).hasClass("d-none")){
                                $('.empty-sp-element'+kunik).addClass("d-none");
                                if (typeof cmsConstructor.sp_params[spId].blockParent == "undefined" && (typeof height != "undefined" || $element.css('height') != "max-content")){
                                    $element.css('height' , '250px');
                                }
                        }
                    
                    } 
                }
            },
            updateBlock:function(blockType,path,valueToSet,name,payload,value,tabKey,afterUpdate=""){
                var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId)
                ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId]);
                var olderDataChanged = {};
                var newValueToSet = {};
                tplCtx = {};
                tplCtx.id = cmsConstructor.spId;
                tplCtx.collection = "cms";
                tplCtx.value = {};
                tplCtx.path =  path;
                tplCtx.saveLog = {
                    costumId : costum.contextId,
                    action : "costum/editBlock",
                    blockName : cmsConstructor.sp_params[tplCtx.id].path.split(".").pop(),
                    path : path,
                    blockPath : cmsConstructor.sp_params[tplCtx.id].path,
                    page : page,
                    keyPage : location.hash == "" ? "#welcome" : location.hash,
                    blockType : blockType,
                    kunik : cmsConstructor.kunik
                }


                var keypath = tabKey == 'hover' ? "css.hover." : "css.";
                
                if (typeof value != "object"){
                    tplCtx.value = value;
                    
                    jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, value);
                    jsonHelper.setValueByPath(newValueToSet, tplCtx.path, value);
                    if (name == "iconStyle"){
                        tplCtx.value = "fa fa-"+value;
                        jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, "fa fa-"+value);
                        jsonHelper.setValueByPath(newValueToSet, tplCtx.path, "fa fa-"+value);
                    }

                    var ancientValue = typeof jsonHelper.getValueByPath(ancientData, tplCtx.path) == "undefined" ? "" : jsonHelper.getValueByPath(ancientData, tplCtx.path);
                    jsonHelper.setValueByPath(olderDataChanged, tplCtx.path, ancientValue);

                } 
                else if (typeof value == "object"){
                    if( typeof cssHelpers.json[name] != "undefined" && cssHelpers.rejectFromCss.includes(name) == false ){
                    
                        tplCtx.path = "css" ;

                        if (path.includes("css.hover.")) {
                            path = path.replace("css.hover.", keypath);
                        }else if (path.includes("css.")) {
                            path = path.replace("css.", keypath);
                        }
                        
                          
                        $.each(value, function(key,val){
                            var eachPath = path.split(".");
                            eachPath[eachPath.length - 1] = key;
                            var finalPath = eachPath.join(".");
                            jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], finalPath, val);
                            var ancientValue = typeof jsonHelper.getValueByPath(ancientData, finalPath) == "undefined" ? "" : jsonHelper.getValueByPath(ancientData, finalPath);
                            if ( ancientValue !== val ){
                                jsonHelper.setValueByPath(olderDataChanged, finalPath, ancientValue);
                                jsonHelper.setValueByPath(newValueToSet, finalPath, val);
                            }
                        });

                        //remove entries with value null 
                        var objCleanedToSave = cmsConstructor.helpers.removeUnnecessaryEntriesBeforeSave(cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId].css));
                        tplCtx.value = objCleanedToSave ;

                    } else {

                        //remove entries with value null 
                        var objCleanedToSave = cmsConstructor.helpers.removeUnnecessaryEntriesBeforeSave(value);

                        tplCtx.value = objCleanedToSave;

                        var ancientValue = typeof jsonHelper.getValueByPath(ancientData, tplCtx.path) == "undefined" ? "unset" : jsonHelper.getValueByPath(ancientData, tplCtx.path);
                        jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], tplCtx.path, value);
                        jsonHelper.setValueByPath(olderDataChanged, tplCtx.path, ancientValue);
                        jsonHelper.setValueByPath(newValueToSet, tplCtx.path, value);
                    }
                }   

                tplCtx.saveLog["value"] = newValueToSet;
                tplCtx.saveLog["olderData"] = olderDataChanged;


                dataHelper.path2Value(tplCtx, function (params) {
                    toastr.success(tradCms.editionSucces);

                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "updateBlockData", 
                        data:  { id : cmsConstructor.spId , newData : cmsConstructor.sp_params[cmsConstructor.spId]  }
                    })

                    if ( typeof cmsConstructor.block.events.afterUpdate[afterUpdate] == "function"){
                        cmsConstructor.block.events.afterUpdate[afterUpdate](valueToSet,name,payload,value,tplCtx.id,tplCtx.path,cmsConstructor.kunik);
                    }else{
                        if(typeof cmsConstructor.blocks[blockType].afterSave == "function")
                            cmsConstructor.blocks[blockType].afterSave(path,valueToSet,name,payload,value,tplCtx.id,tplCtx.path);
    
                        // Pingrefresh AfterSave custom block 
                        if( $(`.cmsbuilder-block[data-id="${tplCtx.id}"]`).data("blocktype") === "custom"){
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "refreshBlock", 
                                data:{ id: tplCtx.id}
                            })
                        }
                    }
                    

                    //local storage management 
                    cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                    var actualPage = location.hash == "" ? "#welcome" : location.hash;
                    var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                    ctrlY[actualPage] = [];
                    localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));

                    delete tplCtx.saveLog;
                });
                
            },
            setDefaultValueCoInput : function(inputs, blockValues, tabKey , payload , tabKeyPath ){
                valueToSend={};
                const valueToUse = JSON.parse(JSON.stringify(blockValues));
                var mode = costumizer.mode == "md" ? "" : costumizer.mode;

                // Need a right place to initialize dropdown views mode value
                $(".cmsbuilder-right-content").find(".all-views-mode").val(costumizer.mode);
                
                // const propertiesToUpdate = ["link", "image"];

                // propertiesToUpdate.forEach(property => {
                //     if (typeof valueToUse[property] === "object") {
                //         valueToUse["save" + property.charAt(0).toUpperCase() + property.slice(1)] = valueToUse[property];
                //         if (typeof valueToUse[property] !== "undefined" && typeof valueToUse[property][userConnected.language] !== "undefined") {
                //             valueToUse[property] = valueToUse[property][userConnected.language];
                //         } else {
                //             valueToUse[property] = valueToUse[property]["default"];
                //         }
                //     }
                // });

                if ( typeof valueToUse.css == "undefined"){
                    valueToUse.css = {};
                }

                if ( typeof valueToUse.css.hover == "undefined"){
                    valueToUse.css.hover = {};
                }

                    
                
                    var { sm,xs,hover, ...lgData } = valueToUse['css'];
                    smData = typeof valueToUse['css']['sm'] != "undefined" ? valueToUse['css']['sm'] : {};
                    xsData = typeof valueToUse['css']['xs'] != "undefined" ? valueToUse['css']['xs'] : {};
                    mergedLgSmData =  Object.assign({}, lgData, smData);
                    
                    var { sm,xs, ...lgHoverData } = valueToUse['css']['hover'];
                    smHoverData = typeof valueToUse['css']['hover'] != "undefined" && typeof valueToUse.css['hover']['sm'] != "undefined" ? valueToUse.css['hover']['sm'] : {};
                    xsHoverData = typeof valueToUse['css']['hover'] != "undefined" && typeof valueToUse.css['hover']['xs'] != "undefined" ? valueToUse.css['hover']['xs'] : {};
                    mergedHoverLgSmData =  Object.assign({}, lgHoverData, smHoverData);

                    var { sm,xs, ...advancedOtherCssData } = valueToUse['advanced'] ?? {};
                    smAdvanceOtherCssData = typeof valueToUse['advanced'] != "undefined" && typeof valueToUse.advanced['sm'] != "undefined" && typeof valueToUse.advanced['sm']['otherCss'] != "undefined" ? valueToUse.advanced['sm']['otherCss'] : {};
                    xsAdvancedOtherCssData = typeof valueToUse['advanced'] != "undefined" && typeof valueToUse.advanced['xs'] != "undefined" && typeof valueToUse.advanced['xs']['otherCss'] != "undefined" ? valueToUse.advanced['xs']['otherCss'] : {};
                    mergedAdvancedOtherCssLgSmData =  Object.assign({}, advancedOtherCssData, smAdvanceOtherCssData);

                    if (tabKey == "hover"){
                        if ( mode != "" ){
                            valueToSend = (typeof valueToUse.css.hover != "undefined" && typeof valueToUse.css.hover[mode] != "undefined" ) ? valueToUse.css.hover[mode] : {} ;
                            switch (mode) {
                                case "xs":
                                    valueToSend =  Object.assign({}, mergedHoverLgSmData, xsHoverData);
                                    break;
                                case "sm":
                                    valueToSend =  mergedHoverLgSmData;
                                    break;
                                default:
                                    valueToSend = (typeof valueToUse.css.hover != "undefined" && typeof valueToUse.css.hover[mode] != "undefined" ) ? valueToUse.css.hover[mode] : {} ;
                            }
                        } else {
                            valueToSend = valueToUse.css['hover'] ? valueToUse.css['hover'] : {} ;
                        }
                    } else if( tabKey =="style" ){

                        if ( mode != ""){
                            switch (mode) {
                                case "xs":
                                    valueToSend =  Object.assign({}, mergedLgSmData, xsData);
                                    break;
                                case "sm":
                                    valueToSend =  mergedLgSmData;
                                    break;
                                default:
                                    valueToSend = valueToUse['css'][mode] ? valueToUse['css'][mode]:{};
                            }
                        } else {
                            valueToSend = valueToUse.css ? valueToUse.css : {} ;
                        }
                    } else if ( tabKey != "general"){
                        inputs.forEach(function callback(value, index) {

                            valueKey = cmsConstructor.helpers.getCoInputName(value);

                            if( typeof value == "object" && typeof value["payload"] != "undefined"  && typeof value["payload"]["path"] != "undefined" ) {
                                jsonHelper.setValueByPath(valueToUse, tabKey+"."+valueKey, jsonHelper.getValueByPath(valueToUse, value["payload"]["path"]));
                            } else if ( typeof value == "string" && typeof cssHelpers.json[valueKey] != "undefined" && typeof cssHelpers.json[valueKey]["input"] != "undefined" && typeof cssHelpers.json[valueKey]["input"]["responsive"] != "undefined" && cssHelpers.json[valueKey]["input"]["responsive"] == true) {
                                if ( mode != "" ){
                                    jsonHelper.setValueByPath(valueToUse, tabKey+"."+valueKey, jsonHelper.getValueByPath(valueToUse, tabKey+"."+mode+"."+valueKey));
                                } else {
                                    jsonHelper.setValueByPath(valueToUse, tabKey+"."+valueKey, jsonHelper.getValueByPath(valueToUse, tabKey+"."+valueKey));
                                }
                            }

                        });
                        valueToSend = jsonHelper.getValueByPath(valueToUse, tabKeyPath);

                    } else {
                        valueToSend = valueToUse;
                        inputs.forEach(function callback(value, index) {
                            valueKey = cmsConstructor.helpers.getCoInputName(value);

                            // if ( typeof value == "string" ) {
                            //     valueKey =  value;
                            // } else if ( typeof value == "object" && typeof value["options"]["name"] != "undefined"  ) {
                            //     valueKey = value["options"]["name"];
                            // }
                            
                            if( typeof value == "object" && typeof value["payload"] != "undefined"  && typeof value["payload"]["path"] != "undefined" ) {
                                valueToSend[valueKey] = jsonHelper.getValueByPath(valueToUse, value["payload"]["path"]);
                            } else if (typeof cssHelpers.json[valueKey] != "undefined" && cssHelpers.rejectFromCss.includes(valueKey) == false  ) {
                                if ( mode != "" ){
                                    cssToUse = {};
                                    switch (mode) {
                                        case "xs":
                                            cssToUse =  Object.assign({}, mergedLgSmData, xsData);
                                            break;
                                        case "sm":
                                            cssToUse =  mergedLgSmData;
                                            break;
                                    }
                                    valueToSend[valueKey] = typeof cssToUse[valueKey] != "undefined" ?  cssToUse[valueKey] : "";
                                } else {
                                    valueToSend[valueKey] = typeof  valueToUse.css[valueKey] != "undefined" ?  valueToUse.css[valueKey] : "";
                                }
                            } else if ( valueKey != ""){
                                valueToSend[valueKey] = typeof valueToUse[valueKey] != "undefined" ? valueToUse[valueKey] : "";
                            }
                        })   
                    }

                return valueToSend;
            },
            setPayload : function(inputs,tabKey,payload){
                inputs.forEach(function callback(value, index) {
                    valueKey = cmsConstructor.helpers.getCoInputName(value);
                    if ( typeof value == "object" && value["type"] == "section" ){
                        cmsConstructor.editor.actions.setPayload(value["options"]["inputs"],tabKey,payload);
                    } else {
                        if( typeof value == "object" && typeof value["payload"] != "undefined" ) {
                            jsonHelper.setValueByPath(payload, tabKey+"."+valueKey, value["payload"]);
                        } else {
                            jsonHelper.setValueByPath(payload, tabKey+"."+valueKey, "");
                        }
                    }
                    
                });
                return payload;
            },
            setDefaultValueTextEditor:function(styles){
                Object.keys(styles).forEach(function(property){
                    $(`.cms-text-editor-actions button[data-property="${property}"][data-value="${styles[property]}"]`).addClass("active")
                })

                setTimeout(function(){
                   if (window.getSelection()) {
                    var selectedText = window.getSelection().getRangeAt(0).startContainer.parentNode
                    var parentStyle = window.getComputedStyle(selectedText);
                    if (parentStyle.color){
                        $("#myColor").spectrum("set", parentStyle.color);
                    }else if(parentStyle.style.color){
                        let colorRGB = parentStyle.style.color;
                        let hexColor = cmsConstructor.helpers.colorRGBtoHex(colorRGB)   
                        $("#myColor").spectrum("set", hexColor);
                    }else{
                        $("#myColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(cmsConstructor.textColor));
                    }
                    if (parentStyle.backgroundColor) {
                        $("#myBgColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(parentStyle.backgroundColor));
                    }/*else{
                        $("#myBgColor").spectrum("set", cmsConstructor.helpers.colorRGBtoHex(cmsConstructor.helpers.InheritedBackgroundColor(cmsConstructor.selectedDom).bgTextColor));
                    }*/

                    if (parentStyle.fontFamily){
                        var computedFontF = parentStyle.fontFamily
                        computedFontF = computedFontF.replace(/"/g, '')
                        $("#input-font").val(computedFontF)
                    }else{
                        $("#input-font").val("Arial")
                    }

                    if (parentStyle.textAlign) {
                        var parentTextAlign = (parentStyle.textAlign)[0].toUpperCase() + (parentStyle.textAlign).slice(1)
                        if (parentTextAlign == "Full") {
                            parentTextAlign = "justify"
                        }
                        parentTextAlign = parentTextAlign.toLowerCase()
                        $(`.cms-text-editor-actions[data-action="align"] li button[data-value='${parentTextAlign}']`).addClass("active")
                    }
                    
                    if (parentStyle.fontWeight == "700"){
                        $("#fontSize").val(parentStyle.size)
                        $(`.cms-text-editor-actions li button[data-value="700"]`).addClass("active")
                    }
                    
                    if (parentStyle.fontStyle == "italic"){
                        $(`.cms-text-editor-actions li button[data-value="italic"]`).addClass("active")
                    }
                    
                    if (parentStyle.textDecorationLine =="line-through"){
                        $(`.cms-text-editor-actions li button[data-value="line-through"]`).addClass("active")
                    }
                    if (parentStyle.textDecorationLine.includes("underline")){
                        $(`.cms-text-editor-actions li button[data-value="underline"]`).addClass("active")
                    }
                    // if (parentStyle.textDecorationLine == "line-through"){
                        // mylog.log("textDecoration",parentStyle.textDecorationLine)
                    //     $(`.cms-text-editor-actions li button[data-property="underline"]`).addClass("active")
                    // }
                    //else{
                    //     $("#fontSize").val(14)
                    // }

                    if (parentStyle.fontSize){
                        $("#fontSize").val(parseInt(parentStyle.fontSize));
                    }else{                    
                        $("#fontSize").val(14);
                    }

                    if (typeof cmsConstructor.sp_params[cmsConstructor.spId].language != "undefined") {
                        var langSelected = cmsConstructor.sp_params[cmsConstructor.spId].language;
                        $(".text-btn-flag-translate-active").html(`<span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}`);
                        $(".text-translate-form-select button.dropdown-item").removeClass("active").removeAttr("disabled");
                        $(`.text-translate-form-select button.dropdown-item[data-value='${langSelected}']`).addClass("active").attr("disabled", "disabled");
                    }
                }
                })
            },
            // setStyleHover: function(css){
            //     $("#hover"+cmsConstructor.kunik).remove();
            //     var element  = document.createElement("style");
            //     element.id = "hover"+cmsConstructor.kunik;
            //     if(cmsConstructor.kunik.startsWith('button')){
            //         var cssText = `.btn-${cmsConstructor.kunik}:hover{`;
            //     }else{
            //         var cssText = `.${cmsConstructor.kunik}:hover{`;
            //     }
            //     if(cmsConstructor.kunik.startsWith("button")){
            //         cssText += `
            //         font-size: ${css['hover']['font-size']} !important;
            //         color: ${css['hover']['color']} !important;
            //         height:${css.size.height == "auto" ? "" : css.size.height} !important;`;
            //     }

            //     cssText += `width: ${css.size.width} !important;
                    
            //         background-color: ${css.hover.background.color} !important;
            //         border-style: ${css["hover"]["border"]["type"]} !important;
            //         border-color: ${css["hover"]["border"]["color"]} !important;
            //         border-width: ${css["hover"]["border"]["width"]}px !important;
            //         border-top-width: ${css["hover"]["border"]["top"]}px !important;
            //         border-right-width: ${css["hover"]["border"]["right"]}px !important;
            //         border-bottom-width: ${css["hover"]["border"]["bottom"]}px !important;
            //         border-left-width: ${css["hover"]["border"]["left"]} !importantpx;
            //         border-radius: ${css["hover"]["border"]["radius"]["top-left"]}px ${css["hover"]["border"]["radius"]["top-right"]}px ${css["hover"]["border"]["radius"]["bottom-right"]}px ${css["hover"]["border"]["radius"]["bottom-left"]}px !important; 
            //         padding: ${css["hover"]["padding"]["top"]} ${css["hover"]["padding"]["right"]} ${css["hover"]["padding"]["bottom"]} ${css["hover"]["padding"]["left"]} !important;
            //         margin: ${css["hover"]["margin"]["top"]} ${css["hover"]["margin"]["right"]} ${css["hover"]["margin"]["bottom"]} ${css["hover"]["margin"]["left"]} !important;
            //         -webkit-box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //         -moz-box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //         box-shadow: ${css["hover"]["box-shadow"]["inset"]} ${css["hover"]["box-shadow"]["color"]} ${css["hover"]["box-shadow"]["x"]}px ${css["hover"]["box-shadow"]["y"]}px ${css["hover"]["box-shadow"]["blur"]}px ${css["hover"]["box-shadow"]["spread"]}px !important;
            //     }`;
            //     element.innerHTML = cssText;
            //     if(cmsConstructor.kunik.startsWith('button')){
            //         $(element).insertBefore(".btn-"+cmsConstructor.kunik);
            //     }else{
            //         $(element).insertBefore("."+cmsConstructor.kunik);
            //     }
            // }
        },
        views: {
            init: function (dom="") {
                var kunik = cmsConstructor.kunik,
                    spId = cmsConstructor.spId,
                    values = cmsConstructor.sp_params[spId] ? cmsConstructor.sp_params[spId]:null;

                var blockType = cmsConstructor.helpers.getBlockType(kunik,spId);
                cmsConstructor.panelConfig = [];
                
                if( typeof cmsConstructor.blocks[blockType] != "undefined" ){ 
                    defaultPanelConfig = {
                        general :  {
                            key: "general",
                            keyPath: "",
                            icon: "sliders",
                            label: tradCms.general,
                        },
                        style : {
                            key: "style",
                            icon: "paint-brush",
                            keyPath: "css",
                            label: tradCms.style,
                        },
                        hover : {
                            key: "hover",
                            icon: "mouse-pointer",
                            keyPath: "css.hover",
                            label: tradCms.styleHover,
                            content: this.tabConstructViews(kunik, spId, values,blockType,"hover")                 
                        },
                        advanced : {
                            key: "advanced",
                            icon: "cogs",
                            keyPath: "advanced",
                            label: tradCms.advanced,
                        }
                    }

                    tabsOptions = {
                        onInitialized : function(){
                            costumizer.actions.cancel("#menu-right-costumizer");
                        }
                    };
                    

                    const blockConfig = cmsConstructor.blocks[blockType].configTabs;

                    for (const key in blockConfig) {
                        const tabConfig = blockConfig[key];

                        if (!tabConfig.hasOwnProperty("content")) {
                            tabConfig.content = this.tabConstructViews(kunik, spId, values, blockType, key);
                        }

                        if (defaultPanelConfig.hasOwnProperty(key)) {
                            for (const subKey in defaultPanelConfig[key]) {
                                if (!tabConfig.hasOwnProperty(subKey)) {
                                    tabConfig[subKey] = defaultPanelConfig[key][subKey];
                                }
                            }
                        }

                        cmsConstructor.panelConfig.push(tabConfig);
                    }



                    if (cmsConstructor.panelConfig){
                        parent.$(".cmsbuilder-right-content").addClass("active");
                        costumizer.actions.tabs.init(dom, cmsConstructor.panelConfig , tabsOptions);
                    }

                    $.each(cmsConstructor.panelConfig, function(e, v){
                        cmsConstructor.editor.actions.generateCoInputs(kunik, spId, values,blockType,v);
                    });

                    if (blockType == "text"){
                        parent.$(".cmsbuilder-tab-section #general").html(cmsConstructor.editor.views.inputsText(cmsConstructor.kunik,cmsConstructor.spId)) 
                        if( cmsConstructor.sp_params[spId].path !== "tpls.blockCms.superCms.elements.supertext"){
                            parent.$(".cmsbuilder-tabs-header").find("li[data-key='style']").remove();
                            parent.$(".cmsbuilder-tabs-header").find("li[data-key='advanced']").remove();
                        }
                    } 
                    if (blockType == "image" && typeof costum.useOpenAI == "undefined") {
                        let viewInfoOpenAI = `
                        <div class="info-for-use-open-ai" style="margin-top: 30px">
                            <a><i class="fa fa-info"></i>${tradCms.useOpenAIGenerateImage}</a>
                        </div>`;
                        parent.$(".cmsbuilder-tab-section #general").append(viewInfoOpenAI);
                        $(".info-for-use-open-ai a").off("click").on("click", function(e) {
                            e.stopImmediatePropagation();
                            $(".lbh-costumizer[data-space='generalInfos']").trigger( "click");
                        })
                    }
                }
                
            },
            tabConstructViews: function (kunik, spId, values , blockType , tabKey) {
                
                var $container = $(`<div class="col-md-12 sp-cms-options dark" id="${tabKey}" ></div>`);
                return $container;

            },
            inputsText: function (kunik, spId, values = null) {
                var dropdownLang = "",
                    langSelected = costum.langCostumActive || "fr",
                    generateID = Math.random().toString(36).slice(-6),
                    tradAuto = false,
                    allLanguagesSelectedInDb = dataHelper.getAllLanguageInDb(),
                    allLanguages = {};

                for (var i = 0; i < allLanguagesSelectedInDb.length; i++) {
                    var keyLang = allLanguagesSelectedInDb[i].toUpperCase();
                    if (themeParams.DeeplLanguages.hasOwnProperty(keyLang)) {
                        allLanguages[keyLang] = themeParams.DeeplLanguages[keyLang];
                    }
                }
                
                $.each(allLanguages, function (langkey, langValue) {
                    dropdownLang +=  `<button class="btn dropdown-item${ (langSelected === langkey.toLowerCase()) ? ' active': '' }" type="button" data-value="${langkey.toLowerCase()}"${ (langSelected === langkey.toLowerCase()) ? ' disabled': '' }><span style="font-size: 20px; margin-right: 10px">${langValue["icon"]}</span> ${tradCms[langValue["label"]]}</button>`
                })
                var markdownSwitcher = `
                    <div class="form-group input-group-sm  switch ">                
                        <label class="switch-text-label" for="activateMarkdown">Markdown</label>                        
                        <input type="checkbox" name="hideOnDesktop" class="form-control switch co-input-inputSimple-input-checkbox active-markdown" value="checkboxValue" id="activateMarkdown">
                        <label for="activateMarkdown"></label>
                    </div>`

                        var markdownOptions = '<option value="">'+tradCms.selectAnOption+'</option>';
                        $.each(costum.availableNote, function (noteKey, noteValue) {
                            markdownOptions += `<option value="${noteKey}">${noteValue.name}</option>`
                        })
                   

                var markdownNote = `                    
                    <div class="form-group input-group-sm  switch ">                
                        <label class="switch-text-label" for="activateAliasMD">Alias depuis un note</label>                        
                        <input type="checkbox" name="hideOnDesktop" class="form-control switch co-input-inputSimple-input-checkbox active-alias-markdown" value="checkboxValue" id="activateAliasMD">
                        <label for="activateAliasMD"></label>
                    </div>
                    <div class="form-group input-group-sm alias-md-options hidden">
                        <select class="alias-md form-control bg-dark">${markdownOptions}</select>              
                    </div>`
                    markdownSwitcher += `<div class='alias-note-editor hidden' ${(Object.keys(costum.availableNote).length > 0) ? "" : "style='display:none !important;'"} >${markdownNote}</div>`;
                var $inputsText = $(`
                  ${ cmsConstructor.sp_params[cmsConstructor.spId].path == 'tpls.blockCms.superCms.elements.supertext' ? markdownSwitcher : ''}
                    <div class="cms-text-editor">
                        <div class="cms-text-editor-header">
                            ${(Object.keys(allLanguages).length > 1) ? `
                            <div class="cms-text-editor-group cms-text-editor-for-select-language">
                                <label>Languages</label>
                                <div class="dropdown translate-form">
                                    <button class="btn btn-primary dropdown-toggle text-btn-flag-translate-active" type="button" id="dropdownForTranslate" data-toggle="dropdown">
                                        <span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownForTranslate">
                                        <div class="text-translate-form-select">
                                            <div class="language-props switch">
                                                <label for="languageProps_${generateID}" class="label-for-language-props_${generateID}">Traduction automatique</label>
                                                <input id="languageProps_${generateID}" type="checkbox" name="inputInset" class="form-control language-props-input">
                                                <label for="languageProps_${generateID}"></label>
                                            </div>
                                            ${dropdownLang}
                                        </div>
                                    </div>
                                </div>
                            </div>` : ''}
                            <div class="cms-text-editor-group">
                                <div class="cms-text-editor-form cms-text-editor-form-font">
                                    <select class="cms-text-editor-form-control" id="input-font">
                                    </select>
                                </div>
                                <div class="cms-text-editor-form ajout-new-font">
                                    <button class="btn btn-add-new-font btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="cms-text-editor-form">
                                    <select class="cms-text-editor-form-control" id="fontSize">
                                    </select>
                                </div>
                                <div class="cms-text-editor-form">
                                    <div class="cms-text-editor-form-color">
                                        <input type="color" id="myColor">
                                    </div>
                                </div>
                                <div class="cms-text-editor-form hidden">
                                    <div class="cms-text-editor-form-color">
                                        <span><i class="fa fa-font" aria-hidden="true"></i></span>
                                        <input type="color" id="myBgColor">
                                    </div>
                                </div>
                            </div>
                            <div class="cms-text-editor-group">
                                <ul class="cms-text-editor-actions" data-action="style">
                                    <li><button data-property='font-style' data-value='italic'><i class="fa fa-italic" aria-hidden="true"></i></button></li>
                                    <li><button data-property='font-weight' data-value='700'><i class="fa fa-bold" aria-hidden="true"></i></button></li>
                                    <li>
                                    <button class="hidden" data-property='text-decoration' data-value='line-through'><i class="fa fa-strikethrough" aria-hidden="true"></i></button>
                                    </li>
                                </ul>
                                <ul class="cms-text-editor-actions" data-action="align">
                                    <li><button data-property='text-align' data-value='left'><i class="fa fa-align-left" aria-hidden="true"></i></button></li>
                                    <li><button data-property='text-align' data-value='center'><i class="fa fa-align-center" aria-hidden="true"></i></button></li>
                                    <li><button data-property='text-align' data-value='right'><i class="fa fa-align-right" aria-hidden="true"></i></button></li>
                                    <li><button data-property='text-align' data-value='justify'><i class="fa fa-align-justify" aria-hidden="true"></i></button></li>
                                </ul>
                                <ul class="cms-text-editor-actions">
                                    <li><button onclick="cmsConstructor.block.actions.textLinkInsertion('[label](http://www)')"><i class="fa fa-link" aria-hidden="true"></i></button></li>
                                </ul>

                                <ul class="text-white">
                                    <button role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseguide" aria-expanded="false" aria-controls="collapseguide" class="">
                                       <i class="fa fa-question"></i>
                                    </button>
                                </ul>
                            </div>

                            <div>
                                <div id="collapseguide" class="panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <div class="panel-body">
                                        Guide pour l'utilisation de lien.<br>
                                        Pour ajouter un lien, il faut cliquez sur <button onclick="cmsConstructor.block.actions.textLinkInsertion('[label](http://www)')"><i class="fa fa-link" aria-hidden="true"></i></button> ou utiliser le syntax <code>[label](url)</code><br>Voici les exemples:<br>
                                        - Pour le lien externe ou autre lien.
                                            <pre>[Voir sur communecter](https://www.communecter.org)</pre>
                                            <pre>[Contact](mailto:ifa@gmail.com)</pre>
                                        - Pour le lien lbh, il faut ajouter <code >#</code> + slug de la page.<br>
                                        Exemple pour les pages <br>
                                        <code>#welcome</code> et <code>#about</code><br>
                                            <code>[Home](#welcome)</code> ou <br>
                                            <code>[A propos](#about)</code><br>
                                        - Pour le preview, 
                                        <br> il faut ajouter <code>#?preview=</code> avant l'url<br>
                                        <div style="background-color:#f9f2f4">
                                            <pre>[Voici le detail](#?preview=page.type.projects.id.<b>id_element)</b></pre>
                                        </div>
                                        Dont : <br>
                                        * <b>projet</b>: type de l'element.<br>
                                        * <b>id_element</b>: ID de l'element à prevoir.<br>
                                    </div>
                                </div>
                            </div>
                            ${(typeof costum.useOpenAI == "undefined") ? `
                            <div class="info-for-use-open-ai">
                                <a><i class="fa fa-info"></i>${tradCms.useOpenAIAutocompletionTexte}</a>
                            </div>` : ''}
                        </div>
                    </div>
                `)
                for (var i = 5; i < 501; i++) {
                    $inputsText.find("#fontSize").append("<option value='"+i+"'>"+i+"px</option>")
                }

                var optionsForSelectFont = "";
                if (notEmpty(costum.fonts)) {
                    $.each(costum.fonts, function(key, value) {
                        if ( typeof fontObj[value] !== "undefined" ) {    
                            optionsForSelectFont += `<option style="font-family:'${fontObj[value].split(' ').join('_')}'" value="${fontObj[value].split(' ').join('_')}">${fontObj[value]}</option>`;
                        }
                    })
                } else {
                    optionsForSelectFont += `<option selected>${tradCms.nofontsSelectPleaseChoose || "Aucune police sélectionnée, veuillez ajouter des polices"}</option>`;
                }

                $inputsText.find("#input-font").append(optionsForSelectFont);

                $inputsText.find(`.language-props .label-for-language-props_${generateID}, .language-props #languageProps_${generateID}`).click(function() {
                    $(this).closest(".translate-form").addClass("open");
                    tradAuto = ($(this).is(":checked")) ? true : false;
                })
                
                $inputsText.find(".text-translate-form-select button.dropdown-item").click(function() {
                    langSelected = $(this).data("value");
                    field = $(cmsConstructor.selectedDom).data('field');
                    $inputsText.find(".text-translate-form-select button.dropdown-item").removeClass("active").removeAttr("disabled");
                    $(this).addClass("active").attr("disabled", "disabled");
                    cmsConstructor.selectedDom.attr("data-lang", langSelected);
                    cmsConstructor.sp_params[cmsConstructor.spId].language = langSelected;
                    if (tradAuto) {
                        var textTotranslate = cmsConstructor.helpers.removeAllBaliseHtml(cmsConstructor.selectedDom.html());
                        new Promise(function(resolve, reject) {
                            cmsConstructor.helpers.autoTranslate(textTotranslate, langSelected, "translationLabel", function(error, translatedText) {
                                if (error) {
                                    reject(error); 
                                } else {
                                    resolve(cmsConstructor.selectedDom.html(translatedText));
                                    cmsConstructor.block.actions.textChanged(field);
                                    tradAuto = false;
                                    $inputsText.find(`.language-props #languageProps_${generateID}`).prop('checked', false);
                                }
                            });
                        });
                    } else {
                        var parent = cmsConstructor.sp_params[cmsConstructor.spId];
                        if (typeof parent[cmsConstructor.selectedDom.data("field")] != "undefined" && parent[cmsConstructor.selectedDom.data("field")][langSelected]) {
                            cmsConstructor.selectedDom.html(parent[cmsConstructor.selectedDom.data("field")][langSelected]);
                        }
                    }
                    $inputsText.find(".text-btn-flag-translate-active").html(`<span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langSelected.toUpperCase()]["icon"]}</span> ${langSelected}`);
                })

                $inputsText.find(".info-for-use-open-ai a").click(function() {
                    $(".lbh-costumizer[data-space='generalInfos']").trigger( "click");
                })

                $inputsText.find(".btn-add-new-font").click(function() {
                    $(".lbh-costumizer[data-space='fonts']").trigger('click');
                })

                return $inputsText;
            },
            /*historyContent : function (idBlock) {
                str = `<div class="cmsbuilder-tabs-header">
                        <ul>
                            <li data-key="history" class="active">
                                <span><i class="fa fa-history" aria-hidden="true"></i></span>
                                <span>${tradCms.blockHistory}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="history-content-list" style="overflow-y: auto;height: 100%;">
                        <ul class="media-list padding-10 margin-bottom-50" id="historyList">
                        </ul>
                    </div>`;
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cms/getlog",
                    {
                        costumId    : costum.contextId,
                        idBlock     : idBlock
                    },
                    function(data) {
                        if ( data && data.result ) {
                            var strList = '';
                            $.each(data.items, function(k, v) {
                                var iconLeft = "cog";
                                if (v.action == "costum/editBlock")
                                    iconLeft = "edit text-green"
                                else if (v.action == "costum/deleteBlock")
                                    iconLeft = "trash-o text-red"
                                else if (v.action == "costum/addBlock")
                                    iconLeft = "plus-circle text-blue"

                                if (v.action == "costum/editBlock") {
                                    viewDetail = "<span>" + tradCms.key + " :</span> " + v.idBlock + "<br><span>"+trad.collection+" :</span> " + v.collection + "<br> <span>"+tradCms.path+" :</span> " + v.path + "<br> <span>"+tradCms.value+"  :</span> <pre>"+ JSON.stringify(v.value)+"</pre>" ;
                                }else if (v.action == "costum/deleteBlock" || v.action == "costum/addBlock") {
                                    viewDetail = "<span>" + tradCms.key + " :</span> " + v.idBlock + "<br><span>" + trad.collection + " :</span> " + v.collection;
                                    viewDetail += "<br><span>"+tradCms.path+" :</span> " + v.blockPath;
                                }
                                strList +=
                                    `
									<li class="list-group-item">
										<div class="media">
											<i class="fa fa-${iconLeft} pull-left"></i>
											<div class="media-body">
												<span class="name">${v.blockName}</span> 
												<span class="label label-info">#${v.page}</span>
												<span class="number pull-right">${directory.showDatetimePost(null, null, v.created)}</span>
												<p class="info"> 
													<i class="fa fa-user"></i> ${v.userName} <br> 
													<i class="fa fa-edit"></i> ${v.action}
												</p>
											</div>
										</div>
										<div class="panel panel-info">
											<div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse${v.created}">
											  <h3 class="panel-title">
												${tradBadge.viewDetails}
											  </h3>
											</div>
											<div id="collapse${v.created}" class="collapse">
											  <div class="panel-body">
												<p class="info">
												  ${viewDetail}
												</p>
											  </div>
											</div>
										</div>
									</li>
									`;
                            });
                            $('.history-content-list #historyList').append(strList);
                            // toastr.success(data.msg);
                        }
                    })
                return str;
            }*/
        },
        events: {
            init: function () {
                $.each(cmsConstructor.editor.events, function (key, eventHandler) {
                        if (key !== "init" && typeof eventHandler === "function")
                            eventHandler()
                })
            },            
            textEditor:function(){
                $("#myColor").spectrum({
                    type: "color",
                    color: $("#myColor").val(),
                    showInput: true,
                    showInitial: true,
                    showAlpha: false,
                    palette: [
                        dataHelper.getDefaultColorCostum(),
                        ["#000000", "#444444", "#5b5b5b", "#999999", "#bcbcbc", "#eeeeee", "#f3f6f4", "#ffffff"],
                        ["#f44336", "#744700", "#ce7e00", "#8fce00", "#2986cc", "#16537e", "#6a329f", "#c90076"],
                        ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                        ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                        ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                        ["#cc0000", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
                        ["#990000", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
                        ["#660000", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
                    ],
                    change:function(color){
                        var mycolor = "";
                        restoreSelection(editable, divSelection); 
                        mycolor = color.toRgbString();
                        mycolor = cmsConstructor.helpers.rgb2hex(mycolor);
                        // setTimeout(function(){
                        //     document.execCommand('foreColor', false,mycolor);
                        // },90);
                        $(cmsConstructor.selectedDom).coEditor("color", mycolor)

                        cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                    }
                })

                $("#myBgColor").spectrum({
                    type: "color",
                    color: $("#myBgColor").val(),
                    showInput: true,
                    showInitial: true,
                    change:function(color){
                        $(cmsConstructor.selectedDom).coEditor("background-color", color.toHexString())
                        cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                    }
                })

                $("#input-font").off("change").on("change", function () {
                    $(cmsConstructor.selectedDom).coEditor("font-family", $(this).val())
                    cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                });

                // $("#input-lang").off("change").on("change", function(){
                //     cmsConstructor["lang"] = $(this).find(":selected").val()
                //     $("#sp-"+cmsConstructor.spId).attr("data-lang", cmsConstructor.lang)
                //     if (cmsConstructor.sp_params[cmsConstructor.spId].saveText.hasOwnProperty(cmsConstructor.lang)) {
                //          $("#sp-"+cmsConstructor.spId).html(cmsConstructor.sp_params[cmsConstructor.spId].saveText[cmsConstructor.lang])
                //     }
                // })

                $("#myBgColor").spectrum({
                    type: "color",
                    color: $("#myBgColor").val(),
                    showInput: true,
                    showInitial: true,
                    change:function(color){
                        $(cmsConstructor.selectedDom).coEditor("background-color", color.toHexString())
                        cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                    }
                })

                $("#activateMarkdown").off("change").on("change", function(){
                    var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId)
                    var checkboxValue = $(this).val();
                    var isChecked = $(this).prop("checked");
                    var lang = typeof cmsConstructor.sp_params[cmsConstructor.spId].language != "undefined" ? cmsConstructor.sp_params[cmsConstructor.spId].language : "fr";
                    ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId]);
                    var olderDataChanged = {};
                    var newValueToSet = {};
                    var updateText = {
                        id : cmsConstructor.spId,
                        collection : "cms",
                        path : "allToRoot"
                    }
                    updateText.value = {};
                    updateText.value.markdownText = {}
                    if(isChecked){
                        dataHelper.activateMarkdown('.sp-markdown-'+cmsConstructor.spId)
                        ancientMarkDownText = jsonHelper.getValueByPath(ancientData, "markdownText."+lang) === "" ? $(".sp-text[data-kunik='"+cmsConstructor.kunik+"']").text() : jsonHelper.getValueByPath(ancientData, "markdownText."+lang);
                        $(".sp-text[data-kunik='"+cmsConstructor.kunik+"']").addClass("hidden")
                        $(".alias-note-editor").removeClass("hidden")
                        $('.markdown-container[data-id="'+cmsConstructor.spId+'"]').removeClass("hidden")
                        updateText.value.isMarkdown = isChecked;
                        jsonHelper.setValueByPath(olderDataChanged, "isMarkdown", false);
                        jsonHelper.setValueByPath(olderDataChanged, "markdownText."+lang, ancientMarkDownText);
                        jsonHelper.setValueByPath(newValueToSet, "isMarkdown", true);
                        jsonHelper.setValueByPath(newValueToSet, "markdownText."+lang, $(".sp-text[data-kunik='"+cmsConstructor.kunik+"']").text());

                        updateText.saveLog = {
                            costumId : costum.contextId,
                            action : "costum/editBlock",
                            blockName : cmsConstructor.sp_params[updateText.id].path.split(".").pop(),
                            path : "isMarkdown",
                            blockPath : cmsConstructor.sp_params[updateText.id].path,
                            page : page,
                            keyPage : location.hash == "" ? "#welcome" : location.hash,
                            blockType : blockType,
                            kunik : cmsConstructor.kunik,
                            value : newValueToSet,
                            olderData : olderDataChanged
                        }

                        dataHelper.path2Value(updateText, function (params) {
                            cmsConstructor.sp_params[cmsConstructor.spId].isMarkdown = isChecked
                            cmsConstructor.sp_params[cmsConstructor.spId].markdownText[lang] = $(".sp-text[data-kunik='"+cmsConstructor.kunik+"']").text();
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: cmsConstructor.spId }
                            })
                            $('.markdown-container[data-id="'+cmsConstructor.spId+'"]').find("textarea").val($(".sp-text[data-kunik='"+cmsConstructor.kunik+"']").text())   
                            $(".cms-text-editor").hide()

                            //local storage management 
                            cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                            var actualPage = location.hash == "" ? "#welcome" : location.hash;
                            var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                            ctrlY[actualPage] = [];
                            localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                

                        });
                    }else{           
                        jsonHelper.setValueByPath(olderDataChanged, "isMarkdown", true);
                        jsonHelper.setValueByPath(newValueToSet, "isMarkdown", false);
                        updateText.value.isMarkdown = isChecked;

                        updateText.saveLog = {
                            costumId : costum.contextId,
                            action : "costum/editBlock",
                            blockName : cmsConstructor.sp_params[updateText.id].path.split(".").pop(),
                            path : "isMarkdown",
                            blockPath : cmsConstructor.sp_params[updateText.id].path,
                            page : page,
                            keyPage : location.hash == "" ? "#welcome" : location.hash,
                            blockType : blockType,
                            kunik : cmsConstructor.kunik,
                            value : newValueToSet,
                            olderData : olderDataChanged
                        }
                        dataHelper.path2Value(updateText, function (params) {
                            cmsConstructor.sp_params[cmsConstructor.spId].isMarkdown = isChecked
                            // cmsConstructor.sp_params[cmsConstructor.spId].markdownText[lang] = cmsConstructor.sp_params[cmsConstructor.spId].markdownText[lang]
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: cmsConstructor.spId }
                            })
                            $(".cms-text-editor").show()
                            $(".alias-note-editor").addClass("hidden")
                            
                            $(".sp-text[data-kunik='"+cmsConstructor.kunik+"'").removeClass("hidden")
                            $('.markdown-container[data-id="'+cmsConstructor.spId+'"]').addClass("hidden")
                            if (typeof cmsConstructor.sp_params[cmsConstructor.spId].text != "undefined") {
                                $(".sp-text[data-kunik='"+cmsConstructor.kunik+"'").html(cmsConstructor.sp_params[cmsConstructor.spId].text[lang])
                            }

                            //local storage management 
                            cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                            var actualPage = location.hash == "" ? "#welcome" : location.hash;
                            var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                            ctrlY[actualPage] = [];
                            localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                        });
                        
                    }
                })

                $("#activateAliasMD").off("change").on("change", function(){
                    var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId);
                    var isChecked = $(this).prop("checked");
                    var ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId]);
                    var olderDataChanged = {};
                    var newValueToSet = {};
                    if(isChecked){
                        $('.alias-md-options').removeClass("hidden")
                        if (typeof cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias != "undefined") {
                            $(".markdown-container[data-id='"+cmsConstructor.spId+"'").html(dataHelper.markdownToHtml(costum.availableNote[cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias.noteId].doc))
                        }
                    }else{            
                        
                        jsonHelper.setValueByPath(olderDataChanged, "markdownAlias", ancientData.markdownAlias);
                        jsonHelper.setValueByPath(newValueToSet, "markdownAlias", null);

                        var updateText = {
                            id : cmsConstructor.spId,
                            collection : "cms",
                            path : "markdownAlias",
                            value : isChecked 
                        }

                        updateText.saveLog = {
                            costumId : costum.contextId,
                            action : "costum/editBlock",
                            blockName : cmsConstructor.sp_params[updateText.id].path.split(".").pop(),
                            path : "markdownAlias",
                            blockPath : cmsConstructor.sp_params[updateText.id].path,
                            page : page,
                            keyPage : location.hash == "" ? "#welcome" : location.hash,
                            blockType : blockType,
                            kunik : cmsConstructor.kunik,
                            value : newValueToSet,
                            olderData : olderDataChanged
                        }

                        dataHelper.unsetPath(updateText, function (params) {
                            $('.alias-md-options').addClass("hidden")
                            $(".markdown-container[data-id='"+cmsConstructor.spId+"'")
                            .html(`<div class="sp-markdown-${cmsConstructor.spId} markdown">
                                ${typeof cmsConstructor.sp_params[cmsConstructor.spId].markdownText != "undefined" ? cmsConstructor.sp_params[cmsConstructor.spId].markdownText[costum.langCostumActive] : ""}</div>` )
                            dataHelper.activateMarkdown('.sp-markdown-'+cmsConstructor.spId)
                            delete cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias;

                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: cmsConstructor.spId }
                            })

                            //local storage management 
                            cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                            var actualPage = location.hash == "" ? "#welcome" : location.hash;
                            var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                            ctrlY[actualPage] = [];
                            localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));

                        });
                    }
                })

                $(".alias-md").off("change").on("change", function(){
                    var thisValue = $(this).val();
                    var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId);
                    var ancientData = (typeof cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias !== "undefined") ? cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias) : {} ;
                    var olderDataChanged = {};
                    var newValueToSet = {};
                    if (thisValue != "") {

                        jsonHelper.setValueByPath(olderDataChanged, "markdownAlias.noteId", typeof ancientData.noteId !== "undefined" ? ancientData.noteId : null);
                        jsonHelper.setValueByPath(newValueToSet, "markdownAlias.noteId", thisValue);

                        var updateText = {
                            id : cmsConstructor.spId,
                            collection : "cms",
                            path : "allToRoot"
                        }
                        updateText.value = {                            
                            isMarkdown    : true,
                            markdownAlias : { noteId : thisValue }
                        } 

                        updateText.saveLog = {
                            costumId : costum.contextId,
                            action : "costum/editBlock",
                            blockName : cmsConstructor.sp_params[updateText.id].path.split(".").pop(),
                            path : "markdownAlias.noteId",
                            blockPath : cmsConstructor.sp_params[updateText.id].path,
                            page : page,
                            keyPage : location.hash == "" ? "#welcome" : location.hash,
                            blockType : blockType,
                            kunik : cmsConstructor.kunik,
                            value : newValueToSet,
                            olderData : olderDataChanged
                        }

                        dataHelper.path2Value(updateText, function (params) {
                            cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias = {noteId : thisValue};
                            $(".markdown-container[data-id='"+cmsConstructor.spId+"'").html(dataHelper.markdownToHtml(costum.availableNote[thisValue].doc))
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: cmsConstructor.spId }
                            })

                            //local storage management 
                            cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                            var actualPage = location.hash == "" ? "#welcome" : location.hash;
                            var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                            ctrlY[actualPage] = [];
                            localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                        }); 
                    }else{
                        var updateText = {
                            id : cmsConstructor.spId,
                            collection : "cms",
                            path : "markdownAlias"
                        }
                        dataHelper.unsetPath(updateText, function (params) {
                            $(".markdown-container[data-id='"+cmsConstructor.spId+"'")
                            .html(`<div class="sp-markdown-${cmsConstructor.spId} markdown">
                                ${typeof cmsConstructor.sp_params[cmsConstructor.spId].markdownText != "undefined" ? cmsConstructor.sp_params[cmsConstructor.spId].markdownText[costum.langCostumActive] : ""}</div>` )
                            dataHelper.activateMarkdown('.sp-markdown-'+cmsConstructor.spId)
                            delete cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias;
                            costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "replaceBlock", 
                                data:  { id: cmsConstructor.spId }
                            })
                        });
                    }
                }).select2({
                    allowClear: true,
                    maximumSelectionLength: 1 
                });


                $("#fontSize").off("change").on("change", function(){
                    $(cmsConstructor.selectedDom).coEditor("font-size", $(this).val()+"px")
                    cmsConstructor.block.actions.textChanged()
                    cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                })

                $(".cms-text-editor-actions li button").off("click").on("click", function(e){
                    var $thisActionParent =  $(this).parents(".cms-text-editor-actions")
                    if ($thisActionParent.data("action") == "align") {              
                        $(cmsConstructor.selectedDom).coEditor($(this).data("property"), $(this).data("value"))          
                        $(this).parents('ul[data-action="'+$thisActionParent.data("action")+'"]').find("button").removeClass("active");
                        $(this).addClass("active")
                    }

                    if ($(this).data("property") == "font-weight") {
                        if ($(this).hasClass("active")) 
                            $(cmsConstructor.selectedDom).coEditor($(this).data("property"), "400")
                        else
                            $(cmsConstructor.selectedDom).coEditor($(this).data("property"), $(this).data("value"))

                        if (spTextEditor.getWrapperOfHighlightedText().css("fontWeight") == "700"){                    
                            $(this).addClass("active");
                        }else {                          
                            $(this).removeClass("active");
                        }
                    }
                    if ($(this).data("property") == "font-style") {
                        $(cmsConstructor.selectedDom).coEditor($(this).data("property"), $(this).data("value"))
                        if (spTextEditor.getWrapperOfHighlightedText().css("font-style") == "italic")                       
                            $(this).addClass("active");
                        else                           
                        $(this).removeClass("active");
                    }
                    cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)                             
                    
                })
            }
        }
    },
    block: {
        actions : {
            addActions:function($target, $pin = false){
                var dragnotStart = !cmsConstructor.dragstart,
                    blockActionIsNotExist = $target.find("> .block-actions-wrapper").length < 1,
                    isSelected = $target.find("> .block-actions-wrapper.selected").length > 0;
                if(isSelected)
                    $(".block-actions-wrapper:not(.selected)").remove()

                if( dragnotStart && blockActionIsNotExist ){
                    $(".block-actions-wrapper:not(.selected)").remove()
                    
                    var type = $target.data("blocktype")
                    cmsConstructor.block.views.addActions(
                        type, 
                        $target, 
                        cmsConstructor.helpers.getBlockTitle($target), 
                        $pin ? 'selected':""
                    )

                    cmsConstructor.block.events.actions.bindOnclickActionItem()
                    cmsConstructor.block.events.bindEventsSuperText();
                }
            },
            saveTextEdition:function(dom){
                cmsConstructor.textFieldChanged = field;
                if(!costum.isPageAlias && Object.keys(cmsConstructor.sp_params[cmsConstructor.spId].parent)[0] == costum.contextId){
                    var ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[cmsConstructor.spId]);
                    var olderDataChanged = {};
                    var text = dom.html(),
                    tplCtx = {};
                    
                    var tempElement = $('<div>').html(text);

                    (tempElement.find('*')).filter(function(k,v) {
                        if (this.tagName.toLowerCase() != "br") {
                            return Object.keys(this.attributes).length === 0;
                        }
                    }).each(function() {
                        var innerText = $(this).html();
                        $(this).replaceWith(innerText);
                    });
                    text = tempElement.html();

                    tplCtx.id = dom.data("id");
                    tplCtx.collection = "cms";
                    $(".save-text").find("i").removeClass("fa-save").addClass("fa-spin fa-spinner");
                    // $.each(cmsConstructor.textFieldChanged, function(index, value) {
                        var newValueToSet = {};
                        field = dom.data("field");
                        lang = dom.attr("data-lang");
                        tplCtx.value =  dom.html();
                        tplCtx.value = tplCtx.value == "" ? null : tplCtx.value;
                        tplCtx.path = field+"."+lang;
                        tplCtx.saveLog = {
                            costumId : costum.contextId,
                            action : "costum/editBlock",
                            blockName : cmsConstructor.sp_params[tplCtx.id].path.split(".").pop(),
                            path : tplCtx.path,
                            blockPath : cmsConstructor.sp_params[tplCtx.id].path,
                            page : page,
                            keyPage : location.hash == "" ? "#welcome" : location.hash,
                            kunik : cmsConstructor.kunik,
                            blockType : "text"
                        }
                        var json = cmsConstructor.helpers.string2Json(tplCtx.value, true);
                        
                        //set value and olderData for log
                        var ancientValue = typeof jsonHelper.getValueByPath(ancientData, tplCtx.path) == "undefined" ? "" : jsonHelper.getValueByPath(ancientData, tplCtx.path);
                        jsonHelper.setValueByPath(olderDataChanged, tplCtx.path, ancientValue);
                        jsonHelper.setValueByPath(newValueToSet, tplCtx.path, tplCtx.value);


                        tplCtx.saveLog["value"] = newValueToSet;
                        tplCtx.saveLog["olderData"] = olderDataChanged; 
                        
                        if (cmsConstructor.helpers.isContains(json, "script") == true) {
                            toastr.error("Script detecté "+value);
                        } else {
                            dataHelper.path2Value(tplCtx, function (params) {
                                costumSocket.emitEvent(wsCO, "update_component", {
                                    functionName: "replaceBlock", 
                                    data:  { id: tplCtx.id }
                                })

                                jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], field+"."+lang, dom.html());

                                delete costum.members.focused[userId]
                                // costumSocket.postData(coWsConfig.pingUpdateCostum, "costum", "elementUnfocused", costumSocket.socketId, { kunik: cmsConstructor.kunik });
                                var params = {functionName: "elementUnfocused", blockId : {}, userId : userId}
                                costumSocket.emitEvent(wsCO, "edit_component", params)

                                if (typeof cmsConstructor.sp_params[cmsConstructor.spId]["text"] == "undefined") {
                                    cmsConstructor.sp_params[cmsConstructor.spId]["text"] = {};
                                }
                                cmsConstructor.textFieldChanged = "";
                                delete tplCtx.saveLog;

                                 //local storage management 
                                cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                                var actualPage = location.hash == "" ? "#welcome" : location.hash;
                                var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costum.slug)) || {} ;
                                ctrlY[actualPage] = [];
                                localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                            });
                        }
                    // });

                    
                    // if ( (cmsConstructor.textFieldError).length > 1 ){
                    //     $.each(cmsConstructor.textFieldError, function(index, value) {
                    //         toastr.error("Script detecté "+value);
                    //     });
                    // } 
                    
                    // if ( (cmsConstructor.textFieldChanged).length > 0 ){
                    //     toastr.success(tradCms.elementwelladded);
                    //     $.each(cmsConstructor.textFieldChanged, function(index, value) {
                    //         jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], value+"."+$('.sp-text[data-id="' + tplCtx.id + '"][data-field="' + value + '"]').data('lang'), $('.sp-text[data-id="' + tplCtx.id + '"][data-field="' + value + '"]').html());
                    //     });
                    //     $(".save-text").find("i").replaceWith('<i class="fa fa-floppy-o" aria-hidden="true"></i>')
                    //     $(".cmsbuilder-right-content").removeClass("active")
                    //     $("#right-panel").html("")
                    //     $(".block-actions-wrapper").removeClass("selected")
                    //     $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');

                    //     var actualPage = location.hash == "" ? "#welcome" : location.hash;
                    //     var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costum.slug)) || {} ;
                    //     ctrlY[actualPage] = [];
                    //     localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                    // }

                    // cmsConstructor.textFieldChanged = [];
                    // cmsConstructor.textFieldError = [];
                }
            },
            textChanged : function (fieldChanged) {

                // if ($.inArray( fieldChanged, cmsConstructor.textFieldChanged) === -1) {
                //     cmsConstructor.textFieldChanged.push(fieldChanged);
                // }

                cmsConstructor.textFieldChanged = fieldChanged;

                // if( (cmsConstructor.textFieldChanged).length > 0  ){
                //     $elementAction = $('.block-actions-wrapper.selected').find('.cmsbuilder-block-action-list');
                //     $elementAction.html("<ul><li class='save-text' data-kunik="+cmsConstructor.kunik+" data-id="+cmsConstructor.spId+"><span><i class='fa fa-save' aria-hidden='true'></i><label>Enregistrer</label></span></li><li><span class='bg-orange btnReset' data-original-title='Annuler'><i class='fa fa-reply' aria-hidden='true'></span></li></ul>");    
                //     cmsConstructor.block.events.bindEventsSuperText();
                // }
                // cmsConstructor.sp_params[cmsConstructor.spId]["changesMade"] = true;
            },  
            textLinkInsertion : function(defaultSyntax) {
                let sel, range;
                if (window.getSelection) {
                    sel = window.getSelection();
                    selectedText = sel.toString();
                    if (selectedText !== "") {
                        label = "["+selectedText+"](http://www.) ";
                    }else{
                        label = defaultSyntax;
                    }
                    if (sel.getRangeAt && sel.rangeCount) {
                        range = sel.getRangeAt(0);
                        range.deleteContents();
                        range.insertNode( document.createTextNode(label) );
                    }
                } else if (document.selection && document.selection.createRange) {
                    document.selection.createRange().label = label;
                }
            },
            chooseContentStructure : function (structure,idSectionParent) {
                var generateID = cmsConstructor.helpers.generateUniqueId();
                var dataStructure = {
                    structure: structure,
                    sectionParent: idSectionParent,
                    page: page,
                }

                // If persistent parent, Update the added block into persistent
                if (typeof cmsConstructor.sp_params[idSectionParent].advanced != "undefined" && typeof cmsConstructor.sp_params[idSectionParent].advanced.persistent != "undefined") {
                    dataStructure.persistent =  cmsConstructor.sp_params[idSectionParent].advanced.persistent;
                }
                // $(".sample-cms").remove();
                var strNewBlc = '<div class="col-md-12 sample-cms text-center custom-block-cms other-cms ' + generateID + '" data-id="undefined">' +
                    '<div class="selected cms-area-selected blink-info">' + trad.waitendofloading + '</div>' +
                    '</div>';

                $(".sortable-container" + idSectionParent).replaceWith(strNewBlc)
                ajaxPost(
                    null,
                    baseUrl + "/co2/cms/addstructuredcolumn",
                    {
                        dataStructure
                    },
                    function (response) {
                        $("." + generateID).replaceWith(response.html);
                        cmsBuilder.block.initEvent();
                        cmsBuilder.block.updateOrder();
                        toastr.success(trad.finish);
                    }
                );
            }
        },
        init:function(){
            this.events.init()
        },
        views:{
            addActions: function(type, $el, label, className=""){
                    id = $el.data("id");
                    kunik = $el.data("kunik");
                    blockName = $el.data("name");
                    blockType = cmsConstructor.helpers.getBlockType(kunik,id);
                    actions = [
                        [
                            {
                                name:"move_up",
                                icon:"arrow-up",
                                label: tradCms.moveToUp
                            },
                            {
                                name:"move_down",
                                icon:"arrow-down",
                                label: tradCms.moveToBottom
                            }
                        ],
                        [
                            {
                                name:"edit",
                                icon:"pencil",
                                label: tradCms.edit
                            },
                            {
                                name:"select_parent",
                                icon:"level-up",
                                label: tradCms.selectParent
                            },
                            {
                                name:"duplicate",
                                icon:"clone",
                                label: tradCms.duplicate
                            },
                            {
                                name:"copy",
                                icon:"copy",
                                label: "Copier"
                            },
                            {
                                name:"delete",
                                icon:"trash-o",
                                label: tradCms.delete,
                                className:"bg-red"
                            },
                            {
                                name:"deselect",
                                icon:"ban",
                                label: tradCms.deselect,
                                className:"bg-orange"
                            },
                            {
                                name:"history",
                                icon:"history",
                                label: tradCms.history,
                                className:"bg-blue"
                            }
                        ]
                    ]

                    if (localStorage.getItem("cmsCopied") != null) {
                        if ((label == "Section" || label == "Colonne") && !blockName.includes("Gallery slide")) {
                            var pasteParams = {name:"paste", icon:"paste", label: trad.paste}
                            var pasteAsLink = {name:"pastealias", icon:"link", label:"Coller comme bloc miroir"}
                            actions[1].splice(4,0,pasteParams);
                            actions[1].splice(5,0,pasteAsLink); 
                        }
                    }

                    if ( blockType == "section" && typeof cmsConstructor.sp_params[id].blockParent === "undefined") {
                            if( jsonHelper.getValueByPath(cmsConstructor.sp_params[id], "advanced.persistent")  !== "footer" ){
                                var insertSection = {name:"insertSection", label: tradCms.insertSection}
                                actions[1].splice( actions[1].length-2 ,0,insertSection);
                            };
                            
                            if (isSuperAdmin || isSuperAdminCms){
                                var saveAsTemplate = {name:"save", icon:"save", label: trad.save}
                                actions[1].splice( actions[1].length-2 ,0,saveAsTemplate);
                            }
                    }

                    if (Object.keys(cmsConstructor.sp_params[$el.data("id")].parent)[0] != costum.contextId) {
                        actions = [[
                        {
                            name:"unlink", 
                            icon:"unlink", 
                            label: trad.cancellink
                        },
                        {
                            name:"select_parent",
                            icon:"level-up",
                            label: tradCms.selectParent
                        },
                        {
                            name:"lookup",
                            icon:"eye",
                            label: "Voir l'original"
                        }
                        ]]
                    }
                    
                    if ($.inArray(label, ["image", "text"]) >= 0) {
                        var objectIAbtn = {};
                        if (label == "image") {
                            objectIAbtn = {name: "generateImageIA", icon: "android", label: tradCms.generateImgIA, className: "bg-green"}
                        } else if (label == "text") {
                            objectIAbtn = {name: "questionIA", icon: "android", label: tradCms.questionIA, className: "bg-green btn-questionIA"}
                        }
                        actions[1].splice(actions[1].length - 2, 0, objectIAbtn);
                    }

                    if (blockName.includes("Gallery slide") || blockName.includes("Carousel section")) {
                        const namesToRemove = ["move_up", "move_down", "duplicate", "copy", "delete"];
                        actions = actions.map(subArray => 
                            subArray.filter(item => {
                                const shouldRemove = namesToRemove.includes(item.name);
                                return !shouldRemove;
                            })
                        );
                    }

                var labelTrad = label == "image" ? tradCms.image : label == "icon" ? tradCms.icon : label == "button" ? tradCms.button : label == "text" ? tradCms.text : label == "separator" ? tradCms.separator : label == "une section" ? tradCms.oneSection : label ;
                var cmsOptions = `
                <div class="cmsbuilder-block-action">
                    <div class="cmsbuilder-block-action-list"  data-kunik="${$el.data('kunik')}" ></div>
                </div>`
                var $actions = $(`<div class="block-actions-wrapper">${cmsOptions}</div>`)

                actions.forEach(function(actionsGroup){
                    var $ul = $(`<ul></ul>`)
                    actionsGroup.forEach(function(action){
                        if ( action.name === "insertSection" ){
                            $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span data-toggle="tooltip"  title="${action.label}" ><img src="${assetPath}/cmsBuilder/img/drop-block.png"/ style="width: 25px;"></span>
                            </li>
                        `)
                        }  else if (label !== "text") {
                        $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span data-toggle="tooltip" title="${action.label}" ><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                            </li>
                        `)
                        }else if (action.name !== "edit") {
                            $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}" >
                                <span data-toggle="tooltip" title="${action.label}" ><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                            </li>
                        `)
                        }
                    })
                    $actions.find(".cmsbuilder-block-action-list").append($ul)
                })

                $actions.prepend(`<div class="cmsbuilder-block-action-label" data-kunik="${$el.data('kunik')}" >${labelTrad}</div>`);

                $(".block-actions-wrapper").not(".selected").remove()

                $el.css({ overflow:"visible" })
                $el.prepend($actions) 

                if( ( ( $("#mainNav").length > 0 && cmsConstructor.helpers.elementIsUnder($(".cmsbuilder-block-action-label")[0],$("#mainNav")[0]) ) || ( $("#subMenu").length > 0 && cmsConstructor.helpers.elementIsUnder($(".cmsbuilder-block-action-label")[0],$("#subMenu")[0]) )  ) ){
                    $("#all-block-container").css({ "margin-top":"20px"})
                }else {
                    $("#all-block-container").css({ "margin-top":"0px"})
                }

            }
        },
        events:{
            init:function(){
                if(window.costumizer && costum && costum.editMode){
                    this.bindEventsSuperText()
                    this.actions.bindOnMouseover()
                    this.actions.bindKeyPressed()
                    this.actions.bindOnPast()
                }
            },
            actions:{
                bindOnMouseover:function(){
                    $(".cmsbuilder-block:not(.element-busy)").off("mouseover").on("mouseover", function(e){
                        e.stopPropagation();
                        if (window.getSelection().toString() == "" && !$(this).hasClass("element-busy") && !costum.isPageAlias) {
                                cmsConstructor.block.actions.addActions($(this))
                                //Wrapper list action
                                //Slide from right to left if it's too close of right side
                                var $btnTooCloseRight = $(this).find(".block-actions-wrapper li[data-action='delete']")
                                if ( $btnTooCloseRight.length > 0) {                                
                                    var elementOffset = $btnTooCloseRight.offset().left;
                                    var windowWidth = $(".cmsbuilder-center-content").width();
                                    var minimumDistance = 0; 
                                    if (windowWidth - elementOffset < minimumDistance) {
                                        $(this).find(".cmsbuilder-block-action").css({
                                            "cssText": "right: 0 !important; left: auto !important;"
                                        });
                                    }

                                }
                                

                                if( costumizer.mode == "xs"){
                                    // Update left position for .cmsbuilder-block-action-label
                                    var $labelOutOfScreen = $(this).find(".block-actions-wrapper li[data-action='move_up']");
                                    cmsConstructor.helpers.updateLeftPosition($labelOutOfScreen, $(this).find(".cmsbuilder-block-action"));

                                    // Update left position for .cmsbuilder-block-action
                                    var $btnTooCloseRight = $(this).find(".cmsbuilder-block-action"); // Assuming you meant $btnTooCloseRight instead of $labelOutOfScreen
                                    cmsConstructor.helpers.updateLeftPosition($btnTooCloseRight, $(this).find(".cmsbuilder-block-action"));

                                    // Update left position for $blockAction
                                    var $blockAction = $(this).find(".cmsbuilder-block-action");
                                    if ($blockAction.length > 0) {
                                        var blockActionLeft = $blockAction.css('left');
                                        var labelOffset = $labelOutOfScreen.offset().left;
                                        var finalBlockActionValue = parseInt(blockActionLeft) - labelOffset;
                                        $blockAction.css({
                                            "cssText": "left: " + finalBlockActionValue + "px !important;"
                                        });
                                    } 
                                }
                        }
                    })
                    
                },
                bindOnclickActionItem:function(){
                    var self = this;
                    $(".block-actions-wrapper li").off("click").on("click", function(e){
                        e.stopPropagation();
                        var $owner = $($(this).parents(".cmsbuilder-block")[0]),
                            kunik = $owner.data("kunik"),
                            id = $owner.data("id"),
                            ownerType = $owner.data("blocktype"),
                            path = $owner.data("path"),
                            name = $owner.data("name")

                        cmsConstructor.kunik = kunik
                        cmsConstructor.spId = id
                        cmsConstructor["ownerType"] = ownerType;
                        var blockType =  cmsConstructor.helpers.getBlockType(kunik,id);

                        // close sub right panel
                        costumizer.actions.closeRightSubPanel()
                        if ($owner.hasClass('element-busy')){
                            cmsConstructor.helpers.appendBusy($owner.data('userid'));
                        }else{
                            switch($(this).data("action")){
                                case "edit":
                                    if( (ownerType == "custom" && typeof cmsConstructor.blocks[blockType] == "undefined" ) ||  (ownerType == "section" && path != "tpls.blockCms.superCms.container")){
                                        if(costumizer){
                                            costumizer.actions.closeRightSubPanel()
                                            costumizer.actions.toogleSidePanel("right", false)
                                        }
                                        $(`.edit${kunik}Params`).click()
                                    }else{
                                        $(".block-actions-wrapper").removeClass("selected")
                                        $(this).closest(".block-actions-wrapper").addClass("selected")
                                        cmsConstructor.editor.init("#right-panel")
                                        cmsConstructor.layer.actions.selectPath(kunik)
                                        $(".configuration-menus-costumizer").removeClass("focused")
                                        // costumSocket.emitEvent(wsCO, "update_component", {
                                        //     functionName: "elementFocused", 
                                        //     data:{
                                        //         elementTarget: cmsConstructor.kunik,
                                        //         userId: userId
                                        //     } 
                                        // })

                                        costumizer.focusComponentSocket("elementFocused",cmsConstructor.kunik);
                                        costum.members.focused[userId] = {page : page, elementTarget : cmsConstructor.kunik}; 
                                    }
                                break;
                                case "duplicate":                  
                                    cmsConstructor.builder.actions.duplicateBlock(cmsConstructor.spId,null,"duplicate",id);
                                break;
                                case "delete":
                                     cmsConstructor.builder.actions.deleteBlock(null,null,$owner,name)
                                break;
                                case "select_parent":
                                    var $target = null;
                                    var blockParentID = null;
                                    if (typeof cmsConstructor.sp_params[cmsConstructor.spId].blockParent != "undefined") {                                        
                                       blockParentID = cmsConstructor.sp_params[cmsConstructor.spId].blockParent
                                    }
                                    if (notNull(blockParentID) && $owner.parents(".cmsbuilder-block[data-id='"+blockParentID+"']").hasClass('element-busy')){
                                        cmsConstructor.helpers.appendBusy($owner.parents(".cmsbuilder-block[data-id='"+blockParentID+"']").data('userid'));
                                    }else{
                                        if($owner.parents(".cmsbuilder-block[data-blockType='column']:not(.element-busy)").length > 0)
                                            $target = $($owner.parents(".cmsbuilder-block[data-blockType='column']")[0])
                                        else if($owner.parents(".cmsbuilder-block[data-blockType='section']:not(.element-busy)").length > 0)
                                            $target = $($owner.parents(".cmsbuilder-block[data-blockType='section']")[0])

                                    //si le parent existe et les actions ne sont pas encore ajoutés
                                        if($target && $target.find("> .block-actions-wrapper").length < 1){
                                            var targetType = $target.data("blocktype"),
                                            targetId = $target.data("id"),
                                            targetKunik = $target.data("kunik"),
                                            targetName = $target.data("name"),
                                            actionLabel = targetName;

                                            if(targetType == "section"){
                                                actionLabel = "Section"
                                                if(typeof cmsConstructor.sp_params[targetId].advanced != "undefined"){
                                                    if(typeof cmsConstructor.sp_params[targetId].advanced.persistent != "undefined")
                                                        actionLabel = tradCms[cmsConstructor.sp_params[targetId].advanced.persistent]
                                                }
                                            }

                                            else if(targetType == "column")
                                                actionLabel = "Colonne"

                                            
                                            $(".block-actions-wrapper").remove()
                                            cmsConstructor.block.views.addActions(targetType, $target, actionLabel, "selected")

                                            self.bindOnclickActionItem()

                                            $(".block-actions-wrapper").addClass("selected");
                                            $(".configuration-menus-costumizer").removeClass("focused")

                                            cmsConstructor.kunik = targetKunik
                                            cmsConstructor.spId = targetId
                                            cmsConstructor.editor.init("#right-panel")
                                        // close sub panel 
                                            costumizer.actions.closeRightSubPanel();
                                            // costumSocket.emitEvent(wsCO, "update_component", {
                                            //     functionName: "elementFocused", 
                                            //     data:{
                                            //         elementTarget: cmsConstructor.kunik,
                                            //         userId: userId
                                            //     } 
                                            // })

                                            costumizer.focusComponentSocket("elementFocused", cmsConstructor.kunik);
                                            costum.members.focused[userId] = {page : page, elementTarget : cmsConstructor.kunik}; 
                                        }
                                    }
                                break;
                                case "deselect":
                                    $(".cmsbuilder-right-content").removeClass("active")
                                    
                                    $("#right-panel").html("")
                                    $(".block-actions-wrapper").removeClass("selected")
                                    $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');
                                    delete costum.members.focused[userId]
                                    // costumSocket.postData(coWsConfig.pingUpdateCostum, "costum", "elementUnfocused", costumSocket.socketId, { kunik: cmsConstructor.kunik });
                                    var params = {functionName: "elementUnfocused", blockId : {}, userId : userId}
                                    costumSocket.emitEvent(wsCO, "edit_component", params)
                                    cmsConstructor.spId = "";
                                    cmsConstructor.kunik = "";
                                break;
                                case "move_up":
                                    cmsConstructor.builder.actions.sortBlock($owner, "move_up")
                                break;
                                case "move_down":
                                    cmsConstructor.builder.actions.sortBlock($owner, "move_down")
                                break;
                                case "copy":
                                    var copiedData = {
                                        costumId   : costum.contextId,
                                        costumType : costum.contextType,
                                        costumSlug : costum.contextSlug,
                                        blockId    : cmsConstructor.spId
                                    }
                                    localStorage.setItem("cmsCopied", JSON.stringify(copiedData));
                                    toastr.success("CMS copié!");
                                break;
                                case "paste":    
                                    //empty-sp-element      
                                    if ($(".empty-sp-element"+cmsConstructor.kunik)[0])
                                        $(".empty-sp-element"+cmsConstructor.kunik).hide()
                                    
                                    var copiedCms = JSON.parse(localStorage.getItem("cmsCopied"))
                                    cmsConstructor.builder.actions.duplicateBlock(copiedCms.blockId, cmsConstructor.spId , "paste",$owner);
                                    // cmsBuilder.block.initEvent();    
                                    // $owner.find(".block-container-html").append(strNewBlc);         
                                    // cmsConstructor.builder.actions.duplicate(cmsConstructor["copy"], cmsConstructor.spId);           
                                    // delete cmsConstructor["copy"]
                                break;                                                         
                                case "pastealias":    
                                    //empty-sp-element   
                                    if ($(".empty-sp-element"+cmsConstructor.kunik)[0])
                                        $(".empty-sp-element"+cmsConstructor.kunik).hide()
                                    
                                    var copiedCms = JSON.parse(localStorage.getItem("cmsCopied"))
                                    aliasCms = {};
                                    aliasCms.collection = "cms";
                                    aliasCms.id = cmsConstructor.spId;
                                    aliasCms.path = "alias."+copiedCms.costumId                                    
                                    aliasCms.value = {
                                        type      : copiedCms.costumType,
                                        name      : copiedCms.costumSlug,
                                        container : [copiedCms.blockId]
                                    };

                                    if (typeof cmsConstructor.sp_params[cmsConstructor.spId].alias != "undefined" && 
                                        typeof cmsConstructor.sp_params[cmsConstructor.spId].alias[copiedCms.costumId] != "undefined") 
                                    {                                         
                                        aliasCms.path = "alias."+copiedCms.costumId+".container";                                             
                                        cmsConstructor.sp_params[cmsConstructor.spId].alias[copiedCms.costumId].container.push(copiedCms.blockId)                                       
                                        aliasCms.value = cmsConstructor.sp_params[cmsConstructor.spId].alias[copiedCms.costumId].container;
                                    }

                                    dataHelper.path2Value(aliasCms, function (params) {
                                        cmsConstructor.helpers.insertBlockView("paste",cmsConstructor.spId,"",true,null,null);
                                        costumSocket.emitEvent(wsCO, "update_component", {
                                            functionName: "duplicateBlock", 
                                            data: { actionName : "paste" , ownerBlock : cmsConstructor.spId , newBlockParent : cmsConstructor.spId , newDiv : "" }
                                        })
                                        toastr.success(trad.finish);
                                    });
                                    // cmsConstructor.builder.actions.duplicateBlock(localStorage.getItem("cmsCopied"), cmsConstructor.spId , "paste",$owner);
                                    // cmsBuilder.block.initEvent();    
                                    // $owner.find(".block-container-html").append(strNewBlc);         
                                    // cmsConstructor.builder.actions.duplicate(cmsConstructor["copy"], cmsConstructor.spId);           
                                    // delete cmsConstructor["copy"]
                                break;
                                case "unlink":                                      
                                    cmsConstructor.builder.actions.unlinkBlock($owner)
                                        // mylog.log(cmsConstructor.sp_params[cmsConstructor.spId])

                                        // (cmsConstructor.sp_params[cmsConstructor.spId].alias).each(function(costumParent, valueAlias) {
                                        //     valueAlias.container.each(function(key, idCMSAlias) {                                                
                                        //         mylog.log(idCMSAlias)
                                        //     });
                                        // });
                                        // mylog.log(cmsConstructor.sp_params[cmsConstructor.spId].alias)
                                break;
                                case "lookup": 
                                    var url = baseUrl+"/costum/co/index/slug/"+cmsConstructor.sp_params[cmsConstructor.spId].source.key+"#"+cmsConstructor.sp_params[cmsConstructor.spId].page
                                    window.open(url, '_blank');
                                break;
                                case "insertSection": 
                                    cmsConstructor.builder.actions.insertBlock("after", cmsConstructor.kunik)
                                break;
                                case "save":
                                    cmsConstructor.helpers.saveBlockAsBlockCms(cmsConstructor.spId);
                                break;
                                case "history":
                                    $(this).parents(".block-actions-wrapper").addClass("selected");
                                    $("a.lbh-costumizer[data-dom='#menu-right-history']").trigger( "click" );
                                    //$(".cmsbuilder-right-content").addClass("active");
                                    //$(".cmsbuilder-right-content #right-panel").html(cmsConstructor.editor.views.historyContent(cmsConstructor.spId));
                                break;
                                case "generateImageIA":
                                    if (typeof costum.useOpenAI != "undefined") {
                                        $(`.cmsbuilder-block[data-kunik="${kunik}"]`).click();
                                        cmsConstructor.helpers.generateImageIA($(this), $owner);
                                    } else {
                                        toastr.info(`<button class="btn btn-sm btn-primary btn-show-how-togetKey" id='generate-image'>${tradCms.howgetkeyApiOpenAI}</button>`, tradCms.dontEnteredKeyAPI, {
                                            "closeButton": true,
                                            "preventDuplicates" : true,
                                            "timeOut": 0,
                                            "positionClass": "toast-bottom-right",
                                            "toastClass": "toast-open-ai-cms", 
                                            "extendedTimeOut": "1500", 
                                        });
                                        $("#generate-image.btn-show-how-togetKey").on("click", function() {
                                            $(".bootbox.modal").hide();
                                            $(".lbh-costumizer[data-space='generalInfos']").click();
                                            $("#costum-use-open-ai .open-ai-modal-information").click();
                                        });
                                    }
                                break;
                            }
                        }
                    })

                    
                    $(".cmsbuilder-block:not(.sp-text)").off("click").on("click", function(e){
                        e.stopPropagation() 
                        if ($(this).hasClass('element-busy')){                            
                            cmsConstructor.helpers.appendBusy($(this).data('userid'));
                        } else if(Object.keys(cmsConstructor.sp_params[$(this).data("id")].parent)[0] == costum.contextId){
                            var spTextEditable = $("#all-block-container").find(".sp-text[contenteditable=true]").length !== 0;
                            // if (spTextEditable){
                                var noteditable = $(this).data("noteditable") ? $(this).data("noteditable") : 0;
                                if(!(($(e.target).parents().hasClass("carousel-control") || $(e.target).parents().hasClass("carousel-indicators")) || ($(e.target).data("toggle") == "collapse")) ){
                                    e.stopPropagation() 
                                    var $cmsbuilderBlock = $(this).closest(".cmsbuilder-block")
                                    var kunik = $(this).data("kunik") ? $(this).data("kunik"):"",
                                    id = $(this).data("id");
                                    if ( cmsConstructor.textFieldChanged !== "" ){
                                        cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom);
                                    } 

                                    if ( noteditable === 0 ){
                                        if (window.getSelection().toString().length == 0) {   
                                            ownerType = $cmsbuilderBlock.data("blocktype"),
                                            path = $cmsbuilderBlock.data("path");
                                            $(".block-actions-wrapper").removeClass("selected")  
                                            $cmsbuilderBlock.find("> .block-actions-wrapper").addClass("selected")
                                            $(".configuration-menus-costumizer").removeClass("focused")
                                            cmsConstructor.kunik = kunik
                                            cmsConstructor.spId = id
                                            cmsConstructor.ownerType = ownerType;
                                            cmsConstructor.editor.init("#right-panel")
                                            cmsConstructor.layer.actions.selectPath(kunik)

                                            var editableElements = $(`[contenteditable]:not([data-id="${cmsConstructor.spId}"])`);
                                            editableElements.each(function() {
                                                $(this).removeAttr('contenteditable');
                                            });

                                            if(cmsConstructor.ownerType == "text"){
                                                if (cmsConstructor.sp_params[cmsConstructor.spId].isMarkdown) {                                                        
                                                    $(".active-markdown").prop("checked", true);
                                                    $(".alias-note-editor").removeClass("hidden")
                                                    $(".cms-text-editor").hide()
                                                    if (typeof cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias != "undefined") {                                                            
                                                        $(".active-alias-markdown").prop("checked", true);                                                            
                                                        $('.alias-md-options').removeClass("hidden")

                                                        if (Object.keys(costum.availableNote).length > 0){
                                                            $(".alias-md option[value='"+cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias.noteId+"']").prop("selected", true);
                                                            $(".alias-md").val(cmsConstructor.sp_params[cmsConstructor.spId].markdownAlias.noteId).trigger('change');

                                                        }
                                                    }
                                                }
                                                
                                            }
                                            costumizer.focusComponentSocket("elementFocused", cmsConstructor.kunik);
                                            costum.members.focused[userId] = {page : page, elementTarget : cmsConstructor.kunik}; 

                                            return false;
                                        }
                                    }
                                }
                            // }
                        }
                    })

                    $(".choose-structure").off().on("click",function(e){
                        cmsConstructor.block.actions.chooseContentStructure($(this).data("structure") , $(this).data("id"))
                        costumSocket.emitEvent(wsCO, "update_component", {
                            functionName: "refreshBlock", 
                            data:  { id: $(this).data("id") }
                        })
                    })


                    $(".choose-model-cms").off().on("click",function(e){
                        cmsConstructor['blockParent'] = $(this).data("id");
                        // $('#modal-blockcms').addClass('open')
                        var params = {
                               space:"template",
                                   // title:"Model CMS",
                                    showSaveButton:false,
                                    key:"blockCms"
                                }
                               // delete costumizer.cms["list"]
                                costumizer.template.views.initAjax(params)
                        e.stopPropagation()
                    })
                    $(".choose-model-page").off().on("click",function(e){
                        var params = {
                               space:"template",
                                   // title:"Model CMS",
                                    showSaveButton:false,
                                    key:"page"
                                }
                               // delete costumizer.cms["list"]
                                costumizer.template.views.initAjax(params)
                        e.stopPropagation()
                    })
                    $(".show-tutorial").off().on("click",function(e){
                        costumizer.actions.showTutorial($(this).data("imgsrc"),'How to add an element??')
                    })

                    $(".lbh-menu-app,.lbh").children().off().on('click',function(event){
                        var changeMade = false;
                        $(".costum-info").addClass("hidden")
                        $.each(cmsConstructor.sp_params, function(k,val){
                            if (typeof cmsConstructor.sp_params[k]["changesMade"]  != "undefined") {
                                changeMade = true
                            }
                        })
                        if (changeMade) {
                            const confirmationMessage = tradCms.leavePageUnsaved;
                            var userConfirmed = confirm(confirmationMessage);
                            if (!userConfirmed) {
                                event.stopImmediatePropagation()
                                event.preventDefault()
                            }
                        }    
                    })
                    $('.markdown-container[data-id="'+cmsConstructor.spId+'"]').find("textarea").off().on("blur", function(e){
                        e.stopImmediatePropagation()
                        var blockType = cmsConstructor.helpers.getBlockType(cmsConstructor.kunik,cmsConstructor.spId);
                        var lang = typeof cmsConstructor.sp_params[cmsConstructor.spId].language != "undefined" ? cmsConstructor.sp_params[cmsConstructor.spId].language : "fr";
                        cmsConstructor.editor.actions.updateBlock(blockType, "markdownText."+lang, "valueToSet", "markdown", {}, $(this).val(), "","");
                    })

                },
                bindKeyPressed: function(){
                    $("#page-top").on('keyup', function(event) {
                        event.stopImmediatePropagation() 
                        if (event.target.tagName !== "INPUT") {
                            var spTextEditable = $("#all-block-container").find(".sp-text[contenteditable=true]").length == 0;
                            if (spTextEditable) {

                                var ctrlZ = JSON.parse(localStorage.getItem("ctrlZ"+costumizer.mode+costum.slug)) || {} ;
                                var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                                var actualPage = location.hash == "" ? "#welcome" : location.hash;  

                                var dataCtrlZPage = typeof ctrlZ[actualPage] !== "undefined" ? ctrlZ[actualPage] : [] ;
                                var dataCtrlYPage = typeof ctrlY[actualPage] !== "undefined" ? ctrlY[actualPage] : [] ;

                                if (event.ctrlKey && event.key === "z") {
                                    if ( dataCtrlZPage.length > 0 ) {
                                        $(".cmsbuilder-right-content").removeClass("active")
                                        $("#right-panel").html("")

                                        var dataToRestore = dataCtrlZPage[0];
                                        if ( dataToRestore["type"] === "block") {
                                            owner = $(`.cmsbuilder-block[data-id="${dataToRestore.id}"]`);
                                            var childElements = owner.find('.cmsbuilder-block');
                                            var hasElementBusy = false;
                                            childElements.each(function() {
                                                if ($(this).hasClass('element-busy')) {
                                                    hasElementBusy = true;
                                                    return false;
                                                }
                                            });
                                            
                                            if ( owner.hasClass('element-busy') || hasElementBusy ) {
                                                var message = `<p class="padding-20 text-center">${costum.members.lists[owner.data('userid')].avatar} ${costum.members.lists[owner.data('userid')].name} ${tradCms.ctrlZRefused}</p>`;
                                                bootbox.dialog({
                                                    message: message,
                                                    title: tradCms.ctrlZRefusedTitle,
                                                    show: true,
                                                    backdrop: true,
                                                    closeButton: false,
                                                    animate: true,
                                                    className: "my-modal",
                                                    backdrop: 'static',
                                                    buttons: { 
                                                        cancel: {
                                                            label: "OK",
                                                            className: 'btn-default'
                                                        }
                                                    }
                                                });
                                            } else {
                                                var dataToInsert = {};
                                                var dataToCtrlY = {};
                                                tplCtx = {};
                                                tplCtx.id = dataToRestore.id;
                                                tplCtx.collection = "cms";
                                                tplCtx.path = "allToRoot";
                                                
                            
                                                var status = cmsConstructor.helpers.ctrlChangeAction(cmsConstructor.helpers.deepCopy(dataToRestore.data),dataToRestore.kunik,dataToRestore.id);
                                                $.each(cmsConstructor.helpers.deepCopy(dataToRestore.data), function(key,val){
                                                    actualValue = typeof cmsConstructor.sp_params[dataToRestore.id] !== "undefined" && typeof cmsConstructor.sp_params[dataToRestore.id][key] !== "undefined" ? cmsConstructor.sp_params[dataToRestore.id][key] : {};
                                                    if ( typeof val === "object" ){
                                                        valueToUpdate = cmsConstructor.helpers.mergeObjectsStructure2(val,cmsConstructor.helpers.deepCopy(actualValue));
                                                        dataToInsert[key] = valueToUpdate;
                                                        valueToCtrlY = cmsConstructor.helpers.mergeObjectsStructure1(val,cmsConstructor.helpers.deepCopy(actualValue));
                                                        dataToCtrlY[key] = valueToCtrlY;
                                                    } else {
                                                        dataToInsert[key] = val;
                                                        dataToCtrlY[key] = actualValue;
                                                    }
                                                });

                                                // clean before save 
                                                tplCtx.value =  cmsConstructor.helpers.removeUnnecessaryEntriesBeforeSave(dataToInsert);

                                                tplCtx.saveLog = {
                                                    costumId : costum.contextId,
                                                    action : "costum/editBlock",
                                                    value : tplCtx.value,
                                                    blockName : cmsConstructor.sp_params[dataToRestore.id].path.split(".").pop(),
                                                    path : tplCtx.path,
                                                    blockPath : cmsConstructor.sp_params[dataToRestore.id].path,
                                                    page : page,
                                                    keyPage : location.hash == "" ? "#welcome" : location.hash,
                                                    blockType : dataToRestore.blockType,
                                                    kunik : dataToRestore.kunik ,
                                                    olderData : dataToCtrlY
                                                }

                                                if ( status ) {
                                                    dataHelper.path2Value(tplCtx, function (params) {
                                                        $.each(cmsConstructor.helpers.deepCopy(dataToRestore.data), function(key,val){
                                                            actualValue = typeof cmsConstructor.sp_params[tplCtx.id] !== "undefined" && typeof cmsConstructor.sp_params[tplCtx.id][key] !== "undefined" ? cmsConstructor.sp_params[tplCtx.id][key] : {};
                                                            cmsConstructor.sp_params[tplCtx.id][key] = cmsConstructor.helpers.mergeObjectsStructure2(val,actualValue); 
                                                        });

                                                        if ( dataToRestore.blockType === "section" && typeof cmsConstructor.sp_params[dataToRestore.id].blockParent === "undefined" ){
                                                            costumSocket.emitEvent(wsCO, "update_component", {
                                                                functionName: "refreshBlock", 
                                                                data:{ id: dataToRestore.id }
                                                            })
                                                        } else {
                                                            costumSocket.emitEvent(wsCO, "update_component", {
                                                                functionName: "replaceBlock", 
                                                                data:{ id: dataToRestore.id }
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    dataHelper.path2Value(tplCtx, function (params) {
                                                        cmsConstructor.helpers.scrollToAndRefresh(tplCtx.id)
                                                    })
                                                }

                                                

                                                //clean restored data from local storage
                                                cmsConstructor.helpers.insertObjectToLocalStorage("ctrlY"+costumizer.mode+costum.slug,dataToRestore.id,dataToRestore.kunik,"block",dataToCtrlY);
                                            
                                                dataCtrlZPage.splice(0, 1);
                                                ctrlZ[actualPage] = dataCtrlZPage;

                                                localStorage.setItem("ctrlZ"+costumizer.mode+costum.slug, JSON.stringify(ctrlZ));
                                          }
                                            
                                        } else if ( dataToRestore["type"] === "menu") {
                                            // restore menu goes here 
                                            var timeOut = 0;
                                            if (!(cmsConstructor.helpers.isInViewport($(`#mainNav`)[0],0))) {
                                                cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(`#mainNav`).first());
                                                timeOut = 1000;
                                            }

                                            setTimeout(function () {
                                                var arrayStyleToRemove=[];
                                                var pathValueToUpdate = {};
                                                $.each(dataToRestore["pathChanged"], function (k, pathList) {
                                                    var valueOlder = jsonHelper.getValueByPath(dataToRestore["data"], pathList)
                                                    jsonHelper.setValueByPath(costumizer.obj, pathList, valueOlder);
                                                    costumizer.actions.change(pathList);
                                                    pathValueToUpdate[pathList] = valueOlder;
                                                    if(pathList.indexOf("css")>= 0){
                                                        if (costumizer.history.refresh == true) {
                                                            cssHelpers.render.addStyleUI(pathList, valueOlder, "cosDyn");
                                                            costumizer.updateComponentSocket({functionName: "addStyleUI", data: {path: pathList, value: valueOlder, dom: "cosDyn"}})
                                                        }else {
                                                            arrayStyleToRemove.push(pathList.slice(pathList.indexOf(".")+1,pathList.lastIndexOf(".")));
                                                            costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
                                                            costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.obj.css, path: "costumizer.obj.css"}})
                                                        }
                                                    }
                                                })

                                                costumizer.actions.update(true,"ctrlZ");
                                                costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
                                                //$(".cmsbuilder-right-content").removeClass("active");
                                                // refresh style
                                                if(notEmpty(arrayStyleToRemove)) {
                                                    cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");
                                                    costumizer.updateComponentSocket({functionName: "removeStyleUI", data: { arrayStyleToRemove: arrayStyleToRemove, dom: "cosDyn"}})
                                                }
                                            },timeOut);

                                            //clean restored data from local storage
                                            
                                            dataCtrlZPage.splice(0, 1);
                                            ctrlZ[actualPage] = dataCtrlZPage;

                                            localStorage.setItem("ctrlZ"+costumizer.mode+costum.slug, JSON.stringify(ctrlZ));
                                        }
                                    }

                                } else if (event.ctrlKey && event.key === "y") {

                                    if ( dataCtrlYPage.length > 0 ) {
                                        $(".cmsbuilder-right-content").removeClass("active")
                                        $("#right-panel").html("")
                                        var dataToRestore = dataCtrlYPage[0];
                                        if ( dataToRestore["type"] === "block" ) {
                                            owner = $(`.cmsbuilder-block[data-id="${dataToRestore.id}"]`);
                                            var childElements = owner.find('.cmsbuilder-block');
                                            var hasElementBusy = false;
                                            childElements.each(function() {
                                                if ($(this).hasClass('element-busy')) {
                                                    hasElementBusy = true;
                                                    return false;
                                                }
                                            });
                                            
                                            if ( owner.hasClass('element-busy') || hasElementBusy ) {
                                                var message = `<p class="padding-20 text-center">${costum.members.lists[owner.data('userid')].avatar} ${costum.members.lists[owner.data('userid')].name} ${tradCms.ctrlYRefused}</p>`;
                                                bootbox.dialog({
                                                    message: message,
                                                    title: tradCms.ctrlYRefusedTitle,
                                                    show: true,
                                                    backdrop: true,
                                                    closeButton: false,
                                                    animate: true,
                                                    className: "my-modal",
                                                    backdrop: 'static',
                                                    buttons: { 
                                                        cancel: {
                                                            label: "OK",
                                                            className: 'btn-default'
                                                        }
                                                    }
                                                });
                                            } else {

                                                var dataToInsert = {};
                                                var dataToCtrlZ = {};
                                                tplCtx = {};
                                                tplCtx.id = dataToRestore.id;
                                                tplCtx.collection = "cms";
                                                tplCtx.path = "allToRoot"
                                                var status = cmsConstructor.helpers.ctrlChangeAction(cmsConstructor.helpers.deepCopy(dataToRestore.data),dataToRestore.kunik,dataToRestore.id);
                                                $.each(cmsConstructor.helpers.deepCopy(dataToRestore.data), function(key,val){
                                                    actualValue = typeof cmsConstructor.sp_params[dataToRestore.id] !== "undefined" && typeof cmsConstructor.sp_params[dataToRestore.id][key] !== "undefined" ? cmsConstructor.sp_params[dataToRestore.id][key] : {};
                                                    if ( typeof val === "object" ){
                                                        valueToUpdate = cmsConstructor.helpers.mergeObjectsStructure2(val,cmsConstructor.helpers.deepCopy(actualValue));
                                                        dataToInsert[key] = valueToUpdate;
                                                        valueToCtrlZ = cmsConstructor.helpers.mergeObjectsStructure1(val,cmsConstructor.helpers.deepCopy(actualValue));
                                                        dataToCtrlZ[key] = valueToCtrlZ;
                                                    } else {
                                                        dataToInsert[key] = val;
                                                        dataToCtrlZ[key] = actualValue;
                                                    }
                                                });
                                                // clean before save 
                                                tplCtx.value =  cmsConstructor.helpers.removeUnnecessaryEntriesBeforeSave(dataToInsert);

                                                tplCtx.saveLog = {
                                                    costumId : costum.contextId,
                                                    value : tplCtx.value,
                                                    action : "costum/editBlock",
                                                    blockName : cmsConstructor.sp_params[dataToRestore.id].path.split(".").pop(),
                                                    path : tplCtx.path,
                                                    blockPath : cmsConstructor.sp_params[dataToRestore.id].path,
                                                    page : page,
                                                    keyPage : location.hash == "" ? "#welcome" : location.hash,
                                                    blockType : dataToRestore.blockType,
                                                    kunik : dataToRestore.kunik ,
                                                    olderData : dataToCtrlZ
                                                }

                                                if ( status ) {
                                                    dataHelper.path2Value(tplCtx, function (params) {
                                                        $.each(cmsConstructor.helpers.deepCopy(dataToRestore.data), function(key,val){
                                                            actualValue = typeof cmsConstructor.sp_params[tplCtx.id] !== "undefined" && typeof cmsConstructor.sp_params[tplCtx.id][key] !== "undefined" ? cmsConstructor.sp_params[tplCtx.id][key] : {};
                                                            cmsConstructor.sp_params[tplCtx.id][key] = cmsConstructor.helpers.mergeObjectsStructure2(val,actualValue); 
                                                        });

                                                        if ( dataToRestore.blockType === "section" && typeof cmsConstructor.sp_params[dataToRestore.id].blockParent === "undefined" ){
                                                            costumSocket.emitEvent(wsCO, "update_component", {
                                                                functionName: "refreshBlock", 
                                                                data:{ id: dataToRestore.id }
                                                            })
                                                        } else {
                                                            costumSocket.emitEvent(wsCO, "update_component", {
                                                                functionName: "replaceBlock", 
                                                                data:{ id: dataToRestore.id }
                                                            })
                                                        }

                                                    })
                                                } else {
                                                    dataHelper.path2Value(tplCtx, function (params) {
                                                        cmsConstructor.helpers.scrollToAndRefresh(tplCtx.id)
                                                        $(".cmsbuilder-right-content").removeClass("active")
                                                        $("#right-panel").html("")
                                                    })
                                                }
                                                cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,dataToRestore.id,dataToRestore.kunik,"block",dataToCtrlZ); 
                                                //clean restored data from local storage
                                                dataCtrlYPage.splice(0, 1);
                                                ctrlY[actualPage] = dataCtrlYPage;
                                                localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                                            }
                                        
                                        } else if ( dataToRestore["type"] === "menu" ) {

                                            // restore menu goes here 
                                            var timeOut = 0;
                                            if (!(cmsConstructor.helpers.isInViewport($(`#mainNav`)[0],0))) {
                                                cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(`#mainNav`).first());
                                                timeOut = 1000;
                                            }

                                            setTimeout(function () {
                                                var arrayStyleToRemove=[];
                                                var pathValueToUpdate = {};
                                                $.each(dataToRestore["pathChanged"], function (k, pathList) {
                                                    var valueOlder = jsonHelper.getValueByPath(dataToRestore["data"], pathList)
                                                    jsonHelper.setValueByPath(costumizer.obj, pathList, valueOlder);
                                                    costumizer.actions.change(pathList);
                                                    pathValueToUpdate[pathList] = valueOlder;
                                                    if(pathList.indexOf("css")>= 0){
                                                        if (costumizer.history.refresh == true) {
                                                            cssHelpers.render.addStyleUI(pathList, valueOlder, "cosDyn");
                                                            costumizer.updateComponentSocket({functionName: "addStyleUI", data: {path: pathList, value: valueOlder, dom: "cosDyn"}})
                                                        }else {
                                                            arrayStyleToRemove.push(pathList.slice(pathList.indexOf(".")+1,pathList.lastIndexOf(".")));
                                                            costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
                                                            costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.obj.css, path: "costumizer.obj.css"}})
                                                        }
                                                    }
                                                })

                                                costumizer.actions.update(true,"ctrlY");
                                                costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
                                                //$(".cmsbuilder-right-content").removeClass("active");
                                                // refresh style
                                                if(notEmpty(arrayStyleToRemove)) {
                                                    cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");
                                                    costumizer.updateComponentSocket({functionName: "removeStyleUI", data: { arrayStyleToRemove: arrayStyleToRemove, dom: "cosDyn"}})
                                                }
                                            },timeOut);

                                            //clean restored data from local storage
                                            dataCtrlYPage.splice(0, 1);
                                            ctrlY[actualPage] = dataCtrlYPage;
                                            localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));
                                        }

                                       
                                    }
                                } else if (typeof cmsConstructor.keyEvent[event.key] == "function" && !$("#all-block-container").find(".md-editor").hasClass("active")) {
                                    cmsConstructor.keyEvent[event.key]();
                                }
                            }
                            // if( spTextEditable && typeof cmsConstructor.keyEvent[event.key] == "function"){
                            //     cmsConstructor.keyEvent[event.key]();
                            // }
                        }
                    });
                },
                bindOnPast : function(){
                    $("#page-top").off("paste").on("paste", async function (e) {
                        e.stopPropagation();
                        if ( e.target.tagName !== "INPUT" && e.target.tagName !== "TEXTAREA" && e.target.tagName !== "TEXT"  ){
                            var blockSelectedExist =  $("#all-block-container").find(".block-actions-wrapper.selected").length > 0;
                            if (blockSelectedExist) {
                                var kunik = cmsConstructor.kunik,
                                id = cmsConstructor.spId;
                                var blockType = cmsConstructor.helpers.getBlockType(kunik,id);
                                if( blockType === "image" ){
                                    const items = (e.originalEvent.clipboardData || e.clipboardData).items;
                                    for (const item of items) {
                                        if (item.type.indexOf("image") !== -1) {
                                            const blob = item.getAsFile();
                                            try {
                                                const response = await cmsConstructor.helpers.uploadFilesImage(blob);
                                                if (response.result) {

                                                    var lang = typeof cmsConstructor.sp_params[id].image.languageSelected !== "undefined" ? cmsConstructor.sp_params[id].image.languageSelected : costum.langCostumActive;
                                                    cmsConstructor.editor.actions.updateBlock(blockType, "image."+lang, "valueToSet", "image", {}, response.docPath, "","updateImage");
                                                    // var ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[id].image);
                                                    // dataToCtrlZ = {};
                                                    // var olderDataChanged = {};
                                                    // var lang = typeof cmsConstructor.sp_params[id].image.languageSelected !== "undefined" ? cmsConstructor.sp_params[id].image.languageSelected : costum.langCostumActive;
                                                    // var newValueToSet = {};
                                                    // tplCtx = {};
                                                    // tplCtx.id = id;
                                                    // tplCtx.collection = "cms";
                                                    // tplCtx.path = "image."+lang;
                                                    // tplCtx.value = response.docPath;


                                                    // jsonHelper.setValueByPath(newValueToSet, "image."+lang, response.docPath);
                                                    // jsonHelper.setValueByPath(olderDataChanged, "image."+lang, jsonHelper.getValueByPath(ancientData, lang));

                                                    // tplCtx.saveLog = {
                                                    //     costumId : costum.contextId,
                                                    //     action : "costum/editBlock",
                                                    //     blockName : cmsConstructor.sp_params[tplCtx.id].path.split(".").pop(),
                                                    //     path : tplCtx.path,
                                                    //     blockPath : cmsConstructor.sp_params[tplCtx.id].path,
                                                    //     page : page,
                                                    //     keyPage : location.hash == "" ? "#welcome" : location.hash,
                                                    //     blockType : blockType,
                                                    //     kunik : kunik,
                                                    //     value : newValueToSet,
                                                    //     olderData : olderDataChanged
                                                    // }
                                                    
                                                    // dataHelper.path2Value(tplCtx, function (params) {
                                                    //     $(".this-content-"+kunik).attr('src', response.docPath);
                                                    //     jsonHelper.setValueByPath(cmsConstructor.sp_params[id], tplCtx.path, response.docPath);


                                                    //     //socket ping change image
                                                    //     costumSocket.emitEvent(wsCO, "update_component", {
                                                    //         functionName: "changeImage", 
                                                    //         data:{ kunik: kunik , value:  response.docPath}
                                                    //     })

                                                    //     //local storage management 
                                                    //     cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                                                    //     var actualPage = location.hash == "" ? "#welcome" : location.hash;
                                                    //     var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                                                    //     ctrlY[actualPage] = [];
                                                    //     localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));

                                                    //     $(`.cmsbuilder-block[data-kunik="${kunik}"]`).click();
                                                    // });
                                                } else {
                                                    toastr.error(response.msg);
                                                }
                                            } catch (error) {
                                                toastr.error("Error uploading image: " + error.statusText);
                                            }
                
                                        }
                                    }
                                } else if ( blockType === "section" ) {
                                    const items = (e.originalEvent.clipboardData || e.clipboardData).items;
                                    for (const item of items) {
                                        if (item.type.indexOf("image") !== -1) {
                                            const blob = item.getAsFile();
                                            try {
                                                const response = await cmsConstructor.helpers.uploadFilesImage(blob);
                                                if (response.result) {
                                                    var ancientData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[id].css);
                                                    var cssData = cmsConstructor.helpers.deepCopy(cmsConstructor.sp_params[id].css);
                                                    dataToCtrlZ = {};
                                                    var olderDataChanged = {};
                                                    var newValueToSet = {};
                                                    tplCtx = {};
                                                    tplCtx.id = id;
                                                    tplCtx.collection = "cms";
                                                    tplCtx.path = "css";
                                                    tplCtx.value = cssData;
                                                    tplCtx.value.backgroundType = "backgroundUpload";
                                                    tplCtx.value.backgroundImage = "url('" + response.docPath + "')";
                                                    

                                                    jsonHelper.setValueByPath(newValueToSet, "css.backgroundType", "backgroundUpload");
                                                    jsonHelper.setValueByPath(newValueToSet, "css.backgroundImage", "url('" + response.docPath + "')");
                                                    jsonHelper.setValueByPath(olderDataChanged, "css.backgroundType", typeof ancientData.backgroundType !== "undefined" ? ancientData.backgroundType : "");
                                                    jsonHelper.setValueByPath(olderDataChanged, "css.backgroundImage", typeof ancientData.backgroundImage !== "undefined" ? ancientData.backgroundImage : "" );


                                                    tplCtx.saveLog = {
                                                        costumId : costum.contextId,
                                                        action : "costum/editBlock",
                                                        blockName : cmsConstructor.sp_params[tplCtx.id].path.split(".").pop(),
                                                        path : tplCtx.path,
                                                        blockPath : cmsConstructor.sp_params[tplCtx.id].path,
                                                        page : page,
                                                        keyPage : location.hash == "" ? "#welcome" : location.hash,
                                                        blockType : blockType,
                                                        kunik : kunik,
                                                        value : newValueToSet,
                                                        olderData : olderDataChanged
                                                    }
                                                    
                                                    dataHelper.path2Value(tplCtx, function (params) {
                                                        $(`.cmsbuilder-block-container[data-kunik="${kunik}"]`).css('background-image', "url('" + response.docPath + "')");
                                                        jsonHelper.setValueByPath(cmsConstructor.sp_params[id], "css.backgroundType", "backgroundUpload");
                                                        jsonHelper.setValueByPath(cmsConstructor.sp_params[id], "css.backgroundImage", "url('" + response.docPath + "')");

                                                        costumSocket.emitEvent(wsCO, "update_component", {
                                                            functionName: "changeBackgroundImage", 
                                                            data:{ id: id , kunik: kunik , backgroundImage: "url('" + response.docPath + "')"}
                                                        })

                                                        //local storage management 
                                                        cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,cmsConstructor.spId,cmsConstructor.kunik,"block",olderDataChanged);
                                                        var actualPage = location.hash == "" ? "#welcome" : location.hash;
                                                        var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costumizer.mode+costum.slug)) || {} ;
                                                        ctrlY[actualPage] = [];
                                                        localStorage.setItem("ctrlY"+costumizer.mode+costum.slug, JSON.stringify(ctrlY));

                                                        $(`.cmsbuilder-block[data-kunik="${kunik}"]`).click();
                                                    });
                                                } else {
                                                    toastr.error(response.msg);
                                                }
                                            } catch (error) {
                                                toastr.error("Error uploading image: " + error.statusText);
                                            }
                
                                        }
                                    }
                                }
                            }
                        }
                    });
                },
            },
            afterUpdate:{
                updateImage : function (valueToSet,name,payload,value,id,path,kunik){
                    $(".this-content-"+kunik).attr('src', value);

                    //socket ping change image
                    costumSocket.emitEvent(wsCO, "update_component", {
                        functionName: "changeImage", 
                        data:{ kunik: kunik , value:  value}
                    })

                    $(`.cmsbuilder-block[data-kunik="${cmsConstructor.kunik}"]`).click();
                }
            },
            bindEventsSuperText:function(){
                var prevText = "";
                var isTextSelectionned = false;
                var divSelectionned = {};
                var typingTimer;
                var doneTypingInterval = 2000; // 2 secondes
                function checkSelection(dom) {
                    const selection = window.getSelection(),
                          range = selection.rangeCount > 0 ? selection.getRangeAt(0) : null;
                    if (range && (dom[0].contains(range.commonAncestorContainer) || range.commonAncestorContainer === dom[0])) {
                        isTextSelectionned = true;
                        divSelectionned = { textSelectionned: selection.toString(), elementParent: dom, range: range };
                    }
                }
                
                // $(".save-text").off().on("click", function(e){
                //     e.stopPropagation() 
                //     $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');
                //     $(this).find("i").removeClass("fa-save").addClass("fa-spin fa-spinner");
                //     if ( typeof cmsConstructor.sp_params[cmsConstructor.spId] != 'undefined'){
                //         clearTimeout(cmsConstructor.sp_params[cmsConstructor.spId].spTextKeyupTimer);
                //     }
                //     cmsConstructor.block.actions.saveTextEdition()  
                   
                // });


                $(".sp-text").off("blur").on("blur", function(e){
                    e.stopPropagation() 
                    field = $(this).data('field');  
                    $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');
                    if(cmsConstructor.textFieldChanged !== "")
                        cmsConstructor.block.actions.saveTextEdition($(this))  
                });

                // $(".btnReset").off().on("click", function(e){
                //     e.stopPropagation() 
                //     $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');
                //     $.each(cmsConstructor.textFieldChanged, function(index, value) {
                //         $spText = $('.sp-text[data-id="' + cmsConstructor.spId + '"][data-field="' + value + '"]');
                //         $previousText = typeof cmsConstructor.sp_params[cmsConstructor.spId][value][$spText.data('lang')] !== "undefined" ? cmsConstructor.sp_params[cmsConstructor.spId][value][$spText.data('lang')] : cmsConstructor.sp_params[cmsConstructor.spId][value][0];
                //         $spText.html($previousText)
                //     });
                //     cmsConstructor.textFieldChanged = [];
                //     $(".cmsbuilder-right-content").removeClass("active")
                //     $("#right-panel").html("")
                //     $(".block-actions-wrapper").removeClass("selected")
                //     delete costum.members.focused[userId]
                //     $("#all-block-container").find(".sp-text[contenteditable=true]").removeAttr('contenteditable');
                // });


                $(".sp-text").off("mousedown").on("mousedown", function(e){
                    e.stopPropagation() 
                    if ($(this).hasClass('element-busy')){
                        cmsConstructor.helpers.appendBusy($(this).attr("data-userId"));
                    } else { 
                        // if($(".cmsbuilder-block-action-list:not(.cmsbuilder-block-action-list[data-kunik='"+$(this).data("kunik")+"'])").find(".save-text").length < 1 || $(".cmsbuilder-block-action-list").find(".save-text").data("id") === $(this).data("id")) {
                            // if ( cmsConstructor.textFieldChanged !== "" ){
                            //     cmsConstructor.block.actions.saveTextEdition(cmsConstructor.textFieldChanged);
                            // } 
                            
                            cmsConstructor.selectedDom = $(this);
                            cmsConstructor.spTextSelected = true;
                            field = cmsConstructor.selectedDom.data("field");
                            cmsConstructor.spId = $(this).data("id");
                            var dataText = cmsConstructor.sp_params[cmsConstructor.spId]
                            //remove editable text 
                            cmsConstructor.helpers.removeEditableSpText();

                            //update sp-text ancient block value for trad
                            var textValue = jsonHelper.getValueByPath(dataText, field);
                            if ( typeof textValue == "string" && ancientBlockWithSptextUpdatable.includes(dataText["path"])){
                                tplCtx = {};
                                tplCtx.collection = "cms";
                                tplCtx.id = cmsConstructor.spId;
                                tplCtx.value = {};
                                tplCtx.value.fr = cmsConstructor.sp_params[cmsConstructor.spId][field];
                                tplCtx.path = field ;

                                dataHelper.path2Value(tplCtx, function (params) {
                                    toastr.success(tradCms.updateValueForTradSucces);
                                    jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], field, tplCtx.value);
                                });
                            }

                        jsonHelper.setValueByPath(cmsConstructor.sp_params[cmsConstructor.spId], "language", cmsConstructor.selectedDom.attr("data-lang"));
                        // cmsConstructor.sp_params[cmsConstructor.spId].language = cmsConstructor.selectedDom.attr("data-lang");
                        if(typeof $(this).data('field') !== 'undefined') {
                            var $cmsbuilderBlock = $(this).closest(".cmsbuilder-block")
                            $(".block-actions-wrapper").removeClass("selected")  
                            $cmsbuilderBlock.find("> .block-actions-wrapper").addClass("selected")
                            cmsConstructor.kunik = "text"+cmsConstructor.spId;
                            cmsConstructor.ownerType = "text";                      
                            cmsConstructor.selectedDom.attr('contenteditable','true');
                            cmsConstructor.layer.actions.selectPath(cmsConstructor.kunik)

                                if ( typeof cmsConstructor.sp_params[cmsConstructor.spId] != 'undefined'){
                                    cmsConstructor.sp_params[cmsConstructor.spId]["spTextKeyupTimer"] = ""
                                }
                            }

                            cmsConstructor.textOnEdit = field;

                        $(".cmsbuilder-center-content").on("mouseup", function(eMouseup) {
                            eMouseup.stopImmediatePropagation();
                            var spTextEditable = $("#all-block-container").find(".sp-text[contenteditable=true]");          
                                if(spTextEditable !== cmsConstructor.selectedDom && typeof cmsConstructor.selectedDom.data('field') !== 'undefined' && !costum.isPageAlias && Object.keys(cmsConstructor.sp_params[cmsConstructor.spId].parent)[0] == costum.contextId){
                                    cmsConstructor.editor.init("#right-panel")
                                    var $this = cmsConstructor.selectedDom

                                    prevText = $this.html()

                                    var thisName = "", 
                                    thisParent = "";
                                    if(notNull(cmsConstructor.sp_params[cmsConstructor.spId])){
                                        thisParent = cmsConstructor.sp_params[cmsConstructor.spId].parent
                                        thisName = cmsConstructor.sp_params[cmsConstructor.spId].name
                                    }
                                    var thisParams = {
                                        name : thisName, 
                                        id: cmsConstructor.spId, 
                                        parentId : thisParent, 
                                        $this : cmsConstructor.selectedDom
                                    };

                                    cmsConstructor.selectedDom.bind("paste", function(e){
                                        e.stopImmediatePropagation()
                                        e.preventDefault()
                                        let text = e.originalEvent.clipboardData;
                                        text = text.getData("text/plain")
                                        document.execCommand('insertText', false, text)
                                    })

                                    setTimeout(function () {             
                                        editable = $this[0];
                                        if (window.getSelection().type != "None") {   
                                            divSelection = saveSelection(editable);    
                                            cmsConstructor.editor.actions.setDefaultValueTextEditor(cmsConstructor.helpers.getStyleOfSelection())
                                            cmsConstructor.editor.events.init()
                                        }   
                                    },10)
                                } 
                                $(".cmsbuilder-center-content").off("mouseup");
                            })
                        // }
                        e.stopImmediatePropagation()      
                    }  
                })

                $(".sp-text").off("keyup").on("keyup", function(e){
                    e.stopPropagation();
                    var textSelected = $(this).text();  
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(function () { 
                        var regex = /]\(#([^)]+)\)/g;
                        var matches = textSelected.match(regex);
                        if (matches) {
                            matches.forEach(function(match) {
                                var pageVerify = match.substring(match.indexOf("](") + 2, match.indexOf(")"));
                                if (typeof costumizer.obj.app[pageVerify] == 'undefined') {
                                    //$(".sp-text").off();
                                    pageVerify = pageVerify.slice(1);
                                    bootbox.confirm({
                                        message: "la page #" + pageVerify + " n'existe pas encore, Voulez-vous le créer ? </div>",
                                        buttons: {
                                            confirm: {
                                                label: "<i class='fa fa-check'></i> " + trad.yes,
                                                className: 'btn-success'
                                            },
                                            cancel: {
                                                label: "<i class='fa fa-times'></i> " + trad.cancel,
                                                className: 'btn-default'
                                            }
                                        },
                                        callback: function (result) {
                                            if (result) {
                                                cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom)
                                                $(".cmsbuilder-right-content").removeClass("active");
                                                costumizer.pages.actions.save();
                                                $("#right-sub-panel-container").css("right", "0px");
                                                $("#right-sub-panel-container .containte-form-page .keySlug input[name='keySlug']").val(pageVerify);
                                                $("#costum-form-pages input[name='keySlug']").trigger("valueChange",{name:"keySlug",value:pageVerify,payload:{}});
                                                costumizer.pages.actions.checkUniqueApp(pageVerify,costumizer.obj.app["#"+pageVerify])
                                                pageVerify = pageVerify.replace(/-/g, ' ')
                                                $("#costum-form-pages input[name='name']").val(pageVerify[0].toUpperCase() + pageVerify.slice(1));
                                                $("#costum-form-pages input[name='name']").trigger('valueChange',{name:"name",value:pageVerify[0].toUpperCase() + pageVerify.slice(1),payload:{}});
                                            }
                                        }
                                    });
                                }
                            });
                           
                        }
                    }, doneTypingInterval); 
                    if (costum.editMode)
                        checkSelection($(this));
                });

                $(".sp-text").off("keydown").on("keydown", function(e){
                    if (costum.editMode) {      
                        field = $(this).data("field"); 
                        cmsConstructor.block.actions.textChanged(field)  
                       
                        editable = $(this)[0];
                        if (e.ctrlKey) {    
                            if (e.keyCode == 65) {
                                setTimeout(function () {     
                                    if (window.getSelection().type != "None") {   
                                        divSelection = saveSelection(editable);                         
                                        cmsConstructor.editor.actions.setDefaultValueTextEditor(cmsConstructor.helpers.getStyleOfSelection())
                                        cmsConstructor.editor.events.init()
                                    }
                                },100)
                            }
                        }             

                        if(e.keyCode==13){
                            e.preventDefault();
                            if (window.getSelection) {
                                var selection = window.getSelection(),
                                range = selection.getRangeAt(0),
                                br = document.createElement("br"),
                                textNode = document.createTextNode("\u00a0"); //Passing " " directly will not end up being shown correctly

                                range.deleteContents();
                                range.insertNode(br);
                                range.collapse(false);
                                range.insertNode(textNode);
                                range.selectNodeContents(textNode);

                                selection.removeAllRanges();
                                selection.addRange(range);

                                return false;
                            }
                        }    
                    } 
                });
                $(".sp-text").off("mouseup").on("mouseup", function(e){
                    if (costum.editMode)
                        checkSelection($(this));
                })

                $(".btn-questionIA").off("click").on("click", function(e) {
                    e.stopPropagation();
                    var btnClicked = $(this);
                    var message = "";
                    var isRestoreSelection 
                    var field = "";
                    if (isTextSelectionned == true && notEmpty(divSelectionned)) {
                        restoreSelection(editable, divSelection);
                        message = (divSelectionned["textSelectionned"] != "") ? divSelectionned["textSelectionned"] : cmsConstructor.selectedDom.text();
                        isTextSelectionned = false;
                    } else {
                        if (typeof cmsConstructor.selectedDom != "undefined")
                            message = cmsConstructor.selectedDom.text();
                            field = $(cmsConstructor.selectedDom).data('field');
                    }
                    if (message != "") {
                        if (typeof costum.useOpenAI == "undefined") {
                            toastr.info(`<button class="btn btn-sm btn-primary btn-show-how-togetKey" id='autocompletion-text'>${tradCms.howgetkeyApiOpenAI}</button>`, tradCms.dontEnteredKeyAPI, {
                                "closeButton": true,
                                "preventDuplicates" : true,
                                "timeOut": 0,
                                "positionClass": "toast-bottom-right",
                                "toastClass": "toast-open-ai-cms", 
                                "extendedTimeOut": "1500", 
                            });
                            $("#autocompletion-text.btn-show-how-togetKey").on("click", function() {
                                $(".bootbox.modal").hide();
                                $(".lbh-costumizer[data-space='generalInfos']").click();
                                $("#costum-use-open-ai .open-ai-modal-information").click();
                            });
                        } else {
                            btnClicked.find("i").removeClass("fa-android").addClass("fa-spin fa-spinner");
                            $(".cmsbuilder-container").css({"pointer-events": "none"});
                            dataHelper.chatopenai(message, function(error, response) {
                                if (notNull(response)) {
                                    btnClicked.find("i").removeClass("fa-spin fa-spinner").addClass("fa-android");
                                    if (notEmpty(divSelectionned) && divSelectionned.textSelectionned != "" && divSelectionned.range) {
                                        const range = divSelectionned.range;
                                        range.deleteContents();
                                        const textNode = document.createTextNode(response);
                                        range.insertNode(textNode);
                                        range.setStartBefore(textNode);
                                        range.setEndAfter(textNode);
                                        const selection = window.getSelection();
                                        selection.removeAllRanges();
                                        selection.addRange(range);
                                    } else {
                                        cmsConstructor.selectedDom.text(response);
                                    }    
                                    cmsConstructor.block.actions.saveTextEdition(cmsConstructor.selectedDom);
                                    $(".cmsbuilder-container").css({"pointer-events": ""});
                                    cmsConstructor.helpers.removeEditableSpText();
                                } else {    
                                    btnClicked.find("i").removeClass("fa-spin fa-spinner").addClass("fa-android");
                                    $(".cmsbuilder-container").css({"pointer-events": ""});
                                    toastr.error(tradCms.errorKeyAPIorConnexion);
                                }
                            });
                        }
                    } else {
                        toastr.info(tradCms.noTextSelected);
                    }
                });
            }
        }
    },
    importExport : {
        getSelectedPage : function(type,data){
            var selectedPage  = []
            var pagesContent  = {
                pages : {},
                thematic : data.thematic
            }       
            // console.log("pagesContent",data) 
            if ($("."+type+"-option").val() != "page") {
                $.each($("#"+type+"Tab").find(".import-exportTpl:checked"), function () {
                    selectedPage.push($(this).data("page"))              
                }); 
            }else{
                selectedPage.push(page)
            }
            $.each(data.pages, function (pageId, pageVal) { 
              pagesContent.pages[pageId] = {}
              pagesContent.pages[pageId].cms = []
              if (typeof pageVal.templateOrigin != "undefined") {
                pagesContent.pages[pageId].templateOrigin = pageVal.templateOrigin;
              }
              if (typeof pageVal.isTplPage != "undefined") {
                pagesContent.pages[pageId].isTplPage = pageVal.isTplPage;
              }
              if (type == "export" &&  typeof costum.app["#"+pageId] != "undefined" && typeof costum.app["#"+pageId].templateOrigin != "undefined") {
                pagesContent.pages[pageId].templateOrigin = costum.app["#"+pageId].templateOrigin;
              }
              // console.log("pagesContent pages"+pageId ,selectedPage)  
                $.each(pageVal.cms, function (blockId, blockVal) {   
                    // console.log("pagesContent #"+pageId ,blockVal)               
                    if (selectedPage.includes(pageId.replace("#",""))) {
                        if ((typeof blockVal.text != "undefined" || typeof blockVal.title != "undefined" || typeof blockVal.css != "undefined"  || typeof blockVal.image != "undefined") ) {
                            pagesContent.pages[pageId].cms.push(blockVal);
                        }
                    }else{
                        delete pagesContent.pages[pageId];
                    }
                }); 
                // console.log("pagesContent #"+blockId ,blockVal)               
                // if (selectedPage.includes(blockVal.page)) {
                //     pagesContent.pages[blockId] = blockVal;
                // }
            }); 

            return pagesContent;
        },
        buildStructure : function(arrayCms){
            const transformedCms = Object.values(arrayCms.allCms).reduce((acc, curr) => {
                const page = curr.page;
                delete curr.page;
                if (!acc.pages[page]) acc.pages[page] = { cms: [] };
                acc.pages[page].cms.push(curr);
                // acc.pages[page].templateOrigin = "66f15b4defff59588e0751fb"; 
                if (typeof costum.app["#"+page].templateOrigin != "undefined") {
                    acc.pages[page].templateOrigin = costum.app["#"+page].templateOrigin
                }
                return acc;
            }, { pages: {} });

            return transformedCms;
        },
        unsetTemplateOrigin : function(){
            if (typeof costum.app["#"+page].templateOrigin != "undefined") {                
                var unsetTemplateOrigin = {
                    id         : costum.contextId,
                    collection : costum.contextType,
                    path       : 'costum.app.#'+page+'.templateOrigin'
                }
                dataHelper.unsetPath( unsetTemplateOrigin, function(params) {
                    delete costum.app["#"+page].templateOrigin
                } );
            }
        },
        exportCMS : function(data){

          const jsonString = JSON.stringify(data, null, 2);
          const blob = new Blob([jsonString], { type: "application/json" });
          const link = document.createElement("a");
          link.href = URL.createObjectURL(blob);
          link.download = data.thematic+"_" + costum.contextSlug + "_" + getCurrentDateTime() + ".json";
          link.click();
        },
        /*importCMS : function(dataJson){          
            var message = "Importation du JSON";
            var importedJson = {};
            message += `<input type="file" id="fileInputJson" accept=".json" />`
            message += `<div class="json-displayer"></div>`
            if (true) {}
            bootbox.confirm({
                message: message,
                title: "Templates JSON",
                show: true,
                backdrop: true,
                closeButton: false,
                animate: true,
                className: "my-modal",
                backdrop: 'static',
                buttons: {
                    confirm: {
                        label: "Importer",
                        className: 'btn-success'
                    },
                    cancel: {
                        label: "<i class='fa fa-times'></i> " + trad.cancel,
                        className: 'btn-default'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var index = 0;
                        var isValid = false;
                        mylog.log("jsonData import",importedJson)   
                        mylog.log("jsonData exist",dataJson)
                        var prepData = {
                            allCms : {},
                            templateOrigin : dataJson.templateOrigin
                        }
                        // $.each(dataJson.allCms, function (key, cms) { 
                        //     if (typeof cms.path != "undefined" && cms.path == importedJson.allCms[Object.keys(importedJson.allCms)[index]].path) {  
                        //         prepData.allCms[cms.id] = importedJson.allCms[Object.keys(importedJson.allCms)[index]]     
                        //         index++
                        //         isValid = true;                   
                        //     }else{
                        //         toastr.error("Importation échoué. Structure incompatible")
                        //         isValid = false
                        //         return isValid;
                        //     }

                        // })       
                        // if (isValid) {
                            ajaxPost(
                                null,
                                baseUrl+"/"+moduleId+"/cms/importbcms",
                                importedJson,
                                function(data){
                                    toastr.success(trad.finish)
                                    urlCtrl.loadByHash(location.hash);
                                },
                                null

                                );
                        // }
                    }
                }
            });            

            $('#fileInputJson').on('click', function(event) {
                $(".json-displayer").html("")
            })

            $('#fileInputJson').click()

             $('#fileInputJson').on('change', function(event) {
                const file = event.target.files[0];
                if (file) {
                    const reader = new FileReader();
                    reader.onload = function(e) {
                        const content = e.target.result;
                        importedJson = JSON.parse(content);
                        mylog.log('jsonData', importedJson)
                        if (typeof importedJson.allCms != "undefined" && typeof importedJson.templateOrigin != "undefined" && typeof costum.app["#"+page].templateOrigin != "undefined"  && costum.app["#"+page].templateOrigin == importedJson.templateOrigin) {
                            var count = 1
                            $.each(importedJson.allCms, function (index, cms) {
                                    mylog.log("Importer", cms)
                                    var cmsContent = `<p class="editable-cms" data-id="${index}">${count++} ${cms.path.split('.').pop()}</p>`;
                                    if ((cms.path == "tpls.blockCms.superCms.elements.supertext" || "tpls.blockCms.superCms.elements.button") && typeof cms.text != "undefined") {
                                        if (cms.text.hasOwnProperty(costum.langCostumActive)) {
                                            cmsContent += cms.text[costum.langCostumActive];
                                        } else {
                                            cmsContent += cms.text[Object.keys(cms.text)[0]];
                                        }
                                    }
                                    if (cms.path == "tpls.blockCms.superCms.elements.title" && typeof cms.title != "undefined") {
                                        if (cms.title.hasOwnProperty(costum.langCostumActive)) {
                                            cmsContent += cms.title[costum.langCostumActive];
                                        } else {
                                            cmsContent += cms.title[Object.keys(cms.title)[0]];
                                        }
                                    } 
                                    if (cms.path == "tpls.blockCms.superCms.elements.image" && typeof cms.image != "undefined") {
                                        cmsContent += `<img style="height:200px;" src="${cms.image.hasOwùnProperty(costum.langCostumActive) ? cms.image[costum.langCostumActive] : cmsContent += cms.image[Object.keys(cms.image)[0]]}">`;
                                    }

                                    $(".json-displayer").append(cmsContent) 
                            });
                        }else{
                            toastr.error("Importation échoué. Structure incompatible")
                        }

                    };
                    reader.readAsText(file);
                }
            });
        },*/
         getPageContentJson : function(){  
         var importedJson = {}    
         var selectedJson = {}
         const docUrl     = `<br><a href="https://oce.co.tools/citoyens/notes/5efb85e369086498098b4c4b/editorDoc/c6ebacb99a3aa9a2602f968a" target="_blank">Lire la documentation</a>`
           ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/cms/exportbcmsasjson",
                {
                    page : page
                },
                function(pagesData){
                    mylog.log("getPageContentJson ddss", pagesData)
                    var pageContent  = ""
                    var newStructure = {}
                    var isImportable = false;


                    pageContent += `                    
                    <div class="import-export-tabset">
                      <input class="check-import-export" type="radio" name="tabset" id="export" aria-controls="exportTab" checked>
                      <label for="export">Exporter</label>
                      <input class="check-import-export" type="radio" name="tabset" id="import" aria-controls="importTab">
                      <label for="import">Importer</label>
                      
                      <div class="tab-panels">
                        <section id="exportTab" class="import-export-tab-panel">`
                            var countPageDispo = 0;
                            var pageDispo      = "";
                            pageContent += `<label>Type de l'export</label>`
                            pageContent += `<select class="form-control export-option">
                                                <option value="default" selected disabled>${tradCms.selectAnOption}</option>
                                                <option value="costum">Costum entier</option>
                                                <option value="page">Cette page seulement</option>
                                           </select>`
                            $.each(pagesData.pages, function (keyPage, page) {
                                if (typeof page.templateOrigin != "undefined") {
                                    countPageDispo++
                                    pageDispo += ` 
                                    <div>
                                    <input type="checkbox" class="import-exportTpl" data-page="${keyPage.replace("#","")}" name="scales" />
                                    <span for="scales">#${keyPage}</span>
                                    </div>`
                                }
                            });                                 
                            mylog.log("getPageContentJson page", countPageDispo)
                            pageContent +="<div class='exportCostum hidden'>"
                            if (countPageDispo > 0) {
                                pageContent += "<strong>Pages disponibles:</strong><br>"+pageDispo
                            }else{
                                pageContent += `Aucune page exportable. Veuillez enregistrer cet COSTUM en tant que Template`
                                setTimeout(function() {                                    
                                $(".modal-content").addClass("import-only")
                                },50)
                            }
                            pageContent +="</div>"
                            pageContent += `<label for="export">Nom du json ou Thématique *</label>`
                            pageContent += `<input type="text" name="name" class="form-control thematic-input" value="">`
                            pageContent += `<div class="json-message" style="border-top: 1px solid #e5e5e5;"></div>`
                            pageContent += `
                        </section>
                        <section id="importTab" class="import-export-tab-panel">
                            <input type="file" id="fileInputJson" accept=".json" />
                            <div class="json-selecter"></div>
                            <div class="json-displayer" style="max-height: 170px;overflow-y: scroll;"></div>
                        </section>
                      </div>                      
                    </div>`

                    bootbox.dialog({
                        message: `<div style="display:flex;flex-direction: column">${pageContent}</div>`,
                        title: "Importer/Exporter le(s) contenu(s) de ce COSTUM ",
                        show: true,
                        backdrop: true,
                        closeButton: false,
                        animate: true,
                        className: "my-modal",
                        backdrop: 'static',
                        buttons: {
                            import: {
                                label: "<i class='fa fa-download'></i> Importer",
                                className: 'btn-success importer-cms hidden',
                                callback: function() {                       
                                    $(".importer-cms").prop("disabled",true)
                                    $(".importer-cms").find("i").replaceWith("<i class='fa fa-spinner fa-spin'></i>")
                                    if ($(".import-option").val() != "default" && typeof selectedJson.isUploaded == "undefined") {
                                        co.importCostumContent({ thematic : $(".import-option").val(), page : page })
                                        .then(() => {
                                            window.location.reload();
                                        })
                                        .catch((error) => {
                                            toastr.error("Error importing content:", error);
                                        });
                                    }else{                                        
                                     selectedJson = cmsConstructor.importExport.getSelectedPage("import",selectedJson)
                                     // console.log("selectedJson",selectedJson)
                                     selectedJson["page"] = page
                                     $(".import-exportTpl").prop('disabled', true);
                                     $(".importer-cms").addClass("hidden")
                                     ajaxPost(
                                        null,
                                        baseUrl+"/"+moduleId+"/cms/importbcms",
                                        selectedJson,
                                        function(data){
                                            window.location.reload()
                                        },
                                        null

                                        );
                                 }
                                 return false;
                             }
                            },
                            save: {
                                label: trad.save,
                                className: 'btn-default export-to-db export-cms disabled',
                                callback: function() {
                                    var isExportable = false;
                                    if ($(".thematic-input").val() != "") { 
                                        isExportable = true;
                                        if ($(".export-option").val() == "page") { 
                                            if (typeof pagesData.pages[page] != "undefined") {
                                                pagesData.pages = {
                                                    [page] : pagesData.pages[page]
                                                }
                                                pagesData.pages[page].isTplPage = true
                                            }else{                 
                                                isExportable = false;                               
                                                $(".json-message").html("<span class='text-info'>Cette page ne peut pas être exporter! "+docUrl+"</span>")
                                            }    
                                        }
                                    }else{
                                        isExportable = false;
                                        $(".json-message").html("<span class='text-red'>Le champ thématique est obligatoire</span>")
                                    }
                                    if (isExportable) {
                                       pagesData["thematic"] = ($(".thematic-input").val()).toLowerCase();
                                       ajaxPost(
                                        null,
                                        baseUrl+"/"+moduleId+"/cms/inserttemplatejson",
                                        cmsConstructor.importExport.getSelectedPage("export",pagesData),
                                        function(data){
                                            toastr.success(trad.saved)
                                        },
                                        null

                                        );
                                    }else{                                        
                                        return false;
                                    }
                                }
                            },
                            export: {
                                label: "Exporter",
                                className: 'btn-primary export-cms disabled',
                                callback: function() {
                                    var isExportable = false;
                                    if ($(".thematic-input").val() != "") { 
                                        isExportable = true;
                                        if ($(".export-option").val() == "page") { 
                                            if (typeof pagesData.pages[page] != "undefined") {
                                                pagesData.pages = {
                                                    [page] : pagesData.pages[page]
                                                }
                                                pagesData.pages[page].isTplPage = true
                                            }else{                 
                                                isExportable = false;                               
                                                $(".json-message").html("<span class='text-info'>Cette page ne peut pas être exporter! "+docUrl+"</span>")
                                            }    
                                        }
                                    }else{
                                        isExportable = false;
                                        $(".json-message").html("<span class='text-red'>Le champ thématique est obligatoire</span>")
                                    }
                                    if (isExportable) {                                       
                                        pagesData["thematic"] = $(".thematic-input").val();
                                        cmsConstructor.importExport.exportCMS(cmsConstructor.importExport.getSelectedPage("export",pagesData));
                                    }else{                                        
                                        return false;
                                    }
                                }
                            },
                            cancel: {
                                label: "<i class='fa fa-times'></i> " + trad.cancel,
                                className: 'btn-default',
                                callback: function() {
                                }
                            }
                        }
                    });
                    // if (!isImportable) {
                    //     $('.importer-cms').first().prop('disabled', true);
                        // $(".json-message").html(`<p class="text-red padding-10" style="border-radius: 5px;display:flex;align-items: center;"><i class="fa fa-warning" style="font-size: 40px;"></i><span style="padding-left: 15px;">La structure de la page ne correspond à aucun template.</span></p>`)
                    // }
                    $(".check-import-export").change(function () {
                        if ($(this).attr("id") == "import") {
                            $(".importer-cms").removeClass("hidden")
                            $(".export-cms").addClass("hidden")
                            $(".json-displayer").html("")
                            
                            ajaxPost(
                                null,
                                baseUrl+"/"+moduleId+"/cms/gettemplatejson",
                                {},
                                function(data){
                                    importedJson = data
                                    const sortedData = Object.fromEntries(
                                        Object.entries(data.items).sort(([ , a], [ , b]) => a.thematic.localeCompare(b.thematic))
                                        );
                                    var jsonlistHtml = `<div class="sp-cms-100 text-center" style="justify-content: center;align-content: center;height: 50px;border-top: solid 1.5px darkgray;margin-top: 20px;"><strong>Json disponible.</strong></div>`;
                                    jsonlistHtml += "<select class='form-control import-option'>"
                                    jsonlistHtml += `<option value="default" disabled selected>${tradCms.selectAnOption}</option>`
                                    $.each(sortedData, function (idJson, json) {
                                        var jsonType = ""
                                        if (Object.keys(json.pages).length == 1 && typeof json.pages[Object.keys(json.pages)[0]].isTplPage != "undefined") {
                                            jsonType = "<span class='text-turq'>(page)</span>"
                                        }
                                        jsonlistHtml += `<option value="${idJson}" class="tpl-json-item">${json.thematic} ${jsonType}`
                                        // $.each(json.pages, function (idPage, page) {
                                        //     jsonlistHtml += `<option class="cursor-hand tpl-json-item sp-cms-100 text-center" style="justify-content: center;align-content: center;height: 50px;background-color: whitesmoke;border-bottom: solid 1px lightgray;">${idPage}</option>`
                                        // })
                                        jsonlistHtml +="</option>"

                                    })
                                    jsonlistHtml += "</select>"

                                    $(".json-selecter").html(jsonlistHtml)
                                    // console.log("gettemplatejson", data)
                                },
                                null

                            );
                        }else{                            
                            $(".importer-cms").addClass("hidden")
                            $(".export-cms").removeClass("hidden")
                        }
                    })

                    $("#exportTab").on("change",".export-option",function () {
                        const $this = $(this);
                        $(".json-message").html("")
                        $(".export-cms").removeClass("disabled")
                        if (typeof costum.app["#"+page].templateOrigin != "undefined") {
                            ajaxPost(
                            null,
                            baseUrl+"/"+moduleId+"/cms/gettemplate",
                            {templateType : "templates", templateId : costum.app["#"+page].templateOrigin},
                            function (data) {    
                                // console.log(data.template.type)
                                if (data.template.type != $this.val()) {
                                    $(".json-message").html("<span class='text-info'>Cette page ne peut pas être exporter en "+$this.val()+" json! "+docUrl+"</span>")                                    
                                    $(".export-cms").addClass("disabled")
                                }

                                if ($this.val() == "page") {      
                                    $(".exportCostum").addClass("hidden")       
                                }else{               
                                    $("#exportTab").find(".import-exportTpl").prop('checked', true);
                                    $(".exportCostum").removeClass("hidden")

                                }
                            }      
                            );  
                        }else{
                            $(".json-message").html("<span class='text-info'>Cette page ne peut pas être exporter en "+$this.val()+" json! "+docUrl+"</span>")                        
                            $(".export-cms").addClass("disabled")
                        }        
                        
                    })

                    // $("#importTab").on("click",".tpl-json-item",function () {
                    //     $(".tpl-json-item").css("background-color", "whitesmoke").removeClass("selected")
                    //     $(this).css("background-color", "gainsboro").addClass("selected")
                    // })

                    $("#importTab").on("change",".import-option",function () {
                        // $(".importer-cms").removeClass("hidden")
                        mylog.log('jsonData'+$(this).val(), importedJson.items[$(this).val()])
                        var count = 1
                        var pages = []
                        const jsonId = $(this).val()
                        var jsonSelectedHtml = ""
                        selectedJson = importedJson.items[jsonId]
                        // mylog.log("Importer", Object.keys(selectedJson.pages).length,typeof selectedJson.pages[Object.keys(selectedJson.pages)[0]].isTplPage != "undefined")
                        if (Object.keys(selectedJson.pages).length == 1 && typeof selectedJson.pages[Object.keys(selectedJson.pages)[0]].isTplPage != "undefined") {
                            jsonSelectedHtml += "<span class='text-turq'>(JSON pour une seule page. Il sera importé dans la page "+page+")</span>"
                        }else{               
                            jsonSelectedHtml = "<strong>Les pages disponibles à importer.</strong>"             
                            $.each(selectedJson.pages, function (index, cms) {

                                if ($(".pageImported-"+index).length == 0) {                 
                                    jsonSelectedHtml += `<div class="pageImported-${index}">
                                    <input type="checkbox" class="import-exportTpl" data-page="${index}" name="scales" checked disabled/>
                                    <span for="scales">${index}</span>
                                    </div>`
                                }

                            });
                        }

                        $(".json-displayer").html(jsonSelectedHtml) 
                    })

                    $('#fileInputJson').on('click', function(event) {
                        $(".json-displayer").html("")
                        $(".import-option").val("default")
                    })

                    // $('#fileInputJson').click()

                    $('#fileInputJson').on('change', function(event) {
                        $(".tpl-json-item").removeClass("selected")
                        const file = event.target.files[0];
                        if (file) {
                            const reader = new FileReader();
                            reader.onload = function(e) {
                                const content = e.target.result;
                                selectedJson = JSON.parse(content);
                                selectedJson["isUploaded"] = true;
                                $(".importer-cms").removeClass("hidden")
                                mylog.log('jsonData', selectedJson)
                                // if (typeof importedJson.allCms != "undefined" && typeof selectedJson.templateOrigin != "undefined" && typeof costum.app["#"+page].templateOrigin != "undefined"  && costum.app["#"+page].templateOrigin == importedJson.templateOrigin) {
                                    var count = 1
                                    var pages = []
                                    $(".json-displayer").append(`<strong>Les pages disponibles à importer.</strong>`) 
                                    $.each(selectedJson.pages, function (index, cms) {
                                        mylog.log("Importer", cms)
                                        // var cmsContent = `<p class="editable-cms" data-id="${index}">${count++} ${cms.path.split('.').pop()}</p>`;
                                        // if ((cms.path == "tpls.blockCms.superCms.elements.supertext" || "tpls.blockCms.superCms.elements.button") && typeof cms.text != "undefined") {
                                        //     if (cms.text.hasOwnProperty(costum.langCostumActive)) {
                                        //         cmsContent += cms.text[costum.langCostumActive];
                                        //     } else {
                                        //         cmsContent += cms.text[Object.keys(cms.text)[0]];
                                        //     }
                                        // }
                                        // if (cms.path == "tpls.blockCms.superCms.elements.title" && typeof cms.title != "undefined") {
                                        //     if (cms.title.hasOwnProperty(costum.langCostumActive)) {
                                        //         cmsContent += cms.title[costum.langCostumActive];
                                        //     } else {
                                        //         cmsContent += cms.title[Object.keys(cms.title)[0]];
                                        //     }
                                        // } 
                                        // if (cms.path == "tpls.blockCms.superCms.elements.image" && typeof cms.image != "undefined") {
                                        //     cmsContent += `<img style="height:200px;" src="${cms.image.hasOwùnProperty(costum.langCostumActive) ? cms.image[costum.langCostumActive] : cmsContent += cms.image[Object.keys(cms.image)[0]]}">`;
                                        // }

                                        if ($(".pageImported-"+index).length == 0) {                                            
                                            $(".json-displayer").append(`<div class="pageImported-${index}">
                                                    <input type="checkbox" class="import-exportTpl" data-page="${index}" name="scales" checked disabled/>
                                                    <span for="scales">${index}</span>
                                                </div>`) 
                                        }

                                    });
                                // }else{
                                //     toastr.error("Importation échoué. Structure incompatible")
                                // }

                            };
                            reader.readAsText(file);
                        }
                    });

                },
                null

                );
        }
    }
}


 /**************Required for execCommand*************/
var saveSelection, restoreSelection, divSelection;
var editable = null;

var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"); 

// function rgb2hex(rgb) {
//  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
//  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
// }

// function hex(x) {
//   return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
//  }

if (window.getSelection && document.createRange) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = win.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var selectedTextRange = doc.selection.createRange();
        var preSelectionTextRange = doc.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}

// socketInit();