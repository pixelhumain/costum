aicostumizer = {
	init : function(){
		$(".terminal").html(`
			<div class=fakeMenu>
			  <div class="fakeButtons fakeClose"></div>
			  <div class="fakeButtons fakeMinimize"></div>
			  <div class="fakeButtons fakeZoom"></div>
			</div>
			<div class="fakeScreen">
			  <p class="line1">$ yo gulp-webapp<span class="cursor1">_</span></p>
			  <p class="line2">Out of the box I include HTML5 Boilerplate, jQuery, and a gulpfile.js to build your app.<span class="cursor2">_</span></p>
			  <p class="line3">[?] What more would you like? (Press space to select)<span class="cursor3">_</span></p>
			  <p class="line4">><span class="cursor4">_</span></p>
			</div>

			`)
	}
}