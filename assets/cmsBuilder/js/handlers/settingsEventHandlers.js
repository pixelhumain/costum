function showSettings(dom, title, link, saveButton){
    if($("#toolsBar-edit-block").is(':visible')){
        $("#toolsBar-edit-block").hide();  
    };

    //costumizer.closeModal();
    $("#menu-right-costumizer, #menu-right-costumizer .modal-tools-costumizer").css({"width":"300px"});
    let domTarget = dom ? dom : "#modal-content-costumizer-administration";
    $(domTarget).fadeIn(500);
    coInterface.showLoader(domTarget+" .contains-view-admin");
    $(domTarget).find(".costumizer-title > span").text(title);
    // Initialization of views
    costumizer.space=link;
    if(typeof costumizer[costumizer.space] != "undefined"
        && typeof costumizer[costumizer.space].init === "function")
        costumizer[costumizer.space].init();
    
    
    if(typeof costumizer[costumizer.space].views != "undefined"
        && typeof costumizer[costumizer.space].views.init === "function"){
        $(domTarget+" .contains-view-admin").html(costumizer[costumizer.space].views.init());
    }
    else if(typeof costumizer[costumizer.space].views != "undefined"
        && typeof costumizer[costumizer.space].views.initAjax === "function"){
            costumizer[costumizer.space].views.initAjax(domTarget+" .contains-view-admin");
    }
    else{
        ajaxPost(domTarget+" .contains-view-admin", baseUrl+"/"+moduleId+"/cms/admindashboard", 
            { 
                type: costum.contextType,
                id: costum.contextId,
                view:costumizer.space
            },
            function(){r[costumizer.space].events != "undefined"
        && typeof costu
                if(typeof costumizer[costumizer.space].events != "undefined"
                    && typeof costumizer[costumizer.space].events.bind === "function")
                    costumizer[costumizer.space].events.bind();
            }
        );
    }
    //Bind events for space costumizer opened 
    if(typeof costumizer[costumizer.space].events.bind === "function")
        costumizer[costumizer.space].events.bind();
    //Function initCallback for workspace costumizer opened
    if(typeof costumizer[costumizer.space].actions != "undefined"
        && typeof costumizer[costumizer.space].actions.init === "function")
        costumizer[costumizer.space].actions.init();

    if(saveButton)
        $(".save-costumizer").show().addClass("disabled");
    else
        $(".save-costumizer").hide();
    $("#ajax-modal.portfolio-modal.modal").css({"z-index":"100003","top" : "100px", "left": "40px"});
}

costumizerV2.on(costumizerV2.actions.EDIT_COSTUM_PAGE, function(){
    /* add the business logic here */

    alert(`Add the business logic of EDIT_COSTUM_PAGE in: "modules/costum/assets/cmsBuilder/js/handlers/settingsEventHandlers.js:4"`)
})

costumizerV2.on(costumizerV2.actions.EDIT_SITEMAP, function(){
    /* add the business logic here */

    mylog.log("customizerV2", `Add the business logic of EDIT_SITEMAP in: "modules/costum/assets/cmsBuilder/js/handlers/settingsEventHandlers.js:10"`)
    
    showSettings("#modal-content-costumizer-administration","Test", "htmlConstruct", null)
})

costumizerV2.on(costumizerV2.actions.EDIT_COSTUM_CSS, function(){
    /* add the business logic here */

    alert(`Add the business logic of EDIT_COSTUM_CSS in: "modules/costum/assets/cmsBuilder/js/handlers/settingsEventHandlers.js:16"`)
})

costumizerV2.on(costumizerV2.actions.EDIT_COSTUM_ADMIN, function(){
    /* add the business logic here */

    alert(`Add the business logic of EDIT_COSTUM_ADMIN in: "modules/costum/assets/cmsBuilder/js/handlers/settingsEventHandlers.js:22"`)
})

costumizerV2.on(costumizerV2.actions.EDIT_COSTUM_SETTING, function(){
    /* add the business logic here */

    alert(`Add the business logic of EDIT_COSTUM_SETTING in: "modules/costum/assets/cmsBuilder/js/handlers/settingsEventHandlers.js:28"`)
})