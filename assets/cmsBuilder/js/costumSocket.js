costumSocket = {
	socketId: "",
	init : function() {
		wsCOpubSub.subscriber(function(wsCO){
			costumSocket.emitEvent(wsCO, "join_room", {userId: userId, userName: currentUser.name})
			wsCO
			.on("update_state", (params) => {
				var adminReady = setInterval(function () {
					if (typeof costum.members  != "undefined")	{
						clearInterval(adminReady);	
						costumSocket.userState.reveal(params)
					}
				}, 300)
			})
			.on("update_component", (params) => {
				if ( typeof params.functionName === "object"){
					$.each(params.functionName, function(key, value) {
						costumSocket.socketListener[value](params.data);
					}) 
				} else {
					costumSocket.socketListener[params.functionName](params.data);
				}

			})
			.on("tpl_change", (params) => {
				costumSocket.socketListener[params.functionName](params.data);
			})
			.on("edit_component", (params) => {						
				costumSocket.userState.reveal(params)
				// $.each(params.state.componentsOnEdit, function(key, value) {					
				// 	costumSocket.socketListener.elementFocused({userId:key,elementTarget:value.elementTarget, page : value.page});	
				// }) 

			})
			.on("user_leave", (params) => {
				costumSocket.userState.userDisconnected(params.userId) 
			})
		})
	},
	emitEvent: function(wsCO, nameEvent, params) {
		if (coWsConfig.enable) {
			wsCO.emit("costum_event", {
				name: nameEvent,
				costumId: costum.contextId,
				params: params,
				page: page
			})
		}
	},
	userState : {		
		reveal : function(params) {
			if (notEmpty(params.state.members)){
				$.each(params.state.members, function(key, value) {
					costumSocket.userState.userConnected(key);
					if(typeof params.state.componentsOnEdit != "undefined" && typeof params.state.componentsOnEdit[key] != "undefined" && userId != key){
						costumSocket.socketListener.elementFocused({userId:key,elementTarget:params.state.componentsOnEdit[key].elementTarget, page : params.state.componentsOnEdit[key].page});	
						costum.members.focused = params.state.componentsOnEdit
					}
				})
			}
		},		
		userConnected : function(connectedUserId){
			if ($(".useractive").find(".admin"+connectedUserId).length == 0) {
				console.log("atoy ary", costum.members, costum.members.lists, connectedUserId);
				$(".useractive").append(costum.members["lists"][connectedUserId].avatar)
				$(".useradmin").append(costum.members["lists"][connectedUserId].avatarWithName)
				$("body").find(".user-dot[data-userid='"+connectedUserId+"']").addClass("active-now")
				if (connectedUserId != userId) {
					$("#connectedAudio")[0].play();
				}
			}
			costum.members.active[connectedUserId] = costum.members["lists"][connectedUserId].slug;
			costum.members["onlineCounter"] = Object.keys(costum.members.active).length;			
			$(".onlineCounter").html(costum.members.onlineCounter)

			costum.members["lists"][connectedUserId]["avatarWithName"] = `<div class="user-admin sp-cms-100" data-userid="${connectedUserId}">${costum.members["lists"][connectedUserId]["avatar"]}
			<a href="${typeof costum.members["lists"][connectedUserId].slug != "undefined" ? baseUrl+"#@"+costum.members["lists"][connectedUserId].slug : ""}" 
			class="avatar-admin-name"
			target='_blank'> ${costum.members["lists"][connectedUserId].name}
			<span class="pull-right" style="right: 0;position: absolute;">
			<i data-userid="${connectedUserId}" class="user-dot fa fa-circle active-now"></i>
			</span>
			</a></div>`;
		},
		userDisconnected : function(deconnectedUserId) {
			if ($(".useractive").find(".admin"+deconnectedUserId).length > 0) {
				$('.admin'+deconnectedUserId).popover('hide');
				$(".useractive").find(".admin"+deconnectedUserId).remove()
				$(".useradmin").find(".user-admin[data-userid='"+deconnectedUserId+"']").remove()
				$("body").find(".user-dot[data-userid='"+deconnectedUserId+"']").removeClass("active-now")
				if (deconnectedUserId != userId) {
					$("#leavedAudio")[0].play();
				}
			}
			if (typeof costum.members != "undefined") {
				if (typeof costum.members.active != "undefined")
					delete costum.members.active[deconnectedUserId];
				if (typeof costum.members.focused != "undefined")
					delete costum.members.focused[deconnectedUserId];
			}

			costum.members["onlineCounter"] = Object.keys(costum.members.active).length;
			$(".onlineCounter").html(costum.members.onlineCounter)

			costum.members["lists"][deconnectedUserId]["avatarWithName"] = `<div class="user-admin sp-cms-100" data-userid="${deconnectedUserId}">${costum.members["lists"][deconnectedUserId]["avatar"]}
			<a href="${typeof costum.members["lists"][deconnectedUserId].slug != "undefined" ? baseUrl+"#@"+costum.members["lists"][deconnectedUserId].slug : ""}" 
			class="avatar-admin-name"
			target='_blank'> ${costum.members["lists"][deconnectedUserId].name}
						<span class="pull-right" style="right: 0;position: absolute;">
					<i data-userid="${deconnectedUserId}" class="user-dot fa fa-circle"></i>
				</span>
			</a></div>`;
			costumizer.actions.onElementUnfocused({userId: deconnectedUserId});
		},
	},
	socketListener: {
        cssChange : function(data){

			changeMode = cmsConstructor.helpers.giveModeValue(data.mode);
			actualMode = cmsConstructor.helpers.giveModeValue(costumizer.mode)

			
			if ( changeMode === actualMode ) {
				cmsConstructor.editor.actions.onChangeCss(data.kunik,data.spId,data.path, data.valueToSet, data.name, data.payload, data.value, data.tabKey, data.css);
			} else if ( changeMode > actualMode) {
				dataCss = cmsConstructor.sp_params[data.spId]["css"] ?? [];
				if ( typeof dataCss[costumizer.mode] === "undefined" || typeof dataCss[costumizer.mode][data.name] === "undefined" ) {
					cmsConstructor.editor.actions.onChangeCss(data.kunik,data.spId,data.path, data.valueToSet, data.name, data.payload, data.value, data.tabKey, data.css);
				}
			}
        },
        insertSection : function(data) {
			if (data.page === location.hash){
				if ( data.reloadByHash) {
					urlCtrl.loadByHash( location.hash );
				} else {
					switch(data.position){
						case "in":
							$("#all-block-container").append(data.view)
						break;
						case "before":
							$(data.view).insertBefore($(`.block-container-${data.parentKunik}`))
						break;
						case "after":
							$(data.view).insertAfter($(`.block-container-${data.parentKunik}`))
						break;
					};
					cmsBuilder.block.initEvent(data.idBlock, data.idBlock);
					coInterface.initHtmlPosition();
					toastr.success("un utilisateur a ajouter un nouveau section");
				}
			}
        },
        insertBlockCms : function(data) {
			if (data.page === location.hash){
				$(".empty-sp-element"+data.kunik).addClass("d-none");  
				var strNewBlc = '<div class="col-md-12 sample-cms sample-cms'+data.id+' text-center custom-block-cms other-cms" data-id="undefined">'+
						'<div class="selected cms-area-selected blink-info">'+trad.waitendofloading+'</div>'+
						'</div>';
				$(".cmsbuilder-block-container[data-id="+data.id+"]").append(strNewBlc); 
				$(".sample-cms"+data.id).replaceWith(data.view)
				toastr.success("un utilisateur a ajouter un nouveau block");
				cmsBuilder.block.initEvent();
			}
        },
        deleteBlock : function(data) {
            delete costum.members.focused[data.userId]
            cmsConstructor.helpers.deleteRemoveElement(data.idBlock);
        },
		updateObjAfterChange: function(data) {
			jsonHelper.setValueByPath(costumizer.obj, data["path"], data["value"]);
		},
        addStyleUI : function(data) {
            cssHelpers.render.addStyleUI(data["path"], data["value"], data["dom"]);	
        },
		removeStyleUI: function(data) {
			cssHelpers.render.removeStyleUI(data["arrayStyleToRemove"], data["dom"]);
		},
		initCssCustom: function(data) {
			costum.initCssCustom(data["obj"], data["path"]);
		},
		addClassSection: function(obj, path) {
			costum.addClassSection(obj,path);
		},
		refreshmenu: function(data) {
			costumizer.htmlConstruct.actions.refreshmenu(data["pathToUp"], data["obj"], data["isSocket"]);
		},
		revealFocus: function() {
			$.each(costum.members.focused, function(key, value) {	
				if (notEmpty(value)) {
					var params = {
						userId:key,
						elementTarget:value.elementTarget
					}
					if (typeof value.page != "undefined") {
						params["page"] = value.page
					}
					costumSocket.socketListener.elementFocused(params);
				}		
			})
		},
		updateCostum: function(data) {
			$.each(data, function(key, value) {
				jsonHelper.setValueByPath(costumizer.obj, key, value);
				jsonHelper.setValueByPath(costum, key, value);
			})
        },
        elementFocused : function(data){
        	if (data.userId != userId) {
        		if (typeof data.elementTarget != "undefined" && !(data.elementTarget).includes("lbh-costumizer") && !(data.elementTarget).includes("data-path")) {
        			if ($(data.elementTarget).hasClass("configuration-menus-costumizer")) {
        				costumSocket.socketListener.elementUnfocused({userId: data.userId});
        				$(data.elementTarget).addClass("element-busy")
        				$(data.elementTarget).attr("data-userid",data.userId) 
        				var $actions = $(`<div class="block-busy-wrapper" data-anchor-target="anchor${data.userId}" data-userid="${data.userId}" style="border: 2px solid ${costum.members.lists[data.userId].color};background-color: ${costum.members.lists[data.userId].color}29"></div>`)
        				$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][data.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[data.userId].color};">${costum.members["lists"][data.userId].name} - en train d'éditer</span></div>`);
        				$(data.elementTarget).prepend($actions)
        			}else if ($('[id="'+data.elementTarget+'"]').hasClass("pages-line")){
        				costumSocket.socketListener.elementUnfocused({userId: data.userId});
        				$('[id="'+data.elementTarget+'"]').css("pointer-events", "none")
        				$('[id="'+data.elementTarget+'"]').attr("data-userid",data.userId) 
        				$('[id="'+data.elementTarget+'"]').addClass("edition-desabled")
        				$('[id="'+data.elementTarget+'"]').find(".editPage").attr("data-userid",data.userId)
        				var $actions = $(`<div class="block-busy-wrapper" data-anchor-target="anchor${data.userId}" data-userid="${data.userId}" style="border: 1.5px solid ${costum.members.lists[data.userId].color};background-color: ${costum.members.lists[data.userId].color}29"></div>`)
        				$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][data.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[data.userId].color};">${costum.members["lists"][data.userId].name} - en train d'éditer</span></div>`);
        				$('[id="'+data.elementTarget+'"]').prepend($actions)
        			}else if (!(data.elementTarget).includes("undefined")){
        				costumSocket.socketListener.elementUnfocused({userId: data.userId});
        				var $actions = $(`<div class="block-busy-wrapper" data-userid="${data.userId}" style="border: 2px solid ${costum.members.lists[data.userId].color};background-color: ${costum.members.lists[data.userId].color}29"></div>`)
        				$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][data.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[data.userId].color};">${costum.members["lists"][data.userId].name} - en train d'éditer</span></div>`);
        				$(".cmsbuilder-block[data-kunik='"+data.elementTarget+"']").prepend($actions)
        				if($(".cmsbuilder-block[data-kunik='"+data.elementTarget+"']").data("blocktype") == "text"){            
        					$(".sp-text[data-kunik='"+data.elementTarget+"']").addClass("element-busy")
        					$(".sp-text[data-kunik='"+data.elementTarget+"']").attr("data-userid",data.userId)
        				}                                              
        				$(".cmsbuilder-block[data-kunik='"+data.elementTarget+"']").addClass("element-busy")
        				$(".cmsbuilder-block[data-kunik='"+data.elementTarget+"']").attr("data-userid",data.userId)
						//Layer busy
        				$(".page-layer-item-btn[data-kunik='"+data.elementTarget+"']").addClass("element-busy")
        				$(".page-layer-item-btn[data-kunik='"+data.elementTarget+"']").attr("data-userid",data.userId)

        				if(typeof costumizer.isWaitForLoad != "undefined" && costumizer.isWaitForLoad == data.userId){
        					costumizer.scrollToTarget(".block-busy-wrapper[data-userid='"+data.userId+"']");
        					delete costumizer.isWaitForLoad;
        				}		

        			}
        		}else{
        			costumSocket.socketListener.onMenuBusy(data);
        		}
        		costum.members.focused[data.userId] = data;
			}
        },
        elementUnfocused : function(params){     
        	if (params.userId != userId) {
        		delete costum.members.focused[params.userId]
        		$(".block-busy-wrapper[data-userid='"+params.userId+"'").remove()
        		$(".element-busy[data-userid='"+params.userId+"'").removeClass("element-busy").removeAttr("data-userid")
        		$(".edition-desabled[data-userid='"+params.userId+"'").css({"pointer-events": "", "margin-top": "0"}).css("background-color","").removeClass("edition-desabled").removeAttr("data-userid")
        	}
        },
        moveBlock : function(data) {
            cmsConstructor.helpers.moveBlockView(data.action,data.idBlock,true);
            cmsConstructor.layer.init()
        },
		handlePersistent : function(data) {
			cmsConstructor.helpers.hanldePersistence(data.spId,data.value);
		},
        duplicateBlock: function(data) {
            cmsConstructor.helpers.insertBlockView(data.actionName,data.ownerBlock,"",true,data.newBlockParent,data.newDiv);
        },
		updateBlockData: function(data) {
			cmsConstructor.sp_params[data.id] = data.newData;
		},
		refreshBlock: function(data){
			cmsConstructor.helpers.refreshBlock(data.id, ".cmsbuilder-block[data-id='"+data.id+"']");
			var params = {
				"state": {
					"members": costum.members.active,
					"componentsOnEdit": costum.members.focused
					}
				}
			costumSocket.userState.reveal(params)
		},		
		replaceBlock: function(data){
			cmsConstructor.helpers.replaceBlock(data.id, ".cmsbuilder-block[data-id='"+data.id+"']");
			var params = {
				"state": {
					"members": costum.members.active,
					"componentsOnEdit": costum.members.focused
				}
			}
			costumSocket.userState.reveal(params)
		},
		changeText: function(data){
			$('.sp-text[data-id="' + data.id + '"][data-field="' + data.value + '"]').html(data.text);
		},
		changeBackgroundImage: function(data){
			$(`.cmsbuilder-block-container[data-kunik="${data.kunik}"]`).css('background-image', data.backgroundImage);
			jsonHelper.setValueByPath(cmsConstructor.sp_params[data.id].css,"backgroundImage",data.backgroundImage);
			jsonHelper.setValueByPath(cmsConstructor.sp_params[data.id].css,"backgroundType","backgroundUpload");
		},
        templateChoosed : function(data){
        	costumizer.template.actions.validate({tplId: data.tplId, userId: data.userId,page: data.page, type : data.type});
        },
        newUserConnected : function(data){
        	costumSocket.userState.userConnected(data.userId)
        	costumSocket.userState.refreshUserConnected()
        },
        reloadOnlineList : function(data) {
        	if (typeof data.userFocused != "undefined") {
        		$.each(data.userFocused, function(key, value) {
        			if (value.elementTarget.includes("lbh-costumizer"))
        				costumSocket.socketListener.onMenuBusy({userId: key, elementTarget: value.elementTarget,page:value.page});
        			else 					
        				costumSocket.socketListener.elementFocused({userId:key,elementTarget:value.elementTarget, page : value.page});
        		})
        	}
        },
        response : function(data){
        	if (data.type == "approval") {
        		$(".tpl-confirmation[data-userid='"+data.userId+"']").find("i").removeClass("text-white")
        		$(".tpl-confirmation[data-userid='"+data.userId+"']").find("i").addClass("text-green")
        		var allAdminIsOK = true;

        		$('.tpl-confirmation').each(function() {
        			if (!$(this).find('i').hasClass('text-green')) {
        				allGreen = false;
        				return false; 
        			}
        		});

        		if (allAdminIsOK) {
        			costumizer.template.actions.use(data.tplId, "templates","")
        		}

        	}else if (data.type == "rejection"){        		
        		$(".tpl-confirmation[data-userid='"+data.userId+"']").find("i").attr("class", "fa fa-times-circle text-danger")
        	}
        },
		advancedChange : function(data) {
			if (cmsConstructor.commonEditor.advanced.includes(data.name)) {
				cmsConstructor.helpers.onchangeAdvanced(data.name, data.kunik, data.value, data.spId);
			} else {
				if (cmsConstructor.sp_params[data.spId].path === "tpls.blockCms.superCms.container" && typeof cmsConstructor.sp_params[data.spId].blockParent === "undefined") {
					cmsConstructor.helpers.refreshBlock(data.spId, ".cmsbuilder-block[data-id='"+data.spId+"']");
				} else {
					cmsConstructor.helpers.replaceBlock(data.spId, ".cmsbuilder-block[data-id='"+data.spId+"']");
				}
			}
		},
		videoLinkChange :function(data) {
			$("."+data.kunik).attr("src",cmsConstructor.helpers.videoLinkProcess(data.value));
		},
		changeImage :function(data) {
			$(".this-content-"+data.kunik).attr("src", data.value);
		},
		changeIcon :function(data) {
			$('.' + data.kunik).removeClass(function (index, css) {
				return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
			});
			$('.' + data.kunik).addClass("fa fa-" + data.value)
		},
		changePositionLineSeparator: function(data) {
			$("#wrapperforLineSeparator"+data.kunik).toggleClass("bottom-lineSeparator"+data.kunik);  
            $("#wrapperforLineSeparator"+data.kunik).toggleClass("top-lineSeparator"+data.kunik); 
		},
		managePage : function(data){
			var newLine = costumizer.pages.views.linePage(data.newKey, data.formData);	
			if(notEmpty(data.keyApp)){
				$(".tabling-pages tr[id='keyPages-"+data.keyApp+"']").replaceWith(newLine);
			}else{
				$(".tabling-pages tbody").append(newLine);
			}

			costumizer.obj.app[data.newKey] = data.formData;
			urlCtrl.loadableUrls[data.newKey] = data.formData;
			costum.resetCache()
			costum.app[data.newKey] = data.formData
			costumizer.pages.events.bind();
		},
		deletePage : function(data){
			costum.resetCache()
			costumizer.pages.actions.callBackDeletePage(data.pages,data.result);
		},
		onMenuBusy: function(data) {
			costumSocket.socketListener.elementUnfocused({userId: data.userId});	
			var arrayToCheck = ["page", "template", "history"];
			if (typeof data.elementTarget != "undefined" && data.userId != userId  && !arrayToCheck.some(element => data.elementTarget.includes(element))) {
				var $actions = $(`<div class="block-busy-wrapper" data-userid="${data.userId}" style="border: 2px solid ${costum.members.lists[data.userId].color};background-color: ${costum.members.lists[data.userId].color}29"></div>`)
				$actions.prepend(`
					<div class="cmsbuilder-block-action-label" style="background-color: transparent">
						<span class="block-busy-label" style="background-color: ${costum.members.lists[data.userId].color};">
							<label class="name">${costum.members["lists"][data.userId].name}</label> - en train d'éditer
						</span>
					</div>
				`);
				var menuToCheck = ["menuLeft", "header.menuTop.left", "header.menuTop.center", "header.menuTop.right", "menuRight", "menuBottom", "subMenu", "header.menuTop"];
				if (costumizer.space == "htmlConstruct" && data.elementTarget.includes("data-path") && menuToCheck.some(element => data.elementTarget.includes(element))) {
					var path = data.elementTarget.replaceAll("[data-path='", "").replaceAll("']", "");
					var target = ".sortable-list[data-ref='htmlConstruct."+path+".buttonList']";
					var $wrapperBloc = $(`<div class="block-busy-wrapper" data-anchor-target="anchor${data.userId}" data-userid="${data.userId}" style="border: 2px solid ${costum.members.lists[data.userId].color};background-color: ${costum.members.lists[data.userId].color}29"></div>`)
					$wrapperBloc.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][data.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[data.userId].color};">${costum.members["lists"][data.userId].name} - en train d'éditer</span></div>`);
					$(target).closest(".sortable-parent-wrapper").prepend($wrapperBloc).addClass("edition-desabled").css({"pointer-events": "none"}).attr("data-userid",data.userId);
				}
				$(data.elementTarget).prepend($actions).addClass("edition-desabled").attr("data-userid", data.userId);
				if ($(data.elementTarget).hasClass("lbh-costumizer")) {
					$(data.elementTarget).css({"margin-top": "15px"})
				}
			}
		},
		updateFontsCache: function(data) {
			var fontStyle = `
				@font-face {
					font-family: "${data["nameFont"].split(' ').join('_')}";
					src: url("${data["pathFont"]}");
				}
			`;
			$("#font-head-style").prepend(fontStyle);
			fontObj[data["pathFont"]] = data["nameFont"];
			costum.updateFontsCache(data["fontObj"]);
		}
    }
}

costumSocket.init();