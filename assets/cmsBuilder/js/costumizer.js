/* Lobjet costumizer.obj est une extension de l'objet costum 
	** Il permet d'apporter les modifs js à volonté jusque l'enregistrement finalement des modifications qui vient modifier la db et le costum js
	** L'objet retient et centralise les méthodes d'édition du Costum :
			[ ] Pages (costum.app)
			[ ] htmlConstruct 
				[ ] menu 
				[ ] page Element
				[ ] Adminstration dash 
				[ ] ...
	** Le but de ces méthodes est de limiter au maximum les reload instempestif sur les modifications apportées au costum
	** Il est générique et généré dans le mainSearch.php rendu donc global à tout l'environnement d'un costum ou pourquoi pas de co2    
	!!! TRAVAIL À RÉALISER et à organiser !!!!!
	[ ] CSS
	[ ] JS
	[ ] Métier 
	==> TOUTES LES TÂCHES SONT DÉCRITES AU FIL DES CLICS ET DES METHODES DANS L'OBJECT
*/ 

var costumizer = {
	config : {},
	obj : {},
	preview : false,
	liveChange : false,
	mode : "md",
	connectedMode : true,
	allBtnMenuToDragged : {},
	space : "",
	multiSpaces : [],
	lastView : "",
	costumDataOlder : {},
	coInputParamsData : {},
	init : function(mode = "md"){
		costumizer.obj=jQuery.extend(true,{},costum);
		costumizer.costumDataOlder = jQuery.extend(true,{},costum);
		//costumizer.initView();
		costumizer.mode = mode;
		costumizer.initEvents();
		cssHelpers.form.extendCoInput();
		costumizer.actions.admins();
		costumizer.getNoteMd();
		costumizer.getCategoryUsedByTemplate();
		costumizer.getFormContext(true).then(
			function (responses) {
				if (responses.result && typeof responses.allForms != "undefined") {
					costumizer.coInputParamsData.allAapFormsContext = responses.allForms;
				}
			}
		)
		$(".image-menu").attr("src", costumizer.obj.logo);
		costumizer.allBtnMenuToDragged = jQuery.extend(true, {}, themeParams.mainMenuButtons);
		// Auto generate invitation link (invit as admin) (Needed! eg: on costum community list)
		if (typeof costum.invitationLink == "undefined") {
			costumizer.generateLinkInvitasAdmin()
		}
		var color;

		// Fonction pour initialiser la palette avec les couleurs par défaut
		function initializePalette() {
			const defaultColors = jsonHelper.getValueByPath(costumizer.obj, "css.color", {});
			 // Vérification si defaultColors n'est pas vide
			if (notNull(defaultColors)) {
				const colorArray = Object.keys(defaultColors).map(key => ({
					type: key,
					color: defaultColors[key]
				}));

				// Mettre à jour la palette avec ces couleurs
				updateColorPalette(colorArray);
			}
		}

		// Fonction pour mettre à jour dynamiquement le contenu de la palette
		function updateColorPalette(value) {
			// Vérifie si les données sont valides
			const isDataValid = Array.isArray(value) && value.every(item => item.type && item.color);
			if (!isDataValid) {
				console.warn('Invalid data format or missing fields:', value);
				return;
			}

			// Mettre à jour la palette avec les nouvelles données
			costumizer.actions.appendColorPalette(value);
			
		}

		// Appeler initializePalette pour définir la palette au chargement
		$(document).ready(function () {
			initializePalette();
		});
	},
	initEvents : function(){
		$.each(costumizer.events, function(e, v){
			v();
		});
		/*costumizer.events.startView();
		costumizer.events.close();
		costumizer.events.previewLink();
		costumizer.events.viewMode();
		costumizer.events.save();
		costumizer.events.editMenu();*/
		/*$(".design-costumizer").on("click", function(){
			titleRightCostumizer="Design";
			$("#menu-right-costumizer").find(".costumizer-title > span").text(titleRightCostumizer);
			$("#menu-right-costumizer").fadeIn(1000);
			costumizer.design.views.init();
			costumizer.design.events.bind();
		});*/

	
		//open dropdown on click
		$(".cmsbuilder-toolbar-item").click(function(e) {
			e.stopPropagation()
			var hasClass = $(this).find(".cmsbuilder-toolbar-dropdown").hasClass("dropdown-active")
			$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
			if(!hasClass)
				$(this).find(".cmsbuilder-toolbar-dropdown").addClass("dropdown-active")
		})
		//close dropdown on click outside
		$(document).click(function(){
			$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
		})
		$(".menu-item-version-and-info").off().on("click", function() {
			var idModal = $(this).data("value");
			if(idModal == "#observatory" ){
				coInterface.showLoader(".tab_container_observatory");
				obsCostum.getInfo();
			}
			$(idModal).modal("show")
		})
		$(".menu-item-help-and-feedback").off().on("click", function() {
			var value = $(this).data("value");
			$("#openModalHelpAndFeedback .tab-item, #openModalHelpAndFeedback .tab-pane").removeClass("active");
			$("#openModalHelpAndFeedback").modal("show");
			$("#openModalHelpAndFeedback a[href='#"+value+"'].tab-item").addClass("active");
            $("#openModalHelpAndFeedback #"+value+"").addClass("in active");
			$("#openModalHelpAndFeedback #question_form, #openModalHelpAndFeedback #feedback_form").trigger('reset');
		})
		$(".user-state").on("click", ".go-to-user",function(){
			var $this = $(this);

			if (typeof costum.members.focused[$this.data("userid")] != "undefined" && typeof costum.members.focused[$this.data("userid")].elementTarget != "undefined") {
				const focusedByThisAdmin = costum.members.focused[$this.data("userid")]
				// Navigate to menu costumizer
				if (focusedByThisAdmin.elementTarget.includes("lbh-costumizer")) {
					if(!focusedByThisAdmin.elementTarget.includes("page") && !focusedByThisAdmin.elementTarget.includes("template")){
						setTimeout(function(){
							$(focusedByThisAdmin.elementTarget).parents(".cmsbuilder-toolbar-dropdown").addClass("dropdown-active")
						},30)
					}else{
						toastr.info(tradCms.usernotediting)
					}

				// Navigate to page editon 
				}else if (focusedByThisAdmin.elementTarget.includes("keyPages")){
					$(".lbh-costumizer[data-space='pages']").click()

				// Navigate to menu top
				}else if (focusedByThisAdmin.elementTarget.includes("header")) {
					if ($(focusedByThisAdmin.elementTarget).length) {
						$('html, body,.cmsbuilder-center-content').animate({ scrollTop: 0 }, 'slow');
					}

				// Navigate to block 
				}else{
					mylog.log('go-to-edition',page, focusedByThisAdmin, ".block-busy-wrapper[data-userid='"+$this.data("userid")+"']")
					if (page == focusedByThisAdmin.page && $("#all-block-container").length != 0) {
						if ($(".block-busy-wrapper[data-userid='"+$this.data("userid")+"']").length != 0 ) {							
							costumizer.scrollToTarget(".block-busy-wrapper[data-userid='"+$this.data("userid")+"']");
						}else{							
							urlCtrl.loadByHash("#"+page)
						}
					}else{
						costumizer["isWaitForLoad"] = $this.data("userid")
						urlCtrl.loadByHash("#"+focusedByThisAdmin.page)
					}
				}
			} else {
				toastr.info(tradCms.usernotediting)
			}
		})
		$(".user-state").on("click", ".link-chat",function(){
			var $this = $(this);
			rcObj.loadChat(($this.data("name")).toLowerCase() ,"citoyens" ,false ,false, {id: $this.data("id") , name: ($this.data("name")).toLowerCase() , slug: $this.data("slug"), type: "citoyens" , username: $this.data("username")} );
		})
		$(".user-state").on("click", ".btn-contribution",function(){
			var $this = $(this);
			var hidden = $('#action-count-'+$this.data("slug"));
			if (hidden.hasClass('visible')){
				hidden.animate({"left":"-1000px"}, "slow").removeClass('visible');
				$this.html("Voir la contribution");
			} else {
				hidden.animate({"left":"0px"}, "slow").addClass('visible');
				$this.html("Masquer la contribution");
			}
		});
		$(".user-state").on("click", ".ssmla",function(){
			var hasRc = (typeof costum.hasRC != "undefined" || costum.contextType == "citoyens") ? true : false;
			if (costum.contextType == "citoyens") {
				rcObj.loadChat(costum.contextSlug, costum.contextType, (params && params.element && params.element.preferences && params.element.preferences.private && params.element.preferences.private === true) ? false : true, hasRc, costum);
			} else {
				var subChat = (costum && costum.rocketchatMultiEnabled && costum.tools && costum.tools.chat && costum.tools.chat.int && costum.tools.chat.int.length > 0 || costum && costum.tools && costum.tools.chat && costum.tools.chat.ext && costum.tools.chat.ext.length > 0) ? true : false;
				if (subChat) {
					mylog.log("pageProfil.views.chatManager");
					getAjax('', baseUrl + '/' + moduleId + '/settings/chatmanager/type/' + costum.contextType + '/id/' + costum.id, function(html) {
						$("#central-container").html("<div class='col-xs-12 bg-white'>" + html + "</div>");
					}, "html");
				} else {
					rcObj.loadChat(costum.contextSlug, costum.contextType, (params && params.element && params.element.preferences && params.element.preferences.private && params.element.preferences.private === true) ? false : true, hasRc, costum);
				}
			}

		});

		$(".cmsbuilder-body").on("mouseup",".lbh-menu-app", function(e){	
			if(typeof costumizer.reloadApp != "undefined") {
				e.preventDefault();
				var h = "";
				if($(this).data("hash"))
					h = $(this).data("hash")
				else{
					try{ h = new URL($(this).attr("href")).hash }
					catch{ h = $(this).attr("href") }
				}		
				if (typeof costum.app[h] != "undefined" && costum.app[h].hash != "#app.view") {	
					window.location.href = window.current_full_Url.split('#')[0]+h	
					window.location.reload()
				}
			}
		});

		document.addEventListener("visibilitychange", () => {
			if (document.hidden) {
				var params = {functionName: "elementUnfocused", blockId : {}, userId : userId}
				costumSocket.emitEvent(wsCO, "edit_component", params);
			}
		});
	},

	initView : function(config){
		var domTarget=config.dom;
		$(domTarget).fadeIn(500);
		$(domTarget).find(".costumizer-title > span").text(config.title);
			
		if(typeof costumizer[costumizer.space] != "undefined"
			&& typeof costumizer[costumizer.space].views != "undefined"
			&& typeof costumizer[costumizer.space].views.init === "function"){
			$(domTarget+" > .contains-view-admin").html(costumizer[costumizer.space].views.init(config));
			if (costumizer.space == "fonts")
				$(domTarget+" > .contains-view-admin").addClass("contains-views-for-font-manager");
			else 
				$(domTarget+" > .contains-view-admin").removeClass("contains-views-for-font-manager");
		}
		else if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].views != "undefined"
				&& typeof costumizer[costumizer.space].views.initAjax === "function"){
				costumizer[costumizer.space].views.initAjax(config);
		}
		else{
			ajaxPost(domTarget+" .contains-view-admin", baseUrl+"/"+moduleId+"/cms/admindashboard", 
				{ 
					type: costum.contextType,
					id: costum.contextId,
					view:costumizer.space
				},
				function(){
					if(typeof costumizer[costumizer.space] != "undefined"
						&& typeof costumizer[costumizer.space].events != "undefined"
						&& typeof costumizer[costumizer.space].events.bind === "function")
						costumizer[costumizer.space].events.bind();
				}
			);
		}
		//Bind events for space costumizer opened 
		if(typeof costumizer[costumizer.space] != "undefined"
			&& typeof costumizer[costumizer.space].events != "undefined"
			&& typeof costumizer[costumizer.space].events.bind === "function")
			costumizer[costumizer.space].events.bind(config);
		//Function initCallback for workspace costumizer opened
		if(typeof costumizer[costumizer.space] != "undefined"
			&& typeof costumizer[costumizer.space].actions != "undefined"
			&& typeof costumizer[costumizer.space].actions.init === "function")
			costumizer[costumizer.space].actions.init(config);
	},
	generateUUID : function() {
		return Math.random().toString(36).slice(-6)
	},
	updateComponentSocket : function(params) {
		costumSocket.emitEvent(wsCO, "update_component", params)
	},
	focusComponentSocket : function(functionName, componentId) {
		var params = {functionName: functionName, blockId : {elementTarget: componentId, page: page}, userId : userId}
		costumSocket.emitEvent(wsCO, "edit_component", params)
	},
	tplChange : function(params) {
		mylog.log("tplChange", params)
		params.data["userId"] = userId;
		costumSocket.emitEvent(wsCO, "tpl_change", params)
	},
	scrollToTarget: function(target) {
		var offsetTop = $(target).offset().top;
		$('html, body,.cmsbuilder-center-content').animate({
			scrollTop: offsetTop
		}, 1000); 
	},
	generateLinkInvitasAdmin() {
		ajaxPost(
			null,
			baseUrl+"/co2/link/createinvitationlink", {
				targetType: costum.contextType,
				targetId: costum.contextId,
				roles: "",
				isAdmin: true,
				urlRedirection: ""
			},
			function(response) {
				var saveInvitationLink = {
					id : costum.contextId,
					collection : costum.contextType,
					path : "costum.invitationLink",
					value : response.result.link
				}
				dataHelper.path2Value(saveInvitationLink, function () {
					costum.resetCache()					
					costum["invitationLink"] = response.result.link
				})

			}
			)
	},
	getFormContext(isAapOnly = false) {
		return new Promise(function (resolve, reject) {
			const post = {
				contextId: costum.contextId,
				aapFormOnly: isAapOnly
			};
			var url = '/co2/cms/getallcontextform';
			ajaxPost(null, url, post, resolve, function (arg0, arg1, arg2) {
				reject({
					code: 400,
					msg: [arg0, arg1, arg2]
				})
			});
		});
	},
	getNoteMd() {
		if (typeof costum.availableNote == "undefined") {
			var dataCostum = {
				contextId   : costum.contextId,
				contextType : costum.contextType
			}

			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/cms/notemarkdown",
				{
					dataCostum
				},
				function(data){
					costum.availableNote = data.data
				},
				null

				);
		}
	},
	getCategoryUsedByTemplate(callback) {
		if (typeof costum.availableNote == "undefined") {
			var dataCostum = {
				contextId   : costum.contextId,
				contextType : costum.contextType
			}

			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/cms/categoryusedbytplcms",
				{
					dataCostum
				},
				function(data){
					costumizer.template.categoryused = {};
					$.each(data, function(type, tplByType) {
						if (!costumizer.template.categoryused[type]) {
							costumizer.template.categoryused[type] = {};
						}
                        $.each(tplByType, function(keyC, tplByCategory) {
    						if (!costumizer.template.categoryused[type][tplByCategory.category]) {
    							if (typeof tplByCategory.category == "string") {
    								var category = (typeof tradCms[tplByCategory.category] == "undefined" ? (typeof trad[tplByCategory.category] == "undefined" ? tplByCategory.category : trad[tplByCategory.category] ) : tradCms[tplByCategory.category] )
    								
    								tplByCategory.category = tplByCategory.category.replace("&","&amp;")
    								costumizer.template.categoryused[type][tplByCategory.category] = category
    								if (!isSuperAdminCms && typeof tplByCategory.shared == "undefined") {
    									delete costumizer.template.categoryused[type][tplByCategory.category]
    								}
    							}else{
    								$.each(tplByType, function(keyC, tplByCategory) {
    									if (!costumizer.template.categoryused[type][tplByCategory.category]) {
    										if (typeof tplByCategory.category == "string") {
    											var category = (typeof tradCms[tplByCategory.category] == "undefined" ? (typeof trad[tplByCategory.category] == "undefined" ? tplByCategory.category : trad[tplByCategory.category] ) : tradCms[tplByCategory.category] )
    											
    											tplByCategory.category = tplByCategory.category.replace("&","&amp;")
    											costumizer.template.categoryused[type][tplByCategory.category] = category
    											if (!isSuperAdminCms && typeof tplByCategory.shared == "undefined") {
    												delete costumizer.template.categoryused[type][tplByCategory.category]
    											}
    										}
    									}
    								});
    							}
    						}
    					});
                        if (callback && typeof callback === 'function') {
                        	callback();
                        }
					});
				},
				null

				);
		}
	},
	promptToReload() {
		bootbox.confirm({
			message: `Veuillez recharger pour terminer !`,
			title: 'Chargement incomplet!',
			buttons: {
				confirm: {
					label: "Recharger",
					className: 'btn-success'
				},
				cancel: {
					label: trad.cancel,
					className: 'btn-default hidden'
				}
			},
			callback: function (response) {
				if (response) {
					window.location.reload()
				}
			}
		});
	},
	forceReload (page=null) {
		if (localStorage.getItem("reload"+costum.contextId) != null) {
			if (notNull(page)) {
					urlCtrl.loadByHash(page)
			}else{
				urlCtrl.loadByHash(localStorage.getItem("reload"+costum.contextId))
			}
			if (page == localStorage.getItem("reload"+costum.contextId)) {	
				localStorage.removeItem("reload"+costum.contextId)	
				window.location.reload()
			}
		}
	},
	checkCostumAlias() {
		if (typeof costum.app["#"+page] != "undefined") {
			if (typeof costum.app["#"+page].linked != "undefined") {
				costum.isPageAlias = true            
				cmsConstructor.helpers.removeEditableSpText();
				var aliasPreviewMode = setInterval(function(){
					if(typeof convAndCheckLink == 'function'){
						clearInterval(aliasPreviewMode)                
						convAndCheckLink(".sp-text",true)
					}   
				}, 100)
				$(".cmsbuilder-left-content,.empty-section-blocks").addClass("hidden")

				if (typeof Object.keys(cmsConstructor.sp_params).find(id => (cmsConstructor.cmsList).includes(id)) != "undefined") {
					const parentObject = cmsConstructor.sp_params[Object.keys(cmsConstructor.sp_params).find(id => (cmsConstructor.cmsList).includes(id))].parent;
					const parentId = Object.keys(parentObject)[0];
					if(parentId == costum.contextId){                          
						costumizer.promptToReload()
					}
				}else{
					costumizer.promptToReload()
				}

				$(".cmsbuilder-block").removeClass("cmsbuilder-block").removeClass("cmsbuilder-block-droppable").removeClass("cmsbuilder-block-container")

			}else{  
				$(".cmsbuilder-left-content,.empty-section-blocks").removeClass("hidden")
				costum.isPageAlias = false
			}            
		}
		costumizer.pages.views.updataPageInfo()
	},
	aliasCostumList() {
		smallMenu.open(`
			<h1 class="bg-green" style="border-radius: 50px;padding: 10px;"> Choisir le COSTUM à afficher </h1>
			<div class='contains-view-alias-head'></div>
			<div class='costum-alias-container'>
				<div class='costum-alias-list'></div>
			</div>`
		)
		$('.portfolio-modal').has('.costum-alias-list').addClass('has-costum-alias-list');
		$('.costum-alias-container').css("height", ($(document).height()-368)+"px")
		paramsFilter = {
				urlData: baseUrl + "/co2/search/globalautocomplete",
				container: ".contains-view-alias-head",
				loadEvent : {
					default : "scroll"
				},
				scroll : {
					options : {
						domTarget:".costum-alias-container",
						scrollTarget: ".costum-alias-container"
					}
				},
				defaults: {
					notSourceKey: true,
                        types : ["NGO","LocalBusiness","Group","GovernmentOrganization","Cooperative","projects","events"],
                        filters : {
                            $or : {
                                $and : [
                                    {"costum.slug" :{'$exists':true}}
                                ]
                            },
            
                        },
					fields : ["costum"]

				},
				results: {
					dom: ".costum-alias-list",
					smartGrid: true,
					
					renderView : "directory.elementPanelHtmlSmallCard",
					map: {
						active: false
					}
				},
				filters: {
					text: true
				}
			};

			var filterGroupTemplate = searchObj.init(paramsFilter);
			filterGroupTemplate.search.init(filterGroupTemplate);
			directory.elementPanelHtmlSmallCard = function(params){
               mylog.log("elementPanelHtmlSmallCard", "Params", params);
				str = `
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 searchEntityContainer ${params.containerClass}" data-href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}">
					<div class="small-card item-slide">
						<div class="icon-elm bg-${params.color}">
							<span><i class="fa fa-${params.icon} "></i></span>
						</div>
						<div class="block-card-img">
							<a href="${params.hash }" class=" ${params.hashClass}">
								<div class="card-img-container " >
									${params.imageProfilHtml}					
								</div>
							</a>
							<div class="block-card-body">						
								<h4 class="entityName"><a href="${params.hash }" class=" ${params.hashClass}">${params.name}</a></h4>`					
							str+= `</div>`
						str+= `</div>`
						str+= ` <div class="block-card-hover co-scroll">
									<div class="text-wrap sp-cms-std" style="justify-content: center;">
										<h4 class="entityName"> <a>${params.name}</a> </h4>`
										var pageOptions = ""
										str+= directory.typeConfigAppend (params.type);
										if (notEmpty(params.statusLinkHtml))
											str += params.statusLinkHtml;

										str+= directory.roleConfigAppend (params.rolesHtml);

										if (typeof params.edit != 'undefined' && notNull(params.edit))
											str += directory.getAdminToolBar(params);
										if(params.id == userId && typeof params.rolesAndStatusLink != "undefined" && typeof params.rolesAndStatusLink["isInviting"] != "undefined" && typeof params.rolesAndStatusLink["invitorId"] != "undefined" && params.rolesAndStatusLink["invitorId"] != userId){
											str += directory.getUserInvitToolBar(params);
										}
										str+= directory.localityConfigAppend (params.localityHtml);
										str += directory.countLinksHtml(params);
										str += "<hr>"
										if (typeof params.costum.app != "undefined" && Object.keys(params.costum.app).length != 0) {
											pageOptions = '<option value="" disabled selected>Selectionner la page</option>'
											$.each(params.costum.app, function(key, value) {
												if (typeof params.costum.app[key].restricted == "undefined" && params.costum.app[key].hash == "#app.view") {													
													pageOptions += `<option value='${JSON.stringify({costumId: params.id, slug: params.slug, keypage: key} || [])}'>${key}</option>`
												}
											});
											if (pageOptions != "") {	
												str += `<select class="select-costum-alias form-control" style="width:80%" data-id="${params.id}"  data-page="${params.id}" >${pageOptions}</select>`
											}	
										}
										if (pageOptions == "") {
											str += `<p class="text-white" style="border-top: 1px solid;"> Aucun page CMS </p>`	
										}
						str+= `</div>` 
				str += `</div>`  
			str += `</div> 
				</div> `

                return str;
            };

            $(".cmsbuilder-center-content").on("change",".select-costum-alias", function(e){
            	 var aliasChoosed = JSON.parse($(this).val())
            	 mylog.log(aliasChoosed)
            	$(".costum-alias[name='costumId'").val(aliasChoosed.costumId)
            	$(".costum-alias[name='costumPage'").val(aliasChoosed.keypage.replace("#",""))
            	$(".costum-alias[name='costumSlug'").val(aliasChoosed.slug)
            	$(".close-modal").click()
            })
            $("#openModal").on("click",".close-modal", function(e){
            	if ($(this).parents("#openModal").hasClass("has-costum-alias-list") && $(".costum-alias[name='costumPage'").val() == "" && $(".costum-alias[name='costumSlug'").val() == "") {
            		$('.enable-alias button[data-value="false"]').click()
            	}
            	//  mylog.log(aliasChoosed)
            	// $(".costum-alias[name='costumId'").val(aliasChoosed.costumId)
            	// $(".costum-alias[name='costumPage'").val(aliasChoosed.keypage.replace("#",""))
            	// $(".costum-alias[name='costumSlug'").val(aliasChoosed.slug)
            	// $(".close-modal").click()
            })
	},
	checkKeyOpenAI(callBack) {
		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/cms/checkkeyopenai",
			null,
			function(data){
				if (data.result)
					callBack(null, data.apiKey);
				else {
					callBack("error", null); 
					toastr.error(tradCms.somethingWrong);
				}
			},
			"json"
		);
	},
	events : {		
		startView : function(){
			$(".lbh-costumizer").off().on("click", function(e){
				e.stopPropagation()
				if (!$(this).hasClass("edition-desabled")){
					var space = $(this).data("space");
					$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")

					if ($(this).data("link") == "css"){
						if ($("#amazing-costumizer-css").length > 0 ){
							$("#amazing-costumizer-css").attr("type", "text");
						} else if ($("#amazing-costumizer-css-new").length > 0){
							$("#amazing-costumizer-css-new").attr("type", "text");
						}
					}else{
						if ($("#amazing-costumizer-css").length > 0 ){
							$("#amazing-costumizer-css").attr("type", "text/css");
						} else if ($("#amazing-costumizer-css-new").length > 0){
							$("#amazing-costumizer-css-new").attr("type", "text/css");
						}	
					}

								// Concatène toutes les valeurs data du bouton
								//data-dom, data-link pour l'espace, data-
					var params = {};
					$.each(Array.from($(this).get(0).attributes), function(e, v){
						if(v.nodeName.indexOf("data") >= 0)
							params[v.nodeName.replace("data-", "")]=v.nodeValue;
					});

				/*				var	params = attrArray.reduce((attrs, attr) => {
										if(attr.nodeName.indexOf("data") >= 0){
										    attrs !== '' && (attrs += ' ')
										    attrs += `${attr.nodeName}="${attr.nodeValue}"`
									    }
									    alert(attrs);
									    return attrs
				
									}, '');*/

					$("#right-panel").html("")
					$(".cmsbuilder-right-content").removeClass("active");	
					costumizer.focusComponentSocket("elementFocused", ".lbh-costumizer[data-space='"+space+"']");	
								// costumizer.focusComponentSocket({functionName: "onMenuBusy", data:{
								// 	elementTarget: ".lbh-costumizer[data-space='"+space+"']",
								// 	userId: userId
								// }})                          
								// costum.members.focused[userId] = {page: page, elementTarget: ".lbh-costumizer[data-space='"+space+"']"};
					costumizer.actions.openCostumizer(params)
				}else{
					cmsConstructor.helpers.appendBusy($(this).data('userid'),true);
				}
				$("#observatory").modal("hide")
			});
		},
		close : function(){
			$(".close-dash-costumizer").off().on("click", function(){
				costumizer.actions.cancel($(this).data("dom"));
				// costumizer.postDataSocket("elementUnfocused", {});
				var params = {functionName: "elementUnfocused", blockId : {}, userId : userId}
				costumSocket.emitEvent(wsCO, "edit_component", params)
				//$($(this).data("dom")).fadeOut(1000);
				$("#content-btn-delete-page").remove();
			});
		},
		save : function(){
			$(".save-costumizer").off().on("click", function(){
				$(this).find("i").removeClass("fa-save").addClass("fa-spin fa-spinner");
				$(this).addClass("disabled");
				if ($("#costum-design #collapsefont").hasClass('collapse in')) {
					if (costumizer.obj.css.font) {
						if (costumizer.obj.css.font.url == "" && $('#inputs-font input[name="uploadFont"]').val() != "") {
							costumizer.design.events.getDataFont();
							if (!costumizer.design.fontUploadExist) {
								costumizer.design.events.addFont();
								uploadedFont = "/upload/communecter/"+costumizer.obj.contextType+"/"+costumizer.obj.contextId+"/file/"+$('#inputs-font input[name="uploadFont"]').val();
								costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");	
							}
						}
					}
				}
				if(costumizer.space == "generalInfos" && $("#costum-use-open-ai .isUseOpenAI button[data-value='true']").hasClass("btn-active") && costumizer.generalInfos.isOpenAIKeyEntered == false) {
					$(this).find("i").removeClass("fa-spin fa-spinner").addClass("fa-save");
					toastr.error("Veuillez ajouter une clé API pour pouvoir utiliser l'API d'OpenAI");
					return false;
				}
				costumizer.actions.update();
				if ($(this).closest("#modal-content-costumizer-administration").css("display") == "block" && costumizer.generalInfos.refreshdropdownlanguage) {
					$(".dropdown-languages-content .dropdown-languages-select-form").html("");
					var allLanguagesInDb = dataHelper.getAllLanguageInDb();
					var costumLangActive = costum.langCostumActive;
					if (allLanguagesInDb.includes(costumLangActive)) {
						var allLanguages = {}, dropdownView = "";
						for (var i = 0; i < allLanguagesInDb.length; i++) {
							var keyLang = allLanguagesInDb[i].toUpperCase();
							if (themeParams.DeeplLanguages.hasOwnProperty(keyLang)) {
								allLanguages[keyLang] = themeParams.DeeplLanguages[keyLang];
							}
						}
						$.each(allLanguages, function(key, value) {
							dropdownView += 
							`<li class="btn-language-flag${(key.toLowerCase() === costumLangActive) ? ' active' : ''}">
								<a href="javascript:;" onclick="coInterface.setLanguage('${key.toLowerCase()}','${true}')">
									<span style="font-size: 20px; margin-right: 10px">${value["icon"]}</span> ${tradCms[value["label"]]}                               
								</a>
							</li>`;
						})
						$(".dropdown-languages-content .dropdown-languages-select-form").html(dropdownView);
						costumizer.generalInfos.refreshdropdownlanguage  = false;
					} else {
						var languagesInCookie = $.cookie("costumlanguages");
						var costumLanguages = (typeof languagesInCookie != "undefined") ? JSON.parse(languagesInCookie) : {};
						jsonHelper.deleteByPath(costumLanguages, costum.contextSlug);
						if (notEmpty(costumLanguages))
							$.cookie("costumlanguages", JSON.stringify(costumLanguages), { expires: 365, path: "/"});	
						else
							$.removeCookie("costumlanguages", { path: "/" });

						window.location.reload();
					}
				}
				$("#menu-right-costumizer, #modal-content-costumizer-administration").fadeOut(1000);
				costumizer.forceReload()
			});

			$(".save-costum-version").off().on("click", function() {
				costumizer.actions.saveCostum()
			});
		},
		previewLink : function(){
			$(".open-preview-costum").off().on("click", function(){
				window.open($(this).data("url")+location.hash);
			});
		},
		viewSwitcher : function (mode = "md") {		
			// $(".cmsbuilder-right-content").find(".all-views-mode").val(mode);
			$(".btn-costumizer-viewMode").removeClass("active");
			$(".btn-costumizer-viewMode[data-mode='"+mode+"']").addClass("active");
			$(".cmsbuilder-center-content").scrollTop(0);
			costumizer.responsive[mode]();
			// Remove useless config in !md
			// if (costumizer.mode != "md") {
			// 	$(".cmsbuilder-tabs-header").find("[data-key='layers']").click()
			// }
			
			// $(`.cmsbuilder-block[data-kunik="${cmsConstructor.kunik}"]`).click();
			$(".cmsbuilder-right-content").removeClass("active")
			$("#right-panel").html("")
			$(".block-actions-wrapper").removeClass("selected")
		},
		viewMode : function(){
			$(".costumizer-edit-mode-connected").off().on("click", function(){
				//.slideToggle();
				$(".costumizer-edit-mode-connected").find("i.fa-eye").parent().remove();
				$(this).append('<span><i class="fa fa-eye" aria-hidden="true"></i></span>');
				costumizer.connectedMode=($(this).data("value")===true) ? true : false;
				var addClassIcon=($(this).data("value")===true) ? "fa-sign-in" : "fa-sign-out";
				$(".activeModeConnected").find("i.statusConnected").slideToggle( "fast", function() {
				    $(this).removeClass("fa-sign-in fa-sign-out").addClass(addClassIcon).slideToggle();
				  });
				//$(".activeModeConnected").find("i.statusConnected").removeClass("fa-sign-in fa-sign-out").addClass(addClassIcon);
			//	$(".activeModeConnected").find("i.statusConnected").slideToggle();
				costumizer.actions.switchMode();
			});
			
			$(".btn-costumizer-viewMode").off().on("click", function(){
				// costumizer.responsive[$(this).data("mode")]();
				costumizer.events.viewSwitcher($(this).data("mode"))
			});

			$
			$(".cmsbuilder-right-content").on("change",".all-views-mode", function () {		
				costumizer.events.viewSwitcher($(this).val())
			})
		},
		editMenu : function(){
			$(".configuration-menus-costumizer").off().hover(function(e){
				$(this).addClass("blur");
				if($(this).find(".editMenuStandalone").length > 0)
					$(this).find(".editMenuStandalone").show();
				else {
					$(this).append("<button class='editMenuStandalone badge circle'><i class='fa fa-pencil'></i></button>");
					$(".configuration-menus-costumizer .editMenuStandalone").off().on("click", function(e){
						$(".configuration-menus-costumizer").removeClass("focused");
						$(".block-actions-wrapper").remove();
						$(this).addClass("focus");
						$(this).parent(".configuration-menus-costumizer").addClass("focused");
						e.preventDefault();
						var viewMenuConstruct=costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+$(this).parent().data("path"));		

						costumizer.focusComponentSocket("elementFocused", "[data-path='"+$(this).parent().data("path")+"']");

						costumizer.space="htmlConstruct";
						var path = $(this).parent().data("path");

						parent.$(".cmsbuilder-right-content").addClass("active")
						costumizer.actions.tabs.init(
							"#right-panel",
							[
								{
									key: "htmlConstruct",
									icon: "list-ul",
									label: tradCms.content,
									containerClassname:"contains-view-admin"
								},
								{
									key: "design",
									icon: "paint-brush",
									label: tradCms.style,
									containerClassname:"contains-view-admin"
								}
							],
							{
								footer:`
									<button class="btn btn-sm btn-danger save-costumizer disabled"><i class="fa fa-floppy-o" aria-hidden="true"></i>${trad.save}</button>
								`,
								getContent: function(tabKey){
									if( $.inArray(costumizer.space, costumizer.multiSpaces) < 0)
										costumizer.multiSpaces.push(costumizer.space);

									if($.inArray(tabKey,costumizer.multiSpaces) < 0)
										costumizer.multiSpaces.push(tabKey);

									costumizer.space = tabKey;

									if(tabKey == "htmlConstruct")
										return costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+path);
									else{
										costumizer.design.activeObjCss = costumizer.design.events.generateObj("htmlConstruct."+path);
										return costumizer.design.views.css()
									}
								},
								onTabChanged:function(tabKey){
									costumizer.space=tabKey;
									if(typeof costumizer[costumizer.space] != "undefined"
										&& typeof costumizer[costumizer.space].events != "undefined"
										&& typeof costumizer[costumizer.space].events.bind === "function")
										costumizer[costumizer.space].events.bind();
									if(typeof costumizer[costumizer.space] != "undefined"
										&& typeof costumizer[costumizer.space].actions != "undefined"
										&& typeof costumizer[costumizer.space].actions.init === "function")
										costumizer[costumizer.space].actions.init();
								},
								onInitialized:function(){
									costumizer.events.save()
								}
							}
						)

						costumizer.liveChange=true;
							
					});
				}
			}, function(){
				$(this).removeClass("blur");
				$(this).find(".editMenuStandalone").hide();
			});
		},
		bindLivePanel : function(){
			$(".btn-live-menu-edit").off().on("click", function(){
				$("#menu-right-costumizer-hover, #menu-sub-right-costumizer").fadeOut(500);
				if($.inArray(costumizer.space, costumizer.multiSpaces) < 0)
					costumizer.multiSpaces.push(costumizer.space);
				if($.inArray($(this).data("object"),costumizer.multiSpaces) < 0)
					costumizer.multiSpaces.push($(this).data("object"));
				costumizer.space=$(this).data("object");
				$('.btn-live-menu-edit').removeClass("active");
				$(this).addClass("active");
				//alert("path:"+cssMenuConstruct);
				if(costumizer.space=="htmlConstruct")
					var viewMenuConstruct=costumizer.htmlConstruct.views.menuConstruct("htmlConstruct."+$(this).parent().data("path"));
				else {
					costumizer.design.activeObjCss=costumizer.design.events.generateObj("htmlConstruct."+$(this).parent().data("path"));
					var viewMenuConstruct=costumizer.design.views.css();

				}
				$("#menu-right-costumizer > .contains-view-admin").html(viewMenuConstruct);
				costumizer.space=$(this).data("object");
				if(typeof costumizer[costumizer.space] != "undefined"
					&& typeof costumizer[costumizer.space].events != "undefined"
					&& typeof costumizer[costumizer.space].events.bind === "function")
					costumizer[costumizer.space].events.bind();
				if(typeof costumizer[costumizer.space] != "undefined"
					&& typeof costumizer[costumizer.space].actions != "undefined"
					&& typeof costumizer[costumizer.space].actions.init === "function")
					costumizer[costumizer.space].actions.init();
				
					
			});
		},
		uiEvents:function(){
			$(".btn-toggle-cmsbuilder-sidepanel").click(function() {
				costumizer.actions.toogleSidePanel($(this).data("target"))
				$(".cmsbuilder-tabs-header ul li[data-key='blocks']").trigger( "click" );
				cmsConstructor.page.init();
            })
			$(".btn-toggle-cmsbuilder-sidepanel-page").click(function() {
				costumizer.actions.toogleSidePanel($(this).data("target"))
				$(".cmsbuilder-tabs-header ul li[data-key='page']").trigger( "click" );
				cmsConstructor.page.init();
			})
		}
	},
	actions : {
		pathToValue : [],
		//objDiff : [],
		switchMode : function(){
			var refreshCostumMenu={};
			$.each(["htmlConstruct.header.menuTop","htmlConstruct.menuLeft", "htmlConstruct.subMenu","htmlConstruct.menuRight","htmlConstruct.menuBottom"], function(e,v){
				if(jsonHelper.notNull("costumizer.obj."+v))
					refreshCostumMenu[v]=true;
			});
			costumizer.htmlConstruct.actions.refreshmenu(refreshCostumMenu, costumizer.obj.htmlConstruct);
		},
		getObjectDifference : function (obj1, obj2) {
			let diff = {};

			$.each(obj1, function(key, value) {
				if ($.isPlainObject(value) && $.isPlainObject(obj2[key])) {
					let nestedDiff = costumizer.actions.getObjectDifference(value, obj2[key]);
					if (!$.isEmptyObject(nestedDiff)) {
						diff[key] = nestedDiff;
					}
				} else if (value !== obj2[key]) {
					diff[key] = obj2[key];
				}
			});

			$.each(obj2, function(key, value) {
				if (!(key in obj1)) {
					diff[key] = value;
				}
			});
			$.each(diff, function(key, value) {
				if (value === undefined) {
					diff[key] = 'deleted';
				}
			});


			return diff;
		},
		removeIncludedStrings(arr) {
			return arr.filter(function(currentString) {
				return !arr.some(function(otherString) {
				  return currentString !== otherString && currentString.includes(otherString);
				});
			});
		},
		change : function(path) {
			// Cette fonction permet d'itérer qu'elle attribut au final nous devront mettre à jour en base de donnée sur le costum
			if($(".save-costumizer").hasClass("disabled")){
				$(".save-costumizer").removeClass("disabled");
			}
			if (typeof path !== "undefined") {
				let keyByPath=path.split(".");
				let lenghtPath=keyByPath.length;
				let finalPath="";
				let count=0;
				$.each(keyByPath, function(e,v){
					if(count==0){
						if($.inArray(v, costumizer.actions.pathToValue) >= 0)
							return false;
						else
							finalPath=v;
					} else
					    finalPath+="."+v;
				    if($.inArray(finalPath, costumizer.actions.pathToValue) >= 0){
					    return false;
				    }
				    count++;
			    });
			    if(notEmpty(finalPath) ){
			    	if ($.inArray(finalPath, costumizer.actions.pathToValue) < 0)
				    	costumizer.actions.pathToValue.push(finalPath);
					//if ($.inArray(finalPath, costumizer.actions.objDiff))
						//costumizer.actions.objDiff.push(costumizer.actions.getObjectDifference(jsonHelper.getValueByPath(costum,finalPath), jsonHelper.getValueByPath(costumizer.obj,finalPath)));
			    }
				// Filtrage pour conserver les éléments les plus courts		
				costumizer.actions.pathToValue = costumizer.actions.removeIncludedStrings(costumizer.actions.pathToValue);		
            }
			costumizer.updateComponentSocket({functionName: "updateObjAfterChange", data: {path: path, value: jsonHelper.getValueByPath(costumizer.obj,path)}});
		},
		commonSpaceFunction: function(actionName, data){
			if(notEmpty(costumizer.multiSpaces)){
				$.each(costumizer.multiSpaces, function(e, space){
					if(typeof costumizer[space].actions != "undefined"
						&& typeof costumizer[space].actions[actionName] === "function")
						data=costumizer[space].actions[actionName](data);
				});
			}else if(notEmpty(costumizer.space) && typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions[actionName] === "function")
				data=costumizer[costumizer.space].actions[actionName](data);
			
			return data;
		},
		getCommonValuesFromArray: function(arr1, arr2){
			return arr1.filter(value => arr2.includes(value));
		},
		prepData : function(){
			var objToUp={};
			$.each(costumizer.actions.pathToValue, function(e, v){
				let valueToUp=jsonHelper.getValueByPath(costumizer.obj,v);
				objToUp[v]=(typeof valueToUp != "undefined" && notEmpty(valueToUp)) ? valueToUp : "$unset";
				jsonHelper.setValueByPath(costum, v, valueToUp);
			});
			return objToUp=costumizer.actions.commonSpaceFunction("prepData", objToUp);
		},
		changeAndSave : function(path,value){
			jsonHelper.setValueByPath(costumizer.obj, path, value);
            costumizer.actions.change(path);
            costumizer.actions.update(false);
            costumizer.actions.pathToValue=[];
		},
		getOlderData : function(){
			var objOlder={};
			var newObjData = {};
			$.each(costumizer.actions.pathToValue, function(e, v){
				let valueObj=jsonHelper.getValueByPath(costumizer.costumDataOlder,v);
				objOlder[v]=(typeof valueObj != "undefined" && notEmpty(valueObj)) ? valueObj : "$unset";
				newObjData = jsonHelper.getValueByPath(costumizer.obj,v);
				jsonHelper.setValueByPath(costumizer.costumDataOlder, v, newObjData);
			});
			
			return objOlder;
		},
		update : function(success=true,actionFor="update"){
				var olderData = costumizer.actions.getOlderData();
				var pathValueToUpdate=costumizer.actions.prepData();
				costumizer.updateComponentSocket({functionName: "updateCostum", data: pathValueToUpdate});
				if(!$(".save-costumizer").hasClass("disabled")){
					$(".save-costumizer").addClass("disabled");
				}
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/updatecostum",
					{
						params : pathValueToUpdate,
						olderData : olderData,
						costumId    : costum.contextId,
						costumType  : costum.contextType,
						pathChanged : costumizer.actions.pathToValue

					},
					function(data){
						costumizer.actions.commonSpaceFunction("afterUpdate");
						if(success){
							toastr.success(tradCms.updateConfig);
							$("#right-panel").html("");
							$(".cmsbuilder-right-content").removeClass("active");
							$(`#right-sub-panel-container`).html("").css({width:0}).removeClass("active")
						}
						$(".save-costumizer").find("i").removeClass("fa-spin fa-spinner").addClass("fa-save");

						//local storage management 

						ctrlZOlderData = {};
						$.each(olderData, function(key, value){
							cmsConstructor.helpers.createObject(ctrlZOlderData , key, value);
						});

						if ( actionFor === "update" ) {
							cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,null,"menu", { "olderData" : ctrlZOlderData , "pathChanged" : costumizer.actions.pathToValue });
							var actualPage = location.hash == "" ? "#welcome" : location.hash;
							var ctrlY = JSON.parse(localStorage.getItem("ctrlY"+costum.slug)) || {} ;
							ctrlY[actualPage] = [];
							localStorage.setItem("ctrlY"+costum.slug, JSON.stringify(ctrlY));
						} else if ( actionFor === "ctrlZ" ) {
							cmsConstructor.helpers.insertObjectToLocalStorage("ctrlY"+costumizer.mode+costum.slug,null,null,"menu", { "olderData" : ctrlZOlderData , "pathChanged" : costumizer.actions.pathToValue });
						} else if ( actionFor === "ctrlY") {
							cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,null,null,"menu", { "olderData" : ctrlZOlderData , "pathChanged" : costumizer.actions.pathToValue });
						}



						costumizer.actions.pathToValue=[];
						//costumizer.actions.objDiff = [];
						if(notEmpty(costumizer.multiSpaces)){
							$.each(costumizer.multiSpaces, function(e, space){
								if(typeof costumizer[space].actions != "undefined"
									&& typeof costumizer[space].actions.callback === "function")
									costumizer[space].actions.callback(data);
							});
						} else if(typeof costumizer[costumizer.space].actions != "undefined"
							&& typeof costumizer[costumizer.space].actions.callback === "function")
							costumizer[costumizer.space].actions.callback(data);
					},
					null,
					"json"//,
					//{contentType: 'application/json'}

				);
				costumizer["reloadApp"] = true;
		},
		cancel : function(dom){
			if(((costumizer.liveChange && dom=="#menu-right-costumizer") || dom=="#modal-content-costumizer-administration") 
				&& !$(".save-costumizer").hasClass("disabled") &&
				notEmpty(costumizer.actions.pathToValue)){
				bootbox.confirm({
				    message: "<span class='text-center bold'>"+tradCms.warningUnsavedChanges+"</span>",
				    buttons: {
				        confirm: {
				            label: "<i class='fa fa-save'></i> "+trad.save,
				            className: 'btn-danger'
				        },
				        cancel: {
				            label: trad.cancel,
				            className: 'btn-default'
				        }
				    },
				    callback: function (result) {
				    	if(result){
				    		$(".save-costumizer").trigger("click");
				    		costumizer.actions.closeModal(dom, true);
				    	}else{
				    		costumizer.actions.commonSpaceFunction("cancel");
				    		/*if(notEmpty(costumizer.multiSpaces)){
				    			$.each(costumizer.multiSpaces, function(e, space){
									if(typeof costumizer[space].actions != "undefined"
										&& typeof costumizer[space].actions.cancel === "function"){
										costumizer[space].actions.cancel();
									}
								});
							}else if(typeof costumizer[costumizer.space].actions.cancel=="function"){
				    			costumizer[costumizer.space].actions.cancel();
				    		}*///else{
								costumizer.actions.pathToValue=[];
								//costumizer.actions.objDiff = [];
	    						costumizer.obj=jQuery.extend(true,{},costum);
				    		//}
				    		costumizer.actions.closeModal(dom, true); 
				    	}
				    }
				});
			}else{
				costumizer.actions.closeModal(dom); 
				costumizer.liveChange = true
			}
		},
		closeModal: function(dom, mainDom){
			//$("#menu-right-costumizer, #menu-right-costumizer .modal-tools-costumizer").css({"width":"300px"});		
			if(notEmpty(dom))
				$(dom).fadeOut(600);
			if(notEmpty(mainDom) || !notEmpty(dom)){
				$("#menu-right-costumizer, #modal-content-costumizer-administration").fadeOut(1000);
				$(".lbh-costumizer").parent().removeClass("active");
			}
			costumizer.actions.commonSpaceFunction("close");
		},
		tabs:{
			/**
			 * Initialize tabs views
			 * @param {object[]} tabs
			 * @param {string} tabs[].key - Clé pour identifier chaque tab
			 * @param {string} tabs[].icon - Icon du menu de navigation
			 * @param {string} tabs[].label - Label du menu de navigation
			 * @param {string|object|null} tabs[].content - Contenu du tab 
			 * @param {string} tabs[].containerClassname
			 * 
			 * @param {object} options
			 * @param {function} options.onInitialized
			 * @param {function} options.onTabChanged
			 * @param {function} options.getContent
			 * @param {string|object} options.footer - Pied de la vue
			 */
			init:function(container, tabs, options={}){
				var $tabsContainer = $(`
					<div class="cmsbuilder-tabs">
						<div class="cmsbuilder-tabs-header"><ul></ul></div>
						<div class="cmsbuilder-tabs-body"></div>
						<div class="cmsbuilder-tabs-footer"></div>
					</div>
				`)
	
				tabs.forEach(function(tab){
					/* add header */
					$tabsContainer.find(".cmsbuilder-tabs-header ul").append(`
						<li data-key="${tab.key}">
							<span><i class="fa fa-${tab.icon}" aria-hidden="true"></i></span>
							<span>${tab.label}</span>
						</li>
					`)
	
					/* add body */
					var $content = $(`<div class="cmsbuilder-tab-section ${tab.containerClassname?tab.containerClassname:""}" data-key="${tab.key}"></div>`)
					$content.append(tab.content ? tab.content:"");
					$tabsContainer.find(".cmsbuilder-tabs-body").append($content);
				})

				/* add footer */
				if(options.footer)
					$tabsContainer.find(".cmsbuilder-tabs-footer").append(options.footer);
				else
					$tabsContainer.find(".cmsbuilder-tabs-footer").remove();
	
				$tabsContainer.find(".cmsbuilder-tabs-header ul li").click(function(){
					$tabsContainer.find(".cmsbuilder-tabs-header ul li").removeClass("active");
					$(this).addClass("active");

					$tabsContainer.find(".cmsbuilder-tab-section").removeClass("active");

					var tabKey = $(this).data("key"),
						$container = $tabsContainer.find(`.cmsbuilder-tab-section[data-key="${tabKey}"]`)
					if(typeof options.getContent == "function")
						$container.html(options.getContent(tabKey))
					if(typeof options.onTabChanged == "function")
						options.onTabChanged(tabKey)

					$container.addClass("active");
					if (container == "#left-panel") {
						cmsConstructor.tabKeyLeftActive = tabKey;
					}

				});

				parent.$(container).html("").append($tabsContainer);
				if (container == "#left-panel") {
					$tabsContainer.find(`.cmsbuilder-tabs-header ul li[data-key="${cmsConstructor.tabKeyLeftActive}"]`).click();
				}else {
					$tabsContainer.find(`.cmsbuilder-tabs-header ul li`).first().click();
				}


				if(typeof options.onInitialized === "function")
					options.onInitialized();
				coInterface.initHtmlPosition();
			}
		},
		openRightPanel: function(view, config){
			/* if($("#toolsBar-edit-block").is(':visible')){
				$("#toolsBar-edit-block").hide();  
			}; */
			var domTarget="#menu-right-costumizer";
			if(typeof config.hover != "undefined" && config.hover){
				domTarget="#menu-right-costumizer-hover";
				$(domTarget).css({top : $("#menu-right-costumizer > .modal-tools-costumizer").outerHeight()+40})
			}else{
				$(domTarget+ " > #menu-right-costumizer-hover").hide();
			}

			if(typeof config.subRight != "undefined" && config.subRight){
				domTarget="#menu-sub-right-costumizer";
				$(domTarget+ ", "+domTarget+" > .modal-tools-costumizer, "+domTarget+" > .modal-tools-costumizer-footer").css({right : $("#menu-right-costumizer").outerWidth()});
			}
			var titleConfig=(typeof config.title != "undefined") ? config.title : "";
			var widthContainer=(typeof config.width != "undefined") ? config.width : 300;	
			$(domTarget+" , "+domTarget+" > .modal-tools-costumizer, "+domTarget+" > .modal-tools-costumizer-footer").css({"width":widthContainer+"px"});
			$(domTarget+" > .modal-tools-costumizer > .costumizer-title > span").text(titleConfig);
			$(domTarget+" > .modal-tools-costumizer").css({height : (typeof config.subMenuTools != "undefined") ? 80 : 45});
			$(domTarget+" > .contains-view-admin").css({"top": $(domTarget+" > .modal-tools-costumizer").outerHeight()});
			
			$(domTarget+" > .contains-view-admin").html(view);
			if(typeof config.subMenuTools != "undefined"){
				if($(domTarget+" > .modal-tools-costumizer .sub-nav").length <= 0)
					$(domTarget+" > .modal-tools-costumizer").append("<div class='sub-nav'></div>");
				costumizer.multiSpaces=[costumizer.space];
				$(domTarget+" > .modal-tools-costumizer .sub-nav").html(config.subMenuTools);
				costumizer.events.bindLivePanel();
			}
			else
				$(domTarget+" > .modal-tools-costumizer .subMenuTools").remove();
			if(typeof config.footer != "undefined"){
				$(domTarget+" > .modal-tools-costumizer-footer").show();
				$(domTarget+" > .contains-view-admin").css({"bottom": $(domTarget+" > .modal-tools-costumizer-footer").outerHeight()});
			}
			else{
				$(domTarget+" > .modal-tools-costumizer-footer").hide();
				$(domTarget+" > .contains-view-admin").css({"bottom": 0});
				
			}
			//$(domTarget).fadeIn(500);
			$(domTarget).addClass("active")
			//Bind events for space costumizer opened 
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].events != "undefined"
				&& typeof costumizer[costumizer.space].events.bind === "function")
				costumizer[costumizer.space].events.bind();
				//Function initCallback for workspace costumizer opened
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions.init === "function")
				costumizer[costumizer.space].actions.init();
			
		},
		/**
		 * 
		 * @param {'left'|'right'} targetPosition 
		 * @param {'open'|'close'} action 
		 */
		toogleSidePanel: function(targetPosition, action=null){
			var $target = parent.$(`.cmsbuilder-${targetPosition}-content`),
				open = action?(action == "open"):!$target.hasClass("active");

				if(open){
					$target.addClass("active")
					$(".btn-toggle-cmsbuilder-sidepanel-page").hide();
					if($target.find("#right-panel").html() == "")
						$target.find("#right-panel").html(`<p class="padding-20 text-center">${tradCms.PleasechooseAnItemToDisplayTheSettings}</p>`)
				}else{
					costumizer.actions.cancel("#menu-right-costumizer");
					$target.removeClass("active")
					$(".btn-toggle-cmsbuilder-sidepanel-page").show();
			}
			coInterface.initHtmlPosition();
		},

		/**
		 * @param {string|object} views
		 * @param {object} options
		 * @param {number} [options.width]
		 * @param {number} [options.distanceToRight]
		 * @param {string} [options.className]
		 * @param {function} [options.onInitialized]
		 * @param {object} [options.header]
		 * @param {string} options.header.title
		 * @param {"top"|"left"} options.header.position
		 */
		openRightSubPanel: function(views, options={}){
			var config = $.extend(true, {}, {
				width:150,
				distanceToRight:300,
				className:"",
				header:{
					title:"",
					position:"top"
				}
			}, options)

			var $container = $(`#right-sub-panel-container`)
			$container.html("")
			$container.css({ width:config.width+"px", right:config.distanceToRight+"px",display:"block"}).addClass("active")
			
			/* begin:header */
			var $header = $(`
				<div class="right-sub-panel-header right-sub-panel-header-${config.header.position}">
					<span>${config.header.title}</span>
					<button><i class="fa fa-${ (config.header.position==="top")?"times":"chevron-right" }" aria-hidden="true"></i></button>
				</div>
			`)
			$header.find("button").click(function(){
				$container.css({ width:0 }).removeClass("active")
				var params = {functionName: "elementUnfocused", blockId : {}, userId : userId}
				costumSocket.emitEvent(wsCO, "edit_component", params)
			})
			$container.append($header)
			/* end:header */

			var $body = $(`<div class="right-sub-panel-body ${config.className}"  ${typeof config.idName != "undefined" ? "id='"+ config.idName + "'" : ""}></div>`)
			$body.append(views)
			$container.append($body)

			if(typeof config.onInitialized == "function")
				config.onInitialized()
			
			//Bind events for space costumizer opened 
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].events != "undefined"
				&& typeof costumizer[costumizer.space].events.bind === "function")
				costumizer[costumizer.space].events.bind();
				//Function initCallback for workspace costumizer opened
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].actions != "undefined"
				&& typeof costumizer[costumizer.space].actions.init === "function")
				costumizer[costumizer.space].actions.init();
		},
		closeRightSubPanel:function(){
			$(`#right-sub-panel-container`).html("").css({width:0}).removeClass("active")
		},
		/**
		 * 
		 * @param {object} params
		 * @param {string} params.space
		 * @param {string} params.title
		 * @param {string} [params.domTarget]
		 * @param {boolean} [params.showSaveButton] 
		 */
		openCostumizer: function(params){

			this.closeRightSubPanel()

			var config = $.extend(true, {}, {
				dom:"#modal-content-costumizer-administration",
				savebutton:true
			}, params)

			if($("#toolsBar-edit-block").is(':visible')){
				$("#toolsBar-edit-block").hide();  
				coInterface.initHtmlPosition();
			};
 
			costumizer.space=config.space;
			costumizer.multiSpaces=[];
			if($(config.dom+" .contains-view-admin").length >= 0)
				coInterface.showLoader(config.dom+" .contains-view-admin");
			costumizer.liveChange = false
			
			// Initialization of views
			if(typeof costumizer[costumizer.space] != "undefined"
				&& typeof costumizer[costumizer.space].init === "function")
				costumizer[costumizer.space].init();

			if(config.dom == "#menu-right-costumizer"){
				costumizer.actions.tabs.init(
					"#right-panel",
					[
						{
							key:"design",
							icon:"paint-brush",
							label:config.title,
							content:costumizer[costumizer.space].views.init(),
							containerClassname:"contains-view-admin"
						}
					],
					{
						footer:`
							<button class="btn btn-sm btn-danger save-costumizer disabled"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
						`,
						onInitialized:function(){
							costumizer.events.save()
							
							//Bind events for space costumizer opened 
							if(typeof costumizer[costumizer.space] != "undefined"
								&& typeof costumizer[costumizer.space].events != "undefined"
								&& typeof costumizer[costumizer.space].events.bind === "function")
								costumizer[costumizer.space].events.bind();
								//Function initCallback for workspace costumizer opened
							if(typeof costumizer[costumizer.space] != "undefined"
								&& typeof costumizer[costumizer.space].actions != "undefined"
								&& typeof costumizer[costumizer.space].actions.init === "function")
								costumizer[costumizer.space].actions.init();
						}
					}
				)
			}else if(config.dom == "#menu-right-history"){
				costumizer.actions.tabs.init(
					"#right-panel",
					[
						{
							key:"history",
							icon:"history",
							label:cmsConstructor.kunik != "" && $(".block-actions-wrapper").hasClass("selected") ? tradCms.blockHistory : tradCms.history,
							content:costumizer[costumizer.space].views.init(),
							containerClassname:"contains-view-admin"
						}
					],
					{
						onInitialized:function(){
							costumizer.events.save()

							//Bind events for space costumizer opened
							if(typeof costumizer[costumizer.space].events != "undefined"
								&& typeof costumizer[costumizer.space].events.bind === "function")
								costumizer[costumizer.space].events.bind();
							//Function initCallback for workspace costumizer opened
							if(typeof costumizer[costumizer.space].actions != "undefined"
								&& typeof costumizer[costumizer.space].actions.init === "function")
								costumizer[costumizer.space].actions.init();
						}
					}
				)
			}
			else{
				costumizer.initView(config);
			}

			if(config.savebutton === true)
				$("#modal-content-costumizer-administration .save-costumizer").show().addClass("disabled");
			else
				$("#modal-content-costumizer-administration .save-costumizer").hide();

			$("#ajax-modal.portfolio-modal.modal").css({"z-index":"100003","top" : "100px", "left": "40px"});

			// SocketIO, Reveal element focused in panel page edition
			if (typeof costum.members    != "undefined" && typeof costum.members.focused != "undefined") {
				$.each(costum.members.focused, function(key, value) {
					costumSocket.socketListener.elementFocused({elementTarget: value.elementTarget, userId: key,page:value.page});    
				})
			}
		},
		showTutorial : function (gifUrl,destination="") {
				var dialog = bootbox.dialog({
					title: "Help center",
					message: `<p>${destination}</p><img class="img-responsive" src="${gifUrl}">`,
					size: 'large',
					buttons: {
						ok: {
							label: trad.Close,
							callback: function() {
							}
						}
					}
				});
		},
		saveCostum : function () {
				var today = moment().format('D MMM, YYYY');

				var saveDialog = `
				<div class="form-group">
				  <label for="costum-name">${tradCms.versionname}</label>
				  <input type="text" class="form-control" value="${costum.title}: ${today}" id="co-version">
				  <label for="costum-name">${tradCms.shortDescription} (${tradCms.optional})</label>
				  <textarea type="textarea" class="form-control" value="" id="version-desciption"></textarea>
				</div>
				`
				var btnDisabler = ""
				costumizer.versionning.helpers.getVersion().then(function(savedCostum){
					//Show up the list of version if their length is superior 2  
					if (Object.keys(savedCostum.items).length > 1) {
						saveDialog = `<span style="color:red;">${tradCms.savingcostum}, ${trad.refused}!</span><br> ${tradCms.numberofbuckuplimited};`
						saveDialog += savedCostum.costum
						btnDisabler = "disabled"
						setTimeout(function() {									
							costumizer.versionning.events.init();
						},100)
					}
					bootbox.dialog({
						message: saveDialog,
						title: `${tradCms.savingcostum}`,
						buttons: {
							main: {
								label: trad.save,
								className: "btn-primary "+btnDisabler,
								callback: function() {
									var dataVersion = {
										versionName : $("#co-version").val(),
										versionDesc : $("#version-desciption").val(),
										contextId   : costum.contextId,
										contextType : costum.contextType
									}
									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/saveversion",
										{
											dataVersion
										},
										function(data){
											costumizer.versionning.helpers.getVersion()
											toastr.success(tradCms.elementwelladded);
										},
										null

										);
								}
							}
						}
					});
				})

		},
		admins : function () {	
			ajaxPost(
				null,
				baseUrl+"/"+moduleId+"/cms/costumadmin",
				{
					contextId : costum.contextId, contextType : costum.contextType
				},
				function(data){
					var users = "";
					costum["members"] = {}
					costum["members"]["lists"] = {};
					$.each(data.admins, function (k,val) {
						if (notNull(val)) {
							val.name = val.name.replace("'","&quot;")
							costum["members"]["lists"][val._id.$id]= {};
							costum["members"]["lists"][val._id.$id]["name"]   = val.name;
							costum["members"]["lists"][val._id.$id]["slug"]   = val.slug;
							costum["members"]["lists"][val._id.$id]["roles"]  = val.roles;
							costum["members"]["lists"][val._id.$id]["action"]  = val.action;
							costum["members"]["lists"][val._id.$id]["color"]  = "#"+val._id.$id.substring(13, 19)
							costum["members"]["active"] = {};
							costum["members"]["focused"] = {};
							var userHtml = `<a class="user-connected-item admin${val._id.$id} letter-azure user-trigger" data-userid="${val._id.$id}" data-original-title title data-content='
							
							<div class="user-connected-information">
								<div class="user-header${(val._id.$id != userId) ? ' another-user-header' : ''}">
									<a href="${typeof val.slug != "undefined" ? baseUrl+"#@"+val.slug : ""}" class="admin${val._id.$id} padding-5 user-trigger img-user-profil" target="_blank">`
									if (typeof val.profilImageUrl != "undefined") {
										userHtml += `<img style="display: inline;border: solid ${costum["members"]["lists"][val._id.$id]["color"]}c9;height: 60px; width: 60px;" src="${val.profilImageUrl}" />`
									}else{
										userHtml += `<i class="fa fa-user" style="border: solid ${costum["members"]["lists"][val._id.$id]["color"]}c9; color:${costum["members"]["lists"][val._id.$id]["color"]};height: 60px; width: 60px;font-size: 50px;text-align: center;"></i>`
									}
									userHtml +=`</a>
									<div class="user-name-text">
										<strong>${val.name} ${val._id.$id == userId ? '('+trad.you+')' : ""}</strong>
										<small>${typeof val.roles.superAdmin !="undefined" || typeof val.roles.superAdminCms !="undefined" ? "(Super admin CMS)" :"(Admin)" }</small>
									</div>
								</div>
								`

								if (val._id.$id != userId) {
									userHtml += `
									<div class="user-footer">
										<a class="admin${val._id.$id} letter-azure go-to-user link-modification" data-userid="${val._id.$id}">Voir sa modification</a>
										<a data-id="${val._id.$id}" data-name="${val.name}" data-username="${typeof val.username != "undefined" ? val.username : val.slug}" data-slug="${val.slug}" href="javascript:;" class="link-chat hidden-xs"><i class="fa fa-comments"></i> Message</a>
									</div>`
								}
								userHtml += `
							</div>
							<div id="action-count-${val.slug}" class="section-action-count">
								<div class="panel panel-default" style="border: 0;box-shadow: none;">
									<div class="panel-body" style="padding: 5px 5px 0px 5px;;">
										<ul>`;
										if (typeof val.action != "undefined"){
											$.each(val.action, function (key,value) {
												var labelCount = "Page";
												if (key == "costum/editBlock")
													labelCount = tradCms.editBlock;
												else if (key == "costum/addBlock")
													labelCount = tradCms.addBlock;
												else if (key == "costum/deleteBlock")
													labelCount = tradCms.deleteBlock;
												else if (key == "costum/config")
													labelCount = tradCms.menuAndOther;
												else if (key == "costum/options")
													labelCount = tradCms.optionsCostum;

												userHtml += `
												<li>
													  <strong>${labelCount}</strong>
													  <span class="badge pull-right hidden-xs">${value}</span>
												</li>`;
											});
										} else {
											userHtml += `
												<li>
													 <strong> ${val.name} n\`as pas encore contribuer dans le site </strong>
												</li>`;
										}


							userHtml += `
										</ul>
									</div>
								</div>
							</div>
							<a data-slug="${val.slug}" href="javascript:;" class="btn btn-contribution btn-success " >Voir la contribution</a>
						'>`
						if (typeof val.profilImageUrl != "undefined") {
							userHtml += `<img class="costum-user-img-avatar tooltips" data-placement="right" data-toggle="tooltip" title="${val.name}" style="display: inline;border: solid ${costum["members"]["lists"][val._id.$id]["color"]}c9;" src="${val.profilImageUrl}" />`
						}else{
							userHtml += `<i class="costum-user-img-avatar fa fa-user tooltips" data-placement="right" data-toggle="tooltip" title="${val.name}" style="border: solid ${costum["members"]["lists"][val._id.$id]["color"]}c9; color:${costum["members"]["lists"][val._id.$id]["color"]}"></i>`
						}
						userHtml += `</a>`;
						costum["members"]["lists"][val._id.$id]["avatar"] = userHtml;
						costum["members"]["lists"][val._id.$id]["avatarWithName"] = `
						<div class="user-admin sp-cms-100" data-userid="${val._id.$id}">${costum.members["lists"][val._id.$id]["avatar"]}
						<a href="${typeof costum.members["lists"][val._id.$id].slug != "undefined" ? baseUrl+"#@"+costum.members["lists"][val._id.$id].slug : ""}" 
						class="avatar-admin-name"
						target='_blank'> ${costum.members["lists"][val._id.$id].name}
									<span class="pull-right" style="right: 0;position: absolute;">
								<i data-userid="${val._id.$id}" class="user-dot fa fa-circle"></i>
							</span>
						</a></div>`;
						
						}
					})					
					
				},
				null

				);
		},
		onElementFocused: function(params){
			if (params.userId != userId && !params.elementTarget.includes("lbh-costumizer")) {
				// $(".cmsbuilder-block[data-kunik='"+params.elementTarget+"']").find("li[data-action='deselect']").click()
				if ($(params.elementTarget).hasClass("configuration-menus-costumizer")) {
					costumizer.actions.onElementUnfocused({userId: params.userId});
					$(params.elementTarget).addClass("element-busy")
					$(params.elementTarget).attr("data-userid",params.userId) 
					var $actions = $(`<div class="block-busy-wrapper" data-anchor-target="anchor${params.userId}" data-userid="${params.userId}" style="border: 2px solid ${costum.members.lists[params.userId].color};background-color: ${costum.members.lists[params.userId].color}29"></div>`)
					$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][params.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[params.userId].color};">${costum.members["lists"][params.userId].name} - en train d'éditer</span></div>`);
					$(params.elementTarget).prepend($actions)
				}else if ($('[id="'+params.elementTarget+'"]').hasClass("pages-line")){
					costumizer.actions.onElementUnfocused({userId: params.userId});
					$('[id="'+params.elementTarget+'"]').css("pointer-events", "none")
					$('[id="'+params.elementTarget+'"]').attr("data-userid",params.userId) 
					$('[id="'+params.elementTarget+'"]').addClass("edition-desabled")
					$('[id="'+params.elementTarget+'"]').find(".editPage").attr("data-userid",params.userId)
					var $actions = $(`<div class="block-busy-wrapper" data-anchor-target="anchor${params.userId}" data-userid="${params.userId}" style="border: 1.5px solid ${costum.members.lists[params.userId].color};background-color: ${costum.members.lists[params.userId].color}29"></div>`)
					$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][params.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[params.userId].color};">${costum.members["lists"][params.userId].name} - en train d'éditer</span></div>`);
					$('[id="'+params.elementTarget+'"]').prepend($actions)
				}else{
					costumizer.actions.onElementUnfocused({userId: params.userId});
					var $actions = $(`<div class="block-busy-wrapper" data-userid="${params.userId}" style="border: 2px solid ${costum.members.lists[params.userId].color};background-color: ${costum.members.lists[params.userId].color}29"></div>`)
					$actions.prepend(`<div class="cmsbuilder-block-action-label" style="background-color: transparent"><span class="block-busy-avatar" style="background-color: #fff;">${costum.members["lists"][params.userId].avatar}</span><span class="block-busy-label" style="background-color: ${costum.members.lists[params.userId].color};">${costum.members["lists"][params.userId].name} - en train d'éditer</span></div>`);
					$(".cmsbuilder-block[data-kunik='"+params.elementTarget+"']").prepend($actions)
					if($(".cmsbuilder-block[data-kunik='"+params.elementTarget+"']").data("blocktype") == "text"){            
						$(".sp-text[data-kunik='"+params.elementTarget+"']").addClass("element-busy")
						$(".sp-text[data-kunik='"+params.elementTarget+"']").attr("data-userid",params.userId)
					}                                              
					$(".cmsbuilder-block[data-kunik='"+params.elementTarget+"']").addClass("element-busy")
					$(".cmsbuilder-block[data-kunik='"+params.elementTarget+"']").attr("data-userid",params.userId)
					//Layer busy
					$(".page-layer-item-btn[data-kunik='"+params.elementTarget+"']").addClass("element-busy")
					$(".page-layer-item-btn[data-kunik='"+params.elementTarget+"']").attr("data-userid",params.userId)

					if(typeof costumizer.isWaitForLoad != "undefined" && costumizer.isWaitForLoad == params.userId){
						costumizer.scrollToTarget(".block-busy-wrapper[data-userid='"+params.userId+"']");
						delete costumizer.isWaitForLoad;
					}		

				}
			}
			costum.members.focused[params.userId] = {page : params.page, elementTarget : params.elementTarget}; 
		},
		onElementUnfocused: function(params){			
			if (params.userId != userId) {
				delete costum.members.focused[params.userId]
				$(".block-busy-wrapper[data-userid='"+params.userId+"'").remove()
				$(".element-busy[data-userid='"+params.userId+"'").removeClass("element-busy").removeAttr("data-userid")
				$(".edition-desabled[data-userid='"+params.userId+"'").css({"pointer-events": "", "margin-top": "0"}).css("background-color","").removeClass("edition-desabled").removeAttr("data-userid")
			}
		},
		getAnswerPath(value){
			var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
			var children = [];
			var finder = [];
			for(const stepKey in childForm[value] ){
				for(const inputKey in childForm[value][stepKey]){
					var input = childForm[value][stepKey][inputKey];
		
					if(input["type"].includes(".multiCheckboxPlus")){
						children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
					} else if(input["type"].includes(".multiRadio")){
						children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
					}/*else if(input["type"].includes(".radiocplx")){
						children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
					}else if(input["type"].includes(".checkboxcplx")){
						children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
					}*/else if(input["type"].includes(".checkboxNew")){
						children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
					}else if(input["type"].includes(".radioNew")){
						children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
					}else if(input["type"].includes(".finder")){
						finder.push({value: stepKey+'.finder'+inputKey, label: input["label"]});
					}/* else {
						children.push({value: stepKey+'.'+inputKey, label: input["label"]});
					}  */
				}
			}
			return {input: children, finder : finder};
		},
		getAnswerValue(coformId, value){
			var coforme = [];
			if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
				coforme = costum["dashboardGlobalConfig"]["formTL"];
			}
			if(typeof coforme[coformId] != "undefined" ){
				coforme = coforme[coformId];
			}
			var input = value.split(".");
			var answerValue = [];
			if(typeof input[1] != "undefined"){
				input = input[1];
				if(typeof input!="undefined" && ( input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx") ) ){
					if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["global"]){
						for(const paramValue of coforme["params"][input]["global"]["list"]){
							answerValue.push({value: paramValue, label: paramValue})
						}
					}
				}
			
				if( typeof input!="undefined" && ( input.includes("checkboxNew") || input.includes("radioNew") ) ){
					if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["list"]){
						for(const paramValue of coforme["params"][input]["list"]){
							answerValue.push({value: paramValue, label: paramValue})
						}
					}
				}
			}
			return answerValue;
		},
		configFilterAnswers(appKey){
			var filterAnswers = {};
			if(typeof costumizer.obj.app[appKey].filters != "undefined" && typeof costumizer.obj.app[appKey].filters.answers != "undefined"){
				filterAnswers = costumizer.obj.app[appKey].filters.answers;
			}
			var answerData = $.extend(true, {}, filterAnswers);
			var newData = $.extend(true, {}, filterAnswers);
			costumizer.actions.openRightSubPanel(
				"<form id='costum-form-pages' style='padding:0px 10px;'></form>",
				{
					width:500,
					distanceToRight: costumizer.liveChange ? 300:0,
					className:"contains-view-admin",
					header:{
						title:"Configurer le filtre par réponse"
					},
					onInitialized: function(){
						$("#costum-form-pages").append(`<div class="containte-form-page cmsbuilder-tabs-body col-xs-12"></div>`);
						var coformOptions = [];
						var answerPathData = [];
						if(typeof costum["dashboardGlobalConfig"] != "undefined") {
							coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
								return {value: val,label: key}
							})
						}else{
							costumizer.actions.getFormsData();
							coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
								return {value: val,label: key}
							})
						}
						var selectedData = "";
						var myCoInput = {
							container : ".containte-form-page",
							inputs :[
								{
									type : "select",
									options: {
										label : tradCms.selectaform ?? "Selectionner un formulaire",
										isSelect2 : true,
										configForSelect2: {
											allowClear: true
										},
										name : "coform",
										class: "coform-answers",
										options : coformOptions.length > 0 ? coformOptions : []
									}
								},
								{
									type : "select",
									options : {
										name : "answerPath",
										isSelect2 : true,
										class: "answerpath-answers",
										label : "Selectionner une question",
										options : answerPathData.length > 0 ? answerPathData : []
									}
								},
								{
									type : "select",
									options : {
										name : "finderPath",
										isSelect2 : true,
										class: "finderpath-answers",
										label : "Selectionner la question pour connaitre l'élément qui a répondu",
										options : []
									}
								},
								{
									type : "inputSimple",
									options : {
										name : "label",
										class: "label-answers",
										label : "Nom du filtre"
									}
								},
								{
									type : "selectMultiple",
									options : {
										name : "answerValue",
										class: "answervalue-answers",
										label : "Valeur à afficher sur le filtre",
										options : []
									}
								},
								{
									type : "selectMultiple",
									options : {
										name : "answerFilter",
										class: "answerfilter-answers",
										label : "Valeur à chercher dans les tags",
										options : []
									}
								},
							],
							onchange:function(name, value, payload){
								if(name == "coform"){
									var answerData = costumizer.actions.getAnswerPath(value);
									$(".answerpath-answers > select").select2("destroy")
									var str = '<option></option>';
									answerData.input.forEach((element) => {
										str += `<option value="${element.value}">${element.label}</option>`;
									});
									$(".answerpath-answers > select").html(str);
									$(".answerpath-answers > select").select2({});

									$(".finderpath-answers > select").select2("destroy")
									var str = '<option></option>';
									answerData.finder.forEach((element) => {
										str += `<option value="${element.value}">${element.label}</option>`;
									});
									$(".finderpath-answers > select").html(str);
									$(".finderpath-answers > select").select2({});
								}
		
								if(name == "answerPath"){
									var answerValue = costumizer.actions.getAnswerValue($(".coform-answers > select").val() ,value);
									$(".answervalue-answers > select").select2("destroy")
									$(".answerfilter-answers > select").select2("destroy")
									var str = '<option></option>';
									answerValue.forEach((element) => {
										str += `<option value="${element.value}">${element.label}</option>`;
									});
									$(".answervalue-answers > select").html(str);
									$(".answervalue-answers > select").val(answerValue.map(function(element){
										return element.value;
									}));
									$(".answervalue-answers > select").select2({});

									$(".answerfilter-answers > select").html(str);
									$(".answerfilter-answers > select").select2({});
									
								}
							}
						}
						
						new CoInput(myCoInput);
		
						$("#costum-form-pages > .containte-form-page").append(`<div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;">
							<a href="javascript:;" class="btn btn-primary col-xs-12 " id="btn-add-data"><i class="fa fa-save"></i> Enregistrer le filtre</a>
						</div>`);
						var answerStr = "";
						$.each(answerData, function(k, v){
							answerStr += `
								<tr class="update-config-answers" data-key='${k}' data-answers='${JSON.stringify(v)}'>
									<td>${v.label}</td>
									<td width="10%">
										<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${k}'><i class='fa fa-trash'></i></button>
									</td>
								</tr>
							`;
						})
						$("#costum-form-pages").append(`<div class="cmsbuilder-table-content col-xs-12 no-padding" style="margin-top: 10px">
							<h6>Liste des filtres</h6>
							<table class="table table-bordered" id="liste-filters-answers">
								<tbody>
									${answerStr}
								</tbody>
							</table>
						</div>`);
								
						$("#costum-form-pages #btn-add-data").click(function() {
							if($(".form-control.label-answers").val() && $(".answerpath-answers > select").val()){
								key = selectedData.length > 0 ? selectedData : $(".form-control.label-answers").val().toLowerCase();
								if(typeof newData[key] == "undefined"){
									newData[key] = {
										"label": $(".form-control.label-answers").val(),
										"forms": $(".coform-answers > select").val(),
										"path": $(".answerpath-answers > select").val()
									}
									if($(".answervalue-answers > select").val() != null){
										newData[key].visible = $(".answervalue-answers > select").val();
									}
									if($(".answerfilter-answers > select").val() != null){
										newData[key].filterByTag = $(".answerfilter-answers > select").val();
									}
									if($(".finderpath-answers > select").val() != ''){
										newData[key].finderPath = `answers.${$(".finderpath-answers > select").val()}`;
									}
									$("#liste-filters-answers > tbody").append(`<tr class="update-config-answers" data-key='${key}'>
											<td>${newData[key].label}</td>
											<td width="10%">
												<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${key}'><i class='fa fa-trash'></i></button>
											</td>
										</tr>`);
									resetForm();
								}else{
									if(selectedData.length > 0){
										newData[key] = {
											"label": $(".form-control.label-answers").val(),
											"forms": $(".coform-answers > select").val(),
											"path": $(".answerpath-answers > select").val()
										}
										if($(".answervalue-answers > select").val() != null){
											newData[key].visible = $(".answervalue-answers > select").val();
										}
										if($(".answerfilter-answers > select").val() != null){
											newData[key].filterByTag = $(".answerfilter-answers > select").val();
										}
										$(`#liste-filters-answers > tbody > tr.update-config-answers[data-key='${key}']`).html(
											`<td>${newData[key].label}</td>
											<td width="10%">
												<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${key}'><i class='fa fa-trash'></i></button>
											</td>`
										)
										resetForm();
										selectedData = "";
									}else{
										toastr.error("Il existe déjà un filtre avec ce nom");
									}
								}
								
							}
							$("#costum-form-pages #btn-submit-form").attr('disabled', JSON.stringify(answerData) == JSON.stringify(newData));
						});
						function resetForm(){
							$(".form-control.label-answers").val("");
							$(".coform-answers > select").val("");
							$(".answerpath-answers > select").val("");
							$(".answervalue-answers > select").val([]);
							$(".answerfilter-answers > select").val([]);
							$(".finderpath-answers > select").val('');
							$(".coform-answers > select").trigger("change.select2");
							$(".answerpath-answers > select").trigger("change.select2");
							$(".answervalue-answers > select").trigger("change.select2");
							$(".finderpath-answers > select").trigger("change.select2");
							$(".answerfilter-answers > select").trigger("change.select2");
						}
						$(document).on("click", "#costum-form-pages .update-config-answers", function() {
							var key = $(this).data("key");
							var value = newData[key];
							selectedData = key;
							$(".coform-answers > select").val(value.forms);
							$(".coform-answers > select").trigger("change");
							$(".form-control.label-answers").val(value.label);
							$(".answerpath-answers > select").val(value.path);
							$(".answerpath-answers > select").trigger("change");
		
							if(typeof value.visible != "undefined"){
								$(".answervalue-answers > select").val(value.visible);
								$(".answervalue-answers > select").trigger("change.select2");
							}
							if(typeof value.finderPath != "undefined"){
								$(".finderpath-answers > select").val(value.finderPath.replace("answers.", ""));
								$(".finderpath-answers > select").trigger("change.select2");
							}
							if(typeof value.filterByTag != "undefined"){
								$(".answerfilter-answers > select").val(value.filterByTag);
								$(".answerfilter-answers > select").trigger("change.select2");
							}
						});		
						$(document).on('click', "#costum-form-pages .btn-remove-config", function() {
							var key = $(this).data("key");
							delete newData[key];
							$(this).parents("tr").remove();
		
							$("#costum-form-pages #btn-submit-form").attr('disabled', JSON.stringify(answerData) == JSON.stringify(newData));
							
						});
		
						$("#costum-form-pages").append(`<div class="cmsbuilder-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
							<a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled><i class="fa fa-save"></i> `+tradCms.register+`</a>
						</div></div>`);
						
						$("#costum-form-pages #btn-submit-form").click(function() {
							var dataToSend = costumizer.obj.app;
							if(typeof dataToSend[appKey].filters == "undefined"){
								dataToSend[appKey].filters = {};
							}
							dataToSend[appKey].filters.answers = newData;
							var params = {
								"app": dataToSend
							}
							ajaxPost(
								null,
								baseUrl+"/"+moduleId+"/cms/updatecostum",
								{
									params: params,
									costumId    : costum.contextId,
									costumType  : costum.contextType,
									page : appKey,
									action : "filter/answers",
									subAction : "saveFilters"
								},
								function(data){
									costum.app=jQuery.extend(true,{},costumizer.obj.app);
									toastr.success(tradCms.updateConfig);
									$("#right-sub-panel-container").fadeOut();
								}			
							);
						});
						
					}
				}
			)
		},
		getFormsData: function(){
			var params = {
				"costumId":costum.contextId, 
				"costumSlug": costum.contextSlug,
				"costumType":costum.contextType
			}
			ajaxPost(
				null, 
				baseUrl+"/costum/blockcms/getformsdata", 
				params, 
				function(res){
					costum["dashboardGlobalConfig"] = res["global"];
				},
				null,
				null,
				{async:false}
			)
		},
		//Fonction append color palette 
		appendColorPalette: function(valueColor){
			// Sélectionner le conteneur
			const $paletteDiv = $('#id-palette-color');
			$paletteDiv.empty();

			var pallette = '';
			// Ajouter dynamiquement les divs pour chaque couleur
			valueColor.forEach((colorObj, index) => {
				pallette += `
								<div id="${colorObj.type}" 
									class="draggabled-co kl-palette-drag-drop"
									draggable="true"
									data-color=${colorObj.color}
									style="background-color: ${colorObj.color};">
								</div>`;
			});

			$paletteDiv.append(pallette);
			cmsConstructor.builder.events.dragAndDropColor();

			// Si le tableau est vide, masquer la palette
			if (valueColor.length === 0) {
				$paletteDiv.css('display', 'none');
			} else {
				$paletteDiv.css('display', 'block');
			}
		}
	},
	responsive : {
		md : function(){
			currentPage = "";
			if ( $('#responsiveScreen').length > 0){
				
				currentPage = document.getElementById("responsiveScreen").contentWindow.location.hash;

				// if ( currentPage != ""){
				// 	// urlCtrl.loadByHash(currentPage)
				// }else {
				// 	// urlCtrl.loadByHash("#welcome")
				// }

				window.location = `${baseUrl}/costum/co/index/slug/${costum.slug}/edit/true/${currentPage}`;
				window.location.reload();
				// costumizer.mode = "md";
			}

			
			$(".cmsbuilder-center-content").removeClass("overflow-hidden")
			$(".cmsbuilder-content-wrapper").addClass("d-none")
			$("#cmsbuilder-content-md").removeClass("d-none")
		},
		switchToMobileView: function(format, marginVertical=0){
			var $container = $("#cmsbuilder-content-"+format)

			$(".cmsbuilder-center-content").addClass("overflow-hidden")
			$(".cmsbuilder-content-wrapper").addClass("d-none")
			$("#responsiveScreen").remove()
			$container.removeClass("d-none")
			$(".device-time").html(moment().format('HH:mm'))
			var adjustScale = function(){
				var scaleHeight =  ($(".cmsbuilder-center-content").height()-marginVertical)/$container.height(),
					scaleWidth = $(".cmsbuilder-center-content").width()/$container.width();

				$container.css({
					"transform":`scale(${ scaleHeight < scaleWidth ? scaleHeight:scaleWidth })`
				})	
			}

			adjustScale()

			var $iframe = $(`<iframe  id="responsiveScreen" src="${baseUrl}/costum/co/index/slug/${costum.slug}/edit/true/responsive/${format}/${location.hash}" frameborder="0" allow-same-origin></iframe>`)
			$iframe.on("load", function(){
				

				$iframe.contents().find("head").append(`
					<style>
						body::-webkit-scrollbar {
							display: none !important;
						}

						body{
							overflow: auto !important;
						}
						
					</style>
				`)

				//then set up some access points
				var contents = $(this).contents(); // contents of the iframe

			})
			$container.find(".device-page").html($iframe)
			costumizer.mode = format;
		},
		sm : function(){
			this.switchToMobileView("sm")
		},
		xs : function(){
			this.switchToMobileView("xs", 60)
		}
	},
	valueImage : "",
	
	generalInfos : {
		refreshdropdownlanguage: false,
		isOpenAIKeyEntered: null,
		views : {
			init : function(){
				return str = '<div class="col-xs-12">' +
								'<div id="costum-info" class="col-xs-12 col-sm-12 col-md-8"></div>' +
								'<div class="col-xs-12 col-sm-12 col-md-4 position-sticky">'+
									'<div class="contain-info">'+
										'<div id="avatar">'+
											'<img class="logo-image-infoG" src="'+costumizer.obj.logo+'" />'+
										'</div>'+
										'<div id="cover"></div>'+
										'<div id="info">'+
											'<h4><i class="fa fa-info-circle"></i> '+tradCms.costumGeneralSettingsUpdate+'</h4>'+
											'<p></p>'+
										'</div>'+
									'</div>'+
									'<div id="costum-other-info" class="well contain-other-info"></div>'+
									/*'<div id="costum-version" class="well contain-other-info bg-white"></div>'+*/
									'<div id="costum-use-open-ai">'+
										'<div class="well">'+
											'<div class="title-header">'+
												'<h4>OPENAI</h4>'+
												'<a href="https://openai.com/" target="_blank" data-toggle="tooltip" title="'+tradCms.goToOfficialSIte+'"><i class="fa fa-external-link"></i></a>'+
											'</div>'+
											'<div id="costum-use-open-ai-input"></div>'+
											'<div id="open-modal-to-add-or-replace-key"><button class="btn btn-sm co-input-btn-style open-modal-btn"></button></div>'+
										'</div>'+
										'<div class="footer"><a class="open-ai-modal-information"><small>'+tradCms.howgetkeyApiOpenAI+'</small></a></div>'+
									'</div>'+
									'<div class="well contain-version-info">' +
										'<div class="version-title"><h4>Version Costum</h4></div>' +
										'<div id="costum-version">' +	
											'<center class="padding-20">'+tradCms.none+'</center>'+									
										'</div>' +
										'<div class="sp-cms-100" style="justify-content: center;">'+
											'<button onclick="costumizer.actions.saveCostum()" class="btn btn-sm co-input-btn-style save-costum-version"><i class="fa fa-download" aria-hidden="true"></i> '+tradCms.savethiscostum+'</button>'+
										'</div>' +
									'</div>'+
								'</div>'+
							  '</div> ';
			}

		},
		actions : {
			callback : function(data){
				/* dyFObj.commonAfterSave(costumizer.obj,function(){}); */
				$(".modal-content-costumizer-administration").hide();
			},
			initImageByPath : function(key, path){
				if(path.indexOf(costum.assetsUrl) === -1 && path.indexOf("/upload/") === -1 && path.indexOf("/images/thumbnail-default.jpg") === -1 )
					jsonHelper.setValueByPath(costumizer.obj, key, costum.assetsUrl+path);
				else{
					if(key == "faviconSourceImage" && path != "")
						jsonHelper.setValueByPath(costumizer.obj, key, path);
				}
			},
			updateLanguageCostum: function(value) {
				var paramsToSave= {};
				var olderData = {};
				olderData["language"] = costum.language;
				paramsToSave["language"] = value;
				if (typeof costum.otherLanguages != "undefined" && notEmpty(costum.otherLanguages)) {
					const indexLangueInit = costum.otherLanguages.indexOf(value);
					if (indexLangueInit !== -1) {
						olderData["otherLanguages"] = costum.otherLanguages;
						costum.otherLanguages.splice(indexLangueInit, 1);
						paramsToSave["otherLanguages"] = costum.otherLanguages;
					}
				}
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/updatecostum",
					{
						params : paramsToSave,
						olderData: olderData
					},
					function(data){
						toastr.success(tradCms.updateConfig);
						window.location.reload();
					},
					null, 
					"json"		
				);
			},
			getImageDocument: function(id , type, subkey, contentKey = null){
				costumizer.valueImage="";
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/getimagedocument",
					{
						id: id,
						type: type,
						subkey: subkey,
						contentKey : contentKey
					},
					function(data){
						costumizer.valueImage = data;
					},
					null,
					"json",
					{
						async: false
					}

				);

			}, 
			deleteDocument : function(id) {
				var urlToSend = baseUrl+"/co2/document/deletedocumentbyid/slug/"+costumizer.obj.contextSlug+"/id/"+id;
				ajaxPost(
					null,
					urlToSend,
					null,
					function(data){}
				);
			},
			findKeyPathObject: function(obj, targetKey, currentPath = "") {
				let keyPathArray = [];
				for (const key in obj) {
					if (obj.hasOwnProperty(key)) {
						const newPath = currentPath ? `${currentPath}.${key}` : key;
						if (key === targetKey) {
							keyPathArray.push(currentPath);
						}
						if (typeof obj[key] === 'object') {
							const result = costumizer.generalInfos.actions.findKeyPathObject(obj[key], targetKey, newPath);
							keyPathArray = keyPathArray.concat(result);
						}
					}
				}
				return keyPathArray;
			}
		},
		events :{ 
			bind : function(){
				var listPageIntoJSON = {};
				var checkOpenAI = false;
				$.each(costumizer.obj.app, function(k, v){
					if(typeof v.lbhAnchor == "undefined" || (typeof v.lbhAnchor != "undefined" && (v.lbhAnchor == false || v.lbhAnchor == "false"))) {
						if (typeof v.name != "undefined" && typeof v.name == "object" && notEmpty(v.name)) {
							listPageIntoJSON[k.replace("#", "")] = (typeof v.name[costum.langCostumActive] != "undefined") ? v.name[costum.langCostumActive] : v.name[Object.keys(v.name)[0]];
						} else {
							listPageIntoJSON[k.replace("#", "")] = k.replace("#", "");
						}
					}
				}); //redirection page
				

				if(typeof costumizer.obj["favicon"] != "undefined" && costumizer.obj["favicon"] == "" && typeof costumizer.obj["faviconSourceImage"] == "undefined"){
					costumizer.generalInfos.actions.getImageDocument(costum.contextId, costum.contextType, "favicon", "slider");
					if(typeof costumizer.valueImage != "undefined" && typeof costumizer.valueImage[0] != "undefined" && typeof costumizer.valueImage[0].imagePath != "undefined"){
						costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.valueImage[0].imagePath);
					}
					//costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["favicon"])
				}

				if(typeof costumizer.obj["favicon"] != "undefined" && costumizer.obj["favicon"] != "" && ((typeof costumizer.obj["faviconSourceImage"] == "undefined") || (typeof costumizer.obj["faviconSourceImage"] != "undefined" && costumizer.obj["faviconSourceImage"] == ""))){
					costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["favicon"])
				}

				if(typeof costumizer.obj["faviconSourceImage"] != "undefined" && costumizer.obj["faviconSourceImage"] != ""){
					costumizer.generalInfos.actions.initImageByPath("faviconSourceImage", costumizer.obj["faviconSourceImage"])
				}

				if(typeof costumizer.obj["logo"] != "undefined" && costumizer.obj["logo"] != ""){
					costumizer.generalInfos.actions.initImageByPath("logo", costumizer.obj["logo"])
				}

				if(typeof costumizer.obj["logoMin"] != "undefined" && costumizer.obj["logoMin"] != ""){
					costumizer.generalInfos.actions.initImageByPath("logoMin", costumizer.obj["logoMin"])
				}

				if(typeof costumizer.obj["metaImg"] != "undefined" && costumizer.obj["metaImg"] != ""){
					costumizer.generalInfos.actions.initImageByPath("metaImg", costumizer.obj["metaImg"])
				}

				if(typeof costumizer.obj["loaderImg"] != "undefined" && costumizer.obj["loaderImg"] != ""){
					costumizer.generalInfos.actions.initImageByPath("loaderImg", costumizer.obj["loaderImg"])
				}
				
				var defaultValues = costumizer.obj;
				
				new CoInput({
					container : "#costum-info",
					inputs :[
						{
							type : "inputSimple",
							options: {
								name : "title",
								label : tradCms.title,
								defaultValue : (typeof defaultValues["title"] != "undefined") ? defaultValues["title"]:"",
								icon : "chevron-down",
							}
						},
						{
							type : "inputSimple",
							options: {
								name : "adminEmail",
								label :  tradCms.emailAdmin,
								defaultValue : (typeof defaultValues["admin"] != "undefined" && typeof defaultValues["admin"]["email"] !="undefined")? defaultValues["admin"]["email"]:"",
								icon : "chevron-down",
								payload : {
									path : "admin.email",
								}
							},
						},
						{
							type : "textarea",
							options: {
								name : "description",
								label : tradCms.shortDescription,
								defaultValue :(typeof defaultValues["description"] != "undefined") ? defaultValues["description"]:"",
								icon : "chevron-down",
								/* rows : 1 */
							},
						},
						{
							type : "inputSimple",
							options: {
								name : "metaTitle",
								label : tradCms.metaTitle,
								defaultValue : (typeof defaultValues["metaTitle"] != "undefined") ? defaultValues["metaTitle"]:"",
								icon : "chevron-down",
							},
						},
						{
							type : "tags",
							options : {
								name : "tags",
								label : tradDynForm.keyWords,
								options : [""],
								defaultValue : (typeof defaultValues["tags"] != "undefined") ? defaultValues["tags"]:"",
								icon : "chevron-down"
							}
						},
						{
							type: "matomoInput",
							options: {
								name : "matomo",
								label: "Activer matomo",
								defaultValues : (typeof defaultValues["matomo"] != "undefined") ? defaultValues["matomo"]: ""
							}
						},
						
						{
							
							type : "inputFileImage",
							options : {
								name : "favicon",
								label : tradCms.favicon,
								collection : "documents",
								defaultValue : (typeof defaultValues["faviconSourceImage"] != "undefined" && defaultValues["faviconSourceImage"] != "") ? defaultValues["faviconSourceImage"]: "",
								payload : {
									path : "faviconSourceImage",
								},
								icon : "chevron-down",
								class : "faviconUploader",
								endPoint : "/subKey/favicon",
								domElement : "favicon",		
								filetype : ["jpeg", "jpg", "gif", "png"], 
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "logo",
								label : tradCms.logo,
								collection : "documents",
								defaultValue : (typeof defaultValues["logo"] != "undefined" && defaultValues["logo"] != "") ? defaultValues["logo"]: "",
								payload : {
									path : "logo",
								},
								icon : "chevron-down",
								class : "logoUploader",
								endPoint : "/subKey/logo",
								domElement : "logo",		
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "logoMin",
								label : tradCms.logoMin,
								collection : "documents",
								defaultValue : (typeof defaultValues["logoMin"] != "undefined" && defaultValues["logoMin"] != "") ? defaultValues["logoMin"]: "",
								payload : {
									path : "logoMin",
								},
								icon : "chevron-down",
								class : "logoMinUploader",
								endPoint : "/subKey/logoMin",
								domElement : "logoMin",		
							}
						},
						{
							
							type : "inputFileImage",
							options : {
								name : "metaImage",
								label : tradCms.metaImage,
								collection : "documents",
								defaultValue : (typeof defaultValues["metaImg"] != "undefined" && defaultValues["metaImg"] != "") ? defaultValues["metaImg"]: "",
								payload : {
									path : "metaImg",
								},
								icon : "chevron-down",
								class : "metaImageUploader",
								endPoint : "/subKey/metaImg",
								domElement : "metaImg",		
							}
						},
						{

							type : "inputFileImage",
							options : {
								name : "loaderImg",
								label : "Image loader",
								collection : "documents",
								defaultValue : (typeof defaultValues["loaderImg"] != "undefined" && defaultValues["loaderImg"] != "") ? defaultValues["loaderImg"]: "",
								payload : {
									path : "loaderImg",
								},
								icon : "chevron-down",
								class : "loaderImgUploader",
								endPoint : "/subKey/loaderImg",
								domElement : "loaderImg",
							}
						},
					],
					onchange:function(name, value, payload){
						var finalPath = costumizer.obj;
						var path = `${payload.path}`;

						// if(value.indexOf("/images/thumbnail-default.jpg") === -1){
						if(exists(payload.path)){
							jsonHelper.setValueByPath(finalPath, path, value);
							costumizer.actions.change(path);
						}
						else{
							jsonHelper.setValueByPath(costumizer.obj, name, value);
							costumizer.actions.change(name);
						}
						// }
						

						if(name == "logo" ){ //Emplacement du logo
							$(".image-menu").attr("src", costumizer.obj.logo);
							$(".logo-image-infoG").attr("src", costumizer.obj.logo);
							
						}

						// if(name == "favicon"){ //Pour vider value favicon
						// 	jsonHelper.setValueByPath(costumizer.obj, "favicon", "");
						// 	costumizer.actions.change("favicon");
						// 	$("link[rel='shortcut icon']").attr("href", value);
						// }

						if(($.inArray(name, ["favicon", "logo", "logoMin", "metaImage", "loaderImg"]) >= 0) && value.indexOf("/images/thumbnail-default.jpg") !== -1) {
			
							costumizer.generalInfos.actions.getImageDocument(costum.contextId, costum.contextType, name, "slider");
							for(var i = 0; i < costumizer.valueImage.length; i++){
								costumizer.generalInfos.actions.deleteDocument(costumizer.valueImage[i].id);
							}

							jsonHelper.setValueByPath(costumizer.obj, name, "");
							costumizer.actions.change(name);

							if(name == "favicon")
								jsonHelper.setValueByPath(costumizer.obj, "faviconSourceImage", "");
							
							if(name == "logo" )
								$(".image-menu").attr("src", costumizer.obj.logo);
						}
					}
				});

				
	
				new CoInput({
					container : "#costum-other-info",
					inputs:[
						{
							type: "select",
							options: {
								name: "language",
								label: tradCms.language,
								isSelect2: true,
								options: $.map(themeParams.DeeplLanguages, function (value, key) {
									return {
										label: tradCms[value["label"]],
										value: key.toLowerCase()
									};
								}),
								icon : "language",
								defaultValue : (typeof costum.language != "undefined") ? costum.language : ""
							},
						},
						{
							type: "selectMultiple",
							options: {
								name: "otherLanguages",
								label: "Autre language",
								options: $.map(themeParams.DeeplLanguages, function (value, key) {
									if (key.toLowerCase() !== costum.language) {
										return {
											label: tradCms[value["label"]],
											value: key.toLowerCase()
										}
									}
								}),
								icon : "globe",
								defaultValue : (typeof costum.otherLanguages != "undefined") ? costum.otherLanguages : []
							}
						},
						{
							type : "select",
							options : {
								name : "logged",
								label : tradCms.redirectionHomePageLogged,
								options : Object.keys(listPageIntoJSON).map(function(value) {
									return {
										label: listPageIntoJSON[value],
										value: value
									}
								}),
								icon : "home",
								payload : {
									path : `htmlConstruct.redirect.logged`,
								},
								defaultValue : (typeof defaultValues["htmlConstruct"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"]["logged"] != "undefined" && typeof costumizer.obj.app["#"+defaultValues["htmlConstruct"]["redirect"]["logged"]] != "undefined") ? defaultValues["htmlConstruct"]["redirect"]["logged"]: "",
							}
						},
						{
							type : "select",
							options : {
								name : "unlogged",
								label : tradCms.redirectionHomePageUnlogged,
								options : Object.keys(listPageIntoJSON).map(function(value) {
									return {
										label : listPageIntoJSON[value] ,
										value : value
									}
								}),
								icon : "home",
								payload : {
									path : `htmlConstruct.redirect.unlogged`,
								},
								defaultValue : (typeof defaultValues["htmlConstruct"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"] != "undefined" && typeof defaultValues["htmlConstruct"]["redirect"]["unlogged"] != "undefined" && typeof costumizer.obj.app["#"+defaultValues["htmlConstruct"]["redirect"]["unlogged"]] != "undefined") ? defaultValues["htmlConstruct"]["redirect"]["unlogged"]: "",
							}
						}
					],
					onchange:function(name, value, payload){
						var path = (typeof payload != "undefined" && typeof payload.path != "undefined") ? payload.path : name;
						if (name == "language") {
							costumizer.generalInfos.actions.updateLanguageCostum(value);
						} else {
							jsonHelper.setValueByPath(costumizer.obj, path, value);
							costumizer.actions.change(path);
							if (name == "otherLanguages") {
								let pathLanguage = costumizer.generalInfos.actions.findKeyPathObject(costumizer.obj.htmlConstruct, "languages");
								if (notEmpty(pathLanguage)) {
									costumizer.generalInfos.refreshdropdownlanguage = true;
								}
							}
						}
					}
				})

				new CoInput({
					container : "#costum-use-open-ai-input",
					inputs:[
						{
							type : "groupButtons",
							options: {
								name : "useOpenAI",
								label : tradCms.useOpenAIAPI,
								class : "isUseOpenAI",
								options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
								defaultValue : (typeof costum.useOpenAI != "undefined") ? costum.useOpenAI : false
							}
						}
					],
					onchange:function(name, value, payload){
						var path = (typeof payload != "undefined" && typeof payload.path != "undefined") ? payload.path : name;
						if (value == true) {
							$("#open-modal-to-add-or-replace-key").show();
							jsonHelper.setValueByPath(costumizer.obj, path, value);
							if (typeof costum.useOpenAI == "undefined" && checkOpenAI == false && costumizer.generalInfos.isOpenAIKeyEntered == null) 
								costumizer.generalInfos.isOpenAIKeyEntered = false;
						} else {
							if (typeof costum.useOpenAI != "undefined" && checkOpenAI == true) {
								bootbox.confirm({
									message: "<h3 class='text-red' style='margin: 0 0 5px 0'>"+tradCms.warningActionIrreversible+"</h3>"+tradCms.warningDeleteKey+"", 
									buttons: {
										confirm: {
											label: tradCms.yesDelete,
											className: 'btn-danger'
										},
										cancel: {
											label: trad.cancel
										}
									},
									callback: function(result) {
										if (result) {
											ajaxPost(
												null,
												baseUrl+"/"+moduleId+"/cms/savekeyopenai",
												{
													openAIKey: "$unset",
												},
												function(data){
													if (data.result) {
														toastr.success(data.msg);
														$("#open-modal-to-add-or-replace-key").hide();
														jsonHelper.deleteByPath(costumizer.obj, path);
														checkOpenAI = false;
														$("#open-modal-to-add-or-replace-key .open-modal-btn").html("<i class='fa fa-key' aria-hidden='true'></i> "+tradCms.addAPIkey+"").attr("data-action", "add");
													} else {
														toastr.error(tradCms.somethingWrong);
													}
												},
												"json"
											);
										} else {
											$("#costum-use-open-ai-input").find(".isUseOpenAI button[data-value='true']").trigger("click");
											jsonHelper.setValueByPath(costumizer.obj, path, value);
										}
									}
								})
							} else {
								$("#open-modal-to-add-or-replace-key").hide();
								jsonHelper.deleteByPath(costumizer.obj, path);
							}
						}	
						costumizer.actions.change(path);
					}
				})

				if (typeof costum.useOpenAI == "undefined") {
					$("#open-modal-to-add-or-replace-key").hide();
				}
				$('#costum-info #btn-submit-form').hide();
				$('#costum-info .mainDynFormCloseBtn').hide();
				costumizer.versionning.helpers.getVersion();
				costumizer.generalInfos.events.openModalOpenAI();
				costumizer.generalInfos.events.addorReplaceKeyOpenAIModal();
				costumizer.checkKeyOpenAI(function(error, response) {
					if (notNull(response)) {
						checkOpenAI = response;
						if (checkOpenAI)
							$("#open-modal-to-add-or-replace-key .open-modal-btn").html("<i class='fa fa-key' aria-hidden='true'></i> "+tradCms.changeAPIkey+"").attr("data-action", "edit");
						else
							$("#open-modal-to-add-or-replace-key .open-modal-btn").html("<i class='fa fa-key' aria-hidden='true'></i> "+tradCms.addAPIkey+"").attr("data-action", "add");
					}
				});
			},
			openModalOpenAI: function() {
				let html = `
					<div class="modal-help-create-key-open-ai">
						<h2 class="title-modal">${tradCms.stepToCreateKeyOpenAI}</h2>
						<ul class="help-list">
							<li class="help-item">
								<h3><i class="fa fa-check"></i><strong>${tradCms.step} 1 :</strong>${tradCms.registrationorlogin}</h3>
								<p>${tradCms.stepOneCreateKeyOpenAI}</p>
							</li>
							<li class="help-item">
								<h3><i class="fa fa-check"></i><strong>${tradCms.step} 2 :</strong>${tradCms.gotoApiSection}</h3>
								<p>${tradCms.stepTwoCreateKeyOpenAI}</p>
							</li>
							<li class="help-item">
								<h3><i class="fa fa-check"></i><strong>${tradCms.step} 3 :</strong>${tradCms.createnewApiKey}</h3>
								<p>${tradCms.stepThreeCreateKeyOpenAI}</p>
							</li>
							<li class="help-item">
								<h3><i class="fa fa-check"></i><strong>${tradCms.step} 4 :</strong>${tradCms.configureBilling}</h3>
								<p>${tradCms.stepFourCreateKeyOpenAIOne} :</p>
								<ol>
									<li>${tradCms.stepFourCreateKeyOpenAITwo}</li>
									<li>${tradCms.stepFourCreateKeyOpenAIThree}.</li>
								</ol>
								<p>${tradCms.stepFourCreateKeyOpenAIFour} : <b class='blue-text'>gpt-3.5-turbo</b>, <b class='blue-text'>DALLE 3</b></p>
							</li>
							<li class="help-item">
								<h3><i class="fa fa-check"></i><strong>${tradCms.step} 5 :</strong>${tradCms.configuringAPI}</h3>
								<p>${tradCms.stepFiveCreateKeyOpenAIOne} :</p>
								<ol>
									<li>${tradCms.stepFiveCreateKeyOpenAITwo}</li>
									<li>${tradCms.stepFiveCreateKeyOpenAIThree}</li>
									<li>${tradCms.stepFiveCreateKeyOpenAIFour}</li>
									<li>${tradCms.stepFiveCreateKeyOpenAIFive}</li>
								</ol>
								<p>${tradCms.stepFiveCreateKeyOpenAISix}</p>
							</li>
						</ul>
						<p class="more-info-open-ai">${tradCms.moreInfoCreateKeyOpenAI}</p>
					</div>
				`;
				$("#costum-use-open-ai .open-ai-modal-information").click(function() {
					bootbox.dialog({
						message: html,
						title: tradCms.createKeyOpenAI,
						show: true,
						backdrop: true,
						animate: true,
						size: 'large',
						buttons: {
							"addKey": {
								label: tradCms.addKey,
								className: "btn-primary",
								callback: function() {
									if (typeof costum.useOpenAI == "undefined") {
										$(".bootbox.modal").hide();
										$("#open-modal-to-add-or-replace-key .open-modal-btn").click();
									} else {
										toastr.info(tradCms.haveKeySaved);
									}
								}
							}
						}
					});
				})
			},
			addorReplaceKeyOpenAIModal: function() {
				$("#open-modal-to-add-or-replace-key .open-modal-btn").click(function() {
					bootbox.dialog({
						title: $(this).text(),
						className: "save-key-openai-modal",
						message: "<div id='open-ai-input-content'></div>",
						buttons: {
							cancel: {
								label: trad.cancel,
								className: "btn-secondary"
							}
						}	
					});
					new CoInput({
						container : "#open-ai-input-content",
						inputs:[
							{
								type : "inputSimple",
								options: {
									name : "openAIKey",
									label : tradCms.apiKey
								}
							}
						],
						onchange:function(name, value, payload){
							if (value != "") 
								$("#open-ai-input-content").find(".btn-save-key-content .btn-save-key").removeClass("disabled");
							else
								$("#open-ai-input-content").find(".btn-save-key-content .btn-save-key").addClass("disabled");
						}
					});
					$("#open-ai-input-content").append(`<div class='btn-save-key-content'><button class='btn-save-key btn btn-success disabled'>${trad.save}</button></div>`);
					costumizer.generalInfos.events.savekeyOpenAI();
				})
			},
			savekeyOpenAI: function() {
				$("#open-ai-input-content").find(".btn-save-key-content .btn-save-key").click(function(e) {
					e.stopPropagation();
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/cms/savekeyopenai",
						{
							openAIKey: $("#open-ai-input-content").find("input[name='openAIKey']").val(),
						},
						function(data){
							if (data.result) {
								toastr.success(data.msg);
								$(".save-key-openai-modal").modal("hide");
								costumizer.generalInfos.isOpenAIKeyEntered = true;
								$("#costum-use-open-ai-input").find(".isUseOpenAI button[data-value='true']").trigger("click");
								$("#open-modal-to-add-or-replace-key .open-modal-btn").html("<i class='fa fa-key' aria-hidden='true'></i> "+tradCms.changeAPIkey+"").attr("data-action", "edit");
							} else {
								toastr.error(tradCms.somethingWrong);
							}
						},
						"json"
					);
				}) 
			}
		}
		
	},
	loader : {
		options: {
			loading_modal : {
				view : "loading_modal",
				name : tradCms.doubleRing
			},
			loader_1 : {
				view : "loader_1",
				name : tradCms.fourBlock
			},
			loader_2 : {
				view : "loader_2",
				name : tradCms.dyingLight
			},
			loader_3 : {
				view : "loader_3",
				name : tradCms.horizontalBar
			},
			loader_4 : {
				view : "loader_4",
				name : tradCms.newtonsCradle
			},
			loader_5 : {
				view : "loader_5",
				name : tradCms.square
			},
			loader_6 : {
				view : "loader_6",
				name : tradCms.circle
			},
			loader_7 : {
				view : "loader_7",
				name : tradCms.spanne
			},
			loader_8 : {
				view : "loader_8",
				name : tradCms.ball
			},
			loader_9: {
				view : "loader_9",
				name : tradCms.color
			}

		},
		views : {
			init : function(){
				var bgLoader = costumizer.obj.css.loader.background;
				$('style#cssLoader').html("");
				var  str=
					'<div class="col-xs-12 col-lg-10 col-lg-offset-1 col-md-12 col-sm-12">';
						$.each(costumizer.loader.options, function(e, v){
						str+=`<div class="card_loader col-md-4 col-sm-6 col-xs-12 ${ typeof costumizer.obj.css.loader.loaderUrl &&  costumizer.obj.css.loader.loaderUrl == v.view || typeof costumizer.obj.css.loader.loaderUrl == "undefined" && v.view == "loading_modal" ? "active" : "" }" >
								<div class="loader_section">
									<div class="box-loader"  style="background-color: ${bgLoader};">
										<div class="loader-position">
											${costumizer.loader.views.typeLoader(v.view)}
										</div>
									</div>
									<div class="box-content">
										<h3 class="title">${v.name}</h3>
										${ typeof costumizer.obj.css.loader.loaderUrl &&  costumizer.obj.css.loader.loaderUrl == v.view || typeof costumizer.obj.css.loader.loaderUrl == "undefined" && v.view == "loading_modal" ? '<span class="post">'+tradCms.currentLoader+'</span>' : '<a href="javascript:;" class="activeLoader" data-name="'+v.view+'"><span class="post">'+trad.activate+'</span></a>' }
									</div>
								</div>
							</div>`;
						});
				str+=`</div>
					`;


				return  str;
			},
			typeLoader : function(name){
				var objLoader = costumizer.obj.css.loader;
				var ring1 = objLoader.ring1; var ring2 = objLoader.ring2;
				var loaderImg = (typeof costumizer.obj.loaderImg != "undefined" && costumizer.obj.loaderImg != "") ? costumizer.obj.loaderImg : costumizer.obj.logo
				if (cmsConstructor.helpers.isValidColor(ring1.color)) {
					ring1.color = ring1.color
				}else{
					ring1.color = color1
				}
				if (cmsConstructor.helpers.isValidColor(ring2.color)) {
					ring2.color = ring2.color
				}else{
					ring2.color = color2
				}
				var loading_modal =
					'<div class="processingLoader">'+
					'<center>'+
					'<div class="lds-css ng-scope">'+
					'<div style="width:100%; height:100%;" class="lds-doubl-ring">'+
					'<div id="ring1" style="border-color: transparent '+ring1.color+'; "></div>'+
					'<div id="ring2" style="border-color: transparent '+ring2.color+';"></div>'+
					'<img src="'+loaderImg+'">'+
					'</div>'+
					'</div>'+
					'</center>'+
					'</div>';
				var loader_1 =
					'<div class="loader_1">' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'    <span style="background-color: '+ring1.color+';"></span>' +
					'</div>';

				var loader_2 =
					'<div class="dl">' +
					'  <div class="dl_container">' +
					'    <div class="dl_corner--top" style="color:'+ring1.color+';"></div>' +
					'    <div class="dl_corner--bottom" style="color:'+ring2.color+';"></div>' +
					'  </div>' +
					'  <div class="dl_square"><img class="dl_img" src="'+loaderImg+'"></div>' +
					'</div>';
				var loader_3 =
					'<div class="horizontal_bar">' +
					'    <div class="horizontal_bar_logo">' +
					'        <img src="'+loaderImg+'">' +
					'    </div>' +
					'    <div class="horizontal_bar_loader" style="background-color:'+ring2.color+';height:'+ring2.borderWidth+';">' +
					'        <div class="horizontal_bar_loading" style="background-color:'+ring1.color+';height:'+ring1.borderWidth+';"></div>' +
					'    </div>' +
					'</div>';
				var loader_4 =
					'<img class="newtons-cradle-img" src="'+loaderImg+'">'+
					'<div class="newtons-cradle">'+
						'<div style="background-color: '+ring1.color+';"></div>'+
						'<div style="background-color: '+ring1.color+';"></div>'+
						'<div style="background-color: '+ring2.color+';"></div>'+
						'<div style="background-color: '+ring2.color+';"></div>'+
					'</div>';
				var loader_5 =
					'<div class="loader-square">' +
					'    <img src="'+loaderImg+'">' +
					'</div>';
				var loader_6 =
					'<div class="loader-circle-2" style="border: '+ring1.borderWidth+' solid '+ring1.color+';">' +
					'    <img src="'+loaderImg+'">' +
					'</div>';
				var loader_7 =
					'<div class="loader-spanne">' +
					'    <div class="loader-spanne-img">' +
					'        <img src="'+loaderImg+'">' +
					'    </div>' +
					'    <span style="height: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="width: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="height: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'    <span style="width: '+ring1.borderWidth+';background: '+ring1.color+';"></span>' +
					'</div>';
				var loader_8 =
					'<img class="loader-ball-img" src="'+loaderImg+'">' +
					'<div class="loader-ball">' +
					'<div class="dot1" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot2" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot3" style="background-color: '+ring1.color+';"></div>' +
					'<div class="dot4" style="background-color: '+ring1.color+';"></div>' +
					'</div>';
				var loader_9 = `
						<div id="ct-loadding" class="ct-loader style13" style="/* display: none; */">
                            <div class="loading-infinity">
								<div>
									<span></span>
								</div>
								<div>
									<span></span>
								</div>
								<div>
									<span></span>
								</div>
							</div>
                        </div>`;
				if (name == "loader_1") {
					$('style#cssLoader').append('@keyframes loaderBlock {0%, 30% {transform: rotate(0);}55% {background-color: '+ring2.color+';}100% {transform: rotate(360deg);}}@keyframes loaderBlockInverse {0%, 20% {transform: rotate(0);}55% {background-color: '+ring2.color+';}100% {transform: rotate(-360deg);}}');
				} else if (name == "loader_5") {
					$('style#cssLoader').append('.loader-square:before{ border: solid '+ring1.borderWidth+' '+ring1.color+'; } .loader-square:after{ border: solid '+ring2.borderWidth+' '+ring2.color+'; }');
				} else if (name == "loader_6") {
					$('style#cssLoader').append('.loader-circle-2::before {background-color: '+ring2.color+';}');
				} else if (name == 'loader_7') {
					$('style#cssLoader').append('@keyframes spanMoveAnimation { 0% {transform: scale(1);margin: 0;} 20% {transform: scale(1);margin: 0;} 60% {transform: scale(0.5);margin: 20px;background: '+ring2.color+';}  100% {  transform: scale(1);  margin: 0;  }  }');
				}else if (name == "loader_8") {
					$('style#cssLoader').append('.loader-ball div::after {background-color: '+ring2.color+';}');
				}else if (name == "loader_9") {
					$('style#cssLoader').append(`

						.loading-infinity div span {
							background: ${ring1.color};
						}

						@keyframes ctLoadingmove {
							0%, 50% {
								left: -10px;
							}
							25% {
								background:${ring2.color};
							}
							75% {
								background: #85cc02;
							}
							50.0001%, 100% {
								left: auto;
								right: -10px;
							}
						}
					`);
				}

				return eval(name);
			}
		},
		events : {
			bind: function () {
				$(".activeLoader").off().on("click", function (e) {
					valueLoader = $(this).data("name");
					var finalPath = costumizer.obj;
					var path = "css.loader.loaderUrl";

					jsonHelper.setValueByPath(costumizer.obj, path, valueLoader);
					costumizer.actions.change(path);
				});
			}
		}
	},
	pages : {
		valueForNamePage : {},
		callbackPage : false,
		views : {
			init : function(){
				var str="";
				if(!notEmpty(costumizer.obj.app)){
					str=`<div><span>`+tradCms.emptyCostum+`</span></div>
							<button class="btn-primary create-page btn costumizer-button-add"><i class="fa fa-plus"></i> `+tradCms.createNewPage+`</button>`;
				}else{
					str=`<div class='pages-toolbar'>
							<button class="btn create-page btn costumizer-button-add"><i class="fa fa-plus"></i> `+tradCms.createNewPage+`</button>
							<button class="delete-page multiple-delete-page disabled btn costumizer-button-delete" data-id="delete_all"><i class="fa fa-trash"></i> `+trad.delete+`</button>
						</div>`;
					str+=`<table class="tabling-pages table  table-hover col-xs-12">
					            <thead>
					                <tr>
					                    <th></th>
					                    <th class='col title'>`+tradCms.pageName+`</th>
					                    <th class='col key-app'>`+tradCms.keyslug+`</th>
					                    <th class='col key-app'>`+trad.type+`</th>
										<th class='col key-app'>`+tradCms.visibility+`</th>
										<th class='col key-app'>`+tradCms.status+`</th>
					                    <th class='col toolsEdit'>`+trad.actions+`</th>
					                </tr>
					            </thead>
					            <tbody>`;
								if(Array.isArray(costumizer.obj.app)){
									costumizer.obj.app = Object.assign({}, costumizer.obj.app);
								}
								$.each(costumizer.obj.app, function(k, e){
					str+=			costumizer.pages.views.linePage(k,e,costum.langCostumActive);
								});
					str+=		`</tbody>
					   	 	</table>`;
				}
				return str;
			},
			allLanguageSelector: function(keyPage, pageName, allPage) {
				var btnLangView = "",
					allLanguagesSelectedInDb = dataHelper.getAllLanguageInDb(),
					allLanguages = {};

				for (var i = 0; i < allLanguagesSelectedInDb.length; i++) {
					var keyLang = allLanguagesSelectedInDb[i].toUpperCase();
					if (themeParams.DeeplLanguages.hasOwnProperty(keyLang) && !allPage.hasOwnProperty(allLanguagesSelectedInDb[i])) {
						allLanguages[keyLang] = themeParams.DeeplLanguages[keyLang];
					}
				}
				if (notEmpty(allLanguages)) {
					$.each(allLanguages, function(key, value) {
						btnLangView += `<button class="btn dropdown-item btnLang" type="button" data-key="${keyPage}" data-lang="${key.toLowerCase()}" data-page="${pageName}"><span style="margin-right: 5px">${value["icon"]}</span> ${tradCms[value["label"]]}</button>`;
					})
				}
				return btnLangView;
			},
			linePage : function(k,v,costumLang="fr"){
				var htmlStr ="";
				var forAll = ["green", tradCms.true];
				var status = ["green", tradCms.published];
				var btnPublie = " <a href='javascript:;' class='brouillon-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.draftPage+"</a>";
				var typePage="pageCMS";
				var defaultValueRestricted = "all";
				var uuid = costumizer.generateUUID();
				var pageName = k.replace("#", "");
				if(typeof v["name"] !='undefined') {
					if (typeof v["name"] === "object" && notEmpty(v["name"]))
						pageName = (typeof v["name"][costumLang] != "undefined") ? v["name"][costumLang] : v["name"][Object.keys(v["name"])[0]];
					else if (typeof v["name"] === "string" && v["name"] != "")
						pageName = v["name"];
				}
				if(v != null){
		
					if((typeof v.appTypeName != "undefined" && v.appTypeName != "") || typeof v.useFilter != "undefined"){
						typePage="advancedApp";
					}

					if(typeof v.lbhAnchor != "undefined" && v.lbhAnchor == true)
						typePage="anchorMenu";

					if(typeof v.target != "undefined" && v.target == true && typeof v.urlExtern != "undefined")
						typePage="externalLink";

					if(typeof v.hash != "undefined" && v.hash == "#app.aap")
						typePage="aap";

					if(typeof v.restricted != "undefined" && v.restricted != null){
						if(typeof v.restricted.admins != "undefined"){
							if( v.restricted.admins == true || v.restricted.admins == "true"){
								defaultValueRestricted = "admins";
								forAll = ["red", tradCms.visibleToAdminOnly];
							}
						}

						if(typeof v.restricted.members != "undefined"){
							if( v.restricted.members == true || v.restricted.members == "true"){
								defaultValueRestricted = "members";
							}
						}

						if(typeof v.restricted.draft != "undefined" && v.restricted.draft == true){
							status = ["red", tradCms.draft];
							btnPublie = " <a href='javascript:;' class='publie-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.publishPage+"</a>";
						}
					}
					
					htmlStr += `<tr id='keyPages-${k}' class='pages-line'>
								<th class='col checkbox-tools'>`;
								if (k!="#welcome")
									htmlStr +=	"<input type='checkbox' class='selectPages' value='"+k+"'/>" ;
					htmlStr +=	"</th>"+
								"<td  data-key='"+k+"'' class='col title editPage'><span class=' tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>";
								if(typeof v["name"] !='undefined' && typeof v["name"] === "object" && notEmpty(v["name"])) {
									if (typeof v["name"][costumLang] != "undefined") {
										htmlStr += "<span style='font-size: 20px; margin-right: 10px'>"+themeParams.DeeplLanguages[costumLang.toUpperCase()]["icon"]+"</span>";
									}
								}
								htmlStr += pageName;
								htmlStr += "</span></td>"+
											"<td  data-key='"+k+"'' class='col key-app' >" ;
								if(typePage != "anchorMenu") {
									htmlStr += "<a href='" + ((typePage == "externalLink") ? "" + v.urlExtern + "" : "" + k + "") + "' class='lbh-costumizer-edit-page tooltips btn btn-default' data-type='" + typePage + "'>" + k + "</a>";
								}else {
									htmlStr+= "<span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+ k +"</span>";
								}
								htmlStr +=	"</td>"+
											"<td  data-key='"+k+"'' class='col key-app editPage' ><span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+tradCms[typePage]+"</span></td>";
								
								
								htmlStr += "<td  class='col key-app'><span class='tooltips select-visibility-value-content'>"+
									"<select data-key='"+k+"'' class='form-control select-visibility-value' style='background-color: transparent;border: none;border-bottom: 2px solid #ccc;'>";
									htmlStr +=  (defaultValueRestricted == "all") ? "<option value='all' selected='selected'>"+tradCms.forAll+"</option>" : "<option value='all'>"+tradCms.forAll+"</option>";
									htmlStr +=  (defaultValueRestricted == "admins") ? "<option value='admins'  selected='selected'>"+tradCms.forAdmin+"</option>": "<option value='admins'>"+tradCms.forAdmin+"</option>";
									htmlStr +=  (defaultValueRestricted == "members") ? "<option value='members' selected='selected'>"+tradCms.forMembers+"</option>" : "<option value='members'>"+tradCms.forMembers+"</option>";
									htmlStr +=  "</select></span></td>" ;

								htmlStr += "<td  data-key='"+k+"'' class='col key-app' style='color:"+status[0]+"'><span class=' tooltips' data-toggle='tooltip'>"+status[1]+"</span></td>" ;
								htmlStr+= "<td class='col toolsEdit'>"
								htmlStr+= "<div class='dropdown dropdown-menu-action-page'>"+
												"<button class='btn btn-primary dropdown-toggle' type='button' id='"+k+"' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"+
												"<i class='fa fa-cog'></i>"+
												"</button>"+
												"<ul class='dropdown-menu pull-right' aria-labelledby='"+k+"'>";
												if(typePage != "anchorMenu"){
													htmlStr+= "<li><a href='"+((typePage == "externalLink") ? ""+v.urlExtern+"" : ""+k+"")+"' class='lbh-costumizer-edit-page tooltips' data-type='"+typePage+"'><i class='fa fa-eye'></i> "+tradCms.showPage+"</a></li>";
													if (typePage == "pageCMS")
														htmlStr+= "<li class='new-feature'><a href='javascript:costumizer.template.views.initAjax({space:`template`,key:`page`,page:`"+k+"`});' class='tooltips'><i class='fa fa-star'></i> "+tradCms.getTemplatePage+"</a></li>";
												}
								htmlStr+=			"<li><a href='javascript:;' class='editPage ' data-key='"+k+"''><i class='fa fa-pencil '></i> "+trad.edit+"</a></li>";
													if(typePage != "anchorMenu")
								htmlStr+=			"<li><a href='javascript:;' class='duplicate-page tooltips' data-key='"+k+"''><i class='fa fa-copy'></i> "+tradCms.duplicate+"</a></li>";
													if (k!="#welcome" && typePage != "externalLink"){
								htmlStr+= 				"<li>"+btnPublie+"</li>";
													}
													if (v.hash == "#app.search"){
								htmlStr+= 				"<li><a class='tooltips' href=javascript:costumizer.actions.configFilterAnswers(`"+k+"`)><i class='fa fa-filter'></i> Configurer le filtre par réponse</a></li>";
													}
													if (k!="#welcome" && typePage != "anchorMenu"){
								htmlStr+=				"<li><a href='javascript:;' class='delete-page text-red' data-id='delete_one' id='"+k+"' ><i class='fa fa-trash '></i> "+trad.delete+"</a></li>";
													}
													if (typeof v["name"] !='undefined' && typeof v["name"] === "object" && notEmpty(v["name"]) && typeof v["name"][costumLang] == "undefined" && k!="#welcome" && dataHelper.getAllLanguageInDb().length > 1) {
								htmlStr += 				`<li class="btn-collapse-translate-content new-feature">
															<a role="button" class="btn btn-collapse-translate" type="button" data-toggle="collapse" data-target="#collapseTranslate${uuid}" aria-expanded="false" aria-controls="collapseTranslate${uuid}">
																<span><i class="fa fa-language"></i>${tradCms.translate}</span><i class="fa fa-chevron-down"></i>
															</a>
															<div class="collapse all-btn-language" id="collapseTranslate${uuid}">
																${costumizer.pages.views.allLanguageSelector(k, pageName, v["name"])}
															</div>
														</li>`;
													}
												"</ul>"+
											"</div>";
								htmlStr+= "</td>"+
										"</tr>";

				}
							
				return htmlStr;
			},
			alertBoxContain : function (title, body, confirmation) {
				var htmlStr = "";
				htmlStr += '<div class="alert_box">'+
					'<div class="icon">'+
					'<img src="'+assetPath+'/cmsBuilder/img/warning.gif" alt="Computer man" style="width: 80px;height: auto;">'+
					'</div>'+
					'<header>'+title+'</header>'+
					'<p>'+body+' !</p>'+
					'<p>'+confirmation+'</p>'+
					'</div>';
					return htmlStr;
			},
			updataPageInfo : function(){
				var costumInfo = ""
				$(".costum-info").addClass("hidden")
				if(typeof costum.app["#"+page] != "undefined"){
					if (typeof costum.app["#"+page].restricted != "undefined") {
						var restrictionInfo = costum.app["#"+page].restricted
						if (typeof restrictionInfo.admins != "undefined" && restrictionInfo.admins == true) {                
							costumInfo = '<i class="fa fa-user-circle padding-right-20" aria-hidden="true"></i> Page '+tradCms.visibleToAdminOnly.toLowerCase()
							$(".costum-info").removeClass("hidden")
						}            
						if (typeof restrictionInfo.members != "undefined" && restrictionInfo.members == true) {                
							costumInfo += '<i class="fa fa-users padding-right-20" aria-hidden="true"></i> Page '+tradCms.visibleMembers.toLowerCase()
							$(".costum-info").removeClass("hidden")
						}
						if (typeof restrictionInfo.draft != "undefined" && restrictionInfo.draft == true) {                
							costumInfo += " ("+tradCms.draft.toLowerCase()+")"
							$(".costum-info").removeClass("hidden")
						}
					}

					if (costum.isPageAlias == true) {                
						costumInfo += `<strong class=" padding-left-20 alias-cms"> <i class="fa fa-link padding-right-5 padding-left-5" aria-hidden="true"></i> <a target="_blank" href="${typeof costum.app["#"+page].linked.url != "undefined"? baseUrl+"/costum/co/index/slug/"+costum.app["#"+page].linked.url+"/#"+costum.app["#"+page].linked.page : ''}"> Alias depuis un autre costum </a><span class='text-white'> // Edition des blocks desactivés</span></strong>`
						$(".costum-info").removeClass("hidden")
					}
				}
				$(".costum-info").html(costumInfo)
			}
		},
		events :{ 
			bind : function(){
				$(".delete-page").off().on("click",function(e){
					costumizer.pages.actions.delete(e);
				});
				
				$(".editPage:not(.element-busy), .create-page").off().on("click",function(){
					costumizer.pages.actions.save($(this).data("key"),costumizer.obj.app[$(this).data("key")]);
					costumizer.focusComponentSocket("elementFocused", "keyPages-"+$(this).data("key"));
					costum.members.focused[userId] = {page : page, elementTarget : "keyPages-"+$(this).data("key")};
					/*if ($(this).data("switch") == "page") {
						$(".lbh-costumizer-edit-page[href='"+$(this).data("key")+"']").trigger( "click" );
					}*/
				});
				$(".duplicate-page").off().on("click", function(){
					costumizer.pages.actions.duplicate($(this).data("key"));
				});

				$(".brouillon-page").off().on("click", function(){
					costumizer.pages.actions.publiePage($(this).data("key"), "brouillon");
				});
				$(".publie-page").off().on("click", function(){
					costumizer.pages.actions.publiePage($(this).data("key"), "publie");
				});

				$(".editPageLeftPanel").off().on("click",function(e){
					e.stopImmediatePropagation();
					costumizer.pages.actions.save($(this).data("key"),costumizer.obj.app[$(this).data("key")]);
					costumizer.focusComponentSocket("elementFocused", "keyPages-"+$(this).data("key"));
					costum.members.focused[userId] = {page : page, elementTarget : "keyPages-"+$(this).data("key")};
					if ($(this).data("switch") == "page") {
						costumizer.forceReload($(this).data("key"));
						urlCtrl.loadByHash($(this).data("key"));
					}
				});
				$(".btn-action-page").click(function() {
					var keyBtn = $(this).data("id");
					setTimeout(function () {
						$("#list-page-container").animate({
							scrollTop:$("#dropdown-page-"+keyBtn+".open .dropdown-menu").offset().top
						},500)
					},400)

                });

				$(".lbh-costumizer-edit-page").off().on("click", function(e){
					e.preventDefault();
					if ($(this).attr("href") != "") {
						if ($(this).data("type") == "externalLink") {
							window.open($(this).attr("href"), '_blank');
						} else {
							costumizer.forceReload($(this).attr("href"));
							urlCtrl.loadByHash($(this).attr("href"));
							costumizer.actions.closeModal();
							//pour le navigation page dans le .cmsbuilder-left-content
							//cmsConstructor.page.events.loadListPage();
						}
					} else {
						toastr.error(tradCms.linkNotFound);
					}
				});
				$(".checkbox-tools .selectPages").off().on("click",function(){
					if($(".checkbox-tools .selectPages:checked").length > 0)
						$(".multiple-delete-page").removeClass("disabled");
					else
						$(".multiple-delete-page").addClass("disabled");
				});

				$(".select-visibility-value").change(function(e) {
					e.stopImmediatePropagation()
					costumizer.pages.actions.visibility($(this).val(), $(this).data("key"));
			 	});

				$(document).on("click", '.btn-collapse-translate', function(event) {
					event.stopPropagation();
					$(this).closest(".dropdown-menu-action-page").addClass("open");	
				});

				$(".all-btn-language button.btnLang").on("click", function(e) {
					costumizer.pages.events.savePageTranslated($(this));
				})
				$("#costum-form-pages").find(".costum-alias input").on("focus" , function(){
					$('.enable-alias .btn-active').click()
				})
			},
			change : function(name, value, payload, keyApp=null, anchorClassHideOrShow="", paramsToSend, urlExternOptions = "", aapClassHideOrShow = "", restrectedAdminClassHideOrShow = "", restrectedMembersClassHideOrShow = "", restrectedRoleClassHideOrShow = ""){
				var languageCostum = costum.langCostumActive;
				var deletelbhAnchor = ["metaDescription", "metaTags", "selectApp", "appTypeName","target"];
				if(typeof $("#costum-form-pages #btn-submit-form").attr("disabled") != "undefined" && $("#costum-form-pages #btn-submit-form").attr("disabled") != false){
					$("#costum-form-pages #btn-submit-form").removeAttr("disabled");
				}
				if(name != "keySlug") {
					if (name == "name") {
						costumizer.pages.valueForNamePage[value["lang"]] = value["text"];
						paramsToSend[name] = costumizer.pages.valueForNamePage;
					} else {
						if(typeof payload != "undefined" && typeof payload.path != "undefined"){
							paramsToSend[payload.path] = value;
						}
						else{
							paramsToSend[name] = value;
						}	
					}
				}	
				if (name === "coevent") {
					var inputsParent = ".lbhAnchor, .target, .enable-note, .enable-alias, .selectApp";
					var inputsChild = ".urlExtern, .select-note, .costum-alias, .appTypeName, .filtersTypes";
					if (value) {
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
						$("#costum-form-pages .event").show();
					}
					else {
						$(`#costum-form-pages ${inputsParent}`).show();
						$("#costum-form-pages .event").hide();
					}
					$("#costum-form-pages [name=event] + .help-block").hide().removeClass('letter-red').text("");
				}
				if (name === "alias") {
					var inputsParent = ".lbhAnchor, .coevent, .target, .enable-note, .selectApp";
					var inputsChild = ".event, .urlExtern, .select-note, .appTypeName, .filtersTypes";
					if (value){
						$("#costum-form-pages .costum-alias").show();
						costumizer.aliasCostumList();
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
						$(".enable-note").hide()						
						localStorage.setItem("reload"+costum.contextId,"#"+$(".keySlug[name='keySlug']").val())
					}else{
						delete paramsToSend["linked"];	
						$(`#costum-form-pages ${inputsParent}`).show();
						$(".costum-alias[name='costumId']").val("")
						$(".costum-alias[name='costumPage']").val("")
						$(".costum-alias[name='costumSlug']").val("")
						$("#costum-form-pages .costum-alias").hide();
						localStorage.removeItem("reload"+costum.contextId)
					}
				}
				if (name === "note") {
					var inputsParent = ".lbhAnchor, .coevent, .target, .enable-alias, .selectApp";
					var inputsChild = ".event, .urlExtern, .costum-alias, .appTypeName, .filtersTypes";
					if (value){
						$("#costum-form-pages .select-note").removeClass("hidden").show();					
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
					}else{
						delete paramsToSend["note"];	
						$(`#costum-form-pages ${inputsParent}`).show();
						$(".enable-note").prop("selected", false);
						$("#costum-form-pages .select-note").addClass("hidden").val("");
					}
				}

				if((name == "name" && notEmpty(value) && value["text"] != "" && value["lang"] == languageCostum && keyApp==null) || name == "keySlug") {
					let slugApp="",
						valueToCheck = "";
					switch (name) {
						case "name":
							valueToCheck = value["text"];
							slugApp = slugify(value["text"]).toLowerCase();
							break;
						case "keySlug":
							valueToCheck = value;
							slugApp = slugify(value).toLowerCase();
							break;
						default:
							break;
					}
					$("#costum-form-pages input[name=keySlug]").val(slugApp);	
					costumizer.pages.actions.checkUniqueApp(valueToCheck, keyApp);
					paramsToSend["keySlug"] = slugApp;
				}

				if(name == "lbhAnchor"){
					var inputsParent = ".coevent, .target, .enable-note, .enable-alias, .selectApp";
					var inputsChild = ".event, .urlExtern, .select-note, .costum-alias, .appTypeName, .filtersTypes";
					if(value == true || value == "true"){		
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
						$.each(deletelbhAnchor, function(kk, val){
							if(typeof paramsToSend[val] != "undefined"){
								delete paramsToSend[val];
							}		
						});
					}
					else{
						$(`#costum-form-pages ${inputsParent}`).show();
					}
				}

				if(name == "aap"){
					if(value == true || value == "true"){
						$(aapClassHideOrShow).hide();
						
						$.each(deletelbhAnchor, function(kk, val){
							if(typeof paramsToSend[val] != "undefined"){
								delete paramsToSend[val];
							}		
						});
					}
					else{
						$(aapClassHideOrShow).show();
						if($("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == "false" || $("#costum-form-pages .selectApp .btn-group .btn-primary").data("value") == false){
							$("#costum-form-pages .appTypeName").hide();
						}
							
						if($("#costum-form-pages .target .btn-group .btn-primary").data("value") == "false" || $("#costum-form-pages .target .btn-group .btn-primary").data("value") == false){
							$("#costum-form-pages .urlExtern").hide();
						}
					}
				}

				if(name == "selectApp"){
					var inputsParent = ".lbhAnchor, .coevent, .target, .enable-note, .enable-alias";
					var inputsChild = ".event, .urlExtern, .select-note, .costum-alias";
					if(value == true || value == "true"){
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
						$("#costum-form-pages .appTypeName").show();
						if ($("#costum-form-pages select[name=appTypeName]").val() == "search") {
							$("#costum-form-pages .filtersTypes").show().removeClass("hidden");
						}
					}
					else{
						$(`#costum-form-pages ${inputsParent}`).show();
						$("#costum-form-pages .appTypeName").hide();
						$("#costum-form-pages .filtersTypes").hide().addClass("hidden");
					}
				}

				if(name == "target"){
					var inputsParent = ".lbhAnchor, .coevent, .enable-note, .enable-alias, .selectApp";
					var inputsChild = ".event, .select-note, .costum-alias, .appTypeName, .filtersTypes";
					if(value == true || value == "true"){
						$(`#costum-form-pages ${inputsParent}, #costum-form-pages ${inputsChild}`).hide();
						$("#costum-form-pages .urlExtern").show();
					}	
					else{
						$(`#costum-form-pages ${inputsParent}`).show();
						$("#costum-form-pages .urlExtern").hide();
					}
				}

				if(name == "members"){
					if(value == true || value == "true")
						$(restrectedMembersClassHideOrShow).hide();
					else
						$(restrectedMembersClassHideOrShow).show();
				}

				if(name == "isAdmin"){
					if(value == true || value == "true")
						$(restrectedAdminClassHideOrShow).hide();
					else
						$(restrectedAdminClassHideOrShow).show();
				}

				if(name == "roles"){
					if(value == "")
						$(restrectedRoleClassHideOrShow).hide();
					else
						$(restrectedRoleClassHideOrShow).show();
				}
				if (name == "appTypeName") {
					if (value == "search")
						$("#costum-form-pages .filtersTypes").show().removeClass("hidden");
					else {
						$("#costum-form-pages .filtersTypes").hide().addClass("hidden");
					}
				}
				if (name == "particleActive") {
					if (value == true || value == "true")
						$("#costum-form-pages .particle-color").show().removeClass("hidden");
					else
						$("#costum-form-pages .particle-color").hide().addClass("hidden");
				}
			},
			savePageTranslated: function(dom) {
				var langToTranslate = dom.data("lang"),
				pageToTranslate = dom.data("page"),
				pageName = dom.closest(".pages-line").find(".title span"),
				parentCollapse = dom.closest(".all-btn-language"),
				keyPage = dom.data("key");
				if (pageToTranslate != "" && langToTranslate != "") {
					new Promise(function(resolve, reject) {
						dataHelper.autoTranslateDeepl(pageToTranslate, langToTranslate, function(error, textTranslated) {
							if (error) {
								reject(error);
							} else {
								jsonHelper.setValueByPath(costumizer.obj.app, keyPage + ".name." + langToTranslate, textTranslated);
								jsonHelper.setValueByPath(themeParams.pages, keyPage + ".name." + langToTranslate, textTranslated);
								jsonHelper.setValueByPath(costum.appConfig.pages, keyPage + ".name." + langToTranslate, textTranslated);
								jsonHelper.setValueByPath(urlCtrl.loadableUrls, keyPage + ".name." + langToTranslate, textTranslated);
								var params = {};
								var olderData = {};
								olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder,"app.");
								params["app"] = costumizer.obj.app;
								ajaxPost(
									null,
									baseUrl+"/"+moduleId+"/cms/updatecostum",
									{
										params: params,
										costumId : costum.contextId,
										costumType : costum.contextType,
										page : pageToTranslate,
										keyPage : keyPage,
										action : "costum/page",
										subAction : "EditPage",
										olderData : olderData,
									},
									function(data){
										costum.app=jQuery.extend(true,{},costumizer.obj.app);
										toastr.success(tradCms.updateConfig);
										let btnAllLanguageSelectorView = costumizer.pages.views.allLanguageSelector(keyPage, textTranslated, jsonHelper.getValueByPath(costumizer.obj.app, keyPage + ".name"));
										if (btnAllLanguageSelectorView != "") {
											parentCollapse.html(btnAllLanguageSelectorView);
											parentCollapse.find("button.btnLang").on("click", function(e) {
												costumizer.pages.events.savePageTranslated($(this));
											})
										} else {
											parentCollapse.parent(".btn-collapse-translate-content").remove();
										}
										pageName.html(`<span style="font-size: 20px; margin-right: 5px">${themeParams.DeeplLanguages[langToTranslate.toUpperCase()]["icon"]}</span> ${textTranslated}`);
									}			
								);
								resolve(textTranslated);
							}
						})
					});
				}
			}
		},
		actions : {
			load: function() {
			},
			callback : function(data){	
				$("#content-btn-delete-page").remove();
				$("#menu-right-costumizer").fadeOut();
				
			},
			callBackDeletePage : function(pages,data) {
				if ( data && data.result ) {
					$.each(pages,function(key,value){
						if (exists(costumizer.obj.app[value])){
							delete costumizer.obj.app[value];
							$(".tabling-pages tr[id='keyPages-"+value+"']").remove();
						}
						if (exists(themeParams.pages[value])){
							delete themeParams.pages[value];
						}
						if (exists(urlCtrl.loadableUrls[value])){
							delete urlCtrl.loadableUrls[value];
						}
						if (exists(costum.appConfig.pages[value])){
							delete costum.appConfig.pages[value];
						}
						if (exists(costum.app[value])) {
							delete costum.app[value];
						}
					});
					if (typeof data.pathToDelete != "undefined" && notEmpty(data.pathToDelete)) {
						if (data.pathToDelete.length > 1) 
							data.pathToDelete.sort((a, b) => b.split(".").length - a.split(".").length);
						Object.values(data.pathToDelete).forEach(function(value) {
							jsonHelper.deleteByPath(costumizer.obj, value);
							costumizer.actions.change(value);
							let newPath = "";
							let objSplice = value.split(".");
							for (let index = 0; index < objSplice.length - 2; index++) {
								if (index == 0)
									newPath = objSplice[index];
								else 
								newPath +="."+objSplice[index];
							}
							let menuParent = objSplice[objSplice.length - 3];
							if (menuParent.charAt(0) == "#") {
								let objNewPath = jsonHelper.getValueByPath(costumizer.obj, newPath+".buttonList");
								if (typeof objNewPath != "undefined" && !notEmpty(objNewPath)) {
									jsonHelper.setValueByPath(costumizer.obj, newPath, true);
									costumizer.actions.change(newPath);
								}
							} else {
								let objNewPath = jsonHelper.getValueByPath(costumizer.obj, newPath+".buttonList");
								if (typeof objNewPath != "undefined" && !notEmpty(objNewPath)) {
									jsonHelper.deleteByPath(costumizer.obj, newPath+".buttonList");
									costumizer.actions.pathToValue = [];
									costumizer.actions.change(newPath);
								}
							}
						})
						var pathValueToUpdate = {};
						$.each(costumizer.actions.pathToValue, function(k,v) {
							var valueToUp = jsonHelper.getValueByPath(costumizer.obj, v);
							pathValueToUpdate[v] = valueToUp;
						})

						costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
						costum.app=jQuery.extend(true,{},costumizer.obj.app);
						costumizer.actions.update();

					}
				} else {
					toastr.error("Something went wrong!! please try again. " + data.msg);
				}
			},
			deletePage : function(pages) {
				bootbox.confirm({
					message: costumizer.pages.views.alertBoxContain(tradBadge.confirm, tradCms.unrecoverablePage, tradCms.confirmDeletePage),
					buttons: {
						confirm: {
							label: tradCms.yesDelete,
							className: 'btn-danger'
						},
						cancel: {
							label: trad.cancel,
							className: 'btn-default'
						}
					},
					callback: function (response) {
						if (response) {
							ajaxPost(
								null,
								baseUrl+"/"+moduleId+"/cms/deletepage",
								{
									costumId : costum.contextId,
									costumType : costum.contextType,
									costumSlug: costum.contextSlug,
									pages: pages
								},
								function(result) {
									costumizer.pages.actions.callBackDeletePage(pages,result);
									costumizer.updateComponentSocket({functionName: "deletePage", data: {result: result, pages: pages}})
									cmsConstructor.page.events.loadListPage();
								}
								);
						}
					}
				});
				
			},
			delete: function(e){
				// Check if there is no one editing the page Before delete,
				var pages = [];
				var pageBusy  = [];
				var pageCheckedCantBeDelete = []

				$('.selectPages:checked').each(function () {
					pages.push($(this).val())
				})

				if (typeof $(e.currentTarget).attr("id") != "undefined") {					
					pages.push($(e.currentTarget).attr("id"));
				}

				for (const key in costum.members.focused) {
					pageBusy.push("#"+costum.members.focused[key].page);
				}

				pageCheckedCantBeDelete = costumizer.actions.getCommonValuesFromArray(pages,pageBusy)

				if (notEmpty(pageCheckedCantBeDelete)) {

					let stringPageCantDelete = "<span class='text-red'>"+pageCheckedCantBeDelete.join(', ')+"</span>";

					pages = pages.filter(value => !pageCheckedCantBeDelete.includes(value));

					bootbox.confirm({
						message: `${pageCheckedCantBeDelete.length > 1 ? "Les pages "+stringPageCantDelete+" sont " : "La page "+stringPageCantDelete+" est "} occupé par les autre utilisateurs.`,
						buttons: {
							confirm: {
								label: pageCheckedCantBeDelete.length > 1 ? "Ignorer ces pages" : "Ignorer ce page",
								className: 'btn-danger'
							},
							cancel: {
								label: "Annuler la suppression",
								className: 'btn-default'
							}
						},
						callback: function (response) {
							if (response) {		
								if (notEmpty(pages)) {									
									costumizer.pages.actions.deletePage(pages);
								}
							}
						}
					});
				}else if (notEmpty(pages)) {									
					costumizer.pages.actions.deletePage(pages);
				}
			},
			duplicate : function(datakey){
				$("#firstLoader").show();
				$("#firstLoader").css({
					'z-index': '200001'
				});
				$("#firstLoader #loadingModal").css({
					'opacity': '0.5'
				});

				let data = {
					"page": datakey,
					"costumId": costumizer.obj.contextId,
					"costumType": costumizer.obj.contextType,
					"action" : "costum/page",
					"subAction" : "DuplicatePage"
				};
				ajaxPost(
					null,
					baseUrl + "/co2/cms/duplicate",
					data,
					function(response) {
						$(".newPage").remove();
						$.each(response.page, function(k, v) {
							costumizer.obj.app[k] = v;
							urlCtrl.loadableUrls[k] = v;
							$(".tabling-pages > tbody").append(costumizer.pages.views.linePage(k, v, costum.langCostumActive));
							costumizer.updateComponentSocket({functionName: "managePage", data: {newKey: k, formData: v, keyApp: null}})
						});
						$("#firstLoader").hide();
						costumizer.pages.events.bind();
						cmsConstructor.page.events.loadListPage();
					}
				);
			},
			publiePage: function(datakey, mode){

				var message = (mode == "brouillon") ? tradCms.confirmDraft: tradCms.confirmPublish;
				
				bootbox.confirm({
					message: costumizer.pages.views.alertBoxContain(tradBadge.confirm, message,tradCms.confirmToContinue),
					buttons: {
						confirm: {
							label: trad.yes,
							className: 'btn-danger'
						},
						cancel: {
							label: trad.cancel,
							className: 'btn-default'
						}
					},
					callback: function(response) {
						if (response) {

							costumizer.actions.change("app."+datakey+".restricted");
							if(mode == "brouillon"){
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted", {});
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.draft", true);
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.admins", true);
							}
							else{
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.admins", "$unset" );
								jsonHelper.setValueByPath(costumizer.obj, "app."+datakey+".restricted.draft", "$unset");
							}
								
								
							costumizer.actions.update()
							$("#menu-right-costumizer").fadeOut();
							var publish = costumizer.pages.views.linePage(datakey, costumizer.obj.app[datakey], costum.langCostumActive);		
							$(".tabling-pages tr[id='keyPages-"+datakey+"']").replaceWith(publish);	
							costumizer.pages.events.bind();
							cmsConstructor.page.events.loadListPage();
							
						}
					}
				});
			},
			save : function(keyApp, values, htmlConstruct){
				//Todo 
				// Utiliser cette fonction pour le create et le update 
				// Créer les inputs dans la modal right avec le max d'options
					/*
						[x] slug key
						[x] name
						[x] icon 
						[x] admin  
						[x] shortdescription (le jour où on se préocuppe des méta data)
						[x] metatags (le jour où on se préocuppe des méta data)
						[x] Static page or app 

						[x] Si app selecteur (agenda, live, and others )
						[/] Si app selecteur add options can select filters
						[x] use filter
						[x] type filtre  
						[x] trier par
					*/
				// Créer un ctk dans cmsControlleur => SavePagesAction
				// Au retour enregistrer la nouvelle entrée ou changer la valeur existante 
				// rafraichisser la vue du tableau tbody et bind events
				
				var titleRightCostumizer=(notEmpty(keyApp)) ? tradCms.editPage : tradCms.createNewPage;
				var createPage=(notEmpty(keyApp)) ? false : true;

			
				// var anchorClassHideOrShow=".metaDescription, .metaTags, .selectApp, .appTypeName, .target, .urlExtern, .aap";
				// var aapClassHideOrShow = ".metaDescription, .metaTags, .selectApp, .appTypeName, .target, .urlExtern, .lbhAnchor, .coevent";
				// var urlExternOptions=".metaDescription, .metaTags, .selectApp, .appTypeName, .lbhAnchor, .aap, #costum-form-pages .appTypeName";
				var anchorClassHideOrShow=".selectApp, .appTypeName, .target, .urlExtern, .aap";
				var aapClassHideOrShow = ".selectApp, .appTypeName, .target, .urlExtern, .lbhAnchor, .coevent";
				var urlExternOptions=".selectApp, .appTypeName, .lbhAnchor, .aap";
				const restrectedAdminClassHideOrShow="#restrictionPage .members, #restrictionPage .roles";
				const restrectedMembersClassHideOrShow="#restrictionPage .isAdmin, #restrictionPage .roles";
				const restrectedRoleClassHideOrShow="#restrictionPage .isAdmin, #restrictionPage .members";
				var paramsToSend = {};
				var indexOfInput = {name : 0, keySlug : 1, icon : 2, lbhAnchor : 3, aap : 4, coevent: 5, target : 6, urlExtern : 7, metaDescription : 8, 
					metaTags : 9, selectApp : 10, appTypeName : 11};

				if(notEmpty(values) && notEmpty(values.lbhAnchor)){
					values.lbhAnchor = (values.lbhAnchor == "false" || values.lbhAnchor == false) ? false : true;
				}
				
				costumizer.actions.openRightSubPanel(
					"<form id='costum-form-pages' style='padding:0px 10px;'></form>",
					{
						width:300,
						distanceToRight: costumizer.liveChange ? 300:0,
						className:"contains-view-admin",
						header:{
							title:titleRightCostumizer
						},
						onInitialized: function(){
							//alert();
							var rulesForm={};
							var defaultDraft = false;
							var defaultIsAdmin = false;
							var defaultValuePageName = [];
							if(notEmpty(keyApp)){
								defaultValues = costumizer.obj.app[keyApp];
							}
							else{
								if(typeof defaultValues != "undefined")
									delete defaultValues;
								defaultDraft = true;
								defaultIsAdmin = true;
							}
							if (typeof defaultValues !== "undefined" && defaultValues["name"] != "undefined") {
								if (typeof defaultValues["name"] === 'object' && notEmpty(defaultValues["name"]))
									defaultValuePageName = defaultValues["name"];
								else
									defaultValuePageName[costum.langCostumActive] = defaultValues["name"];
							}
							paramsAppType = {
								search : tradCms.directoryPage,
								live : tradCms.journal,
								projects : tradCategory.projects,
								annonces : tradCategory.classifieds,
								agenda : trad.agenda,
								dda : trad.survey,
								map : trad.map,
								training : "Formation"
							};

							if(!$("#costum-form-pages").hasClass(`cmsbuilder-tabs`))
								$("#costum-form-pages").addClass(`cmsbuilder-tabs`);

							$("#costum-form-pages").append(`<div class="containte-form-page cmsbuilder-tabs-body col-xs-12"></div>`);
							
							// Getting subevent of the element
							var url = baseUrl + "/costum/coevent/get_events/request/element_event";
							var post = {
								id: costum.contextId,
								type: costum.contextType
							}
							var elementEvents = {};
							ajaxPost(null, url, post, function (events) {
								elementEvents = events;
							}, null, "json", { async: false });
							var appTypeNameVal = (typeof defaultValues != "undefined" && typeof defaultValues["appTypeName"] != "undefined") ? defaultValues["appTypeName"]:false;
							
							var inputUseInPage = [];
							if(notEmpty(keyApp) && typeof costumizer.obj.app[keyApp] != "undefined" && costumizer.obj.app[keyApp] != null && typeof costumizer.obj.app[keyApp].hash != "undefined" && costumizer.obj.app[keyApp].hash.includes("aap")) {
								inputUseInPage = [
									{
										type : "inputSimple",
										options: {
											name : "name",
											label :  tradCms.pageName,
											class : "pageName",
											translate : true,
											defaultValue :  defaultValuePageName
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "keySlug",
											label :  tradCms.urlKey,
											class : "keySlug",
											defaultValue : (notEmpty(keyApp)) ? keyApp.replace("#", "") : "",
										}
									},
									{
										type: "inputIcon",
										options: {
											name: "icon",
											label: tradCms.icon,
											class : "icon",
											defaultValue: (typeof defaultValues != "undefined" && typeof defaultValues["icon"] != "undefined" && notEmpty(defaultValues["icon"])) ? "fa fa-"+defaultValues["icon"]:tradCms.noIcon,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "btnScrollToTop",
											label :  tradCms.backToTop,
											class : "btnScrollToTop",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["btnScrollToTop"] != "undefined") ? defaultValues["btnScrollToTop"]:false,
										}
									},	
									
									{
										type : "textarea",
										options: {
											name : "metaDescription",
											label : tradCms.shortDescription,
											class : "metaDescription",
											defaultValue :(typeof defaultValues != "undefined" && typeof defaultValues["metaDescription"] != "undefined") ? defaultValues["metaDescription"]:"",
											rows : 1 
										},
									},
									{
										type : "tags",
										options : {
											name : "metaTags",
											label : tradDynForm.keyWords,
											class : "metaTags",
											options : [""],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["metaTags"] != "undefined") ? defaultValues["metaTags"]:"",
										}
									},	
									{
										type : "groupButtons",
										options: {
											name : "lbhAnchor",
											label :  tradCms.simpleAnchorForMenu,
											class : "lbhAnchor",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["lbhAnchor"] != "undefined") ? defaultValues["lbhAnchor"]:false,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "aap",
											label :  tradCms.aapType,
											class : "aap",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["hash"] != "undefined"  &&  defaultValues["hash"] == "#app.aap") ? true : false,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "coevent",
											label :  "Coevent",
											class : "coevent",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues.coevent !== "undefined"  &&  defaultValues["hash"] == "#app.view" && defaultValues.coevent),
										}
									},
									{
										type : "select",
										options : {
											name : "event",
											label :"Parent",
											class : "event",
											options : Object.keys(elementEvents).map(function(key) {
												return {
													label: elementEvents[key].name,
													value: key
												}
											}),
											defaultValue : (typeof defaultValues !== "undefined" && defaultValues.event) ? defaultValues.event : "",
										}
									},	
									{
										type : "groupButtons",
										options: {
											name : "target",
											label :  tradCms.addExternalLink,
											class : "target",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["target"] != "undefined") ? defaultValues["target"]:false,
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "urlExtern",
											label :  tradCms.externalLink,
											class : "urlExtern",
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["urlExtern"] != "undefined") ? defaultValues["urlExtern"]:"",
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "selectApp",
											label :   tradCms.usedAvancedApp,
											class : "selectApp",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["selectApp"] != "undefined") ? defaultValues["selectApp"]:false,
										}
									},
									{
										type : "select",
										options : {
											name : "appTypeName",
											label : trad.type +" App",
											class : "appTypeName",
											options : Object.keys(paramsAppType).map(function(value) {
												return {
													label: paramsAppType[value],
													value: value
												}
											}),
											defaultValue : appTypeNameVal,
										}
									},
									{
										type : "tags",
										options : {
											name : "types",
											label : "Filtres",
											class : "filtersTypes hidden",
											options : (typeof urlCtrl.loadableUrls["#search"] != "undefined" && typeof urlCtrl.loadableUrls["#search"]["filters"] != "undefined" && typeof urlCtrl.loadableUrls["#search"]["filters"]["types"] != "undefined" && notEmpty(urlCtrl.loadableUrls["#search"]["filters"]["types"])) ? urlCtrl.loadableUrls["#search"]["filters"]["types"] : ["organizations", "projects", "events", "poi", "news"],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["filters"] != "undefined" && typeof defaultValues["filters"]["types"] != "undefined") ? defaultValues["filters"]["types"]: ""
										}
									}
								];
							}
							else {
								inputUseInPage = [
									{
										type : "inputSimple",
										options: {
											name : "name",
											label :  tradCms.pageName,
											class : "pageName",
											translate : true,
											defaultValue :  defaultValuePageName
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "keySlug",
											label :  tradCms.urlKey,
											class : "keySlug",
											defaultValue : (notEmpty(keyApp)) ? keyApp.replace("#", "") : "",
										}
									},
									{
										type: "inputIcon",
										options: {
											name: "icon",
											label: tradCms.icon,
											class : "icon",
											defaultValue: (typeof defaultValues != "undefined" && typeof defaultValues["icon"] != "undefined" && notEmpty(defaultValues["icon"])) ? "fa fa-"+defaultValues["icon"]:tradCms.noIcon,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "btnScrollToTop",
											label :  tradCms.backToTop,
											class : "btnScrollToTop",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["btnScrollToTop"] != "undefined") ? defaultValues["btnScrollToTop"]:false,
										}
									},
									{
										type : "textarea",
										options: {
											name : "metaDescription",
											label : tradCms.shortDescription,
											class : "metaDescription",
											defaultValue :(typeof defaultValues != "undefined" && typeof defaultValues["metaDescription"] != "undefined") ? defaultValues["metaDescription"]:"",
											rows : 1 
										},
									},
									{
										type : "tags",
										options : {
											name : "metaTags",
											label : tradDynForm.keyWords,
											class : "metaTags",
											options : [""],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["metaTags"] != "undefined") ? defaultValues["metaTags"]:"",
										}
									},		
									{
										type : "groupButtons",
										options: {
											name : "lbhAnchor",
											label :  tradCms.simpleAnchorForMenu,
											class : "lbhAnchor",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["lbhAnchor"] != "undefined") ? defaultValues["lbhAnchor"]:false,
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "coevent",
											label :  "Coevent",
											class : "coevent",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues.coevent !== "undefined"  &&  defaultValues["hash"] == "#app.view" && defaultValues.coevent),
										}
									},
									{
										type : "select",
										options : {
											name : "event",
											label :"Parent",
											class : "event",
											options : Object.keys(elementEvents).map(function(key) {
												return {
													label: elementEvents[key].name,
													value: key
												}
											}),
											defaultValue : (typeof defaultValues !== "undefined" && defaultValues.event) ? defaultValues.event : "",
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "target",
											label :  tradCms.addExternalLink,
											class : "target",
											options : [{"label" : "Oui", "value" : true, "icon" : ""},{"label" : "Non", "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["target"] != "undefined") ? defaultValues["target"]:false,
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "urlExtern",
											label :  tradCms.externalLink,
											class : "urlExtern",
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["urlExtern"] != "undefined") ? defaultValues["urlExtern"]:"",
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "note",
											label :  "Page notes",
											class : "costum enable-note",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues.note !== "undefined"),
										}
									},
									{
										type: "selectMultiple",
										options: {
											name : "select-note",
											label :  "Selectionner la note",
											class : "costum select-note",
											configForSelect2: {
												maximumSelectionSize: 1
											},									
											options : Object.keys(costum.availableNote).map(function(key) {
												return {
													label: costum.availableNote[key].name,
													value: key
												}
											}),
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["note"] != "undefined") ? defaultValues["note"]["noteId"] : "",
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "alias",
											label :  "Alias entre deux costum",
											class : "costum enable-alias",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues.linked !== "undefined"),
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "costumId",
											label :  "Id de costum",
											class : "costum-alias hidden",
											defaultValue :  (typeof defaultValues != "undefined" && typeof defaultValues.linked !== "undefined") ? defaultValues.linked.costumId : "",
											payload : {
												path : "linked.costumId"
											}
										}
									},								
									{
										type : "inputSimple",
										options: {
											name : "costumSlug",
											label :  "Slug du costum",
											class : "costum-alias",
											defaultValue :  (typeof defaultValues != "undefined" && typeof defaultValues.linked !== "undefined") ? defaultValues.linked.url : "",
											payload : {
												path : "linked.url"
											}
										}
									},
									{
										type : "inputSimple",
										options: {
											name : "costumPage",
											label :  "Slug de la page CMS à afficher",
											class : "costum-alias",
											defaultValue :  (typeof defaultValues != "undefined" && typeof defaultValues.linked !== "undefined") ? defaultValues.linked.page : "",
											payload : {
												path : "linked.page"
											}
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "selectApp",
											label :   tradCms.usedAvancedApp,
											class : "selectApp",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["selectApp"] != "undefined") ? defaultValues["selectApp"]:false,
										}
									},
									{
										type : "select",
										options : {
											name : "appTypeName",
											label : trad.type +" App",
											class : "appTypeName",
											options : Object.keys(paramsAppType).map(function(value) {
												return {
													label: paramsAppType[value],
													value: value
												}
											}),
											defaultValue : appTypeNameVal,
										}
									},
									{
										type : "tags",
										options : {
											name : "types",
											label : "Filtres",
											class : "filtersTypes hidden",
											options : (typeof urlCtrl.loadableUrls["#search"] != "undefined" && typeof urlCtrl.loadableUrls["#search"]["filters"] != "undefined" && typeof urlCtrl.loadableUrls["#search"]["filters"]["types"] != "undefined" && notEmpty(urlCtrl.loadableUrls["#search"]["filters"]["types"])) ? urlCtrl.loadableUrls["#search"]["filters"]["types"] : ["organizations", "projects", "events", "poi", "news"],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["filters"] != "undefined" && typeof defaultValues["filters"]["types"] != "undefined") ? defaultValues["filters"]["types"]: ""
										}
									}
								];
							}
							
							var myCoInput = {
								container : ".containte-form-page",
								inputs :inputUseInPage,
								onchange:function(name, value, payload){
									costumizer.pages.events.change(name, value, payload, keyApp, anchorClassHideOrShow, paramsToSend, urlExternOptions, aapClassHideOrShow, restrectedAdminClassHideOrShow, restrectedMembersClassHideOrShow, restrectedRoleClassHideOrShow);
								}
							}
							
							new CoInput(myCoInput);

							$(".containte-form-page").append(`
							<div class="panel-group restriction-container" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab">
										<p class="panel-title">
											<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#restrictionPage" aria-expanded="true" aria-controls="collapse">
												<span  class="fa fa-exclamation-triangle" aria-hidden="true"></span>`+ tradCms.accessRestriction +
											`</a>
										</p>
									</div>
								</div>
							</div>
							<div id="restrictionPage" class="panel-collapse collapse"></div>
						
							<div class="panel-group restriction-container" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading" role="tab">
										<p class="panel-title">
											<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#particlePage" aria-expanded="true" aria-controls="collapse">
												<span  class="fa fa-connectdevelop" aria-hidden="true"></span>Particule
											</a>
										</p>
									</div>
								</div>
							</div>
							<div id="particlePage" class="panel-collapse collapse"></div>
							`)
							

							new CoInput({
								container : "#restrictionPage",
								inputs :[
									{
										type : "groupButtons",
										options: {
											name : "members",
											label :  tradCms.visibleMembers,
											class : "members",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["members"] != "undefined") ? defaultValues["restricted"]["members"]:false,
											payload : {
												path : "restricted.members",
											}
										}
									},
									
									{
										type : "groupButtons",
										options: {
											name : "isAdmin",
											class : "isAdmin",
											label :   tradCms.visibleToAdminOnly,
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["admins"] != "undefined") ? defaultValues["restricted"]["admins"]:defaultIsAdmin,
											payload : {
												path : "restricted.admins",
											}
										}
									},
									{
										type : "tags",
										options : {
											name : "roles",
											label : tradCms.visibleRoles,
											class : "roles",
											options : (notEmpty(costum.roles)) ? costum.roles : (notEmpty(rolesList)?rolesList : ["Financeur","Partenaire","Sponsor","Organisateur","Président","Directeur","Conférencier","Intervenant"]),
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined" && typeof defaultValues["restricted"]["roles"] != "undefined") ? defaultValues["restricted"]["roles"]:"",
											payload : {
												path : "restricted.roles",
											}
										}
									},
									{
										type : "groupButtons",
										options: {
											name : "draft",
											label :   "status",
											class : "draft",
											options : [{"label" : trad.yes, "value" : true, "icon" : ""},{"label" : trad.no, "value" : false, "icon" : ""}],
											defaultValue : (typeof defaultValues != "undefined" && typeof defaultValues["restricted"] != "undefined"  && typeof defaultValues["restricted"]["draft"] != "undefined") ? defaultValues["restricted"]["draft"]: defaultDraft,
											payload : {
												path : "restricted.draft",
											}
										}
									},
								],
								onchange:function(name, value, payload){
									costumizer.pages.events.change(name, value, payload, keyApp, anchorClassHideOrShow, paramsToSend, urlExternOptions, aapClassHideOrShow, restrectedAdminClassHideOrShow, restrectedMembersClassHideOrShow, restrectedRoleClassHideOrShow);
								}
							});

							new CoInput({
								container : "#particlePage",
								inputs :[
									{
										type: "groupButtons",
										options: {
											name: "particleActive",
											label: "Activé",
											class: "particle-active",
											options: [{"label": trad.yes, "value": true, "icon": ""}, {
												"label": trad.no,
												"value": false,
												"icon": ""
											}],
											defaultValue: (typeof defaultValues != "undefined" && typeof defaultValues["particle"] !== "undefined" && defaultValues["particle"]["particleActive"] !== "undefined") ? defaultValues["particle"]["particleActive"] : false,
										}
									},
									{
										type : "colorPicker",
										options: {
											name : "pointColor",
											label :  "Couleur du point",
											class : "particle-color hidden",
											defaultValue :  (typeof defaultValues != "undefined" && typeof defaultValues["particle"] !== "undefined" && defaultValues["particle"]["pointColor"] !== "undefined") ? defaultValues["particle"]["pointColor"] : "",
										}
									},
									{
										type : "colorPicker",
										options: {
											name : "lineColor",
											label :  "Couleur du ligne",
											class : "particle-color hidden",
											defaultValue :  (typeof defaultValues != "undefined" && typeof defaultValues["particle"] !== "undefined" && defaultValues["particle"]["lineColor"] !== "undefined") ? defaultValues["particle"]["lineColor"] : "",
										}
									}

								],
								onchange:function(name, value, payload){
									costumizer.pages.events.change(name, value, payload, keyApp, anchorClassHideOrShow, paramsToSend, urlExternOptions, aapClassHideOrShow, restrectedAdminClassHideOrShow, restrectedMembersClassHideOrShow, restrectedRoleClassHideOrShow);
								}
							});

							$("#restrictionPage .draft").css("display", "none");

							if(!notEmpty(keyApp)){
								$("#costum-form-pages .keySlug").removeClass("has-success").addClass("has-error");
							}
							
							$("#costum-form-pages .keySlug, .pageName, .appTypeName, .event, .urlExtern").append("<span class='help-block'></span>");
							

							if(myCoInput.inputs[indexOfInput.selectApp].options.defaultValue == false || myCoInput.inputs[indexOfInput.selectApp].options.defaultValue == "false" ){
								$("#costum-form-pages .appTypeName").hide(); 
							}
							if(myCoInput.inputs[indexOfInput.target].options.defaultValue == false || myCoInput.inputs[indexOfInput.target].options.defaultValue == "false" ){
								$("#costum-form-pages .urlExtern").hide(); 
							}

							if(typeof values != "undefined" && values != null){
								if(typeof values.lbhAnchor != "undefined" && values.lbhAnchor)
									$(anchorClassHideOrShow).hide();

								else if(typeof values.hash  != "undefined" && values.hash == "#app.aap")
									$(aapClassHideOrShow).hide();

								else if(typeof values.target != "undefined" && values.target)
									$(urlExternOptions).hide();
							}
							
							$("#costum-form-pages").append(`<div class="cmsbuilder-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;">
								<a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled><i class="fa fa-save"></i> `+tradCms.register+`</a>
							</div></div>`);
							
							$("#costum-form-pages #btn-submit-form").click(function() {

								if(notEmpty($("#costum-form-pages input[name=name]").val()) && notEmpty($("#costum-form-pages input[name=keySlug]").val())){
									$("#costum-form-pages .pageName .help-block, #costum-form-pages .keySlug .help-block").removeClass('letter-red').addClass('letter-green').text("");
									if (!$("#costum-form-pages .keySlug").parent().hasClass("has-error")) {
										var formData = {};
										$.each(myCoInput.inputs, function(index, val){
											var path = (typeof val.options.payload != "undefined" && typeof val.options.payload.path != "undefined") ? val.options.payload.path : val.options.name;
											if(val.type == "inputSimple" && val.options.name != "isAdmin"  && val.options.name != "keySlug") {
												if (val.options.name == "name") {
													if (typeof paramsToSend["name"] != "undefined" && notEmpty(paramsToSend["name"])) {
														formData[path] = {};
														if (notEmpty(keyApp) && typeof costumizer.obj.app[keyApp] != "undefined" && typeof costumizer.obj.app[keyApp][path] != "undefined") {
															if (typeof costumizer.obj.app[keyApp][path] === "object") {
																formData[path] = costumizer.obj.app[keyApp][path];
															} else {
																let pagesToObject = {},
																	language = costum.langCostumActive;
																pagesToObject[language] = costumizer.obj.app[keyApp][path];
																formData[path] = pagesToObject;
															}
														}
														$.each(paramsToSend["name"], function(key, val) {
															formData[path][key] = val;
														})
													} else {
														formData[path] = costumizer.obj.app[keyApp][path];
													}
												} else {
													if($("input[name="+val.options.name+"]").val() != false && $("input[name="+val.options.name+"]").val() != "false")
													formData[path] = $("input[name="+val.options.name+"]").val();
												}
											}

											if(val.type == "textarea")
												formData["metaDescription"] = $("textarea[name="+val.options.name+"]").val();
										});
										var newKey = "#" + $("#costum-form-pages input[name=keySlug]").val();	
										formData.restricted = {};

										if (notEmpty(keyApp) && ($("#costum-form-pages .aap .btn-group .btn-primary").data("value") === "false" || $("#costum-form-pages .aap .btn-group .btn-primary").data("value") === false)) {
											if(newKey.length > 3 && newKey.substring(newKey.length - 3, newKey.length) == "Aap")
												newKey = newKey.replace("Aap", "");
										}
										formData.selectApp = JSON.parse($("#costum-form-pages .selectApp .btn-group .btn-primary").data("value"));
										if(formData.selectApp){
											formData.appTypeName = $("select[name=appTypeName]").val();
											if (typeof formData.appTypeName !== "undefined" && formData.appTypeName == "search" && typeof paramsToSend["types"] != "undefined") {
												formData["filters"] = {};
												formData["filters"]["types"] = paramsToSend["types"].split(',');
											}
										} else {
											delete formData.appTypeName;
											delete formData.selectApp;
											delete formData.filters;
										}
										formData.coevent = JSON.parse($("#costum-form-pages .coevent .btn-group .btn-primary").data("value"));
										$("#costum-form-pages [name=event] + .help-block").hide();
										if(formData.coevent) {
											var event_value = $("#costum-form-pages [name=event]").val();
											if (!event_value) {
												$("#costum-form-pages [name=event] + .help-block").show().addClass('letter-red').text(tradCms.thisFieldIsRequired);
												return (true);
											}
											formData.event = event_value;
										} else {
											delete formData.coevent;
											delete formData["event"];
										}
										var particleChange = false;
										if (typeof paramsToSend["particleActive"] != "undefined" || typeof paramsToSend["pointColor"] != "undefined" || typeof paramsToSend["lineColor"] != "undefined") {
											var particleActive = JSON.parse($("#costum-form-pages .particle-active .btn-group .btn-primary").data("value"));
											if (particleActive)	 {
												formData.particle = {};
												formData.particle["particleActive"] = particleActive;
												var pointColor_value = $("#costum-form-pages [name='pointColor']").val();
												var lineColor_value = $("#costum-form-pages [name='lineColor']").val();
												pointColor_value !== "" && (formData.particle["pointColor"] = pointColor_value);
												lineColor_value !== "" && (formData.particle["lineColor"] = lineColor_value);
											}else {
												delete formData.particle;
											}
											particleChange = true;
										}

										if ($(".costum-alias[name='costumId']").val() != "") {
											formData.linked = {
												costumId : $(".costum-alias[name='costumId']").val(),
												page 	 : $(".costum-alias[name='costumPage']").val(),
												url  	 : $(".costum-alias[name='costumSlug']").val()
											}
										}else{
											delete formData.linked;
										}

										if (notNull($("#costum-form-pages select[name='select-note'").val()) && $("#costum-form-pages select[name='select-note'").val()[0] != "" && $(".enable-note").find(".btn-active").data("value")) {
											formData.note = {
												noteId : $("#costum-form-pages select[name='select-note'").val()[0]
											}
										}else{
											delete formData.note;
										}


										if(!formData.selectApp || (formData.selectApp && typeof formData.appTypeName != "undefined" && notEmpty(formData.appTypeName))){
											$("#costum-form-pages .appTypeName .help-block").removeClass('letter-red').addClass('letter-green').text("");
											if (notEmpty(keyApp)) {
												if (typeof costumizer.obj.app[keyApp].urlExtra == "undefined" || formData.selectApp === true)
													formData.urlExtra = "/page/" + keyApp.replace("#", "");

												if(typeof costumizer.obj.app[keyApp].icon != "undefined")
													formData.icon = costumizer.obj.app[keyApp].icon;
												else
													formData.icon = "";
		
												if(typeof costumizer.obj.app[keyApp].metaTags != "undefined")
													formData.metaTags =costumizer.obj.app[keyApp].metaTags;
												else
													formData.metaTags = [];

												if(typeof costumizer.obj.app[keyApp].restricted != "undefined" && typeof costumizer.obj.app[keyApp].restricted.roles != "undefined")
													formData.restricted["roles"] = costumizer.obj.app[keyApp].restricted.roles;
												else
													delete formData.restricted["roles"];
											} else {
												formData.urlExtra = "/page/" + newKey.replace("#", "");
													
												formData.restricted["draft"] = true;												
											}

											if(typeof paramsToSend["icon"] != "undefined")
												formData.icon = paramsToSend["icon"];
											
											if(typeof paramsToSend["metaTags"] != "undefined"/* && notEmpty(paramsToSend["metaTags"])*/)
												formData.metaTags = paramsToSend["metaTags"].split(", ");
												
											if (formData.selectApp && typeof urlCtrl.loadableUrls["#" + formData.appTypeName] != "undefined") {
												$.each(urlCtrl.loadableUrls["#" + formData.appTypeName], function (e, v) {
													if ($.inArray(e, ["name","icon", "metaDescription", "metaTags","urlExtra"]) < 0) {
														var filtersAvailable = (e == "filters" && typeof formData["filters"] != "undefined" && typeof formData["filters"]["types"] != "undefined" && notEmpty(formData["filters"]["types"]) && notEmpty(formData["filters"]["types"][0]));														
														var appData = costumizer.obj.app[keyApp];
														if (e == "module") delete formData.urlExtra;
														if(notEmpty(keyApp)){
															if(typeof appData != "undefined" && typeof appData[e] == "undefined"){
																formData[e] = filtersAvailable ? formData["filters"] : v;
															} else {
																if (e == "filters") {
																	formData[e] = filtersAvailable ? formData["filters"] : v;
																} else 
																	formData[e] = (typeof appData != "undefined" && typeof appData[e] != "undefined") ? appData[e] : v;
															}
														} else {
															formData[e] = filtersAvailable ? formData["filters"] : v;
														}
													}
												});
												if(formData.appTypeName != "dda" && formData.appTypeName != "annonces" ){
													formData.urlExtra = "/page/" + newKey.replace("#", "");
												}
											} else
												formData.hash = "#app.view";
											
											if ($("#costum-form-pages .lbhAnchor .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .lbhAnchor .btn-group .btn-primary").data("value") === true) {
												formData.lbhAnchor = true;
												formData.metaDescription = "";
												formData.metaTags = [];
												formData.appTypeName = "";
												formData.urlExtern = "";
												formData.urlExtra = "/page/welcome";
												formData.hash = "#app.view";
												
												var appValueToDelete = ["module", "initFilters", "filterObj", "type", "useFilter", "useFooter"];

												if(notEmpty(keyApp)){
													$.each(appValueToDelete, function(key, value){
														if(typeof formData[value] != "undefined"){
															delete formData[value];
														}
													});		
												}
											}

											if ($("#costum-form-pages .btnScrollToTop .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .btnScrollToTop .btn-group .btn-primary").data("value") === true) {
												formData.btnScrollToTop = true;
											} else {
												delete formData.btnScrollToTop;
											}
											
											if ($("#costum-form-pages .target .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .target .btn-group .btn-primary").data("value") === true) {
												formData.target = true;
												formData.metaDescription = "";
												formData.metaTags = [];
												var urlExternValue = $("#costum-form-pages input[name=urlExtern]").val();
												if (urlExternValue == "") {
													$("#costum-form-pages [name=urlExtern] + .help-block").show().addClass('letter-red').text(tradCms.thisFieldIsRequired);
													return (true);
												} 
												formData.urlExtern = urlExternValue;	
												formData.hash = "#app.view";
												delete formData.urlExtra;
												
												var appValueToDelete = ["module", "initFilters", "filterObj", "type", "useFilter", "useFooter"];

												if(notEmpty(keyApp)){
													$.each(appValueToDelete, function(key, value){
														if(typeof formData[value] != "undefined"){
															delete formData[value];
														}
													});		
												}
											}
											else{ 
												delete formData.urlExtern;
											}

											if(notEmpty(keyApp)){
												if(typeof costumizer.obj.app[keyApp] != "undefined" && typeof costumizer.obj.app[keyApp].useFilter != "undefined"  && typeof costumizer.obj.app[keyApp].selectApp == "undefined" ){
													var tmpFormData = formData;
													formData = {};

													$.each(costumizer.obj.app[keyApp], function(key, val){
														formData[key] = val;
													});
										
													formData.name = tmpFormData.name;
													formData.icon = tmpFormData.icon;
												}

												
												if(!formData.selectApp && !formData.lbhAnchor){	
													if ( $("#costum-form-pages .target .btn-group .btn-primary").data("value") === "false" || $("#costum-form-pages .target .btn-group .btn-primary").data("value") === false) 
														formData.urlExtra = "/page/" + newKey.replace("#", "");
													else
														delete formData.urlExtra;

													if(typeof costumizer.obj.app[keyApp].urlExtra != "undefined" && costumizer.obj.app[keyApp].urlExtra.indexOf("/page/") != -1)
														formData.urlExtra = costumizer.obj.app[keyApp].urlExtra.replace(keyApp.replace("#", ""), newKey.replace("#", ""));
												}
											} 

											if ($("#costum-form-pages .aap .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .aap .btn-group .btn-primary").data("value") === true) {
												var appValueForAap = ["hash", "name", "urlExtra", "icon", "restricted"];

												$.each(formData, function(key, value){
													if(typeof formData[key] != "undefined" && !appValueForAap.includes(key)){
														delete formData[key];
													}
												});	
												
												formData.urlExtra = "/page/"+newKey.replace("#", "");
												formData.hash = "#app.aap";
												if(newKey.length > 3 && newKey.substring(newKey.length - 3, newKey.length) != "Aap")
													newKey = newKey+"Aap";
											}
											else{
												if(newKey.length > 3 && newKey.substring(newKey.length - 3, newKey.length) == "Aap")
													newKey = newKey.replace("Aap", "");
											}

											if(typeof formData.restricted == "undefined")
												formData.restricted = {};

											if ($("#costum-form-pages .isAdmin .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .isAdmin .btn-group .btn-primary").data("value") === true) {
												formData.restricted["admins"] = true;
												($("#costum-form-pages .draft .btn-group .btn-primary").data("value") == "true" || $("#costum-form-pages .draft .btn-group .btn-primary").data("value") == true) ? formData.restricted["draft"] = true : delete formData.restricted["draft"];
											}
											else { 
												delete formData.restricted["admins"];
												delete formData.restricted["draft"];
											}

											if ($("#costum-form-pages .members .btn-group .btn-primary").data("value") === "true" || $("#costum-form-pages .members .btn-group .btn-primary").data("value") === true) {
												formData.restricted["members"] = true;
											}else{
												delete formData.restricted["members"];
											} 
											if(typeof paramsToSend["restricted.roles"] != "undefined" && notEmpty(paramsToSend["restricted.roles"]))
												formData.restricted["roles"] = paramsToSend["restricted.roles"].split(",");	
											
											jsonHelper.setValueByPath(costumizer.obj, "app." + newKey, formData);
											jsonHelper.setValueByPath(themeParams.pages, newKey, formData);
											jsonHelper.setValueByPath(costum, "appConfig.pages." + newKey, formData);
											jsonHelper.setValueByPath(urlCtrl.loadableUrls, newKey, formData);
											
											if (notEmpty(keyApp) && newKey != keyApp) {
												if(typeof costumizer.obj.app[keyApp] != "undefined" && typeof costumizer.obj.app[keyApp].urlExtra != "undefined" && costumizer.obj.app[keyApp].urlExtra.indexOf("/page/") != -1){
													ajaxPost(
														null,
														baseUrl+"/"+moduleId+"/cms/updatevaluepagecms",
														{
															params : {'keyApp' : keyApp.replace("#",""), 'newKey' : newKey.replace("#","")}
														},
														function(data){
															return true;
														}			
													);
												}	
												jsonHelper.deleteByPath(costumizer.obj, "app." + keyApp);
												jsonHelper.deleteByPath(themeParams, "pages." + keyApp);
												jsonHelper.deleteByPath(costum, "appConfig.pages." + keyApp);
												$("a[href='" + keyApp + "']").attr("href", newKey);
											}
											if (notEmpty(htmlConstruct)) {
												if(notEmpty(keyApp))
													costumizer.htmlConstruct.actions.replacePage(htmlConstruct.path, keyApp, newKey);
												else
													costumizer.htmlConstruct.actions.addPageInMenu(htmlConstruct.path, newKey, htmlConstruct.domAppend);
												$("#right-sub-panel-container").fadeOut(100);
											} else {
												var newLine = costumizer.pages.views.linePage(newKey, formData, costum.langCostumActive);	

												if(notEmpty(keyApp)){
													$(".tabling-pages tr[id='keyPages-"+keyApp+"']").replaceWith(newLine);
												}else{
													$(".tabling-pages tbody").append(newLine);
												}
												costumizer.pages.events.bind();
												
											}
											var params = {};
											params["app"] = costumizer.obj.app;
											var pathHtmlConstruct = costumizer.htmlConstruct.actions.findIfKeyPageExist(costumizer.obj.htmlConstruct, keyApp);
											if (notEmpty(pathHtmlConstruct)) {
												costumizer.htmlConstruct.actions.editPageInMenu(keyApp, newKey, pathHtmlConstruct);
											}

											var subAction = (notEmpty(keyApp)) ? "EditPage" : "NewPage";
											//var olderData = jsonHelper.getValueByPath(costumizer.costumDataOlder,"app."+keyApp);
											var olderData = {};
											olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder,"app");
											var pageSelected = $("#costum-form-pages input[name=name]").val();
											ajaxPost(
												null,
												baseUrl+"/"+moduleId+"/cms/updatecostum",
												{
													params: params,
													costumId    : costum.contextId,
													costumType  : costum.contextType,
													page : pageSelected,
													keyPage : (notEmpty(keyApp)) ? keyApp : newKey,
													action : "costum/page",
													subAction : subAction,
													olderData : olderData,
													pathChanged : ["app"]
												},
												function(data){
													costum.app=jQuery.extend(true,{},costumizer.obj.app);
													costumizer.updateComponentSocket({functionName: "managePage", data: {
														newKey   : newKey,
														formData : formData,
														keyApp   : keyApp,
														app 	 : costum.app
													}})
													toastr.success(tradCms.updateConfig);
													$("#right-sub-panel-container").fadeOut();
													if($(".appTypeName select").val()=="training"){
														costumizer.app.training.callback(newKey,pageSelected);
													}
													if (formData.coevent)
														location.href = baseUrl + "/costum/co/index/slug/" + costum.contextSlug + "/edit/true/" + newKey + "?text=coevent";
													//mylog.log("test",keyApp.replace("#",""),page)
													if (particleChange && notEmpty(keyApp) && keyApp.replace("#","") == page) {
														window.location.reload();
													}

													cmsConstructor.page.events.loadListPage();
												}			
											);
											costumizer.pages.valueForNamePage = {};
										}
										else{
											$("#costum-form-pages .appTypeName .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
										}
									} 
									$(this).find("i").removeClass("fa-save").addClass("fa-spin fa-spinner");
								}else{
									if (!notEmpty($("#costum-form-pages input[name=name]").val())) {
										$("#costum-form-pages .pageName .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
										$(".right-sub-panel-body #costum-form-pages .pageName")[0].scrollIntoView({
											block : "end",
											behavior : "smooth"
										});
									}
									if (!notEmpty($("#costum-form-pages input[name=keySlug]").val())) {
										$("#costum-form-pages .keySlug .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
										$(".right-sub-panel-body #costum-form-pages .keySlug")[0].scrollIntoView({
											block : "end",
											behavior : "smooth"
										});	
									}
									$(this).find("i").removeClass("fa-spin fa-spinner").addClass("fa-save");
								}
								$(this).attr("disabled", "disabled");
							});
							

							var valueToCheck = ["lbhAnchor", "coevent", "target", "note", "linked", "selectApp"];
							var inputsParent = ".lbhAnchor, .coevent, .target, .enable-note, .enable-alias, .selectApp";
							var inputsChild = ".event, .urlExtern, .select-note, .costum-alias, .appTypeName";
							$(`#costum-form-pages ${inputsChild}`).hide();
							if (notEmpty(keyApp)) {
								mylog.log("defaultValues1",defaultValues)
								$.each(defaultValues, function(key, value) {
									if ($.inArray(key, valueToCheck) >= 0 && (value != "" || value != false)) {
										$(`#costum-form-pages ${inputsParent}`).hide();
										if ($("#costum-form-pages ."+key+"").length >= 1) {
											$("#costum-form-pages ."+key+"").show();
										}
										switch (key) {
											case "coevent":
												$("#costum-form-pages .event").show();
												break;
											case "target":
												$("#costum-form-pages .urlExtern").show();
												break;
											case "note":
												$("#costum-form-pages .enable-note, #costum-form-pages .select-note").show();
												break;
											case "linked":
												$("#costum-form-pages .costum-alias, #costum-form-pages .enable-alias").show();
												break;
											case "selectApp":
												$("#costum-form-pages .appTypeName").show();
												if ($("#costum-form-pages select[name=appTypeName]").val() != "" && $("#costum-form-pages select[name=appTypeName]").val() == "search") {
													$("#costum-form-pages .filtersTypes").show().removeClass("hidden");
												}
												break;
											default:
												break;
										}
									}
									if (typeof defaultValues["particle"] != "undefined" && typeof defaultValues["particle"]["particleActive"] != "undefined") {
										if (defaultValues["particle"]["particleActive"]) {
											$("#costum-form-pages .particle-color").show().removeClass("hidden");
										}
									}
								})
							}

						}

					}
				)			
        	},
			callback : function(){
							
			},
			checkUniqueApp :function(newKey, oldKey){
				oldKey=(notEmpty(oldKey)) ? oldKey.replace("#", "") : "";
				newKey = slugify(newKey).toLowerCase();
				if (newKey === "") {
					$("#costum-form-pages .keySlug").removeClass("has-success").addClass("has-error");
					$("#costum-form-pages .keySlug .help-block").removeClass('letter-green').addClass('letter-red').text(tradCms.thisFieldIsRequired);
				} else if(newKey != oldKey && typeof costum.app["#"+newKey] != "undefined"){
					$("#costum-form-pages .keySlug").removeClass("has-success").addClass("has-error");
					$("#costum-form-pages .keySlug .help-block").removeClass('letter-green').addClass('letter-red');

					(typeof costumizer.obj.app["#"+newKey] != "undefined") ? $("#costum-form-pages .keySlug .help-block").text(tradCms.keyUsed) : $("#costum-form-pages .keySlug .help-block").text(tradCms.privateKey);
				}
				else{
					$("#costum-form-pages .keySlug").removeClass("has-error").addClass("has-success");
					$("#costum-form-pages .keySlug .help-block").removeClass('letter-red').addClass('letter-green').text(tradCms.keyAvailable);
				}
			},

			visibility: function(value, keyPage) {
				if(notEmpty(value) && notEmpty(keyPage)){
					var draft;
					if(typeof costumizer.obj != "undefined" && typeof costumizer.obj.app != "undefined"  && typeof costumizer.obj.app[keyPage] != "undefined" && typeof costumizer.obj.app[keyPage].restricted != "undefined" && typeof costumizer.obj.app[keyPage].restricted.draft != "undefined"){
						draft = costumizer.obj.app[keyPage].restricted.draft;
					}
					 	
					var path = "app."+keyPage+".restricted", obj = costumizer.obj;

					jsonHelper.setValueByPath(obj, path, {});

					if(value != "all"){
						jsonHelper.setValueByPath(obj, "app."+keyPage+".restricted."+value, true);

						if(typeof draft != "undefined" && notEmpty(draft) && value == "admins")
							jsonHelper.setValueByPath(obj, "app."+keyPage+".restricted.draft", draft);		
					}
					else
						jsonHelper.setValueByPath(obj, path, "$unset");
					
					costumizer.actions.change(path);
					costumizer.actions.update();
										
					$(".tabling-pages tr[id='keyPages-"+keyPage+"']").replaceWith(costumizer.pages.views.linePage(keyPage, costumizer.obj.app[keyPage], costum.langCostumActive));
					costumizer.pages.events.bind();
					cmsConstructor.page.events.loadListPage();
				}
			},
		},	
	},
	lastChange : {
		init : function(){
			costumizer.lastChange.actions.getLastLogs();
		},
		views : {
			init : function(){
				var str = "<div id='lastChange-content' class='col-md-10 col-md-offset-1 col-sm-12 col-xs-12'></div>";
				return str;
			}
		},
		actions : {
			getLastLogs : function () {
				var urlToSend = baseUrl+"/"+moduleId+"/cms/lastchange";
				var params = {
					costumSlug    : costum.contextSlug
				};
				ajaxPost(
					null,
					urlToSend,
					params,
					function(data){
						if(data.result && data.items.length != 0){
							var str = "",
								msgDetail = "";
							if  (Object.keys(data.items.lastAdmin).length  != 0 ) {
								str += "<div class='col-lg-6 col-xs-12'>" +
											"<h2>"+tradCms.usersParticipated+"</h2>" +
											"<ul class='version-costum' style='margin-top: 40px;'>";
								$.each(data.items.lastAdmin, function(key, value){
									str += '<li>' +
											'<div class="detail-version">'+value +'</div> ' +
											'<hr style="border-top: 1px solid #d4d4d4;">' +
										'</li>' ;
								})
								str +=	"</ul>" +
									"</div>";
							}
							if (Object.keys(data.items.lastModif).length  != 0 ) {
								str += "<div class='col-lg-6 col-xs-12'>" +
									"<h2>"+ Object.keys(data.items.lastModif).length +" "+tradCms.latestSiteChanges+"</h2>"+
									"<ul class='timeline  version-costum'>";
								$.each(data.items.lastModif, function(key, change){
									if (change.action == "costum/config")
										msgDetail = tradCms.menuOrOtherConfig;
									else if (change.action == "costum/page")
										msgDetail = tradCms[change.subAction.charAt(0).toLowerCase() + change.subAction.slice(1)];
									else
										msgDetail = tradCms[change.action.replaceAll("costum/","")]+ ' '+ change.blockName;

									str += '<li>' +
												'<div class="detail-version">'+directory.showDatetimePost(null, null, change.created)+'</div>' +
												'<div class="detail-version float-right">'+change.action.replace("costum/","") +'</div> ' +
												'<p>'+msgDetail+' '+trad.by+' <b>'+change.userName+'</b></p>' +
												'<hr style="border-top: 1px solid #d4d4d4;">' +
											'</li>';
								});
								str	+= '</ul>'+
									'</div>';

							}
							str = str.replaceAll("dateUpdated","");
							$("#lastChange-content").html(str);
						}else{
							toastr.error(data.msg);
						}
					}
				);
			}
		}
	},
	htmlConstruct  : {
		init : function(){
			costumizer.liveChange=false;
			// Initialisation de la config htmlConstruct du costum @keyConf 
			if(costumizer.htmlConstruct.actions.isActivated("menuLeft")){
				if(costumizer.htmlConstruct.actions.isActivated("header.banner"))
					costumizer.htmlConstruct.keyConf= "configFour"; 
				else
					costumizer.htmlConstruct.keyConf = (costumizer.htmlConstruct.actions.isActivated("header.menuTop")) ? "configThree" : "configFive";
			}else
				costumizer.htmlConstruct.keyConf = (costumizer.htmlConstruct.actions.isActivated("header.banner")) ? "configTwo" : "configOne";
			
		},
		keyConf : "",
		map : {
			header:{
				titleClass: "labelEditMenu",
				containerClass: "col-xs-12 main-section editMenuCostumizer",
				title : tradCms.headerSection,
				subList : {
					// banner : {
					// 	title : tradCms.banner,
					// 	view : "blockCms",
					// 	activable : "disabled"
					// },
					menuTop: {
						title : tradCms.topMenu,
						activable : true,
						subList : {
							left : {
								title : tradCms.leftSection,
								domId : "menuTopLeft",
								view : "menuConstruct",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
								
							},
							center : {
								view : "menuConstruct",
								title : tradCms.centerSection,
								domId : "menuTopCenter",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
							}	,
							right : {
								view : "menuConstruct",
								title : tradCms.rightSection,
								domId : "menuTopRight",
								containerClass : "col-xs-12 col-sm-4 manageMenusCostum column-menu-costumizer"
							}
						}
					}
				}
			},
			menuLeft : {
				title : tradCms.leftMenu,
				view : "menuConstruct",
				domId : "menuLeft",
				activable : true
			},
			subMenu : {
				title : tradCms.subMenu,
				view : "menuConstruct",
				domId : "subMenu",
				activable : true
			},
			footer : {
				title : tradCms.footer,
				view : "blockCms",
				activable : true,
				img : assetPath+"/cmsBuilder/img/configFooter.gif"
			}
		}, 
		options: {
			configOne : {
				img : assetPath+"/cmsBuilder/img/configOne.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : false,
					subMenu : false,
					footer : false
				}
			},
			configTwo : {
				img : assetPath+"/cmsBuilder/img/configTwo.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : false,
					subMenu : true,
					footer : false
				}
			},
			configThree : {
				img : assetPath+"/cmsBuilder/img/configThree.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : true,
					subMenu : false,
					footer : false
				}
			},
			configFour : {
				img : assetPath+"/cmsBuilder/img/configFour.png",
				map: {
					header : {
						banner : false,
						menuTop : true
					},
					menuLeft : true,
					subMenu : true,
					footer : false
				}
			},
			configFive : {
				img : assetPath+"/cmsBuilder/img/configFive.png",
				map: {
					header : {
						banner : false,
						menuTop : false
					},
					menuLeft : true,
					subMenu : false,
					footer : false
				}
			}
		},
		buttonValue:{},
		//btnNavigation: [],
		//btnNavigationTrue: [],
		listInput: {
			logo : {
				"height" : {
					label : tradCms.height,
					type : "text"
				},
				"width" : {
					label : tradCms.width,
					type : "text"
				},
				"heightMin" : {
					label : tradCms.heightXs,
					type : "text"
				},
				"widthMin" : {
					label : tradCms.widthXs,
					type : "text"
				},
				labelString : false,
				icon : false,
				spanTooltip : false,
				labelClass : false,
				class : false
			},
			app : {
				"icon" : {
					label : tradCms.showIcon,
					type : "select", 
					options : ["false", "true"]
				},
				"label" : {
					label : tradCms.showLabel,
					type : "select", 
					options : ["false", "true"]
				},
				"spanTooltip" : {
					label : tradCms.showTooltip,
					type : "select", 
					options : ["false", "true"]
				},
				"dropdownFullWidth" : {
					label : tradCms.dropdownfullwidth,
					type : "select", 
					options : ["false", "true"]
				},
			},
			userProfil : {
				"img" : {
					label : tradCms.showImage,
					type : "select", 
					options : ["false", "true"]
				},
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				},
				"name" : {
					label : tradCms.showName,
					type : "select",
					options : ["false", "true"]
				},
				label : false,
				icon : false,
				labelClass: false,
				class: false
			},
			cotools : {
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				}
			},
			editcms : {
				"dashboard" : {
					label : tradCms.showDashboard,
					type : "select", 
					options : ["false", "true"]
				}
			},
			searchBar : {
				"dropdownResult" : {
					label : tradCms.displayResultsDropDown,
					type : "select",
					options : ["false", "true"]
				},
				"label" : {
					label : tradCms.showLabel,
					type : "select", 
					options : ["false", "true"]
				},
			}
		},
		formOpt : {
			"label" : {
				label : tradCms.buttonLabel,
				type : "text"
			},
            "icon" : {
				label : tradCms.buttonIcon,
				type : "selectFontAwesome",
				options: Object.keys(fontAwesome).map(function(key, value) {
					return key
				})
			},
			"spanTooltip" : {
				label : tradCms.tooltip,
				type : "text"
			},
			"labelClass" : {
				label : tradCms.buttonLabelClass,
				type : "text"
			},
			"class" : {
				label : tradCms.ButtonClass,
				type : "text"
			},
			"adminOnly" : {
				label : tradCms.visibleToAdminOnly,
				type : "select", 
				options : ["false", "true"]
			},
		},
		views : {
			init : function(){
				var str="";
				str+=costumizer.htmlConstruct.views.helper()+
				 	"<div class='config-table-menu col-xs-12 margin-bottom-50'>";
							/*"<h2 class='costumizer-title'>Configuration of menus</h2>";*/
							$.each(costumizer.htmlConstruct.options[costumizer.htmlConstruct.keyConf].map, function(e , v){
				str+=			costumizer.htmlConstruct.views.siteMap(e, e, v);
							});
				str+="</div>"; 
				return str;
			},
			configOptions : function(showAll){
				var str="";
				$.each(costumizer.htmlConstruct.options, function(e, v){
					var activeClass=(e==costumizer.htmlConstruct.keyConf) ? " active" : " notActive";
					var sizeFlexClass=(!notEmpty(showAll)) ? "col-md-4 col-sm-6 col-xs-12" : "";
					var displayHide = (e==costumizer.htmlConstruct.keyConf || notEmpty(showAll)) ? "" : "display: none;";
					str+='<div class="parentSitemapOptions '+activeClass+' '+sizeFlexClass+'" style="'+displayHide+'">'+
						'<div class="box-mockup configOptions" data-key="'+e+'">'+
							'<img  src = "'+baseUrl+v.img+'" class="img-responsive"/>'+
							'<div class="box-content">'+
								'<h3 class="title">'+
									e+
								'</h3>';
					if(e==costumizer.htmlConstruct.keyconf)
					str+=		'<span class="post">'+tradCms.mapActivated+'</span>';
					str+=	'</div>'+
						'</div>'+
					'</div>';
				});
				return str;
			},
			helper : function(){
				var str='<div class="pages-toolbar padding-15">'+
							'<div class="explain-admin-costumizer col-xs-12 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-1"> '+
								'<h3>'+tradCms.sitemapCurrentlyActivated+'</h3>'+
								this.configOptions();
					str+=		'<div class="col-xs-12 text-center">'+
									'<a href="javascript:;" class="showAllSitemapOptions">'+
										'<i class="fa fa-caret-down"></i>'+tradCms.seeFeasibleWebsiteModels+
									'</a>'+
									'<h5>'+tradCms.enableDisableFollowingMenuSections+'.</h5>'+
									'<span>'+
										'<i class="fa fa-angle-double-left"></i> '+tradCms.WarningChangeOfConfiguration+' <i class="fa fa-angle-double-right"></i>'+
									'</span>'+
									'<label type="submit" class="icon-info badge"><i class="fa fa-info-circle"></i></label>'+
								'</div>'+
							'</div>	'+
						'</div>';
				
				return str;
			
			},
			siteMap : function(pathMap, key, activate, level=0){
				if (typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key) != "undefined") {
					if (typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated") != "undefined" && typeof jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated") == "boolean") {
						activate = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, key+".activated");
					}
				}
				if(typeof jsonHelper.getValueByPath(costumizer.htmlConstruct.map, pathMap) != "undefined"){
					var objMap= jsonHelper.getValueByPath(costumizer.htmlConstruct.map, pathMap);
					var str="";
					var containerClass=(typeof objMap.containerClass != "undefined") ? objMap.containerClass : "editMenuCostumizer col-xs-12"; 
					str+=  "<div class='"+containerClass+" level-tree-"+level+"'>";
							if(typeof objMap.title != "undefined"){
								classTit=( typeof objMap.titleClass != "undefined") ? objMap.titleClass : "";
					str+=		"<div class='"+classTit+" titleSection'>"+
									"<span>"+objMap.title+"</span>";
								if(typeof objMap.activable != "undefined" && objMap.activable){
									attrDisabled=(objMap.activable=="disabled")	? " disabled" : "";
					str+=			'<input type="checkbox" name="section-sitemap" value="'+activate+'" data-type="'+key+'" data-size="mini" data-label-width="1" data-on-text="Activé" data-off-text="Désactivé" data-handle-width="1" '+attrDisabled+'/>';
								}
					str+=		"</div>";
							}
							if(typeof objMap.subList != "undefined"){
								level++;
								$.each(objMap.subList, function(e, v){
									let activableValue=jsonHelper.getValueByPath(costumizer.htmlConstruct.options[costumizer.htmlConstruct.keyConf].map, key+"."+e);
					str+=			costumizer.htmlConstruct.views.siteMap(pathMap+".subList."+e, key+"."+e, activableValue, level);
								});
								
							}
							else if(typeof objMap.view != "undefined"){
								let params=(typeof objMap.params != "undefined") ? objMap.params : null;
					str+=		costumizer.htmlConstruct.views[objMap.view]("htmlConstruct."+key, objMap.params);
							}
					return str+= "</div>";
				}

			},
			blockCms : function(key){
				var str= 	`<span class="col-xs-12 badge comming-soon-badge">
								<div class='col-xs-12 padding-10 manageMenusCostum'>
								<div class="padding-10">
									<a href="#" onclick="costumizer.actions.showTutorial(costumizer.htmlConstruct.map.footer.img,tradCms.howusefooter)" class="cursor-hand" style="text-transform: none;">${tradCms.howusefooter}</a>
								</div>
								
								<!-- <button class="btn btn-primary custom-footer" data-path="${key}"></button> -->
						</div>`;
				return str;
			},
			menuConstruct : function(pathJson, params){
				var obj= jsonHelper.getValueByPath(costumizer.obj,pathJson);
				var str="";
				str+= 	"<div class='padding-10 manageMenusCostum sortable-parent-wrapper col-xs-12'>";
				str+=		'<div class="sortable-list col-xs-12 no-padding" data-ref="'+pathJson+'.buttonList">';
								if(notNull(obj) && typeof obj.buttonList != "undefined"){
									$.each(obj.buttonList, function(e,v){
										str+=costumizer.htmlConstruct.views.buttons.view(pathJson+".buttonList", e, v, "draggable col-xs-12", true);
									});
								}
				str+=   	`</div>
						 	<div class='button-menu-costumizer button-show-list text-center'>
								<i class='fa fa-plus'></i> ${tradCms.addButton || "Ajouter un boutton"}
							</div>	
						</div>`;
				return str;
			},
			// footerConstruct : function(){
			// 	setTimeout(function () {
			// 			if (costum.htmlConstruct.footer.activated == false) {
			// 				$(".footer-cms").html("")
			// 				$(".block-footer").remove()
			// 			}else{
			// 				let required = {
			// 					displayFooter : true
			// 				}
			// 				ajaxPost(
			// 					null, 
			// 					baseUrl+"/costum/blockcms/loadfootercms",
			// 					required,
			// 					function(data){
			// 						if (data.footerCMS) {
			// 							$(".footer-cms").html(data.html) 
			// 							var scrollDOm=($(".cmsbuilder-center-content").length > 0) ? ".cmsbuilder-center-content" : 'html, body'; 
			// 							if($(".footer-cms").length > 0){
			// 								$(scrollDOm).animate({scrollTop:$("#all-block-container").height()}, 100, 'easeInSine');
			// 							}
			// 						}else{
			// 							// localStorage.setItem("parentCmsIdForChild", "tpl-footer");
			// 							cmsConstructor["blockParent"] = "tpl-footer"
			// 							$("#modal-blockcms").addClass("open")
			// 							$("#blocklist-search-text").parent().hide()
			// 							$("#blocklist-search-text").val("Template footer")
			// 							$("#blocklist-search-text").keyup()
			// 						}
			// 					},
			// 					{async:true}
			// 					);
			// 			}
			// 		},200)
			// },
			buttons : {
				view : function(keyMenu, keyButton, value, classes, edit){
					// @ThemeParams.mainMenuButtons est une entrée qui hérite du json présent dans co2/config/buttons/main.json
					if( typeof themeParams.mainMenuButtons[keyButton] != "undefined"
							&& typeof themeParams.mainMenuButtons[keyButton].costumizer != "undefined"){
						var entryBtn=themeParams.mainMenuButtons[keyButton];
						var labelBtnTranslate=(typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
						var statusBtn="";
						if(typeof entryBtn.connected != "undefined"){
							if(entryBtn.connected === true)
								statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forConnectedUser+"</span>";
							else
								statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forDisconnectedUser+"</span>";	
						}
						if(typeof entryBtn.restricted != "undefined")
							statusBtn="<span class='statusButtonMenu elipsis'>"+tradCms.forAdministrator+"</span>";
						var classLabelStatus=(notEmpty(statusBtn)) ? "labelStatusMenuButton" : "labelMenuButton";
						var str="<div class='button-menu-costumizer "+classes+"' data-key-menu='"+keyMenu+"' data-key-button='"+keyButton+"'>"+
								"<div class='iconMenuContains'><i class='fa fa-"+entryBtn.costumizer.icon+"'></i></div>"+
								"<div class='"+classLabelStatus+"'>"+
									"<span class='titleButtonMenu elipsis'>"+labelBtnTranslate+"</span>"+
									statusBtn+
								"</div>";
							if(edit)
								str+=costumizer.htmlConstruct.views.buttons.editTools(keyMenu, keyButton, labelBtnTranslate);
							if(notEmpty(value)){
								var dataPath = "";
								var selectValue = value.buttonList;
								/*if (keyButton == "add" && notEmpty(costumizer.obj.typeObj)) { 
									dataPath = ""+keyMenu+"."+keyButton+"";
									selectValue = costumizer.obj.typeObj;
								} else {*/ 
									dataPath = ""+keyMenu+"."+keyButton+".buttonList";
								//}

								if (keyButton == "xsMenu" && edit && typeof jsonHelper.getValueByPath(costumizer.obj, dataPath) != "undefined") {
									if (notEmpty(jsonHelper.getValueByPath(costumizer.obj, dataPath))) {
										str += '<div class="xsMenuButtonCollapse">'+
											'<button type="button" data-toggle="collapse" data-target=".collaspe'+keyMenu.replaceAll(".", "")+'" aria-expanded="false" aria-controls="collaspe'+keyMenu.replaceAll(".", "")+'"><i class="fa fa-bars"></i></button>'+
										'</div>';
									}
								}
								str+='<div class="sortable-list sortable-content col-xs-12 no-padding '+(keyButton == "dropdown" ? "sortable-dropdown" : "")+' '+(keyButton == "xsMenu" ? "xsMenuCollapse" : "")+' '+(keyButton == "xsMenu" && typeof jsonHelper.getValueByPath(costumizer.obj, dataPath) != "undefined" && notEmpty(jsonHelper.getValueByPath(costumizer.obj, dataPath)) ? "collapse collaspe"+keyMenu.replaceAll(".", "")+"" : "")+'" data-ref="'+dataPath+'" data-key="'+keyButton+'">';
								if( typeof selectValue != "undefined" && notNull(selectValue)){
									$.each(selectValue,function(k, v){
										str+=costumizer.htmlConstruct.views.buttons.view(dataPath, k, v, "col-xs-12", true);
									});
								}
								if(keyButton == "app" && edit/* || keyButton == "add"*/){
									str+='<div class="col-xs-12 add-page-in-nav no-padding" data-path="'+dataPath+'">'+
											'<div class="select-navigation-page col-xs-10 no-padding">'+costumizer.htmlConstruct.views.buttons.form.selectPageMenu(selectValue, keyButton)+'</div>'+
											'<button class="btn btn-success add-navigation-menu-btn col-xs-2"><i class="fa fa-plus"></i></button>'+
											'<button class="btn create-newpage costumizer-button-add"><i class="fa fa-plus"></i> '+tradCms.createNewPage+'</button>'+
										'</div>';
								}
								str+=`</div>`;
							}
						str+="</div>";
					}else{
						var str="<div class='button-menu-costumizer draggable "+classes+"' data-key-menu='"+keyMenu+"' data-key-button='"+keyButton+"'>"+
						// Si il y a des boutons indépendants créé via un costum on le génére ici avec la même map json que les btn présent co2/config/buttons/main.json
							 	"<span class='pull-left no-padding keyButton'>"+keyButton+"</span>"+
							 	costumizer.htmlConstruct.views.buttons.editTools(keyMenu, keyButton, "", true )+
								"<div class='sortable-list col-xs-12 no-padding sortable-subMenu sortable-content' data-ref='"+keyMenu+"."+keyButton+".buttonList' data-key="+keyButton+" data-path="+keyMenu+">";
								if (typeof value.buttonList != "undefined") {
									$.each(value.buttonList ,function(k, v){
										str+=costumizer.htmlConstruct.views.buttons.view(keyMenu+"."+keyButton+".buttonList", k, v, "col-xs-12", true);
									});
								}
							str+="</div>"+
						"</div>";
					}
					return str;
				},
				editTools : function(kMenu, kButton, title, onlyDelete){
					if(notEmpty(onlyDelete)) { 
						var str='<div class="pull-right toolsButtonEdit no-padding">'+
							'<a href="javascript:;" class="badge bg-blue editMenuBtn noLabel" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
								'<i class="fa fa-pencil"></i>'+
							'</a>'+		
							'<a href="javascript:;" class="removeMenuBtn badge bg-red noLabel margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
								'<i class="fa fa-trash"></i>'+
							'</a>'+
						'</div>';
						/*if (kMenu.split(".")[kMenu.split(".").length - 1] == "add") {
							var str='<div class="pull-right toolsButtonEdit no-padding">'+
								'<a href="javascript:;" class="removeMenuBtn badge bg-white text-red margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
									'<i class="fa fa-trash"></i>'+
								'</a>'+
								'<a href="javascript:;" class="editMenuBtn badge bg-white" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'" data-title="'+title+'">'+
									'<i class="fa fa-pencil"></i>'+
								'</a>'+
							'</div>';
						}*/
					}
					else{
						var str='<div class="col-xs-12 toolsButtonEdit">'+
								'<a href="javascript:;" class="editMenuBtn badge bg-blue" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'" data-title="'+title+'">'+
									'<i class="fa fa-pencil"></i> '+trad.edit+
								'</a>'+
								'<a href="javascript:;" class="removeMenuBtn badge bg-red margin-left-5" data-key-menu="'+kMenu+'" data-key-button="'+kButton+'">'+
									'<i class="fa fa-trash"></i> '+trad.delete+
								'</a>'+
							'</div>';
					}
					return str;
				},
				form : {
					init :  function(pathJson, keyButton) {
						var formOptions = {};
						//costumizer.htmlConstruct.btnNavigationTrue = [];
						var str = "";
						/*str += '<div class="title-edit-mockcostumizer.htmlConstruct.btnNavigationup-menu" style="display: flex; align-items: center; width: 100%; justify-content: center; padding: 5px; border-bottom: 1px solid #f5f5f5; margin-bottom: 5px">'+
								'<button class="btn btn-danger" id="btn-save-mockup-menu" data-path="'+pathJson+'.'+keyButton+'">'+tradCms.validate+'</button>'+
							'</div>';*/
						formOptions[keyButton] = JSON.parse(JSON.stringify(costumizer.htmlConstruct.formOpt));
						if (typeof costumizer.htmlConstruct.listInput[keyButton] != "undefined") {
							$.each(costumizer.htmlConstruct.listInput[keyButton], function(k, v) {
								if (v === false) {
									delete formOptions[keyButton][k];
								} else {
									formOptions[keyButton][k] = v;
								}
							})
						}
						str += costumizer.htmlConstruct.views.buttons.form.inputs(formOptions[keyButton], keyButton, pathJson);
						// if (typeof costumizer.htmlConstruct.btnNavigation === 'object' && costumizer.htmlConstruct.btnNavigation.length != 0) {
						// 	str += costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs();
						// }
						return str;
					},
					allMenuSelected : function(menu) {
						if (typeof menu == "object" && typeof menu != "undefined") {
							$.each(menu, function(key, value) {
								if (typeof key == "string") {
									if (key.charAt(0) == "#")
										costumizer.htmlConstruct.btnNavigation.push(key);
								}
								if (typeof value == "object") {
									costumizer.htmlConstruct.views.buttons.form.allMenuSelected(value);
								}
							})
						}
					},
					/*btnNavigationInputs: function() {
						costumizer.htmlConstruct.btnNavigationTrue = costumizer.htmlConstruct.btnNavigation;
						let html = "";
						html += `
						<div class="panel panel-default" id="panel-navigation-content">
							<div class="panel-heading" role="tab" id="btn-navigation-heading">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-btn-navigation" aria-expanded="true" aria-controls="collapse-btn-navigation">
										<span  class="fa fa-bars" aria-hidden="true"></span>Ajouter ou supprimer un bouton de navigation
									</a>
								</h4>
							</div>
							<div id="collapse-btn-navigation" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="btn-navigation-heading">
								<div class="panel-body" id="inputs-navigation-btn">
									<div id="form-group-navigation-menu">`;
									Object.values(costumizer.htmlConstruct.btnNavigation).forEach(function (value){
					    html+=      	`<div class="form-group menu-item">
											<label>${value}</label>
											<button class="btn btn-danger" id="remove-navigation-menu-true" data-value="true" data-path="buttonList.${value}" data-key="${value}"><i class="fa fa-times"></i></button>
										</div>`;
									})
						html+=  	`</div>
									<div id="select-navigation-page"></div>
									<button class="btn btn-success" id="add-navigation-menu-btn">Ajouter</button>
								</div>
							</div>
						</div>`;
						costumizer.htmlConstruct.btnNavigation = [];
						return html;
					},*/
					selectPageMenu : function(menuList/*, keyButton*/) {
						costumizer.htmlConstruct.btnNavigation = [];
						let elementNotSelected = []
						costumizer.htmlConstruct.views.buttons.form.allMenuSelected(menuList);
						let menuNotSelected = Object.keys(costumizer.obj.app).filter((value) => {
							return !Object.values(costumizer.htmlConstruct.btnNavigation).find(all => (all === value));
						});
						defaultSelect=(menuNotSelected.length > 0) ? tradCms.selectPage : tradCms.allPagesAreSelected;
						/*if (keyButton == "add") {
							$.each(typeObj, function(k, v){
								if (v.add == false) {
									elementNotSelected.push(k);
								}
							})
							menuNotSelected = elementNotSelected	
							mylog.log("srghosr", menuNotSelected)
						defaultSelect=(menuNotSelected.length > 0) ? tradCms.selectAnElement : tradCms.allElementsAreSelected;
						}*/			
						
						$("#add-navigation-menu-btn").addClass("disabled");
						var html = `<div class="select-menu-page-form">
								<div class="form-group">
									<select class="form-control navigation-menu-select-form">
										<option value="" selected><italic>${defaultSelect}</italic></option>`;
										if(menuNotSelected.length){
											$.each(menuNotSelected, function(e, value) {
									html += `<option value="${value}">${value}</option>`;
										});
									}
								html += `
									</select>
								</div>
								</div>`;
						return html;
					},
					inputs : function(value, keyButton, pathJson, keyPath="", level=0, title="") {
						let html = "";
						$.each(value, function(k, v) {
							let valInput = "";
							if (typeof costumizer.htmlConstruct.buttonValue != "undefined") {
								if (typeof costumizer.htmlConstruct.buttonValue[k] != "undefined") {
									valInput = costumizer.htmlConstruct.buttonValue[k];
								} else {
									if (typeof jsonHelper.getValueByPath(costum, "appConfig") != "undefined") {
										if (typeof jsonHelper.getValueByPath(costum, `appConfig.${pathJson.slice(14)}.${keyButton}`) != "undefined") {
											let path = jsonHelper.getValueByPath(costum, `appConfig.${pathJson.slice(14)}.${keyButton}`);
											valInput = path[k];
										} else if (typeof jsonHelper.getValueByPath(costum, `appConfig.mainMenuButtons.${keyButton}`) != "undefined") {
											let path = jsonHelper.getValueByPath(costum, `appConfig.mainMenuButtons.${keyButton}`);
											valInput = path[k];
										}
									}
								}
							}
							if (v.type == "text") {
								html +=
								`<div class="form-group input-group-sm form-edit-htmlConstruct-input">
									<label>${v.label}</label>
									<input type="text" class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}" value="${(typeof valInput != "undefined")? valInput:""}">
								</div>`
							} else if (v.type == "select") {
								html += 
								`<div class="form-group input-group-sm form-edit-htmlConstruct-input">
									<label>${v.label}</label>
									<select class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}">`;
										$.each(v.options, function(kOptions, vOptions) {
								html +=		`<option value="${vOptions}" ${(typeof valInput != "undefined" && valInput === parseBool(vOptions)) ? "selected" : ""}>${vOptions}</option>`;
										})
								html +=
									`</select>
								</div>`;
							} else if (v.type == "selectFontAwesome") {
								html += 
								`<div class="form-group input-group-sm form-edit-htmlConstruct-input">
									<label>${v.label}</label>
									<select class="form-control" name="${k}" data-path="${pathJson}" data-key="${keyButton}">`;
										$.each(v.options, function(kOptions, vOptions) {
								html +=		`<option value="${vOptions}" ${(typeof valInput != "undefined" && valInput === vOptions) ? "selected" : ""}><span><i class="fa fa-${vOptions.trim()}"></i> ${vOptions}</span></option>`;
										})
								html +=
									`</select>
								</div>`;
							}
							/*if (k === "generatePage") {
								if (notNull(costumizer.htmlConstruct.buttonValue.buttonList) && costumizer.htmlConstruct.buttonValue.buttonList != "") {
									if (typeof costumizer.htmlConstruct.buttonValue.buttonList != "undefined") {
										costumizer.htmlConstruct.views.buttons.form.allMenuSelected(costumizer.htmlConstruct.buttonValue.buttonList);
									}
								}
								// html += costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs();
								$(".add-new-menu-content").html("");
								$(`.add-new-menu-content.${pathJson.replaceAll(".", "")}`).append(costumizer.htmlConstruct.views.buttons.form.btnNavigationInputs());
							}*/
						})
						return html;
					},
					configOpenModal: function(pathBtn, keyBtn) {
						var pathParent = pathBtn+"."+keyBtn+".config";
						const thisCurrentConfigValue = jsonHelper.getValueByPath(costumizer.obj, pathParent)
						const pageData = {
							customPages : $.map( costumizer.obj.app, function( val, key ) {
								return {value: key,label: key}
							}),
							pageToModal: []
						}
						pageData.pageToModal = $.extend(true, [], pageData.customPages);
						if (typeof costumizer.coInputParamsData.allAapFormsContext != "undefined" && costumizer.coInputParamsData.allAapFormsContext) {
							$.each(costumizer.coInputParamsData.allAapFormsContext, (formKey, formVal) => {
								pageData.customPages.push({
									value : `#coformAap.context.${ costumizer.obj.contextSlug }.formid.${ formKey }.step.aapStep1.answerId.new`,
									label : `#coformAap (${ formVal.name })`
								})
								pageData.pageToModal.push({
									value : `/survey/answer/answer/id/new/form/${ formKey }`,
									label : `Nouvelle réponse au formulaire <b>${ formVal.name }</b>`
								})
							});
						}
						new CoInput({
							container: "#configOpenModalEdition .input-wrapper",
							inputs: [
								{
									type : "select",
									options : {
										name : "showLabel",
										label : "Afficher le label",
										options : [{value: "true", label: "Oui"}, {value: "false", label: "Non"}],
										defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.showLabel ? thisCurrentConfigValue.showLabel : "true"
									}
								},
								{
									type : "select",
									options : {
										name : "showIcon",
										label : "Afficher l'icône",
										options : [{value: "true", label: "Oui"}, {value: "false", label: "Non"}],
										defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.showIcon ? thisCurrentConfigValue.showIcon : "true"
									}
								},
								{
									type: "inputSwitcher",
									options: {
										name: "typeUrl",
										label: `Pour le boutton`,
										class: "urlOptions ",
										tabs: [
											{
												value: "internalLink",
												label: "Lien interne",
												inputs: [
													{
														type: "select",
														options: {
															name: "button",
															isSelect2: true,
															label: `Page`,
															options: pageData.customPages,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.button ? thisCurrentConfigValue.button : null
														}
													}
												]
											},
											{
												value: "externalLink",
												label: "Lien externe",
												inputs: [
													{
														type: "inputSimple",
														options: {
															name: "button",
															viewPreview: true,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.button ? thisCurrentConfigValue.button : null
														}
													}
												]
											},
											{
												value: "insideModal",
												label: "Lien interne (Modal)",
												inputs: [
													{
														type : "select",
														options : {
															name : "button",
															isSelect2 : true,
															label : `Page`,
															options : pageData.pageToModal,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.button ? thisCurrentConfigValue.button : null
														}
													},
													{
														"type": "groupButtons",
														"options": {
															"name": "getFirstStepOnly",
															"label": "Récuperer l'etape un seulement",
															"options": [
																{
																	"label": "Oui",
																	"value": true,
																	"icon": ""
																},
																{
																	"label": "Non",
																	"value": false,
																	"icon": ""
																}
															],
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.getFirstStepOnly ? thisCurrentConfigValue.getFirstStepOnly : false
														}
													},
													{
														type : "inputSimple",
														options : {
															name : "modalLabel",
															label : `Titre du modal`,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.modalLabel ? thisCurrentConfigValue.modalLabel : null
														}
													},
													{
														type : "inputSimple",
														options : {
															name : "modalBtnLabel",
															label : `Label du bouton`,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.modalBtnLabel ? thisCurrentConfigValue.modalBtnLabel : null
														}
													},
													{
														type: "select",
														options: {
															name : "modalBtnRedirection",
															label : `Lien de redirection`,
															isSelect2: true,
															options: pageData.customPages,
															defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.modalBtnRedirection ? thisCurrentConfigValue.modalBtnRedirection : null
														}
													}
												]
											},
										],
										defaultValue : thisCurrentConfigValue && thisCurrentConfigValue.typeUrl ? thisCurrentConfigValue.typeUrl : "internalLink"
									}
								}
							],
							onchange:function(name, value, payload){
								var finalPath = pathParent+"."+name;
								jsonHelper.setValueByPath(costumizer.obj, finalPath, value);
							},
							onblur: function(name, value, payload){
								var finalPath = pathParent+"."+name;
								costumizer.htmlConstruct.actions.change(finalPath);
							}
						})
					}
				}
			}
			
		},
		events :{ 
			bind : function(){
				$(".showAllSitemapOptions").off().on("click", function(){
					if($(this).hasClass("toHide")){
						$(this).removeClass('toHide');
						$(".parentSitemapOptions.notActive").fadeOut(600);
						$(this).html("<i class='fa fa-caret-down'></i> "+tradCms.seeFeasibleWebsiteModels);
					}else{
						$(this).addClass('toHide');
						$(".parentSitemapOptions.notActive").fadeIn(600);
						$(this).html("<i class='fa fa-caret-up'></i> "+tradCms.hideFeasibleWebsiteModels);
					}
				});
				costumizer.htmlConstruct.events.activateSection();
				//costumizer.htmlConstruct.events.addPageInNav();
				costumizer.htmlConstruct.events.draggable();
				costumizer.htmlConstruct.events.sortUpdate();
				costumizer.htmlConstruct.events.toolsButton();
				costumizer.htmlConstruct.events.changeSitemap()
			},
			toolsButton : function(){
				// Delete button in menu
				$(".toolsButtonEdit .editMenuBtn").off().on("click", function(){
					if(notNull($(this).data("construct")) && 
						typeof costumizer.htmlConstruct.actions.edit[$(this).data("construct")] == "function"){
						costumizer.htmlConstruct.actions.edit[$(this).data("construct")]($(this).data("key-menu"), $(this).data("key-button"), $(this).data("title"));
					}else if($(this).data("key-menu").indexOf("app.buttonList") > 0 && $(this).data("key-button").startsWith("#")){
						costumizer.pages.actions.save($(this).data("key-button"),costumizer.obj.app[$(this).data("key-button")], {path :$(this).data("key-menu")});
					}else {
						costumizer.htmlConstruct.actions.edit.common($(this).data("key-menu"), $(this).data("key-button"), $(this).data("title"));
					}
				});
				// Edit config button in menu
				$(".toolsButtonEdit .removeMenuBtn").off().on("click", function(){
					costumizer.htmlConstruct.actions.delete($(this).data("key-menu"), $(this).data("key-button"));
					$(this).parents().eq(1).fadeOut().remove();

					var objDelete = jsonHelper.getValueByPath(costumizer.obj, $(this).data("key-menu"));
					if (typeof objDelete != "undefined") {
						if (!notEmpty(objDelete)) {
							let pathParent = "";
							let allKey = $(this).data("key-menu").split(".");
							let keyHastag = allKey[allKey.length - 2];
							if (keyHastag.charAt(0) == "#") {
								for (let index = 0; index < allKey.length - 1; index++) {
									if (index == 0)
										pathParent = allKey[index];
									else
									pathParent +="."+allKey[index];
								}
								jsonHelper.setValueByPath(costumizer.obj, pathParent, true);
							}
						}
						let menuDeleted = $(this).data("key-button");
						$(".navigation-menu-select-form").append('<option value='+menuDeleted+'>'+menuDeleted+'</option>')
					}
				});
				// Add new button in menu 
				$(".button-menu-costumizer.button-show-list").off().on("click", function(){
					costumizer.htmlConstruct.actions.add();
				});
				$(".add-navigation-menu-btn").off().on("click", function(e) {
					e.stopImmediatePropagation();
					var selectorInput=$(this).parent().find(".navigation-menu-select-form");
					var pageSelected = selectorInput.val();
					var parentPath = $(this).parent().data("path");
					//let splitParentPath = parentPath.split(".")
					//let valueInsert = true;	

					if (notEmpty(pageSelected)) {
						costumizer.htmlConstruct.actions.addPageInMenu(parentPath, pageSelected, $(this).parent());
						
						selectorInput.find("option[value='"+pageSelected+"']").remove();

							//$(this).parent().data("path");
							//costumizer.htmlConstruct.btnNavigationTrue.push(menuSelected);	
							//$("#navigation-menu-select-form").val("");
							//jsonHelper.setValueByPath(costumizer.htmlConstruct.buttonValue, keyPath, true );
					} 
				});
				$(".create-newpage").off().on("click",function(){
					htmlConstructLink={path :$(this).parent().data("path"), domAppend : $(this).parent()};
					costumizer.pages.actions.save(null,null, htmlConstructLink);
				});

				$(".custom-footer").off().on("click", function (){	
					$(".save-costumizer").click()
					costumizer.htmlConstruct.views.footerConstruct()
					
				})
				$(".save-costumizer").on("mouseup", function (e){
					e.stopImmediatePropagation()
					if (Object.keys(costumizer.actions.prepData())[0] == "htmlConstruct.footer")
						costumizer.htmlConstruct.views.footerConstruct()
				})
			
			},
			/*addPageInNav : function(){
				$(".add-navigation-menu-btn").off().on("click", function(e) {
					e.stopImmediatePropagation();
					costumizer.htmlConstruct.views.buttons.form.selectPageMenu(costumizer.obj.app);
				});
			},*/
			updateButtons : function(){
				$("#form-mockup-menu-edit .form-edit-htmlConstruct-input input, #form-mockup-menu-edit .form-edit-htmlConstruct-input select").on("change", function(e) {
					e.stopImmediatePropagation();
					var objBtn = jsonHelper.getValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key"));
					if (typeof objBtn == "boolean"){
						jsonHelper.setValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key"), {});
					}
					jsonHelper.setValueByPath(costumizer.obj, $(this).data("path")+"."+$(this).data("key")+"."+$(this).attr("name"), ($(this).val() == "true" || $(this).val() == "false") ? parseBool($(this).val()) : $(this).val());

					costumizer.htmlConstruct.actions.change($(this).data("path"));
				});

			},
			sortUpdate : function() {
				let inc=0;
			      $(".sortable-list").each(function(){
				        sortable('.sortable-list')[inc].addEventListener('sortupdate', function(e) {
							e.stopImmediatePropagation();
				        	if($(e.detail.item).hasClass("dragNewBtn")){
				            	/*
									tODO : transformer le btn en btn draggable 
									=> [X] vue à remplacer : costumizer.htmlConstruct.views.buttons.view("htmlConstruct.menuLeft.buttonList", e, v, "draggable col-xs-12", true);
									=> [X] bind les events sur le nouveau html
									=> [X] ajouter l'entrée themeParams.mainMenuButtons[this.dataKey] à l'objet costumizer.obj
									=> ouvrir la modal right en edition du bouton pour le configurer (la même chose que "edit" click sur les boutons déjà présent)

				            	*/
				            	let finalContainerPath=$(e.detail.destination.container).data("ref");
				            	let buttonKey=$(e.detail.item).data("key-button");
								if (buttonKey === "app") {
									themeParams.mainMenuButtons[buttonKey]["label"] = true;
								}
				            	let paramsButton=themeParams.mainMenuButtons[buttonKey];
				            	if(typeof paramsButton.buttonList != "undefined")
				            		paramsButton.buttonList={};
				            	jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, finalContainerPath,buttonKey, paramsButton, e.detail.destination.index);
				            	$(e.detail.item).replaceWith(costumizer.htmlConstruct.views.buttons.view(finalContainerPath,buttonKey, paramsButton, "draggable col-xs-12", true));
				            	costumizer.htmlConstruct.events.toolsButton();
				            	costumizer.htmlConstruct.events.dragDestroy();
								let entryBtn = themeParams.mainMenuButtons[buttonKey];
								let title = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
								costumizer.htmlConstruct.actions.edit.common(finalContainerPath, buttonKey, title);
				            	//$(".button-menu-costumizer[data-key-menu='"+finalContainerPath+"'][data-key-button='"+buttonKey+"'] .toolsButtonEdit .editMenuBtn").trigger("click");
				            }else{
					            let moveObj=jsonHelper.getValueByPath(costumizer.obj,$(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
								if($(e.detail.origin.container).data("ref") != $(e.detail.destination.container).data("ref")){
									if ($(e.detail.item).data("key-button").charAt(0) == "#") {
										var dataKey = $(e.detail.destination.container).data("key");
										var obj = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.destination.container).data("path"));
										if (typeof obj != "undefined" && typeof dataKey != "undefined") {
											if (typeof obj[dataKey] != "undefined" && typeof obj[dataKey] != "object") {
												obj[dataKey] = {};
												obj[dataKey]["buttonList"] = {};
											}
										}
									}
									jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, $(e.detail.destination.container).data("ref"), $(e.detail.item).data("key-button"), moveObj, e.detail.destination.index);
				            		jsonHelper.deleteByPath(costumizer.obj, $(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
									let objOrigin = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.origin.container).data("ref"));
									if (typeof objOrigin != "undefined") {
										if (!notEmpty(objOrigin)) {
											let keyOrigin = $(e.detail.origin.container).data("key");
											let objOriginChange = jsonHelper.getValueByPath(costumizer.obj, $(e.detail.origin.container).data("path"));
											if (typeof objOriginChange != "undefined") {
												objOriginChange[keyOrigin] = true;
											}
										}
									}
									var path = `${$(e.detail.destination.container).data("ref")}.${$(e.detail.item).data("key-button")}`;
									var paramsButton = jsonHelper.getValueByPath(costumizer.obj, path);
									$(e.detail.item).replaceWith(costumizer.htmlConstruct.views.buttons.view($(e.detail.destination.container).data("ref"), $(e.detail.item).data("key-button"), paramsButton, "draggable col-xs-12", true));
									costumizer.htmlConstruct.events.draggable();
									costumizer.htmlConstruct.events.sortUpdate();
									costumizer.htmlConstruct.events.toolsButton();
				           		}else{
				           			var inc=e.detail.destination.index;
				           			if(e.detail.origin.index < e.detail.destination.index && e.detail.destination.index > 0 && e.detail.origin.index > 1)
				           				inc--;
				           			jsonHelper.deleteByPath(costumizer.obj, $(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
									jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, $(e.detail.origin.container).data("ref"),$(e.detail.item).data("key-button"), moveObj, inc);
								}
							}
							// Todo Pousser un peu dans  le up jusqu'au container parent ex:
							// costumizer.pathToUp.htmlConstruct.header.menuTop o||r costumizer.pathToUp.htmlConstruct.menuLeft
							var originRef = $(e.detail.origin.container).data("ref");
							var destinationRef = $(e.detail.destination.container).data("ref");
							if (typeof destinationRef !== "undefined" && notEmpty(destinationRef)) {
								if (typeof originRef === "undefined") {
									costumizer.htmlConstruct.actions.change(destinationRef);
								} else if (originRef == destinationRef) {
									costumizer.htmlConstruct.actions.change(originRef);
								}
							}
							if (typeof originRef !== "undefined" && notEmpty(originRef) && originRef !== destinationRef) {
								if (originRef.includes(destinationRef)) {
									costumizer.htmlConstruct.actions.change(destinationRef);
								} else {
									costumizer.htmlConstruct.actions.change(originRef);
								}
							}
							$(".sortable-subMenu").removeClass("subMenuSortableContainer");
				        });
						sortable('.sortable-list')[inc].addEventListener('sortstart', function(e) {
							var keyButtonItem = $(e.detail.item).data("key-button");
							$(".sortable-content").each(function() {
								if (keyButtonItem == $(this).data("key")) {
									$(this).removeClass("sortable-list").css("pointer-events", "none");
								}
							})
							if (keyButtonItem == "app") {
								$(".xsMenuCollapse").addClass("subMenuSortableContainer");
							} else if (keyButtonItem.charAt(0) == "#" || keyButtonItem == "openModalBtn") {
								$(".sortable-subMenu").each(function() {
									if (keyButtonItem == $(this).data("key") || $(this).data("ref").indexOf(keyButtonItem) !== -1) {
										$(this).removeClass("subMenuSortableContainer");
									} else {
										$(this).addClass("subMenuSortableContainer");
									}
								})
								$(e.detail.item).find('.sortable-subMenu').removeClass("subMenuSortableContainer");	
							}
							if ($(".sortable-dropdown").length > 0)
								$(".sortable-dropdown").addClass("subMenuSortableContainer");
				        });
						sortable('.sortable-list')[inc].addEventListener('sortstop', function(e) {
							$(".sortable-list").removeClass("subMenuSortableContainer");
							$(".sortable-content").addClass("sortable-list").css("pointer-events", "");
				        });
				        inc++;
			        });
			    //}
			},
			activateSection : function(){
				if(!$("[name='section-sitemap']").parent().hasClass("bootstrap-switch-container")){
					$("[name='section-sitemap']").off();
					$("[name='section-sitemap']").bootstrapSwitch();
					$("[name='section-sitemap']").on("switchChange.bootstrapSwitch", function (event, state) {
						pathInJson=$(this).data("type")
						if (state == true) {
							$(this).val(1);
							if(typeof jsonHelper.getValueByPath(costumizer.obj, "htmlConstruct."+pathInJson) == "undefined"){
								valueToInsert=jsonHelper.getValueByPath(htmlConstructParams, pathInJson);
								jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson, valueToInsert);
							}
							jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson+".activated", true);
							$(this).parents().eq(3).find(".manageMenusCostum").fadeIn(500);
						} else {
							jsonHelper.setValueByPath(costumizer.obj, "htmlConstruct."+pathInJson+".activated", false);
							$(this).val(0);
							$(this).parents().eq(3).find(".manageMenusCostum").fadeOut(500);
						}
						if (pathInJson != "footer"){
							costumizer.htmlConstruct.actions.change("htmlConstruct."+pathInJson);
						}		
					});
					$("[name='section-sitemap']").each(function(){
						if($.inArray($(this).val(),["true","1"]) >=0){
							$(this).bootstrapSwitch('state', true, true);
						}else{
							$(this).parents().eq(3).find(".manageMenusCostum").hide();
						}
					});
				}
				
			},
			dragDestroy : function(){


				//sortable(".sortable-list" , "destroy" );
				costumizer.htmlConstruct.events.draggable();	
			},
			draggable : function(){
				/*
					TODO Pouvoir editer bouger l'ensemble des boutons dans les sections des menus mais aussi entre section
					[ ] Bien finir la partie js simple de mouvement des boites
					[ ] A la fin du drag, mettre à jour l'entrée concerné 
						[ ] dans le bouton il y a les valeurs:
							data-key-menu (ex : costum.htmlConstruct.header.menuTop.left.buttonList)
							data-key-bouton (ex : app, userProfil, etc)
						[ ] Le supprime
				*/
				sortable(".sortable-list", {
		            placeholderClass: "ph-class",
		            hoverClass: "hover",
		            acceptFrom : ".sortable-list, .add-to-list-sort"
		        });
			},
			changeSitemap : function(callback){
				$(".parentSitemapOptions").off().on("click", function(){
					if(!$(this).hasClass("active")){
						$(".parentSitemapOptions").removeClass("active");
						$(this).addClass("active");
						var keyConf=$(this).find(".configOptions").data("key");
						costumizer.htmlConstruct.actions.changeSitemap(keyConf);
						if(typeof callback == "function")
							callback();
					}
				});
			}
		},
		actions: {
			isActivated: function(path){
				// La variable htmlConstruct.menuLeft.activated permet de désactiver ou activer le menu sans effacer l'ensemble de l'objet 
				menuVal=jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, path);
				if(notEmpty(menuVal) 
					&& (typeof menuVal.activated == "undefined" || menuVal.activated===true))
					return true;
				else
					return false;
			},
			init : function(){
				// Variable utilisé pour le backup au cas où l'utilisateur effectue des changemets et veut restaurer l'ancien plutot que de sauvegarder  
				//costumizer.htmlConstruct.copy=$.extend({},costumizer.obj);
			},
			cancel : function(/*callback*/){

				if(costumizer.liveChange){
					var pathValueToUpdate={};
					var iteration = 0;			
					$.each(costumizer.actions.pathToValue, function(e, v){
						iteration++;		
						costumizer.updateComponentSocket({functionName: "updateObjAfterChange", data: {path: v, value: jsonHelper.getValueByPath(costum,v)}});
						if(v.indexOf("htmlConstruct") >= 0){
							var valueToUp=jsonHelper.getValueByPath(costum,v);
							if (typeof valueToUp != "undefined") {
								if(typeof valueToUp.buttonList != "undefined" && !notEmpty(valueToUp.buttonList))
								valueToUp="$unset";
							}
							pathValueToUpdate[v]=valueToUp;
						}
					});
					if (iteration == costumizer.actions.pathToValue.length) 
						costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costum.htmlConstruct);
					//if(typeof callback == "function") callback();
				}
			},
			change : function(path){
				costumizer.actions.change(path);
				if(costumizer.liveChange){
					var pathValueToUpdate={};
					$.each(costumizer.actions.pathToValue, function(e, v){
						var valueToUp=jsonHelper.getValueByPath(costumizer.obj,v);
						if (typeof valueToUp != "undefined") {
							if(typeof valueToUp.buttonList != "undefined" && !notEmpty(valueToUp.buttonList))
							valueToUp="$unset";
						}
						pathValueToUpdate[v]=valueToUp;
					});
					costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
				}

			},
			prepData : function(data){
				$.each(data, function(k, v){
					if (typeof v != "undefined") {
						if(typeof v.buttonList != "undefined" && !notEmpty(v.buttonList))
						data[k]="$unset";
					}
				});
				return data;
			},
			changeSitemap : function(keyConf){
				costumizer.htmlConstruct.keyConf=keyConf;
				$.each(costumizer.htmlConstruct.options[keyConf].map, function(e, v){
					if(e == "header"){
					 	if(typeof costumizer.obj.htmlConstruct[e] == "undefined") costumizer.obj.htmlConstruct[e]={};
					 	$.each(v, function(k, sub){
					 		if(sub === true ){
								//if(typeof costumizer.obj.htmlConstruct[e][k] == "undefined"){
								if(k=="menuTop")
									costumizer.obj.htmlConstruct[e][k]={left:{"buttonList" : {logo : true, app : {label:true, icon : false, spanTooltip:false, buttonList:{}}}}, center:{"buttonList" : {}}, right:{"buttonList" : {}}};	
								else
									costumizer.obj.htmlConstruct[e][k]={"buttonList" : {}};
								costumizer.obj.htmlConstruct[e][k].activated=true;
							}else if(sub === false && typeof costumizer.obj.htmlConstruct[e][k] != "undefined"){
							 	costumizer.obj.htmlConstruct[e][k].activated=false;
							}
							costumizer.htmlConstruct.actions.change("htmlConstruct."+e+"."+k);
					 	});
					}else if(v === true ){
						if(typeof costumizer.obj.htmlConstruct[e] == "undefined"){
							objList={}; 
							if(e=="menuLeft"){
								if(!costumizer.htmlConstruct.actions.isActivated("header.menuTop"))
									objList.logo=true;
								objList.app={"buttonList":{}};
							}	
							costumizer.obj.htmlConstruct[e]={"buttonList" : objList};
							
						}
						if(keyConf=="configFive"){
							costumizer.obj.htmlConstruct[e].class="toggleAll";
							if(typeof costumizer.obj.htmlConstruct[e].buttonList.app != "undefined")
								costumizer.obj.htmlConstruct[e].buttonList.app.spanTooltip=false;
						}
						costumizer.obj.htmlConstruct[e].activated=true;
						costumizer.htmlConstruct.actions.change("htmlConstruct."+e);
					 }else if(v === false && typeof costumizer.obj.htmlConstruct[e] != "undefined"){
					 	costumizer.obj.htmlConstruct[e].activated=false;
						costumizer.htmlConstruct.actions.change("htmlConstruct."+e);
					}
					
				});
				costumizer.actions.update(false);
			},
			replacePage: function(parentPath, oldKey, newkey){
				$(".button-menu-costumizer[data-key-menu='"+parentPath+"'][data-key-button='"+oldKey+"']").replaceWith(costumizer.htmlConstruct.views.buttons.view(parentPath, newkey, true, "col-xs-12", true));
				costumizer.htmlConstruct.events.bind();
			
			},
			addPageInMenu: function(parentPath, pageSelected, $domAppendBefore){
				posPage=(notEmpty(jsonHelper.getValueByPath(costumizer.obj, parentPath))) ? Object.keys(jsonHelper.getValueByPath(costumizer.obj, parentPath)).length :0;
				jsonHelper.insertKeyValueByPathAndInc(costumizer.obj, parentPath, pageSelected, true, posPage);
				costumizer.htmlConstruct.actions.change(parentPath);
				$domAppendBefore.before(costumizer.htmlConstruct.views.buttons.view(parentPath, pageSelected, true, "col-xs-12", true));
				costumizer.htmlConstruct.events.bind();
			},
			findIfKeyPageExist: function(obj, element, path = "", paths = []) {
				for (const key in obj) {
					if (obj.hasOwnProperty(key)) {
						const currentPath = path ? `${path}.${key}` : key;
						if (key === element) {
							const splitPath = currentPath.split(".");
							splitPath.pop();
							paths.push(splitPath.join("."));
						}
						if (typeof obj[key] === "object") {
							costumizer.htmlConstruct.actions.findIfKeyPageExist(obj[key], element, currentPath, paths);
						}
					}
				}
				return paths;
			},
			renameKeyPage: function (obj, oldKey, newKey) {
				var res = {};
				Object.keys(obj).forEach(function(key){
					var value = obj[key],
						key = (key==oldKey) ? newKey : key;
					res[key] = value;
				})
				return res;
			},
			editPageInMenu: function(oldKey, newKey, paths) {
				if (notEmpty(paths)) {
					var pathValueToUpdate = {};
					Object.values(paths).forEach(function(value) {
						if (oldKey != newKey) {
							var obj = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, value);
							var newObj = costumizer.htmlConstruct.actions.renameKeyPage(obj, oldKey, newKey);
							jsonHelper.setValueByPath(costumizer.obj.htmlConstruct, value, newObj);

						}
						var valueToUp = jsonHelper.getValueByPath(costumizer.obj.htmlConstruct, value);
						pathValueToUpdate["htmlConstruct."+value] = valueToUp;
					})
					costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
				}
			},
			refreshmenu : function(pathToUp, obj, isSocket = true){
				var reloadMenu=[];
				$.each(pathToUp, function(e, v){
					if(e.indexOf("htmlConstruct") >= 0 && v != "$unset"){
						var pathJson="";
						var alreadyRefresh=false;
						$.each(["menuLeft","header.menuTop.left","header.menuTop.center", "header.menuTop.right", "menuRight", "menuBottom", "subMenu","header.menuTop"], function(k,menu){
							if(e.indexOf(menu) >= 0){
								pathJson=menu;
								if($.inArray(menu, reloadMenu) >= 0)
									alreadyRefresh=true;
									reloadMenu.push(menu);
							}
						});
						if(!alreadyRefresh){
							var menuSelect =jsonHelper.getValueByPath(themeParams, pathJson+".id");
							var domMenu = typeof  menuSelect == "undefined" ? pathJson : menuSelect;
							
							$(domMenu).addClass("sp-is-loading");
							if((typeof v =="string" && v==='$unset') || (typeof v != "undefined" && typeof v.activated != "undefined" && !v.activated)){
								$("#"+domMenu).hide();
								coInterface.initHtmlPosition();
							}
							else{
								var paramsViewMenu=jsonHelper.getValueByPath(obj, pathJson);
								ajaxPost(
									null, 
									baseUrl+"/"+moduleId+"/cms/refreshmenu", 
									{
										menuConfig:paramsViewMenu,
										path: pathJson,
										pagesConfig : costum.appConfig.pages,
										connectedMode : costumizer.connectedMode
									},
									function(data){
										if($('#'+domMenu).length <= 0){
											$(".cmsbuilder-content-wrapper").append(data);
										}else{
											if(!$('#'+domMenu).is(":visible"))
												$('#'+domMenu).show();
											$('#'+domMenu).replaceWith(data);
										}
										
										if(typeof costum.typeObj != "undefined")
											costum.initTypeObj(costum.typeObj);
											themeObj.init();

										coInterface.initHtmlPosition();
										coInterface.bindLBHLinks();
										costumizer.events.editMenu();
										costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
										costumizer.updateComponentSocket({functionName:"revealFocus"});
									},
									null, 
									"html"
								);
							}
						}
					} else {
						if (v === "$unset") {
							jsonHelper.deleteByPath(costumizer.obj, e);
							if (!notEmpty(jsonHelper.getValueByPath(costum,e)) || typeof jsonHelper.getValueByPath(costum,e) == "undefined")
								jsonHelper.deleteByPath(costum, e);
						}
					}
				});
				if (isSocket === true) {
					costumizer.updateComponentSocket({functionName: "refreshmenu", data: {pathToUp: pathToUp, obj: obj, isSocket: false}})
				}
			},
			callback : function(data){
				costumizer.htmlConstruct.actions.refreshmenu(data.params, costum.htmlConstruct);
			},
			edit : {
				common : function(pathJson, keyButton, title){
					// alert("pathJson de l'entrée dans costum:"+pathJson+"// Clé du bouton :"+keyButton+" Travail à réaliser décris dans @costumizer.htmlConstruct.actions.edit.common");
					
					/* Todo 
						[ ] récupérer les valeurs du bouton à éditer 
							Liste non exhaustive des params à mettre en place 
							[] Name 
							[] icon 
							[] href - Pages ? 
							[] visible non visible en mode xs -sm - md
							[] Autres class style etc etc 
						[ ] Afficher le formulaire 
					*/
					var value= jsonHelper.getValueByPath(costumizer.obj,pathJson+"."+keyButton);
					costumizer.design.activeObjCss = costumizer.design.events.generateObj(pathJson+"."+keyButton, keyButton);
					
					costumizer.htmlConstruct.buttonValue=(typeof value == "object") ? jQuery.extend(true,{}, value) : value;
					if(typeof costumizer.htmlConstruct.buttonValue != "object" && 
						(costumizer.htmlConstruct.buttonValue===true || costumizer.htmlConstruct.buttonValue==="true") && 
						 typeof themeParams.mainMenuButtons[keyButton])
						costumizer.htmlConstruct.buttonValue=jQuery.extend(true,{},themeParams.mainMenuButtons[keyButton]);
					var $subView=$(`<div id="form-mockup-menu-edit" style="padding:5px;">
							${costumizer.htmlConstruct.views.buttons.form.init(pathJson, keyButton)}
							${(keyButton == "openModalBtn") ? "<div class='design-style-for-editBtn dark' id='configOpenModalEdition'><div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'></div></div>" : ""}
							<div class="design-style-for-editBtn dark" id="designStyleForEditBtn">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>	
							</div>
						</div>`);
					costumizer.design.views.collapseView(costumizer.design.activeObjCss, $subView, "#designStyleForEditBtn #accordion");

					// coInput config for style editMenu
					var position = pathJson.match(/\b(right|left|center)\b/);
					if (pathJson.indexOf("menuLeft") !== -1) {
						var path = `css.menuLeft`;
					}else {
						var path = `css.menuTop.${(position) ? position[0] : ""}`;
					}
					var container = $subView.find(`#designStyleForEditBtn #inputs-${keyButton} .input-wrapper`);
					var inputs = costumizer.design.activeObjCss[keyButton]["inputs"];
					var payload = {
						path: path+"."+keyButton
					};
					var defaultValues = jsonHelper.getValueByPath(costumizer.obj, payload["path"]);
					var onchange = function(path, valueToSet, name, payload, value) {
						if(notEmpty(path) && notEmptyAllType(valueToSet))
							costumizer.design.actions.change(path,valueToSet,name);
					}
					cssHelpers.form.launch(container,inputs,payload,defaultValues,{},onchange);

					var configRight = {
						width:300,
						header:{
							title:`Edition - `+title,
							position:"top"
						}
					};
					if(!costumizer.liveChange)
						configRight.distanceToRight = 0;

					costumizer.actions.openRightSubPanel($subView, configRight);
					costumizer.htmlConstruct.events.updateButtons();
					if (keyButton == "openModalBtn") {
						var idConfigKeyButton = keyButton+"config"
						$subView.find("#configOpenModalEdition #accordion").append(`
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading${idConfigKeyButton}">
									<h4 class="panel-title">
										<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${idConfigKeyButton}" aria-expanded="true" aria-controls="collapse${idConfigKeyButton}">
											<span class="fa fa-external-link" aria-hidden="true"></span>${title}
										</a>
									</h4>
								</div>
								<div id="collapse${idConfigKeyButton}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${idConfigKeyButton}">
									<div class="panel-body" id="inputs-${idConfigKeyButton}">
										<div class="input-wrapper"></div>
									</div>
								</div>
							</div>
						`);
						costumizer.htmlConstruct.views.buttons.form.configOpenModal(pathJson, keyButton);
					}
				}
			},
			add : function(){
				var str="<div class='col-xs-12 add-to-list-sort'>";
					$.each(costumizer.allBtnMenuToDragged, function(e, v){
						if(typeof v.costumizer != "undefined")
							str+=costumizer.htmlConstruct.views.buttons.view("", e, v, "col-xs-6 dragNewBtn");					
					});
				str+="</div>";
				/* var configRight = {width : 150, title : tradCms.dragIt, subRight : false};
				if(costumizer.liveChange) configRight.subRight=true; */
					
				//costumizer.actions.openRightPanel(str, configRight);
				var configRight = {
					width:150,
					header:{
						title:tradCms.dragElement,
						position:"left"
					}
				};
				if(!costumizer.liveChange)
					configRight.distanceToRight = 0;
				costumizer.actions.openRightSubPanel(str, configRight)
					
			//	$(domSub+" .contains-view-admin").html(str);
				 sortable(".add-to-list-sort", {
						copy:false
						//appendTo : ".column-menu-costumizer"
						//connectToSortable : ".column-menu-costumizer"
					});
				 sortable(".sortable-list", {
			            placeholderClass: "ph-class",
			            hoverClass: "hover",
			            acceptFrom : ".sortable-list, .add-to-list-sort"
			        });
				//costumizer.htmlConstruct.events.draggable();
				
			},
			delete : function(pathJson, keyButton, change){
				// Todo Rien du tout juste à la sauvergarde 
				// jsonHelper.deleteByPath(costumizer.obj, pathJson+"."+keyButton);
				//if()
				// costumizer.htmlConstruct.actions.change(pathJson);
				/*mylog.log("sogi", pathJson);
				if (pathJson.split(".")[pathJson.split(".").length - 1] == "add"){ 
					costumizer.typeObj.actions.change(pathJson+".delete."+keyButton);
				}
				else { */
					jsonHelper.deleteByPath(costumizer.obj, pathJson+"."+keyButton);
					costumizer.htmlConstruct.actions.change(pathJson);
				//}
			}
		}
	},
	medias : {
		views : {
			initAjax : function(config){
				var url = "gallery/index/id/"+costum.contextId+"/type/"+costum.contextType+"/docType/image/contentKey/slider";
				ajaxPost(config.dom+" .contains-view-admin", baseUrl+'/'+moduleId+'/'+url, 
					{initGallery:true},
					function(){
						$("#breadcrumGallery").hide();
						$(".dropdown-add-file .dropdown-menu, .btn-add-folder, .dropdown-collection.btn-move-collection").remove();
						$(".dropdown-add-file .show-nav-add").attr("href", "javascript:dyFObj.openForm('addPhoto')");
					});

			}
		}
	},
	files : {
		views : {
			initAjax : function(config){
				var url = "gallery/index/id/"+costum.contextId+"/type/"+costum.contextType+"/docType/file";
				ajaxPost(config.dom+" .contains-view-admin", baseUrl+'/'+moduleId+'/'+url, 
					{initGallery:true},
					function(){
						$("#breadcrumGallery").hide();
						$(".dropdown-add-file .dropdown-menu, .btn-add-folder, .dropdown-collection.btn-move-collection").remove();
						$(".dropdown-add-file .show-nav-add").attr("href", "javascript:dyFObj.openForm('addPhoto')");
					});

			}
		}
	},
	fonts : {
		textTypingInit: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima neque quisquam magni, ratione molestiae natus deserunt provident repellat quo odit, vero illum error, possimus at minus rem corrupti optio. Voluptatibus.",
		fontNameSelectionned: "",
		fontPathSelectionned: "",
		uploadFontValue: {},
		fontActive: [],
		views : {
			init : function() {
				if (typeof costumizer.obj.fonts != "undefined" && notEmpty(costumizer.obj.fonts)) {
					costumizer.fonts.fontActive = costumizer.obj.fonts.filter(font => fontObj[font]).sort((font1, font2) => fontObj[font1].localeCompare(fontObj[font2]));
				}
				costumizer.fonts.fontNameSelectionned = Object.values(fontObj)[0];
				costumizer.fonts.fontPathSelectionned = Object.keys(fontObj)[0];
				if (notEmpty(costumizer.fonts.fontActive) && typeof fontObj[costumizer.fonts.fontActive[0]] != "undefined") {
					costumizer.fonts.fontNameSelectionned = fontObj[costumizer.fonts.fontActive[0]];
					costumizer.fonts.fontPathSelectionned = costumizer.fonts.fontActive[0];
				}
				var str = `
				<div class="font-manager-content">
					<div class="row font-manager-row">
						<div class="col-lg-8 col-md-12 col-sm-12 col-12 column-font-manager">
							<div class="font-review-content">
								<div class="typing-text-to-review">
									<div class="font-review-selected">
										<h2 path="${costumizer.fonts.fontPathSelectionned}" class="text-selectionned">${costumizer.fonts.fontNameSelectionned}</h2>
										<button class="btn btn-sm btn-check-font ${(costumizer.fonts.fontPathSelectionned == costumizer.fonts.fontActive[0]) ? "btn-remove-font" : "btn-select-font"}">${(costumizer.fonts.fontPathSelectionned == costumizer.fonts.fontActive[0]) ? `${tradCms.deleteFromSelection}` : `${tradCms.select}`}</button>
									</div>
									<div class="input-config" id="input-config-for-text-preview"></div>
									<div class="wrapper-content">
										<div class="title-wrapper-content"><span class="title-wrapper">${tradCms.previews}</span></div>
										<div class="wrapper">
											<p class="text-style">${costumizer.fonts.textTypingInit}</p>
										</div>
									</div>
								</div>
								<h2 class="title-see-more">${tradCms.style}</h2>
								<div class="see-more-about-text-preview">
									${costumizer.fonts.views.allCharacterView()}
									${costumizer.fonts.views.allFontWithWeight()}
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 col-sm-12 col-12 column-font-manager">
							${costumizer.fonts.views.fontList()}
						</div>
					</div>
				</div>
				`;
				return  str;
			},
			allCharacterView: function() {
				var allCharacter = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "`", "¬", "!", "\"", "£", "$", "%", "^", "&", "*", "(", ")", "-", "_", "=", "+", "{", "}", "[", "]", ":", ";", "'", "@", "#", "~", "\\", "|", "<", ">", ",", ".", "?", "/"];
				var str = "<div class='allcharacter-content'>"+
					"<ul>";
					$.each(allCharacter, function(key, value) {
						str += `<li><p class="char">${value}</p><span class="text-with-font-selected">${value}</span></li>`;
					})
				str += "</ul>"+
				"</div>";
				return str;
			},
			allFontWithWeight: function() {
				var weightArray = ["lighter", "italic", "bolder"];
				var str = "<div class='all-font-with-weight'>";
					$.each(weightArray, function(key, value) {
						str += `
						<div class='font-weight-item'>
							<h3 class="title-font-weight-item">${value}</h3>
							<p class="text-style" style="${(value == "italic") ? `font-style: ${value}` : `font-weight: ${value}`}">${costumizer.fonts.textTypingInit}</p>
						</div>
						`;
					})
				str += "</div>";
				return str;
			},
			fontList : function() {
				var str = "<div class='all-font-avalaible-content'>"+
					`<h3 class='title-font-active'>${tradCms.selectedFonts}</h3>`;
					str += costumizer.fonts.views.listFontsActive()+
					`<h3 class='title-list-font-avalaible'>${tradCms.listOfExistingFonts}</h3>`+
					"<div class='search-font-and-add-new'>"+
						`<input class='input-filter-font form-control' type='text' placeholder='${tradCms.typeToSearch}'>`+
						"<button class='btn-upload-font btn'><i class='fa fa-upload'></i></button>"+
					"</div>"+
					"<div class='list-font-avalaible'>"+
						"<ul id='list-all-font-avalaible-content'>";
						if (notEmpty(fontObj)) {
							$.each(fontObj, function(key, value) {
								str += `<li class="font-item" data-path="${key}" data-name="${value}" data-value="${value.split(' ').join('_')}">${(costumizer.fonts.fontActive.includes(key)) ? `<i class="fa fa-check font-selected"></i>` : ""}<span>${value}</span><p class="font-text text-style" style="font-family: '${value.split(' ').join('_')}'">${costumizer.fonts.textTypingInit}</p></li>`;
							})
						}
				str+= 	"</ul>";
					"</div>"+
				"</div>";

				return str;
			},
			listFontsActive: function() {
				var str = "<div class='font-active'>"+
					`<ul class='list-font-active' ${(!notEmpty(costumizer.fonts.fontActive)) ? " style='display: none;'" : ""}>`;
						if (notEmpty(costumizer.fonts.fontActive)) {
						$.each(costumizer.fonts.fontActive, function(key, value) {
							str += `<li class='font-active-item' data-path="${value}">
								<label><i class='fa fa-check'></i> ${fontObj[costumizer.fonts.fontActive[key]]}</label>
								<button class="btn-deselect-font-active btn"><i class='fa fa-times'></i></button>
							</li>`;
						})
					}
					str += "</ul>"+
					`<div class='font-active-empty'${(notEmpty(costumizer.fonts.fontActive)) ? " style='display: none;'" : ""}><p>${tradCms.notFontSelected}</p><small>${tradCms.doesntSelectFont}</small></div>`+
				"</div>";
				
				return str;
			}
		},
		events : {
			bind : function() {
				$(".font-review-content .text-style, .font-review-content .text-with-font-selected").css("font-family", "'"+costumizer.fonts.fontNameSelectionned.split(' ').join('_')+"'");
				$(".font-manager-content .input-filter-font").on("keyup", function(e) {
					e.stopPropagation();
					var value = $(this).val().toLowerCase();
					$("#list-all-font-avalaible-content li").filter(function() {
						$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
					});
				})

				$(".font-manager-content .search-font-and-add-new .btn-upload-font").on("click", function(e) {
					e.stopPropagation();
					bootbox.dialog({
						title: tradCms.fontUpload,
						className: "upload-font-manager-modal",
						message: "<div id='input-upload-font-content'></div>",
						buttons: {
							cancel: {
								label: trad.cancel,
								className: "btn-secondary"
							}
						}	
					})
					new CoInput({
						container : "#input-upload-font-content",
						inputs :[
							{
								type: "inputFileFont",
								options: {
									name: "uploadFont",
									label: tradCms.chooseFile,
									file: null,
									canDeleteFont: false,
									accept: [".ttf", ".otf"]
								}
							}
						],
						onchange:function(name, value, payload){
							if (typeof value == 'object') {
								var index = value.name.lastIndexOf(".");
								var extension = value.name.substring(index + 1);
								if ($.inArray(extension, ["ttf", "otf"]) >= 0) {
									$("#input-upload-font-content .btn-upload-font").removeClass("disabled");
									costumizer.fonts.uploadFontValue = value;
								} else {
									toastr.error(tradCms.fontTypeNotAccepted);
									$("#input-upload-font-content input[name='uploadFont']").val("");
									$("#input-upload-font-content .btn-upload-font").addClass("disabled");
								}
							}
						}
					});
					$("#input-upload-font-content").append(`<div class='btn-upload-font-content'><button class='btn-upload-font btn btn-success disabled'>${trad.save}</button></div>`);
					costumizer.fonts.events.saveFontUpload();
				})

				$(".font-review-content .btn-check-font").on("click", function() {
					var path = $(this).prev(".text-selectionned").attr("path");
					if (costumizer.fonts.fontActive.includes(path)) {
						$(".font-active .list-font-active .font-active-item[data-path='"+path+"']").remove();
						costumizer.fonts.fontActive = costumizer.fonts.fontActive.filter(element => element !== path);
						$(this).removeClass("btn-remove-font").addClass("btn-select-font").text(`${tradCms.select}`);
						$("#list-all-font-avalaible-content .font-item[data-path='"+path+"'] i.font-selected").remove();
					} else {
						costumizer.fonts.fontActive.push(path);
						var newFont = `
						<li class='font-active-item' data-path="${path}">
							<label><i class='fa fa-check'></i> ${fontObj[path]}</label>
							<button class="btn-deselect-font-active btn"><i class='fa fa-times'></i></button>
						</li>`;

						$(".font-active .list-font-active").append(newFont);
						costumizer.fonts.events.removeFontActiveBtn();
						$(this).removeClass("btn-select-font").addClass("btn-remove-font").text(`${tradCms.deleteFromSelection}`);
						$("#list-all-font-avalaible-content .font-item[data-path='"+path+"']").prepend("<i class='fa fa-check font-selected'></i>");
					}
					costumizer.fonts.events.refreshViewFontActive();
					costumizer.fonts.actions.change(costumizer.fonts.fontActive);
				});
				costumizer.fonts.events.clickOnFontAvalaible();
				costumizer.fonts.events.inputConfigForTextPreview();
				costumizer.fonts.events.removeFontActiveBtn();
			},
			clickOnFontAvalaible: function() {
				$(".font-manager-content .list-font-avalaible .font-item").off().on("click", function() {
					$(".font-manager-content .list-font-avalaible .font-item").removeClass("font-item-selected");
					$(".font-review-content .text-selectionned").attr("path", $(this).data("path")).text($(this).data("name"));
					$(".font-review-content .text-style, .font-review-content .text-with-font-selected").css("font-family", "'"+$(this).data("value")+"'");
					$(this).toggleClass("font-item-selected");
					if (costumizer.fonts.fontActive.includes($(this).data("path"))) {
						$(".font-review-content .btn-check-font").removeClass("btn-select-font").addClass("btn-remove-font").text(`${tradCms.deleteFromSelection}`);
					} else {
						$(".font-review-content .btn-check-font").removeClass("btn-remove-font").addClass("btn-select-font").text(`${tradCms.select}`);
					}
				}) 
			},
			inputConfigForTextPreview : function() {
				new CoInput({
					container : "#input-config-for-text-preview",
					inputs :[
						{
							type : "inputSimple",
							options: {
								name : "preview",
								placeholder: `${tradCms.typeHereToPreviewText}`
							}
						},
						{
                            type: "inputNumberRange",
                            options: {
                                name: "size",
								minRange: "10",
								maxRange: "100",
                                filterValue: cssHelpers.form.rules.checkLengthProperties, 
								defaultValue: "18px"
                            }
                        }
					],
					onchange:function(name, value, payload){
						if (name == "preview")
							(value != "") ? $(".font-manager-content .text-style").text(value) : $(".font-manager-content .text-style").text(costumizer.fonts.textTypingInit);
						if (name == "size")
							$(".font-review-content .text-style").css({"font-size": `${value}px`});
					}
				});
			},
			refreshViewFontActive: function() {
				if (!notEmpty(costumizer.fonts.fontActive)) {
					$(".all-font-avalaible-content .list-font-active").hide();
					$(".all-font-avalaible-content .font-active-empty").show();
					$(".font-review-content .btn-check-font").removeClass("btn-remove-font").addClass("btn-select-font").text(`${tradCms.select}`);
				} else {
					$(".all-font-avalaible-content .list-font-active").show();
					$(".all-font-avalaible-content .font-active-empty").hide();
				}	
			},
			removeFontActiveBtn: function() {
				$(".list-font-active .btn-deselect-font-active").on("click", function() {
					var path = $(this).parent(".font-active-item").attr("data-path");
					$(this).parent(".font-active-item").remove();
					if (costumizer.fonts.fontActive.includes(path)) {
						costumizer.fonts.fontActive = costumizer.fonts.fontActive.filter(element => element !== path);
						$("#list-all-font-avalaible-content .font-item[data-path='"+path+"'] i.font-selected").remove();
					} else {
						costumizer.fonts.fontActive.push(path);
						$("#list-all-font-avalaible-content .font-item[data-path='"+path+"']").prepend("<i class='fa fa-check font-selected'></i>");
					}
					if ($(".font-review-content .text-selectionned").attr("path") == path) {
						$(".font-review-content .btn-check-font").removeClass("btn-remove-font").addClass("btn-select-font").text(`${tradCms.select}`);
					}
					costumizer.fonts.events.refreshViewFontActive();
					costumizer.fonts.actions.change(costumizer.fonts.fontActive);
				})
			},
			saveFontUpload: function() {
				$("#input-upload-font-content .btn-upload-font").on("click", function(e) {
					e.stopPropagation();
					var pathFont = "/upload/communecter/"+costumizer.obj.contextType+"/"+costumizer.obj.contextId+"/file/"+$('#input-upload-font-content input[name="uploadFont"]').val().replaceAll("_", "").replaceAll(" ", "-");
					if (typeof fontObj[pathFont] != "undefined") {
						toastr.info(tradCms.fontalreadyExists);
						$("#input-upload-font-content input[name='uploadFont']").val("");
						$("#input-upload-font-content .btn-upload-font").addClass("disabled");
					} else {
						var fontValues = costumizer.fonts.uploadFontValue;
						if (fontValues != {}) {
							var index = fontValues.name.lastIndexOf(".");
							var extension = fontValues.name.substring(index + 1);
							var name = fontValues.name.replace(`.${extension}`, "").replaceAll("_", "").replaceAll(" ", "-");
							costumizer.design.events.addFont(fontValues);
							fontObj[pathFont] = name;
							var fontStyle = `
								@font-face {
									font-family: "${name.split(' ').join('_')}";
									src: url("${pathFont}");
								}
							`;
							$("#font-head-style").prepend(fontStyle);
							costumizer.fonts.fontActive.push(pathFont);
							costumizer.fonts.actions.change(costumizer.fonts.fontActive);
							$(".font-review-content .text-selectionned").attr("path", pathFont).text(name);
							$(".font-review-content .text-style, .font-review-content .text-with-font-selected").css("font-family", "'"+name.split(' ').join('_')+"'");
							$(".font-review-content .btn-check-font").removeClass("btn-remove-font").addClass("btn-select-font").text(`${tradCms.select}`);
							var fontUpload = `
							<li class='font-active-item' data-path="${pathFont}">
								<label><i class='fa fa-check'></i> ${fontObj[pathFont]}</label>
								<button class="btn-deselect-font-active btn"><i class='fa fa-times'></i></button>
							</li>`;
							$(".font-active .list-font-active").append(fontUpload);
							$("#list-all-font-avalaible-content").append(`<li class="font-item" data-path="${pathFont}" data-name="${name}" data-value="${name.split(' ').join('_')}">${(costumizer.fonts.fontActive.includes(pathFont)) ? `<i class="fa fa-check font-selected"></i>` : ""}<span>${name}</span><p class="font-text text-style" style="font-family: '${name.split(' ').join('_')}'">${costumizer.fonts.textTypingInit}</p></li>`);
							costumizer.fonts.events.clickOnFontAvalaible();
							costumizer.fonts.events.removeFontActiveBtn();
							costumizer.fonts.events.refreshViewFontActive();
							costumizer.fonts.uploadFontValue = {};
							costum.updateFontsCache(fontObj);
							costumizer.updateComponentSocket({functionName: "updateFontsCache", data: {fontObj: fontObj, pathFont: pathFont, nameFont: name}});
							$(".upload-font-manager-modal").modal("hide");
						}
					}
 					
				})
			}
		},
		actions : {
			change : function(value) {	
				jsonHelper.setValueByPath(costumizer.obj, "fonts", value);
				costumizer.actions.change("fonts");
			}
		}
	},
	design : {
		cssClone: {},
		activeObjCss : {},
		activePathDesign : "",
		fontUploadFiles: {},
		fontUploadExist: false,
		changeStyleNeedToInit : false,
		formOptions : {
			container : {
				inputs: [
					"background","height","width","display","alignItems","justifyContent","columnWidth","margin","padding","textAlign","hideOnDesktop","hideOnTablet","hideOnMobil"
				]
			},
			button : {
				inputs: [
					"background", "color", "fontSize", "fontWeight", "margin", "padding", "borderRadius", "border", "textTransform",
					{
						type: "section",
						options: {
							name: "hover",
							label: "hover",
							inputs: ["background","color","border"]
						}
					}
				]
			}
		},
		cssGeneralMap: {
			loader : {
				name :tradCms.loader,
				icon : 'spinner',
					inputs:[
						"background",
						{
							type:"section",
							options:{
								name:"ring1",
								label: tradCms.color+' 1',
								inputs:["color","borderWidth","height","width","top","left"]
							}
						},
						{
							type:"section",
							options:{
								name:"ring2",
								label: tradCms.color+' 2',
								inputs:["color","borderWidth","height","width","top","left"]
							}
						}
					]
			},
			progress : {
				name :tradCms.progress,
				icon : 'tasks',
				inputs:[
					{
						type:"section",
						options:{
							name:"bar",
							label:tradCms.bar,
							inputs:["background"]
						}
					},
					{
						type:"section",
						options:{
							name:"value",
							label:tradCms.value,
							inputs:["background"]
						}
					}
				]
			},
			menuTop : {
				name : tradCms.menuTop,
				icon : 'outdent',
				inputs: [
					"background", "padding", "boxShadow","hideOnDesktop","hideOnTablet","hideOnMobil"
				]
			},
			menuLeft : {
				name: tradCms.menuLeft,
				icon: 'outdent',
				inputs: [
					"background",
					"boxShadow",
					"marginTop",
				]
			},
			color : {
				name :tradCms.color,
				icon : 'paint-brush',
				inputs: [
					{
						type: "inputMultiple",
						options: {
							name: "multiColor",
							max: 10,
							inputs: [
								[
									{
										type: "select",
										options: {
											label: tradCms.type,
											name: "type",
											options: [
												"color1","color2","color3","color4","color5","color6","color7","color8","color9","color10"
												// "black","blue","brown","lightblue","lightpurple","darkblue","green","orange","red","yellow","yellow-k","purple","azure","pink","phink","dark","green-k","red-k","blue-k","nightblue","turq","green-poi","bg-color","label-color","border-color","color1","color2","color3","color4","color5"
											]
										}
									},
									"color"
								]
							]
						}
					}
				]
			},
			font: {
				name : tradCms.font,
				icon : "font",
				inputs : [
					{
						type: "selectFont",
						options: {
							name: "fontSelect",
							label: tradCms.selectFont
						}
					},
					{
						type: "inputFileFont",
						options: {
							name: "uploadFont",
							label: tradCms.fontUpload,
							file: null,
							accept: [".ttf"]
						}
					}
				]
			},
			title : {
				name: tradCms.title,
				icon: "header",
				inputs : [
					{
						type: "section",
						options: {
							name: "h1",
							label: "Texte principale",
							showInDefault: true,
							inputs: [
								"fontSize",
								"color",
								"textTransform",
								"fontFamily"
							]
						}
					},
					{
						type: "section",
						options: {
							name: "h2",
							label: "Texte secondaire",
							showInDefault: true,
							inputs: [
								"fontSize",
								"color",
								"textTransform",
								"fontFamily"
							]
						}
					},
					{
						type: "section",
						options: {
							name: "h3",
							label: "Texte tertiaire",
							showInDefault: true,
							inputs: [
								"fontSize",
								"color",
								"textTransform",
								"fontFamily"
							]
						}
					}
				]
			},
			text : {
				name: trad.text,
				icon: "text-height",
				inputs : [
					"fontSize",
					"color",
					"textTransform",
					"fontFamily"
				]
			},
			// link : {
			// 	name : tradCms.link ,
			// 	icon : 'link',
			// 	inputs: [
			// 		"fontSize",
			// 		"color",
			// 		"textTransform",
			// 		"fontFamily"
			// 	]
			// }
		},
		views : {
			init : function(){
				$("#modal-content-costumizer-administration").fadeOut(1000);
				$(".cmsbuilder-right-content").addClass("active")
				Object.keys(costumizer.design.cssGeneralMap).forEach(function(key) {
					costumizer.design.activeObjCss = {};
					if (key == "menuLeft") {
						if (!costumizer.obj.css.hasOwnProperty(key)) {
							if (!costumizer.obj.htmlConstruct.hasOwnProperty(key)) {
								delete costumizer.design.cssGeneralMap[key];
							}
						}
					}
				})
				return costumizer.design.views.css();
			},
			collapseView : function(objOptions, $form, parentClass) {
				$.each(objOptions, function(e, v){
					mylog.log('atao', e)
					$form.find(parentClass).append(`
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading${e}">
								<h4 class="panel-title">
									<a  role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse${e}" aria-expanded="true" aria-controls="collapse${e}">
										<span  class="fa fa-${v.icon}" aria-hidden="true"></span>${v.name}
									</a>
								</h4>
							</div>
							<div id="collapse${e}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${e}">
								<div class="panel-body" id="inputs-${e}">
									<div class="input-wrapper"></div>
								</div>
							</div>
						</div>
					`)
				});
			},
			css : function () {
				var objOptions = (notEmpty(costumizer.design.activeObjCss)) ? costumizer.design.activeObjCss : costumizer.design.cssGeneralMap ;
				var $form = $(`
					<form method="post" action="" enctype="multipart/form-data" id="costum-design">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>	
					</form>
				`)
				costumizer.design.views.collapseView(objOptions, $form, "#accordion");

				return $form;
				
			},
			loaderView : function() {
				var objLoader = costumizer.obj.css.loader;
				let bgLoader = objLoader.background; let ring1 = objLoader.ring1; let ring2 = objLoader.ring2;
				var str =
					'<a href="javascript:;" class="loader-costumizer" data-dom="#modal-content-costumizer-administration" data-title="Loader" data-space="loader"><div class="col-xs-12 text-center loader-contain" style="background-color: '+bgLoader+';"></div></a>';

				var objProgress = costumizer.obj.css.progress;
				let bar = objProgress.bar;
				let value = objProgress.value;
				var progress =
				'<div class="col-xs-12" style="padding: 20px 0">'+
					'<div class="progress" style="background-color: '+bar.background+';">'+
						'<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%; background-color: '+value.background+'">'+
							'<span class="sr-only">60% Complete</span>'+
							'<span class="progress-type">Progress</span>'+
						'</div>'+
					'</div>'+
				'</div>';

				$( "#collapseloader .panel-body" ).prepend(str);
				$( "#collapseprogress .panel-body" ).prepend(progress);
				$('style#cssLoader').html("");
				if (typeof(costumizer.obj.css.loader.loaderUrl) == "undefined") {
					$(".loader-contain").html(costumizer.loader.views.typeLoader("loading_modal"));
				}else {
					$(".loader-contain").html(costumizer.loader.views.typeLoader(costumizer.obj.css.loader.loaderUrl));
				}
				$(".loader-costumizer").off().on("click", function(e){
					e.stopPropagation()
					$(".cmsbuilder-toolbar-dropdown").removeClass("dropdown-active")
					var params = {
						space:$(this).data("save"),
						title:$(this).data("title"),
						savebutton:true
					}
					costumizer.actions.openCostumizer(params)
				});
			},
			generteColor : function(){
				var btnGenerate = `
					<fieldset>
						<legend>Géneration du couleur</legend>
							<style>
								fieldset {
									border: 1px solid #444;
									border-radius: 5px;
									margin-bottom: 20px;
									padding: 10px 20px;
								}
								
								legend {
									background-color: #444;
									color: white;
									padding: 5px 10px;
									border-radius: 5px;
									font-weight: bold;
									text-align: center;
									font-size: 15px;
								}

								.co-container-co .co-cards-co .co-card-co:nth-of-type(5) .co-canvas-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(4) .co-canvas-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(3) .co-canvas-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(2) .co-canvas-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(1) .co-canvas-co {
									height: 50px;
									width: 50px;
									border-radius: 2px;
									transition: 0.3s ease all;
								}

								.co-container-co .co-cards-co .co-card-co:nth-of-type(5) .co-hex-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(4) .co-hex-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(3) .co-hex-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(2) .co-hex-co, 
								.co-container-co .co-cards-co .co-card-co:nth-of-type(1) .co-hex-co {
									padding-top: 0.75rem;
									padding-bottom: 0.5rem;
									font-weight: 700;
									padding-left: 5px;
								}
								.co-container-co h2 {
									font-size: 1.8rem;
									word-spacing: 0.25rem;
									text-align: center;
								}
								.co-container-co .co-cards-co .co-card-co {
									width: 190px;
									margin: 10px 0;
									background-color: #fff;
									display: flex;
									border-radius: 5px;
									padding: 10px;
								}
								.co-container-co .co-cards-co .co-card-co:hover {
									box-shadow: 0 2px 12px #A3B2C2;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(1) .co-canvas-co {
									background-color: #93B8E3;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(1) .co-hex-co {
									color: #7C7C84;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(2) .co-canvas-co {
									background-color: #A3B2C2;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(2) .co-hex-co {
									color: #7C7C84;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(3) .co-canvas-co {
									background-color: #FBABA4;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(3) .co-hex-co {
									color: #7C7C84;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(4) .co-canvas-co {
									background-color: #7C7C84;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(4) .co-hex-co {
									color: #4C545F;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(5) .co-canvas-co {
									background-color: #AF4884;
								}
								.co-container-co .co-cards-co .co-card-co:nth-of-type(5) .co-hex-co {
									color: #7C7C84;
								}
								.co-container-co .co-btn-co {
									margin-bottom: 1.5rem;
									border: none;
									outline: none;
									background-color: #8e54c7;
									color: #fff;
									font-weight: 600;
									padding: 5px;
									border-radius: 5px;
									cursor: pointer;
									transition: 0.3s ease all;
									width: 100%;
								}
								.co-container-co .co-btn-co:hover {
									background-color: red;
								}
								.container:has(.co-container-co) {
									width: min-content !important;
								}

							</style>
						<div class="co-container-co">
							<div class="co-cards-co" style="display: none;">
								<div class="co-card-co">
									<div class="co-canvas-co"></div>
									<div class="co-hex-co"></div>
								</div>
								<div class="co-card-co">
									<div class="co-canvas-co"></div>
									<div class="co-hex-co"></div>
								</div>
								<div class="co-card-co">
									<div class="co-canvas-co"></div>
									<div class="co-hex-co"></div>
								</div>
								<div class="co-card-co">
									<div class="co-canvas-co"></div>
									<div class="co-hex-co"></div>
								</div>
								<div class="co-card-co">
									<div class="co-canvas-co"></div>
									<div class="co-hex-co"></div>
								</div>
							</div>
							<input type="text" id="baseColorInput" placeholder="#rrggbb ou nom" style="padding: 0.5rem; margin-bottom: 1rem; border: 1px solid #ccc; border-radius: 5px; width: 100%;">
							<br>
							<button class="co-btn-co" id="btnGenerate">
								<i class="fa fa-arrow-up" aria-hidden="true" style="margin-right:10px;"></i>
								Générer à partir de ce couleur
							</button>
							<p style="text-align: center;">ou</p>
							<button class="co-btn-co" id="btn">
								<i class="fa fa-refresh" aria-hidden="true" style="margin-right:10px;"></i>
								Générer aléatoire
							</button>
							<p style="text-align: center;">ou</p>
							<button class="co-btn-co" id="btn-palete">
								<i class="fa fa-th-large"" aria-hidden="true" style="margin-right:10px;"></i>
								Palette
							</button>
						</div>									
					</fieldset>
				`;

				$("#collapsecolor .panel-body").prepend(btnGenerate);
				
				
				const btn = document.getElementById('btn');
				const cards = document.querySelectorAll('.co-card-co');
				const btnBaseColor = document.getElementById('btnGenerate');
				const palete = document.getElementById('btn-palete');

				// Fonction pour générer les nuances avec TinyColor
				function generateShades(baseColor) {
					const shades = [];
					let hsl = tinycolor(baseColor).toHsl();

					// Générer 5 nuances en ajustant la luminosité
					for (let i = 1; i <= 5; i++) {
						hsl.l = 0.1 * i; // Ajuste la luminosité pour chaque nuance
						shades.push(tinycolor(hsl).toHexString());
					}
					return shades;
				}

				// Fonction pour appliquer les couleurs au div "id-palette-color"
				function updatePaletteColors(colors) {
					// Récupérer tous les enfants de #id-palette-color ayant la classe .draggabled-co
					const draggableDivs = document.querySelectorAll("#id-palette-color .draggabled-co");

					// Appliquer les couleurs générées à chaque div enfant
					draggableDivs.forEach((div, index) => {
						if (colors[index]) {
							div.style.backgroundColor = colors[index];
							div.setAttribute('data-color', colors[index]);
						}
					});
				}

				if(notNull(btn)){
					btn.addEventListener('click', () => {
	
						const randomColors = tinycolor.random().toHexString();
						const shades = generateShades(randomColors);
						const generatedColors = [];
	
						shades.forEach((shade, index) => {
							const card = cards[index];
							card.querySelector('.co-canvas-co').style.backgroundColor = shade;
							card.querySelector('.co-hex-co').innerHTML = `color${index + 1} : ${shade}`;
							
							generatedColors.push({
								type: `color${index + 1}`,
								color: shade
							});
						});
						updatePaletteColors(shades);
	
						$("#collapsecolor .input-wrapper").html("");
	
						new CoInput({
							container : "#collapsecolor .input-wrapper",
							inputs:[
								{
									type: "inputMultiple",
									options: {
										name: "multiColor",
										max: 10,
										inputs: [
											[
												{
													type: "select",
													options: {
														label: tradCms.type,
														name: "type",
														options: [
															"color1","color2","color3","color4","color5","color6","color7","color8","color9","color10"
														]
													}
												},
												"color"
											]
										],
										defaultValue: generatedColors
									}
								},
							],
							onchange:function(name, value, payload){
								// Transformer le tableau en objet pour colorFInal
								const isDataValid = Array.isArray(value) && value.every(item => item.type && item.color);
								if (!isDataValid) {
									mylog.log('Invalid data format or missing fields in value:', value);
									return;
								}
	
								// Transformer le tableau en objet pour colorFInal
								const colorFInal = value.reduce((acc, item) => {
									acc[item.type] = item.color;
									return acc;
								}, {});
	
								// Mettre à jour costumizer.obj avec les nouvelles données
								jsonHelper.setValueByPath(costumizer.obj, "css.color", colorFInal);
								costumizer.actions.change("css.color");
	
								// Mettre à jour la palette avec les nouvelles données
								costumizer.actions.appendColorPalette(value);

							}
						})
	
						// Mettre à jour la palette avec les nouvelles données
						costumizer.actions.appendColorPalette(value);
					})

				}

				function updateCards(shades) {
					const generatedColors = [];
					shades.forEach((shade, index) => {
						const card = cards[index];
						card.querySelector('.co-canvas-co').style.backgroundColor = shade;
						card.querySelector('.co-hex-co').innerHTML = `color${index + 1} : ${shade}`;
				
						// Ajouter la couleur dans le tableau des résultats
						generatedColors.push({
							type: `color${index + 1}`,
							color: shade
						});
					});
					return generatedColors;
				}
				
				if (notNull(btnBaseColor)) {
					btnBaseColor.addEventListener('click', () => {
						const baseColorInput = document.getElementById('baseColorInput').value.trim();

						// Vérification si le champ est vide ou la couleur est invalide
						if (!baseColorInput || !tinycolor(baseColorInput).isValid()) {
							toastr.error("Champ vide ou couleur invalide");
							return; // Arrête l'exécution du code si la validation échoue
						}

						const baseColor = tinycolor(baseColorInput).toHexString();
						const shades = generateShades(baseColor);
						const generatedBaseColors = updateCards(shades);

						$("#collapsecolor .input-wrapper").html("");

						new CoInput({
							container: "#collapsecolor .input-wrapper",
							inputs: [
								{
									type: "inputMultiple",
									options: {
										name: "multiColor",
										max: 10,
										inputs: [
											[
												{
													type: "select",
													options: {
														label: tradCms.type,
														name: "type",
														options: [
															"color1", "color2", "color3", "color4", "color5", 
															"color6", "color7", "color8", "color9", "color10"
														]
													}
												},
												"color"
											]
										],
										defaultValue: generatedBaseColors
									}
								}
							],
							onchange: function(name, value, payload) {
								// Vérification de la validité des données
								const isDataValid = Array.isArray(value) && value.every(item => item.type && item.color);
								if (!isDataValid) {
									mylog.log('Invalid data format or missing fields in value:', value);
									return;
								}

								// Transformer le tableau en objet pour colorFInal
								const colorFInal = value.reduce((acc, item) => {
									acc[item.type] = item.color;
									return acc;
								}, {});

								// Mettre à jour costumizer.obj avec les nouvelles données
								jsonHelper.setValueByPath(costumizer.obj, "css.color", colorFInal);
								costumizer.actions.change("css.color");

								// Mettre à jour la palette avec les nouvelles données
								costumizer.actions.appendColorPalette(value);
							}
						});

						// Mettre à jour la palette avec les nouvelles données
						costumizer.actions.appendColorPalette(generatedBaseColors);
					});
				}


				function generateRandomPalette() {
					const baseColor = tinycolor.random();
					const palette = [];

					palette.push(baseColor.toHexString());
					const complementaryColor = baseColor.complement().toHexString();
					palette.push(complementaryColor);
					palette.push(baseColor.lighten(20).saturate(15).toHexString());
					palette.push(baseColor.complement().darken(20).desaturate(15).toHexString());
					const intermediateColor = tinycolor.mix(baseColor, complementaryColor, 50).toHexString();
					palette.push(intermediateColor);

					return palette;
				}
				
				if(notNull(palete)) {
					palete.addEventListener('click', () => { 
						const randomPalette = generateRandomPalette();
						const generatedColors = updateCards(randomPalette);
	
						$("#collapsecolor .input-wrapper").html("");
						updatePaletteColors(randomPalette);
	
						// Variable globale pour conserver la valeur précédente
						let previousColors = null;
	
						new CoInput({
							container : "#collapsecolor .input-wrapper",
							inputs:[
								{
									type: "inputMultiple",
									options: {
										name: "multiColor",
										max: 10,
										inputs: [
											[
												{
													type: "select",
													options: {
														label: tradCms.type,
														name: "type",
														options: [
															"color1","color2","color3","color4","color5","color6","color7","color8","color9","color10"
														]
													}
												},
												"color"
											]
										],
										defaultValue: generatedColors
									}
								},
							],
							onchange: function(name, value, payload) {
								// Transformer le tableau en objet pour colorFInal
								const isDataValid = Array.isArray(value) && value.every(item => item.type && item.color);
								if (!isDataValid) {
									mylog.log('Invalid data format or missing fields in value:', value);
									return;
								}
	
								// Transformer le tableau en objet pour colorFInal
								const colorFInal = value.reduce((acc, item) => {
									acc[item.type] = item.color;
									return acc;
								}, {});
	
								// Mettre à jour costumizer.obj avec les nouvelles données
								jsonHelper.setValueByPath(costumizer.obj, "css.color", colorFInal);
								costumizer.actions.change("css.color");
	
								// Mettre à jour la palette avec les nouvelles données
								costumizer.actions.appendColorPalette(value);
							}											
							
						})

						// Mettre à jour la palette avec les nouvelles données
						costumizer.actions.appendColorPalette(generatedColors);
					});

				}
				if(typeof generatedColors !="undefined"){
					CoInput.onchange('multiColor', generatedColors, {}); 
				}
			}
		},
		events : {
			bind : function() {	
				if(costumizer.liveChange){
					$('#costum-design .panel-collapse.collapse').addClass("in");
					setTimeout(function () {
						$('#costum-design .section-buttonList .panel-collapse.collapse').addClass("in");
						$.each($(".switch input[type='checkbox']"), function(){
							if($(this).val()=="on"){
								$(this).prop('checked', true);
							}
						});
					},200)
					$('#costum-design .input-section').hide();
					$(`#costum-design .input-section.section-${costumizer.design.activePathDesign.pop()}`).show();
				}
				costumizer.design.costumCssClone = JSON.parse(JSON.stringify(costumizer.obj.css));
				costumizer.design.events.getDataFont();
				$(".sp-color-picker").spectrum({
					type: "text",
					showInput: "true",
					showInitial: "true"
				});
			},
			generateObj: function(ObjPath, btnNameEdit = "") {
				var objSource = jsonHelper.getValueByPath(costumizer.obj,ObjPath);
				costumizer.design.activePathDesign = ObjPath.split('.');
				var cssMenuPath = costumizer.design.activePathDesign.pop();
				var objCss = {};
				if (typeof objSource != "undefined") {
					if (btnNameEdit === "") {
						objCss[cssMenuPath] = {
							name : tradCms.container,
							icon : 'outdent',
							inputs:	JSON.parse(JSON.stringify(costumizer.design.formOptions.container.inputs))
						}
						if (typeof objSource === "object") {
							$.each(objSource, function (k, v) {
								if (typeof v == 'object') {
									if (k == "buttonList") {
										$.each(v, function (key, val) {
											if ((typeof val == 'object' && exists(val["buttonList"])) ) {
												var entryBtn = themeParams.mainMenuButtons[key];
												var labelBtnTranslate = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
												var iconBtn = "";
												if (key == "xsMenu" || key == "dropdown" || "toolbarAdds" ) {
													iconBtn = {
														type: "section",
														options: {
															name: "icon",
															label: "icon",
															inputs: ["fontSize","color","padding"]
														}
													}
												}
												objCss[cssMenuPath]["inputs"].push({
													type: "section",
													options: {
														name: key,
														label: labelBtnTranslate,
														inputs: [
															"margin",
															"padding",
															{
																type: "section",
																options: {
																	name: k,
																	label: tradCms[k] || k,
																	category : "subSection",
																	inputs: costumizer.design.formOptions.button.inputs
																}
															},
															iconBtn
														]
													}
												});
											} else if (typeof val == 'object' || typeof val) {
												var entryBtn = themeParams.mainMenuButtons[key];
												var labelBtnTranslate = "";
												if (typeof entryBtn != "undefined") { 
													if (typeof entryBtn.costumizer != "undefined") {
														labelBtnTranslate = (typeof tradCms[entryBtn.costumizer.keyTrad] != "undefined") ? tradCms[entryBtn.costumizer.keyTrad] : entryBtn.costumizer.label;
													} else if (typeof entryBtn.label != "undefined") {
														labelBtnTranslate = entryBtn.label;
													}
												}
												if (key == "logo" || key == "searchBar") {
													objCss[cssMenuPath]["inputs"].push({
														type: "section",
														options: {
															name: key,
															label: labelBtnTranslate,
															inputs: ["background", "padding", "borderRadius", "border"]
														}
													});
												}if (key == "userProfil") {
													objCss[cssMenuPath]["inputs"].push({
														type: "section",
														options: {
															name: key,
															label: labelBtnTranslate,
															inputs: ["color","background", "padding", "borderRadius", "border"]
														}
													});
												} else {
													objCss[cssMenuPath]["inputs"].push({
														type: "section",
														options: {
															name: key,
															label: labelBtnTranslate,
															inputs: [
																"background","color","fontSize","fontWeight","padding","borderRadius","border","textTransform"
															]
														}
													});
												}
											}
										});
									}
								}
							})
						}
					} else {
						objCss[cssMenuPath] = {
							name : "Style",
							icon : 'paint-brush',
							inputs:	[]
						}
						if ($.inArray(btnNameEdit, ["xsMenu", "dropdown", "toolbarAdds", "app"]) >= 0) {
							if (typeof objSource === "object") {
								$.each(objSource, function (key, val) {
									var iconBtn = "";
									iconBtn = {
										type: "section",
										options: {
											name: "icon",
											label: "icon",
											inputs: ["fontSize","color","padding"]
										}
									}
									objCss[cssMenuPath]["inputs"] = [
										"margin",
										"padding",
										{
											type: "section",
											options: {
												name: key,
												label: tradCms[key] || key,
												category : "subSection",
												inputs: costumizer.design.formOptions.button.inputs
											}
										},
										iconBtn
									];
								});
							}
						} else if ($.inArray(btnNameEdit, ["logo", "searchBar"]) >= 0) {
							objCss[cssMenuPath]["inputs"] = [
								"background", 
								"padding", 
								"borderRadius", 
								"border"
							]
						}else if ($.inArray(btnNameEdit, ["userProfil"]) >= 0) {
							objCss[cssMenuPath]["inputs"] = [
								"color",
								"background",
								"padding",
								"borderRadius",
								"border"
							]
						} else {
							objCss[cssMenuPath]["inputs"] = [
								"background",
								"color",
								"fontSize",
								"fontWeight",
								"padding",
								"borderRadius",
								"border",
								"textTransform"
							]
						}
					}
				}
				return objCss;

			},
			addFont: function (values = costumizer.design.fontUploadFiles) {
				var fd = new FormData();
				var files = values;
				if (files != {}) {
					fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
					fd.append('qqfilename',files.name);
					fd.append('qqtotalfilesize',files.size);
					fd.append('qqfile',files);
					fd.append('costumSlug', costumizer.obj.contextSlug);
					fd.append('costumEditMode', costum.editMode);
					ajaxPost(
						null,
						baseUrl+'/co2/document/upload-save/dir/communecter/folder/'+costumizer.obj.contextType+'/ownerId/'+costumizer.obj.contextId+'/input/qqfile/docType/file/subKey/costumFont',
						fd,
						function(data){
							if(data.result){
								toastr.success(data.msg);
							}else{
								toastr.error(data.msg);
							}
						},
						null,
						null,
						{
							contentType: false,
							processData: false
						}
					);
				} else {
					toastr.error("Something went wrong !!");
				}
			},
			getDataFont: function () {
				var urlToSend = baseUrl+"/"+moduleId+"/cms/getlistfile";
				var params = {
					costumId : costum.contextId,
					costumType : costum.contextType,
					"doctype" : "file",
					"subKey" : "costumFont"
				}
				ajaxPost(
					null,
					urlToSend,
					params,
					function(data){
						if (data && data.result.length != 0) {
							$.each(data.result, function(k,v) {
								$('#inputs-font input[name="uploadFont"]').val(v.name);
								$("#add-font-upload").hide();
								$("#delete-font-upload").show();
								$("#delete-font-upload").attr("data-id", v._id.$id);
								costumizer.design.fontUploadExist = true;
							});
						} else {
							$("#add-font-upload").show();
							$("#delete-font-upload").hide();
							costumizer.design.fontUploadExist = false;
						}
					}
				);
			},
			deleteFontUpload: function() {
				$("#inputs-font .input-wrapper #delete-font-upload").off().on("click", function(e) {
					e.stopImmediatePropagation();
					bootbox.confirm({
						message: trad.areyousuretodelete,
						buttons: {
							confirm: {
								label: trad.yes,
								className: 'btn-success'
							},
							cancel: {
								label: trad.no,
								className: 'btn-danger'
							}
						},
						callback: function(response) {
							if (response) {
								if (typeof $("#delete-font-upload").attr("data-id") != "undefined") {
									var id = $("#delete-font-upload").attr("data-id");
									var urlToSend = baseUrl+"/co2/document/deletedocumentbyid/slug/"+costumizer.obj.contextSlug+"/id/"+id;
									ajaxPost(
										null,
										urlToSend,
										null,
										function(data){
											if ( data && data.result ) {
												$('#inputs-font input[name="uploadFont"]').val('');
												$("#add-font-upload").show();
												$("#delete-font-upload").hide();
												var valueFont = { isFontExist: false };
												uploadedFont = "";
												jsonHelper.setValueByPath(costumizer.obj, "css.font", valueFont);
												var fontUpdate = {};
												fontUpdate.id = costum.contextId;
												fontUpdate.collection = costum.contextType;
												fontUpdate.path = "costum.css.font";
												fontUpdate.value = valueFont;
												dataHelper.path2Value(fontUpdate, function(params){
													toastr.success(tradCms.elementDeleted);
													costum.css.font = jQuery.extend(true, {}, costumizer.obj.css.font);
													costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
													costumizer.design.fontUploadExist = false;
												})
											} else {
												toastr.error(tradCms.somethingWrong);
											}
										}
									);
								}
							}
						}
					})
				});
			},
			chooseFontForm: function() {
				$('.btn-upload-false').off().on('click',  function() {
					costumizer.design.actions.useSelectFont();
					if ($('#inputs-font select[name="fontSelect"]').val() != "") {
						costumizer.obj.css['font'] = {};
						valueFont = { url: $('#inputs-font select[name="fontSelect"]').val(), useUploader: false,  isFontExist: true }
						costumizer.design.actions.change("css.font", valueFont);
					}
				});
				$('.btn-upload-true').off().on('click',  function() {
					costumizer.design.actions.useUploadFont();
					if ($('#inputs-font input[name="uploadFont"]').val() != "") {
						costumizer.obj.css['font'] = {};
						valueFont = { url: "", useUploader: true,  isFontExist: true }
						costumizer.design.actions.change("css.font", valueFont);
					}
				});
			}
		},
		actions : {
			init : function(){
				var objOptions = (notEmpty(costumizer.design.activeObjCss)) ? costumizer.design.activeObjCss : costumizer.design.cssGeneralMap ;
				var defaultValues = {};

				Object.keys(objOptions).forEach(function(key) {
					if (key === "left" || key === "right" || key === "center") {
						if (typeof costumizer.obj.css.menuTop != "undefined" && typeof costumizer.obj.css.menuTop[key] != "undefined") {
							defaultValues = JSON.parse(JSON.stringify(costumizer.obj.css.menuTop[key]));
						}
					} else {
						defaultValues = costumizer.obj.css[key];
						if(key === "color") {
							if (typeof defaultValues != "undefined") {
								var multiColor = Object.keys(defaultValues).map(function(key){
									return {
										type:key,
										color:defaultValues[key]
									}
								})
								defaultValues.multiColor = multiColor
							}
						} else if (key === "font") {
							if (defaultValues) {
								if(defaultValues.url != "") {
									defaultValues.fontSelect = defaultValues.url;
								}
							}
						}
					}
					var savePath = (key == "left" || key == "right" || key == "center") ? `css.menuTop.${key}` : `css.${key}`

					// call coInput
					var container = `#inputs-${key} .input-wrapper`;
					var inputs = objOptions[key].inputs.map(function(input) {
						if (input.options?.name == "fontSelect") {
							var fontOptions = Object.keys(fontObj).map(function(value) {
								return {
									value: value,
									label: fontObj[value]
								}
							})
							input.options.options = fontOptions;
						}
						return input;
					});
					var payload = {
						path: savePath,
					};
					var onchange = function(path, valueToSet, name, payload, value){
						if ($('#collapseloader').hasClass('in')) {
							costumizer.design.actions.customizeLoader();
						} else if($('#collapseprogress').hasClass('in')) {
							costumizer.design.actions.customizeProgress();
						}

						if (key == "color") {
							valueToSet = {};
                            Object.values(value).forEach(function(value) {
                                valueToSet[value.type] = value.color;
                            })

							// Mettre à jour la palette avec les nouvelles données
							costumizer.actions.appendColorPalette(value);

                        } else if (key == "font") {
                            if (name == "fontSelect" && typeof value == "string" && value != "") {
                                valueToSet = { url: value, useUploader: false,  isFontExist: true }
                            } else if (name == "uploadFont" && typeof value == 'object') {
								var index = value.name.lastIndexOf(".");
								var extension = value.name.substring(index + 1);
								if (extension != "ttf") {
									toastr.error(tradCms.fontTypeNotAccepted);
									$('#inputs-font input[name="uploadFont"]').val("");
								} else {
									valueToSet = { url: "", useUploader: true,  isFontExist: true }
									costumizer.design.fontUploadFiles = value;
									$('#inputs-font input[name="uploadFont"]').val(value.name);
								}
                            } else {
                                valueToSet = { isFontExist: false };
                            }
                        }
						if(notEmpty(path) && notEmptyAllType(valueToSet))
							costumizer.design.actions.change(path, valueToSet,name);
					};

					cssHelpers.form.launch(container,inputs,payload,defaultValues,{},onchange);
				})
				costumizer.design.actions.checkFontsExist();
				costumizer.design.views.loaderView();
				costumizer.design.views.generteColor();
				costumizer.design.events.deleteFontUpload();
				costumizer.design.events.chooseFontForm();
			},
			change : function(path, value,name) {
				if (path === "css.font") {
					jsonHelper.setValueByPath(costumizer.obj, path, value);
					costumizer.actions.change(path);
					costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
					costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.obj.css, path: "costumizer.obj.css"}})
				} else if ($.inArray(name, ["hideOnDesktop", "hideOnTablet", "hideOnMobil","columnWidth"])>= 0){
					jsonHelper.setValueByPath(costumizer.obj, path, value);
					costumizer.actions.change(path);
					var objVal = jsonHelper.getValueByPath(costumizer.obj, path.replace("."+name, ''));
					costum.addClassSection(objVal,path.replace("."+name, ''));
					costumizer.updateComponentSocket({functionName: "addClassSection", data: {obj: objVal, path: path.replace("."+name, '')}});
				} else {
					if (typeof value == "object") {
						if (path == "css.color") {
							jsonHelper.setValueByPath(costumizer.obj, path, value);
							costumizer.actions.change(path);
						} else {
							$.each(value, function(keyChange, valueChange) {
								jsonHelper.setValueByPath(costumizer.obj, path+"."+keyChange, valueChange);
								costumizer.actions.change(path+"."+keyChange);
								cssHelpers.render.addStyleUI(path+"."+keyChange, valueChange, "cosDyn");
								costumizer.updateComponentSocket({functionName: "addStyleUI", data: { path: path+"."+keyChange, value: valueChange, dom: "cosDyn"}});
							})	
						}
					} else {
						jsonHelper.setValueByPath(costumizer.obj, path, value);
						costumizer.actions.change(path);
						cssHelpers.render.addStyleUI(path, value, "cosDyn");
						costumizer.updateComponentSocket({functionName: "addStyleUI", data: {path: path, value: value, dom: "cosDyn"}})		
					}
				}
				var mainBaliceCss = ["title", "text", "link"];
				if (path.includes("icon") == true || path.includes("hover") == true || mainBaliceCss.includes(path.split(".")[1])) {
					costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
					costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.obj.css, path: "costumizer.obj.css"}})
					costumizer.design.changeStyleNeedToInit = true;
				};
				costumizer.history.refresh = false;
			},
			cancel : function(){
				arrayStyleToRemove=[];
				$.each(costumizer.actions.pathToValue, function(e, path){
					if(path.indexOf("css")>= 0){
						arrayStyleToRemove.push(path.slice(path.indexOf(".")+1,path.lastIndexOf(".")));
					}
				});
				if(notEmpty(arrayStyleToRemove)) {
					cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");
					costumizer.updateComponentSocket({functionName: "removeStyleUI", data: { arrayStyleToRemove: arrayStyleToRemove, dom: "cosDyn"}})
				}
				if (costumizer.design.changeStyleNeedToInit == true) {
					costumizer.design.changeStyleNeedToInit = false;
					costumizer.design.cssClone = jQuery.extend(true,{},costum);
					costum.initCssCustom(costumizer.design.cssClone.css, "costumizer.design.cssClone.css");
					costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.design.cssClone.css, path: "costumizer.design.cssClone.css"}})
				};
				costumizer.history.refresh = false;
			},
			close: function(){
				costumizer.design.activeObjCss={};
			},
			afterUpdate : function () {
				var arrayStyleToRemove=[];
				var objToUp={};
				$.each(costumizer.actions.pathToValue, function(e, path){
					if(path.indexOf("css")>= 0){
						objToUp[path.replace("css.", "")]=jsonHelper.getValueByPath(costumizer.obj, path);
						arrayStyleToRemove.push(path.slice(path.indexOf(".")+1,path.lastIndexOf(".")));
					}
				});
				if(notEmpty(objToUp))
					cssHelpers.render.addStyleInDomByPath(objToUp,"#amazing-costumizer-css", "cosDyn");
				//if(notEmpty(arrayStyleToRemove))
				//	cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");

			},
			customizeLoader: function() {
				let background = $('#inputs-loader input[name="background"]').val();
				let r1borderColor = $('#inputs-loader .input-section:first input[name="color"]').val();
				let r1borderwidth = $('#inputs-loader .input-section:first input[name="borderWidth"]').val();
				let r1height = $('#inputs-loader .input-section:first input[name="height"]').val();
				let r1width = $('#inputs-loader .input-section:first input[name="width"]').val();
				let r1top = $('#inputs-loader .input-section:first input[name="top"]').val();
				let r1left = $('#inputs-loader .input-section:first input[name="left"]').val();
				let r2borderColor = $('#inputs-loader .input-section:last input[name="color"]').val();
				let r2borderwidth = $('#inputs-loader .input-section:last input[name="borderWidth"]').val();
				let r2height = $('#inputs-loader .input-section:last input[name="height"]').val();
				let r2width = $('#inputs-loader .input-section:last input[name="width"]').val();
				let r2top = $('#inputs-loader .input-section:last input[name="top"]').val();
				let r2left = $('#inputs-loader .input-section:last input[name="left"]').val();

				$('#collapseloader .panel-body .loader-contain').css({"background-color": `${background}`});
				//loading_modal
				$('#collapseloader .panel-body #ring1').attr("style",
					`border-width: ${r1borderwidth}; border-color: transparent ${r1borderColor}; height: ${r1height}; width: ${r1width}; left: ${r1left}; top: ${r1top}`
				);
				$('#collapseloader .panel-body #ring2').attr("style", 
					`border-width: ${r2borderwidth}; border-color: transparent ${r2borderColor}; height: ${r2height}; width: ${r2width}; left: ${r2left}; top: ${r2top}`
				);
				//loader 1
				$('#collapseloader .panel-body .loader_1 span').attr("style",`background-color: ${r1borderColor};`);
				var loaderStyle = '.loader-square:before{ border: solid '+r1borderwidth+' '+r1borderColor+'; } .loader-square:after{ border: solid '+r2borderwidth+' '+r2borderColor+';}';
				//loader_2
				$('#collapseloader .panel-body .dl .dl_corner--top').attr("style",`color: ${r1borderColor};`);
				$('#collapseloader .panel-body .dl .dl_corner--bottom').attr("style",`color: ${r2borderColor};`);
				//loader_3
				$('#collapseloader .panel-body .horizontal_bar_loader').attr("style",`background-color: ${r2borderColor};height: ${r2borderwidth};`);
				$('#collapseloader .panel-body .horizontal_bar_loading').attr("style",`background-color: ${r1borderColor};height: ${r1borderwidth};`);
				//loader_4
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(1)').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(2)').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(3)').attr("style",`background-color: ${r2borderColor};`);
				$('#collapseloader .panel-body .newtons-cradle div:nth-of-type(4)').attr("style",`background-color: ${r2borderColor};`);
				//loader 5
				loaderStyle += ' @keyframes loaderBlock { 0%, 30% {transform: rotate(0);} 55% {background-color: '+r2borderColor+';} 100% {transform: rotate(360deg);}}@keyframes loaderBlockInverse { 0%, 20% {transform: rotate(0);} 55% {background-color: '+r2borderColor+';} 100% {transform: rotate(-360deg);}}';
				//loader 6
				loaderStyle += ' .loader-circle-2::before {background-color: '+r2borderColor+';}'
				$('#collapseloader .panel-body .loader-circle-2').attr("style",`border: ${r1borderwidth} solid ${r1borderColor};`);
				//loader 7
				loaderStyle += '@keyframes spanMoveAnimation { 0% {transform: scale(1);margin: 0;} 20% {transform: scale(1);margin: 0;} 60% {transform: scale(0.5);margin: 20px;background: '+r2borderColor+';}  100% {  transform: scale(1);  margin: 0;  }  }'
				//loader 8
				$('#collapseloader .panel-body .loader-ball div.dot1').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot2').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot3').attr("style",`background-color: ${r1borderColor};`);
				$('#collapseloader .panel-body .loader-ball div.dot4').attr("style",`background-color: ${r1borderColor};`);
				loaderStyle += '.loader-ball div::after {background-color: '+r2borderColor+';}';

				$('style#cssLoader').html("");
				$('style#cssLoader').append(loaderStyle);

			},
			customizeProgress: function() {
				let bar = $('#inputs-progress .input-section:first input[name="background"]').val();
				let value = $('#inputs-progress .input-section:last input[name="background"]').val();
				$( "#collapseprogress .panel-body .progress").css({"backgroundColor": `${bar}`});
				$( "#collapseprogress .panel-body .progress-bar").css({"backgroundColor": `${value}`});
			},
			useSelectFont : function() {
				$('#inputs-font .input-wrapper .form-group:first').show();
				$('#inputs-font .input-wrapper .form-group:last').hide();
				$('.btn-upload-false').css({"color":"#ffffff !important", "background-color":"#34a853"});
				$('.btn-upload-true').css({"color":"#34a853 !important", "border-color":"#ccc", "background-color":"transparent"});
				$('.btn-upload-true').removeClass("disabled");
				$('.btn-upload-false').addClass("disabled");
			},
			useUploadFont : function() {
				$('#inputs-font .input-wrapper .form-group:first').hide();
				$('#inputs-font .input-wrapper .form-group:last').show();
				$('.btn-upload-true').css({"color":"#ffffff !important", "background-color":"#34a853"});
				$('.btn-upload-false').css({"color":"#34a853 !important", "border-color":"#ccc", "background-color":"transparent"});
				$('.btn-upload-true').addClass("disabled");
				$('.btn-upload-false').removeClass("disabled");
			},
			checkFontsExist : function() {
				$("#inputs-font").prepend(`
					<div class="form-group">
						<div class="button-font" style="display: flex; justify-content: center; aligns-items: center; width: 100%; flex-direction: column; padding: 10px 0">
							<button type="button" class="btn btn-sm btn-default text-bold btn-upload-true">${tradCms.fontUpload}</button>
							<button type="button" class="btn btn-sm btn-default text-bold btn-upload-false" style="margin-top: 5px;">${tradCms.selectFont}</button>
						</div>
					</div>
				`);
				if (costumizer.obj.css.font) {
					if (costumizer.obj.css.font.url != "" && costumizer.obj.css.font.useUploader === false) {
						costumizer.design.actions.useSelectFont();
					} else if (costumizer.obj.css.font.url == "" && costumizer.obj.css.font.useUploader === true) {
						costumizer.design.actions.useUploadFont();
					} else {
						costumizer.design.actions.useSelectFont();
					}
				} else {
					costumizer.design.actions.useSelectFont();
				}
			}
		}
	},
	css : {
		views : {
			init : function(){
				let cssFile = costumizer.css.actions.get();
				var str=`
						<div style="display: flex; justify-content: space-around;">
							<div class=" form-group cssCodecustom" style="width: 80vw;">
								<label class="col-xs-12 text-left control-label no-padding" style="color:#333;" for="cssCode">
									<i class="fa fa-chevron-down"></i> ${tradCms.cssCode}
								</label>
								<div class="col-xs-12 text-left div-wrapper-high" style="padding-left: 0px;padding-right: 0px; display: flex; justify-content: space-around;">
									<textarea id="cssFileLeft" maxlength="" class="form-control textarea col-xs-12 no-padding valid" name="cssFileLeft" is="highlighted-code" language="css" tab-size="1" style="position: relative; overflow: unset; height: 70vh; width: 80vw ; resize: none; tab-size: 1; white-space: pre; font-family: monospace; color: transparent; background-color: rgb(250, 250, 250); caret-color: rgb(56, 58, 66);">${cssFile}</textarea>
								</div>
							</div>
						</div>
					`;
				return str;
			},
		},
		actions : {
			get: function () {
			
				(async ({chrome, netscape}) => {
    
					// add Safari polyfill if needed
					if (!chrome && !netscape)
					  await import('https://unpkg.com/@ungap/custom-elements');
				
				  
					const {default: HighlightedCode} =
					  await import('https://unpkg.com/highlighted-code');
				
				  
					HighlightedCode.useTheme('atom-one-light');
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				  })(self);

				var result = new Array();
				ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cssFile/getbycostumid",
                    {id: costum.contextId},
                    function(data){ 
                        $.each(data, function(key,value) {
                            if (notEmpty(value.css)){
                                result.push({"css" : value.css})
                            }
                         });
                      },
                    null,
                    "json",
                    extraParamsAjax
                );

                if (result.length > 0){
                    return result[0]["css"];
                }
                else{
                    return "";
                }

				
			},

			save: function () {
				var input = document.querySelector('#cssFileLeft');
				// let validate = input.value.replace(/[^\S+]/g,"");
				// validate = validate.replace(/\/\*[\s\S]*?\*\//g,"");
				// validate = validate.replace(/http:\/\//gm,"");
				// validate = validate.replace(/((#|[.]|[*]){0,1}[\w\-[\]^="':>~+,()])+\s*\{((?:[A-Za-z\- \s]+[:]\s*([#'"0-9\w .,\/()\-!%]+\;)))+\}(?:\s*)/mg,"");
				// validate = validate.replace(/@importurl\([\"']([^)]*)[\"']\);|@import(\"|\')([^)]*)(\"|\');/g,"");
				// validate = validate.replace(/(@.*?:?[^}{@]+(\{(?:[^}{]+|\{(?:[^}{]+|\{[^}{]*\})*\})*\}))/g,"")
				

				const validation = csstreeValidator.validate(input.value);
				params = {  
					"id" : costum.contextId, 
					"css" : input.value
				};

				let saveButton = `<button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> ${trad.save}</button>`;

				if(validation.length > 0){ 
					toastr.error(validation[0]["message"]+": line "+validation[0]["line"]+",column "+validation[0]["column"]);
					$(".save-cssFile").replaceWith(saveButton);
				}
                else {
                    ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/cssFile/insert",
				        params,
					);
					toastr.success(tradCms.saved);
                    location.reload();
					$(".save-cssFile").replaceWith(saveButton);
                }
			}
				
		},
		events :{ 
			bind : function(){

				$("#cssFileLeft").on("mouseenter", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				// $("#cssFileLeft").on("paste", async function (e) {
				// 	const items = (e.originalEvent.clipboardData || e.clipboardData).items;
				// 	for (const item of items) {
				// 		if (item.type.indexOf("image") !== -1) {
				// 			// const blob = item.getAsFile();
				// 			// showMyImageFromBlob(blob);
							
				// 			// // Add a listener to the form submit event
				// 			// 	e.preventDefault();
			
				// 			// 	// Perform the AJAX POST request to save the image
				// 			// 	try {
				// 			// 		const response = await saveImage(blob);
				// 			// 		if (response.result) {
				// 			// 			$(".imagesNews").last().val(response.id.$id);
				// 			// 			$(".imagesNews").last().attr("name","");
				// 			// 			$(".newImageAlbum").last().find("img").removeClass("grayscale");
				// 			// 			$(".newImageAlbum").last().find("i.noGoSaveNews").remove();
				// 			// 			$(".newImageAlbum").last().append("<a href='javascript:;' onclick='deleteImage(\""+response.id.$id+"\",\""+response.name+"\")'><i class='fa fa-times fa-x padding-5 text-white removeImage' id='deleteImg"+response.id.$id+"'></i></a>");
				// 			// 			toastr.success(response.msg);
				// 			// 		} else {
				// 			// 			toastr.error(response.msg);
				// 			// 		}
				// 			// 	} catch (error) {
				// 			// 		toastr.error("Error uploading image: " + error.statusText);
				// 			// 	}
				// 		}
				// 	}
				// });


				$("#cssFileLeft").on("click", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				$("#highlighted-code").on("change", function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
				});

				$('#cssFileLeft').keydown(function(){
					$(".highlighted-code").css({"left": "auto","top":"0px"});
					if($('#cssFileLeft').val() != "") {
						$(".save-costumizer").replaceWith(`<button class="btn btn-danger save-cssFile" onclick="costumizer.css.actions.save()" style="margin-left: 50px;"><i class="fa fa-save"></i> ${trad.save}</button>`);
					}
				}); 

				$(".close-dash-costumizer").off().on("click", function(){
					costumizer.actions.cancel($(this).data("dom"));
						
					if ($("#amazing-costumizer-css").length > 0 ){
						$("#amazing-costumizer-css").attr("type", "text/css");	
					} else if ($("#amazing-costumizer-css-new").length > 0){
						$("#amazing-costumizer-css-new").attr("type", "text/css");
					}
					//$($(this).data("dom")).fadeOut(1000);
					$("#content-btn-delete-page").remove();
					$(".save-cssFile").replaceWith(`<button class="btn btn-danger save-costumizer disabled"><i class="fa fa-save"></i> ${trad.save}</button>`);
				});

				// $(".save-cssFile").on("click", function(){
				// 	costumizer.css.actions.save();
				// });
			}
		},

	},
	history : {
		refresh : true,
		views: {
			init: function () {
				$(".cmsbuilder-right-content").addClass("active");
				var str = $(`
				<div class="history-content-list">
					<div class="filters-history"></div>
					<div id="listAction"></div>
					<ul class="media-list col-xs-12" id="historyList">
                        
                    </ul>
				</div>
				`);

				return str;
			}
		},
		events : {

			bind: function () {
				// Mettre à jour le Logs
				costumizer.obj.olderData = {};
				costumizer.obj.actionsLogs = {};
				costumizer.obj.dataBlockLogs = {};
				var paramsFilter= {
					container : ".filters-history",
					results :{
						renderView : "directory.listLogs",
						smartGrid : false,
						dom : ".history-content-list #historyList",
						scrollableDom : ".cmsbuilder-tabs-body"
					},
					loadEvent : {
						default : "scroll"
					},
					interface : {
						events : {
							scroll : true,
							scrollOne : true
						}
					},
					defaults : {
						types : ["logs"],
						notSourceKey : true,
						fields : [
							"userId","userName" ,"costumId" ,"costumSlug" ,"costumEditMode","action", "subAction" ,"value" ,"path" ,"idBlock" ,"blockPath" ,"blockName" ,"collection" ,"created","page","pathChanged","type","subtype","children","olderData","keyPage","blockType","kunik"
						],
						indexStep :10,
						filters : {
							costumSlug : costum.slug
						},
						sortBy : {"created" : -1},
					},

					filters : {
						action : {
							view : "horizontalList",
							type : "filters",
							dom : "#listAction",
							field :"action",
							//name : "<i class='fa fa-filter'></i> Action:",
							active : true,
							typeList : "object",
							event : "filters",
							classList : "pull-left favElBtn btn",
							keyValue : false,
							list :  {
								"costum/editBlock" : "<i class='fa fa-edit tooltips' data-original-title='"+tradCms.editBlock+"'></i> ",
								"costum/addBlock" : "<i class='fa fa-plus-circle tooltips' data-original-title='"+tradCms.addBlock+"'></i>",
								"costum/deleteBlock" : "<i class='fa fa-trash-o tooltips' data-original-title='"+tradCms.deleteBlock+"'></i>",
								"costum/config" : "<i class='fa fa-outdent tooltips' data-original-title='"+tradCms.menuAndOther+"'></i>",
								"costum/page" : "<i class='fa fa-file-o  tooltips' data-original-title='Page'></i>",
								"costum/options" : "<i class='fa fa-sliders  tooltips' data-original-title='"+tradCms.optionsCostum+"'></i>",
							}
						}
					}

				};
				if (cmsConstructor.spId != "" && $(".block-actions-wrapper").hasClass("selected")) {
					paramsFilter.defaults.filters.idBlock = cmsConstructor.spId ;
					paramsFilter.filters.action.list = {
						"costum/editBlock" : "<i class='fa fa-edit tooltips' data-original-title='Edition de bloc'></i> ",
						"costum/addBlock" : "<i class='fa fa-plus-circle tooltips' data-original-title='Ajout de bloc'></i>"
					}
				}

				filterSearch = searchObj.init(paramsFilter);
				filterSearch.search.init(filterSearch);
				directory.listLogs = function(params){
					costumizer.obj.olderData[params.id] = {};
					costumizer.obj.actionsLogs[params.id] = {};
					costumizer.obj.actionsLogs[params.id] = params.action;
					costumizer.obj.olderData[params.id]["olderData"] = params.olderData;
					costumizer.obj.olderData[params.id]["pathChanged"] = params.pathChanged;
					costumizer.obj.olderData[params.id]["page"] = params.page;
					costumizer.obj.olderData[params.id]["params"] = params;

					var iconLeft = "outdent",
						setRecovery = "",
						viewSubAction = "",
						viewDetail  = "",
						pageChange = '<span class="label label-info tooltips"  data-original-title="Page">#'+params.page+'</span>',
						keyPage = (typeof params.keyPage != "undefined") ? params.keyPage : "",
						actionMsg = "",
						labelName = "";

					switch (params.action) {
						case "costum/editBlock":
							iconLeft = "edit text-green";
							actionMsg = tradCms.editBlock;
							labelName ="<span class='name  text-green'>Bloc : </span>";
							costumizer.obj.dataBlockLogs[params.id] = {
								id: params.idBlock,
								path : params.blockPath,
								kunik : params.kunik,
								blockType : params.blockType
							}
							viewDetail = "<span>" + tradCms.key + " :</span> " + params.idBlock + "<br><span>"+trad.collection+" :</span> " + params.collection + "<br> <span>"+tradCms.path+" :</span> " + params.path + "<br> <span>"+tradCms.value+"  :</span> <pre>"+ JSON.stringify(params.value)+"</pre>" ;
							if (typeof params.subAction != "undefined")
								viewSubAction = "<span class='textRecovery'><i class='fa fa-history'></i> "+params.subAction+"</span>";
							if (typeof params.olderData  == "object") {
								setRecovery = '<button class="btn btn-default btn-recovery" data-id="'+params.id+'" data-key="'+params.idBlock+'" data-keyPage="'+keyPage+'"> <i class="fa fa-history"></i> '+tradCms.restoreThisChange+'</button>';
							}
							break;
						case "costum/addBlock":
						case "costum/deleteBlock":
							if (params.action == "costum/addBlock"){
								actionMsg = tradCms.addBlock;
								iconLeft = "plus-circle text-blue";
								labelName ="<span class='name  text-blue'>"+tradCms.block+" : </span>";
							} else {
								iconLeft = "trash-o text-red";
								actionMsg = tradCms.deleteBlock;
								labelName ="<span class='name text-red'>"+tradCms.block+" : </span>";
							}
							viewDetail = "<span>" + tradCms.key + " :</span> " + params.idBlock + "<br><span>"+trad.collection+" :</span> " + params.collection;
							if (notEmpty(params.blockPath) && params.collection != "templates"){
								viewDetail += "<br><span>"+tradCms.path+" :</span> " + params.blockPath;
							}
							if (notEmpty(params.children)){
								viewDetail += "<br><span>"+tradCms.blocksList+" :</span> " +params.children;
							}
							if (params.collection == "templates"){
								viewSubAction = "<span><i class='fa fa-file-o'></i> "+tradCms.template+" :</span> " + params.type;
								params.action = "costum/useTemplate";
								if (params.type == "costum")
									params.page = "";
							}
							break;
						case "costum/config":
							params.blockName = tradCms.menuAndOther;
							actionMsg = tradCms.menuOrOtherConfig;
							pageChange = "";
							setRecovery = '<button class="btn btn-default btn-recovery" data-id="'+params.id+'"> <i class="fa fa-history"></i> '+tradCms.restoreThisChange+'</button>';
							if ( Array.isArray(params.pathChanged)) {
								viewDetail  = "<span>"+tradCms.path+" :</span> <br> <ul>";
								$.each(params.pathChanged, function(k, v) {
									viewDetail  += "<li>" + v + "</li>";
								})
								viewDetail += "</ul>"
							}
							break;
						case "costum/page":
							iconLeft = "file-o text-blue"
							actionMsg = tradCms.sitePage;

							if (params.subAction == "NewPage") {
								viewSubAction  = '<i class="fa fa-plus-circle"></i> '+ tradCms.newPage;
							} else if (params.subAction == "DeletePage") {
								viewSubAction  = '<i class="fa fa-trash-o text-red"></i> '+ tradCms.deletePage;
							} else if (params.subAction == "EditPage"){
								viewSubAction  = '<i class="fa fa-edit"></i> '+ tradCms.pageModification;
								setRecovery = '<button class="btn btn-default btn-recovery" data-id="'+params.id+'" data-keyPage="'+keyPage+'"> <i class="fa fa-history"></i> '+tradCms.restoreThisChange+'</button>';
							} else if (params.subAction == "DuplicatePage") {
								viewSubAction  = '<i class="fa fa-copy"></i> '+ tradCms.duplicatePage;
							} else if (typeof params.olderData  == "object") {
								viewSubAction  = '<i class="fa fa-history"></i> '+ params.subAction;
								setRecovery = '<button class="btn btn-default btn-recovery" data-id="'+params.id+'" data-keyPage="'+keyPage+'"> <i class="fa fa-history"></i> '+tradCms.restoreThisChange+'</button>';
							}
							params.blockName = "Page";
							break;
						case "costum/options":
							iconLeft = "sliders text-blue"
							actionMsg = tradCms.optionsCostum;
							pageChange = "";
							if (params.subAction == "EditPage"){
								viewSubAction  = '<i class="fa fa-edit"></i> '+ tradCms.optionsCostum;
							}
							viewDetail  = "<span>"+tradCms.path+" :</span> <br><ul><li> " + params.path + "</li></ul> <span>"+tradCms.value+" :</span> <br><ul><li> " + params.value + "</li></ul>";

							params.blockName = tradCms.optionsCostum;
							break;
					}

					var str =
						`
							<li class="list-group-item">
								<div class="media">
									<i class="fa fa-${iconLeft} pull-left"></i>
									<div class="media-body">
										${labelName}<span class="name">${params.blockName}</span>
										${pageChange}
										<span class="number pull-right">${directory.showDatetimePost(null, null, params.created)}</span>
										<p class="info" >
											<span>${costum.members["lists"][params.userId].avatarWithName}</span>
											<i class="fa fa-cogs"></i> ${actionMsg} <br>
											${viewSubAction}
										</p>
									</div>
								</div>
								${params.userId == userId || isSuperAdmin ? setRecovery : ''}
								<div class="panel panel-info ${params.action == 'costum/page' ? 'hidden' : ''}">
									<div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse${params.id}">
									  <h3 class="panel-title">
									${tradBadge.viewDetails}
									  </h3>
									</div>
									<div id="collapse${params.id}" class="collapse">
									  <div class="panel-body">
										<p class="info">
										  ${viewDetail}
										</p>
									  </div>
									</div>
								</div>
							</li>
						`;
					return str;
				};
			}
		},
		actions: {
			init: function () {
				$(".history-content-list").off().on("click", ".btn-recovery", function (e) {
					var idLog = $(this).data("id"),
						pathValueToUpdate = {},
						keyBlock = $(this).data("key"),
						keyPage = $(this).data("keypage"),
						actualPage = location.hash == "" ? "#welcome" : location.hash,
						messageConfirm = (keyPage != "undefined" && keyPage != actualPage && keyBlock != "undefined" && costumizer.obj.actionsLogs[idLog] == "costum/editBlock") ? "<span>Ce bloc est dans la page "+keyPage+",</span><br>" : "";

					bootbox.confirm({
						message: "<div class='text-center bold'>"+messageConfirm+"Voulez-vous vraiment restoré cette version ?</div>",
						buttons: {
							confirm: {
								label: "<i class='fa fa-check'></i> " + trad.yes,
								className: 'btn-success'
							},
							cancel: {
								label: "<i class='fa fa-times'></i> " + trad.cancel,
								className: 'btn-default'
							}
						},
						callback: function (result) {
							if (result) {
								$(".cmsbuilder-right-content").removeClass("active");
								if ( costumizer.obj.actionsLogs[idLog] == "costum/editBlock") {
									$target = $(`.cmsbuilder-block[data-id="${keyBlock}"]`).first();
									var dataToCtrlZ = {};
									var valueOlder = jsonHelper.getValueByPath(costumizer.obj.olderData[idLog]["olderData"]);
									var dataToInsert = {};
									var dateVersion = moment(costumizer.obj.olderData[idLog].params.created*1000).format("DD/MM/YYYY à HH:mm");
									tplCtx = {};
									tplCtx.id = costumizer.obj.dataBlockLogs[idLog].id;
									tplCtx.collection = "cms";
									tplCtx.path = "allToRoot";
									$.each(valueOlder, function(key,val){
									actualValue = typeof cmsConstructor.sp_params[tplCtx.id] !== "undefined" && typeof cmsConstructor.sp_params[tplCtx.id][key] !== "undefined" ? cmsConstructor.sp_params[tplCtx.id][key] : {};
										if ( typeof val === "object" ){
											valueToUpdate = cmsConstructor.helpers.mergeObjectsStructure2(val,cmsConstructor.helpers.deepCopy(actualValue));
											dataToCtrlZ[key] = cmsConstructor.helpers.mergeObjectsStructure1(val,cmsConstructor.helpers.deepCopy(actualValue));
											dataToInsert[key] = valueToUpdate;
										} else {
											dataToInsert[key] = val;
											dataToCtrlZ[key] = actualValue;
										}
									});

									tplCtx.value =  dataToInsert;
									tplCtx.saveLog = {
										costumId : costum.contextId,
										action : "costum/editBlock",
										blockName : costumizer.obj.olderData[idLog].params.blockName,
										path : costumizer.obj.olderData[idLog].params.path,
										blockPath : costumizer.obj.olderData[idLog].params.blockPath,
										page : costumizer.obj.olderData[idLog].params.page ,
										keyPage : costumizer.obj.olderData[idLog].params.keyPage,
										subAction :  "Restauré à partir de la version du "+dateVersion,
										olderData : dataToCtrlZ,
										blockType : costumizer.obj.olderData[idLog].params.blockType,
                					    kunik : costumizer.obj.olderData[idLog].params.kunik
									}


									dataHelper.path2Value(tplCtx, function (params) {

										if ( keyPage == actualPage ) {
											cmsConstructor.helpers.scrollToAndRefresh(tplCtx.id)
										} else if (keyPage != "") {
											urlCtrl.loadByHash(keyPage);

											var blockTarget = tplCtx.id;
											if ( !(cmsConstructor.sp_params[tplCtx.id].path.includes("superCms")) ){
												blockTarget = cmsConstructor.sp_params[tplCtx.id].blockParent;
											}

											if ($(`.cmsbuilder-block[data-id="${blockTarget}"]`).length > 0) {
												if (!(cmsConstructor.helpers.isInViewport($(`.cmsbuilder-block[data-id="${blockTarget}"]`)[0],0))) {
													cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(`.cmsbuilder-block[data-id="${blockTarget}"]`));
												}

											} else {
												var interval = setInterval(function() {
													if ($(`.cmsbuilder-block[data-id="${blockTarget}"]`).length > 0) {
														clearInterval(interval);
														if (!(cmsConstructor.helpers.isInViewport($(`.cmsbuilder-block[data-id="${blockTarget}"]`)[0],0))) {
															cmsConstructor.helpers.scrollTo($(".cmsbuilder-center-content"), $(`.cmsbuilder-block[data-id="${blockTarget}"]`).first());
														}
													}
												}, 200);
											}
										}

										cmsConstructor.helpers.insertObjectToLocalStorage("ctrlZ"+costumizer.mode+costum.slug,tplCtx.id,costumizer.obj.olderData[idLog].params.kunik,"block",dataToCtrlZ,keyPage);

									})

								} else if (typeof keyPage != "undefined" && costumizer.obj.actionsLogs[idLog] == "costum/page") {
									var params = {};
									params["app"] = costumizer.obj.olderData[idLog]["olderData"]["app"];
									//var subAction = "RestorePage";
									var olderData = {};	
									var dateVersion = moment(costumizer.obj.olderData[idLog].params.created*1000).format("DD/MM/YYYY à HH:mm");
									olderData["app"] = jsonHelper.getValueByPath(costumizer.costumDataOlder,"app");

									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/updatecostum",
										{
											params: params,
											costumId    : costum.contextId,
											costumType  : costum.contextType,
											page : costumizer.obj.olderData[idLog]["page"],
											keyPage : keyPage,
											action : "costum/page",
											subAction : "Restauré à partir de la version du "+dateVersion,
											olderData : olderData,
											pathChanged : ["app"]
										},
										function(data){
											jsonHelper.setValueByPath(costumizer.obj, "app", olderData["app"]);
											costum.app=jQuery.extend(true,{},costumizer.obj.app);
											$(".lbh-costumizer[data-space='pages']").trigger( "click" );
											$(".tabling-pages tr[id='keyPages-"+keyPage+"']").addClass("blink-bg");
											//jsonHelper.setValueByPath(costumizer.costumDataOlder, "app", costumizer.obj.olderData[idLog]["params"]["app"]);
										}

									);

								} else {
									var arrayStyleToRemove=[];
									$.each(costumizer.obj.olderData[idLog]["pathChanged"], function (k, pathList) {
										var valueOlder = jsonHelper.getValueByPath(costumizer.obj.olderData[idLog]["olderData"], pathList)
										jsonHelper.setValueByPath(costumizer.obj, pathList, valueOlder);
										costumizer.actions.change(pathList);
										var valueToUp = jsonHelper.getValueByPath(costumizer.obj, pathList);
										pathValueToUpdate[pathList] = valueOlder;
										if(pathList.indexOf("css")>= 0){
											if (costumizer.history.refresh == true) {
												cssHelpers.render.addStyleUI(pathList, valueOlder, "cosDyn");
												costumizer.updateComponentSocket({functionName: "addStyleUI", data: {path: pathList, value: valueOlder, dom: "cosDyn"}})
											}else {
												arrayStyleToRemove.push(pathList.slice(pathList.indexOf(".")+1,pathList.lastIndexOf(".")));
												costum.initCssCustom(costumizer.obj.css, "costumizer.obj.css");
												costumizer.updateComponentSocket({functionName: "initCssCustom", data: {obj: costumizer.obj.css, path: "costumizer.obj.css"}})
											}
										}
									})
									costumizer.actions.update(true);
									costumizer.htmlConstruct.actions.refreshmenu(pathValueToUpdate, costumizer.obj.htmlConstruct);
									//$(".cmsbuilder-right-content").removeClass("active");
									// refresh style
									if(notEmpty(arrayStyleToRemove)) {
										cssHelpers.render.removeStyleUI(arrayStyleToRemove, "cosDyn");
										costumizer.updateComponentSocket({functionName: "removeStyleUI", data: { arrayStyleToRemove: arrayStyleToRemove, dom: "cosDyn"}})
									}

								}
								toastr.success(tradCms.completed);
								setTimeout(function() {
									coInterface.setTopPosition();
								}, 800);

							}
						}
					});
				});
			}
		}
	},
	communityAdmin: {
		views : {
			initAjax:function(config){
				// adminPanel.views.community(config.dom+" .contains-view-admin");
				var dom = config.dom+" .contains-view-admin",
				data = {
					title : trad.community,
					context : {
						id : costum.contextId,
						collection : costum.contextType
					},
					// invite : {
					// 	contextId : costum.contextId,
					// 	contextType : costum.contextType
					// },
					community : {
						contextId : costum.contextId,
						contextType : costum.contextType,
						contextSlug : costum.contextSlug
					},
					kanban : {
						contextId : costum.contextId,
						contextType : costum.contextType,
						contextSlug : costum.contextSlug
					},
					table : {
						name: {
							name : "Membres"
						},
						tobeactivated : {
							name : "Validation de compte",
							class : "col-xs-2 text-center"
						},
						isInviting : {
							name : "Validation pour être membres",
							class : "col-xs-2 text-center"
						},
						roles : {
							name : "Roles",
							class : "col-xs-1 text-center"
						},
						admin : {
							name : "Admin",
							class : "col-xs-1 text-center"
						}
					},
					paramsFilter : {
						container : "#filterContainer",
						defaults : {
							types : [ "citoyens"],
							fields : [ "name", "email", "links", "collection","roles","profilImageUrl" ],
							notSourceKey : true,
							indexStep: 50
						},
						filters : {
							text : true
						}
					},
					actions : {
						admin : true,
		            // roles : true,
						disconnect : true
					}
				};

				data.paramsFilter.defaults.filters = {};


				if(costum.contextType=="projects" || costum.contextType=="events"){
					data.paramsFilter.defaults.filters["links."+costum.contextType+"."+costum.contextId] = {
						'$exists' : 1 
					}
				}else{		
					costumizer.communityAdmin.views.dropdown = "Les membres";	
					data.paramsFilter.defaults.filters["$or"] = {};
					data.paramsFilter.defaults.filters["$or"]["links.memberOf."+costum.contextId] = {'$exists' :true};
					data.paramsFilter.defaults.filters["$or"]["roles.superAdminCms"] = {'$exists' :true};
				}

				if (typeof config.show != "undefined" && config.show != ""){				
					if (config.show == "lists") {
						costumizer.communityAdmin.views.dropdown = "Les admins et super admins";						
						data.paramsFilter.defaults.filters.slug = Object.values(costum.members[config.show]).map(entry => entry.slug);
					}else if (config.show == "active"){	
						costumizer.communityAdmin.views.dropdown = "Les membres actifs";					
						data.paramsFilter.defaults.filters.slug = Object.values(costum.members.active).map(entry => entry);
					}
					delete data.paramsFilter.defaults.filters["$or"]
				}else {
					costumizer.communityAdmin.views.dropdown = "Les membres";	
					data.paramsFilter.defaults.filters["$or"] = {};
					data.paramsFilter.defaults.filters["$or"]["links.memberOf."+costum.contextId] = {'$exists' :true};
					data.paramsFilter.defaults.filters["$or"]["roles.superAdminCms"] = {'$exists' :true};
				}
				var searchAdminType=(typeof paramsAdmin != "undefined" 
					&& typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["community"] != "undefined"
					&& typeof paramsAdmin["menu"]["community"]["initType"] != "undefined") ? paramsAdmin["menu"]["community"]["initType"]: ["citoyens"];
				data.paramsFilter.defaults.types=searchAdminType;
				let domTarget=(notEmpty(dom)) ? dom :'#content-view-admin'; 
				ajaxPost(domTarget, baseUrl+'/'+moduleId+'/admin/directory', data, function(res){
					if(typeof costum.htmlConstruct!="undefined" && 
						typeof costum.htmlConstruct.adminPanel!="undefined"  && 
						typeof costum.htmlConstruct.adminPanel.menu!="undefined" &&
						typeof costum.htmlConstruct.adminPanel.menu.community!="undefined" && 
						costum.htmlConstruct.adminPanel.menu.community.mailing){
						ajaxPost(null, baseUrl+'/'+moduleId+'/admin/mailing', {}, function(memberMailingHtml){
							$('#content-view-admin').append(memberMailingHtml);
						},"html");
					}
				},"html");


				$(".persist-loader").html(loadingMsg+"...").removeClass("hidden")
				$(document).ajaxStop(function() {
					if (!$(".invitation-link").length > 0) {
						var htmlInviteLink = `<div class="text-left" id="generated-link-container" style="display: flex;flex-wrap: wrap;border: solid 1px lightgrey;padding: 10px;">
						<b class="padding-bottom-5">Lien d'invitation en tant que Admin</b>
						<div style="background-color: white; width: 100%; min-height: 35px;padding: 5px;cursor: text;">

							<span id="generated-link" class="invitation-link">${costum.invitationLink}</span>						
								<div class="co-admin-dropdown pull-right">
								<button class="co-admin-dropbtn"><i class="fa fa-ellipsis-v"></i></button>
								<div class="co-admin-dropdown-content">
								<a href="javascript:;" class="btn-copy-link" style="background-color:white;"><i class="fa fa-clipboard" aria-hidden="true"></i> Copier le lien</a>
								<a href="#element/invite/type/${costum.contextType}/id/${costum.contextId}" class="lbhp"><i class="fa fa-cogs" aria-hidden="true"></i> ${tradCms.other} options</a>
								</div>
								</div>
							</div>
						</div>
						`
						var filterBtn = `
						<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle menu-button btn-menu filter-to-show" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								${costumizer.communityAdmin.views.dropdown} 
								<i class="fa fa-angle-down margin-left-5"></i>
							</a>
							<div class="dropdown-menu arrow_box " style="overflow-y: auto;" aria-labelledby="dropdownTypes">
								<div class="list-filters">
									<button type="button" class="btn-filters-select type col-xs-12" data-key="active">
										<span class="label-text">En ligne</span>
									</button>
									<button type="button" class="btn-filters-select type col-xs-12" data-key="lists">
										<span class="label-text">Admins et super admins</span>
									</button>
									<button type="button" class="btn-filters-select type col-xs-12" data-key="">
										<span class="label-text">Les membres</span>
									</button>
								</div>
							</div>
						</li>`
						$("#modal-content-costumizer-administration").find("#filterContainerInside").append(filterBtn)
						$("#modal-content-costumizer-administration").find(".headerSearchleft").parent().append(htmlInviteLink)
					}
					
					$(".btn-filters-select").off().on("click",function(){
						costumizer.communityAdmin.views.initAjax({dom : "#modal-content-costumizer-administration", show : $(this).data("key")})
					})
					
					$(".btn-copy-link").off().on("click",function(){
						var input = document.createElement("input");
						input.setAttribute("value", costum.invitationLink);

						document.getElementById("adminDirectory").appendChild(input);
						input.select();
						var result = document.execCommand('copy');
						document.getElementById("adminDirectory").removeChild(input);

						toastr.success("Le lien est copié.")

					})
					$(".lbhp").unbind("click").on("click",function(e) {
						e.preventDefault();
						$("#openModal").modal("hide");
						var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
						if( $(this).data("modalshow") ){	
							url = (h.indexOf("#") == 0 ) ? urlCtrl.convertToPath(h) : h;
							if(h.indexOf("#page") >= 0)
								url="app/"+url
							smallMenu.openAjaxHTML( baseUrl+'/'+moduleId+"/"+url);

						}
						else {
							url = (h.indexOf("#") == 0 ) ? urlCtrl.convertToPath(h) : h;
							smallMenu.openAjaxHTML( baseUrl+'/'+moduleId+"/"+url);
						}
					});
					// Admin online counter inside community list panel
					if (!$(".countOnline").length > 0) {
						var onlineHtml = `<h4 class="pull-left countOnline letter-green margin-left-5"> (<span class="onlineCounter"> ${typeof costum.members.onlineCounter != "undefined" ? costum.members.onlineCounter : "aucune personne "}</span><span class="count-lbl"> En ligne</span>)</h4>`
						$(".headerSearchleft").append(onlineHtml)
						$(".countOnline").off().on("click", function() {
							$(".btn-filters-select[data-key='active'").click()
						})
					}
					$("#modal-content-costumizer-administration .directoryLines").find(".name").each(function() {
						if ($(this).find("a").parent().prop("tagName") != "DIV") {
							var adminUrl = $(this).find("a").attr("href");
							if (typeof adminUrl != "undefined" && notNull(adminUrl.match(/[0-9a-fA-F]{24}$/))) {
								var adminId = adminUrl.match(/[0-9a-fA-F]{24}$/)[0];
								if (typeof costum.members["lists"][adminId] != "undefined") {
									$(this).html(costum.members["lists"][adminId].avatarWithName)
									$(this).removeClass("center")
								}/*else{									
									$(this).removeClass("center")
									if(!$(this).find(".costum-user-img-avatar").length > 0)
										$(this).addClass("bold")
										$(this).prepend(`<a class="padding-left-10" style="padding-right: 20px;"><i class="costum-user-img-avatar fa fa-user" data-placement="right" style="border: solid ${color1}; color:${color1}"></i></a`)
								}*/
							}
						}
					});
					$(".persist-loader").addClass("hidden")
				});
			}
		}
	},
	typeObj: {
		views: {
			init: function () {
				// Travail @Mahefa costumizer.typeObj
				// Initialiser la vue typeObj avec des élements (travail html et css à partir de la données)  :
				//	=> Afficher que les elements suivant : citoyens, project, organizations (nGo, governemental, business, group), events, classified
				//  => Pour chaque élément, afficher la couleur, l'icon et le nom afficher dans typeObj 
				// 	=> exemple des données pour une personne: {add: true,color: "yellow-k",createLabel: "Inviter vos amis",name: "Personne"}
				// Pouvoir éditer ces dernieres entrées  
				// => le add => true , avec switcher et expliquer que s'il est activé l'ajout d'elt sera disponible sur le bouton create element
				// Ajouter des nouveaux éléments (articles, formulaire, etc) => Deuxième temps !!!
				$.each(typeObj, function (e, obj) {
					var entryObj = e;
					if (typeof obj.sameAs != "undefined")
						entryObj = obj.sameAs;
					if (typeof costum.typeObj[e] != "undefined") {
						if (typeof costum.typeObj[e] == "object") {
							$.each(costum.typeObj[e], function (entry, value) {
								typeObj[entryObj][entry] = value;
							});
						}
					} else if (typeof obj.add != "undefined" && obj.add) {
						typeObj[entryObj].add = false;
					}
				});
				$.each(costum.typeObj, function (e, v) {
					if (typeof typeObj[e] == "undefined")
						typeObj[e] = v;
				});
				
				return costumizer.typeObj.views.showElements();
			},
			showElements: function () {
				let str = "", defaultObj = {}, specificObj = {}, newTypeObj = {}, costumTypeObj = [];
				$.each(typeObj, function (e, v) {
					if (( v.col == "organization" || v.col == "organizations" || v.col == "citoyens" || v.col == "projects" || v.col == "events" || v.col == "classifieds") && v.name != undefined) {
						jsonHelper.setValueByPath(v, "defaultElt", true)
						defaultObj[e] = v;
					}
				});
				
				Object.keys(defaultObj).map(function(k, v) {
					Object.keys(costum.typeObj).map(function(key, value) {
						if (k == key) {
							costumTypeObj.push(key);
						}
					})
				})

				$.each(costum.typeObj, function (e, v) {
					if (typeof typeObj[e].sameAs != "undefined" && typeof defaultObj[typeObj[e].sameAs] != "undefined") {
						delete defaultObj[typeObj[e].sameAs];
					}					
					if(!costumTypeObj.includes(e)) {
						jsonHelper.setValueByPath(v, "defaultElt", false)
						specificObj[e] = v;
					}
				})
				newTypeObj = {...defaultObj, ...specificObj}
				str += "<div class='row eltToggle' id='standard-element'>"
				$.each(newTypeObj, function (e, v) {
					if (jsonHelper.pathExists("costumizer.obj.typeObj."+e) && jsonHelper.getValueByPath(costumizer.obj.typeObj, e) === '$unset') {
						delete costumizer.obj.typeObj[e];
						v.add = false;
					}
						
					let context = (jsonHelper.pathExists("contextType")) ? contextType : (jsonHelper.pathExists("costum.contextType")) ? costum.contextType : "";

					if (context !== "" && typeObj.get(e).ctrl === "person") {
						jsonHelper.setValueByPath(costumizer.obj.typeObj, e+".explainText","")
						v.createLabel = (jsonHelper.pathExists("tradDynForm."+context+"Invite")) ? tradDynForm[context+"Invite"] : tradDynForm.defaultInvite;
						v.color = "red";
						v.icon = "plus-circle";

						if (jsonHelper.pathExists("costum.title"))
							v.createLabel = v.createLabel + " " + costum.title;
					} 

					v.name = (typeof v.addLabel !== "undefined") ? v.addLabel : v.name;

					if ( typeof jsonHelper.getValueByPath(typeObj, e + ".add") == "undefined")
						jsonHelper.setValueByPath(typeObj, e + ".add", false);

					let defaultLabel = e.split(/(?=[A-Z])/).map(function(value) {
						return value.charAt(0).toLowerCase() + value.slice(1);
						});

					const defaultValues = {
						color: "dark",
						createLabel: defaultLabel.join(" ").replace(/^./, defaultLabel.join(" ").charAt(0).toUpperCase()),
						name: defaultLabel.join(" ").replace(/^./, defaultLabel.join(" ").charAt(0).toUpperCase()),
						icon: "no-icon"
					};
						
					const setValue = (e, prop) =>  costumizer.typeObj.views.setElementValue(e, prop, v[prop]) || defaultValues[prop];
					
					for (let prop in defaultValues) {
						v[prop] = setValue(e, prop);
					}
					
					const escapeSingleQuote = (str) => {
						if (str !== undefined && str !== null) {
							return str.replace(/'/g,"&#146;");
						}
						return str;
					};
					
					if (typeof v.name != "undefined" && typeof v.createLabel !== "undefined" && (typeof v.createLabel === "string" || typeof v.name === "string")) {
						v.name = escapeSingleQuote(v.name);
						v.createLabel = (typeof costumizer.obj.typeObj[e] !== "undefined" && typeof costumizer.obj.typeObj[e].createLabel !== "undefined")? costumizer.obj.typeObj[e].createLabel : (trad["add"+e] !== undefined)? trad["add"+e] : v.createLabel;
					}

					const isHexColor = (color) => {
						if (costumizer.typeObj.actions.testHexColor(color)) {
							return '<div id="'+e+'-color-icon" class="icon" style="background-color:'+color+'">';
						} else {
							return '<div id="'+e+'-color-icon" class="icon bg-'+color+'" ">';
						}
					}

					str += 
					'<div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 standard-element-item standard-element-item-{$e}">' +
						`<div id="elt-${e}" class="standard-element-content ${(!v.add) ? "element-disabled" : ""}">`+
							'<div class="mid">'+
								'<label class="rocker">'+
									`<input type="checkbox" class="active-input" data-default=${v.defaultElt} data-value="${e}" ${(v.add) ? "checked" : ""}>`+
									'<span class="switch-left"><i class="fa fa-check"></i></span>'+
									'<span class="switch-right"><i class="fa fa-times"></i></span>'+
								'</label>'+
							'</div>'+	
							'<div class="icon-standard-element">'+
									isHexColor(v.color)+
									'<i id="'+e+'-icon-'+v.icon+'" class="fa fa-'+ v.icon + '"></i>'+
								'</div>'+
								'<div class="label-content">'+
									'<span id="label-content-'+e+'" >' + v.createLabel + ' </span>'+
									'<span id="name-content-'+e+'">(' + v.name + ')</span>'+
								'</div>' +
								`<a href="javascript:;" style="z-index: inherit;"
									onclick='costumizer.typeObj.events.edit("${e}","${escapeSingleQuote(v.createLabel)}","${v.name}","${v.color}","${v.icon}","${v.formType}","${v.formSubType}","${v.defaultElt}")'  class='btn-elt-${e} editElement badge'>` +
									'<i class="fa fa-pencil"></i> ' + trad.edit +
								'</a>'+
							'</div>' +
						'</div>'+
					'</div>';
				});

				str += "</div>";
				
				if (!notEmpty(str)) 
					str = "+ Ajouter des elements";

				return str;
			},
			setElementValue : function(elt, atr, value) {
				return 	(typeof jsonHelper.getValueByPath(costumizer.obj.typeObj[elt], atr) != "undefined")? 
						jsonHelper.getValueByPath(costumizer.obj.typeObj[elt], atr) : (typeof value == "undefined")? 
						(typeof typeObj[elt].sameAs != "undefined")? typeObj[typeObj[elt].sameAs][atr] : typeObj[elt][atr]
						: value;
			}
			
		},
		events: {
			bind : function(){	
				$(".active-input").on("change", function(e) {
					e.stopImmediatePropagation();
					if ($(this).is(":checked")) {
						$(this).closest('.standard-element-content').removeClass('element-disabled');
						jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value")+".add", true);
						costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
						$(".btn-elt"+$(this).data("value")).css({"pointer-events":""})
					} else {
						$(this).closest('.standard-element-content').addClass('element-disabled');
						if ($(this).data("default")) {
							jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value"), '$unset');
							costumizer.typeObj.actions.change("typeObj."+$(this).data("value"));
						} else {
							jsonHelper.setValueByPath(costumizer.obj.typeObj, $(this).data("value")+".add", false);
							costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
						}
									
						$(".btn-elt"+$(this).data("value")).css({"pointer-events":"none"});
					}
					// costumizer.typeObj.actions.change("typeObj."+$(this).data("value")+".add");
				})

				// if (screen.width > 560) {
					if ($("#right-sub-panel-container").attr("class") === "active") { 
						let contentWidth = $(".contains-view-admin").width() - 300;
							$('.eltToggle').animate({ width: contentWidth + "px" }, {duration: 300, queue: false, easing: 'swing'});
					}
					$("#right-sub-panel-container").find("button").click(function(){
						$('.eltToggle').animate({ width: "100%" }, {duration: 300, queue: false, easing: 'swing'});
						$(".selected-element").removeAttr("style").removeClass("selected-element");
					})
				// }

			},
			edit: function (key, eltLabel, eltName, eltColor, eltIcon, type, subType, defaultElt) {
				let infoValue = (jsonHelper.pathExists("costum.typeObj."+key+".dynFormCostum.beforeBuild.properties.info.html")) ? costum.typeObj[key].dynFormCostum.beforeBuild.properties.info.html : 
				                (jsonHelper.pathExists("costum.typeObj."+key+".dynFormCostum.onload.actions.html.infocustom")) ? costum.typeObj[key].dynFormCostum.onload.actions.html.infocustom : "";
				let formType = (typeObj.get(key).formType !== undefined)? typeObj.get(key).formType : (typeObj.get(key).ctrl !== undefined)? typeObj.get(key).ctrl : key;
					if (defaultElt === "true") {
						currentKFormType = (subType) ? subType : null;
						formType = (type !== "undefined")? type : key;
					} 

				let panelContent = {
					"key": key, 
					"createLabel": eltLabel, 
					"name": eltName, 
					"color": eltColor, 
					"icon": eltIcon, 
					"explainText": ""
				}
			
				if (defaultElt == "false") {
					panelContent.explainText = infoValue;
					costumizer.typeObj.events.openPanel(panelContent);
					formType = "none"
				}

				if (typeof $("#elt-"+key).html() === "string"){
					if ($(".selected-element").html() !== undefined)
						$(".selected-element").removeAttr("style").removeClass("selected-element");

					$("#elt-"+key).css("border", "2px solid").css("border-color", "#086784")
					$("#elt-"+key).addClass("selected-element")
				}

				if (formType !== undefined && formType !== "none"){
					dyFObj.getDynFormObj(formType, function() { 					
						try {
							infoValue = dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties.info.html
						} 
						catch (e) {}
			
						if (typeof currentKFormType !== "undefined" && currentKFormType !== null) {
								if(typeof typeObj[currentKFormType] !== "undefined" && typeof typeObj[currentKFormType].explainText !== "undefined") 
									infoValue = typeObj[currentKFormType].explainText;
						}

						panelContent.explainText = infoValue;
						costumizer.typeObj.events.openPanel(panelContent);
					});
				}
			  },
			  
			openPanel: function (eltValue) {
				let configRight = {
					width:300,
					header:{
						title:tradCms.ModifyElement,
						position:"top"
					}
				};

				for (let prop in costumizer.obj.typeObj[eltValue.key]) {
					$.each(eltValue, function (e, v) {
						if (prop == e) {
							eltValue[e] = costumizer.obj.typeObj[eltValue.key][prop]
						}
					})
				}
				
				if(!costumizer.liveChange)
					configRight.distanceToRight = 0;

				costumizer.actions.openRightSubPanel(
					'<div id="form-mockup-menu-edit" style="padding:0px 10px; margin-bottom:50px"></div>',
					configRight
				);

				let panelContent = {
					container: "#form-mockup-menu-edit",
					inputs: [
						{
							type: "inputSimple",
							options: {
								name: "createLabel",
								label: tradCms.label,
								defaultValue: eltValue.createLabel.replace(/<i[^>]*>|<\/i>/g, ''),
							}
						},
						{
							type: "inputSimple",
							options: {
								name: "name",
								label: trad.Name,
								defaultValue: eltValue.name,
							}
						},
						{
							type: "colorPicker",
							options: {
								name: "eltColor",
								label: tradCms.color,
								defaultValue: (!costumizer.typeObj.actions.testHexColor(eltValue.color))?
											  $('#'+eltValue.key+'-color-icon').css('background-color') : eltValue.color,
							}
						},
						{
							type: "inputIcon",
							options: {
								name: "icon",
								label: tradCms.icon,
								defaultValue: (eltValue.icon === "") ? "" : "fa fa-"+eltValue.icon,
							}
						},
						{
							type : "textarea",
							options: {
								name : "info",
								label : tradCms.elementInfo,
								defaultValue : eltValue.explainText.replace(/(<([^>]+)>)/ig, "")
							}
						},
					],
					onchange: function (name, value, payload) {
						let finalPath = costumizer.obj;
						let path = `typeObj.${eltValue.key}.${name}`;
						if (exists(value)) {
							if (name == "eltColor") {
								$('#'+eltValue.key+'-color-icon')	
									.attr('class', 'icon')
									.attr('style', 'background-color:'+value);
								path = `typeObj.${eltValue.key}.color`;
							}
							if (name == "icon") {
								let etlIcon = (eltValue.icon === "") ? "no-icon" : eltValue.icon;
								$('#'+eltValue.key+'-icon-'+etlIcon).attr('class', 'fa fa-'+value);
								jsonHelper.setValueByPath(costumizer.obj.typeObj[eltValue.key], "dynFormCostum.onload.actions.setTitle", "<i class='fa fa-"+value+"'></i> "+eltValue.createLabel);
								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle");
							}
							if (name == "createLabel") {
								$('#label-content-'+eltValue.key).replaceWith(`
									<span id="label-content-${eltValue.key}" >${value} </span>
								`); 
								jsonHelper.setValueByPath(
									costumizer.obj.typeObj[eltValue.key], 
									"dynFormCostum.onload.actions.setTitle", 
									`<i class='fa fa-${eltValue.icon}'></i> ${value}`
								);

								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.onload.actions.setTitle");
									
							}
							if (name == "name") {
								$('#name-content-'+eltValue.key).replaceWith(`
									<span id="name-content-${eltValue.key}">(${value})</span>
								`);

								if (jsonHelper.pathExists("typeObj."+eltValue.key+".addLabel") && !costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".addLabel")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".addLabel");
									
							}
							if (name == "info") { 
								let infoTemplate = "";
								if (value !== "")
									infoTemplate = "<p class='text-"+eltValue.color+"'><i class='fa fa-info-circle'></i> "+value
								jsonHelper.setValueByPath(
									costumizer.obj.typeObj[eltValue.key], 
									"dynFormCostum.beforeBuild.properties.info.html", 
									infoTemplate
								);
								path =`typeObj.${eltValue.key}.explainText`;
								if (!costumizer.actions.pathToValue.includes("typeObj."+eltValue.key+".dynFormCostum.beforeBuild.properties.info.html")) 
									costumizer.actions.pathToValue.push("typeObj."+eltValue.key+".dynFormCostum.beforeBuild.properties.info.html");
							}
							costumizer.typeObj.actions.updateChanges(finalPath, path, value);
						}
					}
				}
				if (new CoInput(panelContent)) {
					$('input[name="eltColor"]').parent().removeClass("sp-original-input-container").removeClass("sp-colorize-container");
				}
			}
		},
		actions: { 
			updateChanges: function(finalPath, path, value) {
				jsonHelper.setValueByPath(finalPath, path, value);
				costumizer.typeObj.actions.change(path);
			},
			callback : function(data){
				costumizer.htmlConstruct.actions.refreshmenu(data.params, costum.htmlConstruct);
			},
			testHexColor : function (color) {
				const hexColorRegex = /^#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{8})$/;
					if (hexColorRegex.test(color)) {
						return true
					}
					return false
			},
			change: function (path) {
				costumizer.actions.change(path);

				if (typeof $(".show-bottom-add").html() != "undefined") {
					if ($(".show-bottom-add").parent().length > 1) {
						$.each($(".show-bottom-add").parent(), function (id, content) {
							let btnPath = $(this).data("path");
							if (!costumizer.actions.pathToValue.includes("htmlConstruct."+btnPath)) {
								costumizer.actions.pathToValue.push("htmlConstruct."+btnPath);
							}	
						})
					} 
					else {
						costumizer.actions.change("htmlConstruct."+$(".show-bottom-add").parent().data("path"));
					}
				}
			}
		}
	},
	/*cms : {
		views : {
			init : function(){	
				return costumizer.template.views.init()
			}
		},
		actions : {
			init: function(){
				costumizer.template.actions.cmsModelList();				
	  		},
	  		bindEvents : function() {
	  			$(".bodySearchTemplate").on("click", ".use-this-template", function(e){
					var $this = $(this)	  				
					cmsConstructor.helpers.chooseBlockCms($this)
					e.stopImmediatePropagation();
					$(".close-dash-costumizer").click()
				})
	  			$(".bodySearchTemplate").on("click", ".share-this-template", function(e){
	  				let $this = $(this);
	  				if ($this.hasClass("action-hide")) {
	  					shareTpl = {
	  						id : $this.data("id"),
	  						collection : $this.data("collection"),
	  						path : "shared",
	  						value : true
	  					}
	  					dataHelper.path2Value( shareTpl, function(params) {
	  						$("#cmsBuilder-right-actions .lbh-costumizer").click()
	  						toastr.success("Shared!");
	  					})
	  				}else{
	  					hideTpl = {
	  						id : $this.data("id"),
	  						collection: $this.data("collection"),
	  						path : "shared"
	  					}
	  					dataHelper.unsetPath( hideTpl, function(params) {
	  						$("#cmsBuilder-right-actions .lbh-costumizer").click()
	  						toastr.success("hidden!");        
	  					} );
	  				}
	  				e.stopImmediatePropagation();
	  			})

	  			$(".bodySearchTemplate").on("click", ".mc-expans", function(e){
	  				e.stopImmediatePropagation();
	  				var $this_mc_btn_action = $(this)
	  				var card = $this_mc_btn_action.parents('.material-card');
	  				var icon = card.find('.mc-btn-action i');
	  				icon.addClass('fa-spin-fast');

	  				if (icon.hasClass("fa-arrow-left")) {
	  					card.removeClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").hide()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-arrow-left ripple')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-bars');

	  					}, 800);
	  				} else {
	  					card.addClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").show()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-bars')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-arrow-left ripple');

	  					}, 800);
	  				}
	  			})
	  			$(".bodySearchTemplate").on("click", ".preview_screenshot", function(e){
	  				e.stopImmediatePropagation();
	  				var tptScreenshot = $(this).data("screenshot")
	  				var tplViewScreen = ""
	  				$.each( tptScreenshot , function(k,val) { 
	  					tplViewScreen += `<img src="${val}">`
	  				});
	  				// mylog.log("screenshot",screenshot)
	  				var dialog = bootbox.dialog({
	  					message: tplViewScreen,
	  					size: 'large',
	  					buttons: {
	  						ok: {
	  							label: trad.Close,
	  							callback: function() {
	  							}
	  						}
	  					}
	  				});
	  			})
	  			$(".bodySearchTemplate").on("click", ".delete-tpl", function(e) {
					e.stopImmediatePropagation();
	  				directory.deleteElement($(this).data("collection"),$(this).data("id"),$(this));

	  			})
	  			$(".bodySearchTemplate").on("click", ".edit-tpl", function(e) {	  	
					e.stopImmediatePropagation();			
	  				costumizer.template.actions.update( $(this).data("id") , $(this).data("collection") ,$(this).data("subtype"))
	  			})
	  		}
		}
	},*/
	template: {	
		dataTpl:{},
		paramsTemplateList:{},
		category :{
			blockCms : {
			          	textImage : tradCms.TextWithImage,
			           // column : tradCms.column,
			            activity : trad.activity, 
			           // actuality : trad.news,
			            //team : tradCms.team
			        //} 
			    //},
			    //directory :{
			      //  name : trad.directory,
			        //subCategory:{
			            directory :trad.directory,
			            map : trad.map,
			          //  search : trad.search,
			            community: trad.community,
			            agenda: trad.agenda,
			            calendar: trad.calendar,
			            live : trad.live,
						aap : tradCms.aap,
			            //projects : trad.projects
			        //}
			    //},
			    //media : {
			     //   name : tradCms.media, 
			       // subCategory : {
			            galleryImage : tradCms.photoGallery,
			            galleryVideo : tradCms.videoGallery, 
			            documents : tradCms.documents,
						list : tradCms.list,
			        //}
			    
			    //},
			    //form : {
			      //  name : tradCms.forms,
			        //subCategory : {
			            //contactForm : trad.contact,
			            form : tradCms.forms,
			            faq : tradCms.faq, 
			            //coForm : tradCms.CoForm,
			            //aap : tradCms.aap,
			            //message : tradForm.message
			        //}
			    //},
			   	// observatory:{
			     	//   name : trad.observatory,
			       	// subCategory:{
			        observatory: trad.observatory,
			        graph : tradCms.graph
			       //     map : trad.map,
			        //chart : tradCms.chart
			        //}
			    //}
			},
			// page : {
		    //     activity 	: trad.activity, 
	        //    	directory 	: trad.directory,
			//     map 		: trad.map,
		    //     community   : trad.community,
			//     agenda 		: trad.agenda,
			//     calendar    : trad.calendar,
			//     live 		: trad.live,
		    //     contactForm : trad.contact,
			// 	faq 		: tradCms.faq, 
			//     observatory : trad.observatory,
			//     graph 		: tradCms.graph,
			//  	coEvent 	: "CoEvent",
			// 	training    : "training"
			//     //chart : tradCms.Chart
			// },
			costum : {
		        activity 			  : trad.activity, 
				thirdPlace 			  : tradCategory.thirdPlace, 
			 	commons 			  : tradCategory.commons, 
			 	greenTransition		  : tradCategory.greenTransition,
			 	aap 				  : tradCategory.aap,
			 	territorialConnectivity : tradCategory.territorialConnectivity,
			 	Cocity 				  : tradCategory.Cocity, 
			 	Community 	 		  : trad.community,
			 	citizenAgora 	  	  : tradCategory.citizenAgora, 
			 	researchAndInnovation : tradCategory.researchAndInnovation,
			 	socialMapping 		  : tradCategory.socialMapping,
			 	media 				  : "Media",
			 	ArtsAndMusic 	  	  : tradCategory.ArtsAndMusic,
			 	SportAndActivity 	  : tradCategory.SportAndActivity,
			 	coEvent 			  : "CoEvent",
				training              : "training",
				faq 				  : tradCms.faq, 
			    observatory			  : trad.observatory,
			    notes			  	  : trad.Notes,
			 	businessCorporation   : tradCategory.businessCorporation
			}
		},
		form: {
			idBlocToDuplicate : "",
			collection : "templates",
			jsonSchema : {
				col    : "templates",
                modalFormClass : "template-form-design",
                beforeBuild : function(){
			    	uploadObj.update = true; 
			    	uploadObj.type = "templates";
			    	uploadObj.id = dyFObj.currentElement.id;
			    	// formData["subtype"] = "lmd"
			    },
			    afterSave : function(data,callB){ 
					if( data.result && typeof data.map.blocTplId !== "undefined" ) {
						ajaxPost(
							null,
							baseUrl+"/co2/cms/saveasblockcms",
							{
								idCmsToDuplic : costumizer.template.form.idBlocToDuplicate,
								newId : data.map.blocTplId,
								type : "templateBlock",
								costumId    : costum.contextId,
								costumType  : costum.contextType,
								costumSlug : costum.contextSlug
							}, 
							function (response) {          
							}
						);
					}

			    	dyFObj.commonAfterSave(data,function(data){});
			    	var tplData = {
			    			    id : data.id,
			    			  path : "allToRoot",
			    			 value : {},
			    		collection : "templates"
			    	}
			    	if(data.map.type =="costum"){	
			    		if (costumizer.template.canEditParams) {
				    		tplData.value.siteParams = {
				    					  css : costumizer.obj.css,
				    			          app : costumizer.obj.app,
				    			htmlConstruct : costumizer.obj.htmlConstruct,
				    		}

				    		if (typeof costumizer.obj.lists != "undefined"){
				    			tplData.value.siteParams.lists = costumizer.obj.lists;
				    		}
				    		if (typeof costumizer.obj.js != "undefined"){
				    			tplData.value.siteParams.js = costumizer.obj.js;
				    		}
				    		if (typeof costum.typeObj != "undefined" && notEmpty(costum.typeObj)){
				    			tplData.value.siteParams.typeObj = costum.typeObj;
				    		}

				    		tplData.value.cmsList = costumizer.template["costumCms"];

				    		dataHelper.path2Value( tplData, function(params) {			 	
				    			costumizer.template.helpers.saveTplContent(data.id)
				    			costumizer.template.actions.openPreview(data.id, data.map.type, data.map.name);
				    			costumizer.template.views.initAjax({space:"template",key:data.map.type});
				    		})
				    	}else{				    		
				    		costumizer.template.actions.openPreview(data.id, data.map.type, data.map.name);
				    		costumizer.template.views.initAjax({space:"template",key:data.map.type});
				    	}	

				    }else if(data.map.type=="page"){  
				    	if (costumizer.template.canEditParams) {
				    		tplData.value.cmsList = cmsConstructor.cmsList;
				    		dataHelper.path2Value( tplData, function(params) {			 
				    			costumizer.template.helpers.saveTplContent(data.id,page)   			
				    			costumizer.template.actions.openPreview(data.id, data.map.type, data.map.name);				    			
				    			costumizer.template.views.initAjax({space:"template",key:data.map.type});
				    		})
				    	}else{
				    		costumizer.template.actions.openPreview(data.id, data.map.type, data.map.name);
				    		costumizer.template.views.initAjax({space:"template",key:data.map.type});
				    	}
				    }

			    },
                properties : {
                    name : {
                        label: tradCms.nameTpl,
                        inputType : "text",
                        rules:{
                            required:true
                        }
                    },  
                    category : {
                        label : trad.category,
                        inputType : "selectMultiple",
                        isSelect2: true,
                       // select2: {tags: true},
                        rules:{
                            required:true
                        },
                        options : {},
                        placeholder : tradCms.selectAnOption
                    },
                    image: dyFInputs.image(),
                    tplAlbum : {
                    	inputType: "uploader",
                    	docType: "image",
                    	contentKey: "slider",
						endPoint : "/subKey/tplAlbum",
						domElement : "tplAlbum",		
						filetype : ["jpeg", "jpg", "gif", "png"], 
                    	label: "Image pour carrousel teasing:",
                    	itemLimit: 5
                    },
                    description : {
                        label: tradDynForm.description,
                        inputType : "textarea",
                        value : ""
                    },
                    medias : {
                		// label : "teasing video",
				    	placeholder : tradDynForm.sharevideourl+" ...",
				        inputType : "hidden",
				        value : [],
				        initOptions : {type:"video",labelAdd:"Add video link"},
				        init:function(){
				            processUrl.getMediaFromUrlContent(".addmultifield0", ".resultGetUrl0",2, "video");
				        }
                    },
                    type : {
                    	inputType: "hidden",
                    	value:null
                    }
                }  
            }
		},
		views : {
			initAjax : function(config){	
				$('#modal-blockcms').addClass('open');
				$('.information').html("<h3 style='color: white !important;'>Liste des Templates "+ config.key+"<h3>");
				$("#modal-template-results").attr("data-type",config.key)
				$("#btn-hide-modal-blockcms").show()				
				costumizer.template.paramsTemplateList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: ".modal-blockcms-header .modal-filter-template",
					loadEvent : {
						default : "scroll"
					},
					scroll : {
						options : {
							domTarget:"#modal-template-results",
							scrollTarget: ".modal-blockcms-list"
						}
					},
					defaults: {
						notSourceKey: true,
						types: ["templates"],
						filters: {
							type: config.key,
							shared: true
						},
						fields : ["userCounter","shared","blocTplId","source.key"]
					},
					results: {
						dom: "#modal-template-results",
						smartGrid: true,
						imgObsCallback : function($this){
							if($($this).outerHeight()<=175){
								$($this).addClass("centerImgMinHeight");
								$($this).css({"margin-left" : -($($this).width()/6)});
							}
						},
						renderView: "costumizer.template.views.templateListHtml",
						map: {
							active: false
						},
						callBack : function(obj){
							costumizer.template.events.bind();
						}
					},
					filters: {
						text: true,
						category: {
							view: "verticalList",
							dom : "#category-template-filter",
							type: "filters",
							field: "category",
							event: "filters",
							keyValue:false,
							multiple:true,
							active : true,
							list: costumizer.template.category[config.key]
						}
					}
				};
				if (isSuperAdminCms) {
					delete costumizer.template.paramsTemplateList.defaults.filters.shared;
					costumizer.template.paramsTemplateList.filters["shared"] = {
						view: "dropdownList",
						type: "filters",
						action: "filters",
						name: "Trier par restriction",
						icon: "globe",
						typeList : "object",
						event : "exists",
						keyValue: false,
						list: {
							restricted : {
								label: "Unrestricted",
								field : "shared",
								value : true
							},
							shared : {
								label: "Restricted",
								field : "shared",
								value : false
							}
						}
					}
				}
				if (typeof config.category != "undefined") {
					costumizer.template.paramsTemplateList.defaults.filters["category"] = config.category;
				}

				
				if (typeof config.page != "undefined") {
					costumizer.template["targetPage"] = config.page;
				}else{
					delete costumizer.template.targetPage
				}

				if ( config.key == "page") {					
					costumizer.template.paramsTemplateList.filters.category.list = costumizer.template.categoryused[config.key];
					$(".tpl-info-container").css("background","linear-gradient(to right, #333333, #4CAF50, #333333)")
				}
				if ( config.key == "costum"){
					$(".tpl-info-container").css("background","linear-gradient(to right, #333333, #ffa500, #333333)");
					// var pageAndCostumCategory = costumizer.template.categoryused.costum
					// for (const key in costumizer.template.categoryused.page) {
					// 	if (costumizer.template.categoryused.page.hasOwnProperty(key) && !pageAndCostumCategory.hasOwnProperty(key)) {
					// 		pageAndCostumCategory[key] = costumizer.template.categoryused.page[key];
					// 	}
					// }
					// costumizer.template.paramsTemplateList.filters.category.list = pageAndCostumCategory;
					costumizer.template.paramsTemplateList.filters.category.list = costumizer.template.categoryused["costum"];
					// costumizer.template.paramsTemplateList.defaults.filters.type = [config.key,"page"]
					costumizer.template.paramsTemplateList.defaults.filters.type = [config.key]
				}
				if ( config.key == "blockCms"){
					costumizer.template.paramsTemplateList.filters.category.list = costumizer.template.categoryused["blockCms"];
					$(".tpl-info-container").css("background","linear-gradient(to right, #333333, #2196F3, #333333)") 
				}
				var filterGroupTemplate = searchObj.init(costumizer.template.paramsTemplateList);
				filterGroupTemplate.search.init(filterGroupTemplate);
				
			},
			initDocument : function(tplData) {
				var screenShotItem = ""
				var data = {}
				var completedItems = 4;
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/getlistfile",
					{costumType : tplData.collection, costumId : tplData.id, doctype : "image"},
					function (screenData) {
						completedItems -= screenData.result.length
						screenShotItem += '<font class="">Screenshot</font><br>'
						// screenShotItem += '<strong class="">NB: Le nombre maximum de screenshot est limité à 4</strong>'
						screenShotItem += '<div class="biblio-image" style="border: solid 1px #BDBDBD;">'
						for (var i = 0; i < 4; i++) {
							if (typeof screenData.result[i] != "undefined") {
								screenShotItem += `
								<div class="img-content">
								<img src="${screenData.result[i].imagePath}" data-value="${screenData.result[i].imagePath}" class="image-on-bibliotheque">
								</div>`
							}else{
								screenShotItem += `
								<div class="img-content">
								<button class="insert-screenshot btn" style="width: 100%;height: 100%;"><i class="fa fa-plus" style="background-color: inherit;font-size: 35px;color: darkgray;"></i></button>
								</div>`
							}
						}
						screenShotItem += '</div>';
					},
					null,
					"json",
					{
						async: false
					}												
					);
					data = {html : screenShotItem, remainLength : completedItems}
				return data;
			},
			/*openCostumizer : function(tplType) {
				// let path = (canSelectMultipleImage) ? [] : "";
				// bibliotheque = getListImageDocuments();

				var tplData = {};
				var screenShot = [];
				tplData.value={}

				if (tplType == "page") {
					dyFObj.setMongoId('cms',function(data){
						tplData.id=dyFObj.currentElement.id;
						tplData.collection=dyFObj.currentElement.type;
						tplData.value.cmsList = cmsConstructor.cmsList;
					});
				}else{									
					dyFObj.setMongoId('templates',function(data){
						// tplData.id=dyFObj.currentElement.id;
						tplData.collection=dyFObj.currentElement.type;
					});
					tplData.id = "65688edfd49bca61277a888b";
					tplData.collection = "templates";
					// params["allPage"] = true;
					tplData.value.siteParams = {}
					tplData.value.siteParams.css = costumizer.obj.css;
					tplData.value.siteParams.app = costumizer.obj.app
					tplData.value.siteParams.htmlConstruct = costumizer.obj.htmlConstruct;
					if (typeof costumizer.obj.lists != "undefined") {
						tplData.value.siteParams.lists = costumizer.obj.lists;
					}
					tplData.value.subtype = "site";
				}

				tplData.value.parent={};
				tplData.value.parent[costum.contextId] = {
					type : costum.contextType, 
				};
				var tplDocument = costumizer.template.views.initDocument(tplData)					
				var tplHtml = `				
			        <div id="stepper" style="min-height:310px;">
			            <div class="template-step" id="tpl-step1">
			                <div class="form-group">
							    <label class="col-xs-12 text-left control-label no-padding" for="name">
							        <i class="fa fa-chevron-down"></i> Nom du template*
							    </label>
							    <input type="text" class="form-control" id="tplName" value="" placeholder="">
							</div>
							<div class="form-group">
							    <label class="col-xs-12 text-left control-label no-padding" for="category">
							        <i class="fa fa-chevron-down"></i> Catégorie*
							    </label>
							    <select class="form-control" placeholder="Choisir une option" id="tplCategory" style="width: 100%; height:auto;" data-placeholder="Choisir une option*">
							        <option class="text-red" style="font-weight:bold" disabled="" selected="">Choisir une option</option>
							        <option value="AppelAProjet" data-value="">AppelAProjet</option>
							        <option value="ArtsAndActivity" data-value="">ArtsAndActivity</option>
							        <option value="Cocity" data-value="">Cocity</option>
							        <option value="Commons" data-value="">Commons</option>
							        <option value="CompagnyPrez" data-value="">CompagnyPrez</option>
							        <option value="EnvironmentChange" data-value="">EnvironmentChange</option>
							        <option value="Institution" data-value="">Institution</option>
							        <option value="Observatory" data-value="">Observatory</option>
							        <option value="Smarterre" data-value="">Smarterre</option>
							        <option value="TiersLieux" data-value="">TiersLieux</option>
							        <option value="citizenPower" data-value="">citizenPower</option>
							        <option value="socialMapping" data-value="">socialMapping</option>
							        <option value="technologicalGeek" data-value="">technologicalGeek</option>
							    </select>
							</div>
							<div class="form-group">
							    <label class="col-xs-12 text-left control-label no-padding" for="name">
							        <i class="fa fa-chevron-down"></i> Description du template*
							    </label>
							    <textarea type="textarea" rows="6" class="form-control" name="name" id="tplDescription" value="" placeholder=""></textarea>
							</div>
							<div class="form-group">
							   ${tplDocument.html}
							</div>
			            </div>
			            <div class="template-step" id="tpl-step2" style="display:;"></div>	
			            <div class="template-step bg-white padding-10" id="tpl-step3" style="display:none;position: fixed;top: 0;z-index:10;border-radius: 5px;left:0;">
			            	<div class="uploader"></div>
			            	<div class="text-center padding-15">
				            	<button type="button" class="btn btn-default closeUploader margin-top-10">${trad.cancel}</button>
				            	<button type="button" class="btn btn-primary starUpload margin-top-10">OK</button>
			            	</div>
			            </div>					           
			        </div>
				`;
				var tplTitle = `
				<div class="bg-dark" style="border-radius: 50px;">
				    <h3 class="modal-title text-center" id="ajax-modal-modal-title">
				        <i class="fa fa-undefined"></i> Enregistrement du template
				    </h3>
				</div>`;


				bootbox.dialog({
					message: tplHtml ,
					title: tplTitle,
					size: 'fullscreen',
					buttons: { 
						back: {
							label: trad.previous,
							className: "btn-success hidden backToDescription",
							callback: function() {
								var $this = $(this)
								$(".template-step").hide();
								$("#tpl-step1").show();
								$this.find('.backToDescription').addClass('hidden');
								$this.find('.goToScreenshot').removeClass('hidden');
								$this.find('.btnFinish').addClass('hidden');
								// costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))
								return false;
							}
						},
						continue: {
							label: trad.save,
							className: "btn-success goToScreenshot",
							callback: function() {
								var $this = $(this)								
								tplData.value.name = $this.find("#tplName").val()
								tplData.value.description = $this.find("#tplDescription").val()
								tplData.value.category = $this.find("#tplCategory").val()
								$(".template-step").hide();
								$("#tpl-step2").show();
								$this.find('.goToScreenshot').addClass('hidden');
								$this.find('.backToDescription').removeClass('hidden');
								$this.find('.btnFinish').removeClass('hidden');
								$this.find('.btn-cancel').remove();
								ajaxPost(
									null,
									baseUrl+"/"+moduleId+"/cms/updatetemplate",
									tplData,
									function(data){},
									null,
									"json",
									{async: false}
									); 

								if ($('#tpl-step2').html() == '') {		
									$this.on('click', ".insert-screenshot", function(){		
										$this.find("#tpl-step3").show()
										$this.find('.backToDescription').addClass('hidden');	
										$this.find('.btnFinish').addClass('hidden');		
										if ($("#tpl-step3 .uploader").html() == "") {
											$.dynForm({
												beforeBuild : function(){	
													mylog.log("tplData.collection",tplData.collection, tplData.id, tplDocument.remainLength)
													uploadObj.set(tplData.collection,tplData.id);
													uploadObj.initUploader();
												},
												formId: "#tpl-step3 .uploader",
												formObj : {
													jsonSchema : {  
														properties : {
															"image": {
																"inputType": "uploader",
																"docType": "image",
																"contentKey": "templates",
																"endPoint" : "/subKey/tplalbum",
																"domElement" : "tplalbum",		
																"filetype" : ["jpeg", "jpg", "gif", "png"], 
																"label": "Image :",
																"itemLimit": tplDocument.remainLength,
																onComplete: function(data){
																	screenShot.push(data.docPath);
																}
															}
														}
													}	
												}
											})
										}
									})

									$this.on('click', ".starUpload" , function(){
										$this.find("#tpl-step3").hide()
										$this.find('.backToDescription').removeClass('hidden');	
										$this.find('.btnFinish').removeClass('hidden');
										dyFObj.commonAfterSave(params = null, function() {	
											var tplDocument = costumizer.template.views.initDocument(tplData)					
											$('#tpl-step2').html(tplDocument.html);	
										});	
									})
									$this.on('click', ".closeUploader" , function(){
										$this.find("#tpl-step3").hide()
										$this.find('.backToDescription').removeClass('hidden');	
										$this.find('.btnFinish').removeClass('hidden');	
									})
								}
								return false;
							}
						},
						finish: {
							label: trad.finish,
							className: "btn-primary btnFinish hidden",
							callback: function() {
								var $this = $(this)
								$(".template-step").hide();
								$("#tpl-step2").show();
								$this.find('.backToDescription').addClass('disabled');
								$this.find('.btnFinish').addClass('disabled');
								mylog.log("humm2",screenShot)
								
								return false;
							}
						},
						cancel: {
							label: trad.cancel,
							className: 'btn-default btn-cancel'
						}
					}
				});
			},*/
			templateListHtml: function (params) {
				imgSrc=baseUrl+assetPath+'/images/templates/default_directory.png';
				if( typeof params.screenshot != 'undefined' && params.screenshot != '') 
					imgSrc=baseUrl + '/' + params.screenshot[0];
				else if( typeof params.profilMediumImageUrl != 'undefined' && params.profilMediumImageUrl != '')
					imgSrc=baseUrl + '/' + params.profilMediumImageUrl;
				var str =  '<div id="entity_'+params.collection+'_'+params.id+'" class=" shared-'+params.shared+' col-md-4 col-sm-6 col-xs-12 searchEntityContainer smartgrid-slide-element start-masonry msr-'+params.id+'">'+
                          		'<div class="gallery-masonry-item-wrapper preFade fadeIn" data-animation-role="image" style="overflow: hidden; transition-timing-function: ease; transition-duration: 1.5s; transition-delay: 0.57801s;">'+
									'<a href="javascript:;" class="gallery-masonry-lightbox-link" style="height: 100%;">'+
					 					'<img class="img-responsive lzy_img isLoading" data-id="'+params.id+'" data-src="'+imgSrc+'"/>'+
            						'</a>'+
            						'<div class="gallery-masonry-lightbox-link tools-kit" style="height: 100%;">'+
            							'<div class="btnContent">'+
				       						'<a href="javascript:;" class="btn btn-prymary use-this-template margin-bottom-10" data-id="'+((params.type=="blockCms") ? params.blocTplId : params.id)+'" data-type="'+params.type+'"><i class="fa fa-plus"></i> '+tradCms.choose+'</a>'+
				       						`<a href="javascript:;" class="btn btn-prymary see-this-template margin-top-10" data-source="${typeof params.source != 'undefined' ? params.source.key:''}" data-id="${params.id}" data-type="${params.type}" data-title="${params.name}"><i class="fa fa-eye"></i> ${tradBadge.viewDetails}</a>`+
			       						'</div>'+
			       					'</div>'+
        						'</div>'+
        						'<div class="gallery-caption gallery-caption-grid-masonry">'+
						            '<div class="gallery-caption-wrapper">'+
						            	'<p class="gallery-caption-content preFade fadeIn" style="transition-timing-function: ease; transition-duration: 1.5s; transition-delay: 0.581152s;">'+
						            		params.name;
						            		if(typeof params.shared == "undefined" || !params.shared)
					str+=						"<span style='cursor:pointer;color:orange;' class='share-this-template locked pull-right' data-id='"+params.id+"' data-type='"+params.type+"'><span class='tooltips' data-toggle='tooltip' data-original-title='Cliquez pour rendre publique'>Verroullé <i class='fa fa-lock' ></i></span></span>";
											else
					str+=						"<span style='cursor:pointer;' class='share-this-template pull-right' data-id='"+params.id+"' data-type='"+params.type+"'><i class='fa fa-globe tooltips' data-toggle='tooltip' data-original-title='Cliquez pour verrouller'></i></span>";

					str+=	            	'</p>'+
						            '</div>'+
						        '</div>'+      
        					'</div>';
        		return str;
			},
			preview : function(data){
				var str="";
				if(notNull(data.profilImageUrl) || notNull(data.medias) || notNull(data.tplAlbum)){
					str+=`<div class="swiper swiperElement">
						    <div class="swiper-wrapper">`;
							    if(notEmpty(data.medias))
							    	$.each(data.medias, function(e, v){	
							    		if (notEmpty(v)) {
							    			str+=`<div class="swiper-slide"><iframe width="560" height="315" src="${v.content.videoLink ? v.content.videoLink : ''}" title="YouTube video player" frameborder="0" allow="autoplay; clipboard-write; encrypted-media;" allowfullscreen></iframe></div>`;
							    		}
							    	});
						      	if(notEmpty(data.profilImageUrl))
					str+=			'<div class="swiper-slide"><img src="'+baseUrl+"/"+data.profilImageUrl+'" /></div>';
						   		if(notEmpty(data.tplAlbum)){
							   		$.each(data.tplAlbum, function(e, v){
					str+=    			'<div class="swiper-slide"><img src="'+baseUrl+"/"+v.imagePath+'" /></div>';
							    	});
						   		} 
					str+=	'</div>'+
						  	'<div class="swiper-template-pagination"></div>'+
						'</div>';
				}
				if(notEmpty(data.category)){
					str+="<div class='categoryList'>";
					if(typeof data.category == "string")
						str+="<span class='badge'>"+data.category+"</span>";
					else{
						$.each(data.category, function(e, v){
							str+="<span class='badge'>"+v+"</span>";
						});
					}
					str+="</div>";
				}
				if(notEmpty(data.description))
					str+="<p class='description'>"+data.description+"</p>";
				if (notEmpty(data.usesTpl)) {
					var fusionUsesTpl = {};
					//mylog.log("data.usesTpl",data.usesTpl);
					$.each(data.usesTpl, function(index, tplUse) {
						if (tplUse.source && tplUse.source.key) {
							if (fusionUsesTpl[tplUse.source.key] && !page.includes(tplUse.page)) {
								fusionUsesTpl[tplUse.source.key].pages.push(
									tplUse.page
								);
							} else {
								fusionUsesTpl[tplUse.source.key] = {
									sourceKey: tplUse.source.key,
									pages: [tplUse.page]
								};
							}
						}
					});

					str += ` <h3 style="color:#29b0a0">Utiliser ${Object.keys(data.usesTpl).length} fois dans des costum :</h3><br>
							<div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">`;
					$.each(fusionUsesTpl, function(e, v){
					str += ` <div class="col-md-6 col-sm-6 col-xs-12 usesTpl-panel">
								<div class="panel panel-default">
									 <div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#${e}" aria-expanded="true" aria-controls="collapseOne">
												${e} 
											</a>
										</h4>
									 </div>
									 <div id="${e}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">`;
						$.each(v.pages, function(key, page){
							if (typeof  page == "undefined" )
								str += `<a target="_blank" class="btn" href="${baseUrl}/costum/co/index/slug/${e}" style=" border: 1px solid; margin: 3px;"> welcome <i class="fa fa-fw fa-arrow-right ripple"></i> </a>`;
							else
								str += `<a target="_blank" class="btn" href="${baseUrl}/costum/co/index/slug/${e}#${page}" style=" border: 1px solid; margin: 3px;"> ${page} <i class="fa fa-fw fa-arrow-right ripple"></i> </a>`;
						});

					str += `			</div>
									</div>
								</div>
							</div>`;
					});
					str += `</div>`;
				}
				if (notEmpty(data.tplJson)) {
					var strJson = '<div style="display: inline-block;text-align: left;color: white;">'
					strJson += '<label value="default" selected>À utiliser avec du contenu</label><br>	'
					strJson += '<select class="template-json-selector form-control" style="width: 200px;display: inline;padding-top: 1px;">'
					strJson += '<option style="color: gray;" value="default" selected>'+trad.default+'</option>'
					$.each(data.tplJson, function(jsonKey, jsonValue){						
						// console.log("preview",JsonValue)
						strJson += '<option value="'+jsonKey+'">'+jsonValue.thematic+'</option>'
					});
					strJson += '</select></div>'
					if ($(".template-json-selector").length == 0) {						
						$("#modal-preview-template .action-template").prepend(strJson)
					}else{
						$("#modal-preview-template .template-json-selector").parent().replaceWith(strJson)
					}
				}else{
					$("#modal-preview-template .template-json-selector").parent().remove()
				}

				$("#modal-preview-template .preview-body").html(str);
			
			},
			/*useTplsView : function () {
				var
				$("#modal-preview-template .preview-body").append(str);
			}*/
	  	},
	  	events : {
	  		bind : function(){
	  			$('.searchEntityContainer').off().on("mouseenter", function () {
					$(this).find(".tools-kit").show();
				}).on("mouseleave",function(){
					$(this).find(".tools-kit").hide();	
				});
				$(".see-this-template").off().on("click", function(){
					costumizer.template.actions.openPreview($(this).data("id"), $(this).data("type"), $(this).data("title"),$(this).data("source"));
				});
				$(".use-this-template").off().on("click", function(){
					var $this = $(this)
					$this.addClass('disabled');
					$this.html('<span style=""><i class="fa fa-spin fa-circle-o-notch"></i> <span>'+trad.processing+' ...</span></span>');	
					if($this.data("type")=="blockCms"){
						cmsConstructor.helpers.chooseBlockCms($this);
						costumizer.template.actions.closeModal();
					}else if ($this.data("type")=="costum") {						
		  				if (Object.keys(cmsConstructor.sp_params).length > 1 || costum.app.length > 1) {
		  					bootbox.dialog({
		  						message: `<p>${tradCms["It appears that you have recently modified your site"]}</p>
		  						<div class="alert alert-warning" role="alert">
		  						<p><b><i class="fa fa-exclamation-triangle">
		  						</i> NB</b>: ${tradCms["By choosing a Template, you risk losing all of your configuration"]}.</p>
		  						</div>`,
		  						title: "",
		  						onEscape: function() {},
		  						show: true,
		  						backdrop: true,
		  						closeButton: true,
		  						animate: true,
		  						className: "my-modal",
		  						buttons: { 
		  							continue: {
		  								label: tradCms["OK, I choose this template anyway"],
		  								className: "btn-success",
		  								callback: function() {
		  									if (costum.members.onlineCounter > 1) {
		  										var activeAdminHtml = ""
		  										var activeAdminArray = costum.members.active;
		  										delete activeAdminArray[userId]
		  										$.each( activeAdminArray , function(k,val) { 
		  											activeAdminHtml += `
		  											<div class="sp-cms-100">
		  												<div class="sp-cms-90">${costum.members.lists[k].avatar} <div style="padding: 10px;">${costum.members.lists[k].name}</div></div>
		  												<div class="tpl-confirmation" data-userid="${k}"><i class="fa fa-check-circle text-white" style="font-size: 30px;padding-top: 5px;"></i></div>
		  											</div>`
		  										});

												costumizer.tplChange({functionName: "templateChoosed", data: {type:"costum", tplId : $this.data("id")}})

		  										bootbox.dialog({
		  											message: `<p>Veuillez attendre la confirmation des autres administrateurs actifs</p>
		  											${activeAdminHtml}
		  											`,
		  											title: "Accord entre administrateurs",
		  											onEscape: function() {},
		  											show: true,
		  											backdrop: true,
		  											closeButton: false,
		  											animate: true,
		  											className: "my-modal",
		  											backdrop: 'static',
		  											buttons: { 
		  												cancel: {
		  													label: trad.cancel,
		  													className: 'btn-default'
		  												}
		  											}
		  										});
		  									}else{
		  										if (typeof $(".template-json-selector").val() != "undefined" && $(".template-json-selector").val() != "default") {
		  											co.importCostumContent({ thematic : $(".template-json-selector").val(), page : page })
		  											.then(() => {
		  												window.location.reload();
		  											})
		  											.catch((error) => {
		  												toastr.error("Error importing content:", error);
		  											});
		  										}else{
		  											costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))		  											
		  										}
		  									}

		  								}
		  							},
		  							cancel: {
		  								label: trad.cancel,
		  								className: 'btn-default'
		  							}
		  						}

		  					});
		  				}else{ 		
							costumizer.tplChange({functionName: "templateChoosed", data: {
								page : typeof costumizer.template["targetPage"] != "undefined" ? costumizer.template["targetPage"] : page,
		  						type:"costum"
							}})
		  					costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))
		  				}
		  			}else{  
						costumizer.tplChange({functionName: "templateChoosed", data: {
							page : typeof costumizer.template["targetPage"] != "undefined" ? costumizer.template["targetPage"] : page,
		  					type:"page"
						}})
		  				costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))
		  			}
				});
				$(".delete-template").off().on("click", function() {
	  				costumizer.template.actions.delete($(this).attr("data-id"),$(this));
	  			});
	  			$(".edit-template").off().on("click", function() {
		  			if (costum.slug == $(this).data("source"))
		  				costumizer.template["canEditParams"] = true
		  			else
		  				costumizer.template["canEditParams"] = false
	  				costumizer.template.actions.update( $(this).attr("data-id") ,$(this).attr("data-type"));
	  			});
	  			$("#modal-preview-template .close-modal").off().on("click", function(){
	  				$("#modal-preview-template").removeClass("open");
	  			});
	  			$(".share-this-template").off().on("click", function(e){
	  				let $this = $(this);
	  				if ($this.hasClass("locked")) {
	  					shareTpl = {
	  						id : $this.data("id"),
	  						collection : "templates",
	  						path : "shared",
	  						value : true
	  					}
	  					dataHelper.path2Value( shareTpl, function(params) {
	  						$this.hide()
	  						costumizer.template.views.initAjax({space:`template`,key:$this.data("type")});
	  						toastr.success("Shared!");
	  					})
	  				}else{
	  					hideTpl = {
	  						id : $this.data("id"),
	  						collection: "templates",
	  						path : "shared"
	  					}
	  					dataHelper.unsetPath( hideTpl, function(params) {
	  						costumizer.template.views.initAjax({space:`template`,key:$this.data("type")});
	  						toastr.success("hidden!");        
	  					} );
	  				}
	  				e.stopImmediatePropagation();
	  			});
	  		}
	  	},
	  	actions: {
	  		closeModal : function(){
	  			$("#modal-blockcms").removeClass("open");
				if($("#modal-preview-template").is(":visible")){
					$("#modal-preview-template").removeClass("open");	
				}
		
	  		},
	  		openPreview : function(id, type, title,source=""){
	  			coInterface.showLoader("#modal-preview-template .preview-body");
	  			$("#modal-preview-template .title-template").text(title);
	  			$("#modal-preview-template .action-template .btn").each(function(){
	  				/*var str=	'<a href="javascript:;" class="btn btn-default use-this-template" data-id="+id" data-type=""><i class="fa fa-plus"></i> <?php echo Yii::t("common", "Add") ?></a>
                                    '<a href="javascript:;" class="btn btn-success share-this-template" style="display:none;" data-id="" data-type=""><i class="fa fa-unlock"></i> <?php echo Yii::t("common", "Open") ?></a>
                    '<a href="javascript:;" class="btn btn-primary edit-template" data-id="" data-type=""><i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit") ?></a>
                    '<a href="javascript:;" class="btn btn-danger delete-template" data-id="" data-type=""><i class="fa fa-trash"></i> <?php echo Yii::t("common", "Delete") ?></a>
                	} */
	  				$(this).attr("data-id", id);
	  				$(this).attr("data-type", type);
	  				$(this).attr("data-source", source);
	  				if($(this).hasClass("share-this-template"))
	  					$(this).hide();
	  			});
	  			$("#modal-preview-template").addClass("open").modal("show").attr("data-type",type);
  				ajaxPost(
					null,
					baseUrl + "/co2/template/get/id/" + id,
					null,
					function(data) {
						costumizer.template.dataTpl=data;
						costumizer.template.dataTpl.id=id;
						if(typeof data.blocTplId != "undefined")
							$("#modal-preview-template .use-this-template").data("id", data.blocTplId);
						if(typeof data.shared == "undefined" || !data.shared)
							$("#modal-preview-template .share-this-template").fadeIn(200);

						costumizer.template.views.preview(data);
   						var swiper = new Swiper(".swiperElement", {
					      effect: "coverflow",
					      grabCursor: true,
					      centeredSlides: true,
					      slidesPerView: "auto",
					      autoplay: {
					      	delay : 3000
					      },
					    //  translate3d(29px, 0px, 0px) rotateX(0deg) rotateY(-54.0012deg) scale(1.3)
					      coverflowEffect: {
					        rotate: 70,
					        stretch: 0,
					        depth: 100,
					        scale: 1.4, 
					        modifier: 1,
					        slideShadows: true,
					      },
					      pagination: {
					        el: ".swiper-template-pagination",
					        clickable : true
					      },
					    });
						costumizer.template.events.bind();
						//costumizer.template.views.useTplsView();
					},
					null,
					"json"
				);
	  		},
			delete: function(idTpl,collection, del) {
				var params = {
					"type": "template",
					"collection": collection
				}
				var urlToSend = baseUrl + "/co2/cms/deletetemplate/id/" + idTpl;
				if (del == "delete") {
					ajaxPost(
						null,
						urlToSend,
						params,
						function(data) {
							if (data.result){
								if($("#modal-preview-template").is(":visible")){
									$("#modal-preview-template").removeClass("open");	
								}
								if($("#modal-template-results  #entity_templates_"+idTpl).is(":visible"))
									$("#modal-template-results  #entity_templates_"+idTpl).fadeOut(500);
							} else {
								toastr.error(data.msg);
							}
						},
						null,
						"json"
					);
				}  
				if (del == undefined) {
					bootbox.confirm({message: trad.areyousuretodelete,
						buttons: {
							confirm: {
								label: trad.yes,
								className: 'btn-success'
							},
							cancel: {
								label: trad.no,
								className: 'btn-danger'
							}
						}, 
					callback: function(response) {
						if (response) {
							ajaxPost(
								null,
								baseUrl + "/co2/cms/deletetemplate/id/" + idTpl,
								{
									"type": "template",
									"collection": "templates"
								},
								function(data) {
									if (data.result){
										toastr.success(data.msg);
										if($("#modal-preview-template").is(":visible")){
											$("#modal-preview-template").removeClass("open");	
										}
										if($("#modal-template-results  #entity_templates_"+idTpl).is(":visible"))
											$("#modal-template-results  #entity_templates_"+idTpl).fadeOut(500);
										//urlCtrl.loadByHash( location.hash );
									} else {
										toastr.error(data.msg);
									}
								},
								null,
								"json"
							);
						}
					}});
				}
			},	
			use : function (tplId, tplCollection, action, callback) {
				var contextPage;
				if (typeof tplId === "object") {
					if (typeof tplId.contextPage === "string")
						contextPage = tplId.contextPage;
					if (typeof tplId.context_page === "string")
						contextPage = tplId.contextPage;
				}
				if (typeof contextPage === "undefined")
					contextPage = (typeof costumizer.template.targetPage != "undefined") ? costumizer.template.targetPage : pageMenu
				var params = {
					"id"		 : typeof tplId === "string" ? tplId : tplId.id,
					"action"	 : typeof tplId === "object" && typeof tplId.action !== "undefined" ? tplId.action : action,
					"collection" : typeof tplId === "object" && typeof tplId.collection !== "undefined" ? tplId.collection : tplCollection,
					"parentId"   : typeof tplId === "object" && typeof tplId.context_id !== "undefined" ? tplId.context_id : costum.contextId,
					"page" 		 : contextPage,
					"parentSlug" : typeof tplId === "object" && typeof tplId.context_slug !== "undefined" ? tplId.context_slug : costum.contextSlug,
					"parentType" : typeof tplId === "object" && typeof tplId.context_type !== "undefined" ? tplId.context_type : costum.contextType
				};
				ajaxPost(
					null,
					baseUrl + "/co2/template/use",
					params,
					function (data) {
						costumizer.template.actions.closeModal();
						//callback();
						if(data.type == "page"){
							urlCtrl.loadByHash(contextPage);
							costum.app["#"+page].templateOrigin = tplId;
							$(".close-dash-costumizer").click()
						}else{
							window.location.reload()
						}
					},
					{ async: false }
				);
			},
			validate : function(params={}){ 

				if (params.type == "costum") {
					bootbox.dialog({
							message: `${costum.members["lists"][params.userId].name} a change le theme du COSTUM entier. Veuillez confirmer.`,
							title: costum.members["lists"][params.userId].avatarWithName,
							onEscape: function() {},
							show: true,
							backdrop: true,
							closeButton: true,
							animate: true,
							className: "my-modal",
							buttons: { 
								continue: {
									label: "Je confirm",
									className: "btn-success",
									callback: function() {
										costumizer.tplChange({functionName: "response", data: {
											tplId : params.tplId,
											type : "approval",
											userAdressId : params.userId,
											userId: userId
										}})
									}
								},
								cancel: {
									label: "Refuser",
									className: 'btn-default',
									callback : function(){		
										costumizer.tplChange({functionName: "response", data: {
											type : "rejection",
											userAdressId : params.userId,
											userId: userId
										}})
									}
								}
							}

						});
				}else{
					var pageChanged = typeof params.page != "undefined" ? params.page.replace("#", "") : "";
					if (pageChanged == page){
						bootbox.dialog({
							message: `${costum.members["lists"][params.userId].name} a appliqué un Template sur cette page. Veuillez actualiser la page.`,
							title: costum.members["lists"][params.userId].avatarWithName,
							onEscape: function() {},
							show: true,
							backdrop: true,
							closeButton: true,
							animate: true,
							className: "my-modal",
							buttons: { 
								continue: {
									label: "OK",
									className: "btn-success",
									callback: function() {
										urlCtrl.loadByHash("#"+page)
									}
								},
								cancel: {
									label: "Plus tard",
									className: 'btn-default'
								}
							}

						});
					}else{
						var templateInfo = `${costum.members["lists"][params.userId].avatar} ${costum.members["lists"][params.userId].name} a utilisé un Template sur la page ${pageChanged} <br> <span class="toast-show-more">Cliquez pour voir!</span>`;

						toastr.options = {
							"closeButton": true,
							"progressBar": true,
							"positionClass": "toast-bottom-right",
							"timeOut": 30000
						}

						toastr.info(templateInfo, '', {
							onShown: function() {
								$('.toast').on('click', function() {
									urlCtrl.loadByHash("#"+pageChanged)
								});
							}
						});
					}
				}
			},
			save : function(type, idBlocToDuplicate=null , newId){
				if(notNull(idBlocToDuplicate)){
					costumizer.template.form.jsonSchema.properties.blocTplId={inputType: "hidden", value : newId};
					costumizer.template.form.idBlocToDuplicate =  idBlocToDuplicate;
				}
		        costumizer.template.form.jsonSchema.properties.type.value=type;
		        costumizer.template.form.jsonSchema.properties.category.options=costumizer.template.category[type];	
		        // Now, the Template page & costum has the same category
		        if (type != "blockCms") {					
		        	costumizer.template.form.jsonSchema.properties.category.options=costumizer.template.category.costum;	
		        }	        
		        costumizer.getCategoryUsedByTemplate();
		        costumizer.template.form.jsonSchema.title  = tradCms.registerTpl+" "+type;
		        if (type == "costum") {
		        	var paramsTpl = {allPage : true};

		        	ajaxPost(
		        		null,
		        		baseUrl+"/"+moduleId+"/cms/getallcms",
		        		params,
		        		function(data){
		        			costumizer.template["costumCms"] = data.allCms;
		        			dyFObj.setMongoId('templates', function(){			
		        				costumizer.template["canEditParams"] = true    		
		        				uploadObj.set('templates',dyFObj.currentElement.id);
		        				dyFObj.openForm(costumizer.template.form, null, null, null);
		        			});                
		        		},
		        		null,
		        		null,
		        		{
		        			async: false
		        		}
		        		);
		        }else{
		        	dyFObj.setMongoId('templates', function(){			
		        		uploadObj.set('templates',	);
		        		costumizer.template["canEditParams"] = true
		        		dyFObj.openForm(costumizer.template.form, null, null, null);
		        	});
		        }

		        
			},
			update : function(id, type){
				dyFObj.editMode=true;
				uploadObj.update = true; 
				uploadObj.type = "templates";
				uploadObj.id = id;
				dyFObj.currentElement={type : "templates", id : id};
				costumizer.template.form.jsonSchema.properties.category.options=costumizer.template.category[type];
				costumizer.template.form.jsonSchema.properties.tplAlbum.initList = "";
                costumizer.template.form.jsonSchema.title  = tradCms.registerTpl+" "+type;
				if(type=="blockCms")
					costumizer.template.form.jsonSchema.properties.blocTplId={inputType: "hidden"};
		        
				if(notEmpty(costumizer.template.dataTpl) && id==costumizer.template.dataTpl){
					if(notEmpty(costumizer.template.dataTpl.tplAlbum))
						costumizer.template.form.jsonSchema.properties.tplAlbum.initList=costumizer.template.dataTpl.tplAlbum;
					dyFObj.openForm(costumizer.template.form, null, costumizer.template.dataTpl, null);
				}else{
					
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/template/get/id/"+id,
						{},
						function (data) {  
							data.id=id;
							costumizer.getCategoryUsedByTemplate();
							if(notEmpty(data.tplAlbum))
								costumizer.template.form.jsonSchema.properties.tplAlbum.initList=data.tplAlbum;
							if (costum.slug == data.source.key) {								
									costumizer.template["canEditParams"] = true
									if (type == "costum") {
										var paramsTpl = {allPage : true};
										ajaxPost(
											null,
											baseUrl+"/"+moduleId+"/cms/getallcms",
											params,
											function(dataCms){
												costumizer.template["costumCms"] = dataCms.allCms;   
												dyFObj.openForm(costumizer.template.form, null, data, null);            
											},
											null,
											null,
											{
												async: false
											}
											);

									}else{
										bootbox.confirm({
											title: "<span class='text-center bold'>Pour commencer!</span>",
											message: `<span class='text-center bold'>
											Souhaitez-vous poursuivre à la remplacement de la liste des CMS de ce Modele par les CMS de la page <span class='text-red'>"${page}"</span>?</span><br>
											`,
											buttons: {
												confirm: {
													label: trad.yes,
													className: 'btn-info'
												},
												cancel: {
													label: trad.no,
													className: 'btn-default'
												}
											},
											callback: function (result) {
												if(!result)
													costumizer.template["canEditParams"] = false
												
												dyFObj.openForm(costumizer.template.form, null, data, null);
											}
										});
									}
							}else{
								dyFObj.openForm(costumizer.template.form, null, data, null);
							}
							
						},
						null,
						"json"											
					);
				}
			}
			//bindEvents : function(){
	  			/*$(".bodySearchTemplate").on("click", ".use-this-template", function(e){
	  				var $this 	  = $(this)
	  				const appKeys = costum.app;

	  				if (Object.keys(cmsConstructor.sp_params).length > 1 || appKeys.length > 1) {
	  					bootbox.dialog({
	  						message: `<p>${tradCms["It appears that you have recently modified your site"]}</p>
	  						<div class="alert alert-warning" role="alert">
	  						<p><b><i class="fa fa-exclamation-triangle">
	  						</i> NB</b>: ${tradCms["By choosing a Template, you risk losing all of your configuration"]}.</p>
	  						</div>`,
	  						title: "",
	  						onEscape: function() {},
	  						show: true,
	  						backdrop: true,
	  						closeButton: true,
	  						animate: true,
	  						className: "my-modal",
	  						buttons: { 
	  							continue: {
	  								label: tradCms["OK, I choose this template anyway"],
	  								className: "btn-success",
	  								callback: function() {
	  									$this.addClass('disabled');
	  									$this.html('<span style=""><i class="fa fa-spin fa-circle-o-notch"></i> <span>'+trad.processing+' ...</span></span>');
	  									costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))

	  								}
	  							},
	  							cancel: {
	  								label: trad.cancel,
	  								className: 'btn-default'
	  							}
	  						}

	  					}
	  					);
	  				}else{
	  					$this.addClass('disabled');
	  					$this.html('<span style=""><i class="fa fa-spin fa-circle-o-notch"></i> <span>'+trad.processing+' ...</span></span>');
	  					costumizer.template.actions.use($this.data("id"), $this.data("collection"),$this.data("action"))
	  				}
	  				e.stopImmediatePropagation();
	  			})*/
	  			/*$(".bodySearchTemplate").on("click", ".share-this-template", function(e){
	  				let $this = $(this);
	  				if ($this.hasClass("action-hide")) {
	  					shareTpl = {
	  						id : $this.data("id"),
	  						collection : $this.data("collection"),
	  						path : "shared",
	  						value : true
	  					}
	  					dataHelper.path2Value( shareTpl, function(params) {
	  						$("#cmsBuilder-right-actions .lbh-costumizer[data-link='template']").click()
	  						toastr.success("Shared!");
	  					})
	  				}else{
	  					hideTpl = {
	  						id : $this.data("id"),
	  						collection: $this.data("collection"),
	  						path : "shared"
	  					}
	  					dataHelper.unsetPath( hideTpl, function(params) {
	  						$("#cmsBuilder-right-actions .lbh-costumizer").click()
	  						toastr.success("hidden!");        
	  					} );
	  				}
	  				e.stopImmediatePropagation();
	  			})*//*$(".bodySearchTemplate").on("click", ".mc-expans", function(e){
	  				e.stopImmediatePropagation();
	  				var $this_mc_btn_action = $(this)
	  				var card = $this_mc_btn_action.parents('.material-card');
	  				var icon = card.find('.mc-btn-action i');
	  				icon.addClass('fa-spin-fast');

	  				if (icon.hasClass("fa-arrow-left")) {
	  					card.removeClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").hide()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-arrow-left ripple')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-bars');

	  					}, 800);
	  				} else {
	  					card.addClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").show()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-bars')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-arrow-left ripple');

	  					}, 800);
	  				}
	  			})*/
	  			/*$(".bodySearchTemplate").on("click", ".preview_screenshot", function(e){
	  				e.stopImmediatePropagation();
	  				var tptScreenshot = $(this).data("screenshot")
	  				var tplViewScreen = ""
	  				$.each( tptScreenshot , function(k,val) { 
	  					tplViewScreen += `<img src="${val}">`
	  				});
	  				var dialog = bootbox.dialog({
	  					message: tplViewScreen,
	  					size: 'large',
	  					buttons: {
	  						ok: {
	  							label: trad.Close,
	  							callback: function() {
	  							}
	  						}
	  			/*$(".bodySearchTemplate").on("click", ".mc-expans", function(e){
	  				e.stopImmediatePropagation();
	  				var $this_mc_btn_action = $(this)
	  				var card = $this_mc_btn_action.parents('.material-card');
	  				var icon = card.find('.mc-btn-action i');
	  				icon.addClass('fa-spin-fast');

	  				if (icon.hasClass("fa-arrow-left")) {
	  					card.removeClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").hide()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-arrow-left ripple')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-bars');

	  					}, 800);
	  				} else {
	  					card.addClass('mc-active');
	  					$this_mc_btn_action.parent().find(".tpl-stat").show()
	  					window.setTimeout(function() {
	  						icon
	  						.removeClass('fa-bars')
	  						.removeClass('fa-spin-fast')
	  						.addClass('fa-arrow-left ripple');

	  					}, 800);
	  				}
	  			})*/
	  			/*$(".bodySearchTemplate").on("click", ".preview_screenshot", function(e){
	  				e.stopImmediatePropagation();
	  				var tptScreenshot = $(this).data("screenshot")
	  				var tplViewScreen = ""
	  				$.each( tptScreenshot , function(k,val) { 
	  					tplViewScreen += `<img src="${val}">`
	  				});
	  				var dialog = bootbox.dialog({
	  					message: tplViewScreen,
	  					size: 'large',
	  					buttons: {
	  						ok: {
	  							label: trad.Close,
	  							callback: function() {
	  							}
	  						}
	  					}
	  				});
	  			})

	  			$(".bodySearchTemplate").on("click", ".delete-tpl", function(e) {
	  				directory.deleteElement($(this).data("collection"),$(this).data("id"),$(this));
	  				e.stopImmediatePropagation();
	  			})

	  			$(".bodySearchTemplate").on("click", ".edit-tpl", function(e) {	  				
	  				e.stopImmediatePropagation();
	  				costumizer.template.actions.update( $(this).data("id") ,$(this).data("subtype"))
	  			});*/
	  		//},
			/*templateListHtml: function (params) {
				mylog.log("templateListHtml", "Params", params);
				if (typeof params.collection == "undefined")
					params.collection = "cms";
				var str = '';
				let state = "";
				if (typeof costum.tplUsed != "undefined") {
					if (typeof costum.tplUsed[params.id] != "undefined") {
						state = costum.tplUsed[params.id]
					}
				}
				str += '<div id="entity_' + params.collection + '_' + params.id + '" class="tpls col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer ' + params.containerClass + ' ">' +
					'<div class="'+state+' item-slide">' +
					'<div class="img-back-card">' 
					if ( typeof params.screenshot != 'undefined' && params.screenshot != '') {       
						str += '<div class="div-img"><img class="img-responsive" src="'+baseUrl + '/' + params.screenshot[0]+'"></div>'+
						 '<a href="'+baseUrl + '/' + params.screenshot[0]+'" class="icon-preview-tpl thumb-info text-center" data-lightbox="' +params.name+params.id+'" data-title="Aperçu de ' + params.name + '">' +
                                		'<i class="fa fa-search-plus"></i>'+
                                '</a>'
					}else{                                     
						str += params.imageProfilHtml
					}
					str += '<div class="text-wrap searchEntity">' +
					'<h4 class="entityName">' +
					'<a href="javascript:;" class="use-this custom-block-cms" data-path="' + params.path + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' + params.name + '</a>' +
					'</h4>' +
					'<div class="small-infos-under-title text-center">';
					if (typeof params.type != 'undefined') {
						str += '<div class="text-center entityType">'
						if (typeof params.userCounter != "undefined") {
							str += '<span class="badge bg-azure">'+params.userCounter+'</span><span> users</span>'
						}else{
							str += '<span class="text-white">' + ((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type) + '</span>'
						}
						if (typeof params.shared == "undefined") {			
							str += '<div class="pull-right padding-right-10">'
							str += '<i class="fa fa-lock"></i>'
							str += '</div>'
						}
						str += '</div>';
					}
				if (notEmpty(params.statusLinkHtml))
					str += params.statusLinkHtml;
				if (notEmpty(params.rolesHtml))
					str += '<div class=\'rolesContainer elipsis\'>' + params.rolesHtml + '</div>';
				if (notNull(params.localityHtml)) {
					str += '<div class="entityLocality no-padding">' +
						'<span>' + params.localityHtml + '</span>' +
						'</div>';
				}

				str += '</div>' +
					'<div class="entityDescription"></div>' +
					'</div>' +
					'</div>' +
					//hover
					'<div class="slide-hover co-scroll">' +
					'<div class="text-wrap">' +
					'<h4 class="entityName">' +
					'<a href="javascript:;" class="use-this custom-block-cms" data-path="' + params.path + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' + params.name + '</a>' +
					'</h4>' +
					'<hr>' +
					'<div class="socialEntityBtnActions btn-link-content">' 
					if (state == "using") {
						str += '<p class="capitalize">'+tradCms.currenttemplate+'</p>'
						str += '<a href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
						'<font style="vertical-align: inherit;" class="capitalize">' +
						'<i class="fa fa-refresh"></i> '+trad.reset+' </font>' +
						'</a>';
					}else if(state == "used"){
						str += '<p class="capitalize">Template '+tradCms.used+'</p>'
						str += '<a href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
						'<font style="vertical-align: inherit;" class="capitalize">' +
						'<i class="fa fa-history"></i> '+tradCms.useagain+' </font>' +
						'</a>';
					}else{
						str +='<a href="javascript:;" class="btn btn-info btn-link custom-block-cms use-this-template" data-action="'+state+'" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
						'<font style="vertical-align: inherit;">' +
						'<i class="fa fa-plus"></i> ' + trad.choose + '</font>' +
						'</a>';
					}
					if (isSuperAdminCms) {
						if (typeof params.shared == "undefined") {
							str +='<a href="javascript:;" class="btn btn-info btn-link custom-block-cms share-this-template action-hide" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
							'<font class="capitalize" style="vertical-align: inherit;">' +
							'<i class="fa fa-share"></i> ' + tradCms.shareforpublic + '</font>' +
							'</a>';
						}else{
							str +='<a href="javascript:;" class="btn btn-info btn-link custom-block-cms share-this-template action-share" data-id="' + params.id + '" data-name="' + params.name + '" data-collection="' + params.collection + '">' +
							'<font class="capitalize" style="vertical-align: inherit;">' +
							'<i class="fa fa-lock"></i> ' + trad.hide + '</font>' +
							'</a>';
						}
					}
				if ('undefined' != typeof params.profilImageUrl && params.profilImageUrl != '') {
					str += '<a href="' + baseUrl + '/' + params.profilImageUrl + '" class="btn btn-info btn-link thumb-info" data-title="Aperçu de ' + params.name + '" data-lightbox="all">' +
						'<font style="vertical-align: inherit;"><i class="fa fa-eye"></i>' + trad.Preview + '</font>' +
						'</a>';
				}
				if (Object.keys(params["parent"]).includes(costum.contextId) && (typeof userConnected.roles != "undefined" && typeof userConnected.roles.superAdmin != "undefined" && userConnected.roles.superAdmin == true)) {
					str += `<a href='javascript:;' class='btn btn-info btn-link' onclick='costumizer.template.actions.update("${params.id}","${params.collection}");'>` +
						'<font style="vertical-align: inherit;"><i class="fa fa-pencil-square-o"> </i> ' + trad.update + '</font>' +
						'</a>';

					str += "<a href='javascript:;' class='btn btn-info btn-link' onclick='costumizer.template.actions.delete(\"" + params.id + "\",\"" + params.collection + "\");'>";
					str += '<font style="vertical-align: inherit;"><i class="fa fa-pencil-square-o"> </i> ' + trad.delete + '</font>' +
						'</a>';
				}
				str += '</div>';
				str += '<p class="p-short">' + params.descriptionStr + '</p>';
				if (typeof params.tagsHtml != "undefined")
					str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';
				if (typeof params.path != "undefined") {
					str += '<ul class="tag-list">';
					var eachPath = params.path.split(".");
					$.each(eachPath, function (e, v) {
						str += '<span class="badge bg-red btn-tag">+ ' + v + '</span>';
					});
					str += '</ul>';
				}
				str += '</div>' +
					'</div>' +
					'</div>' +
					'</div>';


				return str;
			}*/
			/*templateList: function () {			
				var costumizer.template.paramsTemplateList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterContainer",
					interface : {
						events : {
							scroll : true,
							scrollOne : true
						}
					},
					header: {
						dom: ".headerSearchTemplate",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					defaults: {
						notSourceKey: true,
						types: ["templates"],
						filters: {
							shared: true
						},
						fields : ["userCounter","shared", "source","screenshot","cmsList"]

					},
					results: {
						dom: ".bodySearchTemplate",
						smartGrid: true,
						renderView: "costumizer.template.views.templateListHtml",
						map: {
							active: false
						}
					},
					filters: {
						text: true,

						categorie: {
							view: "dropdownList",
							type: "filters",
							name: tradCms.choooseCategory,
							field: "category",
							action: "filters",
							event: "filters",
							keyValue: false,
							list: costumizer.template.category,
						},
					}
				}

				if (isSuperAdminCms){
					delete costumizer.template.paramsTemplateList.defaults.filters.shared;

					costumizer.template.paramsTemplateList.filters["typeSubtype"] = {
						view: "dropdownList",
						type: "filters",
						action: "filters",
						name: tradCms.content,
						typeList : "object",
						event : "exists",
						keyValue: false,
						list: {
							restricted : {
								label: "Unrestricted",
								field : "shared",
								value : true
							},
							shared : {
								label: "Restricted",
								field : "shared",
								value : false
							}
						}
					}
				}

				typeObj.cms.createLabel = " " + tradCms.addSite + "";
				typeObj.cms.color = "green";
				typeObj.template.color = "green";
				var filterGroupTemplate = searchObj.init(costumizer.template.paramsTemplateList);
				filterGroupTemplate.search.init(filterGroupTemplate);
				costumizer.template.actions.bindEvents()
			},*/
			/*update: function(id, collection, subType=null) {
				var tplId = id;
				var tplCollection = collection;
				var tplCtx = {};
				tplCtx.id = tplId;
				var screenShot = []
				var tplDocument = costumizer.template.views.initDocument(tplCtx)	
                var editTplActiveForm = {
                    "jsonSchema" : {
                        "title" : tradCms.registerTpl,
                        "type" : "object",
                        "properties" : {
                            name : {
                                label: tradCms.nameTpl,
                                inputType : "text",
                                rules:{
                                    "required":true
                                }
                            },  
                            category : {
                                label : trad.category,
                                inputType : "select",
                                rules:{
                                    "required":true
                                },
                                options : costumizer.template.category,
                                placeholder : tradCms.selectAnOption,
								value: "" 
                            },                            
                            "image": {
                            	"inputType": "uploader",
                            	"docType": "image",
                            	"contentKey": "templates",
								"endPoint" : "/subKey/tplalbum",
								"domElement" : "tplalbum",		
								"filetype" : ["jpeg", "jpg", "gif", "png"], 
                            	"label": "Image :",
                            	"itemLimit": 4
                            },
                            description : {
                                label: tradDynForm.description,
                                inputType : "text",
                                value : ""
                            },
                            screenshot : {
                            	"label": "Screenshot",
                            	"placeholder": "",
                            	"inputType": "custom",
                            	"html" : tplDocument.html
                            }
                        },
                        beforeBuild : function(){	
                        	uploadObj.set(collection,tplId);
                        	uploadObj.initUploader();
                        },
                        save : function () {                       	  
                            tplCtx.value = {};
                            params = {};
                            $.each( editTplActiveForm.jsonSchema.properties , function(k,val) { 
                                tplCtx.value[k] = $("#"+k).val();
                            });
                            tplCtx.value.parent={};
                            // tplCtx.value["screenshot"] = screenShot;
                            tplCtx.value.parent[costum.contextId] = {
                                type : costum.contextType, 
                            };
                            tplCtx.collection = collection
                            tplCtx.value.collection = collection
                            if (collection == "templates"){    
                                params["allPage"] = true;
                                tplCtx.value.siteParams = {}
                                tplCtx.value.siteParams.css = costumizer.obj.css;
                                tplCtx.value.siteParams.app = costumizer.obj.app
                                tplCtx.value.siteParams.htmlConstruct = costumizer.obj.htmlConstruct;
                                if (typeof costumizer.obj.lists != "undefined") {
                                	tplCtx.value.siteParams.lists = costumizer.obj.lists;
                                }
                            }else{  
                            	if (subType == "page") {
                            		tplCtx.value.cmsList = cmsConstructor.cmsList;
                            		delete tplCtx.value.subtype;
                            	}
                            }
								ajaxPost(
									null,
									baseUrl+"/"+moduleId+"/cms/updatetemplate",
									tplCtx,
									function(data){
										dyFObj.commonAfterSave(params,function(){
											ajaxPost(
												null,
												baseUrl+"/"+moduleId+"/cms/getlistfile",
												{costumType : collection, costumId : tplId, doctype : "image","subKey": "tplalbum"},
												function (screenData) {  
													var completedItems = 0;

													$.each(screenData.result, function(k,val) { 
														screenShot[k] = val.imagePath
														completedItems++
														if (completedItems === screenData.result.length) {
															var upScreenShot = {}
															upScreenShot.id = tplId
															upScreenShot.collection = tplCollection
															upScreenShot.path = "screenshot"
															upScreenShot.value = [...new Set(screenShot)]

															dataHelper.path2Value(upScreenShot, function (params) { toastr.success(tradCms.editionSucces); });
														}
													});
												},
												null,
												"json",
												{
													async: false
												}												
												);
											$("#ajax-modal").modal('hide');           
										});
									},
									null,
									"json",
									{
										async: false
									}

								);  
                        },
                    }
                };
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/gettemplate",
					{templateType : collection, templateId : tplId},
					function (data) {    
							// editTplActiveForm.jsonSchema.properties.subtype.value = (data.template.subtype) ? data.template.subtype : "page";
							editTplActiveForm.jsonSchema.properties.category.value = new DOMParser().parseFromString(data.template.category, "text/html").documentElement.textContent;
							// if (!notEmpty(data.map.images)) {
							// 	data.map["images"] = [0,1,2,3]
							// }
								
							// screenShot = data.map.screenshot;
							// screenShot = data.template.screenshot
							
							$.each( data.template.images , function(k,val) { 
								screenShot[k] = val["imagePath"]
							});
							
							// mylog.log("templated",data)
							dyFObj.openForm(editTplActiveForm, null,data.template);
							$(".close-dash-costumizer").click()
						}      
				);          
			},
			save : function(key, id){

			},*/
			/*saveThis : function(type=null){
				console.log(cmsConstructor.cmsList, "fuckdashit");
				if (cmsConstructor.cmsList == "") {
					bootbox.alert(tradCms.emptyCmsTpl);
				}else{
					var tplCtx 	   = {};
					var screenShot = [];
					var editTplActiveForm = {
						"jsonSchema" : {
							"title" : tradCms.registerTpl,
							"type" : "object",
							"properties" : {
								name : {
									label: tradCms.nameTpl,
									inputType : "text",
									rules:{
										"required":true
									}
								},
								category : {
									label : trad.category,
									inputType : "select",
									rules:{
										"required":true
									},
									options : costumizer.template.category,
									placeholder : tradCms.selectAnOption,
									value: "" 
								},                            
								"image": {
									"inputType": "uploader",
									"docType": "image",
									"contentKey": "templates",
									"filetype" : ["jpeg", "jpg", "gif", "png"], 
									"label": "Image à la une:",
									"itemLimit": 1/*,
									onComplete: function(data){
										screenShot.push(data.docPath)
									}
								},
								description : {
									label: tradDynForm.description,
									inputType : "text",
									value : ""
								}
							},
							beforeBuild : function(){	
								/*if (type == "page") {
									dyFObj.setMongoId('cms',function(data){
										tplCtx.id=dyFObj.currentElement.id;
										tplCtx.collection=dyFObj.currentElement.type;
									});
									uploadObj.set("cms",tplCtx.id);
									uploadObj.initUploader();
								}else{									
									dyFObj.setMongoId('templates',function(data){
										tplCtx.id=dyFObj.currentElement.id;
										tplCtx.collection=dyFObj.currentElement.type;
									});
									uploadObj.set("templates",tplCtx.id);
									uploadObj.initUploader();
								//}
							},
							save : function () {
								tplCtx.value = {};
								params = {};
								$.each( editTplActiveForm.jsonSchema.properties , function(k,val) { 
									tplCtx.value[k] = $("#"+k).val();
								});
								tplCtx.value.parent={};
								tplCtx.value["screenshot"] = screenShot;
								tplCtx.value.parent[costum.contextId] = {
									type : costum.contextType 
								};
								tplCtx.value.cmsList ={};
								/*if (type != "page"){   
									params["allPage"] = true;
									tplCtx.value.siteParams = {}
									tplCtx.value.siteParams.css = costumizer.obj.css;
									tplCtx.value.siteParams.app = costumizer.obj.app
									tplCtx.value.siteParams.htmlConstruct = costumizer.obj.htmlConstruct;
									if (typeof costumizer.obj.lists != "undefined") {
										tplCtx.value.siteParams.lists = costumizer.obj.lists;
									}
									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/getallcms",
										params,
										function(data){    
											tplCtx.value.cmsList = data.allCms;                
										},
										null,
										null,
										{
											async: false
										}
										);

								}else{  
									params["page"] = page
									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/getallcms",
										params,
										function(data){ 
											mylog.log("allcsm",data.allCms);    
											tplCtx.value.cmsList = data.allCms;                
										},
										null,
										null,
										{
											async: false
										}
										);
									tplCtx.value.type = "blockCms";
									delete tplCtx.value.subtype;
								}
								dataHelper.path2Value( tplCtx, function(params) {
									dyFObj.commonAfterSave(params,function(){
										toastr.success(tradCms.elementwelladded);
										if (notEmpty(screenShot)) {
											var upScreenShot = {}
											upScreenShot.id = tplCtx.id
											upScreenShot.collection = tplCtx.collection
											upScreenShot.path = "screenshot"
											upScreenShot.value = screenShot;
											dataHelper.path2Value(upScreenShot, function (params) { 
												toastr.success(tradCms.editionSucces); 
											});
										}
										$("#ajax-modal").modal('hide');           
									}); 
								});  
							}
						}
					};
					dyFObj.openForm(editTplActiveForm, null,null);
					$(".close-dash-costumizer").click();
				}
			},*/
			/*cmsModelList: function () {
				var params = {
					"action": "getType",
					"type": "blockCms"
				}
				var costumizer.template.paramsTemplateList = {
					urlData: baseUrl + "/co2/search/globalautocomplete",
					container: "#filterContainer",
					
					loadEvent : {
						default : "scroll"
					},				
					header: {
						dom: ".headerSearchTemplate",
						options: {
							left: {
								classes: 'col-xs-8 elipsis no-padding',
								group: {
									count: true,
									types: true
								}
							}
						},
					},
					defaults: {
						notSourceKey: true,
						types: ["cms"],
						filters: {
							type: "blockCms",
							shared: true
						},
						fields : ["userCounter","shared", "source","screenshot","cmsList"]

					},
					results: {
						dom: ".bodySearchTemplate",
						smartGrid: true,
						renderView: "costumizer.template.views.templateListHtml",
						map: {
							active: false
						}
					},
					filters: {
						text: true
					}
				}

				if (typeof costumizer.cms.list != "undefined") {
					costumizer.template.paramsTemplateList.defaults.filters["cmsList"] = {$exists : true}
				}

				if (isSuperAdminCms){
					delete costumizer.template.paramsTemplateList.defaults.filters.shared;
					costumizer.template.paramsTemplateList.filters["typeSubtype"] = {
						view: "dropdownList",
						type: "filters",
						action: "filters",
						name: tradCms.content,
						typeList : "object",
						event : "exists",
						keyValue: false,
						list: {
							restricted : {
								label: "Unrestricted",
								field : "shared",
								value : true
							},
							shared : {
								label: "Restricted",
								field : "shared",
								value : false
							}
						}
					}
				}

				typeObj.cms.createLabel = " " + tradCms.addSite + "";
				typeObj.cms.color = "green";
				typeObj.template.color = "green";
				var filterGroupTemplate = searchObj.init(costumizer.template.paramsTemplateList);
				filterGroupTemplate.search.init(filterGroupTemplate);
				costumizer.cms.actions.bindEvents()
				// cmsBuilder.bindEvents();
			}*/
		},
		helpers : {
			saveTplContent : function(tplId,page = ""){
				var paramsTpl = {
					page : page, 
					contextId: costum.contextId,
					contextType : costum.contextType,
					tplId : tplId
				};
				if (page == "") {
					delete paramsTpl.page
				}
				ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/cms/savetplcontent",
					{
						paramsTpl
					},
					function(data){mylog.log("Templates content added")},
					null

					);
			}
		}

	},
	versionning : {
		events : {
			init : function() {
				$(".restore-version").off().on("click",function () {
					var $this = $(this)
					var versionData = {
						versionId   : $this.data("id"),
						contextSlug : costum.contextSlug,
						versionTime : $this.data("timestamp")
					}
					bootbox.hideAll();

					var saveDialog = `<strong><strong class="text-red">ATTENTION!!</strong> <br> La restoration nécessite une suppression de toutes les données du COSTUM actuel.<br> 
					Veuillez enregistrer si vous pensez qu'un jour vous le réutiliserez.</strong>`
					bootbox.dialog({
						message: saveDialog,
						title: trad.areyousure,
						buttons: [{
								label: trad.save,
								className: "btn-success",
								callback: function() {
									$(".save-costum-version").click()
								}
							},
							{
								label: "Restorer",
								className: "btn-danger",
								callback: function() {
									$this.find(".fa-undo").addClass("fa-spin fa-circle-o-notch")
									$this.find(".fa-undo").removeClass("fa-undo")
									$this.addClass("disabled")
									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/restoreversion",
										{
											versionData
										},
										function(data){
											window.location.reload()
										},
										null

										);
								}
							}

							]
					});
				})
				$(".delete-version").off().on("click",function () {
					var $this = $(this)
					bootbox.hideAll();

					var saveDialog = `<strong class="text-red">${tradCms.warning}</strong> <br> ${tradCms.deletitionirreversible}!`
					bootbox.dialog({
						message: saveDialog,
						title: trad.areyousure,
						buttons: {
							main: {
								label: trad.delete,
								className: "btn-danger",
								callback: function() {
									$this.find(".fa-trash").addClass("fa-spin fa-circle-o-notch")
									$this.find(".fa-trash").removeClass("fa-trash")
									$this.addClass("disabled")
									ajaxPost(
										null,
										baseUrl+"/"+moduleId+"/cms/deletesavedcostum",
										{
											version : $this.data("timestamp")
										},
										function(data){
											costumizer.versionning.helpers.getVersion()
											toastr.success(tradCms.elementDeleted);
										},
										null

										);
								}
							}
						}
					});
				})
			}
		},
		// Gestion de versionnage d'un costum
		helpers : {
			getVersion : function(){
				return new Promise (
					function(resolve,reject) {
						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/cms/getversion",
							{
								versionName : "Version"
							},
							function(data){
								var $version ='<ul class="timeline version-costum">' ;
								// Sort descending order via version date
								const sortedData = Object.keys(data.items)
								.map(key => ({ key, version: data.items[key].version }))
								.sort((a, b) => b.version - a.version)
								.reduce((acc, item) => ({ ...acc, [item.key]: data.items[item.key] }), {});

								if (notEmpty(sortedData)) {
									$.each( sortedData , function(k,val) { 
										$version += `
										<li>
											<div class="detail-version">${val.versionName}</div> 
											<div  class="detail-version float-right">${directory.showDatetimePost(null, null, val.version)}</div>
											<p>${val.versionDesciption ? val.versionDesciption : ""}</p>
											<div class="col-xs-12 no-padding text-center margin-top-10 margin-bottom-15">
												<a class='btn costumizer-button-add restore-version' data-id='${k}' data-timestamp='${val.version}'><i class="fa fa-undo"></i> Restorer</a>
												<a class='btn costumizer-button-delete delete-version' data-timestamp='${val.version}'><i class="fa fa-trash"></i>${trad.delete}</a></div>
											<hr>
										</li>`;
									});
									$version +='</ul>' ;
									$version = $version.replaceAll("dateUpdated","")
									$("#costum-version").html($version)
									costumizer.versionning.events.init();
									data["costum"] = $version;
								}
								resolve(data)
							},
							null

							);
					}
					)
				
			}
		}

	},
	icon : {
		selected : "",
		state : "",
		openIcon : function(defaultIcon){
            let displayHtml = ""
            let newIconHtml = "" 
            if (typeof defaultIcon !== "undefined") 
              defaultIcon = `<i class='`+defaultIcon+`' style='font-size:80px;color:'></i><br><br><button class='btn sp-rem-icon'>Effacer</button>`
            else
              defaultIcon = "<br>Choisir une icône"
            // $.each( cmsBuilder.iconList , function(k,val) { 
            //   iconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
            // });
            $.each( fontAwesome , function(k,val) { 
                newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='fa-"+k+"'><i class='fa fa-"+k+"'></i></a>"
            });
            displayHtml = "<div class='col-md-12 padding-bottom-10 bg-white'>"+
                            "<div class='col-md-5 sp-display-icon text-center' style='padding-top: 9%;height:190px;border: solid 1px #cccccc; left:1px;background-color:;'>"+
                                defaultIcon+
                            "</div>"+
                            "<div class='col-md-7 no-padding' style='height:190px;background-color: #fff;border: solid 1px #cccccc;'>"+
                                "<input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' onkeyup='costumizer.icon.searchIcon()' placeholder='Taper pour filtrer'>"+
                                "<div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>"+
                                    newIconHtml+
                                "</div>"+
                            "</div>"+
                          "</div>";
             bootbox.confirm({
              title : "Choix d'icône",
              message : displayHtml,
              callback: function (result) {
               if (result) {   
               	if (costumizer.icon.state == "use") {
                  costumizer.icon.selected = $(".sp-display-icon i").data("icon").substring(3)               		
               	}
                 $(document).trigger("icon-changed");
               }
             }
             });
			 $(".this-icon").on("click", function(e) {
				e.stopImmediatePropagation();
				$(".sp-display-icon").html("<div>fa "+$(this).data("icon")+"</div><i class='fa "+$(this).data("icon")+"' data-icon='"+$(this).data("icon")+"' style='font-size:80px;color:'></i>")
				costumizer.icon.state = "use"
			 })
			 $(".sp-rem-icon").on("click", function(e) {
				e.stopImmediatePropagation();
				$(".sp-display-icon").html("<br>Choisir une icône")
				costumizer.icon.state = "delete"
			 })
         },
        searchIcon : function () {
            var input, filter, i;
            input = $('#iconSearcher');
            filter = input.val().toUpperCase();

			var newIconHtml = "";
			var newIconList = Object.keys(fontAwesome).map(function(key, value) {
				return "fa-"+key;
			});
			$.each( newIconList , function(k,val) { 
			newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;color: inherit;' data-icon='"+val+"'><i class='fa "+val+"'></i></a>"
			});

            ti = Object.keys(newIconList).length;
            for (i = 0; i < ti; i++) {
                paragraph = newIconList[i]
                paragraph = paragraph.replace("fa-","")         
                paragraph = paragraph.split('-').join(' ');
                paragraph = paragraph.toUpperCase();
                if (paragraph.includes(filter))
                    $(".this-icon ."+newIconList[i]).parent().show();
                else
                    $(".this-icon ."+newIconList[i]).parent().hide();
            }
        },
		rigthPanelIcon : function(defaultIcon, element) {
			let displayHtml = ""
            let newIconHtml = ""          
            $.each( fontAwesome , function(k,val) { 
                newIconHtml += "<a class='this-icon btn' style='float: left;width: 35px;height: 35px;margin: 0 12px 12px 0;text-align: center;cursor: pointer; border-radius: 3px;font-size: 14px;box-shadow: 0 0 0 1px #ddd;' data-icon='fa-"+k+"'><i class='fa fa-"+k+"'></i></a>"
            });
            displayHtml = "<div id='edit-icon' data-element='"+element+"' style='padding: 0px;' class='col-md-12 padding-bottom-10'>"+
                            "<div class=' sp-display-icon text-center' style='padding-top: 9%;height:150px; left:1px;'>"+
                                "<div>"+defaultIcon+"</div>"+
								"<i class='"+defaultIcon+"' data-icon='' style='font-size:70px;color:'></i>"+
                            "</div>"+
				
                            "<div class=' no-padding' style='height:150px;'>"+
								"<input type='text' style='border-radius: inherit;' class='form-control' id='iconSearcher' onkeyup='costumizer.icon.searchIcon()' placeholder='Taper pour filtrer'>"+
                                "<div id='sp-icon-list' style='position: relative;clear: both;float: none;padding: 12px 0 0 12px;background: #fff;margin: 0;overflow: hidden;overflow-y: auto;max-height: 154px;display: flex;flex-wrap: wrap;'>"+
                                    newIconHtml+
                                "</div>"+
                            "</div>"+
                          "</div>";
			if ($(".fa-"+defaultIcon).html() == '') {
				$(".sp-display-icon").html("<div>fa "+$(this).data("icon")+"</div><i class='fa "+$(this).data("icon")+"' data-icon='"+$(this).data("icon")+"' style='font-size:70px;color:'></i>")
			}         
			return displayHtml;
		}
	},
	app : {
		training : {
			callback : function(newKey,pageSelected){
				
					mylog.log("appTypeName select",$(".appTypeName select").val());
					
					var training={
						"typeObj" : {
							"formation" : {
								"color" : "nightblue",
								"name" : "Formations",
								"add" : "onlyAdmin",
								"createLabel" : "Créer une formation",
								"icon" : "users",
								"sameAs" : "project",
								"formType" : "formation",
								"formParent" : "project",
								"dynFormCostum" : {
									"beforeBuild" : {
										"properties" : {
											"name" : {
												"label" : "Nom de la formation",
												"placeholder" : "Nom de la formation"
											},
											"shortDescription" : {
												"label" : "Description courte (public cible, objectifs génériques)",
												"inputType" : "textarea",
												"rules" : {
													"maxlength" : 140
												}
											},
											"description" : {
												"inputType" : "textarea",
												"label" : "Contenu détaillé, objectifs pédagogiques",
												"markdown" : true
											},
											"urlsDoc" : {
												"label" : "Liens vers la documentation,  le programme, ...",
												"inputType" : "array",
												"initOptions" : {
													"labelAdd" : "Ajouter un lien",
													"inputType" : "text"
												}
											},
											"parent" : {
												"inputType" : "finder",
												"initContext" : true,
												"label" : "Porteur de la formation",
												"initType" : [ 
													"organizations"
												],
												"buttonLabel" : "Changer le porteur de la formation",
												"placeholder" : "Chercher puis sélectionner le porteur de la formation",
											},
											"partner" : {
												"label" : "Structure(s) partenaire(s) de la formation",
												"inputType" : "finder",
												"initType" : [ 
													"organizations"
												],
												"buttonLabel" : "Sélectionner les partenaires de la formation",
												"placeholder" : "Chercher puis sélectionner les partenaires",
												"initContext" : true
											},
											"rate" : {
												"label" : "Tarif de la formation",
												"inputType" : "text",
												"rules" : {
													"number" : true
												}
											},
											"competence" : {
												"inputType" : "tags",
												"label" : "Compétences",
												"placeholder" : "Compétences",
												"info" : "Si autre compétence, vous pouvez saisir la compétence manquante et valider avec la touche Entrée (ou en finissant par une virgule)",
												"tagsList" : "competence"
											},
											"image" : {
												"label" : "Image, logo, illustration de la formation"
											},
											"email" : {
												"label" : "Email de contact"
											},
											"category" : {
												"inputType" : "hidden"
											}
										}
									},
									"onload" : {
										"actions" : {
											"setTitle" : "Ajouter une formation",
											"presetValue" : {
												"tags" : "Formation",
												"category" : "formation"
											},
											"hide" : {
												"tagstags" : 1,
												"publiccheckboxSimple" : 1,
												"urlurl" : 1
											}
										}
									},
									"afterSave" : "costum[costum.slug].formation.afterSave"
								}
							},
							"sessionFormation" : {
								"color" : "orange",
								"name" : "Session de formation",
								"add" : "onlyAdmin",
								"createLabel" : "Créer une session",
								"icon" : "users",
								"sameAs" : "event",
								"formType" : "sessionFormation",
								"formParent" : "event",
								"dynFormCostum" : {
									"beforeBuild" : {
										"properties" : {
											"name" : {
												"label" : "Nom de la session de formation",
												"placeholder" : "Nom de la session de formation"
											},
											"shortDescription" : {
												"label" : "Description courte spécifique à la session (période, contexte)",
												"inputType" : "textarea",
												"rules" : {
													"maxlength" : 300
												}
											},
											"description" : {
												"inputType" : "textarea",
												"label" : "Détails spécifiques à la session",
												"markdown" : true
											},
											"startDate" : {
												"label" : "Date de la première journée de la session de formation"
											},
											"endDate" : {
												"label" : "Date de la dernière journée de la session de formation"
											},
											"duration" : {
												"label" : "Nombre de journées de formation",
												"inputType" : "text",
												"rules" : {
													"number" : true
												}
											},
											"organizer" : {
												"label" : "Session de formation issue de quelle formation ?",
												"inputType" : "finder",
												"initType" : [ 
													"projects"
												],
												"openSearch" : false,
												"filters" : {
													"tags" : "Formation",
													"category" : "formation"
												},
												"search" : {
													"sourceKey" : costum.slug
												},
												"buttonLabel" : "Changer de formation",
												"placeholder" : "Chercher puis sélectionner une formation",
												"multiple" : false,
												"initContext" : false,
												"initContacts" : false,
												"rules" : {
													"required" : true
												}
											},
											"trainer" : {
												"label" : "Intervenants de la session",
												"buttonLabel" : "Sélectionner les formateurs intervenants",
												"placeholder" : "Chercher puis sélectionner les intervenants",
												"inputType" : "finder",
												"initType" : [ 
													"citoyens"
												]
											},
											"url" : {
												"label" : "Lien du formulaire d’inscription à la visio de présentation",
												"placeholder" : "Lien disponible depuis l'interface d'édition de votre formulaire",
												"topClass" : "hidden"
											},
											"image" : {
												"label" : "Image, logo, illustration de la session"
											},
											"public" : {
												"label" : "Publier la session dans le catalogue public des sessions"
											},
											"email" : {
												"label" : "Email de contact spécifique à la session"
											},
											"category" : {
												"inputType" : "hidden"
											},
											"gatheringDates" : {
												"inputType" : "hidden"
											},
											"visioDate" : {
												"inputType" : "hidden"
											}
										}
									},
									"onload" : {
										"actions" : {
											"setTitle" : "Ajouter une session",
											"presetValue" : {
												"type" : "course",
												"category" : "sessionFormation"
											},
											"hide" : {
												"tagstags" : 1,
												"typeselect" : 1,
												"recurrencycheckbox" : 1,
												"parentfinder" : 1
											}
										}
									},
									"afterBuild" : "costum[costum.slug].sessionFormation.afterBuild",
									"afterSave" : "costum[costum.slug].sessionFormation.afterSave"
								}
							}
						},
						"lists" : {
							"competence" : [ 
								"Suivi et gestion FEDER", 
								"Comptabilité", 
								"Gestion des paies", 
								"Recrutement", 
								"Gestion RH", 
								"Qualiopi", 
								"Graphisme", 
								"Developpement web", 
								"Hébergement web", 
								"Permaculture", 
								"Open data", 
								"Photographie", 
								"Conception Rédaction", 
								"Architecte", 
								"Facilitation", 
								"Médiation et gestion de crise", 
								"Restauration collective"
							]
						},
						"app" : {
							"#listing-sessions" : {
								"name" : {
									"fr" : "Listing sessions"
								},
								"metaDescription" : "",
								"restricted" : {
									"admins" : true
								},
								"icon" : "",
								"hash" : "#app.view",
								"urlExtra" : "/page/listing-sessions/url/costum.views.custom.formationGenerique.gestionFormation"
							},
							"#sessions-publics" : {
								"name" : {
									"fr" : "Annuaire des formations"
								},
								"metaDescription" : "",
								"icon" : "",
								"hash" : "#app.view",
								"urlExtra" : "/page/sessions-publics"
							},
							"#listing-formations" : {
								"name" : {
									"fr" : "Listing formations"
								},
								"metaDescription" : "",
								"restricted" : {
									"admins" : true
								},
								"icon" : "",
								"hash" : "#app.view",
								"urlExtra" : "/page/listing-formations/url/costum.views.custom.formationGenerique.gestionFormation"
							}
						},
						"htmlConstruct" : {
							"header" : {
								"menuTop" : {
									"left" : {
										"buttonList" : {
											"app" : {
												"buttonList" : {
													
												}
											}
										}
									}
								}
							},
							"preview" : {
								"element" : "costum.views.custom.formationGenerique.preview"
							}					
						}	
					};
					training.htmlConstruct.header.menuTop.left.buttonList.app.buttonList[newKey] = {
						"buttonList" : {
							"#listing-sessions" : true,
							"#listing-formations" : true,
							"#sessions-publics" : true
						}
					};

					var costumTraining={};
					
					if(typeof costumizer.obj.app!="undefined"){
						costumTraining.app=jQuery.extend(true,costumizer.obj.app,training.app);
					}	
					if(typeof costumizer.obj.typeObj!="undefined" && notEmpty(costumizer.obj.typeObj)){
						costumTraining.typeObj=jQuery.extend(true,costumizer.obj.typeObj,training.typeObj);
					}else{
						costumTraining.typeObj=training.typeObj;
					}
					if(typeof costumizer.obj.htmlConstruct!="undefined"){
						costumTraining.htmlConstruct=jQuery.extend(true,costumizer.obj.htmlConstruct,training.htmlConstruct);
					}		
					// if(typeof costumTraining.htmlConstruct!="undefined" && typeof costumTraining.htmlConstruct.header!="undefined" &&
					// typeof costumTraining.htmlConstruct.header.menuTop!="undefined" && typeof costumTraining.htmlConstruct.header.menuTop.left!="undefined" &&
					// typeof costumTraining.htmlConstruct.header.menuTop.left.buttonList!="undefined" && typeof costumTraining.htmlConstruct.header.menuTop.left.buttonList.app!="undefined" &&
					// typeof costumTraining.htmlConstruct.header.menuTop.left.buttonList.app.buttonList!="undefined"){
					// 	costumTraining.htmlConstruct.header.menuTop.left.buttonList.app.buttonList["#"+$(this).data("key")]=training.htmlConstruct.header.menuTop.left.buttonList.app.buttonList["#training"];
					// }
					mylog.log("costumTraining",costumTraining);
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/cms/updatecostum",
						{
							params: costumTraining,
							costumId    : costum.contextId,
							costumType  : costum.contextType,
							page : pageSelected,
							action : "costum/page",
							subAction : "add training listings"
						},
						function(data){
							costum=jQuery.extend(true,costum,costumTraining);
							var refreshObj={
								"htmlConstruct.header.menuTop.left.buttonList.app.buttonList" : {},
								"userId" : userConnected._id.$id
							};
							refreshObj["htmlConstruct.header.menuTop.left.buttonList.app.buttonList"][newKey]=	{
								"buttonList" : {
									"#listing-sessions" : "true",
									"#listing-formations" : "true",
									"#sessions-publics" : "true"
								}
							};
							costumizer.htmlConstruct.actions.refreshmenu(refreshObj,costum.htmlConstruct);

							toastr.success(tradCms.updateConfig);
							mylog.log("new costum config",costum,data);
							ajaxPost(
								null,
								baseUrl+"/"+moduleId+"/search/globalautocomplete",
								{category: "training",searchType: ["templates"],notSourceKey:true,type:["page"]},
								function(data){
									if(Object.keys(data.results).length==1){
										var idPublicSessionTpl=Object.keys(data.results)[0];
										ajaxPost(
											null,
											baseUrl+"/"+moduleId+"/template/use",
											{
												"id": idPublicSessionTpl,
												"parentId"   : costum.contextId,
												"page" 		 : " #sessions-publics",
												"parentSlug" : costum.contextSlug,
												"parentType" : costum.contextType
											},
											function(data){
												toastr.success(tradCms.updateConfig);
											}
										);
									}
								}
							);
						},null,null,{async:false}
					);
					window.location.reload();
				}
			}
		
	},
	options : {
	    optionsGeneralMap : {
			menuTop : {
				name : tradCms.menuTop,
				icon : 'outdent',
				inputs: [
					"background", "padding", "boxShadow","hideOnDesktop","hideOnTablet","hideOnMobil"
				]
			},
		},
		views : {
			init : function(dom=""){
				
				tabsOptions = {
					onInitialized : function(){
						costumizer.actions.cancel("#menu-right-costumizer");
					}
				};

				defaultPannelOptions = [
					{
						content: $(`<div class="col-md-12 sp-cms-options dark" id="costumOptions" ></div>`),
						key: "options",
						icon: "sliders",
						label: "Options",
					}
				]

				costumizer.actions.tabs.init("#right-panel", defaultPannelOptions , tabsOptions);


				$("#modal-content-costumizer-administration").fadeOut(1000);
				$(".cmsbuilder-right-content").addClass("active");
				
				var defaultValuesOptions = typeof costum.options !== "undefined" ? costum.options : {};
				new CoInput({
					container : "#costumOptions",
					inputs:[
						{
							type : "inputSimple",
							options : {
								type : "checkbox",
								name : "slidePageMobile",
								label : tradCms.slideChangePageOnMobile,
								class : "switch",
								defaultValue : (typeof defaultValuesOptions["slidePageMobile"] != "undefined") ? defaultValuesOptions["slidePageMobile"] : false,
							}
						},
					],
					onchange:function(name, value, payload){
						if ( value === true && typeof jsonHelper.getValueByPath(costum, "htmlConstruct.header.menuTop.left.buttonList.xsMenu.buttonList.app.buttonList") === "undefined"){
							bootbox.confirm({
								message: "<span class='text-center bold'>"+tradCms.warningNoMenuXs+"</span>",
								buttons: {
									confirm: {
										label: "<i class='fa fa-save'></i> Ok",
										className: 'btn-danger'
									},
									cancel: {
										label: trad.cancel,
										className: 'btn-default'
									}
								},
								callback: function (result) {
									if(result){
										$(".editMenuStandalone").click();
									} else {
										$(".cmsbuilder-right-content").find("#right-panel").html(`<p class="padding-20 text-center">${tradCms.PleasechooseAnItemToDisplayTheSettings}</p>`)
										$(".cmsbuilder-right-content").removeClass("active");
									}
								}
							});
						} else {
							tplCtx = {};
							tplCtx.id = costum.contextId;
							tplCtx.collection = costum.contextType;
							tplCtx.value = value;
							tplCtx.path =  "costum.options."+name;
							tplCtx.saveLog = {
								costumId : costum.contextId,
								costumType : costum.contextType,
								action : "costum/options",
								path : tplCtx.path,
								name : name,
								value : value,
							}

							dataHelper.path2Value(tplCtx, function (params) {
								toastr.success(tradCms.editionSucces);
								jsonHelper.setValueByPath(costum, "options."+name, value);
								costum.resetCache();
							});
						}
					}
				})

			},


		}
	}
	
}