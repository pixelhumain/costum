costumPreviewSocket = {
	socketId: "",
	init : function() {
		wsCOpubSub.subscriber(function(wsCO){
			costumPreviewSocket.emitEvent(wsCO, "client_join_room" ,{})

            wsCO.on("ping_change", (params) => {
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-left",
                    "preventDuplicates": true,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                  }
                if (params.includes(page)){
                    
                    toastr.options.onclick = function(e) {  urlCtrl.loadByHash("#"+page); }

                    toastr.info(tradCms.costumUpdated,tradCms.updateSite);
                }

			})
		})
	},
	emitEvent: function(wsCO, nameEvent) {
		if (coWsConfig.enable) {
			wsCO.emit("costum_event", {
				name: nameEvent,
				costumId: costum.contextId,
                page: page
			})
		}
	},
}

if ( !costum.editMode ) {
    costumPreviewSocket.init();
}