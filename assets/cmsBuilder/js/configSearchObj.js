var configSearchObj = {
    query : function(paramsData,filliaireCategories,kunik){
	    var filterSearch = {};

        if (paramsData["displayConfig"]["designType"]  == "elementPanelHtml" &&  paramsData["query"]["types"].length == 1 && paramsData["query"]["types"][0] == "events" )
            paramsData["displayConfig"]["designType"] = "eventPanelHtml";
        
        paramsData.displayConfig.filterDesign = {
            displayType : "row"
        };


	    var paramsFilter = {
            container : "#filterContainers"+kunik,
            defaults : {
                types : paramsData["query"]["types"],
                indexStep : parseInt(paramsData["query"]["indexStep"],10),
                filters : {
                    
                },
                fieldShow : paramsData["query"]["fieldShow"]
            },
            results : {
                scrollableDom : ".cmsbuilder-center-content",
                renderView : "directory."+ paramsData["displayConfig"]["designType"],
                smartGrid : paramsData["displayConfig"]["smartGrid"],
                map : {
                    show : true 
                },
                afterRenderingEvent : function () {
                    if ($(".commun-desc.desc-2-ellipsis, .commun-title .desc-2-ellipsis").length > 0) {
                        $(".commun-desc.desc-2-ellipsis, .commun-title .desc-2-ellipsis").each(function() {
                            const currentElem = $(this);
                            var ajustScrollVal = 6;
                            if (currentElem.hasClass("this-commun-title")) {
                                ajustScrollVal = 9;
                            }
                             if (currentElem[0].scrollHeight - ajustScrollVal > currentElem[0].clientHeight) {
                                 currentElem.tooltip({
                                     title: currentElem.text(),
                                     placement: "top",
                                     trigger: "hover",
                                     container: "body.index",
                                     template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner x-large custom-bg"></div></div>',
                                 });
                             }
                             if (typeof coInterface.eventTohideTooltip != "undefined" && currentElem[0]) {
                                currentElem[0].removeEventListener("click", coInterface.eventTohideTooltip)
                                currentElem[0].addEventListener("click", coInterface.eventTohideTooltip)
                            }
                         });
                    }
                    if ($(".commun-desc.desc-3-ellipsis").length > 0) {
                        $(".commun-desc.desc-3-ellipsis").each(function() {
                            const currentElem = $(this);
                            var ajustScrollVal = 6;
                             if (currentElem[0].scrollHeight - ajustScrollVal > currentElem[0].clientHeight) {
                                 currentElem.tooltip({
                                     title: currentElem.text(),
                                     placement: "top",
                                     trigger: "hover",
                                     container: "body.index",
                                     template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner x-large custom-bg"></div></div>',
                                 });
                             }
                             if (typeof coInterface.eventTohideTooltip != "undefined" && currentElem[0]) {
                                currentElem[0].removeEventListener("click", coInterface.eventTohideTooltip)
                                currentElem[0].addEventListener("click", coInterface.eventTohideTooltip)
                            }
                         });
                    }
                    if ($(".commun-tags .tags.text-ellipsis").length > 0) {
                        $(".commun-tags .tags.text-ellipsis").each(function() {
                            const currentElem = $(this);
                            if (currentElem[0].scrollWidth - 1 > currentElem[0].clientWidth) {
                                currentElem.tooltip({
                                    title: currentElem.text(),
                                    placement: "top",
                                    trigger: "hover",
                                    container: "body.index",
					                template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large custom-bg"></div></div>',
                                });
                            }
                            if (typeof coInterface.eventTohideTooltip != "undefined" && currentElem[0]) {
                                currentElem[0].removeEventListener("click", coInterface.eventTohideTooltip)
                                currentElem[0].addEventListener("click", coInterface.eventTohideTooltip)
                            }
                        })
                    }
                    if ($(".commun-tags .tags").length > 0) {
                        $(".commun-tags .tags").off('click').on('click', function () {
                            const val = $(this).text().trim().replace(/#/g, "").trim();
                            $(`#filterContainers${ kunik } #filterCollapse-filtre-par-besoin .panel-body [data-value="${val}"]`).trigger('click');
                        })
                    }
                    if ($(".commun-card .commun-delete-btn").length > 0) {
                        $(".commun-card .commun-delete-btn").off('click').on('click', function () {
                            const currentBtn = $(this);
                            $.confirm({
                                title: trad["delete"], 
                                type: 'red', 
                                content: "<span class='bold'>" + trad["Are you sure you want to delete the common"] + " \"" + currentBtn.attr("data-commun-title") + "\"?</span><br><span class='text-red bold'>" + trad.actionirreversible + "</span>", 
                                buttons: {
                                    no: {
                                        text: trad.no,
                                        btnClass: 'btn btn-default'
                                    }, yes: {
                                        text: trad.yes,
                                        btnClass: 'btn btn-danger',
                                        action: function () {
                                            getAjax("", baseUrl + "/survey/co/delete/id/" + currentBtn.attr("data-communid"), function (data) {
                                                if (data.result) {
                                                    toastr.success(trad["processing delete ok"] + " !");
                                                    if ($(`[data-need-refreshblock]`).length > 0) {
                                                        $(`[data-need-refreshblock]`).each((index, elem) => {
                                                            const blockId = $(elem).attr("data-need-refreshblock")
                                                            if (typeof refreshBlock == "function") {
                                                                refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                                            }
                                                        })
                                                    }
                                                }
                                            }, "html");
                                        }
                                    }
                                }
                            });
                        })
                    }

                    if ($(".cute-card").length > 0) {
                        if (typeof coInterface.counterAnimation == "function") {
                            const cardHoverCounterAnim = function () {
                                coInterface.counterAnimation($(this).find(".animate-on-hover"), $(this).find(".animate-on-hover").attr("data-start"), $(this).find(".animate-on-hover").attr("data-end"), 1000)
                                $.each($(this).find(".change-on-hover"), function() {
                                    $(this).text($(this).attr("data-hover-value"))
                                    $(this).fadeIn(500)
                                })
                            }
                            const cardLeave = function () {
                                coInterface.counterAnimation($(this).find(".animate-on-hover"), $(this).find(".animate-on-hover").attr("data-end"), $(this).find(".animate-on-hover").attr("data-start"), 1000)
                                $.each($(this).find(".change-on-hover"), function() {
                                    $(this).text($(this).attr("data-default-value"))
                                    $(this).fadeIn(500)
                                })
                            }
                            $(".cute-card").off("mouseenter", cardHoverCounterAnim).on("mouseenter", cardHoverCounterAnim)
                            $(".cute-card").off("mouseleave", cardLeave).on("mouseleave", cardLeave)
                        }
                    }
                },
                dom : ".dropdown-search-"+kunik
            }
        };
	    if (typeof paramsData["query"]["filters"] != "undefined" && notEmpty(paramsData["query"]["filters"]))
            paramsFilter.defaults.filters = paramsData["query"]["filters"];

        if (typeof paramsData["footerDom"] != "undefined" && notEmpty(paramsData["footerDom"]))
            paramsFilter.footerDom = paramsData["footerDom"];


        if (typeof paramsData["query"]["formId"] != "undefined" && notEmpty(paramsData["query"]["formId"]) && paramsData["query"]["types"] && (paramsData["query"]["types"].indexOf("answers") > -1 || paramsData["query"]["types"].indexOf("projects") > -1)) {
            paramsFilter.defaults.filters.form = paramsData["query"]["formId"];
            paramsFilter.defaults.filters['answers.aapStep1.titre'] = {
                '$exists': true
            }
        }
        if(paramsData["query"]["orderBy"] != ""){
        	paramsFilter.defaults.sortBy = {};
        	paramsFilter.defaults.sortBy[paramsData["query"]["orderBy"]] = paramsData["query"]["orderType"];
        }

        /* More options for filter */
        if (typeof paramsData["moreQueryConfig"] != "undefined") {
            if (typeof paramsData["moreQueryConfig"]["url"] != "undefined") {
                if (typeof paramsData["query"]["formId"] != "undefined") {
                    if (paramsData["moreQueryConfig"]["url"] == "aapProposalDir" &&  paramsData["query"]["types"] && paramsData["query"]["types"].indexOf("answers") > -1) {
                        paramsFilter.urlData = baseUrl + "/co2/aap/directoryproposal/source/" + costum.slug + "/form/" + paramsData["query"]["formId"];
                    }
                    if (paramsData["moreQueryConfig"]["url"] == "aapProjectDir" &&  paramsData["query"]["types"] && paramsData["query"]["types"].indexOf("projects") > -1) {
                        paramsFilter.urlData = baseUrl + "/co2/aap/directoryproject/context/" + costum.contextId + "/form/" + paramsData["query"]["formId"];
                    }
                } else if (paramsData["moreQueryConfig"]["url"] == "communDir" && paramsData["query"]["types"] && paramsData["query"]["types"].indexOf("answers") > -1) {
                    paramsFilter.urlData = baseUrl + "/co2/aap/aac/method/communs_directory";
                }
                if (paramsData["moreQueryConfig"]["url"] == "aacDir" && paramsData["query"]["types"] && paramsData["query"]["types"].indexOf("forms") > -1) {
                    paramsFilter.urlData = baseUrl + "/co2/aap/aac/method/aac_directory";
                    if (typeof paramsFilter.defaults.filters != "undefined") {
                        paramsFilter.defaults.filters.aapType = ["aac"];
                    }
                }
            }
            if (typeof paramsData["moreQueryConfig"]["redirectPage"] != "undefined") {
                paramsFilter.redirectPage = paramsData["moreQueryConfig"]["redirectPage"];
            }
            /* Add options in defaults fiters */
            if (typeof paramsData["moreQueryConfig"]["filtersMoreOptions"] != "undefined") {
                if (paramsData["moreQueryConfig"]["filtersMoreOptions"]["filtersOption"]) {
                    var parsedStr = null;
                    // Remplacer les variables dynamiques formaté en ${var} par leur valeur
                    parsedStr = paramsData["moreQueryConfig"]["filtersMoreOptions"]["filtersOption"].replace(/\$\{([^}]+)\}/g, (match, p1) => {
                        try {
                            // Évaluer l'expression à l'intérieur de ${}
                            const evalResult = eval(p1);
                            return Array.isArray(evalResult) ? JSON.stringify(evalResult) : evalResult;
                        } catch (error) {
                            mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
                            return p1;
                        }
                    })
                    var parsedObj = null;
                    try {
                        parsedObj = JSON.parse(parsedStr)
                    } catch (e) {
                        mylog.error("the current value is not a valid json", parsedStr)
                    }
                    if (parsedObj) {
                        const addParsedObjToFilters = (parsedObject) => {
                            $.each(parsedObject, function (parsedObjKey, parsedObjVal) {
                                paramsFilter.defaults.filters[parsedObjKey] = parsedObjVal;
                            })
                        }
                        var applyFor = "all"
                        if (paramsData["moreQueryConfig"]["filtersMoreOptions"]["applyFor"]) {
                            applyFor = paramsData["moreQueryConfig"]["filtersMoreOptions"]["applyFor"]
                        }
                        switch (applyFor) {
                            case "forAdmin":
                                if (isSuperAdmin || isInterfaceAdmin) {
                                    addParsedObjToFilters(parsedObj)
                                }
                                break;
                            case "forUser":
                                if (!isSuperAdmin && !isInterfaceAdmin) {
                                    addParsedObjToFilters(parsedObj)
                                }
                                break;
                            default:
                                addParsedObjToFilters(parsedObj)
                                break;
                        }
                        
                    }
                }
            }
        }
        if(paramsData["query"]["notSourceKey"])
        	paramsFilter.defaults["notSourceKey"] = true



        if(paramsData["query"]["community"]){
        	paramsFilter.defaults["notSourceKey"] = true;
            paramsFilter.defaults.filters['$or'] = {};
            paramsFilter.defaults.filters['$or']["source.keys"] = costum.slug;
            paramsFilter.defaults.filters['$or']["reference.costum"] = costum.slug;
            paramsFilter.defaults.filters['$or']["links.contributors."+costum.contextId] = {'$exists':true};
            paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        }
        if(paramsData["query"]["loadEvent"] == "scroll"){
        	paramsFilter.loadEvent = {};
        	paramsFilter.loadEvent.default = "scroll";
        }else if(paramsData["query"]["loadEvent"] == "pagination"){
        	if(typeof paramsFilter.loadEvent != "undefined" && typeof paramsFilter.loadEvent.default)
        		delete paramsFilter.loadEvent.default
        }
        if(paramsData["displayResults"] == "map"){
        	paramsFilter.results.map.active = true
        	$("#mapContent").show();
        }else{
        	$("#mapContent").hide();
        	paramsFilter.results.map.active = false
        }
        if (paramsData["displayConfig"] && paramsData["displayConfig"]) {
            if (paramsData["displayConfig"]["disableScrollTop"]) {
                paramsFilter.disableScrollTop = paramsData["displayConfig"]["disableScrollTop"]
            }
            if (paramsData["displayConfig"]["disableScrollTopPageClick"]) {
                paramsFilter.disableScrollTopPageClick = paramsData["displayConfig"]["disableScrollTopPageClick"]
            }
        }
        // /* Header */
        if(notEmpty(paramsData["header"])){
            paramsFilter["header"] = {};
            paramsFilter["header"]["dom"] = ".headerSearchIncommunity"+kunik;
            paramsFilter["header"]["options"] = {
                left : {
                    classes : 'col-xs-8 elipsis no-padding',
                    group:{
                        count : true
                    }
                },
                right : {
                    classes : 'col-xs-4 text-right no-padding',
                    group : { 
                        map : true,
                        add : true
                    }
                }
            } 
        }
        // /* Header left */
        if(typeof paramsData["header"] != "undefined" && typeof paramsData["header"]["left"] != "undefined"){
        	var keyExistsLeft = Object.keys(paramsData["header"]["left"]);
        	$.each(paramsData["header"]["left"],function(kl,vl){
                if (vl) {
                    if(typeof paramsFilter["header"]["options"] != "undefined" && typeof paramsFilter["header"]["options"]["left"]!= "undefined" && typeof paramsFilter["header"]["options"]["left"]["group"] != "undefined"){
                        paramsFilter.header.options.left.group[kl] = true;
                    }else{
                        paramsFilter.header.options.left.group = {}
                        paramsFilter.header.options.left.group[kl] = true
                    }
                } else if (paramsFilter["header"]["options"] && paramsFilter["header"]["options"]["left"] && paramsFilter["header"]["options"]["left"]["group"] && paramsFilter["header"]["options"]["left"]["group"][kl]) {
                    delete paramsFilter.header.options.left.group[kl]
                }
        	})
        	keyNotExists =  $(Object.keys(keyHeaderGroup)).not($(keyExistsLeft)).get();
        	if(Object.keys(keyNotExists).length != 0 )
        		$.each(keyNotExists,function(kne,vne){
        			if(typeof paramsFilter["header"]["options"] != "undefined" && 
        			typeof paramsFilter["header"]["options"]["left"] != "undefined" && 
        			typeof paramsFilter["header"]["options"]["left"]["group"] != "undefined" &&  
        			typeof paramsFilter["header"]["options"]["left"]["group"][vne] != "undefined")
        		 		delete paramsFilter.header.options.left.group[vne]
        		})
        }
        // /* Header right */
        if(typeof paramsData["header"] != "undefined" && typeof paramsData["header"]["right"] != "undefined"){
        	var keyExistsRight = Object.keys(paramsData["header"]["right"]);
        	$.each(paramsData["header"]["right"],function(kr,vr){
                if (vr) {
                    if(typeof paramsFilter["header"]["options"] != "undefined" && 
                    typeof paramsFilter["header"]["options"]["right"]!= "undefined" && 
                    typeof paramsFilter["header"]["options"]["right"]["group"] != "undefined"){
                        paramsFilter.header.options.right.group[kr] = true;
                    }else{
                        paramsFilter.header.options.right.group = {}
                        paramsFilter.header.options.right.group[kr] = true
                    }
                } else if (paramsFilter["header"]["options"] && paramsFilter["header"]["options"]["right"] && paramsFilter["header"]["options"]["right"]["group"] && paramsFilter["header"]["options"]["right"]["group"][kr]) {
                    delete paramsFilter.header.options.right.group[kr]
                }
        	})
        	keyNotExists =  $(Object.keys(keyHeaderGroup)).not($(keyExistsRight)).get();
        	if(Object.keys(keyNotExists).length != 0 )
        		$.each(keyNotExists,function(kne,vne){
        			if(typeof paramsFilter["header"]["options"] != "undefined" && 
        			typeof paramsFilter["header"]["options"]["right"] != "undefined" && 
        			typeof paramsFilter["header"]["options"]["right"]["group"] != "undefined" &&  
        			typeof paramsFilter["header"]["options"]["right"]["group"][vne] != "undefined")
        		 		delete paramsFilter.header.options.right.group[vne]
        		})
        }
        /* Filters  */
        if(notEmpty(paramsData["filters"])){
            paramsFilter.filters = {};
            var isFilterVertical = false;
            /* Type d'affichage du filtre : row -> horizontal, column : vertical */
            if (typeof paramsData.filters.filterDesign != "undefined" && notEmpty(paramsData.filters.filterDesign)) {
                if (paramsData.filters.filterDesign.displayType) {
                    paramsData.displayConfig.filterDesign.displayType = paramsData.filters.filterDesign.displayType;
                    paramsData.filters.filterDesign.displayType == "column" ? (isFilterVertical = true, $('#filterContainerParent'+ kunik).addClass("designed-column-filter")) : "";
                    delete paramsData.filters.filterDesign.displayType;
                }
            }
            if(typeof paramsData.filters.text != "undefined"){
                if(paramsData.filters.text.active){
                    paramsFilter.filters.text = true;
                    paramsData.filters.hasTextFilter = true;
                    if(paramsData.filters.text.placeholder) {
                        if(typeof paramsFilter.filters.text != "object") {
                            paramsFilter.filters.text =  {};
                            paramsFilter.filters.text.placeholder = paramsData.filters.text.placeholder;
                        }
                    }
                    if(paramsData.filters.text.searchBy) {
                        if(typeof paramsFilter.filters.text != "object") {
                            paramsFilter.filters.text =  {};
                            paramsFilter.filters.text.searchBy = paramsData.filters.text.searchBy;
                        }else{
                            paramsFilter.filters.text.searchBy = paramsData.filters.text.searchBy;
                        }
                    }
                    if (isFilterVertical) {
                        paramsFilter.filters.text.icon = "fa-search";
						paramsFilter.filters.text.customClass = "set-width to-sticky to-set-value";
                    }

                }              
            }
            if(typeof paramsData.filters.types != "undefined"){
                var lists = paramsData["query"]["types"];
                if(paramsData.filters.types.lists)
                    lists = paramsData.filters.types.lists
                if(paramsData.filters.types.active){
                    if (isFilterVertical) {
                        var thisLists = {}
                        $.each(lists, function(index, type) {
                            const elt = directory.getTypeObj(type)
                            thisLists[type] = ucfirst(elt.name);
                        })
                        paramsFilter.filters.types = {
                            view: "accordionList",
                            name: tradCms.elementType,
                            list: thisLists,
                            keyValue : false
                        };

                        ;
                    } else {
                        paramsFilter.filters.types = {
                            lists : lists
                        };
                    }
                }
            }
            if(typeof paramsData.filters.scope != "undefined"){
                if(paramsData.filters.scope.active)
                    paramsFilter.filters.scope = true;
            }
            if(typeof paramsData.filters.themes != "undefined"){
                if(paramsData.filters.themes.active) {
                    if (isFilterVertical) {
                        paramsFilter.filters.themes = {
                            view : "themesList",
                            event : "themes",
                            lists : filliaireCategories,
                        }
                    } else {
                        paramsFilter.filters.themes = {
                            lists : filliaireCategories
                        };
                    }
                }
            }
            if(typeof paramsData.filters.scopeList != "undefined"){
                if(paramsData.filters.scopeList.active)
                    paramsFilter.filters.scopeList = {
                        name : paramsData.filters.scopeList.name,
                        params : {
                            countryCode : (paramsData.filters.scopeList.coutryCode)?paramsData.filters.scopeList.coutryCode:"FR", 
                            level : ["3"],
                            sortBy:"name"
                        }
                    };
            }
            if(typeof paramsData.filters.filtersPersonal != "undefined" && typeof paramsData.filters.filtersPersonal.filterPersonal != "undefined"){
                $.each(paramsData.filters.filtersPersonal.filterPersonal,function(k,v){
                    var key = slugify(v.label).toLowerCase();
                    
                    paramsFilter.filters[key] = {
                        name : v.label && trad[v.label] ? trad[v.label] : v.label,
                        view : v.view,
                        field : v.field,
                        event : v.events
                    }
                    if(v.field == "tags"){
                        paramsFilter.filters[key].type = "tags"
                    } else if (v.events == "sortBy") {
                        paramsFilter.filters[key].type = "sortBy"
                    } else {
                        paramsFilter.filters[key].type = "filters"
                    }

                    if (v.field && v.field.indexOf("${") > -1) {
                        paramsFilter.filters[key].field = v.field.replace(/\$\{([^}]+)\}/g, (match, p1) => {
                            try {
                                // Évaluer l'expression à l'intérieur de ${}
                                return eval(p1);
                            } catch (error) {
                                mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
                                return p1;
                            }
                        })
                    }
                    
                    if (v.events != "text") {
                        if(v.lists == "exists"){
                            paramsFilter.filters[key].list = costum.lists[v.exist]
                        }else if (v.lists == "keyValue" && v.listObject) {
                            paramsFilter.filters[key].list = {}
                            $.each(v.listObject, function (index, val) {
                                var currentValue = val.objectvalue;
                                try {
                                    currentValue = JSON.parse(val.objectvalue.replace(/'([^"]+)'/g, '"$1"'))
                                } catch (e) {
                                    mylog.log("the current value is not a valid json")
                                }
                                if (typeof currentValue == "string" && trad[currentValue]) {
                                    currentValue = trad[currentValue]
                                }
                                if (typeof val.objectkey == "string" && trad[val.objectkey]) {
                                    val.objectkey = trad[val.objectkey]
                                }
                                paramsFilter.filters[key].list[val.objectkey] = currentValue
                            })
                        } else if (v.lists == "dynamic") {
                            if (notEmpty(v.dynamic)) {
                                var manipulatedObject = {
                                    splittedValDyn : v.dynamic.split("."),
                                    formatedValDyn : ""
                                };
                                $.each(manipulatedObject.splittedValDyn, (index, item) => {
                                    if (index > 0) {
                                        manipulatedObject.formatedValDyn += "?.['" + item + "']"
                                    } else manipulatedObject.formatedValDyn += item
                                })
                                if (eval(manipulatedObject.formatedValDyn)) {
                                    paramsFilter.filters[key].list = eval(manipulatedObject.formatedValDyn)
                                } else {
                                    paramsFilter.filters[key].list = []    
                                }
                            } else {
                                paramsFilter.filters[key].list = []
                            }
                        }else{
                            v.listTags ? paramsFilter.filters[key].list = v.listTags.split(",") : paramsFilter.filters[key].list = []
                        }
                    } else {
                        // searchBar config
                        // paramsFilter.filters[key].field = "text",
                        paramsData.filters.hasTextFilter = true;
                        paramsFilter.filters[key].placeholder = v.label && trad[v.label] ? trad[v.label] : v.label;
                    }
                    if (v.options) {
                        var currentOptions = {}
                        try {
                            currentOptions = JSON.parse(v.options.replace(/'([^"]+)'/g, '"$1"'))
                            var isForAdminOnly = false;
                            var isConnectedOnly = false;
                            $.each(currentOptions, function (optKey, optVal) {
                                if (optKey.indexOf("name") > -1 && typeof optVal == "string" && trad[optVal]) {
                                    optVal = trad[optVal]
                                }
                                paramsFilter.filters[key][optKey] = optVal;
                                if (optKey.indexOf("isAdminOnly") > -1 && (/true/).test(optVal)) {
                                    isForAdminOnly = true
                                }
                                if (optKey.indexOf("isConnectedOnly") > -1 && (/true/).test(optVal)) {
                                    isConnectedOnly = true
                                }
                            })
                            if (isForAdminOnly && !isSuperAdmin && !isInterfaceAdmin) {
                                delete paramsFilter.filters[key]
                            }
                            if (isConnectedOnly && !userId) {
                                delete paramsFilter.filters[key]
                            }
                        } catch (error) {
                            mylog.log("the current options is not a valid json")
                        }
                    }
                    // list :  costum.listStatutAction
                })

            }
        }
        filterSearch = searchObj.init(paramsFilter);
        if (filterSearch.search && filterSearch.search.obj && filterSearch.search.obj.text && filterSearch.pInit && filterSearch.pInit.filters && filterSearch.pInit.filters) {
            var textFilterPath = "";
            var textFilterKey = "";
            $.each(filterSearch.pInit.filters, function (keyFilter, valFilter) {
                if (valFilter.event && valFilter.event == "text" && valFilter.field && !notEmpty(textFilterPath)) {
                    textFilterPath = valFilter.field
                    textFilterKey = keyFilter
                }
            })

            if (textFilterPath) {
                filterSearch.search.obj.textPath = textFilterPath;
                const mainFilterAliasSelector = `#filterAlias${ kunik } .alias-main-search-bar, #filterSecondAlias${ kunik } .alias-main-search-bar, #filterContainers${ kunik } .searchBar-filters input[data-field="${ textFilterKey }"]`;
                $(mainFilterAliasSelector).val(filterSearch.search.obj.text)
            }
        }
        filterSearch.search.init(filterSearch);

    },
    common : {
        range: function (__start, __end) {
			const output = [];
			const alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
			if (typeof __start === 'number' && typeof __end === 'number' && !isNaN(__start) && !isNaN(__end)) {
				for (let i = __start; i <= __end; i++) output.push(i);
			} else if (typeof __start === 'string' && typeof __end === 'string' && __start.length === 1 && __end.length === 1) {
				let first_index_of = alphabets.findIndex(function (__alphabet) {
					return __alphabet === __start
				});
				let last_index_of = alphabets.findIndex(function (__alphabet) {
					return __alphabet.toLowerCase() === __end || __alphabet.toUpperCase() === __end
				});
				if (first_index_of >= 0) {
					if (last_index_of >= first_index_of) {
						for (let i = first_index_of; i <= last_index_of; i++) output.push(alphabets[i]);
					} else {
						output.push(alphabets[first_index_of]);
					}
				} else {
					first_index_of = alphabets.findIndex(function (__alphabet) {
						return __alphabet.toUpperCase() === __start
					});
					if (first_index_of >= 0) {
						if (last_index_of >= first_index_of) {
							for (let i = first_index_of; i <= last_index_of; i++) output.push(alphabets[i].toUpperCase());
						} else {
							output.push(alphabets[first_index_of].toUpperCase());
						}
					}
				}
			}
			return output;
		}
    },
    filterEvents: function(paramsData, kunik) {
        const mainFilterAliasSelector = `#filterAlias${ kunik } .alias-main-search-bar, #filterSecondAlias${ kunik } .alias-main-search-bar`;
        const pointedFilterElem = $("#"+$("#filterAlias"+kunik).attr("data-connected-filter") + " #filterContainerInside .searchBar-filters .main-search-bar")
        if (paramsData.displayConfig.filterDesign.displayType == "column") {
            $('#filterContainerParent'+ kunik).addClass("hide")
            $("#searchBodyContainer"+ kunik + " .header-grid-container .center-filter-header").removeClass("set-sticky")
            if ($(window).width() < 768) {
                $('#filterContainerParent' + kunik + " .visible-xs button#show-filters-xs").parent().css({
                    cssText: "display: none !important"
                })
                $("#searchBodyContainer"+ kunik + " .header-grid-container .center-filter-header").addClass("set-sticky")
                const stickyElem = $('.header-grid-container .center-filter-header.set-sticky');

                function checkStickyPos() {
                    const navBarHeight = $("#mainNav.navbar-top.cosDyn-menuTop")?.outerHeight() ? $("#mainNav.navbar-top.cosDyn-menuTop").outerHeight() : 0;
                    const scrollTop = $(window).scrollTop() + navBarHeight;
                    const elemOffsetTop = stickyElem?.offset()?.top ? stickyElem?.offset()?.top : 0;
                    const initialPosition = $(".header-grid-container")?.offset()?.top ? $(".header-grid-container").offset().top : 0
                    if($(window).width() > 767)
                        return;
                    if (scrollTop >= elemOffsetTop && scrollTop - navBarHeight > initialPosition) {
                        stickyElem.css({
                            cssText: `
                                position: fixed;
                                top: ${navBarHeight}px;
                                width: 98%;
                                left: 3px;
                                border-end-start-radius : 4px;
                                border-end-end-radius: 4px;
                                box-shadow: rgba(82, 79, 82, 0.7) 0px 1px 7px 0px;
                                padding-right: 3.2% !important;
                            `
                        });
                    } else {
                        stickyElem.css({
                            position: 'relative',
                            top: 'inherit',
                            width: '69%',
                            left: 'inherit',
                            'box-shadow': 'inherit',
                            'padding-right': 'inherit'
                        });
                    }
                }
                window.addEventListener('scroll', checkStickyPos);
            }

            // show design specific for vertical filter
            $("#filterAlias"+kunik).removeClass("hide")
            if (paramsData.displayConfig && paramsData.displayConfig.disableScrollTop && paramsData.displayConfig.disableScrollTop == true) {
                $("#searchBodyContainer"+ kunik).css("min-height", "80vh")
            }
            if (paramsData.filters && paramsData.filters.hasTextFilter) {
                $("#filterSecondAlias"+kunik).removeClass("hide")
            } else {
                $("#header-filter-container"+kunik).addClass("filter-global-shown")
                $("#filterAlias"+kunik + " .filter-alias").addClass("hide")
            }
            $('#filterContainerParent'+ kunik + " .column-filter-title").removeClass("hide")

            if (paramsData.displayConfig.filterDesign.displayType == "column" && $(window).width() > 767) {
                if ($('#filterContainerParent'+ kunik).is(":visible")) {
                    $('#searchBodyContainer'+ kunik).addClass("col-lg-9 col-md-9 col-sm-12 fit-cover-card-img");
                }
                $('#filterContainerParent'+ kunik).addClass("col-lg-3 col-md-3 col-sm-12");
            } else {
                $('#searchBodyContainer'+ kunik).removeClass("col-lg-9 col-md-9 fit-cover-card-img");
                $('#filterContainerParent'+ kunik).removeClass("col-lg-3 col-md-3");
            }
            $(mainFilterAliasSelector).attr({
                "placeholder": pointedFilterElem.attr("placeholder"),
                "data-text-path": pointedFilterElem.attr("data-text-path"),
                "data-field": pointedFilterElem.attr("data-field"),

            })

            $(mainFilterAliasSelector).off().on("keyup", function (e) {
				const thisInputAlias = this;
                $(mainFilterAliasSelector).val($(this).val())
				if (e.keyCode != 13) {
                    $("#"+$("#filterAlias"+kunik).attr("data-connected-filter") + " .to-set-value input.main-search-bar").val($(thisInputAlias).val()).trigger("keyup")
				}
			})
        } else {
            $("#header-filter-container"+kunik).removeClass("row header-grid-container bs-ml-0 bs-mr-0")
        }
        $(`#show-filters-lg${ kunik }.showHide-filters-xs,#btnHideFilter.showHide-filters-xs`).off().on("click", function (event) {
            event.stopImmediatePropagation();
            if ($(".open-xs-menu-collapse").children('i').hasClass("fa-times")) {
                $(".open-xs-menu-collapse").children('i').toggleClass("fa-times");
                $(".open-xs-menu-collapse").children('i').toggleClass("fa-bars");
                $("#menuTopCenter").toggleClass("in")
            }
            if ($(window).width() > 767) {
                if ($('#filterContainerParent'+ kunik).is(":visible")) {
                    $(`#show-filters-lg${ kunik }.showHide-filters-xs`).find("i").removeClass("fa-times").addClass("fa-sliders");
                    if (paramsData.filters && paramsData.filters.hasTextFilter) {
                        $("#header-filter-container"+kunik).removeClass("filter-global-shown");
                    }
                    $('#filterContainerParent'+ kunik).hide(300, function () {
                        $.each(configSearchObj.common.range(1, 12), function (__, __col) {
                            $('#searchBodyContainer'+kunik).removeClass(`col-xs-${__col} col-md-${__col}`);
                        });
                        $("#searchBodyContainer"+kunik).addClass('col-xs-12 col-md-12').removeClass("fit-cover-card-img");
                    });
                    $(".filterHidden").show('slide', {}, 300);
                    $(".openFilterParams").hide('fade', {}, 600)
                } else {
                    $(`#show-filters-lg${ kunik }.showHide-filters-xs`).find("i").removeClass("fa-sliders").addClass("fa-times");
                    $("#header-filter-container"+kunik).addClass("filter-global-shown");
                    $(".filterHidden").hide();
                    $.each(configSearchObj.common.range(1, 12), function (__, __col) {
                        $('#searchBodyContainer'+kunik).removeClass(`col-xs-${__col} col-md-${__col}`);
                    });
                    $("#searchBodyContainer"+kunik).addClass('col-xs-9 col-md-9 fit-cover-card-img');
                    $('#filterContainerParent'+ kunik).removeClass("hide").show(300);
                    if (!$("#filterContainerInside").is(":visible")) {
                        $("#filterContainerInside").show()
                    }
                    $(".openFilterParams").show('fade', {}, 600);
                }
            } else {
                $('#filterContainerParent'+ kunik).removeClass("hide").show(300);
                $('#filterContainerParent' + kunik + " #filterContainerInside").show();
            }
        });
        $(".close-filter-xs").off("click").on("click", function(e) {
            e.stopImmediatePropagation();
            $(this).closest(".menu-filters-lg.designed-column-filter").hide(300)
        })

    },

    bindEvents: function(paramsData, kunik) {
        if (paramsData["displayConfig"]["smartGrid"] == true) {
            $('.elem-stdr-content-' + kunik + ' div').removeClass('swiper-slide')
            if ( $('.elem-stdr-content-' + kunik + ' .smartgrid-slide-element').hasClass("col-lg-4 col-md-4 col-sm-6") == false  && paramsData["displayConfig"]["designType"] != "materialDesign")
                $('.elem-stdr-content-' + kunik + ' .smartgrid-slide-element').addClass('col-lg-4 col-md-4 col-sm-6');
        } else {
            $('.elem-stdr-content-' + kunik + ' .smartgrid-slide-element').removeClass('col-lg-4 col-md-4 col-sm-6');
        }

        $('.elem-stdr-content-' + kunik + ' .swiper-slide .lbh').removeClass('lbh').addClass('lbh-preview-element');
        $('.elem-stdr-content-' + kunik + ' .swiper-slide .see-more').removeClass('lbh-preview-element').addClass('lbh');
        coInterface.bindLBHLinks();
        directory.bindBtnElement();
        configSearchObj.filterEvents(paramsData, kunik);
        configSearchObj.cssCarousel(paramsData, kunik);
        swiperObj.initSwiper('.elem-stdr-content-'+kunik ,paramsData["swiper"])
    },
    cssCarousel: function(paramsData, kunik) {
        $(`head #carousel-${kunik}` ).remove();
        var containerKunik = ".container-" + kunik;
        var css = `<style id="carousel-${kunik}" >`;

        // no corousel
        if (exists(paramsData["displayConfig"]["carousel"]) && paramsData["displayConfig"]["carousel"] == false){
            css += `${containerKunik} .swiper-slide {
                            display: block;
                       }`;
            css += `.elem-stdr-content-${kunik} .swiper-pagination,
                          .elem-stdr-content-${kunik} .swiper-button-next,
                          .elem-stdr-content-${kunik} .swiper-button-prev{
                              display:none
                          }

                          .elem-stdr-content-${kunik} .swiper-wrapper{
                            flex-wrap : wrap;
                            justify-content: space-evenly;
                          }
                          .elem-stdr-content-${kunik} .swiper-slide{
                            margin-right: 0px !important;
                          }
                          @media (max-width:400px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["xs"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["xs"] : 1}) !important;
                            }
                          }
                          @media (max-width:768px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["sm"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["sm"] : 2}) !important;
                            }
                          }
                          @media (min-width:769px){
                            .elem-stdr-content-${kunik} .swiper-slide{
                                flex-basis : calc(95%/ ${exists(paramsData["swiper"]["breakpoints"]["slidesPerView"]["md"]) ? paramsData["swiper"]["breakpoints"]["slidesPerView"]["md"] : 3}) !important;
                            }
                          }
                  `;


        }else {
            css += `${containerKunik} .swiper-slide {
                          display: -webkit-box;
                          display: -ms-flexbox;
                          display: -webkit-flex;
                          display: flex;
                          }`;
        }
        css += "</style>";

        $('head').append(css);
    }

}