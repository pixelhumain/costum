function PubSub(){
    this.events = {}

    this.subscribe = function(eventName, handler){
        if(this.events[eventName])
            this.events[eventName].push(handler)
        else
            this.events[eventName] = [handler]

        return { eventName, handler }
    }

    this.unsubscribe = function(subscriber){
        if(this.events[subscriber.eventName]){
            this.events[eventName] = this.events[eventName].filter((handler) => handler !== subscriber.func);
        }
    }

    this.publish = function(eventName, payload){
        var handlers = this.events[eventName] || []
        handlers.forEach(function(handler){
            handler.apply(null, [payload])
        })
    }
}

var costumizerV2 = {
    _pubsub:new PubSub(),
    actions: {
        EDIT_COSTUM_PAGE: "edit_costum_page",
        EDIT_SITEMAP: "edit_sitemap",
        EDIT_COSTUM_DESIGN: "edit_costum_design",
        EDIT_COSTUM_CSS: "edit_costum_css",
        EDIT_COSTUM_ADMIN: "edit_costum_admin",
        EDIT_COSTUM_SETTING: "edit_costum_setting",

        CHANGE_SCREEN_MODE:"change_screen_mode",
        OPEN_PREVIEW:"open_preview",
        OPEN_TEMPLATE:"open_template",
        SELECT_TEMPLATE:"select_template",

        ADD_BLOCK:"add_block",
        MOVE_BLOCK_TO_TOP:"move_block_to_top",
        MOVE_BLOCK_TO_BOTTOM:"move_block_to_bottom",
        EDIT_BLOCK:"edit_block",
        DUPLICATE_BLOCK:"duplicate_block",
        DELETE_BLOCK:"delete_block"
    },
    on(action, handler){
        this._pubsub.subscribe(action, handler)
    },
    do(action, payload=null){
        this._pubsub.publish(action, payload)
    },
    events:{
        initUIEvents:function(){
            $(".btn-toggle-cmsbuilder-sidepanel").click(function() {
                $(this).toggleClass("arrow-inverse")
                $(`.cmsbuilder-${$(this).data("target")}-content`).toggleClass("active")
            })
    
            $(".cmsbuilder-left-content-header ul li").click(function() {
                $(".cmsbuilder-left-content-header ul li").removeClass("active")
                $(this).addClass("active")
            })
    
            $(".cmsbuilder-right-content-header ul li").click(function() {
                $(".cmsbuilder-right-content-header ul li").removeClass("active")
                $(this).addClass("active")
            })
        },
        initBlockEvents:function(){
            var self = costumizerV2;

            //block actions events
            $(".cmsbuilder-block-action-list li").off("click").click(function(e){
                e.stopPropagation();

                self.do($(this).data("action"),{
                    name:$(this).data("name"),
                    id:$(this).data("id"),
                    path:$(this).data("path")
                })
            })

            //toggle show block actions on hover
            $(".block-cms-wrapper").off("mousemove").mousemove(function(e) {
                e.stopPropagation()
                $(".cmsbuilder-block-action-element").removeClass("active")
                $(this).find("> .cmsbuilder-block-action-element").addClass("active")
            }).off("mouseleave").mouseleave(function(e) {
                e.stopPropagation()
                $(".cmsbuilder-block-action-element").removeClass("active")
            })

            //make the block droppable
            $(".block-cms-section").droppable({
                greedy:true,
                hoverClass:"draghover",
                over:function(){
                    var $highlight = $(`
                        <div class="block-cms-section-highlight">
                            <span><i class="fa fa-plus-square-o" aria-hidden="true"></i></span><span>Ajouter le bloc ici</span>
                        </div>
                    `)
                    $(this).append($highlight);
                },
                out:function(){
                    $(".block-cms-section-highlight").remove()
                },
                drop:function(_, ui){
                    $(".block-cms-section-highlight").remove()

                    var id = $(this).data("id");
                    self.do(self.actions.ADD_BLOCK, {
                        block:$(ui.draggable).data("name"),
                        target:{
                            type:"block",
                            position:"in",
                            id:id
                        }
                    })
                }
            })
        },
        initPageEvents:function(){
            
            //make the page droppable
            $(".pageContent").droppable({
                greedy:true,
                drop:function(_, ui){
                    self.do(self.actions.ADD_BLOCK, {
                        block:$(ui.draggable).data("name"),
                        target:{
                            type:"page"
                        }
                    })
                }
            })
        },
        initToolbarEvents:function(){
            var self = costumizerV2;

            //open dropdown on click
            $(".cmsbuilder-toolbar-item").click(function(e) {
                e.stopPropagation()
                var hasClass = $(this).find(".cmsbuilder-toolbar-dropdown").hasClass("active")
                $(".cmsbuilder-toolbar-dropdown").removeClass("active")
                if(!hasClass)
                    $(this).find(".cmsbuilder-toolbar-dropdown").addClass("active")
            })
            //handle click dropdown item
            $(".cmsbuilder-toolbar-dropdown li").off("click").click(function(){
                self.do($(this).data("action"))
            })
            //close dropdown on click outside
            $(document).click(function(){
                $(".cmsbuilder-toolbar-dropdown").removeClass("active")
            })
    
            $(".btn-costumizer-viewMode").off("click").click(function(){
                $(".btn-costumizer-viewMode").removeClass("active")
                $(this).addClass("active")
                self.do(self.actions.CHANGE_SCREEN_MODE, { mode:$(this).data("mode") })
            })
    
            $("#cmsBuilder-right-actions a").off("click").click(function(){
                self.do($(this).data("action"))
            })
        },
        initBlockComponentEvents:function(){
            var self = costumizerV2;

            //make draggable block component
            $(".cmsbuilder-block-list-item").draggable({
                containment:"document",
                scroll:true,
                zIndex:999999999,
                helper:"clone",
                start:function(){
                    var addHightLight = function($el, position){
                        var id = $el.data("id")
                        var $highlight = $(`
                            <div class="col-sm-12 block-droppable-highlight">
                                <span><i class="fa fa-plus-square-o" aria-hidden="true"></i></span>
                                <span>Ajouter le bloc ici</span>
                            </div>
                        `)

                        $highlight.droppable({
                            greedy:true,
                            hoverClass:"draghover",
                            drop:function(_, ui){
                                self.do(self.actions.ADD_BLOCK, {
                                    block:$(ui.draggable).data("name"),
                                    target:{
                                        type:"block",
                                        position:position,
                                        id:id
                                    }
                                })
                            }
                        })

                        if(position === "before")
                            $highlight.insertBefore($el)
                        else if(position === "after")
                            $highlight.insertAfter($el)
                    }

                    $(".block-cms-section").each(function(index){
                        addHightLight($(this), "before")
                        if((index+1) === $(".block-cms-section").length)
                            addHightLight($(this), "after")
                    })
                },
                stop:function(){
                    $(".block-droppable-highlight").remove();
                }
            })
        },
        init(){
            this.initUIEvents()
            this.initBlockEvents()
            this.initPageEvents()
            this.initToolbarEvents()
            this.initBlockComponentEvents()
        }
    },
    init(){
        this.events.init()
    }
}