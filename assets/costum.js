costum.init = function(where){
    if(typeof costum.filters != "undefined"){
        if(typeof costum.filters.scopes != "undefined"){
            if(typeof costum.filters.scopes.cities != "undefined"){
                setOpenBreadCrum({'cities': costum.filters.scopes.cities }, true, null, "costum");
            }
            if(typeof costum.filters.scopes.zones != "undefined"){
                setOpenBreadCrum({'zones': costum.filters.scopes.zones}, true, null, "costum");
            }
        
        }
        if(typeof costum.filters.sourceKey != "undefined"){
            searchObject.sourceKey=costum.request.sourceKey;
        }
    }
    // if(typeof costum.js != "undefined" && typeof costum.js.urls != "undefined"){
    //     $.each(costum.js.urls, function(e,v){
    //         lazyLoad(assetPath+"/js/"+costum.slug+"/"+v, null, function(){
                if( typeof costum[costum.slug] != "undefined" && 
                    typeof costum[costum.slug].init != "undefined" && 
                    typeof costum[costum.slug].init == "function")
                    costum[costum.slug].init(); 
    //         });
    //     });
    // }
    
    if(typeof costum != "undefined"){
        costum.initCssCustom();
        costum.addClassSection ();
    }
    if(typeof costum.typeObj != "undefined")
        costum.initTypeObj(costum.typeObj);
    /*if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.indexStep != "undefined") 
        searchObject.indexStep=costum.searchObject.indexStep;
    if(typeof costum != "undefined" && typeof costum.searchObject != "undefined" && typeof costum.searchObject.sortBy != "undefined")
        searchObject.sortBy=costum.searchObject.sortBy;*/
    // mylog.log("directoryViewMode 1", directoryViewMode);
    // if(typeof costum.htmlConstruct != "undefined" 
    //     && typeof costum.htmlConstruct.directory != "undefined"
    //     && typeof costum.htmlConstruct.directory.viewMode != "undefined"){
    //     mylog.log("directoryViewMode 2", directoryViewMode);
    //     directoryViewMode=costum.htmlConstruct.directory.viewMode;
    // }
    if(typeof costum.htmlConstruct != "undefined"
        && typeof costum.htmlConstruct.directoryViewMode != "undefined"){
        directoryViewMode=costum.htmlConstruct.directoryViewMode;
        directory.viewMode = directoryViewMode;
    }
    if(typeof costum.htmlConstruct.header != "undefined" && typeof costum.htmlConstruct.header.menuTop != "undefined") {
        costum.htmlConstruct.header.menuTop=jQuery.extend(true,{},themeParams.header.menuTop);
        // change all values btn menu app object to boolean
        var menuTopPosition = ["left","center","right"];
        $.each(menuTopPosition, function(key, value) {
            if (typeof costum.htmlConstruct.header.menuTop[value] != "undefined" && typeof costum.htmlConstruct.header.menuTop[value]["buttonList"] != "undefined" && typeof costum.htmlConstruct.header.menuTop[value]["buttonList"]["app"] != "undefined") {
                if (typeof costum.htmlConstruct.header.menuTop[value]["buttonList"]["app"]["buttonList"] != "undefined") {
                    costum.changeAllValueAppObjectToBoolean(costum.htmlConstruct.header.menuTop[value]["buttonList"]["app"]["buttonList"]);
                } 
            }
        })
    }   
    
    var link = window.location.href;
    var edit = "?edit=true";
    if( (userId == "" ||  typeof userId == "undefined") && (link.includes(edit) == true || link.includes("/edit/true") == true)){
		toastr.error("<p>"+tradCms.adminArea+"</p>");
		Login.openLogin();
	} else if ( costum.editMode == false && costum.isCostumAdmin && (link.includes(edit) == false || link.includes("/edit/true") == false)) {
        var btnEdit =
            //'<li role="separator" class="divider"></li>' +
            '<li class="menu-button btn-menu btn-menu-tooltips menu-btn-top">' +
            '<a href="javascript:;" class="modeEditBtn cosDyn-buttonList">' +
            '<i class="fa fa-pencil"></i> '+trad.edit +
            '</a>' +
            '</li>';
        if ($("#menuTopRight .dropdown-menu-top ul.dropdown-menu").length) {
            $('#menuTopRight .dropdown-menu-top ul.dropdown-menu').prepend(btnEdit);
        }else {
            $('#menuTopRight').prepend(`<a href="javascript:;" class="modeEditBtn cosDyn-buttonList btn btn-default"><i class="fa fa-pencil"></i> ${trad.edit}</a>`)
        }

    }
    $('.modeEditBtn').click(function (){
        var urlCostum = link+edit;
        if(link.includes('#')) {
            urlCostum = link.split('#').shift()+edit+location.hash;
        }
        window.open(urlCostum, '_blank');
    });

    // initialize cssFile
    $.each(["#menuTopLeft","#menuTopCenter","#menuTopRight"],function(e,dom){
        if(!notEmpty($(dom).html()))
            $(dom).hide();
    });
    var strCss = costum.getCss();
    str="<style id='cssFile' type='text/css'>";
    if(notEmpty(strCss))
        str+= strCss;
        str+="</style>"; 

    $("head").append(str);
    if(typeof getRolesList == "function")
        getRolesList(costum.contextType,costum.contextId);
   
};
costum.initTypeObj=function(typeObjCostum){
    mylog.log("costum.initTypeObj", typeObjCostum);
    $.each(typeObj, function(e, obj){
        var entryObj=e;
        if(typeof obj.sameAs != "undefined")
                entryObj=obj.sameAs;
        if(typeof typeObjCostum[e] != "undefined"){
            if(typeof typeObjCostum[e] == "object"){
                $.each(typeObjCostum[e], function(entry, value){
                    typeObj[entryObj][entry]=value;
                });
            }
        }else if(typeof obj.add != "undefined" && obj.add){
            typeObj[entryObj].add=false;
        }
    });
    $.each(typeObjCostum, function(e, v){
        if(typeof typeObj[e]=="undefined")
            typeObj[e]=v;
    });
    mylog.log("initTypeObj typeObj", typeObj);
    typeObj.buildCreateButton(".toolbar-bottom-adds", true, null, costum);
    coInterface.bindButtonOpenForm();
};
costum.styleProperties={
    "background": "background",
    "backgroundColor" : "background-color",
    "backgroundSize" : "background-size",
    "border": "border",
    "borderBottom": "border-bottom",
    "borderLeft": "border-left",
    "borderRight": "border-right",
    "borderTop": "border-top",
    "borderWidth": "border-width",
    "borderColor": "border-color",
    "borderRadius": "border-radius",
    "borderBottomLeftRadius": "border-bottom-left-radius",
    "borderBottomRightRadius" : "border-bottom-right-radius",
    "borderTopLeftRadius" : "border-top-left-radius",
    "borderTopRightRadius" : "border-top-right-radius",
    "box": "box-shadow",
    "boxShadow": "box-shadow",
    "fontSize": "font-size",
    "fontWeight": "font-weight",
    "fontFamily" : "font-family",
    "color": "color",
    "padding": "padding",
    "paddingTop": "padding-top",
    "paddingBottom": "padding-bottom",
    "paddingLeft": "padding-left",
    "paddingRight": "padding-right",
    "margin": "margin",
    "marginTop": "margin-top",
    "marginBottom": "margin-bottom",
    "marginLeft": "margin-left",
    "marginRight": "margin-right",
    "height": "height",
    "width": "width",
    "top": "top",
    "bottom": "bottom",
    "right": "right",
    "left": "left",
    "lineHeight": "line-height",
    "display": "display",
    "textTransform": "text-transform",
    "textDecoration": "text-decoration",
    "textAlign": "text-align",
    "position": "position",
    "verticalAlign":"vertical-align",
    "float": "float",
    "clipPath":"clip-path",
    "justifyContent":"justify-content",
    "alignItems":"align-items",
};
costum.cssStyleWithPx=[
    "fontSize",
    "width", 
    "height", 
    "borderWidth", 
    "borderRadius",
    "borderBottomLeftRadius",
    "borderBottomRightRadius",
    "borderTopLeftRadius",
    "borderTopRightRadius",
    "padding", 
    "paddingTop", 
    "paddingBottom", 
    "paddingLeft", 
    "paddingRight",
    "margin", 
    "marginTop", 
    "marginBottom", 
    "marginLeft", 
    "marginRight", 
    "top",
    "left",
    "right", 
    "left"
];
costum.getSpecPx=function(k, v){
    if($.inArray(k, costum.cssStyleWithPx) >=0){
        if(typeof v == "number"){
            // alert("")
            return "px";
        }
        else if(v == "auto")
            return "";
        else if (v.toString().indexOf("%") >=0){
            //alert("ici%");
            return "";
        }
        else if(v.toString().indexOf("px")>=0){
            //alert("icipx");
            return "";
        }
        else
            return "px";
    }else
        return "";
};
costum.getStyleCustom=function(cssObj,cssPath){
    var cssAdd="";
    $.each(costum.styleProperties, function(e, v){
        if(jsonHelper.notNull(cssPath+"."+e))
            cssAdd+=v+":"+costum.value(cssObj,cssPath+"."+e)+costum.getSpecPx(e, costum.value(cssObj,cssPath+"."+e))+";";
    
    });
   
    return cssAdd;
};
costum.getUpdateClass=function(selectClass,column,newClass){
    var classDiv = $(selectClass).attr("class");
    mylog.log("classDiv",classDiv);
    if(newClass == "col-"+column+"-")
        newClass = "";
    if (typeof classDiv != "undefined") {
        for (let index = 1; index <= 12; index++) {
            if (classDiv.indexOf("col-"+column+"-"+index)!==-1){
                classDiv=classDiv.replace("col-"+column+"-"+index,"");
                mylog.log("classDiv2",classDiv);
            }
        }
    }
    //$(selectClass).removeAttr("class");
    $(selectClass).attr("class",(classDiv+" "+newClass).replace(/\s+/g, ' '));
};
costum.getClassCustom=function(classSection,cssObj){
    var md = jsonHelper.getValueByPath(cssObj,"columnWidth.colMd");
    var sm = jsonHelper.getValueByPath(cssObj,"columnWidth.colSm");
    var xs = jsonHelper.getValueByPath(cssObj,"columnWidth.colXs");
    if(typeof md != "" && typeof md != "undefined"){
        costum.getUpdateClass(classSection,"md","col-md-"+md)
    }
    if(typeof sm != "" && typeof sm != "undefined"){
        costum.getUpdateClass(classSection,"sm","col-sm-"+sm)
    }
    if(typeof xs != "" && typeof xs != "undefined") {
        costum.getUpdateClass(classSection,"xs","col-xs-"+xs)
    }
};

costum.getCss = function(){

                var result = new Array();
                extraParamsAjax={
                    async: false
                };

                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cssFile/getbycostumid",
                    {id: costum.contextId},
                    function(data){ 
                        $.each(data, function(key,value) {
                            if (notEmpty(value.css)){
                                result.push({"css" : value.css})
                            }
                         });
                      },
                    null,
                    "json",
                    extraParamsAjax
                    );
                
				mylog.log("cssFile costum");
                if (result.length > 0){
                    return result[0]["css"];
                }
                else{
                    return null;
                }

};


costum.initCssCustom=function(cssObj,path){
    var cssObj = (notNull(cssObj)) ? cssObj : costum.css;
    var style = (notNull(path)) ? path : "costum.css";
    // if(jsonHelper.notNull(style+".urls")  && costum.css.urls.length >= 1){
    //     $.each(costum.css.urls, function(e,v){
    //         lazyLoad(null, assetPath+"/css/"+costum.slug+"/"+v, null);
    //     });
    // }
    var str="";
    var idCSS="amazing-costumizer-css";
    var removeDomCss="";
    if($("#amazing-costumizer-css").length > 0){
        idCSS="amazing-costumizer-css-new";
        removeDomCss="#amazing-costumizer-css";
    }
    else if($("#amazing-costumizer-css-new").length > 0)
        removeDomCss="#amazing-costumizer-css-new";

    if($("#iframe").contents().find("#amazing-costumizer-css").length > 0){
        $("#iframe").contents().find("#amazing-costumizer-css").remove();
    }

    if(jsonHelper.notNull(style+".font") ){
        let urlString = "";
        if(exists(cssObj.font.useUploader) && cssObj.font.useUploader == true){
            urlString = uploadedFont.replaceAll('/', "");
            str+="@font-face {font-family: 'customFont"+urlString.replaceAll(" ","")+"';src: url('"+uploadedFont+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont"+urlString.replaceAll(" ","")+"'}";
        } else if(exists(cssObj.font.useUploader) && cssObj.font.useUploader == false){
            urlString = cssObj.font.url.replaceAll('/', "");
            str+="@font-face {font-family: 'customFont"+urlString.replaceAll(" ","")+"';src: url('"+assetPath+"/"+cssObj.font.url+"')}";
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'customFont"+urlString.replaceAll(" ","")+"'}";            
        } else {
            str+="body, h1, h2, h3, h4, h5, h6, button, input, select, textarea, a, p, span{font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;}";
        }
    }

    if (exists(cssObj.color)) {
        // str += ".pageContent button, a.superButton {";
        // if (typeof cssObj.color.color1 != "undefined")
        //     str += "background-color: " + cssObj.color.color1 + ";";
        // if (typeof cssObj.color.color2 != "undefined")
        //     str += "color: " + cssObj.color.color2 + ";";
        // if (typeof cssObj.color.color3 != "undefined")
        //     str += "border: 1px solid " + cssObj.color.color3 + ";";
        // str += "}";

        if (typeof cssObj.color.color1 != "undefined") {
            str += ".pageContent h1 {";
                str += "color: " + cssObj.color.color1 + ";";
            str += "}";
        }
        if (typeof cssObj.color.color2 != "undefined") {
            str += ".pageContent h2, .pageContent a:not(.searchEntityContainer a, .btnSlide) {";
                str += "color: " + cssObj.color.color2 + ";";
            str += "}";
        }
        if (typeof cssObj.color.color3 != "undefined") {
            str += ".pageContent h3 {";
                str += "color: " + cssObj.color.color3 + ";";
            str += "}";
        }
    }

    if (exists(cssObj.title)) {
        $.each(cssObj.title, function (balise, val) {
            str += ".pageContent "+ balise + ":not(.pageContent .searchEntityContainer "+ balise +") {";
            $.each(val, function (property, value) {
                if (property == "fontFamily")
                    value = `'${value}'`;
                if (costum.styleProperties[property] != "undefined")
                    str += costum.styleProperties[property] + ":" + value + ";";
            });
            str += "}";
        });
    }

    if (exists(cssObj.text)) {
        str += `.pageContent span:not(.cmsbuilder-block-action-list span, .sp-text span, .searchEntityContainer span, .social-main-container span), 
                .pageContent p, .pageContent textarea, .pageContent select, .pageContent input, .pageContent label, .pageContent b {`;
        $.each(cssObj.text, function (key, val) {
            if (key == "fontFamily")
                val = `'${val}'`;
            if (costum.styleProperties[key] != "undefined")
                str += costum.styleProperties[key] + ":" + val + ";";
        });
        str += "}";
    }

    if (exists(cssObj.link)) {
        str += ".pageContent a:not(.searchEntityContainer a, .social-main-container a){";
        $.each(cssObj.link, function (key, val) {
            if (key == "fontFamily")
                val = `'${val}'`;
            if (costum.styleProperties[key] != "undefined")
                str += costum.styleProperties[key] + ":" + val + ";";
        });
        str += "}";
    }

    if(jsonHelper.notNull(style+".fonts") ){

        $.each(cssObj.fonts, function(kF,vF){
            str+="@font-face { font-family: '"+vF.name+"';src: url('"+assetPath+"/"+vF.url+"')}";
        });
    }

    if(jsonHelper.notNull(style+".styles") ){
        $.each(cssObj.styles, function(e,r){
           if(e.indexOf("#"))
            str += e+"{";
           $.each(r, function(k,c){
            str += k+":"+c;
           });
           str += "}"; 
        });

    }

    if(jsonHelper.notNull(style+".progress") ){
        if(jsonHelper.notNull(style+".progress.bar") )
            str+=".progressTop::-webkit-progress-bar { "+costum.getStyleCustom(cssObj, style+".progress.bar")+" }";
        if(jsonHelper.notNull(style+".progress.value") )
            str+=".progressTop::-webkit-progress-value{"+costum.getStyleCustom(cssObj, style+".progress.value")+"}.progressTop::-moz-progress-bar{"+costum.getStyleCustom(cssObj, style+".progress.value")+"}";
    }
   

    if(typeof cssObj.color != "undefined" ){
       // colorCSS=["purple", "orange", "blue", "red", "black", "green", "green-k", "yellow", "yellow-k", "vine", "brown","main1","main2"];
        $.each(cssObj.color, function(e,v){
           // if(typeof cssObj.color[v] != "undefined" ){
                str+=".text-"+e+" { color:"+v+" !important;}"+
                    ".border-"+e+" { border-color:"+v+" !important;}"+
                     " .bg-"+e+" { background-color:"+v+" !important;}";
            //}
        });
    }
    str+="@media (max-width: 767px){"+
            "#mainNav{margin-top:0px !important;}";
            str+="#mainNav .menu-btn-top{font-size:22px !important;}";
            str+=".aap-organism-chooser{display:none !important}"
            if( jsonHelper.notNull(style+".menuTop") &&
                jsonHelper.notNull(style+".menuTop.responsive") &&
                jsonHelper.notNull(style+".menuTop.responsive.connectBtn")){
                str+= "#mainNav .btn-menu-connect{"+costum.getStyleCustom(cssObj, style+".menuTop.responsive.connectBtn")+"}";
            }
            
        str+="#mainNav .logo-menutop{height:40px;}"+
        "}";
    str+=costum.generalCssStyle(cssObj);
   // if($("#amazing-costumizer-css").length <= 0)
     //   str+="</style>";
    //if($("#amazing-costumizer-css").length <= 0)
    $("head").append("<style type='text/css' id='"+idCSS+"'>"+str+"</style>");
    $("#iframe").contents().find("head").append("<style type='text/css' id='"+idCSS+"'>"+str+"</style>");
    if(notEmpty(removeDomCss))
        setTimeout(function(){$(removeDomCss).remove()},200);
    //else
      //  $("#amazing-costumizer-css").html(str);
    $("#menuLeft .menu-name-profil").removeClass("pull-right");
    $("#menuLeft .image-menu ").removeClass("pull-left");
    if(jsonHelper.notNull("costum.htmlConstruct.header.menuTop.left") && costum.htmlConstruct.header.menuTop.left == false)
        $("#mainNav").removeClass("padding-top-5 padding-bottom-5");
    if(jsonHelper.notNull("costum.htmlConstruct.header.menuTop.left.addClass") && costum.htmlConstruct.header.menuTop.left.addClass.indexOf("visible-xs") != -1){
        $("#mainNav").removeClass("padding-top-5 padding-bottom-5");
    }
};
costum.rejectCssProperties=["urls", "font"];
costum.getCssInCascade=function(obj, objClass, path){
    $.each(obj,function(e,v){
        if($.inArray(e, costum.rejectCssProperties) < 0 ){
            //var classCss=classTreeCss;

             //   classCss=(notEmpty(classCss)) ? classCss+" .cosDyn-"+e : ".cosDyn-"+e;
            if(typeof costum.styleProperties[e]== "undefined" || typeof v == "object"){
                //var keypath=path;
                keypath=(notEmpty(path)) ? path+"."+e : e;
                objClass[keypath]=[];
            }
            if(typeof costum.styleProperties[e]!= "undefined" && typeof v != "object"){
            
                objClass[path].push(costum.styleProperties[e]+":"+v+costum.getSpecPx(e, v)+"!important;");
            }
            else if(typeof v == "object"){
                //if($.inArray(e, ["hover", "active", "focus"]) >=0) classCss=classCss.replace(" ."+e, ":"+e);
                objClass=costum.getCssInCascade(v, objClass, keypath);
            }
            //".costumizer-css-menuTop > .costumizer-css-menuTopLeft > .costumizer-css-app
        }
    });
    return objClass;
}

costum.addClassSection = function (ObjClass,srcPath){
    var clsObj = (notNull(ObjClass)) ? ObjClass : costum.css;
    var classPath = (notNull(srcPath)) ? srcPath : "css";
    mylog.log ("srcPath",srcPath);
    $.each(clsObj,function(e,v) {
        if ($.inArray(e, ["hideOnDesktop", "hideOnTablet", "hideOnMobil", "columnWidth"]) >= 0) {
            var newPath = classPath.replace('css.', '');
            var tabClass=newPath.split(".");
            var arrayTab = tabClass.map(function (params) {
                return ".cosDyn-"+params
            });
            var classList = arrayTab.join(" ");
            if (e == "columnWidth") {
                costum.getClassCustom(classList, clsObj);
            } else {
                if (v == true)
                    $(classList).addClass(cssHelpers.json[e].classValue);
                else
                    $(classList).removeClass(cssHelpers.json[e].classValue);
            }
        } else if (typeof v == "object") {
            //alert(classPath+"."+e);
            callObjClass = costum.addClassSection(v, classPath+"."+e);
        }
    });
}
costum.generalCssStyle=function(obj){
    var str="";
    var objClass=costum.getCssInCascade(obj, {});
    
    $.each(objClass, function(classes,style){
        tabClass=classes.split(".");
        classStr="";
        $.each(tabClass,function(i,classTarget){
            if($.inArray(classTarget, ["hover", "active", "focus"]) >=0) 
                classStr+=":"+classTarget;
            else if ($.inArray(classTarget, ["icon"]) >=0)
                classStr+=" i";
            else
                classStr+=" .cosDyn-"+classTarget;
        });

        if(notEmpty(style))
            str+=classStr+"{"+style.join('')+"}";
    });
    return str;
};
costum.value = function(obj,path){
    //mylog.log("costum.value ",path);
    if( jsonHelper.notNull(path) ){
        //mylog.log("costum.value ",path);
        if( path.indexOf("this.") > -1 ){
            pieces = path.split('.');
            return jsonHelper.eval(obj,pieces[1]);
        } else {
            path=path.split("css.")[1];
           // path=path.replace("costum.","");
            return jsonHelper.getValueByPath(obj,path);
        }
    }
    else 
        return null;
};

costum.resetCache = function(){
    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/cms/resetcache",
        null,
        function(data){},
        null
        );
}

costum.updateFontsCache = function(fontsValue){
    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/cms/updatecachefonts",
        {
            fonts: fontsValue
        },
        function(data){},
        null
    );
}

costum.getCtxCMS = function(ctxPage=null,withChildren=false) {
    return new Promise(function(resolve, reject) {
        var params = {};
        if (typeof ctxPage != "undefined") {
            params.page = ctxPage;
        }
        if(withChildren)
            params.withChildren = true;
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/cms/getallcms",
            params,
            function(data){
                resolve(data);
            },
            function(error){
                reject(error);
            },
            null,
            {
                async: true
            }
        );
    });
}
costum.changeAllValueAppObjectToBoolean = function(btnListApp) {
    $.each(btnListApp, function(key, value) {
        if (key.charAt(0) == "#") {
            if (typeof value == "object") {
                if (typeof value["buttonList"] != "undefined") {
                    costum.changeAllValueAppObjectToBoolean(value["buttonList"]);
                    btnListApp[key] = {"buttonList" : value["buttonList"]};
                } else {
                    btnListApp[key] = true;
                }
            }
        }
    })
}

costum.getCostumOptions = function(){
    var result = {};
    extraParamsAjax={
        async: false
    };

    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/cms/getvaluebyidwithfields",
        {id: costum.contextId , collection : costum.contextType , fields : ["costum"]},
        function(response){ 
            if ( response.result ) { 
                if ( typeof response.data.costum !== "undefined" && typeof response.data.costum.options !== "undefined"){
                    result = response.data.costum.options;
                }
            }
        },
        null,
        "json",
        extraParamsAjax
    );

    return result;
}

costum.costumOptions = costum.getCostumOptions();

//costum.init();

/*
template(col) : {
    id : 908478427428722,
    name : artiste,
    type : costum,
    params : {
        etct
    },
    costumUsingTpls : {
        "987876868767"
        "987876868767"
        "987876868767"
        "987876868767"
        "987876868767",
        etc autant d'id que de personne qui utilisent un template
    }
} 

ou stocker dans le duplicate de tpl 
== costum : {
    id: 66476476457625462,
    name : 'je suis utilisateur du costum template',
    params: {
        blablablabla
    },
    htmlConstruct: {
        etc 
    }
    tplsOrigin : {id tpl artiste}
}
*/