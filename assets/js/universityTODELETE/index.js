function checkThis(){
	//Check if some input is empty
	var result = $("#formQuest input").filter(function () {
		return $.trim($(this).val()).length == 0
	}).length == 0;
	if (result !== 0) {
		addThisCitoyens()
	}
}
$(".coFormbody input").off().on('click',function(){
  	$('#formQuest').each(function(){
  		if($(this).find('.addToMember').length == 0){
  			$(this).append(`
  		<div class="col-md-12 padding-20 addToMember">
  		<div class="text-center">
  		<button onclick="checkThis();" class="btn btn-default text-bold letter-green">Valider <i class="fa fa-arrow-circle-right"></i></button>
  		</div>
  		</div>
  		`);
  		}
  	});
  });

/************Add citizen to MemberOf after answering************/
	function addThisCitoyens() {
	/*	if (isInterfaceAdmin === false) {*/
		tplCtx.id = userId;
		tplCtx.collection = "citoyens";
		tplCtx.path = "links.memberOf."+costum.contextId;
		tplCtx.value = {};
		tplCtx.value.type = costum.contextType;  
		tplCtx.value.roles = ["Partenaire"]
		dataHelper.path2Value( tplCtx, function(params) {
			dyFObj.commonAfterSave(params,function(){
				/*toastr.success("Élément bien ajouter");*/
			});
		} );
		addToMemberOf();
/*		}*/
	}

	function addToMemberOf() {
		tplCtx.id = costum.contextId;
		tplCtx.collection = "organizations";
		tplCtx.path = "links.members."+userId;
		tplCtx.value = {};
		tplCtx.value.type = "citoyens";
		tplCtx.value.roles = ["Partenaire"],
		dataHelper.path2Value( tplCtx, function(params) {
			dyFObj.commonAfterSave(params,function(){				
				$(".addToMember").remove();
			});
		} );
	}
/************end add citizen to MemberOf after answering************/

;
/*$("#coFormbody input[required]").filter(function () {
    return $.trim($(this).val()).length == 0
}).length == 0;*/

$(".coFormbody").removeClass("col-xs-12");
$(".coFormbody").addClass("container");
/*function alertss() {
alert("worls");
}*/

$(".btn-open-form, .generateUniv").off().on('click',function(){
	dyFObj.openForm('organization');
	setTimeout(function() {

		$(".typehidden").hide();

	}, 800);
});

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["name"] = {
	"inputType" : "text",
	"label" : "Nom de votre université"
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["type"] = {
	"inputType" : "hidden",
	"class" : "hidden",
	"value" : "Group"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["univ"] =  {
	"inputType" : "select",
	"label" : "Cours",
	"order" : "3",
	"placeholder" : "Séléctionner",
	"class" : "form-control",
	"labelInformation" : "test",
	"options" : {
		"online" : "En ligne",
		"classroom" : "En salle",
		"mixte" : "En alternance"
	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["thematic"] =  {
	"inputType" : "selectMultiple",
	"label" : "Differents filières",
	"order" : "4",
	"placeholder" : "Séléctionner",
	"class" : "form-control",
	"labelInformation" : "test",
	"options" : {
		"Administration" : "Administration",
		"Droit" : "Droit",
		"Teaching" : "Enseignement",
		"Civil engineering" : "Génie civil",
		"International" : "International",
		"Letters-Languages" : "Lettres-Langues",
		"Communication" : "Communication",
		"Mathematics" : "Mathématique",
		"Mechanics-Materials" : "Mécanique-Matériaux",
		"Digital-Electronic" : "Numérique-Électronique",
		"Physics-Chemistry" : "Physique-Chimie",
		"Logistics-Maintenance" : "Logistique-Maintenance",
		"Health" : "Santé",
		"Human and Social Sciences" : "Sciences Humaines et Sociales",
		"Earth and Universe Sciences" : "Sciences de la Terre et de l'Univers",
		"Life Sciences" : "Sciences de la Vie",
		"Sports sciences" : "Sciences du sport",
		"Other" : "Autre"
	}
}


costum[costum.slug] = {
	/*init : function(){
		coInterface.bindButtonOpenForm = function(){
			$(".btn-open-form, #joinBtn").off().on("click",function(){
				setTimeout(function() {
					$('.addListLineBtncertificationsAwards').text(trad.addrow);
					if (costum.slug == "ries") {                        
						$('#btn-submit-form').attr("disabled", true);
					}
					$('.extraInfo-agreecheckboxSimple').click(function(){
						if ($('#extraInfo-agree').val() == "true")
							$('#btn-submit-form').attr("disabled", false);
						else
							$('#btn-submit-form').attr("disabled", true);
					});
					$(".typeselect").hide();

				}, 800);

				var typeForm = $(this).data("form-type");

				if(typeForm =="reseau"){
					getNetwork();
				}
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				if(contextData && contextData.type && contextData.id ){

					dyFObj.openForm(typeForm);
				}
				else{
					
					dyFObj.openForm(typeForm);
				}
			});
		};    
		coInterface.bindButtonOpenForm();
	},*/

	"organizations" : {
/*		formData : function(data){

			var certifications = [];
			$.each(data, function(e, v){
				if(e.indexOf("-") != -1) {					
					ee = e.split("-").join("[")+"]";
					data[ee] = data[e];
					delete data[e]
					}

					if(typeof costum.lists[e] != "undefined"){
						if(notNull(v)){

							if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
							if(typeof v == "string")
								data.tags.push(v);
							else{
								$.each(v, function(i,tag){
									data.tags.push(tag);	
								});
							}
						}
						delete data[e];

					}				
				});
			return data;
		},*/
		afterSave : function(orga){	
			var data = {
				type : orga.map.type,
				id : orga.map._id["$id"],
				collection:"organizations",
				path : "allToRoot",
				value : {"type": orga.map.univ}
			};

			dataHelper.path2Value( data, function(params) { 
				urlCtrl.loadByHash("#@"+orga.map.slug+".view.detail");
			} );

		}
	}
}

alert("yes");