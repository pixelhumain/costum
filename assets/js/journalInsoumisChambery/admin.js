adminPanel.views.articles = function(){
	var data={
		title : "Gestion des articles",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                filters : {
                    type : "article"
                },
                types : [ "poi" ]
            },
            filters : {
                text : true
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            created: {
                name : "Créé le"
            },
            description : {
            	name : "Description",
            	class : "col-xs-2 text-center"
            }
        },

        actions : {
            update : true,
            delete : true
        }
    };
        data.csv = [{
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                    indexStep : 0,
                    fields : [
                        "name","description", "tags", "created","parent","profilMediumImageUrl","profilThumbImageUrl"
                                            ]
                }
            }];
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};
adminPanel.views.events = function(){
    var data={
        title : "Gestion des événements",
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "events" ]
            },
            filters : {
                text : true
            }
        },
        table : {
            name: {
                name : "Nom"
            },
            created: {
                name : "Créé le"
            },
            description : {
                name : "Description",
                class : "col-xs-2 text-center"
            }
        },
        actions : {
            update:true,
            delete : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};