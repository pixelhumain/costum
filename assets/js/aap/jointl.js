(function (X, $) {
	if (typeof X.join_tl !== 'function') {
		X.join_tl = function () {
			var finder_filter = {
				$or: {
					'source.keys': 'franceTierslieux',
					'reference.costum': 'franceTierslieux',
				}
			};
			finder_filter['links.members.' + userId] = {
				$exists: false
			};
			var form = {
				jsonSchema: {
					title: 'Rejoindre un tiers-lieu',
					properties: {
						tls: {
							label: 'Mes tiers-lieux',
							inputType: 'finder',
							filters: finder_filter,
							initType: ['organizations'],
							initBySearch: true,
							initMe: false,
							initContacts: false,
							initContext: false,
						},
						becomeAdmin: dyFInputs.checkboxSimple(true, 'becomeAdmin', {
							"onText": trad.yes,
							"offText": trad.no,
							"onLabel": 'Administrateur',
							"offLabel": 'Simple membre',
							"labelText": 'Demander à être admin ?'
						})
					},
					save: function (data) {
						$.each(data.tls, function (id, tl) {
							var url = baseUrl + '/co2/link/connect';
							var post = {
								childId: userId,
								childType: 'citoyens',
								parentType: 'organizations',
								parentId: id,
								connectType: data.becomeAdmin === 'true' ? 'admin' : 'member'
							};
							ajaxPost(null, url, post, null, null, 'json');
						});
						toastr.success("Votre demande est envoyée. Après confirmation d'un administrateur, vous serez considéré comme membre.")
						dyFObj.closeForm();
					}
				}
			};
			var extra_params = {
				type: 'bootbox'
			};

			dyFObj.openForm(form, null, null, null, null, extra_params);
		}
	}
})(window, jQuery);