(function () {
	function req_take_random_proposition(form, extraParams = {}) {
		var url = baseUrl + '/co2/aap/proposition/request/random';
		var post = { form: form };
		extraParams.filters ? post.filters = extraParams.filters : ""
		return new Promise(function (resolve, reject) {
			ajaxPost(null, url, post, resolve, reject);
		});
	}
	if (typeof window.campFinancDesc !== 'function')
		window.campFinancDesc = function () {
			var form;
			var self = this;
			if ((!this.block_cms || !this.block_cms.form) && (!this.aap || !this.aap.form))
				return;
			if (this.block_cms && this.block_cms.form)
				form = this.block_cms.form;
			else if(this.aap && this.aap.form)
				form = this.aap.form._id.$id;
			var filtersParams = {};
			if (this.block_cms && this.block_cms?.filtersMoreOptions?.filtersOption) {
				var parsedStr = null;
				// Remplacer les variables dynamiques formaté en ${var} par leur valeur
				parsedStr = this.block_cms.filtersMoreOptions.filtersOption.replace(/\$\{([^}]+)\}/g, (match, p1) => {
					try {
						// Évaluer l'expression à l'intérieur de ${}
						return eval(p1);
					} catch (error) {
						mylog.error("Erreur lors de l'évaluation de l'expression : ", p1, error);
						return p1;
					}
				})
				var parsedObj = null;
				try {
					parsedObj = JSON.parse(parsedStr)
				} catch (e) {
					mylog.error("the current value is not a valid json", parsedStr)
				}
				if (parsedObj) {
					const addParsedObjToFilters = (parsedObject) => {
						$.each(parsedObject, function (parsedObjKey, parsedObjVal) {
							filtersParams[parsedObjKey] = parsedObjVal;
						})
					}
					var applyFor = "all"
					if (this.block_cms.filtersMoreOptions.applyFor) {
						applyFor = this.block_cms.filtersMoreOptions.applyFor
					}
					switch (applyFor) {
						case "forAdmin":
							if (isSuperAdmin || isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						case "forUser":
							if (!isSuperAdmin && !isInterfaceAdmin) {
								addParsedObjToFilters(parsedObj)
							}
							break;
						default:
							addParsedObjToFilters(parsedObj)
							break;
					}
					
				}
			}
			req_take_random_proposition(form, {filters: filtersParams}).then(function (resp) {
				var aap_s1;
				if (!resp.success) {
					if (self.block_cms?._id) {
						$(`.cmsbuilder-block[data-id="${ self.block_cms._id.$id }"]`).hide();
					}
					return;
				}
				aap_s1 = resp.content;
				var funding = {
					total: 0,
					funded: 0
				};
				var fund_percentage = 0;
				funding = aap_s1.depense.reduce(function(prev, current) {
					return ({
						total: prev.total + current.price,
						funded: prev.funded + current.financer.reduce((a, b) => (a + b), 0)
					})
				}, funding);
				fund_percentage = parseInt((funding.funded / (funding.total > 0 ? funding.total : 1)) * 100);
				$('.campDesc-content .desc-title span').text((new DOMParser()).parseFromString(aap_s1.titre, 'text/html').documentElement.textContent);
				$('.campDesc-body img')
					.attr('src', baseUrl + aap_s1.image)
					.attr('data-src', baseUrl + aap_s1.image);
				$('.campDesc-body .tags-content').html(aap_s1.tagsbesoin.map(tag => ('<span class="tags">' + tag + '</span>')).join(''));
				$('.campDesc-body .sp-text').empty().html(aap_s1.shortDesc);
				$('.campDesc-body .progress-bar')
				.text(fund_percentage + '%')
				.css('width', fund_percentage + '%');
				$('.objectif .chiffre').empty().html(funding.total.toLocaleString() + ' <i class="fa fa-eur"></i>');
				$('.recolte .chiffre').empty().html(funding.funded.toLocaleString() + ' <i class="fa fa-eur"></i>');
				$('.campDesc-body #actionBtn'+ self.kunik).attr('href', $('.campDesc-body #actionBtn'+ self.kunik).attr('href') + aap_s1.id)
			});
		}
})();