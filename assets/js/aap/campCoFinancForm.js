(function () {
    function reqLoadForm(form, container = "") {
		var url = `${ baseUrl }/survey/answer/answer/id/new/form/${ form }/step/lesCommunsDesTierslieux1282024_157_0`;
		var post = { form: form };
		return new Promise(function (resolve, reject) {
            showLoader(container);
			ajaxPost(container, url, post, resolve, reject);
		});
	}
	if (typeof window.campCoFinancForm != 'function')
		window.campCoFinancForm = function () {
			var form;
			var self = this;
            mylog.log("check this", this)
			if (!this.block_cms || !this.block_cms.form)
				return;
            form = this.block_cms.form;
            reqLoadForm(form, "#formCampagne" + self.kunik).then(function (responses) {

            })
			
		}
})();