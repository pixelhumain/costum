var dyfCompetences = {
    beforeBuild: {
        properties: {
            name: {
                label: "Votre competences",
                placeholder: "Javascript, Yii",
                order: 4,
            },
            sectionBtn: {
                label: "Quelle categorie de competences?",
                list: {
                    backend: {
                        label: "Backend",
                        labelFront: "Backend",
                        key: "backend",
                        icon: "database",
                    },
                    frontend: {
                        label: "Frontend",
                        labelFront: "Frontend",
                        key: "frontend",
                        icon: "home",
                    },
                    devops: {
                        label: "Devops",
                        labelFront: "Devops",
                        key: "devops",
                        icon: "cloud",
                    },
                    autres: {
                        label: "Autres",
                        labelFront: "Autres",
                        key: "autres",
                        icon: "list-alt",
                    },
                },
                init: function() {
                    $(".sectionBtn")
                        .off()
                        .on("click", function(e) {
                            console.log($(this).attr("data-key"))
                            $("input#section").val($(this).attr("data-key"))
                                // $(".typeBtntagList").show();
                            $(".sectionBtn").removeClass("active btn-dark-blue text-white");
                            $("." + $(this).data("key") + "Btn").toggleClass(
                                "active btn-dark-blue text-white"
                            );
                            $("#ajaxFormModal #type").val(
                                $(this).hasClass("active") ? $(this).data("key") : ""
                            );

                            $(".breadcrumbcustom").html(
                                "<h4><a href='javascript:;'' class='btn btn-xs btn-danger'  onclick='dyFObj.elementObj.dynForm.jsonSchema.actions.clear()'>" +
                                "<i class='fa fa-times'></i></a> " +
                                $(this).data("tag") +
                                "</h4>"
                            );
                            $(".sectionBtntagList").hide();
                            $(".nametext, .categoryselect").show();
                            dyFObj.canSubmitIf();
                        });
                },
            },
            category: {
                label: "Niveau",
                inputType: "select",
                placeholder: "---- Votre niveau ----",
                options: {
                    1: "Novice",
                    2: "Amateur",
                    3: "Pro",
                    4: "Expert",
                    5: "Legende",
                },
                rules: {
                    required: true,
                },
                order: 5,
            },
        },
    },
    onload: {
        actions: {
            setTitle: "Ajouter une competence",
            src: {
                infocustom: "Remplir le champ",
            },
            required: {
                niveau: 1,
            },
            presetValue: {
                public: true,
            },
            hide: {
                categoryselect: 1,
                publiccheckboxSimple: 1,
                typeselect: 1,
                imageuploader: 1,
                recurrencycheckbox: 1,
                formLocalityformLocality: 1,
                emailtext: 1,
                tagstags: 1,
                parentfinder: 1,
                organizerfinder: 1,
                urltext: 1,
                infocustom: 1,
            },
        },
    },
};

dyfCompetences.afterSave = function(data) {
    dyFObj.commonAfterSave(data, function() {
        dyFObj.closeForm();
        refreshDendo(width, height)
    });
};

var dyfCompetencesUpdate = {
    beforeBuild: {
        properties: {
            name: {
                label: "Votre competences",
                placeholder: "Javascript, Yii",
                order: 4,
            },
            sectionBtn: {
                label: "Quelle categorie de competences?",
                list: {
                    backend: {
                        label: "Backend",
                        labelFront: "Backend",
                        key: "backend",
                        icon: "database",
                    },
                    frontend: {
                        label: "Frontend",
                        labelFront: "Frontend",
                        key: "frontend",
                        icon: "home",
                    },
                    devops: {
                        label: "Devops",
                        labelFront: "Devops",
                        key: "devops",
                        icon: "cloud",
                    },
                    autres: {
                        label: "Autres",
                        labelFront: "Autres",
                        key: "autres",
                        icon: "list-alt",
                    },
                },
                init: function(data) {
                    $(".sectionBtn")
                        .off()
                        .on("click", function(e) {
                            console.log($(this).attr("data-key"))
                            $("input#section").val($(this).attr("data-key"))
                                // $(".typeBtntagList").show();
                            $(".sectionBtn").removeClass("active btn-dark-blue text-white");
                            $("." + $(this).data("key") + "Btn").toggleClass(
                                "active btn-dark-blue text-white"
                            );
                            $("#ajaxFormModal #type").val(
                                $(this).hasClass("active") ? $(this).data("key") : ""
                            );
                            $(".breadcrumbcustom").html(
                                "<h4><a href='javascript:;'' class='btn btn-xs btn-danger'  onclick='dyFObj.elementObj.dynForm.jsonSchema.actions.clear()'>" +
                                "<i class='fa fa-times'></i></a> " +
                                $(this).data("tag") +
                                "</h4>"
                            );
                            $(".sectionBtntagList").hide();
                            $(".nametext, .categoryselect").show();
                            dyFObj.canSubmitIf();
                        });
                },
            },
            breadcrumb: {
                init: function(data) {
                    setTimeout(() => {
                        var breadCrumbTxt = `<h4><a href='javascript:;' class='btn btn-xs btn-danger'  onclick="dyFObj.elementObj.dynForm.jsonSchema.actions.clear(); $('.categoryselect').hide()"><i class='fa fa-times'></i></a> ${$("input#section").val()}</h4>`;
                        $(".breadcrumbcustom").html(
                            breadCrumbTxt
                        );
                        $(".categoryselect").show();
                    }, 100)
                }
            },
            category: {
                label: "Niveau",
                inputType: "select",
                placeholder: "---- Votre niveau ----",
                options: {
                    1: "Novice",
                    2: "Amateur",
                    3: "Pro",
                    4: "Expert",
                    5: "Legende",
                },
                rules: {
                    required: true,
                },
                order: 5,
                init: function() {
                    $(".categoryselect").addClass("categoryBtntagList")
                }
            },
        },
    },
    onload: {
        actions: {
            setTitle: "Ajouter une competence",
            src: {
                infocustom: "Remplir le champ",
            },
            required: {
                niveau: 1,
            },
            presetValue: {
                public: true,
            },
            hide: {
                contactInfotext: 1,
                deviseselect: 1,
                pricetext: 1,
                descriptiontextarea: 1,
                // breadcrumbcustom: 1,
                publiccheckboxSimple: 1,
                typeselect: 1,
                imageuploader: 1,
                recurrencycheckbox: 1,
                formLocalityformLocality: 1,
                emailtext: 1,
                tagstags: 1,
                parentfinder: 1,
                organizerfinder: 1,
                urltext: 1,
                infocustom: 1,
            },
        },
    },
};

dyfCompetencesUpdate.afterSave = function(data) {
    dyFObj.commonAfterSave(data, function() {
        dyFObj.closeForm();
        refreshDendo(width, height)
    });
};

function dendo(w, h, data, isAdmin = true, groupNode = null) {
    const color = d3.scaleOrdinal(d3.schemeCategory10);
    const height = h,
        width = w;

    const svg = (groupNode ?
            groupNode :
            d3.select("g#menu-competence").select("svg").select("g")
        )
        .attr("height", height)
        .attr("width", width);
    svg.selectAll("g").remove();
    const g = svg.append("g").attr("transform", "translate(60,20)");
    var experienceName = [
        "",
        "Novice 1.0",
        "Amateur 2.0",
        "Pro 3.0",
        "Expert 4.0",
        "Legende 5.0",
    ];
    var formatSkillPoints = function(d) {
        return experienceName[d % 6];
    };
    var xScale = d3.scaleLinear().domain([0, 5]).range([0, 400]);

    var xAxis = d3.axisTop().scale(xScale).ticks(5).tickFormat(formatSkillPoints);

    // Setting up a way to handle the data
    var tree = d3
        .cluster() // This D3 API method setup the Dendrogram datum position.
        .size([height, width - 520]) // Total width - bar chart width = Dendrogram chart width
        .separation(function separate(a, b) {
            return a.parent == b.parent || // 2 levels tree grouping for category
                a.parent.parent == b.parent ||
                a.parent == b.parent.parent ?
                0.4 :
                0.8;
        });

    const root = d3.hierarchy(data);
    tree(root);
    console.log(root);
    // Draw every datum a line connecting to its parent.
    var link = g
        .selectAll(".link")
        .data(root.descendants().slice(1))
        .enter()
        .append("path")
        .attr("class", "link")
        .attr("d", function(d) {
            return (
                "M" +
                d.y +
                "," +
                d.x +
                "C" +
                (d.parent.y + 100) +
                "," +
                d.x +
                " " +
                (d.parent.y + 100) +
                "," +
                d.parent.x +
                " " +
                d.parent.y +
                "," +
                d.parent.x
            );
        });

    // Setup position for every datum; Applying different css classes to parents and leafs.
    var node = g
        .selectAll(".node")
        .data(root.descendants())
        .enter()
        .append("g")
        .attr("class", function(d) {
            return "node" + (d.children ? " node--internal" : " node--leaf");
        })
        .attr("transform", function(d) {
            return "translate(" + d.y + "," + d.x + ")";
        });

    // Draw every datum a small circle.
    const circles = node.append("circle").attr("r", 4);

    if (isAdmin) {
        const root_node = node.filter((d) => d.data.id == "root");
        root_node.select("circle").attr("r", 15);
        root_node
            .append("foreignObject")
            .attr("x", -15)
            .attr("y", -15)
            .attr("width", 30)
            .attr("height", 30)
            .append("xhtml:div")
            .style("width", "100%")
            .style("height", "100%")
            .style("display", "flex")
            .style("justify-content", "center")
            .style("align-items", "center")
            .append("xhtml:i")
            .style("color", "white")
            .classed("fa fa-plus", true);
        root_node.on("click", (e) => {
            e.stopPropagation();
            dyFObj.openForm("classifieds", null, null, null, dyfCompetences);
        });
    }

    // Setup G for every leaf datum.
    var leafNodeG = g
        .selectAll(".node--leaf")
        .append("g")
        .attr("class", "node--leaf-g")
        .attr("transform", "translate(" + 8 + "," + -13 + ")");

    const foreign_node = leafNodeG
        .append("foreignObject")
        .attr("class", "shadow")
        .style("background-color", function(d, i) {
            return (d.data.color = color(i));
        })
        .style("border-radius", "3px")
        .attr("width", 2)
        .attr("height", 30);

    foreign_node
        .transition()
        .duration(800)
        .attr("width", function(d) {
            return xScale(d.data.value);
        });
    const div_rect = foreign_node
        .append("xhtml:div")
        .style("display", "flex")
        .style("justify-content", "flex-end")
        .style("align-items", "center")
        .style("padding-right", "5px")
        .style("width", "100%")
        .style("height", "100%");

    if (isAdmin) {
        const edit = div_rect.append("xhtml:i").style("color", "white").style("padding-left", "1px").style("padding-right", "1px").classed("fa fa-edit", true);
        const trash = div_rect.append("xhtml:i").style("color", "white").style("padding-left", "1px").style("padding-right", "1px").classed("fa fa-trash", true);
        edit.on('click', handleEditBtn)
        trash.on('click', handleDeleteBtn)
    }


    leafNodeG
        .append("text")
        .attr("dy", 19.5)
        .attr("x", 8)
        .style("text-anchor", "start")
        .text(function(d) {
            return d.data.name;
        });

    // Write down text for every parent datum
    var internalNode = g.selectAll(".node--internal");
    internalNode
        .append("text")
        .attr("y", -10)
        .style("text-anchor", "middle")
        .text(function(d) {
            return d.data.name;
        });

    // Attach axis on top of the first leaf datum.
    var firstEndNode = g.select(".node--leaf");
    firstEndNode
        .insert("g")
        .attr("class", "xAxis")
        .attr("transform", "translate(" + 7 + "," + -14 + ")")
        .call(xAxis);

    // tick mark for x-axis
    firstEndNode
        .insert("g")
        .attr("class", "grid")
        .attr("transform", "translate(7," + (height - 15) + ")")
        .call(
            d3
            .axisBottom()
            .scale(xScale)
            .ticks(5)
            .tickSize(-height, 0, 0)
            .tickFormat("")
        );

    // Emphasize the y-axis baseline.
    svg
        .selectAll(".grid")
        .select("line")
        .style("stroke-dasharray", "20,1")
        .style("stroke", "black");

    // The moving ball
    var ballG = svg
        .insert("g")
        .attr("class", "ballG")
        .attr("transform", "translate(" + 1100 + "," + height / 2 + ")");
    ballG
        .insert("circle")
        .attr("class", "shadow")
        .style("fill", "steelblue")
        .attr("r", 5);
    ballG.insert("text").style("text-anchor", "middle").attr("dy", 5).text("0.0");

    // Animation functions for mouse on and off events.
    d3.selectAll(".node--leaf-g")
        .on("mouseover", handleMouseOver)
        .on("mouseout", handleMouseOut);

    function handleMouseOver(event, d, i) {
        var leafG = d3.select(this);

        leafG.select("rect").attr("stroke", "#4D4D4D").attr("stroke-width", "2");

        ballG.each(console.log);
        var ballGMovement = ballG
            .transition()
            .duration(400)
            .attr(
                "transform",
                "translate(" +
                (d.y + xScale(d.data.value) + 90) +
                "," +
                (d.x + 1.5) +
                ")"
            );

        ballGMovement.select("circle").style("fill", d.data.color).attr("r", 18);

        ballGMovement
            .select("text")
            .delay(300)
            .text(Number(d.data.value).toFixed(1));
    }

    function handleMouseOut(event) {
        var leafG = d3.select(this);
        leafG.select("rect").attr("stroke-width", "0");
    }

    function handleEditBtn(event, d) {
        console.log("EDIT", d)
        const data = d.data
        dyFObj.editElement("classifieds", data.id, null, dyfCompetencesUpdate)
    }

    function handleDeleteBtn(event, d) {
        const data = d.data
        bootbox.confirm("Voulez vous vraiment supprimer ce competences?", (res) => {
            if (res) {
                var url = baseUrl + "/" + moduleId + "/element/delete/id/" + data.id + "/type/classifieds";
                ajaxPost(
                    null,
                    url,
                    null,
                    function(data) {
                        if (data.result) {
                            toastr.success("Ce competences a été supprimé avec succes");
                            refreshDendo(width, height);
                        } else {
                            toastr.error(data.msg);
                        }
                    }
                );
            }
        })

    }
}