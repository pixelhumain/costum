var coSinDniObj = {
    sousOrganisationName : "CACs",
    filterCacs : {},
    init : function(coObj,formParent=null){
        coObj.sousOrganizationFilter(coObj);
    },
    getcacs : function(cobj){
        /*var cacs = {};
        ajaxPost(
            null,
            baseUrl + "/co2/aap/cacs/action/listcacs/slug/"+costum.contextSlug,
            {
                searchType: ["organizations"],
                count: true,
                countType : ["organizations"],
                indexStep: 0  ,
                notSourceKey: true
            },
            function(data){ 
                cacs = data.results
            },null,null,{async:false}
        );
        return cacs;*/
    },
    sousOrganizationFilter : function(cobj){
        //var cacs = cobj.getcacs(cobj);
        $.each(aapObj.subOrganizations,function(k,v){ 
            cobj.filterCacs[v.formId] = v.name;
            trad[v.formId] = v.name
        })
    },
    paramsFilters : function(coObj){
        var params = {
            text : {
                view : "text",
                field : "answers.aapStep1.titre",
                event : "text",
                placeholder : trad.searchBy + " " + trad.projectName.toLocaleLowerCase(),
                icon : "fa-search",
                customClass : "set-width visible-xs to-sticky to-set-value",
                inputAlias : ".filter-alias .main-search-bar-addon"
            },
            theme: {
                view: "megaMenuAccordion",
                type: "filters",
                field: "answers.aapStep1.tags",
                remove0: true,
                countResults: true,
                activateCounter: true,
                countFieldPath: "answers.aapStep1.tags",
                remove0: false,
                name: trad["search by theme"],
                event: "filters",
                list: aapObj.form?.["params"]?.["tags"]?.["list"]
            },
            acceptation: {
                view: "accordionList",
                type: "filters",
                field: "acceptation",
                name: "Retenu ou Rejeté",
                event: "filters",
                keyValue: false,
                list: {
                    "retained": "Retenu",
                    "rejected": "Rejeté",
                    "pending": "En attente"
                }
            },
            seen    : {
                view : "accordionList",
                type : "filters",
                field: "views",
                name : trad.notSeen+"/"+trad.Seen,
                event: "filters",
                keyValue: false,
                list : {
                    "notSeen": trad.notSeen,
                    "seen" : trad.Seen,
                }
            },
            inproject : {
                view : "accordionList",
                type : "filters",
                field : "inproject",
                name : trad.projectstate+"/"+trad.proposal,
                event : "filters",
                keyValue : false,
                list : {
                    inproject : trad.projectstate,
                    inproposal : trad.proposal
                }
            },
            urgency: {
                view: "accordionList",
                type: "filters",
                field: "answers.aapStep1.urgency",
                name: "Urgence",
                event: "filters",
                list: {
                    "Urgent": "Urgent"
                }
            },
            status: {
                view: "accordionList",
                type: "filters",
                field: "status",
                name: "status",
                event: "filters",
                keyValue: false,
                list: aapObj.status
            },
            favorite : {
                view : "accordionList",
                type : "filters",
                field : "vote",
                name : trad.Favorites,
                event : "filters",
                keyValue : false,
                list : {
                    [userId] : trad.Favorites,
                }
            },
            sortBy: {
                view: "accordionList",
                type: "sortBy",
                name: trad.sortby,
                event: "sortBy",
                list: {
                    "Ordre alphabetique": {
                        "answers.aapStep1.titre": 1,
                    },
                    "Plus de votes": {
                        "answers.aapStep2.allVotes": -1,
                    },
                    "Moins de votes": {
                        "answers.aapStep2.allVotes": 1,
                    },
                    "Date de création croissant": {
                        "created": 1,
                    },
                    "Date de création décroissant": {
                        "created": -1,
                    },
                    "Date de mise à jour croissant": {
                        "updated": 1,
                    },
                    "Date de mise à jour décroissant": {
                        "updated": -1,
                    },
                }
            },
            year : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.year",
                name : trad.year,
                event : "filters",
                list : generateIntArray(2022,new Date().getFullYear()).map(v => v = v.toString())
            },
            oneSubOrganization : {
                view : "accordionList",
                type : "filters",
                field : "oneSubOrganization",
                name : coObj.sousOrganisationName,
                event : "filters",
                keyValue : false,
                list : coObj.filterCacs
            }
        }
        return params;
    },
    defaultFilters: function(coObj){
        var params = {
            notSourceKey : true,
            indexStep: "15",
            types:["answers"],
            forced:{
                filters:{
                }
            },
            filters:{
                'form' : aapObj.form["_id"]["$id"],
                'answers.aapStep1.titre' : {'$exists':true},
                'status':{'$not':"finish"},
                'project.id' : {'$exists': "false"},
                "answers.aapStep1.year" : aapObj.aapSession,
                'allSubOrganisation' : Object.keys(coObj.filterCacs)
            },
            sortBy:{"updated":-1},
            fields: [],
            tagsPath:"answers.aapStep1.tags",
        }
        return params;
    },
}

coSinDniObj.init(coSinDniObj);
