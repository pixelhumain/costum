function bindDynFormEditable(){ 
		// $(".btn-update-when").off().on( "click", function(){
		// 	var dynFormUpDate={
		// 		afterSave : function(data){
		// 			mylog.dir(data);
		// 			if(data.result && notEmpty(data.map)){
		// 				contextData.recurrency=data.map.recurrency;
		// 				if(data.map.recurrency){
		// 					if(typeof contextData.startDate != "undefined")
		// 						delete contextData.startDate;
		// 					if(typeof contextData.endDate != "undefined")
		// 						delete contextData.endDate;
		// 					if(typeof data.map.openingHours != "undefined")
		// 						contextData.openingHours=data.map.openingHours;
		// 				}else{
		// 					if(typeof data.map.startDate != "undefined")
		// 						contextData.startDate = data.map.startDate;
		// 					if(typeof data.map.endDate != "undefined")
		// 						contextData.endDate = data.map.endDate;
		// 					if(typeof contextData.openingHours != "undefined")
		// 						delete contextData.openingHours;
		// 				}
		// 				pageProfil.initDateHeaderPage(contextData);
		// 				initDate();
		// 				//updateCalendar();
		// 			}
		// 			//urlCtrl.loadByHash(location.hash);
		// 			dyFObj.closeForm();
		// 			urlCtrl.loadByHash(location.hash);
		// 		},
		// 		onload : {
		// 			actions : {
		// 				setTitle : tradDynForm.editEventDate,
		// 				hide : {
		// 					"nametext":1,
		// 					"organizerfinder":1,
		// 					"typeselect":1,
		// 					"imageuploader":1,
		// 					"formLocalityformLocality":1,
		// 					"shortDescriptiontextarea":1,
		// 					"tagstags":1,
		// 					"urltext":1,
		// 					"urlsarray" : 1,
  //                           "emailtext":1,
  //                           "publiccheckboxSimple":1,
  //                           "parentfinder":1,
  //                           "locationlocation" : 1
		// 				}
		// 			}
		// 		}
		// 	};
		// 	dyFObj.editElement("events", contextData.id, null, dynFormUpDate);
		// });
		$(".btn-update-when").off().on( "click", function(){
            var editMode = $(this).data("editMode");
            var propDynForm = {};
            if(contextData.type == "organizations"){
                propDynForm = {
                    openingHours :  {
                        inputType : "openingHours",
                        label : tradDynForm.weekrepeat,
                        options: {
                            "allWeek" : true,
                            "multipleHours": false
                        },
                    }
                };
            }else if(contextData.type == "projects"){
                propDynForm = {
                    startDate :  {
                            inputType : "datetime",
                            placeholder: tradDynForm.startDate,
                            label : tradDynForm.startDate
                        },
                        endDate :  {
                            inputType : "datetime",
                            placeholder: tradDynForm.endDate,
                            label : tradDynForm.endDate
                        }
                };
            }

			var dynFormUpDate={
				beforeBuild: {
                    properties : propDynForm    
                },  
                afterSave : function(data,){
					mylog.dir(data);
					if(data.result && notEmpty(data.map)){
						contextData.recurrency=data.map.recurrency;
						if(data.map.recurrency){
							if(typeof contextData.startDate != "undefined")
								delete contextData.startDate;
							if(typeof contextData.endDate != "undefined")
								delete contextData.endDate;
							if(typeof data.map.openingHours != "undefined")
								contextData.openingHours=data.map.openingHours;
						}else{
							if(typeof data.map.startDate != "undefined")
								contextData.startDate = data.map.startDate;
							if(typeof data.map.endDate != "undefined")
								contextData.endDate = data.map.endDate;
							if(typeof contextData.openingHours != "undefined")
								delete contextData.openingHours;
						}
						if(typeof data.map.openingHours != "undefined")
								contextData.openingHours=data.map.openingHours;

						pageProfil.initDateHeaderPage(contextData);
						initDate();
						//updateCalendar();
					}
					//urlCtrl.loadByHash(location.hash);
					dyFObj.closeForm();
					pageProfil.views.detail();
					
				},
				onload : {
					actions : {
						setTitle : getdynTitle(),
						hide : {
							"nametext":1,
							"organizerfinder":1,
							"typeselect":1,
							"imageuploader":1,
							"formLocalityformLocality":1,
							"shortDescriptiontextarea":1,
							"tagstags":1,
							"urltext":1,
							"urlsarray" : 1,
                            "emailtext":1,
                            "publiccheckboxSimple":1,
                            "parentfinder":1,
                            "roleselect":1,
                            "locationlocation":1,
						}
					}
				}
			};
			mylog.log("btn-update-when editElement contextData.type", contextData.type, contextData.id)
			dyFObj.editElement(contextData.type, contextData.id, null, dynFormUpDate);
		});

		$(".btn-update-info").off().on( "click", function(){
			
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update general information"],
						icon : "fa-key",
						type: "object",
						onLoads : {
							initUpdateInfo : function(){
								mylog.log("initUpdateInfo");
								$(".emailOptionneltext").slideToggle();
								$("#ajax-modal .modal-header").removeClass("bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
							},
							onload : function (data) { 
								//this is a hack too a strange bug
								//this select doesn't carry it's value
								if( contextData.type == typeObj.event.col || contextData.type == typeObj.organization.col )
									$("#type").val(data.type);
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave");
							removeFieldUpdateDynForm(contextData.type);
					    },
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){

								if(typeof data.resultGoods.values.name != "undefined"){
									contextData.name = data.resultGoods.values.name;
									$("#nameHeader > .name-header").html(contextData.name);
									$("#nameAbout").html(contextData.name);
									if(inMyContacts(contextData.type, contextData.id))
										changeNameFloopEntity(contextData.id, contextData.type, contextData.name);
								}

								if(typeof data.resultGoods.values.username != "undefined"){
									contextData.username = data.resultGoods.values.username;
									$("#usernameAbout").html(contextData.username);
								}
									
								if(typeof data.resultGoods.values.tags != "undefined"){
									contextData.tags = data.resultGoods.values.tags;
									var strHeader = "";
									var strAbout = trad["notSpecified"];
									if($('.header-tags').length && typeof contextData.tags != "undefined" && contextData.tags.length > 0){
										strAbout = "" ;
										$.each(contextData.tags, function (key, tag){
											/*str +=	'<div class="tag label label-danger pull-right" data-val="'+tag+'">'+
														'<i class="fa fa-tag"></i>'+tag+
													'</div>';*/
											strHeader += '<a href="'+baseUrl+'co2/#search?text=#'+tag+'" class="badge letter-red bg-white" style="vertical-align: top;">#'+tag+'</a>';
											/*if(typeof globalTheme == "undefined" || globalTheme != "network")
												addTagToMultitag(tag);*/
											strAbout +=	'<a href="'+baseUrl+'co2/#search?text=#'+tag+'"  class="badge letter-red bg-white">'+tag+'</a>';
										});
									}
									$('.header-tags').html(strHeader);
									$('#tagsAbout').html(strAbout);
									if(strHeader == "" && typeof contextData.address == "undefined")
										$('.header-address-tags').addClass("hidden");
									else
										$('.header-address-tags').removeClass("hidden");

									if(strHeader == "")
										$('#separateurTag').addClass("hidden");
									else
										$('#separateurTag').removeClass("hidden");

								}

								if(typeof data.resultGoods.values.avancement != "undefined"){
									contextData.avancement = data.resultGoods.values.avancement.trim();
									val=0;
							    	if(contextData.avancement=="idea")
										val=5;
									else if(contextData.avancement=="concept")
										val=20;
									else if (contextData.avancement== "started")
										val=40;
									else if (contextData.avancement == "development")
										val=60;
									else if (contextData.avancement == "testing")
										val=80;
									else if (contextData.avancement == "mature")
										val=100;
									$('#progressStyle').val(val);
									$('#labelProgressStyle').html(contextData.avancement);
									$('#avancementAbout').html(trad[contextData.avancement] );
								}

								if(typeof data.resultGoods.values.type != "undefined"){

									if(contextData.type == typeObj.organization.col ){
										contextData.typeOrga = data.resultGoods.values.type;
										$(".pastille-type-element").removeClass("bg-azure bg-red bg-green bg-turq").addClass("bg-"+typeObj[contextData.typeOrga]["color"]);
										$("#nameHeader").find("i").removeClass("fa-university fa-industry fa-users fa-group").addClass("fa-"+typeObj[contextData.typeOrga]["icon"]);
									}
									else
										contextData.typeEvent = data.resultGoods.values.type;
									//$("#typeHeader").html(data.resultGoods.values.type);
									$("#typeAbout").html(tradCategory[data.resultGoods.values.type]);
									$("#typeHeader .type-header").html(tradCategory[data.resultGoods.values.type]);
								}

								if(typeof data.resultGoods.values.email != "undefined"){
									mylog.log("update email");
									contextData.email = data.resultGoods.values.email;
									if(contextData.email != "" )
										$("#emailAbout").html(contextData.email);
									else
										$("#emailAbout").html("<i>"+trad["notSpecified"]+"</i>");
								}

								if(typeof data.resultGoods.values.url != "undefined"){
									mylog.log("update url");
									contextData.url = data.resultGoods.values.url.trim();
									if(contextData.url != "" ){
										$("#webAbout").html('<a href="'+contextData.url+'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'+contextData.url+'</a>');
									}else{
										$("#webAbout").html("<i>"+trad["notSpecified"]+"</i>");
									}
								}  
									
								if(typeof data.resultGoods.values.birthDate != "undefined"){
									mylog.log("update birthDate");
									contextData.birthDate = data.resultGoods.values.birthDate;
									$("#birthDateAbout").html(moment(contextData.birthDate.sec * 1000).local().format("DD/MM/YYYY"));
								}

								if(typeof data.resultGoods.values.fixe != "undefined"){
									mylog.log("update fixe");
									contextData.fixe = parsePhone(data.resultGoods.values.fixe);
									$("#fixeAbout").html(contextData.fixe);
								}

								if(typeof data.resultGoods.values.mobile != "undefined"){
									mylog.log("update mobile");
									contextData.mobile = parsePhone(data.resultGoods.values.mobile);
									$("#mobileAbout").html(contextData.mobile);
								}

								/*if(typeof data.resultGoods.values.fax != "undefined"){
									mylog.log("update fax");
									contextData.fax = parsePhone(data.resultGoods.values.fax);
									$("#faxAbout").html(contextData.fax);
								}*/

								if(typeof data.resultGoods.values.parent != "undefined"){
									mylog.log("modif parent", data.resultGoods.values.parent);
									contextData.parent = data.resultGoods.values.parent;
								//	contextData.parentId = data.resultGoods.values.parent.parentId;
								//	contextData.parentType = data.resultGoods.values.parent.parentType;

									var htmlAbout = "<i>"+trad["notSpecified"]+"</i>";
									var htmlHeader = "";
									if(notEmpty(contextData.parent) && Object.keys(contextData.parent).length > 0){
										count=Object.keys(contextData.parent).length;
										htmlAbout="";
										$.each(contextData.parent, function(e, v){
											mylog.log("modif parent each" , e, v);
											heightImg=(count>1) ? 35 : 25;
											imgIcon = (typeof v.profilThumbImageUrl != "undefined" && v.profilThumbImageUrl!="" ) ? baseUrl+"/"+v.profilThumbImageUrl: assetPath + "/images/thumb/default_"+v.type+".png";  
											htmlAbout+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh tooltips" ';
											if(count>1) htmlAbout+= 'data-toggle="tooltip" data-placement="left" title="'+v.name+'"';
											htmlAbout+=">"+
												'<img src="'+imgIcon+'" class="img-circle margin-right-10" width='+heightImg+' height='+heightImg+' />';
											if(count==1) htmlAbout+=v.name;
											htmlAbout+="</a>";
										});
										htmlHeader = ((contextData.type == typeObj.event.col) ? tradCategory["Planned on"] : tradCategory.carriedby ) ;
										htmlHeader += " : "+htmlAbout;
									}
									else if(notEmpty(contextData.parentId) && contextData.parentId != "dontKnow"){
										htmlAbout = '<a href="#page.type.'+contextData.parentType+'.id.'+contextData.parentId+'" class="lbh">'+ 
											'<i class="fa fa-'+dyFInputs.get(contextData.parentType).icon+'"></i> '+
											contextData.parent.name+'</a><br/>';

										htmlHeader =((contextData.type == typeObj.event.col) ? tradCategory["Planned on"] : tradCategory.carriedby ) ;
										htmlHeader += htmlAbout;
									}
									mylog.log("modif parent html" , htmlAbout);
									mylog.log("modif parent htmlHeader" , htmlHeader);
									$("#parentAbout").html(htmlAbout);
									$("#parentHeader").html(htmlHeader);
								}

								if(typeof data.resultGoods.values.organizer != "undefined"){
									mylog.log("modif organizer", data.resultGoods.values.organizer);

									contextData.organizer = data.resultGoods.values.organizer;
									//contextData.organizerId = data.resultGoods.values.organizer.organizerId;
									//contextData.organizerType = data.resultGoods.values.organizer.organizerType;

									var htmlAbout = "<i>"+trad["notSpecified"]+"</i>";
									var htmlHeader = "";
									if(notEmpty(contextData.organizer) && Object.keys(contextData.organizer).length > 0){
										count=Object.keys(contextData.organizer).length;
										htmlAbout="";
										$.each(contextData.organizer, function(e, v){
											mylog.log("modif organizer each" , e, v);
											heightImg=(count>1) ? 35 : 25;
											imgIcon = (typeof v.profilThumbImageUrl != "undefined" && v.profilThumbImageUrl!="" ) ? baseUrl+"/"+v.profilThumbImageUrl: assetPath + "/images/thumb/default_"+v.type+".png";  
											htmlAbout+='<a href="#page.type.'+v.type+'.id.'+e+'" class="lbh tooltips" ';
											if(count>1) htmlAbout+= 'data-toggle="tooltip" data-placement="left" title="'+v.name+'"';
											htmlAbout+=">"+
												'<img src="'+imgIcon+'" class="img-circle margin-right-10" width='+heightImg+' height='+heightImg+' />';
											if(count==1) htmlAbout+=v.name;
											htmlAbout+="</a>";
										});
										htmlHeader =tradDynForm.organizedby + " " + htmlAbout;
									}
									else if(notEmpty(contextData.organizerId) && contextData.organizerId!="dontKnow"){
										htmlAbout = '<a href="#page.type.'+contextData.organizerType+'.id.'+contextData.organizerId+'" class="lbh">'+ 
													'<i class="fa fa-'+dyFInputs.get(contextData.organizerType).icon+'"></i> '+
												data.resultGoods.values.organizer.organizer.name+'</a><br/>';
										htmlHeader = tradDynForm.organizedby + " : " + html;
									}
									mylog.log("modif organizer html" , htmlAbout);
									mylog.log("modif organizer htmlHeader" , htmlHeader);
									$("#organizerAbout").html(htmlAbout);
									$("#organizerHeader").html(htmlHeader);
								}
							}
							dyFObj.closeForm();
							changeHiddenFields();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							name : dyFInputs.name(contextData.type),
							similarLink : dyFInputs.similarLink,
							typeElement : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true)
						}
					}
				}
			};

			if(contextData.type == typeObj.person.col ){
				//form.dynForm.jsonSchema.properties.username = dyFInputs.inputText("Username", "Username", { required : true, validUserName : true,rangelength : [4, 32] }); //,uniqueUserName:true
				form.dynForm.jsonSchema.properties.birthDate = dyFInputs.birthDate;
			}

			if(contextData.type == typeObj.organization.col ){
				form.dynForm.jsonSchema.properties.type = dyFInputs.inputSelect(tradDynForm.organizationType, tradDynForm.organizationType, organizationTypes, { required : true });
			}

			// else if(contextData.type == typeObj.event.col ){
			// 	mylog.log("Type event ", typeObj.event.col, contextData.type);
			// 	form.dynForm.jsonSchema.properties.type = dyFInputs.inputSelect(tradDynForm.eventTypes, tradDynForm.eventTypes, eventTypes, { required : true });
			// }

			else if( contextData.type == typeObj.project.col ){
				form.dynForm.jsonSchema.properties.avancement = dyFInputs.inputSelect(tradDynForm.theprojectmaturity, tradDynForm.projectmaturity, avancementProject);
			}
			var listTagHva = null ;
			var tagsHVA = ( (contextData.type == "classifields") ? "tagsClassifields" : "tagsO" ) ;
			if( typeof costum.paramsData != "undefined"  && 
					typeof costum.paramsData[tagsHVA] != "undefined")
				listTagHva = costum.paramsData[tagsHVA];
			form.dynForm.jsonSchema.properties.tags = {
														inputType : "tags",
														values : (listTagHva != null) ? listTagHva : tagsList,
														label : "Catégorie",
														placeholder : "Choisir une catégorie",
							                            minimumInputLength : 0,
							                            tagsList : "tagsO",
							                            maximumSelectionLength : 2
													};
			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				var ruleMail = ( (contextData.type == typeObj.person.col) ? { email: true, required : true } : { email: true } ) ;
				form.dynForm.jsonSchema.properties.email = dyFInputs.email(tradDynForm.mainemail, tradDynForm.mainemail, ruleMail);
			}
			
			if(contextData.type == typeObj.person.col || contextData.type == typeObj.organization.col ){
				form.dynForm.jsonSchema.properties.fixe= dyFInputs.inputText(tradDynForm["fix"],tradDynForm["enterfixnumber"]);
				form.dynForm.jsonSchema.properties.mobile= dyFInputs.inputText(tradDynForm["mobile"],tradDynForm["entermobilenumber"]);
				/*form.dynForm.jsonSchema.properties.fax= dyFInputs.inputText(tradDynForm["fax"],tradDynForm["enterfaxnumber"]);*/
			}

			if(contextData.type != typeObj.poi.col) 
				form.dynForm.jsonSchema.properties.url = dyFInputs.inputUrl();


			var listParent =  ["organizations"] ;

			if(contextData.type == typeObj.event.col)
				listParent =  ["events"] ;
			else if(contextData.type == typeObj.project.col)
				listParent =  ["organizations", "projects"] ;

			if(contextData.type == typeObj.event.col || contextData.type == typeObj.project.col){
				/*form.dynForm.jsonSchema.properties.parentId = {
		         	label : tradDynForm["ispartofelement"]+" ?",
	            	inputType : "select",
	            	class : "",
	            	placeholder : tradDynForm["ispartofelement"]+" ?",
	            	options : firstOptions(),
	            	"groupOptions" : parentList( listParent, contextData.parentId, contextData.parentType ),
	            	init : function(){ 
	            		mylog.log("init ParentId");
		            	$("#ajaxFormModal #parentId").off().on("change",function(){
							var selected = $(':selected', this);
		            		mylog.log("change ParentId", selected, selected.data('type'));
	    					$("#ajaxFormModal #parentType").val(selected.data('type'));
		            	});
		            }
	            };

	            form.dynForm.jsonSchema.properties.parentType = dyFInputs.inputHidden();*/
	            multiple=(contextData.type==typeObj.event.col)? false : true;
	            label=(contextData.type==typeObj.event.col)? tradDynForm.ispartofevent : tradDynForm.whoiscarrytheproject;
	            initType=(contextData.type==typeObj.event.col)? ["events"] : ["organizations", "projects"];
	          //   form.dynForm.jsonSchema.properties.parent= {
	          //   	inputType : "finder",
		         //    label : label,
		         //   	multiple : multiple,
		         //   	initType: initType,
		         //   	update : true,
        			// openSearch :true,
        			// values : (typeof contextData.parent != "undefined" && Object.keys(contextData.parent).length > 0) ? contextData.parent : null
	          //   };
	       //      if(contextData.type==typeObj.event.col){
	       //      	form.dynForm.jsonSchema.properties.parent.init= function(){
	       //      		var finderParams = {
		      //   			id : "parent",
		      //   			multiple : false,
		      //   			initType : ["events"],
		      //   			values : form.dynForm.jsonSchema.properties.parent.values,
		      //   			update : true
		      //   		};
		      //   		finder.init(finderParams, function(){
	    			// 			$.each(finder.selectedItems, function(e, v){
	    			// 				startDateParent=v.startDate;
	    			// 				endDateParent=v.endDate;
	    			// 			});
			     //        		$("#startDateParent").val(startDateParent);
			     //        		$("#endDateParent").val(endDateParent);
			     //        		if($("#parentstartDate").length <= 0){
				    //         		$("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
			    	// 				$("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
			     //        		}
			     //        		$("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
				    // 			$("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm'));	
	    			// 		});
    				// }
	       //      }
	        }

            if(contextData.type == typeObj.event.col){
            	form.dynForm.jsonSchema.properties.organizer= {
	            	inputType : "finder",
		            label : tradDynForm.whoorganizedevent,
		           	initType: ["projects", "organizations"],
	    			multiple : true,
	    			update : true,
	    			openSearch :true,
	    			values : (typeof contextData.organizer != "undefined" && Object.keys(contextData.organizer).length > 0) ? contextData.organizer : null
	            };
	            if(contextData.type==typeObj.event.col){
	            	form.dynForm.jsonSchema.properties.organizer.init= function(){

	            		var finderParams = {
		        			id : "organizer",
		        			multiple : true,
		        			initType : ["projects", "organizations"],
		        			values : form.dynForm.jsonSchema.properties.organizer.values,
		        			update : true
		        		};
		        		finder.init(finderParams);
	    				//finder.init("organizer", true, ["projects", "organizations"], form.dynForm.jsonSchema.properties.organizer.values, true);
    				}
	            }
            	//form.dynForm.jsonSchema.properties.organizerId =  dyFInputs.organizerId(contextData.parentId, contextData.parentType);
	            //form.dynForm.jsonSchema.properties.organizerType = dyFInputs.inputHidden();
            }
            
			
			var dataUpdate = {
				block : "info",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name,	
			};
			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0 && typeof formData != "undefined") {
				$.each(finder.object, function(key, object){
					formData[key]=object;
				});
			}
			if(notNull(contextData.slug) && contextData.slug.length > 0)
				dataUpdate.slug = contextData.slug;

			if(notNull(contextData.tags) && contextData.tags.length > 0)
				dataUpdate.tags = contextData.tags;

			if(contextData.type == typeObj.person.col ){
				if(notNull(contextData.username) && contextData.username.length > 0)
					dataUpdate.username = contextData.username;
				if(notEmpty(contextData.birthDate))
					dataUpdate.birthDate = moment(contextData.birthDate.sec * 1000).local().format("DD/MM/YYYY");
			}
			
			mylog.log("ORGA ", contextData.type, typeObj.organization.col, dataUpdate.type);
			
			if(contextData.type == typeObj.organization.col ){
				mylog.log("ORGA type", contextData.typeOrga, contextData.typeOrganization);
				if(notEmpty(contextData.typeOrga))
					dataUpdate.type = contextData.typeOrga;
				mylog.log("ORGA resultType", dataUpdate.type);
			}else if(contextData.type == typeObj.event.col ){
				if(jsonHelper.notNull("contextData.typeEvent") )
					dataUpdate.type = contextData.typeEvent;
			}else if(contextData.type == typeObj.project.col ){
				if(notEmpty(contextData.avancement))
					dataUpdate.avancement = contextData.avancement;
			}

			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				mylog.log("test email", contextData, contextData.email);
				if(notEmpty(contextData.email)) {
					mylog.log("test email2", contextData, contextData.email);
					dataUpdate.email = contextData.email;
				}
				if(notEmpty(contextData.fixe))
					dataUpdate.fixe = contextData.fixe;
				if(notEmpty(contextData.mobile))
					dataUpdate.mobile = contextData.mobile;
				/*if(notEmpty(contextData.fax))
					dataUpdate.fax = contextData.fax;*/
			}
			
			if(contextData.type != typeObj.poi.col && notEmpty(contextData.url)) 
				dataUpdate.url = contextData.url;

			if(notEmpty(contextData.parentId)) 
				dataUpdate.parentId = contextData.parentId;
			else
				dataUpdate.parentId = "dontKnow";

			if(notEmpty(contextData.parentType)) 
				dataUpdate.parentType = contextData.parentType;

			if(notEmpty(contextData.organizerId)) 
				dataUpdate.organizerId = contextData.organizerId;

			if(notEmpty(contextData.organizerType)) 
				dataUpdate.organizerType = contextData.organizerType;
			
			mylog.log("dataUpdate", dataUpdate);
			dyFObj.openForm(form, "initUpdateInfo", dataUpdate);
		});

		$(".btn-update-descriptions").off().on( "click", function(){

			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update description"],
						icon : "fa-key",
						onLoads : {
							
							markdown : function(){
								dataHelper.activateMarkdown("#ajaxFormModal #description");
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
								//bindDesc("#ajaxFormModal");
								///Initialisation de ton textearea
								// mentionsInit.get("textarea#description");
								// if(typeof(contextData.descMentions) != "undefined")
        //   							$("textarea#description").val( mentionsInit.addMentionInText( $("textarea#description"), contextData.descMentions ) ) ;
							}
						},
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){
								if(data.resultGoods.values.shortDescription=="")
									$(".contentInformation #shortDescriptionAbout").html('<i>'+trad["notSpecified"]+'</i>');
								else
									$(".contentInformation #shortDescriptionAbout").html(data.resultGoods.values.shortDescription);
								$(".contentInformation #shortDescriptionAboutEdit").html(data.resultGoods.values.shortDescription);
								$("#shortDescriptionHeader").html(data.resultGoods.values.shortDescription);
								if(data.resultGoods.values.description=="")
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml('<i>'+trad["notSpecified"]+'</i>'));
								else
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml(data.resultGoods.values.description));
								$("#descriptionMarkdown").html(data.resultGoods.values.description);
							}
							dyFObj.closeForm();
							changeHiddenFields();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							descMentions : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true),
							shortDescription : 	dyFInputs.textarea(tradDynForm["shortDescription"], "...",{ maxlength: 140 }),
							description : dyFInputs.textarea(tradDynForm["longDescription"], "..."),
						}
					}
				}
			};

			var dataUpdate = {
				block : "descriptions",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name,
		        shortDescription : $(".contentInformation #shortDescriptionAboutEdit").html(),
				description: dataHelper.htmlToMarkdown($(".contentInformation #descriptionAbout").html()),	
			};

			dyFObj.openForm(form, "markdown", dataUpdate);
		});


		$(".btn-update-network").off().on( "click", function(){
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update network"],
						icon : "fa-key",
						onLoads : {
							sub : function(){
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  				  .addClass("bg-dark");
								//bindDesc("#ajaxFormModal");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave", contextData["socialNetwork"]);
					    	//removeFieldUpdateDynForm(contextData.type);
					    	var SNetwork = [ "telegram", "github", "skype", "twitter", "facebook", "gpplus", "instagram", "diaspora", "mastodon"];
							$.each(SNetwork, function(key, val){ 
								mylog.log("val", val);
								mylog.log("val2", $("#ajaxFormModal #"+val).val(), $("#ajaxFormModal #"+val).length);

								if(	notNull(contextData["socialNetwork"]) && 
									notNull(contextData["socialNetwork"][val]) &&
									( 	$("#ajaxFormModal #"+val).length &&
										$("#ajaxFormModal #"+val).val().trim() == contextData["socialNetwork"][val] ) ) {
									mylog.log("if", val);
									$("#ajaxFormModal #"+val).remove();
								} else if (	(	!notNull(contextData["socialNetwork"]) || 
												!notNull(contextData["socialNetwork"][val]) ) && $("#ajaxFormModal #"+val).length ){
									mylog.log("else", val);
									//$("#ajaxFormModal #"+val).remove();
								}
									
							});
					    },
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){

								if(!notEmpty(contextData.socialNetwork))
									contextData.socialNetwork = {};

								if(typeof data.resultGoods.values.telegram != "undefined"){
									contextData.socialNetwork.telegram = data.resultGoods.values.telegram.trim();
									if(contextData.socialNetwork.telegram.length != 0 )
										changeNetwork('#divTelegram', '#telegramAbout', 'https://web.telegram.org/#/im?p=@'+contextData.socialNetwork.telegram, 'https://web.telegram.org/#/im?p=@'+contextData.socialNetwork.telegram);
									else
										changeNetwork('#divTelegram', '#telegramAbout', contextData.socialNetwork.telegram, contextData.socialNetwork.telegram);
								}

								if(typeof data.resultGoods.values.diaspora != "undefined"){
									contextData.socialNetwork.diaspora = data.resultGoods.values.diaspora.trim();
									changeNetwork('#divDiaspora', '#diasporaAbout', contextData.socialNetwork.diaspora, contextData.socialNetwork.diaspora);
								}

								if(typeof data.resultGoods.values.mastodon != "undefined"){
									contextData.socialNetwork.mastodon = data.resultGoods.values.mastodon.trim();
									changeNetwork('#divMastodon', '#mastodonAbout', contextData.socialNetwork.mastodon, contextData.socialNetwork.mastodon);
								}

								if(typeof data.resultGoods.values.facebook != "undefined"){
									contextData.socialNetwork.facebook = data.resultGoods.values.facebook.trim();
									changeNetwork('#divFacebook','#facebookAbout', contextData.socialNetwork.facebook, contextData.socialNetwork.facebook);
								}

								if(typeof data.resultGoods.values.twitter != "undefined"){
									contextData.socialNetwork.twitter = data.resultGoods.values.twitter.trim();
									changeNetwork('#divTwitter','#twitterAbout', contextData.socialNetwork.twitter, contextData.socialNetwork.twitter);
								}

								if(typeof data.resultGoods.values.github != "undefined"){
									contextData.socialNetwork.github = data.resultGoods.values.github.trim();
									changeNetwork('#divGithub','#githubAbout', contextData.socialNetwork.github, contextData.socialNetwork.github);
								}

								if(typeof data.resultGoods.values.skype != "undefined"){
									contextData.socialNetwork.skype = data.resultGoods.values.skype.trim();
									changeNetwork('#divSkype','#skypeAbout', contextData.socialNetwork.skype, contextData.socialNetwork.skype);
								}

								if(typeof data.resultGoods.values.gpplus != "undefined"){
									contextData.socialNetwork.gpplus = data.resultGoods.values.gpplus.trim();
									changeNetwork('#divGpplus','#gpplusAbout', contextData.socialNetwork.gpplus, contextData.socialNetwork.gpplus);
								}

								if(typeof data.resultGoods.values.instagram != "undefined"){
									contextData.socialNetwork.instagram = data.resultGoods.values.instagram.trim();
									changeNetwork('#divInstagram','#instagramAbout', contextData.socialNetwork.instagram, contextData.socialNetwork.instagram);
								}
							}
							dyFObj.closeForm();
							changeHiddenFields();
						},

						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true), 
							skype : dyFInputs.inputUrl(tradDynForm["linkSkype"]),
							// github : dyFInputs.inputUrl(tradDynForm["linkGithub"]), 
							// gpplus : dyFInputs.inputUrl(tradDynForm["linkGplus"]),
							twitter : dyFInputs.inputUrl(tradDynForm["linkTwitter"]),
							facebook :  dyFInputs.inputUrl(tradDynForm["linkFacebook"]),
							instagram :  dyFInputs.inputUrl(tradDynForm["linkInstagram"]),
					        // diaspora :  dyFInputs.inputUrl(tradDynForm["linkDiaspora"]),
					        // mastodon :  dyFInputs.inputUrl(tradDynForm["linkMastodon"]),
						}
					}
				}
			};

			if(contextData.type == typeObj.person.col ){
				form.dynForm.jsonSchema.properties.telegram = dyFInputs.inputText("Votre Speudo Telegram","Votre Speudo Telegram");
			}

			var dataUpdate = {
				block : "network",
				id : contextData.id,
				typeElement : contextData.type,
			};

			if(notEmpty(contextData.socialNetwork) )
			{
				if( notEmpty(contextData.socialNetwork.twitter) )
					dataUpdate.twitter = contextData.socialNetwork.twitter;
				// if( notEmpty(contextData.socialNetwork.googleplus) )
				// 	dataUpdate.gpplus = contextData.socialNetwork.googleplus;
				// if( notEmpty(contextData.socialNetwork.github) )
				// 	dataUpdate.github = contextData.socialNetwork.github;
				if( notEmpty(contextData.socialNetwork.skype) )
					dataUpdate.skype = contextData.socialNetwork.skype;
				// if( notEmpty(contextData.socialNetwork.telegram) )
				// 	dataUpdate.telegram = contextData.socialNetwork.telegram;
				if( notEmpty(contextData.socialNetwork.facebook) )
					dataUpdate.facebook = contextData.socialNetwork.facebook;
				if(notEmpty(contextData.socialNetwork.instagram))
					dataUpdate.instagram = contextData.socialNetwork.instagram;
				// if( notEmpty(contextData.socialNetwork.diaspora) )
				// 	dataUpdate.diaspora = contextData.socialNetwork.diaspora;
				// if( notEmpty(contextData.socialNetwork.mastodon) )
				// 	dataUpdate.mastodon = contextData.socialNetwork.mastodon;
			}
			dyFObj.openForm(form, "sub", dataUpdate);

			
		});
	}