if(typeof costum[costum.slug]=="undefined"){
	costum[costum.slug]={};

}
    Login.runRegisterValidator  = function(params) { 
		var form4 = $('.form-register');
		var errorHandler3 = $('.errorHandler', form4);
		var createBtn = null;
		$(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");

        $("#modalRegister .nameRegister, #modalRegister .usernameRegister").remove();

		if($("#surname").length==0){
			$("#modalRegister .form-register-inputs div:eq(5)").append(`
                <div class="surnameRegister">
		            <label class="letter-black"><i class="fa fa-user"></i> Nom </label><br>
		            <input class="form-control" id="surname" name="surname" placeholder="Nom"><br/>
		        </div>
                <div class="firstNameRegister">
		            <label class="letter-black"><i class="fa fa-address-card"></i> Prénom </label><br>
		            <input class="form-control" id="firstName" name="firstName" placeholder="Prénom"><br/>
		        </div>
                <div class="structureRegister">
		            <label class="letter-black"><i class="fa fa-building"></i> Structure </label><br>
		            <input class="form-control" id="structure" name="structure" placeholder="Structure"><br/>
		        </div>
				<div class="telephoneRegister">
		            <label class="letter-black"><i class="fa fa-phone"></i> Téléphone </label><br>
		            <input class="form-control" id="telephone" name="telephone" placeholder="Numéro téléphone"><br/>
		        </div>
                <div class="callbackRegister">
		            <input type="hidden" class="form-control" id="callbackRegister" name="callbackRegister">
		        </div>
                <div class="sessionRegister hide">
					<input type="checkbox" class="form-control" id="sessionRegister" name="sessionRegister">
				</div>
			`);
            $("#modalRegister label[for='charte']").remove();
            $("#modalRegister .agreeContent .checkbox-content").append(`
                <label for="newsletter" class="margin-top-10">
                    <input type="checkbox" class="newsletter" id="newsletter" name="newsletter">
                    <span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>
                    <span class="agreeMsg checkbox-msg letter-red pull-left">
                        Je m'inscris à la newsletter 

                    </span>
        </label>
            `)
		}
		
		form4.validate({
			rules : {
                surname : {
                    required : true
                },
                firstName : {
                    required : true
                },
    			email3 : {
    				required : { 
    				 	depends:function(){
    				 		$(this).val($.trim($(this).val()));
    				 		return true;
    				 	}
    				},
    				email : true
    			},
    			password3 : {
    				minlength : 8,
    				required : true
    			},
    			passwordAgain : {
    				equalTo : "#password3",
    				required : true
    			}
    		},
    		submitHandler : function(form) { 
    			errorHandler3.hide();
    			$(".createBtn").prop('disabled', true);
        		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
    			var params = { 
    			   "name" : $('.form-register #surname').val() +" "+ $('.form-register #firstName').val(),
    			   "username" : $('.form-register #surname').val() + $('.form-register #firstName').val(),
                   "surname" : $('.form-register #surname').val(),
                   "firstName" : $('.form-register #firstName').val(),
                   "structure" : $('.form-register #structure').val(),
    			   "email" : $(".form-register #email3").val(),
    			   "telephone" : $(".form-register #telephone").val(),
    			   "pendingUserId" : pendingUserId,
    			   "pwd" : $(".form-register #password3").val(),
                };
                if($('.form-register #isInvitation').val())
                	params.isInvitation=true;
                if($('.form-register #sessionRegister').val())
                	params.sessionRegister=true;
                if(Object.keys(scopeObj.selected).length > 0){
    		  		params.scope = scopeObj.selected;
    		  	}
    		  	if($(".form-register #newsletter").is(":checked"))
    		  		params.newsletter=true;
    		  	if($(".form-register #newsletterCollectif").is(":checked"))
    		  		params.newsletterCollectif=true;
    		  	var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
    
    		  	ajaxPost(
    	            null,
    	            baseUrl+"/costum/lacompagniedestierslieux/register",
    	            params,
    	            function(data){ 
    	                if(data.result) {
    	                	mylog.log("Register Form",data);
    						$(".createBtn").prop('disabled', false);
    	    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
    						$("#registerName").val("");
    						$("#username").val("");
                            $("#surname").val("");
                            $("#firstName").val("");
                            $("#structure").val("");
                            $("#telephone").val("");
    						$("#email3").val("");
    						$("#password3").val("");
    						$("#passwordAgain").val("");
    						$("#passwordAgain").val("");
    						$("#registerSurname").val("");
    						$(".form-register #gender").val("");
    			   			$(".form-register #situation").val("");
    			   			$(".form-register #telephone").val("");
    			   			$(".form-register #arrondissement").val("");
    						$('#newsletter').prop('checked', true);
    						$('#newsletterCollectif').prop('checked', true);
    						$('#redirectLaunchCollectif').prop('checked', true);
    	    		  		
                            $('.modal').modal('hide');
    	    		  		
    	    		  		$("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
    		    		  	$("#modalRegisterSuccess").modal({ show: 'true' }); 
                            var actionCallback=$(".form-register #callbackRegister").data("action");
                            mylog.log("action callback register",actionCallback);
                            if(typeof actionCallback!="undefined"){
                                if(actionCallback=="connect"){
                                    if(typeof $(".form-register #callbackRegister").attr("element")!="undefined"){                                   
                                        var contextElement=JSON.parse($(".form-register #callbackRegister").attr("element"));
                                        mylog.log("contextElement callback",contextElement);
                                        var refLink=(typeof contextElement.interestedLink!="undefined") ? contextElement.interestedLink.split("connect")[1] : "";
                                        var visioDate=(typeof contextElement.chosenDate!="undefined") ? contextElement.chosenDate : "";
                                        var extraUrlConnect="?visioDate="+visioDate;
                                        
                                        links.connectAjax(contextElement.collection, contextElement.id, data.id, "citoyens", "attendees", ["Intéressé.e"], function(res){
                                            mylog.log("callback connect registered to session");
                                        },extraUrlConnect);
    
                                       
    
    
                                    }
                                    
                                }
                                $("#callbackRegister").val("");
                                $("#callbackRegister").data("action","");
                                $("#callbackRegister").attr("element","");
                                
                            }
    		    		}else {
    		    		  	toastr.error(data.msg);
    		    		  	$('.modal').modal('hide');	    		  	
    	    		  		scopeObj.selected={};
    		    		}
    	            },function(data) {
    		    	  	toastr.error(trad["somethingwentwrong"]);
    		    	  	$(".createBtn").prop('disabled', false);
    					$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
    		    	  	//createBtn.stop();
    		    	}
    	        );
    		  	return false;
    		},
    		invalidHandler : function(event, validator) {
    			errorHandler3.show();
    			$(".createBtn").prop('disabled', false);
        		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
    		}
    	});
    };
    Login.runRegisterValidator();

    costum[costum.slug].sessionFormation = {
        afterSave : function(data){
            mylog.log("aftersave date events sessionFormation", data);
            urlCtrl.loadByHash(location.hash);
			var modalDom = $('#session-modal');
            ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + data.id, null, function (html) {
				modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
					modalDom.css('padding', '');
				});
				// modalDom.find('.modal-header').prepend('<h4 style="display: inline-block;margin: 0;">Gestion de la session de formation</h4>');
				modalDom.find('.modal-body').empty().html(html);
				modalDom.modal('show');
			});
        },
        afterBuild : function(data){
            mylog.log("afterbuild data sessionFormation",data);
            $("#ajaxFormModal .visioDatecodate label[for=eventDurationcodate-visioDate]").text("Durée (en heure) des visios d'information relatives à cette session");
            $("#ajaxFormModal .removeDateBlock").parent().css({"position": "absolute","left":"-10px","top": "2px"});
        }
    };

    costum[costum.slug].formation = {
        afterSave : function(data){
            mylog.log("aftersave formation", data);
            urlCtrl.loadByHash(location.hash);
        }
    };

    var modalSessionHtml='<div class="modal fade" id="session-modal">'+
       '<div class="modal-dialog">'+
        '<div class="modal-content">'+
           ' <div class="modal-header">'+
		       '<h3 style="display: inline-block;margin: 0;font-size: 22px;font-style: italic;">Interface de gestion de la session</h3>'+
               '<a id="mini-info" href="javascript:;">   <i style="font-size:25px;" class="fa fa-info-circle"></i>'+  
			   ' <button type="button" class="close" data-dismiss="modal">'+
                   ' <span aria-hidden="true">&times;</span>'+
                    '<span class="sr-only">Close</span>'+
                '</button>'+
            '</div>'+
            '<div class="modal-body">'+
           ' </div>'+
        '</div>'+
       '</div>'+
    '</div>';
    $("body").append(modalSessionHtml);
    
      

            
    



