/* adminPanel _object_ permet l'initialisation et la navigation entre les différentes vues 
des différents espaces d'administrations*/ 
var adminPanel = {
	params:{},
	init : function(){
		if(typeof paramsAdmin.add != "undefined")
			adminPanel.initAddButton(".contain-admin-add");
		
		//connect all actions to buttons
		adminPanel.bindButtonMenu();

		//open de proper page 
		adminPanel.initView();	
	},
	initAddButton : function(domContain){
		var menuButtonCreate="";
		let hexColorRegex = /#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{3})/g;
		let context = "";
		let type = "";
	    $.each(typeObj, function(e,v){
			let colorTest = hexColorRegex.test(v.color);
	        if(typeof v.add != "undefined" && v.add){
                var hash=(typeof v.hash !== "undefined") ? v.hash : "javascript:;";
                var formType=(typeof v.formType != "undefined") ? 'data-form-type="'+v.formType+'" ' : "";
                var subFormType= (typeof v.formSubType != "undefined") ? 'data-form-subtype="'+v.formSubType+'" ' : "";
                var addClass = (typeof v.class !== "undefined") ? v.class : "";
                var nameLabel=(typeof v.addLabel!= "undefined") ? v.addLabel : v.name;
                var actionClass = (typeof v.optionModal!= "undefined") ? "btn-open-optionModal"  : 'btn-open-form';
				var colorClass = (!colorTest) ? 'text-'+v.color : "";
				var colorStyle = (colorTest) ? "style='color: "+v.color+"'" : "";
				if (hash !== "javascript:;" && hash.includes("invite")) {
					if (jsonHelper.pathExists("costum.typeObj")) {
						Object.keys(costum.typeObj).map(function(key, value) {
							if (typeObj.get(key).ctrl === "person") {
								colorStyle = (jsonHelper.pathExists("costum.typeObj."+key+".color")) ? "style='color: "+costum.typeObj[key].color+"'" : "style='color: #ea4335'";
								v.icon = (jsonHelper.pathExists("costum.typeObj."+key+".icon")) ? costum.typeObj[key].icon : "plus-circle";
							}
						})
					}
					context = (typeof contextId !== "undefined") ? contextId : (jsonHelper.pathExists("costum.contextId")) ? costum.contextId : "";
					type = (typeof contextType !== "undefined") ? contextType : (jsonHelper.pathExists("costum.contextType")) ? costum.contextType : "";
					let hrefLink = hash;
					if (context !== "" && type !== "") {
						hrefLink = v.hash+'.type.'+type+'.id.'+context;
					}
					menuButtonCreate+='<a '+colorStyle+' href="'+hrefLink+'"'+ 
                    'class="btn '+addClass+' col-xs-6 col-sm-6 col-md-4 col-lg-4  margin-bottom-10">'+ 
                        '<h6><i class="fa fa-'+v.icon+'"></i><br/>'+
                        nameLabel+'</h6>'+
                    '</a>';
				} else {
					menuButtonCreate+='<button '+colorStyle+' '+ 
                    formType+
                    subFormType+ 
                    'class="btn btn-link '+actionClass+' col-xs-6 col-sm-6 col-md-4 col-lg-4 '+colorClass+' margin-bottom-10">'+ 
                        '<h6><i class="fa fa-'+v.icon+'"></i><br/>'+
                        nameLabel+'</h6>'+
                    '</button>';
				}
	        }
	    });
	    $(domContain).html(menuButtonCreate);
	},
	initView: function(){
		adminPanel.initNav();

		if(adminPanel.params.view!=""){
			adminPanel.views[adminPanel.params.view]();
		}else{
			adminPanel.views.index();
		}
	},
	initNav:function(){
		coInterface.showLoader("#content-view-admin");
		if(adminPanel.params.view=="index" || adminPanel.params.view==""){
			$("#goBackToHome, #content-view-admin").hide(700);
			$("#navigationAdmin").show(700);
			//$("#goBackToHome .addServices, #goBackToHome .show-form-new-circuit").hide(700);
		} else {
			$("#navigationAdmin").hide(700);
			$("#goBackToHome, #content-view-admin").show(700);
		}
	},
	bindViewActionEvent:function(){
		$(".btnNavAdmin").off().on("click", function(){
			adminPanel.params.view = $(this).data("view");
			adminPanel.params.dir = $(this).data("dir");	
			if(!notNull($(this).data("action"))){
				onchangeClick=false;
				adminPanel.initNav();
				var hashAdmin=adminPanel.params.hashUrl;
				var indexView=(notEmpty(adminPanel.params.subView)) ? "subview" : "view";
				if(notEmpty(adminPanel.params.view) && adminPanel.params.view!="index") hashAdmin+="."+indexView+"."+$(this).data("view");
				location.hash=hashAdmin;
			}
			//Get view admin
			//Opens the page
			adminPanel.views[adminPanel.params.view]();
		});
	},
	bindButtonMenu : function(){
		adminPanel.bindViewActionEvent();
		coInterface.bindButtonOpenForm();
	},


	//views are all pages available 
	//for navigation 
	views : {
		index: function(){
			adminPanel.initNav();
		},
		directory : function(){
			var data={
				title : "Gestion des utilisateurs-trices",
				//types : [ "citoyens" ],
				table : {
		            name: {
		                name : "Nom"
		            },
		            email : { 
		            	name : "E-mail"
		            }
		        },
		        actions:{
					switchToUser : true,
					banUser : true,
					deleteUser : true
				},
		        paramsFilter : {
					container : "#filterContainer",
					defaults : {
						types : [ "citoyens" ],
						fields : [ "name", "email", "collection" ],
						sort : { name : 1 }
					},
					filters : {
						text : true
					}
				}
			};
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
		},
		community:function(dom){
			//var connectLink=;
			var data={
		        title : trad.community,
		        context : {
		        	id : costum.contextId,
		        	collection : costum.contextType
		        },
		        invite : {
					contextId : costum.contextId,
					contextType : costum.contextType,
		        },
				community : {
					contextId : costum.contextId,
					contextType : costum.contextType,
					contextSlug : costum.contextSlug
		        },
				kanban : {
					contextId : costum.contextId,
					contextType : costum.contextType,
					contextSlug : costum.contextSlug
		        },
		        table : {
		            name: {
		                name : "Membres"
		            },
		            tobeactivated : {
		                name : "Validation de compte",
		                class : "col-xs-2 text-center"
		            },
		            isInviting : {
		                name : "Validation pour être membres",
		                class : "col-xs-2 text-center"
		            },
		            roles : {
		                name : "Roles",
		                class : "col-xs-1 text-center"
		            },
		            admin : {
		                name : "Admin",
		                class : "col-xs-1 text-center"
		            }
		        },
		        paramsFilter : {
					container : "#filterContainer",
					defaults : {
						types : [ "citoyens"],
						fields : [ "name", "email", "links", "collection" ],
						notSourceKey : true,
						indexStep: 50
					},
					filters : {
						text : true
					}
				},
		        actions : {
		            admin : true,
		            // roles : true,
		            disconnect : true
		        }
		    };

		    data.paramsFilter.defaults.filters = {};

		    if(costum.contextType=="projects" || costum.contextType=="events"){
		    	data.paramsFilter.defaults.filters["links."+costum.contextType+"."+costum.contextId] = {
					'$exists' : 1 
				}
		    }else{
		    	data.paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {
					'$exists' : 1 
				};
		    }

		    var searchAdminType=(typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["community"] != "undefined"
			&& typeof paramsAdmin["menu"]["community"]["initType"] != "undefined") ? paramsAdmin["menu"]["community"]["initType"]: ["citoyens"];
			data.paramsFilter.defaults.types=searchAdminType;
			let domTarget=(notEmpty(dom)) ? dom :'#content-view-admin'; 
		  	ajaxPost(domTarget, baseUrl+'/'+moduleId+'/admin/directory', data, function(){
		  		if(typeof costum.htmlConstruct!="undefined" && 
					typeof costum.htmlConstruct.adminPanel!="undefined"  && 
				    typeof costum.htmlConstruct.adminPanel.menu!="undefined" &&
					typeof costum.htmlConstruct.adminPanel.menu.community!="undefined" && 
					costum.htmlConstruct.adminPanel.menu.community.mailing){
					ajaxPost(null, baseUrl+'/'+moduleId+'/admin/mailing', {}, function(memberMailingHtml){
						$('#content-view-admin').append(memberMailingHtml);
					},"html");
				}
		  	},"html");
		},
		reference : function(){
			var searchAdminType=(typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["reference"] != "undefined"
			&& typeof paramsAdmin["menu"]["reference"]["initType"] != "undefined") ? paramsAdmin["menu"]["reference"]["initType"]: ["organizations", "events", "projects"];
	
			var filterAdmin = (typeof paramsAdmin != "undefined" 
			&& typeof paramsAdmin["menu"] != "undefined" 
			&& typeof paramsAdmin["menu"]["reference"] != "undefined"
			&& typeof paramsAdmin["menu"]["reference"]["filters"] != "undefined") ? paramsAdmin["menu"]["reference"]["filters"]: [];
			var data={initType:searchAdminType,filters:filterAdmin};
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/reference', data, function(){});

		},
		pledge : function(){

		  var data={
		    title : "Promesses de don",
		    //types : ["crowdfunding"],
		    table : {
		      name: {
		        name : "Nom",
		        preview : true
		      },
		      amount : {
		        name : "Montant",
		        sum : true
		      },
		      type : {
		        name : "type",
		        preview : true
		      }
		      // validateField :{
		      //   field : "type",
		      //   value : "donation"
		      // }

		    },  
		    actions:{
		       delete : true,
		       validatePledge : true
		    },
		    paramsFilter : {
		        container : "#filterContainer",
		        options : {
			 		tags : {
			 			verb : '$all'
			 		}
			 	},
		        defaults : {
		          types : [ "crowdfunding" ],
		          //type : "pledge"
		          filters : {
		            type : "pledge"
		          }
		        },
		        filters : {
			 		publicData : {
			 			view : "dropdownList",
			 			type : "filters",
			 			name : "Données publiques",
			 			//event : "selectList",
			 			field : "publicDonationData",
			 			action : "filters",
           				event : "filters",
			 			keyValue : false,
			 			list : {
			 				"true" : "Données publiques",
			 				"false" : "Données privées"
			 			}
			 		},
			 		invoice : {
			 			view : "dropdownList",
			 			type : "filters",
			 			name : "Demande de facture",
			 			//event : "selectList",
			 			field : "invoice",
			 			action : "filters",
            			event : "filters",
			 			keyValue : false,
			 			list : {
			 				"true" : "Facture demandée",
			 				"false" : "Facture non demandée"
			 			}

			 			
			 		}
	 			}
		      }
		      
		  };
		  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
		},
		donation : function(){

		  var data={
		    title : "Dons validés",
		    //types : ["crowdfunding"],
		    table : {
		      name: {
		        name : "Nom",
		        preview : true
		      },
		      amount : {
		        name : "Montant",
		        sum : true
		      },
		      type : {
		        name : "type",
		        preview : true
		      }
		    },  
		    actions:{
		       delete : true
		    },
		    options : {
			 	tags : {
			 		verb : '$all'
			 	}
			},
		    paramsFilter : {
		        container : "#filterContainer",
		        defaults : {
		          types : [ "crowdfunding" ],
		          //type : "pledge"
		          filters : {
		            type : "donation"
		          }
		        },
		        filters : {
			 		publicData : {
			 			view : "dropdownList",
			 			type : "filters",
			 			name : "Données publiques",
			 			//event : "selectList",
			 			field : "publicDonationData",
			 			action : "filters",
           				event : "filters",
			 			keyValue : false,
			 			list : {
			 				"true" : "Données publiques",
			 				"false" : "Données privées"
			 			}
			 		},
			 		invoice : {
			 			view : "dropdownList",
			 			type : "filters",
			 			name : "Demande de facture",
			 			//event : "selectList",
			 			field : "invoice",
			 			action : "filters",
            			event : "filters",
			 			keyValue : false,
			 			list : {
			 				"true" : "Facture demandée",
			 				"false" : "Facture non demandée"
			 			}

			 			
			 		}
	 			}
		    }
		      
		  };
		  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
		},
		log : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/log/monitoring', null, function(){});
		},
		moderate : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/moderate/one', null, function(){});
		},
		addData:function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/adddata', null, function(){});
		},
		import: function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/createfile', null, function(){});
		},
		mailerror : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/mailerrordashboard', null, function(){});
		},
		notsendmail : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/notsendmail', null, function(){});
		},
		mailslist : function(){ 
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/mailslist', null, function(){});
		},
		statistic : function(){
				ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/app/info/p/stats', null, function(){});
		},
		mailing : function(){
		  	ajaxPost('#content-view-admin-community-mailing', baseUrl+'/'+moduleId+'/admin/communityMailing', {}, function(){},"html");
		},
		activitypubdashboard : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/activitypubdashboard', null, function(){});
		},
		spamobservatoire : function(){
			ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/spamobservatoire', null, function(){});
		},
	}
};
