$(function () {
	function is_hidden(date_dom) {
		var parent_dom = date_dom.parent();
		var parent_width = parent_dom.width();
		var offset = (date_dom.offset().left - parent_dom.offset().left) + date_dom.outerWidth(true);
		return (offset > parent_width);
	}
	function do_update_navigation() {
		$('.df-container').each(function () {
			var container_dom = $(this);
			var date_list_dom = container_dom.find('.df-date');
			var navigation_dom = container_dom.find('.df-navigation');
			navigation_dom.hide();
			date_list_dom.each(function () {
				if (is_hidden($(this))) {
					navigation_dom.css('display', '');
					return (false);
				}
			})
		});
	}
	$('.df-navigation').on('click', function () {
		var self = $(this);
		var dates_dom = self.parent().find('.df-dates');
		var current_scroll = dates_dom.scrollLeft();
		const transition_duration = 500;
		if (self.hasClass('df-next'))
			dates_dom.animate({scrollLeft: current_scroll + dates_dom.width()}, transition_duration);
		else if (self.hasClass('df-prev'))
			dates_dom.animate({scrollLeft: current_scroll - dates_dom.width()}, transition_duration);
	});
	window.addEventListener('resize', do_update_navigation);
	do_update_navigation();
})