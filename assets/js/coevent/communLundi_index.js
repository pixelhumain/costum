costum.communLundi={
    init: function(){
        getTemplateEvents = function(){
        ajaxPost(
            null,
            baseUrl +"/"+moduleId+ "/search/globalautocomplete",
            {"searchType":["events"],filters:{"tags":"Atelier Comm'un Lundi récurrent"}},
            function(res){
                // costum.lists.pattern={};
                $.each(res.results,function(k,v){
                    costum.lists.pattern[k]=v.name;
                });
            }

        );
        }
        getTemplateEvents();
        eventTypes={
            feedback : "feedback",
            training : "training",
	        presentation : "presentation",
	        contribution : "contribution",
            getTogether : "getTogether"
        };
        // alert($('#mainNav').length);
        costum.initHtmlPosition = function(){
            $('#mainNav').affix({
                offset: {
                  top: $("#mainNav").height()
                }
            }).on('affixed.bs.affix', function(){
                $(this).attr("style","background-color:white !important; border-bottom:solid 1px #01305E !important;");
                // $(this).css("background-color","white");
    
            }).on('affixed-top.bs.affix', function(){
                $(this).attr("style","background-color:transparent !important;");
                // $(this).css("background-color","transparent");
            });

        };

        
        

        // show connection button with tiers-lieux.org
        $("#authclientCo").css({"display": "block","position": "relative"});
        $("#authclientCo").prev().css("display", "none");

        coInterface.bindButtonOpenForm = function(){
			$(".btn-open-form").off().on("click",function(){
				
		        var typeForm = $(this).data("form-type");
		        currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
		        //dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
		        if(contextData && contextData.type && contextData.id ){
		            if(typeForm=="event"){
						dyFObj.openForm(typeForm,"afterLoad");
					}else{
		                dyFObj.openForm(typeForm,"sub");
					}	
		        }
                else if(typeForm=="event" && location.hash.split(".")[0]=="#admin"){
                    var adminHash=location.hash.split(".");
                    typeElt=typeObj[typeForm].col;
                    if(adminHash[2]=="templateEvent"){
                        var editDynF=jQuery.extend(true, {},costum.typeObj[typeElt].dynFormCostum);
                        editDynF.onload.actions.hide.patternselect = 1;
                        editDynF.onload.actions.hide.infocustom = 1;
                        editDynF.onload.actions.setTitle="Nouvel événement récurrent";
                            editDynF.beforeBuild.properties.template=dyFInputs.checkboxSimple("true", "template", {
                                "onText": trad.yes,
                                "offText": trad.no,
                                "onLabel": tradDynForm.public,
                                "offLabel": tradDynForm.private,
                                "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
                                "labelInformation": tradDynForm.explainvisibleevent
                            });
                        presetVal={
                            startDate : moment().format("DD/MM/YYYY HH:mm"),
                            endDate : moment().add(1,"hour").format("DD/MM/YYYY HH:mm")
                        };
                        dyFObj.openForm(typeForm,null,presetVal,null,editDynF);
                    }else{
                        dyFObj.openForm(typeForm,"afterLoad");
                    }
                }
		        else{
		        	if(typeForm=="event"){
						dyFObj.openForm(typeForm,"afterLoad");
					}else{
		                dyFObj.openForm(typeForm);
					}
		        }
		    });
		};  
    },
    "events" : {
        formData : function(data){
            mylog.log("formData events", data);
            if(typeof data.template!="undefined"){
                // alert("template");
                if(typeof data.tags=="undefined" || data.tags==null){
                    data.tags=[];
                }
                //    alert("tags");
                if(data.template=="true"){
                    data.tags.push("Atelier Comm'un Lundi récurrent");
                }else if(data.template=="false"){
                    data.tags=data.tags.filter((tag) => tag!=="Atelier Comm'un Lundi récurrent");
                }
            }else{
                if(typeof data.tags!="undefined" && data.tags.length>0){
                    delete data.tags;
                }
            }
            if(typeof data.parent!="undefined"){
                data.parent[costum.contextId]={
                    "type" : costum.contextType,
                    "name" : costum.title
               }
            }
            return data;
        },
        afterBuild : function(data){
            mylog.log("afterbuild event communLundi", data);
            // alert("afterbuild");
            dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
                // // -------- start original onload -------
                // if ($("#ajaxFormModal #recurrency").is(':checked')) {
                //     $("#ajaxFormModal .allDaycheckbox").fadeOut("slow");
                //     $("#ajaxFormModal .startDatedatetime").fadeOut("slow");
                //     $("#ajaxFormModal .endDatedatetime").fadeOut("slow");
                //     $("#ajaxFormModal .openingHoursopeningHours").fadeIn("slow");
                // } else {
                //     $("#ajaxFormModal .openingHoursopeningHours").fadeOut("slow");
                //     $("#ajaxFormModal .allDaycheckbox").fadeIn("slow");
                //     $("#ajaxFormModal .startDatedatetime").fadeIn("slow");
                //     $("#ajaxFormModal .endDatedatetime").fadeIn("slow");
                // }    
                // // -------- end original onload -------    
                mylog.log("afterLoad event communLundi", data);


                $("#ajaxFormModal #btn-submit-form").toggle();
                $("#ajaxFormModal .form-group").toggle();
                $("#ajaxFormModal .openingHoursopeningHours").hide();
                $("#ajaxFormModal .patternselect").show();
                $(".selectPattern").on("change",function(){
                    pattern=$(this).val();
                    typeElt=dyFObj[dyFObj.activeElem].col;
                    ctrlElt=dyFObj[dyFObj.activeElem].ctrl;
                    var editDynF=jQuery.extend(true, {},costum.typeObj[typeElt].dynFormCostum);
                    editDynF.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
                        "onText": "Oui",
                        "offText": "Non",
                        "onLabel": "Il s'agit d'un atelier récurrent",
                        "offLabel": "Il s'agit d'un atelier ponctuel",
                        "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
                        "labelInformation": "Votre atelier apparaîtra dans la liste des ateliers récurrents. Le formulaire de création d'atelier sera alors pré-rempli avec les informations que vous venez de compléter. Vous n'aurez plus qu'à préciser les spécificités de votre prochain atelier (dates, informations spécifiques, ...)"
                    });

                    var presetData={};
                    if ($('.program-content .event-hour').length) {
                        // var selected_date = moment($('#event-content<?= $blockKey ?> .dinalternate.date').text(), 'DD.MM.YYYY');
                        // selected_date.hour(parseInt($('.event-hour:last').data('last-hour')));
                        var last_schedule = $('.program-content .event-hour:last').data('last-schedule');
                        // presentValuesEvent.startDate = selected_date.format('DD/MM/YYYY HH:mm');
                        presetData.startDate = last_schedule;
                        presetData.endDate = moment(last_schedule, 'DD/MM/YYYY HH:mm').add(1, 'hours').format('DD/MM/YYYY HH:mm');
                    }

                    if(pattern=="Aucun"){
                        
                        editDynF.onload.actions.hide.patternselect = 1;
                        // if(costum.isCostumAdmin){
                            
                        // }
                        
                        // $.each(['startDate', 'endDate'], function(_, key) {
						// 	if (data[key])
						// 		presetData[key] = moment(data[key]).format('DD/MM/YYYY HH:mm');
						// });
                        dyFObj.openForm(ctrlElt, null, presetData, null, editDynF);
                    }else{
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/element/get/type/"+typeElt+"/id/"+pattern,
                        {},
                        function(res){
                            mylog.log("res elt",res);
							var maped = res.map;
							$.each(['startDate', 'endDate'], function(_, key) {
								if (maped[key])
									maped[key] = moment(maped[key]).format('DD/MM/YYYY HH:mm');
							});
                            presetData=$.extend(true, maped,presetData);
                            // dyFObj.closeForm();
                            // var elt=res[Object.keys(res)[0]];
                            // mylog.log("res elt prep",elt);
                            // var editDynF=jQuery.extend(true, {},costum.typeObj[typeElt].dynFormCostum);
                            editDynF.onload.actions.hide.patternselect = 1;
                            editDynF.onload.actions.hide.similarLinkcustom = 1;
                            editDynF.onload.actions.hide.templatecheckboxSimple = 1;
                            // editDynF.onload.actions.presetValue={
                            //     "tags":{
                            //         "value":"",
                            //         "forced":true
                            //     }
                            // };
                            editDynF.beforeBuild.properties.tags.values=[];

							// $.each(['startDate', 'endDate'], function(_, key) {
							// 	if (data[key])
							// 		presetData[key] = moment(data[key]).format('DD/MM/YYYY HH:mm');
							// });
                            // if(data.template){
                                presetData.template="false";
                            // }
                            dyFObj.openForm(ctrlElt,null,presetData, null, editDynF);
                            // $("#similarLink").hide();
                        }
                    );
                    }
                    
                });
            };    
            $(dyFObj.activeModal).addClass('in');
        },
        afterSave : function(data){
            mylog.log("aftersave events",data);
            urlCtrl.loadByHash(location.hash,null,getTemplateEvents);
            // getTemplateEvents();

        }    
    }
};

defaultCoevent = '';