adminPanel.views.templateEvent = function(){
	var data={
		title : "Ateliers réccurents",
		table : {
            name : {
                name : "Nom"
            },
            shortDescription : {
               name : "Description courte"
            },
            organizer: {
                name : "Structure organisatrice"
            },
            email : {
                name : "Contact animateur"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            header : {
                dom : ".headerSearchContainerAdmin",
               options : {
                   left : {
                       classes : 'col-xs-8 elipsis no-padding',
                       group:{
                           count : true
                       }
                   },
                   right : {
                       classes : 'col-xs-4 text-right no-padding',
                       group : { 
                           add : {
                               label : "Ajouter un atelier<br/>(et l'enregistrer en tant qu'atelier récurrent)"
                           }
                       }
                   }
               }
            },
            defaults : {
                types : [ "events" ],
                filters : {
                    "tags" : "Atelier Comm'un Lundi récurrent"
                },
                fields : [ "name", "organizer", "collection", "email", "tags", "shortDescription" ]
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
        	delete : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.events = function(){
	var data={
		title : "Tous les événements",
		table : {
            name : {
                name : "Nom"
            },
            shortDescription : {
                name : "Description courte"
            },
            organizer: {
                name : "Structure organisatrice"
            },
            email : {
                name : "Contact animateur"
            },
            template :{
                name : "Atelier récurrent"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            header : {
                dom : ".headerSearchContainerAdmin",
               options : {
                   left : {
                       classes : 'col-xs-8 elipsis no-padding',
                       group:{
                           count : true
                       }
                   },
                   right : {
                       classes : 'col-xs-4 text-right no-padding',
                       group : { 
                           add : {
                               label : "Ajouter un atelier"
                           }
                       }
                   }
               }
            },
            defaults : {
                types : [ "events" ],
                fields : [ "name", "organizer", "collection", "email", "tags", "shortDescription" ]
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
        	delete : true,
            templatizeEvent : true
        }
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminDirectory.values.organizer=function(e,id,type,aObj){
    mylog.log("values organizer",e);

    var str='';
    if(typeof e.organizer!="undefined"){
        var nbOrga=Object.keys(e.organizer).length;
        var i=0;
        $.each(e.organizer,function(id,v){
            var coma=(i<nbOrga-1) ? ", " : "";
            str+='<span><a target="_blank" href="#page.type.'+v.type+'.id.'+id+'">'+v.name+'</a>'+coma+'</span>';
            i++;
        })
    }
    
    return str;
};

adminDirectory.values.template=function(e,id,type,aObj){
    mylog.log("values template",e);

    var str='Non';
    if(typeof e.tags!="undefined" && e.tags.indexOf("Atelier Comm'un Lundi récurrent")>-1){
        str='Oui';
    }
    
    return str;
};

adminDirectory.actions.templatizeEvent=function(e, id, type, aObj){
    mylog.log("adminDirectory.actions.templatizeEvent", id, type, e);
    var boolTpl=(typeof e.tags!="undefined" && e.tags.indexOf("Atelier Comm'un Lundi récurrent")>-1) ? 1 : 0 ;
    var labelTpl=(typeof e.tags!="undefined" && e.tags.indexOf("Atelier Comm'un Lundi récurrent")>-1) ? "Enlever des atelier recurrents" : "Transformer en atelier récurrent" ;
			var str ='<button data-id="'+id+'" data-type="'+type+'" data-template='+boolTpl+' class="col-xs-12 templatizeBtn btn bg-orange text-white">'+labelTpl+'</button>';
			
			return str;
};

adminDirectory.events.templatizeEvent=function(aObj){
    mylog.log("adminDirectory.events.templatizeEvent", aObj);
			//mylog.log(e);
            $("#"+aObj.container+" .templatizeBtn").off().on("click", function(){
                mylog.log("template bool", $(this).data("template"));
                var valTag=($(this).data("template")) ? {'$pullAll':["Atelier Comm'un Lundi récurrent"]} : "Atelier Comm'un Lundi récurrent" ;
                var params={
                    id:$(this).data("id"),
                    type:$(this).data("type"),
                    name : "tags",
                    value : valTag
                };
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/element/updatefield",
                    params,
                    function(data){
                        mylog.log("templatize callback", data);
                        var msgCb=(typeof data.tags=="Object" && typeof data.tags['$pushAll']!="undefined") ? "enlevé des ateliers récurrents" : "enregistré en tant qu'atelier récurrent";
                        toastr.success("Bien "+msgCb);
                        urlCtrl.loadByHash(location.hash);

                    }
                );    

            });
};



adminDirectory.events.update= function(aObj){
    $("#"+aObj.container+" .updateBtn").off().on("click", function(){
        mylog.log("adminDirectory..updateBtn customize", $(this).data("id"), $(this).data("type"));
        var editDynF=jQuery.extend(true, {},costum.typeObj["events"].dynFormCostum);
                        editDynF.onload.actions.hide.patternselect = 1;
                        if(costum.isCostumAdmin){
                            editDynF.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
                                "onText": trad.yes,
                                "offText": trad.no,
                                "onLabel": tradDynForm.public,
                                "offLabel": tradDynForm.private,
                                "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
                                "labelInformation": tradDynForm.explainvisibleevent
                            })
                        }
        dyFObj.editElement($(this).data("type"), $(this).data("id"), null,editDynF);
    });
};