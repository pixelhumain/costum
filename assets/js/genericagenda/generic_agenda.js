function genericAgenda(gantt_component, $, W, blockKey, kunik, globals, wsCO) {
	"use strict";
	/**
	 * @typedef {Object} FilterView
	 * @property {string} color Code hexadécimal de la couleur associée
	 * @property {string} [name] Nom à afficher ; Si c'est un event, c'est la traduction du type d'event
	 * @property {string} [info] Information à affichier au survol du filtre
	 * @property {number} count Nombre d'élément contenu dans la catégorie
	 * @property {string} value Si c'est un projet: identifian du projet
	 */
	/**
	 * @typedef {Object} Task
	 * @property {boolean} checked Status de la sous-tâche si elle est terminée ou pas
	 * @property {string} color Code hexadécimal de la couleur associée
	 * @property {string} endDate Date de fin
	 * @property {string} id Identifiant de la sous-tâche
	 * @property {string} name Titre ou nom
	 * @property {string} startDate Date de début
	 */
	/**
	 * @typedef {Object} Action
	 * @property {string} id Identifiant de l'action
	 * @property {string} color Code hexadécimal de la couleur associée
	 * @property {string} description Description longue (complète)
	 * @property {string} endDate Date de fin
	 * @property {string} group Nom du projet qui la porte
	 * @property {string} name Titre ou nom
	 * @property {string} profilImageUrl Image de présentation
	 * @property {string} shortDescription Une description courte
	 * @property {string} slug Slug
	 * @property {string} startDate Date de début
	 * @property {Task[]} subs Liste des sous-tâches
	 */
	/**
	 * @typedef {Object} ViewElement
	 * @property {string} id Identifiant
	 * @property {string} name Titre ou nom de l'élément
	 * @property {string} color Code hexadécimal de la couleur associée
	 * @property {string} description Description longue (complète)
	 * @property {string} endDate Date de fin
	 * @property {string} profilImageUrl Image de présentation
	 * @property {string} shortDescription Une description courte
	 * @property {string} slug Slug
	 * @property {string} startDate Date de début
	 * @property {number} original_created Timestamp de la date de création
	 * @property {Action[]} [subs] Si c'est un projet il contient toutes les actions
	 * @property {string} [group] Si c'est un event: le group est le nom traduit du type d'évènement
	 * @property {string} [type] Si c'est un event: c'est la représentation du type de l'event depuis la base de données (pas traduit)
	 */

	var searchLastValue = '';
	gantt_component.plugins({
		tooltip: true
	});
	gantt_component.config.date_format = "%Y-%m-%d %H:%i";
	gantt_component.templates.tooltip_text = function (start, end, task) {
		return dataHelper.printf("<b>Tache:</b> {{text}}<br /><b>Durée:</b> {{duration}} {{jours}}<br><b>Progression : </b> {{progress}}%", {
			text: task.text,
			duration: task.duration,
			jours: task.duration === 1 ? 'Jour' : 'Jours',
			progress: Math.floor(task.progress * 100)
		});
	};
	var viewLoadings = {};
	viewLoadings[dataHelper.printf('calendar{{blockKey}}', { blockKey: blockKey })] = function (__params) {
		var key;
		var check_filters;
		var params;
		var calendarDom;
		var calendarDomSelector;

		if (typeof __params === 'undefined')
			__params = { reload: false };
		else
			__params = $.extend(true, {}, { reload: false }, __params);
		calendarDomSelector = dataHelper.printf('#calendar_container{{blockKey}}', { blockKey: blockKey });
		if (!$(calendarDomSelector).parents('.tab-pane').hasClass('load') && !__params.reload)
			return true;
		calendarDom = $(calendarDomSelector);
		calendarDom.parents('.tab-pane').removeClass('load');
		if (calendarDom.length > 0) {
			var search_field_changed = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val() !== searchLastValue;
			if (search_field_changed || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(show_loader());
			}
			if (__params.reload) {
				calendarDom.fullCalendar('destroy');
				calendarDom.html(show_loader());
			}
			// dans un costum classic et non dans un appel à projet ou seulement events
			if (globals.cmsData.content_type === 'events') {
				params = {
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				check_filters = loadStorage();
				for (key in check_filters) {
					if (!check_filters[key]) {
						params.filter.exclude.push(key);
					}
				}

				getEvents(params).then(function (eventsList) {
					if (__params.reload) {
						calendarDom.empty().html('');
					}
					var groups = eventsList.groups.map(function (group) {
						group.name = W.tradCategory[group.value];
						return group;
					});
					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
						loadFilterView(groups);
					}
					load_calendar(eventsList.data, 'event');
				});
			} else if (globals.cmsData.content_type === 'projects') {
				params = {
					reorder: 1
				};

				if (W.costum.contextType === 'organizations') {
					params.request = 'all';
					params.filter = {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					};
					check_filters = loadStorage();

					var temporaryKey;
					if (typeof W.costum.type !== 'undefined' && W.costum.type === 'aap') {
						get_projects_ids().then(function (__ids) {
							for (temporaryKey in check_filters) {
								if (!check_filters[key] && __ids.includes(temporaryKey)) {
									params.filter.exclude.push(temporaryKey);
								}
							}
							params.filter.ids = __ids;

							if (__ids.length) {
								get_actions(params).then(function (__actions) {
									if (__params.reload) {
										calendarDom.empty();
									}
									var data = [];

									__actions.data.forEach(function (project) {
										data = data.concat(project.subs);
									});

									if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
										loadFilterView(__actions.groups);
									}
									load_calendar(data, 'action');
								});
							} else {
								if (__params.reload) {
									calendarDom.empty();
								}
								$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
								load_calendar([], 'action');
							}
						});
					} else {
						for (temporaryKey in check_filters) {
							if (!check_filters[temporaryKey]) {
								params.filter.exclude.push(temporaryKey);
							}
						}
						get_actions(params).then(function (__actions) {
							if (__params.reload) {
								calendarDom.empty();
							}
							var data = [];

							__actions.data.forEach(function (project) {
								data = data.concat(project.subs);
							});

							if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
								loadFilterView(__actions.groups);
							}
							load_calendar(data, 'action');
						});
					}
				} else {
					get_actions(params).then(function (__actions) {
						if (__params.reload) {
							calendarDom.empty().html('');
						}
						load_calendar(__actions, 'action');
					});
				}
			} else if (globals.cmsData.content_type === 'event_actions') {
				params = {
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				check_filters = loadStorage();
				for (key in check_filters) {
					if (!check_filters[key]) {
						params.filter.exclude.push(key);
					}
				}
				getEventActions(params).then(function (consumer) {
					if (__params.reload) {
						calendarDom.empty().html('');
					}
					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0 || typeof __params.filter_reload !== 'undefined' && __params.filter_reload) {
						loadFilterView(consumer.groups);
					}
					load_calendar(consumer.data, 'action');
				});
			}
		}
	};
	viewLoadings[dataHelper.printf('vertical_timeline{{blockKey}}', { blockKey: blockKey })] = function () {
		var timeline = dataHelper.printf('#vertical_timeline_container{{blockKey}}', { blockKey: blockKey });
		var params;
		var checkFilters;
		var key;

		if (!$(timeline).parents('.tab-pane').hasClass('load')) {
			return true;
		}
		$(timeline).parents('.tab-pane').removeClass('load');

		if ($(timeline).length > 0) {
			var search_field_changed = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val() !== searchLastValue;
			if (search_field_changed) {
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(show_loader());
			}
			// dans un costum classic et non dans un appel à projet ou seulement events
			if (globals.cmsData.content_type === 'events') {
				params = {
					reorder: 1,
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				checkFilters = loadStorage();
				for (key in checkFilters) {
					if (!checkFilters[key]) {
						params.filter.exclude.push(key);
					}
				}

				getEvents(params).then(function (__events) {
					var groups = __events.groups.map(function (__group) {
						__group.name = W.tradCategory[__group.value];
						return __group;
					});

					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
						loadFilterView(groups);
					}
					loadVerticalTimeline(__events.data, 'event');
				});
			} else if (globals.cmsData.content_type === 'projects') {
				params = {
					reorder: 1
				};

				if (W.costum.contextType === 'organizations') {
					params.request = 'all';
					params.filter = {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					};
					checkFilters = loadStorage();

					if (typeof W.costum.type !== 'undefined' && W.costum.type === 'aap') {
						get_projects_ids().then(function (__ids) {
							for (var key in checkFilters) {
								if (!checkFilters[key] && __ids.includes(key)) {
									params.filter.exclude.push(key);
								}
							}
							params.filter.ids = __ids;

							if (__ids.length) {
								get_actions(params).then(function (__actions) {
									var data = [];

									__actions.data.forEach(function (project) {
										data = data.concat(project.subs);
									});

									if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
										loadFilterView(__actions.groups);
									}
									loadVerticalTimeline(data, 'action');
								});
							} else {
								$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
								loadVerticalTimeline([], 'action');
							}
						});
					} else {
						for (key in checkFilters) {
							if (!checkFilters[key]) {
								params.filter.exclude.push(key);
							}
						}
						get_actions(params).then(function (__actions) {
							var data = [];
							__actions.data.forEach(function (project) {
								data = data.concat(project.subs);
							});

							if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
								loadFilterView(__actions.groups);
							}
							loadVerticalTimeline(data, 'action');
						});
					}
				} else {
					get_actions(params).then(function (__events) {
						loadVerticalTimeline(__events, 'action');
					});
				}
			} else if (globals.cmsData.content_type === 'event_actions') {
				params = {
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				checkFilters = loadStorage();
				for (key in checkFilters) {
					if (!checkFilters[key]) {
						params.filter.exclude.push(key);
					}
				}
				getEventActions(params).then(function (consumer) {
					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
						loadFilterView(consumer.groups);
					}
					loadVerticalTimeline(consumer.data, 'action');
				});
			}
		}
	};
	viewLoadings[dataHelper.printf('horizontal_timeline{{blockKey}}', { blockKey: blockKey })] = function () {
		var timeline = dataHelper.printf('#horizontal_timeline_container{{blockKey}}', { blockKey: blockKey });
		var key;
		var params;
		var checkFilters;

		if (!$(timeline).parents('.tab-pane').hasClass('load'))
			return true;
		$(timeline).parents('.tab-pane').removeClass('load');
		if ($(timeline).length > 0) {
			var search_field_changed = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val() !== searchLastValue;
			if (search_field_changed) {
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(show_loader());
			}
			$(timeline).html(show_loader());
			// dans un costum classic et non dans un appel à projet ou seulement events
			if (globals.cmsData.content_type === 'events') {
				params = {
					reorder: 1,
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				checkFilters = loadStorage();
				for (key in checkFilters) {
					if (!checkFilters[key]) {
						params.filter.exclude.push(key);
					}
				}

				getEvents(params).then(function (__events) {
					$(timeline).empty();
					var groups = __events.groups.map(function (__group) {
						__group.name = W.tradCategory[__group.value];
						return __group;
					});
					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
						loadFilterView(groups);
					}
					load_horizontal_timeline(__events.data);
				});
			} else if (globals.cmsData.content_type === 'projects') {
				params = {
					reorder: 1
				};
				if (W.costum.contextType === 'organizations') {
					params.request = 'all';
					params.filter = {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					};
					var check_filters = loadStorage();

					if (typeof W.costum.type !== 'undefined' && W.costum.type === 'aap') {
						get_projects_ids().then(function (__ids) {
							for (var key in check_filters) {
								if (!check_filters[key] && __ids.includes(key)) {
									params.filter.exclude.push(key);
								}
							}
							params.filter.ids = __ids;

							if (__ids.length) {
								get_actions(params).then(function (__actions) {
									$(timeline).empty();
									var data = [];
									__actions.data.forEach(function (project) {
										data = data.concat(project.subs);
									});
									if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
										loadFilterView(__actions.groups);
									}
									load_horizontal_timeline(data);
								});
							} else {
								$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
								$(timeline).empty();
								load_horizontal_timeline([]);
							}
						});
					} else {
						for (key in check_filters) {
							if (!check_filters[key]) {
								params.filter.exclude.push(key);
							}
						}
						get_actions(params).then(function (__actions) {
							$(timeline).empty();
							var data = [];
							__actions.data.forEach(function (project) {
								data = data.concat(project.subs);
							});
							if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
								loadFilterView(__actions.groups);
							}
							load_horizontal_timeline(data);
						});
					}
				} else {
					get_actions(params).then(function (__actions) {
						$(timeline).empty().html('');
						load_horizontal_timeline(__actions);
					});
				}
			} else if (globals.cmsData.content_type === 'event_actions') {
				params = {
					filter: {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					}
				};
				checkFilters = loadStorage();
				for (key in checkFilters) {
					if (!checkFilters[key]) {
						params.filter.exclude.push(key);
					}
				}
				getEventActions(params).then(function (consumer) {
					if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
						loadFilterView(consumer.groups);
					}
					load_horizontal_timeline(consumer.data, 'action');
				});
			}
		}
	};
	viewLoadings[dataHelper.printf('gantt{{blockKey}}', { blockKey: blockKey })] = function (__params) {
		__params = __params ? __params : { reload: false };
		var gantt = dataHelper.printf('#gantt_container{{blockKey}}', { blockKey: blockKey });

		if (!$(gantt).parents('.tab-pane').hasClass('load')) {
			return true;
		}
		$(gantt).parents('.tab-pane').removeClass('load');

		if ($(gantt).length > 0) {
			var search_field_changed = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val() !== searchLastValue;
			if (search_field_changed) {
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(show_loader());
			}
			if (__params.reload) {
				$(gantt).empty().html(show_loader());
			}
			if (globals.cmsData.content_type === 'projects') {
				var params = {
					reorder: 1,
					request: 'actions_tasks'
				};
				if (W.costum.contextType === 'organizations') {
					params.request = 'all';
					params.filter = {
						text: $(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).val(),
						exclude: []
					};
					var check_filters = loadStorage();

					// Pour les appels à projets
					if (typeof W.costum.type !== 'undefined' && W.costum.type === 'aap') {
						get_projects_ids().then(function (__ids) {
							for (var key in check_filters) {
								if (!check_filters[key] && __ids.includes(key)) {
									params.filter.exclude.push(key);
								}
							}
							params.filter.ids = __ids;

							if (__ids.length) {
								get_actions(params).then(function (__actions) {
									if (__params.reload) {
										$(gantt).empty().html('');
									}
									if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
										loadFilterView(__actions.groups);
									}
									load_gantt(__actions.data);
								});
							} else {
								$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl').remove();
								if (__params.reload) {
									$(gantt).empty().html('');
								}
								load_gantt([]);
							}
						});
					} else {
						for (var key in check_filters) {
							if (!check_filters[key]) {
								params.filter.exclude.push(key);
							}
						}
						get_actions(params).then(function (__actions) {
							if (__params.reload) {
								$(gantt).empty().html('');
							}
							if (search_field_changed || $(dataHelper.printf('#cpl{{blockKey}} .cpl', { blockKey: blockKey })).length === 0) {
								loadFilterView(__actions.groups);
							}
							load_gantt(__actions.data);
						});
					}
				} else {
					if (__params.reload) {
						$(gantt).empty().html('');
					}
					get_actions(params).then(load_gantt);
				}
			}
		}
	};

	var coIntereventsFilter = {};
	if (W.notEmpty(W.costum)) {
		coIntereventsFilter[dataHelper.printf('parents.{{costumId}}.type', { costumId: W.costum.contextId })] = W.costum.contextType;
		coIntereventsFilter[dataHelper.printf('parent.{{costumId}}.type', { costumId: W.costum.contextId })] = W.costum.contextType;
		coIntereventsFilter[dataHelper.printf('links.{{costumType}}.{{costumId}}.type', {
			costumId: W.costum.contextId,
			costumType: W.costum.contextType
		})] = W.costum.contextType;
		coIntereventsFilter._id = { $in: globals.organizationMembers };
	}
	var _forms = {
		openagenda: {
			jsonSchema: {
				title: 'Open Agenda',
				description: 'une description',
				icon: 'fa-question',
				properties: {
					api_key: {
						inputType: 'text',
						label: 'Clef d\'API',
						value: W.notEmpty(globals.openagendaApiKey) ? globals.openagendaApiKey : 'e41cc653ed574d53b16034473ceb0dc5',
						info: W.trad['Do not change unless you want to import your private calendar'],
						rules: {
							required: true
						}
					},
					public_agenda: {
						inputType: 'custom',
						html: '<label class="col-xs-12 text-left control-label no-padding"><i class="fa fa-chevron-circle-right"></i> Autre agenda public</label>'
					},
					openagenda_urls: {
						label: 'Slugs ou urls',
						inputType: 'array',
						value: globals.openagendaUrls
					}
				},
				save: function (__form_value) {
					delete __form_value.collection;
					delete __form_value.scope;
					var agenda = {
						api_key: __form_value.api_key,
						urls: W.getArray('.openagenda_urlsarray')
					};
					var parameters = {
						id: W.costum.contextId,
						collection: W.costum.contextType,
						path: 'agendas.openagenda',
						value: agenda

					};
					W.dataHelper.path2Value(parameters, function (__response) {
						if (__response.result) {
							document.location.reload();
						}
					});
				}
			}
		},
		icalendar: {
			jsonSchema: {
				title: 'iCalendar',
				description: 'une description',
				icon: 'fa-question',
				properties: {
					ical: {
						inputType: 'formArray',
						template:
							'<div class="form-group">\n' +
							'    <label>Nom du calendrier</label>\n' +
							'    <input type="text" class="form-control" data-form-control="name" placeholder="Calendar">\n' +
							'</div>\n' +
							'<div class="form-group">\n' +
							'    <label>Lien</label>\n' +
							'    <input type="text" class="form-control" data-form-control="url" placeholder="https://domaine.net/fichier.ics">\n' +
							'</div>',
						mapping: {
							name: 'text',
							url: 'text'
						},
						value: globals.icalendarData
					}
				},
				save: function (__form_value) {
					delete __form_value.collection;
					delete __form_value.scope;

					var parameters = {
						id: W.costum.contextId,
						collection: W.costum.contextType,
						path: 'agendas.icalendars',
						value: __form_value.ical
					};
					W.dataHelper.path2Value(parameters, function () {
						document.location.reload();
					});
				}
			}
		},
		cointerevents: {
			jsonSchema: {
				title: 'Communecter',
				properties: {
					internalSources: {
						inputType: 'finder',
						label: 'Source',
						multiple: true,
						initType: ['projects', 'organizations', 'events'],
						initBySearch: true,
						initMe: false,
						initContext: false,
						initContacts: false,
						openSearch: true,
						search: {
							filters: { $or: coIntereventsFilter }
						},
						values: W.notEmpty(globals.internalEvents) ? globals.internalEvents : {}
					}
				},
				save: function (formValues) {
					delete formValues.collection;
					delete formValues.scope;
					var path2Value = {
						id: W.costum.contextId,
						collection: W.costum.contextType,
						value: {},
						path: 'agendas.communecter'
					};
					for (var id in formValues.internalSources) {
						path2Value.value[id] = formValues.internalSources[id];
					}
					W.dataHelper.path2Value(path2Value, function () {
						document.location.reload();
					});
				}
			}
		}
	};

	function show_loader() {
		return dataHelper.printf('<div class="view_loader" style="width: 100%; text-align: center"><img src="{{baseUrl}}/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>', { baseUrl: W.baseUrl });
	}

	/**
	 * Récupérer tous les identifiants des projets
	 * relatifs au formulaire
	 * @return {Promise.<string[]>}
	 */
	function get_projects_ids() {
		var url = dataHelper.printf('{{baseUrl}}/costum/project/project/request/aap_project_ids', { baseUrl: W.baseUrl });
		var post = {
			costumSlug: W.costum.slug,
			costumType: W.costum.contextType,
			costumId: W.costum.contextId,
			form: typeof W.aapObj !== 'undefined' ? W.aapObj.form._id.$id : W.aapObject.formParent._id.$id
		};
		return new W.Promise(function (__resolve) {
			if (globals.defaultFilters.length) {
				__resolve(globals.defaultFilters);
			} else {
				W.ajaxPost(null, url, post, __resolve);
			}
		});
	}

	/**
	 * @param {Event} group Element
	 * @param {Object} options Options
	 * @param {bool} options.checked L'élément est-il sélectionné ou pas
	 * @param {bool} options.is_external C'est un élément externe ou pas
	 * @return {HTMLDivElement}
	 */
	function cplHtmlTemplate(group, options) {
		const default_option = {
			checked: true,
			is_external: false
		};
		if (typeof options === 'undefined')
			options = default_option;
		else
			options = $.extend(true, {}, default_option, options);
		const check_id = group.value + blockKey;
		mylog.log('Rinelfi debugger', group, blockKey, check_id);
		const container_classlist = ["cpl"];
		if (options.is_external)
			container_classlist.push("external-filter");
		const cpl_check_dom = $("<input>", {
			id: check_id,
			type: "checkbox",
			value: group.value,
			checked: options.checked,
			class: "cpl-check",
		});
		const checkmark_dom = $("<label>", {
			class: "checkmark",
			for: check_id,
			css: {
				border: `3px solid ${W.notEmpty(globals.color) ? globals.color.foreground : group.color}`,
				backgroundColor: options.checked ? (W.notEmpty(globals.color) ? globals.color.background : group.color) : "#fff"
			}
		});
		const cpl_text_dom = $("<label>", {
			class: "cpl-text",
			for: check_id,
			text: group.name,
		});
		if (W.notEmpty(group.info))
			cpl_text_dom.addClass("tooltip")
				.attr("data-toggle", "tooltip")
				.attr("data-placement", "bottom")
				.attr("data-original-title", group.info);
		const cpl_dom = $("<div>", {
			class: container_classlist.join(' '),
			attr: {
				"data-group": group.value,
			}
		}).append(
			$("<div>", {
				class: "cpl-actions"
			}).append(cpl_check_dom)
				.append(checkmark_dom)
		)
			.append(cpl_text_dom)
			.append($("<span>", {
				class: "badge",
				text: group.count,
			}));
		cpl_check_dom.on('change', function ($e) {
			var select_all = document.getElementById(dataHelper.printf('cpl_filter_select_all{{blockKey}}', { blockKey: blockKey }));
			select_all.checked = $e.target.checked;
			select_all.indeterminate = false;
			$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl-check').each(function (i, element) {
				if (element.checked !== $e.target.checked) {
					select_all.indeterminate = true;
					select_all.checked = false;
				}
			});
			store_preference($e.target.value, $e.target.checked);
			$(dataHelper.printf('#agenda_view_tabs{{blockKey}} + .tab-content > .tab-pane', { blockKey: blockKey })).each(function () {
				$(this).addClass('load');
			});
			reload_current_view();
		});
		return cpl_dom;
	}

	function store_preference($key, $value) {
		var user_connected = W.userConnected && W.userConnected._id && W.userConnected._id.$id ? W.userConnected._id.$id : '';
		var key = dataHelper.printf('{{userConnected}}{{storageVersion}}{{blockKey}}{{contentType}}filter', {
			contentType: globals.cmsData.content_type,
			blockKey: blockKey,
			storageVersion: globals.storageVersion,
			userConnected: user_connected
		});
		var data = loadStorage();
		data[$key] = $value;
		if (user_connected) {
			localStorage.setItem(key, W.JSON.stringify(data));
		} else {
			sessionStorage.setItem(key, W.JSON.stringify(data));
		}
	}

	function loadStorage() {
		var user_connected = W.userConnected && W.userConnected._id && W.userConnected._id.$id ? W.userConnected._id.$id : '';
		var key = dataHelper.printf('{{userConnected}}{{storageVersion}}{{blockKey}}{{contentType}}filter', {
			contentType: globals.cmsData.content_type,
			blockKey: blockKey,
			storageVersion: globals.storageVersion,
			userConnected: user_connected
		});
		var data = user_connected ? localStorage.getItem(key) : sessionStorage.getItem(key);
		if (data === null) {
			data = {};
		} else {
			data = W.JSON.parse(data);
		}
		return data;
	}

	/**
	 * Vue sur le filtre de l'agenda
	 * @param {FilterView[]} filterGroupsList Elements à afficher
	 */
	function loadFilterView(filterGroupsList) {
		if (typeof filterGroupsList === 'undefined') {
			filterGroupsList = [];
		}
		var i;
		var checked;
		$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.view_loader').remove();
		var storage = loadStorage();
		var selectAllDom = document.getElementById(dataHelper.printf('cpl_filter_select_all{{blockKey}}', { blockKey: blockKey }));
		var filteredOpenagendasList = globals.openagendaData.filter(filterExternalAgenda);
		var filteredIcalendarsList = globals.icalendarData.filter(filterExternalAgenda);

		if (selectAllDom !== null) {
			selectAllDom.checked = true;
			selectAllDom.indeterminate = false;
		}
		if (filteredOpenagendasList.length > 0 && selectAllDom !== null) {
			selectAllDom.checked = typeof storage[filteredOpenagendasList[0].value] !== 'undefined' ? storage[filteredOpenagendasList[0].value] : true;
		} else if (filteredIcalendarsList.length > 0 && selectAllDom !== null) {
			selectAllDom.checked = typeof storage[filteredIcalendarsList[0].value] !== 'undefined' ? storage[filteredIcalendarsList[0].value] : true;
		} else if (filterGroupsList.length > 0 && selectAllDom !== null) {
			selectAllDom.checked = typeof storage[filterGroupsList[0].value] !== 'undefined' ? storage[filterGroupsList[0].value] : true;
		}
		for (i = 0; i < filteredOpenagendasList.length && selectAllDom !== null; i++) {
			var openagenda = filteredOpenagendasList[i];
			checked = typeof storage[openagenda.value] !== 'undefined' ? storage[openagenda.value] : true;
			if (checked !== selectAllDom.checked) {
				selectAllDom.indeterminate = true;
				selectAllDom.checked = false;
			}
			if (openagenda.count > 0)
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(cplHtmlTemplate(openagenda, {
					checked: checked,
					is_external: true
				}));
		}
		for (i = 0; i < filteredIcalendarsList.length && selectAllDom !== null; i++) {
			var icalendar = filteredIcalendarsList[i];
			checked = typeof storage[icalendar.value] !== 'undefined' ? storage[icalendar.value] : true;
			if (checked !== selectAllDom.checked) {
				selectAllDom.indeterminate = true;
				selectAllDom.checked = false;
			}
			if (icalendar.count > 0)
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(cplHtmlTemplate(icalendar, {
					checked: checked,
					is_external: true
				}));
		}
		for (i = 0; i < filterGroupsList.length && selectAllDom !== null; i++) {
			var group = filterGroupsList[i];
			checked = typeof storage[group.value] !== 'undefined' ? storage[group.value] : true;
			if (checked !== selectAllDom.checked) {
				selectAllDom.indeterminate = true;
				selectAllDom.checked = false;
			}
			if (group.count > 0)
				$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).append(cplHtmlTemplate(group, { checked: checked }));
		}

		W.coInterface.bindTooltips();
	}

	function reload_current_view() {
		var tabs = $(dataHelper.printf('#agenda_view_tabs{{blockKey}}', { blockKey: blockKey }));
		var target = tabs.find('li.active a').attr('href');
		viewLoadings[target.substring(1)]();
		searchLastValue = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val();
	}

	/**
	 * Filtre pour les agenda externes
	 * @param {Object} agenda
	 * @return {boolean}
	 */
	function filterExternalAgenda(agenda) {
		var search_input = $(dataHelper.printf('#cpl-search{{blockKey}} input[type=search]', { blockKey: blockKey })).val();
		if (search_input) {
			var regex = new RegExp(search_input, 'ig');
			return agenda.name.match(regex);
		}
		return true;
	}

	function loadVerticalTimeline(events, contentType) {
		if (typeof events === 'undefined') {
			events = [];
		}
		var data = [];

		globals.openagendaData.filter(filterExternalAgenda).forEach(function (openagenda) {
			if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: openagenda.value })).is(':checked')) {
				data = data.concat(openagenda.events);
			}
		});
		globals.icalendarData.filter(filterExternalAgenda).forEach(function (icalendar) {
			if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: icalendar.value })).is(':checked')) {
				data = data.concat(icalendar.events);
			}
		});
		data = data.concat(events);
		data = data.map(function (mapElement) {
			var title = mapElement.name;
			if (mapElement.maxContributor) {
				title = '(' + Object.keys(mapElement.contributors).length + '/' + mapElement.maxContributor + ') ' + title;
			}
			var mapReturn = {
				id: mapElement.id,
				title: title,
				start: mapElement.startDate,
				end: mapElement.endDate,
				short_description: mapElement.shortDescription,
				full_description: W.dataHelper.markdownToHtml(mapElement.description),
				backgroundColor: (W.notEmpty(globals.color) ? globals.color.background : mapElement.color),
				textColor: W.notEmpty(globals.color) ? globals.color.foreground : invert_color(mapElement.color),
				group: mapElement.group,
				type: contentType
			};
			if (typeof mapElement.actions !== 'undefined') {
				mapReturn.actions = mapElement.actions;
			}
			return mapReturn;
		});

		globals.verticalTimeline(dataHelper.printf('#vertical_timeline_container{{blockKey}}', { blockKey: blockKey }), {
			events: data,
			on_click: elementDetail
		});
	}

	function load_horizontal_timeline(__events) {
		var tl_interval = setInterval(function () {
			if (typeof globals.chronology !== 'undefined') {
				clearInterval(tl_interval);
				var data = [];
				globals.openagendaData.filter(filterExternalAgenda).forEach(function (openagenda) {
					if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: openagenda.value })).is(':checked'))
						data = data.concat(openagenda.events);
				});
				globals.icalendarData.filter(filterExternalAgenda).forEach(function (icalendar) {
					if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: icalendar.value })).is(':checked'))
						data = data.concat(icalendar.events);
				});
				data = data.concat(__events);
				var option = {
					events: data.map(function (event) {
						var moment_start = globals.moment(event.startDate);
						var moment_end = globals.moment(event.endDate);
						var map = {
							start_date: {
								year: moment_start.year(),
								month: moment_start.month() + 1,
								day: moment_start.date()
							},
							end_date: {
								year: moment_end.year(),
								month: moment_end.month() + 1,
								day: moment_end.date()
							},
							text: {
								headline: event.name,
								text: dataHelper.printf('<blockquote>{{shortDescription}}</blockquote> {{longDescription}}', {
									shortDescription: event.shortDescription,
									longDescription: W.dataHelper.markdownToHtml(event.description)
								})
							}
						};
						if (globals.color) {
							map.background = {
								color: globals.color.background
							};
						} else if (typeof event.color !== 'undefined') {
							map.background = {
								color: event.color
							};
						}
						if (event.profilImageUrl !== '') {
							map.media = {
								url: event.profilImageUrl,
								thumbnail: event.profilImageUrl
							};
						}
						return map;
					})
				};
				var options = {
					script_path: dataHelper.printf('{{baseUrl}}/plugins/knightlab_timeline3/js/', { baseUrl: W.baseUrl }),
					language: 'fr',
					font: 'bevan-pontanosans',
					theme: 'contrast'
				};
				if (data.length > 0)
					new globals.chronology.Timeline(dataHelper.printf('horizontal_timeline_container{{blockKey}}', { blockKey: blockKey }), option, options);
				else
					$(dataHelper.printf('#horizontal_timeline_container{{blockKey}}', { blockKey: blockKey })).empty().html(('<div class="tv-empty-message">La liste des éléments est vide</div>'));
			}
		});
	}

	function elementDetail(element) {
		if (element.type === 'action') {
			var url = dataHelper.printf('{{baseUrl}}/costum/project/action/request/action_detail_html', { baseUrl: W.baseUrl });
			var id = element.id;
			id = id.indexOf('act') >= 0 ? id.substring(3) : id;
			var post = {
				id: id
			};
			W.ajaxPost(null, url, post, function (html) {
				$(dataHelper.printf('#action-preview{{blockKey}}', { blockKey: blockKey })).empty().html(html);
			}, null, 'text');
		} else {
			function reload_attendees() {
				request_event_attendees(element.id)
				.then(function (attendees) {
					var is_contributor = attendees.citoyens.find(c => c.id === userId);
					if (is_contributor)
						$(".participateUser")
							.prop("disabled", false)
							.data("action", "quit")
							.empty()
							.html("<i class='fa fa-unlink'></i> Quitter");
					else
						$(".participateUser")
							.prop("disabled", false)
							.data("action", "join")
							.prop("disabled", false)
							.empty()
							.html("<i class='fa fa-link'></i> Participer");
					$(".attendees-list")
						.hide()
						.empty()
					$.each(attendees, function (type, list) {
						$.each(list, function (_, item) {
							item.type = type;
							$(".attendees-list").append(W.dataHelper.printf('' +
								'<div class="col-xs-12 element-finder shadow2 padding-10">' +
								'    <img' +
								'        src="{{image}}"' +
								'        class="img-circle pull-left margin-right-10"' +
								'        height="35"' +
								'        width="35"' +
								'    >' +
								'    <span class="info-contact pull-left margin-right-5">' +
								'        <span class="name-contact text-dark text-bold">{{name}}</span>' +
								'        <br>' +
								'        <span class="cp-contact text-light pull-left">{{type}}</span>' +
								'    </span>' +
								'</div>'
								, item));
						})
					});
				})
			}
			$(dataHelper.printf('#event-color{{blockKey}}', { blockKey: blockKey })).css('background-color', element.backgroundColor);
			$(dataHelper.printf('#event-name{{blockKey}}', { blockKey: blockKey })).text(element.title);
			var start = element.start;
			var end = element.end;
			if (start.format('LL') === end.format('LL')) {
				$(dataHelper.printf('#event-date{{blockKey}}', { blockKey: blockKey })).text(dataHelper.printf('{{completeDate}} {{completeHourStart}} - {{completeHourEnd}}', {
					completeDate: start.format('LL'),
					completeHourStart: start.format('HH:mm'),
					completeHourEnd: end.format('HH:mm')
				}));
			} else {
				$(dataHelper.printf('#event-date{{blockKey}}', { blockKey: blockKey })).text(dataHelper.printf('{{startDate}} - {{endDate}}', {
					startDate: start.format('LLL'),
					endDate: end.format('LLL')
				}));
			}
			$(".participateUser")
				.prop("disabled", true)
				.data("id", element.id)
				.data("name", element.title)
				.off("click")
				.on("click", function (e) {
					var self;

					e.preventDefault();
					self = $(this);
					if (self.data("action") === "join")
						request_event_join(self.data("id")).then(reload_attendees);
					else
						request_event_quit(self.data("id")).then(reload_attendees);
				});
			$(".attendeeList").off("click").on("click", function (e) {
				e.preventDefault();

				$(".attendees-list").slideToggle();
			});
			$(dataHelper.printf('#event-group{{blockKey}}', { blockKey: blockKey })).text(element.group);
			var eventDetailModalDom = $('#event-detail');
			eventDetailModalDom
				.find('.rin-modal-action[data-action=preview]')
				.attr('href', dataHelper.printf('#page.type.{{type}}.id.{{id}}', {
					type: 'events',
					id: element.id
				}));
			if (element.actions.length) {
				$('.action-panels').show();
				$('.action-panels .list-group-item').empty().append(element.actions.map(function (actionMap) {
					var isUserContributor = userConnected !== null && typeof actionMap.contributors[userConnected._id.$id] !== 'undefined';
					var actionListItemDom = $('<li>')
						.addClass('list-group-item')
						.css('margin-right', '5px');
					var participateButtonDom = $('<button>')
						.addClass('btn btn-success btn-sm')
						.data('id', actionMap.id)
						.css('margin-right', '5px')
						.html(dataHelper.printf('<span class="fa fa-{{linkIcon}}"></span>', {
							linkIcon: isUserContributor ? 'unlink' : 'link'
						})).on('click', function () {
							var self = $(this);
							if (self.hasClass('disabled') || userConnected === null) { return true; }
							const participate_action = self.find('.fa').hasClass('fa-link') ? 1 : 0;
							const participation_icons = ["fa-link", "fa-unlink"];
							self.find(".fa").removeClass("fa-link").removeClass("fa-unlink").addClass("fa-spinner fa-spin");
							coInterface.actions.request_participation({
								action: self.data('id'),
								contributor: userId,
								participate:  participate_action,
							}).then((response) => {
								const contributors = {};
								$.each(response, (_, c) => {
									contributors[c.id] = {type: "citoyens"};
								});
								textDom.data('contributors', contributors);
								textDom.text(`${Object.keys(textDom.data('contributors')).length}/${actionMap.max} ${actionMap.name}`);
								self.find(".fa").removeClass("fa-spinner fa-spin").addClass(participation_icons[participate_action]);
								self.tooltip('destroy').empty().html(`<span class="fa ${participation_icons[participate_action]}"></span>`);
								setTimeout(function () {
									self.tooltip({
										container: '#event-detail',
										animation: true,
										html: false,
										placement: 'top',
										title: participate_action ? 'Ne plus participer' : "Participer",
									});
								}, 500);
							}).catch(error => {
								toastr.error(`${error.side.toUpperCase()} : ${error.msg}`);
								self.find(".fa").removeClass("fa-spinner fa-spin")
									.addClass(participation_icons[(participate_action + 1) % 2]);
							});
						}).tooltip({
							container: '#event-detail',
							animation: true,
							html: false,
							placement: 'top',
							title: function () {
								return isUserContributor ? 'Ne plus participer' : 'Participer'
							}
						});
					var infoDom = $('<button>')
						.addClass('btn btn-info btn-sm')
						.css('margin-right', '5px')
						.data('id', actionMap.id)
						.html(dataHelper.printf('<span class="fa fa-info-circle"></span>', {
							linkIcon: isUserContributor ? 'unlink' : 'link'
						})).on('click', function () {
							var self = $(this);
							elementDetail({
								type: 'action',
								id: self.data('id')
							});
							eventDetailModalDom.rinModal('close');
						});
					var textDom = $('<span>')
						.data('contributors', actionMap.contributors)
						.text(dataHelper.printf('({{contributor}}/{{max}}) {{name}}', {
							contributor: Object.keys(actionMap.contributors).length,
							max: actionMap.max,
							name: actionMap.name
						}));
					return actionListItemDom.append(infoDom, participateButtonDom, textDom);
				}));

			} else
				$('.action-panels').hide();
			eventDetailModalDom.off('rinmodal.shown').on('rinmodal.shown', function () {
				coInterface.bindLBHLinks();
			});
			eventDetailModalDom.rinModal('show');
			reload_attendees();
		}
	}

	function request_event_attendees(id) {
		return (new Promise(function (resolve, reject) {
			var url,
				post;

			url = baseUrl + "/co2/event/attendees/method/get";
			post = {
				id: id
			};
			W.ajaxPost(null, url, post, function (response) {
				if (response.success)
					resolve(response.content);
				else
					reject({
						code: 41,
						msg: response.content
					});
			}, function (arg0, arg1, arg2) {
				reject({
					code: 42,
					msg: [arg0, arg1, arg2]
				});
			})
		}));
	}

	function request_event_join(id) {
		return (new Promise(function (resolve, reject) {
			var url,
				post;

			url = baseUrl + "/co2/event/attendees/method/join";
			post = {
				id: id
			};
			W.ajaxPost(null, url, post, function (response) {
				if (response.success)
					resolve(response.content);
				else
					reject({
						code: 41,
						msg: response.content
					});
			}, function (arg0, arg1, arg2) {
				reject({
					code: 42,
					msg: [arg0, arg1, arg2]
				});
			})
		}));
	}

	function request_event_quit(id) {
		return (new Promise(function (resolve, reject) {
			var url,
				post;

			url = baseUrl + "/co2/event/attendees/method/quit";
			post = {
				id: id
			};
			W.ajaxPost(null, url, post, function (response) {
				if (response.success)
					resolve(response.content);
				else
					reject({
						code: 41,
						msg: response.content
					});
			}, function (arg0, arg1, arg2) {
				reject({
					code: 42,
					msg: [arg0, arg1, arg2]
				});
			})
		}));
	}

	function update_action_room(id) {
		return new Promise(function (resolve) {
			var url = baseUrl + "/costum/project/action/request/set_room";
			ajaxPost(null, url, {
				id: id
			}, resolve);
		});
	}

	/**
	 * Charger la vue calendrier
	 * @param {Action[]} [events=[]] Contenu à insérer
	 * @param {string} contentType Type de contenu (projects/events)
	 */
	function load_calendar(events, contentType) {
		if (typeof events === 'undefined') {
			events = [];
		}
		var calendarDom = $(dataHelper.printf('#calendar_container{{blockKey}}', { blockKey: blockKey }));
		var data = [];
		globals.openagendaData.filter(filterExternalAgenda).forEach(function (openagenda) {
			if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: openagenda.value })).is(':checked')) {
				data = data.concat(openagenda.events);
			}
		});
		globals.icalendarData.filter(filterExternalAgenda).forEach(function (icalendar) {
			if ($(dataHelper.printf('.cpl-check[value={{value}}]', { value: icalendar.value })).is(':checked')) {
				data = data.concat(icalendar.events);
			}
		});
		data = data.concat(events);
		
		const event_list = data.map(function (event) {
			var map = {
				id: event.id,
				title: dataHelper.printf(event.contributors && event.maxContributor ? '({{current}}/{{total}}) {{name}}' : '{{name}}', {
					current: event.contributors ? Object.keys(event.contributors).length : 0,
					total: event.maxContributor,
					name: event.name
				}),
				group: event.group,
				group_id: event.group_id,
				start: globals.moment(event.startDate).format(),
				end: globals.moment(event.endDate).format(),
				type: contentType
			};
			if (typeof event.actions !== 'undefined') {
				map.actions = event.actions;
			}
			if (globals.color) {
				map.backgroundColor = globals.color.background + ' !important';
				map.textColor = globals.color.foreground + ' !important';
			} else if (typeof event.color !== 'undefined') {
				map.backgroundColor = event.color + ' !important';
				map.textColor = invert_color(event.color) + ' !important';
			}
			return map;
		});

		if (calendarDom.children().length > 0) {
			calendarDom.fullCalendar('removeEvents');
			calendarDom.fullCalendar('addEventSource', event_list);
		} else {
			calendarDom.fullCalendar({
				selectable: globals.isAdmin || globals.isCostumAap && W.notEmpty(W.userConnected),
				height: 600,
				events: event_list,
				eventAfterAllRender: function (__view) {
					checkWindowScroll();
					if (__view.name === 'month')
						calendarDom.find('.fc-scroller.fc-day-grid-container').css('overflow', 'inherit');
				},
				eventClick: function ($event, $js_event) {
					$js_event.preventDefault();
					elementDetail($event);
				},
				select: function (__start, __end) {
					if (typeof __end === 'object' && __end.format('HH:mm') === '00:00')
						__end.subtract(1, 'minutes');
					if (globals.isAdmin || globals.isCostumAap && W.notEmpty(W.userConnected)) {
						if (globals.cmsData.content_type === 'events') {
							var params = {
								startDate: __start.format('DD/MM/YYYY HH:mm'),
								endDate: __end.format('DD/MM/YYYY HH:mm')
							};
							W.dyFObj.openForm('event', null, params, null, {
								afterSave: function () {
									$("#agenda_view_tabs" + blockKey).trigger("refresh-filter");
									dyFObj.closeForm();
								}
							}, {
								notCloseOpenModal: true
							});
						} else {
							var defaultFinder = {};
							defaultFinder[W.userConnected._id.$id] = {
								type: 'citoyens',
								name: W.userConnected.name
							};
							var defaultActionParent = {};
							if (globals.defaultFilters.length) {
								defaultActionParent[globals.defaultFilters[0]] = {
									type: 'projects'
								};
							}
							W.dyFObj.openForm({
								jsonSchema: {
									title: 'Ajouter une action',
									properties: {
										generic_agenda_name: W.dyFInputs.name('action'),
										generic_agenda_parentType: W.dyFInputs.inputHidden('projects'),
										generic_agenda_parentId: W.dyFInputs.inputHidden(''),
										generic_agenda_creator: W.dyFInputs.inputHidden(W.userConnected._id.$id),
										generic_agenda_credits: {
											inputType: 'text',
											label: 'Crédits',
											rules: {
												number: true
											},
											value: '1'
										},
										generic_agenda_idUserAuthor: W.dyFInputs.inputHidden(W.userConnected._id.$id),
										generic_agenda_idParentRoom: W.dyFInputs.inputHidden(''),
										generic_agenda_tags: {
											inputType: 'tags',
											label: W.trad.tags
										},
										generic_agenda_min: W.dyFInputs.inputHidden(0),
										generic_agenda_max: {
											label: coTranslate('Maximum contributor'),
											inputType: 'text',
											rules: {
												number: true,
												required: true
											}
										},
										generic_agenda_status: W.dyFInputs.inputHidden('todo'),
										generic_agenda_startDate: {
											label: 'Date de début',
											inputType: 'datetime',
											value: __start.format('DD/MM/YYYY HH:mm'),
											rules: {
												required: true
											}
										},
										generic_agenda_endDate: {
											label: 'Date de fin',
											inputType: 'datetime',
											value: __end.format('DD/MM/YYYY HH:mm'),
											rules: {
												required: true
											}
										}
									},
									beforeBuild: function () {
										if (typeof W.costum !== 'undefined' && W.costum.contextType === 'projects') {
											this.properties.generic_agenda_parentId = W.dyFInputs.inputHidden(W.costum.contextId);
										} else {
											if (globals.isCostumAap) {
												this.properties.generic_agenda_parentId = W.dyFInputs.inputHidden(globals.defaultFilters[0]);
											} else {
												var filters = {};
												var label;
												var init_type;
												if (globals.cmsData.content_type === 'event_actions') {
													label = 'Evénement';
													filters._id = {
														"$in": $("#cpl" + blockKey + " .cpl input.cpl-check").toArray().map(function (consumer) {
															return consumer.value;
														})
													};
													init_type = ['events'];
													this.properties.generic_agenda_parentType = W.dyFInputs.inputHidden("events");
												} else {
													label = 'Projet';
													filters[W.dataHelper.printf('parent.{{id}}.type', { id: W.costum.contextId })] = W.costum.contextType;
													init_type = ['projects'];
													this.properties.generic_agenda_parentType = W.dyFInputs.inputHidden("projects");
												}
												this.properties.generic_agenda_parentId = {
													inputType: 'finder',
													label: label,
													initType: init_type,
													search: {
														filters: filters
													},
													rules: {
														required: true
													},
													multiple: false,
													initBySearch: true,
													initMe: false,
													initContext: false,
													initContacts: false,
													openSearch: true
												};
											}
										}
									},
									save: function (__form) {
										var value = {};
										// Nettoyer les champs
										$.each(['scope', 'collection', 'generic_agenda_contributors'], function (__, __key) {
											delete __form[__key];
										});
										$.each(Object.keys(__form), function (__, __name) {
											var name = __name.substring('generic_agenda_'.length);
											if (name === 'tags')
												value[name] = __form[__name].split(',');
											else if (name === 'parentId' && typeof __form[__name] === 'object')
												value[name] = Object.keys(__form[__name])[0];
											else if (name === 'startDate' || name === 'endDate')
												value[name] = globals.moment(__form[__name], 'DD/MM/YYYY HH:mm').format();
											else
												value[name] = __form[__name];
										});
										var params = {
											collection: 'actions',
											value: value
										};

										function save_data() {
											return new Promise(function (resolve) {
												var view_tabs = [
													dataHelper.printf('calendar{{blockKey}}', { blockKey: blockKey }),
													dataHelper.printf('vertical_timeline{{blockKey}}', { blockKey: blockKey }),
													dataHelper.printf('horizontal_timeline{{blockKey}}', { blockKey: blockKey }),
													dataHelper.printf('gantt{{blockKey}}', { blockKey: blockKey })
												];
												// Sauvegarder les données
												W.dataHelper.path2Value(params, function (r) {
													if (r.result === true && typeof r.saved !== "undefined") {
														W.dyFObj.closeForm();
														update_action_room(r.saved.id).then(function (room_id) {
															var path2value = {
																id: r.saved.id,
																collection: "actions",
																value: room_id,
																path: "idParentRoom"
															};
															W.dataHelper.path2Value(path2value, function (s_room) {
																mylog.log("Saved", s_room);
															});
														});
														var view_param;
														if ($('.cpl input[value=' + globals.parentId + ']').length === 0) {
															view_param = {
																filter_reload: true
															};
														}
														$.each(view_tabs, function (__, __view) {
															$(dataHelper.printf('#{{view}}', { view: __view })).addClass('load');
														});
														viewLoadings[dataHelper.printf('calendar{{blockKey}}', { blockKey: blockKey })](view_param);
														resolve(value);
													}
												});
											});
										}

										if (value.parentType === 'projects')
										// Création de room
										{
											get_project_room(value.parentId).then(function (__room) {
												value.idParentRoom = __room;
												save_data().then(function (value) {
													// Envoyer une notification rocket chat pour la nouvelle action
													var url = dataHelper.printf('{{baseUrl}}/costum/project/action/request/new_action_rocker_chat', { baseUrl: W.baseUrl });
													var post = {
														project: value.parentId,
														action_name: value.name
													};
													W.ajaxPost(null, url, post);
												});
											});
										} else
											save_data().then();
									}
								}
							});
						}
					}
				},
				viewRender: view => {
					const visibles = event_list.filter(e => {
						const start = moment(e.start);
						const end = moment(e.end);
						const is_start_in = start.isSameOrBefore(view.start) || start.isBetween(view.start, view.end);
						const is_end_in = end.isSameOrAfter(view.end) || end.isBetween(view.start, view.end);
						return (is_start_in && is_end_in);
					});
					$(`#cpl${blockKey} .cpl`).each(function () {
						const self = $(this);
						
						if (visibles.length === 0)
							self.css("display", "");
						else
							self.css("display", visibles.filter(v => v.group_id === self.data("group")).length > 0 ? "" : "none");
					});
				}
			}
			);
			if (calendarDom.parents('.tab-pane').hasClass('active')) {
				setTimeout(function () {
					calendarDom.fullCalendar('render');
				}, 500);
			}
		}
		calendarDom.fullCalendar('render');
	}

	/**
	 * Lire charger la vue GANTT
	 * @param {ViewElement[]} __elements L'élément qu'on va charger dans le gantt
	 */
	function load_gantt(__elements) {
		if (typeof __elements === 'undefined') {
			__elements = [];
		}
		var e_length = __elements.length;
		var data = [];
		var links = [];

		for (var i = 0; i < e_length; i++) {
			var project = __elements[i];
			var element_moment_start = globals.moment(project.startDate);
			var element_moment_end = globals.moment(project.endDate);
			var element_duration = element_moment_end.diff(element_moment_start, 'days');

			var t_project = {
				id: project.id,
				text: project.name,
				start_date: element_moment_start.format('YYYY-MM-DD HH:mm'),
				duration: element_duration,
				progress: 0,
				color: globals.color ? globals.color.background : project.color,
				textColor: globals.color ? globals.color.foreground : invert_color(project.color)
			};
			data.push(t_project);

			var progress_ratio = [0, 0];
			var actions_length = project.subs.length;
			for (var j = 0; j < actions_length; j++) {
				var action = project.subs[j];
				var action_start_date = globals.moment(action.startDate);
				var action_end_date = globals.moment(action.endDate);
				var action_duration = action_end_date.diff(action_start_date, 'days');
				var actionSubs = Array.isArray(action.subs) ? action.subs : [];
				var checked_tasks_length = actionSubs.filter(function (__task) {
					return __task.checked;
				}).length;

				progress_ratio[0] += actionSubs.length ? checked_tasks_length : 0;
				progress_ratio[1] += actionSubs.length ? actionSubs.length : 1;

				if (j === 0) {
					links.push({
						id: links.length,
						source: project.id,
						target: action.id,
						type: '1'
					});
				} else {
					var action_before = project.subs[j - 1];
					links.push({
						id: links.length,
						source: action_before.id,
						target: action.id,
						type: '0'
					});
				}

				data.push({
					id: action.id,
					text: action.name,
					start_date: action_start_date.format('YYYY-MM-DD HH:mm'),
					duration: action_duration,
					parent: project.id,
					progress: actionSubs.length ? checked_tasks_length / actionSubs.length : 0,
					color: globals.color ? globals.color.background : project.color,
					textColor: globals.color ? globals.color.foreground : invert_color(project.color)
				});

				if (typeof actionSubs !== 'undefined') {
					var ss_length = actionSubs.length;
					for (var k = 0; k < ss_length; k++) {
						var task = actionSubs[k];
						var task_start_date = globals.moment(task.startDate);
						var task_end_date = globals.moment(task.endDate);
						var task_duration = task_end_date.diff(task_start_date, 'days');

						if (k === 0) {
							links.push({
								id: links.length,
								source: action.id,
								target: task.id,
								type: '1'
							});
						} else {
							var last_task = actionSubs[k - 1];
							links.push({
								id: links.length,
								source: last_task.id,
								target: task.id,
								type: '0'
							});
						}

						data.push({
							id: task.id,
							text: task.name,
							start_date: task_start_date.format('YYYY-MM-DD HH:mm'),
							duration: task_duration,
							parent: action.id,
							progress: task.checked ? 1 : 0,
							color: globals.color ? globals.color.background : project.color,
							textColor: globals.color ? globals.color.foreground : invert_color(project.color)
						});
					}
				}

				t_project.progress = progress_ratio[1] ? progress_ratio[0] / progress_ratio[1] : 0;
			}
		}
		var ganttContainerDom = document.getElementById(dataHelper.printf('gantt_container{{blockKey}}', { blockKey: blockKey }));
		if (ganttContainerDom.childElementCount) {
			gantt_component.clearAll();
		} else {
			gantt_component.init(ganttContainerDom);
		}

		gantt_component.parse({
			data: data,
			links: links
		});
	}

	/**
	 * Récupérer le room du projet sélectionné
	 * @param {string} __id Identifiant du projet associé
	 * @return {Promise.<string>}
	 */
	function get_project_room(__id) {
		var url = dataHelper.printf('{{baseUrl}}/costum/project/project/request/room', { baseUrl: W.baseUrl });
		var post = {
			project: __id
		};
		return new W.Promise(function (__resolve) {
			W.ajaxPost(null, url, post, __resolve);
		});
	}

	function getEventActions(params) {
		var defaultParams = {};
		params = typeof params === 'undefined' ? defaultParams : $.extend(true, {}, defaultParams, params);
		var url = baseUrl + '/costum/agenda/events/request/all_actions';
		return new W.Promise(function (resolve) {
			W.ajaxPost(null, url, params, resolve);
		});
	}

	/**
	 * Récupérer les évènements ainsi que les sous events
	 * depuis la base de données
	 * @param {Object} params
	 * @param {Object} [params.filter] Filtres de sélection qu'on envoie
	 * @param {string} [params.request] Type de requête pour le contrôleur généralement 'all'
	 * @param {number} [params.reorder] Reordonner la réponse ou pas
	 * @returns {Promise.<{data: ViewElement[], groups: FilterView[]}}>}
	 */
	function getEvents(params) {
		if (typeof params === 'undefined') {
			params = {};
		}
		var url = dataHelper.printf('{{baseUrl}}/costum/agenda/events/request/all', { baseUrl: W.baseUrl });
		var post = {
			costumType: W.costum.contextType,
			costumId: W.costum.contextId
		};

		if (typeof params.filter !== 'undefined') {
			post = $.extend(true, {}, post, params.filter);
		}
		if (typeof params.reorder !== 'undefined') {
			url += '/reorder/' + params.reorder;
		}
		return new W.Promise(function (__resolve) {
			W.ajaxPost(null, url, post, __resolve);
		});
	}

	/**
	 * Récupérer les actions ainsi que la liste des projets
	 * depuis la base de données
	 * @param {Object} __params
	 * @param {Object} [__params.filter] Filtres de sélection qu'on envoie
	 * @param {string} [__params.request] Type de requête pour le contrôleur généralement 'all'
	 * @param {number} [__params.reorder] Reordonner la réponse ou pas
	 * @returns {Promise<{data: ViewElement[], groups: FilterView[]}}>}
	 */
	function get_actions(__params) {
		if (typeof __params === 'undefined') {
			__params = {};
		}
		var url = dataHelper.printf('{{baseUrl}}/costum/agenda/projects/request', { baseUrl: W.baseUrl });
		var post = {
			costumType: W.costum.contextType,
			costumId: W.costum.contextId
		};

		if (typeof __params.filter !== 'undefined') {
			post = $.extend(true, {}, post, __params.filter);
		}

		if (typeof __params.request !== 'undefined') {
			url += '/' + __params.request;
		} else {
			url += '/actions';
		}
		if (typeof __params.reorder !== 'undefined') {
			url += '/reorder/' + __params.reorder;
		}
		if (globals.filterContentName) {
			post.agendaContentText = globals.filterContentName;
		}
		if (globals.filterContributors.length) {
			post.members = globals.filterContributors;
		}
		return new W.Promise(function (__resolve) {
			W.ajaxPost(null, url, post, __resolve);
		});
	}

	function invert_color(hex) {
		if (hex.indexOf('#') === 0) {
			hex = hex.slice(1);
		}
		// convert 3-digit hex to 6-digits.
		if (hex.length === 3) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}
		if (hex.length !== 6) {
			throw new Error('Invalid HEX color.');
		}
		// invert color components
		var r = parseInt(hex.slice(0, 2), 16),
			g = parseInt(hex.slice(2, 4), 16),
			b = parseInt(hex.slice(4, 6), 16);
		// pad each with zeros and return
		return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#ffffff';
	}

	/**
	 * Action exécutée lors du scroll de l'écran ou aussi après actualisation des filtres
	 */
	function checkWindowScroll(__event) {
		var main_nav = $(__event && __event === 'costumizer' ? '#menu-top-costumizer' : '#mainNav');

		var filter_container = $(dataHelper.printf('#cpl_container{{blockKey}}', { blockKey: blockKey }));
		var content_container = filter_container.next('.content');
		var agenda_view = filter_container.parent('.agenda-viewer');
		var filter_button = $(dataHelper.printf('#filter-xs{{blockKey}}', { blockKey: blockKey }));
		var overlay_dom = $(dataHelper.printf('#card-overlay-modal{{blockKey}}', { blockKey: blockKey }));

		if (content_container.length === 0) {
			return true;
		}

		var content_container_on_screen = content_container.get(0).getBoundingClientRect();
		var agenda_view_on_screen = agenda_view.get(0).getBoundingClientRect();
		var offset = 20;

		var view_is_not_mobile = W.innerWidth > 1024;
		var filter_exceed = (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight()) < content_container.outerHeight() && content_container_on_screen.top + content_container.outerHeight() <= main_nav.outerHeight() + offset + (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight());

		if (view_is_not_mobile) {
			filter_button.css('display', '');
			overlay_dom.css('display', '');
			if (typeof __event !== 'undefined' && __event === 'resize') {
				filter_container.removeAttr('style');
				filter_container.find('.card-body').css('max-height', '');
			}
		}

		if (main_nav.outerHeight() + offset >= agenda_view_on_screen.top) {
			var top;

			if (filter_exceed) {
				top = content_container_on_screen.top + content_container.outerHeight() - (view_is_not_mobile ? filter_container.outerHeight() : filter_button.outerHeight());
			} else {
				top = main_nav.outerHeight() + offset;
			}

			if (view_is_not_mobile) {
				content_container.css({
					'margin-left': $(filter_container).outerWidth() + offset
				});
				filter_container.css({
					position: 'fixed',
					top: top,
					left: agenda_view_on_screen.left
				});
			} else {
				content_container.css('margin-left', '');
			}

			filter_button.css({
				position: 'fixed',
				top: top,
				left: agenda_view_on_screen.left
			});

		} else {
			filter_button.css('position', '')
				.css('left', '')
				.css('top', '');
			if (view_is_not_mobile) {
				filter_container.css('position', '')
					.css('left', '')
					.css('top', '');

				content_container.css('margin-left', '');
			}
		}
	}

	$(function () {
		var timeout = null;

		$.each(Object.keys(viewLoadings), function (__, __tab) {
			var element = $('[href="#' + __tab + '"]');
			if (element.length > 0) {
				element.on('shown.bs.tab', function () {
					$(".cpl[data-group]").css("display", "");
					viewLoadings[__tab]({
						reload: true
					});
				});
			}
		});

		$(dataHelper.printf('#agenda_view_tabs{{blockKey}}', { blockKey: blockKey })).on('click', '[role=tab]', function ($e) {
			$e.preventDefault();
			$(this).tab('show');
		}).on("refresh-filter", function () {
			var active_tab = $(this).find(".active>[role=tab]");
			viewLoadings[active_tab.attr("href").substring(1)].call(W, { reload: true });
		});

		$(dataHelper.printf('#agenda_view_tabs{{blockKey}} [role=tab]:first', { blockKey: blockKey })).tab('show');

		$(dataHelper.printf('#cpl-search{{blockKey}} input', { blockKey: blockKey })).on('input', function () {
			if (timeout !== null) {
				clearTimeout(timeout);
			}
			timeout = setTimeout(function () {
				$(dataHelper.printf('#agenda_view_tabs{{blockKey}}+.tab-content>.tab-pane', { blockKey: blockKey })).each(function () {
					$(this).addClass('load');
				});
				reload_current_view();
			}, 800);
		});
		$(dataHelper.printf('#cpl_filter_select_all{{blockKey}}', { blockKey: blockKey })).on('change', function (__e) {
			var checked = __e.target.checked;
			$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).find('.cpl .cpl-check').each(function (__, __element) {
				__element.checked = checked;
				var checkmark = $(__element).next('.checkmark');
				checkmark.css('background-color', checked ? checkmark.css('border-color') : '#fff');
				store_preference(__element.value, checked);
			});

			$(dataHelper.printf('#agenda_view_tabs{{blockKey}}+.tab-content>.tab-pane', { blockKey: blockKey })).each(function () {
				$(this).addClass('load');
			});
			reload_current_view();
		});
		$(dataHelper.printf('#cpl{{blockKey}}', { blockKey: blockKey })).on('change', '.cpl .cpl-check', function () {
			var checked = this.checked;
			var checkmark = $(this).next('.checkmark');
			checkmark.css('background-color', checked ? checkmark.css('border-color') : '#fff');
		});
		$(dataHelper.printf('#setting{{blockKey}} form', { blockKey: blockKey })).on('submit', function ($e) {
			$e.preventDefault();
			$(this).find('button[type="submit"] .fa').remove();
			$(this).find('button[type="submit"]').prepend('<span class="fa fa-spinner fa-spin"></span>');
			var form = {
				content_type: $(this).find('[name=content_type]:checked').val(),
				allow_external_sources: $(this).find('[name=allow_external_sources]').is(':checked'),
				width: $(this).find('[name=width]:checked').val() === 'true',
				available_views: [],
				show: []
			};
			$(this).find('[name=available_views]:checked').each(function () {
				form.available_views.push($(this).val());
			});
			$(this).find("[name=show]:checked").each(function () {
				form.show.push(this.value);
			})
			var path2value = {
				collection: 'cms',
				id: blockKey,
				path: 'allToRoot',
				value: form,
				setType: [{
					path: 'width',
					type: 'boolean'
				}]
			};
			W.dataHelper.path2Value(path2value, function () {
				var parentsDom = $(dataHelper.printf('div[data-kunik="{{kunik}}"]', { kunik: kunik })).parents(".custom-block-cms");
				var id = parentsDom.data("id");
				var path = parentsDom.data("path");
				var uniqueKey = parentsDom.data("kunik");
				W.cmsBuilder.block.loadIntoPage(id, W.page, path, uniqueKey);
				if (form.show.length === 0) {
					W.dataHelper.unsetPath({
						id: path2value.id,
						collection: path2value.collection,
						path: "show"
					}, function (params) {
						if (params.result) {
							$('input[name="show"]').prop('checked', false);
							$('.all-btn-content').hide();
						}
					}
					);
				}
			});
		});

		$(dataHelper.printf('#setting{{blockKey}} [name=content_type]', { blockKey: blockKey })).on('change', function () {
			var checked_element = $(dataHelper.printf('#setting{{blockKey}} [name=content_type]:checked', { blockKey: blockKey }));
			var gantt_view_element = $(dataHelper.printf('#available_views_gantt{{blockKey}}', { blockKey: blockKey }));

			if (checked_element.val() !== 'projects') {
				gantt_view_element.parents('.checkbox').css('display', 'none');
				gantt_view_element.prop('checked', false);
			} else {
				gantt_view_element.parents('.checkbox').css('display', '');
				gantt_view_element.prop('checked', true);
			}
		});

		$(dataHelper.printf('#card-overlay-modal{{blockKey}}, #cpl_container{{blockKey}} .card-action .fa-times', { blockKey: blockKey })).on('click', function () {
			var cpl_container = $(dataHelper.printf(dataHelper.printf('#cpl_container{{blockKey}}', { blockKey: blockKey }), { blockKey: blockKey }));
			var close = $(dataHelper.printf('#cpl_container{{blockKey}} .card-action .fa-times', { blockKey: blockKey }));
			var overlay = $(dataHelper.printf('#card-overlay-modal{{blockKey}}', { blockKey: blockKey }));
			$(dataHelper.printf('#filter-xs{{blockKey}}', { blockKey: blockKey })).fadeIn();
			overlay.fadeOut();
			cpl_container.animate({
				opacity: 0,
				top: 0
			}, {
				complete: function () {
					close.css({
						position: '',
						display: '',
						opacity: '',
						top: '',
						left: '',
						transform: ''
					});
					cpl_container.find('.card-body').css({
						maxHeight: ''
					});
				}
			});
		});

		$(dataHelper.printf('#filter-xs{{blockKey}} .btn', { blockKey: blockKey })).on('click', function () {
			var cpl_container = $(dataHelper.printf('#cpl_container{{blockKey}}', { blockKey: blockKey }));
			var main_nav = $('#mainNav');

			cpl_container.find('.card-body').css({
				maxHeight: dataHelper.printf('calc(100vh - {{pixel}}px)', { pixel: cpl_container.find('.card-header').outerHeight() + main_nav.outerHeight() + 20 * 2 })
			});
			cpl_container.css({
				position: 'fixed',
				display: 'block',
				opacity: 0,
				top: 0,
				left: '50%',
				transform: 'translate(-50%, -50%)'
			}).animate({
				opacity: 1,
				top: '50%'
			});
			$(dataHelper.printf('#card-overlay-modal{{blockKey}}', { blockKey: blockKey })).fadeIn();
			$(this).parent(dataHelper.printf('#filter-xs{{blockKey}}', { blockKey: blockKey })).fadeOut();
		});

		$('.cmsbuilder-center-content').on('scroll', function () {
			checkWindowScroll('costumizer');
		});

		$('[data-target=event_sources]').on('click', function () {
			$('.source-management').slideToggle();
		});

		$('.source-management .source-management-item a').on('click', function (e) {
			e.preventDefault();
			var self = $(this);
			var target = self.attr('href').substring(1);
			if (typeof _forms[target] !== "undefined") {
				W.dyFObj.openForm(_forms[target]);
			} else {
				W.dyFObj.openForm(target);
			}
			$('.source-management').slideToggle();
		});

		W.addEventListener('scroll', function () {
			checkWindowScroll();
		});

		W.addEventListener('resize', function () {
			checkWindowScroll('resize');
		});

		var eventDetailModalDom = $('#event-detail');
		eventDetailModalDom.on('click', '.rin-modal-action[data-action=preview]', function () {
			eventDetailModalDom.rinModal('close');
		});
	});
	// socket io section
	wsCO = wsCO && wsCO.connected ? wsCO : io.connect(coWsConfig.serverUrl);
	wsCO.on('.update-action-contributor', function (data) {
		var fullCalendarContainerDom = $(dataHelper.printf('#calendar_container{{blockKey}}', { blockKey: blockKey }));
		var events = fullCalendarContainerDom.fullCalendar('clientEvents', 'act' + data.target);
		events.forEach(function (event) {
			event.title = dataHelper.printf(event.maxContributor ? '({{current}}/{{total}}) {{name}}' : '{{name}}', {
				current: Object.keys(data.contributors).length,
				total: data.info.max,
				name: data.info.name
			});
			fullCalendarContainerDom.fullCalendar('updateEvent', event);
		});
	});
}