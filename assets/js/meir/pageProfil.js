
pageProfil.views.newspaper = function() {
    //if(isInterfaceAdmin){
    if (contextData.type == "organizations"){
        $("#social-header").css("display","none");
        $("#menu-top-profil-social").css("display","none");
        ajaxPost(
            '#central-container',
            baseUrl + '/costum/meir/elementhome/type/' + contextData.type + "/id/" + contextData.id,
            null,
            function() {},
            "html"
        );
    } else if(contextData.type == "citoyens"){        
        urlCtrl.loadByHash("#welcome");
    }else{
        pageProfil.views.newspaper();
    }
    // }else{
    //     $("#social-header").css("display","none");
    //     $("#menu-top-profil-social").css("display","none");
    //     $(".social-main-container").css("min-height","0");
    //     $(".acteurMeir .divNotConnected").css("height","900px !important"); 
    //     $(".acteurMeir").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
    // }
}
pageProfil.views.settings = function(){
    if(!userId){
        $('#modalLogin').modal("show");
    }else{
        getAjax('', baseUrl+'/'+moduleId+'/settings/index',
        function(html){
            $("#central-container").html(html);
        }
        ,"html")
    }
    // if(typeof userConnected != "undefined"){
    //     pageProfil.views.settings();
    // }else{
    //     showPanel("box-login");
    // }

}
pageProfil.views.home = function() {
    //if(isInterfaceAdmin){
    if (contextData.type == "organizations"){
        $("#social-header").css("display","none");
        $("#menu-top-profil-social").css("display","none");
        ajaxPost(
            '#central-container',
            baseUrl + '/costum/meir/elementhome/type/' + contextData.type + "/id/" + contextData.id,
            null,
            function() {},
            "html"
        );
    //}
    // else if(contextData.type == "citoyens"){        
    //     urlCtrl.loadByHash("#welcome");
    }else{
        pageProfil.views.detail();
    }
    // }else{
    //     if (contextData.type == "organizations"){
    //         $("#social-header").css("display","none");
    //         $("#menu-top-profil-social").css("display","none");
    //         $(".social-main-container").css("min-height","0");
    //         $(".acteurMeir .divNotConnected").css("height","900px !important"); 
    //         $(".acteurMeir").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
    //     }else{
    //         pageProfil.views.detail();
    //     }
    // }
};

pageProfil.views.settings = function() {
    if (contextData.type == "organizations"){
        pageProfil.views.home();
    }else{
        getAjax('', baseUrl+'/'+moduleId+'/settings/index',
				function(html){
					$("#central-container").html(html);
				}
			,"html");
    }

};
pageProfil.views.directory = function() {
   // if(isInterfaceAdmin){
    if(pageProfil.params.dir == "members"){
        $("#social-header").css("display","none");
        var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
        if(contextData.type=="organizations") url+="/view/costum.views.custom.meir.element.community";
            ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");
    }
    // }else{
    //     $("#social-header").css("display","none");
    //     $("#menu-top-profil-social").css("display","none");
    //     $(".social-main-container").css("min-height","0");
    //     $(".acteurMeir .divNotConnected").css("height","900px !important"); 
    //     $(".acteurMeir").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
    // }
}