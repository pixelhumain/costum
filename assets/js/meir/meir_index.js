costum.loadByHash = function(h){
    if($.inArray(h, ["#questinnovationgraph", "#graphinnovation"])){
        ajaxPost(
            null, 
            baseUrl+"/costum/blockgraph/getdashboarddata", 
            {
                "costumId":costum.contextId,  
                "costumSlug": costum.contextSlug,
                "costumType":costum.contextType,
                "specificBlock":[]
            }, 
            function(res){
                costum["dashboardData"] = res["data"];
                costum["dashboardGlobalConfig"] = res["global"];
            },
            null,
            null,
            {async:false}
        )
    }
    if($("body").hasClass("modal-open")){
        $("body").removeClass("modal-open")
    }
    return h;
}

costum.meir = {
	init : function(){
		dyFObj.formInMap.forced.countryCode="RE";
		dyFObj.formInMap.forced.map={"center":[-21.115141, 55.536384],"zoom" : 9};
		dyFObj.formInMap.forced.showMap=true;
        //$(".loginLogo").html('<a href="https://www.technopole-reunion.com/" target="_blank"><img class="logoLoginRegister img-responsive" src="'+assetPath + '/images/meir/Logo-transparent.png""></a>')
        $(".logoLoginRegister").attr("src", assetPath + "/images/meir/Logo-transparent.png");
        $(".close-modal").html("<span clss='btn btn-default'><i class='fa fa-arrow-left'></i>  Retour</span>")
	},
    organizations: {
        source : {},
        reference : {},
        referentId : "",
        toBeValidated : "",
        beforeSave : function(data){  
            if(typeof data.reference != "undefined")
                costum.meir.organizations.reference = data.reference 
            if(typeof data.category == "undefined" || (typeof data.category != "undefined" && data.category != "acteurMeir") ){
                costum.meir.organizations.toBeactivate = true
            }
            if(typeof data.source == "undefined" || (typeof data.source != "undefined" && typeof data.source.key != "undefined" && data.source.key != "meir")){
                costum.meir.organizations.source = false
            }
            if(typeof data.referentId != "undefined"){
                costum.meir.organizations.referentId = data.referentId 
            }
        },
        afterSave: function(data) {
            var paramsEmail = {
                tpl : "basic",
                tplMail : costum.admin.email,
                tplObject:"Nouvel acteur",
                fromMail: costum.admin.email,
                replyTo: data.map.email,
                subject :"Nouvel acteur", 
                html : "Il y a un nouvel acteur <a href='"+baseUrl+'/costum/co/index/slug/meir#@'+data.map.slug+"' target='_blanck'>"+data.map.name+"</a>",
            };                
            if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                ajaxPost(
                    null,
                    baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
                    paramsEmail,
                    function(data){
                    },
                    function(xhr, status, error){
                        toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                    }
                );
            }

            mylog.log("commonAfterSavedata",data);
            var tplCtx = {};
            var phone = "";
           
            if(typeof data.map.mobile != "undefined" )
                phone = data.map.mobile;
            if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                tplCtx.id = data.map._id.$id;
                tplCtx.value = {
                    "telephone.mobile" : [phone],
                    "toBeValidated" : true,
                    "preferences.isOpenData" : true,
                    "preferences.isOpenEdition" : true 
                };    
            }
            if(typeof data.id != "undefined" && typeof data.map._id == "undefined"){
                tplCtx.id = data.id;
                tplCtx.value = {
                    "telephone.mobile" : [phone]
                };
                if(costum.meir.organizations.source == false){
                    if(notNull(costum.meir.organizations.reference) && notNull(costum.meir.organizations.reference.costum)){
                        if((costum.meir.organizations.reference.costum).indexOf("meir") == -1){
                            costum.meir.organizations.reference.costum.push("meir");
                            tplCtx.value["reference.costum"] = costum.meir.organizations.reference.costum;
                        }
                    }else{
                        tplCtx.value["reference.costum"] = ["meir"]
                    }
                    if(costum.meir.organizations.referentId == "")
                        tplCtx.value["referentId"] = userId;
                }
                if(typeof costum.meir.organizations.toBeactivate != "" && costum.meir.organizations.toBeactivate == true){
                    tplCtx.value["toBeValidated"] = costum.meir.organizations.toBeactivate
                } 
            }
            $.each(dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties,function(field, values){
                if(typeof data.map[field] == "undefined" && values.inputType=="selectMultiple"){
                    tplCtx.value[field] = null;
                }
            });

            tplCtx.value.tags=[];
            tplCtx.path = "allToRoot";        
            if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
                $.each(data.map.tags, function(i,tg){
                    tplCtx.value.tags.push(tg.replace("&amp;","&"));
                });
            }       
            if(typeof data.map.jobFamily != "undefined"){ 
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                    if(typeof data.map.jobFamily == "string" ){
                        if ((tplCtx.value.tags).indexOf(data.map.jobFamily) == -1){
                            tplCtx.value.tags.push(data.map.jobFamily);
                        }
                    }else if(typeof data.map.jobFamily == "object" ){
                        $.each(data.map.jobFamily, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(costum.lists.family,function(kf,vf){
                        if(typeof data.map.jobFamily == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.jobFamily != vf ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }else if(typeof data.map.jobFamily == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.jobFamily).indexOf(vf) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }
                    })
                }else{
                    if(typeof data.map.jobFamily == "string"){
                        tplCtx.value.tags.push(data.map.jobFamily);
                    }
                    else{
                        $.each(data.map.jobFamily, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    } 
                }
               
            }else{
                $.each(costum.lists.family,function(kf,vf){
                    if((tplCtx.value.tags).indexOf(vf) >-1 ){
                        delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vf)]; 
                    }
                })
            }
            if(typeof data.map.thematic != "undefined"){
                tplCtx.value.thematic = []; 
                if(typeof data.map.thematic != "undefined" || notNull(data.map.thematic)) {
                    if(typeof data.map.thematic == "string" ){
                        tplCtx.value.thematic  = data.map.thematic.replace("&amp;","&");
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            tplCtx.value.thematic.push(tag.replace("&amp;","&"));
                        });
                    }
                }
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                    if(typeof data.map.thematic == "string" ){
                        if ((tplCtx.value.tags).indexOf((data.map.thematic)) == -1){
                            tplCtx.value.tags.push((data.map.thematic));
                        }
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){                                    
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(costum.lists.themes,function(kt,vt){
                        if(typeof data.map.thematic == "string" && (tplCtx.value.tags).indexOf(vt) >-1 && data.map.thematic != vt ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1);                                 
                        }else if(typeof data.map.thematic == "object" && (tplCtx.value.tags).indexOf(vt) >-1  && (data.map.thematic).indexOf(vt) == -1){
                            delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vt)]; 
                        }
                    })
                } else{
                    if(typeof data.map.thematic == "string"){
                        tplCtx.value.tags.push(data.map.thematic);
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }
               
            }else{
                $.each(costum.lists.themes,function(kt,vt){
                    if((tplCtx.value.tags).indexOf(vt) >-1 ){
                        delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vt)]; 
                    }
                })
            }
            if(typeof data.map.statusActor != "undefined"){
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {  
                    if(typeof data.map.statusActor == "string" ){ 
                        if ((tplCtx.value.tags).indexOf((data.map.statusActor)) == -1){
                            tplCtx.value.tags.push((data.map.statusActor)); 
                        }
                    }
                    else{
                        $.each(data.map.statusActor, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){                                    
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(costum.lists.status,function(ks,vs){ 
                        if(typeof data.map.statusActor == "string" && (tplCtx.value.tags).indexOf(ks) >-1 && data.map.statusActor != ks ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(ks), 1);                                 
                        }else if(typeof data.map.statusActor == "object" && (tplCtx.value.tags).indexOf(ks) >-1  && (data.map.statusActor).indexOf(ks) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(ks), 1);   
                        }
                    })
                } else{
                    if(typeof data.map.statusActor == "string"){
                        tplCtx.value.tags.push(data.map.statusActor);
                    }
                    else{
                        $.each(data.map.statusActor, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }
               
            }else{
                $.each(costum.lists.status,function(ks,vs){ 
                    if((tplCtx.value.tags).indexOf(vs) >-1 ){
                        delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vs)]; 
                    }
                })
            }
            if((typeof data.map.jobFamily == "string" && data.map.jobFamily != "Financement") || (typeof data.map.jobFamily == "object" && $.inArray("Financement",data.map.jobFamily) ==-1)) {
                tplCtx.value.linkFinancialDevice = "";
                tplCtx.value.modality = "";
                tplCtx.value.typeFinancing = "";
                tplCtx.value.maximumAmount = "";
                tplCtx.value.publicCible = "";
                tplCtx.value.financialPartners = "";
                if(typeof costum.lists.typeFinancing != "undefined") {
                    $.each(costum.lists.typeFinancing,function(ktf,tff){                
                        if((tplCtx.value.tags).indexOf(tff) >-1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(tff), 1);                                 
                        }
                    })
                }
            }
            paramst = {};
            paramst.id = costum.contextId;
            paramst.collection = costum.contextType;
            paramst.path = "allToRoot"; 
            paramst.updateCache = true; 
            paramst.value = {};
            if(typeof data.map.typeFinancing != "undefined" && typeof data.map.jobFamily != "undefined" && (data.map.jobFamily == "Financement" || $.inArray("Financement",data.map.jobFamily) > -1) ){
                typeFinancing = data.map.typeFinancing.split(",");
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {  
                        $.each(typeFinancing, function(i,tagtf){
                            if((tplCtx.value.tags).indexOf(tagtf) == -1){                                    
                                tplCtx.value.tags.push(tagtf);
                            }
                        });
                    $.each(costum.lists.typeFinancing,function(kt,vt){                         
                        if((tplCtx.value.tags).indexOf(vt) >-1  && (typeFinancing).indexOf(vt) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1);                                 
                        }
                    })
                }else{
                    if(typeof data.map.typeFinancing == "string"){
                        tplCtx.value.tags.push(data.map.typeFinancing);
                    }
                    else{
                        $.each(data.map.typeFinancing, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }
                $.each(typeFinancing,function(ktf,vtf){
                    if(typeof costum.lists.typeFinancing != "undefined"){
                        if( (costum.lists.typeFinancing).indexOf(vtf)== -1 ){
                            paramst.value["costum.lists.typeFinancing"] = [];
                            paramst.value["costum.lists.typeFinancing"] = costum.lists.typeFinancing;
                            paramst.value["costum.lists.typeFinancing"].push(vtf);                             
                        }
                    }
                })
            }else{
                $.each(costum.lists.typeFinancing,function(ktf,tff){                
                    if((tplCtx.value.tags).indexOf(tff) >-1){
                        tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(tff), 1);                                 
                    }
                })
            }
            if(typeof data.map.legalStatus != "undefined"){
                 if(typeof costum.lists.legalStatus != "undefined"){
                    if( (costum.lists.legalStatus).indexOf(data.map.legalStatus)== -1 ){
                        paramst.value["costum.lists.legalStatus"] = [];
                        paramst.value["costum.lists.legalStatus"] = costum.lists.legalStatus;
                        paramst.value["costum.lists.legalStatus"].push(data.map.legalStatus);     
                    }
                }
            }
            if(typeof data.map.modality != "undefined"){
                modality = data.map.modality.split(",");
                if(typeof costum.lists.modality != "undefined"){
                    $.each(modality,function(kmd,md){
                        if( (costum.lists.modality).indexOf(md)== -1 ){
                            paramst.value["costum.lists.modality"] = [];
                            paramst.value["costum.lists.modality"] = costum.lists.modality;
                            paramst.value["costum.lists.modality"].push(md);     
                        }
                    })
                }
            }

            mylog.log("commonAfterSavedatatags",tplCtx.value);
            tplCtx.collection = "organizations";
            dataHelper.path2Value(tplCtx, function(params) {
                toastr.success("Acteur bien ajouté");
                urlCtrl.loadByHash("#@"+data.map.slug);
                if(Object.keys(paramst.value).length !== 0){                    
                    dataHelper.path2Value(paramst, function(params) {
                        location.reload()
                    });
                }
                
            });
            
            dyFObj.commonAfterSave(data, function() {
            });
        }
    },
    startup :{
        afterSave :function(data){
            var paramsEmail = {
                tpl : "basic",
                tplMail : costum.admin.email,
                tplObject:"Nouvel startup",
                fromMail: costum.admin.email,
                replyTo: data.map.email,
                subject :"Nouvel startup", 
                html : "Il y a un nouvel startup <a href='"+baseUrl+'/costum/co/index/slug/meir#@'+data.map.slug+"' target='_blanck'>"+data.map.name+"</a>",
            };                
            if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                ajaxPost(
                    null,
                    baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
                    paramsEmail,
                    function(data){
                    },
                    function(xhr, status, error){
                        toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                    }
                );
            }
            dyFObj.commonAfterSave(data,function(){
                var phone = "";
                if(typeof data.map.mobile != "undefined" )
                    phone = data.map.mobile;
                if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                    tplCtx.id = data.map._id.$id;
                    tplCtx.value = {
                        "telephone.mobile" : [phone]
                    };                   
                }
                if(typeof data.id != "undefined" && typeof data.map._id == "undefined"){
                    tplCtx.id = data.id;
                    tplCtx.value = {
                        "telephone.mobile" : [phone]
                    };
                }
                tplCtx.value.tags=[];
                tplCtx.path = "allToRoot";                   
                             
                if(typeof data.map.thematic != "undefined"){
                    if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
                        $.each(data.map.tags, function(i,tg){
                            
                            tplCtx.value.tags.push(tg.replace("&amp;","&"));
                        });
                        if(typeof data.map.thematic == "string" ){
                            if ((tplCtx.value.tags).indexOf((data.map.thematic)) == -1){
                                tplCtx.value.tags.push((data.map.thematic));
                            }
                        }
                        else{
                            $.each(data.map.thematic, function(i,tag){
                                if((tplCtx.value.tags).indexOf(tag) == -1){                                    
                                    tplCtx.value.tags.push(tag);
                                }
                            });
                        }
                        $.each(costum.lists.themes,function(kt,vt){
                            if(typeof data.map.thematic == "string" && (tplCtx.value.tags).indexOf(vt) >-1 && data.map.thematic != vt ){
                                tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1);                                 
                            }else if(typeof data.map.thematic == "object" && (tplCtx.value.tags).indexOf(vt) >-1  && (data.map.thematic).indexOf(vt) == -1){
                                tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1); 
                            }
                        })
                    } else{
                        if(typeof data.map.thematic == "string"){
                            tplCtx.value.tags.push(data.map.thematic);
                        }
                        else{
                            $.each(data.map.thematic, function(i,tag){
                                tplCtx.value.tags.push(tag);
                            });
                        }
                    }
                   
                }
                if(typeof data.map.objectiveOdd != "undefined"){ 
                    if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                        if(typeof data.map.objectiveOdd == "string" ){
                            if ((tplCtx.value.tags).indexOf(data.map.objectiveOdd) == -1){
                                tplCtx.value.tags.push(data.map.objectiveOdd);
                            }
                        }else if(typeof data.map.objectiveOdd == "object" ){
                            $.each(data.map.objectiveOdd, function(i,tag){
                                if((tplCtx.value.tags).indexOf(tag) == -1){
                                    tplCtx.value.tags.push(tag);
                                }
                            });
                        }
                        $.each(costum.lists.objectiveOdd,function(kf,vf){
                            if(typeof data.map.objectiveOdd == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.objectiveOdd != vf ){
                                tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                            }else if(typeof data.map.objectiveOdd == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.objectiveOdd).indexOf(vf) == -1){
                                tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                            }
                        })
                    }else{
                        if(typeof data.map.objectiveOdd == "string"){
                            tplCtx.value.tags.push(data.map.objectiveOdd);
                        }
                        else{
                            $.each(data.map.objectiveOdd, function(i,tag){
                                tplCtx.value.tags.push(tag);
                            });
                        } 
                    }
                
                }

                mylog.log("data", data);
                tplCtx.collection = "organizations";
                dataHelper.path2Value(tplCtx, function(params) {
                    toastr.success("Startup bien ajouté");
                    urlCtrl.loadByHash("#@"+data.map.slug);
                });
                
            });
        }
    }

}
costum.typeObj.organization.dynFormCostum.afterBuild = function(){   
    if(notEmpty(contextData)){ 
        if(typeof contextData.creator != "undefined" && contextData.creator == userId ){
            $(".roleselect").show(); 
        }else
            $(".roleselect").hide();
    }
    if($("#statusActor").val() == null){
        $("#statusActor").val("Une personne morale");
    }

    if($("#jobFamily").val() != "Financement" || $.inArray("Financement",$("#jobFamily").val()) ==-1){
        $(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").hide();
    }
    if($("#jobFamily").val() == "Financement" || $.inArray("Financement",$("#jobFamily").val()) >= 0){
        $(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").show();
    }
    $("#jobFamily").change(function(){
        setTimeout(function(){
            if($("#jobFamily").val() == "Financement" || $.inArray("Financement",$("#jobFamily").val()) >= 0){
                $(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").show();
            }else {
                $(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").hide(); //cacher les champs financement
            }       
        }, 500)
    });
}

jQuery(document).ready(function() { 
    keyboardNav.keyMapCombo = null
});