function bindAboutPodElement() {
		$("#editGeoPosition").click(function(){
			Sig.startModifyGeoposition(contextData.id, contextData.type, contextData);
			showMap(true);
		});
		
		$("#editElementDetail").on("click", function(){
			switchModeElement();
		});
}

	function changeHiddenFields() { 
		mylog.log("-----------------changeHiddenFields----------------------");
		//
		listFields = [	"username", "birthDate", "email", "avancement", "url", "fixe",
						"mobile","fax", "facebook", "twitter", /*"gpplus",*/ "github", "gitlab", "instagram","telegram","diaspora","mastodon"];
		
		$.each(listFields, function(i,value) {
			mylog.log("listFields", value, typeof contextData[value]);
			if(typeof contextData[value] != "undefined" && contextData[value].length == 0)
				$("."+value).val("<i>"+trad.notSpecified+"<i>");
		});
		mylog.log("-----------------changeHiddenFields END----------------------");
	}

	function removeAddresses (index, formInMap){

		bootbox.confirm({
			message: trad.suredeletelocality+"<span class='text-red'></span>",
			buttons: {
				confirm: {
					label: trad.yes,
					className: 'btn-success'
				},
				cancel: {
					label: trad.no,
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (!result) {
					return;
				} else {
					var addresses = { addressesIndex : index };
					var param = new Object;
					param.name = "addresses";
					param.value = addresses;
					param.pk = contextData.id;
					ajaxPost(
				        null,
				        baseUrl+"/"+moduleId+"/element/updatefields/type/"+contextData.type,
				        param,
				        function(data){
				        	if(data.result){
								toastr.success(data.msg);

								if(formInMap == true){
									$(".locationEl"+ index).remove();
									dyFInputs.locationObj.elementLocation = null;
									dyFInputs.locationObj.elementLocations.splice(ix,1);
									//TODO check if this center then apply on first
									//$(".locationEl"+dyFInputs.locationObj.countLocation).remove();
								}
								else
									urlCtrl.loadByHash(location.hash);
					    	}
				        }
				    );
				}
			}
		});		
	}
	
	function getdynTitle(){
	//Titre de la formulaire date
	var dynTitle = "";
		if(contextData.type == "events"){
	            dynTitle = tradDynForm.editEventDate
	        }else if(contextData.type == "organizations") {
	            dynTitle = tradDynForm.addOrganizationDate
	        }else if (contextData.type == "projects") {
	            dynTitle = tradDynForm.addProjectDate
	        }
	        return dynTitle;
	}  

	function bindDynFormEditable(){ 
		$(".btn-update-when").off().on( "click", function(){
            var editMode = $(this).data("editMode");
            var propDynForm = {};
            if(contextData.type == "organizations"){
                propDynForm = {
                    openingHours :  {
                        inputType : "openingHours",
                        label : tradDynForm.weekrepeat,
                        options: {
                            "allWeek" : true,
                            "multipleHours": false
                        },
                    }
                };
            }else if(contextData.type == "projects"){
                propDynForm = {
                    startDate :  {
                            inputType : "datetime",
                            placeholder: tradDynForm.startDate,
                            label : tradDynForm.startDate
                        },
                        endDate :  {
                            inputType : "datetime",
                            placeholder: tradDynForm.endDate,
                            label : tradDynForm.endDate
                        }
                };
            }

			var dynFormUpDate={
				beforeBuild: {
                    properties : propDynForm    
                },  
                afterSave : function(data,){
					mylog.dir(data);
					if(data.result && notEmpty(data.map)){
						contextData.recurrency=data.map.recurrency;
						if(data.map.recurrency){
							if(typeof contextData.startDate != "undefined")
								delete contextData.startDate;
							if(typeof contextData.endDate != "undefined")
								delete contextData.endDate;
							if(typeof data.map.openingHours != "undefined")
								contextData.openingHours=data.map.openingHours;
						}else{
							if(typeof data.map.startDate != "undefined")
								contextData.startDate = data.map.startDate;
							if(typeof data.map.endDate != "undefined")
								contextData.endDate = data.map.endDate;
							if(typeof contextData.openingHours != "undefined")
								delete contextData.openingHours;
						}
						if(typeof data.map.openingHours != "undefined")
								contextData.openingHours=data.map.openingHours;

						pageProfil.initDateHeaderPage(contextData);
						initDate();
					}
					//urlCtrl.loadByHash(location.hash);
					dyFObj.closeForm();
					pageProfil.views.detail();
					
				},
				onload : {
					actions : {
						setTitle : getdynTitle(),
						hide : {
							"nametext":1,
							"organizerfinder":1,
							"typeselect":1,
							"imageuploader":1,
							"formLocalityformLocality":1,
							"shortDescriptiontextarea":1,
							"tagstags":1,
							"urltext":1,
							"urlsarray" : 1,
                            "emailtext":1,
                            "publiccheckboxSimple":1,
                            "parentfinder":1,
                            "roleselect":1,
                            "locationlocation":1,
						}
					}
				}
			};

			dyFObj.editElement(contextData.type, contextData.id, null, dynFormUpDate);
		});

		$(".btn-update-info").off().on( "click", function(){ 
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update general information"],
						icon : "fa-key",
						type: "object",
						onLoads : {
							initUpdateInfo : function(){
								mylog.log("initUpdateInfo");
								$(".emailOptionneltext").slideToggle();
								$("#ajax-modal .modal-header").removeClass("bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
							},
							onload : function (data) { 
								//this is a hack too a strange bug
								//this select doesn't carry it's value
								if( contextData.type == typeObj.event.col || contextData.type == typeObj.organization.col )
									$("#type").val(data.type);
							}
						},
						afterBuild : function(){
							if(contextData.category=="common"){	
								$("#ajax-modal #avancement").val("");
								$("#ajax-modal .avancementselect").hide();
							}
							$("#ajax-modal #category").on("change",function(){
								var category=$(this).val();
								if(category=="common"){
									$("#ajax-modal .avancementselect").hide();
								}else{
									$("#ajax-modal .avancementselect").show();
								}
							});
						},
						beforeSave : function(){
							mylog.log("beforeSave");
							removeFieldUpdateDynForm(contextData.type);
					    },
						afterSave : function(data){
							mylog.dir(data);
                            //
                            dyFObj.closeForm();
                            coInterface.showLoader("#central-container");
                            //pageProfil.views.detail();
                            urlCtrl.loadByHash(currentUrl);
                            
						},
						properties : {
							block : dyFInputs.inputHidden(),
							name : dyFInputs.name(contextData.type),
							similarLink : dyFInputs.similarLink,
							typeElement : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true)
						}
					}
				}
			};

			if(contextData.type == typeObj.person.col ){
				//form.dynForm.jsonSchema.properties.username = dyFInputs.inputText("Username", "Username", { required : true, validUserName : true,rangelength : [4, 32] }); //,uniqueUserName:true
				form.dynForm.jsonSchema.properties.birthDate = dyFInputs.birthDate;
			}

			if(contextData.type == typeObj.organization.col ){
				//form.dynForm.jsonSchema.properties.type = dyFInputs.inputSelect(tradDynForm.organizationType, tradDynForm.organizationType, organizationTypes, { required : true });
				
				
				// form.dynForm.jsonSchema.properties.famille = {
    //                 "inputType" : "select",
    //                 "label" : "Famille",
    //                 "placeholder" : "Choisir une famille",
    //                 "list" : "famille",
    //                 "optionsValueAsKey" : true,
    //                 "rules" : {
    //                     "required" : true
    //                 }
    //             } ;

   				

				
			}else if(contextData.type == typeObj.event.col ){
				mylog.log("Type event ", typeObj.event.col, contextData.type);
				form.dynForm.jsonSchema.properties.type = dyFInputs.inputSelect(tradDynForm.eventTypes, tradDynForm.eventTypes, eventTypes, { required : true });
				var valueMonthEss = "";
				if(notEmpty(contextData.monthEss))
					valueMonthEss = contextData.monthEss;
				form.dynForm.jsonSchema.properties.monthEss = {
                    "inputType" : "select",
                    "value" : valueMonthEss,
                    "label" : "Voulez-vous que cet événement participe au mois de l'ESS ?",
                    "list" : "yesNo",
                    "optionsValueAsKey" : true,
                    "placeholder" : "Voulez-vous que cet événement participe au mois de l'ESS ?"
                }



			}else if( contextData.type == typeObj.project.col ){
				

				// $.each(avancementProject,function(k,v){
				// 	avancementProject[k] = trad[k]
				// });
				// avancementProject[""] = "Non spécifié";
				// avancementProject.abandoned = "Abandoné";
				
				//form.dynForm.jsonSchema.properties.avancement = dyFInputs.inputSelect(tradDynForm.theprojectmaturity, tradDynForm.projectmaturity, {"idea" : "Idée", "development" : "En cours"});
				form.dynForm.jsonSchema.properties.category = {
                    "inputType" : "select",
                    "label" : "Nature de l'élément",
                    "placeholder" : "Choisir la nature de l'élément",
                    "optionsValueAsKey" : false,
                    "isSelect2": true,
                    "options" : {
                    	"research" : "Recherche-action",
                    	"common" : "Commun"
                    }
                };
                //if(contextData.category=="research"){
	                form.dynForm.jsonSchema.properties.avancement = {
	                    "inputType" : "select",
	                    "label" : "Etat d'avancement",
	                    "placeholder" : "Etat d'avancement",
	                    "optionsValueAsKey" : false,
	                    "isSelect2": true,
	                    "list" : "state"
	                };
	            //}    
			}

			form.dynForm.jsonSchema.properties.tags = dyFInputs.tags();
			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				var ruleMail = ( (contextData.type == typeObj.person.col) ? { email: true, required : true } : { email: true } ) ;
				form.dynForm.jsonSchema.properties.email = dyFInputs.email(tradDynForm.mainemail, tradDynForm.mainemail, ruleMail);
			}
			
			if(contextData.type == typeObj.person.col || contextData.type == typeObj.organization.col ){
				form.dynForm.jsonSchema.properties.fixe= dyFInputs.inputText(tradDynForm["fix"],tradDynForm["enterfixnumber"]);
				form.dynForm.jsonSchema.properties.mobile= dyFInputs.inputText(tradDynForm["mobile"],tradDynForm["entermobilenumber"]);
				//form.dynForm.jsonSchema.properties.fax= dyFInputs.inputText(tradDynForm["fax"],tradDynForm["enterfaxnumber"]);
			}

			if(contextData.type != typeObj.poi.col) 
				form.dynForm.jsonSchema.properties.url = dyFInputs.inputUrl();


			var listParent =  ["organizations"] ;

			if(contextData.type == typeObj.event.col)
				listParent =  ["events"] ;
			else if(contextData.type == typeObj.project.col)
				listParent =  ["organizations", "projects"] ;

			if( contextData.type == typeObj.event.col || 
				contextData.type == typeObj.project.col || 
				contextData.type == typeObj.organization.col){
				multiple=(contextData.type==typeObj.event.col)? false : true;
	            label=(contextData.type==typeObj.event.col)? tradDynForm.ispartofevent : tradDynForm.whoiscarrytheproject;
	            initType=(contextData.type==typeObj.event.col)? ["events"] : ["organizations", "projects"];
	            form.dynForm.jsonSchema.properties.parent= {
	            	inputType : "finder",
		            label : label,
		           	multiple : multiple,
		           	initType: initType,
		           	update : true,
        			openSearch :true,
        			values : (typeof contextData.parent != "undefined" && Object.keys(contextData.parent).length > 0) ? contextData.parent : null
	            };
	            if(contextData.type==typeObj.event.col){
	            	form.dynForm.jsonSchema.properties.parent.init= function(){
	            		var finderParams = {
		        			id : "parent",
		        			multiple : false,
		        			initType : ["events"],
		        			values : form.dynForm.jsonSchema.properties.parent.values,
		        			update : true
		        		};
		        		finder.init(finderParams, function(){
	    						$.each(finder.selectedItems, function(e, v){
	    							startDateParent=v.startDate;
	    							endDateParent=v.endDate;
	    						});
			            		$("#startDateParent").val(startDateParent);
			            		$("#endDateParent").val(endDateParent);
			            		if($("#parentstartDate").length <= 0){
				            		$("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
			    					$("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
			            		}
			            		$("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
				    			$("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm'));	
	    					});
	    				// finder.init("parent", false, ["events"], form.dynForm.jsonSchema.properties.parent.values, true, 
	    				// 	function(){
	    				// 		$.each(finder.selectedItems, function(e, v){
	    				// 			startDateParent=v.startDate;
	    				// 			endDateParent=v.endDate;
	    				// 		});
			      //       		$("#startDateParent").val(startDateParent);
			      //       		$("#endDateParent").val(endDateParent);
			      //       		if($("#parentstartDate").length <= 0){
				     //        		$("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent ,"YYYY-MM-DD HH:mm").format('DD/MM/YYYY HH:mm')+"</span>");
			    		// 			$("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
			      //       		}
			      //       		$("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
				    	// 		$("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm'));	
	    				// 	}
	    				// );
    				}
	            }
	        }

            if(contextData.type == typeObj.event.col){
            	form.dynForm.jsonSchema.properties.organizer= {
	            	inputType : "finder",
		            label : tradDynForm.whoorganizedevent,
		           	initType: ["projects", "organizations"],
	    			multiple : true,
	    			update : true,
	    			openSearch :true,
	    			values : (typeof contextData.organizer != "undefined" && Object.keys(contextData.organizer).length > 0) ? contextData.organizer : null
	            };
	            if(contextData.type==typeObj.event.col){
	            	form.dynForm.jsonSchema.properties.organizer.init= function(){

	            		var finderParams = {
		        			id : "organizer",
		        			multiple : true,
		        			initType : ["projects", "organizations"],
		        			values : form.dynForm.jsonSchema.properties.organizer.values,
		        			update : true
		        		};
		        		finder.init(finderParams);
	    				//finder.init("organizer", true, ["projects", "organizations"], form.dynForm.jsonSchema.properties.organizer.values, true);
    				}
	            }
            	//form.dynForm.jsonSchema.properties.organizerId =  dyFInputs.organizerId(contextData.parentId, contextData.parentType);
	            //form.dynForm.jsonSchema.properties.organizerType = dyFInputs.inputHidden();
            }
            
			
			var dataUpdate = {
				block : "info",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name,	
			};
			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0 && typeof formData != "undefined") {
				$.each(finder.object, function(key, object){
					formData[key]=object;
				});
			}
			if(notNull(contextData.slug) && contextData.slug.length > 0)
				dataUpdate.slug = contextData.slug;

			if(notNull(contextData.tags) && contextData.tags.length > 0)
				dataUpdate.tags = contextData.tags;

			if(contextData.type == typeObj.person.col ){
				if(notNull(contextData.username) && contextData.username.length > 0)
					dataUpdate.username = contextData.username;
				if(notEmpty(contextData.birthDate))
					dataUpdate.birthDate = moment(contextData.birthDate.sec * 1000).local().format("DD/MM/YYYY");
			}
			
			mylog.log("ORGA ", contextData.type, typeObj.organization.col, dataUpdate.type);
			
			if(contextData.type == typeObj.organization.col ){
				// mylog.log("ORGA type", contextData.typeOrga, contextData.typeOrganization);
				// if(notEmpty(contextData.typeOrga))
				// 	dataUpdate.type = contextData.typeOrga;
				// mylog.log("ORGA resultType", dataUpdate.type);
				

			}else if(contextData.type == typeObj.event.col ){
				if(jsonHelper.notNull("contextData.typeEvent") )
					dataUpdate.type = contextData.typeEvent;

				if(notEmpty(contextData.monthEss))
					dataUpdate.monthEss = contextData.monthEss;

			}else if(contextData.type == typeObj.project.col ){
				if(notEmpty(contextData.avancement))
					dataUpdate.avancement = contextData.avancement;
				if(notEmpty(contextData.category))
					dataUpdate.category = contextData.category;
				// if(notEmpty(contextData.state))
				// 	dataUpdate.state = contextData.state;
			}

			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				mylog.log("test email", contextData, contextData.email);
				if(notEmpty(contextData.email)) {
					mylog.log("test email2", contextData, contextData.email);
					dataUpdate.email = contextData.email;
				}
				if(notEmpty(contextData.fixe))
					dataUpdate.fixe = contextData.fixe;
				if(notEmpty(contextData.mobile))
					dataUpdate.mobile = contextData.mobile;
				//if(notEmpty(contextData.fax))
					//dataUpdate.fax = contextData.fax;
			}
			
			if(contextData.type != typeObj.poi.col && notEmpty(contextData.url)) 
				dataUpdate.url = contextData.url;

			if(notEmpty(contextData.parentId)) 
				dataUpdate.parentId = contextData.parentId;
			else
				dataUpdate.parentId = "dontKnow";

			if(notEmpty(contextData.parentType)) 
				dataUpdate.parentType = contextData.parentType;

			if(notEmpty(contextData.organizerId)) 
				dataUpdate.organizerId = contextData.organizerId;

			if(notEmpty(contextData.organizerType)) 
				dataUpdate.organizerType = contextData.organizerType;
			
			mylog.log("dataUpdate", dataUpdate);
			dyFObj.openForm(form, "initUpdateInfo", dataUpdate);
		});

		$(".btn-update-descriptions").off().on( "click", function(){

			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update description"],
						icon : "fa-key",
						onLoads : {
							
							markdown : function(){
								dataHelper.activateMarkdown("#ajaxFormModal #description");
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
								//bindDesc("#ajaxFormModal");
								///Initialisation de ton textearea
								// mentionsInit.get("textarea#description");
								// if(typeof(contextData.descMentions) != "undefined")
        //   							$("textarea#description").val( mentionsInit.addMentionInText( $("textarea#description"), contextData.descMentions ) ) ;
							}
						},
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){
								if(data.resultGoods.values.shortDescription=="")
									$(".contentInformation #shortDescriptionAbout").html('<i>'+trad["notSpecified"]+'</i>');
								else
									$(".contentInformation #shortDescriptionAbout").html(data.resultGoods.values.shortDescription);
								$(".contentInformation #shortDescriptionAboutEdit").html(data.resultGoods.values.shortDescription);
								$("#shortDescriptionHeader").html(data.resultGoods.values.shortDescription);
								if(data.resultGoods.values.description=="")
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml('<i>'+trad["notSpecified"]+'</i>'));
								else
									$(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml(data.resultGoods.values.description));
								$("#descriptionMarkdown").html(data.resultGoods.values.description);
								contextData.description = data.resultGoods.values.description;
							}
							
							dyFObj.closeForm();
							changeHiddenFields();
							pageProfil.views.detail();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							descMentions : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true),
							shortDescription : 	dyFInputs.textarea(tradDynForm["shortDescription"], "...",{ maxlength: 140 }),
							description : dyFInputs.textarea(tradDynForm["longDescription"], "..."),
						}
					}
				}
			};

			var dataUpdate = {
				block : "descriptions",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name,
		        shortDescription : $(".contentInformation #shortDescriptionAboutEdit").html(),
				description: contextData.description,	
			};

			dyFObj.openForm(form, "markdown", dataUpdate);
		});
		$(".btn-update-otherInfo").off().on( "click", function(){

			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : "Mettre à jour les informations",
						icon : "fa-key",
						onLoads : {
							onload : function (data) { 
								//this is a hack too a strange bug
								//this select doesn't carry it's value
								if( contextData.type == typeObj.event.col || contextData.type == typeObj.organization.col )
									$("#type").val(data.type);
							}
							
						},
						afterSave : function(data){
							mylog.dir(data);
                            dyFObj.closeForm();
                            coInterface.showLoader("#central-container");
                            urlCtrl.loadByHash(currentUrl);
                            
						},
						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							descMentions : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true)
						}
					}
				}
			};

			//form.dynForm.jsonSchema.properties.doYouHaveEmployees = dyFInputs.inputText("Avez-vous des salariés ?","Avez-vous des salariés ?");
            form.dynForm.jsonSchema.properties.doYouHaveEmployees = {
                    "inputType" : "select",
                    "label" : "Avez-vous des salariés ?",
                    "placeholder" : "Avez-vous des salariés ?",
                    "optionsValueAsKey" : true,
                    "list" : "yesNo",
                    "isSelect2" : true
                };
            
            form.dynForm.jsonSchema.properties.ifYesEmployees = dyFInputs.inputText("Si oui ,combien avez-vous de salarié ?","Si oui ,combien avez-vous de salarié ?");
            form.dynForm.jsonSchema.properties.doYouHaveVolunteers = {
                    "inputType" : "select",
                    "label" : "Avez-vous des bénévoles ?",
                    "placeholder" : "Avez-vous des bénévoles ?",
                    "optionsValueAsKey" : true,
                    "list" : "yesNo",
                    "isSelect2" : true
                };
            form.dynForm.jsonSchema.properties.ifYesVolunteers = dyFInputs.inputText("Si oui combien avez-vous de bénévole ?","Si oui combien avez-vous de bénévole ?");


           
            var dataUpdate = {
				block : "otherInfo",
				id : contextData.id,
				typeElement : contextData.type,
			};

            if(typeof contextData.doYouHaveEmployees != "undefined")
            	dataUpdate.doYouHaveEmployees = contextData.doYouHaveEmployees;

            if(typeof contextData.ifYesEmployees != "undefined")
            	dataUpdate.ifYesEmployees = contextData.ifYesEmployees;

            if(typeof contextData.doYouHaveVolunteers != "undefined")
            	dataUpdate.doYouHaveVolunteers = contextData.doYouHaveVolunteers;

            if(typeof contextData.ifYesVolunteers != "undefined")
            	dataUpdate.ifYesVolunteers = contextData.ifYesVolunteers;

			dyFObj.openForm(form, "onload", dataUpdate);
		});


		$(".btn-update-network").off().on( "click", function(){
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update network"],
						icon : "fa-key",
						onLoads : {
							sub : function(){
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  				  .addClass("bg-dark");
								//bindDesc("#ajaxFormModal");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave", contextData["socialNetwork"]);
					    	//removeFieldUpdateDynForm(contextData.type);
					    	var SNetwork = [ "telegram", "github", "gitlab", "twitter", "facebook", /*"gpplus",*/ "instagram", "diaspora", "mastodon"];
							$.each(SNetwork, function(key, val){ 
								mylog.log("val", val);
								mylog.log("val2", $("#ajaxFormModal #"+val).val(), $("#ajaxFormModal #"+val).length);

								if(	notNull(contextData["socialNetwork"]) && 
									notNull(contextData["socialNetwork"][val]) &&
									( 	$("#ajaxFormModal #"+val).length &&
										$("#ajaxFormModal #"+val).val().trim() == contextData["socialNetwork"][val] ) ) {
									mylog.log("if", val);
									$("#ajaxFormModal #"+val).remove();
								} else if (	(	!notNull(contextData["socialNetwork"]) || 
												!notNull(contextData["socialNetwork"][val]) ) && $("#ajaxFormModal #"+val).length ){
									mylog.log("else", val);
									//$("#ajaxFormModal #"+val).remove();
								}
									
							});
					    },
						afterSave : function(data){
							mylog.dir(data);
							if(data.result&& data.resultGoods && data.resultGoods.result){

								if(!notEmpty(contextData.socialNetwork))
									contextData.socialNetwork = {};

								if(typeof data.resultGoods.values.telegram != "undefined"){
									contextData.socialNetwork.telegram = data.resultGoods.values.telegram.trim();
									if(contextData.socialNetwork.telegram.length != 0 )
										changeNetwork('#divTelegram', '#telegramAbout', 'https://web.telegram.org/#/im?p=@'+contextData.socialNetwork.telegram, 'https://web.telegram.org/#/im?p=@'+contextData.socialNetwork.telegram);
									else
										changeNetwork('#divTelegram', '#telegramAbout', contextData.socialNetwork.telegram, contextData.socialNetwork.telegram);
								}

								if(typeof data.resultGoods.values.diaspora != "undefined"){
									contextData.socialNetwork.diaspora = data.resultGoods.values.diaspora.trim();
									changeNetwork('#divDiaspora', '#diasporaAbout', contextData.socialNetwork.diaspora, contextData.socialNetwork.diaspora);
								}

								if(typeof data.resultGoods.values.mastodon != "undefined"){
									contextData.socialNetwork.mastodon = data.resultGoods.values.mastodon.trim();
									changeNetwork('#divMastodon', '#mastodonAbout', contextData.socialNetwork.mastodon, contextData.socialNetwork.mastodon);
								}

								if(typeof data.resultGoods.values.facebook != "undefined"){
									contextData.socialNetwork.facebook = data.resultGoods.values.facebook.trim();
									changeNetwork('#divFacebook','#facebookAbout', contextData.socialNetwork.facebook, contextData.socialNetwork.facebook);
								}

								if(typeof data.resultGoods.values.twitter != "undefined"){
									contextData.socialNetwork.twitter = data.resultGoods.values.twitter.trim();
									changeNetwork('#divTwitter','#twitterAbout', contextData.socialNetwork.twitter, contextData.socialNetwork.twitter);
								}

								if(typeof data.resultGoods.values.github != "undefined"){
									contextData.socialNetwork.github = data.resultGoods.values.github.trim();
									changeNetwork('#divGithub','#githubAbout', contextData.socialNetwork.github, contextData.socialNetwork.github);
								}

								if(typeof data.resultGoods.values.gitlab != "undefined"){
									contextData.socialNetwork.gitlab = data.resultGoods.values.gitlab.trim();
									changeNetwork('#divGitlab','#gitlabAbout', contextData.socialNetwork.gitlab, contextData.socialNetwork.gitlab);
								}
								/*
								if(typeof data.resultGoods.values.gpplus != "undefined"){
									contextData.socialNetwork.gpplus = data.resultGoods.values.gpplus.trim();
									changeNetwork('#divGpplus','#gpplusAbout', contextData.socialNetwork.gpplus, contextData.socialNetwork.gpplus);
								}*/

								if(typeof data.resultGoods.values.instagram != "undefined"){
									contextData.socialNetwork.instagram = data.resultGoods.values.instagram.trim();
									changeNetwork('#divInstagram','#instagramAbout', contextData.socialNetwork.instagram, contextData.socialNetwork.instagram);
								}
							}
							dyFObj.closeForm();
							changeHiddenFields();
						},

						properties : {
							block : dyFInputs.inputHidden(),
							typeElement : dyFInputs.inputHidden(),
							isUpdate : dyFInputs.inputHidden(true), 
							gitlab : dyFInputs.inputUrl(tradDynForm["linkGitlab"]),
							github : dyFInputs.inputUrl(tradDynForm["linkGithub"]), 
							//gpplus : dyFInputs.inputUrl(tradDynForm["linkGplus"]),
							twitter : dyFInputs.inputUrl(tradDynForm["linkTwitter"]),
							facebook :  dyFInputs.inputUrl(tradDynForm["linkFacebook"]),
							instagram :  dyFInputs.inputUrl(tradDynForm["linkInstagram"]),
					        diaspora :  dyFInputs.inputUrl(tradDynForm["linkDiaspora"]),
					        mastodon :  dyFInputs.inputUrl(tradDynForm["linkMastodon"]),
						}
					}
				}
			};

			if(contextData.type == typeObj.person.col ){
				form.dynForm.jsonSchema.properties.telegram = dyFInputs.inputText("Votre Speudo Telegram","Votre Speudo Telegram");
			}

			var dataUpdate = {
				block : "network",
				id : contextData.id,
				typeElement : contextData.type,
			};

			if(notEmpty(contextData.socialNetwork) )
			{
				if( notEmpty(contextData.socialNetwork.twitter) )
					dataUpdate.twitter = contextData.socialNetwork.twitter;
				/*if( notEmpty(contextData.socialNetwork.googleplus) )
					dataUpdate.gpplus = contextData.socialNetwork.googleplus;*/
				if( notEmpty(contextData.socialNetwork.github) )
					dataUpdate.github = contextData.socialNetwork.github;
				if( notEmpty(contextData.socialNetwork.gitlab) )
					dataUpdate.gitlab = contextData.socialNetwork.gitlab;
				if( notEmpty(contextData.socialNetwork.telegram) )
					dataUpdate.telegram = contextData.socialNetwork.telegram;
				if( notEmpty(contextData.socialNetwork.facebook) )
					dataUpdate.facebook = contextData.socialNetwork.facebook;
				if(notEmpty(contextData.socialNetwork.instagram))
					dataUpdate.instagram = contextData.socialNetwork.instagram;
				if( notEmpty(contextData.socialNetwork.diaspora) )
					dataUpdate.diaspora = contextData.socialNetwork.diaspora;
				if( notEmpty(contextData.socialNetwork.mastodon) )
					dataUpdate.mastodon = contextData.socialNetwork.mastodon;
			}
			dyFObj.openForm(form, "sub", dataUpdate);

			
		});
	}
	
	function changeNetwork(id, idUrl, url, str){
		mylog.log("changeNetwork", id, idUrl, url, str.length);
		//$(id).attr('href', url);

		if(str.length == 0)
			$(id).html(trad.notSpecified);
		else
			$(id).html('<a href="'+url+'" target="_blank" id="'+id+'" class="socialIcon" >'+str+'</a>');



		
	}

	function parsePhone(arrayPhones){
		var str = "";
		$.each(arrayPhones, function(i,num) {
			if(str != "")
				str += ", ";
			str += num.trim();
		});
		return str ;
	}


	/*function bindDesc(parent){
		$(".maxlengthTextarea").off().keyup(function(){
			var name = "#" + $(this).attr("id") ;
			mylog.log(".maxlengthTextarea", parent+" "+name, $(this).attr("id"), $(parent+" "+name).val().length, $(this).val().length);
			$(parent+" #maxlength"+$(this).attr("id")).html($(parent+" "+name).val().length);
			maxlengthshortDescription
		});
	}*/


	function updateUrl(ind, title, url, type) {
		mylog.log("updateUrl", ind, title, url, type);
		var params = {
			title : title,
			type : type,
			url : url,
			index : ind.toString()
		}
		mylog.log("params",params);
		dyFObj.openForm( 'url','sub', params);
	}

	function updateRoles(childId, childType, childName, connectType, roles) {
		mylog.log("editInPlace costum")
		var form = {
				saveUrl : baseUrl+"/"+moduleId+"/link/removerole/",
				dynForm : {
					jsonSchema : {
						title : tradDynForm.modifyoraddroles+"<br/>"+childName,// trad["Update network"],
						icon : "fa-key",
						onLoads : {
							sub : function(){
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  				  .addClass("bg-dark");
								//bindDesc("#ajaxFormModal");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave");
					    	//removeFieldUpdateDynForm(contextData.type);
					    	var r = [];
				    	$('#s2id_roles li').each(function(i,li){ 
				    		
				    		if(typeof $(li).children("div").html()!="undefined") 
								r.push($(li).children("div").html()) 
							})
				    	$("#roles").val( r.join() ) ;
					    },
						afterSave : function(data){
							mylog.dir(data);
							dyFObj.closeForm();
							pageProfil.views.directory();
							//loadDataDirectory(connectType, "user", true);
							//changeHiddenFields();
						},
						properties : {
							contextId : dyFInputs.inputHidden(),
							contextType : dyFInputs.inputHidden(), 
							roles : {
								"inputType" : "tags",
								"label":tradDynForm["addroles"],
								"placeholder":tradDynForm["addroles"],
								"values":rolesList
							},//dyFInputs.tags(rolesList, tradDynForm["addroles"] , tradDynForm["addroles"], null, true),
							childId : dyFInputs.inputHidden(), 
							childType : dyFInputs.inputHidden(),
							connectType : dyFInputs.inputHidden()
						}
					}
				}
			};

			var dataUpdate = {
		        contextId : contextData.id,
		        contextType : contextData.type,
		        childId : childId,
		        childType : childType,
		        connectType : connectType,
			};

			if(notEmpty(roles))
				dataUpdate.roles = roles.split(",");
			dyFObj.openForm(form, "sub", dataUpdate);		
	}
	function updateBookmark(id) {
		mylog.log("updBook",id);
		filesUp=docsList[id];
		var params=new Object;
		params.id=id;
		if(filesUp.url != "undefined")
			params.url=filesUp.url;
		if(filesUp.name != "undefined")
			params.name=filesUp.name;
		if(filesUp.tags != "undefined")
			params.tags=filesUp.tags;
		if(filesUp.description != "undefined")
			params.description=filesUp.description;
		mylog.log("params",params);
		dyFObj.openForm( 'bookmark','sub', params);
	}


	function updateContact(ind, name, email, role, telephone) {
		mylog.log("updateContact", ind, name, email, role, telephone);
		dataUpdate = { index : ind.toString() } ;
		if(name != "undefined")
			dataUpdate.name = name;
		if(email != "undefined")
			dataUpdate.email = email;
		if(role != "undefined")
			dataUpdate.role = role;
		if(telephone != "undefined")
			dataUpdate.phone = telephone;
		mylog.log("dataUpdate", dataUpdate);
		dyFObj.openForm ('contactPoint','sub', dataUpdate);
	}
	function updateDocument(id, title) {
		mylog.log("updateDocument", id, name);
		dataUpdate = { docId : id } ;
		if(title != "undefined")
			dataUpdate.title = title;
		mylog.log("dataUpdate", dataUpdate);
		dyFObj.openForm ('document','sub', dataUpdate);
	}

	function removeUrl(ind) {
		bootbox.confirm({
			message: trad["suretodeletelink"]+"<span class='text-red'></span>",
			buttons: {
				confirm: {
					label: trad["yes"],
					className: 'btn-success'
				},
				cancel: {
					label: trad["no"],
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (!result) {
					return;
				} else {
					param = new Object;
			    	param.name = "urls";
			    	param.value = {index : ind};
			    	param.pk = contextData.id;
					param.type = contextData.type;
					ajaxPost(
				        null,
				        baseUrl+"/"+moduleId+"/element/updatefields/type/"+contextData.type,
				        param,
				        function(data){
				        	mylog.log("data", data);
					    	if(data.result){
								toastr.success(data.msg);
								urlCtrl.loadByHash(location.hash);
					    	}
				        }
				    );
				}
			}
		});
		
	}

	function removeContact(ind) {
		bootbox.confirm({
			message: trad["suretodeletecontact"]+"<span class='text-red'></span>",
			buttons: {
				confirm: {
					label: trad["yes"],
					className: 'btn-success'
				},
				cancel: {
					label: trad["no"],
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				if (!result) {
					return;
				} else {
					param = new Object;
			    	param.name = "contacts";
			    	param.value = {index : ind};
			    	param.pk = contextData.id;
					param.type = contextData.type;
					ajaxPost(
				        null,
				        baseUrl+"/element/updatefields/type/"+contextData.type,
				        param,
				        function(data){
				        	mylog.log("data", data);
					    	if(data.result){
								toastr.success(data.msg);
								urlCtrl.loadByHash(location.hash);
					    	}
				        }
				    );
				}
			}
		});
	}

	function updateNetwork(id) {
		mylog.log("updateNetwork", id);
		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/network/get/id/"+id,
	        {},
	        function(data){
	        	mylog.log("data", data);
				var update = {};
				update.id = id;
				update.visible = data.visible;
				update["skin[title]"] = data.skin.title;
				update["skin[shortDescription]"] = data.skin.title;
				update["skin[logo]"] = data.skin.logo;
				update["displayCommunexion"] = dataHelper.stringToBool(data.skin.displayCommunexion);
				//update["add"] = dataHelper.stringToBool(data.skin.add);
				update["request[scope]"] = data.request.scope;
				update["request[searchType]"] = data.request.searchType;
				update["request[searchTag]"] = data.request.searchTag;
				update["request[sourceKey]"] = data.request.sourceKey;

				update.visible = data.visible;

				var filters = [] ;
				var i = 0 ;
				$.each(data.filter.linksTag, function(cat, fil){
					var f = {};
					f.name = cat;
					$.each(fil,function(ktag,vtag) {
						i = 0 ;
						if(ktag == "tags"){
							mylog.log("updateNetwork vtag", typeof vtag, vtag);
							$.each(vtag,function(k,v) {
								f["keyVal"+i] = k ;
								f["tagskeyVal"+i] = v.toString();
								i++;
							});
							
						}
					});
					filters.push( f );
				});
				update.filter = filters;
				dyFObj.openForm( 'network','sub', update);
	        }
	    );
	}

	function removeNetwork(id) {
		bootbox.confirm({
			message: trad["suretodeletecontact"]+"<span class='text-red'></span>",
			buttons: {
				confirm: {
					label: trad["yes"],
					className: 'btn-success'
				},
				cancel: {
					label: trad["no"],
					className: 'btn-danger'
				}
			},
			callback: function (result) {
				alert("Voir avec TIBTIB");
				// if (!result) {
				// 	return;
				// } else {
				// 	param = new Object;
			 //    	param.id = id;
			 //    	param.userId = userId;
				/* 	ajaxPost(
				        null,
				        baseUrl+"/"+moduleId+"/network/remove",
				        {},
				        function(data){
				        	mylog.log("data", data);
				// 	    	if(data.result){
				// 				toastr.success(data.msg);
				// 				urlCtrl.loadByHash(location.hash);
				// 	    	}
				        }
			  	 	);*/
				// }
			}
		});
	}

	function removeFieldUpdateDynForm(collection){
		mylog.log("------------------------ removeFieldUpdateDynForm", collection);
		var fieldsElement = [ 	"name", "slug", "email", "url", "fixe", "mobile", "fax", 
								"telegram", "github", "gitlab", "twitter", "facebook", /*"gpplus",*/
								"diaspora","mastodon"];
		var fieldsPerson = ["username",  "birthDate"];
		var fieldsProject = [ "startDate", "endDate", "parentId" ];
		var fieldsOrga = [ "type", "parentId" ];
		var fieldsEvent = [ "type", "startDate", "endDate", "parentId", "organizerId"];

		var SNetwork = [ "telegram", "github", "gitlab", "twitter", "facebook", /*"gpplus",*/"diaspora","mastodon"];

		if(collection == typeObj.person.col)
			fieldsElement = fieldsElement.concat(fieldsPerson);
		else if(collection == typeObj.project.col)
			fieldsElement = fieldsElement.concat(fieldsProject);
		else if(collection == typeObj.organization.col)
			fieldsElement = fieldsElement.concat(fieldsOrga)
		else if(collection == typeObj.event.col)
			fieldsElement = fieldsElement.concat(fieldsEvent);
		var valCD = "";


		$.each(fieldsElement, function(key, val){ 

			valCD = val;
			if(val == "type" && collection == typeObj.organization.col)
				valCD = "typeOrga";
			else if(val == "type" && collection == typeObj.event.col)
				valCD = "typeEvent";

			mylog.log("val", val, valCD);
			mylog.log("val2", $("#ajaxFormModal #"+val).length);
			if(	$("#ajaxFormModal #"+val).length && 
				( 	( 	typeof contextData[valCD] != "undefined" && 
						contextData[valCD] != null && 
						$("#ajaxFormModal #"+val).val() == contextData[valCD] 
					) ||  
					( 	( 	typeof contextData[valCD] == "undefined" || 
							contextData[valCD] == null ) && 
						$("#ajaxFormModal #"+val).val().trim().length == 0 ) || 
					//social network
					( 	$.inArray( val, SNetwork ) >= 0 && 
						( 	typeof contextData["socialNetwork"] != "undefined" && 
							contextData["socialNetwork"] != null ) && 
						(
							( 	typeof contextData["socialNetwork"][val] != "undefined" || 
								contextData["socialNetwork"][val] != null && 
								$("#ajaxFormModal #"+val).val().trim() == contextData["socialNetwork"][val] )
							||
							( 	( 	typeof contextData["socialNetwork"][val] == "undefined" || 
								contextData["socialNetwork"][val] == null ) && 
							$("#ajaxFormModal #"+val).val().trim().length == 0 ) )
					)
				) 
			) {
				$("#ajaxFormModal #"+val).remove();
			}
			else if(val == "birthDate"){
				var dateformat = "DD/MM/YYYY";
			    $("#ajaxFormModal #"+val).val( moment( $("#ajaxFormModal #"+val).val(), dateformat).format());
			}

		});
	}


