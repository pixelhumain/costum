var widthLabel = {
  "modeLg":tradCms.largemode,
  "modeMd":tradCms.desktopmode,
  "modeSm":tradCms.tabletmode,
  "modeXs":tradCms.phonemode
},marginLabel = {
  "marginTop":tradCms.top,
  "marginBottom":tradCms.bottom,
  /*"marginLeft":"Gauche",
  "marginRight":"Droite"*/
},paddingLabel = {
  "paddingTop":tradCms.top,
  "paddingBottom":tradCms.bottom,
  "paddingLeft":tradCms.left,
  "paddingRight":tradCms.right
},
blockCmsBgLabel = {
"blockCmsBgTarget" : tradCms.targetblc,
"blockCmsBgType" : trad.type,
"blockCmsBgColor": tradCms.color,
"blockCmsBgImg"  : trad.image,
"blockCmsBgPaint": tradCms.paintedblc,
"blockCmsBgSize"  : tradCms.size,
"blockCmsBgPosition"  : tradCms.position,
"blockCmsBgRepeat"  : tradCms.repeat,
},
blockCmsPoliceLabel = {
  "blockCmsPoliceTitle1" : tradCms.title+" 1",
  "blockCmsPoliceTitle2" : tradCms.title+" 2",
  "blockCmsPoliceTitle3" : tradCms.title+" 3",
  "blockCmsPoliceTitle4" : tradCms.title+" 4",
  "blockCmsPoliceTitle5" : tradCms.title+" 5",
  "blockCmsPoliceTitle6" : tradCms.title+" 6",
},
blockCmsTextSizeLabel = {
"blockCmsTextSizeTitle1" : tradCms.title+" 1 (px)",
"blockCmsTextSizeTitle2" : tradCms.title+" 2 (px)",
"blockCmsTextSizeTitle3" : tradCms.title+" 3 (px)",
"blockCmsTextSizeTitle4" : tradCms.title+" 4 (px)",
"blockCmsTextSizeTitle5" : tradCms.title+" 5 (px)",
"blockCmsTextSizeTitle6" : tradCms.title+" 6 (px)",
},
blockCmsTextLineHeightLabel = {
"blockCmsTextLineHeightTitle1" : tradCms.title+" 1",
"blockCmsTextLineHeightTitle2" : tradCms.title+" 2",
"blockCmsTextLineHeightTitle3" : tradCms.title+" 3",
"blockCmsTextLineHeightTitle4" : tradCms.title+" 4",
"blockCmsTextLineHeightTitle5" : tradCms.title+" 5",
"blockCmsTextLineHeightTitle6" : tradCms.title+" 6",
},
blockCmsTextUnderlineLabel = {
"blockCmsUnderlineTitle1": tradCms.title+" 1 (T1)",
"blockCmsUnderlineTitle2": tradCms.title+" 2 (T2)",
"blockCmsUnderlineTitle3": tradCms.title+" 3 (T3)",
"blockCmsUnderlineTitle4": tradCms.title+" 4 (T4)",
"blockCmsUnderlineTitle5": tradCms.title+" 5 (T5)",
"blockCmsUnderlineTitle6": tradCms.title+" 6 (T6)",

"blockCmsUnderlineColorTitle1" : tradCms.color+" T1",
"blockCmsUnderlineColorTitle2" : tradCms.color+" T2",
"blockCmsUnderlineColorTitle3" : tradCms.color+" T3",
"blockCmsUnderlineColorTitle4" : tradCms.color+" T4",
"blockCmsUnderlineColorTitle5" : tradCms.color+" T5",
"blockCmsUnderlineColorTitle6" : tradCms.color+" T6",

"blockCmsUnderlineWidthTitle1" : tradCms.size+" T1",
"blockCmsUnderlineWidthTitle2" : tradCms.size+" T2",
"blockCmsUnderlineWidthTitle3" : tradCms.size+" T3",
"blockCmsUnderlineWidthTitle4" : tradCms.size+" T4",
"blockCmsUnderlineWidthTitle5" : tradCms.size+" T5",
"blockCmsUnderlineWidthTitle6" : tradCms.size+" T6",


"blockCmsUnderlineHeightTitle1" : tradCms.height+" T1",
"blockCmsUnderlineHeightTitle2" : tradCms.height+" T2",
"blockCmsUnderlineHeightTitle3" : tradCms.height+" T3",
"blockCmsUnderlineHeightTitle4" : tradCms.height+" T4",
"blockCmsUnderlineHeightTitle5" : tradCms.height+" T5",
"blockCmsUnderlineHeightTitle6" : tradCms.height+" T6",

"blockCmsUnderlineSpaceTitle1" : tradCms.spacebetween+" T1",
"blockCmsUnderlineSpaceTitle2" : tradCms.spacebetween+" T2",
"blockCmsUnderlineSpaceTitle3" : tradCms.spacebetween+" T3",
"blockCmsUnderlineSpaceTitle4" : tradCms.spacebetween+" T4",
"blockCmsUnderlineSpaceTitle5" : tradCms.spacebetween+" T5",
"blockCmsUnderlineSpaceTitle6" : tradCms.spacebetween+" T6",

"blockCmsUnderlineMargeBottomTitle1" : tradCms.marginbottom+" T1",
"blockCmsUnderlineMargeBottomTitle2" : tradCms.marginbottom+" T2",
"blockCmsUnderlineMargeBottomTitle3" : tradCms.marginbottom+" T3",
"blockCmsUnderlineMargeBottomTitle4" : tradCms.marginbottom+" T4",
"blockCmsUnderlineMargeBottomTitle5" : tradCms.marginbottom+" T5",
"blockCmsUnderlineMargeBottomTitle6" : tradCms.marginbottom+" T6",
},
blockCmsBorderLabel = {
  "blockCmsBorderTop" : tradCms.top,
  "blockCmsBorderBottom" : tradCms.bottom,
  "blockCmsBorderLeft" : tradCms.left,
  "blockCmsBorderRight": tradCms.right,
  "blockCmsBorderColor": tradCms.color,
  "blockCmsBorderWidth": tradCms.thickness,
  "blockCmsBorderType": trad.type
},
blockCmsLineSeparatorLabel = {
  "blockCmsLineSeparatorTop" : tradCms.top,
  "blockCmsLineSeparatorBottom" : tradCms.bottom,
  "blockCmsLineSeparatorWidth" : tradCms.length+"(%)",
  "blockCmsLineSeparatorHeight" : tradCms.height,
  "blockCmsLineSeparatorBg" : tradCms.color,
  "blockCmsLineSeparatorPosition" : tradCms.position,
  "blockCmsLineSeparatorIcon" : tradCms.smalliconinthemiddle+" :"
},
blockCmsTextAlignLabel = {
"blockCmsTextAlignTitle1" : tradCms.title+" 1",
"blockCmsTextAlignTitle2" : tradCms.title+" 2",
"blockCmsTextAlignTitle3" : tradCms.title+" 3",
"blockCmsTextAlignTitle4" : tradCms.title+" 4",
"blockCmsTextAlignTitle5" : tradCms.title+" 5",
"blockCmsTextAlignTitle6" : tradCms.title+" 6",

},
blockCmsColorLabel = {
  "blockCmsColorTitle1" : tradCms.title+" 1",
  "blockCmsColorTitle2" : tradCms.title+" 2",
  "blockCmsColorTitle3" : tradCms.title+" 3",
  "blockCmsColorTitle4" : tradCms.title+" 4",
  "blockCmsColorTitle5" : tradCms.title+" 5",
  "blockCmsColorTitle6" : tradCms.title+" 6",
},
blockCmsScrollAnimation = {
  "blockCmsAos" : "Animation",
  "blockCmsAosDuration" : "Duration",
},
checkboxSimpleParams={
  "onText" : trad.yes,
  "offText" : trad.no,
  "onLabel" : trad.yes,
  "offLabel" : trad.no,
  "labelText" : tradCms.label
}

var elTypeOptions = {
    "poi" : trad.poi,
    "events" : trad.events,
    "projects" : trad.project,
    "ressources" : trad.ressources,
    "classifieds" : trad.classifieds,
    "organizations" : trad.organization,
    "citoyens" :trad.members,
    "answers" : ucfirst(trad.answers),
    "forms" : ucfirst(trad.Form),
    "costum" : "costum"
};
var aosAnimation = {
  "fade-up": tradCms.fadeup,
  "fade-down": tradCms.fadedown,
  "fade-right": tradCms.faderight,
  "fade-left": tradCms.fadeleft,
  "fade-up-right": tradCms.fadeupright,
  "fade-up-left": tradCms.fadeupleft,
  "fade-down-right": tradCms.fadedownright,
  "fade-down-left": tradCms.fadedownleft,
  "flip-left": tradCms.flipleft,
  "flip-right": tradCms.flipright,
  "flip-up": tradCms.flipup,
  "flip-down": tradCms.flipdown,
  "zoom-in": tradCms.zoomin,
  "zoom-in-up": tradCms.zoominup,
  "zoom-in-down": tradCms.zoomindown,
  "zoom-in-left": tradCms.zoominleft,
  "zoom-in-right": tradCms.zoominright,
  "zoom-out": tradCms.zoomout,
  "zoom-out-up": tradCms.zoomoutup,
  "zoom-out-down": tradCms.zoomoutdown,
  "zoom-out-right": tradCms.zoomoutright,
  "zoom-out-left": tradCms.zoomoutleft
}
var bgSize = {
  "cover": tradCms.cover,
  "contain": tradCms.contain,
  "10%": "10%",
  "20%": "20%",
  "30%": "30%",
  "40%": "40%",
  "50%": "50%",
  "60%": "60%",
  "70%": "70%",
  "80%": "80%",
  "90%": "90%",
  "100%": "100%"
}
var bgPosition = {
  "left": tradCms.left,
  "left top": tradCms.lefttop,
  "left bottom": tradCms.leftbottom,
  "right": tradCms.right,
  "right top": tradCms.righttop,
  "right bottom": tradCms.rightbottom,
  "center": tradCms.center,
  "center top": tradCms.centertop,
  "center bottom": tradCms.centerbottom,
}

contentAlign = {
  "baseline": tradCms.baseline,
  "center": tradCms.center,
  "end": tradCms.end,
  "flex-end": tradCms.flexEnd,
  "flex-start": tradCms.flexStart,
  "inherit": tradCms.inherit,
  "initial": tradCms.initial,
  "normal": tradCms.normal,
  "revert" : tradCms.revert,
  "space-arround": tradCms.spaceArround,
  "space-between": tradCms.spaceBetween,
  "space-evenly": tradCms.spaceEvenly,
  "start" : tradCms.start,
  "stretch": tradCms.stretch,
  "unset": tradCms.unset
}

contentJustify = {
  "baseline": tradCms.baseline,
  "center": tradCms.center,
  "end": tradCms.end,
  "flex-end": tradCms.flexEnd,
  "flex-start": tradCms.flexStart,
  "inherit": tradCms.inherit,
  "initial": tradCms.initial,
  "normal": tradCms.normal,
  "revert" : tradCms.revert,
  "space-arround": tradCms.spaceArround,
  "space-between": tradCms.spaceBetween,
  "space-evenly": tradCms.spaceEvenly,
  "start" : tradCms.start,
  "stretch": tradCms.stretch,
  "unset": tradCms.unset
}

var bgRepeat = {
  "repeat": tradCms.repeat,
  "no-repeat": tradCms.norepeat
}

var flexDirection = {
  "column" : tradCms.column,
  "column-reverse" : tradCms.columnReverse,
  "inherit" : tradCms.inherit,
  "initial" : tradCms.initial,
  "row" : tradCms.row,
  "row-reverse" : tradCms.rowReverse ,
  "revert" : tradCms.revert,
  "unset" : tradCms.unset
}

var flexWrap  = {
  "wrap" : tradCms.wrap,
  "nowrap" : tradCms.nowrap,
  "wrap-reverse" : tradCms.wrapReverse ,
  "inherit" : tradCms.inherit,
  "initial" : tradCms.initial,
  "revert" : tradCms.revert,
  "unset" : tradCms.unset
}

var alignItems  = {
  "baseline": tradCms.baseline,
  "center": tradCms.center,
  "end": tradCms.end,
  "flex-end": tradCms.flexEnd,
  "flex-start": tradCms.flexStart,
  "inherit": tradCms.inherit,
  "initial": tradCms.initial,
  "normal": tradCms.normal,
  "revert" : tradCms.revert,
  "self-end": tradCms.selfEnd,
  "self-start": tradCms.selfStart,
  "start" : tradCms.start,
  "stretch": tradCms.stretch,
  "unset": tradCms.unset
}

var alignSelf  = {
  "baseline": tradCms.baseline,
  "center": tradCms.center,
  "end": tradCms.end,
  "flex-end": tradCms.flexEnd,
  "flex-start": tradCms.flexStart,
  "inherit": tradCms.inherit,
  "initial": tradCms.initial,
  "normal": tradCms.normal,
  "revert" : tradCms.revert,
  "self-end": tradCms.selfEnd,
  "self-start": tradCms.selfStart,
  "start" : tradCms.start,
  "stretch": tradCms.stretch,
  "unset": tradCms.unset,
}

var objFits = {
  "contain" : tradCms.contain,
  "cover" : tradCms.cover,
  "fill" : tradCms.fill,
  "initial" : tradCms.initial,
  "none" : tradCms.none, 
  "revert" : tradCms.revert,
  "inherit" : tradCms.inherit,
}

var borderTypes = {
  "dashed" : tradCms.dashed,
  "dotted" : tradCms.dotted,
  "double" : tradCms.double,
  "groove" : tradCms.groove, 
  "inset" : tradCms.inset,
  "ridge" : tradCms.ridge,
  "solid" : tradCms.solid,
  "outset" : tradCms.outset,
}

var poiOptions = {};
$.each(poi.filters,function(k,v){
  poiOptions[k] = typeof trad[k] != "undefined" ? trad[k] : k  ;
})

var fieldsetClass=[];
function lazyWelcomeDyFObj(time){
    if(typeof dyFObj != "undefined" && typeof dyFObj.openForm != "undefined")
       surchargeDyFObj();
    else
      setTimeout(function(){
        lazyWelcomeDyFObj(time+200)
      }, time);
}

/*surcharge openForm*/
function surchargeDyFObj(){
  dyFObj.openForm = function  (type, afterLoad,data, isSub, dynFormCostumIn,options) { 
      dyFObj.dynFormCostum = null; //was some times persistant between forms 
      mylog.warn("openForm","--------------- Open Form ",type, afterLoad,data, isSub, dynFormCostumIn);
      $.unblockUI();
      if(typeof options == "undefined" || (typeof options != "undefined" && options.notCloseOpenModal == "undefined"))
        $("#openModal").modal("hide");
      
      mylog.dir(data);
      uploadObj.contentKey="profil"; 
      if(notNull(data)){
        if(typeof data.images != "undefined")
          uploadObj.initList.image=data.images;
        if(typeof data.files != "undefined" )
          uploadObj.initList.file=data.files;

        data = dyFObj.prepData(data);

      }else{
        uploadObj.initList={};
      }
      dyFObj.activeElem = (isSub) ? "subElementObj" : "elementObj";
      dyFObj.activeModal = (isSub) ? "#openModal" : "#ajax-modal";

      if( typeof dynFormCostumIn != "undefined"){
        mylog.warn("openForm", "dynFormCostum",dynFormCostumIn);
        dyFObj.dynFormCostum = dynFormCostumIn;
      } else {
        mylog.warn("openForm", "no dynFormCostum");
      }

      if(notNull(finder))
        finder.initVar();

      if(notNull(scopeObj))
        scopeObj.selected = {};

      dyFInputs.locationObj.initVar();

      if(dyFObj.unloggedMode || userId)
      {
        if(typeof formInMap != 'undefined')
          formInMap.formType = type;

        if(typeof dyFObj.formInMap != 'undefined')
          dyFObj.formInMap.formType = type;

        dyFObj.getDynFormObj(type, function() { 
          dyFObj.startBuild(afterLoad,data,options);
        },afterLoad, data, dynFormCostumIn);
      } else {
        dyFObj.openFormAfterLogin = {
          type : type, 
          afterLoad : afterLoad,
          data : data
        };
        toastr.error(tradDynForm.mustbeconnectforcreateform);
        $('#modalLogin').modal("show");
      }

      if (typeof type.jsonSchema != "undefined" && type.jsonSchema.properties != "undefined" && isInterfaceAdmin) {
        //setTimeout(function(){
          createFieldsets(type.jsonSchema.properties);
          $(".fieldsetblockCmsPolice").before(`<div class="col-xs-12">
            <button type="button" class="btn  bg-green-k text-white more-config"><i class="fa fa-arrow-down"></i>&nbsp;`+tradCms.moreconfig+` <i class="fa fa-arrow-down"></i></button>
            <button type="button" class="btn  btn-block bg-green-k text-white less-config" style='display:none'><i class="fa fa-arrow-up"></i>&nbsp;`+tradCms.hide+` <i class="fa fa-arrow-up"></i></button>
            </div>`);
          $('.more-config').click(function(){
            $(this).hide();
            $('.less-config').show();
            $('.fieldset.toHide').css({
              "margin": "15px 0 15px 0",
              "padding":"15px 0px 15px 0",
              "visibility" :"visible",
              "height" : "auto", 
            });
          })
          $('.less-config').click(function(){
            $(this).hide();
            $('.more-config').show();
            $('.fieldset.toHide').css({
              "margin": "0px",
              "padding":"0px",
              "visibility" :"hidden",
              "height" : "0px",
            });
          })

          //toogle background choice
          $('#blockCmsBgType').click(function(){
            if($(this).val() == 'color'){
              $(".blockCmsBgColorcolorpicker").fadeIn("slow");
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
            if($(this).val() == 'image'){
              $(".blockCmsBgImguploader").css({
                  'display':'block',
                  'visibility':'visible',
                  'height' : 'auto'
              });
              $(".blockCmsBgColorcolorpicker,.blockCmsBgPaintselect").fadeOut("slow");
              $('.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect').fadeIn("slow");
            }
            if($(this).val() == 'paint'){
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeIn("slow");
              $(".blockCmsBgColorcolorpicker").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
          })

          
          $("select[id^='blockCmsPoliceTitle'],select[id*=' blockCmsPoliceTitle']").append(fontOptions);
          var fontAwesomeOptions = "";
          $.each(fontAwesome,function(k,v){
            fontAwesomeOptions += `<option value="${k}">${v}</option>`;
          })

          $('#blockCmsLineSeparatorIcon').append(fontAwesomeOptions);
          $(".fieldset.fieldsetmode").append("<div class='info tooltips' data-toggle='tooltip' data-placement='left' data-html='true' data-original-title='- "+tradCms.rowandhalf+"'><i class='fa fa-question-circle'></i></div>");
          $('.tooltips').tooltip();
          paperPaintObj.init();

        //},1000)   
      }
    }
}

function deleteBlockNotFound(id,type){
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var btnClick = $(this);
    var urlToSend = baseUrl+"/"+moduleId+"/cms/delete/id/"+id;

    bootbox.confirm(tradCms.confirmdeletblock+" ?",
    function(result)
    {
      if(result==true)
          ajaxPost(
            null,
            urlToSend,
            {},
            function(data){ 
                if (data.result ) {
                    toastr.success(tradCms.Blocksuccessfullydeleted);
                    $("#"+id).remove();
                    urlCtrl.loadByHash(location.hash);
                } else {
                     toastr.error(tradCms.erroroccurred);
                }
            }
          );
    });
}

function sortBlock(classItem){
    $(classItem).wrapAll('<div id="sortablee" class="row no-padding"></div>');
    if($('#sortablee').length){
        sortable("#sortablee", {
            handle : ".handleDrag",
            /*forcePlaceholderSize: true,*/
            placeholderClass: "ph-class",
            hoverClass: "hover"
        });
        sortable('#sortablee')[0].addEventListener('sortupdate', function(e) {
          var elements = e.detail.destination.items;
          var elementsObj = {};
          var params = {
              id : thisContextId,
              collection : thisContextType,
              idAndPosition : elementsObj
          }
          elements.forEach(function(v,k){
              elementsObj[k] = v.dataset.id
          })
          ajaxPost(
              null,
              baseUrl+"/costum/blockcms/dragblock",
              params,
              function(data){
                  if(data.result)
                      toastr.success("Bien déplacé");
                  else
                      toastr.success(trad.somethingwrong);
              }
           );
        });
        sortable('#sortablee')[0].addEventListener('sortstart', function(e) {
            var element =  e.detail.origin.container;
            var children = element.children;
            for(var i=0; i<children.length; i++){
                var child = children[i];
                console.log(child,"child");
                child.style.borderColor = "red";
                child.style.borderWidth = "1px";
                child.style.borderStyle = "dashed";
            }
        });
        sortable('#sortablee')[0].addEventListener('sortstop', function(e) {
            var element =  e.detail.origin.container;
            var children = element.children;
            for(var i=0; i<children.length; i++){
                var child = children[i];
                console.log(child,"child");
                child.style.borderColor = "red";
                child.style.borderWidth = "0px";
                child.style.borderStyle = "dashed";
            }
        })
    }
}

var paperPaintObj = {
  init : function(){
      this.append(function(){
        $("#blockCmsBgPaint").imagepicker({
          hide_select : false,
          show_label  : true
        });
      })
  },
  append : function(callback){
    $("#blockCmsBgPaint").addClass("show-html").append(`
    `);

    $('.more-config').click(function(){
        $(".blockCmsBgPaintselect").animate({
            scrollTop: ((exists($(".thumbnail.selected").offset()) && exists($(".thumbnail.selected").offset().top)) ? $(".thumbnail.selected").offset().top :null)
        }, 1000);
    })

    if(typeof callback == "function")
      callback();
  }
}

function getDateFormatedCms(params, onlyStr, allInfos){
  var dateObj = {};
  if(typeof params != 'undefined'){
    if( typeof params.recurrency != 'undefined' && params.recurrency){
      dateObj.initOpeningHours = initOpeningHoursCms(params, allInfos);
      return dateObj;
    }else{
      params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
      params.startDay = notEmpty(params.startDate) ? moment.unix(params.startDate.sec).format('DD') : '';
      params.startMonth = notEmpty(params.startDate) ? moment.unix(params.startDate.sec).format('MM') : '';
      params.startYear = notEmpty(params.startDate) ? moment.unix(params.startDate.sec).format('YYYY') : '';
      params.startDayNum = notEmpty(params.startDate) ? moment.unix(params.startDate.sec).format('ddd') : '';
      params.startTime = notEmpty(params.startDate) ? moment.unix(params.startDate.sec).format('HH:mm') : '';
      params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
      params.endDay = notEmpty(params.endDate) ? moment.unix(params.endDate.sec).format('DD') : '';
      params.endMonth = notEmpty(params.endDate) ? moment.unix(params.endDate.sec).format('MM') : '';
      params.endYear = notEmpty(params.startDate) ? moment.unix(params.endDate.sec).format('YYYY') : '';
      params.endDayNum = notEmpty(params.startDate) ? moment.unix(params.endDate.sec).format('ddd') : '';
      params.endTime = notEmpty(params.endDate) ? moment.unix(params.endDate.sec).format('HH:mm') : '';
      //params.startDayNum = directory.getWeekDayName(params.startDayNum);
      //params.endDayNum = directory.getWeekDayName(params.endDayNum);

      params.startMonth = directory.getMonthName(params.startMonth);
      params.endMonth = directory.getMonthName(params.endMonth);
      

      var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
      var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
      mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
       
        
      var str = '';
      var dStart = params.startDay + params.startMonth + params.startYear;
      var dEnd = params.endDay + params.endMonth + params.endYear;
      mylog.log('DATEE', dStart, dEnd);

      if(params.startDate != null){
        if(notNull(onlyStr)){
          if(params.endDate != null && dStart != dEnd)
          dateObj.fromdate = trad.fromdate;
          dateObj.startDay = params.startDay;

          if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
            dateObj.startMonth = params.startMonth;
          if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
            dateObj.startYear = params.startYear;

          if(params.endDate != null && dStart != dEnd){
            dateObj.endLbl = trad['todate'];
            dateObj.endDay = params.endDay;
            dateObj.endMonth = params.endMonth.substring(0,3);
            dateObj.endYear = params.endYear;
          }
            dateObj.startTime = params.startTime;
            dateObj.endTime = endTime;            
        }else{
          dateObj.startLbl = startLbl;
          dateObj.startDayNum = params.startDayNum;
          dateObj.startDay = params.startDay;
          dateObj.startMonth = params.startMonth.substring(0,3);
          dateObj.startYear = params.startYear;
          if(params.collection == "events")
            dateObj.startTime = params.startTime;
        }
      }    
        
      if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
            dateObj.endLbl = trad['todate'];
            dateObj.endDayNum = params.endDayNum;
            dateObj.endDay = params.endDay;
            dateObj.endMonth = params.endMonth;
            dateObj.endYear = params.endYear;
            if(params.collection == "events")
              dateObj.endTime = params.endTime;
      }   
      return dateObj
    }

  }
 
}

function updatePaletteColors(colors) {
  mylog.log('colors', colors);

  // Récupérer tous les enfants de #id-palette-color ayant la classe .draggabled-co
  const draggableDivs = document.querySelectorAll("#id-palette-color .draggabled-co");

  // Appliquer les couleurs générées à chaque div enfant
  draggableDivs.forEach((div, index) => {
    if (colors[index]) {
      div.style.backgroundColor = colors[index];
      div.setAttribute('data-color', colors[index]);
    }
  });
}

function initOpeningHoursCms (params, allInfos) {
    mylog.log('initOpeningHours', params, allInfos);
    var html = '<h5 class="text-center">';
    html += (notNull(allInfos)) ? '<span class=\' ttr-desc-4 bold uppercase" style="padding-left: 15px; padding-bottom: 10px;"\'><i class="fa fa-calendar"></i>&nbsp;'+trad.eachWeeks+'</span>' : '<span class=\' title-2 uppercase bold no-padding\'>'+trad.each+' </span>' ;
    mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
    if(notNull(params.openingHours) ){
      var count=0;

      var openHour = '';
      var closesHour = '';
      $.each(params.openingHours, function(i,data){
        mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
        mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
        if( (typeof data == 'object' && notNull(data) ) || (typeof data == 'string' && data.length > 0) ) {
          var day = '' ;
          var dayNum=(i==6) ? 0 : (i+1);
          if(notNull(allInfos)){
            mylog.log('initOpeningHours data.hours', data.hours);
            day = '<li class="tl-item">';
            day += '<div class="item-title">'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</div>';
            $.each(data.hours, function(i,hours){
              mylog.log('initOpeningHours hours', hours);
              day += '<div class="item-detail">'+hours.opens+' : '+hours.closes+'</div>';
            });
            day += '</li>';
            if( moment().format('dd') == data.dayOfWeek )
              html += day;
            else
              html += day;
          }else{
            if(count > 0) html+=', ';

          
            var color = '';
          
            if( typeof agenda != 'undefined' && agenda != null && 
            (moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1 ) == i &&
            typeof data.hours[i] != 'undefined' && 
            typeof data.hours[i].opens != 'undefined' && 
            typeof data.hours[i].closes != 'undefined' ) {

              openHour = data.hours[i].opens;
              closesHour = data.hours[i].closes;
            }
  

            html+= '<b style=\'font-variant: small-caps;font-size:x-large !important;\' class=\'title-3'+color+'\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b>';
          
          }
          count++;
        }
        
      });

      html +=  '&nbsp<small class=" margin-top-5"><b><i class="fa fa-clock-o"></i> '+openHour+'-'+closesHour+'</b></small>';
    } else 
      html = '<i>'+trad.notSpecified+'</i>'; 

    return html+'</h5>';
}

var createFieldsets = function(objProperties){ 
    alignInput2(objProperties,"margin",2,6,2,null,tradCms.Margininpx,"blue","hidden");
    alignInput2(objProperties,"padding",2,6,2,null,tradCms.Paddinginpx,"blue","hidden");
    alignInput2(objProperties,"mode",2,6,2,null,tradCms.screensize,"blue","hidden");
    alignInput2(objProperties,"blockCmsBg",8,12,2,2,tradCms.blockbackgroundcolororimage,"blue","");
    alignInput2(objProperties,"blockCmsPolice",2,6,null,null,tradCms.font,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextAlign",2,6,null,null,tradCms.textalignment,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextSize",2,6,null,null,tradCms.textsize,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextLineHeight",2,6,null,null,tradCms.interline,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsUnderline",2,3,null,null,tradCms.textunderline,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsColor",2,6,null,null,tradCms.textcolor,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsBorder",2,6,null,null,tradCms.sectionborder,"blue","hidden");
    alignInput2(objProperties,"blockCmsLineSeparator",2,6,null,null,tradCms.dividinglinebetweensections,"blue","hidden");
    alignInput2(objProperties,"blockCmsAos",6,6,null,null,"Animation","blue","hidden");
}

// ancient block with sp-text updatable
var ancientBlockWithSptextUpdatable = [
  "tpls.blockCms.affiche.affiche",
  "tpls.blockCms.annuaire.annuaireBadges",
  "tpls.blockCms.app.blocApp",
  "tpls.blockCms.article.actualiteTimeLine",
  "tpls.blockCms.community.communityEnPod",
  "tpls.blockCms.community.communityWithBadge",
  "tpls.blockCms.community.communityWithListAndDescription",
  "tpls.blockCms.community.e_community_caroussel",
  "tpls.blockCms.contact.contacterNous",
  "tpls.blockCms.contact.e_contact",
  "tpls.blockCms.contact.contactForm",
  "tpls.blockCms.crowdfunding",
  "tpls.blockCms.docs.showing_pdf_in_popup_modal",
  "tpls.blockCms.elements.standardElement",
  "tpls.blockCms.filters.directFilterEventNews",
  "tpls.blockCms.footer.footer_contact",
  "tpls.blockCms.footer.footer_with_link",
  "tpls.blockCms.footer.footerCocity",
  "tpls.blockCms.footer.footerWithCollabo",
  "tpls.blockCms.gallery.gallery",
  "tpls.blockCms.gallery.galleryCommunecter",
  "tpls.blockCms.gallery.galleryCommunecter2",
  "tpls.blockCms.graph.graphOfElements",
  "tpls.blockCms.graph.horizontalBar",
  "tpls.blockCms.graph.pourcentageMultiple",
  "tpls.blockCms.graph.progressBar",
  "tpls.blockCms.graph.progressCircle",
  "tpls.blockCms.graph.textWithValue",
  "tpls.blockCms.header.banner_With_2_btn",
  "tpls.blockCms.header.banner_with_logo",
  "tpls.blockCms.header.bannerWithSearch",
  "tpls.blockCms.header.bannerAvecTitre",
  "tpls.blockCms.header.header",
  "tpls.blockCms.map.communitymaps",
  "tpls.blockCms.map.filterByContinent",
  "tpls.blockCms.map.kaf_img_and_adress_map",
  "tpls.blockCms.projects.blockcarousel",
  "tpls.blockCms.ressources.ressourcesByCategories",
  "tpls.blockCms.social.teamwithdesc",
  "tpls.blockCms.tags.costumActifs",
  "tpls.blockCms.timeline.timeline",
  "tpls.blockCms.text.title_style"
]

var cmsEngine = {
    particlePage : function(page) {
        $("#page-Particle").remove()
        if (typeof costum.app != "undefined" && typeof costum.app["#"+page] != "undefined" && typeof costum.app["#"+page]["particle"] != "undefined" && typeof costum.app["#"+page]["particle"]["particleActive"] != "undefined" &&  costum.app["#"+page]["particle"]["particleActive"] ) {
            $(".main-container").prepend("<div id='page-Particle' style='position:fixed;width:100%;height:100vh;'></div>");
            var pointColor = null,
                lineColor = null;
            if (typeof costum.app["#"+page]["particle"]["pointColor"] != "undefined")
                pointColor = costum.app["#"+page]["particle"]["pointColor"];
            if (typeof costum.app["#"+page]["particle"]["lineColor"] != "undefined")
                lineColor = costum.app["#"+page]["particle"]["lineColor"]

                cmsEngine.callParticle("page-Particle",pointColor,lineColor);
        }
    },
    callParticle : function (divParticle, pointColor = null, lineColor = null) {

        $.getScript(modules.co2.url+"/js/particles.min.js", function(){
            particlesJS(divParticle,
                {
                    "particles": {
                        "number": {
                            "value": 50,
                            "density": {
                                "enable": true,
                                "value_area": 800
                            }
                        },
                        //couleur du point
                        "color": {
                            "value": (notNull(pointColor)) ? pointColor :"#49cbe3"
                        },
                        "shape": {
                            "type": "circle",
                            "stroke": {
                                "width": 0,
                                "color": "#36c3eb"
                            },
                            "polygon": {
                                "nb_sides": 5
                            },
                            "image": {
                                "width": 100,
                                "height": 100
                            }
                        },
                        "opacity": {
                            "value": 0.5,
                            "random": false,
                            "anim": {
                                "enable": false,
                                "speed": 1,
                                "opacity_min": 0.1,
                                "sync": false
                            }
                        },
                        "size": {
                            "value": 5,
                            "random": true,
                            "anim": {
                                "enable": false,
                                "speed": 40,
                                "size_min": 0.1,
                                "sync": false
                            }
                        },
                        //couleur de la ligne
                        "line_linked": {
                            "enable": true,
                            "distance": 150,
                            "color": (notNull(lineColor)) ? lineColor : "#b4bfc1",
                            "opacity": 0.4,
                            "width": 1
                        },
                        "move": {
                            "enable": true,
                            "speed": 4,
                            "direction": "none",
                            "random": false,
                            "straight": false,
                            "out_mode": "out",
                            "attract": {
                                "enable": false,
                                "rotateX": 600,
                                "rotateY": 1200
                            }
                        }
                    },
                    "interactivity": {
                        "detect_on": "canvas",
                        "events": {
                            "onhover": {
                                "enable": true,
                                "mode": "repulse"
                            },
                            "onclick": {
                                "enable": true,
                                "mode": "push"
                            },
                            "resize": true
                        },
                        "modes": {
                            "grab": {
                                "distance": 400,
                                "line_linked": {
                                    "opacity": 1
                                }
                            },
                            "bubble": {
                                "distance": 400,
                                "size": 40,
                                "duration": 2,
                                "opacity": 8,
                                "speed": 3
                            },
                            "repulse": {
                                "distance": 200
                            },
                            "push": {
                                "particles_nb": 4
                            },
                            "remove": {
                                "particles_nb": 2
                            }
                        }
                    },
                    "retina_detect": true,
                    "config_demo": {
                        "hide_card": false,
                        "background_color": "#b61924",
                        "background_image": "",
                        "background_position": "50% 50%",
                        "background_repeat": "no-repeat",
                        "background_size": "cover"
                    }
                }
            );

        });
    }
}

jQuery.fn.removeClassExcept = function (val) {
  return this.each(function (index, el) {
    var keep = val.split(" "),
    reAdd = [],
    $el = $(el);         
    for (var i = 0; i < keep.length; i++){
      if ($el.hasClass(keep[i])) reAdd.push(keep[i]);

    }          
    $el
    .removeClass()
    .addClass(reAdd.join(' '));
  });
};

/* Check link and convert into <a> tag for sp-text (in preview mode) */
// PLEASEEEE KEEP THIS FUNCTION FOR OUR sp-text!!!
function convAndCheckLink(elems, alias=false){
    if (costum.editMode == false || alias) {           
      var current_full_Url = window.location.href;
      $(elems).each(function(i, objelems) {
        var derText = $(objelems).html();
        $(objelems).fadeIn("show");
        if (derText !== 'undefined') {
          derText = derText.replace(/(?:\r\n|\r|\n)/g, '<br>');
          let elements = derText.match(/\[.*?\)/g);
          if( elements !== null && elements.length > 0){
            for(el of elements){
              if (el.match(/\[(.*?)\]/) !== null) {
                let eltxt = el.match(/\[(.*?)\]/);
                let elurl = el.match(/\((.*?)\)/);
                if (elurl !== null && eltxt !== null) {            
                  let txt = eltxt[1];
                  let spUrl = elurl[1];
                  if (spUrl.includes("#")) {                
                    let textBeforHtag = spUrl.substring(0, spUrl.indexOf("#"));
                    let textBefor_full_UrlHtag = current_full_Url.substring(0, current_full_Url.indexOf("#"));
                    let textAfterHtag = spUrl.split('#')[1]

                    let extLoadableUrls= (typeof costum!="undefined" && costum.app!="undefined") ? Object.keys(urlCtrl.loadableUrls).concat(Object.keys(costum.app)) : Object.keys(urlCtrl.loadableUrls);
                    if(textAfterHtag.includes("?preview=")){
                      if (textAfterHtag.includes("preview=poi")) {
                        textAfterHtag = "page.type.poi.id."+textAfterHtag.split('poi.')[1]
                      }else if (textAfterHtag.includes("preview=projects")) {
                        textAfterHtag = "page.type.projects.id."+textAfterHtag.split('projects.')[1]
                      }else if (textAfterHtag.includes("preview=organizations")) {
                        textAfterHtag = "page.type.organizations.id."+textAfterHtag.split('organizations.')[1]
                      }else if (textAfterHtag.includes("preview=citoyens")) {
                        textAfterHtag = "page.type.citoyens.id."+textAfterHtag.split('citoyens.')[1]
                      }else if (textAfterHtag.includes("preview=events")) {
                        textAfterHtag = "page.type.events.id."+textAfterHtag.split('events.')[1]
                      }else if (textAfterHtag.includes("preview=news")) {
                        textAfterHtag = "page.type.news.id."+textAfterHtag.split('news.')[1]
                      }else if (textAfterHtag.includes("preview=cities")) {
                        textAfterHtag = "page.type.cities.id."+textAfterHtag.split('cities.')[1]
                      }
                      textAfterHtag = textAfterHtag.replace("?preview=","")
                      derText = derText.replace(el,'<a class="lbh-preview-element super-href" href="#'+textAfterHtag+'">'+txt+'</a>');
                    }else if (textBeforHtag == textBefor_full_UrlHtag || textBeforHtag == "") {   
                      let hashToLoad = spUrl.slice(spUrl.lastIndexOf('#') + 1);
                      derText = derText.replace(el,`<a class="lbh super-href" href="#`+hashToLoad+`">`+txt+`</a>`)          
                    }else{
                      derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>');
                    }
                  }else{              
                    derText = derText.replace(el,'<a target="_blank" class="super-href" href="'+spUrl+'">'+txt+'</a>')
                  }
                }else{
                  derText = derText.replace(el,'<spcms style="color:red;background-color:whitesmoke;" data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t("cms", "Syntax error. Please try again")?>!">'+el+'</spcms>')

                }
              }
            }
          }
          $(objelems).html(derText);
          $(objelems).css("cursor", "inherit");
        }

      }); 
      coInterface.bindLBHLinks()
    }
}
function onClickOpenLink(typeUrl,link,target,downloadFile=false) {
  if (downloadFile) {
    var filename = link.substring(link.lastIndexOf('/') + 1);

    fetch(link)
    .then(response => response.blob())
    .then(blob => {
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = filename;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    });
  }else {
    if ( typeUrl === "externalLink"){
      if (target) { 
          window.open(link, "_blank");
      } else { 
          window.location.href = link;
      }
    } else if ( typeUrl === "internalLink" ) {
        if (target) {
            link = window.location.href.split('#')[0]+link;
            window.open(link, "_blank");
        } else {
            urlCtrl.loadByHash(link);
        }
    }
  }
}


function appendTextLangBased(dom,costumLanguage,text,id){
    var textToShow = "";

    if (text.hasOwnProperty(costumLanguage)) {
      textToShow = text[costumLanguage];
    } else {
      textToShow = text[Object.keys(text)[0]];
    }

    $(dom).html(textToShow);

    if (costum.editMode){
      if (typeof cmsConstructor.sp_params[id] != "undefined" && typeof cmsConstructor.sp_params[id].language != "undefined") 
        cmsConstructor.sp_params[id].language = costumLanguage;
    }
    $(dom).attr("data-lang", costumLanguage);
    return textToShow;
}

function refreshBlock(id, dom) {
  ajaxPost(
      null,
      baseUrl+"/co2/cms/refreshblock",
      {
          idBlock: id,
          clienturi : window.location.href
      },
      function(data) {
          if(data.result){
              $(`${dom}`).html(data["view"]);
          } else {
              toastr.error(tradCms.somethingWrong);
          }
      },
      null,
      "json"
  )
}

function replaceBlock(id, dom) {
  ajaxPost(
      null,
      baseUrl+"/co2/cms/refreshblock",
      {
          idBlock: id,
          clienturi: window.location.href,
      },
      function(data) {
          if(data.result){
              var prevElement = $(`${dom}`).prev();
              var nextElement = $(`${dom}`).nextAll('script:first');
              prevElement.remove();
              nextElement.remove();
              $(`${dom}`).replaceWith(data["view"]);
              cmsBuilder.block.initEvent();
              if (updateOrder)
                  cmsBuilder.block.updateOrder();
          } else {
              toastr.error(tradCms.somethingWrong);
          }
      },
      null,
      "json",
      {
          async: false
      }
  )
}