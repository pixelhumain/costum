var coSindniSmarterreObj = {
    init : function(coObj,formParent){

    },
    getQuatiers : function(){
        const q = {}
        $.each(aapObj.quartiers,function(k,v){
            if(v !=="Tous quartiers"){
                q[k] = v["name"];
                trad[k] = v["name"];
            }
        });
        return q;
    },
    getFonds : function(){
        const f = {}
        $.each(aapObj.fonds,function(k,v){
            f[v] = v;
            trad[v] = v;
        });
        return f;
    },
    communicationTools : {
        templates : {
            "avis_favorable_QPV" : {label : "Avis favorable QPV"},
            "avis_favorable_hors_QPV" : {label : "Avis favorable hors QPV"},
            "duplicate-action-coSindniSmarterre" : {label :"Dupliquer le dossier",event : "coSindniSmarterreObj.communicationTools.events.duplicateActionCoSindniSmarterre()"}
        },
        events : {
            duplicateActionCoSindniSmarterre : function(){
                duplYear = new Date().getFullYear();
                bootbox.prompt({
                    size: "small",
                    title: "Demande de duplication en quelle année ?",
                    required : true,
                    value : duplYear,
                    callback: function(result){
                        communicationToolsObj.payload.duplicateyear = result;
                        communicationToolsObj.sendMailOrPreview(communicationToolsObj,true,communicationToolsObj.choosedTemplate);
                    }
                });
                return duplYear;
            }
        }
    },
    getAssociations : function(){
        const a = {}
        $.each(aapObj.associations,function(k,v){
            a[v] = v;
            trad[v] = v;
        });
        return a;
    },
    getFinancors : function(coObj,formParent){
        var financors = {};
        ajaxPost('',baseUrl+'/co2/aap/getfinancor/id/'+formParent._id.$id,
        null,
        function(data){
            financors = data;
            var parse = {};
            $.each(financors,function(k,v){
                parse[k+"-idAndName-"+v] = v
            })
            financors = parse;
        },
        null,
        null,
        {async : false})
        return financors;
    },
    /*getFonds :function(coObj,formParent){
        var results = [];
        var params = {
            searchType : ["lists"],
            fields : ["name"],
            filters :{
                form : formParent["_id"]["$id"],
                type:"fonds"
            },
            notSourceKey : true
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                results = data.results;
                var allFonds = [];
                $.each(results,function(k,v){
                    allFonds.push(v.name);
                })
                results = allFonds;
            },null,null,{async:false}
        )
        return results;
    },
    getFonds2 :function(coObj,formParent){
        var results = [];
        var params = {
            searchType : ["answers"],
            fields : ["answers.aapStep1.depense.financer.line"],
            filters :{
                "answers.aapStep1.depense.financer.line" : {'$exists':true},
                "answers.aapStep1.titre" : {'$exists':true},
                form : formParent["_id"]["$id"],
            },
            notSourceKey : true
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                $.each(data.results,function(k,v){
                   $.each(v.answers.aapStep1.depense,function(kk,vv){
                        if(exists(vv.financer)){
                            $.each(vv.financer,function(kkk,vvv){
                                if(exists(vvv.line) && (vvv.line != "" || vvv.line != "" || vvv.line != null)){
                                    if(results.includes(vvv.line) === false){
                                        results.push(vvv.line);
                                    }
                                }
                            })
                        }
                    })
                })
            },null,null,{async:false}
        )
        mylog.log("datako",results)
        results = [...new Set(results)];
        results = results.filter(function (el) {
            return el != null && el != '';
        })
        return results;
    },*/
    /*getAssociation : function(coObj){
        var association = [];
        var params = {
            searchType : ["answers"],
            fields : ["answers.aapStep1.association"],
            filters :{
                "answers.aapStep1.association" : {'$exists':true},
                "answers.aapStep1.titre" : {'$exists':true},
                form : aapObj.form._id.$id,
            },
            notSourceKey : true
        };
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                let allAnswers = data.results;
                $.each(allAnswers,function(k,v){
                    if(notNull(v.answers) && notNull(v.answers.aapStep1) && notNull(v.answers.aapStep1.association) && !association.includes(v.answers.aapStep1.association))
                        association.push(v.answers.aapStep1.association);
                })
            },null,null,{async:false}
        )
        return association;
    },*/
    paramsFilters : function(coObj,formParent){
        var params = {
            admissibility : {
                view : "accordionList",
                type : "filters",
                field : "admissibility",
                name : "Admissible et Inadmissible",
                event : "filters",
                keyValue : false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : {
                    "admissible" : "Admissible",
                    "inadmissible" : "Inadmissible",
                }
            },
            text: {
                view: "text",
                field: "answers.aapStep1.titre",
                event: "text",
                placeholder: "Recherche par nom du projet",
                icon: "fa-search",
                customClass: "set-width visible-xs to-sticky to-set-value",
                inputAlias : ".filter-alias .main-search-bar-addon"
            },
            association : {
                view : "text",
                field : "answers.aapStep1.association",
                event : "text",
                placeholder : "Nom de l'association",
                icon: "fa-search",
                customClass: "set-width",
            },
            associations : {
                view : "megaMenuAccordion",
                type : "filters",
                field : "answers.aapStep1.association",
                remove0: true,
                countResults: true,
                activateCounter:true,
                countFieldPath: "answers.aapStep1.association",
                remove0: true,
                name : "Trier par Association",
                event : "filters",
                list : coObj.getAssociations(),
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                //classDom : "full-width",
                // classList : "col-xs-4",
            },
            principalDomaine : {
                view : "megaMenuAccordion",
                type : "filters",
                field : "answers.aapStep1.principalDomaine",
                remove0: true,
                countResults: true,
                activateCounter:true,
                countFieldPath: "answers.aapStep1.principalDomaine",
                remove0: false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                name : "Pilier",
                event : "filters",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["principalDomaine"]) && exists(formParent["params"]["principalDomaine"]["list"]) && notNull(formParent["params"]["principalDomaine"]["list"]) ? formParent["params"]["principalDomaine"]["list"] : []
            },
            interventionArea : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.interventionArea",
                name : "Quartier(s) d'intervention",
                event : "filters",
                keyValue : false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : coObj.getQuatiers(),
            },
            financors : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.depense.financer.idAndName",
                name : "Financeurs",
                event : "filters",
                keyValue : false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : coObj.getFinancors(coObj,aapObj.form),
            },
            fond : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.depense.financer.line",
                name : "Fonds",
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : coObj.getFonds(),
            },
            typologie : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.typologie",
                name : "Typologie du projet",
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["checkboxNewtypologie"]) && exists(formParent["params"]["checkboxNewtypologie"]["list"]) && notNull(formParent["params"]["checkboxNewtypologie"]["list"]) ? formParent["params"]["checkboxNewtypologie"]["list"] : []
            },
            year : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep1.year",
                name : trad.year,
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : generateIntArray(2020,new Date().getFullYear()).map(v => v = v.toString())
            },
            session : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.aapStep2lg3a7leik6jhs2chajq",
                name : "Session",
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]) && exists(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"]) && notNull(formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"]) ? formParent["params"]["radioNewaapStep2lg3a7leik6jhs2chajq"]["list"] : []
            },
            qpv : {
                view : "accordionList",
                type : "filters",
                field : "answers.aapStep2.qpv",
                name : "QPV ou Hors QPV",
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : exists(formParent) && exists(formParent["params"]) && exists(formParent["params"]["radioNewqpv"]) && exists(formParent["params"]["radioNewqpv"]["list"]) && notNull(formParent["params"]["radioNewqpv"]["list"]) ? formParent["params"]["radioNewqpv"]["list"] : []
            },
            seen    : {
                view : "accordionList",
                type : "filters",
                field: "views",
                name : trad.notSeen+"/"+trad.Seen,
                event: "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                keyValue: false,
                list : {
                    "notSeen": trad.notSeen,
                    "seen" : trad.Seen,
                }
            },
            favorite : {
                view : "accordionList",
                type : "filters",
                field : "vote",
                name : trad.Favorites,
                event : "filters",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                keyValue : false,
                list : {
                    [userId] : trad.Favorites,
                }
            },
            inproject : {
                view : "accordionList",
                type : "filters",
                field : "inproject",
                name : trad.projectstate+"/"+trad.proposal,
                event : "filters",
                keyValue : false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : {
                    inproject : trad.projectstate,
                    inproposal : trad.proposal
                }
            },
            status : {
                view : "accordionList",
                type : "filters",
                field : "status",
                name : trad.Status,
                event : "filters",
                keyValue : false,
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                list : aapObj.status
            },
            sortBy : {
                view : "accordionList",
                type : "sortBy",
                name : trad.sortby,
                event : "sortBy",
                classDom : "scrollable-row",
                classList : "label-text-to-ellipsis",
                isRadioButton : {
                    "answers.aapStep2.allVotes": [
                        "answers.aapStep1.titre",
                        "answers.aapStep2.allVotes",
                        "created",
                        "updated"
                    ],
                    "created" : [
                        "answers.aapStep1.titre",
                        "answers.aapStep2.allVotes",
                        "created",
                        "updated"
                    ],
                    "updated" : [
                        "answers.aapStep1.titre",
                        "answers.aapStep2.allVotes",
                        "created",
                        "updated"
                    ]
                },
                list : {
                    "Ordre alphabetique" : {
                        "answers.aapStep1.titre" : 1,
                    },
                    "Plus de votes" : {
                        "answers.aapStep2.allVotes" : -1,
                    },
                    "Moins de votes" : {
                        "answers.aapStep2.allVotes" : 1,
                    },
                    "Date de création croissant" : {
                        "created" : 1,
                    },
                    "Date de création décroissant" : {
                        "created" : -1,
                    },
                    "Date de mise à jour croissant" : {
                        "updated" : 1,
                    },
                    "Date de mise à jour décroissant" : {
                        "updated" : -1,
                    },
                }
            }
            /*missingData : {
                view : "dropdownList",
                type : "filters",
                field : "answers.aapStep1",
                name : "Données manquantes",
                typeList : "object",
                action : "filters",
                event : "exists",
                keyValue : false,
                list :  {
                    "titre":{
                        "label" : "Pas de titre",
                        "field" : "answers.aapStep1.titre",
                        "value" : false
                    },
                    email : {
						"label" : "Pas d\"email",
						"field" : "answers.aapStep1.email",
						"value" : false
					}
                }
            }*/
        }
        return params;
    },
    defaultFilters: function(coObj){
        var params = {
            notSourceKey : true,
            indexStep: "15",
            types:["answers"],
            forced:{
                filters:{
                }
            },
            filters:{
                'form' : aapObj.form["_id"]["$id"],
                'answers.aapStep1.titre' : {'$exists':true},
                'status':{'$not':"finish"},
                "answers.aapStep1.year" : aapObj.aapSession,
                'project.id' : {'$exists': "false"}
            },
            sortBy:{"updated":-1},
            fields: [],
            onResize : true,
            onResizeFunc : () => aapObj.common.switchFilter(),
            appendToXs : ".filterXs",
            appendToLg : "#filterContainerAapOceco",
            tagsPath:"answers.aapStep1.tags",
        }
        return params;
    },
    events : function(coObj,formParent){

    }
}


