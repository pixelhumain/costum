adminPanel.views.collectifLocaux = function(){
  var data={
    title : "Collectif locaux",
    types : ["organizations"],
    csv : [
      {
        url : baseUrl+'/costum/pacte/collectifsignedcsv',
        name : "Les collectifs actifs avec lien de modification",
        defaults : {
          indexStep : 0
        }
      },
      {
        url : baseUrl+'/api/organization/get/format/csv/key/siteDuPactePourLaTransition',
        name : "Tous les groupes",
        defaults : {
          indexStep : 0,
        }
      }
    ],
    table : {
      nameCollectif: {
        name : "Nom",
      },
      scope:{
        name : "Ville"
      },
      statusPacte : {
        name : "Statut"
      }
      //pdf : true,
    },
    actions:{
       validateSource : true,
       updateCollectif : true,
       delete : true
    },
    paramsFilter : {
        container : "#filterContainer",
        defaults : {
          types : [ "Group" ]
        },
        filters : {
          text : true,
          category : {
            view : "dropdownList",
            type : "filters",
            keyValue:false,
            field : "category",
            name : "Engagement du collectif",
            action : "filters",
            event : "filters",
            list :  {
              "soutien" : "Soutien pacte",
              "actif" : "Collectif actif",
              "actifSigned" : "Collectif engagé",
            }
          },
          status : {
            view : "dropdownList",
            type : "filters",
            field : "source.toBeValidated",
            name : "Statuts",
            typeList : "object",
            action : "filters",
            event : "exists",
            list :  {
              "toBeValidated":{
                  "label" : "En attente de validation",
                  "value" : true
                },
                "validated":{
                  "label": "Groupe validé",
                  "value":false
                }

            }
          }
        }
      }
      
  };
  ajaxPost('#content-view-admin', baseUrl + '/' + moduleId + '/admin/directory/', data, function () {
    $("a[data-id='1']").attr("href", baseUrl + '/api/organization/get/format/csv/key/siteDuPactePourLaTransition');
    $("a[data-id='1']").attr("target", '_blank');
  }, "html");

};
adminPanel.views.contract = function(){

  var data={
    title : "Contrat",
    types : ["poi"],
    table : {
      name: {
        name : "Nom",
        notLink : true
      },
      scope:{
        name : "Ville"
      },
      statusPacte : {
        name : "Statut"
      }
      //pdf : true,
    },
    actions:{
       validateSource : true,
       updateContract : true,
       delete : true
    },
    paramsFilter : {
        container : "#filterContainer",
        defaults : {
          types : [ "poi" ],
          type : "contract",
          filters : {
            type : "contract"
          },
          fields : {}

        },
        filters : {
          text : true,
          status : {
            view : "dropdownList",
            type : "filters",
            field : "source.toBeValidated",
            name : "Statuts",
            action : "filters",
            event : "exists",
            list :  {
              "1" : "En attente de validation"
            }
          }
        }
      }
      
  };
  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};
adminPanel.views.exportcontract = function(){
  window.open(baseUrl+'/api/poi/get/format/csv/key/siteDuPactePourLaTransition/type/contract/limit/0');
};
adminPanel.views.exportcandidat = function(){
  window.open(baseUrl+'/api/person/get/format/csv/key/siteDuPactePourLaTransition/tags/candidat2020/limit/0');
};
adminPanel.views.importcontrat = function(){
  ajaxPost('#content-view-admin', baseUrl+'/costum/pacte/importcontrat/', {}, function(){},"html");
};
adminPanel.views.importcollectif = function(){
  ajaxPost('#content-view-admin', baseUrl+'/costum/pacte/importcollectif/', {}, function(){},"html");
};
adminDirectory.bindCostum = function(aObj){
  mylog.log("validateSourceBtn");
    $(".validateSourceBtn").off().on("click", function(){
      $(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
      var $this=$(this);
      var params={
        id:$(this).data("id"),
        type:$(this).data("type")
      };
      ajaxPost(
        null,
        baseUrl+'/costum/pacte/validategroup',
        params,
        function(data){ 
              if ( data && data.result ) {
                successToast=($this.data("type")=="organizations") ? "Le collectif est bien validé": "Le contrat est validé";
                successMsg=($this.data("type")=="organizations") ? "Groupe validé" : "Contrat validé"; 
                toastr.success(successToast);
                $this.remove();
                $("#"+$this.data("type")+$this.data("id")+" .statusPacte").html("<span class='badge bg-green-k'><i class='fa fa-check'></i> "+successMsg+"</span>");
              } else {
                 toastr.error("Un problème est survenu lors de la validation");
              }
        }
      );
  });
};
adminDirectory.values.nameCollectif = function(e, id, type, aObj){
  var str="";
  if(typeof e.category != "undefined" && $.inArray(e.category, ["actif", "actifSigned"]) >= 0)
    str = '<a href="https://pacte-transition.org/#page.type.organizations.id.'+id+'.edit.'+id+'" target="_blank">'+e.name+'</a>';
  else  
    str = '<span>'+e.name+'</span>';
    return str;
};

adminDirectory.values.statusPacte = function(e, id, type, aObj){
  var str="";
  if(typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && e.source.toBeValidated)
    str+="<span class='badge bg-orange'><i class='fa fa-check'></i> En attente de validation</span>";
  else
    str+="<span class='badge bg-green-k'><i class='fa fa-check'></i> "+((type=="organizations") ? "Collectif" : "Contrat")+" validé</span>";
  return str;
};
adminDirectory.actions.validateSource = function(e, id, type, aObj){
  mylog.log("adminDirectory.actions statusAnswer", e, id, type, aObj);
  var str="";
  if(typeof e.source != "undefined" && typeof e.source.toBeValidated != "undefined" && e.source.toBeValidated){
    str = '<button data-id="'+id+'" data-type="'+type+'" class="margin-right-5 validateSourceBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider le '+((type=="organizations") ? "collectif" : "contrat")+'</button>';
  }
  return str;
};
adminDirectory.actions.updateCollectif = function(e, id, type, aObj){
  mylog.log("adminDirectory.actions updateCollectif", e, id, type, aObj);
  var str="";
  if(typeof e.category != "undefined" && $.inArray(e.category, ["actif", "actifSigned"]) >= 0)
    str = '<button onclick="dyFObj.editElement(\'organizations\', \''+id+'\',\'collectif\')" class="margin-right-5 btn text-green-k col-xs-12"><i class="fa fa-pencil"></i> Modifier</button>';
  return str;
};
adminDirectory.actions.updateContract = function(e, id, type, aObj){
  mylog.log("adminDirectory.actions statusAnswer", e, id, type, aObj);
  var str = '<button onclick="dyFObj.editElement(\'poi\', \''+id+'\',\'contract\')" class="margin-right-5 btn text-green-k col-xs-12"><i class="fa fa-pencil"></i> Modifier</button>';
  return str;
};


