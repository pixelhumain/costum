dyFObj.formInMap.forced.map = {
    center : ["16.15", "-61.66667"],
    zoom : 9
};
dyFObj.formInMap.forced.countryCode = "GP";
dyFObj.formInMap.forced.showMap = true;

directory.profils = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    str += '<div class="blog-item profil-item item-type1 col-xl-3 col-md-4 col-sm-6">'+
            '<div class="light">'+
                '<div class="profile-userpic">'+
                    '<img src="'+(params.profilMediumImageUrl||defaultImage)+'" class="img-responsive" alt=""> </div>'+
                '<div class="profile-usertitle">'+
                    '<div class="profile-usertitle-name"> '+(params.name?`<h4 class="title">${params.name}</h4>`:"")+' </div>'+
                    '<div class="profile-usertitle-job"> '+((params.id && costum.communityLinks && costum.communityLinks[params.id])?costum.communityLinks[params.id]:"")+' </div>'+
                '</div>'+
                '<div class="profile-userbuttons">'+
                    '<button type="button" class="btn btn-info  btn-sm">Follow</button>'+
                    '<button type="button" class="btn btn-info  btn-sm">Message</button>'+
                '</div>'+
            '</div>'+
        '</div>';
    return str;
}

directory.cards = function(params){
    if(params._id.$id!==costum.contextId){
        mylog.log("directory actus", "Params", params);
        var pendingAdminClass = (notEmpty(params.links) && notEmpty(params.links.members) && notEmpty(params.links.members[userId]) && notEmpty(params.links.members[userId]["isAdminPending"]) && params.links.members[userId]["isAdminPending"]) ? " isAdminPending" : "";
        var str = "";
        var dateAndTags = "";
        if(params.collection=="news"){
            dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
        }
        if(params.collection=="events" && params.startDate){
            dateAndTags = '<span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY HH:mm") +'</span>';
        }
        let img=(params.profilMediumImageUrl) ? '<div class="image"><img src='+params.profilMediumImageUrl+' class="img-responsive" alt="" decoding="async"></div>' : params.imageProfilHtml; 
        let linkedElement = "";
        if(params.collection!="organizations" && (params.parent || params.organizer)){
            let p = params.organizer || params.parent;
            linkedElement = '<div style="position:absolute;  max-width:45%;font-weight:bold; top:35px; left: 30px;">'
            $.each(p, function(oi, ov){
                linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5 margin-bottom-5" style="background-color:var(--primary-color); color:white; display:inline-block; border-radius:4px">'+
                        ov.name+
                    '</a><br/>';
            }),
            linkedElement+="</div>"
        }
        let inValidation="";
        if(typeof params.preferences!="undefined" && typeof params.preferences.toBeValidated!="undefined" && typeof params.preferences.toBeValidated[costum.slug]!="undefined" && params.preferences.toBeValidated[costum.slug]){
            inValidation="<div class='padding-20' style='position:absolute;background:var(--secondary-color);z-index:1;'><span class='text-white'>En cours de validation</br>par les administrateurs</span></div>"

        }

        str += '<div class="blog-item actus-item item-type1 col-md-4 col-sm-6 col-xs-12">'+
            inValidation+
            '<div class="item-inner '+pendingAdminClass+'" style="position:relative">'+
            img+
            linkedElement+
            ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
            ((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(tradCategory[params.type]||"")+'">'+(tradCategory[params.type]||"")+'</span>':"")+
            '<div class="item-body">'+
            '<div class="top-wrapper">'+
            '<span class="cat-date">'+dateAndTags+'</span>'+
            '<span class="category lowercase">#'+(((params.tags&&params.tags[0])?params.tags[0]:"")||"")+'</span>'+
            '</span>'+
            //'<span class="tag"></span>'+
            '</div>'+
            (params.name?`<h4 class="title">${params.name}</h4>`:"")+
            '<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
            '<div class="link-bordered">'+
            '<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>';
        return str;
    }    
}

directory.answers = function(params){
    mylog.log("directory actus", "Params", params);
    let str = "";
    let desc = jsonHelper.getValueByPath(params, "answers.ekitia25102023_918_0.ekitia25102023_918_0lo5d934pla097xvfvnk")||jsonHelper.getValueByPath(params, "answers.ekitia25102023_954_0.ekitia25102023_954_0lo5eiwo956j2a3f57u4")||jsonHelper.getValueByPath(params, "answers.ekitia25102023_942_0.ekitia25102023_942_0lo5e3czhfhla9u6o74m");
    if(!desc){
        desc="<i>Description non renseigné(s)</i>"
    }

    let linkedElement = "";
    if(params.form && typeof jsonHelper.getValueByPath(params, "links.organizations")!="undefined"){
        $.each(jsonHelper.getValueByPath(params, "links.organizations"), function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5" style="position:absolute; background-color:#ddd; color:var(--primary-color); font-weight:bold; top:35px; left: 30px; border-radius:4px">'+
    	            '<span class="ico-circle"><img width="40" height="30" src="'+baseUrl+'/upload/communecter/'+ov.type+'/'+oi+'/thumb/profil-resized.png"/></span> '+ov.name+
                '</a>';
        })
    }
    str += '<div class="blog-item item-type1 col-xl-3 col-md-4 col-sm-6">'+
        '<div class="item-inner">'+
            '<div class="image">'+
                '<img width="450" height="222" src="'+(params.profilMediumImageUrl||(modules.co2.url+"/images/logo-coform.png"))+'" class="img-responsive" alt="" decoding="async">'+
            '</div>'+
            linkedElement+
            '<div class="item-body">'+
                '<div class="top-wrapper">'+
                    '<span class="cat-date">'+
                        '<span class="date">'+(params.created?formatDate(params.source.date):formatDate(params.created))+' | </span>'+
                        '<span class="category">'+costum.lists.numericServiceForms[params.form].label+'</span>'+
                    '</span>'+
                    '<span class="tag"></span>'+
                '</div>'+
                '<div class="desc">'+desc.substring(0, 60)+'...</div>'+
                '<div class="link-bordered margin-top-5">'+
                    '<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '</div>';
    return str;
}

/*
directory.toolSimple = function(params){
    mylog.log("directory.tableOfContent", params);
    let str = "";
    let i = Object.keys(costum.lists.principes).indexOf(params.ethicCharte);
    let subIndex = Object.keys(costum.lists.principes[params.ethicCharte]).indexOf(params.name)
    let order = 0;
    if(i!=-1 && typeof costum[costum.slug].listedPrincipe[params.ethicCharte]=="undefined"){
        order = (i+1)*100
        costum[costum.slug].listedPrincipe[params.ethicCharte] = params.ethicCharte;
            str = '<div id="principe'+(i+1)+'" class="col-xs-12 padding-left-35" style="order:'+order+'">'+
            '<div class="margin-top-5 margin-left-35">'+
                '<h3 class="margin-bottom-10 margin-left-35">PRINCIPE <span class="h1" style="color:var(--primary-color)">'+(i+1)+ "</span> : "+params.ethicCharte+'</h3>'+
            '</div>'+
            '</div>';
    } 

    str += '<div class="col-xs-12  padding-left-35"  style="order:'+(order+subIndex+1)+'">'+
            '<div class="margin-left-35">'+
                '<a href="'+params.hash+'" class="lbh-preview-element margin-left-35"><h4 class="padding-left-35" style="color:var(--primary-color)">'+(1+i)+"."+(subIndex+1)+". " +params.name+'</h4></a>'+
            '</div>'+
        '</div>';
    return str;
}

directory.toolCard = function(params){
    mylog.log("directory tool card", "Params", params);
    var str = "";
    var dateAndTags = "";
    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    let linkedElement = "";
    if(params.parent || params.organizer){
        let p = params.organizer || params.parent;
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5" style="position:absolute; background-color:#ddd; color:var(--primary-color); font-weight:bold; top:35px; left: 30px; border-radius:4px" title="'+ov.name+'">'+
    	            '<span class="ico-circle"><img width="40" height="30" src="'+baseUrl+'/upload/communecter/'+ov.type+'/'+oi+'/thumb/profil-resized.png"/></span> '+
                '</a>';
        })
    }
    str += '<div class="blog-item actus-item item-type1 col-xl-3 col-md-4 col-sm-6">'+
                '<div class="item-inner">'+
                    linkedElement+
                    '<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+((params.toolLevel)||"")+'</span>'+
                    '<div class="item-body margin-top-20">'+
                        '<a href="'+params.hash+'" class="lbh-preview-element">'+(params.name?`<h4 class="title">${params.name}</h4>`:"")+'</a>'+
                        '<div class="category">'+(((params.toolType)?params.toolType:"")||"")+'</div>'+
                        '<div class="category">'+(((params.tags)?params.tags:"")||"")+'</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
    return str;
}*/

function addData(_context, params) {
    if (params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude) {
        if (!params.opt)
            params.opt = {}

        params.opt.icon = _context.getOptions().mapCustom.icon.getIcon(params)

        var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
        _context.setDistanceToMax(latLon);

        var marker = L.marker(latLon, params.opt);
        _context.getOptions().markerList[params.elt.id] = marker;

        if (_context.getOptions().activePopUp) {
            _context.addPopUp(marker, params.elt)
        }

        _context.getOptions().arrayBounds.push(latLon)

        if (_context.getOptions().activeCluster) {
            _context.getOptions().markersCluster.addLayer(marker)
        } else {
            marker.addTo(_context.getMap());
            if (params.center) {
                _context.getMap().panTo(latLon)
            }
        }

        if (_context.getOptions().mapOpt.doubleClick) {
            marker.on('click', function (e) {
                this.openPopup()
                coInterface.bindLBHLinks();
            })
            marker.on('dbclick', function (e) {
                _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                coInterface.bindLBHLinks();
            })
        } else {
            marker.on('click', function (e) {
                if (_context.getOptions().mapOpt.onclickMarker) {
                    _context.getOptions().mapOpt.onclickMarker(params)
                } else {
                    _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                    coInterface.bindLBHLinks();
                }
            });
        }
    }
}


paramsMapCO = $.extend(true, {}, paramsMapCO, {
    // activeCluster:false,
    mapCustom: {
        icon: {
            getIcon: function (params) {
                var elt = params.elt;
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof elt.profilMediumImageUrl !== "undefined" && elt.profilMediumImageUrl != "")
                    imgProfilPath = baseUrl + elt.profilMediumImageUrl;
                var myIcon = L.divIcon({
                    className: "ekitia-div-marker",
                    iconAnchor: [35, 70],
                    iconSize: [70, 70],
                    labelAnchor: [-70, 0],
                    popupAnchor: [0, -70],
                    html: `<div style='background-color:var(--primary-color);' class='marker-ekitia'></div><img src="${imgProfilPath}">`
                });
                return myIcon;
            }
        },
        getClusterIcon: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            if (childCount < 100) {
                c += 'small';
            } else if (childCount < 1000) {
                c += 'medium';
            } else {
                c += 'large';
            }
            return L.divIcon({
                html: '<div>' + childCount + '</div>',
                className: 'marker-cluster' + c,
                iconSize: new L.Point(40, 40)
            });
        },
        addMarker: function (_context, params) {
            addData(_context, params);
            if(typeof params.elt.addresses != "undefined" && params.elt.addresses != null){
                params.elt.addresses.forEach((element) => {
                    var data = $.extend({}, params, true);
                    data.elt.address = element.address;
                    data.elt.geo = element.geo;
                    data.elt.geoPosition = element.geoPosition;

                    addData(_context, data);
                })
            }
        }
    }
});

eventTypes = {
    "workshop": "Workshop",
    "competition" : "Competition",
    "concert" : "Concert",
    "contest" : "Contest",
    "conference": "Conference",
    "debate" : "Debate",
    "exhibition" : "Exhibition",
    "festival" : "Festival",
    "crowdfunding" : "Crowdfunding",
    "course": "Course",
    "fresk" : "Fresk",
    "protest" : "Protest",
    "market" : "Market",
    "openHouse" : "Open house",
    "filmProjection" : "Film projection",
    "getTogether" : "Get together",
    "meeting": "Meeting",
    "spectacle" : "Spectacle",
    "internship" : "Internship"
}

var lists = {
    "activity" : [
        "Administrations & Collectivités",
        "Aéro/Spacial",
        "Agriculture & Agronomie",
        "Banque/Assurance",
        "Bâtiment/BIM",
        "Communication",
        "Conseil",
        "Données",
        "Economie",
        "Emploi/Formation",
        "Energie",
        "Enseignement",
        "Environnement",
        "Géographie",
        "Intelligence Artificielle",
        "Juridique",
        "Logiciels",
        "Mobilité",
        "Recherche Scientifique",
        "Santé",
        "Télécom & Hébergement",
        "Tourisme",
        "Transformation Numérique",
        "Autres"
    ],
    "structure": {
        "Administration ou institution publique":"Administration ou institution publique",
        "Collectivité territoriale":"Collectivité territoriale",
        "Entreprise": "Entreprise",
        "Etablissement d'enseignement":"Etablissement d'enseignement",
        "Etablissement de recherche et santé":"Etablissement de recherche et santé",
        "Etablissement public ou chargé de missions de services publics":"Etablissement public ou chargé de missions de services publics",
        "Structures Publiques" : "Structures Publiques",
        "Pôles, Clusters ou Associations" : "Pôles, Clusters ou Associations",
        "Recherche" : "Recherche",
        "Académique": "Académique"
    }
}

costum.loadByHash = function(hash){
    mylog.log("costum.loadByHash",hash);
    var soonHash=["#je-minforme","#je-fais-valoir-mes-interets","#projets-de-territoire"];
    if(soonHash.indexOf(hash)>-1){
        urlCtrl.afterLoad=function(){
            // alert("aferLoad");
            $(".alertSoon").off().on("click",function(){
                bootbox.alert("Bientôt disponible, restez informé.e !");
            });
        };
    }else if(hash=="#page.type."+costum.contextType+".id."+costum.contextId+".view.directory.dir.members" && costum.isCostumAdmin){
        // urlCtrl.afterLoad=function(){
            // alert("aferLoad");
            // urlCtrl.loadByHash("#admin.view.group");
            return "#admin.view.group";
        // };
       

    }else if((hash=="#page.type."+costum.contextType+".id."+costum.contextId+".view.newspaper" || (userId!=="" && hash=="#page.type."+costum.contextType+".id."+costum.contextId)) && !costum.isCostumAdmin){
        // urlCtrl.afterLoad=function(){
            // alert("aferLoad");
            // urlCtrl.loadByHash("#admin.view.group");
            return "#actus";
        // };
    }else if(hash=="#page.type."+costum.contextType+".id."+costum.contextId+".view.agenda" && !costum.isCostumAdmin){
        // urlCtrl.afterLoad=function(){
            // alert("aferLoad");
            // urlCtrl.loadByHash("#admin.view.group");
            return "#events";
        // };
    }else if(hash=="#page.type."+costum.contextType+".id."+costum.contextId+".view.directory.dir.poi" && !costum.isCostumAdmin){
        // urlCtrl.afterLoad=function(){
            // alert("aferLoad");
            // urlCtrl.loadByHash("#admin.view.group");
            return "#boites-a-outils";
        // };
    }else if(hash=="#customRegister"){
        if(userId!==""){
            hash=costum.htmlConstruct.redirect.logged;
            // urlCtrl.loadByHash(costum.htmlConstruct.redirect.logged);
        }else{
            hash=costum.htmlConstruct.redirect.unlogged;
            // urlCtrl.loadByHash(costum.htmlConstruct.redirect.unlogged);
            urlCtrl.afterLoad=function(){
                $('#modalRegister').modal("show");
            }
            

        }
    }

    
    // $(".banner-outer").remove();
    return hash;
}

ajaxPost(null,baseUrl+"/survey/form/get/slug/"+costum.slug+"/tpl/json",null,function(data){
    costum.lists["forms"]={};
    $.each(data.forms, function(id,form){
        costum.lists.forms[id]={
            label : form.name,
            icon:"fa-wpforms",
            value: id
        };


    });
});
costum.lists["numericServiceForms"] = {
    "65389e3790b2f58f3503d8e2":{
        label: "Catalogue de données",
        icon:"fa-wpforms",
        value: "65389e3790b2f58f3503d8e2"
    },
    "6538adfc90b2f58f3503d8e3":{
        label: "Prestations de conseil et de services, expertises",
        icon:"fa-wpforms",
        value: "6538adfc90b2f58f3503d8e3"
    },
    "6538ae1a8312ccf36c042c94":{
        label: "Solutions logicielles, solutions numériques",
        icon:"fa-wpforms",
        value: "6538ae1a8312ccf36c042c94"
    },
    "6538aed38312ccf36c042c95":{
        label: "Formations",
        icon:"fa-wpforms",
        value: "6538aed38312ccf36c042c95"
    },
    "659ceff39c9ae3090c6b3dce":{
        label: "Algorithmes, modèles, réseaux de neurones, librairies",
        icon:"fa-wpforms",
        value: "659ceff39c9ae3090c6b3dce"
    },
    "65b21195ac117c27326a1858":{
        label: "Capacité de stockage ou de calcul",
        icon:"fa-wpforms",
        value: "65b21195ac117c27326a1858"
    }
}

costum.lists["charteEthique"] = {
    "Sciences des données et société" : {
        "Bienfaisance" : {
            description: "Le principe de bienfaisance et son corollaire, le principe de ne pas nuire, requièrent des Signataires qu’ils prennent en considération un objectif de bien-être collectif de l’humanité et de durabilité dans la conduite de leurs activités. Concrètement, le fait d’œuvrer pour le bien-être collectif se traduit par la conduite d’activités visant à améliorer le quotidien des générations actuelles et la qualité de vie du plus grand nombre. De manière complémentaire, le fait d’œuvrer dans une logique de durabilité se traduit par la prise en considération du bien commun des générations futures. Ainsi, les usages des Données mis en œuvre par les Signataires doivent contribuer autant que possible à la réalisation des Objectifs de Développement Durable adoptés par les 193 États membres de l’ONU. En outre, les Signataires s’assurent que les innovations qu’ils développent grâce aux Données apportent un réel progrès par rapport à des dispositifs existants ou des méthodes alternatives (y compris non numériques). De plus, les Signataires s’engagent à évaluer les retombées de leurs projets au regard des finalités de ceux-ci (cf. principe 5.3). Enfin, les Signataires favorisent la mise à disposition de Données pour des finalités d’intérêt général, dans les conditions prévues par la présente Charte et dans le respect des droits et libertés fondamentaux exercés dans une société démocratique."
        },
        "Innovation soutenable" : {
            description: "La conduite bienfaisante des activités des Signataires les mène à développer des innovations soutenables. Ainsi, tout projet de rupture technologique, sociale ou organisationnelle réalisé en tout ou partie grâce aux Données est mis en œuvre dans des conditions respectueuses de l’humain et de l’environnement. Dans cette optique, les Signataires portent une attention particulière à leur empreinte écologique en vue de la réduire. Pour ce faire, ils inscrivent leurs usages des Données dans une démarche de sobriété et de frugalité. La sobriété implique par exemple de limiter les traitements de Données aux finalités essentielles d’un projet, de privilégier des centres de données qui mettent en œuvre des mesures pour compenser leur impact environnemental, et d’être attentifs à l’effet rebond  des innovations. Quant à la frugalité, elle vise à élaborer des solutions efficientes, dépourvues de sophistication et de superflu, avec le moins de moyens possible mais sans faire de concession sur la qualité du service rendu . Dans la mesure du possible les Signataires évaluent l’impact environnemental, en particulier énergétique, de leurs projets dès la conception et tout au long du cycle de vie de ceux-ci (cf. principe 5.3). Enfin, afin de compenser leur éventuel impact négatif sur l’environnement, ils favorisent des projets à impact positif direct sur l’environnement."
        },
        "Solidarité, diversité et non-discrimination":{
            description: "Les Signataires veillent à ce que les projets qu’ils réalisent grâce aux Données n’aient pas pour conséquence de créer ou de renforcer des inégalités sociales, et sont attentifs à la prise en compte de la diversité de la société. Conformément au principe de non-discrimination, ils veillent également à ce que ces projets n’aient pas pour objet ou pour effet de créer, directement ou indirectement, une discrimination à l’encontre d’un individu ou d’un groupe d’individus, ou une forme de stigmatisation. A cette fin, ils sont vigilants quant aux biais discriminatoires susceptibles d’affecter les Données (cf. principe 3.1) et, le cas échéant, ceux susceptibles d’affecter les algorithmes utilisés pour traiter les Données. A l’aide d’une approche pluridisciplinaire, les Signataires élaboreront progressivement des stratégies visant à surmonter cette problématique de manière durable. Ils sont également vigilants quant aux questions de fracture numérique : ils évitent de creuser les différences de niveau d’équipement et de littératie numérique au sein de la population, et font en sorte que les dispositifs politiques et sociaux les plus importants soient accessibles quel que soit le degré d’accès aux outils numériques."
        },
        "Facteur humain" : {
            description: "Les Signataires tiennent compte du fait que tout projet s’intègre dans un système humanisé, composé d’agents compétents, au sein duquel la technologie n’est qu’un support de l’innovation. Dès la conception, leurs projets doivent être organisés de manière aussi pluridisciplinaire que possible, c’est-à-dire en mobilisant toutes les compétences nécessaires à l’exploitation des Données et à l’analyse des enjeux y étant liés, au niveau humain comme au niveau technologique. Les Signataires impliqués dans les projets mis en œuvre grâce aux Données s’inscrivent dans une logique de responsabilisation et de responsabilité éthique et juridique, de contrôle humain  de l’innovation et de garantie que toute prise de décision fondée sur l’utilisation de la technologie est opérée par des personnes humaines maîtrisant les outils technologiques et leurs risques. En ce sens, ils réfléchissent en amont du projet, à la répartition de leurs responsabilités au regard de leur implication dans le cycle de vie de celui-ci. Par ailleurs, les innovations qu’ils développent grâce aux Données n’annihilent pas la possibilité d’interagir avec un humain compétent."
        }
    },
    "Sciences des données et individu" : {
        "Respect et renforcement de l’autonomie humaine":{
            description: "Tout d’abord, les Signataires considèrent le respect de l’autonomie humaine comme l’élément central de leurs activités de traitement de données personnelles. En ce sens, lorsque la licéité d’un traitement de données personnelles dépend du recueil du consentement des personnes concernées, ils mettent en œuvre les meilleures pratiques pour leur permettre d’exprimer leurs choix de manière éclairée, spécifique et univoque. Cela inclut une information compréhensible pour des personnes non expertes. De plus, ils portent une attention particulière aux modalités du recueil du consentement des personnes vulnérables, notamment les mineurs, les personnes âgées et les personnes dépendantes.  En toute hypothèse, dès la collecte de données à caractère personnel et quelle qu’en soit la base légale, les responsables de traitement facilitent l’exercice par les personnes concernées des droits dont elles disposent sur leurs données. Ceci concerne notamment le droit à l’information (primordial du fait qu’il conditionne la possibilité d’exercer les autres), le droit de s’opposer au traitement de leurs données, le droit à l’effacement de leurs données, le droit d’accéder à leurs données et le droit à la portabilité de leurs données. Ce faisant, ils prennent en considération les particularités liées à la diversité des utilisateurs, à leurs contraintes et à leurs capacités, ainsi que leurs avis. Plus généralement, les Signataires veillent à préserver le libre-arbitre humain en ne mettant pas en œuvre des stratégies consistant à influencer le comportement ou les émotions des individus par des suggestions cachées, par la seule utilisation de leurs biais cognitifs (stratégies dites « de nudge » ). En outre, ils sont particulièrement attentifs à ne pas développer, grâce aux Données, des innovations susceptibles d’être utilisées à des fins de désinformation ou de manipulation."
        },
        "Respect de la vie privée" : {
            description: "Les Signataires se conforment aux règles applicables en matière de protection de la vie privée et des données personnelles. De manière complémentaire, la présente Charte prévoit dans ses différents principes des garanties nécessaires pour assurer le respect des droits des personnes en la matière. Les Signataires sont particulièrement attentifs à ce que la protection de la vie privée des individus soit garantie tout au long du « cycle de vie » des Données, notamment en prenant soin d’appliquer scrupuleusement les principes de minimisation, de protection par défaut et par conception, et de définir pour chaque traitement de données personnelles une durée de conservation adéquate. Les Signataires sont conscients que le croisement de Données (même anonymisées) augmente considérablement le risque de réidentification des individus à l’issue de leur traitement. En ce sens ils s’engagent à déterminer et à appliquer, au cas par cas et de manière proportionnée à la sensibilité des Données concernées, les techniques qu’ils estiment être les plus adéquates pour optimiser la protection de la vie privée"
        }
    },
    "Qualité des données et sécurité du système d’information": {
        "Sécurité de l’hébergement et des traitements de données":{
            description: "Afin de protéger les Données d’attaques physiques ou virtuelles susceptibles de compromettre leur disponibilité, leur intégrité et leur confidentialité, les Signataires privilégient des centres de données situés sur le territoire de l’UE et respectueux de normes de sécurité adaptées au regard de la nature des Données et des usages envisagés. Ils prennent notamment les mesures techniques, juridiques et organisationnelles nécessaires afin de se conformer aux règles européennes relatives à l’accès et au transfert international de Données détenues dans l’UE. Ils prennent soin d’appliquer des mesures de cybersécurité adaptées à la confidentialité des Données traitées au sein de leur infrastructure. Le recours à des prestataires tiers ne doit pas diminuer la capacité de gestion confidentielle des Données.            Leur vigilance est accrue dès lors qu’ils sont en présence de données à caractère personnel sensibles , de données liées à la sécurité publique nationale ou de données protégées par des exigences de confidentialité (telles que des données commerciales, des données couvertes par le secret statistique, des données protégées par des droits de propriété intellectuelle)."
        },
        "Robustesse des algorithmes" : {
            description: "Les Signataires qui envisagent de traiter les Données à l’aide d’un système algorithmique choisissent un modèle suffisamment robuste au regard de la finalité du traitement des Données. La robustesse d’un algorithme dépend du caractère fiable  et reproductible  des résultats qu’il permet d’obtenir, ces caractères étant influencés par la qualité du modèle algorithmique ainsi que par la qualité des Données d’entraînement (cf. principe 3.1). Concernant spécifiquement les modèles qui continuent à évoluer suite à leur entraînement grâce à un apprentissage en continu, leur robustesse dépend également de la qualité de toutes les Données qui y sont entrées et devrait donc faire l’objet d’une surveillance tout au long de son cycle de vie. En toute hypothèse, les Signataires s’efforcent de parvenir à une marge d’erreur proportionnée à la finalité d'utilisation du système. Lorsqu’un système algorithmique a vocation à être utilisé dans le contexte d’un processus décisionnel, les Signataires garantissent que la prise de décision sera in fine assurée par un opérateur humain informé des capacités et des limites du système (cf. principe 1.4)."
        }
    },
    "Transparence": {
        "Information claire et accessible": {
            description: "De manière générale, les Signataires s’efforcent d’apporter, par les moyens qu’ils estiment appropriés, une information éclairée et accessible aux citoyens quant aux progrès et aux risques susceptibles d’être générés par la science des données. En vue d’inspirer la confiance, et dans le respect du secret industriel, du secret des affaires, du secret défense, du secret professionnel et des droits et intérêts des individus, les Signataires fournissent une information claire et accessible, en tenant compte du public concerné, quant aux innovations développées grâce aux Données, notamment concernant les éléments suivants : les Données utilisées, leur lieu d’hébergement, la méthode d’analyse de ces Données et la finalité de cette analyse. Lorsque c’est possible, ils précisent également aux utilisateurs finaux le contexte optimal et les prérequis techniques ou organisationnels liés à l’utilisation des outils qu’ils mettent sur le marché ou qu’ils mettent en service."
        },
        "Auditabilité" : {
            description: "Les Signataires reconnaissent l’importance de faciliter le contrôle de la conformité des activités qu’ils mènent grâce aux Données au cadre légal qui s’applique à eux et, dans la mesure du possible, aux règles qu’ils mettent en œuvre afin d’appliquer la présente Charte. Ainsi, chaque étape d’un projet réalisé grâce aux Données est documentée, ces documents étant destinés à fournir des informations ou à servir de base à une évaluation ou à un contrôle : - Chaque participant au projet veille à conserver une description des Données qu’il a apportées ; - Chaque participant veille à assurer, en amont du projet, la traçabilité de ses Données par des mécanismes permettant de recenser et de détailler toutes les transformations effectuées ; - En cas d’utilisation d’algorithmes pour traiter les Données, au moins un participant prend soin de conserver une description de leur fonctionnement ;- Chaque participant documente les évaluations des impacts et des risques qu’il a réalisées et, pour les projets à fort impact sociétal, rend ces documents accessibles au public. Dans le cas où les Signataires décideraient de soumettre un projet à une évaluation, un contrôle ou un audit, ils favorisent des experts indépendants."
        }
    },
    "Gouvernance des données dans un cadre de confiance" : {
        "Évaluation des bénéfices et des risques" : {
            description : "Les Signataires appliquent le principe de précaution  dès le stade de la conception et tout au long de la mise en œuvre de leurs projets intégrant des traitements de Données. Ils s’efforcent d’évaluer les risques, directs ou indirects, susceptibles de découler de ces projets sur ce qui constitue leur écosystème, c’est-à-dire les individus, la société et l’environnement. Au regard des résultats révélés par les évaluations des risques, les Signataires cherchent à maximiser les effets bénéfiques et à minimiser les effets défavorables de leurs projets, tant à l’échelle individuelle qu’à l’échelle collective. Enfin, dans la mesure du possible et lorsque cela apparaît pertinent, les Signataires expérimentent les innovations développées grâce au traitement de Données à petite échelle, ou dans le cadre de « bacs à sable » , avant de les déployer."
        },
        "Inclusion des citoyens et des utilisateurs finaux" : {
            description: "De manière générale les Signataires offrent aux citoyens les moyens de développer leur littératie numérique. De manière plus ciblée, ils impliquent les futurs utilisateurs des solutions développées dans la conception et dans la mise en œuvre de leurs projets. Cette inclusion est réfléchie au stade de la conception et pour l’accessibilité des produits et services développés, notamment pour répondre aux attentes des personnes concernées à propos des modalités de traitement de leurs données et de l’exercice de leurs droits liés à ce traitement. Ce principe inclut la mise en place de moyens de communication spécifiques, appropriés et effectifs, tels que des consultations ou des processus de co-construction, à l’image de ceux mis en œuvre pour l’élaboration de la présente Charte."
        },
        "Apprentissage collectif" : {
            description: "Les Signataires privilégient des modes de gouvernance multipartite des projets, permettant de représenter dans les processus décisionnels les différents acteurs impliqués et concernés (pouvant notamment inclure les utilisateurs finaux ou les citoyens). Ces modes de gouvernance collaborative multipartite peuvent notamment s’inspirer du concept de commun numérique. Les Signataires assurent une veille quant à l’évolution des bonnes pratiques relatives aux usages Données et, dans le cadre d’un projet collaboratif, les partagent avec leurs collaborateurs. Les Signataires soulignent l’importance de la collaboration entre les différentes disciplines impliquées dans un projet, comprenant les domaines techniques et les sciences humaines et sociales du fait de la diversité des enjeux soulevés par l’usage des Données. En lien avec le mouvement de Science ouverte  et l’élaboration de communs numériques, les Signataires favorisent le partage des Données qu’ils produisent sur la base du principe « autant ouvert que possible, aussi fermé que nécessaire »  visant à maximiser le bénéfice des connaissances qui en découlent tout en respectant leur confidentialité (secret des affaires, propriété intellectuelle…). En ce sens, dans la mesure du possible, les Signataires privilégient une mise à disposition en accès ouvert (open access) en utilisant des licences adaptées. Enfin, les Signataires s’engagent à échanger régulièrement avec Ekitia sur l’évolution de ces bonnes pratiques et à faire remonter les difficultés rencontrées quant à la mise en œuvre d’un ou de plusieurs principes de la présente Charte."
        },
        "Intégrité" : {
            description :"Toute personne intervenant dans un projet réalisé grâce aux Données respecte les règles déontologiques auxquelles elle est soumise et agit dans un esprit d’intégrité intellectuelle et de coopération. Cela couvre tant les finalités de ces travaux que la méthode utilisée, la gestion des ressources humaines, la diffusion des connaissances et la communication scientifique. En outre les Signataires s’abstiennent d’utiliser les Données à des fins de falsification, de plagiat ou de rétention illégitime. Par ailleurs, les Signataires s’engagent à respecter un principe de loyauté au regard de la finalité des traitements de Données. Enfin, les Signataires qui souhaiteraient développer des systèmes d’IA générative veillent spécifiquement à ne pas les entraîner avec des Données protégées par des droits de propriété intellectuelle sans avoir préalablement obtenu l’accord explicite des détenteurs de ces droits."
        }
    },
    "Réciprocité" : {
        "Reconnaissance" : {
            description : "Dans le contexte d’un projet collaboratif entre plusieurs Signataires, chaque contribution essentielle à la réalisation de ce projet est explicitement reconnue et rendue publique (ce dans la limite de la confidentialité et des accords entre les collaborateurs). Ces contributions s’apparentent notamment à la production et à la fourniture de Données, à la fourniture d’algorithmes ou à la fourniture de travaux de recherche ayant permis d’impulser le projet et sur lesquelles repose son bon déroulement."
        },
        "Répartition équitable de la création de valeur" : {
            description : "Les Signataires reconnaissent que la création de valeur, qu’elle soit sociale, financière ou technique, ne doit pas être accaparée par un ou des acteurs dominants. Ainsi, lors d’un projet impliquant un traitement collaboratif de Données, d’algorithmes ou de recherches scientifiques, les Signataires recherchent des modèles économiques permettant un juste retour à chaque partie y ayant contribué."
        }
    }
}

function formatDate(d){
    let date = new Date(d);
    let newdate= "";
    if(!isNaN(date.getMonth())){
        newdate = moment(date).format('DD MMMM YYYY');
    }else{
        newdate = moment(d).format('DD MMMM YYYY');
    }
    return newdate;
}

// Object.assign(costum.lists, lists);

costum[costum.slug] = {
    activeContext: null,
    listedPrincipe : {},
    init: function(){
        coInterface.bindButtonOpenForm = function () {
            $(".btn-open-form").off().on("click", function () {
                var typeForm = $(this).data("form-type");
                currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
                //dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
                if (contextData && contextData.type && contextData.id) {
                    if (typeForm == "organization" || typeForm == "tiersLieux") {
                        dyFObj.openForm(typeForm, "afterLoad");
                    } else {
                        dyFObj.openForm(typeForm, "sub");
                    }
                }
                else {
                    if (typeForm == "organization" || typeForm == "tiersLieux") {
                        dyFObj.openForm(typeForm, "afterLoad");

                    } else {
                        dyFObj.openForm(typeForm);
                    }
                }
            });
        };


        var mainColors=Object.values(costum.css.color)
        var primaryColor = mainColors[0];
        var secondColor = mainColors[1];
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined"&& typeof costum.css.color.color1 !="undefined"){
            primaryColor = costum.css.color["color1"]; 
        }
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined"&& typeof costum.css.color.color2 !="undefined"){
            secondColor = costum.css.color["color2"];
        }
        stylecolor = ":root{ --primary-color: "+primaryColor+"; --secondary-color:"+secondColor+" }"
        $("style").append(stylecolor);
        if(userId == "" && location.hash!="#mentions" && location.hash!="#confidentialityRules" && location.hash!="#charte"){
            // Login.openLogin();
        }

        // if(userId!="" && location.hash=='#panel.box-register'){
        //     window.location.href = baseUrl+window.location.pathname;
        // }

        costum.typeObj.tiersLieux.dynFormCostum.prepData=function(data){
			mylog.log("prepData beginning",data);
		    var listObj=costum.lists;
		    $.each(listObj, function(e, v){
		        constructDataForEdit=[];
		        $.each(v, function(k, tag){
                    if(typeof tag=="string"){
    		            if(typeof tag=="string" && $.inArray(tag, data.map.tags) >=0){
    		                constructDataForEdit.push(tag);
    		             
    		                data.map.tags.splice(data.map.tags.indexOf(tag),1);
    		            }
                    }else{
                        $.each(tag, function(i, t){
                            if(typeof t=="string" && $.inArray(t, data.map.tags) >=0){
                                constructDataForEdit.push(t);
                             
                                data.map.tags.splice(data.map.tags.indexOf(t),1);
                            }
                        });
                    }    
    		    });
		        data.map[e]=constructDataForEdit;
		    });

			if (notNull(data.map.photo)) {
                uploadObj.initList.photo = data.map.photo;
            }

			if(typeof data.map.telephone!="undefined"){
			    if(typeof data.map.telephone.fixe=="object"){
				    data.map.telephone=data.map.telephone.fixe[0];
				}else if(typeof data.map.telephone.mobile=="object"){
					data.map.telephone=data.map.telephone.mobile[0];
				}
			}

			if(typeof data.map.socialNetwork!="undefined"){
				mappingSocialNetwork={
					"facebook" : "votre page Facebook", 
                    "twitter" : "votre compte Twitter", 
                    "instagram" : "votre compte Instagram", 
                    "linkedin" : "votre compte Linkedin", 
                    "mastodon" : "votre compte Mastodon", 
                    "movilab" : "votre page Movilab"
				};
				socialArray=[];
				$.each(data.map.socialNetwork,function(name,link){
					var socialObj={
						platform : mappingSocialNetwork[name],
						url : link
					};
					socialArray.push(socialObj);
				});
				data.map.socialNetwork=socialArray;
			}
		    mylog.log("prepData return",data);
		    return data;
		};

        costum[costum.slug].getOrganizations();
        $("#authclientCo").parent().remove(); // To do: add params to check if external service needed

        if(localStorage.getItem("IhaveSeenCookieInfoEkisphere")==null){
            costum[costum.slug].cookieInfo.initView();
            costum[costum.slug].cookieInfo.initEvent();
        }
    },
    cookieInfo: {
        initView:function(){
            // $("body").append(`
            //     <div id="cookie-notice" role="dialog" class="cookie-revoke-hidden cn-position-bottom cn-effect-fade cookie-notice-visible" aria-label="Cookie Notice" style="background-color: rgba(33,33,33,1);">
            //         <div class="cookie-notice-container" style="color: #ffffff">
            //             <span id="cn-notice-text" class="cn-text-container">Le réseau Lakou et Communecter déposent uniquement des cookies utiles au bon fonctionnement du site. Pour tout renseignement complémentaire, veuillez vous référer à notre page de politique de protection des données personnelles. Bonne visite</span><span id="cn-notice-buttons" class="cn-buttons-container">
            //             <button id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie cn-button" aria-label="OK" style="background-color: #ba1c24">OK</button>
            //             <a href="https://www.ekitia.fr/politique-de-protection-des-donnees/" target="_blank" id="cn-more-info" class="cn-more-info cn-button" aria-label="Politique de protection des données personnelles" style="background-color: #ba1c24">Politique de protection des données personnelles</a></span>
            //             <span id="cn-close-notice" data-cookie-set="accept" class="cn-close-icon" title="Non"></span>
            //         </div>
            //     </div>
            // `)
        },
        initEvent: function(){
            $("#cn-accept-cookie").on("click", function(){
                localStorage.setItem("IhaveSeenCookieInfoEkisphere", true)
                $("#cookie-notice").remove();
            })
        }
    },
    organizations: {
        formData: function(data){
            mylog.log("start custom formData",data);
            var invertCategory={};
            $.each(costum.lists.category,function(cat,sub){
                $.each(sub,function(ind,val){
                    invertCategory[val]=cat;
                })
            });
               
            $.each(data, function(e, v){
                if(typeof costum.lists[e] != "undefined"){
                    if(notNull(v)){
                        if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
                        if(typeof v == "string"){
                            data.tags.push(v);
                        }else{
                            $.each(v, function(i,tag){
                                data.tags.push(tag);
                            });
                        }
                    }
                    delete data[e];
                }

                if(e === "structure"){
                    if(v === "Entreprise"){
                        data.type = "LocalBusiness";
                    }else if(v === "Structures Publiques"){
                        data.type = "GovernmentOrganization";
                    }else if(v.indexOf("Association") >= 0){
                        data.type = "NGO";
                    }else{
                        data.type = "Cooperative";
                    }
                }

                if (e === "extraActivity") {
                    $.each(v, function (k, d) {
                        data.tags.push(d.activity);
                    })
                    delete data[e];
                }
                if (e === "linkedin") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["linkedin"] = v;
                    delete data[e];
                }
                if(e.indexOf("admin") >= 0){
                    data.contacts = typeof data.contacts != "undefined" ? data.contacts : {};
                    const index = e.replace("admin", "").toLowerCase();

                    data.contacts[index] = v;
                    delete data[e];
                }
            });

            data["preferences"] = {
                "isOpenData" : true,
                "isOpenEdition" : false
            },

            data.tags.splice(data.tags.indexOf("Autres"), 1);
            data.role = "admin";
            if(!costum.isCostumAdmin && !(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[data.id] != "undefined")){
                data.preferences.toBeValidated = {};
                data.preferences.toBeValidated[costum.slug] = true;

            }
            // if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
            //     data.preferences = typeof data.preferences != "undefined" ? data.preferences : {};
            //     data.preferences.toBeValidated = {};
            //     data.preferences.toBeValidated[costum.slug] = true;
            // }else if(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined"){
            //     data.preferences.isOpenData=true;
            // }

            mylog.log("end custom formData",data);
            return data;
        },
        afterBuild: function(data){
            $(".extraActivitylists").hide();
            alignInput2(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties ,"admin",4,12,0,null,"Contact(s)","#ba1c24","");
            $("#activity").off("change").on("change", function(){
                if($(this).val().includes("Autres")){
                    $(".extraActivitylists").show();
                }else{
                    $(".extraActivitylists").hide();
                }
            })
            $("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
            $("#divCity input").attr("placeholder", "Saisir le code postal");
            dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
				mylog.log("afterload reseauTL data", data);
                dyFObj.formInMap.resumeLocality=function(cancel){
                    mylog.log("formInMap resumeLocality", cancel);
                    if(dyFObj.formInMap.NE_street != ""){
                        $('#ajaxFormModal #street_sumery_value').html(dyFObj.formInMap.NE_street );
                        $('#ajaxFormModal #street_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #street_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_cp != ""){
                        $('#ajaxFormModal #cp_sumery_value').html(dyFObj.formInMap.NE_cp );
                        $('#ajaxFormModal #cp_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #cp_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_city != ""){
                        $('#ajaxFormModal #city_sumery_value').html(dyFObj.formInMap.NE_city);
                        $('#ajaxFormModal #city_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #city_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_country != ""){
                        $('#ajaxFormModal #country_sumery_value').html(dyFObj.formInMap.NE_country);
                        $('#ajaxFormModal #country_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #country_sumery').addClass("hidden");
        
        
                    if(dyFObj.formInMap.NE_country != "" && dyFObj.formInMap.NE_city != ""){
                        //$("#btnValideAddress").prop('disabled', false);
                        $("#ajaxFormModal #btnValideAddress").show();
                        // toastr.error(trad.pleaseConfirmAddress);
                    }else{
                        //$("#btnValideAddress").prop('disabled', true);
                        $("#ajaxFormModal #btnValideAddress").hide();
                    }
                };
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
                    dyFObj.formInMap.newAddress(false);
					$('#ajaxFormModal #divNewAddress').hide();
				}

				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);

        			    }
        			    // Div récapitulatif de l'adresse 
                        
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    

        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

    
        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
            };

        },
        afterSave: function(data){
            dyFObj.commonAfterSave(data, function(response){
                var orgaData=response;
                if(typeof dyFObj.elementObj.stayOpened!="undefined" && dyFObj.elementObj.stayOpened){
                    delete dyFObj.elementObj.stayOpened;
                }
                delete typeObj.organization.dynForm;
                // ajouter automatiquement l"orga dans les membre lakou
                // links.connectAjax(costum.contextType,costum.contextId,response.id, response.map.collection, "member",null,function(callbackData){
                //     mylog.log("callback connect ajax",callbackData);
                    urlCtrl.loadByHash('#mesinfos');
                // });

                
                // var dataMembers = {
                //     id: costum.contextId,
                //     collection: costum.contextType,
                //     path: "links.members",
                //     value: typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" ? costum.communityLinks.members : {}
                // }
                // dataMembers.value[response.id] = {type: response.map.collection};
                // if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
                //     dataMembers.value[response.id].toBeValidated = true;
                // }
                // mylog.log("dataMembers",dataMembers);

                // dataHelper.path2Value(dataMembers, function(){
                //     mylog.log("orgaData.id",orgaData.id);
                //     // return
                //     var dataMemberOf = {
                //         id: orgaData.id,
                //         collection: orgaData.map.collection,
                //         path: "links.memberOf",
                //         value: {}
                //     }
                //     mylog.log("dataMemberOf",dataMemberOf);
                //     dataMemberOf.value[costum.contextId] = {type: costum.contextType};
                //     if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
                //         dataMemberOf.value[costum.contextId].toBeValidated = true;
                //     }
                //     dataHelper.path2Value(dataMemberOf, function(){
                //         urlCtrl.loadByHash("#mesinfos");
                //     })
                // })
            })
        }
    },
    tiersLieux : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData In",data);

			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0){
				$.each(finder.object, function(key, object){
					if(typeof formData[key]!="undefined"){
						delete formData[key];
					}
				});
			}

			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string"){
							if(e=="manageModel" && v=="Autre"){
								v="Autre mode de gestion";
							}
							if(e=="typePlace" && v=="Autre"){
								v="Autre famille de tiers-lieux";
							}
							data.tags.push(v);
							
						}else{
							$.each(v, function(i,tag){
								if(e=="typePlace" && tag=="Autre"){
									tag="Autre famille de tiers-lieux";
								}
								if(e=="manageModel" && tag=="Autre"){
									tag="Autre mode de gestion";
								}
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});

			if(typeof data.telephone == "string"){
				var mapIndicTel={
					"FR" : "33",
					"GP" : "590",
					"MQ" : "596",
					"GF" : "594",
					"RE" : "262",
					"PM" : "508",
					"YT" : "269",
					"WF" : "681",
					"PF" : "689",
					"NC" : "687"
				};

				var phone=data.telephone;
				var numIndex=2;
				phone=phone.replace(/\s/g,'');
				// remplacer 00 par +
				phone=(phone.substring(2,0)=="00") ? "+"+phone.substring(2) : 
				    ((phone.substring(0,1)!="+" && typeof data.address!="undefined" && typeof data.address.addressCountry!="undefined" && typeof mapIndicTel[data.address.addressCountry]!="undefined") 
					   ? "+"+mapIndicTel[data.address.addressCountry]+phone.substring(1) : phone );
				if(phone.substring(0,1)=="+"){
					if(phone.substring(1,3)=="33"){
						numIndex=4;
					}else{
						numIndex=5;
					}
				}
				
				var mobOrfixeInd=phone.substring(numIndex,numIndex-1);
				var typeNumber= (mobOrfixeInd=="6" || mobOrfixeInd=="7") ? "mobile" : "fixe" ;	
			    data.telephone={};
				data.telephone[typeNumber]=[phone];
			}

			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}

			if(data.socialNetwork){
				var formatedSocialNetwork={};
                $.each(data.socialNetwork,function(i,net){
					var labelArray=net["platform"].split(" ");
  
					var nameSocNet=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["url"]!="undefined" && net["url"]!=""){
                        formatedSocialNetwork[nameSocNet]=net["url"];						
					}
					delete data.socialNetwork[i];

				});
                data.socialNetwork=formatedSocialNetwork;
			}

            if(data.openingHours){
                var i=0;
                $.each(openingHoursResult, function(e,v){
                    var plage=openingHoursResult[i];
                    if(!notEmpty(plage) || typeof plage.disabled != "undefined"){
                        openingHoursResult.splice(i,1);
                    }else{
                        i++
                    }
                });
                data.openingHours=openingHoursResult;
            }   

            mylog.log("formData return",data);
            orgaFormData=costum[costum.slug].organizations.formData(data);

			return orgaFormData;
		},
		afterBuild : function(data){
			mylog.log("afterbuild reseauTierslieux",data);
			if(typeof data.manageModel=="undefined" || (typeof data.manageModel!="undefined" && !data.manageModel.includes("Autre mode de gestion"))){
					// alert("showthos");
				$("#ajaxFormModal .extraManageModeltext").hide();
			}
			if(typeof data.typePlace=="undefined" || (typeof data.typePlace!="undefined" && !data.typePlace.includes("Autre famille de tiers-lieux"))){
				$("#ajaxFormModal .extraTypePlacetext").hide();
			}

			$("#divCity input").attr("placeholder","Saisir votre code postal");
						    
			$("#ajaxFormModal .manageModelselect select").change(function(){
			    var modelStatus=$(this).val();
			    if(modelStatus!="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext input").val("");
				   $("#ajaxFormModal .extraManageModeltext").hide();
			    }
			    var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre mode de gestion"	: "Group"
			    }; 
			    $("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});
			             
			$("#ajaxFormModal .typePlaceselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var placeCat=$(this).val();
			    if(placeCat==null || placeCat.includes("Autre famille de tiers-lieux")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(notNull(placeCat) && placeCat.includes("Autre famille de tiers-lieux")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var themeNetwork=$(this).val();
			    if(themeNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraThemeNetworktext").hide();
			       $("#ajaxFormModal .extraThemeNetwork input").val("");
			    }
			    if(notNull(themeNetwork) &&  themeNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraThemeNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});

			$("#ajaxFormModal .localNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var localNetwork=$(this).val();
			    if(localNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraLocalNetworktext").hide();
			       $("#ajaxFormModal .extraLocalNetwork input").val("");
			    }
			    if(notNull(localNetwork) && localNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraLocalNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


            //init openingHours
            if(typeof data.openingHours!="undefined"){
			    $.each(openingHoursResult, function(e,v){
      				mylog.log("custom init openinghours",data.openingHours,e);
      				if(typeof data.openingHours[e]=="undefined" || !notEmpty(data.openingHours[e]) ){
      					openingHoursResult[e].disabled=true;
      				}
      			});	      
			}	
					
            $(".btn-select-day").off().on("click",function(){
                mylog.log("selectDay", openingHoursResult, $(this).data("key"));
                key=$(this).data("key");
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                    $.each(openingHoursResult, function(e,v){
                    if(v.dayOfWeek==key)
                        openingHoursResult[e].disabled=true;
                    });
                    $("#contentDays"+key).fadeOut();
                }else{
                    $(this).addClass("active");
                    $.each(openingHoursResult, function(e,v){
                        if(v.dayOfWeek==key)
                        	delete openingHoursResult[e].disabled;
                    });
                    $("#contentDays"+key).fadeIn();
                }
            });

		    $('.timeInput').off().on('changeTime.timepicker', function(e) {
			    mylog.log("changeTimepicker");
			    var typeInc=$(this).data("type");
			    var daysInc=$(this).data("days");
			    var hoursInc=$(this).data("value");
			    var firstEnabled=null;
			    var setFirst=false
			    $.each(openingHoursResult, function(i,v){
				    if(!setFirst && typeof openingHoursResult[i].disabled=="undefined"){
				       mylog.log("firtEnable",v.dayOfWeek);
				       firstEnabled=v.dayOfWeek;
				       setFirst=true;
			        }
	                if(firstEnabled==daysInc){
	        	        mylog.log("first enabled change causes recursive change",v.dayOfWeek);
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
                        var typeHours="";
	        	        if(typeInc=="opens"){
	        		        typeHours="start";
	        	        }else if(typeInc=="closes"){
	        		        typeHours="end";
	        	        }
	        	        if(typeHours!=""){
	        		        $("#"+typeHours+"Time"+v.dayOfWeek+hoursInc).val(e.time.value);
	        	        }
	                }else if(v.dayOfWeek==daysInc){
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
	                }
		        });
		    });

			$('.timeInput').on("focus",function (){
			    $(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();   

			// AFTERLOAD
            $("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
            $("#divCity input").attr("placeholder", "Saisir le code postal");
            
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
				mylog.log("afterload reseauTL data", data);
                dyFObj.formInMap.resumeLocality=function(cancel){
                    mylog.log("formInMap resumeLocality", cancel);
                    if(dyFObj.formInMap.NE_street != ""){
                        $('#ajaxFormModal #street_sumery_value').html(dyFObj.formInMap.NE_street );
                        $('#ajaxFormModal #street_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #street_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_cp != ""){
                        $('#ajaxFormModal #cp_sumery_value').html(dyFObj.formInMap.NE_cp );
                        $('#ajaxFormModal #cp_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #cp_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_city != ""){
                        $('#ajaxFormModal #city_sumery_value').html(dyFObj.formInMap.NE_city);
                        $('#ajaxFormModal #city_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #city_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_country != ""){
                        $('#ajaxFormModal #country_sumery_value').html(dyFObj.formInMap.NE_country);
                        $('#ajaxFormModal #country_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #country_sumery').addClass("hidden");
        
        
                    if(dyFObj.formInMap.NE_country != "" && dyFObj.formInMap.NE_city != ""){
                        //$("#btnValideAddress").prop('disabled', false);
                        $("#ajaxFormModal #btnValideAddress").show();
                        // toastr.error(trad.pleaseConfirmAddress);
                    }else{
                        //$("#btnValideAddress").prop('disabled', true);
                        $("#ajaxFormModal #btnValideAddress").hide();
                    }
                };
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
                    dyFObj.formInMap.newAddress(false);
					$('#ajaxFormModal #divNewAddress').hide();
				}

				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);

        			    }
        			    // Div récapitulatif de l'adresse 
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    

        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

    
        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
            };
		},
		beforeSave : function(data){
			mylog.log("beforeSave data",data);
            if(notNull(data.newElement_city)){
			    delete data.newElement_city;
            }

		    if(notNull(data.newElement_street)){
			    delete data.newElement_street;
		    }
		    return data;
		},
		afterSave:function(data){
            mylog.log("aftersaved tiers-lieux",data);
            costum[costum.slug].organizations.afterSave(data);
				// urlCtrl.loadByHash(location.hash);
			// uploadObj.afterLoadUploader=false;
			// dyFObj.commonAfterSave(data, function(response){
            //     mylog.log("commonaftersave tiers-lieux",response);
            //     if(userId==""){
			// 		if(!dyFObj.unloggedProcess.isAlreadyRegister)
			// 			$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite électronique</span>");
			// 	}
            //     // lazyLoad( 
            //     //     parentModuleUrl+'/js/dynForm/organization.js',
            //     //     null,
            //     //     function() {
            //     //         // originalDyFOrga["dynForm"]=dynForm;
            //     //         openSecondStep(dynForm);       
            //     //     }
            //     // );       

            //     // function openSecondStep(dyf){
            //     //     var originalDyFOrga={
            //     //         // dynForm : {}
            //     //         dynForm : dyf
            //     //     };
            //     //     // originalDyFOrga.dynForm.jsonSchema.properties={};
            //     //     typeObj.miniOrga=$.extend(true,typeObj.organization,originalDyFOrga);
            //     //     typeObj.miniOrga.stayOpened=true;
            //     //     delete typeObj.miniOrga.dynForm.jsonSchema.beforeBuild;
            //     //     // typeObj.miniOrga.dynForm.jsonSchema.properties.image.afterUploadComplete=function(){alert("afteruplade");}
            //     //     mylog.log("okcharge",typeObj.miniOrga);
            //     //     var tlFields=$.extend(true,typeObj.tiersLieux.dynForm.jsonSchema.properties,typeObj.tiersLieux.dynFormCostum.beforeBuild.properties);
            //     //     var dyFOrga=$.extend({},costum.typeObj.organizations);
            //     //     dyFOrga.dynFormCostum.prepData=function(prepData){
            //     //         return prepData;
            //     //     }
            //     //     $.each(dyFOrga.dynFormCostum.beforeBuild.properties,function(key,value){
            //     //         if(key=="structure"){
            //     //             delete dyFOrga.dynFormCostum.beforeBuild.properties[key];
            //     //         }
            //     //         if(Object.keys(tlFields).indexOf(key) > -1){
            //     //             if(typeof value.inputType=="undefined"){
            //     //                 value.inputType= (typeof typeObj.miniOrga.dynForm.jsonSchema.properties[key]!="undefined") ? typeObj.miniOrga.dynForm.jsonSchema.properties[key]["inputType"] : "";
            //     //             }
            //     //             dyFOrga.dynFormCostum.onload.actions.hide[key+value.inputType]=1;
            //     //         }
    
            //     //     })
                    
            //     //     async function closeFirstStep(paramsData){ 
            //     //         // dyFObj.closeForm();
            //     //         dyFObj.closeForm = function() {
            //     //             mylog.log("closeForm",dyFObj.elementObj);
            //     //             dyFObj.editMode=false;
            //     //             dyFObj.dynFormCostum=null;
            //     //             dyFObj.currentElement=null;
            //     //              mylog.log("close form top");
            //     //             if(typeof dyFObj.elementObj.stayOpened=="undefined" || (typeof dyFObj.elementObj.stayOpened!="undefined" && !dyFObj.elementObj.stayOpened)){
            //     //                 mylog.log("close form notTL");
            //     //                 $(dyFObj.activeModal).attr('class', 'portfolio-modal modal fade');
            //     //                 $(".modal-header").removeClass("hide");
            //     //                 $('#ajax-modal').modal("hide");
            //     //                 $("body").css("overflow-y","auto");                            
            //     //             //clear the unecessary DOM
            //     //                 $("#ajaxFormModal").html('');
            //     //             }    
            //     //             uploadObj.set();
            //     //             uploadObj.update = false;
            //     //             if(typeof dialogbt != "undefined"){
            //     //                 dialogbt.modal("hide");delete dialogbt;
            //     //             }
            //     //             if (typeof dyFObj.onclose === "function")
            //     //                 dyFObj.onclose();
            //     //         }; 
            //     //         return paramsData;
            //     //     }    
    
            //     //     closeFirstStep(response).then(
            //     //         function(value){
            //     //             mylog.log(value,"closeFirstStep");
            //     //             ajaxPost(
            //     //                 null,
            //     //                 baseUrl+"/"+moduleId+"/element/get/type/"+value.map.collection+"/id/"+value.id+"/update/true",
            //     //                 {},
            //     //                 function (callbackData) {
            //     //                     mylog.log("callbackData update ajax",callbackData);
            //     //                     callbackData=dyFOrga.dynFormCostum.prepData(callbackData);
            //     //                     mylog.log("callbackData after prepData",callbackData);
            //     //                     callbackData.map.id = callbackData.map["_id"]["$id"];
            //     //                     dyFObj.openForm(typeObj.miniOrga,"afterLoad",callbackData.map,null,dyFOrga.dynFormCostum);
            //     //                     // dyFObj.openForm(typeObj.miniOrga,"afterLoad",{id:callbackData.id,collection:callbackData.map.collection,tags:callbackData.map.tags},null,dyFOrga.dynFormCostum);
            //     //                     $( "#ajax-modal").scrollTop(0);
            //     //                 },
            //     //                 null,
            //     //                 null,
            //     //                 {async : false}
            //     //             );        
            //     //         },function(error){
            //     //             // alert(error);
            //     //         }
            //     //     );  
            //     // }
			// });
		}
	},
    tools: {
        afterBuild: function(){
            alignInput2(costum.typeObj.tools.dynFormCostum.beforeBuild.properties ,"admin",4,12,0,null,"Qui contacter en cas de besoin de précision sur l'outil ?","#777","");
        }
    },
    initAddress : function(initData){
        let data=initData;
                dyFObj.formInMap.resumeLocality=function(cancel){
                    mylog.log("formInMap resumeLocality", cancel);
                    if(dyFObj.formInMap.NE_street != ""){
                        $('#ajaxFormModal #street_sumery_value').html(dyFObj.formInMap.NE_street );
                        $('#ajaxFormModal #street_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #street_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_cp != ""){
                        $('#ajaxFormModal #cp_sumery_value').html(dyFObj.formInMap.NE_cp );
                        $('#ajaxFormModal #cp_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #cp_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_city != ""){
                        $('#ajaxFormModal #city_sumery_value').html(dyFObj.formInMap.NE_city);
                        $('#ajaxFormModal #city_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #city_sumery').addClass("hidden");
        
                    if(dyFObj.formInMap.NE_country != ""){
                        $('#ajaxFormModal #country_sumery_value').html(dyFObj.formInMap.NE_country);
                        $('#ajaxFormModal #country_sumery').removeClass("hidden");
                    }else
                        $('#ajaxFormModal #country_sumery').addClass("hidden");
        
        
                    if(dyFObj.formInMap.NE_country != "" && dyFObj.formInMap.NE_city != ""){
                        //$("#btnValideAddress").prop('disabled', false);
                        $("#ajaxFormModal #btnValideAddress").show();
                        // toastr.error(trad.pleaseConfirmAddress);
                    }else{
                        //$("#btnValideAddress").prop('disabled', true);
                        $("#ajaxFormModal #btnValideAddress").hide();
                    }
                };

				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);

        			    }
        			    // Div récapitulatif de l'adresse 
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    

        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

    
        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
    },
    events : {
        afterBuild: function(afterloadData){
            mylog.log("afterBuild events",afterloadData);
            let data=afterloadData;
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
                    dyFObj.formInMap.newAddress(false);
					$('#ajaxFormModal #divNewAddress').hide();
				}
                costum[costum.slug].initAddress();
                // dyFObj.formInMap.init();
        },
        afterSave : function(afterSaveData){
            dyFObj.commonAfterSave(afterSaveData, function(response){
                mylog.log("callback common aftersave events",response);
                dyFObj.closeForm();
                // urlHistoric=hashNav+"?preview="+$(this).data("type")+"."+$(this).data("id");
                // history.replaceState({}, null, urlHistoric);
	        	urlCtrl.openPreview("#page.type."+response.map.collection+".id."+response.id);
            })
        }
    },
    openInModal : function(url, type=null, context){
        let myContext = {_id:{$id:context.id}, profilThumbImageUrl:context.img, ...context};
        if(type!=null){
            let defaultValue = {};
            defaultValue[context.id] = myContext;
            console.log("open in modal -- here" , defaultValue);
            if(type == "event"){
                let defautDyFInputsInit = dyFInputs.init;
                dyFInputs.init = function(){
                    typeObj[type].dynForm.jsonSchema.properties.shortDescription = dyFInputs.textarea(tradDynForm.shortDescription, "...", {})
                    defautDyFInputsInit()
                }
                dyFObj.openForm(type, null, {organizer: defaultValue});
            }else{
                dyFObj.openForm(type, null, {parent: defaultValue});
            }
        }else if(url.indexOf("#")>=0){
            let ctx = "";
            if(url.indexOf("answer")>=0){
                ctx = ".contextId."+context.id+".contextType."+context.type;
            }
            urlCtrl.loadByHash(url+ctx, true);
        }else{
            if(userId != ""){
                smallMenu.open(`<div id="contentHtml" class="container"><div>`);
                costum[costum.slug].postNumericService("#contentHtml", url, "formulaire", myContext)
            }
        }
    },
    beforePost : function(url, type=null){
        let titleForm="<h4>Sélectionner la structure pour la publication</h4>";
		var dialog = bootbox.dialog({
		    title: titleForm,
		    message: '<div id="finderSelectHtml" style="max-height:480px;">'+
		    			//'<input class="form-group form-control padding-5" type="text" id="populateFinder" placeholder="context"/>'+
						'<div id="list-my-context" class="padding-5" style="max-height:380px; overflow-y:auto; border: 1px solid #eee">'+
                            '<div id="list-loader"></div>'+
                        '</div>'+
						'<div class="col-md-12 col-sm-12 col-xs-12 text-center">'+
                        '</div>'+
		    		'</div>',
		    closeButton:true,
		    buttons: {
			    cancel: {
			        label: trad.Close,
			        className: 'btn-default',
			        callback: function(){
			            dialog.modal('hide');
			        }
			    }
			}
		});
		dialog.on('shown.bs.modal', function(e){
			if(userId != ""){
                coInterface.showLoader("#list-loader");
				let params = {
                    "searchType":["organizations"],
                    "notSourceKey":true,
                    "filters" : {
                        "$or":{
                            "source.keys":costum.slug, 
                            "reference.costum":costum.slug
                        }
                    }
                };
                params.filters["links.members."+userId+".isAdmin"] = {'$exists':true};
                ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/search/globalautocomplete",
					params,
					function(data){
                        mylog.log("Here we go modal", data);
                        $("#list-my-context").empty();
                        var results=data.results;
                        let img = modules.co2.url + "/images/thumb/default_citoyens.png";
                        // if user admin add ess Connect object in orga to choose
                        if(costum.isCostumAdmin){
                            var contextObj={
                                name : costum.title,
                                type : "Group",
                                collection : costum.contextType,
                                profilMediumImageUrl : costum.metaImg
                            };
                            results[costum.contextId]=contextObj;
                            mylog.log("add ctxt obj",data.results);
                        }

                        $.each(results, function(index, item){
                            if(!(notEmpty(item.links) && notEmpty(item.links.members) && notEmpty(item.links.members[userId]) && notEmpty(item.links.members[userId].isAdminPending) && item.links.members[userId].isAdminPending)){
                                if(item.profilMediumImageUrl){
                                    img = item.profilMediumImageUrl;
                                }
                                var str ="<div class='population-elt-finder population-elt-finder-"+index+" col-xs-12 padding-5' style='border-bottom:1px solid #eee' data-id='"+index+"' data-type='"+item.collection+"' data-img='"+img+"' data-name='"+item.name+"'>"+
                                    "<div class='element-finder element-finder-"+index+"'>"+
                                        '<img src="'+ img +'" class="thumb-send-to pull-left img-circle" height="50" width="50">'+
                                        '<span class="info-contact pull-left margin-left-20">' +
                                            '<span class="name-element text-dark text-bold" data-id="'+index+'">' + item.name + '</span>'+
                                            '<br/>'+
                                            '<span class="type-element text-light pull-left">' + trad[item.type]+ '</span>'+
                                        '</span>' +
                                    "</div>"+
                                "</div>";
                                $("#list-my-context").append(str);
                            }
                        });
                        dialog.find(".population-elt-finder").on("click", function(e){
                            dialog.modal("hide");
                            costum[costum.slug].openInModal(url, type, {id:$(this).data("id"), type:$(this).data("type"), name:$(this).data("name"), img:$(this).data("img")});
                        })
                        if($(".population-elt-finder").length==1){
                            $(".population-elt-finder").click();
                        }else if($(".population-elt-finder").length==0){
                            $("#list-my-context").append("<h3 style='text-transform: unset;'>Vous devez être admin d'une structure validée pour pouvoir en ajouter</h3>");
                            $("#list-my-context").next().append("<div style='font-size:16px;' class='margin-top-10 col-xs-12 text-center'><a style='color: var(--secondary-color);' class='closeModalOpenSearch bold' href='#search'>Rendez vous dans l'annuaire</a> pour demander à administrer une structure existante ou en ajouter un nouvelle.</div>");
                            $(".closeModalOpenSearch").off().on("click",function(){
                                urlCtrl.loadByHash("#search");
                                dialog.modal("hide");
                            })
                            $(".bootbox div.modal-header").hide();
                        }
					},
					null,
					"json",
					{
						async: true
					}
				);
			}else{
                Login.openLogin();
                dialog.modal("hide");
            }
		});
    },
    finderOrga : function(url, type=null){
        let titleForm="<h4>Rechercher une structure</h4>";
		var dialog = bootbox.dialog({
		    title: titleForm,
		    message: '<div id="finderSelectHtml" style="max-height:480px;">'+
		    			'<input class="form-group form-control padding-5" type="text" id="populateFinder" placeholder="Saisir le nom d\'une structure"/>'+
						'<div id="list-my-context" class="padding-5" style="max-height:380px; overflow-y:auto; border: 1px solid #eee">'+
                            '<div id="list-loader"></div>'+
                        '</div>'+
						'<div class="col-md-12 col-sm-12 col-xs-12 text-center">'+
                        '</div>'+
		    		'</div>',
		    closeButton:true,
		    buttons: {
			    cancel: {
			        label: trad.Close,
			        className: 'btn-default',
			        callback: function(){
			            dialog.modal('hide');
			        }
			    }
			}
		});
		dialog.on('shown.bs.modal', function(e){
            dialog.attr("id","finderOrga");
			if(userId != ""){
                $("#populateFinder").keyup(function(){
                    coInterface.showLoader("#list-my-context");
                    mylog.log("finder.showPanel keyup");
                    if($(this).val().length > 3){
                        // finder.filterPopulation($(this).val());
                        let params = {
                            "searchType":["organizations"],
                            "notSourceKey":true,
                            "name" : $(this).val(),
                            "filters" : {
                                "$or":{
                                    "source.keys":costum.slug, 
                                    "reference.costum":costum.slug
                                }
                            }
                        };
                        // params.filters["links.members."+userId] = {'$exists':true};
                        ajaxPost(
                            null,
                            baseUrl+"/"+moduleId+"/search/globalautocomplete",
                            params,
                            function(data){
                                mylog.log("Here we go modal", data);
                                $("#list-my-context").empty();
                                var results=data.results;
                                let img = modules.co2.url + "/images/thumb/default_citoyens.png";
                                // if user admin add ess Connect object in orga to choose
                                if(costum.isCostumAdmin){
                                    var contextObj={
                                        name : costum.title,
                                        type : "Group",
                                        collection : costum.contextType,
                                        profilMediumImageUrl : costum.metaImg
                                    };
                                    results[costum.contextId]=contextObj;
                                    mylog.log("add ctxt obj",data.results);
                                }
        
                                $.each(results, function(index, item){
                                    if(item.profilMediumImageUrl){
                                        img = item.profilMediumImageUrl;
                                    }
                                    var str ="<div class='population-elt-finder population-elt-finder-"+index+" col-xs-12 padding-5' style='border-bottom:1px solid #eee' data-id='"+index+"' data-type='"+item.collection+"' data-img='"+img+"' data-name='"+item.name+"'>"+
                                        "<div class='element-finder element-finder-"+index+"'>"+
                                            '<img src="'+ img +'" class="thumb-send-to pull-left img-circle" height="50" width="50">'+
                                            '<span class="info-contact pull-left margin-left-20">' +
                                                '<span class="name-element text-dark text-bold" data-id="'+index+'">' + item.name + '</span>'+
                                                '<br/>'+
                                                '<span class="type-element text-light pull-left">' + trad[item.type]+ '</span>'+
                                            '</span>' +
                                        "</div>"+
                                    "</div>";
                                    $("#list-my-context").append(str);
                                });
                                dialog.find(".population-elt-finder").on("click", function(e){
                                    dialog.modal("hide");
                                    // e.preventDefault();
                        			// $("#openModal").modal("hide");
                        			// mylog.warn("***************************************");
                        			// mylog.warn("coInterface.bindLBHLinks",$(this).attr("href"));
                        			// mylog.warn("***************************************");
                        			// //searchObject.reset();
                        			// var h = ($(this).data("hash")) ? $(this).data("hash") : $(this).attr("href");
                        			// urlCtrl.closePreview();
                        			// $("#modalExplain").modal("hide");
                        			// dyFObj.closeForm();
                                    hashNav=(hashT[0].indexOf("#") < 0) ? "#"+hashT[0] : hashT[0];
                                    urlHistoric=hashNav+"?preview="+$(this).data("type")+"."+$(this).data("id");
                                    history.replaceState({}, null, urlHistoric);
	        	                    urlCtrl.openPreview("#page.type."+$(this).data("type")+".id."+$(this).data("id"));

                        		    // urlCtrl.loadByHash("#page.type."+$(this).data("type")+".id."+$(this).data("id"));
                                    // costum[costum.slug].openInModal(url, type, {id:$(this).data("id"), type:$(this).data("type"), name:$(this).data("name"), img:$(this).data("img")});
                                })
                                coInterface.bindLBHLinks();
                                if($(".population-elt-finder").length==1){
                                    $(".population-elt-finder").click();
                                }else if($(".population-elt-finder").length==0){
                                    $("#list-my-context").append("<h3 class=''>Aucun résultat n'a été trouvé.</h3><br/><a href='javascript:;' onclick='costum[costum.slug].chooseOrgaType();$(`#finderOrga`).modal(`hide`);' class='orga-choice-modal btn btn-primary-ekitia btn-primary-outline-ekitia'><i class='fa fa-plus margin-right-5'></i> Ajouter une structure</a>");
                                    $(".bootbox div.modal-header").hide();
                                }
                            },
                            null,
                            "json",
                            {
                                async: true
                            }
                        );
                    }else{
                        $("#list-my-context").empty().html("<p>Veuillez saisir plus de 3 caractères pour la recherche.</p>");
                    }
                    // else{
                    //     if(notNull(open) && open){
                    //         finder.filterPopulation($(this).val());
                    //         finder.searchAndPopulateFinder(keyForm,$(this).val(), typeSearch, multiple,field);
                    //     }else{
                    //         finder.filterPopulation($(this).val());
                    //     }
                    // }
                });
			}else{
                Login.openLogin();
                dialog.modal("hide");
            }
		});
    },
    chooseOrgaType : function(){
        let titleForm="<h4>Sélectionnez le type de structure que vous souhaitez ajouter</h4>";
		var dialog = bootbox.dialog({
		    title: titleForm,
		    message: '<a href="javascript:;" data-form-type="tiersLieux" class="text-center bootbox-close-button btn-primary-outline-ekitia btn-open-form col-xs-10 col-xs-offset-1 margin-bottom-10">Un tiers-lieu</a>'+
            '<a href="javascript:;" data-form-type="organization" class="text-center bootbox-close-button btn-primary-outline-ekitia btn-open-form col-xs-10 col-xs-offset-1 margin-bottom-10">Une autre structure de proximité</a>',
		    closeButton:true,
		    buttons: {
			    cancel: {
			        label: trad.Close,
			        className: 'btn-default',
			        callback: function(){
			            dialog.modal('hide');
			        }
			    }
			}
		});
		dialog.on('shown.bs.modal', function(e){
			coInterface.bindButtonOpenForm();
		});
    },
    postNumericService : function(formContainer, formId, name, context){
        var formAnswerId = function (formId, descKey) {
            var dataId = "";
            ajaxPost(
                null,
                baseUrl + `/survey/answer/newanswer/form/${formId}`,
                {
                    "action": "new"
                },
                function (data) {
                    if (data._id) {
                        dataId = data._id.$id;
                        var dataToSendLinks = {
                            id: data._id.$id,
                            collection: "answers",
                            path: "links.organizations."+context._id.$id,
                            value : {
                                name : context.name,
                                type : context.type
                            } 
                        }
                        dataHelper.path2Value(dataToSendLinks, function (params) {});
                    }
                },
                null,
                "json",
                { async: false }  
            )	
            return dataId;
        };
        var renderForm = function (formId, answerId, mode="w") {
            var formAjaxPath = baseUrl + '/survey/answer/index/id/' + answerId + '/form/' + formId + '/mode/' + mode;
            var renderHtml = "";
            ajaxPost(
                null, 
                formAjaxPath,
                { 
                    url : window.location.href 
                },
                function(data){
                    renderHtml = data;
                },
                "html",
                null,
                { async: false } 
            );
            return renderHtml;
        }
        $(formContainer).html(renderForm(formId, formAnswerId(formId, name)));
    },

    afterInviteOrgaTeam: function(inv){
        mylog.log("after invite", inv)
        if(inv && typeof inv.listInvite != "undefined" && typeof inv.listInvite.citoyens!="undefined"){
            $.each(inv.listInvite.citoyens, function(ci, citoyen){
                links.connectAjax(costum.contextType, costum.contextId, ci, "citoyens", null, null, null);
            })
        }
    },

    getOrganizations: function(){
        let params = {
            "searchType":["organizations"],
            "notSourceKey":true,
            "fields": ["name"],
            "filters" : {
                "$or":{
                    "source.keys":costum.slug, 
                    "reference.costum":costum.slug,
                }
            },
            indexStep: 0
        };
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/search/globalautocomplete",
            params,
            function(data){
                if(data.results){
                    costum.lists["communities"] = {}
                    $.each(data.results, function(index, org){
                        costum.lists["communities"][index] = org.name;
                    })
                }
            },
            null,
            "json",
            {
                async: true
            }
        );
    },
    directoryEvent : function(keyE, valE){
		mylog.log("costum.essconnect.directoryEvent", keyE, valE);
		var now = moment().format("YYYY-MM-DD");
		var isSameOrAfter = moment( moment(valE.startDate).format("YYYY-MM-DD") ).isSameOrAfter(now);
		if( $("#journal").length && $("#journal").hasClass("active") )
			isSameOrAfter = true;
			
		mylog.log("costum.laRaffinerie3.directoryEvent isSameOrAfter", isSameOrAfter);
		var str = "";
		if ( isSameOrAfter === true ||  ( typeof valE.openingHours != "undefined" && valE.openingHours != null ) ) {
			var style = "background-color: #d0e0e35c;";
			// $.each(costum.paramsData.poles, function(kT, vT){
			// 	//mylog.log("directoryEvent poles", kT, vT);
			// 	if(typeof  valE.tags != "undefined" && 
			// 		 valE.tags != null &&
			// 		  valE.tags.length > 0 &&
			// 		  $.inArray(kT, valE.tags) > -1)
			// 		style = "background-color: "+vT.color+";";
			// });

			var colEvent = "col-xs-12 col-sm-6 ";
			var colEventImg = "col-xs-6 ";
			var colEventText = "col-xs-6 ";
			if( $("#journal").length && $("#journal").hasClass("active") ){
				
				colEvent = "col-xs-12 ";
				colEventImg = "col-sm-6 hidden-xs ";
				colEventText = "col-sm-6 col-xs-12 ";
			}

			str = "<div class='"+colEvent+" margin-bottom-10 text-center eventDirRaf' style='' >"+
					"<a href='#page.type.events.id."+keyE+"' class='lbh-preview-element col-xs-12 no-padding ' style='"+style+"'' >"+
						"<div class='"+colEventImg+" no-padding divImg'  style='height: 200px;overflow: hidden;background: black;display: flex;align-items: center;' >";
							if(typeof valE.imgMediumProfil != "undefined" && valE.imgMediumProfil != null)
								str+= valE.imgMediumProfil;
							else if(typeof valE.profilMediumImageUrl != "undefined" && valE.profilMediumImageUrl != null)
								str+= "<img src='"+baseUrl+valE.profilMediumImageUrl+"' style='width:100%'/>";
                            else if ('undefined' != typeof directory.costum && notNull(directory.costum)
                                && typeof directory.costum.results != 'undefined'
                                && typeof directory.costum.results.events != 'undefined'
                                && typeof directory.costum.results.events.defaultImg != 'undefined'){
                                //defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+assetPath+directory.costum.results[params.type].defaultImg+'\'/>';
                                str += "<img src='"+ assetPath + directory.costum.results.events.defaultImg + "' style='width:100%' />";
                            }    
						str+="</div>"+
						"<div class='"+colEventText+" eventDirText' style='height: 200px;overflow:hidden;' >"+
							// "<div style='width:100%;text-overflow: ellipsis;white-space: nowrap;'>"+
								"<span style='text-transform: capitalize; font-size: 22px; ' class=' titleFont1 col-xs-12'>";
									var secStart = "";									
									if(typeof valE.startDateSec != "undefined" && valE.startDateSec != null)
										secStart = valE.startDateSec;
									else if(typeof valE.startDate != "undefined" && valE.startDate != null &&
											typeof valE.startDate.sec != "undefined" && valE.startDate.sec != null)
										secStart = valE.startDate.sec;

									if(secStart != ""){
										str+=	moment(secStart*1000).locale("fr").format("ddd DD.MM.YY")+"<br/>"+
		    								moment(secStart*1000).locale("fr").format("HH:mm");
									}
	    						str+=	"</span>"+
	    						"<span style='font-size: 22px;  color : black' class='titleFont2 col-xs-12 no-padding'>"+
	    							valE.name+
	    						"</span>"+
	    					// "</div>"+
						"</div>"+
					"</a>"+
				"</div>";
		}
		return str;
	},
    dashboard : function(params){ // Logo listing design
		mylog.log("dashboard", "Params", params);
		let str = '';
		let imageUrl = baseUrl + moduleUrl + "/images/thumbnail-default.jpg";
		if (typeof params.profilMediumImageUrl !== "undefined" && params.profilMediumImageUrl !== "") {
			imageUrl = baseUrl + params.profilMediumImageUrl;
		}
		str += '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item">'+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>';
			
		if(params.collection == "citoyens"){
			str = '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item tooltips" data-emplacement = "center" data-toggle = "tooltip" data-original-title="'+params.name+'"> '+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>'+
			'</div>';
        }
        if(notEmpty(params.answers)){
            str='<div id="answersDirectory" class="col-xs-12">'
            $.each(params.answers,function(id,ans){
                if(Object.keys(costum.lists.forms).indexOf(ans.form) >-1){
                    str+='<a href="#answer.answer.id.new.form.'+ans.form+'.view.slide" class="btn btn-defaults">'+costum.lists.forms[ans.form]["label"]+'</a>';
                }
            })
            str+='</div>'
        }
        str+='</div>';

		return str;
	}
}

costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="isEthicCharte"){
            $("#ajax-modal .ethicCharteselectMultiple").show();
        }
    },
    false : function(id){
        if(id=="isEthicCharte"){
            $("#ajax-modal .ethicCharteselectMultiple").hide();
        }
    }
}

// User register form modal
Login.runRegisterValidator  = function(params) { 
    var form4 = $('.form-register');
    var errorHandler3 = $('.errorHandler', form4);
    var createBtn = null;
    $(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");
    $("#modalRegister label[for='charte']").remove();

    if($("#fonction").length==0){
        $("#modalRegister .form-register-inputs div:eq(5)").append(`
            <div class="telephoneRegister">
                <label class="letter-black"><i class="fa fa-phone"></i> Téléphone </label><br>
                <input class="form-control" id="telephone" name="telephone" placeholder="numéro téléphone"><br/>
            </div>

            <div class="fonctionRegister">
                <label class="letter-black"><i class="fa fa-chevron-down"></i> Précisez votre fonction actuelle </label><br>
                <input class="form-control" id="fonction" name="fonction" placeholder="votre fonction"/>
                <br/>
            </div>
        `);
    }
    // <select class="form-control" id="fonction" name="fonction" placeholder="Choisissez votre fonction">
    //                 <option value="transitionCitizen">Un citoyen investi dans la transition de la Guadeloupe</option>
    //                 <option value="thirdPlaceMember">Membre d'un tiers-lieu</option>
    //              </select>   
    
    form4.validate({
        rules : {
            name : {
                required : true,
                minlength : 4
            },
            telephone : {},
            fonction : {},
            username : {
                required : true,
                validUserName : true,
                rangelength : [4, 32]
            },
            email3 : {
                required : { 
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email : true,
            },
            password3 : {
                minlength : 8,
                required : true
            },
            passwordAgain : {
                equalTo : "#password3",
                required : true
            }
        },

        submitHandler : function(form) { 
            if(!$("#agree").is(":checked")){
                var validator = $('.form-register').validate();
                validator.showErrors({
                    "agree": `<div class="margin-left-10">${trad["mustacceptCGU"]}</div>`
                });
                //$("#agree").attr("checked", false);
                //$("#agree").change();
                return false;
            }
            // if(!$("#charte").is(":checked")){
            //     var validator = $( '.form-register' ).validate();
            //     validator.showErrors({
            //         "charte": `<div class="margin-left-10">${trad["mustacceptCGU"]}</div>`
            //     });
            //     //$("#charte").attr("checked", false);
            //     //$("#charte").change();
            //     return false;
            // }
            errorHandler3.hide();
            $(".createBtn").prop('disabled', true);
            $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
            var params = { 
            "name" : $('.form-register #registerName').val(),
            "username" : $('.form-register #username').val(),
            "email" : $(".form-register #email3").val(),
            "fonction" : $(".form-register #fonction").val(),
            "telephone" : $(".form-register #telephone").val(),
            "pendingUserId" : pendingUserId,
            "pwd" : $(".form-register #password3").val()
            };
            if($('.form-register #isInvitation').val()=='true' || $('.form-register #isInvitation').val()==true)
                params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
                params.scope = scopeObj.selected;
            }
            if($(".form-register #newsletter").is(":checked"))
                params.newsletter=true;
            if($(".form-register #newsletterCollectif").is(":checked"))
                params.newsletterCollectif=true;
            var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
            
            //if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
            //	params.msgGroup=$(".form-register #textMsgGroup").val();
            ajaxPost(
                null,
                baseUrl+"/costum/essconnect9711/register",
                params,
                function(data){ 
                    if(data.result) {
                        mylog.log("Register Formulaire",data.result, data, params);
                        toastr.success("Merci, votre compte a bien été créé");
                        // return;
                        $('.modal').modal('hide');
                        if(typeof data.isInvitation!="undefined" && data.isInvitation){
                            window.location.href = baseUrl+window.location.pathname;
                        }else{
                            $("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                            $("#modalRegisterSuccess").modal("show"); 
                            
                            $('#modalRegisterSuccess .btn-default').click(function() {
                                mylog.log("hide modale and reload");
                                $('.modal').modal('hide');
                                if(typeof thisElement == "undefined"){
                                    window.location.reload();
                                }
                            });
                            // COnnect new user to Lakou as a member
                            // if(notEmpty(data.id)){
                            //     links.connectAjax(costum.contextType, costum.contextId, data.id, "citoyens", "member", null, function(){},"/ref/673f24b836b9e");
                            // }    
                        }
                    }else {
                        $('.modal').modal('hide');
                        if(data.msg="Problème à l'insertion du nouvel utilisateur : une personne avec cet mail existe déjà sur la plateforme"){
                            var dialogExistEmail=bootbox.dialog({
                                size:"medium",
                                title: "Un compte utilise déjà cette adresse email",
                                message : "Il semblerait que vous ayez déjà un compte avec cette adresse mail.",
                                buttons : {
                                    login : {
                                        "label" : "Me connecter",
                                        "className" : "btn btn-primary",
                                        "callback" : function(){
                                             dialogExistEmail.hide();
                                            Login.openLogin();
                                         }
                                    },
                                    password :{ 
                                        "label" : "J'ai oublié mon mot de passe",
                                        "callback" : function(){
                                             dialogExistEmail.hide();
                                            $("#modalForgot").modal('show');
                                         }   
                                    }   
                                   
                                }   
                            });
                        }
                        else{
                            toastr.error(data.msg);
                        }    

                        	    		  	
                        scopeObj.selected={};
                    }
                },function(data) {
                    toastr.error(trad["somethingwentwrong"]);
                    $(".createBtn").prop('disabled', false);
                    $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                }
            );
            return false;
        },
        invalidHandler : function(event, validator) {//display error alert on form submit
            errorHandler3.show();
            $(".createBtn").prop('disabled', false);
            $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
        }
    });
};
Login.runRegisterValidator();


// var finderOrga={
//     inputType:"custom",
//     html : '<div id="finderSelectHtml" style="max-height:480px;"><input class="form-group form-control padding-5" type="text" id="populateFinder" placeholder="Saisir le nom d\'une structure"/><div id="list-my-context" class="padding-5" style="max-height:380px; overflow-y:auto; border: 1px solid #eee"><div id="list-loader"></div></div><div class="col-md-12 col-sm-12 col-xs-12 text-center"></div></div>'
// }
// var customForm={
//     jsonSchema : {
//         title : "rechercher",
//         properties : {
//             "finderOrga":finderOrga
//         },
//         onLoads : {
//             onload : function(){
//                 $("#populateFinder").keyup(function(){
//                     coInterface.showLoader("#list-my-context");
//                     mylog.log("finder.showPanel keyup");
//                     if($(this).val().length > 3){
//                         // finder.filterPopulation($(this).val());
//                         let params = {
//                             "searchType":["organizations"],
//                             "notSourceKey":true,
//                             "name" : $(this).val(),
//                             "filters" : {
//                                 "$or":{
//                                     "source.keys":costum.slug, 
//                                     "reference.costum":costum.slug
//                                 }
//                             }
//                         };
//                         // params.filters["links.members."+userId] = {'$exists':true};
//                         ajaxPost(
//                             null,
//                             baseUrl+"/"+moduleId+"/search/globalautocomplete",
//                             params,
//                             function(data){
//                                 mylog.log("Here we go modal", data);
//                                 $("#list-my-context").empty();
//                                 var results=data.results;
//                                 let img = modules.co2.url + "/images/thumb/default_citoyens.png";
//                                 // if user admin add ess Connect object in orga to choose
//                                 if(costum.isCostumAdmin){
//                                     var contextObj={
//                                         name : costum.title,
//                                         type : "Group",
//                                         collection : costum.contextType,
//                                         profilMediumImageUrl : costum.metaImg
//                                     };
//                                     results[costum.contextId]=contextObj;
//                                     mylog.log("add ctxt obj",data.results);
//                                 }
        
//                                 $.each(results, function(index, item){
//                                     if(item.profilMediumImageUrl){
//                                         img = item.profilMediumImageUrl;
//                                     }
//                                     var str ="<div class='population-elt-finder population-elt-finder-"+index+" col-xs-12 padding-5' style='border-bottom:1px solid #eee' data-id='"+index+"' data-type='"+item.collection+"' data-img='"+img+"' data-name='"+item.name+"'>"+
//                                         "<div class='element-finder element-finder-"+index+"'>"+
//                                             '<img src="'+ img +'" class="thumb-send-to pull-left img-circle" height="50" width="50">'+
//                                             '<span class="info-contact pull-left margin-left-20">' +
//                                                 '<span class="name-element text-dark text-bold" data-id="'+index+'">' + item.name + '</span>'+
//                                                 '<br/>'+
//                                                 '<span class="type-element text-light pull-left">' + trad[item.type]+ '</span>'+
//                                             '</span>' +
//                                         "</div>"+
//                                     "</div>";
//                                     $("#list-my-context").append(str);
//                                 });
//                                 dialog.find(".population-elt-finder").on("click", function(e){
//                                     dialog.modal("hide");
//                                     costum[costum.slug].openInModal(url, type, {id:$(this).data("id"), type:$(this).data("type"), name:$(this).data("name"), img:$(this).data("img")});
//                                 })
//                                 if($(".population-elt-finder").length==1){
//                                     $(".population-elt-finder").click();
//                                 }else if($(".population-elt-finder").length==0){
//                                     $("#list-my-context").append("<h3 class=''>Aucun résultat n'a été trouvé.</h3><br/><a href='javascript:;' onclick='costum[costum.slug].chooseOrgaType();$(`#finderOrga`).modal(`hide`);' class='orga-choice-modal btn btn-primary-ekitia btn-primary-outline-ekitia'><i class='fa fa-plus margin-right-5'></i> Ajouter une structure</a>");
//                                     $(".bootbox div.modal-header").hide();
//                                 }

//                                 $(".bootbox bootbox-body").addClass("margin-right-15 margin-left-15");
//                             },
//                             null,
//                             "json",
//                             {
//                                 async: true
//                             }
//                         );
//                     }else{
//                         $("#list-my-context").empty().html("<p>Veuillez saisir plus de 3 caractères pour la recherche.</p>");
//                     }
//                     // else{
//                     //     if(notNull(open) && open){
//                     //         finder.filterPopulation($(this).val());
//                     //         finder.searchAndPopulateFinder(keyForm,$(this).val(), typeSearch, multiple,field);
//                     //     }else{
//                     //         finder.filterPopulation($(this).val());
//                     //     }
//                     // }
//                 });
//             }
//         }
//     }
// }
// dyFObj.openForm(customForm,null,null,null,null,{type:"bootbox"});