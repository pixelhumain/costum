$(function() { 
    var memberElement = {
        init: function () {
            this.event.init();
            this.action.init();
        },
        event : {
            init() {
                memberElement.event.menuTopEvent();
                memberElement.event.changeCommunity();
                memberElement.event.hideMainBtn();
            },
            menuTopEvent : function () {
                $(".link-circle.text-center.tooltips").remove();
                $("span.label-link").removeAttr("class","");
            },
            changeCommunity: function () {
                // const checkBtnCollection = () => {
                //     if (jsonHelper.pathExists("elementType")) {
                //         clearInterval(checkInterval);
                //         if (elementType == "citoyens" && $("#btn-element-community").length > 0) {
                            var titleName = "COLLABORATEURS"
                            var titleHtml = `<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">${titleName}</span><span>${titleName}</span>`;
                            $("#menu-top-profil-social #btn-element-community").html(titleHtml);
                //         }
                //     }
                // }
                // const checkInterval = setInterval(checkBtnCollection, 50);
            },
            hideMainBtn: function () {
                $(".elementButtonHtml[data-dir='projects']").hide();
                $(".elementButtonHtml[data-view='agenda']").hide();
            }
        },
        action : {
            init: function () {
                memberElement.action.deleteEmptyBtnTooltips();
                memberElement.action.changeDetailRender();
            },
            deleteEmptyBtnTooltips () {
                if ($("a.ssmla.btn-menu-tooltips").length > 0) {
                    for (let index = 0; index < $("a.ssmla.btn-menu-tooltips").length; index++) {
                        if (!notEmpty($($("a.ssmla.btn-menu-tooltips")[index]).find("span").html())) {
                            $($("a.ssmla.btn-menu-tooltips")[index]).remove();
                        }
                    }
                }
            },
            changeDetailRender : function() {
                pageProfil.views.detail = function(){
                    var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
                    if(contextData.type=="organizations") url+="/view/costum.views.custom.essConnect9711.element.detail";

                    if(contextData.type!="citoyens"){
                        $("#menu-top-btn-group").show();
                        $("#central-container").show();
                        ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");
                    }else{
                        $("#menu-top-btn-group").hide();
                        $("#central-container").hide();
                        if(userId && typeof contextData !="undefined" && contextData.slug){
                            urlCtrl.loadByHash("#@"+contextData.slug+".view.settings")
                        }
                    }
                };
            }
        }
    }
    memberElement.init();

    pageProfil.directory.init = function(){
        pageProfil.directory.initView("#central-container");

        if(pageProfil.params.dir == "externalNetwork"){
            pageProfil.views.externalNetwork();
            return true;
        }
        // delete pageProfil.directory.communityLinks[pageProfil.params.dir].types;
        var contextDataByLink=pageProfil.directory.communityLinks[pageProfil.params.dir];
        var paramsFilter= {
             container : "#filterContainer",
             header : {
                dom : ".headerSearchIncommunity",
                options : {
                    left : {
                        classes : 'col-xs-4 elipsis no-padding',
                        group:{
                            count : true
                        }
                    },
                    right : {
                        classes : 'col-xs-8 text-right no-padding',
                        group : {
                            graph : true,
                        }
                    }
                }
             },
             defaults : {
                 notSourceKey : true,
                 types : contextDataByLink.types,
                 forced:{
                     filters:{

                     }
                 },
                 filters:{
                 }
            },
            results : {
                 smartGrid : true,
                 renderView : "directory."+(exists(contextDataByLink.render) ? contextDataByLink.render : pageProfil.renderView),
                 community : {
                     links : (typeof contextData.links != "undefined") ? contextData.links : null,
                     connectType : pageProfil.params.dir,
                     edit : canEdit
                 }
             },
             filters : {
                 text : true
             }
        };

        if(!((typeof openEdition != "undefined" && openEdition) && canEdit) ){
            paramsFilter.defaults.forced.filters[`preferences.toBeValidated.${contextData.slug}`] = {'$exists': false};
        }

        // Si canSee on voit les elt privée car on appartient à la communauté
        if(canParticipate){
            paramsFilter.urlData = baseUrl+"/co2/search/globalautocompleteadmin/type/"+contextData.collection+"/id/"+contextData.id+"/canSee/true";
        }

        if(userConnected!=null && contextData!=null &&
            typeof userConnected.links != "undefined" && 
            typeof userConnected.links.memberOf!="undefined" && 
            (
                (typeof userConnected.links.memberOf[costum.contextId]!="undefined" && typeof userConnected.links.memberOf[costum.contextId].isAdmin!="undefined")
                || 
                (typeof userConnected.links.memberOf[contextData.id]!="undefined" && (typeof userConnected.links.memberOf[contextData.id].isAdmin!="undefined" || (typeof userConnected.links.memberOf[contextData.id].roles!="undefined" && userConnected.links.memberOf[contextData.id].roles.includes("Administrateur"))))
            )){
                paramsFilter.header.options.right.group["kanban"] = true

                paramsFilter.header.options.right.group["invite"] = {
                    contextType : contextData.type,
                    contextId : contextData.id,
                    label : "Inviter"
                }
        }

        if(typeof contextDataByLink.links != "undefined"){
            var linksConnectRequest = (typeof contextDataByLink.links == "string") ? contextDataByLink.links : contextDataByLink.links[contextData.collection];
        }
        if(typeof linksConnectRequest != "undefined" 
            && typeof contextDataByLink.parent != "undefined"){
            paramsFilter.defaults.forced.filters["$or"]={};
            paramsFilter.defaults.forced.filters["$or"]["links."+linksConnectRequest+"."+contextData.id]={'$exists' :true};
            paramsFilter.defaults.forced.filters["$or"]["parent."+contextData.id] = {'$exists' :true};
        }

        if(typeof contextDataByLink.organizer != "undefined"){
            paramsFilter.defaults.forced.filters["$or"]["organizer."+contextData.id] = {'$exists' :true};
        }
        else if(typeof linksConnectRequest != "undefined"){
            paramsFilter.defaults.forced.filters["links."+linksConnectRequest+"."+contextData.id] = {'$exists' :true};
            if( (typeof openEdition != "undefined" && !openEdition) && !canEdit ){
                paramsFilter.defaults.forced.filters["links."+linksConnectRequest+"."+contextData.id+".toBeValidated"]= {'$exists' : false};
                paramsFilter.defaults.forced.filters["links."+linksConnectRequest+"."+contextData.id+".isInviting"] = {'$exists' : false};
            }
        }else if(typeof contextDataByLink.parent != "undefined"){
            paramsFilter.defaults.forced.filters["parent."+contextData.id] = {'$exists' :true};
        }


        // -- specific for crowdfunding 
        // changes searchType and forced filter to find crowdfunding campaign among children projects

        if(pageProfil.params.dir=="crowdfunding"){
            paramsFilter.defaults.forced.filters.type="campaign";
            if(contextData.type!="projects" && typeof(contextData.links.projects)!="undefined" && Object.keys(contextData.links.projects).length>0){
                //contextDataByLink.types=["projects"];
                paramsFilter.defaults.types=["projects"];
                paramsFilter.defaults.forced.filters["preferences.crowdfunding"] = {'$exists' : true};
                delete paramsFilter.defaults.forced.filters.type;
            }

            
        } 

        // -- end crowdfunding

        if(typeof contextDataByLink.header != "undefined"){
            if(typeof contextDataByLink.header.options != "undefined"){
                paramsFilter.header.options=contextDataByLink.header.options;
                if(contextData.type=="projects" && typeof contextData.counts!="undefined" && typeof contextData.counts.crowdfunding!="undefined" && contextData.counts.crowdfunding<1){
                    paramsFilter.header.options.map=false;
                }	
            }

        }




        // if(contextDataByLink.types.length > 1){
        //     paramsFilter.filters.types = {
        //          lists : contextDataByLink.types
        //      };
        // }
        if(typeof contextDataByLink.loadEvent != "undefined"){
            paramsFilter.loadEvent=contextDataByLink.loadEvent;
        }
        // if(typeof contextDataByLink.type != "undefined"){
        //     paramsFilter.defaults.forced.filters["type"]=contextDataByLink.type;
        // }

        if(typeof contextDataByLink.urlData != "undefined")
            paramsFilter.urlData =contextDataByLink.urlData;
        if(typeof contextDataByLink.filters != "undefined"){
            if(typeof contextDataByLink.filters == "object" && !Array.isArray(contextDataByLink.filters)){
                if(typeof contextDataByLink.initList != "undefined"){
                    $.each(contextDataByLink.initList, function(e,v){
                        contextDataByLink.filters[v].list=contextListFilter[v];
                    });
                    paramsFilter.filters=$.extend( contextDataByLink.filters , paramsFilter.filters );
                }else
                    $.extend( paramsFilter.filters, contextDataByLink.filters );
            }
            else{
                if($.inArray("roles", contextDataByLink.filters) >= 0){
                    paramsFilter.filters["roles"] = {
                        view : "horizontalList",
                        type : "filters",
                        dom : "#listRoles",
                        field :"links."+linksConnectRequest+"."+contextData.id+".roles",
                        name : "<i class='fa fa-filter'></i> "+trad.sortbyrole+":",
                        active : true,
                           typeList : "object",
                        event : "inArray",
                        classList : "pull-left favElBtn btn", 
                        list :  contextData.rolesLists
                    }
                    //$("#central-container #listRoles").hide();
                }
                if($.inArray( "status", contextDataByLink.filters) >= 0){
                    paramsFilter.filters["status"] = {
                        view : "dropdownList",
                        type : "filters",
                          name : "Statuts",
                        action : "filters",
                        typeList : "object",
                        event : "exists",
                        list :  {
                            "admin" : {
                                label: trad["administrator"],
                                field : "links."+linksConnectRequest+"."+contextData.id+".isAdmin",
                                value : true,
                                count : (exists(contextData.counts) && exists(contextData.counts.admin)) ? contextData.counts.admin : 0 
                            },
                            "members" : {
                                label: trad[pageProfil.params.dir+"Active"],
                                field : "links."+linksConnectRequest+"."+contextData.id+".isInviting&&links."+linksConnectRequest+"."+contextData.id+".toBeValidated",
                                value : false,
                                count : (exists(contextData.counts) && exists(contextData.counts[pageProfil.params.dir+"Active"])) ? contextData.counts[pageProfil.params.dir+"Active"]: 0 
                            }
                        }
                    }
                    if( (typeof openEdition != "undefined" && openEdition) || canEdit ){
                        paramsFilter.filters["status"]["list"]["isInviting"] = {
                            label : trad["unconfirmedinvitation"],
                            field : "links."+linksConnectRequest+"."+contextData.id+".isInviting",
                            value : true,
                            count : (exists(contextData.counts) && exists(contextData.counts.isInviting)) ? contextData.counts.isInviting : 0 
                        };
                        paramsFilter.filters["status"]["list"]["toBeValidated"] = {
                            label:trad["waitingValidation"],
                            field : "links."+linksConnectRequest+"."+contextData.id+".toBeValidated",
                            value : true,
                            count : (exists(contextData.counts) && exists(contextData.counts.toBeValidated)) ? contextData.counts.toBeValidated : 0 
                        };
                    }
                }
            }

        }
        filterGroup = searchObj.init(paramsFilter);
        filterGroup.search.init(filterGroup);
        pageProfil.directory.initEvents();
    };

    pageProfil.directory.communityMenu.organizations = {}

})

