
adminPanel.initAddButton = function(domContain){
    var menuButtonCreate="";
    let hexColorRegex = /#([0-9A-Fa-f]{6}|[0-9A-Fa-f]{3})/g;
    let context = "";
    let type = "";
    $.each(typeObj, function(e,v){
        let colorTest = hexColorRegex.test(v.color);
        if(typeof v.add != "undefined" && v.add){
            var hash=(typeof v.hash !== "undefined") ? v.hash : "javascript:;";
            var formType=(typeof v.formType != "undefined") ? 'data-form-type="'+v.formType+'" ' : "";
            var subFormType= (typeof v.formSubType != "undefined") ? 'data-form-subtype="'+v.formSubType+'" ' : "";
            var addClass = (typeof v.class !== "undefined") ? v.class : "";
            var nameLabel=(typeof v.addLabel!= "undefined") ? v.addLabel : v.name;
            var actionClass = (typeof v.optionModal!= "undefined") ? "btn-open-optionModal"  : 'btn-open-form';
            var colorClass = (!colorTest) ? 'text-'+v.color : "";
            var colorStyle = (colorTest) ? "style='color: "+v.color+"'" : "";
            if (hash !== "javascript:;" && hash.includes("invite")) {
                if (jsonHelper.pathExists("costum.typeObj")) {
                    Object.keys(costum.typeObj).map(function(key, value) {
                        if (typeObj.get(key).ctrl === "person") {
                            colorStyle = (jsonHelper.pathExists("costum.typeObj."+key+".color")) ? "style='color: "+costum.typeObj[key].color+"'" : "style='color: #ea4335'";
                            v.icon = (jsonHelper.pathExists("costum.typeObj."+key+".icon")) ? costum.typeObj[key].icon : "plus-circle";
                        }
                    })
                }
                context = (typeof contextId !== "undefined") ? contextId : (jsonHelper.pathExists("costum.contextId")) ? costum.contextId : "";
                type = (typeof contextType !== "undefined") ? contextType : (jsonHelper.pathExists("costum.contextType")) ? costum.contextType : "";
                let hrefLink = hash;
                if (context !== "" && type !== "") {
                    hrefLink = v.hash+'.type.'+type+'.id.'+context;
                }
                menuButtonCreate+='<a '+colorStyle+' href="'+hrefLink+'"'+
                    'class="btn ekitia-btn-link '+addClass+' col-xs-6 col-sm-6 col-md-4 col-lg-4  margin-bottom-10">'+
                    '<h6><i class="fa fa-'+v.icon+'"></i><br/>'+
                    nameLabel+'</h6>'+
                    '</a>';
            } else {
                menuButtonCreate+='<button '+
                    formType+
                    subFormType+
                    'class="btn btn-link ekitia-btn-link '+actionClass+' col-xs-12 col-sm-6 col-md-4 col-lg-4 margin-bottom-10">'+
                    '<h6><i class="fa fa-'+v.icon+'"></i><br/>'+
                    nameLabel+'</h6>'+
                    '</button>';
            }
        }
    });
    $(domContain).html(menuButtonCreate);

    links.linksTypesModif=links.linksTypes;
    links.linksTypesModif.organizations.organizations = "memberOf";
    // if(!notEmpty(pageProfil)){
    //     lazyLoad( modules.co2.url+'/js/default/profilSocial.js', null , function(data){});
    // }
    
}
adminPanel.bindViewActionEvent = function(){
    $(".btnNavAdmin").off().on("click", function(){
        $(".ekitia-admin-menu-item").removeClass("active");
        $(this).parents(".ekitia-admin-menu-item").addClass("active");
        adminPanel.params.view = $(this).data("view");
        adminPanel.params.dir = $(this).data("dir");
        if($(".ekitia-menu-xs").is(":visible")){
            $(".ekitia-admin-menu").removeClass("active");
        }
        if($(this).data('page')){
            adminPanel.params.page = $(this).data('page');
        }
        if(!notNull($(this).data("action"))){
            onchangeClick=false;
            adminPanel.initNav();
            var hashAdmin=adminPanel.params.hashUrl;
            var indexView=(notEmpty(adminPanel.params.subView)) ? "subview" : "view";
            if(notEmpty(adminPanel.params.view) && adminPanel.params.view!="index") hashAdmin+="."+indexView+"."+$(this).data("view");
            location.hash=hashAdmin;
            if(adminPanel.params.view == "index")
                $(".ekitia-menu-xs").removeClass("menu-top").addClass("menu-left");
            else
                $(".ekitia-menu-xs").removeClass("menu-left").addClass("menu-top");
        }
        //Get view admin
        //Opens the page
        adminPanel.views[adminPanel.params.view]();
    });
    adminDirectory.events.validated = function(){
        $(".validateSourceBtn").off().on("click", function(event){
            event.stopImmediatePropagation();
            $(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
            var $this=$(this);
            var params={
                id:$(this).data("id"),
                type:$(this).data("type"),
                valid: $(this).data("valid")
            };
            ajaxPost(
                null,
                baseUrl+'/costum/ekitia/validategroup',
                params,
                function(data){
                    if ( data && data.result ) {
                        successToast= params.valid ? "Structure acceptée comme membre du réseau Lakou" : "Structure supprimée du réseau Lakou";
                        successMsg=params.valid ? "Structure validée" : "Structure en attente de validation";
                        toastr[params.valid ? "success" : "warning"](successToast);
                        // $this.remove();
                        $this.parent().html(adminDirectory.actions.validated(data.elt, params.id, params.type));
                        adminDirectory.events.validated();
                        $("#"+$this.data("type")+$this.data("id")+" .validated").html(adminDirectory.values.validated(data.elt));
                        urlCtrl.loadByHash(location.hash);
                    } else {
                        toastr.error("Un problème est survenu lors de la validation");
                    }
                }
            );
        });
    }
}

adminPanel.views.projectManagement = function(){
    ajaxPost('#content-view-admin', baseUrl+"/co2/app/view/url/costum.views.custom.kanban.kanban-gestion-orga",{"context":{}, "options":''}, 
        function(){
            setTimeout(function(){
                $("#project-kanban").trigger("click");

            },500);
        },    
        null,null,{async:false});
};

adminPanel.views.reference = function(){
    var searchAdminType=(typeof paramsAdmin != "undefined"
        && typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["reference"] != "undefined"
        && typeof paramsAdmin["menu"]["reference"]["initType"] != "undefined") ? paramsAdmin["menu"]["reference"]["initType"]: ["organizations", "events", "projects"];

    var filterAdmin = (typeof paramsAdmin != "undefined"
        && typeof paramsAdmin["menu"] != "undefined"
        && typeof paramsAdmin["menu"]["reference"] != "undefined"
        && typeof paramsAdmin["menu"]["reference"]["filters"] != "undefined") ? paramsAdmin["menu"]["reference"]["filters"]: [];
    var data={initType:searchAdminType,filters:filterAdmin, page: "costum.views.custom.essConnect9711.admin.reference"};

    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/reference', data, function(){});

}
adminPanel.views.group = function(){
    var data={
        title : "Validation des structures",
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations"]
            },
            filters : {
                text : true

            }
        },
        table : {
            name: {
                name : "Nom"
            },
            validated : {
                name : "Valider",
                class : "col-xs-2 text-center"
            },
            actions : {
                class : "col-xs-3 text-center"
            }
        },
        actions : {
            validated : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.community = function() {
    var data = {
        title : "Gestion des Utilisateurs",
        context:{
            id: costum.contextId,
            collection: costum.contextType
        },
        invite : {
            contextId : costum.contextId,
            contextType : costum.contextType,
            class: "btn-primary-outline-ekitia",
            label: " Inviter des membres"
        },
        // kanban : {
        //     label: "Gérer les rôles",
        //     class: "btn-primary-ekitia"
        // },
        community : {
            contextId : costum.contextId,
            contextType : costum.contextType,
            contextSlug : costum.contextSlug
        },
        table : {
            name: {
                name : "Membres"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Statut de l'invitation pour être membre",
                class : "col-xs-2 text-center"
            },
            created : {
                name : "Inscrit.e le",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : ["citoyens"],
                fields : [ "name", "email", "links", "collection","roles", "created" ],
                indexStep: 50,
                notSourceKey: true,
                sort : { created : -1 }
            },
            filters : {
                text : true
            }
        },
        actions : {
            admin : true,
            addAsMember : true,
            roles : true,
            disconnect : true
        }
    };
    data.paramsFilter.defaults.filters={};
    data.paramsFilter.defaults.filters['$or']={};
    data.paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    data.paramsFilter.defaults.filters['$or']["source.keys"]=costum.slug; 
    data.paramsFilter.defaults.filters['$or']["reference.costum"]=costum.slug;

    // data.paramsFilter.defaults.forced={filters:{}};
    // data.paramsFilter.defaults.forced.filters["links.memberOf."+costum.contextId] = {'$exists':true};

    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){
        $(".headerSearchright").addClass("col-md-6");
        $(".headerSearchleft").addClass("col-md-6");
    },"html");
};

adminPanel.views.tobevalidated = function(){
    var data = {
        title: "Validation de(s) Utilisateur(s)",
        table: {
            name: {
                name: "Nom"
            },
            mobile: {
                name: "Téléphone"
            },
            email: {
                name: "E-mail"
            },
            address: {
                name: "Adresse"
            },
            status : {
                name : "Statut"
            }/*,
            structure: {
                name: "Membre de"
            }*/
        },
        paramsFilter: {
            container: "#filterContainer",
            defaults: {
                types: ["citoyens"],
                fields: ["name", "updated", "address", "links","mobile", "email", "tags", "collection"],
                notSourceKey: true
            },
            filters: {
                text: true,
            }
        },
        actions : {
            accept : true,
            delete : true,
            sendMail : true
        }
    };
    
    data.paramsFilter.defaults.forced={filters:{}};
    data.paramsFilter.defaults.forced.filters["links.memberOf."+costum.contextId+".type"] = costum.contextType;
    data.paramsFilter.defaults.forced.filters["links.memberOf."+costum.contextId+".isInviting"] = true;

    ajaxPost('#content-view-admin', baseUrl + '/' + moduleId + '/admin/directory/', data, function() {

    }, "html");
}

adminPanel.views.organizations = function() {
    var data = {
        title: "Gestion des structures",
        context : {
            id : costum.contextId,
            collection : costum.contextType
        },
        kanban : {
            label: "Gérer les rôles",
            class: "btn-primery-ekitia"
        },
        table: {
            name: {
                name: "Dénomination",
                sort : {
                	field : "name",
                	up : true,
                	down : true
                }
            },
            mobile: {
                name: "Téléphone"
            },
            email: {
                name: "E-mail"
            },
            responsable: {
                name: "Responsable",
                sort : {
                	field : "contacts.firstname",
                	up : true,
                	down : true
                }
            },
            address: {
                name: "Adresse"
            },
            validated : {
                name : "Validée",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Membre du réseau",
                class : "col-xs-2 text-center"
            }
        },
        paramsFilter: {
            container: "#filterContainer",
            defaults: {
                types: ["organizations"],
                fields: ["category","name", "updated", "address", "links", "preferences", "contacts", "email","tags","toBeValidated","collection","source","reference"]
            },
            filters: {
                text: true,
            }
        },
        actions : {
            // accept : true,
            delete : true,
            sendMail : true,
            // addAsMember : true,
            validated : true
        }
    };
    data.paramsFilter.defaults.filters={};
    data.paramsFilter.defaults.filters['_id']={};
    data.paramsFilter.defaults.filters['_id']['$nin']=[costum.contextId];
    ajaxPost('#content-view-admin', baseUrl + '/' + moduleId + '/admin/directory/', data, function() {

    }, "html");
};

adminDirectory.values.validated = function(e, id, type, aObj){
    var isValidated=( typeof e.preferences != "undefined" && typeof e.preferences.toBeValidated != "undefined" && typeof e.preferences.toBeValidated[costum.slug] != "undefined" && e.preferences.toBeValidated[costum.slug] == true) ? false : true;
    var str = "";
    if(!isValidated)
        str = "<span class='badge bg-orange text-wrap-ekitia'><i class='fa fa-pause'></i> Structure en attente de validation</span>";
    else
        str = "<span class='badge bg-green-k text-wrap-ekitia'><i class='fa fa-check'></i> Structure validée</span>";
    return str;
}

adminDirectory.actions.validated = function(e, id, type, aObj){
    var str = "" ;
    var isValidated=( typeof e.preferences != "undefined" && typeof e.preferences.toBeValidated != "undefined" && typeof e.preferences.toBeValidated[costum.slug] != "undefined" && e.preferences.toBeValidated[costum.slug] == true) ? false : true;
    if(!isValidated){
        str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="true" class="col-xs-12 validateSourceBtn btn bg-green-k text-white text-wrap-ekitia"><i class="fa fa-check"></i> Valider la structure</button>';
    } else {
        str ='<button data-id="'+id+'" data-type="'+type+'" data-valid="false" class="col-xs-12 validateSourceBtn btn bg-red-k text-white text-wrap-ekitia"><i class="fa fa-trash"></i> Supprimer la validation de la structure</button>';
    }
    return str ;
}

adminDirectory.setSourceDataAction  = function(aObj, $btnClick){
    var action=$btnClick.data("action");
    var setKey=$btnClick.data("setkey");
    var params={
        id:$btnClick.data("id"),
        type:$btnClick.data("type")
    };
    if(typeof costum != "undefined" && notNull(costum)){
        params.origin="costum";
        params.sourceKey=(typeof costum.isTemplate !="undefined" && costum.isTemplate) ? costum.contextSlug : costum.slug;
    }
    ajaxPost(
        null,
        baseUrl+"/costum/ekitia/setsource/action/"+action+"/set/"+setKey,
        params,
        function(data){
            if ( data && data.result ) {
                toastr.success(data.msg);
                $("#"+params.type+params.id).fadeOut();
                countB=parseInt($("#admin-count-"+setKey).text());
                if(action=="remove")
                    countB--;
                else
                    countB++;
                $("#admin-count-"+setKey).text(countB);
                //window.location.href = baseUrl+"/"+moduleId;
            } else {
                toastr.error("something went wrong!! please try again.");
            }

        }
    );
}

adminDirectory.values.modified = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.modified != "undefined") { 
        if(typeof e.modified == "number"){
            var date = directory.decomposeDate(e.modified, "dayNumber-month-year",true); 
            str += date;
        }else if(typeof e.modified == "object"){
            var date = directory.decomposeDate(e.modified.sec, "dayNumber-month-year",true);
            str += date;
        }
    }else if(typeof e.created != "undefined"){
        if(typeof e.created == "number"){
            var date = directory.decomposeDate(e.created, "dayNumber-month-year",true); 
            str += date;
        }else if(typeof e.created == "object"){
            var date = directory.decomposeDate(e.created.sec, "dayNumber-month-year",true);
            str += date;
        }
    }
    return str;
};

adminDirectory.values.name = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.name != "undefined") {
        if(type == "organizations")
		    str = '<a href="#page.type.organizations.id.'+e._id.$id+'" class="" target="_blank">'+e.name+'</a>';
        else if (type == "projects")
            str = e.name;
        else 
            str = '<a href="#page.type.'+type+'.id.'+e._id.$id+'" class="" target="_blank">'+e.name+'</a>';
	}
    return str;
};
adminDirectory.values.toBeValidated = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.toBeValidated != "undefined") {
        str = e.toBeValidated;
    }
    return str;
};
adminDirectory.values.email = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.contacts!="undefined" && typeof e.contacts.mail != "undefined") {
        str = e.contacts.mail;
    }
    if(str=="" && typeof e.email != "undefined") {
        str = e.email;
    }
    return str;
};
adminDirectory.values.responsable = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.contacts!="undefined" && $.isArray(e.contacts)){
        $.each(e.contacts, function(i, contact){
            if(typeof e.contacts.firstname != "undefined" && typeof e.contacts.lastname != "undefined"){
                str += e.contacts.firstname+" "+e.contacts.lastname;
            }else if(e.contacts.name){
                str=e.contacts.name
            }
        })
    }else if(typeof e.contacts != "undefined"){
        if(typeof e.contacts.firstname != "undefined"){
            str = e.contacts.firstname;
        }
        if (typeof e.contacts.lastname != "undefined") {
            str += " "+e.contacts.lastname;
        }
        if(str=="" && e.contacts.name){
            str=e.contacts.name
        }
    }
    
    return str;
};
adminDirectory.values.link = function(e, id, type, aObj) {
    var str = "";
    if (typeof e.link != "undefined") {
        str = e.link;
    }
    return str;
};
adminDirectory.values.status = function(e, id, type, aObj){
    let str = "Invitation en attente";
    let linkEtoC = links.linksTypesModif[type][costum.contextType];
    if(typeof e.links != "undefined" && e.links[linkEtoC]){
        
    }
    return str;
}

adminDirectory.values.tobeactivated = function(e, id, type, aObj){
    mylog.log("adminDirectory.values.tobeactivated", id, type);
    mylog.log("adminDirectory.values.tobeactivated roles",e.roles);
    var tobeactivated=( typeof e.roles != "undefined" && typeof e.roles.tobeactivated != "undefined" && e.roles.tobeactivated == true) ? true : false;
    var str = "";
    if(tobeactivated)
        str = "<span class='badge bg-orange'><i class='fa fa-pause'></i> "+trad.waiting+"</span>";
    else
        str = "<span class='badge bg-green-k'><i class='fa fa-check'></i> "+trad.validated+"</span>";
    return str;
};

adminDirectory.actions["addAsMember"]=function(e, id, type, aObj){
    mylog.log("adminDirectory.actions.addAsMember", id, type, aObj);
    var label = "Ajouter comme membre";
    let linkEtoC = links.linksTypesModif[type][costum.contextType];e, id, type
        

    var option="";
    if(typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" && notEmpty(e.links[linkEtoC][costum.contextId])){
        if(notEmpty(e.links[linkEtoC][costum.contextId]["isInviting"]) && e.links[linkEtoC][costum.contextId]["isInviting"]){
            option="isInviting";
        }else if(notEmpty(e.links[linkEtoC][costum.contextId]["toBeValidated"]) && e.links[linkEtoC][costum.contextId]["toBeValidated"]){
            option="toBeValidated";
        }
    }

    var str = '<button data-id="'+id+'" data-type="'+type+'" data-linkoption="'+option+'" class="col-xs-12 addAsMemberBtn btn bg-green-k text-white"><i class="fa fa-user-plus"></i> '+label+'</button>';
    return str ;
}

adminDirectory.events.admin = function(aObj){
    $("#"+aObj.container+" .adminBtn").off().click(function(e){
        var thisObj=$(this);
        var id = $(this).data("id");
        var type = $(this).data("type");
        var elt = aObj.getElt(id, type) ;
        var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? links.linksTypesModif["citoyens"][aObj.context.collection] : "members" ) ;
        var isAdmin = $(this).data("isadmin");
        mylog.log("isAdmin", isAdmin);
        if(typeof costum.communityLinks.members[id]=="undefined"){
            
        }
        var params = {
            parentId : aObj.context.id,
            parentType : aObj.context.collection,
               childId : id,
            childType : type,
            connect : connectType,
            isAdmin : (isAdmin === true ? false : true )
        };
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/link/updateadminlink/",
            params,
            function(data){ 
                  mylog.log("success data", data);			        
                if(isAdmin === true)
                    isAdmin = false ;
                else
                    isAdmin = true ;

                if( typeof aObj.context != "undefined" &&
                    typeof aObj.context.id != "undefined" &&
                    typeof aObj.context.collection != "undefined" &&
                    typeof elt.links != "undefined" && 
                    typeof links != "undefined" && 
                    typeof links.linksTypesModif != "undefined" &&
                    typeof links.linksTypesModif["citoyens"] != "undefined" &&
                    typeof links.linksTypesModif["citoyens"][aObj.context.collection] != "undefined" &&
                    typeof elt.links[links.linksTypesModif["citoyens"][aObj.context.collection]] != "undefined" && 
                    typeof elt.links[links.linksTypesModif["citoyens"][aObj.context.collection]][aObj.context.id] != "undefined" && 
                    typeof elt.links[links.linksTypesModif["citoyens"][aObj.context.collection]][aObj.context.id]){
                    elt.links[links.linksTypesModif["citoyens"][aObj.context.collection]][aObj.context.id].isAdmin = isAdmin ;
                }

                aObj.setElt(elt, id, type) ;
                $("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
                thisObj.replaceWith( aObj.actions.admin(elt, id, type, aObj) );
                aObj.bindAdminBtnEvents(aObj);
              },
              function(data){
                  $("#searchResults").html("erreur");
              }
          ); 
    });
};

adminDirectory.events["addAsMember"] = function(aObj){
    $("#"+aObj.container+" .addAsMemberBtn").off().on("click",function (){
        mylog.log("adminDirectory.events.addAsMemberBtn ", $(this).data("id"), $(this).data("type"));
        var id = $(this).data("id");
        var type = $(this).data("type");
        var parentType = costum.contextType;
        var parentId = costum.contextId;
        var linkoption = $(this).data("linkoption");
        //var elt = aObj.getElt(id, type) ;
        var label = "Confirmer l'utilisateur comme membre";

        bootbox.confirm(label, function(result) {
            if (result) {
                // function addAsMember(parentType, parentId, id, type, linkoption){
                //     links.validate(parentType, parentId, id, type, linkoption);
                // };
                if(notEmpty(linkoption)){
                    links.validate(parentType, parentId, id, type, linkoption);
                }else{
                    links.connectAjax(parentType, parentId, id, type, null, null, function(){
                        links.validate(parentType, parentId, id, type, "isInviting");
                    });
                }
                
                
            }
        });
    });
}

adminDirectory.values.isInviting = function(e, id, type, aObj){
    mylog.log("isinviting value view",e, id, type);
    let validation = function(val, clas, icon){
        return "<span class='badge "+clas+" text-wrap-ekitia'><i class='fa "+icon+"'></i> "+val+"</span>"
    }
    let linkEtoC = links.linksTypesModif[type][costum.contextType];

    let str = validation("Non envoyé", "bg-danger", "fa-times");
    // if(typeof e.roles.tobeactivated=="undefined" || e.roles.tobeactivated==false){
        if(typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" && notEmpty(e.links[linkEtoC][costum.contextId]) && e.links[linkEtoC][costum.contextId]["isInviting"]){
            str='<a href="#admin.view.tobevalidated" class="lbh">'+validation("Invitation envoyé à l'utilisateur","bg-orange","fa-pause")+'</a>';

        }else if(typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" && notEmpty(e.links[linkEtoC][costum.contextId]) && e.links[linkEtoC][costum.contextId]["toBeValidated"]){
            str='<a href="#admin.view.tobevalidated" class="lbh">'+validation("Demande de l'utilisateur en attente","bg-red","fa-pause")+'</a>';

        }
        // else{
        //     str=validation("Non envoyé","bg-red","fa-times");
        // }      
    // }
    
    if(typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" && e.links[linkEtoC][costum.contextId] && (!notEmpty(e.links[linkEtoC][costum.contextId]["isInviting"]) && !notEmpty(e.links[linkEtoC][costum.contextId]["toBeValidated"]))){
         str = validation("Confirmé", "bg-green-k", "fa-check");
    }
    return str;
}
adminDirectory.values.roles = function(e, id, type, aObj){
    let validation = function(val, clas){
        return "<span class='badge text-wrap-ekiti'>"+val+"</span> ";
    }
    let str = "";
    let linkEtoC = links.linksTypesModif[type][costum.contextType];
    if(typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" && e.links[linkEtoC][costum.contextId] && e.links[linkEtoC][costum.contextId]["roles"]){
        $.each(e.links[linkEtoC][costum.contextId]["roles"], function(i, rol){
            str += validation(rol);
        })
    }
    return str;
}
adminDirectory.values.structure = function(e, id, type, aObj) {
    mylog.log("adminDirectory.values structure", e, id, type, aObj);
    let str = "";
    let count = 0;
    let colors = ["text-info", "text-success", "text-danger", "text-dark"]
    let linkEtoC = links.linksTypesModif[type][costum.contextType];
    let linkCtoE = links.linksTypesModif[costum.contextType][type];
    
    if( typeof e.links != "undefined" && typeof e.links[linkEtoC] != "undefined" ){
        let ccl = (typeof costum != "undefined" && costum.communityLinks)?costum.communityLinks:[]; // costum communityLinks as ccl
        let others = "";
        $.each(e.links[linkEtoC], function(index, element){
            if(typeof ccl!="undefined" && typeof ccl[linkCtoE]!="undefined" && typeof ccl[linkCtoE][index]!="undefined"){
                if(str!="" && count<=3){
                    str+="; ";
                }else{
                    others+= ccl[linkCtoE][index].name+" - ";
                }
                str += '<a href="#page.type.'+element.type+'.id.'+index+'" class="lbh-preview-element '+colors[count]+ ((count<=3)?"":" hidden") +'">'+ ccl[linkCtoE][index].name + '</a>';
            }
        });
        if(count>3){
            str += '<a href="javascript:;" title="'+others+'" class="badge">+'+(count-4)+'</a>';
        }
    }
    return str;
};
adminDirectory.values.mobile = function(e, id, type, aObj) {
    var str = "";
    // case orga
    if (typeof e.contacts!="undefined" && $.isArray(e.contacts)) {
        $.each(e.contacts, function(i, contact){
            str += contact.phone + "&nbsp; &nbsp;";
        })
    }else if(typeof e.telephone!="undefined" && e.telephone.mobile && $.isArray(e.telephone.mobile)){
        $.each(e.telephone.mobile, function(i, mob){
            str += mob + "&nbsp; &nbsp;";
        })
    }else if(typeof e.telephone!="undefined" && e.telephone.fix && $.isArray(e.telephone.fix)){
        $.each(e.telephone.fix, function(i, fix){
            str += fix + "&nbsp; &nbsp;";
        })
    }
    // case user
    if(str=="" && typeof e.phoneNumber != "undefined"){
        str = e.phoneNumber
    }
    return str;
}; 
adminDirectory.actions.accept = function(e, id, type, aObj){ 
	var str ='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 acceptActor btn bg-green-k text-white"><i class="fa fa-check-circle"></i>Valider</button>';		
	return str ;
}; 
adminDirectory.actions.delete = function(e, id, type, aObj){  
    var str = "";
    var label = "Supprimer cette structure";
    if(type=="citoyens"){
        label="Supprimer cette personne";
    }
    if( typeof e.reference !="undefined" && typeof e.reference.costum !="undefined" && (e.reference.costum).indexOf((costum.slug||"ekisphere"))>=0){
	    str +='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteActor btn bg-red-k text-white" data-deletetype="noadmin"><i class="fa fa-trash"></i>'+label+'</button>';		
    }else{
        str +='<button data-id="'+id+'" data-type="'+type+'" class="col-xs-12 deleteActor btn bg-red-k text-white" data-deletetype="delete"><i class="fa fa-trash"></i>'+label+'</button>';
    }
    return str ;
};
adminDirectory.actions.sendMail = function(e, id, type, aObj){  
	var str =' <div style="margin-top:2px"><a href="javascript:;" data-id="'+id+'" data-email="'+e.email+'" class="tooltips btn openFormContact text-center " data-toggle="modal" data-target="#myModal-contact-us"    data-id-receiver=""  data-email="" data-name=""> Envoyer un email</a>';
    return str ;
};
adminDirectory.bindCostum = function(aObj){
    $("#"+aObj.container+" .acceptActor").on("click", function(){  
        links.validate(costum.contextType, costum.contextId, $(this).data("id"), $(this).data("type"), "isInviting");
        // var tplCtx = {};
        // tplCtx.id = $(this).data("id");
        // tplCtx.collection = $(this).data("type");
        // tplCtx.value = null;
        // tplCtx.path = "toBeValidated";
        // dataHelper.path2Value(tplCtx, function(params) {
        //     toastr.success("Acteur ajouté"); 
        //     urlCtrl.loadByHash(location.hash);
        // } );
    });
    $("#"+aObj.container+" .deleteActor").on("click", function(){
        id = $(this).data("id");
        type = $(this).data("type"); 
        deleteType = $(this).data("deletetype");
        var params = [];
        var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
        if(deleteType == "noadmin" ){
            urlToSend = baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/reference";
                params = {
                id:  $(this).data("id"),
                type: $(this).data("type"),
                origin: "costum",
                sourceKey: costum.slug||"ekisphere"
            }
        }
        if(deleteType == "delete" ){
            urlToSend =  baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
            params = null
        }
        bootbox.confirm("voulez vous vraiment supprimer cet acteur !!",
            function(result)
            {
                if (!result) { 
                    return;
                } else { 
                    ajaxPost(
                        null,
                        urlToSend,
                        params,
                        function(data){
                            if ( data && data.result ) {
                                toastr.success("acteur effacé");
                                $("#"+type+id).remove();
                                urlCtrl.loadByHash(location.hash);
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        }
                    );
                }
            }
        );
    });
    $("#"+aObj.container+" .refuseActor").on("click", function(){
        var tplCtx = {};
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("type");
        tplCtx.value = null;
        tplCtx.path = "category";  
        dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success("Acteur refusé");
            urlCtrl.loadByHash(location.hash);
        } );
    });
    $("#"+aObj.container+" .openFormContact").on("click", function(){
        email = $(this).data("email");
        id = $(this).data("id");
        var  modal = `<div class=" fade in" id="formContact-${id}" tabindex="-1" role="dialog" aria-hidden="true"> 
                    <div class="  padding-top-15">                         
                        <div class="container bg-white">
                        <div class="close-modal" data-dismiss="modal">
                            <div style="background-color:#2C3E50" class="lr">
                                <div style="background-color:#2C3E50" class="rl">
                                </div>
                            </div>
                        </div>
                        <div id="form-group-contact">
                        <div class="col-md-10 text-left padding-top-60 form-group">
                        <h3>
                            <i class="fa fa-send letter-blue"></i> 
                            <small class="letter-blue"> <?php echo Yii::t('cms', 'Send an e-mail to')?> : </small>
                            <span id="contact-name" style="text-transform: none!important;"></span><br>
                            <small class="">
                                <small class="">Ce message sera envoyé à </small>
                                <b><span class="contact-email"></span></b>
                            </small>
                        </h3>
                        <hr><br>
                        <div class="col-md-12">
                            <label for="objet"><i class="fa fa-angle-down"></i> Objet de votre message</label>
                            <input class="form-control" placeholder="Objet de votre message" id="subject">
                            <div  style =" display: none;  width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;" class="invalid-feedback subject">Objet requis.</div><br>
                        </div>
                    </div>
                    <div class="col-md-12 text-left form-group">
                        <div class="col-md-12">
                            <label for="message"><i class="fa fa-angle-down"></i> Votre message</label>
                            <textarea placeholder="Votre message..." class="form-control txt-mail"
                        id="message" style="min-height: 200px;"></textarea>
                    <div style =" display: none;  width: 100%; margin-top: 0.25rem; font-size: 80%; color: #dc3545;" class="invalid-feedback message">Votre message est vide.</div><br>
                    <br>
                    <div class="col-md-12 margin-top-15 pull-left">            
                        <button type="submit" class="btn btn-success pull-right" id="btn-send-mail">
                        <i class="fa fa-send"></i> <?php echo Yii::t('cms', 'Send the message')?>
                        </button>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    
    </div>`;
        smallMenu.open(modal);    
        $("#btn-send-mail").click(function(){ 
            sendEmail();
        });  
        $('#message').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
        $('#subject').filter_input({regex:'[^<>#\"\`/\(|\)/\\\\]'});
        if (notNull(email)){
            $("#formContact-"+id+" .contact-email").html(email); 
        } else{
            bootbox.alert("<?php echo Yii::t('cms', 'Please try again later')?>");
        } 
    });
}
  
function sendEmail(){
    var acceptFields = true;
    $.each(["subject","message"],(k,v)=>{
        if($("#"+v).val() == ""){
          $("."+v).show();
          acceptFields=false
        }
    });
    var seconds = Math.floor(new Date().getTime() / 1000);
    var allowedTime = localStorage.getItem("canSend"); 
    if(acceptFields){
        if(seconds < allowedTime){
            return bootbox.alert("<p class='text-center text-dark'><?php echo Yii::t('cms', 'Send back after')?> "+(Math.floor((allowedTime-seconds)/60)==0 ? allowedTime-seconds +" seconde(s)" : Math.floor((allowedTime-seconds)/60)+" minute(s)")+ "</p>");
        }
        localStorage.removeItem("canSend"); 
        var subject = $("#subject").val(); 
        var message = $("#message").val();
        var emailFrom = $(".contact-email").html();

        var params = {
            tpl : "contactForm",
            tplMail : emailFrom,
            fromMail: costum.admin.email, 
            tplObject:subject,
            subject :subject,  
            emailSender:costum.admin.email,
            message : message,
            logo:"",
        };    
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
            params,
            function(data){ 
                if(data.result == true){
                    localStorage.setItem("canSend", (seconds+300));
                    toastr.success("<?php echo Yii::t('cms', 'Your message has been sent')?>");
                    bootbox.alert("<p class='text-center text-green-k'><?php echo Yii::t('cms', 'Email sent to')?> "+emailFrom+" !</p>");
                    $.each(["subject","message"],(k,v)=>{$("#"+v).val("")});
                    urlCtrl.loadByHash(location.hash);
                }else{
                    toastr.error("An error occurred while sending your message");
                    bootbox.alert("<p class='text-center text-red'><?php echo Yii::t('cms', 'Email not sent to')?> "+emailFrom+" !</p>");
                }   
            },
            function(xhr, status, error){
                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
            }
        );
    }   
}