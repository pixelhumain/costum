function isWelcome () {
  let result = false;
  if (location.hash === '#welcome' || location.hash === '')
    result = true

  return result
}

function scrollTopColor() {
  const topbar = $('#mainNav');
  const blockdiv = $("#formation-sectio");
  const scrollClass = 'bg-white';

  window.addEventListener('scroll', () => {
    const scrolled = $(document).scrollTop() >= 1;
    topbar.toggleClass(scrollClass, scrolled);
    
    $('.cosDyn-buttonList').addClass('text-black');
      if (!scrolled) {
        $('#mainNav').attr('style', '');
        $('.cosDyn-buttonList ,a.dropdown-toggle').attr('style','color: white !important');
        $('.cosDyn-menuTop').attr('style', 'background: black !important;');
      }
      else {
        $('#mainNav').removeClass('bg-transparent text-white');
        $('.cosDyn-buttonList ,a.dropdown-toggle').attr('style','color: black !important');
        $('.bg-white').attr('style', 'background: white !important;');
      }
    
  });
}


function initTop() {
    scrollTopColor();
    $('.cosDyn-menuTop').attr('style', 'background: black !important;');
    $('.cosDyn-buttonList, a.dropdown-toggle').attr('style','color: white !important');
}

$(".dropdown .dropdown-menu-top .cosDyn-dropdown .open").remove()

$(document).ready(function () {
  initTop();
  window.addEventListener("hashchange", function() {
    initTop();
  });
});