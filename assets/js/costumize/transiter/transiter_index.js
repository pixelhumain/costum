dyFObj.formInMap.forced.map = {
    center : ["-21.114533","55.532062499999995"],
    zoom : 9
};
dyFObj.formInMap.forced.countryCode = "RE";
dyFObj.formInMap.forced.showMap = true;

directory.cardsAnnuaire = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    var otherStr = (params.descriptionStr||params.text||"").substring(0, 40)+"...";
    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    if(params.collection=="events"){
        dateAndTags = 'Du <span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span> au <span class="date">'+moment(params.endDateDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span>';
    }
    if(params.collection=="citoyens"){
        otherStr = `<div class="otherStr">
        <span><i class="fa fa-envelope-o"></i>  ${params.email}</span>`;
        if(typeof params.mobile !="undefined"){
            otherStr += `</br><span> <i class="fa fa-phone"></i>  ${params.mobile}</span>`;
        }
        if(typeof params.thematic !="undefined"){
            otherStr += `</br><span><i class="fa fa-link"></i>  ${params.thematic}</span><br>`;
        }
        if(typeof params.link !="undefined"){
            otherStr += `</br><span><a href="${params.link}" target="_blank" > <i class="fa fa-link"></i> Site web</a></span><br>`;
        }
        otherStr += "</div>";
    }
    let linkedElement = "";
    if(params.parent ){
        let p = params.parent;
        linkedElement = '<div style="display: flex;flex-wrap: wrap; gap: calc((.10rem)* 4);" >'
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent " target="_blank">'+
                    ov.name+
                '</a><br/>';
        }),
        linkedElement+="</div>"
    }

    if(params.organizer){
        $.each(params.organizer, function(ko, vo){
            ajaxPost(
                '', 
                baseUrl + "/co2/element/get/type/"+vo.type+"/id/" + ko,
                null,
                function (data) {
                    if(typeof data.map != 'undefined' && typeof data.map.parent != 'undefined'){
                        linkedElement = '<div style="display: flex;flex-wrap: wrap; gap: calc((.10rem)* 4);" >'
                        $.each(data.map.parent, function (oi, ov) {
                            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent " target="_blank">'+
                                ov.name+
                            '</a><br/>';
                        })
                        linkedElement+="</div>";  
                    }else{
                        linkedElement = "";
                    }
                },null,"json",{async : false}
            )
        })

    }
    // str += '<div class="blog-item actus-item item-type1 col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12">';
    str+= `<div id='entity_${params.collection}_${params._id.$id}' href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}"  class="lbh-preview-element blog-item actus-item item-type1 searchEntityContainer smartgrid-slide-element start-masonry msr-id-${params._id.$id} col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12" style="cursor:pointer">`;
    str += '<div class="item-inner">'+
            '<div class="image">'+
                '<img src="'+(params.profilMediumImageUrl||modules.costum.url+"/images/thumbnail-default.jpg")+'" class="img-responsive" alt="" decoding="async">'+
            '</div>'+
            ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
            //((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(tradCategory[params.type]||"")+'">'+(tradCategory[params.type]||"")+'</span>':"")+
            '<div class="item-body">'+
                '<div class="item-text">'+
                    (params.name?`<h4 class="title">${params.name}</h4>`:"")+
                    '<div class="top-wrapper">'+
                        '<span class="cat-date">'+dateAndTags+'</span>'+
                        // '<span class="category lowercase">#'+(((params.tags&&params.tags[0])?params.tags[0]:"")||"")+'</span>'+
                        // '</span>'+ 
                    '</div>'+otherStr+
                    linkedElement+
                '</div>'+
                '<div class="link-bordered">';
                    if(params.collection=="events" && params.type=="course"){
                        str += '<a class="btn  handleSession" onClick ="handleSession(\''+params._id.$id+'\')" data-id="'+params._id.$id+'"fa fa-cog"></i> Gérer cette session</a>';
                    }
                str += '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
    return str;
} 
directory.cardsFormation = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    let buttonSubscribe = "";
    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    if(params.collection=="events"){
        dateAndTags = 'Du <span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span> au <span class="date">'+moment(params.endDateDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span>';
        if(userId!="" && typeof params.links.attendees !="undefined" && typeof params.links.attendees[userId] != "undefined"){
            buttonSubscribe = 'Vous êtes inscrit';
        }else{
            buttonSubscribe = `<a href="javascript:;" class="tooltips followBtn margin-left-5"  
                    data-toggle="tooltip" data-placement="left" data-original-title="Inscription"
                    data-ownerlink="attendees"
                    data-id="${params.id}" 
                    data-type="${params.collection}" 
                    data-name="${params.name}"
                    data-isFollowed="false">
                    Je m'inscris
            </a>`;
        }
    }
    var otherStr = "";

    let linkedElement = "";
    if(params.parent || params.organizer){
        let p = params.organizer || params.parent;
        linkedElement = '<div style="display: flex;flex-wrap: wrap; gap: calc((.10rem)* 4);" >'
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent" target="_blank">'+
                    ov.name+
                '</a>';
        }),
        linkedElement+="</div>"
    }
    if(params.organizer && costum.slug == "transiter"){
        $.each(params.organizer, function(ko, vo){
            ajaxPost(
                '', 
                baseUrl + "/co2/element/get/type/"+vo.type+"/id/" + ko,
                null,
                function (data) {
                    if(typeof data.map != 'undefined' && typeof data.map.parent != 'undefined'){
                        linkedElement = '<div style="display: flex;flex-wrap: wrap; gap: calc((.10rem)* 4);"">'
                        $.each(data.map.parent, function (oi, ov) {
                            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent" target="_blank">'+
                                ov.name+
                            '</a>';
                        })
                        linkedElement+="</div>";  
                    }else{
                        linkedElement = "";
                    }
                },null,"json",{async : false}
            )
        })

    }
    if(params.collection=="citoyens"){
        otherStr = `<div class="otherStr">
        <span><i class="fa fa-envelope-o"></i>  ${params.email}</span>`;
        if(typeof params.mobile !="undefined"){
            otherStr += `</br><span> <i class="fa fa-phone"></i>  ${params.mobile}</span>`;
        }
        if(typeof params.thematic !="undefined"){
            otherStr += `</br><span><i class="fa fa-link"></i>  ${params.thematic}</span><br>`;
        }
        if(typeof params.link !="undefined"){
            otherStr += `</br><span><a href="${params.link}" target="_blank" > <i class="fa fa-link"></i> Site web</a></span><br>`;
        }
        otherStr += "</div>";
    }
    // str += `<div id='entity_${params.collection}_${params._id.$id}' class="msr-id-${params._id.$id} blog-item actus-item item-type1 col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12">`;
    str+= `<div id='entity_${params.collection}_${params._id.$id}' data-href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}" class="blog-item click-element  actus-item item-type1 msr-id-${params._id.$id} col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12" style="cursor:pointer">`;
    
    // str+= `<div id='entity_${params.collection}_${params._id.$id}' data-href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}"  class="actus-item item-type1 searchEntityContainer smartgrid-slide-element start-masonry msr-id-${params._id.$id} col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12" style="cursor:pointer">`;
    
    str += '<div class="item-inner">'+
            '<div class="image">'+
                '<img src="'+(params.profilMediumImageUrl||modules.costum.url+"/images/thumbnail-default.jpg")+'" class="img-responsive" alt="" decoding="async">'+
            '</div>'+
            ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
            //((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(tradCategory[params.type]||"")+'">'+(tradCategory[params.type]||"")+'</span>':"")+
            '<div class="item-body">'+
                '<div class="item-text">'+
                    (params.name?`<h4 class="title">${params.name}</h4>`:"")+
                    '<div class="top-wrapper">'+
                        '<span class="cat-date">'+dateAndTags+'</span>'+
                        // '<span class="category lowercase">#'+(((params.tags&&params.tags[0])?params.tags[0]:"")||"")+'</span>'+
                        // '</span>'+ 
                    '</div>'+otherStr+
                    '<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
                    linkedElement+
                '</div>'+
                '<div class="link-bordered">';
                    if(params.collection=="events" && params.type=="course"){
                        str += '<a class="btn  handleSession" onClick ="handleSession(\''+params._id.$id+'\')" data-id="'+params._id.$id+'"fa fa-cog"></i> Gérer cette session</a>';
                        str += buttonSubscribe;
                    }
                str += '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
    return str;
}  
if(typeof costum[costum.slug].formation =="undefined"){
    costum[costum.slug].formation = {
        afterBuild : function(data){
            if($("#certificationTraining").val() == "false" || !$("#certificationTraining").val()){
                $(".certificationsAwardstext").hide();
            }else{
                $(".certificationsAwardstext").show();
            }
            $(".btn-dyn-checkbox").click(function(){
                if($("#certificationTraining").val() == "false" || !$("#certificationTraining").val()){
                    $(".certificationsAwardstext").hide();
                }else{
                    $(".certificationsAwardstext").show();
                }       
            });
        },
        afterSave : function(data){
            dyFObj.commonAfterSave(data, function() {
                mylog.log("aftersave formation", data);
                ajaxPost(
                    null, 
                    baseUrl + '/costum/formationgenerique/aftersave/type/formation', 
                    {data : data.map}, 
                    function () {}
                );
                urlCtrl.loadByHash("#@"+data.map.slug);
            })
        }
    }
}
if(typeof costum[costum.slug].organismeFormation=="undefined"){
    costum[costum.slug].organismeFormation = {
        afterSave : function(data){
            dyFObj.commonAfterSave(data, function() {
                mylog.log("aftersave formation", data);
                var tplCtx = {};
                if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                    tplCtx.id = data.map._id.$id; 
                }
                if(typeof data.id != "undefined" && typeof data.map._id == "undefined"){
                    tplCtx.id = data.id;
                }
                tplCtx.value = {};
                tplCtx.collection = "organizations";
                tplCtx.value.tags = [];
                if(typeof data.map.tags != "undefined"){
                    tplCtx.value.tags = data.map.tags;
                }
                    
                if(typeof data.map.trainingQualification != "undefined"){
                    trainingQualification = data.map.trainingQualification.split(",");							
                    if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {  
                        /* Rajouter dans tags le trainingQualification */
                        $.each(trainingQualification, function(i,tagtf){
                            if((tplCtx.value.tags).indexOf(tagtf) == -1){                                    
                                tplCtx.value.tags.push(tagtf);
                            }
                        });
                        /* Enlever le tags trainingQualification s'il n'exisste pas dans data.map.trainingQualification  */
                        $.each(costum.lists.trainingQualification,function(kt,vt){                         
                            if((tplCtx.value.tags).indexOf(vt) >-1  && (trainingQualification).indexOf(vt) == -1){
                                tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1);                                 
                            }
                        })
                    }else{ 
                        /* Rajouter dans tags le trainingQualification */
                        $.each(trainingQualification, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }else{
                    /* Enlever le tags trainingQualification s'il n'exisste pas dans data.map.trainingQualification  */
                    if(typeof costum.lists.trainingQualification){
                        $.each(costum.lists.trainingQualification,function(kf,vt){
                            if((tplCtx.value.tags).indexOf(vt) >-1 ){
                                delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vt)]; 
                            }
                        })
                    }
                }
                /* Rajouter le tags "Organisme de formation */
                if(typeof tplCtx.value.tags != "undefined"){
                    if(tplCtx.value.tags.indexOf("Centre de formation") == -1){
                        tplCtx.value.tags.push("Centre de formation");
                    }
                }else{
                    tplCtx.value.tags = ["Centre de formation"];
                }

                if(typeof data.map.source !="undefined" && typeof data.map.source.keys !== "undefined" && data.map.source.keys.indexOf(costum.slug) == -1 ){
                    if(typeof data.map.reference != "undefined" && typeof data.map.reference.costum !== "undefined" && data.map.reference.costum.indexOf(costum.slug) == -1){
                        data.map.reference.costum.push(costum.slug);
                        tplCtx.value.reference = data.map.reference
                    }else if(typeof data.map.reference == "undefined"){
                        tplCtx.value.reference = {};
                        tplCtx.value.reference.costum = [costum.slug];
                    }
                }
                if(typeof data.map.source =="undefined"){
                    if(typeof data.map.reference != "undefined" && typeof data.map.reference.costum !== "undefined" && data.map.reference.costum.indexOf(costum.slug) == -1){
                        data.map.reference.costum.push(costum.slug);
                        tplCtx.value.reference = data.map.reference
                    }else if(typeof data.map.reference == "undefined"){ 
                        tplCtx.value.reference = {};
                        tplCtx.value.reference.costum = [costum.slug];
                    }
                }
                /* Mise à jour costum lists */
                paramsParentCostum = {};
                paramsParentCostum.id = costum.contextId;
                paramsParentCostum.collection = costum.contextType;
                paramsParentCostum.path = "allToRoot"; 
                paramsParentCostum.updateCache = true; 
                paramsParentCostum.value = {};
                if(typeof data.map.trainingQualification != "undefined"){
                    trainingQualification = data.map.trainingQualification.split(",");
                    $.each(trainingQualification,function(ktf,vtf){
                        if(typeof costum.lists.trainingQualification != "undefined"){
                            if( (costum.lists.trainingQualification).indexOf(vtf)== -1 ){
                                paramsParentCostum.value["costum.lists.trainingQualification"] = [];
                                paramsParentCostum.value["costum.lists.trainingQualification"] = costum.lists.trainingQualification;
                                paramsParentCostum.value["costum.lists.trainingQualification"].push(vtf);                             
                            }
                        }
                    })
                }
                dataHelper.path2Value(tplCtx, function(params) {
                    urlCtrl.loadByHash(location.hash);
                    if(Object.keys(paramsParentCostum.value).length !== 0){                    
                        dataHelper.path2Value(paramsParentCostum, function(params) {
                            location.reload()
                        });
                    }							
                });
                
            })
        }
    };
}
if(typeof costum[costum.slug].formateur == "undefined"){
    costum[costum.slug].formateur = {
        afterBuild : function (){
            $("#email").off().on("blur",function(){
                if($("#email").val() != ""){
                    var params = {
                        "email" : $("#email").val(),
                        "searchType" : ["citoyens"],
                        "notSourceKey" : true,
                        "fields" : ["tags","source","reference","link","email"]
                    };
                    ajaxPost(
                        null,
                        baseUrl+"/" + moduleId + "/search/globalautocomplete",
                        params,
                        function(data){
                            if(Object.keys(data.results).length > 0){
                                var listInvite = {
                                    citoyens : {}
                                }
                                $.each(data.results, function(k,v){
                                    if(typeof v.links == "undefined" 
                                        || (typeof v.links != "undefined" && typeof v.links.memberOf == "undefined")
                                        || (typeof v.links != "undefined" && typeof v.links.memberOf != "undefined"&& typeof v.links.memberOf[costum.contextId] == "undefined")  ){
                                             
                                        listInvite["citoyens"][k] = {
                                            name : v.name,
                                            profilThumbImageUrl : v.profilThumbImageUrl,
                                            isAdmin : ""
                                        }
                                        bootbox.dialog({ 
                                            message: "Cet email est déjà utilisée par d'autre utilisateur "+v.name,
                                            buttons: {
                                                success: {
                                                    label: "Rajouter cet  utilisateur comme formateur?",
                                                    className: "btn-primary",
                                                    callback: function () {
                                                        var params = {
                                                            parentId: costum.contextId,
                                                            parentType: costum.contextType,
                                                            listInvite: listInvite
                                                        };                                
                                                        ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function(data) {
                                                        })
                                                        var tplCtx = {
                                                            id: k,
                                                            collection: "citoyens",
                                                            value: {
                                                            }
                                                        }
                                                        if(typeof v.tags == "undefined" || !notEmpty(v.tags)) v.tags=[];
                                                        if(v.tags.indexOf("Formateur") == -1){
                                                            tplCtx.value.tags = v.tags;
                                                            tplCtx.value.tags.push("Formateur");
                                                        }
                                                        if(typeof v.source !="undefined" && typeof v.source.keys !== "undefined" && v.source.keys.indexOf(costum.slug) == -1 ){
                                                            if(typeof v.reference != "undefined" && typeof v.reference.costum !== "undefined" && v.reference.costum.indexOf(costum.slug) == -1){
                                                                tplCtx.value.reference = {};
                                                                tplCtx.value.reference.costum = v.reference.costum;
                                                                tplCtx.value.reference.costum.push(costum.slug);
                                                            }else if(typeof v.reference == "undefined"){
                                                                tplCtx.value.reference = {};
                                                                tplCtx.value.reference.costum = [costum.slug];
                                                            }
                                                        }
                                                        if(typeof v.source =="undefined"){
                                                            if(typeof v.reference != "undefined" && typeof v.reference.costum !== "undefined" && data.reference.costum.indexOf(costum.slug) == -1){
                                                                tplCtx.value.reference = {};
                                                                tplCtx.value.reference.costum = v.reference.costum;
                                                                tplCtx.value.reference.costum.push(costum.slug);
                                                            }else if(typeof data.reference == "undefined"){
                                                                tplCtx.value.reference = {};
                                                                tplCtx.value.reference.costum = [costum.slug];
                                                            }
                                                        }
                                                        dataHelper.path2Value(tplCtx, function(params) {
                                                            toastr.success("Formateur ajouté")
                                                            dyFObj.closeForm();
                                                            urlCtrl.loadByHash(location.hash);
                                                        });
                                                    }
                                                },
                                                cancel: {
                                                    label: trad["cancel"],
                                                    className: "btn-secondary",
                                                    callback: function () {
                                                        dyFObj.closeForm()
                                                    }
                                                }
                                            }
                                        });

                                    }   
                                    if(typeof v.links != "undefined" && typeof v.links.memberOf != "undefined"&& typeof v.links.memberOf[costum.contextId] != "undefined"){
                                        bootbox.alert({ 
                                            message: "Cet utilisateur est déjà sur la liste des formateurs",
                                            callback: function () {
                                                dyFObj.closeForm()
                                            }
                                         });                                        
                                    }
                                });
                            }
                        }
                    )
                }
            })
            if(uploadObj.update){
                $("#email").attr("disabled","disabled");
            }

        }
    }
}
if(typeof costum[costum.slug].tiersLieux=="undefined"){
    costum[costum.slug].tiersLieux = {
        afterSave : function(data){
            dyFObj.commonAfterSave(data, function() {
                mylog.log("aftersave formation", data);
                var tplCtx = {};
                if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                    tplCtx.id = data.map._id.$id; 
                }
                if(typeof data.id != "undefined" && typeof data.map._id == "undefined"){
                    tplCtx.id = data.id;
                }
                tplCtx.value = {};
                tplCtx.collection = data.map.collection;
                tplCtx.value.tags = [];
                if(typeof data.map.tags != "undefined"){
                    tplCtx.value.tags = data.map.tags;
                }
                /* Rajouter le tags "Centre de formation */
                if(typeof tplCtx.value.tags != "undefined"){
                    if(tplCtx.value.tags.indexOf("TiersLieux") == -1){
                        tplCtx.value.tags.push("TiersLieux");
                    }
                }else{
                    tplCtx.value.tags = ["TiersLieux"];
                }


                if(typeof data.map.source !="undefined" && typeof data.map.source.keys !== "undefined" && data.map.source.keys.indexOf(costum.slug) == -1 ){
                    if(typeof data.map.reference != "undefined" && typeof data.map.reference.costum !== "undefined" && data.map.reference.costum.indexOf(costum.slug) == -1){
                        data.map.reference.costum.push(costum.slug);
                        tplCtx.value.reference = data.map.reference
                    }else if(typeof data.map.reference == "undefined"){
                        tplCtx.value.reference = {};
                        tplCtx.value.reference.costum = [costum.slug];
                    }
                }
                if(typeof data.map.source =="undefined"){
                    if(typeof data.map.reference != "undefined" && typeof data.map.reference.costum !== "undefined" && data.map.reference.costum.indexOf(costum.slug) == -1){
                        data.map.reference.costum.push(costum.slug);
                        tplCtx.value.reference = data.map.reference
                    }else if(typeof data.map.reference == "undefined"){ 
                        tplCtx.value.reference = {};
                        tplCtx.value.reference.costum = [costum.slug];
                    }
                }
                dataHelper.path2Value(tplCtx, function(params) {
                    urlCtrl.loadByHash(location.hash);
                });
            })
        }
    };
}

costum.searchExist=function(type,id,name,slug,email,elt){
    if(typeof dyFObj.elementObj !== 'undefined' && typeof dyFObj.elementObj.formType !== 'undefined'){
        if(typeof costum.typeObj[dyFObj.elementObj.formTypej] != "undefined" && typeof costum.typeObj[dyFObj.elementObj.formType].dynFormCostum != "undefined"){
            var editPrev=jQuery.extend(true, {},costum.typeObj[dyFObj.elementObj.formType].dynFormCostum);
        }
        if(typeof editPrev != "undefined"){
            dyFObj.editElement(type,id, dyFObj.elementObj.formType, editPrev);
        }else{
            dyFObj.editElement(type,id, dyFObj.elementObj.formType);

        }
    }else{
        var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
        dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");

    } 
};

urlCtrl.openPreview= function (url, data ,withLoader=true) {
    $("#openModal").modal("hide");
    urlCtrl.previewCurrentlyLoading = true;
    $(".searchEntity").removeClass("active");
    urlCtrl.checkSlugUrl(url, function (res) {
        urlPreview = baseUrl + '/' + moduleId + "/";
        urlPreview += (res.indexOf("#") == 0) ? "app/" + urlCtrl.convertToPath(res) : "app/" + res;
        if (!notNull(data))
            data = { "preview": true };
        else if (typeof data.preview == "undefined")
            data.preview = true;
        ajaxPost(
            "#modal-preview-coop",
            urlPreview,
            data,
            function (data) {
                $("#modal-preview-coop").removeClass("hidden").show();
                urlCtrl.bindModalPreview();
                urlCtrl.resizePreview();
                coInterface.bindEvents();
            }
        );
    });
}

costum.costumizedDirectory = {
    "cards":tradCms.card,
    "cardsFormation": 'Carte formation'

};

typeObj.person.formType="person";
typeObj.person.color="yellow"; 
typeObj.person.createLabel="Ajouter un contact";
typeObj.person.dynForm={
    title : "Ajouter un formateur",
    icon : "user",
    type : "object",
    col : "citoyens",
    jsonSchema : {
        "properties" : {
        },			
        beforeBuild : function(){
			dyFObj.setMongoId('citoyens', function(){
			});
        },
        formatData : function(data){
            mylog.log("beforeSave data dynF start",data);
            if(typeof data.source !="undefined" && typeof data.source.keys !== "undefined" && data.source.keys.indexOf(costum.slug) == -1 ){
                if(typeof data.reference != "undefined" && typeof data.reference.costum !== "undefined" && data.reference.costum.indexOf(costum.slug) == -1){
                    data.reference.costum.push(costum.slug);
                }else if(typeof data.reference == "undefined"){
                    data.reference = {};
                    data.reference.costum = [costum.slug];
                }
            }
            if(typeof data.source =="undefined"){
                if(typeof data.reference != "undefined" && typeof data.reference.costum !== "undefined" && data.reference.costum.indexOf(costum.slug) == -1){
                    data.reference.costum.push(costum.slug);
                }else if(typeof data.reference == "undefined"){
                    data.reference = {};
                    data.reference.costum = [costum.slug];
                }
            }
            mylog.log("beforeSave data dynF return",data);
            return data;
        },
        onLoads : {
            onload : function() {
                $("#ajax-modal-modal-title").html("<i class='fa fa-user'></i> Ajouter un formateur");
            },
            afterLoad : function(data){
                mylog.log("generic afterload",data);
                $("#ajaxFormModal .surnametext label").append(" (modifier si nécessaire)");
                $("#ajaxFormModal .firstNametext label").append(" (modifier si nécessaire)");
                $("#ajaxFormModal .emailtext label").append(" (saisi à l'inscription)");
            }
        },
        afterSave : function(data){
            mylog.log("aftersave person data",data);
            if(typeof data.tags == "undefined" || (typeof data.tags != "undefined" && data.tags.indexOf("Formateur") == -1)){
                var listInvite = {
                    citoyens : {}
                }
                ajaxPost("",
                    baseUrl + "/co2/element/get/type/citoyens/id/" + data.id,
                    null,
                    function (data) {
                        if (data.result) {
                           if(typeof data.map.links == "undefined" 
                                || (typeof data.map.links != "undefined" && typeof data.map.links.memberOf == "undefined")
                                || (typeof data.map.links != "undefined" && typeof data.map.links.memberOf != "undefined"&& typeof data.map.links.memberOf[costum.contextId] == "undefined")  ){
                                
                                listInvite["citoyens"][data.map._id.$id] = {
                                    name : data.map.name,
                                    email : data.map.email,
                                    profilThumbImageUrl : data.map.profilThumbImageUrl,
                                    isAdmin : ""
                                }    
                                var params = {
                                    parentId: costum.contextId,
                                    parentType: costum.contextType,
                                    listInvite: listInvite
                                };                 
                                ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function(data) {
                                })
                            }
                        }
                    }
                )  
                if(!uploadObj.update){
                    var tplCtx = {
                        id: data.id,
                        path: "allToRoot",
                        collection: "citoyens",
                        value: {
                        }
                    }                
                    if(typeof data.tags == "undefined" || !notEmpty(data.tags)) data.tags=[];
                    if(data.tags.indexOf("Formateur") == -1){
                        tplCtx.value.tags = data.tags;
                        tplCtx.value.tags.push("Formateur");
                    }
                    tplCtx.value.roles = {
                        "tobeactivated" : true,
                        "standalonePageAccess" : true
                    }
                    tplCtx.value.pending = true;
                    tplCtx.value.seePreferences = true;
                    tplCtx.value.invitedBy = userId;
                    var chars = "0123456789abcdefghijklmnopqrstuvwxyz";
                    tplCtx.value.username = Array.from({ length: 32 }, () => chars[Math.floor(Math.random() * chars.length)]).join('');
                    tplCtx.value.preferences = {
                        "isOpenData" : false,
                        "sendMail" : false,
                        "publicFields" : [ 
                            "locality", 
                            "directory"
                        ],
                        "privateFields" : [ 
                            "birthDate", 
                            "email", 
                            "phone"
                        ]
                    }
                    
                    tplCtx.value.slug = tplCtx.value.username;
                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/slug/checkandcreate",
                        {
                            text : tplCtx.value.username
                        },
                        function(data){ 
                            if(data.results){
                                tplCtx.value.slug = data.results
                            }
                        },null,"json",{async:false}
                    );
                    dataHelper.path2Value(tplCtx, function(params) {
                        toastr.success("Formateur ajouté")
                    });
                }
            }

            dyFObj.commonAfterSave(data, function() {
                urlCtrl.loadByHash(location.hash);
            })
        }
    }
};	
// User register form modal
Login.runRegisterValidator  = function(params) { 
    var form4 = $('.form-register');
    var errorHandler3 = $('.errorHandler', form4);
    var createBtn = null;
    $(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");

    $(".telephoneRegister").hide();
    $(".fonctionRegister").hide();
    $("label[for='agree'] .agreeMsg").html(`
        Je suis d'accord avec 
        <a href="https://communecter.org/#mentions" target="_blank" class="bootbox-spp text-dark">
            les mentions légales                                </a>
        et                             
        <a href="#confidentialityRules" class="bootbox-spp text-dark" target="_blank">
        La politique de confidentialité</a>
    `);

    $("label[for='charte'] .agreeMsg").html(`
        J'ai pris cccc connaissance <a href="#charte" target="_blank" class="bootbox-spp text-dark">
        Les conditions générales d’utilisation</a>
    `);

    canSubmit(false);
    $("#agree, #charte").on("change", function(){
        if($("#agree").is(":checked") && $("#charte").is(":checked") && form4.valid()){
            canSubmit(true);
            errorHandler3.hide();
        }else{
            canSubmit(false);
        }
    })
    
    form4.validate({
        onfocusout:function(el, ev){
            var validator = $('.form-register').validate();
            
            if(document.getElementById("password3")==el){
                var pattern = new RegExp(
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
                );
                if(!pattern.test($("#password3").val())){
                    validator.showErrors({
                        "password3": `<div class="margin-left-10">Le mot de passe doit conténir au minimum 12 caractères, 1 minuscule, 1 majuscule, 1 chiffre et un caractère spécial</div>`
                    });
                    canSubmit(false);
                }else{
                    validator.element(el);
                }
            }else{
                validator.element(el);
            }

            if($("#agree").is(":checked") && $("#charte").is(":checked") && form4.valid()){
                canSubmit(true);
                errorHandler3.hide();
            }else{
                canSubmit(false);
            }
        },
        rules : {
            name : {
                required : true,
                minlength : 4
            },
            telephone : {},
            fonction : {
                required : true
            },
            username : {
                required : true,
                validUserName : true,
                rangelength : [4, 32]
            },
            email3 : {
                required : { 
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email : true,
            },
            password3 : {
                minlength : 12,
                required : true
            },
            passwordAgain : {
                equalTo : "#password3",
                required : true
            },
            agree:{
                required:true
            },
            charte:{
                required:true
            }
        },

        submitHandler : function(form) {

            errorHandler3.hide();
            $(".createBtn").prop('disabled', true);
            $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
            var params = { 
                "name" : $('.form-register #registerName').val(),
                "username" : $('.form-register #username').val(),
                "email" : $(".form-register #email3").val(),
                "fonction" : $(".form-register #fonction").val(),
                "telephone" : $(".form-register #telephone").val(),
                "pendingUserId" : pendingUserId,
                "pwd" : $(".form-register #password3").val()
            };
            if($('.form-register #isInvitation').val())
                params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
                params.scope = scopeObj.selected;
            }
            if($(".form-register #newsletter").is(":checked"))
                params.newsletter=true;
            if($(".form-register #newsletterCollectif").is(":checked"))
                params.newsletterCollectif=true;
            var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
            
            //if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
            //	params.msgGroup=$(".form-register #textMsgGroup").val();
            ajaxPost(
                null,
                baseUrl+"/costum/ekitia/register",
                params,
                function(data){ 
                    if(data.result) {
                        mylog.log("Register Formulaire",data.result);
                        toastr.success("Merci, votre compte a bien été créé");
                        $('.modal').modal('hide');
                        if(params.isInvitation){
                            window.location.href = baseUrl+window.location.pathname;
                        }else{
                            $("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                            $("#modalRegisterSuccess").modal({ show: 'true' }); 
                            
                            $('#modalRegisterSuccess .btn-default').click(function() {
                                mylog.log("hide modale and reload");
                                $('.modal').modal('hide');
                                if(typeof thisElement == "undefined"){
                                    window.location.reload();
                                }
                            });
                        }
                    }else {
                        toastr.error(data.msg);
                        $('.modal').modal('hide');	    		  	
                        scopeObj.selected={};
                    }
                },function(data) {
                    toastr.error(trad["somethingwentwrong"]);
                    $(".createBtn").prop('disabled', false);
                    $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                }
            );
            return false;
        },
        invalidHandler : function(event, validator) {//display error alert on form submit
            errorHandler3.show();
            $(".createBtn").prop('disabled', false);
            $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
        }
    });
};
Login.runRegisterValidator();


costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="isPresential"){
            $("#ajax-modal .visioLinkundefined").hide();
        }
    },
    false : function(id){
        if(id=="isPresential"){
            $("#ajax-modal .visioLinkundefined").show();
        }
    }
}
// paramsMapCO = $.extend(true, {}, paramsMapCO, {
//     activeCluster:false,
//     mapCustom: {
//         icon: {
//             getIcon: function (params) {
//                 var elt = params.elt;
//                 mylog.log("dddd",elt.collection)
//                 // var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
//                 var marker = {
//                     citoyens: modules.map.assets + '/images/markers/citizen-marker-default.png',
//                     organizations: modules.map.assets + '/images/markers/ngo-marker-default.png',
//                     classified: modules.map.assets + '/images/markers/classified-marker-default.png',
//                     proposal: modules.map.assets + '/images/markers/proposal-marker-default.png',
//                     poi: modules.map.assets + '/images/markers/poi-marker-default.png',
//                     projects: modules.map.assets + '/images/markers/project-marker-default.png',
//                     events: modules.map.assets + '/images/markers/event-marker-default.png',
//                     answer: modules.map.assets + '/images/markers/services/tools-hardware.png',
//                 }
//                 var imgProfilPath = marker[elt.collection];
//                 if (typeof elt.profilMediumImageUrl !== "undefined" && elt.profilMediumImageUrl != "")
//                     imgProfilPath = baseUrl + elt.profilMediumImageUrl;

                
//                 if(imgProfilPath != ""){
//                     var myIcon = L.divIcon({
//                         className: "ekitia-div-marker",
//                         iconAnchor: [35, 70],
//                         iconSize: [70, 70],
//                         labelAnchor: [-70, 0],
//                         popupAnchor: [0, -70],
//                         html: `<div style='background-color:var(--primary-color);' class='marker-ekitia'></div><img src="${imgProfilPath}">`
//                     });

//                 }
//                 return myIcon;
//             }
//         }
//     }
// });

if(typeof paramsMapCO != "undefined" && typeof paramsMapCO.mapCustom != "undefined" && typeof paramsMapCO.mapCustom.icon != "undefined"){
    delete paramsMapCO.mapCustom.icon;
}

function AddReadMoreNews() {
    var moretext = "Lire la suite";
    var lesstext = "Lire moins";
    $(".timeline-inverted .link-preview, .timeline-inverted  .results").hide();
    $('.timeline_text').each(function() {
        var content = $(this).text();
        var htmlContent = $(this).html();
        var textcontent = $(this).text().trim();
        var parentDiv = $(this).parent();
        var hasMorelink = parentDiv.find('.morelink').length > 0;
        var hascontainerclass = parentDiv.find('.container').length > 0;
        var firstLine = content.split(/\r?\n/)[0]; 
        mylog.log("contentcontentff",content,firstLine);
        if (firstLine.length > 0) { 
            var html = '<span class="container"><span>' + firstLine + '</span>' + 
                    '<span class="moreelipses"></span></span>' +
                    '<span class="morecontent" style="display:none">' + htmlContent + '</span>';
            if (!hascontainerclass) {
                $(this).html(html);
            }
            if (hasMorelink ) {
                $(".morelink").remove();
            }
            if(content.split(/\r?\n/).length > 1){
            $(this).after('<a href="" class="morelink">' + moretext + '</a>');
            }
        }
    });
    $(".morelink").off("click").on("click", function() { 
        var linkpreview = $(this).closest('li.timeline-inverted.list-news').find('div.link-preview');
        var targetResults = $(this).closest('li.timeline-inverted.list-news').find('div.results');
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).prev().prev().children(".link-preview").hide();
            linkpreview.hide();
            targetResults.hide();
            $(this).html(moretext);
            $(this).prev().children('.morecontent').fadeToggle(100, function() {
                $(this).prev().fadeToggle(100);
            });
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
            $(this).prev().prev().children(".link-preview").show();
            linkpreview.show();
            targetResults.show();            
            $(this).prev().children('.container').fadeToggle(100, function() {
                $(this).next().fadeToggle(100);
            });
        }
        return false;
    });
}