$(function() { 
    var memberElement = {
        init: function () {
            this.event.init();
            this.action.init();
        },
        event : {
            init() {
                memberElement.event.menuTopEvent();
            },
            menuTopEvent : function () {
                $(".link-circle.text-center.tooltips").remove();
                $("span.label-link").removeAttr("class","");
            }
        },
        action : {
            init: function () {
                memberElement.action.deleteEmptyBtnTooltips();
                memberElement.action.changeDetailRender();
            },
            deleteEmptyBtnTooltips () {
                if ($("a.ssmla.btn-menu-tooltips").length > 0) {
                    for (let index = 0; index < $("a.ssmla.btn-menu-tooltips").length; index++) {
                        if (!notEmpty($($("a.ssmla.btn-menu-tooltips")[index]).find("span").html())) {
                            $($("a.ssmla.btn-menu-tooltips")[index]).remove();
                        }
                    }
                }
            },
            changeDetailRender : function() {
                pageProfil.views.detail = function(){
                    var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
                    if(typeof contextData.category != "undefined" && contextData.category=="formation") url+="/view/costum.views.custom.transiter.element.formation.detail";
                    // if(typeof contextData.tags != "undefined" &&  contextData.tags.indexOf("Centre de formation")) url+="/view/costum.views.custom.transiter.element.detail";
                    if(contextData.type == "organizations" || (contextData.type == "citoyens" && typeof contextData.tags != "undefined" && (contextData.tags).indexOf("Formateur") >-1)) url+="/view/costum.views.custom.transiter.element.detail";
                    $("#menu-top-btn-group").show();
                    $("#central-container").show();
                    ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");

                },
                pageProfil.views.formation = function(){
                    trainingQualification = {};
                    if(costum && typeof costum.lists != "undefined" && typeof costum.lists.trainingQualification != "undefined"){
                       Object.assign(trainingQualification, costum.lists.trainingQualification);
                    }
                    var str = `
                        <div class="formationSearchContent">
                            <div id="filter-nav" class="searchObjCSS"></div> 
                            <div id="header-formation" class="no-padding col-xs-12"></div>
                            <div id="results-formation"></div>
                        </div>
                    `
                    $("#central-container").html(str);
                    var paramsFilterFormation= {
                        container : "#filter-nav",

                        header : {
                            dom : "#header-formation",
                            options : {
                                left : {
                                    classes : 'col-xs-4 elipsis no-padding',
                                    group:{
                                        count : true
                                    }
                                },
                                right : {
                                    classes : 'col-xs-8 text-right no-padding',
                                    group : {
                                        map : true,
                                    }
                                }
                            }
                        },
                        loadEvent : {
                            default : "scroll"
                        },
                        defaults : {
                            notSourceKey : true,
                            types : ["projects"],
                            forced:{
                                filters:{
                                    category : "formation"                                    
                                }
                            },
                            filters:{
                            }
                        },
                        results : {
                            dom : "#results-formation",
                            renderView : "directory.cards",
                            smartGrid : true                       
                        },
                        filters : {
                            text : true,
                            trainingQualification : {
                                view : "dropdownList",
                                type : "tags",
                                name : "Qualification de la formation",
                                event : "tags",
                                list : trainingQualification
                            }
                        }
                    }
                    paramsFilterFormation.defaults.forced.filters["parent."+contextData.id] = {'$exists' : true};
                    filterFormation = searchObj.init(paramsFilterFormation);
                    filterFormation.search.init(filterFormation);
                }
            }
        }
    }
    memberElement.init();

    pageProfil.directory.communityMenu.organizations = {}

})

