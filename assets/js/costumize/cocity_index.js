
costum.tagsFilters = [];
costum.tagsFilters.theme = [];
costum.tagsFilters.theme[tradTags.food] = {
    "agriculture": "agriculture",
    "alimentation": "alimentation",
    "nourriture": "nourriture",
    "AMAP": "AMAP"
}
costum.tagsFilters.theme[tradTags.health] = {
    "santé": "santé"
}


costum.tagsFilters.theme[tradTags.waste] = {
    "déchets": "déchets"
}


costum.tagsFilters.theme[tradTags.transport] = {
    "Urbanisme": "Urbanisme",
    "transport": "transport",
    "construction": "construction"
}


costum.tagsFilters.theme[tradTags.education] = {
    "éducation": "éducation",
    "petite enfance": "petite enfance"
}

costum.tagsFilters.theme[tradTags.citizenship] = {
    "citoyen": "citoyen",
    "society": "society"
}


costum.tagsFilters.theme[tradTags.economy] = {
    "ess": "ess",
    "économie social solidaire": "économie social solidaire"
}

costum.tagsFilters.theme[tradTags.energy] = {
    "énergie": "énergie",
    "climat": "climat"
}


costum.tagsFilters.theme[tradTags.culture] = {
    "culture": "culture",
    "animation": "animation"
}


costum.tagsFilters.theme[tradTags.environment] = {
    "environnement": "environnement",
    "biodiversité": "biodiversité",
    "écologie": "écologie"
}

costum.tagsFilters.theme[tradTags.numeric] = {
    "informatique": "informatique",
    "tic": "tic",
    "internet": "internet",
    "web": "web"
}

costum.tagsFilters.theme[tradTags.sport] = {
    "sport": "sport"
}

costum.tagsFilters.theme[tradTags.associations] = {
    "associations" : "association"
}

costum.tagsFilters.theme[tradTags.pact] = {
    "pacte" : "pacte"
}

costum.tagsFilters.theme[tradTags.thirdplaces] = {
    "TiersLieux" : "TiersLieux"
}
var allThem = {
    "alimentation" : {
        "name" : "Food",
        "icon" : "fa-cutlery",
        "tags" : [ 
            "agriculture", 
            "alimentation", 
            "nourriture", 
            "AMAP"
        ]
    },
    "santé" : {
        "name" : "Health",
        "icon" : "fa-heart-o",
        "tags" : [ 
            "santé"
        ]
    },
    "déchets" : {
        "name" : "Waste",
        "icon" : "fa-trash-o ",
        "tags" : [ 
            "déchets"
        ]
    },
    "transport" : {
        "name" : "Transport",
        "icon" : "fa-bus",
        "tags" : [ 
            "Urbanisme", 
            "transport", 
            "construction"
        ]
    },
    "éducation" : {
        "name" : "Education",
        "icon" : "fa-book",
        "tags" : [ 
            "éducation", 
            "petite enfance"
        ]
    },
    "citoyenneté" : {
        "name" : "Citizenship",
        "icon" : "fa-user-circle-o",
        "tags" : [ 
            "citoyen", 
            "society"
        ]
    },
    "Économie" : {
        "name" : "Economy",
        "icon" : "fa-money",
        "tags" : [ 
            "ess", 
            "économie social solidaire"
        ]
    },
    "énergie" : {
        "name" : "Energy",
        "icon" : "fa-sun-o",
        "tags" : [ 
            "énergie", 
            "climat"
        ]
    },
    "culture" : {
        "name" : "Culture",
        "icon" : "fa-universal-access",
        "tags" : [ 
            "culture", 
            "animation"
        ]
    },
    "environnement" : {
        "name" : "Environnement",
        "icon" : "fa-tree",
        "tags" : [ 
            "environnement", 
            "biodiversité", 
            "écologie"
        ]
    },
    "numérique" : {
        "name" : "Numeric",
        "icon" : "fa-laptop",
        "tags" : [ 
            "informatique", 
            "tic", 
            "internet", 
            "web",
            "numérique"
        ]
    },
    "sport" : {
        "name" : "Sport",
        "icon" : "fa-futbol-o",
        "tags" : [ 
            "sport"
        ]
    },
    "tiers lieux" : {
        "name" : "Third places",
        "icon" : "fa-globe",
        "tags" : [
            "TiersLieux"
        ]
    },
    "pacte" : {
        "name" : "Pact",
        "icon" : "fa-hand-o-up",
        "tags" : [
            "pacte"
        ]
    },
    "associations" : {
        "name" : "Associations",
        "icon" : "fa-chain",
        "tags" : [
            "associations"
        ]
    }
};
var allSugThematic = [
    tradTags["food"],
    tradTags["health"] ,
    tradTags["associations"],
    tradTags["pact"],
    tradTags["third places"],
    tradTags["transport"] ,
    tradTags["education"],
    tradTags["citizenship"],
    tradTags["economy"],
    tradTags["energy"],
    tradTags["culture"],
    tradTags["environment"],
    tradTags["numeric"],
    tradTags["sport"],
    tradTags["waste"] 
];
$("#menuTopLeft .menu-btn-top").after(costum.title);

function getNumberElement(slug = "", template, thema = "", address = "", id = "", typeCocity = "") {
    var htmlNbElm = '';
    var params = {
        searchType: ["organizations", "event", "projects", "citoyens"],
        countType: ["organizations", "event", "projects", "citoyens"],
        count: true,
        notSourceKey: true,
        filters: {
            $or: {}
        }
    };
    if(slug != "" && id != "") {
        params.filters["$or"] = {
            "source.keys": slug, 
            ["links.memberOf."+id] : {$exists:true}
        };
    } else if(slug != "") {
        params.filters["$or"] = {
            "source.keys": slug
        };
    }
    
    if (template == "cocity") {
        let source = {};
        let tags = thema === "tiers lieux" ? "TiersLieux" : thema;
        let sourcekey = thema === "tiers lieux" ? "franceTierslieux" : "siteDuPactePourLaTransition";
        let addressParams = [];
        let idAddress = {};
    
        switch (typeCocity) {
            case "region":
                idAddress = address["level3"] ? { "address.level3": address["level3"] } : null;
                break;
            case "ville":
                idAddress = address["localityId"] ? { "address.localityId": address["localityId"] } : null;
                break;
            case "departement":
            case "district":
                idAddress = address["level4"] ? { "address.level4": address["level4"] } : null;
                break;
            case "epci":
                idAddress = costum.citiesArray && costum.citiesArray.length > 0 
                    ? { "address.localityId": { $in: costum.citiesArray } } 
                    : null;
                break;
            default:
                idAddress = null;
                break;
        }
    
        if (idAddress && Object.keys(idAddress).length > 0) {
            addressParams.push(idAddress);
        }
        if (thema === "tiers lieux" || thema === "pacte") {
            source["$or"] = [
                { "source.keys": sourcekey },
                { "reference.costum": sourcekey },
                { "tags": tags }
            ];
            addressParams.push(source);
        } else {
            addressParams.push({ "tags": tags });
        }
        if (address["postalCode"]) {
            addressParams.push({ "address.postalCode": address["postalCode"] });
        }
        addressParams = addressParams.filter(param => param !== null && Object.keys(param).length > 0);
        if (addressParams.length > 0) {
            params.filters["$or"].$and = addressParams;
        }
    }

    ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/search/globalautocomplete",
        params,
        function(data) {
            if (template == "unnotremonde") {
                htmlNbElm += '<div class="main-section">';
                if (data.count.organizations != 0)
                    htmlNbElm += '<div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-users" aria-hidden="true"></i><br>' +
                    '<small>Organisations</small>' +
                    '<p>' + data.count.organizations + '</p>' +
                    '</div>' +
                    '</div>';
                if (data.count.projects != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-lightbulb-o" aria-hidden="true"></i><br>' +
                    '<small>Projets</small>' +
                    '<p>' + data.count.projects + '</p>' +
                    '</div>' +
                    '</div>';
                if (data.count.citoyens != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-user" aria-hidden="true"></i><br>' +
                    '<small>Personnes</small>' +
                    '<p>' + data.count.citoyens + '</p>' +
                    '</div>' +
                    '</div>';

                if (data.count.event != 0)
                    htmlNbElm += ' <div class="dashbord">' +
                    '<div class="icon-section">' +
                    '<i class="fa fa-calendar" aria-hidden="true"></i><br>' +
                    '<small>Evenements</small>' +
                    '<p>' + data.count.event + '</p>' +
                    '</div>' +
                    '</div>';
                '</div>';
            } else if (template == "cocity") {
                htmlNbElm += '<div class="main-section">';
                if (data.count.citoyens != 0)
                    htmlNbElm += '<i class="fa fa-user" aria-hidden="true"></i>' + data.count.citoyens + " ";
                if (data.count.event != 0)
                    htmlNbElm += '<i class="fa fa-calendar" aria-hidden="true"></i>' + data.count.event + " ";

                if (data.count.organizations != 0)
                    htmlNbElm += '<i class="fa fa-users" aria-hidden="true"></i>' + data.count.organizations + " ";
                if (data.count.projects != 0)
                    htmlNbElm += '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>' + data.count.projects + " ";

                '</div>';
            }
            if(slug != "") {
                $(".element" + slug).html(htmlNbElm);
            } else if(thema != "tiers lieux"){
                $(".element" + thema).html(htmlNbElm);
            } else {
                $(".elementtiers").html(htmlNbElm);
            }
        }
    );
    return htmlNbElm;
}

function getTotalNumberElement(thema, address, typeCocity){
    var htmlNbElm = '';
    var params = {
        searchType: ["organizations", "event", "projects", "citoyens"],
        countType: ["organizations", "event", "projects", "citoyens"],
        count: true,
        notSourceKey: true,
        filters: {
            $or: {}
        },
        onlyCount: true
    };
    
    var source = {};
    var tags = thema == "tiers lieux" ? "TiersLieux" : thema;
    var sourcekey =  thema == "tiers lieux" ?  "franceTierslieux" : "siteDuPactePourLaTransition";
    var addressParams = [];
    var idAddress = {};
    if(address != undefined) {
        if(typeCocity == "region"){
            idAddress = {"address.level3" : address["level3"]};
        } else if(typeCocity == "ville"){
            idAddress = {"address.localityId" : address["localityId"] };
        } else if(typeCocity == "departement" || typeCocity == "district"){
            idAddress = {"address.level4" : address["level4"] };
        } else if(typeCocity == "epci"){
            idAddress = {"address.localityId": {$in : costum.citiesArray} };
        }
    }
    if(thema == "tiers lieux" || thema == "pacte") {
        source["$or"] = [
            {"source.keys" : sourcekey},
            {"reference.costum" : sourcekey},
            {"tags" : tags}
        ];
        addressParams = address != undefined ? [
            idAddress,
            source
        ] : [source];
    } else {
        addressParams = address != undefined ? [
            idAddress,
            {"tags" : tags}
        ] : [
            {"tags" : tags}
        ];
    }
    // address["postalCode"] != "" ? addressParams.push({"address.postalCode" : address["postalCode"]}) : "";

    params.filters["$or"].$and = addressParams;

    ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/search/globalautocomplete",
        params,
        function(data) {
            htmlNbElm = data.count.citoyens + data.count.event + data.count.organizations + data.count.projects;
            if(thema != "tiers lieux"){
                $(".element" + thema).html(htmlNbElm);
            } else {
                $(".elementtiers").html(htmlNbElm);
            }
        }
    );
    return htmlNbElm;
}

// function useTemplateCity(tpl_params, loadpage) {
//     var params = {
//         "id"		 : tpl_params.idTplSelected,
//         "page"		 : "",
//         "newCostum"  : true,
//         "action"	 : tpl_params.action,
//         "collection" : tpl_params.collection,
//         "parentId"   : tpl_params.contextId,
//         "parentSlug" : tpl_params.contextSlug,
//         "parentType" : tpl_params.contextType
//     };
//     ajaxPost(
//         null,
//         baseUrl + "/co2/template/use",
//         params,
//         function (data) {
//             if (loadpage) {
//                 window.location = baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug;
//             }			
//         },
//         { async: false }
//     );
// }
function useTemplate(tpl_params, loadpage, progress = 0, progressBar = '', progressPercentage = '') {
    return new Promise((resolve, reject) => {
        var params = {
            "id"        : tpl_params.idTplSelected,
            "page"      : "",
            "newCostum" : true,
            "action"    : tpl_params.action,
            "collection": tpl_params.collection,
            "parentId"  : tpl_params.contextId,
            "parentSlug": tpl_params.contextSlug,
            "parentType": tpl_params.contextType
        };
        ajaxPost(
            null,
            baseUrl + "/co2/template/use",
            params,
            function (data) {
                if(progress && progress > 0) {
                    progressBar.style.width = progress + '%';
                    progressPercentage.textContent = Math.round(progress) + '%';
                } 
                if (loadpage) {
                    window.location = baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug;
                }
                resolve();
            },
            { async: false }
        );
    });
}

// function createOrgaFiliereCity(params, paramscocity) {
//     ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
//         function(result){
//             var data = {
//                 collection : 'organizations',
//                 id: result.id,
//                 path: 'allToRoot',
//                 value : {
//                     "costum.slug" : "costumize",
//                     "cocity" : paramscocity.cocity, 
//                     "thematic" : paramscocity.thematic, 
//                     "ville" : paramscocity.ville,
//                     "source.key" : paramscocity.keys,
//                     "source.keys" : [paramscocity.keys],
//                     "costum.typeCocity" : paramscocity.typeZone
//                 }
//             }
            
//             if(paramscocity.address) data.value["address"] = paramscocity.address
//             // dataHelper.path2Value(data, function(){
//             //     let tpl_params = {"idTplSelected" : paramscocity.idTemplatefiliere , "collection" : "templates", "action" : "", "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
//             //     let loadpage = false;
//             //     useTemplateCity(tpl_params, loadpage);
//             // });
//             dataHelper.path2Value(data, function(){
//                 let loadpage = false;
//                 let themat = (paramscocity.thematic).toLowerCase();
//                 var params = {"thematic" : themat , "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
//                 co.importCostumContent(params, loadpage);
//             });
//         },
//         function(){
//             toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
//         },
//         "json"
//     );
// }

function createOrgaFiliere(params, paramscocity, progress = 0, progressBar = "", progressPercentage = "") {
    var costumize = paramscocity.thematic == "Tiers lieux" ? "reseauTierslieux" : "costumize";
    return new Promise((resolve, reject) => {
        ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
            function(result){
                var data = {
                    collection : 'organizations',
                    id: result.id,
                    path: 'allToRoot',
                    value : {
                        "costum.slug" : costumize,
                        "cocity" : paramscocity.cocity, 
                        "thematic" : paramscocity.thematic, 
                        "ville" : paramscocity.ville,
                        "source.key" : paramscocity.keys,
                        "source.keys" : [paramscocity.keys],
                        "costum.typeCocity" : paramscocity.typeZone
                    }
                };

                if(paramscocity.address) data.value["address"] = paramscocity.address;
                
                dataHelper.path2Value(data, function(){
                    let loadpage = false;
                    let themat = (paramscocity.thematic).toLowerCase();
                    var params_import = {"thematic" : themat , "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};                    
                    co.importCostumContent(params_import, loadpage, progress, progressBar, progressPercentage);
                    resolve();
                });
            },
            function(){
                toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
                reject();
            },
            "json"
        );
    });
}

function path2ValueCity(params, tpl_params, loadpage) {
    ajaxPost(
        null,
        baseUrl+"/costum/cocity/update",
        params,
        function(results) {
            useTemplate(tpl_params, loadpage);
        }
    )
}

costum.searchExist = function(type,id){

    if (type == "organizations" || type == "projects") {
        var collection = "";
        var elementType = "";
        if(type == "organizations") {
            collection = "Organization" ;
            elementType = "Organisation";
        } else if(type == "projects"){
            collection = 'Project';
            elementType = "Projet";
        }
        var textConfirm = `Souhaitez-vous intégrer cette ${elementType} en tant que membre de cet élément ?`;
        $("#txt-confirm-add").html(textConfirm);
        document.getElementById('popup').classList.remove('hidden');

        var memberUpdate = {
            collection : collection,
            id:id,
            path: 'allToRoot',
            value : costum.slug
        }

        document.getElementById('yesBtn').addEventListener('click', function() {
            ajaxPost(
                null,
                baseUrl+"/costum/cocity/addmember",
                memberUpdate,
                function(results) {}
            )
            dyFObj.closeForm();
            document.getElementById('popup').classList.add('hidden');
        });
        
        document.getElementById('noBtn').addEventListener('click', function() {
            document.getElementById('popup').classList.add('hidden');
        });      
    }
}