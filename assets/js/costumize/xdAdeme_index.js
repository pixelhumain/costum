function isWelcome () {
  let result = false;
  if (location.hash === '#welcome' || location.hash === '')
    result = true

  return result
}

function scrollTopColor() {
  const topbar = $('#mainNav');
  const scrollClass = 'bg-white';
  $('.cosDyn-buttonList').removeClass('text-black').addClass('text-white')
  window.addEventListener('scroll', () => {
    const scrolled = $(document).scrollTop() >= 1;
    topbar.toggleClass(scrollClass, scrolled);
    $(".dropdown-menu .cosDyn-buttonList").attr("style", "color: inherit !important");
    $('.cosDyn-buttonList').addClass('text-black');
      if (!scrolled && isWelcome()) {
        // $('.cosDyn-buttonList')
        //   .removeClass('text-black')
        //   .addClass('text-white');
          // .attr('style','color: none !important');
        $('.cosDyn-buttonList ,a.dropdown-toggle').attr('style','color: white !important');
        $(".dropdown-menu .cosDyn-buttonList").attr("style", "color: inherit !important");
        $('#mainNav').attr('style', '');
      }
      else {
        $('#mainNav').removeClass('bg-transparent text-white');
        // $('.cosDyn-buttonList').removeClass('text-white').addClass('text-black');
        $('.cosDyn-buttonList ,a.dropdown-toggle').attr('style','color: black !important');
        // $('.cosDyn-menuTop').attr('style', 'background: none !important;');
        $('.bg-white').attr('style', 'background: white !important;');
      }
    
  });
}


function initTop() {
  if (isWelcome ()) {
    scrollTopColor();
    $('.cosDyn-menuTop').attr('style', 'background: transparent !important;');
    $('.cosDyn-buttonList, a.dropdown-toggle').attr('style','color: white !important');
    $(".dropdown-menu .cosDyn-buttonList").attr("style", "color: inherit !important");
  } else {
    $('.cosDyn-menuTop').attr('style', 'background: white !important;');
    $('.cosDyn-buttonList, a.dropdown-toggle').attr('style','color: black !important');
  }
}

$(".dropdown .dropdown-menu-top .cosDyn-dropdown .open").remove()

$(document).ready(function () {
  initTop();
  window.addEventListener("hashchange", function() {
    initTop();
  });
});