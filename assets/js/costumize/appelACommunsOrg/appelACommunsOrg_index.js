costum[costum.slug] = {}
costum.costumizedDirectory = {
    aacDesign : "Multi-card of aac",
    cardStat : "Card with number"
};

function formatDepenseList(depenseList, answerId) {
    var depListHtml = "";
    $.each(depenseList, function(depIndex, depVal) {
        if (typeof depVal.include != "undefined" && !(/true/.test(depVal.include))) {
            return;
        }
        const head = `
            <div class="desc-title collapsed" data-toggle="collapse" id="head${ depIndex }${ answerId }" data-target="#financeur${ depIndex }${ answerId }" aria-expanded="true" aria-controls="financeur${ depIndex }${ answerId }">
                <span class="fin-motif-desc-text">
                    {{title}}
                </span>
                <span class="desc-line"></span>
                <span class="montant-financ">
                    <span>
                        {{collected}} <i class="fa fa-eur"></i>
                    </span> / 
                    <span>
                        {{price}} <i class="fa fa-eur"></i>
                    </span>
                </span>
            </div>
        `;
        var headSub = {
            price : depVal.price ? depVal.price : 0,
            title: depVal.poste ? depVal.poste : "",
            collected : 0
        }
        var body = `<div class="financeur-lista bs-p-3 collapse" id="financeur${ depIndex }${ answerId }" aria-labelledby="head${ depIndex }${ answerId }" data-parent="#accordionLdd${ answerId }">`;
        if (typeof depVal.financer != 'undefined' && notEmpty(depVal.financer)) {
            $.each(depVal.financer, function(finIndex, finVal) {
                const finAmount = finVal.amount ? parseFloat(finVal.amount) : 0;
                headSub.collected += finAmount;
                body += `
                    <div class="desc-motif">
                        <span class="motif-text">
                            <span>${ finVal.name ? finVal.name : "" }</span>
                        </span>
                        <span class="desc-line"></span>
                        <span class="montant-financ">
                            ${ finAmount }<i class="fa fa-eur"></i> 
                        </span>
                    </div>
                `
            })
        } else {
            body += `
                <span class="col-xs-12 text-center">Aucun financeur</span>
            `
        }
        body += `</div>`;
        depListHtml += `
            <div class="financ-line-desc">
                ${ dataHelper.printf(head, headSub) }
                ${ body }
            </div>
        `;
    })
    return depListHtml
}
directory.cardStat = function (params) {
    var parentName = "";
    var parentKey = "";
    if (params.parent) {
        const parentKey = Object.keys(params.parent)[0];
        parentName = params.parent[parentKey].name;
    }
    if (params.redirectPage) {
        if (params.redirectPage.indexOf("?") > -1) {
            params.redirectPage += "&";
        } else {
            params.redirectPage += "?";
        }
        params.redirectPage += "searchbyparent=" +params.id
    }
    var cardHtml = `
        <div class="col-xs-12 col-lg-3 col-md-4 col-sm-6 bs-mb-4 bs-px-3">
            <a class="cute-card col-xs-12 lbh" href="${ params.redirectPage ? params.redirectPage : "" }">
                <div class="top-section" style="background-image: url(${  params.profilMediumImageUrl ? params.profilMediumImageUrl : defaultImage })">
                    <div class="border"></div>
                    <div class="icons">
                        <div class="logo">
                            ${ parentName }
                        </div>
                    </div>
                </div>
                <div class="bottom-section">
                    <span class="title">${ params.name }</span>
                    <span class="description">${ params.description }</span>
                    <div class="row row1">
                        <div class="item">
                            <span class="big-text">${ typeof params.stat != "undefined" && typeof params.stat.communs ? params.stat.communs.toLocaleString("fr-FR") : 0 }</span>
                            <span class="regular-text">Communs déposés</span>
                        </div>
                        <div class="item">
                            <span class="big-text">
                                <span class="animate-on-hover" data-start="${typeof params.stat != "undefined" && typeof params.stat.collectedFond ? params.stat.collectedFond : 0}" data-end="${ typeof params.stat != "undefined" && typeof params.stat.solicitedFund ? params.stat.solicitedFund : 0 }">${ typeof params.stat != "undefined" && typeof params.stat.collectedFond ? params.stat.collectedFond.toLocaleString("fr-FR") : 0 }</span>
                                <span>€</span>
                            </span>
                            <span class="regular-text change-on-hover" data-hover-value="Financement demandés" data-default-value="Financement collectés">Financement collectés</span>
                        </div>
                        <div class="item">
                            <span class="big-text">${ typeof params.stat != "undefined" && typeof params.stat.participants ? params.stat.participants.toLocaleString("fr-FR")  : 0 }</span>
                            <span class="regular-text">Contributeurs</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    `;
  return cardHtml;
};
// ********************************
// AAC.ORG design
// ********************************
directory.aacDesign = function(params) {
    mylog.log('PARAMS DIRECTORY', params)
    var tagsHtml = '';
    const nbTagToShow = 2;
    var tagsTooltipsHtml = '';
    var isSelectedCommun = false;
    const shortDescId = "aapStep1lzi62x3etw49gyc424d";
    const besoinsTags = "aapStep1m03ot9qymmfgashp7l";
    var usersCount = 0;
    if (params.answers && params.answers.aapStep1 && params.answers.aapStep1[shortDescId]) {
        params.descriptionStr = params.answers.aapStep1[shortDescId];
    } else {
        params.descriptionStr = null;
    }
    if (params.answers && params.answers.aapStep1 && params.answers.aapStep1[besoinsTags]) {
        params.tags = params.answers.aapStep1[besoinsTags];
    } else {
        params.tags = null;
    }
    if(typeof params.tags != 'undefined' && params.tags != null) {
        for(let i = 0 ; i < params.tags.length ; i++) {
            if (i < nbTagToShow) {
                tagsHtml += `<span class="tags" style="white-space: nowrap">${params.tags[i]}</span>`
            } else {
                tagsTooltipsHtml += `<span class="tags">${params.tags[i]}</span>`
            }
        }
    }
    if (params && 
        costum &&
        costum.contextId &&
        params.answers && 
        params.answers.aapStep2 && 
        params.answers.aapStep2.choose && 
        params.answers.aapStep2.choose[costum.contextId] && 
        params.answers.aapStep2.choose[costum.contextId].value == "selected"
    ) {
        isSelectedCommun = true;
    }
    if (!notEmpty(tagsHtml)) {
        tagsHtml = `<span class="bs-mb-2">&nbsp;</span>`;
    }
    var funding = {
        total: 0,
        funded: 0
    };
    var progress = 0;
    if (typeof params.funds != "undefined") {
        funding = params.funds.reduce(function(prev, current) {
            return ({
                total: prev.total + current.price,
                funded: prev.funded + current.financer.reduce((a, b) => (a + b), 0)
            })
        }, funding);
    }
    progress = parseInt((funding.funded / (funding.total > 0 ? funding.total : 1)) * 100);
    if (typeof params.links != "undefined" && params.links.tls) {
        usersCount = Object.keys(params.links.tls).length;
    }
    if (params.redirectPage) {
        params.redirectPage += ".communId." +params.id
    }
    var str = `
        <div class="aac-design rotating-card aac-card col-xs-12 col-lg-3 col-md-4 col-sm-6" style="--costum-color1: ${ costum.css && costum.css.color && costum.css.color.color1 ? costum.css.color.color1 : "#1b1b1b" }; --costum-color2: ${ costum.css && costum.css.color && costum.css.color.color2 ? costum.css.color.color2 : "#e54e5e" }; --costum-color3: ${ costum.css && costum.css.color && costum.css.color.color3 ? costum.css.color.color3 : "#68ca91" }">
            <div class="rotating-card-container manual-flip">
                <div class="commun-card card-child">
                    <div class="face bs-p-0">
                        <button class="btn btn-simple pull-right btn-rotate-card rotatingCard-${params.id}">
                            <i class="fa fa-mail-forward fa-lg"></i> 
                        </button>
                        <div class="commun-card-image">
                            <img src="${params.image}"/>
                        </div>
                        <div class="commun-card-body">
                            <div class="commun-title">
                                <a class="btn-know-more lbh desc-2-ellipsis this-commun-title" href="${ params.redirectPage ? params.redirectPage : "" }">${params.name}</a>
                            </div>
                            <div class="commun-desc desc-3-ellipsis bs-mb-2 ${ params.descriptionStr ? 'has-desc' : '' }">
                                ${params.descriptionStr ? params.descriptionStr : `(${ trad.nodescription })`}
                            </div>
                            ${tagsHtml != "" ? `
                                <div class="commun-tags">
                                    ${tagsHtml} 
                                    ${ 
                                        tagsTooltipsHtml != "" ? (
                                            `
                                                <span class='dropdown'>
                                                    <a class='dropdown-toggle' type='button' id='${ params.id }' data-toggle='dropdown'>
                                                        <span class="tags small-width"><i class="fa fa-ellipsis-h"></i></span>
                                                    </a>
                                                    <div class='dropdown-menu bs-px-3 tags-dropdown' role='menu' aria-labelledby='${ params.id }'>
                                                        ${ tagsTooltipsHtml }
                                                    </div>
                                                </span>
                                            `
                                        ) : ''
                                    }
                                </div>` : ''
                            }
                            <div class="commun-collect">
                                <span class="collect-montant text-ellipsis"> ${ funding.funded ? funding.funded.toLocaleString("fr-FR") : funding.funded }<i class="fa fa-eur"></i></span>
                                <span class="collect-text">${ trad["Already collected"] }</span>
                            </div>
                            <div class="commun-progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                        aria-valuemin="0" aria-valuemax="100" style="width:${progress}%">
                                        ${progress}%
                                    </div>
                                </div>
                            </div>
                            <div class="commun-footer">
                                <div class="footer-text">
                                    <span>${usersCount} ${usersCount > 1 ? tradCms.users.toLowerCase() : tradCms.user.toLowerCase()}</span>
                                    <span>${params.interrest_count} ${params.interrest_count > 1 ? trad["interested "] : trad["interested"]}</span>
                                </div>    
                                <div class="footer-btn">
                                    <a class="btn-know-more lbh" href="${ params.redirectPage ? params.redirectPage : "" }">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="1.2em" height="1.2em" viewBox="0 0 24 24" style="
                                            transform: rotate(180deg);
                                            margin-top: .4rem;
                                        ">
                                            <g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path d="m8 5l-5 5l5 5"></path><path d="M3 10h8c5.523 0 10 4.477 10 10v1"></path></g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="behind bs-p-0">
                        <button class="btn btn-simple btn-rotate-card rotatingCard-${params.id}">
                            <i class="fa fa-mail-reply fa-lg"></i>
                        </button>
                        <div class="commun-title bs-pt-2 bs-pl-4 bs-mr-3 bs-pr-4">
                            <a class="btn-know-more lbh desc-2-ellipsis this-commun-title" href="${ params.redirectPage ? params.redirectPage : "" }">${params.name}</a>
                        </div>
                        <div class="col-xs-12 no-padding">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#${params.id}-ldd" class="bs-py-2 bs-pr-2" role="tab" data-toggle="tab">
                                        Les lignes de dépense
                                    </a>
                                </li>
                                ${
                                    typeof params.actionsData != "undefined" ? `
                                        <li>
                                            <a href="#${params.id}-actions" class="bs-py-2 bs-pr-2" role="tab" data-toggle="tab">
                                                ${ trad.actions }
                                            </a>
                                        </li>
                                    ` : ''
                                }
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="${params.id}-ldd">
                                    <div class="results-summary-container__options cs-scroll">
                                        <div class="heading-secondary bs-mb-2">Les objectifs et besoins financiers</div>
                                        ${
                                            typeof params.answers?.aapStep1?.depense != 'undefined' && notEmpty(params.answers.aapStep1.depense) ? `
                                                <div class="summary-result-options" id="accordionLdd${ params.id }">
                                                    ${ formatDepenseList(params.answers.aapStep1.depense, params.id) }
                                                </div>
                                            ` : `
                                                <div class="text-center">
                                                    <span>Il n'y a rien ici</span>
                                                </div>
                                            `
                                        }
                                    </div>
                                </div>
                                ${
                                    typeof params.actionsData != "undefined" ? `
                                        <div class="tab-pane" id="${params.id}-actions">
                                            <div class="results-summary-container__options">
                                                <div class="heading-secondary bs-mb-2">Les actions</div>
                                                <div class="summary-result-options">
                                                    <div class="result-option result-option-reaction">
                                                        <div class="icon-box">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 20 20">
                                                                <path stroke="#44546f" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.25" d="M10.833 8.333V2.5l-6.666 9.167h5V17.5l6.666-9.167h-5Z"></path>
                                                            </svg>
                                                            <span class="reaction-icon-text">${ trad.todo }</span>
                                                        </div>
                                                        <div class="result-box"><span>${ params.actionsData.todo }</span> / ${ params.actionsData.all }</div>
                                                    </div>
                                                    <div class="result-option result-option-memory">
                                                        <div class="icon-box">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 20 20">
                                                                <path stroke="#172b4d" fill-rule="evenodd" d="M5.705 17.886L10 15.97l4.295 1.915a1 1 0 0 0 1.402-1.019l-.494-4.677l3.148-3.493a1 1 0 0 0-.535-1.647l-4.6-.976L10.866 2a1 1 0 0 0-1.732 0l-2.35 4.074l-4.6.976a1 1 0 0 0-.535 1.647l3.148 3.494l-.494 4.676a1 1 0 0 0 1.402 1.018m3.888-3.924l-3.119 1.39l.359-3.395a1 1 0 0 0-.252-.774L4.295 8.646l3.34-.708a1 1 0 0 0 .66-.478L10 4.502l1.706 2.958a1 1 0 0 0 .659.478l3.34.708l-2.286 2.537a1 1 0 0 0-.252.774l.359 3.396l-3.119-1.39a1 1 0 0 0-.814 0" clip-rule="evenodd" />
                                                            </svg>
                                                            <span class="memory-icon-text">${ trad.processing }</span>
                                                        </div>
                                                        <div class="result-box"><span>${ params.actionsData.inprogress }</span> / ${ params.actionsData.all }</div>
                                                    </div>
                                                    <div class="result-option result-option-Visual">
                                                        <div class="icon-box">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <g fill="none" stroke="#44546f" stroke-linecap="round" stroke-width="2">
                                                                    <path d="m9 10l3.258 2.444a1 1 0 0 0 1.353-.142L20 5" />
                                                                    <path d="M21 12a9 9 0 1 1-6.67-8.693" />
                                                                </g>
                                                            </svg>
                                                            <span class="visual-icon-text">${ trad.done }</span>
                                                        </div>
                                                        <div class="result-box"><span>${ params.actionsData.completed }</span> / ${ params.actionsData.all }</div>
                                                    </div>
                                                    <div class="summary__cta">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ` : ''
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `
    return str;
}
