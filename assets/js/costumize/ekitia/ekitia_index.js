directory.profils = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    str += '<div class="blog-item profil-item item-type1 col-xl-3 col-md-4 col-sm-6">'+
            '<div class="light">'+
                '<div class="profile-userpic">'+
                    '<img src="'+(params.profilMediumImageUrl||defaultImage)+'" class="img-responsive" alt=""> </div>'+
                '<div class="profile-usertitle">'+
                    '<div class="profile-usertitle-name"> '+(params.name?`<h4 class="title">${params.name}</h4>`:"")+' </div>'+
                    '<div class="profile-usertitle-job"> '+((params.id && costum.communityLinks && costum.communityLinks[params.id])?costum.communityLinks[params.id]:"")+' </div>'+
                '</div>'+
                '<div class="profile-userbuttons">'+
                    '<button type="button" class="btn btn-info  btn-sm">Follow</button>'+
                    '<button type="button" class="btn btn-info  btn-sm">Message</button>'+
                '</div>'+
            '</div>'+
        '</div>';
    return str;
}

directory.cards = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    var tags = params.tags?params.tags:[];
    tags = tags.slice(0,3).sort((a, b) => a.localeCompare(b));

    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    if(params.collection=="events" && params.startDate){
        dateAndTags = '<span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY HH:mm") +'</span>';
    }
    let linkedElement = "";
    if(params.parent || params.organizer){
        let p = params.organizer || params.parent;
        linkedElement = '<div style="position:absolute;  max-width:45%;font-weight:bold; top:35px; left: 30px;">'
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5 margin-bottom-5" style="background-color:var(--primary-color); color:white; display:inline-block; border-radius:4px">'+
    	            ov.name+
                '</a><br/>';
        }),
        linkedElement+="</div>"
    }
    let eTypes = [];
    if(params.collection=="events" && params.type){
        if(typeof params.type == "object"){
            $.each(params.type, function(eIndex, eType){
                if(typeof tradCategory[eType] != "undefined"){
                    eTypes.push(tradCategory[eType])
                }else{
                    eTypes.push(eType)
                }
            })
        }else if(typeof params.type == "string" && typeof tradCategory[params.type]!="undefined"){
            eTypes.push(tradCategory[params.type])
        }else{
            eTypes.push(params.type)
        }
    }

    mylog.log("eTypes", eTypes);

    str += '<div class="blog-item actus-item item-type1 col-xl-3 col-md-4 col-sm-6 col-xs-12">'+
        '<div class="item-inner">'+
        '<div class="image">'+
        '<img src="'+(params.profilMediumImageUrl||"https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg")+'" class="img-responsive" alt="" decoding="async">'+
        '</div>'+
        linkedElement+
        ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#f1c232; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px;" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
        ((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(eTypes.toString()||"")+'">'+(eTypes.toString()||"")+'</span>':"")+
        '<div class="item-body">'+
        '<div class="top-wrapper">'+
        '<span class="cat-date">'+dateAndTags+'</span>'+
        '<span class="category lowercase">'+(tags.map((v, i)=> "#"+v+(i==tags.length-1?"":", ")).join(""))+'</span>'+
        '</span>'+
        //'<span class="tag"></span>'+
        '</div>'+
        (params.name?`<h4 class="title">${params.name}</h4>`:"")+
        '<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
        '<div class="link-bordered">'+
        '<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'+//+(params.collection=="events")?"":""
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';
    return str;
}

directory.answers = function(params){
    mylog.log("directory actus", "Params", params);
    let str = "";
    let desc = jsonHelper.getValueByPath(params, "answers.ekitia25102023_918_0.ekitia25102023_918_0lo5d934pla097xvfvnk")||jsonHelper.getValueByPath(params, "answers.ekitia25102023_954_0.ekitia25102023_954_0lo5eiwo956j2a3f57u4")||jsonHelper.getValueByPath(params, "answers.ekitia25102023_942_0.ekitia25102023_942_0lo5e3czhfhla9u6o74m");
    if(!desc){
        desc="<i>Description non renseigné(s)</i>"
    }

    let linkedElement = "";
    if(params.form && typeof jsonHelper.getValueByPath(params, "links.organizations")!="undefined"){
        $.each(jsonHelper.getValueByPath(params, "links.organizations"), function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5" style="position:absolute; background-color:#ddd; color:var(--primary-color); font-weight:bold; top:35px; left: 30px; border-radius:4px">'+
    	            '<span class="ico-circle"><img width="40" height="30" src="'+baseUrl+'/upload/communecter/'+ov.type+'/'+oi+'/thumb/profil-resized.png"/></span> '+ov.name+
                '</a>';
        })
    }
    str += '<div class="blog-item item-type1 col-xl-3 col-md-4 col-sm-6">'+
        '<div class="item-inner">'+
            '<div class="image">'+
                '<img width="450" height="222" src="'+(params.profilMediumImageUrl||(modules.co2.url+"/images/logo-coform.png"))+'" class="img-responsive" alt="" decoding="async">'+
            '</div>'+
            linkedElement+
            '<div class="item-body">'+
                '<div class="top-wrapper">'+
                    '<span class="cat-date">'+
                        '<span class="date">'+(params.created?formatDate(params.source.date):formatDate(params.created))+' | </span>'+
                        '<span class="category">'+costum.lists.numericServiceForms[params.form].label+'</span>'+
                    '</span>'+
                    '<span class="tag"></span>'+
                '</div>'+
                '<div class="desc">'+desc.substring(0, 60)+'...</div>'+
                '<div class="link-bordered margin-top-5">'+
                    '<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '</div>';
    return str;
}

function addData(_context, params) {
    if (params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude) {
        if (!params.opt)
            params.opt = {}

        params.opt.icon = _context.getOptions().mapCustom.icon.getIcon(params)

        var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
        _context.setDistanceToMax(latLon);

        var marker = L.marker(latLon, params.opt);
        _context.getOptions().markerList[params.elt.id] = marker;

        if (_context.getOptions().activePopUp) {
            _context.addPopUp(marker, params.elt)
        }

        _context.getOptions().arrayBounds.push(latLon)

        if (_context.getOptions().activeCluster) {
            _context.getOptions().markersCluster.addLayer(marker)
        } else {
            marker.addTo(_context.getMap());
            if (params.center) {
                _context.getMap().panTo(latLon)
            }
        }

        if (_context.getOptions().mapOpt.doubleClick) {
            marker.on('click', function (e) {
                this.openPopup()
                coInterface.bindLBHLinks();
            })
            marker.on('dbclick', function (e) {
                _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                coInterface.bindLBHLinks();
            })
        } else {
            marker.on('click', function (e) {
                if (_context.getOptions().mapOpt.onclickMarker) {
                    _context.getOptions().mapOpt.onclickMarker(params)
                } else {
                    _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                    coInterface.bindLBHLinks();
                }
            });
        }
    }
}

paramsMapCO = $.extend(true, {}, paramsMapCO, {
    activeCluster:false,
    mapCustom: {
        icon: {
            getIcon: function (params) {
                var elt = params.elt;
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof elt.profilMediumImageUrl !== "undefined" && elt.profilMediumImageUrl != "")
                    imgProfilPath = baseUrl + elt.profilMediumImageUrl;
                var myIcon = L.divIcon({
                    className: "ekitia-div-marker",
                    iconAnchor: [35, 70],
                    iconSize: [70, 70],
                    labelAnchor: [-70, 0],
                    popupAnchor: [0, -70],
                    html: `<div style='background-color:var(--primary-color);' class='marker-ekitia'></div><img src="${imgProfilPath}">`
                });
                return myIcon;
            }
        },
        getClusterIcon: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            if (childCount < 100) {
                c += 'small-ftl';
            } else if (childCount < 1000) {
                c += 'medium-ftl';
            } else {
                c += 'large-ftl';
            }
            return L.divIcon({
                html: '<div>' + childCount + '</div>',
                className: 'marker-cluster' + c,
                iconSize: new L.Point(40, 40)
            });
        },
        addMarker: function (_context, params) {
            addData(_context, params);
            if(typeof params.elt.addresses != "undefined" && params.elt.addresses != null){
                params.elt.addresses.forEach((element, key) => {
                    var data = $.extend({}, params, true);
                    data.elt.address = element.address;
                    data.elt.geo = element.geo;
                    data.elt.geoPosition = element.geoPosition;
                    data.elt.id = data.elt.id + key;
                    addData(_context, data);
                })
            }
        }
    }
});

eventTypes = {
    "getTogether": "Get together",
    "conference": "Conference",
    "course": "Course",
    "meeting": "Meeting",
    "stand": "Stand",
    "RoundTable": "RoundTable",
    "workshop": "Workshop",
    "WorkingGroup": "WorkingGroup",
    "Webinar": "Webinar"
}

var lists = {
    "activity" : [
        "Administrations & Collectivités",
        "Aéro/Spacial",
        "Agriculture & Agronomie",
        "Banque/Assurance",
        "Bâtiment/BIM",
        "Communication",
        "Conseil",
        "Données",
        "Economie",
        "Emploi/Formation",
        "Energie",
        "Enseignement",
        "Environnement",
        "Géographie",
        "Intelligence Artificielle",
        "Juridique",
        "Logiciels",
        "Mobilité",
        "Recherche Scientifique",
        "Santé",
        "Télécom & Hébergement",
        "Tourisme",
        "Transformation Numérique",
        "Autres"
    ],
    "structure": {
        "Administration ou institution publique":"Administration ou institution publique",
        "Collectivité territoriale":"Collectivité territoriale",
        "Entreprise": "Entreprise",
        "Etablissement d'enseignement":"Etablissement d'enseignement",
        "Etablissement de recherche et santé":"Etablissement de recherche et santé",
        "Etablissement public ou chargé de missions de services publics":"Etablissement public ou chargé de missions de services publics",
        "Structures Publiques" : "Structures Publiques",
        "Pôles, Clusters ou Associations" : "Pôles, Clusters ou Associations",
        "Recherche" : "Recherche",
        "Académique": "Académique"
    }
}

costum.loadByHash = function(hash){
    $(".banner-outer").remove();
    return hash;
}

costum.lists["numericServiceForms"] = {
    "65389e3790b2f58f3503d8e2":{
        label: "Catalogue de données",
        icon:"fa-wpforms",
        value: "65389e3790b2f58f3503d8e2"
    },
    "6538adfc90b2f58f3503d8e3":{
        label: "Prestations de conseil et de services, expertises",
        icon:"fa-wpforms",
        value: "6538adfc90b2f58f3503d8e3"
    },
    "6538ae1a8312ccf36c042c94":{
        label: "Solutions logicielles, solutions numériques",
        icon:"fa-wpforms",
        value: "6538ae1a8312ccf36c042c94"
    },
    "6538aed38312ccf36c042c95":{
        label: "Formations",
        icon:"fa-wpforms",
        value: "6538aed38312ccf36c042c95"
    },
    "659ceff39c9ae3090c6b3dce":{
        label: "Algorithmes, modèles, réseaux de neurones, librairies",
        icon:"fa-wpforms",
        value: "659ceff39c9ae3090c6b3dce"
    },
    "65b21195ac117c27326a1858":{
        label: "Capacité de stockage ou de calcul",
        icon:"fa-wpforms",
        value: "65b21195ac117c27326a1858"
    }
}

costum.lists["charteEthique"] = {
    "Sciences des données et société" : {
        "Bienfaisance" : {
            description: "Le principe de bienfaisance et son corollaire, le principe de ne pas nuire, requièrent des Signataires qu’ils prennent en considération un objectif de bien-être collectif de l’humanité et de durabilité dans la conduite de leurs activités. Concrètement, le fait d’œuvrer pour le bien-être collectif se traduit par la conduite d’activités visant à améliorer le quotidien des générations actuelles et la qualité de vie du plus grand nombre. De manière complémentaire, le fait d’œuvrer dans une logique de durabilité se traduit par la prise en considération du bien commun des générations futures. Ainsi, les usages des Données mis en œuvre par les Signataires doivent contribuer autant que possible à la réalisation des Objectifs de Développement Durable adoptés par les 193 États membres de l’ONU. En outre, les Signataires s’assurent que les innovations qu’ils développent grâce aux Données apportent un réel progrès par rapport à des dispositifs existants ou des méthodes alternatives (y compris non numériques). De plus, les Signataires s’engagent à évaluer les retombées de leurs projets au regard des finalités de ceux-ci (cf. principe 5.3). Enfin, les Signataires favorisent la mise à disposition de Données pour des finalités d’intérêt général, dans les conditions prévues par la présente Charte et dans le respect des droits et libertés fondamentaux exercés dans une société démocratique."
        },
        "Innovation soutenable" : {
            description: "La conduite bienfaisante des activités des Signataires les mène à développer des innovations soutenables. Ainsi, tout projet de rupture technologique, sociale ou organisationnelle réalisé en tout ou partie grâce aux Données est mis en œuvre dans des conditions respectueuses de l’humain et de l’environnement. Dans cette optique, les Signataires portent une attention particulière à leur empreinte écologique en vue de la réduire. Pour ce faire, ils inscrivent leurs usages des Données dans une démarche de sobriété et de frugalité. La sobriété implique par exemple de limiter les traitements de Données aux finalités essentielles d’un projet, de privilégier des centres de données qui mettent en œuvre des mesures pour compenser leur impact environnemental, et d’être attentifs à l’effet rebond  des innovations. Quant à la frugalité, elle vise à élaborer des solutions efficientes, dépourvues de sophistication et de superflu, avec le moins de moyens possible mais sans faire de concession sur la qualité du service rendu . Dans la mesure du possible les Signataires évaluent l’impact environnemental, en particulier énergétique, de leurs projets dès la conception et tout au long du cycle de vie de ceux-ci (cf. principe 5.3). Enfin, afin de compenser leur éventuel impact négatif sur l’environnement, ils favorisent des projets à impact positif direct sur l’environnement."
        },
        "Solidarité, diversité et non-discrimination":{
            description: "Les Signataires veillent à ce que les projets qu’ils réalisent grâce aux Données n’aient pas pour conséquence de créer ou de renforcer des inégalités sociales, et sont attentifs à la prise en compte de la diversité de la société. Conformément au principe de non-discrimination, ils veillent également à ce que ces projets n’aient pas pour objet ou pour effet de créer, directement ou indirectement, une discrimination à l’encontre d’un individu ou d’un groupe d’individus, ou une forme de stigmatisation. A cette fin, ils sont vigilants quant aux biais discriminatoires susceptibles d’affecter les Données (cf. principe 3.1) et, le cas échéant, ceux susceptibles d’affecter les algorithmes utilisés pour traiter les Données. A l’aide d’une approche pluridisciplinaire, les Signataires élaboreront progressivement des stratégies visant à surmonter cette problématique de manière durable. Ils sont également vigilants quant aux questions de fracture numérique : ils évitent de creuser les différences de niveau d’équipement et de littératie numérique au sein de la population, et font en sorte que les dispositifs politiques et sociaux les plus importants soient accessibles quel que soit le degré d’accès aux outils numériques."
        },
        "Facteur humain" : {
            description: "Les Signataires tiennent compte du fait que tout projet s’intègre dans un système humanisé, composé d’agents compétents, au sein duquel la technologie n’est qu’un support de l’innovation. Dès la conception, leurs projets doivent être organisés de manière aussi pluridisciplinaire que possible, c’est-à-dire en mobilisant toutes les compétences nécessaires à l’exploitation des Données et à l’analyse des enjeux y étant liés, au niveau humain comme au niveau technologique. Les Signataires impliqués dans les projets mis en œuvre grâce aux Données s’inscrivent dans une logique de responsabilisation et de responsabilité éthique et juridique, de contrôle humain  de l’innovation et de garantie que toute prise de décision fondée sur l’utilisation de la technologie est opérée par des personnes humaines maîtrisant les outils technologiques et leurs risques. En ce sens, ils réfléchissent en amont du projet, à la répartition de leurs responsabilités au regard de leur implication dans le cycle de vie de celui-ci. Par ailleurs, les innovations qu’ils développent grâce aux Données n’annihilent pas la possibilité d’interagir avec un humain compétent."
        }
    },
    "Sciences des données et individu" : {
        "Respect et renforcement de l’autonomie humaine":{
            description: "Tout d’abord, les Signataires considèrent le respect de l’autonomie humaine comme l’élément central de leurs activités de traitement de données personnelles. En ce sens, lorsque la licéité d’un traitement de données personnelles dépend du recueil du consentement des personnes concernées, ils mettent en œuvre les meilleures pratiques pour leur permettre d’exprimer leurs choix de manière éclairée, spécifique et univoque. Cela inclut une information compréhensible pour des personnes non expertes. De plus, ils portent une attention particulière aux modalités du recueil du consentement des personnes vulnérables, notamment les mineurs, les personnes âgées et les personnes dépendantes.  En toute hypothèse, dès la collecte de données à caractère personnel et quelle qu’en soit la base légale, les responsables de traitement facilitent l’exercice par les personnes concernées des droits dont elles disposent sur leurs données. Ceci concerne notamment le droit à l’information (primordial du fait qu’il conditionne la possibilité d’exercer les autres), le droit de s’opposer au traitement de leurs données, le droit à l’effacement de leurs données, le droit d’accéder à leurs données et le droit à la portabilité de leurs données. Ce faisant, ils prennent en considération les particularités liées à la diversité des utilisateurs, à leurs contraintes et à leurs capacités, ainsi que leurs avis. Plus généralement, les Signataires veillent à préserver le libre-arbitre humain en ne mettant pas en œuvre des stratégies consistant à influencer le comportement ou les émotions des individus par des suggestions cachées, par la seule utilisation de leurs biais cognitifs (stratégies dites « de nudge » ). En outre, ils sont particulièrement attentifs à ne pas développer, grâce aux Données, des innovations susceptibles d’être utilisées à des fins de désinformation ou de manipulation."
        },
        "Respect de la vie privée" : {
            description: "Les Signataires se conforment aux règles applicables en matière de protection de la vie privée et des données personnelles. De manière complémentaire, la présente Charte prévoit dans ses différents principes des garanties nécessaires pour assurer le respect des droits des personnes en la matière. Les Signataires sont particulièrement attentifs à ce que la protection de la vie privée des individus soit garantie tout au long du « cycle de vie » des Données, notamment en prenant soin d’appliquer scrupuleusement les principes de minimisation, de protection par défaut et par conception, et de définir pour chaque traitement de données personnelles une durée de conservation adéquate. Les Signataires sont conscients que le croisement de Données (même anonymisées) augmente considérablement le risque de réidentification des individus à l’issue de leur traitement. En ce sens ils s’engagent à déterminer et à appliquer, au cas par cas et de manière proportionnée à la sensibilité des Données concernées, les techniques qu’ils estiment être les plus adéquates pour optimiser la protection de la vie privée"
        }
    },
    "Qualité des données et sécurité du système d’information": {
        "Qualité des données":{
            description: "La qualité des Données est un élément essentiel et déterminant de la qualité des résultats de leur traitement. Dès lors, les Signataires s’efforcent de prendre toutes les mesures qu’ils considèrent nécessaires pour optimiser la qualité des Données qu’ils produisent et qu’ils utilisent ; par exemple en s’assurant de leur pertinence,notamment de leur fiabilité et de leur représentativité pour l’usage envisagé. Les biais dans les Données servant à l’entraînement et à l’apprentissage des systèmes algorithmiques doivent être systématiquement recherchés et, dans la mesure du possible, éliminés. En France, il existe des données de référence14 ayant vocation à servir plusieurs usages, dont la qualité est garantie par des critères spécifiques et stables permettant de construire des services et des analyses fiables et durables. Les Signataires privilégient donc l’utilisation de ces données de référence lorsque celles-ci s’avèrent pertinentes."
        },
        "Sécurité des données et des centres de données":{
            description: "Afin de protéger les Données d’attaques physiques ou virtuelles susceptibles de compromettre leur disponibilité, leur intégrité et leur confidentialité, les Signataires privilégient des centres de données situés sur le territoire de l’UE et respectueux de normes de sécurité adaptées au regard de la nature des Données et des usages envisagés. Ils prennent notamment les mesures techniques, juridiques et organisationnelles nécessaires afin de se conformer aux règles européennes relatives à l’accès et au transfert international de Données détenues dans l’UE. Ils prennent soin d’appliquer des mesures de cybersécurité adaptées à la confidentialité des Données traitées au sein de leur infrastructure. Le recours à des prestataires tiers ne doit pas diminuer la capacité de gestion confidentielle des Données.            Leur vigilance est accrue dès lors qu’ils sont en présence de données à caractère personnel sensibles , de données liées à la sécurité publique nationale ou de données protégées par des exigences de confidentialité (telles que des données commerciales, des données couvertes par le secret statistique, des données protégées par des droits de propriété intellectuelle)."
        },
        "Robustesse des algorithmes" : {
            description: "Les Signataires qui envisagent de traiter les Données à l’aide d’un système algorithmique choisissent un modèle suffisamment robuste au regard de la finalité du traitement des Données. La robustesse d’un algorithme dépend du caractère fiable  et reproductible  des résultats qu’il permet d’obtenir, ces caractères étant influencés par la qualité du modèle algorithmique ainsi que par la qualité des Données d’entraînement (cf. principe 3.1). Concernant spécifiquement les modèles qui continuent à évoluer suite à leur entraînement grâce à un apprentissage en continu, leur robustesse dépend également de la qualité de toutes les Données qui y sont entrées et devrait donc faire l’objet d’une surveillance tout au long de son cycle de vie. En toute hypothèse, les Signataires s’efforcent de parvenir à une marge d’erreur proportionnée à la finalité d'utilisation du système. Lorsqu’un système algorithmique a vocation à être utilisé dans le contexte d’un processus décisionnel, les Signataires garantissent que la prise de décision sera in fine assurée par un opérateur humain informé des capacités et des limites du système (cf. principe 1.4)."
        }
    },
    "Transparence": {
        "Information claire et accessible": {
            description: "De manière générale, les Signataires s’efforcent d’apporter, par les moyens qu’ils estiment appropriés, une information éclairée et accessible aux citoyens quant aux progrès et aux risques susceptibles d’être générés par la science des données. En vue d’inspirer la confiance, et dans le respect du secret industriel, du secret des affaires, du secret défense, du secret professionnel et des droits et intérêts des individus, les Signataires fournissent une information claire et accessible, en tenant compte du public concerné, quant aux innovations développées grâce aux Données, notamment concernant les éléments suivants : les Données utilisées, leur lieu d’hébergement, la méthode d’analyse de ces Données et la finalité de cette analyse. Lorsque c’est possible, ils précisent également aux utilisateurs finaux le contexte optimal et les prérequis techniques ou organisationnels liés à l’utilisation des outils qu’ils mettent sur le marché ou qu’ils mettent en service."
        },
        "Explicabilité des algorithmes":{
            description: "Les Signataires qui utilisent des systèmes algorithmiques afin de traiter des Données veillent à documenter les processus de prise de décision de ces systèmes en vue de le rendre intelligible auprès de différents publics (en interne, pour des autorités de contrôle, des experts indépendants, ou encore leurs utilisateurs). Pour ce faire, lorsque les Signataires ont recours à des systèmes algorithmiques déterministes pour analyser les Données, ils prennent soin de détailler leur méthode de programmation (c’est-à-dire la méthode par laquelle ils ont intégré au système les règles préétablies par un opérateur humain). Lorsqu’ils ont recours à des systèmes algorithmiques intégrant des techniques d’apprentissage, qui posent aujourd’hui des difficultés d’explicabilité, ils devraient au minimum expliquer la logique générale de leur fonctionnement (c’est-à-dire les Données d’entrée (d’entraînement et d’apprentissage), l’objectif de l’analyse de ces Données, et les variables déterminantes dans le processus de prise de décision). Ils peuvent pour ce faire s’inspirer des méthodes utilisées par les acteurs publics qui sont soumis à une obligation de transparence concernant les systèmes algorithmiques qu’ils utilisent afin d’appuyer des prises de décisions administratives individuelles concernant des personnes physiques ou morales20. De plus, les Signataires qui utilisent des systèmes algorithmiques difficilement explicables pour traiter les Données démontrent au "
        },
        "Auditabilité" : {
            description: "Les Signataires reconnaissent l’importance de faciliter le contrôle de la conformité des activités qu’ils mènent grâce aux Données au cadre légal qui s’applique à eux et, dans la mesure du possible, aux règles qu’ils mettent en œuvre afin d’appliquer la présente Charte. Ainsi, chaque étape d’un projet réalisé grâce aux Données est documentée, ces documents étant destinés à fournir des informations ou à servir de base à une évaluation ou à un contrôle : - Chaque participant au projet veille à conserver une description des Données qu’il a apportées ; - Chaque participant veille à assurer, en amont du projet, la traçabilité de ses Données par des mécanismes permettant de recenser et de détailler toutes les transformations effectuées ; - En cas d’utilisation d’algorithmes pour traiter les Données, au moins un participant prend soin de conserver une description de leur fonctionnement ;- Chaque participant documente les évaluations des impacts et des risques qu’il a réalisées et, pour les projets à fort impact sociétal, rend ces documents accessibles au public. Dans le cas où les Signataires décideraient de soumettre un projet à une évaluation, un contrôle ou un audit, ils favorisent des experts indépendants."
        }
    },
    "Gouvernance des données dans un cadre de confiance" : {
        "Évaluation des bénéfices et des risques" : {
            description : "Les Signataires appliquent le principe de précaution  dès le stade de la conception et tout au long de la mise en œuvre de leurs projets intégrant des traitements de Données. Ils s’efforcent d’évaluer les risques, directs ou indirects, susceptibles de découler de ces projets sur ce qui constitue leur écosystème, c’est-à-dire les individus, la société et l’environnement. Au regard des résultats révélés par les évaluations des risques, les Signataires cherchent à maximiser les effets bénéfiques et à minimiser les effets défavorables de leurs projets, tant à l’échelle individuelle qu’à l’échelle collective. Enfin, dans la mesure du possible et lorsque cela apparaît pertinent, les Signataires expérimentent les innovations développées grâce au traitement de Données à petite échelle, ou dans le cadre de « bacs à sable » , avant de les déployer."
        },
        "Inclusion des citoyens et des utilisateurs finaux" : {
            description: "De manière générale les Signataires offrent aux citoyens les moyens de développer leur littératie numérique. De manière plus ciblée, ils impliquent les futurs utilisateurs des solutions développées dans la conception et dans la mise en œuvre de leurs projets. Cette inclusion est réfléchie au stade de la conception et pour l’accessibilité des produits et services développés, notamment pour répondre aux attentes des personnes concernées à propos des modalités de traitement de leurs données et de l’exercice de leurs droits liés à ce traitement. Ce principe inclut la mise en place de moyens de communication spécifiques, appropriés et effectifs, tels que des consultations ou des processus de co-construction, à l’image de ceux mis en œuvre pour l’élaboration de la présente Charte."
        },
        "Apprentissage collectif" : {
            description: "Les Signataires privilégient des modes de gouvernance multipartite des projets, permettant de représenter dans les processus décisionnels les différents acteurs impliqués et concernés (pouvant notamment inclure les utilisateurs finaux ou les citoyens). Ces modes de gouvernance collaborative multipartite peuvent notamment s’inspirer du concept de commun numérique. Les Signataires assurent une veille quant à l’évolution des bonnes pratiques relatives aux usages Données et, dans le cadre d’un projet collaboratif, les partagent avec leurs collaborateurs. Les Signataires soulignent l’importance de la collaboration entre les différentes disciplines impliquées dans un projet, comprenant les domaines techniques et les sciences humaines et sociales du fait de la diversité des enjeux soulevés par l’usage des Données. En lien avec le mouvement de Science ouverte  et l’élaboration de communs numériques, les Signataires favorisent le partage des Données qu’ils produisent sur la base du principe « autant ouvert que possible, aussi fermé que nécessaire »  visant à maximiser le bénéfice des connaissances qui en découlent tout en respectant leur confidentialité (secret des affaires, propriété intellectuelle…). En ce sens, dans la mesure du possible, les Signataires privilégient une mise à disposition en accès ouvert (open access) en utilisant des licences adaptées. Enfin, les Signataires s’engagent à échanger régulièrement avec Ekitia sur l’évolution de ces bonnes pratiques et à faire remonter les difficultés rencontrées quant à la mise en œuvre d’un ou de plusieurs principes de la présente Charte."
        },
        "Intégrité" : {
            description :"Toute personne intervenant dans un projet réalisé grâce aux Données respecte les règles déontologiques auxquelles elle est soumise et agit dans un esprit d’intégrité intellectuelle et de coopération. Cela couvre tant les finalités de ces travaux que la méthode utilisée, la gestion des ressources humaines, la diffusion des connaissances et la communication scientifique. En outre les Signataires s’abstiennent d’utiliser les Données à des fins de falsification, de plagiat ou de rétention illégitime. Par ailleurs, les Signataires s’engagent à respecter un principe de loyauté au regard de la finalité des traitements de Données. Enfin, les Signataires qui souhaiteraient développer des systèmes d’IA générative veillent spécifiquement à ne pas les entraîner avec des Données protégées par des droits de propriété intellectuelle sans avoir préalablement obtenu l’accord explicite des détenteurs de ces droits."
        }
    },
    "Réciprocité" : {
        "Reconnaissance" : {
            description : "Dans le contexte d’un projet collaboratif entre plusieurs Signataires, chaque contribution essentielle à la réalisation de ce projet est explicitement reconnue et rendue publique (ce dans la limite de la confidentialité et des accords entre les collaborateurs). Ces contributions s’apparentent notamment à la production et à la fourniture de Données, à la fourniture d’algorithmes ou à la fourniture de travaux de recherche ayant permis d’impulser le projet et sur lesquelles repose son bon déroulement."
        },
        "Répartition équitable de la création de valeur" : {
            description : "Les Signataires reconnaissent que la création de valeur, qu’elle soit sociale, financière ou technique, ne doit pas être accaparée par un ou des acteurs dominants. Ainsi, lors d’un projet impliquant un traitement collaboratif de Données, d’algorithmes ou de recherches scientifiques, les Signataires recherchent des modèles économiques permettant un juste retour à chaque partie y ayant contribué."
        }
    }
}

function formatDate(d){
    let date = new Date(d);
    let newdate= "";
    if(!isNaN(date.getMonth())){
        newdate = moment(date).format('DD MMMM YYYY');
    }else{
        newdate = moment(d).format('DD MMMM YYYY');
    }
    return newdate;
}

Object.assign(costum.lists, lists);

costum[costum.slug] = {
    activeContext: null,
    listedPrincipe : {},
    init: function(){
        // var stylecolor= ":root{ --primary-color: #ba1c24; --secondary-color:#F8B030 }";
        var primaryColor = "#ba1c24";
        var secondColor = "#F8B030";
        var color3 = "#358562";
        var color4 = "red";
        var color5 = "black";
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color1 !="undefined"){
            primaryColor = costum.css.color["color1"]; 
        }
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color2 !="undefined"){
            secondColor = costum.css.color["color2"];
        }
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color3 !="undefined"){
            color3 = costum.css.color["color3"];
        }
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color4 !="undefined"){
            color4 = costum.css.color["color4"];
        }
        if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color5 !="undefined"){
            color4 = costum.css.color["color5"];
        }
        stylecolor = ":root{ --primary-color: "+primaryColor+"; --secondary-color:"+secondColor+"; --color3:"+color3+"; --color4:"+color4+"; --color5:"+color5+" }"
        $("style").append(stylecolor);
        if(userId == "" && location.hash!="#mentions" && location.hash!="#confidentialityRules" && location.hash!="#charte"){
            Login.openLogin();
        }

        if(userId!="" && location.hash=='#panel.box-register'){
            window.location.href = baseUrl+window.location.pathname;
        }

        costum[costum.slug].getOrganizations();
        $("#authclientCo").parent().remove(); // To do: add params to check if external service needed
        if(costum.slug=="ekisphere"){
            $(".cosDyn-logo").each(function(){
                $(this).attr("src", baseUrl+assetPath+"/images/costumize/ekitia/EKISPHEREWHITE.png");
            })
        }

        if(localStorage.getItem("IhaveSeenCookieInfoEkisphere")==null){
            costum[costum.slug].cookieInfo.initView();
            costum[costum.slug].cookieInfo.initEvent();
        }
    },
    cookieInfo: {
        initView:function(){
            $("body").append(`
                <div id="cookie-notice" role="dialog" class="cookie-revoke-hidden cn-position-bottom cn-effect-fade cookie-notice-visible" aria-label="Cookie Notice" style="background-color: rgba(33,33,33,1);">
                    <div class="cookie-notice-container" style="color: #ffffff">
                        <span id="cn-notice-text" class="cn-text-container">Ekisphère et Communecter déposent uniquement des cookies utiles au bon fonctionnement du site. Pour tout renseignement complémentaire, veuillez vous référer à notre page de politique de protection des données personnelles. Bonne visite</span><span id="cn-notice-buttons" class="cn-buttons-container">
                        <button id="cn-accept-cookie" data-cookie-set="accept" class="cn-set-cookie cn-button" aria-label="OK" style="background-color: #ba1c24">OK</button>
                        <a href="https://www.ekitia.fr/politique-de-protection-des-donnees/" target="_blank" id="cn-more-info" class="cn-more-info cn-button" aria-label="Politique de protection des données personnelles" style="background-color: #ba1c24">Politique de protection des données personnelles</a></span>
                        <span id="cn-close-notice" data-cookie-set="accept" class="cn-close-icon" title="Non"></span>
                    </div>
                </div>
            `)
        },
        initEvent: function(){
            $("#cn-accept-cookie").on("click", function(){
                localStorage.setItem("IhaveSeenCookieInfoEkisphere", true)
                $("#cookie-notice").remove();
            })
        }
    },
    events:{
        formData: function(data){
            data["preferences"] = {
                "isOpenData" : false,
                "isOpenEdition" : false
            }
            return data;
        }
    },
    projects: {
        formData: function(data){
            data["preferences"] = {
                "isOpenData" : false,
                "isOpenEdition" : false
            }
            return data;
        }
    },
    organizations: {
        formData: function(data){
            $.each(data, function(e, v){
                if(typeof costum.lists[e] != "undefined"){
                    if(notNull(v)){
                        if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
                        if(typeof v == "string"){
                            data.tags.push(v);
                        }else{
                            $.each(v, function(i,tag){
                                data.tags.push(tag);
                            });
                        }
                    }
                    delete data[e];
                }

                if(e === "structure"){
                    if(v === "Entreprise"){
                        data.type = "LocalBusiness";
                    }else if(v === "Structures Publiques"){
                        data.type = "GovernmentOrganization";
                    }else if(v.indexOf("Association") >= 0){
                        data.type = "NGO";
                    }else{
                        data.type = "Cooperative";
                    }
                }

                if (e === "extraActivity") {
                    $.each(v, function (k, d) {
                        data.tags.push(d.activity);
                    })
                    delete data[e];
                }
                if (e === "linkedin") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["linkedin"] = v;
                    delete data[e];
                }
                if(e.indexOf("admin") >= 0){
                    data.contacts = typeof data.contacts != "undefined" ? data.contacts : {};
                    const index = e.replace("admin", "").toLowerCase();

                    data.contacts[index] = v;
                    delete data[e];
                }
            });

            data["preferences"] = {
                "isOpenData" : false,
                "isOpenEdition" : false
            }

            data.tags.splice(data.tags.indexOf("Autres"), 1);
            data.role = "admin";
            if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
                data.preferences = typeof data.preferences != "undefined" ? data.preferences : {};
                data.preferences.toBeValidated = {};
                data.preferences.toBeValidated[costum.slug] = true;
            }
            return data;
        },
        afterBuild: function(){
            $(".extraActivitylists").hide();
            alignInput2(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties ,"admin",4,12,0,null,"Contact(s)","#ba1c24","");
            $("#activity").off("change").on("change", function(){
                if($(this).val().includes("Autres")){
                    $(".extraActivitylists").show();
                }else{
                    $(".extraActivitylists").hide();
                }
            })
        },
        afterSave: function(data){
            dyFObj.commonAfterSave(data, function(response){
                var dataMembers = {
                    id: costum.contextId,
                    collection: costum.contextType,
                    path: "links.members",
                    value: typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" ? costum.communityLinks.members : {}
                }
                dataMembers.value[response.id] = {type: response.map.collection};
                if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
                    dataMembers.value[response.id].toBeValidated = true;
                }
                dataHelper.path2Value(dataMembers, function(){
                    var dataMemberOf = {
                        id: response.id,
                        collection: response.map.collection,
                        path: "links.memberOf",
                        value: {}
                    }
                    dataMemberOf.value[costum.contextId] = {type: costum.contextType};
                    if(!(typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined")){
                        dataMemberOf.value[costum.contextId].toBeValidated = true;
                    }
                    dataHelper.path2Value(dataMemberOf, function(){

                    })
                })
            })
        }
    },
    tools: {
        formData: function(data){
            data["preferences"] = {
                "isOpenData" : false,
                "isOpenEdition" : false
            }
            if(typeof data.contentReusability == "undefined"){
                data.contentReusability = [];
            }
            if(typeof data.mainThematic == "undefined"){
                data.mainThematic = [];
            }
            if(typeof data.ethicCharte == "undefined"){
                data.ethicCharte = [];
            }
            if(typeof data.tags == "undefined"){
                data.tags = [];
            }
            return data;
        },
        afterBuild: function(){
            alignInput2(costum.typeObj.tools.dynFormCostum.beforeBuild.properties ,"admin",4,12,0,null,"Qui contacter en cas de besoin de précision sur l'outil ?","#777","");
        }
    },
    openInModal : function(url, type=null, context){
        let myContext = {_id:{$id:context.id}, profilThumbImageUrl:context.img, ...context};
        let sourceKey = {}
        if(type!=null){
            let defaultValue = {};
            defaultValue[context.id] = myContext;
            console.log("open in modal -- here" , defaultValue);
            if(type == "event"){
                let defautDyFInputsInit = dyFInputs.init;
                dyFInputs.init = function(){
                    typeObj[type].dynForm.jsonSchema.properties.shortDescription = dyFInputs.textarea(tradDynForm.shortDescription, "...", {})
                    defautDyFInputsInit()
                }
                dyFObj.openForm(type, null, {organizer: defaultValue});
            }else{
                dyFObj.openForm(type, null, {parent: defaultValue});
            }
        }else if(url.indexOf("#")>=0){
            let ctx = "";
            if(url.indexOf("answer")>=0){
                ctx = ".contextId."+context.id+".contextType."+context.type;
            }
            urlCtrl.loadByHash(url+ctx, true);
        }else{
            if(userId != ""){
                smallMenu.open(`<div id="contentHtml" class="container"><div>`);
                costum[costum.slug].postNumericService("#contentHtml", url, "formulaire", myContext)
            }
        }
    },
    beforePost : function(url, type=null){
        let titleForm="<h4>Sélectionner la structure pour la publication</h4>";
		var dialog = bootbox.dialog({
		    title: titleForm,
		    message: '<div id="finderSelectHtml" style="max-height:480px;">'+
		    			//'<input class="form-group form-control padding-5" type="text" id="populateFinder" placeholder="context"/>'+
						'<div id="list-my-context" class="padding-5" style="max-height:380px; overflow-y:auto; border: 1px solid #eee">'+
                            '<div id="list-loader"></div>'+
                        '</div>'+
						'<div class="col-md-12 col-sm-12 col-xs-12 text-center">'+
                        '</div>'+
		    		'</div>',
		    closeButton:true,
		    buttons: {
			    cancel: {
			        label: trad.Close,
			        className: 'btn-default',
			        callback: function(){
			            dialog.modal('hide');
			        }
			    }
			}
		});
		dialog.on('shown.bs.modal', function(e){
			if(userId != ""){
                coInterface.showLoader("#list-loader");
				let params = {
                    "searchType":["organizations"],
                    "notSourceKey":true,
                    "filters" : {
                        "$or":{
                            "source.keys":costum.slug, 
                            "reference.costum":costum.slug
                        }
                    }
                };
                params.filters["links.members."+userId] = {'$exists':true};
                ajaxPost(
					null,
					baseUrl+"/"+moduleId+"/search/globalautocomplete",
					params,
					function(data){
                        mylog.log("Here we go modal", data);
                        $("#list-my-context").empty();
                        let img = modules.co2.url + "/images/thumb/default_citoyens.png";
                        $.each(data.results, function(index, item){
                            if(item.profilMediumImageUrl){
                                img = item.profilMediumImageUrl;
                            }
                            var str ="<div class='population-elt-finder population-elt-finder-"+index+" col-xs-12 padding-5' style='border-bottom:1px solid #eee' data-id='"+index+"' data-type='"+item.collection+"' data-img='"+img+"' data-name='"+item.name+"'>"+
                                "<div class='element-finder element-finder-"+index+"'>"+
                                    '<img src="'+ img +'" class="thumb-send-to pull-left img-circle" height="50" width="50">'+
                                    '<span class="info-contact pull-left margin-left-20">' +
                                        '<span class="name-element text-dark text-bold" data-id="'+index+'">' + item.name + '</span>'+
                                        '<br/>'+
                                        '<span class="type-element text-light pull-left">' + trad[item.type]+ '</span>'+
                                    '</span>' +
                                "</div>"+
                            "</div>";
                            $("#list-my-context").append(str);
                        });
                        dialog.find(".population-elt-finder").on("click", function(e){
                            dialog.modal("hide");
                            costum[costum.slug].openInModal(url, type, {id:$(this).data("id"), type:$(this).data("type"), name:$(this).data("name"), img:$(this).data("img")});
                        })
                        if($(".population-elt-finder").length==1){
                            $(".population-elt-finder").click();
                        }else if($(".population-elt-finder").length==0){
                            $("#list-my-context").append("<h3 class=''>Vous devez être admin d'une structure pour pouvoir en ajouter</h3>");
                            $(".bootbox div.modal-header").hide();
                        }
					},
					null,
					"json",
					{
						async: true
					}
				);
			}
		});
    },
    postNumericService : function(formContainer, formId, name, context){
        var formAnswerId = function (formId, descKey) {
            var dataId = "";
            ajaxPost(
                null,
                baseUrl + `/survey/answer/newanswer/form/${formId}`,
                {
                    "action": "new"
                },
                function (data) {
                    if (data._id) {
                        dataId = data._id.$id;
                        var dataToSendLinks = {
                            id: data._id.$id,
                            collection: "answers",
                            path: "links.organizations."+context._id.$id,
                            value : {
                                name : context.name,
                                type : context.type
                            } 
                        }
                        dataHelper.path2Value(dataToSendLinks, function (params) {});
                    }
                },
                null,
                "json",
                { async: false }  
            )	
            return dataId;
        };
        var renderForm = function (formId, answerId, mode="w") {
            var formAjaxPath = baseUrl + '/survey/answer/index/id/' + answerId + '/form/' + formId + '/mode/' + mode;
            var renderHtml = "";
            ajaxPost(
                null, 
                formAjaxPath,
                { 
                    url : window.location.href 
                },
                function(data){
                    renderHtml = data;
                },
                "html",
                null,
                { async: false } 
            );
            return renderHtml;
        }
        $(formContainer).html(renderForm(formId, formAnswerId(formId, name)));
    },

    afterInviteOrgaTeam: function(inv){
        mylog.log("after invite", inv)
        if(inv && typeof inv.listInvite != "undefined" && typeof inv.listInvite.citoyens!="undefined"){
            $.each(inv.listInvite.citoyens, function(ci, citoyen){
                links.connectAjax(costum.contextType, costum.contextId, ci, "citoyens", null, null, null);
            })
        }
    },

    getOrganizations: async function(){
        let params = {
            "searchType":["organizations"],
            "notSourceKey":true,
            "fields": ["name"],
            "filters" : {
                "$or":{
                    "source.keys":costum.slug, 
                    "reference.costum":costum.slug,
                }
            },
            indexStep: 0
        };
        ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/search/globalautocomplete",
            params,
            function(data){
                if(data.results){
                    costum.lists["communities"] = {}
                    $.each(data.results, function(index, org){
                        costum.lists["communities"][index] = org.name;
                    })
                }
            },
            null,
            "json",
            {
                async: false
            }
        );
    }
}

costum.checkboxSimpleEvent = {
    true : function(id){
        if(id=="isEthicCharte"){
            $("#ajax-modal .ethicCharteselectMultiple").show();
        }
    },
    false : function(id){
        if(id=="isEthicCharte"){
            $("#ajax-modal .ethicCharteselectMultiple").hide();
        }
    }
}

function canSubmit(can){
    $(".createBtn").prop("disabled", !can);
    if(can){
        $(".createBtn").removeClass("btn-default text-black");
        $(".createBtn").addClass("btn-success text-white");
    }else{
        $(".createBtn").addClass("btn-default text-black");
        $(".createBtn").removeClass("btn-success text-white");
    }
}

// User register form modal
Login.runRegisterValidator  = function(params) { 
    var form4 = $('.form-register');
    var errorHandler3 = $('.errorHandler', form4);
    var createBtn = null;
    $(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");

    if($("#fonction").length==0){
        $("#modalRegister .form-register-inputs div:eq(5)").append(`
            <div class="telephoneRegister">
                <label class="letter-black"><i class="fa fa-phone"></i> Téléphone </label><br>
                <input class="form-control" id="telephone" name="telephone" placeholder="numéro téléphone"><br/>
            </div>

            <div class="fonctionRegister">
                <label class="letter-black"><i class="fa fa-chevron-down"></i> Précisez votre fonction actuelle </label><br>
                <input class="form-control" id="fonction" name="fonction" placeholder="votre fonction"/>
                <br/>
            </div>
        `);
    }

    $("label[for='agree'] .agreeMsg").html(`
        Je suis d'accord avec 
        <a href="https://communecter.org/#mentions" target="_blank" class="bootbox-spp text-dark">
            les mentions légales                                </a>
        et                             
        <a href="#confidentialityRules" class="bootbox-spp text-dark" target="_blank">
        La politique de confidentialité</a>
    `);

    $("label[for='charte'] .agreeMsg").html(`
        J'ai pris connaissance <a href="#charte" target="_blank" class="bootbox-spp text-dark">
        Les conditions générales d’utilisation</a>
    `);

    canSubmit(false);
    $("#agree, #charte").on("change", function(){
        if($("#agree").is(":checked") && $("#charte").is(":checked") && form4.valid()){
            canSubmit(true);
            errorHandler3.hide();
        }else{
            canSubmit(false);
        }
    })
    
    form4.validate({
        onfocusout:function(el, ev){
            var validator = $('.form-register').validate();
            
            if(document.getElementById("password3")==el){
                var pattern = new RegExp(
                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$"
                );
                if(!pattern.test($("#password3").val())){
                    validator.showErrors({
                        "password3": `<div class="margin-left-10">Le mot de passe doit conténir au minimum 12 caractères, 1 minuscule, 1 majuscule, 1 chiffre et un caractère spécial</div>`
                    });
                    canSubmit(false);
                }else{
                    validator.element(el);
                }
            }else{
                validator.element(el);
            }

            if($("#agree").is(":checked") && $("#charte").is(":checked") && form4.valid()){
                canSubmit(true);
                errorHandler3.hide();
            }else{
                canSubmit(false);
            }
        },
        rules : {
            name : {
                required : true,
                minlength : 4
            },
            telephone : {},
            fonction : {
                required : true
            },
            username : {
                required : true,
                validUserName : true,
                rangelength : [4, 32]
            },
            email3 : {
                required : { 
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email : true,
            },
            password3 : {
                minlength : 12,
                required : true
            },
            passwordAgain : {
                equalTo : "#password3",
                required : true
            },
            agree:{
                required:true
            },
            charte:{
                required:true
            }
        },

        submitHandler : function(form) {

            errorHandler3.hide();
            $(".createBtn").prop('disabled', true);
            $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
            var params = { 
                "name" : $('.form-register #registerName').val(),
                "username" : $('.form-register #username').val(),
                "email" : $(".form-register #email3").val(),
                "fonction" : $(".form-register #fonction").val(),
                "telephone" : $(".form-register #telephone").val(),
                "pendingUserId" : pendingUserId,
                "pwd" : $(".form-register #password3").val()
            };
            if($('.form-register #isInvitation').val())
                params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
                params.scope = scopeObj.selected;
            }
            if($(".form-register #newsletter").is(":checked"))
                params.newsletter=true;
            if($(".form-register #newsletterCollectif").is(":checked"))
                params.newsletterCollectif=true;
            var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
            
            //if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
            //	params.msgGroup=$(".form-register #textMsgGroup").val();
            ajaxPost(
                null,
                baseUrl+"/costum/ekitia/register",
                params,
                function(data){ 
                    if(data.result) {
                        mylog.log("Register Formulaire",data.result);
                        toastr.success("Merci, votre compte a bien été créé");
                        $('.modal').modal('hide');
                        if(params.isInvitation){
                            window.location.href = baseUrl+window.location.pathname;
                        }else{
                            $("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                            $("#modalRegisterSuccess").modal({ show: 'true' }); 
                            
                            $('#modalRegisterSuccess .btn-default').click(function() {
                                mylog.log("hide modale and reload");
                                $('.modal').modal('hide');
                                if(typeof thisElement == "undefined"){
                                    window.location.reload();
                                }
                            });
                        }
                    }else {
                        toastr.error(data.msg);
                        $('.modal').modal('hide');	    		  	
                        scopeObj.selected={};
                    }
                },function(data) {
                    toastr.error(trad["somethingwentwrong"]);
                    $(".createBtn").prop('disabled', false);
                    $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                }
            );
            return false;
        },
        invalidHandler : function(event, validator) {//display error alert on form submit
            errorHandler3.show();
            $(".createBtn").prop('disabled', false);
            $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
        }
    });
};
Login.runRegisterValidator();

/*
function isLastCommunecterTabInBrowser() {
    var tabs = window.open("", "_self").window;
    var currentTabUrl = window.location.href;
    var isLastTabWithUrl = true;
    for (var i = 0; i < tabs.length; i++) {
        if (tabs[i] !== window) {
            if (tabs[i].location.href === currentTabUrl) {
                isLastTabWithUrl = false;
                break;
            }
        }
    }
    return isLastTabWithUrl;
}

window.addEventListener("beforeunload", function(event) {
    event.stopImmediatePropagation();
    if(!event.reload){
        if (isLastCommunecterTabInBrowser() && userId!="") {
            ajaxPost(null, baseUrl+"/"+moduleId+"/person/logout")
        }
    }
})*/