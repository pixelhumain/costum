function request_link_event_address(args) {
	return new Promise(function (resolve, reject) {
		var url = baseUrl + '/costum/coevent/get_events/request/link_tl_to_event';
		ajaxPost(null, url, args, resolve, reject);
	})
}
var defaultCoevent = {
	onload: {
		actions: {
			hide: {
				parentfinder: 1,
				publiccheckboxSimple: 1,
				recurrencycheckbox: 1,
				organizerNametext: 1,
				formLocalityformLocality: 1
			}
		}
	},
	beforeBuild: {
		properties: {
			type: {
				options: eventTypes
			},
			organizer: {
				label: 'Quel tier-lieu organise cet événement',
				initType: ['organizations'],
				initBySearch: true,
				initMe: false,
				initContacts: false,
				multiple: false,
				initContext: false,
				rules: {
					required: true
				},
				noResult: {
					label: 'Créez votre tiers lieux',
					action: function () {
						window.open('https://cartographie.francetierslieux.fr/', '_blank');
					}
				},
				typeAuthorized: ['organizations'],
				filters: {
					$or: {
						'source.keys': 'franceTierslieux',
						'reference.costum': 'franceTierslieux',
					}
				}
			}
		}
	},
	afterBuild: function () {
		$(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
	},
	afterSave: function (data) {
		var callbackParams = {
			parent: {},
			links: {
				attendees: {}
			}
		};
		callbackParams.parent[coevent_parent.id] = {
			type: coevent_parent.type
		};
		callbackParams.links.attendees[userId] = {
			type: 'citoyens'
		};
		var params = {
			id: data.id,
			collection: data.map.collection,
			path: 'allToRoot',
			value: callbackParams
		};
		link_parent = {
			id: coevent_parent.id,
			collection: coevent_parent.type,
			path: 'links.subEvents.' + data.id,
			value: { type: 'events' }
		}
		dataHelper.path2Value(link_parent, function () { });
		request_link_event_address({
			event: data.id,
			tl: Object.keys(data.map.organizer)[0]
		}).then();
		dyFObj.commonAfterSave(null, function () {
			dataHelper.path2Value(params, function () {
				$('.coevent-program').trigger('coevent-filter');
				$('.theme-selector').each(function () {
					var self = $(this);
					self.trigger('block-update');
				});
			});
		});
	}
};
