function initForm() {

	formDefi = formObj.init();
	var contextId = costum.contextId;
	var contextType = costum.contextType;

	//contextType=contextData.collection;
	//formObj.events.addproposition(formDefi);  
}

function lazyForm() {
	mylog.log("init costum", costum);

	setTimeout(initForm, 5000);
}

function getDashboardData(option = "", page = "", callBack = null, scopeId = null) {
	var params = {
		"costumId": costum.contextId,
		"costumSlug": costum.contextSlug,
		"costumType": costum.contextType,
		"specificBlock": ["custom.franceTierslieux.graph.answerByMembers"],
		"page": page,
		"scopeId": scopeId,
		"countByQuestion": true,
		"extraFilter": { "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s": { $exists: true } }
	}
	//if(extraFilter!=null){
	//	params["extraFilter"] = extraFilter;
	//}
	ajaxPost(
		null,
		baseUrl + "/costum/blockgraph/getdashboarddata" + option,
		params,
		function (res) {
			costum["dashboardData"] = res["data"];
			costum["dashboardGlobalConfig"] = res["global"];
			if (typeof callBack == "function") {
				callBack(res);
			}
		},
		null,
		null,
		{ async: false }
	)
}

costum[costum.slug] = {
	init: function () {
		$("#menuTopLeft .lbh-menu-app[href$='#add-answer']").attr("data-hash", "#answer.index.id.new.form.6438366673d20a0de1533c77");
		lazyLoad(modules.survey.url + "/js/form.js", null, lazyForm);
		getDashboardData();
	},
	costumColors : {
		primaryColor: "#43C9B7",
		primaryDark: "#3b9387",
		secondaryColor: "#4623c9",
	},
	organisations : {
		formData : function (data) {
			if (data.Partenaires) {
				delete data.Partenaires;
			}
			return data
		}
	},
	tls: {
		formData: function (data) {
			//alert("formdata");
			mylog.log("formData In", data);

			if (typeof finder != "undefined" && Object.keys(finder.object).length > 0) {
				$.each(finder.object, function (key, object) {
					if (typeof formData[key] != "undefined") {
						delete formData[key];
					}
				});
			}


			//if(dyFObj.editMode){
				$.each(data, function(e, v){
					//alert(e, v){
					if(typeof costum.lists[e] != "undefined"){
						if(notNull(v)){
							
							if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
							if(typeof v == "string"){
								// var newT="";
								// if(e=="manageModel" && v=="Autre"){
								// 	newT="Autre mode de gestion";
								// }
                                // if(e=="typePlace" && v=="Autre"){
                                //     newT="Autre famille de tiers-lieux";
                                // }
								data.tags.push(v);
								
							}else{
								$.each(v, function(i,tag){
									// var newT="";
									// if(e=="typePlace" && tag=="Autre"){
									// 	newT="Autre famille de tiers-lieux";
									// }
                                    // if(e=="manageModel" && tag=="Autre"){
                                    //     newT="Autre mode de gestion";
                                    // }
									data.tags.push(tag);	
								});
							}
						}
						delete data[e];
					}
			    });
			if (typeof data.category != "undefined" && data.category == "network") {
				if (typeof data.tags == "undefined" || !notNull(data.tags)) data.tags = [];
				data.tags.push("Réseau de tiers-lieux");
				// delete data.mainTag;
			}

            if(typeof data.telephone == "string"){
				var mapIndicTel={
					"FR" : "33",
					"GP" : "590",
					"MQ" : "596",
					"GF" : "594",
					"RE" : "262",
					"PM" : "508",
					"YT" : "269",
					"WF" : "681",
					"PF" : "689",
					"NC" : "687"
				};

				var phone=data.telephone;
				var numIndex=2;
				phone=phone.replace(/\s/g,'');
				// remplacer 00 par +
				phone=(phone.substring(2,0)=="00") ? "+"+phone.substring(2) : 
				    ((phone.substring(0,1)!="+" && typeof data.address!="undefined" && typeof data.address.addressCountry!="undefined" && typeof mapIndicTel[data.address.addressCountry]!="undefined") 
					   ? "+"+mapIndicTel[data.address.addressCountry]+phone.substring(1) : phone );
				if(phone.substring(0,1)=="+"){
					if(phone.substring(1,3)=="33"){
						numIndex=4;
					}else{
						numIndex=5;
					}
				}
				
				var mobOrfixeInd=phone.substring(numIndex,numIndex-1);
				var typeNumber= (mobOrfixeInd=="6" || mobOrfixeInd=="7") ? "mobile" : "fixe" ;	
			    data.telephone={};
				data.telephone[typeNumber]=[phone];
			}

			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				// delete data.mainTag;
			}
            // if (data.tags.includes("Autre") || data.tags.includes("Autres")) {
            //     $.each(data.tags, function (i, tag) {
            //         if (tag=="Autre" || tag=="Autres") {
            //             delete data.tags[i];
            //         }
            //     });
            // }

			if (data.socialNetwork) {
				var formatedSocialNetwork={};
                $.each(data.socialNetwork,function(i,net){
					var labelArray=net["platform"].split(" ");
  
					var nameSocNet=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["url"]!="undefined" || net["url"]!=""){
                        formatedSocialNetwork[nameSocNet]=net["url"];						
					}
					delete data.socialNetwork[i];

				});
                data.socialNetwork=formatedSocialNetwork;
			}

			mylog.log("formData return", data);
			return data;
		},
		afterSave: function (data) {
			dyFObj.commonAfterSave(data, function () {
				mylog.log("callback aftersaved", data);
				$("#ajax-modal").modal('hide');
			});
		},
		afterBuild: function (data) {
			mylog.log("afterbuild FTL data", data);
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad = function (data) {
				mylog.log("afterload FTL data", data);
				// cache la carto
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				dyFObj.formInMap.bindStreetResultsEvent = function () {
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
						// only one address (no push);
						dyFInputs.locationObj.elementLocations = [];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if (!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/) {
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
					dyFObj.formInMap.valideLocality = function (country) {
						mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

						if (notEmpty(dyFObj.formInMap.NE_lat)) {
							locObj = dyFObj.formInMap.createLocalityObj();
							mylog.log("formInMap copyMapForm2Dynform", locObj);
							dyFInputs.locationObj.copyMapForm2Dynform(locObj);
							// dyFInputs.locationObj.addLocationToForm(locObj);
						}

						// dyFObj.formInMap.initVarNE();
						dyFObj.formInMap.resumeLocality();

						dyFObj.formInMap.btnValideDisable(false);
						toastr.success("Adresse enregistrée");

						dyFObj.formInMap.initHtml();
						// dyFObj.formInMap.newAddress(false);
						$("#btn-submit-form").prop('disabled', false);
						// $('#ajaxFormModal #divNewAddress').show();


					};

					$("#ajaxFormModal .item-street-found").off().click(function () {
						if (typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0) {
							dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
							setTimeout(function () { dyFObj.formInMap.mapObj.map.setZoom(17) }, 1000);
							$("#divMapLocality").removeClass("hidden");
							$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
						}
						var streetAddressName = $(this).text().split(",")[0].trim();

						$('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);

						dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();

						mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
						$("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
						$('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
						$('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));


						dyFObj.formInMap.NE_lat = $(this).data("lat");
						dyFObj.formInMap.NE_lng = $(this).data("lng");
						dyFObj.formInMap.showWarningGeo(false);

						//Valider au click du résultat
						dyFObj.formInMap.valideLocality();

						$("#ajaxFormModal #sumery").show();
						// $("#ajaxFormModal #divMapLocality").show();

						dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
							var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
							mylog.log('marker drop event', latLonMarker);
							dyFObj.formInMap.NE_lat = latLonMarker.lat;
							dyFObj.formInMap.NE_lng = latLonMarker.lng;

							dyFObj.formInMap.valideLocality();
							toastr.success("Coordonnées mises à jour");
						});

					});

				};

				if (Object.keys(data).length == 0 || typeof data.address == "undefined" || (typeof data.address != "undefined" && Object.keys(data.address).length == 0)) {
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				} else if (typeof data.address != "undefined" && Object.keys(data.address).length > 0) {
					$('#ajaxFormModal #divNewAddress').hide();
				}

			};
			mylog.log("afterbuild", data);
			if(typeof data.manageModel=="undefined" || (typeof data.manageModel!="undefined" && !data.manageModel.includes("Autre mode de gestion"))){
					// alert("showthos");
				$("#ajaxFormModal .extraManageModeltext").hide();
			}
			if(typeof data.typePlace=="undefined" || (typeof data.typePlace!="undefined" && !data.typePlace.includes("Autre famille de tiers-lieux"))){
				$("#ajaxFormModal .extraTypePlacetext").hide();
			}

			$("#divCity input").attr("placeholder", "Saisir votre code postal");


			$("#ajaxFormModal .manageModelselect select").change(function () {
				var modelStatus=$(this).val();
			    if(modelStatus!="Autre mode de gestion"){
			    //    $("#ajaxFormModal .extraManageModeltext").hide();
			       $("#ajaxFormModal .extraManageModeltext input").val("");
				   $("#ajaxFormModal .extraManageModeltext").hide();
			    }
			    //alert(modelStatus);
			    var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre mode de gestion"	: "Group"
			    }; 
			    $("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});


			$("#ajaxFormModal .typePlaceselect select").change(function () {
				var placeCat=$(this).val();
			    if(placeCat==null || placeCat.includes("Autre famille de tiers-lieux")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(placeCat.includes("Autre famille de tiers-lieux")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function () {
				//alert("change");
				//var prevTypePlace = $(this).data('typePlace');
				var themeNetwork = $(this).val();
				if (themeNetwork.includes("Autres") == false) {
					$("#ajaxFormModal .extraThemeNetworktext").hide();
					$("#ajaxFormModal .extraThemeNetwork input").val("");
				}
				if (themeNetwork.includes("Autres")) {
					$("#ajaxFormModal .extraThemeNetworktext").show();
					//$("#ajaxFormModal .extraTypePlacetext input").focus();
				}
			});

			$("#ajaxFormModal .localNetworkselect select").change(function () {
				//alert("change");
				//var prevTypePlace = $(this).data('typePlace');
				var localNetwork = $(this).val();
				if (localNetwork.includes("Autres") == false) {
					$("#ajaxFormModal .extraLocalNetworktext").hide();
					$("#ajaxFormModal .extraLocalNetwork input").val("");
				}
				if (localNetwork.includes("Autres")) {
					$("#ajaxFormModal .extraLocalNetworktext").show();
					//$("#ajaxFormModal .extraTypePlacetext input").focus();
				}
			});


			//init openingHours
			if(typeof data.openingHours!="undefined"){
			    $.each(openingHoursResult, function(e,v){
      				mylog.log("custom init openinghours",data.openingHours,e);
      				if(typeof data.openingHours[e]=="undefined" || !notEmpty(data.openingHours[e]) ){
      					openingHoursResult[e].disabled=true;
      				}
      			});	      
			}
			$(".btn-select-day").off().on("click", function () {
				mylog.log("selectDay", openingHoursResult, $(this).data("key"));
				key = $(this).data("key");
				if ($(this).hasClass("active")) {
					$(this).removeClass("active");
					$.each(openingHoursResult, function (e, v) {
						if (v.dayOfWeek == key)
							openingHoursResult[e].disabled = true;
					});
					$("#contentDays" + key).fadeOut();
				} else {
					$(this).addClass("active");
					$.each(openingHoursResult, function (e, v) {
						if (v.dayOfWeek == key)
							delete openingHoursResult[e].disabled;
					});
					$("#contentDays" + key).fadeIn();
				}
			});

			$('.timeInput').off().on('changeTime.timepicker', function (e) {
				mylog.log("changeTimepicker");
				var typeInc = $(this).data("type");
				var daysInc = $(this).data("days");
				var hoursInc = $(this).data("value");
				var firstEnabled = null;
				var setFirst = false
				$.each(openingHoursResult, function (i, v) {
					if (!setFirst && typeof openingHoursResult[i].disabled == "undefined") {
						mylog.log("firtEnable", v.dayOfWeek);
						firstEnabled = v.dayOfWeek;
						setFirst = true;
					}
					if (firstEnabled == daysInc) {
						mylog.log("first enabled change causes recursive change", v.dayOfWeek);
						openingHoursResult[i]["hours"][hoursInc][typeInc] = e.time.value;
						var typeHours = "";
						if (typeInc == "opens") {
							typeHours = "start";
						} else if (typeInc == "closes") {
							typeHours = "end";
						}
						if (typeHours != "") {
							$("#" + typeHours + "Time" + v.dayOfWeek + hoursInc).val(e.time.value);
						}
					} else if (v.dayOfWeek == daysInc) {
						openingHoursResult[i]["hours"][hoursInc][typeInc] = e.time.value;
					}
				});
			});

			$('.timeInput').on("focus", function () {
				$(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();
		}
	}
};
costum.lists = {
	"level" : {
		"level1" : "National",
		"level3" : "Régional",
		"level4" : "Départemental",
		"level5" : "Communauté de communes"
	},
	"typePlace" : [ 
		"Ateliers artisanaux partagés", 
		"Bureaux partagés / Coworking", 
		"Cuisine partagée / Foodlab", 
		"Fablab / Makerspace / Hackerspace (Espaces du Faire)", 
		"LivingLab / Laboratoire d'innovation sociale", 
		"Tiers-lieu nourricier", 
		"Tiers-lieu culturel / Lieux intermédiaires et indépendants", 
		"Autre famille de tiers-lieux"
	],
	"services" : [ 
		"Accompagnement des publics", 
		"Action sociale", 
		"Aiguillage / Orientation", 
		"Bar / café", 
		"Boutique / Épicerie", 
		"Coopérative d'Activités et d'Emploi / Groupement d'employeur", 
		"Cantine / restaurant", 
		"Centre de ressources", 
		"Chantier participatif", 
		"Complexe évènementiel", 
		"Conciergerie ", 
		"Domiciliation", 
		"Espace de stockage", 
		"Espace détente", 
		"Espace enfants", 
		"Formation / Transfert de savoir-faire / Éducation", 
		"Habitat", 
		"Incubateur", 
		"Lieu d'éducation populaire et nouvelles formes d'apprentissage", 
		"Maison de services au public", 
		"Marché", 
		"Média et son", 
		"MediaLab", 
		"Médiation numérique", 
		"Offre artistique ou culturelle", 
		"Pépinière d'entreprises", 
		"Point d'appui à la vie associative", 
		"Point Information Jeunesse", 
		"Point d'information touristique", 
		"Pratiques de soin (art thérapie, massage, médecine douce, méditation...)", 
		"Résidences d'artistes", 
		"Ressourcerie / recyclerie", 
		"Service enfance-jeunesse", 
		"Services liés à la mobilité"
	],
	"manageModel" : [ 
		"Association", 
		"Collectif citoyen", 
		"Universités / Écoles d’ingénieurs ou de commerce / EPST", 
		"Établissements scolaires (Lycée, Collège, Ecole)", 
		"Collectivités (Département, Intercommunalité, Région, etc)", 
		"SARL-SA-SAS", 
		"SCIC", 
		"SCOP", 
		"Autre mode de gestion"
	],
	"state" : [ 
		"En projet", 
		"Ouvert", 
		"Fermé"
	],
	"spaceSize" : [ 
		"Moins de 60m²", 
		"Entre 60 et 200m²", 
		"Plus de 200m²"
	],
	"network" : [ 
		"A+ C'est Mieux", 
		"Actes‐IF", 
		"Artfactories/Autresparts", 
		"Bienvenue dans la Canopée", 
		"Cap Tiers‐Lieux", 
		"Cédille Pro", 
		"Collectif des Tiers‐Lieux Ile‐de‐France", 
		"Collectif Hybrides", 
		"Compagnie des Tiers‐Lieux", 
		"Coopérative des Tiers‐Lieux", 
		"Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
		"Cowork'in Tarn", 
		"Coworking Grand Lyon", 
		"CRLII Occitanie", 
		"GIP Recia", 
		"Hub France Connectée", 
		"L'ALIM (l'Assemblée des lieux intermédiaires marseillais)", 
		"La Trame 07", 
		"Label Tiers‐Lieux Occitanie", 
		"Label C3", 
		"Label Tiers‐Lieux Normandie", 
		"Le DOG", 
		"Le LIEN", 
		"Lieux Intermédiaires en région Centre", 
		"Réseau des tiers‐lieux Bourgogne Franche Comté", 
		"Réseau Français des Fablabs", 
		"Réseau Médoc", 
		"Réseau TELA", 
		"Tiers‐Lieux Edu"
	],
	"regionalNetwork" : [ 
		"Réseau Relief (Auvergne Rhône-Alpes)", 
		"Réseau Tiers-lieux Bourgogne-France-Comté (BFC)", 
		"Réseau Bretagne Tiers-Lieux (Bretagne)", 
		"Réseau Ambition Tiers Lieux (Centre-Val-de-Loire)", 
		"Réseau Da Locu (Corse)", 
		"Réseau Tiers-Lieu Grand Est", 
		"Réseau La Compagnie des Tiers-Lieux (Hauts-de-France)", 
		"Réseau Ile de France Tiers Lieux (Consortium réunissant A+ c’est mieux, Actifs, le Collectif des Tiers-lieux, Makers IDF)", 
		"Réseau TILINO (Normandie)", 
		"Réseau La Coopérative Tiers-Lieux (Nouvelle Aquitaine)", 
		"Réseau La Rosée (Occitanie)", 
		"Réseau CAP Tiers-Lieux (Pays-de-la-Loire) et la CRESS Pays de la Loire", 
		"Réseau Sud Tiers-Lieux (Provence Alpes Côte d’Azur)", 
		"Réseau La Réunion des Tiers-Lieux (La Réunion)", 
		"Réseau régional des Tiers-lieux de Martinique", 
		"Réseau régional des Tiers-lieux de Guadeloupe"
	],
	"themeNetwork" : [ 
		"Artfactories/Autresparts", 
		"Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)", 
		"Réseau Français des Fablabs, Espaces et Communautés du Faire (RFFLabs)", 
		"Tiers-Lieux Edu", 
		"Réseau National des Ressourceries et Recycleries", 
		"Réseau Cocagne", 
		"Réseau des CREFAD", 
		"Réseau des Cafés Culturels et Cantines Associatifs (RECCCA)", 
		"Les tiers-lieuses", 
		"Autres"
	],
	"localNetwork" : [ 
		"Actes-If - Lieux intermédiaires et indépendants en Ile de France", 
		"A+ c’est mieux !", 
		"Cédille Pro", 
		"Collectif Hybrides", 
		"Collectif des Tiers-Lieux Ile-de-France", 
		"Coworking Grand Lyon", 
		"Cowork’in Tarn", 
		"La Trame 07", 
		"AliiCe - Association des Lieux Intermédiaires indépendants en région Centre", 
		"Réseau Médoc", 
		"Réseau TELA", 
		"RedLab (Réseau des Labs d'Occitanie)", 
		"Makers IDF", 
		"Le LIEN", 
		"CRLii Occitanie", 
		"L’ALIM (l’Assemblée des lieux intermédiaires marseillais) / Collectif indéterminé", 
		"Autre"
	],
	"hubFranceConnecte" : [ 
		"Hub du Sud", 
		"Hubik", 
		"Les Assembleurs", 
		"MedNum Bourgogne-Franche-Comté", 
		"Hub PiNG", 
		"Hub Ultra Numérique", 
		"Hub AURA", 
		"Hubert", 
		"Hub Ile-de-France", 
		"Hub Occitanie", 
		"Hub Antilles-Guyane", 
		"Autres"
	],
	"greeting" : [ 
		"Conseil et orientation avec contact téléphonique", 
		"Point d'échange physique proposé pour monter en compétences respectivement", 
		"Voyage apprenant pour découverte de lieux", 
		"Accueil d'élus pour acculturation", 
		"Visioconférence de présentations", 
		"Étude d'opportunité", 
		"Aide à la réalisation de dossiers pour AMI ou subventions", 
		"Conseil projet", 
		"Incubation de projets", 
		"Mentoring / coaching de porteurs de projet", 
		"Parcours de formation pour des porteurs de projet"
	],
	"certification" : [ 
		"Fabrique de Territoire", 
		"Fabrique Numérique de Territoire"
	],
	"compagnon" : [ 
		"Compagnon France Tiers-Lieux"
	],
	"territory" : [ 
		"En agglomérations", 
		"En métropole", 
		"En milieu rural", 
		"En ville moyenne (entre 20000 et 100000 habitants)"
	]
}
function request_link_event_address(args) {
	return new Promise(function (resolve, reject) {
		var url = baseUrl + '/costum/coevent/get_events/request/link_tl_to_event';
		ajaxPost(null, url, args, resolve, reject);
	})
}
var defaultCoevent = {
	onload: {
		actions: {
			hide: {
				parentfinder: 1,
				publiccheckboxSimple: 1,
				recurrencycheckbox: 1,
				organizerNametext: 1,
				formLocalityformLocality: 1
			}
		}
	},
	beforeBuild: {
		properties: {
			type: {
				options: eventTypes
			},
			organizer: {
				label: 'Quel tier-lieu organise cet événement',
				initType: ['organizations'],
				initBySearch: true,
				initMe: false,
				initContacts: false,
				multiple: false,
				initContext: false,
				rules: {
					required: true
				},
				noResult: {
					label: 'Créez votre tiers lieux',
					action: function () {
						window.open('https://cartographie.francetierslieux.fr/', '_blank');
					}
				},
				typeAuthorized: ['organizations'],
				filters: {
					$or: {
						'source.keys': 'franceTierslieux',
						'reference.costum': 'franceTierslieux',
					}
				}
			}
		}
	},
	afterBuild: function () {
		$(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
	},
	afterSave: function (data) {
		var callbackParams = {
			parent: {},
			links: {
				attendees: {}
			}
		};
		callbackParams.parent[coevent_parent.id] = {
			type: coevent_parent.type
		};
		callbackParams.links.attendees[userId] = {
			type: 'citoyens'
		};
		var params = {
			id: data.id,
			collection: data.map.collection,
			path: 'allToRoot',
			value: callbackParams
		};
		link_parent = {
			id: coevent_parent.id,
			collection: coevent_parent.type,
			path: 'links.subEvents.' + data.id,
			value: { type: 'events' }
		}
		dataHelper.path2Value(link_parent, function () { });
		request_link_event_address({
			event: data.id,
			tl: Object.keys(data.map.organizer)[0]
		}).then();
		dyFObj.commonAfterSave(null, function () {
			dataHelper.path2Value(params, function () {
				$('.coevent-program').trigger('coevent-filter');
				$('.theme-selector').each(function () {
					var self = $(this);
					self.trigger('block-update');
				});
			});
		});
	}
};
