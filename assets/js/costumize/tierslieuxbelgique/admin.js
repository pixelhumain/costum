adminDirectory.values.updated = function(e, id, type, aObj){
    mylog.log("adminDirectory.values.updated", id, type);
    //mylog.log(e);
    var updated="";
    if(typeof e.updated != "undefined"){
        ts=new Date(e.updated*1000);
        updated=ts.toLocaleDateString();//moment(e.created).local().locale('fr').format('DD/MM/Y');
    }
    var str = '<span>'+updated+'</span>';
    return str;
};

adminPanel.views.tiersLieux = function(){
	var data={
		title : "Gestion des Tiers-lieux",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations"]
            },
            filters : {
                text : true
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            created: {
                name : "Créé le",
                sort : {
                	field : "created",
                	up : true,
                	down : true
                }
            },
            updated: {
                name : "Mis à jour le",
                sort : {
                	field : "updated",
                	up : true,
                	down : true
                }
            },
            /*description : {
            	name : "Description",
            	class : "col-xs-2 text-center"
            },
            private : { 
            	name : "Affichage",
        		class : "col-xs-1 text-center"
        	}*/
        },
        actions : {
            /*private : true,*/
            delete : true
        },
        csv : [
            {
                name : "Tous les TLs",
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                    indexStep : 0,
                    fields : [
                        "name","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","geo.latitude","geo.longitude"
                    ]
                }
            }/*,
            {
                name : "TL mis à jour depuis le 20 mars 2023",
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                        indexStep : 0,
                        fields : [
                            "name","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","geo.latitude","geo.longitude"
                        ],
                        extraFilters : {
                            "updated" : {
                                "$gt" :  1679270400
                            }
                        }
                }
            }*/
        ]
    };
		
		
	
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.communitys = function(){
    var data={
        title : trad.community,
        context : {
            id : costum.contextId,
            collection : costum.contextType
        },
        invite : {
            contextId : costum.contextId,
            contextType : costum.contextType,
        },
        table : {
            name: {
                name : "Membres"
            },
            email : {
                name : "Email"
            },
            tobeactivated : {
                name : "Validation de compte",
                class : "col-xs-2 text-center"
            },
            isInviting : {
                name : "Validation pour être membres",
                class : "col-xs-2 text-center"
            },
            roles : {
                name : "Roles",
                class : "col-xs-1 text-center"
            },
            admin : {
                name : "Admin",
                class : "col-xs-1 text-center"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "citoyens","organizations"],
                fields : [ "name","slug", "email", "links", "collection","hasRC","username","roles"],
                notSourceKey : true,
                indexStep: 50
            },
            filters : {
                text : true,
                types : {
                    lists : ["citoyens" ,"organizations"]
                }
                
            }
        },
        actions : {
            openchat : true,
            admin : true,
            roles : true,
            disconnect : true
        }
    };

    data.paramsFilter.defaults.filters = {};

    if(costum.contextType=="projects"){
        data.paramsFilter.defaults.filters["links."+costum.contextType+"."+costum.contextId] = {
            '$exists' : 1 
        }
    }else{
        data.paramsFilter.defaults.filters["links.memberOf."+costum.contextId] = {
            '$exists' : 1 
        };
    }
    adminDirectory.actions.openchat = function(e, id, type, aObj){    
        chatSlug = (e.slug) ? e.slug : ""; 
        chatUsername = (e.username) ? e.username : "";	  
        str = '<button class="btn btn-xs btn-default col-xs-12 btn-open-chat pull-right contact'+id+'" '+
        					'data-name-el="'+e.name+'"  data-slug="'+chatSlug+'" data-username="'+chatUsername+'" data-type-el="'+type+'"  data-open="'+( (typeof e.preferences != "undefined" && e.preferences.isOpenEdition)?true:false )+'"  data-hasRC="'+( ( e.hasRC )?true:false)+'" data-id="'+id+'">'+
        				'<i class="fa fa-comments"></i> Discussion </button>'
        return str ;
    }; 

    var searchAdminType=(typeof paramsAdmin != "undefined" 
    && typeof paramsAdmin["menu"] != "undefined" && typeof paramsAdmin["menu"]["community"] != "undefined"
    && typeof paramsAdmin["menu"]["community"]["initType"] != "undefined") ? paramsAdmin["menu"]["community"]["initType"]: ["citoyens"];
    data.paramsFilter.defaults.types=searchAdminType; 
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory', data, function(){
          ajaxPost(null, baseUrl+'/'+moduleId+'/admin/mailing', {}, function(memberMailingHtml){
              $('#content-view-admin').append(memberMailingHtml);
          },"html");
      },"html");

    adminDirectory.bindCostum = function(aObj){
        $("#"+aObj.container+"  .btn-open-chat").off().on("click", function(){
            var nameElo = $(this).data("name-el");
            var idEl = $(this).data("id");
            var usernameEl = $(this).data("username");
            var slugEl = $(this).data("slug");
            var typeEl = dyFInputs.get($(this).data("type-el")).col;
            var openEl = $(this).data("open");
            var hasRCEl = ( $(this).data("hasRC") ) ? true : false;
            var ctxData = {
                name : nameElo,
                type : typeEl,
                id : idEl
            }
            if(typeEl == "citoyens")
                ctxData.username = usernameEl;
            else if(slugEl)
                ctxData.slug = slugEl;
            rcObj.loadChat(nameElo ,typeEl ,openEl ,hasRCEl, ctxData );
        });
        
        $("#"+aObj.container+" .adminBtn").off().click(function(e){
            var thisObj=$(this);
            var id = $(this).data("id");
            var type = $(this).data("type");
            var elt = aObj.getElt(id, type) ;
            var connectType = ( (typeof aObj.context != "undefined" && typeof aObj.context.collection != "undefined") ? links.linksTypes["citoyens"][aObj.context.collection] : "members" ) ;
            var isAdmin = $(this).data("isadmin");
            mylog.log("isAdmin", isAdmin, "elt",elt);
            var params = {
                parentId : aObj.context.id,
                parentType : aObj.context.collection,
                   childId : id,
                childType : type,
                connect : connectType,
                isAdmin : (isAdmin === true ? false : true )
            };
            if(notEmpty(elt.links) && notEmpty(elt.links[connectType])&& notEmpty(elt.links[connectType][params.parentId])){
                var linkOption=(notEmpty(elt.links[connectType][params.parentId]["isInviting"])) ? "isInviting" : (notEmpty(elt.links[connectType][params.parentId]["toBeValidated"])  ? "toBeValidated" : null);
                if(notNull(linkOption)){
                    links.validate(params.parentType, params.parentId, params.childId, params.childType, linkOption);

                }
                
            }
                ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/link/updateadminlink/",
                params,
                function(data){ 
                      mylog.log("success data", data);			        
                    if(isAdmin === true)
                        isAdmin = false ;
                    else
                        isAdmin = true ;

                    if( typeof aObj.context != "undefined" &&
                        typeof aObj.context.id != "undefined" &&
                        typeof aObj.context.collection != "undefined" &&
                        typeof elt.links != "undefined" && 
                        typeof links != "undefined" && 
                        typeof links.linksTypes != "undefined" &&
                        typeof links.linksTypes["citoyens"] != "undefined" &&
                        typeof links.linksTypes["citoyens"][aObj.context.collection] != "undefined" &&
                        typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]] != "undefined" && 
                        typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id] != "undefined" && 
                        typeof elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id]){
                        elt.links[links.linksTypes["citoyens"][aObj.context.collection]][aObj.context.id].isAdmin = isAdmin ;
                    }

                    aObj.setElt(elt, id, type) ;
                    $("#"+type+id+" .admin").html( aObj.values.admin(elt, id, type, aObj));
                    thisObj.replaceWith( aObj.actions.admin(elt, id, type, aObj) );
                    aObj.bindAdminBtnEvents(aObj);
                  },
                  function(data){
                      $("#searchResults").html("erreur");
                  }
              ); 
        });
    }
}

adminPanel.views.importContact = function(){
	
	ajaxPost('#content-view-admin', baseUrl+'/costum/lacompagniedestierslieux/importcontact', null, function(){},"html");
};

adminPanel.views.mobilisationTL = function(){
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', {page:"costum.views.custom.reseauTierslieux.mobilisationTL"}, function(){},"html");
};


adminPanel.views.events = function(){
	var data={
		title : "Les formations",
		table : {
            name : {
                name : "Nom"
            },
            shortDescription : {
                name : "Description courte"
            },
            organizer: {
                name : "Structure organisatrice"
            },
            email : {
                name : "Contact animateur"
            },
            template :{
                name : "Atelier récurrent"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            header : {
                dom : ".headerSearchContainerAdmin",
               options : {
                   left : {
                       classes : 'col-xs-8 elipsis no-padding',
                       group:{
                           count : true
                       }
                   },
                   right : {
                       classes : 'col-xs-4 text-right no-padding',
                       group : { 
                           add : {
                               label : "Ajouter un atelier"
                           }
                       }
                   }
               }
            },
            defaults : {
                types : [ "events" ],
                fields : [ "name", "organizer", "collection", "email", "tags", "shortDescription" ],
                filters : {}
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
        	delete : true
        }
	};
    data.paramsFilter.defaults.filters["organizer."+costum.contextId]={"$exists":1};

    
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.projects = function(){
	var data={
        container : "formation",
		title : "Les projets",
		table : {
            name : {
                name : "Nom"
            },
            shortDescription : {
                name : "Description courte"
            },
            session : {
                name : "Sessions de formation"
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            header : {
                dom : ".headerSearchContainerAdmin",
               options : {
                   left : {
                       classes : 'col-xs-8 elipsis no-padding',
                       group:{
                           count : true
                       }
                   },
                   right : {
                       classes : 'col-xs-4 text-right no-padding',
                       group : { 
                           add : {
                               label : "Ajouter un atelier"
                           }
                       }
                   }
               }
            },
            defaults : {
                types : [ "projects" ],
                fields : [ "name", "organizer", "collection", "email", "tags", "shortDescription" ],
                filters : {}
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
        	delete : true
        }
	};
    data.paramsFilter.defaults.filters["parent."+costum.contextId]={"$exists":1};
    
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

if(costum.slug=="laCompagnieDesTierslieux"){
    adminPanel.views.addData=function(){
        ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/adminpublic/adddata', null, function(){
            // alert("afterload");
            $("#sumitVerification").attr("id","customSumitAddData");
            $("#customSumitAddData").off().on('click', function(e){
                if($("#chooseElement").val() == "-1"){
                    toastr.error("Vous devez sélectionner un élément");
                    return false ;
                }
                else if(file == ""){
                    toastr.error("Vous devez sélectionner un fichier");
                    return false ;
                }
      
                $.blockUI({
                  message : "<h1 class='homestead text-red'><i class='fa fa-spin fa-circle-o-notch'></i> Processing ...</h1>"
              });
                
                var params = {
                      file : file,
                      typeElement : $("#chooseElement").val()
              };
      
              
              if($("#isLink").val() == "true"){
                  params["isLink"] = true;
                  params["typeLink"] = $("#chooseElementLink").val();
                  params["idLink"] = $("#idSearchEntity").val();
                  params["roleLink"] = $("#roleLink").val();
              }
      
                ajaxPost(
                  null,
                  baseUrl+"/costum/reseautierslieux/adddataindb",
                  params,
                  function(data){ 
                      console.log("data",data);
                      var chaine = "";
                      var csv = '"name";"info";"url"' ;
                      if(typeof data.resData != "undefined"){
                          $.each(data.resData, function(key, value2){
                              chaine += "<tr>" +
                                          "<td>"+value2.name+"</td>"+
                                          "<td>"+value2.info+"</td>"+
                                          "<td>"+(notNull(value2.url) ? baseUrl+value2.url : "")+"</td>"+
                                          "<td>"+value2.type+"</td>"+
                                          "<td>"+(notNull(value2.id) ? baseUrl+value2.id : "")+"</td>"+
                                      "</tr>";
                              csv += "\n";
                              csv += '"'+value2.name+'";"'+value2.info+'";"'+baseUrl+value2.url+'";"'+value2.type+'";"'+value2.id+'";' ;
                              
                          });
                        }
                        
                      $("<a />", {
                          "download": "Data_a_verifier.csv",
                          "href" : "data:application/csv," + encodeURIComponent(csv)
                        }).appendTo("body")
                        .click(function() {
                           $(this).remove()
                        })[0].click() ;
      
                        $("#bodyResult").html(chaine);
                      $.unblockUI();
                  },
                  function(data){
                        mylog.log("error",data);
                        $.unblockUI();
                    }
              );
                       
      
              
              
              return false;
                
            });
        },null,null,{async:false});
    };
}