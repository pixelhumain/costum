pageProfil.views.detail = function() {
    var url = "/co2/element/about/type/"+contextData.type+"/id/"+contextData.id;
    if (contextData.type == "organizations"){
        $("#social-header").css("display","none");
        $("#menu-top-profil-social").css("display","none");
        url = '/costum/institutbleu/elementhome/type/' + contextData.type + "/id/" + contextData.id;
    }
    if(contextData.type == "citoyens") url+="/costum/view/costum.views.custom.institutBleu.element.person.detail"; 
    ajaxPost('#central-container', baseUrl+'/'+url, null, function(){},"html");
};
pageProfil.views.structure = function(){
    family = {};
    if(costum && typeof costum.lists != "undefined" && typeof costum.lists.family != "undefined"){
        Object.assign(family, costum.lists.family);
    }
    var str = `
        <div class="structureSearchContent">
            <div id="filter-nav" class="searchObjCSS"></div> 
            <div id="header-structure" class="no-padding col-xs-12"></div>
            <div id="results-structure"></div>
        </div>
    `
    $("#central-container").html(str);
    var paramsFilterstructure= {
        container : "#filter-nav",

        header : {
            dom : "#header-structure",
            options : {
                left : {
                    classes : 'col-xs-4 elipsis no-padding',
                    group:{
                        count : true
                    }
                },
                right : {
                    classes : 'col-xs-8 text-right no-padding',
                    group : {
                        map : true,
                    }
                }
            }
        },
        loadEvent : {
            default : "scroll"
        },
        defaults : {
            notSourceKey : true,
            fields : ["name", "email","preferences","thematic","collection", "slug", "fromActivityPub", "attributedTo", "objectId", "created", "updated", "profilThumbImageUrl",
                "profilImageUrl","profilMediumImageUrl", "shortDescription", "parent", "tags", "type", "section", "category", "startDate", "endDate", "openingHours",
                "address","addresses", "scope", "geo", "geoPosition", "links", "description", "isSpam", "userSpamDetector", "dateSpamDetector","type"],
            types : ["organizations"],
            forced : {
                filters:{
                }
            },
            filters:{
                '$or' :{

                }
            }
        },
        results : {
            dom : "#results-structure",
            renderView : "directory.cards",
            smartGrid : true                       
        },
        filters : {
            text : true,
            family : {
                view : "dropdownList",
                type : "filters",
                field : "thematic",
                name : "Domaine(s) d'activité(s",
                event : "filters",
                activateCounter:true,
                list : family
            }
                  
        }
    }
    paramsFilterstructure.defaults.forced.filters["links.members."+contextData.id] = {'$exists' : true};
    paramsFilterstructure.defaults.filters['$or']["source.keys"] = costum.slug;
    paramsFilterstructure.defaults.filters['$or']["reference.costum"] = costum.slug;
	paramsFilterstructure.urlData = baseUrl+"/co2/search/globalautocompleteadmin/type/"+contextData.collection+"/id/"+contextData.id+"/canSee/true";
    filterstructure = searchObj.init(paramsFilterstructure);
    filterstructure.search.init(filterstructure);
	$("#openModalContent").addClass("w-100")
}