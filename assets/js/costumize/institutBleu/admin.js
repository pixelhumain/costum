adminPanel.views.organizations = function() {
    var data = {
        title: "Acteurs à modérer",
        types: ["organizations"],
        table: {
            name: {
                name: "Dénomination"
            },
            thematic: {
                name: "Domaine(s) d'activité(s)"
            },
            mobile: {
                name: "Téléphone"
            },
            email: {
                name: "Email"
            }
        },
        paramsFilter: {
            container: "#filterContainer",
            defaults: {
                notSourceKey : true,
                types: ["organizations"],
                fields: ["category","name",  "thematic","legalStatus","updated", "address","link", "mobile", "email", "preferences","tags","collection","source","reference"],
                forced :{
                    filters : {                      
                        ["preferences.toBeValidated."+costum.slug] : {'$exists':true},
                        // category : "acteurInstitutBleu"
                    }
                },

            },
            filters: {
                text: true,
            }
        },
        actions : {
            accept : true,
            delete : true,
            sendMail : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl + '/' + moduleId + '/admin/directory/', data, function() {

    }, "html");
};
adminDirectory.actions.accept = function(e, id, type, aObj){ 
    mylog.log("adminDirectory.actions.accept", e, id, type, aObj);
	var str ='<button data-id="'+id+'" data-email="'+e.email+'" data-type="organizations" class="col-xs-12 acceptActor btn bg-green-k text-white"><i class="fa fa-check-circle"></i>Accepter</button>';		
	return str ;
}; 
adminDirectory.bindCostum = function(aObj){
    $("#"+aObj.container+" .acceptActor").on("click", function(){  
        var tplCtx = {};
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("type");
        tplCtx.value = null;
        tplCtx.path = "preferences.toBeValidated."+costum.slug;
        var invitedEmail = $(this).data("email");
        dataHelper.path2Value(tplCtx, function(params) {
            toastr.success("Acteur ajouté"); 
            if(invitedEmail!=""){
                // update listInvitation status
                dataHelper.path2Value(
                    {
                        id:costum.contextId,
                        collection:costum.contextType,
                        path:"listInvitation."+slugify(invitedEmail, invitedEmail),
                        value: {
                            email : invitedEmail,
                            status: "Accepté"
                        },
                        updateCache : true
                    }, function(){
                        urlCtrl.loadByHash(location.hash);
                    });
            }
        } );
    });
}
adminDirectory.values.thematic = function(e, id, type, aObj) {
    mylog.log("eeeeethematic",e);
    var str = "";
    if (typeof e.thematic != "undefined") {
        str = e.thematic;
    }
    return str;
};
adminDirectory.values.mobile = function(e, id, type, aObj) {
    mylog.log("eeeeemobile",e);
    var str = "";
    if (typeof e.mobile != "undefined") {
        str = e.mobile;
    }
    return str;
};
adminPanel.views.listInvitation = function() {
    var coutUnread = 0;
    var countRefused = 0;
    var countAccepted = 0;
    var listInvitation = {};
    ajaxPost(
        null,
        baseUrl + "/" + moduleId + "/element/get/type/" + costum.contextType + "/id/"+costum.contextId,
        null,
        function (data) {
            if (data.result) {
                if(typeof data.map.listInvitation != "undefined"){
                    listInvitation = data.map.listInvitation;
                }
            }
        },
        null,
        "json",
        { async: false }
    );
    var str = `
    <div id="invitations" >
        <div class="col-xs-12 padding-10 text-center">
            <h2>Liste des invitées</h2>
        </div>
        <div class="containCount col-xs-12"></div>
        <div class = "col-xs-12">
            <table class="table table-bordered table-striped" >
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>`;
                if(notNull(listInvitation)) {
                    $.each(listInvitation, function(k,v){
                        if(v.status == "Non lu"){
                            coutUnread++;
                        }
                        if(v.status == "Accepté"){
                            countAccepted++;
                        }
                        if(v.status == "Refusé"){
                            countRefused++;
                        }
                        str += `<tr>
                            <td>${v.email}</td>
                            <td>${v.status}</td>
                        </tr>`
                    })
                }
                str += `</tbody>
            </table>
        </div>
    </div>`;
    $("#content-view-admin").html(str);
    // var strCount = `<div class="card">
    //                             <div class="card-body">
    //                                 <div class="card-header">
    //                                     <div class="card-size">
    //                                     ${coutUnread}
    //                                     </div>
    //                                 </div>
    //                                 <div class="card-text"> 
    //                                     <p> Non lues </p>
    //                                 </div> 
    //                             </div> 
    //                         </div>`
    // $(".containCount").html(strCount);
    $(".containCount").html(`<p>Nombre d'invitations non lues : ${coutUnread}</p><p>Nombre d'invitations acceptées : ${countAccepted}</p><p>Nombre d'invitations refusées : ${countRefused}</p>`);
    
}