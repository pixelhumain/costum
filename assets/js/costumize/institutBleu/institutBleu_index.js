directory.cards = function(params){
    mylog.log("directory.cards", params);
    var str = "";
    var dateAndTags = "";
    var tags = params.tags?params.tags:[];
    tags = tags.slice(0,3).sort((a, b) => a.localeCompare(b));

    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    if(params.collection=="events" && params.startDate){
        dateAndTags = '<span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY HH:mm") +'</span>';
    }
    let linkedElement = "";
    if(params.parent || params.organizer){
        let p = params.organizer || params.parent;
        linkedElement = '<div style="position:absolute;  max-width:45%;font-weight:bold; top:35px; left: 30px;">';
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5 margin-bottom-5" style="background-color:var(--primary-color); color:white; display:inline-block; border-radius:4px">'+
    	            ov.name+
                '</a><br/>';
        }),
        linkedElement+="</div>"
    }

    let linkedtags = "";
    if(params.thematic){
        let p = params.thematic;
        // linkedtags = '<div style="display: flex;flex-wrap: wrap; gap: calc((.10rem)* 4);" >';
        linkedtags = '<div style="display: flex;flex-wrap: wrap;" >';
        if(typeof p == "object"){
            $.each(p, function(oi, ov){
                linkedtags += '<a href="javascript:onClickThematicBadge(`'+ov+'`)" class="thematic">'+
                        ov+
                    '</a><br/>';
            })
        }else if(typeof p == 'string'){
            linkedtags += '<a href="javascript:onClickThematicBadge(`'+p+'`)" class="thematic">'+p+'</a><br/>';
        }
        linkedtags+="</div>"
    }
    let eTypes = [];
    if(params.collection=="events" && params.type){
        if(typeof params.type == "object"){
            $.each(params.type, function(eIndex, eType){
                if(typeof tradCategory[eType] != "undefined"){
                    eTypes.push(tradCategory[eType])
                }else{
                    eTypes.push(eType)
                }
            })
        }else if(typeof params.type == "string" && typeof tradCategory[params.type]!="undefined"){
            eTypes.push(tradCategory[params.type])
        }else{
            eTypes.push(params.type)
        }
    }

	var containStatus = "";
	if(notNull(params.preferences) 
	&& typeof params.preferences != "undefined" 
	&& typeof params.preferences.toBeValidated != "undefined" 
	&& typeof params.preferences.toBeValidated[costum.slug] != "undefined" 
	&& params.preferences.toBeValidated[costum.slug]){ 
		containStatus = `<div class="status"><a href="javascript:;" onclick="validateActor('${params._id.$id}', '${params.email||''}')" class="btn-validated">Valider la fiche</a></div>`;
	}
    var localityHtml="",streetAddress='', postalCode = '', city='',str='';
    if (params.address != null) {
        streetAddress= params.address.streetAddress ? params.address.streetAddress : '';
        postalCode = params.address.postalCode ? params.address.postalCode : '';
        city = params.address.addressLocality ? params.address.addressLocality : '' ;
    }
    localityHtml = (params.streetAddress != '' || params.postalCode!='' || params.city !='') ? streetAddress+' '+postalCode+' '+city : trad.UnknownLocality;
    
    str += `<div id='entity_${params.collection}_${params._id.$id}' onclick='openElementInPreview(event,"${params.collection}","${params._id.$id}")' class="blog-item actus-item item-type1 searchEntityContainer smartgrid-slide-element start-masonry msr-id-${params._id.$id} col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12" style="cursor:pointer">`+
        '<div class="item-inner" style="display:block">'+
			containStatus+
        '<div class="image">'+
        '<img src="'+(params.profilMediumImageUrl||"https://communecter.org/upload/communecter/organizations/66fa6c01f8b5267b59215dc9/album/1730308874_pastedimage.png")+'" class="img-responsive" alt="" decoding="async">'+
        '</div>'+
        // linkedElement+
        ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#f1c232; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px;" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
        ((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(eTypes.toString()||"")+'">'+(eTypes.toString()||"")+'</span>':"")+
        '<div class="item-body">'+
        '<div class="item-text">'+
        //'<div class="top-wrapper">'+
        //'<span class="cat-date">'+dateAndTags+'</span>'+
        //'<span class="category lowercase">'+(tags.map((v, i)=> "#"+v+(i==tags.length-1?"":", ")).join(""))+'</span>'+
        //'</span>'+
        //'<span class="tag"></span>'+
        //'</div>'+
		(localityHtml!=""?("<i class='fa fa-map-marker margin-right-5'></i>"+localityHtml):"")+
        (params.name?`<h4 class="title">${params.name}</h4>`:"")+

        linkedtags+
        '</div>'+
        //'<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
        '<div class="link-bordered">';
        // if(params.collection == "organizations"){
            //str +=`<a href="javascript:;" onclick = 'smallMenu.openAjaxHTML("${baseUrl}/costum/institutbleu/elementhome/type/${params.collection}/id/${params._id.$id}")' data-id = "'+params._id.$id+'" class="openSmallMenu">En savoir plus</a>`;
        // }else{
        //     str += '<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'
        // }
		// if(isInterfaceAdmin && notNull(params.preferences) 
		// && typeof params.preferences != "undefined" 
		// && typeof params.preferences.toBeValidated != "undefined" 
		// && typeof params.preferences.toBeValidated[costum.slug] != "undefined" 
		// && params.preferences.toBeValidated[costum.slug] ){
		// 	str += `<a href="javascript:;" onclick="validateActor('${params._id.$id}', '${params.email||''}')" class="btn-validated">Valider</a>`;
		// }
        str +='</div>'+
        '</div>'+
        '</div>'+
        '</div>';
    return str;


}

function onClickThematicBadge(tag){
    $("button.btn-filters-select[data-label='"+tag+"']").trigger("click");
}

function addData(_context, params) {
    if (params.elt && params.elt.geo && params.elt.geo.latitude && params.elt.geo.longitude) {
        if (!params.opt)
            params.opt = {}

        params.opt.icon = _context.getOptions().mapCustom.icon.getIcon(params)

        var latLon = [params.elt.geo.latitude, params.elt.geo.longitude];
        _context.setDistanceToMax(latLon);

        var marker = L.marker(latLon, params.opt);
        _context.getOptions().markerList[params.elt.id] = marker;

        if (_context.getOptions().activePopUp) {
            _context.addPopUp(marker, params.elt)
        }

        _context.getOptions().arrayBounds.push(latLon)

        if (_context.getOptions().activeCluster) {
            _context.getOptions().markersCluster.addLayer(marker)
        } else {
            marker.addTo(_context.getMap());
            if (params.center) {
                _context.getMap().panTo(latLon)
            }
        }

        if (_context.getOptions().mapOpt.doubleClick) {
            marker.on('click', function (e) {
                this.openPopup()
                coInterface.bindLBHLinks();
            })
            marker.on('dbclick', function (e) {
                _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                coInterface.bindLBHLinks();
            })
        } else {
            marker.on('click', function (e) {
                if (_context.getOptions().mapOpt.onclickMarker) {
                    _context.getOptions().mapOpt.onclickMarker(params)
                } else {
                    _context.centerOne(params.elt.geo.latitude, params.elt.geo.longitude)
                    coInterface.bindLBHLinks();
                }
            });
        }
    }
}

paramsMapCO = $.extend(true, {}, paramsMapCO, {
    activeCluster:false,
    mapCustom: {
        icon: {
            getIcon: function (params) {
                var elt = params.elt;
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof elt.profilMediumImageUrl !== "undefined" && elt.profilMediumImageUrl != "")
                    imgProfilPath = baseUrl + elt.profilMediumImageUrl;
                var myIcon = L.divIcon({
                    className: "ib-div-marker",
                    iconAnchor: [35, 70],
                    iconSize: [70, 70],
                    labelAnchor: [-70, 0],
                    popupAnchor: [0, -70],
                    html: `<div style='background-color:var(--primary-color);' class='marker-ib'></div><img src="${imgProfilPath}">`
                });
                return myIcon;
            }
        },
        getClusterIcon: function (cluster) {
            var childCount = cluster.getChildCount();
            var c = ' marker-cluster-';
            if (childCount < 100) {
                c += 'small-ftl';
            } else if (childCount < 1000) {
                c += 'medium-ftl';
            } else {
                c += 'large-ftl';
            }
            return L.divIcon({
                html: '<div>' + childCount + '</div>',
                className: 'marker-cluster' + c,
                iconSize: new L.Point(40, 40)
            });
        },
        addMarker: function (_context, params) {
            addData(_context, params);
            if(typeof params.elt.addresses != "undefined" && params.elt.addresses != null){
                params.elt.addresses.forEach((element, key) => {
                    var data = $.extend({}, params, true);
                    data.elt.address = element.address;
                    data.elt.geo = element.geo;
                    data.elt.geoPosition = element.geoPosition;
                    data.elt.id = data.elt.id + key;
                    addData(_context, data);
                })
            }
        }
    }
});


var modeinvit = false;

function updateValue(params) {
    ajaxPost(
        null,
        baseUrl+"/costum/institutbleu/updatevalue",
        params,
        function(data){
            if(data.result){
                
            }
        }
    );
}

// costum[costum.slug] = {
costum["institutBleu"] = {
	init : function(){
		dyFObj.formInMap.forced.countryCode="RE";
		dyFObj.formInMap.forced.map={"center":[-21.115141, 55.536384],"zoom" : 9};
		dyFObj.formInMap.forced.showMap=true;
        $(".close-modal").html("<span clss='btn btn-default'><i class='fa fa-arrow-left'></i>  Retour</span>")

        if(location.hash.indexOf("#invitationanswer.") != -1){
            let email = location.hash.split("=")[1];
            if(location.hash.indexOf("#invitationanswer.true") != -1){
                if(userId==""){
                    localStorage.setItem("reloadAfterSave", true);
                }
                modeinvit = true;
                dyFObj.unloggedModeOrga = true;
                Login.registerOptions.mode = "single_step_register";
                dyFObj.unloggedModeMsg = "Vous n'êtes pas connectés, veuillez ajouter votre email pour pouvoir inscrire votre structure à l'annuaire et vous ajouterez votre mot de passe ulterieurement.";
                dyFObj.openForm("organization", null, {"email":email||""});
            }else{
                if(typeof email != "undefined"){
                    let data = {
                        id:costum.contextId,
                        collection:costum.contextType,
                        path:"listInvitation",
                        value: {}
                    };
                    data.value[email] = "refusé";
                    //updateValue(data);
                }
            }
        }else if(userId == ""){
            localStorage.setItem("reloadAfterSave", true);
            Login.openLogin();
        }
    },
    organizations: {
        source : false,
        reference : {},
        referentId : "",
        toBeValidated : false,
        beforeSave : function(data){  
            if(typeof data.reference != "undefined")
                costum[costum.slug].organizations.reference = data.reference 
            if(typeof data.source == "undefined" || (typeof data.source != "undefined" && typeof data.source.key != "undefined" && data.source.key != costum.slug)){
                costum[costum.slug].organizations.source = false
            }
            if(typeof data.referentId != "undefined"){
                costum[costum.slug].organizations.referentId = data.referentId 
            }
            if(typeof data.preferences != "undefined" && typeof data.preferences.toBeValidated != "undefined" && typeof data.preferences.toBeValidated[costum.slug] != "undefined" ){
                
                costum[costum.slug].organizations.toBeValidated = data.preferences.toBeValidated[costum.slug]
            }
        },
        afterSave: function(data) {
            $("#ajax-modal").removeClass(costum.slug+"Modal");
            if(!uploadObj.update ){
                if(typeof costum.admin != "undefined" && typeof costum.admin.email != "undefined" && costum.admin.email != ""){
               
                    if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                        var paramsEmail = {
                            tpl : "basic",
                            tplMail : costum.admin.email,
                            tplObject:"Nouvelle structure en attente de validation",
                            // replyTo: data.map.email,
                            subject :"Nouvelle structure en attente de validation ", 
                            html : `<p> Une nouvelle structure a été ajoutée sur <a href='${baseUrl}/costum/co/index/slug/${costum.slug}#search' target='_blanck'>Institut bleu</a> et est actuellement en attente de validation. Détails de la structure :</p>
                            <p>Nom : <a href='${baseUrl}/costum/co/index/slug${costum.slug}#@${data.map.slug}' target='_blanck'>${data.map.name}</a> </p>
                            <p> Ajoutée par : ${data.map.nameSign}</p>
                            <p> Date d’ajout : ${data.map.dateSign}</p>`
                        };
                        if(modeinvit){
                            paramsEmail.tplObject = "Invitation acceptée";
                            paramsEmail.subject = "Invitation acceptée";
                            paramsEmail.html = "<a href='"+baseUrl+'/costum/co/index/slug/'+costum.slug+'#@'+data.map.slug+"' target='_blanck'>"+data.map.name+"</a> a accepté l'invitation de rejoindre l'"+costum.slug;
                        }
                        ajaxPost(
                            null,
                            baseUrl+'/'+moduleId+'/mailmanagement/createandsend',
                            paramsEmail,
                            function(data){
                                modeinvit = false;
                                // urlCtrl.loadByHash("#search");
                            },
                            function(xhr, status, error){
                                toastr.error("<?php echo Yii::t('cms', 'An error occurred while sending your message')?>");
                            }
                        );
                    }
                } 
                if(location.hash.indexOf("#invitationanswer.true") != -1 && typeof data.map.email != "undefined" && data.map.email != ""){
                    let dataToUpdate = {
                        id:costum.contextId,
                        collection:costum.contextType,
                        path:"listInvitation",
                        value: {
                            "email": data.map.email,
                            "status": "accepté"
                        }
                    };
                    dataToUpdate.value[data.map.email] = "accepté";
                    //updateValue(dataToUpdate);
                }
            }

            ajaxPost(
                null,
                baseUrl+"/costum/institutbleu/connect",
                {
                    idChild : data.map._id.$id,
                    idParent : costum.contextId
                },
                function(dataConnect){
                }
            );
            mylog.log("commonAfterSavedata",data);
            var tplCtx = {};
            var phone = "";
           
            if(typeof data.map._id != "undefined" && typeof data.map._id.$id != "undefined"){
                tplCtx.id = data.map._id.$id;
                tplCtx.value = {                    
                    "preferences.isOpenData" : true,
                    "preferences.isOpenEdition" : true ,
                    "preferences.isOpenEdition" : true 
                };    
            }
            if(typeof costum[costum.slug] != "undefined" &&  typeof costum[costum.slug].organizations != "undefined" &&  typeof costum[costum.slug].organizations.toBeValidated != "undefined" && costum[costum.slug].organizations.toBeValidated == true ){
                tplCtx.value["preferences.toBeValidated."+costum.slug] = true;
            }
            if(typeof data.map.mobile != "undefined"  &&  data.map.mobile !=""){
                phone = data.map.mobile;
                tplCtx.value["telephone.mobile"] = [phone]
            }
            if(costum[costum.slug].organizations.source == false){
                if(notNull(costum[costum.slug].organizations.reference) && notNull(costum[costum.slug].organizations.reference.costum)){
                    if((costum[costum.slug].organizations.reference.costum).indexOf(costum.slug) == -1){
                        costum[costum.slug].organizations.reference.costum.push(costum.slug);
                        tplCtx.value["reference.costum"] = costum[costum.slug].organizations.reference.costum;
                    }
                }else{
                    tplCtx.value["reference.costum"] = [costum.slug]
                }
                if(costum[costum.slug].organizations.referentId == "")
                    tplCtx.value["referentId"] = userId;
            }
            if(!uploadObj.update){
                tplCtx.value["preferences.toBeValidated."+costum.slug] = true;
                if(costum[costum.slug].organizations.source == false){
                    if(costum[costum.slug].organizations.referentId == "")
                        tplCtx.value["referentId"] = userId;
                }
                tplCtx.value['nameSign'] = "";
                var date = new Date();
                var jour = date.getDate();
                var mois = date.getMonth() + 1;
                var annee = date.getFullYear();
                var dateFormat = `${jour}/${mois}/${annee}`;
                tplCtx.value['dateSign'] = dateFormat;
                if(notNull(userConnected)){
                    tplCtx.value['nameSign'] = userConnected.name;
                }else if(typeof dyFObj.unloggedProcess != "undefined" && notNull(dyFObj.unloggedProcess.userEmail)){
                    params = {
                        "collection" : "citoyens",
                        "email" : (dyFObj.unloggedMode)?dyFObj.unloggedProcess.userEmail: userConnected.email,
                    }
                    ajaxPost(
                        null,
                        baseUrl + "/co2/element/existselement",
                        params,
                        function(data) {
                            if (data != null) {
                                if (typeof data.result != "undefined" && data.result) {
                                    if(typeof data.elt != "undefined" && typeof data.elt.name != "undefined"){
                                        tplCtx.value['nameSign'] = data.elt.name;
                                    }
                                }
                            }
                        },null,null, {
                            async: false
                        }
                    );
                }
            }
            $.each(dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties,function(field, values){
                if(typeof data.map[field] == "undefined" && values.inputType=="selectMultiple"){
                    tplCtx.value[field] = null;
                }
            });

            tplCtx.value.tags=[];
            tplCtx.path = "allToRoot";        
            if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) {
                $.each(data.map.tags, function(i,tg){
                    tplCtx.value.tags.push(tg.replace("&amp;","&"));
                });
            }    
            if(typeof data.map.thematic != "undefined"){
                tplCtx.value.thematic = []; 
                if(notNull(data.map.thematic)) {
                    if(typeof data.map.thematic == "string" ){
                        tplCtx.value.thematic  = data.map.thematic.replace("&amp;","&");
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            tplCtx.value.thematic.push(tag.replace("&amp;","&"));
                        });
                    }
                }
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                    if(typeof data.map.thematic == "string" ){
                        if ((tplCtx.value.tags).indexOf((data.map.thematic)) == -1){
                            tplCtx.value.tags.push((data.map.thematic));
                        }
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){                                    
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(costum.lists.family,function(kt,vt){
                        if(typeof data.map.thematic == "string" && (tplCtx.value.tags).indexOf(vt) >-1 && data.map.thematic != vt ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vt), 1);                                 
                        }else if(typeof data.map.thematic == "object" && (tplCtx.value.tags).indexOf(vt) >-1  && (data.map.thematic).indexOf(vt) == -1){
                            delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vt)]; 
                        }
                    })
                } else{
                    if(typeof data.map.thematic == "string"){
                        tplCtx.value.tags.push(data.map.thematic);
                    }
                    else{
                        $.each(data.map.thematic, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    }
                }
               
            }else{
                $.each(costum.lists.family,function(kt,vt){
                    if((tplCtx.value.tags).indexOf(vt) >-1 ){
                        delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vt)]; 
                    }
                })
            }

            /*if(typeof data.map.objectiveOdd != "undefined"){ 
                if(typeof data.map.tags != "undefined" || notNull(data.map.tags)) { 
                    if(typeof data.map.objectiveOdd == "string" ){
                        if ((tplCtx.value.tags).indexOf(data.map.objectiveOdd) == -1){
                            tplCtx.value.tags.push(data.map.objectiveOdd);
                        }
                    }else if(typeof data.map.objectiveOdd == "object" ){
                        $.each(data.map.objectiveOdd, function(i,tag){
                            if((tplCtx.value.tags).indexOf(tag) == -1){
                                tplCtx.value.tags.push(tag);
                            }
                        });
                    }
                    $.each(costum.lists.objectiveOdd,function(kf,vf){
                        if(typeof data.map.objectiveOdd == "string" && (tplCtx.value.tags).indexOf(vf) >-1 && data.map.objectiveOdd != vf ){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }else if(typeof data.map.objectiveOdd == "object" && (tplCtx.value.tags).indexOf(vf) >-1  && (data.map.objectiveOdd).indexOf(vf) == -1){
                            tplCtx.value.tags.splice((tplCtx.value.tags).indexOf(vf), 1); 
                        }
                    })
                }else{
                    if(typeof data.map.objectiveOdd == "string"){
                        tplCtx.value.tags.push(data.map.objectiveOdd);
                    }
                    else{
                        $.each(data.map.objectiveOdd, function(i,tag){
                            tplCtx.value.tags.push(tag);
                        });
                    } 
                }            
            }else{
                $.each(costum.lists.objectiveOdd,function(kt,vo){
                    if((tplCtx.value.tags).indexOf(vo) >-1 ){
                        delete tplCtx.value.tags[(tplCtx.value.tags).indexOf(vo)]; 
                    }
                })
            }
            paramst = {};
            paramst.id = costum.contextId;
            paramst.collection = costum.contextType;
            paramst.path = "allToRoot"; 
            paramst.updateCache = true; 
            paramst.value = {};
            if(typeof data.map.legalStatus != "undefined"){
                 if(typeof costum.lists.legalStatus != "undefined"){
                    if( (costum.lists.legalStatus).indexOf(data.map.legalStatus)== -1 ){
                        paramst.value["costum.lists.legalStatus"] = [];
                        paramst.value["costum.lists.legalStatus"] = costum.lists.legalStatus;
                        paramst.value["costum.lists.legalStatus"].push(data.map.legalStatus);     
                    }
                }
            }*/
            mylog.log("commonAfterSavedatatags",tplCtx.value);
            tplCtx.collection = "organizations";
            
            dyFObj.commonAfterSave(data, function() {
                dataHelper.path2Value(tplCtx, function(params) {
                    toastr.success("Acteur bien ajouté");
                    urlCtrl.loadByHash("#@"+data.map.slug);
                });
            });
        
            /*
            dyFObj.commonAfterSave(data, function() {
                dataHelper.path2Value(tplCtx, function(params) {
                    toastr.success("Acteur bien ajouté");
                    /*if(Object.keys(paramst.value).length !== 0){
                        dataHelper.path2Value(paramst, function(params) {
                            urlCtrl.loadByHash("#@"+data.map.slug);
                            location.reload()
                        });
                    }else{
                    }*/
                   /*
                   if(data.result){
                        urlCtrl.loadByHash("#@"+data.map.slug);
                        location.reload();
                   }
                });*/
        }
    }

}
costum.costumizedDirectory = {
    "logos":directory.logos
};
document.addEventListener("keydown", function(event) {
    if (event.key === "Escape") { 
        $(".close-modal").click()
    }
});
costum.searchExist=function(type,id,name,slug,email,elt){
    ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/element/get/type/"+type+"/id/"+id,
        {},
        function (data) {
            mylog.log("searchExistsearchExist", data)
            var mesg = "Cette structure existe déjà !";
            if(typeof data.map !="udefined" && typeof data.map.preferences != "undefined" && typeof data.map.preferences.toBeValidated != "undefined" && typeof data.map.preferences.toBeValidated[costum.slug] != "undefined"){
                mesg = "Cette structure existe déjà et en attente de validation";
            }
            if(typeof data.map != "undefined"){
                if((typeof data.map.source != "undefined" && typeof data.map.source.key != "undefined" && data.map.source.key == costum.slug ) || 
                    (typeof data.map.reference != "undefined" && typeof data.map.reference.costum != "undefined" && (data.map.reference.costum).indexOf(costum.slug) >-1)||
                    (typeof data.map.links != "undefined" && typeof data.map.links.memberOf !="undefined" && typeof data.map.links.memberOf[costum.contextId] != "undefined" )){
                    bootbox.alert({ 
                        message: mesg,
                        callback: function () {
                            if(typeof data.map !="udefined" && typeof data.map.preferences != "undefined" && typeof data.map.preferences.toBeValidated != "undefined" && typeof data.map.preferences.toBeValidated[costum.slug] != "undefined"){
                                urlCtrl.loadByHash("#search");
                            }else{
                                urlCtrl.loadByHash("#@"+data.map.slug);
                            }
                        }
                    });   
                }else{
                    if(typeof dyFObj.elementObj !== 'undefined' && typeof dyFObj.elementObj.formType !== 'undefined'){
                        if(typeof costum.typeObj[dyFObj.elementObj.formTypej] != "undefined" && typeof costum.typeObj[dyFObj.elementObj.formType].dynFormCostum != "undefined"){
                            var editPrev=jQuery.extend(true, {},costum.typeObj[dyFObj.elementObj.formType].dynFormCostum);
                        }
                        if(typeof editPrev != "undefined"){
                            dyFObj.editElement(type,id, dyFObj.elementObj.formType, editPrev);
                        }else{
                            dyFObj.editElement(type,id, dyFObj.elementObj.formType);
                
                        }
                    }else{
                        var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
                        dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");
                
                    } 
                    uploadObj.update = false;

                }
            }
            
        },
        "json"
    );
};
function openElementInPreview(event,type,id){
    if (event.target.tagName !== 'A') {
        smallMenu.openAjaxHTML(baseUrl+"/costum/institutbleu/elementhome/type/"+type+"/id/"+id)
    }
}
function validateActor(id, email){
    var tplCtx = {};
    tplCtx.id = id;
    tplCtx.collection = "organizations";
    tplCtx.value = null;
    tplCtx.path = "preferences.toBeValidated."+costum.slug;
    dataHelper.path2Value(tplCtx, function(params) {
        toastr.success("Acteur validé");
        if(email!=""){
            // update listInvitation status
            dataHelper.path2Value(
                {
                    id:costum.contextId,
                    collection:costum.contextType,
                    path:"listInvitation."+slugify(email, email),
                    value: {
                        email : email,
                        status: "Accepté"
                    },
                    // updateCache : true
                }, function(){
                    $("#entity_organizations_"+id +" .status").hide()
                }
            );
        }
    } );
}