
var main1Color = costum.colors.main1;
var main2Color = costum.colors.main2;
var scopeCountryParam= ["BE"];
var scopeLevelParam = ["3"];
var scopeLevelParam = "";
getDataCostum();

function paramsMapLatLon(lat,lon){
	mylog.log("latlon",lat,lon);
	paramsMapCO = $.extend(true, {}, paramsMapCO, {
		mapCustom:{
			icon: {
				getIcon:function(params){
					var elt = params.elt;
					mylog.log("icone ftl", elt.tags);
					var myCustomColour = main1Color;
					var markerHtmlStyles = `
						background-color: ${myCustomColour};
						width: 3.5rem;
						height: 3.5rem;
						display: block;
						left: -1.5rem;
						top: -1.5rem;
						position: relative;
						border-radius: 3rem 3rem 0;
						transform: rotate(45deg);
						border: 1px solid #FFFFFF`;
				
					var myIcon = L.divIcon({
						className: "my-custom-pin",
						iconAnchor: [0, 24],
						labelAnchor: [-6, 0],
						popupAnchor: [0, -36],
						html: `<span style="${markerHtmlStyles}" />`
					});
					return myIcon;
				}
			},
			getClusterIcon:function(cluster){
				var childCount = cluster.getChildCount();
				var c = ' marker-cluster-';
				if (childCount < 20) {
					c += 'small-ftl';
				} else if (childCount < 50) {
					c += 'medium-ftl';
				} else {
					c += 'large-ftl';
				}
				return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
			}
		}
	});
	if (notNull(lat) && notNull(lon)){
		paramsMapCO.forced ={
			latLon : [ 
				lat,
				lon
			]
		};
	}
 }

$("#show-button-map").addClass("changelabel bg-main1");
$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");

$("#btn-filters").parent().css('display','none');

//$("#input_name_filter").attr("placeholder","Filtre par nom ...");
$('.form-register').find("label[for='agree'].agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie.");
//$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");


var changeLabel = function() {
	$("#menuRight").on("click", function() {
		if($(this).find("a").hasClass("changelabel")) {
			$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
		}
		else{
			$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		}
	//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
		$(this).find("a").toggleClass("changelabel");
	});
}


function getDataCostum(){
	ajaxPost(
		null,
		baseUrl+"/co2/element/get/type/"+costum.contextType+"/id/"+costum.contextId,
		null,
		function(data){
			mylog.log("callback contextorga",data);
			mylog.log("costum object",costum);
			if (typeof data.map.level!="undefined"){
				var data = data.map;
				var level=data.level;

				var levelId = data.address[level]
				var levelNb = level.substr(5);

				if (typeof levelId=="undefined" && levelNb=="5"){
					levelNb=levelNb-1;
					levelId=data.address["level"+levelNb];
				}
				
				//var scopeLevel = 
				//if(level!="level1"){
					scopeCountryParam = [data.address.addressCountry];
					scopeLevelParam = (level=="1") ? [Number(levelNb)+2] : [Number(levelNb)+1];
					if(data.address.addressCountry!="BE"){
						scopeLevelParam=["6"];

					}
					upperLevelId=levelId;
					costum.address=data.address;
					costum.parentId=data._id.$id;
				//}

				getZone(data.address[level],level);	
				//referenceTl(levelNb,data.address[level]);
			}		        	
		}, 
		null,
		null,
		{async:false}
	);
}

function getZone(zoneId,level){
	mylog.log("getzone",zoneId,level);
	ajaxPost(
		null,
		baseUrl+"/co2/zone/getscopebyids",
		{zones:{zoneId}},
		function(data){
			var coord = {};
			mylog.log("scopes",data.scopes[zoneId+level].latitude);
			var latit = (typeof data.scopes[zoneId+level].latitude !="undefined") ? data.scopes[zoneId+level].latitude : null;
			//var longit = ;
			coord = {
				latitude : data.scopes[zoneId+level].latitude,
				longitude : data.scopes[zoneId+level].longitude
			};
			mylog.log("coord1",coord);

			//Ne pas forcer coordonnée
			//coord.latitude,coord.longitude
			paramsMapLatLon();						          	
		}, 
		null,
		null,
		{async:false}
	);
}

function referenceTl(levelNb,levelId){
	mylog.log("referenceTl",levelNb,levelId);
	
	if (typeof costum.loaded=="undefined" || costum.loaded==false){
		var dataCustom = {};
		dataCustom.id = costum.contextId;
		dataCustom.collection = costum.contextType;
		dataCustom.path = "costum.loaded";
		dataCustom.value=true;
		dataCustom.tplGenerator=true;
 		dataHelper.path2Value(dataCustom,function(response){
	        if(response.result){
				toastr.success("added");
	            mylog.log("loaded costum",response);       	
			}
	    });

		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/search/globalautocomplete",
			{
				"searchType" : "templates",
				"sourceKey" : costum.assetsSlug,
				"filters" : {
					"subtype" : "site"
				}
			},
			function(data){
				var networkTplId=Object.keys(data.results)[0];
				var params = {
					"id"		 : networkTplId,
					// "page"		 : page,
					"collection" : "templates",
					"parentId"   : costum.contextId,
                    "parentSlug" : costum.contextSlug,
                    "parentType" : costum.contextType,
					"action" : ""
				}
				ajaxPost(
					null,
					baseUrl + "/co2/cms/usetemplate",
					params,
					function (data) {
						window.location.reload()
					},
					null,
					null,
					{ async: false }
				);

			},
			null,
			null,
			{async : false}
		);
		var refTags = {};
	}

	if(location.hash.indexOf("?") > -1){
		searchInterface.init();
	 }	

	if (typeof searchObject.tags !="undefined" && searchObject.tags.length>0){
		mylog.log("searchObject.tags",searchObject.tags);
		refTags = searchObject.tags;
	}else{
		var allTypePlace=costum.lists.typePlace;
		// allTypePlace.pop();
		refTags=allTypePlace;
	}
}

function lightenColor(color, percent) {
	var num = parseInt(color.replace("#",""),16),
	amt = Math.round(2.55 * percent),
	R = (num >> 16) + amt,
	B = (num >> 8 & 0x00FF) + amt,
	G = (num & 0x0000FF) + amt;
	return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? 
    parseInt(result[1], 16)+","+
    parseInt(result[2], 16)+","+
    parseInt(result[3], 16)
  : null;
}

var lighterMain1 = lightenColor(main1Color,10);
var darkerMain1 = lightenColor(main1Color,-10);
var lightestMain1Rgb = hexToRgb(costum.colors.main1);
  				

// dyFObj.unloggedMode=true;

costum.name={
	searchExist : function(type,id,name,slug,email){
	    var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
	    editDynF.onload.actions.hide.similarLinkcustom=1;
	    dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");
	}
};

costum[costum.slug]={
	init : function(){
		var contextData=null;
		//costum.address={};
		document.documentElement.style.setProperty('--main1', main1Color);
		document.documentElement.style.setProperty('--main2', main2Color);
		document.documentElement.style.setProperty('--lighterMain1', lighterMain1);
		document.documentElement.style.setProperty('--lightestMain1Rgb', lightestMain1Rgb);
		document.documentElement.style.setProperty('--darkerMain1', darkerMain1);
		
		coInterface.bindButtonOpenForm = function () {
		$(".btn-open-form").off().on("click", function () {
			    var typeForm = $(this).data("form-type");
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				//dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
				if (contextData && contextData.type && contextData.id) {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");
					} else {
						dyFObj.openForm(typeForm, "sub");
					}
				}
				else {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");
					} else {
						dyFObj.openForm(typeForm);
					}
				}
			});
		};
		coInterface.bindButtonOpenForm();
		changeLabel();
		//getNetwork();
		
		function getNetwork(){
		    mylog.log("---------getNetwork");
		    var params = {
		        contextId : costum.contextId,
		        contextType : costum.contextType
		    };

		    ajaxPost(
		        null,
		        baseUrl + "/costum/reseautierslieux/getnetwork",
		        params,
		        function (data) {
		        	mylog.log("callbacknet",data);
		        	$.each(data, function(e, v){
		        		var posNet=$.inArray(v.name,costum.lists.network);
		        		if (posNet>-1){
		        			mylog.log("networks",posNet,costum.lists.network);
		        			costum.lists.network.splice(posNet, 1);
		        		}
		        	});	
		        }
		    );
		}


		costum.typeObj.organizations.dynFormCostum.prepData=function(data){
			mylog.log("prepData beginning",data);
		    var listObj=costum.lists;
		    // delete listObj.network;
		    $.each(listObj, function(e, v){
		        constructDataForEdit=[];
		        $.each(v, function(i, tag){
		            if($.inArray(tag, data.map.tags) >=0){
		                constructDataForEdit.push(tag);
		             
		                data.map.tags.splice(data.map.tags.indexOf(tag),1);
		            }
		        });
		        data.map[e]=constructDataForEdit;
		    });

			if (notNull(data.map.photo)) {
                uploadObj.initList.photo = data.map.photo;
            }

			if(typeof data.map.telephone!="undefined"){
			    if(typeof data.map.telephone.fixe=="object"){
				    data.map.telephone=data.map.telephone.fixe[0];
				}else if(typeof data.map.telephone.mobile=="object"){
					data.map.telephone=data.map.telephone.mobile[0];
				}
			}

			if(typeof data.map.socialNetwork!="undefined"){
				mappingSocialNetwork={
					"facebook" : "votre page Facebook", 
                    "twitter" : "votre compte Twitter", 
                    "instagram" : "votre compte Instagram", 
                    "linkedin" : "votre compte Linkedin", 
                    "mastodon" : "votre compte Mastodon", 
                    "movilab" : "votre page Movilab"
				};
				socialArray=[];
				$.each(data.map.socialNetwork,function(name,link){
					var socialObj={
						platform : mappingSocialNetwork[name],
						url : link
					};
					socialArray.push(socialObj);
				});
				data.map.socialNetwork=socialArray;
			}

		     mylog.log("prepData return",data);

		    return data;

		};

		if(typeof costum[costum.slug].extendInit=="function"){
		    costum[costum.slug].extendInit();
		}

		costum[costum.slug].logginOnlyWithSSO();

	},
	logginOnlyWithSSO : function(){
		$(".login-form-container").empty();
		/*$("#modalLogin").append(
			'<div class="modal-body">'+
			'<iframe src="https://id.indie.host/auth/realms/tiers-lieux-org/protocol/openid-connect/auth?client_id=communecter&response_type=code&redirect_uri=https%3A%2F%2Fwww.communecter.org%2Fco2%2Fsso%2Fauth%3Fauthclient%3Dtierslieuxorg&xoauth_displayname=My%20Application&scope=openid%20profile%20email&state=afbcc1968a0d793f436c01f52ada0eee4c6739ba8bf760f9cf49aa58dd9db284" width="100%" height="100vh" style="border:none;"></iframe>'+
			'</div>'
		);*/
		$(".cosDyn-login").removeAttr("data-target");
		$(".cosDyn-login").removeAttr("data-toggle");
		$(".cosDyn-login").addClass("auth-link tierslieuxorg");
		$(".cosDyn-login").attr("href", "https://"+window.location.host+"/co2/sso/services?authclient=tierslieuxorg");
		$('body').authchoice({});
	},
	"organizations" : {
		formData : function(data){
			mylog.log("formData In",data);
			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0){
				$.each(finder.object, function(key, object){
					if(typeof formData[key]!="undefined"){
						delete formData[key];
					}
				});
			}
			$.each(data, function(e, v){
				if(typeof costum.lists[e] != "undefined" && e!="enjeux"){
					if(notNull(v)){
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string" && v!=null){
							data.tags.push(v);
						}else{
							$.each(v, function(i,tag){
								if(tag!=null){
									data.tags.push(tag);
								}
							});
						}
						//delete data[e];
					}
				}else{
					data[e] = v;
				}
			});

			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
			}

			if(data.socialNetwork){
				var formatedSocialNetwork={};
                $.each(data.socialNetwork,function(i,net){
					var labelArray=net["platform"].split(" ");
  
					var nameSocNet=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["url"]!="undefined" && net["url"]!=""){
                        formatedSocialNetwork[nameSocNet]=net["url"];						
					}
					delete data.socialNetwork[i];
				});
				if(Object.keys(data.socialNetwork).length!=0){
                	data.socialNetwork=formatedSocialNetwork;
				}
			}
			if(data.enjeux){
				var formatedIssues={};
                $.each(data.enjeux,function(i,net){
					var labelArray=net["enjeu"].split(" ");
  
					var nameEnjeu=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["mission"]!="undefined" && net["mission"]!=""){
                        formatedIssues[nameEnjeu]=net["mission"];
					}
					data.enjeux[i];

					if(typeof net["enjeu"]!="undefined" && typeof net["enjeu"]["enjeu"] != "undefined"){
						data.tags.push(net["enjeu"]["enjeu"]);
					}
					
				});
                data.enjeux=formatedIssues;
			}
			data.tags = data.tags.filter(element => {
				return (element !== null);
			});
			if(userId!="" && typeof costum.communityLinks.members[userId]=="undefined"){
				data.toBeValidated=false;
			}
			return data;
		},
		afterBuild : function(data){
			mylog.log("afterbuild reseauTierslieux",data);
			if(typeof data.manageModel=="undefined" || (typeof data.manageModel!="undefined" && !data.manageModel.includes("Autre mode de gestion"))){
				$("#ajaxFormModal .extraManageModeltext").hide();
			}
			if(typeof data.typePlace=="undefined" || (typeof data.typePlace!="undefined" && !data.typePlace.includes("Autre famille de tiers-lieux"))){
				$("#ajaxFormModal .extraTypePlacetext").hide();
			}

			$("#divCity input").attr("placeholder","Saisir votre code postal");
						    
			$("#ajaxFormModal .manageModelselect select").change(function(){
			    // var prevManageModel = $(this).data('manageModel');
			    var modelStatus=$(this).val();
			    if(modelStatus!="Autre mode de gestion"){
			        //    $("#ajaxFormModal .extraManageModeltext").hide();
			       $("#ajaxFormModal .extraManageModeltext input").val("");
				   $("#ajaxFormModal .extraManageModeltext").hide();
			    }
			    /*var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre mode de gestion"	: "Group"
			    };*/ 
			    //$("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});
			            
			           
			$("#ajaxFormModal .typePlaceselect select").change(function(){
			    //var prevTypePlace = $(this).data('typePlace');
			    var placeCat=$(this).val();
			    if(placeCat==null || placeCat.includes("Autre famille de tiers-lieux")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(placeCat.includes("Autre famille de tiers-lieux")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function(){
			    //var prevTypePlace = $(this).data('typePlace');
			    var themeNetwork=$(this).val();
			    if(themeNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraThemeNetworktext").hide();
			       $("#ajaxFormModal .extraThemeNetwork input").val("");
			    }
			    if(themeNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraThemeNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});

			$("#ajaxFormModal .localNetworkselect select").change(function(){
			    //var prevTypePlace = $(this).data('typePlace');
			    var localNetwork=$(this).val();
			    if(localNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraLocalNetworktext").hide();
			       $("#ajaxFormModal .extraLocalNetwork input").val("");
			    }
			    if(localNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraLocalNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


            //init openingHours
            if(typeof data.openingHours!="undefined"){
			    $.each(openingHoursResult, function(e,v){
      				mylog.log("custom init openinghours",data.openingHours,e);
      				if(typeof data.openingHours[e]=="undefined" || !notEmpty(data.openingHours[e]) ){
      					openingHoursResult[e].disabled=true;
      				}
      			});	      
			}	
					
            $(".btn-select-day").off().on("click",function(){
                mylog.log("selectDay", openingHoursResult, $(this).data("key"));
                key=$(this).data("key");
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                    $.each(openingHoursResult, function(e,v){
                    if(v.dayOfWeek==key)
                        openingHoursResult[e].disabled=true;
                    });
                    $("#contentDays"+key).fadeOut();
                }else{
                    $(this).addClass("active");
                    $.each(openingHoursResult, function(e,v){
                        if(v.dayOfWeek==key)
                        	delete openingHoursResult[e].disabled;
                    });
                    $("#contentDays"+key).fadeIn();
                }
            });


		    $('.timeInput').off().on('changeTime.timepicker', function(e) {
			    mylog.log("changeTimepicker");
			    var typeInc=$(this).data("type");
			    var daysInc=$(this).data("days");
			    var hoursInc=$(this).data("value");
			    var firstEnabled=null;
			    var setFirst=false
			    $.each(openingHoursResult, function(i,v){
				    if(!setFirst && typeof openingHoursResult[i].disabled=="undefined"){
				       mylog.log("firtEnable",v.dayOfWeek);
				       firstEnabled=v.dayOfWeek;
				       setFirst=true;
			        }
	                if(firstEnabled==daysInc){
	        	        mylog.log("first enabled change causes recursive change",v.dayOfWeek);
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
                        var typeHours="";
	        	        if(typeInc=="opens"){
	        		        typeHours="start";
	        	        }else if(typeInc=="closes"){
	        		        typeHours="end";
	        	        }
	        	        if(typeHours!=""){
	        		        $("#"+typeHours+"Time"+v.dayOfWeek+hoursInc).val(e.time.value);
	        	        }
	                }else if(v.dayOfWeek==daysInc){
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
	                }
		        });
		    });

			$('.timeInput').on("focus",function (){
			    $(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();   

			// AFTERLOAD
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
				mylog.log("afterload reseauTL data", data);
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
					$('#ajaxFormModal #divNewAddress').hide();
				}

				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);
        			    }
        			    // Div récapitulatif de l'adresse 
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    
        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
            };
		},
		beforeSave : function(data){
			mylog.log("beforeSave data",data);
            if(notNull(data.newElement_city)){
			    delete data.newElement_city;
            }

		    if(notNull(data.newElement_street)){
			    delete data.newElement_street;
		    }
		    return data;
		},
		afterSave:function(data){
			//uploadObj.afterLoadUploader=false;
		    dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data);
				dyFObj.closeForm();
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister){
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite électronique</span>");
				    }
                }
				if(userId!="" && typeof costum.communityLinks.members[userId]=="undefined"){
					links.connectAjax(costum.contextType, costum.contextId, userId, "citoyens", "member");
				}
				urlCtrl.loadByHash(location.hash);
			});
		}
	}
};

directory.cards = function(params){
    mylog.log("directory actus", "Params", params);
    var str = "";
    var dateAndTags = "";
    var tags = params.tags?params.tags:[];
    tags = tags.slice(0,3).sort((a, b) => a.localeCompare(b));

    if(params.collection=="news"){
        dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
    }
    if(params.collection=="events" && params.startDate){
        dateAndTags = '<span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY HH:mm") +'</span>';
    }
    let linkedElement = "";
    if(params.parent || params.organizer){
        let p = params.organizer || params.parent;
        linkedElement = '<div style="position:absolute;  max-width:45%;font-weight:bold; top:35px; left: 30px;">'
        $.each(p, function(oi, ov){
            linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5 margin-bottom-5" style="background-color:var(--primary-color); color:white; display:inline-block; border-radius:4px">'+
    	            ov.name+
                '</a><br/>';
        }),
        linkedElement+="</div>"
    }
    let eTypes = [];
    if(params.collection=="events" && params.type){
        if(typeof params.type == "object"){
            $.each(params.type, function(eIndex, eType){
                if(typeof tradCategory[eType] != "undefined"){
                    eTypes.push(tradCategory[eType])
                }else{
                    eTypes.push(eType)
                }
            })
        }else if(typeof params.type == "string" && typeof tradCategory[params.type]!="undefined"){
            eTypes.push(tradCategory[params.type])
        }else{
            eTypes.push(params.type)
        }
    }

	let elemAddress = directory.table.values.address(params, params._id.$id, params.collection, directory)

	let profilUrl = params.profilMediumImageUrl||baseUrl+assetPath+"/images/costumize/troistiers/tierslieu.png"
    if(params.collection=="citoyens"){
		profilUrl = params.profilMediumImageUrl||baseUrl+co2AssetPath+"/images/thumb/default_citoyens.png"
	}else if(params.collection=="events"){
		profilUrl = params.profilMediumImageUrl||baseUrl+co2AssetPath+"/images/thumb/default_events.png"
	}else if(params.collection=="projects"){
		profilUrl = params.profilMediumImageUrl||baseUrl+co2AssetPath+"/images/thumb/default_projects.png"
	}

    str += '<div class="blog-item actus-item item-type1 col-xl-2 col-md-3 col-sm-6 col-xs-12">'+
        '<div class="item-inner" style="display:block">'+
        '<div class="image">'+
        '<img src="'+profilUrl+'" class="img-responsive" alt="" decoding="async">'+
        '</div>'+
        linkedElement+
        ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#f1c232; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px;" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
        ((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(eTypes.toString()||"")+'">'+(eTypes.toString()||"")+'</span>':"")+
        '<div class="item-body">'+
        //'<div class="top-wrapper">'+
        //'<span class="cat-date">'+dateAndTags+'</span>'+
        //'<span class="category lowercase">'+(tags.map((v, i)=> "#"+v+(i==tags.length-1?"":", ")).join(""))+'</span>'+
        //'</span>'+
        //'<span class="tag"></span>'+
        //'</div>'+
		(elemAddress!=""?("<i class='fa fa-map-marker margin-right-5'></i>"+elemAddress):"")+
        (params.name?`<h4 class="title">${params.name}</h4>`:"")+
        //'<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
        '<div class="link-bordered">'+
        '<a href="'+params.hash+'" class="lbh">En savoir plus</a>'+//+(params.collection=="events")?"":""
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';
    return str;
}