pageProfil.views.home = function() { 
    if (contextData.type == "organizations"){
        $("#social-header").css("display","none");
        $("#menu-top-profil-social").css("display","none");
        ajaxPost(
            '#central-container',
            baseUrl + '/costum/cercletravailleurssociaux/elementhome/type/' + contextData.type + "/id/" + contextData.id,
            null,
            function() {},
            "html"
        );
    }else{
        pageProfil.views.detail();
    }
};
