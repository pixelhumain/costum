costum[costum.slug] = {
    organizations: {
        formData: function(data){
            $.each(data, function(e, v){
                if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
                if(e == "userSelfEmployed"){  
                    if(v == true  || v == "true"){
                        data.tags.push("en libéral")
                    }else if(v == false  || v == "false"){
                        data.tags.push("en cours de formation")
                    }
                }
                if((data.tags).indexOf("en libéral") >-1 && data["userSelfEmployed"] ==  false && data["userSelfEmployed"] ==  "false" ){
                    data.tags.splice((data.tags).indexOf("en libéral"), 1); 
                }
                if((data.tags).indexOf("en cours de formation") >-1 && data["userSelfEmployed"] == true && data["userSelfEmployed"] == "true" ){
                    data.tags.splice((data.tags).indexOf("en cours de formation"), 1); 
                }
                if (e === "qualificationsStateDiplomas") {
                    vSplit = v.split(",");
                    if(typeof data.tags != "undefined" || notNull(data.tags)) {  
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                        $.each(costum.lists.stateDiplomas,function(kt,vsd){                         
                            if((data.tags).indexOf(vsd) >-1  && (vSplit).indexOf(vsd) == -1){
                                data.tags.splice((data.tags).indexOf(vsd), 1);                                 
                            }
                        })
                    }else{
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                    }
                }
                if(data["qualificationsStateDiplomas"] == "undefined"){
                    $.each(costum.lists.stateDiplomas,function(ktf,tsd){                
                        if((data.tags).indexOf(tsd) >-1){
                            data.tags.splice((data.tags).indexOf(tsd), 1);                                 
                        }
                    })
                }

                if (e === "qualificationsOtherCompetences") {
                    vSplit = v.split(",");
                    if(typeof data.tags != "undefined" || notNull(data.tags)) {  
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                        $.each(costum.lists.otherCompetences,function(kt,vsd){                         
                            if((data.tags).indexOf(vsd) >-1  && (vSplit).indexOf(vsd) == -1){
                                data.tags.splice((data.tags).indexOf(vsd), 1);                                 
                            }
                        })
                    }else{
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                    }
                }
                if(data["qualificationsOtherCompetences"] == "undefined"){
                    $.each(costum.lists.otherCompetences,function(ktf,tsd){                
                        if((data.tags).indexOf(tsd) >-1){
                            data.tags.splice((data.tags).indexOf(tsd), 1);                                 
                        }
                    })
                }

                if (e === "companyInterventionMethods") {
                    if(typeof data.tags != "undefined" || notNull(data.tags)) {
                        if(typeof v == "string"){
                            data.tags.push(v);
                        } else if(typeof v == "object" ){
                            $.each(v, function(i,tag){
                                data.tags.push(tag);
                            });
                        }
                        $.each(costum.lists.interventionMethods,function(kt,vim){             
                            if(typeof v == "string" && (data.tags).indexOf(vim) >-1 && v != vim ){
                                data.tags.splice((data.tags).indexOf(vim), 1); 
                            }else if(typeof v == "object" && (data.tags).indexOf(vim) >-1  && (v).indexOf(vim) == -1){
                                data.tags.splice((data.tags).indexOf(vim), 1); 
                            }
                        })
                    }else{
                        if(typeof v == "string"){
                            data.tags.push(v);
                        } else if(typeof v == "object" ){
                            $.each( v, function(i,tag){
                                data.tags.push(tag);
                            });
                        }
                    }
                }
                if(data["companyInterventionMethods"] == "undefined"){
                    $.each(costum.lists.interventionMethods,function(ktf,tsd){                
                        if((data.tags).indexOf(tsd) >-1){
                            data.tags.splice((data.tags).indexOf(tsd), 1);                                 
                        }
                    })
                }

                if (e === "companyPublicCible") {
                    vSplit = v.split(",");
                    if(typeof data.tags != "undefined" || notNull(data.tags)) {  
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                        $.each(costum.lists.publicCible,function(kt,vsd){                         
                            if((data.tags).indexOf(vsd) >-1  && (vSplit).indexOf(vsd) == -1){
                                data.tags.splice((data.tags).indexOf(vsd), 1);                                 
                            }
                        })
                    }else{
                        $.each(vSplit, function(i,tagsd){
                            if((data.tags).indexOf(tagsd) == -1){                                    
                                data.tags.push(tagsd);
                            }
                        });
                    }
                }
                if(data["companyPublicCible"] == "undefined"){
                    $.each(costum.lists.publicCible,function(ktf,tsd){                
                        if((data.tags).indexOf(tsd) >-1){
                            data.tags.splice((data.tags).indexOf(tsd), 1);                                 
                        }
                    })
                }

                if (e === "linkedin") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["linkedin"] = v;
                }
                if (e === "instagram") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["instagram"] = v;
                }
                if (e === "facebook") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["facebook"] = v;
                }
                if (e === "otherSociaNetworks") {
                    data.socialNetwork = typeof data.socialNetwork != "undefined" ? data.socialNetwork : {};
                    data.socialNetwork["otherSociaNetworks"] = v;
                }
                if (e === "mobile") {
                    data.telephone = typeof data.telephone != "undefined" ? data.telephone : {}; 
                    data.telephone["mobile"] = [v]; 
                }
            });
            return data;
        },
        afterBuild: function(){
            alignInput2(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties ,"admin",4,12,0,null,"Contact(s)","#ba1c24","");
        }
    }
}
paramsMapCO = $.extend(true, {}, paramsMapCO, {
    activePreview : false,
    mapCustom: {
        getPopup: function(data){
            var id = data._id ? data._id.$id:data.id;
            var imgProfil = this.getThumbProfil(data);

            var eltName = data.title ? data.title: ((typeof data.properties != "undefined" && typeof data.properties.name != "undefined") ? data.properties.name : data.name);
           
			var userFirstName = (typeof data.userFirstName != "undefined") ? data.userFirstName : " ";
			var userSurname = (typeof data.userSurname != "undefined") ? data.userSurname : " ";
            var popup = "";
            popup += "<div class='padding-5' id='popup" + id + "'>";
            if(data.imgProfil){
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
            }
            popup += "<span style='margin-left : 5px; font-size:18px'>"+ userFirstName + " "+  userSurname + " " +   eltName + "</span>";

            if(data.tags && data.tags.length > 0){
                popup += "<div style='margin-top : 5px;'>";
                var totalTags = 0;
                $.each(data.tags, function(index, value){
                    totalTags++;
                    if (totalTags < 3) {
                        popup += "<div class='popup-tags'>#" + value + " </div>";
                    }
                })
                popup += "</div>";
            }
            if(data.address){
                var addressStr="";
                if(data.address.streetAddress)
                    addressStr += data.address.streetAddress;
                if(data.address.postalCode)
                    addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                if(data.address.addressLocality)
                    addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                popup += "<div class='popup-address text-dark'>";
                popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                popup += "</div>";
            }
            if(data.shortDescription && data.shortDescription != ""){
                popup += "<div class='popup-section'>";
                popup += "<div class='popup-subtitle'>Description</div>";
                popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                popup += "</div>";
            }
            if((data.url && typeof data.url == "string") || data.email || data.telephone){
                popup += "<div id='pop-contacts' class='popup-section'>";
                popup += "<div class='popup-subtitle'>Contacts</div>";
                
                if(data.url && typeof data.url === "string"){
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                    popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                    popup += "</div>";
                }

                if(data.email){
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                    popup += "</div>";
                }

                if(data.telephone){
                    popup += "<div class='popup-info-profil'>";
                    popup += "<i class='fa fa-phone fa_phone'></i> ";
                    var tel = ["fixe", "mobile"];
                    var iT = 0;
                    $.each(tel, function(keyT, valT){
                        if(data.telephone[valT]){
                            $.each(data.telephone[valT], function(keyN, valN){
                                if(iT > 0)
                                    popup += ", ";
                                popup += valN;
                                iT++; 
                            })
                        }
                    })
                    popup += "</div>";
                }

                popup += "</div>";
                popup += "</div>";
            }
            if(data.collection && id){
                var url = '#page.type.' + data.collection + '.id.' + id;
                popup += "<div class='popup-section'>";
                if(this.activePreview)
                    popup += "<a href='" + url + "' class='lbh-preview-element "+this.popUpBtnClass+" item_map_list popup-marker' id='popup" + id + "'>";
                else
                    popup += "<a href='" + url + "' target='_blank' class='lbh "+this.popUpBtnClass+" item_map_list popup-marker' id='popup" + id + "'>";
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></a>';
            }
            else if (typeof data.isOsmData != "undefined" && data.isOsmData) {
                var action = (typeof data.actionBtn != "undefined" && data.actionBtn != null && typeof data.actionBtn.action != "undefined") ? data.actionBtn.action : "openPreviewOsm";
                let id = (typeof data.osmId != "undefined") ? data.osmId : "";
               
                popup += `<div data-id="`+id+`"  style='cursor:pointer;' onClick="`+action+`('osmPopupBtn` + id + `')" class='popup-marker' id='osmPopupBtn` + id + `'>`;
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></div>';
            }
            popup += '</div>';
            popup += '</div>';

            return popup;
        },
    }
});
