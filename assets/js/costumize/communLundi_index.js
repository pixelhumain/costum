costum.communLundi={
    init: function(){
        var getTemplateEvents = function(){
        ajaxPost(
            null,
            baseUrl +"/"+moduleId+ "/search/globalautocomplete",
            {"searchType":["events"],filters:{"tags":"Atelier Comm'un Lundi récurrent"}},
            function(res){
                // costum.lists.pattern={};
                $.each(res.results,function(k,v){
                    costum.lists.pattern[k]=v.name;
                });
            }

        );
        }
        getTemplateEvents();
        coInterface.bindButtonOpenForm = function(){
			$(".btn-open-form").off().on("click",function(){
				
		        var typeForm = $(this).data("form-type");
		        currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
		        //dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
		        if(contextData && contextData.type && contextData.id ){
		            if(typeForm=="event"){
						dyFObj.openForm(typeForm,"afterLoad");
					}else{
		                dyFObj.openForm(typeForm,"sub");
					}	
		        }
		        else{
		        	if(typeForm=="event"){
						dyFObj.openForm(typeForm,"afterLoad");

					}else{
		                dyFObj.openForm(typeForm);
					}
		        }
		    });
		};  
    },
    "events" : {
        formData : function(data){
            mylog.log("formData events", data);
            if(typeof data.template!="undefined"){
                // alert("template");
                if(typeof data.tags=="undefined"){
                    data.tags=[];
                }
                //    alert("tags");
                if(data.template=="true"){
                    data.tags.push("Atelier Comm'un Lundi récurrent");
                }
            }else{
                if(typeof data.tags!="undefined" && data.tags.length>0){
                    delete data.tags;
                }
            }
            if(typeof data.parent!="undefined"){
                data.parent[costum.contextId]={
                    "type" : costum.contextType,
                    "name" : costum.title
               }
            }
            return data;
        },
        afterBuild : function(data){
            mylog.log("afterbuild event", data);
            // alert("afterbuild");
            dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(){
                // // -------- start original onload -------
                // if ($("#ajaxFormModal #recurrency").is(':checked')) {
                //     $("#ajaxFormModal .allDaycheckbox").fadeOut("slow");
                //     $("#ajaxFormModal .startDatedatetime").fadeOut("slow");
                //     $("#ajaxFormModal .endDatedatetime").fadeOut("slow");
                //     $("#ajaxFormModal .openingHoursopeningHours").fadeIn("slow");
                // } else {
                //     $("#ajaxFormModal .openingHoursopeningHours").fadeOut("slow");
                //     $("#ajaxFormModal .allDaycheckbox").fadeIn("slow");
                //     $("#ajaxFormModal .startDatedatetime").fadeIn("slow");
                //     $("#ajaxFormModal .endDatedatetime").fadeIn("slow");
                // }    
                // // -------- end original onload -------    
                mylog.log("afterLoad event");


                $("#ajaxFormModal #btn-submit-form").toggle();
                $("#ajaxFormModal .form-group").toggle();
                $("#ajaxFormModal .openingHoursopeningHours").hide();
                $("#ajaxFormModal .patternselect").show();
                $(".selectPattern").on("change",function(){

                    pattern=$(this).val();
                    typeElt=dyFObj[dyFObj.activeElem].col;
                    ctrlElt=dyFObj[dyFObj.activeElem].ctrl;

                    if(pattern=="Aucun"){
                        var editDynF=jQuery.extend(true, {},costum.typeObj[typeElt].dynFormCostum);
                        editDynF.onload.actions.hide.patternselect = 1;
                        if(costum.isCostumAdmin){
                            editDynF.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
                                "onText": trad.yes,
                                "offText": trad.no,
                                "onLabel": tradDynForm.public,
                                "offLabel": tradDynForm.private,
                                "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
                                "labelInformation": tradDynForm.explainvisibleevent
                            })
                        }
                        var presetData={};
                        if(data.startDate){
                            presetData.startDate=data.startDate;

                        }
                        if(data.endDate){
                            presetData.endDate=data.endDate;
                            
                        }
                        dyFObj.openForm(ctrlElt, null, presetData, null, editDynF);
                    }else{
                    


                    ajaxPost(
                        null,
                        baseUrl+"/"+moduleId+"/element/get/type/"+typeElt+"/id/"+pattern,
                        {},
                        function(res){
                            mylog.log("res elt",res);
                            // dyFObj.closeForm();
                            // var elt=res[Object.keys(res)[0]];
                            // mylog.log("res elt prep",elt);
                            var editDynF=jQuery.extend(true, {},costum.typeObj[typeElt].dynFormCostum);
                            editDynF.onload.actions.hide.patternselect = 1;
                            editDynF.onload.actions.hide.similarLinkcustom = 1;
                            editDynF.onload.actions.hide.templatetext = 1;
                            // editDynF.onload.actions.presetValue={
                            //     "tags":{
                            //         "value":"",
                            //         "forced":true
                            //     }
                            // };
                            editDynF.beforeBuild.properties.tags.values=[];

                            if(data.startDate){
                                res.map.startDate=data.startDate;
    
                            }
                            if(data.endDate){
                                res.map.endDate=data.endDate;
                                
                            }

                            dyFObj.openForm(ctrlElt,null,res.map, null, editDynF);
                            // $("#similarLink").hide();
                        }
                    );
                    }
                    
                });
            };    
        },
        afterSave : function(data){
            mylog.log("aftersave events",data);
            urlCtrl.loadByHash(location.hash);
            getTemplateEvents();

        }    
    }

};