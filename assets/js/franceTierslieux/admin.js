adminPanel.views.tiersLieux = function(){
	var data={
		title : "Gestion des Tiers-lieux",
		paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations"]
            },
            filters : {
                text : true
            
            }
        },
		table : {
            name: {
                name : "Nom"
            },
            created: {
                name : "Créé le"
            },
            description : {
            	name : "Description",
            	class : "col-xs-2 text-center"
            },
            private : { 
            	name : "Affichage",
        		class : "col-xs-1 text-center"
        	}
        },
        actions : {
            private : true,
            delete : true
        },
        csv : [
            {
                name : "Tous les TLs",
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                        indexStep : 0,
                        fields : [
                            "name","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","geo.latitude","geo.longitude"
                        ]
                }
            },
            {
                name : "TL mis à jour depuis le 20 mars 2023",
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                        indexStep : 0,
                        fields : [
                            "name","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","geo.latitude","geo.longitude"
                        ],
                        extraFilters : {
                            "updated" : {
                                "$gt" :  1679270400
                            }
                        }
                }
            }
        ]
    };
		
		
	
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.existingReference = function () {
    ajaxPost('#content-view-admin', baseUrl + '/costum/francetierslieux/existingreference', {}, function () { }, "html");
};