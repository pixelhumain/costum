paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom: {
		icon: {
			getIcon: function (params) {
				var elt = params.elt;
				mylog.log("icone ftl", elt.tags);
				if (typeof elt.tags != "undefined" && elt.tags != null) {
					if ($.inArray("Compagnon France Tiers-Lieux", elt.tags) == -1) {
						var myCustomColour = '#4623C9';
					} else {
						var myCustomColour = '#FF286B';
					}
				} else {
					var myCustomColour = '#4623C9';
				}

				var markerHtmlStyles = `
					background-color: ${myCustomColour};
					width: 3.5rem;
					height: 3.5rem;
					display: block;
					left: -1.5rem;
					top: -1.5rem;
					position: relative;
					border-radius: 3rem 3rem 0;
					transform: rotate(45deg);
					border: 1px solid #FFFFFF`;

				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: `<span style="${markerHtmlStyles}" />`
				});
				return myIcon;
			}
		},
		getClusterIcon: function (cluster) {
			var childCount = cluster.getChildCount();
			var c = ' marker-cluster-';
			if (childCount < 100) {
				c += 'small-ftl';
			} else if (childCount < 1000) {
				c += 'medium-ftl';
			} else {
				c += 'large-ftl';
			}
			return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
		}
	}
});

$("#show-button-map").addClass("changelabel");

$("#main-search-bar").attr("placeholder", "Quel tiers-lieu recherchez-vous ?");
//$("#panel_map").css('display','block');
//$(".menuRight_header_title").text("Résultat");
$("#btn-filters").parent().css('display', 'none');
$("#btn-panel").html('<i class="fa fa-tags"></i><span>Filtre par mot-clé</span>');
//$("#result").parent().attr("class","col-xs-4");
$("#btn-panel").parent().attr("class", "col-xs-6 pull-right");
//$("#btn-panel").parent().css("overflow","hidden")
$("#input_name_filter").attr("placeholder", "Filtre par nom ...");
$('.form-register').find(".agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='https://francetierslieux.fr/cgu/' target='_blank' style='color: #FF286B;'>CGU</a>.");
$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath + "/images/franceTierslieux/logo-02.png");
$(".form-email-activation .container .row div:first-child h3").replaceWith("<div class=col-xs-12><h3 style='color: #5158ca' class='text-title'>VOUS N'AVEZ PAS REÇU VOTRE MAIL AFIN DE VALIDER VOTRE COMPTE</h3><span class='interogation'>?</span></div>");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
// <a href='' target='_blank' class='text></a>

var changeLabel = function () {
	$("#menuRight").on("click", function () {
		if(location.href.indexOf("obstl2023") > -1){
            urlCtrl.openPreview('view/url/costum.views.tpls.blockCms.map.shapeVisualisation', {"blockCms": {"click": "answers"}})
		}else{
			if ($(this).find("a").hasClass("changelabel")) {
				$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
			}
			else {
				$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
			}
			$(this).find("a").toggleClass("changelabel");
		}
		//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
	});
};

var getDashboardData = function(option="", page="", callBack=null, scopeId=null){
	var params = {
		"costumId":costum.contextId, 
			"costumSlug": costum.contextSlug,
			"costumType":costum.contextType,
			"specificBlock":["custom.franceTierslieux.graph.answerByMembers"],
			"page": page,
			"scopeId" : scopeId,
			"countByQuestion": true,
			"extraFilter" : {"answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s":{$exists:true}}
	}
	//if(extraFilter!=null){
	//	params["extraFilter"] = extraFilter;
	//}
	ajaxPost(
		null, 
		baseUrl+"/costum/blockgraph/getdashboarddata"+option, 
		params, 
		function(res){
			costum["dashboardData"] = res["data"];
			costum["dashboardGlobalConfig"] = res["global"];
			if(typeof callBack=="function"){
				callBack(res);				
			}
		},
		null,
		null,
		{async:false}
	)
}

dyFObj.unloggedMode = true;
costum.searchExist=function(type,id,name,slug,email){
	var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
	editDynF.onload.actions.hide.similarLinkcustom=1;
	dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");
};
costum.franceTierslieux = {
    init : function () {

				
        costum.typeObj.organizations.dynFormCostum.prepData = function (data) {
            mylog.warn("dynformcostum prepdata", data);
            if (notNull(data.map)) {
                $.each(costum.lists, function (e, v) {
                    constructDataForEdit = [];
                    $.each(v, function (i, tag) {
                        if ($.inArray(tag, data.map.tags)>=0) {
                            constructDataForEdit.push(tag);
                            data.map.tags.splice(data.map.tags.indexOf(tag), 1);
                        }
                    });
                    data.map[e] = constructDataForEdit;
                });
            }
            if (notNull(data.map.photo)) {
                uploadObj.initList.photo = data.map.photo;
            }
            if(typeof data.map.telephone!="undefined"){
			    if(typeof data.map.telephone.fixe=="object"){
				    data.map.telephone=data.map.telephone.fixe[0];
				}else if(typeof data.map.telephone.mobile=="object"){
					data.map.telephone=data.map.telephone.mobile[0];
				}
			}
			if(typeof data.map.socialNetwork!="undefined"){
				mappingSocialNetwork={
					"facebook" : "votre page Facebook", 
                    "twitter" : "votre compte Twitter", 
                    "instagram" : "votre compte Instagram", 
                    "linkedin" : "votre compte Linkedin", 
                    "mastodon" : "votre compte Mastodon", 
                    "movilab" : "votre page Movilab"
				};
				socialArray=[];
				$.each(data.map.socialNetwork,function(name,link){
					var socialObj={
						platform : mappingSocialNetwork[name],
						url : link
					};
					socialArray.push(socialObj);
				});
				data.map.socialNetwork=socialArray;
			}
            
            return data;
        };
		
		initParticleForTLDashboard();
		
		dyFObj.formInMap.forced.countryCode = "FR";
		dyFObj.formInMap.forced.map = { "center": ["46.7342232", "2.74686000"], zoom: 5 };
		//dyFObj.formInMap.forced.showMap=true;
		$('#mapContent').append('<div class="pull-right BtnFiltersLieux"></div>');
		$('.BtnFiltersLieux').html(
			'<div id="mainMenuFilter ">' +
			'<div class="list-group panel" id="filters-nav">  ' +
			// '<a href="javascript:;"><button data-type="tags" data-key="0" data-value="Compagnon France Tiers-Lieux" class="btn-filters-select compagnon" id="colCompagnon"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title="Tiers-lieux accueillant et donnant conseil à des porteurs de projet qui souhaitent en savoir plus sur les tiers-lieux"><i class="icon-info fa fa-info-circle"></i></span> Tiers-lieux compagnons</button></a>' +
			//  	'<a href="javascript:;"><button data-field="category" data-type="category" data-key="network" data-value="network" class="btn-filters-select network" id="colNetwork"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title=""><i class="icon-info fa fa-info-circle"></i></span> Les réseaux</button></a>'+
			'</div>' +
			'</div>'
		);

		getDashboardData("", location.hash.replace("#", ""), null, null);

		$(".logoLoginRegister").attr("src", assetPath + "/images/franceTierslieux/logo-02.png");
		var html = '<div id="popup-Tlieux" class="modal fade in animated bounceInDown" role="dialog">' +
			'<div class="modal-dialog">' +
			'<div class="modal-content row">' +
			'<div class="modal-header custom-modal-header">' +
			'<button type="button" class="close" data-dismiss="modal">×</button>' +
			'</div>' +
			'<div class="modal-body">' +
			'<div class="row">' +
			'<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs left-Tlieux">' +
			'<div class="contact-info">' +
			/*'<div class="text-center cont-icon">'+
				'<i class="fa fa-map-marker fa-4x mb-3 animated bounce"></i>'+
				'<img src="'+assetPath+'/images/franceTierslieux/soulignement.svg" alt="image"/>'+
			'</div>'+*/
			'<img src="' + assetPath + '/images/franceTierslieux/logo.svg" alt="image"/>' +
			'</div>' +
			'</div>' +
			'<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">' +
			'<div class="welcome-txt text-center">' +
			'Bienvenue sur<br> <strong>la cartographie nationale des tiers-lieux</strong>, pour en savoir plus sur les tiers-lieux les plus proches de chez vous !' +
			'</div>' +
			'<div class="text-center">' +
			'<img class="space-paragraph" src="' + assetPath + '/images/franceTierslieux/cartographie.png" alt="image"/>' +
			'</div>' +
			'<p class="section-paragraph text-center">' +
			'Si votre tiers-lieu ne se trouve pas sur la carte, <a href="javascript:;" data-form-type="organization" class="btn-open-form" id="btn-add-tl">vous pouvez l’ajouter vous-même</a> !' +
			'</p>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';
		//modal de confirmation
		html += '<div class="modal fade text-center py-5"  id="addConfirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
			'<div class="modal-dialog modal-md" role="document">' +
			'<div class="modal-content">' +
			'<div class="modal-body">' +
			'<div class="text-center cont-icon">' +
			'<img src="' + assetPath + '/images/franceTierslieux/cartographie.png" alt="image"/>' +
			'</div>' +
			'<h3 class="pt-5 mb-0 text-secondary text-confirm">Votre <span id="catTl"></span> a bien été enregistré</h3>' +
			'<div class="append-confirm-message">' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		var recens = '<div id="popupRecensement" class="modal fade in animated bounceInDown" role="dialog">' +
			'<div class="modal-dialog">' +
			'<div class="modal-content row">' +
			'<div class="modal-header custom-modal-header">' +
			'<button type="button" class="close" data-dismiss="modal">×</button>' +
			'</div>' +
			'<div class="modal-body">' +
			'<div class="row">' +
			'<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs left-Tlieux">' +
			'<div class="contact-info">' +
			/*'<div class="text-center cont-icon">'+
				'<i class="fa fa-map-marker fa-4x mb-3 animated bounce"></i>'+
				'<img src="'+assetPath+'/images/franceTierslieux/soulignement.svg" alt="image"/>'+
			'</div>'+*/
			'<img src="' + assetPath + '/images/franceTierslieux/logo.svg" alt="image"/>' +
			'</div>' +
			'</div>' +
			'<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">' +
			'<img src="https://www.communecter.org/upload/communecter/organizations/5ec3b895690864db108b46b8/album/Banniere-site-recensement.png" style="width:100%;text-align:center;display:inline-block;">' +
			'<div class="welcome-txt text-center section-paragraph">' +
			'<span style="font-weight:bold;">Du 22 mars au 10 mai</span>, vous pouvez répondre au questionnaire Recensement des Tiers-Lieux en suivant ce lien : <a href="#recensement2023" id="btn-answer-recensement" style="display: block;word-wrap: break-word;" class="lbh" onclick="">https://cartographie.francetierslieux.fr/#recensement2023</a>' +
			'</div>' +
			// '<div class="text-center">'+
			// 	'<img class="space-paragraph" src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
			// '</div>'+
			// '<p class="section-paragraph text-center">'+
			// 	'Si votre tiers-lieu ne se trouve pas sur la carte, <a href="javascript:;" data-form-type="organization" class="btn-open-form" id="btn-add-tl">vous pouvez l’ajouter vous-même</a> !'+
			// '</p>'+
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';


		$('body').append(html);
		//$(window).on('load',function(){
		$('#popup-Tlieux').modal('show');

		// $('#popupRecensement').modal('show');
		coInterface.bindLBHLinks();
		//});

		//Ajout band sur le top et dessous du modal
		/*$( '.modal-content' ).prepend( '<div class="top-strip" style="display: none;"></div>' );
		  $( '.modal-content' ).append( '<div class="bottom-strip" style="display: none;"></div>' );*/

		// Ajouter style avec logo sous le titre du modal
		var underStyle = '<div class="text-center under-title" style="display: none;">' +
			'<img class="space-paragraph" src="' + assetPath + '/images/franceTierslieux/cartographie.png" alt="image">' +
			'</div>';


		$('.modal-header').append(underStyle);

		//Add Tiers lieux on popup
		$('#btn-add-tl').on('click', function () {
			//alert("okok");
			$("#popup-Tlieux").modal('hide');
		});

		$('#btn-answer-recensement').on('click', function () {
			//alert("okok");
			$("#popupRecensement").modal('hide');
		});



		const popupTlieux = localStorage.getItem('popupTlieux');
		const popupRecensement = localStorage.getItem('popupRecensement');

		if (popupTlieux === 'false') {
			$('#popup-Tlieux').modal('hide');
		}

		// if(popupRecensement === 'false'){
		//   $('#popupRecensement').modal('hide');
		// }

		$('#popup-Tlieux .custom-modal-header .close').on('click', function () {
			localStorage.setItem('popupTlieux', 'false');
		});
		// $('#popupRecensement .custom-modal-header .close').on('click', function(){
		//   localStorage.setItem('popupRecensement', 'false');
		// });


		function getNetwork() {
			mylog.log("---------getNetwork");

			var params = {
				contextId: costum.contextId,
				contextType: costum.contextType
			};

			ajaxPost(
				null,
				baseUrl + "/costum/franceTierslieux/getnetwork",
				params,
				function (data) {
					//alert("dd");
					mylog.log("callbacknet", data);
					$.each(data, function (e, v) {
						var posNet = $.inArray(v.name, costum.lists.network);
						if (posNet > -1) {
							mylog.log("networks", posNet, costum.lists.network);
							costum.lists.network.splice(posNet, 1);


							// var posNet=	$.inArray(v,costum.lists.network);
							// alert(posNet);
						}
					});
				},
				null,
				null,
				{ async: false }
			);

		};

		var networkFullList = [
			"A+ C'est Mieux",
			"Actes‐IF",
			"Artfactories/Autresparts",
			"Bienvenue dans la Canopée",
			"Cap Tiers‐Lieux",
			"Cédille Pro",
			"Collectif des Tiers‐Lieux Ile‐de‐France",
			"Collectif Hybrides",
			"Compagnie des Tiers‐Lieux",
			"Coopérative des Tiers‐Lieux",
			"Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)",
			"Cowork'in Tarn",
			"Coworking Grand Lyon",
			"CRLII Occitanie",
			"GIP Recia",
			"Hub France Connectée",
			"L'ALIM (l'Assemblée des lieux intermédiaires marseillais)",
			"La Trame 07",
			"Label Tiers‐Lieux Occitanie",
			"Label C3",
			"Label Tiers‐Lieux Normandie",
			"Le DOG",
			"Le LIEN",
			"Lieux Intermédiaires en région Centre",
			"Réseau des tiers‐lieux Bourgogne Franche Comté",
			"Réseau Français des Fablabs",
			"Réseau Médoc",
			"Réseau TELA",
			"Tiers‐Lieux Edu"
		];

		urlCtrl.loadableUrls["#answer"].useFooter = false;
		mylog.log("coInterface.menu.set pageMenu answer footer", urlCtrl.loadableUrls["#answer"].useFooter);

		var inputs2Tags = [
			"multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y582k4q3cven",
			"multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab",
			"multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4"
		];

		$.each(inputs2Tags, function (i, kunik) {
			costum[kunik + "Callback"] = function (params) {
				mylog.log("costum Callback multicheckbox", params);
				var fullOptions = (typeof window[kunik + "Options"] != "undefined") ? window[kunik + "Options"] : null;
				mylog.log("Options checkbox", fullOptions);
				if (params.text.length > 0) {
					var activityTL = [];
					$.each(params.text, function (ind, val) {
						var tag = Object.keys(val)[0];
						mylog.log("yooyo", tag);
						var tagInd = fullOptions.indexOf(tag);
						fullOptions.splice(tagInd, 1);
						if (tag != "Autre" || tag != "Autres") {
							activityTL.push(tag);
						}
					});
					if (typeof params.elt.links.organizations != "undefined" && Object.keys(params.elt.links.organizations).length > 0) {
						var idElem = Object.keys(params.elt.links.organizations)[0];
						var typeElem = params.elt.links.organizations[Object.keys(params.elt.links.organizations)[0]].type;
						var projTagsElem = {
							id: idElem,
							collection: typeElem,
							path: "tags",
							value: { '$each': activityTL },
							arrayForm: true
						};

						dataHelper.path2Value(projTagsElem, function (params) {
						});


						projTagsElem.value = fullOptions;
						projTagsElem.pull = true;
						dataHelper.path2Value(projTagsElem, function (params) {
						});
					}

				}

			};
		});

		coInterface.bindButtonOpenForm = function () {
			$(".btn-open-form").off().on("click", function () {
				mylog.log("networkFullList", networkFullList);
				//pb de reset de la liste network au clic
				costum.lists.network = networkFullList;
				var typeForm = $(this).data("form-type");
				if (typeForm == "reseau") {
					getNetwork();
				}
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				//dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
				if (contextData && contextData.type && contextData.id) {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");
					} else {
						dyFObj.openForm(typeForm, "sub");
					}
				}
				else {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");

					} else {
						dyFObj.openForm(typeForm);
					}
				}
			});
		};
		coInterface.bindButtonOpenForm();
		if(location.href.indexOf("obstl2023") > -1){
			$("#show-button-map").empty().html("<i class='fa fa-map-marker'></i> Filtrer par région");
		}
		changeLabel();
	},
	"organizations": {
		formData: function (data) {
			//alert("formdata");
			mylog.log("formData In", data);

			if (typeof finder != "undefined" && Object.keys(finder.object).length > 0) {
				$.each(finder.object, function (key, object) {
					if (typeof formData[key] != "undefined") {
						delete formData[key];
					}
				});
			}


			//if(dyFObj.editMode){
				$.each(data, function(e, v){
					//alert(e, v){
					if(typeof costum.lists[e] != "undefined"){
						if(notNull(v)){
							
							if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
							if(typeof v == "string"){
								// var newT="";
								// if(e=="manageModel" && v=="Autre"){
								// 	newT="Autre mode de gestion";
								// }
                                // if(e=="typePlace" && v=="Autre"){
                                //     newT="Autre famille de tiers-lieux";
                                // }
								data.tags.push(v);
								
							}else{
								$.each(v, function(i,tag){
									// var newT="";
									// if(e=="typePlace" && tag=="Autre"){
									// 	newT="Autre famille de tiers-lieux";
									// }
                                    // if(e=="manageModel" && tag=="Autre"){
                                    //     newT="Autre mode de gestion";
                                    // }
									data.tags.push(tag);	
								});
							}
						}
						delete data[e];
					}
			    });
			if (typeof data.category != "undefined" && data.category == "network") {
				if (typeof data.tags == "undefined" || !notNull(data.tags)) data.tags = [];
				data.tags.push("Réseau de tiers-lieux");
				//delete data.mainTag;
			}

            if(typeof data.telephone == "string"){
				var mapIndicTel={
					"FR" : "33",
					"GP" : "590",
					"MQ" : "596",
					"GF" : "594",
					"RE" : "262",
					"PM" : "508",
					"YT" : "269",
					"WF" : "681",
					"PF" : "689",
					"NC" : "687"
				};

				var phone=data.telephone;
				var numIndex=2;
				phone=phone.replace(/\s/g,'');
				// remplacer 00 par +
				phone=(phone.substring(2,0)=="00") ? "+"+phone.substring(2) : 
				    ((phone.substring(0,1)!="+" && typeof data.address!="undefined" && typeof data.address.addressCountry!="undefined" && typeof mapIndicTel[data.address.addressCountry]!="undefined") 
					   ? "+"+mapIndicTel[data.address.addressCountry]+phone.substring(1) : phone );
				if(phone.substring(0,1)=="+"){
					if(phone.substring(1,3)=="33"){
						numIndex=4;
					}else{
						numIndex=5;
					}
				}
				
				var mobOrfixeInd=phone.substring(numIndex,numIndex-1);
				var typeNumber= (mobOrfixeInd=="6" || mobOrfixeInd=="7") ? "mobile" : "fixe" ;	
			    data.telephone={};
				data.telephone[typeNumber]=[phone];
			}

			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
            // if (data.tags.includes("Autre") || data.tags.includes("Autres")) {
            //     $.each(data.tags, function (i, tag) {
            //         if (tag=="Autre" || tag=="Autres") {
            //             delete data.tags[i];
            //         }
            //     });
            // }

			if (data.socialNetwork) {
				var formatedSocialNetwork={};
                $.each(data.socialNetwork,function(i,net){
					var labelArray=net["platform"].split(" ");
  
					var nameSocNet=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["url"]!="undefined" || net["url"]!=""){
                        formatedSocialNetwork[nameSocNet]=net["url"];						
					}
					delete data.socialNetwork[i];

				});
                data.socialNetwork=formatedSocialNetwork;
			}

			mylog.log("formData return", data);
			return data;
		},
		afterSave: function (data) {
			//uploadObj.afterLoadUploader=false;

			//alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function () {
				mylog.log("callback aftersaved", data);
				var category = "tiers-lieu";
				if (typeof data.map.tags != "undefined" && $.inArray("Réseau de tiers-lieux", data.map.tags) > -1) {
					category = "réseau";
				}
				mylog.log("category aftersave", category);
				$("#catTl").html(category);
				$("#ajax-modal").modal('hide');
				//$("#addConfirmModal").modal('show');

				//  }
				if (userId == "") {
					if (!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite éléctronique</span>");

					// 				$('#addConfirmModal').on('hidden.bs.modal', function () {
					// 					onchangeClick=false;
					// 					history.replaceState({}, null, uploadObj.gotoUrl);
					// //					location.hash(uploadObj.gotoUrl);
					// 				  	window.location.reload();
					// 				});
				}
				// else{
				// 	urlCtrl.loadByHash(uploadObj.gotoUrl);
				// }
			});
		},
		beforeSave: function (data) {
			mylog.log("beforeSave data", data);
			if (notNull(data.newElement_city)) {
				delete data.newElement_city;
			}

			if (notNull(data.newElement_street)) {
				delete data.newElement_street;
			}
			return data;
		},
		afterBuild: function (data) {
			mylog.log("afterbuild FTL data", data);
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad = function (data) {
				mylog.log("afterload FTL data", data);
				// cache la carto
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				// $(".formLocality input[name='newElement_city']").attr("type","number");
				$("#divMapLocality").addClass("hidden");
				dyFObj.formInMap.bindStreetResultsEvent = function () {
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
						// only one address (no push);
						dyFInputs.locationObj.elementLocations = [];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if (!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/) {
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
					dyFObj.formInMap.valideLocality = function (country) {
						mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

						if (notEmpty(dyFObj.formInMap.NE_lat)) {
							locObj = dyFObj.formInMap.createLocalityObj();
							mylog.log("formInMap copyMapForm2Dynform", locObj);
							dyFInputs.locationObj.copyMapForm2Dynform(locObj);
							// dyFInputs.locationObj.addLocationToForm(locObj);
						}

						// dyFObj.formInMap.initVarNE();
						dyFObj.formInMap.resumeLocality();

						dyFObj.formInMap.btnValideDisable(false);
						toastr.success("Adresse enregistrée");

						dyFObj.formInMap.initHtml();
						// dyFObj.formInMap.newAddress(false);
						$("#btn-submit-form").prop('disabled', false);
						// $('#ajaxFormModal #divNewAddress').show();


					};

					$("#ajaxFormModal .item-street-found").off().click(function () {
						if (typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0) {
							dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
							setTimeout(function () { dyFObj.formInMap.mapObj.map.setZoom(17) }, 1000);
							$("#divMapLocality").removeClass("hidden");
							$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
						}
						var streetAddressName = $(this).text().split(",")[0].trim();

						$('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);

						dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();

						mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
						$("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
						$('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
						$('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));


						dyFObj.formInMap.NE_lat = $(this).data("lat");
						dyFObj.formInMap.NE_lng = $(this).data("lng");
						dyFObj.formInMap.showWarningGeo(false);

						//Valider au click du résultat
						dyFObj.formInMap.valideLocality();

						$("#ajaxFormModal #sumery").show();
						// $("#ajaxFormModal #divMapLocality").show();

						dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
							var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
							mylog.log('marker drop event', latLonMarker);
							dyFObj.formInMap.NE_lat = latLonMarker.lat;
							dyFObj.formInMap.NE_lng = latLonMarker.lng;

							dyFObj.formInMap.valideLocality();
							toastr.success("Coordonnées mises à jour");
						});

					});

				};

				if (Object.keys(data).length == 0 || typeof data.address == "undefined" || (typeof data.address != "undefined" && Object.keys(data.address).length == 0)) {
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				} else if (typeof data.address != "undefined" && Object.keys(data.address).length > 0) {
					$('#ajaxFormModal #divNewAddress').hide();
				}

			};
			mylog.log("afterbuild", data);
			if(typeof data.manageModel=="undefined" || (typeof data.manageModel!="undefined" && !data.manageModel.includes("Autre mode de gestion"))){
					// alert("showthos");
				$("#ajaxFormModal .extraManageModeltext").hide();
			}
			if(typeof data.typePlace=="undefined" || (typeof data.typePlace!="undefined" && !data.typePlace.includes("Autre famille de tiers-lieux"))){
				$("#ajaxFormModal .extraTypePlacetext").hide();
			}

			$("#divCity input").attr("placeholder", "Saisir votre code postal");



			//mylog.log("manageModel param",typeof param, param);
			// $("#ajaxFormModal .manageModelselect select").on('click', function () {
			// 	mylog.log("Saving previous manageModel " + $(this).val());
			// 	$(this).data('manageModel', $(this).val());
			// });

			$("#ajaxFormModal .manageModelselect select").change(function () {
				var modelStatus=$(this).val();
			    if(modelStatus!="Autre mode de gestion"){
			    //    $("#ajaxFormModal .extraManageModeltext").hide();
			       $("#ajaxFormModal .extraManageModeltext input").val("");
				   $("#ajaxFormModal .extraManageModeltext").hide();
			    }
			    //alert(modelStatus);
			    var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre mode de gestion"	: "Group"
			    }; 
			    $("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});


			$("#ajaxFormModal .typePlaceselect select").change(function () {
				var placeCat=$(this).val();
			    if(placeCat==null || placeCat.includes("Autre famille de tiers-lieux")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(placeCat.includes("Autre famille de tiers-lieux")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function () {
				//alert("change");
				//var prevTypePlace = $(this).data('typePlace');
				var themeNetwork = $(this).val();
				if (themeNetwork.includes("Autres") == false) {
					$("#ajaxFormModal .extraThemeNetworktext").hide();
					$("#ajaxFormModal .extraThemeNetwork input").val("");
				}
				if (themeNetwork.includes("Autres")) {
					$("#ajaxFormModal .extraThemeNetworktext").show();
					//$("#ajaxFormModal .extraTypePlacetext input").focus();
				}
			});

			$("#ajaxFormModal .localNetworkselect select").change(function () {
				//alert("change");
				//var prevTypePlace = $(this).data('typePlace');
				var localNetwork = $(this).val();
				if (localNetwork.includes("Autres") == false) {
					$("#ajaxFormModal .extraLocalNetworktext").hide();
					$("#ajaxFormModal .extraLocalNetwork input").val("");
				}
				if (localNetwork.includes("Autres")) {
					$("#ajaxFormModal .extraLocalNetworktext").show();
					//$("#ajaxFormModal .extraTypePlacetext input").focus();
				}
			});


			//init openingHours
			if(typeof data.openingHours!="undefined"){
			    $.each(openingHoursResult, function(e,v){
      				mylog.log("custom init openinghours",data.openingHours,e);
      				if(typeof data.openingHours[e]=="undefined" || !notEmpty(data.openingHours[e]) ){
      					openingHoursResult[e].disabled=true;
      				}
      			});	      
			}
			$(".btn-select-day").off().on("click", function () {
				mylog.log("selectDay", openingHoursResult, $(this).data("key"));
				key = $(this).data("key");
				if ($(this).hasClass("active")) {
					$(this).removeClass("active");
					$.each(openingHoursResult, function (e, v) {
						if (v.dayOfWeek == key)
							openingHoursResult[e].disabled = true;
					});
					$("#contentDays" + key).fadeOut();
				} else {
					$(this).addClass("active");
					$.each(openingHoursResult, function (e, v) {
						if (v.dayOfWeek == key)
							delete openingHoursResult[e].disabled;
					});
					$("#contentDays" + key).fadeIn();
				}
			});

			$('.timeInput').off().on('changeTime.timepicker', function (e) {
				mylog.log("changeTimepicker");
				var typeInc = $(this).data("type");
				var daysInc = $(this).data("days");
				var hoursInc = $(this).data("value");
				var firstEnabled = null;
				var setFirst = false
				$.each(openingHoursResult, function (i, v) {
					if (!setFirst && typeof openingHoursResult[i].disabled == "undefined") {
						mylog.log("firtEnable", v.dayOfWeek);
						firstEnabled = v.dayOfWeek;
						setFirst = true;
					}
					if (firstEnabled == daysInc) {
						mylog.log("first enabled change causes recursive change", v.dayOfWeek);
						openingHoursResult[i]["hours"][hoursInc][typeInc] = e.time.value;
						var typeHours = "";
						if (typeInc == "opens") {
							typeHours = "start";
						} else if (typeInc == "closes") {
							typeHours = "end";
						}
						if (typeHours != "") {
							$("#" + typeHours + "Time" + v.dayOfWeek + hoursInc).val(e.time.value);
						}
					} else if (v.dayOfWeek == daysInc) {
						openingHoursResult[i]["hours"][hoursInc][typeInc] = e.time.value;
					}
				});
			});

			$('.timeInput').on("focus", function () {
				$(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();
		}


		// 	alert("after build TL");
		// 	if(userId==""){
		// 		$("#ajaxFormModal").before(
		// 			'<div class="first-register">'+
		// 				'<form id="check-register-by-email" role="form">'+
		//                   		'<div class="form-group input-group" style="margin-top: 20px;">'+
		//                       		'<span class="input-group-addon">@</span>'+
		//                       		'<input type="text" class="form-control" name="registerEmail" id="check-email-register" placeholder="Entrez e-mail" required>'+
		//                   		'</div>'+
		//                   		'<div class="contain-btn">'+
		//                   			'<a href="javascript:dyFObj.closeForm(); " class="mainDynFormCloseBtn btn btn-default btn-cancel" style="margin-right:10px;">'+
		//                   				'<i class="fa fa-times "></i> Annuler'+
		//                   			'</a>'+
		//                         '<button type="submit" class="btn btn-default btn-validate-email">Valider '+
		//                             '<i class="fa fa-arrow-circle-right"></i>'+
		//                         '</button>'+
		//                   		'</div>'+
		//   					'</form>'+
		// 			'</div>');
		// 		$("#ajaxFormModal").hide();
		// 		$("#check-register-by-email").validate({
		// 	      	rules: {
		// 	        	"registerEmail": {
		// 	            	"email": true
		// 	         	}
		// 	       	},
		// 	       	submitHandler: function(form){
		// 			    /* CACHER LE BOUTON */
		// 			    $(".btn-validate-email").html("Valider <i class='fa fa-spin fa-spinner'></i>");
		// 			    if(checkUniqueEmail($(".first-register #check-email-register").val())){

		// 			    }else{

		// 			    }
		// 			    jQuery("#btn_submit").hide();
		// 			}

		// 		});
		// 		// $('second-step-register').append("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='#' target='_blank'>CGU</a>.");
		// 		// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 		// $(".compagnonselect").append("<a href='#' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// 	}
		// 	// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 	// $(".compagnonselect").append("<a href='https://francetierslieux.fr/wp-content/uploads/2020/12/Charte_TLCompagnon.pdf' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// }


	}
}

function initParticleForTLDashboard(){
	const launchWhenReady = function(selector, time, execTime) {
        if(document.querySelector(selector)!=null) {
            $(selector).attr('id', 'hdash');
			$.getScript(modules.co2.url+"/js/particles.min.js")
			.done(function( script, textStatus ) {
				particlesJS("hdash",
				{
					"particles": {
						"number": {
							"value": 50,
							"density": {
								"enable": true,
								"value_area": 800
							}
						},
						"color": {
							"value": ["#F0506A","#4E54C9","#DDDDDD"]
						},
						"shape": {
							"type": "circle",
							"polygon": {
								"nb_sides": 5
							},
							"image": {
							"src": "img/github.svg",
							"width": 100,
							"height": 100
							}
						},
						"opacity": {
							"value": 0.5,
							"random": false,
							"anim": {
							"enable": false,
							"speed": 1,
							"opacity_min": 0.1,
							"sync": false
							}
						},
						"size": {
							"value": 10,
							"random": true,
							"anim": {
							"enable": false,
							"speed": 40,
							"size_min": 0.1,
							"sync": false
							}
						},
						"line_linked": {
							"enable": true,
							"distance": 150,
							"color": "#DDDDDD",
							"opacity": 0.4,
							"width": 4
						},
						"move": {
							"enable": true,
							"speed": 6,
							"direction": "none",
							"random": false,
							"straight": false,
							"out_mode": "out",
							"attract": {
							"enable": false,
							"rotateX": 600,
							"rotateY": 1200
							}
						}
						},
						"interactivity": {
						"detect_on": "canvas",
						"events": {
							"onhover": {
							"enable": true,
							"mode": "repulse"
							},
							"onclick": {
							"enable": true,
							"mode": "push"
							},
							"resize": true
						},
						"modes": {
							"grab": {
							"distance": 400,
							"line_linked": {
								"opacity": 1
							}
							},
							"bubble": {
							"distance": 400,
							"size": 40,
							"duration": 2,
							"opacity": 8,
							"speed": 3
							},
							"repulse": {
							"distance": 200
							},
							"push": {
							"particles_nb": 4
							},
							"remove": {
							"particles_nb": 2
							}
						}
						},
						"retina_detect": true,
						"config_demo": {
						"hide_card": false,
						"background_color": "#b61924",
						"background_image": "",
						"background_position": "50% 50%",
						"background_repeat": "no-repeat",
						"background_size": "cover"
						}
					}
				);
				$(".particles-js-canvas-el").css({"position":"fixed","top":0, "bottom":0, "left":0});
			})
			return;
        }else {
			if(execTime==6){
				return;
			}
			execTime+=1;
            setTimeout(function() {
                launchWhenReady(selector, time, execTime);
            }, time);
        }
    }
	if(!costum.editMode){
		launchWhenReady(".header-dashboard", 3000, 0);
	}
}

// switch view graph to table
$(document).off("click", ".switchToChart").on("click", ".switchToChart", function () {
	$(".chartviz").show();
	$(".tableviz").hide();
	$(".showChart").addClass("active");
	$(".showTable").removeClass("active");
})

$(document).off("click", ".switchToTable").on("click", ".switchToTable", function () {
	$(".chartviz").hide();
	$(".tableviz").show();
	$(".showChart").removeClass("active");
	$(".showTable").addClass("active");
})

$(document).off("click", ".exportCSV").on("click", ".exportCSV", function () {
	getDashboardData("/isExport/true/format/csv", "pre-analyse-recensement-2023", function (response) {
		if (typeof json2csv != "undefined" && typeof json2csv.Parser != "undefined") {
			const parser = json2csv.Parser;
			const fields = response.header;
			const exportation_preparation = new parser({ fields, delimiter: ";" });
			const csv = exportation_preparation.parse(response.body);
			let filename = "recensement2023_data";
			if (typeof filename != "undefined") {
				const splited_filename = filename.split('.');
				const file_signature = moment().format('YYYYMMDD-HHmm');
				filename = splited_filename.length > 1 && splited_filename[splited_filename.length - 1].toLowerCase() === 'csv' ? `${splited_filename.slice(-1).join('')}_${file_signature}.csv` : `${filename}_${file_signature}.csv`;

				//var universalBOM = "\uFEFF";
				const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
					type: 'text/csv;charset=utf-8;'
				});
				if (typeof (navigator) != "undefined" && typeof (navigator.msSaveBlob) != "undefined") { // IE 10+
					navigator.msSaveBlob(blob, filename);
				} else {
					const link = document.createElement('a');
					if (link.download !== undefined) { // feature detection
						// Browsers that support HTML5 download attribute
						const url = URL.createObjectURL(blob);
						link.setAttribute('href', url);
						link.setAttribute('download', filename);
						link.style.visibility = 'hidden';
						document.body.appendChild(link);
						link.click();
						document.body.removeChild(link);
					}
				}
			} else {
				toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
			}
		}
	});
})

$(document).off("click",".dashboard-sommaire").on("click",".dashboard-sommaire", function(e){
	e.stopPropagation();
	e.preventDefault();
	urlCtrl.openPreview("/view/url/costum.views.custom.franceTierslieux.graph.navigatorMenu");
});

$(document).off("click", ".exportJSON").on("click", ".exportJSON", function(){
	var region = localStorage.getItem("activeRegion");
	getDashboardData("/isExport/true", location.hash.replace("#", ""), function(response){
		if (typeof json2csv!= "undefined" && typeof json2csv.Parser !="undefined") {
			const json = JSON.stringify(response.body);
			let filename = "recensement2023_data";
			if (typeof filename !="undefined") {
				const splited_filename = filename.split('.');
				const file_signature = moment().format('YYYYMMDD-HHmm');
				filename = splited_filename.length > 1 && splited_filename[splited_filename.length - 1].toLowerCase() === 'json' ? `${splited_filename.slice(-1).join('')}_${file_signature}.json` : `${filename}_${file_signature}.json`;
	
				//var universalBOM = "\uFEFF";
				const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), json], {
					type: 'text/json; charset=utf-8;'
				});
				if (typeof (navigator) !="undefined" && typeof (navigator.msSaveBlob) !="undefined") { // IE 10+
					navigator.msSaveBlob(blob, filename);
				} else {
					const link = document.createElement('a');
					if (link.download !== undefined) { // feature detection
						// Browsers that support HTML5 download attribute
						const url = URL.createObjectURL(blob);
						link.setAttribute('href', url);
						link.setAttribute('download', filename);
						link.style.visibility = 'hidden';
						document.body.appendChild(link);
						link.click();
						document.body.removeChild(link);
					}
				}
			} else {
				toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
			}
		}
	}, region);
})

/*

$(document).on("click", ".inputPathFinder", function(){

	sectionDyf.inputPathFinderParamsData = {coform:"63e0a8abeac0741b506fb4f7"};
	sectionDyf.inputPathFinderParams = {
		"jsonSchema" : {    
			"title" : "Configuration de graph",
			"description" : "Personnaliser votre graphe",
			"icon" : "fa-cog",
			"properties" : {
				"answerPath" : {
					"inputType" : "select",
					"class" : "inputPathFinder",
					"label" : "À Quelle Question corresponds la graph",
					"rules" : {
						"required" : true
					},
					"isSelect2":true,
					"placeholder":"Chercher la question",
					"options": {}
				}
			},
			afterBuild : function(){
				if(sectionDyf.inputPathFinderParamsData.coform!=""){
					updateInputList(sectionDyf.inputPathFinderParamsData.coform, function(){
						if($("#answerPath.inputPathFinder option[value='"+sectionDyf.inputPathFinderParamsData.answerPath+"']").length > 0){
							$("#answerPath.inputPathFinder").val(sectionDyf.inputPathFinderParamsData.answerPath);
							$("#answerPath.inputPathFinder").change();
							$("#answerValue.inputPathFinder").val(sectionDyf.inputPathFinderParamsData.answerValue)
						}
					});
				}
				$(".form-actions").empty();
			},
			save : function (data) {  
				
			}
		}
	}

            }
        }
    }

    let updateInputList = function (value, callback = null) {
        let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined") ? costum["dashboardGlobalConfig"]["coformInputs"] : {};
        $("#answerPath.inputPathFinder").empty();
        for (const stepKey in childForm[value]) {
            for (const inputKey in childForm[value][stepKey]) {
                let input = childForm[value][stepKey][inputKey];
                //let isSelected = ()?"":""

                if (input["type"].includes(".multiCheckboxPlus")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.multiCheckboxPlus' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"].includes(".multiRadio")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.multiRadio' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"].includes(".radiocplx")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.radiocplx' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"].includes(".checkboxcplx")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.checkboxcplx' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"].includes(".checkboxNew")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.checkboxNew' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"].includes(".radioNew")) {
                    $("#answerPath.inputPathFinder").append('<option value="' + stepKey + '.radioNew' + inputKey + '" >' + input["label"] + '</option>');
                }
                if (input["type"]=="text") {
                    $("#name.inputPathFinder").append('<option value="' + stepKey + '.' + inputKey + '" >' + input["label"] + '</option>');
                }
            }
        }

        if (callback!=null && typeof callback=="function") {
            callback();
        }
    }

    tplCtx["id"] = $(this).data("id");
    tplCtx["collection"] = $(this).data("collection");
    tplCtx["path"] = "allToRoot";
    dyFObj.openForm(sectionDyf.inputPathFinderParams, null, sectionDyf.inputPathFinderParamsData);

		if(input.includes("checkboxNew") || input.includes("radioNew")){
			for(const paramValue of coform["params"][input]["list"]){
				$("#answerValue.inputPathFinder").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
			}
		}
	});
});*/