pageProfil.initCallB=function(){
	$.each(costum.lists, function(e, v){
		constructDataForEdit=[];
		$.each(v, function(i, tag){
			if($.inArray(tag, contextData.tags) >=0){
				constructDataForEdit.push(tag);
				contextData.tags.splice(contextData.tags.indexOf(tag),1);
			}
		});
		contextData[e]=constructDataForEdit;
	});
	if(typeof contextData.tags != "undefined" && notNull(contextData.tags))
	contextData.tags.splice(contextData.tags.indexOf("TiersLieux"),1);
};
pageProfil.views.detail = function(){
	var url = "element/about/type/"+contextData.type+"/id/"+contextData.id;
	if(contextData.type=="organizations") url+="/view/costum.views.custom.reseauTierslieux.element.detail";
		ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, null, function(){},"html");
};

pageProfil.views.dashboard= function(){
	alert("dashboard");
	ajaxPost('#central-container', baseUrl+'/costum/co/dashboard/sk/'+contextData.slug, 
					null,
					function(){},"html");
};

pageProfil.views.photos= function(){
	mylog.log("pageProfil.views.photos");
	var url = "gallery/index/type/"+typeItem+"/id/"+contextData.id+"/docType/image";	
	ajaxPost('#central-container', baseUrl+'/'+moduleId+'/'+url, 
		null,
		function(){
			$(".dropdown-add-file").html(
				'<a class="btn show-nav-add" href="javascript:dyFObj.openForm(\'addFile\')"><i class="fa fa-plus-circle"> Ajouter des photos</a>');
		}
	);
};


$('.sub-menu-social').affix({
	offset: {
	  top: Number(pageProfil.affixPageMenu)
	}
}).on('affixed.bs.affix', function(){
	heightPos = $("#mainNav").outerHeight();
	if ($("#menu-top-costumizer").is(":visible")) {
		heightPos=heightPos+$("#menu-top-costumizer").outerHeight();
	}
	if($("#subMenu").is(":visible")) {
		heightPos=heightPos+$("#subMenu").outerHeight();
	}
	$(this).css({"top":heightPos});
	$(".central-section").css({"padding-top":(pageProfil.menuTopHeight)});
	// if($("#menu-left-container").length > 0 && $("#menu-left-container").is(":visible")){
		$("#menu-left-container").css("display","none");
		// $(".central-section").addClass("col-lg-offset-2 col-md-offset-3 col-sm-offset-3");
	// }

}).on('affixed-top.bs.affix', function(){
	$(this).css({"top":"initial"});
	$(".central-section").css({"padding-top":"initial"});
	$("#menu-left-container").css("display","block");
	// if($("#menu-left-container").length > 0 && $("#menu-left-container").is(":visible")){
		// $("#menu-left-container").css({"position": "relative", "left" : "initial", "top": "initial"});
		// $(".central-section").removeClass("col-lg-offset-2 col-md-offset-3 col-sm-offset-3");
	// }
});