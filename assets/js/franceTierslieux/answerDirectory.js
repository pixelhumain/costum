
var filterDropdown = [];
var AllTiersLiked = {};
var countRanking = 1;
var loadAdminContent = {};
costum.store = (costum.store == undefined) ? {} : costum.store;
var Obj = {
    events: function (cobj) {},
    prepareData: function (params) {
        var results = {
            reseau: params.name,
            reseauId: params['_id']['$id'],
            created: ucfirst(moment.unix(params.created).format('dddd Do MMMM YYYY h:mm:ss')),
            creator: params?.nameAndUser?.name ?? "",
        }
        return results;
    }
}
var flipUtils = {
    init: function () {
        const progressButtons = document.querySelectorAll(".progress-button-flip");
        progressButtons.forEach(button => {
            const percentage = button.getAttribute("data-percentage");
            button.style.setProperty("--percentage", percentage / 100);
            const progressCircle = button.querySelector(".progress-svg-flip");
            const percentageLabel = button.querySelector(".percentage-label-flip");
            percentageLabel.textContent = `${percentage}%`;
            if (percentage === "100") {
            button.classList.remove("non-complete");
            button.classList.add("complete");
            }
        });
    },
    clickBtnForm: function() {
        $(".progress-button-flip").click(function() {
            var targetClass = $(this).data("target");
            var formId = $(this).data("form");
            $("[rel='tooltip']" + targetClass.replace("prev-",".rotatingCard-")).click();
            $("." + targetClass).click()
            const checkFromsBtn = () => {
                if ($(".btn-list-forms").length > 0) {
                    clearInterval(checkInterval);
                    $("#section-allForm").click();
                    setTimeout(() => { $("[data-elformid='"+ formId +"']").click();}, 150);
                }
            }
            const checkInterval = setInterval(checkFromsBtn, 50);
        })
    }
};
directory.adminFlip = function(orgaId="", dom="") {
    var adminFlip = {};
    adminFlip.getAdminValues = function () {
        var data = showAllNetwork.getAllAnswers("." + orgaId).results;
        var formsPercent = {};
        formsPercent.Fréquentation = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.frequentation.id, getNavigatorElement.suplment.frequentation.path, orgaId.replace(".",""), data).toFixed(0), id: getNavigatorElement.suplment.frequentation.id};
        formsPercent.Equipement = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.economicModel.id, getNavigatorElement.suplment.economicModel.path, orgaId.replace(".",""), data).toFixed(0), id: getNavigatorElement.suplment.economicModel.id};
        formsPercent.Activité = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.avtivity.id, getNavigatorElement.suplment.avtivity.path, orgaId.replace(".",""), data).toFixed(0), id: getNavigatorElement.suplment.avtivity.id};
        formsPercent.Satisfaction = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.satisfaction.id,getNavigatorElement.suplment.satisfaction.path, orgaId.replace(".",""), data).toFixed(0), id: getNavigatorElement.suplment.satisfaction.id};
        formsPercent.Outils = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.tool.id, getNavigatorElement.suplment.tool.path, orgaId.replace(".",""), data).toFixed(0), id: getNavigatorElement.suplment.tool.id};
        // formsPercent.Recensement = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.surveyData.formId, getNavigatorElement.surveyData.path, orgaId.replace(".",""),  data).toFixed(0), id: getNavigatorElement.surveyData.formId};
        formsPercent.Réseaux = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.networkSupl.id, getNavigatorElement.suplment.networkSupl.path, orgaId.replace(".",""),  data).toFixed(0), id: getNavigatorElement.suplment.networkSupl.id};
        formsPercent.Mobilize = {percent: showAllNetwork.getBtnPercentage(getNavigatorElement.suplment.mobilize.id, getNavigatorElement.suplment.mobilize.path, orgaId.replace(".",""),  data).toFixed(0), id: getNavigatorElement.suplment.mobilize.id};
        loadAdminContent[orgaId] = formsPercent;
    };
    adminFlip.loadFormsPercentView = function () { 
        coInterface.showLoader(dom);
        const checkClickedFromsBtn = () => {
            if ($(".card-container.manual-flip.hover").length > 0) {
                clearInterval(checkAnsInterval);
                directory.adminFlip(orgaId).getAdminValues();
                if (loadAdminContent[orgaId] != undefined) {
                    var adminFlipHtml = "";
                    var formsPercent = loadAdminContent[orgaId];
                    adminFlipHtml += `<div class="buttons-flip-content">`;
                    $.each(formsPercent, function(k,v){
                     adminFlipHtml += `<div class="progress-flip-content" "> 
                                             <div data-target="prev-${orgaId}" data-form="${v.id}" class="progress-button-flip non-complete" data-percentage="${v.percent}">
                                                 <svg class="progress-svg-flip" viewBox="-2.5 0 105 100">
                                                     <circle class="progress-circle-flip" cx="50" cy="50" r="48"></circle>
                                                     <text class="percentage-label-flip" x="50" y="50" dy=".3em"></text>
                                                 </svg>
                                             </div>
                                             <div class="text-center">${k}</div>
                                         </div>`;
                    })
                    adminFlipHtml +=`</div>`;
                    $(dom).html(adminFlipHtml);
                    flipUtils.init();
                    flipUtils.clickBtnForm();
                }
            }
        }
        const checkAnsInterval = setInterval(checkClickedFromsBtn, 10);
    };
    return adminFlip;
};
directory.modalPanel = function (params) {
    var isAdmin = (
        jsonHelper.getValueByPath(params, "links.members."+ userId +".isAdmin") && 
       (!jsonHelper.getValueByPath(params, "links.members."+ userId +".isAdminPending") ||  jsonHelper.getValueByPath(params, "links.members."+ userId +".isAdminPending") == false) && 
        (!jsonHelper.getValueByPath(params, "links.members."+ userId +".toBeValidated")) || jsonHelper.getValueByPath(params, "links.members."+ userId +".toBeValidated") == false) ? true : false;
    var slug = params.slug;
    var networkSkug = params.networkSlug;
    var nameAndImageUrl = params.nameAndUser;
    var network = params.networkName;
    var networkey = (params?.address?.level3 != undefined) ? params.address.level3  : "";
    var getAnswers = typeof params.answers != "undefined" ? params.answers : {};
    var answerId = typeof params.answers != "undefined" ? params.answers['_id']['$id'] : null;
    var organisationId = params["_id"]["$id"];
    var likeNumber = params?.voteCount?.like;
    var mobilized = typeof params.mobilized != "undefined" ? true : false;
    var fillHeart = (params?.vote != undefined && Object.keys(params.vote).length > 0 && typeof userId != "undefined") ? (Object.keys(params.vote).includes(userId)) ? true : false : false;
    likeNumber = (likeNumber == undefined) ? 0 : likeNumber;
    var imagePath = (params?.profilMediumImageUrl != undefined) ? params.profilMediumImageUrl : defaultImage;
    params = Obj.prepareData(params);
    var dateHtml = (params.created != "Invalid date") ? `class="text-left desc date-nd-person">${params.created}<br>` : "";
    var personWhoAnswered = (typeof nameAndImageUrl != "undefined" && typeof nameAndImageUrl.name != "undefined") ? "Par " + nameAndImageUrl.name : "";
    var networkImage = (jsonHelper.pathExists('whereLists.'+ network +'.imageUrl') && typeof whereLists[network].imageUrl == "string" && notEmpty(whereLists[network].imageUrl)) ? whereLists[network].imageUrl : defaultImage;
    var str = `
        <div class="team">
            <div class="card-team">
                <div class="img-box">
                    <img class="img-responsive-action" alt="Responsive Team Profiles" src="${imagePath}" />
                </div>
                <p class="third-place-title">
                    <a href="javascript:" class=" ${(params.reseau != null) ? params.reseau.replace(" ", "-") : ""}" onclick='showAllNetwork.loadPreview("${answerId}","${imagePath}","${params?.reseau?.replace("'", "#")}","${organisationId}","tiersLieux", "${network}" , ${JSON.stringify(getAnswers).replace(/'/g, "#")}, "${networkey}", function(){},"${slug}")'>
                        ${params.reseau ?? ""}
                    </a>
                <p>
                <p class="network-title">
                    <a href="javascript:" onclick="showAllNetwork.loadPreview('','${networkImage}','${network}','' ,'reseaux', '', '', '${networkey}', function(){}, '${networkSkug}')" style="text-decoration: none;" target="_blank">
                        ${network}
                    </a>
                </p>        
                <p   
                    ${dateHtml}
                    <span>${personWhoAnswered}</span>
                </p>
                ${mobilized ? `<div class="margin-bottom-10">
                    <img src="${assetPath}/images/franceTierslieux/answersImages/mobilized.png" title="Tiers Lieux Mobilisés 2024" style="width: 30px;"> <a href="javascript:void(0)" onclick="showAllNetwork.openModal('view/url/costum.views.custom.franceTierslieux.modalPreview', {})">Tiers Lieux Mobilisés</a> 
                </div>` : ""}
            <div class="btn-group btn-group-xs pull-right like-project-container">
                    <a style="cursor: pointer;color:red;" class="like-orga-${organisationId}" onclick="showAllNetwork.likeThis('${organisationId}','.like-orga-${organisationId}')"> ${(likeNumber == 0) ? '<i style="font-size: 33px;" class="fa fa-heart-o"></i>' : (fillHeart) ? '<i style="font-size: 33px;" class="fa fa-heart"></i> ' + likeNumber : '<i style="font-size: 33px;" class="fa fa-heart-o"></i> ' + likeNumber} </a>
                </div>
                ${(isAdmin) ? '<p style="font-size: 14px;" ><b> <i class="fa fa-tags gradient-fill"></i> Vous êtes Administrateur </b></p>' : ''} 
                <a class=""></a>

            </div>
        </div>
    `;

    if (isAdmin) {
        var elementType = "";
		if (params.collection == "events" || params.collection == "organizations" || params.collection == "citoyens" || params.collection == "projects")
			bannerElementType = params.collection+'.png'
		else
			bannerElementType = 'connexion-lines.jpg'

		var urlBanner = (typeof params.profilRealBannerUrl != 'undefined') ? params.profilRealBannerUrl : themeUrl+'/assets/img/background-onepage/' + bannerElementType;
		mylog.log("urlBanner",urlBanner);
		str = `
		<div  class="team rotatingCard cart-with-history">
             <div class="team card-container manual-flip" style="padding: 0px; box-shadow: none !important; margin: 0px;">
                <div class="card-team card">
                    <div class="front" style="box-shadow: none !important;">
                        <div class="content">
                            <div class="header" style="box-shadow: none !important;">
                                <button class="btn btn-simple btn-rotatingCard rotatingCard-${organisationId}" onclick="directory.adminFlip('${organisationId}', '.form-content-${organisationId}').loadFormsPercentView();">
                                    <i class="fa fa-mail-forward"></i> 
                                </button>
                            </div>
                            <div class="img-box">
                                <img class="img-responsive-action" alt="Responsive Team Profiles" src="${imagePath}" />
                            </div>
                            <p class="third-place-title">
                                <a href="javascript:" class="prev-${organisationId} ${(params.reseau != null) ? params.reseau.replace(" ", "-") : ""}" onclick='showAllNetwork.loadPreview("${answerId}","${imagePath}","${params?.reseau?.replace("'", "#")}","${organisationId}","tiersLieux", "${network}" , ${JSON.stringify(getAnswers).replace(/'/g, "#")}, "${networkey}", function(){},"${slug}")'>
                                    ${params.reseau ?? ""}
                                </a>
                            <p>
                            <p class="network-title">
                                <a href="javascript:" onclick="showAllNetwork.loadPreview('','${networkImage}','${network}','' ,'reseaux', '', '', '${networkey}', function(){}, '${networkSkug}')" style="text-decoration: none;" target="_blank">
                                    ${network}
                                </a>
                            </p>        
                            <p   
                                ${dateHtml}
                                <span>${personWhoAnswered}</span>
                            </p>
                            ${mobilized ? `<div class="margin-bottom-10">
                                <img src="${assetPath}/images/franceTierslieux/answersImages/mobilized.png" title="Tiers Lieux Mobilisés 2024" style="width: 30px;"> <a href="javascript:void(0)" onclick="showAllNetwork.openModal('view/url/costum.views.custom.franceTierslieux.modalPreview', {})">Tiers Lieux Mobilisés</a>
                            </div>` : ""}
                            <div class="btn-group btn-group-xs pull-right like-project-container">
                                <a style="cursor: pointer;color:red;" class="like-orga-${organisationId}" onclick="showAllNetwork.likeThis('${organisationId}','.like-orga-${organisationId}')"> ${(likeNumber == 0) ? '<i style="font-size: 33px;" class="fa fa-heart-o"></i>' : (fillHeart) ? '<i style="font-size: 33px;" class="fa fa-heart"></i> ' + likeNumber : '<i style="font-size: 33px;" class="fa fa-heart-o"></i> ' + likeNumber} </a>
                            </div>
                            ${(isAdmin) ? '<p style="font-size: 14px;" ><b> <i class="fa fa-tags gradient-fill"></i> Vous êtes Administrateur </b></p>' : ''} 
                            <a class=""></a>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back" style="box-shadow: none !important;">
                        <div class="header">
                            <a href="javascript:;" class="btn btn-simple btn-rotatingCard rotatingCard-${organisationId}" rel="tooltip" title="Flip Card">
                                <i class="fa fa-reply"></i> 
                            </a>
                        </div>
                        <div class="form-content-${organisationId}" style="margin-top: 10%;"></div>

                    </div> 
                </div> 
            </div> 
        </div> `;
    }
    return str;
};
directory.listProfile = function (params) {
    let str = "";
    var network = params.networkName;
    var getAnswers = typeof params.answers != "undefined" ? params.answers : {};
    var networkey = (params?.address?.level3 != undefined) ? params.address.level3  : "";
    var imagePath = (params?.profilMediumImageUrl != undefined) ? params.profilMediumImageUrl : defaultImage;
    var answerId = typeof params.answers != "undefined" ? params.answers['_id']['$id'] : null;
    var organisationId = params["_id"]["$id"];
    if (typeof params.profilMediumImageUrl !== "undefined" && params.profilMediumImageUrl !== "") {
        imageUrl = baseUrl + params.profilMediumImageUrl;
    }
    var networkImage = (jsonHelper.pathExists('whereLists.'+ network +'.imageUrl') && typeof whereLists[network].imageUrl == "string" && notEmpty(whereLists[network].imageUrl)) ? whereLists[network].imageUrl : defaultImage;
    str += `
            <li class="listElement">	
            <div class="listEltDiv">
                <img src="${imagePath}" alt="" width="70" height="70" style="border-radius: 100%;object-fit:cover">
                        <div class="informationContent">
                            <div class="desc-content">
                                <p class="third-place-title">
                                    <a class="buttonShowAllTitle" href="javascript:" onclick='showAllNetwork.loadPreview("${answerId}","${imagePath}","${params.name.replace("'", "#")}","${organisationId}","tiersLieux", "${network}" , ${JSON.stringify(getAnswers).replace(/'/g, "#")}, "${networkey}")' >
                                        ${params.name}
                                    </a>
                                <p>
                                <p class="network-title">
                                    <a class="network-title" onclick="showAllNetwork.loadPreview('','${networkImage}','${network}','' ,'reseaux', '', '', '${networkey}')" href="javascript:">
                                        ${network}
                                    </a>
                                </p>
                            </div>
                            <span>
                            <span class="infoDate">
                                <p> ${countRanking} </p>
                            </span>
                        
                    </span>
                </div>
            </div>
        </li>
    `;
    countRanking ++;
    return str;
};
directory.rotatingCard = function (params) {
    mylog.log("paramsparams",params.organizations);
    var orgaObject = {};
    if (jsonHelper.getValueByPath(params, "organizations") != undefined && typeof params.organizations == "object") {
        if (Object.keys(params.organizations).length == 1) {
            orgaObject["key"] = Object.keys(params.organizations)[0];
            orgaObject["name"] = params.organizations[orgaObject.key].name;
        } else if (Object.keys(params.organizations).length) {
            var group = []
            $.each(params.organizations, function (k, v) {
                if (v?.name != undefined) {
                    group.push(v.name);
                    orgaObject["name"] = group.join(', ');
                }
            })
        }
    }
    var elementType = "";
    if (params.collection == "events" || params.collection == "organizations" || params.collection == "citoyens" || params.collection == "projects")
        bannerElementType = params.collection+'.png'
    else
        bannerElementType = 'connexion-lines.jpg'

    var urlBanner = (typeof params.profilRealBannerUrl != 'undefined') ? params.profilRealBannerUrl : themeUrl+'/assets/img/background-onepage/' + bannerElementType;
    mylog.log("urlBanner",urlBanner);
    var str = `
    <div id="entity_` + params.collection + `_` + params.id + `" class="col-xs-12 col-md-4 col-sm-6 rotatingCard searchEntityContainer ${params.containerClass}">
            <div class="card-container manual-flip">
            <div class="card">
                <div class="front">
                    <div class="cover">
                        <img src="${urlBanner}"/>
                    </div>
                    <div class="user">
                        ${params.imageProfilHtml}
                    </div>
                    <div class="content">
                        <div class="main co-scroll">
                            <a href="${params.hash}" class="${params.hashClass}">
                                <h3 class="name">${params.name}</h3>
                            </a>`;
                            if (orgaObject.name != undefined) { 
    str +=                 `<p class="text-center"><a style="color: #8DBC21;font-size: 21px;">(${orgaObject.name})</a></p>`;    
                            }
    str +=                  `<p class="profession">${params.collection}</p>`;
                            if (notNull(params.localityHtml)) {
    str += 					`<p class="text-center">${params.localityHtml}</p>`;
                            }
    str += `            </div>
                        <div class="footer">
                            <button class="btn btn-simple btn-rotatingCard" >
                                <i class="fa fa-mail-forward"></i> ${trad.information}
                            </button>
                        </div>
                    </div>
                </div> <!-- end front panel -->
                <div class="back">
                    <div class="header">
                        <h5 class="motto">${params.name}</h5>
                    </div>
                    <div class="content ">
                        <div class="main co-scroll">`
                            if (typeof params.type != 'undefined') {
    str += 						'<div class="entityType text-center">' +
                                    '<span class="text-dark">' + ((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type) + '</span>'+
                                '</div>';
                            };
                            if (notEmpty(params.statusLinkHtml))
                                str += params.statusLinkHtml;
                            if (notEmpty(params.rolesHtml))
                                str += '<div class=\'rolesContainer\'>' + params.rolesHtml + '</div>';
                            if (typeof params.edit != 'undefined' && notNull(params.edit))
                                str += directory.getAdminToolBar(params);
    str +=                  `<p class="text-center">${params.descriptionStr}</p>`;
                            if (typeof params.tagsHtml != "undefined")
                                str += '<ul class="tag-list text-center">' + params.tagsHtml + '</ul>';

        str +=                 `<div class="stats-container">`;
                                str += directory.countLinksHtml(params);
                                if (typeof params.id != 'undefined' && typeof params.collection != 'undefined')
                                    str += directory.socialToolsHtml(params);
    str +=                   `</div>

                        </div>
                    </div>
                    <div class="footer">
                        <a href="javascript:;" class="btn btn-simple btn-rotatingCard" rel="tooltip" title="Flip Card">
                            <i class="fa fa-reply"></i> ${trad.back}
                        </a>
                        <a href="${params.hash}" class="btn btn-simple btn-rotatingCard ${params.hashClass}" rel="tooltip" title="Flip Card"> 
                            ${tradBadge.viewDetails} <i class="fa  fa-mail-forward"></i>
                        </a>
                    </div>
                </div> 
            </div> 
        </div> 
    </div> `;
    return str;
};
directory.eventPanelHtmlFullWidth = function (params) {
    mylog.log("eventPanelHtml", params.organizations);
    var orgaObject = {};
    if (jsonHelper.getValueByPath(params, "organizations") != undefined && typeof params.organizations == "object") {
        if (Object.keys(params.organizations).length == 1) {
            orgaObject["key"] = Object.keys(params.organizations)[0];
            orgaObject["name"] = params.organizations[orgaObject.key].name;
        } else if (Object.keys(params.organizations).length) {
            var group = []
            $.each(params.organizations, function (k, v) {
                if (v?.name != undefined) {
                    group.push(v.name);
                    orgaObject["name"] = group.join(', ');
                }
            })
        }
    }
    params.id = params._id.$id;
    params.hash = "#page.type." + params.collection + ".id." + params.id;

    if (directory.dirLog) mylog.log('-----------eventPanelHtml', params);
    var str = '';
    str += '<div class=\'col-xs-12 searchEntityContainer events contain_' + params.collection + '_' + params.id + ' \'>';
    str += '<div class=\'searchEntity\' id=\'entity' + params.id + '\'>';
    var dateFormated = directory.getDateFormated(params).replace("letter-orange", "");

    params.attendees = '';
    var cntP = 0;
    var cntIv = 0;

    if (typeof params.links != 'undefined')
        if (typeof params.links.attendees != 'undefined') {
            $.each(params.links.attendees, function (key, val) {
                if (typeof val.isInviting != 'undefined' && val.isInviting == true)
                    cntIv++;
                else
                    cntP++;
            });
        }

    // params.attendees = '<hr class="margin-top-10 margin-bottom-10">';

    var isFollowed = false;
    if (typeof params.isFollowed != 'undefined') isFollowed = true;

    if (userId != null && userId != '' && params.id != userId) {

        var isShared = false;

        params.attendees += '<button id=\'btn-share-event\' class=\'btn btn-share-event btn-xs btn-share\'' +
            ' data-ownerlink=\'share\' data-id=\'' + params.id + '\' data-type=\'' + params.collection + '\' ' +//data-name='"+params.name+"'"+
            ' data-isShared=\'' + isShared + '\'>' +
            '<i class=\'fa fa-share\'></i> ' + trad['share'] + '</button>';
    }
    if (userId != null && userId != '' && params.id != userId) {
        var tip = trad['interested'];
        var actionConnect = 'follow';
        var icon = 'chain';
        var textBtn = '<i class=\'fa fa-chain\'></i> ' + trad['participate'];
        var classBtn = 'text-white';
        if (isFollowed) {
            actionConnect = 'unfollow';
            icon = 'unlink';
            classBtn = 'text-white';
            textBtn = '<i class=\'fa fa-unlink\'></i> ' + trad['alreadyAttendee'];
        }
        params.attendees += '<a href="javascript:;" class="btn btn-xs btn-event-participate tooltips followBtn ' + classBtn + '"' +
            'data-toggle="tooltip" data-placement="left" data-original-title="' + tip + '"' +
            ' data-ownerlink=\'' + actionConnect + '\' data-id=\'' + params.id + '\' data-type=\'' + params.collection + '\' data-name=\'' + params.name + '\'' +
            ' data-isFollowed=\'' + isFollowed + '\'>' +
            textBtn +
            '</a>';
    }

    params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
        'data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad['guest-s'] + '">' +
        cntIv + ' <i class="fa fa-envelope"></i>' +
        '</small>';

    params.attendees += '<small class="light margin-left-10 tooltips pull-left"  ' +
        'data-toggle="tooltip" data-placement="bottom" data-original-title="' + trad['attendee-s'] + '">' +
        cntP + ' <i class="fa fa-street-view"></i>' +
        '</small>';

    var countSubEvents = (params.links && params.links.subEvents) ? '<br/><i class=\'fa fa-calendar\'></i> ' + Object.keys(params.links.subEvents).length + ' ' + trad['subevent-s'] : '';
    var defaultImgDirectory = '<div class="div-img"><i class="fa fa-calendar-o fa-5x"></i></div>';
    if ('undefined' != typeof directory.costum && notNull(directory.costum)
        && typeof directory.costum.results != 'undefined'
        && typeof directory.costum.results[params.collection] != 'undefined'
        && typeof directory.costum.results[params.collection].defaultImg != 'undefined')
        //defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+assetPath+directory.costum.results[params.type].defaultImg+'\'/>';
        defaultImgDirectory = '<img class=\'img-responsive\' src=\'' + assetPath + directory.costum.results[params.collection].defaultImg + '\'/>';
    if ('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
        //defaultImgDirectory= '<img class=\'img-responsive\' onload=\'directory.checkImage(this);\' src=\''+baseUrl+params.profilMediumImageUrl+'\'/>';
        defaultImgDirectory = '<img class=\'img-responsive\' src=\'' + baseUrl + params.profilMediumImageUrl + '\'/>';

    str += '<div class="col-xs-12 col-sm-4 col-md-4 no-padding">';
    str += '<a href="' + params.hash + '" class="container-img-profil lbh add2fav block">' + defaultImgDirectory + '</a>';
    str += '</div>';
    str += '<div class="col-md-8 col-sm-8 col-xs-12 no-padding bg-img-event-header">';
    // '<div class="col-md-8 col-sm-8 col-xs-12 no-padding bg-img-event-header" style="background:url(\''+baseUrl+params.profilMediumImageUrl+'\')">';
    str += '<div class="col-xs-12 no-padding header-opacity">';
    str += '<div class="col-md-6 col-sm-6 col-xs-12 entityName margin-top-20">';
    str += '<h4><a href="' + params.hash + '" class="entityName lbh add2fav">' +
        params.name +
        '</a></h4>';
    if (orgaObject.name != undefined) 
        str += `<p><a style="color: #8DBC21;font-size: 21px;">${orgaObject.name}</a></p>`;    
    
    str += '</div>';
    str += '<div class="col-md-6 col-sm-6 col-xs-12 margin-top-20">';
    str += '<i class="fa fa-calendar fa-2x date-icon"></i> ' + dateFormated + countSubEvents;
    str += '</div>';

    str += '</div>';
    str += '</div>';


    str += '<div class="col-md-8 col-sm-8 col-xs-12 entityRight">';

    if (typeof params.type != "undefined") {
        var typeEvent = (typeof tradCategory[params.type] != 'undefined') ? tradCategory[params.type] : params.type;
        str += '<div class="col-md-4 col-sm-5 col-xs-12">';
        str += '<h5 class="text-dark lbh add2fav"><i class="fa fa-angle-right "></i>&nbsp; ' +
            typeEvent;
        str += '</h5>';
        str += '</div>';
    }
    str += params.localityHtml != '' ? '<div class="col-md-8 col-sm-7 col-xs-12"><span class="h4 event-locality margin-left-5">' + params.localityHtml + '</span></div>' : '';
    if ('undefined' != typeof params.organizer && params.organizer != null) {
        var countOrganizer = Object.keys(params.organizer).length;
        str += '<div class="col-xs-12 orgenizedby no-padding">';
        str += '<span class="bold">' + tradDynForm.organizedby + ' : </span>';
        $.each(params.organizer, function (e, v) {
            var imgIcon = (typeof v.profilThumbImageUrl != 'undefined' && v.profilThumbImageUrl != '') ? baseUrl + '/' + v.profilThumbImageUrl : assetPath + '/images/thumb/default_' + v.type + '.png';
            str += '<a href="#page.type.' + v.type + '.id.' + e + '" class="lbh-preview-element tooltips" ';
            if (countOrganizer > 1) str += 'data-toggle="tooltip" data-placement="top" title="' + v.name + '"';
            str += '>';
            if (countOrganizer == 1) str += '<img src="' + imgIcon + '" class="img-circle img-organizer margin-left-5" width="35" height="35"/> ' + v.name;
            str += '</a>';
        });
        str += '</div>';
    }


    if (notNull(params.descriptionStr))
        str += '<p class="text-dark event-short-description col-xs-12 margin-top-10 margin-bottom-10">' + params.descriptionStr + '</p>';
    if (typeof params.tagsHtml != "undefined")
        str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';

    str += '<div class="margin-bottom-10 col-md-12 no-padding event-bottom">' +
        params.attendees +

        '</div>';
    str += '</div>';

    str += '</div>';
    str += '</div>';

    str += '</div>';
    return str;
};

var showAllNetwork = {
    organizationsData : {},
    isAuthorized: 0,
    answerNameToComplete: "",
    getAllAnswers : function (orgaId = "", verifyPercent=false) {
        var orga = orgaId.replace(".","");
        var results = {};
        if (costum.store.verifyPerc != undefined && typeof costum.store.verifyPerc == "object"  && typeof costum.store.verifyPerc[orga] != "undefined" && Object.keys(costum.store.verifyPerc).length > 0) {
            results = costum.store.verifyPerc[orga];
        } else {
            let formIdNdFinder = {
                suplFormId : {
                    [getNavigatorElement.suplment.frequentation.id] :getNavigatorElement.suplment.frequentation.path + orgaId,
                    [getNavigatorElement.suplment.economicModel.id] :getNavigatorElement.suplment.economicModel.path + orgaId,
                    [getNavigatorElement.suplment.avtivity.id] :getNavigatorElement.suplment.avtivity.path + orgaId,
                    [getNavigatorElement.suplment.satisfaction.id] : [getNavigatorElement.suplment.satisfaction.path + orgaId, userId],
                    [getNavigatorElement.suplment.tool.id]: getNavigatorElement.suplment.tool.path + orgaId,
                    [getNavigatorElement.surveyData.formId]: getNavigatorElement.surveyData.path + orgaId,
                    [getNavigatorElement.suplment.networkSupl.id]: getNavigatorElement.suplment.networkSupl.path + orgaId,
                    [getNavigatorElement.suplment.mobilize.id]: getNavigatorElement.suplment.mobilize.path + orgaId,
                },
                user : userId,
                show : ['answers', 'links.answered', 'user'],
                orgaId : orgaId.replace(".",""),
                showInOrga : ["address", "url", "shortDescription", "socialNetwork", "slug", "profilImageUrl"]
            }
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/getallsuplanswers',
                formIdNdFinder,
                function (data) {   
                    results = data;
                    if (verifyPercent) {
                        costum.store.verifyPerc = typeof costum.store.verifyPerc != "undefined" ? costum.store.verifyPerc : {};
                        costum.store.verifyPerc[orga] = results;
                    }
                },
                null,
                "json",
                { async: false }     
            );
        }
        return results;
    },
    getBtnPercentage : function (suplForm="", btnPath="", organizationId="", data={}) {
        var precent = 0;
        var getSuplForm = (data[suplForm] != undefined) ? data[suplForm] : {};
        var path = btnPath + "." + organizationId;
        var resultAnswerId = showAllNetwork.getExistingAnsId(suplForm, path, organizationId); 	
        var initSuplFormData = getSuplForm;	
        getSuplForm = getSuplForm.allAnswers;
        precent = showAllNetwork.setButtonColorOrPerc(jsonHelper.getValueByPath(getSuplForm, resultAnswerId + ".answers"), initSuplFormData.inputs, initSuplFormData.form.subForms, true);
        return precent;
    },
    getExistingAnsId : function (formId="", answerPath="", organizationId="") {
        var dataId = "";
        var verifyElement = showAllNetwork.getAnswers("/form/" + formId, {where : {form: formId, [answerPath]: {'$exists': true}}});
        if (verifyElement.allAnswers != undefined && !Array.isArray(verifyElement.allAnswers)) {
            showAllNetwork.getAllAnswers("." + organizationId, true).results[formId].allAnswers = verifyElement.allAnswers;
            dataId = Object.keys(verifyElement.allAnswers)[0];
        }
        return dataId;
    },
    listOrgaInCard: function (params) {
        var str = ``;
        if ( params?.links?.organizations != undefined && typeof Object.keys(params['links']['organizations'])[0] != undefined) {
            let answerId = params._id.$id;
            let orgaId = Object.keys(params['links']['organizations'])[0];
            let orgaName = params['links']['organizations'][orgaId]['name'];
            let orgaRegion = params['answers']['franceTierslieux2022023_753_10']['franceTierslieux2022023_753_10lemfa0njory1oxpszb'];
            let orgaImage = (params['links']['organizations'][orgaId]['image'] != undefined) ? params['links']['organizations'][orgaId]['image'] : defaultImage;
            let user = params['links']['organizations'][orgaId]['user'];
            let date = ucfirst(moment.unix(params.created).format('dddd Do MMMM YYYY h:mm:ss'))
            var str = `
                <div class="team">
                    <div class="card-team">
                        <div class="img-box">
                            <img class="img-responsive-action" alt="Responsive Team Profiles" src="${orgaImage}" />
                        </div>
                        <p class="third-place-title">
                            <a href="javascript:" onclick='showAllNetwork.loadPreview("${answerId}","${orgaImage}","${orgaName.replace("'", "#")}","${orgaId}","tiersLieux", "${orgaRegion}")'>
                                ${orgaName}
                            </a>
                        </p>
                        <p class="network-title">
                            <a href="javascript:" onclick="showAllNetwork.loadPreview('','${orgaImage}','${orgaRegion}','' ,'reseaux')" style="text-decoration: none;" target="_blank">
                                ${orgaRegion}
                            </a>
                        </p>
                
                        <p class="text-left desc date-nd-person">${date}<br>
                        </p>
                    </div>
                </div>
            `;
        } 
        return str;
    },
    setButtonColorOrPerc : function (answers={}, subFormsArrays=[], subFrmArray=[],percent=false) {
        let countFormParams = 0;
        let countAnswers = 0;
        if (answers != undefined) {
            if (Object.keys(answers).length > 0) {
                $.each(answers, function(k, v) {
                    if (subFrmArray.includes(k)) {
                        countAnswers = countAnswers + Object.keys(v).length;
                    } else {
                        if (k != "") {
                            countAnswers = countAnswers + 1;
                        }
                    }
                })
            }
        }
        if (subFormsArrays.length > 0) {
            $.each(subFormsArrays, function(k, v) {
                let eachInputLngth = Object.keys(v).length;
                countFormParams = countFormParams + eachInputLngth;
            })
        }
        return  (percent) ? ((((countAnswers / countFormParams) * 100) > 100) ? 100 : ((countAnswers / countFormParams) * 100)) :((countAnswers == countFormParams) ? "green" : "blue");
    },
    likeThis: function (orgaId = "", selector = "") {
        if (orgaId != null && orgaId != undefined) {
            var params = {
                "action": "vote",
                "collection": "organizations",
                "id": orgaId,
                "details": {
                    status: "like"
                }
            };
            try {
                $.ajax({
                    url: baseUrl + '/' + moduleId + "/action/addaction/",
                    data: params,
                    type: 'post',
                    global: false,
                    dataType: 'json',
                    success:
                        function (data) {
                            if (data.result && selector != "") {
                                showAllNetwork.uptateVoteView(data?.element, selector);
                            }
                        },
                    error:
                        function (data) {
                            showAllNetwork.uptateVoteView(showAllNetwork.getElement("organizations", orgaId), selector);
                        }
                });
            } catch (e) {
                showAllNetwork.uptateVoteView(showAllNetwork.getElement("organizations", orgaId), selector);
            }
        }
    },
    uptateVoteView : function (data={}, selector) {
        if (Object.keys(data).length > 0) {
            var dataVote = data?.vote
            if (dataVote != undefined && typeof dataVote == 'object' && Object.keys(dataVote).length > 0) {
                var heatIcon = (typeof userId != "undefined" && Object.keys(dataVote).includes(userId)) ? '<i style="font-size: 33px;" class="fa fa-heart"></i>' : '<i style="font-size: 33px;" class="fa fa-heart-o"></i>';
                $(selector).html(heatIcon + ' ' + Object.keys(dataVote).length);
            } else {
                $(selector).html('<i style="font-size: 33px;" class="fa fa-heart-o"></i>');
            }
        }
    },
    initView: function (title = "", footer = "", col = "", isTiers = "false", toolCatalog=false) {
        var headTitle = (!toolCatalog) ? `
            <p class="page-title"> ${(title != "") ? title : (isTiers == "true") ? 'Les tiers-lieux' : 'Réseaux régionaux'} </p>
            ${(isTiers == "true") ? "" : '<button class="btn-filters-region btn btn-primary" style="margin-left: auto;display: block;" data-action="shape">Mode Carte</button>'}
        ` : `
            <p class="page-title text-left"> Liste d'un ou plusieurs Tiers-Lieux relié à ${(title == "") ? "l'outil": "\""+title+"\""}  </p>
        `;
        var initView = `
            <div class="row">
                ${headTitle}
            
            </div>
            <div class="${(col == "") ? "col-md-12" : col} no-padding">
                <div id='filterContainercacs' class='searchObjCSS'></div>
                <div class='headerSearchIncommunity no-padding col-xs-12'>
                    <div id="activeFilters" class="col-xs-12 no-padding"></div>
                </div>
                <div class='bodySearchContainer margin-top-30'>
                    <div class='no-padding col-xs-12' id='dropdown_search'>
                    </div>
                </div>
                <div class='padding-top-20 col-xs-12 text-left ${(footer != "") ? footer : "footerSearchContainer"}'></div>
            </div>
        `;
        return initView;
    },
    initEvents: function(){
        $(".btn-filters-region").off().on('click', function(){
            if($(this).data("action") == "liste"){
                localStorage.setItem("networkstate","[data-action='liste']");
                $(".ans-dir").show();
                $(".network-map-page").hide();
                $(".shapeContainer").hide();
                $(this).data("")
            }else{
                localStorage.setItem("networkstate","[data-action='shape']");
                $(".ans-dir").hide();
                $(".network-map-page").show();
                $(".shapeContainer").show();
                $("#regionshapeContainer").attr("style","display: flex;justify-content: space-around;");
            }
        });
    },
    paramsFilters: {
        container: "#filterContainercacs",
        interface: {
            events: {
                // page : true,
                scroll: true,
                //scrollOne : true
            }
        },
        header: {
            dom: ".headerSearchIncommunity",
            options: {
                left: {
                    classes: 'col-xs-8 elipsis no-padding',
                    group: {
                        count: true,
                        types: true
                    }
                }
            },
        },
        defaults: {
            notSourceKey: true,
            textPath: "answers.aapStep1.titre",
            types: ["answers"],
            indexStep: 25,
            forced: {
                filters: {
                }
            },
            fields: [],
            sortBy: { updated: -1 }
        },
        results: {
            dom: ".bodySearchContainer",
            // smartGrid: true,
            renderView: "directory.PanelHtml",
            map: {
                active: false
            },
            smartGrid: true,
            events: function (fObj) {
                $(fObj.results.dom + " .processingLoader").remove();
                Obj.events(Obj);
                coInterface.bindLBHLinks();
                directory.bindBtnElement();
                coInterface.bindButtonOpenForm();
            },
        },
        filters: {
            text: true,
        }
    },
    showElements: function (title, footer, isMyTiersLieux = false, getData = false, feedBack = () => { }) {
        $(".ans-dir").html(showAllNetwork.initView(title, footer, (isMyTiersLieux) ? "myTiersLieuxContent" : "tiersLieuxContent", "true")); // col-md-9 tiersLieuxContent
        var searchObjFilter = searchObj.init(showAllNetwork.paramsFilters);
        if (getData == true) {
            searchObjFilter.results.render = function (fObj, results, data) {
                AllTiersLiked = results;
                feedBack(AllTiersLiked);
            }
        }
        var filter = searchObjFilter;
        filter.search.init(filter);
        return filter;
    },
    previewStyle: function () {
        var styles = `
            display: block;
            padding: 0% 9%;
            padding-bottom: 10% !important;
            position: fixed;
            background: rgb(255 255 255 / 89%);
            width: 100%;
            z-index: 2;
        `;
        $("#custom-prev-div").attr("style", styles)
        if ($(".network-dir").length > 0) {
            $(".network-dir").attr("style", "position: absolute;left: 20%;right: 20%;");
            $("#regionshapeContainer").hide();
        } else {
            $(".navigator-page").attr("style", "position: absolute;");
        }
        $("#custom-prev-div").html($("#firstLoader").html())
    },
    answersStyle: function(){
        var styles = `
            display: block;
            padding: 0% 9%;
            padding-bottom: 10% !important;
            position: fixed;
            background: rgb(255 255 255 / 89%);
            width: 100%;
            z-index: 4;
        `;
        $("#custom-prev-answers").attr("style", styles);
    },
    loadPreview: function (answerId = "", answerImage = "", answerName = "", orgaId = "", page = "", network = "", allAnswers = {}, networkKey="", callBack = () => { }, slug="") {
        // if (isUserConnected == "logged") {
            showAllNetwork.previewStyle();
            let previewData = {};
            let inputsWithTitle = {};
            let groupAnswers = {};
            let countValues = {};
            previewData.id = answerId;
            previewData.image = answerImage;
            previewData.name = answerName;
            previewData.orgaId = orgaId;
            previewData.page = page;
            previewData.network = network;
            previewData.pathSectionTitle = {};
            previewData.economicModelAnswer = {};
            previewData.allTiers = {};
            previewData.markerData = {};
            previewData.supplAnswers = {};
            previewData.networkKey = networkKey;
            previewData.slug = slug;
            if (page == "tools") 
                previewData.sections = getNavigatorElement.previewSection.tools;
            if (page == "tiersLieux") {
                previewData.sections = getNavigatorElement.previewSection.thirdPlace;
                if (isUserConnected != "logged") 
                    delete previewData.sections["Votre retour"]
                localStorage.setItem("surveyAnswer", JSON.stringify(allAnswers));
            }
            if (page == "reseaux") {
                let economicModelSection = {
                    "Modèle économique": {
                        createValue: "Creation de valeur",
                        distValue: "Distribution de valeur",
                        captValue: "Capture de valeur",
                        resume: "Résumé"
                    }
                }
                inputsWithTitle = getNavigatorElement.previewSection.networks;
                let dataAnswers = {};
                let params = {
                    show: ["links.organizations", "_id"],
                    inputs: inputsWithTitle
                };
                if (typeof whereLists != "undefined" && typeof whereLists[answerName] != "undefined") {
                    params.where = whereLists[answerName].whereAnswers;
                    previewData.count = whereLists[answerName].count;
                } 
                previewData.pathSectionTitle = inputsWithTitle;
                var data = showAllNetwork.getAnswers("/form/" + getNavigatorElement.surveyData.formId, params);
                dataAnswers = data.allAnswers;
                if(typeof data.organizations != "undefined"){
                    this.organizationsData = data.organizations;
                }
                groupAnswers = showAllNetwork.groupAnswers(dataAnswers, inputsWithTitle);
                mylog.log("Group answers", groupAnswers);
                countValues = showAllNetwork.countObjectValues(groupAnswers);
                previewData.sections = { reseaux: "Le réseau", ...countValues, ...economicModelSection };
                previewData.where =  params.where;
                previewData.where.form = getNavigatorElement.surveyData.formId;
                localStorage.setItem("param-where", JSON.stringify(previewData.where));
                delete previewData.where;
            }
            showAllNetwork.openPreview('view/url/costum.views.custom.franceTierslieux.answerPreview', previewData)
            callBack()
        // } else {
        //     toastr.error("Vous devez être connecté pour y accéder");
        //     return;
        // }
    },
    openPreviewAnswers: function(url, dataContent, refresh = false){
        showAllNetwork.answersStyle();
        $("#openModal").modal("hide");
		$("#custom-prev-answers").removeClass("hidden").show();
		coInterface.showLoader("#custom-prev-answers .answers-prev-content");
		$("#custom-prev-answers").show(200);
        ajaxPost(
            null,
            url,
            dataContent,
            function(data){ 
                if(data.result){
                    $("#custom-prev-answers .answers-prev-content").html(data.html);
                    $(".btn-close-page").click(function() {
                        $("#custom-prev-answers").hide();
                        $(".answers-content-preview").html("");
                        $("#custom-prev-answers").attr("style", "display:none;")
                    });
                    if(refresh){
                        criteriaDescriptions = getNavigatorElement.getCriteria();
                    }
                }else{
                    $("#custom-prev-answers").hide();
                    $("#custom-prev-answers").attr("style", "display:none;")
                    toastr.error(data.msg);
                }
            }
        );
    },
    openPreview : function(url, dataContent){
        var allForm = '';
        if (dataContent.page == "tiersLieux") {
            allForm += `
                <button class="btn btn-list-forms">
                    <i class="fa fa-pencil" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Tous les formulaires
                </button>
            `;
            allForm += `
                <button class="btn btn-hexagone" data-name="${dataContent.name}" data-page="${dataContent.page}">
                    <i class="fa fa-cube" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Hexagone view
                </button>
            `;
        }
        if (dataContent.page == "tiersLieux" || dataContent.page == "reseaux") {
            var click = `urlCtrl.loadByHash("#@${dataContent.slug}.view.detail")`;
            if(dataContent.slug.indexOf("http") >= 0){
                click = `window.open("${dataContent.slug}", "_blank")`;
            }
            allForm += `
                <button onclick='showAllNetwork.openPreviewMap("${dataContent.name}", "${dataContent.page}")' class="btn btn-prev-map">
                    <i class="fa fa-map-o" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Carte
                </button>

                <button class="btn btn-consult-page" onclick='${click}'>
                    <i class="fa fa-file-text" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Consulter la page
                </button>
            `;
        }
        var nameNdReturnBtn = `
            <div class="btn-prev-header">
                <p class="page-title"> ${dataContent.name.replace("#","'")} </p>
                <div class="btn-content">
                    ${allForm}
                    <button class="btn btn-return-page">
                        <i class="fa fa-times-circle" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Fermer
                    </button>
                </div>
            </div>
        `;
		$("#openModal").modal("hide");
		urlCtrl.previewCurrentlyLoading=true;
		$(".searchEntity").removeClass("active");
		mylog.warn("***************************************");
		mylog.warn("***************************************");
		$("#custom-prev-div").removeClass("hidden").show();
		// coInterface.showLoader("#custom-prev-div");
		urlCtrl.checkSlugUrl(url, function(res){
			urlPreview = baseUrl+'/'+moduleId+"/";
			urlPreview+=(res.indexOf("#") == 0 ) ? "app/"+ urlCtrl.convertToPath(res) : "app/"+ res; 
			$("#custom-prev-div").show(200);
			if(!notNull(dataContent))
                dataContent= {"preview" : false};
			else if(typeof dataContent.preview== "undefined")
                dataContent.preview = false;
			ajaxPost(
		        null,
		        urlPreview,
                dataContent,
		        function(data){ 
                    $("#custom-prev-div").html(data);
                    if ($(localStorage.getItem("networkstate")).length > 0) 
                        $(localStorage.getItem("networkstate")).click();
                    // $("#custom-prev-div").css("margin", "0% 9%");
                    $(".navigator-prev-contents").prepend(nameNdReturnBtn);
                    $(".btn-return-page").click(function() {
                        $(".navigator-page").show();
                        $("#custom-prev-div").hide();
                        $("#custom-prev-div").attr("style", "display:none;")
                        $(".navigator-page").attr("style", "");
                        if ($(localStorage.getItem("networkstate")).length > 0) 
                            $(localStorage.getItem("networkstate")).click();
                    });
				}
	        );
		});
	},
    openModal : function(url, dataContent){
        showAllNetwork.previewStyle();
        var nameNdReturnBtn = `
            <div class="btn-prev-header">
                <p class="page-title"> Tiers lieux Mobilisés 2024 </p>
                <div class="btn-content">
                    <button class="btn btn-return-page">
                        <i class="fa fa-times-circle" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Fermer
                    </button>
                </div>
            </div>
        `;
		$("#openModal").modal("hide");
		urlCtrl.previewCurrentlyLoading=true;
		$(".searchEntity").removeClass("active");
		mylog.warn("***************************************");
		mylog.warn("***************************************");
		$("#custom-prev-div").removeClass("hidden").show();
		// coInterface.showLoader("#custom-prev-div");
		urlCtrl.checkSlugUrl(url, function(res){
			urlPreview = baseUrl+'/'+moduleId+"/";
			urlPreview+=(res.indexOf("#") == 0 ) ? "app/"+ urlCtrl.convertToPath(res) : "app/"+ res; 
			$("#custom-prev-div").show(200);
			if(!notNull(dataContent))
                dataContent= {"preview" : false};
			else if(typeof dataContent.preview== "undefined")
                dataContent.preview = false;
			ajaxPost(
		        null,
		        urlPreview,
                dataContent,
		        function(data){ 
                    $("#custom-prev-div").html(data);
                    if ($(localStorage.getItem("networkstate")).length > 0) 
                        $(localStorage.getItem("networkstate")).click();
                    // $("#custom-prev-div").css("margin", "0% 9%");
                    $(".navigator-prev-contents").prepend(nameNdReturnBtn);
                    $(".btn-return-page").click(function() {
                        $(".navigator-page").show();
                        $("#custom-prev-div").hide();
                        $("#custom-prev-div").attr("style", "display:none;")
                        $(".navigator-page").attr("style", "");
                        if ($(localStorage.getItem("networkstate")).length > 0) 
                            $(localStorage.getItem("networkstate")).click();
                    });
				}
	        );
		});
	},
    openPreviewMap : function(name, page) {
        if ($(".btn-list-forms").length > 0) 
            $(".btn-list-forms").hide();
        if ($(".btn-hexagone").length > 0) 
            $(".btn-hexagone").hide();
        $(".map-content-prev").show();
        $(".vertical-menu.all-content-prev").hide();
        $(".btn-prev-map").attr("onclick", "showAllNetwork.openInitialPrev('"+name+"', '"+page+"')");
        $(".btn-prev-map").html(`<i class="fa fa-arrow-circle-left" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Revenir aux sections`);
        // alert($(".map-content-prev").length);
        showAllNetwork.showGenPrevMap('#map-preview-filter', '#map-preview', name, page);
    },
    openInitialPrev : function (name, page) {
        if ($(".btn-list-forms").length > 0) 
            $(".btn-list-forms").show();
        if ($(".btn-hexagone").length > 0) 
            $(".btn-hexagone").show();
        $(".map-content-prev").hide();
        $(".vertical-menu.all-content-prev").show();
        $(".btn-prev-map").attr("onclick", "showAllNetwork.openPreviewMap('"+name+"', '"+page+"')");
        $(".btn-prev-map").html(`<i class="fa fa-map-o" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Carte`);
    },
    openInitialPrevGraph : function (name, page) {
        if ($(".btn-list-forms").length > 0) 
            $(".btn-list-forms").show();
        if ($(".btn-hexagone").length > 0) 
            $(".btn-hexagone").show();
        $("#hexagone").hide();
        $(".vertical-menu.all-content-prev").show();
        $(".btn-prev-map").attr("onclick", "showAllNetwork.openPreviewMap('"+name+"', '"+page+"')");
        $(".btn-prev-map").html(`<i class="fa fa-map-o" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Carte`);
    },
    openAnswersMap : function(name, formId, path) {
        $(".answers-map-preview").show();
        $(".answers-content-preview").hide();
        $(".btn-all-tl").attr("onclick", "showAllNetwork.openInitialAnswers('"+name+"', '"+formId+"', '"+path+"')");
        $(".btn-all-tl").html(`<i class="fa fa-arrow-circle-left" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Revenir aux sections`);
        // alert($(".map-content-prev").length);
        showAllNetwork.showGenPrevAnswers('#map-answers-filter', '#map-answers', name, path, formId);
    },
    openInitialAnswers : function (name, formId, path) {
        $(".answers-map-preview").hide();
        $(".answers-content-preview").show();
        $(".btn-all-tl").attr("onclick", "showAllNetwork.openAnswersMap('"+name+"', '"+formId+"', '"+path+"')");
        $(".btn-all-tl").html(`<i class="fa fa-map-o" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Afficher les tiers lieux`);
    },
    showGenPrevMap : function (domFilter, domMap, thirdPlcOrNtwrk, page) {
        var arrayName = [];
        var level3Network = "";
        var filter = {}
        var count = 30;
        if (page == "tiersLieux") 
            arrayName.push(thirdPlcOrNtwrk)
            filter = {name: {'$in': arrayName}};
        if (page == "reseaux" && jsonHelper.pathExists("whereLists."+ thirdPlcOrNtwrk +".arrayName")) {
            arrayName = [...whereLists[thirdPlcOrNtwrk].arrayName];
            level3Network = whereLists[thirdPlcOrNtwrk].level3Key;
            count = whereLists[thirdPlcOrNtwrk].count;
            filter = {name: {'$in': arrayName}, "address.level3": level3Network};
        }
        if (arrayName.length > 0) {
            var paramsFIlter = {
                urlData: baseUrl + "/co2/search/globalautocomplete",
                container: domFilter,
                defaults: {
                    indexStep : count,
                    notSourceKey: true,
                    types : ["organizations"],
                    filters: filter
                },
                filters : {}
            }
            var filterNetwork = searchObj.init(paramsFIlter);
                filterNetwork.results.render = function(fObj, results, data) {
                    var customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                    customMap.getPopup = function (data) {
                        var id = data._id ? data._id.$id:data.id;
                        var imagePath = (data?.profilMediumImageUrl != undefined) ? data.profilMediumImageUrl : defaultImage;
                        var imgProfil = mapCO.getOptions().mapCustom.getThumbProfil(data);
        
                        var eltName = data.title ? data.title:data.name;
                        var popup = "";
                        popup += "<div class='padding-5' id='popup" + id + "'>";
                        popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                        popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";
        
                        if(data.tags && data.tags.length > 0){
                            popup += "<div style='margin-top : 5px;'>";
                            var totalTags = 0;
                            $.each(data.tags, function(index, value){
                                
                                if (totalTags < 2 && value!=="Compagnon France Tiers-Lieux") {
                                    popup += "<div class='popup-tags'>#" + value + " </div>";
                                    totalTags++;
                                }
                            })
                            popup += "</div>";
                        }
                        if(data.address){
                            var addressStr="";
                            if(data.address.streetAddress)
                                addressStr += data.address.streetAddress;
                            if(data.address.postalCode)
                                addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                            if(data.address.addressLocality)
                                addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                            popup += "<div class='popup-address text-dark'>";
                            popup +=    "<i class='fa fa-map-marker'></i> "+addressStr;
                            popup += "</div>";
                        }
                        if(data.shortDescription && data.shortDescription != ""){
                            popup += "<div class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Description</div>";
                            popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                            popup += "</div>";
                        }
                        if((data.url && typeof data.url == "string") || data.email || data.telephone){
                            popup += "<div id='pop-contacts' class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Contacts</div>";
                            
                            if(data.url && typeof data.url === "string"){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                                popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                                popup += "</div>";
                            }
        
                            if(data.email){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                                popup += "</div>";
                            }
        
                            if(data.telephone){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa-phone fa_phone'></i> ";
                                var tel = ["fixe", "mobile"];
                                var iT = 0;
                                $.each(tel, function(keyT, valT){
                                    if(data.telephone[valT]){
                                        $.each(data.telephone[valT], function(keyN, valN){
                                            if(iT > 0)
                                                popup += ", ";
                                            popup += valN;
                                            iT++; 
                                        })
                                    }
                                })
                                popup += "</div>";
                            }
        
                            popup += "</div>";
                            popup += "</div>";
                        }
                        var url = '#page.type.' + data.collection + '.id.' + id;
                        popup += "<div class='popup-section'>";
                        // Mahefa
                        if(paramsMapCO.activePreview && page == "reseaux")
                            popup += `<a onclick='showAllNetwork.loadPreview("","${imagePath}","${data.name.replace("'", "#")}","${id}","tiersLieux", "${thirdPlcOrNtwrk}" , {}, "${level3Network}", function(){},"${data.slug}")' class='item_map_list popup-marker' id='popup${id}'>`;
                        else
                            popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                        popup += '</div></a>';
                        popup += '</div>';
                        popup += '</div>';
        
                        return popup;                        
                    };
                    var appMap = new CoMap({
                        zoom : 5,
                        container : domMap,
                        activePopUp : true,
                        mapOpt:{
                            zoomControl:true,
							doubleClick : true,
                        },
                        activePreview : false,
                        mapCustom:customMap,
                    })        
					appMap.showLoader();
                    appMap.addElts(results);
                };
                filterNetwork.search.init(filterNetwork);
            }
    },
    showGenPrevAnswers : function (domFilter, domMap, criteria, path, formId) {
        var arrayName = [];
        var level3Network = "";
        var filter = {
        }
        var count = 30;
        if(path && path != "" && formId && formId != ""){
            path = JSON.parse(path);
            if(typeof path == "string"){
                if(path.includes("multiCheckboxPlus")){
                    filter[path+'.'+criteria] = {"$exists": true};
                }else if(path.includes("multiRadio")){
                    filter[path+".value"] = criteria;
                }else if(path.includes("checkboxNew")){
                    filter[path.replace("checkboxNew", "")] = criteria;
                }else if(path.includes("radioNew")){
                    filter[path.replace("radioNew", "")] = criteria;
                }else {
                    filter[path] = criteria;
                }
                filter["links.organizations"] = {'$exists': true};
            }else{
                filter["$or"] = {};
                path.forEach(function(value){
                    if(value.includes("multiCheckboxPlus")){
                        filter["$or"][value+'.'+criteria] = {"$exists": true};
                    }else if(value.includes("multiRadio")){
                        filter["$or"][value+".value"] = criteria;
                    }else if(value.includes("checkboxNew")){
                        filter["$or"][value.replace("checkboxNew", "")] = criteria;
                    }else if(value.includes("radioNew")){
                        filter["$or"][value.replace("radioNew", "")] = criteria;
                    }else {
                        filter["$or"][value] = criteria;
                    }
                });
                filter["links.organizations"] = {'$exists': true};
                console.log("Filters globalauto", filter);
            }
            arrayName.push(criteria);
        }
        if (arrayName.length > 0) {
            var customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
            customMap.getPopup = function (data) {
                var id = data._id ? data._id.$id:data.id;
                var imagePath = (data?.profilMediumImageUrl != undefined) ? data.profilMediumImageUrl : defaultImage;
                var imgProfil = mapCO.getOptions().mapCustom.getThumbProfil(data);

                var eltName = data.title ? data.title:data.name;
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
                    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        
                        if (totalTags < 2 && value!=="Compagnon France Tiers-Lieux") {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                            totalTags++;
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    popup += "<div class='popup-address text-dark'>";
                    popup +=    "<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Description</div>";
                    popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
                    popup += "</div>";
                }
                var url = '#page.type.' + data.collection + '.id.' + id;
                popup += "<div class='popup-section'>";
                // Mahefa
                if(paramsMapCO.activePreview && page == "reseaux")
                    popup += `<a onclick='showAllNetwork.loadPreview("","${imagePath}","${data.name.replace("'", "#")}","${id}","tiersLieux", "${thirdPlcOrNtwrk}" , {}, "${level3Network}", function(){},"${data.slug}")' class='item_map_list popup-marker' id='popup${id}'>`;
                else
                    popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></a>';
                popup += '</div>';
                popup += '</div>';

                return popup;                        
            };
            var appMap = new CoMap({
                zoom : 5,
                container : domMap,
                activePopUp : true,
                mapOpt:{
                    zoomControl:true,
                    doubleClick : true,
                },
                activePreview : false,
                mapCustom:customMap,
            }) 
            ajaxPost(
                null,
                baseUrl + "/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/"+formId+"/addOrga/true",
                {
                    searchType : ["answers"],
                    indexMin: 0,
                    indexStep: 9000,
                    notSourceKey: true,
                    filters: filter,
                    fediverse: false
                },
                function(data){
                    if(typeof data.results != "undefined" && data.results){
                        var results =Object.entries(data.results).reduce((result, [key, value]) => {
                            if (value.organizations && Object.keys(value.organizations).length > 0) {
                                result[key] = value.organizations; // Conserve la clé et remplace la valeur par organizations
                            }
                            return result;
                        }, {}); 
                        if(results){
                            // appMap.showLoader();
                            appMap.addElts(results);
                        }else{
                            toastr.error("Aucun résultat trouvé");
                            appMap.hideLoader();
                        }
                    }
                }
            );
            // var paramsFIlter = {
            //     urlData: baseUrl + "/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/"+formId+"/answerPath/"+path+"/addOrga/true",
            //     container: domFilter,
            //     defaults: {
            //         indexStep : count,
            //         notSourceKey: true,
            //         types : ["answers"],
            //         filters: filter
            //     },
            //     filters : {}
            // }
            // var filterNetwork = searchObj.init(paramsFIlter);
            // filterNetwork.results.render = function(fObj, results, data) {
                     
            //     results =Object.entries(results).reduce((result, [key, value]) => {
            //         if (value.organizations && Object.keys(value.organizations).length > 0) {
            //             result[key] = value.organizations; // Conserve la clé et remplace la valeur par organizations
            //         }
            //         return result;
            //     }, {});  
            //     appMap.showLoader();
            //     appMap.addElts(results);
            // };
            // filterNetwork.search.init(filterNetwork);
        }
    },
    groupAnswers: function (allAnswers = {}, inputs) {
        if (Object.keys(allAnswers).length > 0) {
            $.each(allAnswers, function (key, value) {
                if ((Object.keys(value).length > 0) && (typeof value.answers != "undefined")) {
                    $.each(value.answers, function (inputKey, inputValue) {
                        Object.keys(inputs).map(function (num) {
                            let element = num.split(".")[2];
                            if (inputValue[element] != undefined) {
                                if(element == "multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab"){
                                    inputValue[element].map(function(data){
                                        data[Object.keys(value.links.organizations)[0]] = data[Object.keys(data)[0]];
                                        delete data[Object.keys(data)[0]];
                                        return data;
                                    });
                                }
                                if (typeof allAnswers[element] == "undefined")
                                    allAnswers[element] = [];
                                allAnswers[element] = [...allAnswers[element], ...inputValue[element]];
                            }
                        })
                    })
                }

            })
        }
        return allAnswers;
    },
    countObjectValues: function (obj = {}) {
        const result = {};
        if (Object.keys(obj).length > 0) {
            for (const key in obj) {
                if (Array.isArray(obj[key])) {
                    const count = {};
                    var orgaData = {};
                    for (const item of obj[key]) {
                        if (typeof item == "string") {
                            const value = item;
                            count[value] = (count[value] || 0) + 1;
                        } else {
                            for (const prop in item) {
                                const value = item[prop].value;
                                if(key == "multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab"){
                                    orgaData[value] = (orgaData[value] || []);
                                    orgaData[value].push(prop);
                                }
                                count[value] = (count[value] || 0) + 1;
                            }
                        }
                    }
                    if(key == "multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab"){
                        for(const activity in orgaData){
                            if(activity == "Bureau / Coworking"){
                                var countActivity = 0;
                                for(const orgaId in this.organizationsData){
                                    if((!orgaData[activity].includes(orgaId)) && typeof this.organizationsData[orgaId].tags != "undefined" && (this.organizationsData[orgaId].tags.includes("Bureau / Coworking") || this.organizationsData[orgaId].tags.includes("Bureaux partagés / Coworking"))){
                                        countActivity += 1;
                                    }
                                }
                                count[activity] += countActivity;
                            }
                        }
                    }
                    result[key] = count;
                }
            }
        }
        return result;
    },
    showModal : function (networkName, address = "", arrayOrga=[], footer="") {
        if (typeof arrayOrga == "string") {
            arrayOrga = arrayOrga.replace("#","'");
            arrayOrga = (arrayOrga.includes(",") ? arrayOrga.split(",") : [arrayOrga])
        }
        var initView = (arrayOrga != undefined && Array.isArray(arrayOrga) && arrayOrga.length > 0) ? showAllNetwork.initView(networkName, "searchModalFooter", "", "false", true) : showAllNetwork.initView(networkName,"searchModalFooter");
        smallMenu.open(`<div id="tiers-modal" class="container padding-bottom-20 padding-left-0 padding-right-0 ans-dir"></div>`);
        $("#tiers-modal.ans-dir").html(initView);
        var modalParams = showAllNetwork.paramsFilters;
        initPagination = $(".reseauxFooter").html();
        modalParams.results.renderView = "directory.modalPanel";
        modalParams['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/organizations/form/63e0a8abeac0741b506fb4f7";
        modalParams.container = "#tiers-modal #filterContainercacs";
        modalParams.defaults.types = ["organizations"];
        modalParams.header.dom = "#tiers-modal .headerSearchIncommunity";
        modalParams.results.dom = "#tiers-modal .bodySearchContainer";
        delete modalParams.defaults.filters;
        delete modalParams.defaults.fields;
        modalParams.defaults.filters = {"address.level3": address, '$or': {"source.keys": "franceTierslieux", "source.key": "tierslieuxorg", "reference.costum" : "franceTierslieux" }};
        if (arrayOrga != undefined && Array.isArray(arrayOrga) && arrayOrga.length > 0)
            modalParams.defaults.filters = {"_id" : {'$in' : arrayOrga}}
        searchObj.footerDom = "#tiers-modal .searchModalFooter";
        var modalFilter = searchObj.init(modalParams);
        modalFilter.search.init(modalFilter);
        // if (typeof footer == "string" && notEmpty(footer)) {
            $("[data-dismiss='modal']").off().on("click", function(){
                searchObj.footerDom = footer;
                let filter = searchObj.init(showAllNetwork.paramsFilters);
                filter.search.init(filter);
            });
        // }
        if ($("#openModal .btn-filters-region").length > 0) {
            $("#openModal .btn-filters-region").remove();
        }
    },
    arrayToObject: function (arr = []) {
        return arr.reduce(function (obj, key) {
            obj[key] = key;
            return obj;
        }, {});
    },
    getAnswers: function (path = "", params, getuploads=false) {
        let results = {}
        var type = (getuploads) ? "documents" : "answers";
        if (path != "") {
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/getallanswers/type/' + type + path,
                params,
                function (data) {
                    if (data.result) {
                        results = data;
                    }
                },
                null,
                "json",
                { async: false }
            );
        }
        return results;
    },
    markerParams: function (network, where = null) {
        let params =
        {
            arrayFormPath: {
                "645b300b6d70bb35426fc0e3": "answers.lesCommunsDesTierslieux1052023_949_0.finderlesCommunsDesTierslieux1052023_949_0lat3sl0xwwg06k856jc",
                "6486d24e9cad105cbf29a777": "answers.lesCommunsDesTierslieux1262023_1215_0.finderlesCommunsDesTierslieux1262023_1215_0lix46h1uhnukfgtzj6s",
                "646498a5f2b3d6423d74b7f6": "answers.lesCommunsDesTierslieux1752023_1312_0.finderlesCommunsDesTierslieux1752023_1312_0lix4a7257iht8lp282r"
            },
            showAnswers: "true"
        }
        if (where === null) {
            params.where = {
                "address.level3": network,
                "source.keys": "franceTierslieux",
            };
        } else {
            params.where = where;
        }
        return params;
    },
    getElement: function (type, id, fields = {}) {
        let result = {};
        if (costum.store[type+id] != undefined) {
            result = costum.store[type+id];
        } else {
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/element/get/type/" + type,
                { id: id, ...fields },
                function (data) {
                    if (data.result) {
                        result = data.map;
                        costum.store[type+id] = result;
                    }
                },
                null,
                "json",
                { async: false }
            );
        }
        return result;
    },
    setButtonColor : function (answers={}, subFormsArrays=[], name, subFrmArray=[]) {
        let countFormParams = 0;
        let countAnswers = 0;
        if (answers != undefined) {
            if (Object.keys(answers).length > 0) {
                $.each(answers, function(k, v) {
                    if (subFrmArray.includes(k)) {
                        countAnswers = countAnswers + Object.keys(v).length;
                    } else {
                        if (k != "") {
                            countAnswers = countAnswers + 1;
                        }
                    }
                })
            }
        }
        if (subFormsArrays.length > 0) {
            $.each(subFormsArrays, function(k, v) {
                let eachInputLngth = Object.keys(v).length;
                countFormParams = countFormParams + eachInputLngth;
            })
        }
        return (countAnswers == countFormParams) ? "green" : "blue";
    },
    modalButtonHtml : function (modalClass="", answerId="", formId="", step="empty", btnName="Formulaire", color="", path="", isPublic=false, input="empty") {
        if (showAllNetwork.isAuthorized == 1 || isPublic == true) {
            let getFinderId = ((path != "" || path != undefined) && path.split(".")[2] != undefined) ? path.split(".")[2].replace("finder", "") : "";
            return `
                <button 
                    class="btn btn-form-modal btn-small ${(color != "") ? "bg-" + color : "bg-blue"} ${modalClass}" style="cursor: pointer; color: white; " 
                    data-id="${answerId}" 
                    data-step="${step}"
                    data-elformid="${formId}"
                    data-input="${input}"> 
                    <h5> <i class="fa fa-pencil"> </i> ${btnName} </h5>
                </button>						
            `;
        } else {
            return ``;
        }
    },
    newAnswers : function (formId="", callBack=function(){}, accessValidation=false) {
        let dataIdRes = "";
        
        if (notEmpty(userId) && accessValidation /*|| formId == preview.satisfactionTrId*/) { 
            ajaxPost(
                null,
                baseUrl + `/survey/answer/newanswer/form/${formId}`,
                {
                    "action": "new"
                },
                function (data) {
                    if (data['_id']) {
                        dataIdRes = data['_id']['$id'];
                        callBack(data, dataIdRes);
                    }
                },
                null,
                "json",
                { async: false }  
            )	
        } 
        return dataIdRes;				
    },
    clickModalEvent : function (selector, callBack=()=>{}, page="") {
        if (notEmpty(page)) {
            $(selector).off().on('click', function (e) {
                let containerParentId = $(this).parent().parent().attr('id');
                let finderKey = $(this).data("key");
                let answerId = $(this).data("id");
                let formId = $(this).data("elformid");
                let step = $(this).data("step");
                let input = $(this).data("input");
                containerParentId = (containerParentId == undefined) ? $(this).parent().parent().parent().parent().attr('id') : containerParentId;
                showAllNetwork.answerModal(answerId, formId, step, finderKey, containerParentId = (containerParentId == undefined) ? "section-info" : containerParentId, input, callBack(), page);
            });
        }

    },
    answerModal : function (answerId="", formId="", step="", finderKey="", containerParentId="", input="", callBack=()=>{}, page="") {
        let contentId = $("div.tab-pane.active.in").children().attr("id");
        if ((showAllNetwork.isAuthorized == 1 /*|| formId == preview.satisfactionTrId*/) && containerParentId != "" && containerParentId != undefined) { 
            $(".form-answer-content").show();
            // coInterface.showLoader("#" + contentId);
            $("#" + contentId).html($("#firstLoader").html())
            let ajaxBaseUrl = baseUrl + '/survey/answer/index/id/' + answerId + '/form/' + formId + '/mode/w';
            if (step != undefined && step != "" && step != "empty") {
                ajaxBaseUrl = baseUrl +  "/survey/answer/answer/id/" + answerId + "/mode/w/form/" + formId + "/step/" + step;
                if ((input != undefined && input != "" && input != "empty")) 
                    ajaxBaseUrl = baseUrl +  "/survey/answer/answer/id/" + answerId + "/mode/w/form/" + formId + "/step/" + step + "/input/" + input;
            }

            ajaxPost(null, ajaxBaseUrl,
            { url : window.location.href },
            function(data){
                let returnBtn = `
                    <div class="form-btn">
                        <button style="color:white;" data-form="${formId}" data-answerId="${answerId}" class="bg-main2 btn btn-return-form-${page} btn-default margin-top-20 margin-bottom-20" >
                            <i class="fa fa-arrow-circle-left"></i>
                        </button>
                    </div>
                `;
                $("#" + contentId).html(data).prepend(returnBtn);
                $("#modeSwitch").hide();
                if ($("#customHeader").length > 0)
                    $("#customHeader").css("margin-top","50px");
                callBack();
            },"html");
         } else { 
            toastr.error("Erreur")
        }
    },
    clickSectionHref: function(id) {
        let sectionId = '#' + $(id).parent().attr("id");
        return "[href='"+sectionId+"']";
    }
}




