var getNavigatorElement = {
    getToolsTilte : showAllNetwork.getElement("forms", "636cdf30985a2522284703ec")?.inputs,
    satisfactionForm : showAllNetwork.getElement("forms", "64a3fbd77c66a6036a282e8a"),
    aggregateSimilar : showAllNetwork.getElement("mappings", "64f987cf01e4500d4019a9f0"),
    getToolsAnswers : function (paramsTools={}) {
        var results = {};
        if (typeof paramsTools == "object" && Object.keys(paramsTools).length > 0) {
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/listtoolsanswers',
                paramsTools,
                function (data) { 
                    if (typeof data == "object" && Object.keys(data).length > 0) 
                        results = data;
                },
                null,
                "json",
                { async: false }     
            ) 
        }
        return results;
    },
    getToolFormImages : function () {
        var toolForm = showAllNetwork.getElement("forms", "636cd563e2439b7fc12cd680");
        var imagesPath = {};
        if (typeof toolForm == "object" && toolForm?.params?.imagesPath != undefined) {
            imagesPath = toolForm.params.imagesPath;
        }
        return imagesPath;
    },
    getWhereList: function () {
        if (typeof costum.store != "undefined" && costum.store.whereList == undefined && typeof params != "undefined") {
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/getnetworkswerelist',
                params,
                function (data) {   
                   costum.store.whereList = data;
                },
                null,
                "json",
                { async: false }   
            );
        }
    },
    getSurveySubforms : function (params) {
        let result = {}
        if (costum.store.subFormsInput != undefined) {
            result =  costum.store.subFormsInput;
        } else {
            ajaxPost(
                null,
                baseUrl + '/costum/francetierslieux/getallsuplanswers/subForm/all',
                params,
                function (data) {   
                   result = data;
                   costum.store.subFormsInput = data;
                },
                null,
                "json",
                { async: false }     
            );
        }

        return result;
    },
    subForms :  {
        subformArray : 
      [ 
        "franceTierslieux1522023_1341_0", 
        "franceTierslieux1522023_1352_1", 
        "franceTierslieux1522023_146_2", 
        "franceTierslieux1522023_1549_3", 
        "franceTierslieux1822023_1721_4", 
        "franceTierslieux1922023_1231_5", 
        "franceTierslieux1922023_1231_6", 
        "franceTierslieux1922023_1417_7", 
        "franceTierslieux1922023_1523_8", 
        "franceTierslieux2022023_732_9", 
        "franceTierslieux2022023_753_10"
       ]
    },
    surveyData : {
        formId : "63e0a8abeac0741b506fb4f7",
        path :  "answers.franceTierslieux1522023_1352_1.finderfranceTierslieux1522023_1352_1le5ofi0x733rc1gmbk4"
    },
    suplment : {
        frequentation : {
            path : "answers.lesCommunsDesTierslieux1752023_1312_0.finderlesCommunsDesTierslieux1752023_1312_0lix4a7257iht8lp282r",
            id : "646498a5f2b3d6423d74b7f6"
        },
        avtivity : {
            path : "answers.lesCommunsDesTierslieux1262023_1215_0.finderlesCommunsDesTierslieux1262023_1215_0lix46h1uhnukfgtzj6s",
            id : "6486d24e9cad105cbf29a777"
        },
        satisfaction : {
            path : "answers.lesCommunsDesTierslieux472023_154_0.finderlesCommunsDesTierslieux472023_154_0ljo6ru90tnrau11hui",
            id : "64a3fbd77c66a6036a282e8a"
        },
        tool : {
            path : "answers.lesCommunsDesTierslieux10112022_1423_0.finderlesCommunsDesTierslieux10112022_1423_0lmbmy4z4ze9v982h4fh",
            id : "636cd563e2439b7fc12cd680",
            step : "lesCommunsDesTierslieux10112022_1423_0"
        },
        economicModel : {
            path : "answers.lesCommunsDesTierslieux1052023_949_0.finderlesCommunsDesTierslieux1052023_949_0lat3sl0xwwg06k856jc",
            id : "645b300b6d70bb35426fc0e3",
        },
        networkSupl : {
            path : "answers.lesCommunsDesTierslieux17102023_108_0.finderlesCommunsDesTierslieux17102023_108_0lo5zvgqitv0b2mx2rl",
            id : "652e32b222c3cf69f018ec40",
            thematicPath : "lesCommunsDesTierslieux17102023_108_0.multiCheckboxPluslesCommunsDesTierslieux17102023_108_0lntzgckrxocls9xuwxo",
            leve3Address : "lesCommunsDesTierslieux17102023_108_0.lesCommunsDesTierslieux17102023_108_0lo5zt9q2tj5cik0qfm",
        },
        profile : {
            id : "648c08df85d91a0a7d0475a0",
            exludeInput : "finderlesCommunsDesTierslieux1662023_112_0lji76lrejk3f6tw75oq",
            exludeStep : "lesCommunsDesTierslieux1662023_112_0"
        },
        mobilize: {
            id: "666c245dd4968131956aa91e",
            path: "answers.lesCommunsDesTierslieux1462024_1313_0.finderlesCommunsDesTierslieux1462024_1313_0lxeldfuzo8xtrxheuwo",
            thematicPath: "lesCommunsDesTierslieux1462024_1313_0.finderlesCommunsDesTierslieux1462024_1313_0lxeldfuzo8xtrxheuwo"
            // path : "answers.lesCommunsDesTierslieux1752023_1312_0.finderlesCommunsDesTierslieux1752023_1312_0lix4a7257iht8lp282r",
            // id : "646498a5f2b3d6423d74b7f6"
        }
    },
    showInfo : {
        id : "#info",
    },
    showAllForms : {
        id : "#allForm",
    },
    showHexagone : {
        id : "#hexagone",
    },
    showActivity : {
        id : "#activity",
        step : "lesCommunsDesTierslieux1262023_1215_0",
        input : "lesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq",
        activityAnswerPath : ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq",
        answerPath : [
            "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq",
            "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab"
        ],
    },
    showPrincipleAndValue : {
        id : "#principleAndValue",
        step : "franceTierslieux1522023_146_2",
        input : "franceTierslieux1522023_146_2le5osg2xb6hjxhv95op",
        answerPath: "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op",
    },
    showEquipment : {
        id : "#equipment",
        step : "lesCommunsDesTierslieux1052023_949_0",
        input : "lesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw",
        answerPath: [
            "answers.lesCommunsDesTierslieux1052023_949_0.multiCheckboxPluslesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw",
            "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4",
        ]  
    },
    showDomain : {
        id : "#domain",
        step : "franceTierslieux1522023_146_2",
        input : "franceTierslieux1522023_146_2le5osg2y8qd7qkuri9q",
        answerPath: "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q"
    },
    showFrequentation : {
        id : "#frequentation",
        step : "lesCommunsDesTierslieux1752023_1312_0",
        input : "lesCommunsDesTierslieux1752023_1312_0lhrhuhp0xv1qqamf0i",
        answerPath: [
            "answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhrhuhp0xv1qqamf0i",
            "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e"
        
        ]
    },
    showAgeFrequentation : {
        id : "#ageFrequentation",
        step : "lesCommunsDesTierslieux1752023_1312_0",
        input : "lesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq",
        answerPath: [
            "answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq",
            "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v"
        ]
    },
    showEmployeesContract : {
        id : "#employeeContract",
        step : "franceTierslieux1922023_1231_6",
        input : "franceTierslieux1922023_1231_6lebdout9qitprmvmmwt",
        answerPath: "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt"
    },
    showGouvernance: {
        id : "#gouvernance",
        step : "franceTierslieux1922023_1417_7",
        input : "franceTierslieux1922023_1417_7lebeyrj9evio2mudes",
        answerPath: "answers.franceTierslieux1922023_1417_7.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes"
    },
    listTools : {
        id : "#numericUsage",
        step : "lesCommunsDesTierslieux10112022_1423_0",
        input : "",
    },
    showHealth : {
        id : "#tiersHealth",
        step : "lesCommunsDesTierslieux1052023_949_0",
        input : "lesCommunsDesTierslieux1052023_949_0laazfcfrg2ze8nirm0f",
        memberShip : {
            step : "franceTierslieux1922023_1231_5",
            input : "franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve",
        },
        generatedProductAns : {
            step : "franceTierslieux2022023_732_9",
            input : "franceTierslieux2022023_732_9lecfwuxx9q1jro8u6xf",
        }
    },
    showCreateValue : {
        id : "#createValue",
        equipmentSuplement: {
            step : "lesCommunsDesTierslieux1052023_949_0",
            input : "lesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw",
        },
        production: {
            step : "lesCommunsDesTierslieux1262023_1215_0",
            input : "lesCommunsDesTierslieux1262023_1215_0liu3onic3xd6dvbjzxu",
        },
        activity : {
            step : "lesCommunsDesTierslieux1262023_1215_0",
            input : "lesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq",
        }
    },
    showDistributionValue : {
        id : "#distValue",
        step : "lesCommunsDesTierslieux1262023_1215_0",
        input : "lesCommunsDesTierslieux1262023_1215_0lisl16ek1pd92s71wm8",
    },
    showCaptureValue : {
        id : "#captValue",
        step : "lesCommunsDesTierslieux1262023_1215_0",
        input : "lesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr",
    },
    showResume : {
        id : "#resume",
        negativeValue : {
            step : "lesCommunsDesTierslieux1262023_1215_0",
            input : "lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv",
        },
        positiveValue : {
            step : "lesCommunsDesTierslieux1262023_1215_0",
            input : "lesCommunsDesTierslieux1262023_1215_0liu51awl25edd4zlmsp",
        }
    },
    showForm : {
        id : "#form",
    },
    showFeedBack : {
        id : "#form",
    },
    document : {
        subKey : "lesCommunsDesTierslieux2292023_1125_0.lesCommunsDesTierslieux2292023_1125_0lnbdqp3j23umbyupatv"
    },
    previewSection : {
        thirdPlace : {
            info: "Information générale",
            allForm: "Formulaires",
            // hexagone: "Hexagone",
            activity: "Activités proposées",
            principleAndValue: "Valeurs et principes",
            equipment: "Types équipements",
            domain: "Domaines",
            frequentation: "Fréquentation",
            ageFrequentation: "Fréquentation par âge",
            employeeContract: "Ressource humaines",
            gouvernance: "Gouvernance",
            numericUsage: "Outils numériques",
            "Modèle économique": {
                presentation: "Présentation",
                createValue: "Creation de valeur",
                distValue: "Distribution de valeur",
                captValue: "Capture de valeur",
                tiersHealth: "Santé du tiers lieux",
                resume: "Résumé"
            },
            reseaux: "Réseaux",
            "Votre retour": {
                form: "Formulaire",
                visitFrequency: "Votre Fréquence de visite",
                reasonOfVisit: "Vos Motifs de visite",
                equipmentSatisfaction: "Votre Satisfaction équipement",
                qualityOfEvent: "Qualité des événements",
                ownSatisfaction: "Satisfaction du personnel",
                valuePrice: "Valeur / prix",
                favoriteAspect: "Mes aspects appréciés",
            },
            "Retour utilisateurs": {
                allVisitFrequency: "Fréquence de visite",
                allReasonOfVisit: "Motifs de visite",
                allEquipmentSatisfaction: "Satisfaction équipement",
                allQualityOfEvent: "Qualité des événements",
                allOwnSatisfaction: "Satisfaction du personnel",
                allValuePrice: "Valeur / prix",
                allFavoriteAspect: "Aspects appréciés",
                allAmeliorateDomain: "Domaines à améliorer"
            },
            similarTr: "Tiers-lieux similaires"
        },
        networks : {
            "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab": "#Cartographie des activités",
            "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op": "#Cartographie des valeurs",
            "answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4": "#Cartographie des équipements",
            // "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q"    
            "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gowlj39f2a2e": "#Fréquentation par catégorie sociale",
            "answers.franceTierslieux1922023_1231_5.franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v": "#Fréquentation par âge",
            "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout8liagfm99v2r": "#Metiers pour faire fonctionner le réseau (Ressources humaines)",
            "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt": "#Les Contrats du réseau (Ressources humaines)"
            // "answers.franceTierslieux1922023_1231_6.multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt",  
            // "answers.franceTierslieux1922023_1417_7.multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes",   
        },
        tools : {
            info: "Information sur l'outil"
        }
    },
    getSurveySubform : function () {
       let allSubforms =  getNavigatorElement.getSurveySubforms(getNavigatorElement.subForms);
       let allInputs = {};
       if (allSubforms != undefined && Object.keys(allSubforms).length > 0) {
            $.each(allSubforms, function (k, v) {
                if (v.inputs != undefined) {
                    allInputs = {...v.inputs, ...allInputs}
                }
            })
       }
       return allInputs;
    },
    getIconsImages : function () {
        var iconsImages = {};
        ajaxPost(
            null,
            baseUrl+"/costum/francetierslieux/icons",
            {
                type: 'fetch'
            },
            function(success){
                if(success.results){
                    iconsImages = success.data;
                }
            },
            null,
            "json",
            { async: false }
        )
        return iconsImages;
    },
    getCriteria : function () {
        var criteriaDescriptions = {};
        ajaxPost(
            null,
            baseUrl+"/costum/francetierslieux/criteria",
            null,
            function(success){
                if(success.results){
                    criteriaDescriptions = success.data;
                }
            },
            null,
            "json",
            { async: false }
        )
        return criteriaDescriptions;
    }
};
getNavigatorElement.getWhereList();
var AllToolImage = getNavigatorElement.getToolFormImages();
var allSurveyInput =  [getNavigatorElement.getSurveySubform()];
var linkedImage = getNavigatorElement.getIconsImages();
var whereLists; 
if (costum.store.whereList && costum.store.whereList.whereLists)
	whereLists = costum.store.whereList.whereLists;