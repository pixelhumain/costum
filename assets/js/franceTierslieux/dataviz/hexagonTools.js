var hexagonTools = {
    toolsData: function(tools, btn, path){
        listToolSuplAnswer = jsonHelper.getValueByPath(tools["getSuplForm"], tools["answerId"] + ".answers");
        var criteriaArrays = [];
        let imgData = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        if (Object.keys(listToolSuplAnswer).length > 0 && getNavigatorElement.getToolsTilte != undefined) {
            for (var key in listToolSuplAnswer) {
                if (listToolSuplAnswer.hasOwnProperty(key)) {
                    let paramsKey = key.replace("yesOrNo","");
                    if (jsonHelper.getValueByPath(getNavigatorElement, "getToolsTilte." + paramsKey + ".label") != undefined) {
                        let title = jsonHelper.getValueByPath(getNavigatorElement, "getToolsTilte." + paramsKey + ".label");
                        var criteriaObj = listToolSuplAnswer[key];
                        for (var criteriaKey in criteriaObj) {
                            if (!criteriaArrays.includes(criteriaObj[criteriaKey].criteria)) {
                                criteriaArrays.push(criteriaObj[criteriaKey].criteria);
                                if (criteriaObj.hasOwnProperty(criteriaKey)) {
                                    var criteria = criteriaObj[criteriaKey].criteria;
                                    if (jsonHelper.getValueByPath(criteriaObj, criteriaKey + ".user") != undefined && criteria != "" && criteria != undefined && criteria != null) {
                                        var toolNameKey = criteria.replace(/[\s\/']/g, '').toLowerCase();
                                        var defaultImage = parentModuleUrl+"/images/default_tool.png";
                                        var urlImageTool = (jsonHelper.pathExists("toolsImageUrl."+criteria.toLowerCase().replace(/\s/g, "")+".imageUrl")) ? ((toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl == "") ? defaultImage : toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl) : defaultImage;
                                        urlImageTool = (jsonHelper.pathExists("AllToolImage." + toolNameKey)) ? AllToolImage[toolNameKey] : urlImageTool;
                                        urlImageTool = (jsonHelper.pathExists("linkedImage." + criteria)) ? linkedImage[criteria] : urlImageTool;
                                        imgData.push({image: urlImageTool, text: criteria, color: "transparent", formId: formIdAndPath.formId, path: path});
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return imgData;
    },
    activityData : function(activity, btn, path){
        let activityChartData = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        activityChartData = jsonHelper.getValueByPath(activity["getSuplForm"], activity["answerId"] + getNavigatorElement.showActivity.activityAnswerPath);
        let activityData = activityChartData ? activityChartData.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + Object.values(element)[0].value)) ? linkedImage[Object.values(element)[0].value] : modules.co2.url+"/images/default_tool.png";
            return {text: Object.values(element)[0].value, color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }): [];
        return activityData;
    },
    principleData : function(principle, btn, path){
        let principleChartData = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        principleChartData = jsonHelper.getValueByPath(principle["getSuplForm"], principle["answerId"] + ".answers." + getNavigatorElement.showPrincipleAndValue.step + ".multiCheckboxPlus" + getNavigatorElement.showPrincipleAndValue.input);
        let principleData = principleChartData ? principleChartData.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + Object.values(element)[0].value)) ? linkedImage[Object.values(element)[0].value] : modules.co2.url+"/images/default_tool.png";
            return {text: Object.values(element)[0].value, color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }): [];
        return principleData;
    },
    equipementData: function(equipement, btn, path){
        let equipementPath = ".answers." + getNavigatorElement.showEquipment.step + ".multiCheckboxPlus" + getNavigatorElement.showEquipment.input;
        let equipementSuplAnswer = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        equipementSuplAnswer = jsonHelper.getValueByPath(equipement["getSuplForm"], equipement["answerId"] + equipementPath);
		let equipementData = equipementSuplAnswer ? equipementSuplAnswer.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }): [];
        return equipementData;		
    },
    domainData: function(domain, btn, path){
        let domainData = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        domainData = jsonHelper.getValueByPath(domain["getSuplForm"], domain["answerId"] + ".answers." + getNavigatorElement.showDomain.step + ".multiCheckboxPlus" + getNavigatorElement.showDomain.input);
		let domainDataList = domainData ? domainData.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }) : [];	
        return domainDataList;		
    },
    frequentationData: function(frequentation, btn, path){
        let frequentationSuplAnswer = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        let frequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhrhuhp0xv1qqamf0i";
        frequentationSuplAnswer = jsonHelper.getValueByPath(frequentation["getSuplForm"], frequentation["answerId"] + frequentationPath);
        let frequentationData = frequentationSuplAnswer ? frequentationSuplAnswer.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }) : [];	
        return frequentationData;	
    },
    ageFrequentationData: function(frequentation, btn, path){
        let ageFrequentationSuplAnswer = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        let ageFrequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq";
        ageFrequentationSuplAnswer = jsonHelper.getValueByPath(frequentation["getSuplForm"], frequentation["answerId"] + ageFrequentationPath);
        let ageFrequentationData = ageFrequentationSuplAnswer ? ageFrequentationSuplAnswer.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }) : [];
        return ageFrequentationData;
    },
    contratData: function(contrat, btn, path){
        let employeeContract = {};
        var formIdAndPath = extractFormIdAndPath(btn);
        employeeContract = jsonHelper.getValueByPath(contrat["getSuplForm"], contrat["answerId"] + ".answers." + getNavigatorElement.showEmployeesContract.step + ".multiCheckboxPlus" + getNavigatorElement.showEmployeesContract.input);
        let contratData = employeeContract ? employeeContract.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }) : [];
        return contratData;
    },
    gouvernanceData: function(gouvernance, btn, path){
        let gouvernanceSupplAnswer = [];
        var formIdAndPath = extractFormIdAndPath(btn);
        gouvernanceSupplAnswer = jsonHelper.getValueByPath(gouvernance["getSuplForm"], gouvernance["answerId"] + ".answers." + getNavigatorElement.showGouvernance.step + ".multiCheckboxPlus" + getNavigatorElement.showGouvernance.input);
        let gouvernanceData = gouvernanceSupplAnswer ? gouvernanceSupplAnswer.map((element) => {
            let urlImageTool = (jsonHelper.pathExists("linkedImage." + capitalizeFirstLetter(Object.values(element)[0].value))) ? linkedImage[capitalizeFirstLetter(Object.values(element)[0].value)] : modules.co2.url+"/images/default_tool.png";
            return {text: capitalizeFirstLetter(Object.values(element)[0].value), color: "transparent", image: urlImageTool, formId: formIdAndPath.formId, path: path};
        }) : [];
        return gouvernanceData;
    }
}

function capitalizeFirstLetter(val) {
    return String(val).charAt(0).toUpperCase() + String(val).slice(1);
}

function extractFormIdAndPath(htmlString){
    const elformidMatch = htmlString.match(/data-elformid="([^"]+)"/);
    const pathMatch = htmlString.match(/data-path="([^"]+)"/);
    return {
        formId : elformidMatch ? elformidMatch[1] : null,
        path : pathMatch ? pathMatch[1] : null
    }
}