costum[costum.slug].extendInit=function(){
    costum.checkUniquePersonEmail =function(email) {
        var response;
        extraParamsAjax={
            global: false,
            async: false
        }; //////// QUE FAIRE
        ajaxPost(null,
            baseUrl+"/"+moduleId+"/person/getcontactsbymails",
            {mailsList:[email]},
            function(res){
                mylog.log("res checkemail",res);
                response=res[email];
                mylog.log("response checkemail",response);
                if(response!==false && $("#ajaxFormModal #id").val()!==response.id){
                    if(typeof response.slug!="undefined"){
                        dyFObj.searchExist(response.slug,["citoyens"],null,"name");
                        var waitforContainer=setInterval(function(){
                            mylog.log("waitforContainer");
                            if($(".listSameName.name").is(":visible")){
                                clearInterval(waitforContainer);
                                $(".listSameName.name").children().first().text("Cliquez sur le nom du contact déjà existant :");
                            }
                            

                        },500);
                        // function modifyMessage(){
                        //     return  new Promise(resolve => {
                        //         try{
                        //             // dyFObj.searchExist(response.slug,["citoyens"],null,"name");
                        //             if($(".listSameName.name").is(":visible")){
                        //                 resolve();
                        //             }
                                    
                        //         }    
                        //         catch(err){
                                    
                        //         }
                        //     })
                        // }    
                        // async function searchPerson(){
                        //     dyFObj.searchExist(response.slug,["citoyens"],null,"name");
                        //     per=await modifyMessage();
                        //     $(".listSameName.name").children().first().text("Cliquez sur le nom du contact déjà existant :");

                        // } 
                        // searchPerson();
                        
                        
                    }else{
                        $("#ajaxFormModal #name").val(response.name);
                        $("#ajaxFormModal #name").trigger("blur");
                    }
                    response=true;
                }else{
                    response=false;
                }        
                mylog.log("isUniqueEmail=", response);
                
              
            },
            null,
            "json",
            extraParamsAjax
            );
        mylog.log("return response",!response);   
        return !response;    
        
    }
    jQuery.validator.addMethod("uniqueEmail", function(value, element) {
	    //Check unique username
	   	return costum.checkUniquePersonEmail(value);
	}, "Un utilisateur avec cette adresse email existe déjà.");
    costum.preventModalClosing=function(e){
        var dialog = bootbox.dialog({
            title: "<span class='text-white'>Vous êtes sur le point de quitter le formuaire</span>",
            message: "Êtes-vous sûr.e de vouloir quitter le formulaire et effacer l'ensemble des réponses saisies ?",
            closeButton:false,
            buttons: {
                cancel: {
                        label: trad.cancel,
                        className: 'btn-default',
                        callback: function(be){
                            // be.preventDefault();
                            // be.stopImmediatePropagation();
                        }
                },
                ok: {
                        label: trad.Quit,
                        className: 'btn-success',
                        callback: function(be){
                            // be.preventDefault();
                            // be.stopImmediatePropagation();
                            dyFObj.closeForm()

                            // $('#ajax-modal').trigger('hidden.bs.modal')

                        }
                }
            }
        });
    };

    costum.typeObj.persons.dynFormCostum.prepData=function(data){
        mylog.log("prepData dynform costum",data);
        // alert("here")
        // return data;
        thirdPlaceQuery={
            searchType:["organizations"],
            sourceKey : "franceTierslieux",
            
        };
        thirdPlaceQuery.filters={};
        thirdPlaceQuery.filters["links.members."+data.map.id]={'$exists' :true};
        var mapData=data;
        
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            thirdPlaceQuery,
            function(res){
                if(Object.keys(res.results).length>0){
                    mapData.map.linkedThirdPlace=res.results;
                }
                
            },
            null,
            null,
            {async:false}
            
        );
        return mapData;
        
        
    };
    costum.minimumContactForm={
        "name" : dyFInputs.name("person",{required:true}, null, "name"),
        "similarLink" : {
            inputType : "custom",
            html:"<div class='similarLink name' style=''><div class='listSameName name' style='font-size:12px; overflow-y: scroll; height:150px;border: 1px solid black;display:none;' ></div><a href='javascript:;' style='display:none;' class='btn btn-primary margin-top-5' onclick='document.getElementsByClassName(\"similarLink name\")[0].style.display=\"none\"; '>Cliquez ici si le contact recherché ne figure pas dans la liste ci-dessus</a></div>",
        },
        "email" : {
            "label" : "Email",
            "inputType" : "text",
            "placeholder" : "Addresse email",
            "rules" : {
                required : true,
                email:true,
                uniqueEmail : true
            }
        },
        "surname" : {
            "label" : "Nom",
            "inputType" : "text",
            // "rules" : {
            // 	"required" : true
            // }
        },
        "firstName" : {
            "label" : "Prénom",
            "inputType" : "text",
            // "rules" : {
            // 	"required" : true
            // }
        },
        // "otherEmail" : {
        //     "inputType" : "text",
        //     "label" : "E-mail (à jour)",
        //     "placeholder" : "Autre adresse email",
        //     "rules" : {
        //         "email" : true                                  
        //         }
        // },
        "emailCie" : {
            "inputType" : "text",
            "label" : "E-mail",
            "placeholder" : "Autre adresse email",
            "rules" : {
                "email" : true                                  
                }
        },
        // "mobile" : {
        //     "label" : "Téléphone",
        //     "inputType" : "text",
        //     "rules" : {
        //         "number" : true
        //     }
        // },
        "telephoneCie" : {
            "label" : "Téléphone",
            "inputType" : "text",
            "rules" : {
                "number" : true
            }
        },
        "formLocality" : {
            "label" : "Adresse Cie",
            "placeholder" : "Précisez l'adresse",
            "inputType" : "formLocality",
            // "rules" : {
            // 	"required" : true
            // }
        },
        "addressCie" : {
            "inputType" : "hidden"
        },
        "location" : {
            "inputType" : "custom",
            "html" : "<a href='javascript:;' class='w100p locationBtn btn btn-default'><i class='text-azure fa fa-map-marker fa-2x'></i> Localiser Cie </a>",
            "init" : function(initParam){
                mylog.log("initParam location",initParam);
                // alert("initParam");
                if(notEmpty(dyFObj.elementData) && dyFObj.elementData.map){
                    // alert("initParam if");
                    var formValues=dyFObj.elementData.map;
                    mylog.log("formvalue",formValues,"function addLocationToForm",dyFInputs.locationObj.addLocationToForm);
                    if( formValues.addressCie && formValues.geoCie && formValues.geoPositionCie ){
                        var initAddress = function(){
                            mylog.warn("init Adress location",formValues.addressCie.addressLocality,formValues.addressCie.postalCode);
                            dyFInputs.locationObj.copyMapForm2Dynform({address:formValues.addressCie,geo:formValues.geoCie,geoPosition:formValues.geoPositionCie});
                            
                            dyFInputs.locationObj.addLocationToForm({address:formValues.addressCie,geo:formValues.geoCie,geoPosition:formValues.geoPositionCie});
                        };
                    }
                }    
                    // if( formValues.addresses ){
                //     var initAddresses = function(){
                //         $.each(formValues.addresses, function(i,locationObj){
                //             mylog.warn("init extra addresses location ",locationObj.address.addressLocality,locationObj.address.postalCode);
                //             dyFInputs.locationObj.copyMapForm2Dynform(locationObj);
                //             dyFInputs.locationObj.addLocationToForm(locationObj, i);
                //         });
                //     };
                // }
                dyFObj.initFieldOnload["locationcustom"] = function(){
                    if(initAddress)
                        initAddress();
                    // if(initAddresses)
                    //     initAddresses();
                    dyFInputs.locationObj.init();
                }

            }
        }
       
			
    };
    costum.basicContactForm={
        "baseFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='border-top:1px solid black;margin-top:10px'><h3>Données socles</h3></div>"
        },
        "shortDescriptionCie" : {
            "label" : "Description du profil ",
            "inputType" : "textarea",
            "rules" : {
                "maxlength" : 500
            }
        },
        "status" : {
            "inputType" : "select",
            "label" : "Statut",
            "placeholder" : "Statut",
            "list" : "status",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        // "expertise" : {
        //     "inputType" : "tags",
        //     "label" : "Compétences",
        //     "placeholder" : "Sélectionnez les compétences",
        //     "tagsList" : "competence",
        //     "info": "Si autre, vous pouvez écrire directement"
        //     // "groupSelected" : false,
        //     // "optionsValueAsKey" : true,
        //     // "noOrder" : false,
        //     // "select2" : {
        //     //     "multiple" : true
        //     // }
        // },
        "expertise" : {
            "inputType" : "tags",
            "label" : "Domaines d'expertise",
            "placeholder" : "Domaines d'expertise",
            "tagsList" : "competence",
            "info": "Si autre, vous pouvez écrire directement"
            // "rules" : {
            // 	"required" : true
            // }
        },
        "linkedThirdPlace" : {
            "inputType" : "finder",
            "initMe" : false,
            "label" : "Tiers-lieu rattaché",
            "multiple" : false,
            "initType" : [ 
                "organizations"
            ],
            "filters" : {
                '$or' : {
                    "source.keys" : {'$in':["franceTierslieux",costum.slug]},
                "reference.costum" : {'$in':["franceTierslieux"]},
                "tags" : {'$in':["TiersLieux"]}
                }
                
            },
            "initContacts" : false,
            "initContext" : false,
            "initBySearch" : true,
            "openSearch" : true,
            "noResult": { 
                "label": "Créez un tiers-lieu",
                "action": function () {
                    // alert("action");
                    window.open(costum.url+'/#search');
                    // var contactValues=dyFObj.getFormData("#ajaxFormModal",dyFObj[dyFObj.activeElem].col, dyFObj[dyFObj.activeElem].ctrl);
                    // var customForm = {
                    //     // "beforeBuild" : {
                    //     //     "properties" : {
                                
                    //     //     }    
                    //     // }, 
                    //     "afterSave" : function(afterSaveData) {
                    //         // alert("aftersave");
                    //         uploadObj.afterLoadUploader=false;
                    //         dyFObj.commonAfterSave(afterSaveData, function(){
                    //             mylog.log("aftersave in finder",afterSaveData);
                    //             var elt=afterSaveData.map;
                    //             // finder.addInForm("linkedThirdPlace", elt._id.$id, elt.collection, elt.name, null, elt);
                    //             contactValues["linkedThirdPlace"]={};
                    //             contactValues["linkedThirdPlace"][elt._id.$id]={
                    //                 name : elt.name,
                    //                 type : elt.collection
                    //             };
                                
                    //             mylog.log("openform maj person values",contactValues);
                    //             dyFObj.onclose =function(){
                    //                 dyFObj.openForm("person","afterLoad",contactValues);
                    //                 // editEleme

                    //             }
                                
                                

                    //         });
                    //     }
                    // };
                    // var extendedForm=$.extend(costum.typeObj.organizations.dynFormCostum,customForm);
                    // extendedForm.onload.actions.hide.imageuploader=1;
                    // extendedForm.onload.actions.hide.photouploader=1
                    // // dyFObj.openForm("organization",null,null, null, {},) 
                    // // dyFObj.closeForm();
                    // dyFObj.openForm("organization",null,null, null, extendedForm,{"type":"bootbox"});
                    // $(".bootbox").modal('hide');
                }
            }
        },
        "functionThirdPlace" : {
            "inputType" : "select",
            "label" : "Fonction(s) occupée dans le tiers-lieu",
            "placeholder" : "",
            "list" : "function",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            }
        },
        "otherLinkedStructure" : {
            "inputType" : "finder",
            "initMe" : false,
            "label" : "Autre structure à laquelle la personne est attachée (en dehors d'un tiers-lieu)",
            "multiple" : false,
            "initType" : [ 
                "organizations"
            ],
            "initContacts" : false,
            "initContext" : false,
            "openSearch" : true
        },
        "otherTypeLinkedStructure" : {
            "inputType" : "select",
            "label" : "Quelle est la typologie de cette autre structure ?",
            "placeholder" : "",
            "list" : "typeLinkedStructure",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            }
        },
        "otherThemeLinkedStructure" : {
            "inputType" : "select",
            "label" : "Quelle est la thématique de cette autre structure ?",
            "placeholder" : "",
            "list" : "themeLinkedStructure",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            }
        },
        "functionOtherStructure" : {
            "inputType" : "select",
            "label" : "Fonction(s) dans cette autre structure",
            "placeholder" : "",
            "list" : "function",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            }
        },
        "adherent" : {
            "inputType" : "select",
            "label" : "Adhérent Compagnie",
            "placeholder" : "Adhérent Compagnie",
            "list" : "adherent",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        // "falicitationDevice" : {
        //     "inputType" : "select",
        //     "label" : "Dispositifs d'aide spécifique personne",
        //     "placeholder" : "Dispositifs d'aide spécifique personne",
        //     "list" : "falicitationDevice",
        //     "groupOptions" : false,
        //     "groupSelected" : false,
        //     "optionsValueAsKey" : true,
        //     "noOrder" : true,
        //     "select2" : {
        //         "multiple" : true
        //     },
        //     // "rules" : {
        //     // 	"required" : true
        //     // }
        // },
        "keyMoment" : {
            "inputType" : "select",
            "label" : "A participé à des temps forts du réseau",
            "placeholder" : "A participé à des temps forts du réseau",
            "list" : "keyMoment",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : true,
            "select2" : {
                "multiple" : true
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        "newsletterSubscription" : dyFInputs.checkboxSimple("false", "newsletterSubscription", 
                                                 {"onText" : trad.yes,
                                                  "offText": trad.no,
                                                  "onLabel" : "Inscrit(e) à la newsletter",
                                                  "offLabel": "Non inscrit(e) à la newsletter",
                                                  "labelText": "Accepte de recevoir la newsletter",
                                                  //"labelInInput": "Activer les amendements",
                                                  "labelInformation": "Inscription à la newsletter"
        })
    };
    costum.networkContactForm={
        "networkFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='border-top:1px solid black;margin-top:10px'><h3>Données réseau</h3></div>"
        },
        "supportFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='text-decoration:underline;margin-top:10px'><h5>Accompagnement (données réseau)</h5></div>"
        },
        "supportCercle" : dyFInputs.checkboxSimple("false", "supportCercle", 
                                                         {"onText" : trad.yes,
                                                          "offText": trad.no,
                                                          "onLabel" : "Membre du cercle des accompagnements",
                                                          "offLabel": "Non membre du cercle des accompagnements",
                                                          "labelText": "Membre du cercle des accompagnements",
                                                          //"labelInInput": "Activer les amendements",
                                                          "labelInformation": ""
        }),
        "supportedStructures" : {
            "inputType" : "finder",
            "initMe" : false,
            "initContacts" : false,
            "initContext" : false,
            "label" : "Si dans cercle, a accompagné quelles structures ?",
            "multiple" : true,
            "filters" : {
                "source.key":"franceTierslieux"
            },
            "initType" : [ 
                "organizations"
            ],
            "openSearch" : true
        }, 
        "contributionFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='text-decoration:underline;margin-top:10px'><h5>Contribution (données réseau)</h5></div>"
        },
        "actionContributor" : dyFInputs.checkboxSimple("false", "actionContributor", 
                                                         {"onText" : trad.yes,
                                                          "offText": trad.no,
                                                          "onLabel" : "Contributeurices aux actions du réseau HdF",
                                                          "offLabel": "Non contributeurices aux actions du réseau HdF",
                                                          "labelText": "Contributeurices aux actions du réseau HdF",
                                                          //"labelInInput": "Activer les amendements",
                                                          "labelInformation": "Contribution aux actions du réseau"
                }),
                "networkContributor" : {
                    "inputType" : "select",
                    "label" : "Etat de l'activité contributeurices du réseau HdF",
                    "placeholder" : "Actif, en sommeil, potentiel",
                    "list" : "networkContributor",
                    "groupOptions" : false,
                    "groupSelected" : false,
                    "optionsValueAsKey" : true,
                    "noOrder" : false,
                    "select2" : {
                        "multiple" : true
                    },
                    // "rules" : {
                    // 	"required" : true
                    // }
                },
                "mentor" : {
                    "inputType" : "finder",
                    "label" : "Si contributeur, qui est son parrain ou sa marraine au sein de la Compagnie",
                    "initMe" : false,
                    "multiple" : true,
                    "initType" : [ 
                        "citoyens"
                    ],
                    "openSearch" : true
                },
                "heldAction" : {
                    "inputType" : "select",
                    "label" : "Si contributeur, projet/action ou activité porté au sein du réseau ?",
                    "placeholder" : "Si contributeur, projet/action ou activité porté au sein du réseau ?",
                    "list" : "heldAction",
                    "groupOptions" : false,
                    "groupSelected" : false,
                    "optionsValueAsKey" : true,
                    "noOrder" : false,
                    "select2" : {
                        "multiple" : true
                    },
                    // "rules" : {
                    // 	"required" : true
                    // }
                },
                "territorialCercle" : {
                    "inputType" : "select",
                    "label" : "Si contributeur, membre d'un cercle territoriale ?",
                    "placeholder" : "Si contributeur, membre d'un cercle territoriale ?",
                    "list" : "territorialCercle",
                    "groupOptions" : false,
                    "groupSelected" : false,
                    "optionsValueAsKey" : true,
                    "noOrder" : false,
                    "select2" : {
                        "multiple" : false
                    },
                    // "rules" : {
                    // 	"required" : true
                    // }
                },
                "referentTerritorialCercle" : dyFInputs.checkboxSimple("false", "referentTerritorialCercle", 
                                                         {"onText" : trad.yes,
                                                          "offText": trad.no,
                                                          "onLabel" : "Réferent(e) cercle territorial",
                                                          "offLabel": "Non réferent(e) cercle territorial",
                                                          "labelText": "Si contributeur, si membre d'un cercle territorial, référent cercle territorial",
                                                          //"labelInInput": "Activer les amendements",
                                                          "labelInformation": ""
                }),
                "trainer" : dyFInputs.checkboxSimple("false", "trainer", 
                {"onText" : trad.yes,
                 "offText": trad.no,
                 "onLabel" : "Formateurice",
                 "offLabel": "Non formateurice",
                 "labelText": "Formateurice",
                 //"labelInInput": "Activer les amendements",
                 "labelInformation": ""
}),         
        "monitoringFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='text-decoration:underline;margin-top:10px'><h5>Suivi (données réseau)</h5></div>"
        },
        "reportingMember" : {
            "inputType" : "finder",
            "initMe" : false,
            "label" : "Suivi par qui ?",
            "multiple" : true,
            "initType" : [ 
                "citoyens"
            ],
            "openSearch" : true
        },
        "monitoringLatestExchange" : {
            "label" : "Date des derniers échanges",
            "inputType" : "date"
        },
        "monitoringSupportingDocumentation" : {
            "label" : "Les ressources du collectif/du lieu sur lesquelles s'appuyer",
            "placeholder" : "Urls des documentations commençant par (https://)",
            "inputType" : "array"
        },
        "monitoringWarmInfo" : {
            "label" : "Infos chaudes (les questions, les problèmes, les choses sur lesquels la Cie doit intervenir)",
            "inputType" : "textarea",
            "markdown" : true
        },
        "monitoringColdInfo" : {
            "label" : "Infos froides (les projets en cours, les relations, les infos...)",
            "inputType" : "textarea",
            "markdown" : true
        }
    };    
    costum.trainingContactForm={
        "networkFormTitle" : {
            inputType : "custom",
            html : "<div class='text-center' style='border-top:1px solid black;margin-top:10px'><h3>Données formation</h3></div>"
        },
        "attendedTraining" : {
            "inputType" : "select",
            "label" : "Participe ou a participé à une formation, lesquelles",
            "placeholder" : "Participe ou a participé à une formation, lesquelles",
            "list" : "attendedTraining",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : true
            },
            // "rules" : {
            // 	"required" : true
            // }
        }, 
        "interestingTraining" : {
            "inputType" : "select",
            "label" : "Intéressé(e) par des formations ? lesquelles ?",
            "placeholder" : "Intéressé(e) par des formations ? lesquelles ?",
            "list" : "interestingTraining",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : true
            }
            // "rules" : {
            // 	"required" : true
            // }
        },
        "presentationSession" : {
            "inputType" : "select",
            "label" : "a participé à une visio de présentation",
            "placeholder" : "Sélectionner les sessions",
            "list" : "presentationSession",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : true
            }
            // "rules" : {
            // 	"required" : true
            // }
        },
        "ptlConsideredTraining" : {
            "inputType" : "select",
            "label" : "Session envisagée (Formation \"Piloter un tiers-lieu\")",
            "placeholder" : "Session envisagée (Formation \"Piloter un tiers-lieu\")",
            "list" : "ptlConsideredTraining",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : false
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        "ptlProfileCandidate" : {
            "label" : "Infos/profil du candidat (Formation \"Piloter un tiers-lieu\")",
            "inputType" : "textarea",
            "markdown" : true
        },
        "ptlSentQuote" : dyFInputs.checkboxSimple("false", "ptlSentQuote", 
                                                 {"onText" : trad.yes,
                                                  "offText": trad.no,
                                                  "onLabel" : "Devis envoyé (Formation \"Piloter un tiers-lieu\")",
                                                  "offLabel": "Devis non envoyé (Formation \"Piloter un tiers-lieu\")",
                                                  "labelText": "Devis envoyé (Formation \"Piloter un tiers-lieu\")",
                                                  //"labelInInput": "Activer les amendements",
                                                  "labelInformation": "Devis formation \"Piloter un tiers-lieu\""
        }),
        "ptlComment" : {
            "inputType" : "textarea",
            "label" : "Commentaire (Formation \"Piloter un tiers-lieu\")",
            "markdown" : true
        },
        "ptlPlannedFunding" : {
            "inputType" : "textarea",
            "label" : "Financement envisagé (Formation \"Piloter un tiers-lieu\")",
            "markdown" : true
        },
        "ptlTrainingState" : {
            "inputType" : "select",
            "label" : "Etat (inscrit, abandon, financé, en attente de financement...) (Formation \"Piloter un tiers-lieu\")",
            "placeholder" : "Etat (inscrit, abandon, financé, en attente de financement...) (Formation \"Piloter un tiers-lieu\")",
            "list" : "ptlTrainingState",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : false
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        "consideredTraining" : {
            "inputType" : "select",
            "label" : "Session envisagée",
            "placeholder" : "Session envisagée",
            "list" : "consideredTraining",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : false
            },
            // "rules" : {
            // 	"required" : true
            // }
        },
        "profileCandidate" : {
            "label" : "Infos/profil du candidat",
            "inputType" : "textarea",
            "markdown" : true
        },
        "sentQuote" : dyFInputs.checkboxSimple("false", "sentQuote", 
                                                 {"onText" : trad.yes,
                                                  "offText": trad.no,
                                                  "onLabel" : "Devis envoyé",
                                                  "offLabel": "Devis non envoyé",
                                                  "labelText": "Devis envoyé autre formation",
                                                  //"labelInInput": "Activer les amendements",
                                                  "labelInformation": "Devis formation"
        }),
        "comment" : {
            "inputType" : "textarea",
            "label" : "Commentaire",
            "markdown" : true
        },
        "plannedFunding" : {
            "inputType" : "textarea",
            "label" : "Financement envisagé",
            "markdown" : true
        },
        "trainingState" : {
            "inputType" : "select",
            "label" : "Etat (inscrit, abandon, financé, en attente de financement...)",
            "placeholder" : "Etat (inscrit, abandon, financé, en attente de financement...)",
            "list" : "trainingState",
            "groupOptions" : false,
            "groupSelected" : false,
            "optionsValueAsKey" : true,
            "noOrder" : false,
            "select2" : {
                "multiple" : false
            }
            // "rules" : {
            // 	"required" : true

            // }
        }
    }    
   
    typeObj.person.formType="person";
    typeObj.person.color="pink";
    typeObj.person.bgClass="bg-pink";
    typeObj.person.createLabel="Ajouter un contact";
    typeObj.person.dynForm={
        title : "Ajouter un contact",
        icon : "user",
        type : "object",
        col : "citoyens",
        jsonSchema : {
            "properties" : {
            },			
            beforeBuild : function(){
                // costum.typeObj.persons.dynFormCostum.beforeBuild.properties.linkedThirdPlace.filters={};
                // costum.typeObj.persons.dynFormCostum.beforeBuild.properties.linkedThirdPlace.filters["links.members."+ dyFObj.elementData.map.id ]={'$exists':1};
                // uploadObj.gotoUrl = '#page.type.citoyens.id.'+uploadObj.id;
            },
            mapping : function(formatData){
                mylog.log("mapping data dynF start",formatData);
                
                if(typeof formatData.tags == "undefined" || !notEmpty(formatData.tags)){
                    formatData.tags=[];
                } 
                formatData.tagsCie=formatData.tags;
                delete formatData.tags;


                $.each(dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties,function(field, values){
                    if(typeof formatData[field]!="undefined" && values.inputType=="checkboxSimple"){
                        if(formatData[field]=="true"){
                            formatData.tagsCie.push(values.params.onLabel);
                        }else{
                            formatData.tagsCie.push(values.params.offLabel);
                        }
                    }

                    // ajoute le tag de costum.lists si une valeur est entrée à la main
                    if(notEmpty(formatData[field]) && values.inputType=="tags" ){
                        if(notEmpty(costum.lists[field])){
                            var valuesTagsField=formatData[field].split(",");
                            formData[field]=valuesTagsField;
                            if(costum.isCostumAdmin){
                                var newTags=[];
                                $.each(valuesTagsField,function(ind,tag){
                                    if(costum.lists[field].indexOf(tag)<0){
                                        newTags.push(tag);
                                    }
                                })
                                if(notEmpty(newTags)){
                                    var paramsTag={
                                        id:costum.contextId,
                                        collection:costum.contextType,
                                        path : "costum.lists."+field,
                                        value : {
                                            '$each' : newTags
                                        },
                                        arrayForm:true
                                     };
                                     dataHelper.path2Value( paramsTag, function(params) {
                                        
                                        dyFObj.onclose=function(){
                                            ajaxPost(
                                                null,
                                                baseUrl+"/"+moduleId+"/cms/resetcache",
                                                null,
                                                function(data){
                                                    window.location.reload();
                                                },
                                                null,
                                                null,
                                                {async:false}
                                            );
                                            // alert("ok");
                                            // location.hash=location.hash+"?text="+afterSaveD.map.name;
                                            
                                        }      
                                    },false);
                                }
                            }
                        }
                    }
                });
                if(formatData.tagsCie.indexOf("Contact"+costum.slug)==-1){
                    formatData.tagsCie.push("Contact"+costum.slug);
                }

                // formatData.tagsCie=formatData.tags;
                // delete formatData.tags;
                // //formatData.tags to DELETE;

                // Set address Cie instead of personal address 
                if(formatData.id!==userId){
                    if(notEmpty(formatData.address)){
                        formatData.addressCie=formatData.address;
                        delete formatData.address;
                    }
                    if(notEmpty(formatData.geoPosition)){
                        formatData.geoPositionCie=formatData.geoPosition;
                        delete formatData.geoPosition;
                    }
                    if(notEmpty(formatData.geo)){
                        formatData.geoCie=formatData.geo;
                        delete formatData.geo;
                    }
                }
                
                var mappingData=$.extend({},formatData);
                
                mylog.log("beforeSave data dynF return",mappingData);
                return mappingData;
            },
            onLoads : {
                onload : function(data) {
                    $(document).on('keydown', function(event){
                        mylog.log("keydown escape",event,event.originalEvent["key"],$("#ajax-modal.portfolio-modal.modal.fade.in").length,$(dyFObj.activeModal).attr("class") );
                        if (event.originalEvent["key"]=="Escape")  {
                            // event.preventDefault();
                            event.stopImmediatePropagation();
                           
                            
                              if($(dyFObj.activeModal).css("opacity") =="1"){
                                // e.preventDefault();
                                // alert("ok");
                               
                                costum.preventModalClosing();
                              }
                        }
                      });
                    
                    
                    // $('#ajax-modal').on('hide.bs.modal',function(e){
                    //     mylog.log("hide bs",e);
                    //     e.preventDefault();
                    //     costum.preventModalClosing(e);
                        
                    //     // e.stopImmediatePropagation();
                        
                        
                    // });
                    
                    // dyFObj.onclose = function() {
                    //     // e.preventDefault();
                    //     costum.preventModalClosing();
                    // };
                    mylog.log("onload",data);
                    $("#ajax-modal-modal-title").html("<i class='fa fa-user'></i> Ajouter un contact dans la BDD");

                    // if(typeof data.otherEmail=="undefined" || data.otherEmail==''){
                    //     $("#ajaxFormModal #otherEmail").val($("#ajaxFormModal #email").val())
                    // }
                    if(typeof data.emailCie=="undefined" || data.emailCie==''){
                        $("#ajaxFormModal #emailCie").val($("#ajaxFormModal #email").val())
                    }
                    if(typeof data.telephoneCie=="undefined" || data.telephoneCie==''){
                        if(notEmpty(data.mobile)){
                            $("#ajaxFormModal #telephoneCie").val(data.mobile);

                        }
                       
                    }

                    if(!notEmpty(data)){
                        $("#ajaxFormModal .emailCietext").hide();
                        // $("#ajaxFormModal .otherEmailtext").hide();
                        

                    }else{
                        $("#ajaxFormModal .nametext").hide();
                        $("#ajaxFormModal .emailtext").hide();
                        // $("#ajaxFormModal .otherEmailtext").hide();
                        $("#ajaxFormModal .mobiletext").hide();
                        $("#ajaxFormModal .similarLinkcustom").hide();
                    }

                    
                    // $("#ajaxFormModal .otherEmailtext").hide();
                // },
                // afterLoad : function(data){
                    // alert("afterload");
                    // $()
                    // var esc = $.Event("keydown", { keyCode: 27 });
                    // $("#ajaxFormModal").trigger(esc)
                    mylog.log("generic afterload Cie des TL",data);
                    // alert("afterLoad");
                    // $("#ajaxFormModal .surnametext label").append(" (modifier si nécessaire)");
                    // $("#ajaxFormModal .firstNametext label").append(" (modifier si nécessaire)");
                    // $("#ajaxFormModal .emailtext label").append(" (saisi à l'inscription)");
                   

                    if(typeof data.otherLinkedStructure=="undefined" || Object.keys(data.otherLinkedStructure).length==0){
                        // alert("emptyfinder");
                        $("#ajaxFormModal .otherTypeLinkedStructureselect").hide();
                        $("#ajaxFormModal .otherThemeLinkedStructureselect").hide();
                        $("#ajaxFormModal .functionOtherStructureselect").hide();
                    }
                    finder.callback.otherLinkedStructure=function(){
                        // alert('callback');
                        $("#ajaxFormModal .otherTypeLinkedStructureselect").show();
                        $("#ajaxFormModal .otherThemeLinkedStructureselect").show();
                        $("#ajaxFormModal .functionOtherStructureselect").show();
                        
                    };
                    if(typeof data.linkedThirdPlace=="undefined" || Object.keys(data.linkedThirdPlace).length==0){
                        $("#ajaxFormModal .functionThirdPlaceselect").hide();
                    }
                    finder.callback.linkedThirdPlace=function(){
                        $("#ajaxFormModal .functionThirdPlaceselect").show();
                        
                    };
                    
                
                    mylog.log("afterload FTL data", data);
                    $("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
                    $("#divMapLocality").addClass("hidden");
                    
                    if(Object.keys(data).length==0 || typeof data.addressCie=="undefined" || data.addressCie==null || (typeof data.addressCie!="undefined" && Object.keys(data.addressCie).length==0) ){
                        mylog.log("formInMap newAddress afterload");
                        dyFObj.formInMap.newAddress(true);
                    }else if(typeof data.addressCie!="undefined" && Object.keys(data.addressCie).length>0){
                        $('#ajaxFormModal #divNewAddress').hide();
                    }
    
                    // if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
                    // 	dyFObj.formInMap.newAddress(false);
                    // 	$('#ajaxFormModal #divNewAddress').hide();
                    // }else{
                    // 	dyFObj.formInMap.newAddress(true);
                    // }
                    dyFObj.formInMap.resumeLocality=function(cancel){
                        mylog.log("formInMap resumeLocality", cancel);
                        if(dyFObj.formInMap.NE_street != ""){
                            $('#ajaxFormModal #street_sumery_value').html(dyFObj.formInMap.NE_street );
                            $('#ajaxFormModal #street_sumery').removeClass("hidden");
                        }else
                            $('#ajaxFormModal #street_sumery').addClass("hidden");
            
                        if(dyFObj.formInMap.NE_cp != ""){
                            $('#ajaxFormModal #cp_sumery_value').html(dyFObj.formInMap.NE_cp );
                            $('#ajaxFormModal #cp_sumery').removeClass("hidden");
                        }else
                            $('#ajaxFormModal #cp_sumery').addClass("hidden");
            
                        if(dyFObj.formInMap.NE_city != ""){
                            $('#ajaxFormModal #city_sumery_value').html(dyFObj.formInMap.NE_city);
                            $('#ajaxFormModal #city_sumery').removeClass("hidden");
                        }else
                            $('#ajaxFormModal #city_sumery').addClass("hidden");
            
                        if(dyFObj.formInMap.NE_country != ""){
                            $('#ajaxFormModal #country_sumery_value').html(dyFObj.formInMap.NE_country);
                            $('#ajaxFormModal #country_sumery').removeClass("hidden");
                        }else
                            $('#ajaxFormModal #country_sumery').addClass("hidden");
            
            
                        if(dyFObj.formInMap.NE_country != "" && dyFObj.formInMap.NE_city != ""){
                            //$("#btnValideAddress").prop('disabled', false);
                            $("#ajaxFormModal #btnValideAddress").show();
                            // toastr.error(trad.pleaseConfirmAddress);
                        }else{
                            //$("#btnValideAddress").prop('disabled', true);
                            $("#ajaxFormModal #btnValideAddress").hide();
                        }
                    };
                    dyFInputs.locationObj.addLocationToForm= function (locObj, index){
                        mylog.warn("---------------addLocationToForm----------------", locObj, index);
                        mylog.dir(locObj);
                        var strHTML = "";
                        if( locObj.address.addressCountry)
                            strHTML += locObj.address.addressCountry;
                        if( locObj.address.postalCode)
                            strHTML += ", "+locObj.address.postalCode;
                        if( locObj.address.addressLocality)
                            strHTML += ", "+locObj.address.addressLocality;
                        if( locObj.address.streetAddress)
                            strHTML += ", "+locObj.address.streetAddress;
                        var btnSuccess = "";
                        var locCenter = "";
                        var boolCenter=false;
                        if( dyFInputs.locationObj.countLocation == 0){
                            btnSuccess = "btn-success";
                            //locCenter = "<span class='lblcentre'>(localité centrale)</span>";
                            locCenter = "<span class='lblcentre'> "+tradDynForm.mainLocality+"</span>";
                            boolCenter=true;
                        }
            
                        /*if(typeof index != "undefined"){
                            strHTML = "<a href='javascript:;' class='deleteLocDynForm locationEl"+dyFInputs.locationObj.countLocation+" btn' data-index='"+index+"' data-indexLoc='"+dyFInputs.locationObj.countLocation+"'>"+
                                            "<i class='text-red fa fa-times'></i></a>"+
                                          "<span class='locationEl"+dyFInputs.locationObj.countLocation+" locel text-azure'>"+strHTML+"</span> "+
                                  "<a href='javascript:dyFInputs.locationObj.setAsCenter("+dyFInputs.locationObj.countLocation+")' data-index='"+index+"' class='centers center"+dyFInputs.locationObj.countLocation+" locationEl"+dyFInputs.locationObj.countLocation+" btn btn-xs "+btnSuccess+"'>"+
                                      "<i class='fa fa-map-marker'></i>"+locCenter+"</a> <br/>";
                        }
                        else{
                            strHTML = "<a href='javascript:dyFInputs.locationObj.removeLocation("+dyFInputs.locationObj.countLocation+")' class=' locationEl"+dyFInputs.locationObj.countLocation+" btn'> <i class='text-red fa fa-times'></i></a>"+
                                  "<span class='locationEl"+dyFInputs.locationObj.countLocation+" locel text-azure'>"+strHTML+"</span> "+
                                  "<a href='javascript:dyFInputs.locationObj.setAsCenter("+dyFInputs.locationObj.countLocation+")' class='centers center"+dyFInputs.locationObj.countLocation+" locationEl"+dyFInputs.locationObj.countLocation+" btn btn-xs "+btnSuccess+"'> <i class='fa fa-map-marker'></i>"+locCenter+"</a> <br/>";
                        }*/
                        if(typeof index != "undefined" && dyFObj.editMode){
                            mylog.log("---------------addLocationToForm---------------- IF", index);
                            strHTML =
                                "<div class='col-xs-12 text-left shadow2 padding-15 margin-top-15 margin-bottom-15'>" +
                                  "<span class='pull-left locationEl"+dyFInputs.locationObj.countLocation+" locel text-red bold'>"+
                                    "<i class='fa fa-home fa-2x'></i> "+
                                    strHTML+
                                  "</span> "+
            
                                  "<a href='javascript:echo;' data-index='"+index+"' data-indexLoc='"+dyFInputs.locationObj.countLocation+"' "+
                                    "class='deleteLocDynForm locationEl"+dyFInputs.locationObj.countLocation+" btn btn-sm btn-danger pull-right'> "+
                                    "<i class='fa fa-times'></i> "+tradDynForm.clear+
                                  "</a>"+
            
                                  "<a href='javascript:dyFInputs.locationObj.setAsCenter("+dyFInputs.locationObj.countLocation+")' data-index='"+index+"'"+
                                    "class='margin-right-5 centers pull-right center"+dyFInputs.locationObj.countLocation+" locationEl"+dyFInputs.locationObj.countLocation+" btn btn-sm "+btnSuccess+"'> "+
                                    "<i class='fa fa-map-marker'></i> "+locCenter+
                                  "</a>" +
            
                                "</div>";
                        } else {
                            mylog.log("---------------addLocationToForm---------------- ESLE", index);
                            strHTML =
                                "<div class='col-xs-12 text-left shadow2 padding-15 margin-top-15 margin-bottom-15'>" +
                                  "<span class='pull-left locationEl"+dyFInputs.locationObj.countLocation+" locel text-red bold'>"+
                                    "<i class='fa fa-home fa-2x'></i> "+
                                    strHTML+
                                  "</span> "+
            
                                  "<a href='javascript:dyFInputs.locationObj.removeLocation("+dyFInputs.locationObj.countLocation+", "+boolCenter+")' "+
                                    "class='removeLocalityBtn locationEl"+dyFInputs.locationObj.countLocation+" btn btn-sm btn-danger pull-right'> "+
                                    "<i class='fa fa-times'></i> "+tradDynForm.clear+
                                  "</a>"+
            
                                  "<a href='javascript:dyFInputs.locationObj.setAsCenter("+dyFInputs.locationObj.countLocation+")' "+
                                    "class='setAsCenterLocalityBtn margin-right-5 centers pull-right center"+dyFInputs.locationObj.countLocation+" locationEl"+dyFInputs.locationObj.countLocation+" btn btn-sm "+btnSuccess+"'> "+
                                    "<i class='fa fa-map-marker'></i> "+locCenter+
                                  "</a>" +
            
                                "</div>";
                        }
                          $(".locationcustom").append(strHTML);
            
            
                        // strHTML = "<a href='javascript:removeLocation("+dyFInputs.locationObj.countLocation+", "+true+")'' class=' locationEl"+dyFInputs.locationObj.countLocation+" btn'> <i class='text-red fa fa-times'></i></a>"+
                        // 		  "<span class='locationEl"+dyFInputs.locationObj.countLocation+" locel text-azure'>"+strHTML+"</span> "+
                        // 		  "<a href='javascript:dyFInputs.locationObj.setAsCenter("+dyFInputs.locationObj.countLocation+")' class='centers center"+dyFInputs.locationObj.countLocation+" locationEl"+dyFInputs.locationObj.countLocation+" btn btn-xs "+btnSuccess+"'> <i class='fa fa-map-marker'></i>"+locCenter+"</a> <br/>";
            
                        $(".postalcodepostalcode").prepend(strHTML);
            
                        mylog.log("strAddres", strHTML);
                        //$(".locationlocation").prepend(strHTML);
                        dyFInputs.locationObj.countLocation++;
                    };
    
                    dyFObj.formInMap.bindStreetResultsEvent = function(){	
                        
                        
                        dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
                            mylog.warn("---------------copyMapForm2Dynform afterload----------------");
                            mylog.log("locationObj", locObj);
                            dyFInputs.locationObj.elementLocation = locObj;
                            mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                            // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                            dyFInputs.locationObj.elementLocations=[];
                            dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
                            mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
                            mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
                            if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
                                dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
                                dyFInputs.locationObj.elementLocation.center = true;
                            }
                            mylog.dir(dyFInputs.locationObj.elementLocations);
                        };
                        dyFObj.formInMap.valideLocality = function(country){
                            mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));
    
                            if(notEmpty(dyFObj.formInMap.NE_lat)){
                                locObj = dyFObj.formInMap.createLocalityObj();
                                mylog.log("formInMap copyMapForm2Dynform", locObj);
                                dyFInputs.locationObj.copyMapForm2Dynform(locObj);
    
                            }
                            // Div récapitulatif de l'adresse 
                            dyFObj.formInMap.resumeLocality();
                            // Cache le bouton de validation d'adresse
                            dyFObj.formInMap.btnValideDisable(false);
                            toastr.success("Adresse enregistrée");
                        
                            dyFObj.formInMap.initHtml();
                            $("#btn-submit-form").prop('disabled', false);
                        };
    
                        // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
                        $("#ajaxFormModal .item-street-found").off().click(function(){
                            if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
                                dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
                                setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
                                $("#divMapLocality").removeClass("hidden");
                                $("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
                            }
                            var streetAddressName= $(this).text().split(",")[0].trim();
        
                            $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
        
                            dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
        
                            mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
                            $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
                            $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
                            $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
        
    
                            dyFObj.formInMap.NE_lat = $(this).data("lat");
                            dyFObj.formInMap.NE_lng = $(this).data("lng");
                            dyFObj.formInMap.showWarningGeo(false);
    
                            //Valider au click du résultat
                            dyFObj.formInMap.valideLocality();
        
                            $("#ajaxFormModal #sumery").show();
    
        
                            dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
                                var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
                                mylog.log('marker drop event', latLonMarker);
                                dyFObj.formInMap.NE_lat = latLonMarker.lat;
                                dyFObj.formInMap.NE_lng = latLonMarker.lng;
            
                                dyFObj.formInMap.valideLocality();
                                toastr.success("Coordonnées mises à jour");
                            });
                        });
                    };
                }
            },
            afterSave : function(data){
                mylog.log("aftersave person data",data);
                // var statusMap =(typeof data.map.status !="undefined") ? data.map.status : [] ;
                // var functionMap=(typeof data.map.function !="undefined") ? data.map.function : [] ;
                // var adherentMap=(typeof data.map.adherent !="undefined") ? data.map.adherent : [] ;
                // var newsSubscriberMap=(typeof data.map.newsletterSubscription !="undefined" && data.map.newsletterSubscription=="true") ? ["Newsletter"] : [] ;
                // var supportCercle=(typeof data.map.supportCercle !="undefined" && data.map.supportCercle=="true") ? ["Cercle des accompagnements"] : [] ;
                // var actionContributor=(typeof data.map.actionContributor !="undefined" && data.map.actionContributor=="true") ? ["Contributeur.ices"] : [] ;
                // var referentTerritorial=(typeof data.map.referentTerritorial !="undefined" && data.map.referentTerritorial=="true") ? ["Référent.e teritorial"] : [] ;
                // var trainer=(typeof data.map.referentTerritorial !="undefined" && data.map.referentTerritorial=="true") ? ["Référent.e teritorial"] : [] ;
                // var territorialCercle=(typeof data.map.territorialCercle !="undefined") ? data.map.territorialCercle : [] ;
                // var roles2Attribute=[...statusMap,...functionMap,...adherentMap,...newsSubscriberMap, ...supportCercle];
                // mylog.log("aftersave person roles",roles2Attribute);
                // var roles=[];
                // if(notEmpty(data.map.tags)){
                //     $.each(data.map.tags,function(i,t){
                //         if(roles2Attribute.indexOf(t)>-1){
                //             roles.push(t);
                //         }
                //     })                     
                // }
                // if(notEmpty(roles2Attribute)){
                //     if(typeof data.map.links!="undefined" && typeof data.map.links.memberOf!="undefined" 
                //     && typeof data.map.links.memberOf[costum.contextId]!="undefined"){
                //         ajaxPost(
                //             null,
                //             baseUrl+"/"+moduleId+"/link/removerole",
                //             {
                //                 contextId : costum.contextId,
                //                 contextType : costum.contextType,
                //                 childId : data.id,
                //                 childType : data.map.collection,
                //                 roles : roles2Attribute,
                //                 connectType : "members"
                //             },
                //             function(data){

                //             }
                //         );
                //     }else{
                //         var afterConnectCallback=function(){};
                //         links.connectAjax(costum.contextType, costum.contextId, data.id, data.map.collection, "members", roles, afterConnectCallback);
                //     }	
                // }
                var urlReload=(location.hash.indexOf("?text=")<0) ? location.hash+"?text="+data.map.name : location.hash.replace(/\?text\=.*$/,"?text="+data.map.name);
                urlCtrl.loadByHash(urlReload);	
                dyFObj.closeForm();
      
            }
        }
    };	

    costum.fullContactForm=$.extend(true,{},costum.minimumContactForm,costum.basicContactForm,costum.networkContactForm);
    typeObj.person.dynForm.jsonSchema.properties=costum.fullContactForm;
    
    $.extend(true,typeObj.person.dynForm.jsonSchema.properties,costum.typeObj.persons.dynFormCostum.beforeBuild.properties);
    
    
    typeObj.person.formatData = function(constructData){
        mylog.log("constructData editElement",constructData);
        var data=constructData;
        var tagsCie=(notEmpty(data.tagsCie)) ? data.tagsCie : ((notEmpty(data.tags)) ? data.tags : null);
        if(notEmpty(tagsCie)){
            $.each(dyFObj[dyFObj.activeElem].dynForm.jsonSchema.properties,function(field, values){
                if(values.inputType=="checkboxSimple"){
                    if(tagsCie.indexOf(values.params.onLabel)>-1){
                        data[field]="true";
                    }else if(values.params.offLabel){
                        data[field]="false";
                    }
                }
            });
        }
        return data;
    };
    costum.name={
        searchExist : function(type,id,name,slug,email){
            // dyFObj.closeForm();
            var ctrlType=dyFInputs.get(type).ctrl;
            var editDynF=jQuery.extend(true, {},typeObj[ctrlType].dynForm.jsonSchema);
            mylog.log("editDynF",editDynF);
            // editDynF.onload = {
            //     actions : {
            //         hide : {
            //             "otherEmailtext" : 0,
            //             "emailCietext" : 0,
            //             "similarLinkcustom" : 1
            //         },
            //         setTitle : "<i class='fa fa-user'></i> Ajouter un contact"
            //     }
            // };
                    // $("#ajax-modal-modal-title").html("<i class='fa fa-user'></i> Ajouter un contact");
                    // $("#ajaxFormModal .otherEmailtext").show();
                    // $("#ajaxFormModal .similarLinkcustom").hide();
            
            
            
            editDynF.afterBuild = function(data){
                $("#ajaxFormModal #email, #ajaxFormModal #name").prop("disabled",true);
                // $("#ajaxFormModal .surnametext label").append(" (modifier si nécessaire)");
                // $("#ajaxFormModal #surname").val(data.name);
                // $("#ajaxFormModal #firstName").val(data.name);
                // $("#ajaxFormModal .firstNametext label").append(" (modifier si nécessaire)");
                // $("#ajaxFormModal .emailtext label").append(" (saisi à l'inscription)");
                // $("#ajaxFormModal #email","#ajaxFormModal #name").prop("disabled",true);
                // if($("#ajaxFormModal .otherEmailtext").val()==''){
                //     $("#ajaxFormModal .otherEmailtext").val($("#ajaxFormModal .emailtext").val())

                // }
                $("#ajax-modal").addClass("in");
                // if(typeof data.otherEmail=="undefined" || data.otherEmail==''){
                //     $("#ajaxFormModal #otherEmail").val($("#ajaxFormModal #email").val())
                // }
                // if(typeof data.emailCie=="undefined" || data.emailCie==''){
                //     $("#ajaxFormModal #emailCie").val($("#ajaxFormModal #email").val())
                // }
                // if(typeof data.telephoneCie=="undefined" || data.telephoneCie==''){
                //     $("#ajaxFormModal #telephoneCie").val($("#ajaxFormModal #mobile").val())
                // }

                // $("#ajaxFormModal #email").hide();
                // $("#ajaxFormModal .otherEmailtext").hide();
                // $("#ajaxFormModal #mobile").hide();
                // $("#ajaxFormModal .similarLinkcustom").hide();
            };
            // dyFObj.closeForm();
            dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");
            
        }	
    };

    // Register form : 
    // Login.runEmailValidationValidator = function() {
	// 	var form2 = $('.form-email-activation');
	// 	var errorHandler2 = $('.errorHandler', form2);
	// 	var sendValidateEmailBtn = null;
    //     if($("#surname").length==0){
	// 		$("#modalSendActivation .form-actions").before(`
	// 	       <input type="hidden" class="form-control" id="callbackRegister" name="callbackRegister">
	// 			<input type="checkbox" class="form-control hide" id="sessionRegister" name="sessionRegister">
	// 		`);
	// 	Ladda.bind('.sendValidateEmailBtn', {
	//         callback: function (instance) {
	//             sendValidateEmailBtn = instance;
	//         }
	//     });
	// 	form2.validate({
	// 		rules : {
	// 			email2 : {
	// 				required : true
	// 			}
	// 		},
	// 		submitHandler : function(form) {
	// 			errorHandler2.hide();
	// 			sendValidateEmailBtn.start();
	// 			var params = { 
	// 				"email" : $("#modalSendActivation #email2").val(),
	// 				"type"	: "validateEmail"
	// 			};
	// 			ajaxPost(
	// 		        null,
	// 		        baseUrl+"/"+moduleId+"/person/sendemail",
	// 		        params,
	// 		        function(data){
	// 		        	if (data.result) {
	// 					  	$('.modal').modal('hide');
	// 		    		  	$("#modalSendAgainSuccess").modal("show"); 
	// 		    		  	// Hide modal if "Okay" is pressed
	// 					    $('#modalSendAgainSuccess .btn-default').click(function() {
	// 					        $('.modal').modal('hide');
	// 					    });
	// 					} else if (data.errId == "UNKNOWN_ACCOUNT_ID") {
	// 						toastr.error(data.msg);
	// 						$(".sendValidateEmailBtn").prop("disabled", false).data("loading", false);
	// 						sendValidateEmailBtn.stop();
	// 					}
	// 		        },
	// 		        function(data) {
	// 		    	 	toastr.error("Something went really bad : contact your administrator !");
	// 		    	}

	// 	  	 	);
	// 	        return false;
	// 		},
	// 		invalidHandler : function(event, validator) {//display error alert on form submit
	// 			errorHandler2.show();
	// 			sendValidateEmailBtn.stop();
	// 		}
	// 	});
	// };
    Login.runRegisterValidator  = function(params) { 
		var form4 = $('.form-register');
		var errorHandler3 = $('.errorHandler', form4);
		var createBtn = null;
		$(".form-register .container").removeClass("col-md-offset-3 col-sm-offset-2 col-md-6 col-sm-8 col-xs-12");

        $("#modalRegister .nameRegister, #modalRegister .usernameRegister").remove();

		if($("#surname").length==0){
			$("#modalRegister .form-register-inputs div:eq(5)").append(`
                <div class="surnameRegister">
		            <label class="letter-black"><i class="fa fa-user"></i> Nom </label><br>
		            <input class="form-control" id="surname" name="surname" placeholder="Nom"><br/>
		        </div>
                <div class="firstNameRegister">
		            <label class="letter-black"><i class="fa fa-address-card"></i> Prénom </label><br>
		            <input class="form-control" id="firstName" name="firstName" placeholder="Prénom"><br/>
		        </div>
                <div class="structureRegister">
		            <label class="letter-black"><i class="fa fa-building"></i> Structure </label><br>
		            <input class="form-control" id="structure" name="structure" placeholder="Structure"><br/>
		        </div>
				<div class="telephoneRegister">
		            <label class="letter-black"><i class="fa fa-phone"></i> Téléphone </label><br>
		            <input class="form-control" id="telephone" name="telephone" placeholder="Numéro téléphone"><br/>
		        </div>
                <div class="callbackRegister">
		            <input type="hidden" class="form-control" id="callbackRegister" name="callbackRegister">
		        </div>
                <div class="sessionRegister hide">
					<input type="checkbox" class="form-control" id="sessionRegister" name="sessionRegister">
				</div>
			`);
            $("#modalRegister label[for='charte']").remove();
            $("#modalRegister .agreeContent .checkbox-content").append(`
                <label for="newsletter" class="margin-top-10">
                    <input type="checkbox" class="newsletter" id="newsletter" name="newsletter">
                    <span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>
                    <span class="agreeMsg checkbox-msg letter-red pull-left">
                        Je m'inscris à la newsletter 

                    </span>
        </label>
            `)
		}
		
		form4.validate({
			rules : {
			// 	name : {
			// 	required : true,
			// 	minlength : 4
			// },
            surname : {
                required : true
            },
            firstName : {
                required : true
            },
			// telephone : {
			// 	required : true
			// },
			// gender : {
			// 	required : true
			// },
			// situation : {
			// 	required : true
			// },
			// arrondissement : {
			// 	required : true
			// },
			// username : {
			// 	required : true,
			// 	validUserName : true,
			// 	rangelength : [4, 32]
			// },
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			},
			password3 : {
				minlength : 8,
				required : true
			},
			passwordAgain : {
				equalTo : "#password3",
				required : true
			}
		},
		submitHandler : function(form) { 
			errorHandler3.hide();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #surname').val() +" "+ $('.form-register #firstName').val(),
			   "username" : $('.form-register #surname').val() + $('.form-register #firstName').val(),
               "surname" : $('.form-register #surname').val(),
               "firstName" : $('.form-register #firstName').val(),
               "structure" : $('.form-register #structure').val(),
			   "email" : $(".form-register #email3").val(),
			//    "gender" : $(".form-register #gender").val(),
			//    "situation" : $(".form-register #situation").val(),
			   "telephone" : $(".form-register #telephone").val(),
			//    "arrondissement" : $(".form-register #arrondissement").val(),
			   "pendingUserId" : pendingUserId,
			   "pwd" : $(".form-register #password3").val(),
            //    "callbackRegister" : {
            //         "action" : $(".form-register #callbackRegister").data("action"),
            //         "value" : $(".form-register #callbackRegister").val()
            //    }
            };
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if($('.form-register #sessionRegister').val())
            	params.sessionRegister=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($(".form-register #newsletter").is(":checked"))
		  		params.newsletter=true;
		  	if($(".form-register #newsletterCollectif").is(":checked"))
		  		params.newsletterCollectif=true;
		  	var redirectCallBack=($(".form-register #redirectLaunchCollectif").is(":checked"))? true : false;
            // var actionCallback= $(".form-register #callbackRegister").data("action");
            //   if(typeof actionCallback!="undefined"){
            //     // alert("ok callbacjaction");
            //     if(actionCallback=="connect"){
            //         if(typeof $(".form-register #callbackRegister").attr("element")!="undefined"){
            //             // alert("connect");
                        
            //             var contextElement=JSON.parse($(".form-register #callbackRegister").attr("element"));
            //             mylog.log("contextElement callback",contextElement);
            //             var refLink=(typeof contextElement.interestedLink!="undefined") ? contextElement.interestedLink.split("connect")[1] : "";
            //             var visioDate=(typeof contextElement.chosenDate!="undefined") ? contextElement.chosenDate : "";
            //             params.redirectUrl=refLink+"?visioDate="+visioDate;
            //             params.element2Connect=contextElement;
                        
                        
            //             // links.connectAjax(contextElement.collection, contextElement.id, data.id, "citoyens", "attendees", ["Intéressé.e"], function(){},extraUrlConnect);

            //         }
                    
            //     }    
            // }  
		  	
		  	//if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  	//	params.msgGroup=$(".form-register #textMsgGroup").val();
		  	ajaxPost(
	            null,
	            baseUrl+"/costum/lacompagniedestierslieux/register",
	            params,
	            function(data){ 
	                if(data.result) {
	                	mylog.log("Register Formulaire",data);
		    		  	//createBtn.stop();
						$(".createBtn").prop('disabled', false);
	    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
						$("#registerName").val("");
						$("#username").val("");
                        $("#surname").val("");
                        $("#firstName").val("");
                        $("#structure").val("");
                        $("#telephone").val("");
						$("#email3").val("");
						$("#password3").val("");
						$("#passwordAgain").val("");
						$("#passwordAgain").val("");
						$("#registerSurname").val("");
						$(".form-register #gender").val("");
			   			$(".form-register #situation").val("");
			   			$(".form-register #telephone").val("");
			   			$(".form-register #arrondissement").val("");
						$('#newsletter').prop('checked', true);
						$('#newsletterCollectif').prop('checked', true);
						$('#redirectLaunchCollectif').prop('checked', true);
					   	// toastr.success("Merci, votre soutien a bien été prise en compte");
	    		  		$('.modal').modal('hide');
	    		  		
	    		  		$("#modalRegisterSuccessContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
		    		  	$("#modalRegisterSuccess").modal({ show: 'true' }); 
		    		  	/*$("#modalRegisterSuccess").on('hide.bs.modal' , function(e){
					  		if(typeof thisElement !="undefined"){
					        	subscribeToEvent(thisElement, data.id);
					        }
					  	});*/
                        // alert("ok");
                        var actionCallback=$(".form-register #callbackRegister").data("action");
                        mylog.log("action callback register",actionCallback);
                        if(typeof actionCallback!="undefined"){
                            // alert("ok callbacjaction");
                            if(actionCallback=="connect"){
                                if(typeof $(".form-register #callbackRegister").attr("element")!="undefined"){
                                    // alert("connect");
                                    
                                    var contextElement=JSON.parse($(".form-register #callbackRegister").attr("element"));
                                    mylog.log("contextElement callback",contextElement);
                                    var refLink=(typeof contextElement.interestedLink!="undefined") ? contextElement.interestedLink.split("connect")[1] : "";
                                    var visioDate=(typeof contextElement.chosenDate!="undefined") ? contextElement.chosenDate : "";
                                    var extraUrlConnect="?visioDate="+visioDate;
                                    
                                    links.connectAjax(contextElement.collection, contextElement.id, data.id, "citoyens", "attendees", ["Intéressé.e"], function(res){
                                        mylog.log("callback connect registered to session");
                                        // var paramsmail= {
                                        //     tpl : "sessionInterest",
                                        //     tplObject : "Pré-inscription à la session",
                                        //     tplMail : "essai@gmail.com",
                                        //     html: "",
                                        //     // community : <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" ) ?>,
                                        //     btnRedirect : {
                                        //         hash : "#panel.account-modal",
                                        //         label : "Accéder à mon tableau de bord"
                                        //     }
                                        // };    

                                        //     ajaxPost(
                                        //         null,
                                        //         baseUrl+"/co2/mailmanagement/createandsend",
                                        //         paramsmail,
                                        //         function(data){
                                        //             mylog.log("callback sentmail sessioninterest",data);
                                        //             toastr.success("Un email de pré-inscription à la session vous a été envoyé.");
                                        //         }
                                        //   );


                                    },extraUrlConnect);

                                   


                                }
                                
                            }
                            $("#callbackRegister").val("");
                            $("#callbackRegister").data("action","");
                            $("#callbackRegister").attr("element","");
                            
                        }
		    		  	// $('#modalRegisterSuccess .btn-default').click(function() {
					    //     mylog.log("hide modale and reload");
					    //     $('.modal').modal('hide');
					    //     if(typeof thisElement == "undefined"){
					    // 		window.location.reload();
					    //     }
					    // 	//urlCtrl.loadByHash(window.location.hash);
					    // });
		    		}else {
                        $('.registerResult').html(data.msg);
							$('.registerResult').show();
							$(".createBtn").prop('disabled', false);
							$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
	    	    		  	// toastr.error(data.msg);
	    	    		  	// $('.modal').modal('hide');	    		  	
	        		  		scopeObj.selected={};
	    	    		}
	            },function(data) {
		    	  	toastr.error(trad["somethingwentwrong"]);
		    	  	$(".createBtn").prop('disabled', false);
					$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
		    	  	//createBtn.stop();
		    	}
	        );
		  	return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
    };
    Login.runRegisterValidator();

    costum[costum.slug].sessionFormation = {
        afterSave : function(data){
            mylog.log("aftersave date events sessionFormation", data);
            urlCtrl.loadByHash(location.hash);
            $("#panelAdmin .handleSession").trigger("click");
        },
        afterBuild : function(data){
            mylog.log("afterbuild data sessionFormation",data);
            $("#ajaxFormModal .visioDatecodate label[for=eventDurationcodate-visioDate]").text("Durée (en heure) des visios d'information relatives à cette session");
            $("#ajaxFormModal .removeDateBlock").parent().css({"position": "absolute","left":"-10px","top": "2px"});
            // $("#ajax-modal").before('<style>#ajaxFormModal .removeDateBlock{position: absolute;left: -3.5em;}</style>');
            // var invitLink={
            //     targetType: data.collection,
            //     targetId: data._id.$id,
            //     roles: ["Invité.e"],
            //     isAdmin: false,
            //     urlRedirection: baseUrl+"/costum/co/index/slug/laCompagnieDesTierslieux#menu-sessions-admin"
            // };
            // ajaxPost(
            //     null,
            //     baseUrl+"/"+moduleId+"/link/createinvitationlink",
            //     invitLink,
            //     function(res){
            //         mylog.log("createinvitationlink")
            //     }
            // );

        }
    };

    costum[costum.slug].formation = {
        beforeSave : function(data){
            mylog.log("beforesave formation",data);
            // var invitLink={
            //     targetType: data.collection,
            //     targetId: data._id.$id,
            //     roles: ["Invité.e"],
            //     isAdmin: false,
            //     urlRedirection: baseUrl+"/costum/co/index/slug/laCompagnieDesTierslieux#menu-sessions-admin"
            // };
            // ajaxPost(
            //     null,
            //     baseUrl+"/"+moduleId+"/link/createinvitationlink",
            //     invitLink,
            //     function(res){
            //         mylog.log("createinvitationlink")
            //     }
            // );

        },
        afterSave : function(date){
            mylog.log("aftersave date projects formation", data);
            urlCtrl.loadByHash(location.hash);
        }
    };

    var modalSessionHtml='<div class="modal fade" id="session-modal">'+
       '<div class="modal-dialog">'+
        '<div class="modal-content">'+
           ' <div class="modal-header">'+
           '<h3 style="display: inline-block;margin: 0;font-size: 22px;font-style: italic;">Interface de gestion de la session</h3>'+
               '<a id="mini-info" href="javascript:;">   <i style="font-size:25px;" class="fa fa-info-circle"></i>'+ 
               ' <button type="button" class="close" data-dismiss="modal">'+
                   ' <span aria-hidden="true">&times;</span>'+
                    '<span class="sr-only">Close</span>'+
                '</button>'+
            '</div>'+
            '<div class="modal-body">'+
           ' </div>'+
        '</div>'+
       '</div>'+
    '</div>';
    $("body").append(modalSessionHtml);
    
      

            
    


};