(function (W, $) {
	var create_person_dom = $("<div>")
		.addClass("create-person-modal")
		.addClass("modal")
		.addClass("fade");
	function dom_registration_modal(callable) {
		var fields = {
			email: $("<input>")
				.addClass("form-control")
				.attr('type', 'email')
				.attr("name", "email"),
			firstname: $("<input>")
				.addClass("form-control")
				.attr('type', 'text')
				.attr("name", "firstname"),
			lastname: $("<input>")
				.addClass("form-control")
				.attr('type', 'text')
				.attr("name", "lastname"),
			roles: $("<input>")
				.addClass("form-control")
				.attr('type', 'text')
				.attr("name", "roles")
		};
		return (
			$("<div>")
				.addClass("modal-dialog")
				.attr('id', 'temporary-person')
				.css({
					top: '50%',
					transform: 'translateY(-50%)'
				})
				.append(
					$("<div>")
						.addClass("modal-content")
						.append(
							$("<div>", {
								class: 'modal-header',
								html: "<h5>S'identifier</h5>",
								css: {
									color: 'white'
								}
							}),
							$("<div>")
								.addClass("modal-body")
								.append(
									$("<form>")
										.attr("autocomplete", "off")
										.append(
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Adresse mail"),
													fields.email,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Prénom"),
													fields.firstname,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Nom"),
													fields.lastname,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Structure"),
													fields.roles,
													$("<span>")
														.text('Si plusieurs, séparer par des virgules')
														.addClass("help-block")
												),
											$("<button>")
												.addClass("btn")
												.addClass("btn-primary")
												.css({
													backgroundColor: '#82c17f !important',
													borderColor: 'white',
													color: 'white'
												})
												.attr("type", "submit")
												.text("S'inscrire"),
											$("<button>")
												.addClass("btn")
												.addClass("btn-primary")
												.css({
													backgroundColor: '#82c17f !important',
													borderColor: 'white',
													color: 'white',
													marginLeft: '10px'
												})
												.attr("type", "button")
												.text("J'ai un compte").on('click', function () {
													$("#modalLogin").on('show.bs.modal', function (e) {
														if ($("#infoBL").length == 0) {
															$("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
																Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
																<a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
															</div>`);
														}
													});
													$("#modalLogin").on('hide.bs.modal', function (e) {
														$("#infoBL").remove();
													});
													Login.openLogin();
													$('.create-person-modal').modal('hide');
												})
										).on("submit", function (e) {
											e.preventDefault();
											var valid = true;
											var value;
											var key;

											for (key in fields) {
												if (key === 'roles')
													continue;
												fields[key].nextAll(".help-block").text("");
												fields[key].parents(".form-group").removeClass("has-error");
											}
											if (!fields.email.val()) {
												valid = false;
												fields.email.nextAll(".help-block").text("Ce champ est requis");
												fields.email.parents(".form-group").addClass("has-error");
											}
											if (fields.email.val() && !fields.email.val().match(/^[a-zA-Z0–9._-]+@[a-zA-Z0–9.-]+\.[a-zA-Z]{2,4}$/)) {
												valid = false;
												fields.email.nextAll(".help-block").text("Le format est incorrecte");
												fields.email.parents(".form-group").addClass("has-error");
											}
											if (!fields.firstname.val() && !fields.lastname.val()) {
												valid = false;
												fields.firstname.nextAll(".help-block").text("Veuillez remplir au moins ce champ");
											}
											if (valid) {
												value = {
													email: fields.email.val(),
													name: [fields.firstname.val(), fields.lastname.val()].join(' '),
													firstname: fields.firstname.val(),
													lastname: fields.lastname.val(),
													memberOf: {
														id: costum.contextId,
														type: costum.contextType
													}
												};
												if (fields.roles.val())
													value.roles = fields.roles.val().split(',').map(role => role.trim()).filter(item => item.length > 0);
												var url = baseUrl + '/costum/coevent/user_manager/request/create_temporary';
												ajaxPost(null, url, value, function (resp) {
													$.each(fields, function (_, field) {
														field.val('');
													});
													callable(resp.data);
												})
											}
										})
								)
						)
				)
		);
	}
	function download_pdf(resp, file) {
		resp.blob().then(function (blob) {
			var url = URL.createObjectURL(blob);
			var a_dom = document.createElement('a');
			a_dom.href = url;
			a_dom.download = file;
			a_dom.addEventListener('click', function () {
				setTimeout(function () {
					a_dom.remove();
					URL.revokeObjectURL(url);
				}, 200);
			});
			$(document.body).append(a_dom);
			a_dom.click();
		});
	}
	function request_link_event_address(args) {
		return new Promise(function (resolve, reject) {
			var url = baseUrl + '/costum/coevent/get_events/request/link_tl_to_event';
			ajaxPost(null, url, args, resolve, reject);
		});
	}
	window.eventTypes.learningVisit = 'Visite apprenante';
	var tag_options = {};
	['Formation', 'Education', 'Grand public', 'Entreprise et pouvoirs publics', 'FORMATION-DEFI OCC'].forEach(function (tag) {
		tag_options[tag] = tag;
	});
	W.event_participation = function (dom) {
		var thisElement = $(dom);
		var isExpAttFull = (thisElement.data("expected-attendees") > 0 && Object.keys(thisElement.data("attendees")).length == eventData.expectedAttendees);
		if (!isExpAttFull) {
			if (userId) {
				subscribeToEvent(thisElement);
			} else {
				create_person_dom
					.css('background-color', 'rgba(0, 0, 0, 0.29)')
					.empty()
					.append(dom_registration_modal(function (created) {
						create_person_dom.modal('hide');
						W.userId = created._id.$id;
						W.userConnected = created;
						subscribeToEvent(thisElement, created._id.$id, function () {
							urlCtrl.loadByHash(location.hash);
						});
					}));
				if (!$.contains(document.body, create_person_dom[0]));
				$(document.body).append(create_person_dom);
				create_person_dom.modal('show');
				toastr.info("Veuillez décliner votre identité");
			}
		} else {
			if (userId)
				subscribeToEvent(thisElement);
		}
	};
	W.defaultCoevent = {
		onload: {
			actions: {
				hide: {
					publiccheckboxSimple: 1,
					recurrencycheckbox: 1,
					organizerNametext: 1,
					formLocalityformLocality: 1
				}
			}
		},
		beforeBuild: {
			properties: {
				organizer: {
					label: 'Quel tier-lieu organise cet événement',
					initType: ['organizations'],
					initBySearch: true,
					initMe: false,
					initContacts: false,
					multiple: false,
					initContext: false,
					rules: {
						required: true
					},
					noResult: {
						label: 'Créez votre tiers lieux',
						action: function () {
							window.open('https://cartographie.francetierslieux.fr/', '_blank');
						}
					},
					typeAuthorized: ['organizations'],
					filters: {
						'address.level3': '598412b76ff9920d048b4569',
						$or: {
							'source.keys': 'franceTierslieux',
							'reference.costum': 'franceTierslieux',
						}
					}
				},
				tags: {
					order: 6,
					inputType: "selectMultiple",
					isSelect2: true,
					isMultiple: true,
					label: W.coTranslate('Thematics'),
					options: tag_options
				},
				description: {
					inputType: "textarea",
					markdown: true,
					label: "Description longue",
					order: 17
				},
				parent: {
					init: null
				}
			}
		},
		afterBuild: function () {
			$(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");
		},
		afterSave: function (data) {
			var callbackParams = {
				parent: {},
				links: {
					attendees: {}
				}
			};
			if (typeof data.parent !== 'undefined')
				callbackParams.parent = data.parent;
			callbackParams.parent[coevent_parent.id] = {
				type: coevent_parent.type
			};
			callbackParams.links.attendees[userId] = {
				type: 'citoyens'
			};
			var params = {
				id: data.id,
				collection: data.map.collection,
				path: 'allToRoot',
				value: callbackParams
			};
			link_parent = {
				id: coevent_parent.id,
				collection: coevent_parent.type,
				path: 'links.subEvents.' + data.id,
				value: { type: 'events' }
			}
			dataHelper.path2Value(link_parent, function () { });
			request_link_event_address({
				event: data.id,
				tl: Object.keys(data.map.organizer)[0]
			}).then();
			dyFObj.commonAfterSave(null, function () {
				dataHelper.path2Value(params, function () {
					$('.coevent-program').trigger('coevent-filter');
					$('.theme-selector').each(function () {
						var self = $(this);
						self.trigger('block-update');
					});
					dyFObj.closeForm();
					urlCtrl.loadByHash(location.hash);
				});
			});
		}
	};
	W.ce_prog_download = function (file) {
		var storage = get_session_data();
		var url = baseUrl + '/costum/coevent/getpdfaction?event=' + context;
		url += '&costum_slug=' + costum.slug;
		url += '&costum_page=' + location.hash.match(/\#[\w\-]+/)[0].substring(1);
		url += '&startDate=' + moment().format('YYYY-MM-DD');
		var post = {
			url: (location.protocol + '//' + location.host + location.pathname + location.hash)
		};
		if (storage.regions.length)
			post.regions = storage.regions.map(item => item.id);
		if (storage.tags.length)
			post.tags = storage.tags.slice();
		if (storage.type)
			post.type = storage.type;
		post = {
			method: 'post',
			header: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(post)
		};
		fetch(url, post).then(function (resp) {
			download_pdf(resp, file);
		});
		// ajaxPost(null, url, post, download_pdf, null)
		return false;
	}
})(window, jQuery);