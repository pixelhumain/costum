
var main1Color = costum.colors.main1;
var main2Color = costum.colors.main2;
var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
var scopeLevelParam = ["3"];
var scopeLevelParam = "";
getDataCostum();

function paramsMapLatLon(lat,lon){
	mylog.log("latlon",lat,lon);
	// forced coordinates centered on FR or RE
	// lat = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "-21.115141" : "46.7342232";
	// lon = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ?	"55.536384" : "2.74686000";
	// zoom = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? 9 : 5 ;
	// //
	// dyFObj.formInMap.forced.countryCode=(costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "RE" : "FR";
	// 	dyFObj.formInMap.forced.map={"center":[lat, lon],"zoom" : zoom};
	// 	dyFObj.formInMap.forced.showMap=true;
	paramsMapCO = $.extend(true, {}, paramsMapCO, {
		
		mapCustom:{
			icon: {
				getIcon:function(params){
					var elt = params.elt;
					mylog.log("icone ftl", elt.tags);
					// if (typeof elt.tags != "undefined" && elt.tags != null && typeof costum.settings!="undefined" && typeof costum.settings.compagnonFtl!="undefined" && costum.settings.compagnonFtl){
					// 	if($.inArray("Compagnon France Tiers-Lieux", elt.tags)==-1){
					// 		var myCustomColour = main1Color;
					// 	}else {
					// 		var myCustomColour = main2Color;
					// 	}
					// }else{
						var myCustomColour = main1Color;
					// }
				
					var markerHtmlStyles = `
						background-color: ${myCustomColour};
						width: 3.5rem;
						height: 3.5rem;
						display: block;
						left: -1.5rem;
						top: -1.5rem;
						position: relative;
						border-radius: 3rem 3rem 0;
						transform: rotate(45deg);
						border: 1px solid #FFFFFF`;
				
					var myIcon = L.divIcon({
						className: "my-custom-pin",
						iconAnchor: [0, 24],
						labelAnchor: [-6, 0],
						popupAnchor: [0, -36],
						html: `<span style="${markerHtmlStyles}" />`
					});
					return myIcon;
				}
			},
			getClusterIcon:function(cluster){
				var childCount = cluster.getChildCount();
				var c = ' marker-cluster-';
				if (childCount < 20) {
					c += 'small-ftl';
				} else if (childCount < 50) {
					c += 'medium-ftl';
				} else {
					c += 'large-ftl';
				}
				return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
			}
		}
	});
	if (notNull(lat) && notNull(lon)){
		paramsMapCO.forced =
			{
				latLon : [ 
			    	lat,
			    	lon
			    ]
			};
	}		
	//mapObj.init(paramsMapCO);

 }

$("#show-button-map").addClass("changelabel bg-main1");
$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");

$("#btn-filters").parent().css('display','none');

//$("#input_name_filter").attr("placeholder","Filtre par nom ...");
$('.form-register').find("label[for='agree'].agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie.");
//$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");


var changeLabel = function() {
	$("#menuRight").on("click", function() {
		if($(this).find("a").hasClass("changelabel")) {
			$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
		}
		else{
			$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		}
	//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
		$(this).find("a").toggleClass("changelabel");
	});
}

function getDashboardData(urlParams="", page="", callBack=null, scopeId=null){
	var params = {
		"costumId":costum.contextId, 
		"costumSlug": costum.contextSlug,
		"costumType":costum.contextType,
		"specificBlock":[],
		"scopeId" : scopeId,
		"extraFilter" : {
			"answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10ledy106ry0cl5ibmb4s":{
				$exists:true
			}
		}
	}

	if(page!=""){
		params["page"] = page;
	}

	var asyncCall=true;
	if(page.indexOf("obs")>-1){
		asyncCall=false;
	}
	
	ajaxPost(
		null, 
		baseUrl+"/costum/blockgraph/getdashboarddata"+urlParams, 
		params, 
		function(res){
			costum["dashboardData"] = res["data"];
			costum["dashboardGlobalConfig"] = res["global"];
			if(typeof callBack=="function"){
				callBack(res);
			}
		},
		null,
		null,
		{async:asyncCall}
	)
}

function getDataCostum(){
	ajaxPost(
		null,
		baseUrl+"/co2/element/get/type/"+costum.contextType+"/id/"+costum.contextId,
		null,
		function(data){
			mylog.log("callback contextorga",data);
			mylog.log("costum object",costum);
			if (typeof data.map.level!="undefined"){
				var data = data.map;
				var level=data.level;
				//alert(level);

				var levelId = data.address[level]
				//alert(levelId);
				var levelNb = level.substr(5);

				if (typeof levelId=="undefined" && levelNb=="5"){
					alert("Nous ne trouvons pas la Métropole au sein de laquelle votre réseau agit. Nous vous proposons une portée départementale. Veuillez s'il vous plaît nous contacter afin de nous remonter la problématique à contact@communecter.org.")
					levelNb=levelNb-1;
					levelId=data.address["level"+levelNb];
				}

				
				//var scopeLevel = 
				//if(level!="level1"){
					scopeCountryParam = [data.address.addressCountry];
					scopeLevelParam = (level=="1") ? [Number(levelNb)+2] : [Number(levelNb)+1];
					if(data.address.addressCountry!="FR"){
						scopeLevelParam=["6"];

					}
					upperLevelId=levelId;
					costum.address=data.address;
					costum.parentId=data._id.$id;
				//}
				getDashboardData("", location.hash.replace("#", ""), null, upperLevelId);

				getZone(data.address[level],level);	
				referenceTl(levelNb,data.address[level]);
			}		        	
		}, 
		null,
		null,
		{async:false}
	);
}

function getZone(zoneId,level){
	mylog.log("getzone",zoneId,level);
	ajaxPost(
		null,
		baseUrl+"/co2/zone/getscopebyids",
		{zones:{zoneId}},
		function(data){ 
			//alert(level);
			var coord = {};
			mylog.log("scopes",data.scopes[zoneId+level].latitude);
			var latit = (typeof data.scopes[zoneId+level].latitude !="undefined") ? data.scopes[zoneId+level].latitude : null;
			//var longit = ;
			coord = {
				latitude : data.scopes[zoneId+level].latitude,
				longitude : data.scopes[zoneId+level].longitude
			};
			mylog.log("coord1",coord);

			//Ne pas forcer coordonnée
			//coord.latitude,coord.longitude
			paramsMapLatLon();						          	
		}, 
		null,
		null,
		{async:false}
	);
}

function referenceTl(levelNb,levelId){
	mylog.log("referenceTl",levelNb,levelId);
	
	
	if (typeof costum.loaded=="undefined" || costum.loaded==false){
		// alert("here");
		
		var dataCustom = {};
		dataCustom.id = costum.contextId;
		dataCustom.collection = costum.contextType;
		dataCustom.path = "costum.loaded";
		dataCustom.value=true;
		dataCustom.tplGenerator=true;
 		dataHelper.path2Value(dataCustom,function(response){
	        if(response.result){
				toastr.success("added");
	            mylog.log("loaded costum",response);       	
			}
	    });
	 //    dataCustom.path = "costum.settings.compagnonFtl";
		// dataCustom.value=false;
		// //dataCustom.tplGenerator=true;
 	// 	dataHelper.path2Value(dataCustom,function(response){
	 //        if(response.result){
		// 		//toastr.success("added");
	 //            mylog.log("default setting for compagnon : false",response);       	
		// 	}
	 //    });
	 

		
		// var originId = (typeof window.location.href.split('originId=').pop()!="undefined") ? window.location.href.split('originId=').pop() : "" ;		
		// if(originId!=""){
		// 	ajaxPost(
		// 		null,
		// 		baseUrl+"/survey/form/duplicatetemplate/type/template/templateCostumId/"+originId+"/active/true",
		// 		null,
		// 		function(data){
		// 			toastr.success("Formulaire dupliqué");
		// 			mylog.log("Formulaire dupliqué",data);
		// 			if(typeof(costum.dashboard.config)!="undefined"){
		// 				$.each(costum.dashboard.config, function(k,v){
		// 			    	if(typeof(v.graph.data.coform)!="undefined"){
		// 			       		v.graph.data.coform=data.map._id.$id;
		// 				    }
		// 				});
		// 				dataCustom.path = "costum.dashboard";
		// 				dataCustom.value=costum.dashboard;
		// 				dataHelper.path2Value(dataCustom,function(response){
		// 			        if(response.result){
		// 						toastr.success("added");
		// 			            mylog.log("loaded costum",response);       	
		// 					}
		// 			    });
		// 			}
					
	    //             data=data.map;
		// 		},
		// 		null,
		// 		null,
		// 		{async:false}
		// 	);
		// }
		
		// ajaxPost(
		// 	null,
		// 	baseUrl+"/costum/blockcms/getcmsbywhere",
		// 	{"source_key" : costum.assetsSlug, type:"template" },
		// 	function(data){
		// 		mylog.log("get cms template callback",data);
		// 		var key = Object.keys(data)[0];
		// 		var par = data[key];
		// 		mylog.log("get cms template callback",par);
		// 		var params = {
		// 	        "cmsList" : (par['cmsList'])?par['cmsList']:"",
		// 	        "tplParent"  : par._id.$id,
		// 	        "page"       : par.page,
		// 	        "parentId"   : costum.contextId,
		// 	        "parentSlug" : costum.contextSlug,
		// 	        "parentType" : costum.contextType,
		// 	        "action" : "duplicate" 
		// 	    }

		// 	    dataCustom.id = par._id.$id;
		// 		dataCustom.collection = "cms";
		// 		dataCustom.path = "tplsUser."+costum.contextId+".welcome";
		// 		dataCustom.value="using";
		// 		dataCustom.tplGenerator=true;
		// 		dataHelper.path2Value(dataCustom,function(response){
		// 			if(response.result){
		// 			toastr.success("Modèle graphique importé");
		// 			mylog.log("tplsUSER ADDED",response);       	
		// 			}
		// 		});

		// 		ajaxPost(
		// 			null,
		// 			baseUrl+"/costum/blockcms/getcms",
		// 			params,
		// 			function(data){
		// 				mylog.log("duplicate cms template callback",data);


		// 				//  	var dataCustom = {};
		// 				// //dataCustom.arrayForm = true;

		// 				// // duplicate coform
		// 				// var contextParams = {
		// 				//        "contextId"   : costum.contextId,
		// 				//        "contextSlug" : costum.contextSlug,
		// 				//        "contextType" : costum.contextType,
		// 				//        "creator" : userId
		// 				//    }

		// 				// $.ajax({
		// 				//        type : 'POST',
		// 				//        data : contextParams,
		// 				//        url : baseUrl+"/costum/tierslieux/duplicateform",
		// 				//        dataType : "json",
		// 				//        async : false,
		// 				//        success : function(data){
		// 				//        	toastr.success("Terminé!!");
		// 				//        	//document.location.reload();
		// 				//        	bootbox.dialog({message:`<div class="alert-white text-center">
		// 				// 			<br>
		// 				// 			<strong>Vous pouvez maintenant configurer le formulaire de connaissance de vos tiers lieux</strong>
		// 				// 			<br>
		// 				// 			<br>
		// 				// 			<a href="${baseUrl}/#${costumSlug}.view.forms" class="btn btn-info">Aller à la paramètrage du formulaire</a>
		// 				// 			</div>`}
		// 				// 		);
		// 				//        }
		// 				//    });
		// 			},
		// 			null,
		// 			"json",
		// 			{async : false}
		// 		);
		// 	},
		// 	null,
		// 	null,
		// 	{async:false}
		// );
		//urlCtrl.loadByHash("#welcome");

		// if(Object.keys(refTags).length==0 && typeof(costum.reference)!="undefined" && typeof(costum.reference.themes)!="undefined"){
		// 	refTags=costum.reference.themes;
		// }	

		ajaxPost(
			null,
			baseUrl+"/"+moduleId+"/search/globalautocomplete",
			{
				"searchType" : "templates",
				"sourceKey" : costum.assetsSlug,
				"filters" : {
					"subtype" : "site"
				}
			},
			function(data){
				var networkTplId=Object.keys(data.results)[0];
				var params = {
					"id"		 : networkTplId,
					// "page"		 : page,
					"collection" : "templates",
					"parentId"   : costum.contextId,
                    "parentSlug" : costum.contextSlug,
                    "parentType" : costum.contextType,
					"action" : ""
				}
				ajaxPost(
					null,
					baseUrl + "/co2/cms/usetemplate",
					params,
					function (data) {
						window.location.reload()
					},
					null,
					null,
					{ async: false }
				);

			},
			null,
			null,
			{async : false}
		);
		var refTags = {};
	}

	if(location.hash.indexOf("?") > -1){
		searchInterface.init();
	 }	

	if (typeof searchObject.tags !="undefined" && searchObject.tags.length>0){
		mylog.log("searchObject.tags",searchObject.tags);
		refTags = searchObject.tags;
	}else{
		var allTypePlace=costum.lists.typePlace;
		// allTypePlace.pop();
		refTags=allTypePlace;
	}

	mylog.log("refTags",refTags);
		// No matter which typeplace thirdPlace
		// if(Object.keys(refTags).length>0){
			//alert("fere");

			
			// ajaxPost(
			// 			null,
			// 			baseUrl+"/costum/francetierslieux/multireference/type/organizations",
			// 			{zoneNb:levelNb,zoneId:levelId,tags:[]},
			// 			function(data){ 
			// 				mylog.log("taggedTl",data);	  
			// 			}, 
			// 			null,
			// 			null,
			// 			{async:false}
			// 		);
		// }
	
	
	
}

function lightenColor(color, percent) {
	var num = parseInt(color.replace("#",""),16),
	amt = Math.round(2.55 * percent),
	R = (num >> 16) + amt,
	B = (num >> 8 & 0x00FF) + amt,
	G = (num & 0x0000FF) + amt;
	return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? 
    parseInt(result[1], 16)+","+
    parseInt(result[2], 16)+","+
    parseInt(result[3], 16)
  : null;
}

var lighterMain1 = lightenColor(main1Color,10);
var darkerMain1 = lightenColor(main1Color,-10);
var lightestMain1Rgb = hexToRgb(costum.colors.main1);
  				

dyFObj.unloggedMode=true;

costum.name={
	searchExist : function(type,id,name,slug,email){
	    var editDynF=jQuery.extend(true, {},costum.typeObj[type].dynFormCostum);
	    editDynF.onload.actions.hide.similarLinkcustom=1;
	    dyFObj.editElement( type,id, null, editDynF,{},"afterLoad");
	}
};

costum[costum.slug]={
	init : function(){	
		var contextData=null;
		var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
 		var scopeLevelParam = ["3"];
 		var upperLevelId = "";
 		//costum.address={};
		document.documentElement.style.setProperty('--main1', main1Color);
		document.documentElement.style.setProperty('--main2', main2Color);
		document.documentElement.style.setProperty('--lighterMain1', lighterMain1);
		document.documentElement.style.setProperty('--lightestMain1Rgb', lightestMain1Rgb);
		document.documentElement.style.setProperty('--darkerMain1', darkerMain1);
		
		coInterface.bindButtonOpenForm = function () {
		$(".btn-open-form").off().on("click", function () {
			    var typeForm = $(this).data("form-type");
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				//dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
				if (contextData && contextData.type && contextData.id) {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");
					} else {
						dyFObj.openForm(typeForm, "sub");
					}
				}
				else {
					if (typeForm == "organization") {
						dyFObj.openForm(typeForm, "afterLoad");

					} else {
						dyFObj.openForm(typeForm);
					}
				}
			});
		};
		coInterface.bindButtonOpenForm();
		changeLabel();
		//getNetwork();

		function getNetwork(){
		    mylog.log("---------getNetwork");

		    var params = {
		        contextId : costum.contextId,
		        contextType : costum.contextType
		    };

		    ajaxPost(
		        null,
		        baseUrl + "/costum/reseautierslieux/getnetwork",
		        params,
		        function (data) {
		        	//alert("dd");
		        	mylog.log("callbacknet",data);
		        	$.each(data, function(e, v){
		        		var posNet=$.inArray(v.name,costum.lists.network);
		        		if (posNet>-1){
		        			mylog.log("networks",posNet,costum.lists.network);
		        			costum.lists.network.splice(posNet, 1);


		        			// var posNet=	$.inArray(v,costum.lists.network);
		        			// alert(posNet);
		        		}
		        	});	
		        }
		    );    

		}


		costum.typeObj.organizations.dynFormCostum.prepData=function(data){
		    //alert("afterbuild");
			mylog.log("prepData beginning",data);
		    var listObj=costum.lists;
		    // delete listObj.network;
		    $.each(listObj, function(e, v){
		        constructDataForEdit=[];
		        $.each(v, function(i, tag){
		            if($.inArray(tag, data.map.tags) >=0){
		                constructDataForEdit.push(tag);
		             
		                data.map.tags.splice(data.map.tags.indexOf(tag),1);
		            }
		        });
		        data.map[e]=constructDataForEdit;
		    });

			if (notNull(data.map.photo)) {
                uploadObj.initList.photo = data.map.photo;
            }

			if(typeof data.map.telephone!="undefined"){
			    if(typeof data.map.telephone.fixe=="object"){
				    data.map.telephone=data.map.telephone.fixe[0];
				}else if(typeof data.map.telephone.mobile=="object"){
					data.map.telephone=data.map.telephone.mobile[0];
				}
			}

			if(typeof data.map.socialNetwork!="undefined"){
				mappingSocialNetwork={
					"facebook" : "votre page Facebook", 
                    "twitter" : "votre compte Twitter", 
                    "instagram" : "votre compte Instagram", 
                    "linkedin" : "votre compte Linkedin", 
                    "mastodon" : "votre compte Mastodon", 
                    "movilab" : "votre page Movilab"
				};
				socialArray=[];
				$.each(data.map.socialNetwork,function(name,link){
					var socialObj={
						platform : mappingSocialNetwork[name],
						url : link
					};
					socialArray.push(socialObj);
				});
				data.map.socialNetwork=socialArray;
			}

		     mylog.log("prepData return",data);

		    // $.each(costum.typeObj.organizations.dynFormCostum.beforeBuild.properties,function(key,val){
		    //     if(val.inputType=="openingHours"){
		    //         costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs[val.inputType];
		    //     }
		    //     else if(key=="video"){
		    //         costum.typeObj.organizations.dynFormCostum.beforeBuild.properties[key].init=dyFInputs.videos.init;
		    //     }
		    // });

		    return data;

		};
		
        



		if(typeof costum[costum.slug].extendInit=="function"){
		    costum[costum.slug].extendInit();
		}

	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData In",data);
			mylog.log("formData In Extratags",data.extraTags);

			if(typeof finder != "undefined" && Object.keys(finder.object).length > 0){
				$.each(finder.object, function(key, object){
					if(typeof formData[key]!="undefined"){
						delete formData[key];
					}
				});
			}

			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string"){
							if(e=="extraTags" && v.indexOf(",")>-1){
								var extraArray=v.split(",");
								data.tags=data.tags.concat(extraArray);
								mylog.log("data.tags.contact(extraArray)",data.tags);
							}else{
								data.tags.push(v);
							}
							// if(e=="manageModel" && v=="Autre"){
							// 	v="Autre mode de gestion";
							// }
							// if(e=="typePlace" && v=="Autre"){
							// 	v="Autre famille de tiers-lieux";
							// }
							data.tags.push(v);
							
						}else{
							$.each(v, function(i,tag){
								// if(e=="typePlace" && tag=="Autre"){
								// 	tag="Autre famille de tiers-lieux";
								// }
								// if(e=="manageModel" && tag=="Autre"){
								// 	tag="Autre mode de gestion";
								// }
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});

			if(typeof data.telephone == "string"){
				var mapIndicTel={
					"FR" : "33",
					"GP" : "590",
					"MQ" : "596",
					"GF" : "594",
					"RE" : "262",
					"PM" : "508",
					"YT" : "269",
					"WF" : "681",
					"PF" : "689",
					"NC" : "687"
				};

				var phone=data.telephone;
				var numIndex=2;
				phone=phone.replace(/\s/g,'');
				// remplacer 00 par +
				phone=(phone.substring(2,0)=="00") ? "+"+phone.substring(2) : 
				    ((phone.substring(0,1)!="+" && typeof data.address!="undefined" && typeof data.address.addressCountry!="undefined" && typeof mapIndicTel[data.address.addressCountry]!="undefined") 
					   ? "+"+mapIndicTel[data.address.addressCountry]+phone.substring(1) : phone );
				if(phone.substring(0,1)=="+"){
					if(phone.substring(1,3)=="33"){
						numIndex=4;
					}else{
						numIndex=5;
					}
				}
				
				var mobOrfixeInd=phone.substring(numIndex,numIndex-1);
				var typeNumber= (mobOrfixeInd=="6" || mobOrfixeInd=="7") ? "mobile" : "fixe" ;	
			    data.telephone={};
				data.telephone[typeNumber]=[phone];
			}

			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			// if(data.tags.includes("Autre") || data.tags.includes("Autres")){
			// 	$.each(data.tags,function(i,tag){
            //         if(tag=="Autre" || tag=="Autres"){
            //         	delete data.tags[i];
            //         }
			// 	});
			// }

			if(data.socialNetwork){
				var formatedSocialNetwork={};
                $.each(data.socialNetwork,function(i,net){
					var labelArray=net["platform"].split(" ");
  
					var nameSocNet=labelArray[labelArray.length-1].toLowerCase();
					if(typeof net["url"]!="undefined" && net["url"]!=""){
                        formatedSocialNetwork[nameSocNet]=net["url"];						
					}
					delete data.socialNetwork[i];

				});
                data.socialNetwork=formatedSocialNetwork;
			}

            mylog.log("formData return",data);
			return data;
		},
		afterBuild : function(data){
			mylog.log("afterbuild reseauTierslieux",data);
			if(typeof data.manageModel=="undefined" || (typeof data.manageModel!="undefined" && !data.manageModel.includes("Autre mode de gestion"))){
					// alert("showthos");
				$("#ajaxFormModal .extraManageModeltext").hide();
			}
			if(typeof data.typePlace=="undefined" || (typeof data.typePlace!="undefined" && !data.typePlace.includes("Autre famille de tiers-lieux"))){
				$("#ajaxFormModal .extraTypePlacetext").hide();
			}

			$("#divCity input").attr("placeholder","Saisir votre code postal");

			            

			//mylog.log("manageModel param",typeof param, param);
			// $("#ajaxFormModal .manageModelselect select").on('click', function(){
			// 	mylog.log("Saving previous manageModel " + $(this).val());
			// 	$(this).data('manageModel', $(this).val());
			// });
						    
			$("#ajaxFormModal .manageModelselect select").change(function(){
			 //alert("change");
			    // var prevManageModel = $(this).data('manageModel');
			    var modelStatus=$(this).val();
			    if(modelStatus!="Autre mode de gestion"){
			    //    $("#ajaxFormModal .extraManageModeltext").hide();
			       $("#ajaxFormModal .extraManageModeltext input").val("");
				   $("#ajaxFormModal .extraManageModeltext").hide();
			    }
			    //alert(modelStatus);
			    var manageModel2TypeOrga={
			        "Association" : "NGO", 
			        "Collectif citoyen" : "Group", 
			        "Universités / Écoles d’ingénieurs ou de commerce / EPST" : "GovernmentOrganization", 
			        "Établissements scolaires (Lycée, Collège, Ecole)" : "GovernmentOrganization", 
			        "Collectivités (Département, Intercommunalité, Région, etc)" : "GovernmentOrganization", 
			        "SARL-SA-SAS" : "LocalBusiness", 
			        "SCIC" : "Cooperative", 
			        "SCOP" : "Cooperative", 
			        "Autre mode de gestion"	: "Group"
			    }; 
			    $("#ajaxFormModal .typeselect select").val(manageModel2TypeOrga[modelStatus]);
			    if(modelStatus=="Autre mode de gestion"){
			       $("#ajaxFormModal .extraManageModeltext").show();
			       $("#ajaxFormModal .extraManageModeltext input").focus();
			    }
			});
			            
			           
			$("#ajaxFormModal .typePlaceselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var placeCat=$(this).val();
			    if(placeCat==null || placeCat.includes("Autre famille de tiers-lieux")==false){
			       $("#ajaxFormModal .extraTypePlacetext").hide();
			       $("#ajaxFormModal .extraTypePlacetext input").val("");
			    }
			    if(placeCat.includes("Autre famille de tiers-lieux")){
			       $("#ajaxFormModal .extraTypePlacetext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


			$("#ajaxFormModal .themeNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var themeNetwork=$(this).val();
			    if(themeNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraThemeNetworktext").hide();
			       $("#ajaxFormModal .extraThemeNetwork input").val("");
			    }
			    if(themeNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraThemeNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});

			$("#ajaxFormModal .localNetworkselect select").change(function(){
			    //alert("change");
			    //var prevTypePlace = $(this).data('typePlace');
			    var localNetwork=$(this).val();
			    if(localNetwork.includes("Autres")==false){
			       $("#ajaxFormModal .extraLocalNetworktext").hide();
			       $("#ajaxFormModal .extraLocalNetwork input").val("");
			    }
			    if(localNetwork.includes("Autres")){
			       $("#ajaxFormModal .extraLocalNetworktext").show();
			       //$("#ajaxFormModal .extraTypePlacetext input").focus();
			    }
			});


            //init openingHours
            if(typeof data.openingHours!="undefined"){
			    $.each(openingHoursResult, function(e,v){
      				mylog.log("custom init openinghours",data.openingHours,e);
      				if(typeof data.openingHours[e]=="undefined" || !notEmpty(data.openingHours[e]) ){
      					openingHoursResult[e].disabled=true;
      				}
      			});	      
			}	
					
            $(".btn-select-day").off().on("click",function(){
                mylog.log("selectDay", openingHoursResult, $(this).data("key"));
                key=$(this).data("key");
                if($(this).hasClass("active")){
                    $(this).removeClass("active");
                    $.each(openingHoursResult, function(e,v){
                    if(v.dayOfWeek==key)
                        openingHoursResult[e].disabled=true;
                    });
                    $("#contentDays"+key).fadeOut();
                }else{
                    $(this).addClass("active");
                    $.each(openingHoursResult, function(e,v){
                        if(v.dayOfWeek==key)
                        	delete openingHoursResult[e].disabled;
                    });
                    $("#contentDays"+key).fadeIn();
                }
            });


		    $('.timeInput').off().on('changeTime.timepicker', function(e) {
			    mylog.log("changeTimepicker");
			    var typeInc=$(this).data("type");
			    var daysInc=$(this).data("days");
			    var hoursInc=$(this).data("value");
			    var firstEnabled=null;
			    var setFirst=false
			    $.each(openingHoursResult, function(i,v){
				    if(!setFirst && typeof openingHoursResult[i].disabled=="undefined"){
				       mylog.log("firtEnable",v.dayOfWeek);
				       firstEnabled=v.dayOfWeek;
				       setFirst=true;
			        }
	                if(firstEnabled==daysInc){
	        	        mylog.log("first enabled change causes recursive change",v.dayOfWeek);
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
                        var typeHours="";
	        	        if(typeInc=="opens"){
	        		        typeHours="start";
	        	        }else if(typeInc=="closes"){
	        		        typeHours="end";
	        	        }
	        	        if(typeHours!=""){
	        		        $("#"+typeHours+"Time"+v.dayOfWeek+hoursInc).val(e.time.value);
	        	        }
	                }else if(v.dayOfWeek==daysInc){
	        	        openingHoursResult[i]["hours"][hoursInc][typeInc]=e.time.value;
	                }
		        });
		    });

			$('.timeInput').on("focus",function (){
			    $(this).timepicker('showWidget');
			});

			$('.addHoursRange').hide();

			//init videos
			dyFInputs.videos.init();   

			// AFTERLOAD
			dyFObj[dyFObj.activeElem].dynForm.jsonSchema.onLoads.afterLoad=function(data){
				mylog.log("afterload reseauTL data", data);
				$("#ajaxFormModal [for='newElement_city']").html("<i class='fa fa-chevron-down'></i> Code Postal");
				$("#divMapLocality").addClass("hidden");
				
				if(Object.keys(data).length==0 || typeof data.address=="undefined" || (typeof data.address!="undefined" && Object.keys(data.address).length==0)){
					mylog.log("formInMap newAddress afterload");
					dyFObj.formInMap.newAddress(true);
				}else if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
					$('#ajaxFormModal #divNewAddress').hide();
				}

				// if(typeof data.address!="undefined" && Object.keys(data.address).length>0){
				// 	dyFObj.formInMap.newAddress(false);
				// 	$('#ajaxFormModal #divNewAddress').hide();
				// }else{
				// 	dyFObj.formInMap.newAddress(true);
				// }

				dyFObj.formInMap.bindStreetResultsEvent = function(){	
					dyFInputs.locationObj.copyMapForm2Dynform = function (locObj) {
						mylog.warn("---------------copyMapForm2Dynform afterload----------------");
						mylog.log("locationObj", locObj);
						dyFInputs.locationObj.elementLocation = locObj;
						mylog.log("elementLocation", dyFInputs.locationObj.elementLocation);
                        // Dans ce contexte une seule adresse depuis le dynform par d'adresse secondaire pour le moment;
                        dyFInputs.locationObj.elementLocations=[];
						dyFInputs.locationObj.elementLocations.push(dyFInputs.locationObj.elementLocation);
						mylog.log("dyFInputs.locationObj.elementLocations", dyFInputs.locationObj.elementLocations);
						mylog.log("dyFInputs.locationObj.centerLocation", dyFInputs.locationObj.centerLocation);
						if(!dyFInputs.locationObj.centerLocation /*|| dyFInputs.locationObj.elementLocation.center == true*/){
							dyFInputs.locationObj.centerLocation = dyFInputs.locationObj.elementLocation;
							dyFInputs.locationObj.elementLocation.center = true;
						}
						mylog.dir(dyFInputs.locationObj.elementLocations);
					};
				    dyFObj.formInMap.valideLocality = function(country){
        			    mylog.log("formInMap valideLocality afterload", notEmpty(dyFObj.formInMap.NE_lat));

        			    if(notEmpty(dyFObj.formInMap.NE_lat)){
        			    	locObj = dyFObj.formInMap.createLocalityObj();
        			    	mylog.log("formInMap copyMapForm2Dynform", locObj);
        			    	dyFInputs.locationObj.copyMapForm2Dynform(locObj);

        			    }
        			    // Div récapitulatif de l'adresse 
        			    dyFObj.formInMap.resumeLocality();
        			    // Cache le bouton de validation d'adresse
        			    dyFObj.formInMap.btnValideDisable(false);
        			    toastr.success("Adresse enregistrée");
        			
        			    dyFObj.formInMap.initHtml();
        			    $("#btn-submit-form").prop('disabled', false);
        		    };

        		    // Bind l'événement du clic de la voie (numéro + rue) sélectionnée    	
        	        $("#ajaxFormModal .item-street-found").off().click(function(){
        		        if(typeof dyFObj.formInMap.mapObj != "undefined" && dyFObj.formInMap.mapObj != null && Object.keys(dyFObj.formInMap.mapObj).length > 0){
        		        	dyFObj.formInMap.mapObj.setLatLng([$(this).data("lat"), $(this).data("lng")], 0);
        		        	setTimeout(function(){dyFObj.formInMap.mapObj.map.setZoom(17)},1000);
        		        	$("#divMapLocality").removeClass("hidden");
        		        	$("#divMapLocality").before("<h5 style='display:inline-block;margin-top:20px;'>Déplacer le curseur au besoin, pour préciser la localisation exacte.</h5>");
        		        }
        		        var streetAddressName= $(this).text().split(",")[0].trim();
    
        		        $('#ajaxFormModal .formLocality input[name="newElement_street"]').val(streetAddressName);
    
        		        dyFObj.formInMap.NE_street = $('#ajaxFormModal .formLocality input[name="newElement_street"]').val();
    
        		        mylog.log("lat lon", $(this).data("lat"), $(this).data("lng"));
        		        $("#ajaxFormModal #dropdown-newElement_streetAddress-found").hide();
        		        $('#ajaxFormModal [name="newElement_lat"]').val($(this).data("lat"));
        		        $('#ajaxFormModal [name="newElement_lng"]').val($(this).data("lng"));
    

        		        dyFObj.formInMap.NE_lat = $(this).data("lat");
        		        dyFObj.formInMap.NE_lng = $(this).data("lng");
        		        dyFObj.formInMap.showWarningGeo(false);

        		        //Valider au click du résultat
        		        dyFObj.formInMap.valideLocality();
    
        		        $("#ajaxFormModal #sumery").show();

    
        		        dyFObj.formInMap.mapObj.markerList[0].off().on('mouseup', function (e) {
				    	    var latLonMarker = dyFObj.formInMap.mapObj.markerList[0].getLatLng();
		                    mylog.log('marker drop event', latLonMarker);
		                    dyFObj.formInMap.NE_lat = latLonMarker.lat;
				    	    dyFObj.formInMap.NE_lng = latLonMarker.lng;
        
				        	dyFObj.formInMap.valideLocality();
				        	toastr.success("Coordonnées mises à jour");
		                });
        	        });
                };
            };
		},
		beforeSave : function(data){
			mylog.log("beforeSave data",data);
            if(notNull(data.newElement_city)){
			    delete data.newElement_city;
            }

		    if(notNull(data.newElement_street)){
			    delete data.newElement_street;
		    }
		    return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;
			// var mapDynf2Answer={
            //     "tags" : { 
            //         "territory" : "franceTierslieux62ab1d587f3bb.franceTierslieux28102021_1857_335",   
			//         "typePlace" : "franceTierslieux62ab1d5880058.franceTierslieux28102021_1939_41",
			//         "services" : "franceTierslieux62ab1d5880058.franceTierslieux28102021_1939_443",
			//         "manageModel" : "franceTierslieux62ab1d5880df2.franceTierslieux29102021_912_51"
			//         //"state" : "multiRadiofranceTierslieux28102021_184_01",        
			        
			//     }    
			// };

			

			

		//	alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data);
				dyFObj.closeForm();
				// if(typeof answerObj!="undefined" && typeof answerObj._id.$id!="undefined"){
				// 	//alert("answer");
				//     var savedData = data.map;
				//     var tplCtx = {
				//         id : answerObj._id.$id,
				//         collection : "answers"
				//     };
				//     $.each(savedData , function(key,val){
				//         if(typeof mapDynf2Answer[key]!="undefined"){
				//         tplCtx.path = "answers."+mapDynf2Answer[key];
				//         tplCtx.value = val;
				//         if(key=="tags"){
				//             $.each(costum.lists,function(kl,vl){
				//               if(Object.keys(mapDynf2Answer[key]).includes(kl)){  
				//                 var filteredValues = val.filter(value => vl.includes(value));
				//                 mylog.log("tags'common values",kl,filteredValues);
				//                   if(kl=="state" || kl=="manageModel" || kl=="territory"){
				//                     filteredValues=filteredValues[0];
				//                 }
				//                 tplCtx.path = "answers."+mapDynf2Answer[key][kl];
				//                 tplCtx.value = filteredValues;
				//                 dataHelper.path2Value( tplCtx, function(params) { 
				//                     var stepId= mapDynf2Answer[key][kl].split(".")[0] ;  
				//                     var inputKey=mapDynf2Answer[key][kl].split(".")[1];                       
				//                     reloadInput(inputKey, stepId);
				//                 });
				//               }  

				//             });
				//         }
				//         else if(key=="address"){
				//             ansAddrObj={};
				//             val["name"] = val["streetAddress"]+", "+val["postalCode"]+", "+val["addressLocality"]+", "+val["level1Name"]
				//             ansAddrObj[key]=val;
				//             tplCtx.value=ansAddrObj;
				//         }
				//         else if(key=="socialNetwork"){
				//             var socialTable = [];
				//             socialTable.push(["Plateformes","Url"]);
				//             $.each(val, function(ind,sonet){
				//                 socialTable.push([sonet["platform"],sonet["url"]]);
				//             });
				//             tplCtx.value=socialTable;
				//         }
				//         else if(key=="video"){
				//             tplCtx.value=val[0];
				//         }

				//         if(key!="tags"){
				        
				//             dataHelper.path2Value( tplCtx, function(params) { 
				//                 var stepId= azynf2Answer[key].split(".")[0] ;  
				//                 var inputKey=mapDynf2Answer[key].split(".")[1];                       
				//                 reloadInput(inputKey.substring(inputKey.length-39), stepId);
				//             });

				//         }    
				    

				//         }
				//     });
				// }
				//$("#addConfirmModal").modal('show');
				// if(map.category=="network" && map.name){
					

				// // 	data.name
				//  }
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite électronique</span>");

	// 				$('#addConfirmModal').on('hidden.bs.modal', function () {
	// 					onchangeClick=false;
	// 					history.replaceState({}, null, uploadObj.gotoUrl);
	// //					location.hash(uploadObj.gotoUrl);
	// 				  	window.location.reload();
	// 				});
				}
				// else{
				// 	urlCtrl.loadByHash(uploadObj.gotoUrl);
				// }
				urlCtrl.loadByHash(location.hash);
			});
		}
		
	}
};
	
// switch view graph to table
$(document).on("click", ".switchToChart", function () {
    $(".chartviz").show();
    $(".tableviz").hide();
    $(".showChart").addClass("active");
    $(".showTable").removeClass("active");
})

$(document).on("click", ".switchToTable", function () {
    $(".chartviz").hide();
    $(".tableviz").show();
    $(".showChart").removeClass("active");
    $(".showTable").addClass("active");
})

$(document).on("click", ".exportCSV", function () {
    getDashboardData("/isExport/true/format/csv", location.hash.replace("#", ""), function (response) {
        if (typeof json2csv!="undefined" && typeof json2csv.Parser!="undefined") {
            const parser = json2csv.Parser;
            const fields = response.header;
            const exportation_preparation = new parser({fields, delimiter : ";"});
            const csv = exportation_preparation.parse(response.body);
            let filename = "recensement2023_data";
            if (typeof filename!="undefined") {
                const splited_filename = filename.split('.');
                const file_signature = moment().format('YYYYMMDD-HHmm');
                filename = splited_filename.length>1 && splited_filename[splited_filename.length - 1].toLowerCase()==='csv' ? `${splited_filename.slice(-1).join('')}_${file_signature}.csv` : `${filename}_${file_signature}.csv`;

                //var universalBOM = "\uFEFF";
                const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                    type : 'text/csv;charset=utf-8;'
                });
                if (typeof (navigator)!="undefined" && typeof (navigator.msSaveBlob)!="undefined") { // IE 10+
                    navigator.msSaveBlob(blob, filename);
                } else {
                    const link = document.createElement('a');
                    if (link.download!==undefined) { // feature detection
                        // Browsers that support HTML5 download attribute
                        const url = URL.createObjectURL(blob);
                        link.setAttribute('href', url);
                        link.setAttribute('download', filename);
                        link.style.visibility = 'hidden';
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                    }
                }
            } else {
                toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
            }
        }
    }, upperLevelId);
})

// function getAnswersWithOrga(answerData){
// 	var results = {};
// 	var params = {searchedData : answerData};
// 	ajaxPost(
// 		null,
// 		baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk",
// 		params,
// 		function (data) {
// 			$.each(data, function(key, value){
// 				if (value.count.answers > 0) {
// 					if(typeof value.results == "object" && Object.keys(value.results).length > 0){
// 						results[key] = {};
// 						$.each(value.results, function(k, v){
// 							results[key][k] = {
// 								label: k,
// 								value: v.orgaNameArray
// 							}
// 						});
// 					}
// 					// results = Object.keys(results).sort().reduce((obj, key) => {
// 					// 	obj[key] = results[key];
// 					// 	return obj;
// 					// })
// 					// results = ;
// 				}
// 			});
// 		},
// 		null,
// 		"json",
// 		{ async: false }
// 	);
// 	return results;
// }

$(document).on("click", ".exportJSON", function(){
	var region = localStorage.getItem("activeRegion");
	getDashboardData("/isExport/true/format/json", location.hash.replace("#", ""), function(response){
		if (typeof json2csv!= "undefined" && typeof json2csv.Parser !="undefined") {
			const json = JSON.stringify(response.body);
			let filename = "recensement2023_data";
			if (typeof filename !="undefined") {
				const splited_filename = filename.split('.');
				const file_signature = moment().format('YYYYMMDD-HHmm');
				filename = splited_filename.length > 1 && splited_filename[splited_filename.length - 1].toLowerCase() === 'json' ? `${splited_filename.slice(-1).join('')}_${file_signature}.json` : `${filename}_${file_signature}.json`;
	
				//var universalBOM = "\uFEFF";
				const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), json], {
					type: 'text/json; charset=utf-8;'
				});
				if (typeof (navigator) !="undefined" && typeof (navigator.msSaveBlob) !="undefined") { // IE 10+
					navigator.msSaveBlob(blob, filename);
				} else {
					const link = document.createElement('a');
					if (link.download !== undefined) { // feature detection
						// Browsers that support HTML5 download attribute
						const url = URL.createObjectURL(blob);
						link.setAttribute('href', url);
						link.setAttribute('download', filename);
						link.style.visibility = 'hidden';
						document.body.appendChild(link);
						link.click();
						document.body.removeChild(link);
					}
				}
			} else {
				toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
			}
		}
	}, upperLevelId);
})
/* 
if(costum.editMode){
	costumizer.pages.views.linePage = function(k,v,costumLang="fr"){
		var htmlStr ="";
		var forAll = ["green", tradCms.true];
		var status = ["green", tradCms.published];
		var btnPublie = " <a href='javascript:;' class='brouillon-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.draftPage+"</a>";
		var typePage="pageCMS";
		var defaultValueRestricted = "all";
		var uuid = costumizer.generateUUID();
		var pageName = k.replace("#", "");
		if(typeof v["name"] !='undefined' && typeof v["name"] === "object" && notEmpty(v["name"])) {
			pageName = (typeof v["name"][costumLang] != "undefined") ? v["name"][costumLang] : v["name"][Object.keys(v["name"])[0]];
		}
		if(v != null){

			if((typeof v.appTypeName != "undefined" && v.appTypeName != "") || typeof v.useFilter != "undefined"){
				typePage="advancedApp";
			}

			if(typeof v.lbhAnchor != "undefined" && v.lbhAnchor == true)
				typePage="anchorMenu";

			if(typeof v.target != "undefined" && v.target == true && typeof v.urlExtern != "undefined" && notEmpty(v.urlExtern))
				typePage="externalLink";

			if(typeof v.hash != "undefined" && v.hash == "#app.aap")
				typePage="aap";

			if(typeof v.restricted != "undefined" && v.restricted != null){
				if(typeof v.restricted.admins != "undefined"){
					if( v.restricted.admins == true || v.restricted.admins == "true"){
						defaultValueRestricted = "admins";
						forAll = ["red", tradCms.visibleToAdminOnly];
					}
				}

				if(typeof v.restricted.members != "undefined"){
					if( v.restricted.members == true || v.restricted.members == "true"){
						defaultValueRestricted = "members";
					}
				}

				if(typeof v.restricted.draft != "undefined" && v.restricted.draft == true){
					status = ["red", tradCms.draft];
					btnPublie = " <a href='javascript:;' class='publie-page tooltips' data-key='"+k+"''><i class='fa fa-share-square-o'></i> "+tradCms.publishPage+"</a>";
				}
			}
			
			htmlStr += "<tr id='keyPages-"+k+"' class='pages-line'>"+
						"<th class='col checkbox-tools'>";
						if (k!="#welcome")
							htmlStr +=	"<input type='checkbox' class='selectPages' value='"+k+"'/>" ;
			htmlStr +=	"</th>"+
						"<td  data-key='"+k+"'' class='col title editPage'><span class=' tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>";
						if(typeof v["name"] !='undefined' && typeof v["name"] === "object" && notEmpty(v["name"])) {
							if (typeof v["name"][costumLang] != "undefined") {
								htmlStr += "<span style='font-size: 20px; margin-right: 10px'>"+themeParams.DeeplLanguages[costumLang.toUpperCase()]["icon"]+"</span>";
							}
						}
						htmlStr += pageName;
						htmlStr += "</span></td>"+
									"<td  data-key='"+k+"'' class='col key-app editPage' ><span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+k+"</span></td>"+
									"<td  data-key='"+k+"'' class='col key-app editPage' ><span class='tooltips' data-toggle='tooltip' data-original-title='"+tradCms.clickEdit+"'>"+tradCms[typePage]+"</span></td>";
						
						
						htmlStr += "<td  class='col key-app'><span class='tooltips select-visibility-value-content'>"+
							"<select data-key='"+k+"'' class='form-control select-visibility-value' style='background-color: transparent;border: none;border-bottom: 2px solid #ccc;'>";
							htmlStr +=  (defaultValueRestricted == "all") ? "<option value='all' selected='selected'>"+tradCms.forAll+"</option>" : "<option value='all'>"+tradCms.forAll+"</option>";
							htmlStr +=  (defaultValueRestricted == "admins") ? "<option value='admins'  selected='selected'>"+tradCms.forAdmin+"</option>": "<option value='admins'>"+tradCms.forAdmin+"</option>";
							htmlStr +=  (defaultValueRestricted == "members") ? "<option value='members' selected='selected'>"+tradCms.forMembers+"</option>" : "<option value='members'>"+tradCms.forMembers+"</option>";
							htmlStr +=  "</select></span></td>" ;

						htmlStr += "<td  data-key='"+k+"'' class='col key-app' style='color:"+status[0]+"'><span class=' tooltips' data-toggle='tooltip'>"+status[1]+"</span></td>" ;
						htmlStr+= "<td class='col toolsEdit'>"
						htmlStr+= "<div class='dropdown dropdown-menu-action-page'>"+
										"<button class='btn btn-primary dropdown-toggle' type='button' id='"+k+"' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"+
										"<i class='fa fa-cog'></i>"+
										"</button>"+
										"<ul class='dropdown-menu pull-right' aria-labelledby='"+k+"'>";
										if(typePage != "anchorMenu" && typePage != "externalLink"){
											htmlStr+= "<li><a href='"+k+"' class='lbh-costumizer-edit-page tooltips'><i class='fa fa-eye'></i> "+tradCms.showPage+"</a></li>";
											if (typePage == "pageCMS")
												htmlStr+= "<li class='new-feature'><a href='javascript:costumizer.template.views.initAjax({space:`template`,key:`page`,page:`"+k+"`});' class='tooltips'><i class='fa fa-star'></i> "+tradCms.getTemplatePage+"</a></li>";
										}
						htmlStr+=			"<li><a href='javascript:;' class='editPage ' data-key='"+k+"''><i class='fa fa-pencil '></i> "+trad.edit+"</a></li>";
											if(typePage != "anchorMenu")
						htmlStr+=			"<li><a href='javascript:;' class='duplicate-page tooltips' data-key='"+k+"''><i class='fa fa-copy'></i> "+tradCms.duplicate+"</a></li>";
											if (k!="#welcome" && typePage != "externalLink"){
						htmlStr+= 				"<li>"+btnPublie+"</li>";
											}
											if (v.hash == "#app.search"){
						htmlStr+= 				"<li><a class='tooltips' href=javascript:configFilterAnswers(`"+k+"`)><i class='fa fa-filter'></i> Configurer le filtre par réponse</a></li>";
											}
											if (k!="#welcome" && typePage != "anchorMenu"){
						htmlStr+=				"<li><a href='javascript:;' class='delete-page text-red' data-id='delete_one' id='"+k+"' ><i class='fa fa-trash '></i> "+trad.delete+"</a></li>";
											}
											if (typeof v["name"] !='undefined' && typeof v["name"] === "object" && notEmpty(v["name"]) && typeof v["name"][costumLang] == "undefined" && k!="#welcome" && dataHelper.getAllLanguageInDb().length > 1) {
						htmlStr += 				`<li class="btn-collapse-translate-content new-feature">
													<a role="button" class="btn btn-collapse-translate" type="button" data-toggle="collapse" data-target="#collapseTranslate${uuid}" aria-expanded="false" aria-controls="collapseTranslate${uuid}">
														<span><i class="fa fa-language"></i>${tradCms.translate}</span><i class="fa fa-chevron-down"></i>
													</a>
													<div class="collapse all-btn-language" id="collapseTranslate${uuid}">
														${costumizer.pages.views.allLanguageSelector(k, pageName, v["name"])}
													</div>
												</li>`;
											}
										"</ul>"+
									"</div>";
						htmlStr+= "</td>"+
								"</tr>";

		}
					
		return htmlStr;
	}

	function getAnswerPath(value) {
		var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
		var children = [];
		var finder = [];
		for(const stepKey in childForm[value] ){
			for(const inputKey in childForm[value][stepKey]){
				var input = childForm[value][stepKey][inputKey];
	
				if(input["type"].includes(".multiCheckboxPlus")){
					children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
				} else if(input["type"].includes(".multiRadio")){
					children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
				}/*else if(input["type"].includes(".radiocplx")){
					children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
				}else if(input["type"].includes(".checkboxcplx")){
					children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
				}else if(input["type"].includes(".checkboxNew")){
					children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
				}else if(input["type"].includes(".radioNew")){
					children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
				}else if(input["type"].includes(".finder")){
					finder.push({value: stepKey+'.finder'+inputKey, label: input["label"]});
				}/* else {
					children.push({value: stepKey+'.'+inputKey, label: input["label"]});
				}  
			}
		}
		return {input: children, finder : finder};
	}
	
	function configFilterAnswers(appKey){
		var filterAnswers = {};
		if(typeof costumizer.obj.app[appKey].filters != "undefined" && typeof costumizer.obj.app[appKey].filters.answers != "undefined"){
			filterAnswers = costumizer.obj.app[appKey].filters.answers;
		}
		var answerData = $.extend(true, {}, filterAnswers);
		var newData = $.extend(true, {}, filterAnswers);
		costumizer.actions.openRightSubPanel(
			"<form id='costum-form-pages' style='padding:0px 10px;'></form>",
			{
				width:500,
				distanceToRight: costumizer.liveChange ? 300:0,
				className:"contains-view-admin",
				header:{
					title:"Configurer le filtre par réponse"
				},
				onInitialized: function(){
					$("#costum-form-pages").append(`<div class="containte-form-page cmsbuilder-tabs-body col-xs-12"></div>`);
					var coformOptions = [];
					var answerPathData = [];
					if(typeof costum["dashboardGlobalConfig"] !="undefined") {
						coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
							return {value: val,label: key}
						})
					}
					var selectedData = "";
					var myCoInput = {
						container : ".containte-form-page",
						inputs :[
							{
								type : "select",
								options: {
									label : tradCms.selectaform ?? "Selectionner un formulaire",
									isSelect2 : true,
									configForSelect2: {
										allowClear: true
									},
									name : "coform",
									class: "coform-answers",
									options : coformOptions.length > 0 ? coformOptions : []
								}
							},
							{
								type : "select",
								options : {
									name : "answerPath",
									isSelect2 : true,
									class: "answerpath-answers",
									label : "Selectionner une question",
									options : answerPathData.length > 0 ? answerPathData : []
								}
							},
							{
								type : "select",
								options : {
									name : "finderPath",
									isSelect2 : true,
									class: "finderpath-answers",
									label : "Selectionner la question pour connaitre l'élément qui a répondu",
									options : []
								}
							},
							{
								type : "inputSimple",
								options : {
									name : "label",
									class: "label-answers",
									label : "Nom du filtre"
								}
							},
							{
								type : "selectMultiple",
								options : {
									name : "answerValue",
									class: "answervalue-answers",
									label : "Valeur à afficher sur le filtre",
									options : []
								}
							},
							{
								type : "selectMultiple",
								options : {
									name : "answerFilter",
									class: "answerfilter-answers",
									label : "Valeur à chercher dans les tags",
									options : []
								}
							},
						],
						onchange:function(name, value, payload){
							if(name == "coform"){
								var answerData = getAnswerPath(value);
								$(".answerpath-answers > select").select2("destroy")
								var str = '<option></option>';
								answerData.input.forEach((element) => {
									str += `<option value="${element.value}">${element.label}</option>`;
								});
								$(".answerpath-answers > select").html(str);
								$(".answerpath-answers > select").select2({});

								$(".finderpath-answers > select").select2("destroy")
								var str = '<option></option>';
								answerData.finder.forEach((element) => {
									str += `<option value="${element.value}">${element.label}</option>`;
								});
								$(".finderpath-answers > select").html(str);
								$(".finderpath-answers > select").select2({});
							}
	
							if(name == "answerPath"){
								var answerValue = getAnswerValue($(".coform-answers > select").val() ,value);
								$(".answervalue-answers > select").select2("destroy")
								$(".answerfilter-answers > select").select2("destroy")
								var str = '<option></option>';
								answerValue.forEach((element) => {
									str += `<option value="${element.value}">${element.label}</option>`;
								});
								$(".answervalue-answers > select").html(str);
								$(".answervalue-answers > select").val(answerValue.map(function(element){
									return element.value;
								}));
								$(".answervalue-answers > select").select2({});

								$(".answerfilter-answers > select").html(str);
								$(".answerfilter-answers > select").select2({});
								
							}
						}
					}
					
					new CoInput(myCoInput);
	
					$("#costum-form-pages > .containte-form-page").append(`<div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;">
						<a href="javascript:;" class="btn btn-primary col-xs-12 " id="btn-add-data"><i class="fa fa-save"></i> Enregistrer le filtre</a>
					</div>`);
					var answerStr = "";
					$.each(answerData, function(k, v){
						answerStr += `
							<tr class="update-config-answers" data-key='${k}' data-answers='${JSON.stringify(v)}'>
								<td>${v.label}</td>
								<td width="10%">
									<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${k}'><i class='fa fa-trash'></i></button>
								</td>
							</tr>
						`;
					})
					$("#costum-form-pages").append(`<div class="cmsbuilder-table-content col-xs-12 no-padding" style="margin-top: 10px">
						<h6>Liste des filtres</h6>
						<table class="table table-bordered" id="liste-filters-answers">
							<tbody>
								${answerStr}
							</tbody>
						</table>
					</div>`);
							
					$("#costum-form-pages #btn-add-data").click(function() {
						if($(".form-control.label-answers").val() && $(".answerpath-answers > select").val()){
							key = selectedData.length > 0 ? selectedData : $(".form-control.label-answers").val().toLowerCase();
							if(typeof newData[key] == "undefined"){
								newData[key] = {
									"label": $(".form-control.label-answers").val(),
									"forms": $(".coform-answers > select").val(),
									"path": $(".answerpath-answers > select").val()
								}
								if($(".answervalue-answers > select").val() != null){
									newData[key].visible = $(".answervalue-answers > select").val();
								}
								if($(".answerfilter-answers > select").val() != null){
									newData[key].filterByTag = $(".answerfilter-answers > select").val();
								}
								if($(".finderpath-answers > select").val() != ''){
									newData[key].finderPath = `answers.${$(".finderpath-answers > select").val()}`;
								}
								$("#liste-filters-answers > tbody").append(`<tr class="update-config-answers" data-key='${key}'>
										<td>${newData[key].label}</td>
										<td width="10%">
											<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${key}'><i class='fa fa-trash'></i></button>
										</td>
									</tr>`);
								resetForm();
							}else{
								if(selectedData.length > 0){
									newData[key] = {
										"label": $(".form-control.label-answers").val(),
										"forms": $(".coform-answers > select").val(),
										"path": $(".answerpath-answers > select").val()
									}
									if($(".answervalue-answers > select").val() != null){
										newData[key].visible = $(".answervalue-answers > select").val();
									}
									if($(".answerfilter-answers > select").val() != null){
										newData[key].filterByTag = $(".answerfilter-answers > select").val();
									}
									$(`#liste-filters-answers > tbody > tr.update-config-answers[data-key='${key}']`).html(
										`<td>${newData[key].label}</td>
										<td width="10%">
											<button class='btn btn-sm btn-remove-config btn-danger' type="button" data-key='${key}'><i class='fa fa-trash'></i></button>
										</td>`
									)
									resetForm();
									mylog.log("New Data", newData);
									selectedData = "";
								}else{
									toastr.error("Il existe déjà un filtre avec ce nom");
								}
							}
							
						}
						$("#costum-form-pages #btn-submit-form").attr('disabled', JSON.stringify(answerData) == JSON.stringify(newData));
					});
					function resetForm(){
						$(".form-control.label-answers").val("");
						$(".coform-answers > select").val("");
						$(".answerpath-answers > select").val("");
						$(".answervalue-answers > select").val([]);
						$(".answerfilter-answers > select").val([]);
						$(".finderpath-answers > select").val('');
						$(".coform-answers > select").trigger("change.select2");
						$(".answerpath-answers > select").trigger("change.select2");
						$(".answervalue-answers > select").trigger("change.select2");
						$(".finderpath-answers > select").trigger("change.select2");
						$(".answerfilter-answers > select").trigger("change.select2");
					}
					$(document).on("click", "#costum-form-pages .update-config-answers", function() {
						var key = $(this).data("key");
						var value = newData[key];
						selectedData = key;
						$(".coform-answers > select").val(value.forms);
						$(".coform-answers > select").trigger("change");
						$(".form-control.label-answers").val(value.label);
						$(".answerpath-answers > select").val(value.path);
						$(".answerpath-answers > select").trigger("change");
	
						if(typeof value.visible != "undefined"){
							$(".answervalue-answers > select").val(value.visible);
							$(".answervalue-answers > select").trigger("change.select2");
						}
						if(typeof value.finderPath != "undefined"){
							$(".finderpath-answers > select").val(value.finderPath.replace("answers.", ""));
							$(".finderpath-answers > select").trigger("change.select2");
						}
						if(typeof value.filterByTag != "undefined"){
							$(".answerfilter-answers > select").val(value.filterByTag);
							$(".answerfilter-answers > select").trigger("change.select2");
						}
					});		
					$(document).on('click', "#costum-form-pages .btn-remove-config", function(e) {
						e.stopImmediatePropangation();
						var key = $(this).data("key");
						delete newData[key];
						$(this).parents("tr").remove();
	
						$("#costum-form-pages #btn-submit-form").attr('disabled', JSON.stringify(answerData) == JSON.stringify(newData));
						
					});
	
					$("#costum-form-pages").append(`<div class="cmsbuilder-tabs-footer"><div class="col-xs-12 no-padding" style="margin-top:15px;margin-bottom:15px;padding-bottom: 50px !important">
						<a href="javascript:;" class="btn btn-danger col-xs-12 " id="btn-submit-form" disabled><i class="fa fa-save"></i> `+tradCms.register+`</a>
					</div></div>`);
					
					$("#costum-form-pages #btn-submit-form").click(function() {
						var dataToSend = costumizer.obj.app;
						if(typeof dataToSend[appKey].filters == "undefined"){
							dataToSend[appKey].filters = {};
						}
						dataToSend[appKey].filters.answers = newData;
						var params = {
							"app": dataToSend
						}
						ajaxPost(
							null,
							baseUrl+"/"+moduleId+"/cms/updatecostum",
							{
								params: params,
								costumId    : costum.contextId,
								costumType  : costum.contextType,
								page : appKey,
								action : "filter/answers",
								subAction : "saveFilters"
							},
							function(data){
								costum.app=jQuery.extend(true,{},costumizer.obj.app);
								toastr.success(tradCms.updateConfig);
								$("#right-sub-panel-container").fadeOut();
							}			
						);
					});
					
				}
			}
		)
	}
	
	function getAnswerValue(coformId, value) {
		var coforme = [];
		if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
			coforme = costum["dashboardGlobalConfig"]["formTL"];
		}
		if(typeof coforme[coformId] != "undefined" ){
			coforme = coforme[coformId];
		}
		var input = value.split(".");
		var answerValue = [];
		if(typeof input[1] != "undefined"){
			input = input[1];
			if(typeof input!="undefined" && ( input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx") ) ){
				if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["global"]){
					for(const paramValue of coforme["params"][input]["global"]["list"]){
						answerValue.push({value: paramValue, label: paramValue})
					}
				}
			}
		
			if( typeof input!="undefined" && ( input.includes("checkboxNew") || input.includes("radioNew") ) ){
				if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["list"]){
					for(const paramValue of coforme["params"][input]["list"]){
						answerValue.push({value: paramValue, label: paramValue})
					}
				}
			}
		}
		return answerValue;
	}
} */