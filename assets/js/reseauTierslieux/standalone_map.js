function getAnswersWithOrga(answerData){
	var results = {};
	var params = {searchedData : {}};
	$.each(answerData, function(k, v){
		params.searchedData[k] = {
			searchType : ["answers"],
			params : {thematicPath: v.path, finderPath: v.finderPath},
			fields : ["answers." + v.path, v.finderPath],
			filters : {["answers." + v.path] : {'$exists': true}},
			indexMin : 0,
			initType : '',
			count : false,
			locality : "",
			fediverse : false,
			sortBy : {updated : -1},
			indexStep : 10000,
			notSourceKey : true,
		}
	});
	ajaxPost(
		null,
		baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk",
		params,
		function (data) {
			$.each(data, function(key, value){
				if (value.count.answers > 0) {
					if(typeof value.results == "object" && Object.keys(value.results).length > 0){
						results[key] = {};
						$.each(value.results, function(k, v){
							results[key][k] = {
								label: k,
								value: v.orgaNameArray
							}
						});
					}
				}
			});
		},
		null,
		"json",
		{ async: false }
	);
	return results;
}

var answerData = typeof appMapConfig!="undefined" && typeof appMapConfig.filterConfig!="undefined" && typeof appMapConfig.filterConfig.filters!="undefined" && typeof appMapConfig.filterConfig.filters.answers !="undefined" ? appMapConfig.filterConfig.filters.answers : null ;
if(notEmpty(answerData)){
    var answersOrga = getAnswersWithOrga(answerData);
	$.each(answerData, function(k,v){
		var answerConfig = answersOrga[k];
		answerConfig =  Object.keys(answerConfig).sort().reduce((obj, key) => {
						obj[key.toLowerCase()] = answerConfig[key];
						return obj;
					}, {});
		if(typeof v.visible != "undefined"){
			if(typeof v.visible == "string"){
				answerConfig = answerConfig[v.visible.toLowerCase()];
			}else if(typeof v.visible == "object"){
				v.visible = v.visible.map(function(k){
					return k.toLowerCase();
				});
				$.each(answerConfig, function(key, value){
					if(!v.visible.includes(key)){
						delete answerConfig[key];
					}
				});
			}
		}
		appMapConfig.filterConfig.filters[k] = {
			name : v.label,
			view : "dropdownList",
			type : "valueArray",
			field : "name",
			event : "inArray",
			typeList : "object",
			list : answerConfig,
		};
	});
}
