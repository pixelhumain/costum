adminPanel.views.importsiret = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importsiret/', {}, function(){},"html");
};

adminPanel.views.organizations = function(){
	var data={
		title : "Les organisations !",
		table : {
            name : {
                name : "Nom"
            },
            siret: {
                name : "SIRET"
            },
            waldec : {
                name : "WALDEC"
            },
            email : {
                name : "E-mail"
            },
            famille : {
                name : "Famille"
            },
            arrondissement : {
                name : "Arrondissement"
            },
            tags : { 
                name : "Mots clés" 
            }
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations" ]
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
        	delete : true
        },
        csv : [
            {
                url : baseUrl+'/co2/export/csv/',
                defaults : {
                        indexStep : 0,
                        fields : [
                            "name", "siret", "siren", "sigle", "waldec", "email", "famille", "arrondissement", "interco", "apet700", "secteurEtablissement", "typeEtablissement", "telephone.fixe", "telephone.mobile", "url", "shortDescription", "description", "tags","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","ambassadeurName","ambassadeurFirstName","ambassadeurTel","ambassadeurMail","ambassadeurFonction", "socialNetwork.facebook","socialNetwork.twitter","socialNetwork.instagram"
                        ]
                }
            }
        ]
	};
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.importorga = function(){
	ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importorga/', {}, function(){},"html");
};

adminDirectory.values.siren = function(e, id, type, aObj){
    mylog.log("adminDirectory.values siren", e, id, type, aObj);
    var str = "";
    if( typeof e.siren != "undefined"){
        str = e.siren;
    }
    return str;
};

adminDirectory.values.siret = function(e, id, type, aObj){
    mylog.log("adminDirectory.values siret", e, id, type, aObj);
    var str = "";
    if( typeof e.siret != "undefined"){
        str = e.siret;
    }
    return str;
};

adminDirectory.values.famille = function(e, id, type, aObj){
    mylog.log("adminDirectory.values famille", e, id, type, aObj);
    var str = "";
    if( typeof e.famille != "undefined"){
        str = e.famille;
    }
    return str;
};

adminDirectory.values.arrondissement = function(e, id, type, aObj){
    mylog.log("adminDirectory.values arrondissement", e, id, type, aObj);
    var str = "";
    if( typeof e.arrondissement != "undefined"){
        str = e.arrondissement;
    }
    return str;
};

adminDirectory.values.secteurEtablissement = function(e, id, type, aObj){
    mylog.log("adminDirectory.values secteurEtablissement", e, id, type, aObj);
    var str = "";
    if( typeof e.secteurEtablissement != "undefined"){
        str = e.secteurEtablissement;
    }
    return str;
};

adminPanel.views.diff = function(){
    var data={
        title : "Les organisations à modéré",
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
                types : [ "organizations" ],
                filters : {
                      "source.toBeValidated.cressReunion" : { '$exists' : 1 }
                }
            },
            filters : {
                text : true
            
            }
        },
        table : {
            name : {
                name : "Nom"
            },
            siret: {
                name : "SIRET"
            },
            waldec : {
                name : "WALDEC"
            },
            email : {
                name : "E-mail"
            },
            famille : {
                name : "Famille"
            },
            arrondissement : {
                name : "Arrondissement"
            },
            validated : { 
                name : "Valider",
                class : "col-xs-2 text-center"
            }
        },
        actions : {
            update : true,
            delete : true,
            validated : true
        }
    };
    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

// adminPanel.views.openform = function(){
//     var idF = null;
//     if(typeof costum.forms != "undefined"){
//         $.each(costum.forms, function(kF, vF){
//             idF = kF;
//         });
//     }

//     if(idF != null)
//         ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/'+idF, {}, function(){},"html");
// };

adminPanel.views.listforms = function(){
    //ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/', {}, function(){},"html");

    var data={
        title : "Gestion des formulaires",
        id : costum.contextId,
        collection : costum.contextType,
        slug : costum.contextSlug,
        url : baseUrl+'/survey/form/admindirectory/slug/'+costum.contextSlug,
        table : {
            name: {
                name : "Form"
            }
        }
    };
    if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
        || (typeof canEdit != "undefined" && canEdit) ){
        data.actions={};
    }
            
    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};

adminPanel.views.candidatures = function(){
    var data={
        title : "Gestion des dossiers",
        id : costum.contextId,
        collection : costum.contextType,
        slug : costum.contextSlug,
        url : baseUrl+'/survey/answer/admindirectory/slug/'+costum.contextSlug,
        table : {
            name: {
                name : "Dossier"
            },
            comment : {
                name : "Commentaires"
            }
        },
        csv : {
            post : [
                {
                    url : baseUrl+'/co2/export/csvelement/type/answers/slug/cressReunion/'
                }
            ]
        }
    };
    if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
        || (typeof canEdit != "undefined" && canEdit) ){
        data.actions={
            pdf : true,
            deleteAnswer: true
        };
    }
            
    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
};