function action_modal(W, $, php) {
	var empty_description_text = "**(Aucune description)**";
	var socket_handlers = {
		".action-order": function (data) {
			var after_dom;
			if (data.emiter === W.wsCO.id && data.component === 'panel')
				return (true);
			var moved_dom = this.list.find(".action-thumb[data-id=" + data.moved + "]");
			if (!moved_dom.length)
				return (true);

			function put_at_end_of_group(group, moved) {
				var after_dom = this.list.find(".action-status-group[data-status=" + group + "]");
				after_dom = after_dom.next();
				if (!after_dom.hasClass('action-thumb'))
					return (after_dom.before(moved_dom))
				while (after_dom.length && !after_dom.next().hasClass('action-status-group') && !after_dom.next().hasClass('action-status-referer'))
					after_dom = after_dom.next();
				if (after_dom.length)
					after_dom.after(moved);
			}
			// Mettre avant le 'after'
			if (data.after !== null) {
				after_dom = this.list.find(".action-thumb[data-id=" + data.after + "]");
				// si les deux éléments se trouvent dans le même groupe
				if (after_dom.data("view-status") === data.group)
					after_dom.before(moved_dom);
				else
					put_at_end_of_group.call(this, data.group, moved_dom);
			} else
				put_at_end_of_group.call(this, data.group, moved_dom);
			do_update_action_count.call(this);
		},
		'.action-image': function (data) {
			if (data.emiter === W.wsCO.id)
				return (true);
			var action_thumb = this.list.find('.action-thumb[data-id=' + data.action + ']');
			if (action_thumb.length)
				request_get_last_image(data.action).then(function (p_image) {
					action_thumb.find("img")
						.attr("src", p_image.url)
						.attr("alt", p_image.name);
				});
			if (action_thumb.hasClass('selected') && data.images.length > 0) {
				var image_dom = $(".image-compact-list");
				image_dom.empty();
				$.each(data.images, function (i, image) {
					image_dom.append(template_image(data.action, {
						src: image.url,
						alt: image.name
					}));
				})
			}
		},
		'action-archive': function (data) {
			var thumb_dom = $('.action-thumb[data-id="' + data.id + '"]');
			if (thumb_dom.length)
				thumb_dom.remove();
		}
	};
	socket_handlers['.set-contributors' + php.parent_id] = function (data) {
		var thumb_dom = $('.action-thumb[data-id="' + data.action + '"]');
		if (thumb_dom.hasClass('selected')) {
			do_load_contributors(data.action);
		}
	};
	var searchTimeout;
	var idCostum;
	var typeCostum;
	if (contextData != null && typeof contextData != 'undefined') {
		idCostum = contextData.id;
		typeCostum = contextData.type;
		slugCostum = contextData.slug;
	} else if (typeof W.costum != 'undefined' && W.costum !== null) {
		if (typeof W.costum.contextType != 'undefined') {
			idCostum = W.costum.contextId;
			typeCostum = W.costum.contextType;
			slugCostum = W.costum.contextSlug;
		}
	}
	php.milestones.map(function (milestone) {
		$(".milestone-dropdown .milestone[href=" + milestone.milestoneId + "]").data(milestone).data("all", milestone);
	});

	function request_get_last_image(id) {
		return new Promise(function (resolve) {
			var url = php.server_url + "/costum/project/action/request/last_image_of";
			ajaxPost(null, url, {
				id: id
			}, resolve);
		})
	}

	function request_get_tags_list(id) {
		return new Promise(function (resolve) {
			var url = php.server_url + "/costum/project/action/request/get_tags";
			ajaxPost(null, url, {
				id: id
			}, resolve);
		});
	}

	function template_image(action, image) {
		if (typeof action === "undefined")
			return ($("<div>")
				.addClass("image-element")
				.addClass("loading")
				.append(
					$("<span>")
						.addClass("fa")
						.addClass("fa-spinner")
						.addClass("fa-spin")
				));
		return ($("<div>")
			.addClass("image-element")
			.append(
				$("<a>")
					.attr("href", php.server_url + "/" + image.src)
					.attr("data-lightbox", action)
					.append(
						$("<img>")
							.attr("src", php.server_url + "/" + image.src)
							.attr("alt", image.alt)
					)
			));
	}

	function do_filter_action_list() {
		var search_value = this.list.find('.filter .search');
		var checked_status = [];
		var filtered_tags = [];
		search_value = search_value.length && search_value.val() ? search_value.val() : "";
		search_value = search_value
			.toLowerCase()
			.normalize("NFD")
			.replace(/[\u0300-\u036f]/g, "");

		this.filter.find("[name=status-filter]").each(function () {
			if (this.checked)
				checked_status.push(this.value);
		});
		filtered_tags = this.filter.find("[name=tags]").val() ? this.filter.find("[name=tags]").val() : [];
		this.list.find(".action-filter-tags")
			.empty()
			.html(filtered_tags.map(function (tag) {
				return ('<span class="badge" data-tag="' + tag + '"> <i class="fa fa-times"></i> ' + tag + '</span>');
			}).join(""));
		$(".action-tags .badge").each(function () {
			var self = $(this);
			if (filtered_tags.includes(self.data("tag")))
				self.css("background-color", "#337ab7");
			else
				self.css("background-color", "");
		});
		var milestone = this.filter.find("[name=milestones]").val();
		var thumb_dom = this.list.find(".action-thumb");
		thumb_dom.each(function () {
			var self = $(this);
			var action_name = self.text().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
			var action_status = self.data('status').toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
			var status_in_arg = !checked_status.length || checked_status.includes(action_status.substr(1));
			var action_in_tag = !filtered_tags.length || filtered_tags.some(function (tag) {
				return (self.data("tags").includes(tag));
			});
			var action_in_milestone = !milestone.length || self.data("milestone") === milestone;

			if (action_in_tag && status_in_arg && (action_name.includes(search_value) || action_status.includes(search_value)) && action_in_milestone)
				self.css('display', '');
			else
				self.hide();
		});
		do_update_action_count.call(this);
		setTimeout(do_reload_next_status_button.bind(this.list), 500);
	}

	function request_save_contributors(id, ids) {
		return new Promise(function (resolve) {
			var url = php.server_url + '/costum/project/action/request/set_contributors';
			ajaxPost(null, url, {
				action: id,
				contributors: ids
			}, resolve);
		});
	}

	function do_update_data_status(id, status) {
		var self = $('.action-thumb[data-id=' + id + ']');
		if (self.length === 0)
			return (true);
		var tags = self.data("tags");
		var tag_vars = ["discuter", "next", "totest"];
		self.data("status", "#" + status);
		$.each(tag_vars, function (i, tag) {
			if (tags.includes(tag)) {
				index = tags.indexOf(tag);
				tags.splice(index, 1);
			}
		});
		if (tag_vars.includes(status))
			tags.push(status);
		self.data("tags", tags.slice());
		// update tags
		self.data('view-status', status);
	}

	function request_get_action(id) {
		return new Promise(function (resolve) {
			var url = php.server_url + '/costum/project/action/request/action_basic_info';
			ajaxPost(null, url, {
				id: id,
				user: php.user
			}, resolve);
		});
	}

	function request_get_contributors(id) {
		return new Promise(function (resolve) {
			var url = php.server_url + "/costum/project/action/request/all_contributors";
			ajaxPost(null, url, {
				id: id
			}, resolve);
		});
	}
	
	
	let contribute_timeout;
	function do_save_selected_contributor() {
		const ids = [];
		const action = $(".action-thumb.selected").data("id");
		
		$(".contributor-selector .contributor-list .contributor-container.selected").each(function () {
			ids.push($(this).data("contributor").id);
		});
		request_save_contributors(action, ids).then(function () {
			do_load_contributors(action);
		});
	}

	function do_load_contributors(id) {
		$('.btn-participer span').addClass('fa-user-plus').removeClass('fa-user-times')
		$('.btn-participer').prop('title', 'Participer')
		request_get_contributors(id).then(function (contributors) {
			var contributor_list_dom = $(".contributor-selector .contributor-list");
			var contributor_list_save_dom = $(".contributor-save");
			contributor_list_dom.removeClass("text-only").empty();
			contributor_list_save_dom.css("display", "");
			$(".action-contributors .contributors-container").empty();
			contributors.map(function (contributor) {
				var tooltip_content = {
					title: contributor.name,
					container: ".action-modal",
					placement: "bottom"
				};
				if (userConnected && contributor.action_contributor === true && contributor.id === userConnected._id.$id) {
					$('.btn-participer span').removeClass('fa-user-plus').addClass('fa-user-times')
					$('.btn-participer').prop('title', 'ne plus participer')
				}
				if (contributor.action_contributor) {
					var contributor_item_dom = $("<div>")
						.addClass("contributor-container")
						.append(
							$("<img>")
								.attr("src", contributor.image)
								.attr("alt", contributor.name)
						);
					contributor_item_dom.tooltip(tooltip_content);
					$(".action-contributors .contributors-container").append(contributor_item_dom);
				}
				var contributor_image_dom = $("<div>")
					.data("contributor", contributor)
					.addClass("contributor-container")
					.append(
						$("<img>")
							.attr("src", contributor.image)
							.attr("alt", contributor.name)
					).tooltip(tooltip_content);
				if (contributor.action_contributor)
					contributor_image_dom.addClass("selected");
				contributor_list_dom.append(contributor_image_dom);
				contributor_image_dom.on("click", function () {
					clearTimeout(contribute_timeout);
					contribute_timeout = setTimeout(do_save_selected_contributor, 500);
					$(this).toggleClass("selected");
				});
			});
			if (contributor_list_dom.children().length === 0) {
				contributor_list_dom
					.addClass("text-only")
					.append(
						$("<span>")
							.addClass("empty-contributor-list")
							.text("Le parent de l'action ne contient aucun contributeur")
					);
				contributor_list_save_dom.css("display", "none");
			}
		});
	}

	function do_load_status_bar(action) {
		var self = this;
		var empty_date_preview = "__/__/____ __:__";
		var start_date_preview = empty_date_preview;
		var end_date_preview = empty_date_preview;
		var moment_format = "DD/MM/YYYY HH:mm";
		var status_text;
		if (action.status && action.status === "done")
			status_text = coTranslate("act.done");
		else if (action.tags && Array.isArray(action.tags) && action.tags.includes("totest"))
			status_text = coTranslate("act.totest").ucfirst();
		else if (action.tracking)
			status_text = coTranslate("act.tracking").ucfirst();
		else if (action.tags && Array.isArray(action.tags) && action.tags.includes("next"))
			status_text = coTranslate("act.next").ucfirst();
		else if (action.tags && Array.isArray(action.tags) && action.tags.includes("discuter"))
			status_text = coTranslate("act.discuter").ucfirst();
		else
			status_text = coTranslate("act.todo").ucfirst();
		if (!action.start_date) {
			action_duration_html = "";
			action_duration_class = "undefined-status";
		}
		if (action.start_date) {
			var moment_start = moment(action.start_date);
			start_date_preview = moment_start.format(moment_format);
			if (moment().isAfter(action.start_date))
				action_duration_class = "started-status";
			else
				action_duration_class = "coming-status";
		}
		$(".duration-status .dropdown-toggle").text(status_text);
		if (action.end_date) {
			var moment_end = moment(action.end_date);
			end_date_preview = moment_end.format(moment_format);
			if (moment_end.isValid())
				end_date_preview = moment_end.format(moment_format);
			if (moment_end.isValid() && moment_end.isBefore(moment()))
				action_duration_class = "ended-status";
		}
		$(".duration-status")
			.removeClass([
				"undefined-status",
				"started-status",
				"coming-status",
				"ended-status"
			].join(" "));
		// .addClass(action_duration_class);
		$(".action-duration .start .date-preview").empty().text(start_date_preview);
		$(".action-duration .start .hidden-datepicker").val(start_date_preview === empty_date_preview ? "" : start_date_preview);
		$(".action-duration .end .date-preview").empty().text(end_date_preview);
		$(".action-duration .end .hidden-datepicker").val(end_date_preview === empty_date_preview ? "" : end_date_preview);
		request_get_tags_list(action.id).then(function (p_tags) {
			filtered_tags = self.filter.find("[name=tags]").val() ? self.filter.find("[name=tags]").val() : [];
			var action_tag_dom = $(".action-tags");
			action_tag_dom.empty();
			p_tags.map(function (m_tag) {
				action_tag_dom.append(
					$("<span>")
						.addClass("badge")
						.text("#" + m_tag)
						.attr("data-tag", m_tag)
						.css("background-color", filtered_tags.includes(m_tag) ? "#337ab7" : ""),
					" "
				);
			});
		});
	}

	function request_add_image_and_file(id) {
		var url = php.server_url + '/costum/project/action/request/add_image_file';
		var post = {
			action: id
		};
		ajaxPost(null, url, post,
			function (data) { }, null, null, {
			async: false
		});
	}

	function update_milestone_view(id) {
		$(".milestone-dropdown .selected-milestone").text(coTranslate("Milestone"));
		$(".milestone-dropdown .dropdown-menu>li").removeClass("active");
		if (typeof id === "string" && id) {
			var current = php.milestones.find(function (milestone) {
				return (milestone.milestoneId === id);
			});
			if (typeof current !== "undefined") {
				$(".milestone-dropdown .selected-milestone").text(current.name);
				$(".milestone-dropdown li[data-id=" + id + "]").addClass("active");
			}
		}
	}

	function do_load_action(id) {
		var self = this;

		do_load_contributors(id);
		request_get_action(id).then(function (action) {
			$('.action-detail.co-scroll').data('id', action.id);
			$('.action-modal .modules-container').empty().html('');
			// vérifier si l'utilisateur peut éditer
			if (action.editable) {
				$(".for-admin").each(function () {
					var self = $(this);

					if (self.hasClass("not-admin-hide"))
						self.css("display", "");
					else if (self.hasClass("not-admin-disable"))
						self.removeClass("disabled")
							.removeAttr("disabled");
				});
			} else {
				$(".for-admin").each(function () {
					var self = $(this);

					if (self.hasClass("not-admin-hide"))
						self.css("display", "none");
					else if (self.hasClass("not-admin-disable"))
						self.addClass("disabled").prop("disabled", true);
				});
			}
			if (php.milestones.length === 0)
				$('.milestone-dropdown>.dropdown-toggle').prop('disabled', true);
			if (action.has_tasks)
				do_open_task_view.call(self, id);
			$('.action-modal-name').text(': ' + action.name);
			update_milestone_view(action.milestone);
			var action_duration_class;
			var moment_end;
			$('.action-detail .action-title').empty().html(action.name);
			var creator_img_dom = $(".action-detail .creator-image img");
			creator_img_dom
				.attr("src", action.creator.image)
				.attr("alt", action.creator.name);
			if (creator_img_dom.data("bs.tooltip"))
				creator_img_dom.tooltip('destroy');
			$(".action-description").empty().html(action.description ? action.description : dataHelper.markdownToHtml(empty_description_text));
			$(".image-compact-list").empty();
			action.images.map(function (image) {
				$(".image-compact-list").append(template_image(action.id, image));
			});
			$(".credit-field").empty();
			if (action.credits) {
				$(".credit-field").text("Crédit " + action.credits);
			} else {
				$(".credit-field").text("Aucun crédit accordé");
			}
			do_load_status_bar.call(self, action);
			setTimeout(function () {
				$('.action-detail .creator-image img').tooltip({
					title: action.creator.name,
					container: ".action-modal",
					placement: 'left'
				});
			}, 200);
		});
	}

	/**
	 * Enregistrer la liste des images sélectionnées
	 * @param {string[]} images liste des images à enregistrer
	 * @return {Promise<{status: boolean, filename: stirng, [url]: string, msg: string}[]>}
	 */
	function request_save_image(images) {
		return new Promise(function (resolve) {
			var url = `${php.server_url}/costum/project/action/request/add_image_media`;
			var post = {
				action: $(".action-thumb.selected").data("id"),
				images: images
			};
			ajaxPost(null, url, post, resolve);
		});
	}

	function do_upload_files(file_list) {
		$.each(file_list, function (_, file_item) {
			var file_reader = new FileReader();
			var target_image_dom = template_image();
			$(".image-compact-list").append(target_image_dom);
			file_reader.onload = function (reader_load) {
				request_save_image([reader_load.target.result]).then(function (message_list) {
					var error_message = '';
					var selected_thumb_dom = $(".action-thumb.selected");
					var action_id = selected_thumb_dom.data("id");
					var badge_dom = $('a[href="#imagesTab"] .badge');
					$.each(message_list, function (_, message_item) {
						if (message_item.status) {
							badge_dom.text(parseInt(badge_dom.text(), 10) + 1);
							var loaded_image = template_image(action_id, {
								src: message_item.url,
								alt: message_item.name
							});
							target_image_dom.empty().html('');
							target_image_dom.removeClass('loading').html(loaded_image.html());
							request_get_last_image(action_id).then(function (last_image) {
								selected_thumb_dom.find("img")
									.attr("src", last_image.url)
									.attr("alt", last_image.name);
							});
						} else
							error_message += message_item.msg + '<br>';
					});
					if (notEmpty(error_message)) {
						toastr.error(error_message);
						target_image_dom.remove();
					}
				});
			};
			file_reader.readAsDataURL(file_item);
		});
	}

	function do_reload_next_status_button() {
		var next_state_button;
		var visible_button = this.find(".action-status-group").css("top", this.find(".filter").outerHeight(true) + "px").filter(function () {
			return $(this).is(":visible") && $(this).position().top <= $(this).parent().height();
		});
		if (visible_button.length) visible_button = visible_button.eq(visible_button.length - 1);
		else visible_button = null;
		if (visible_button) next_state_button = visible_button.nextAll(".action-status-group").filter(function () {
			return $(this).is(":visible");
		}).first();
		if (next_state_button && next_state_button.length)
			this.find(".action-status-referer")
				.css("display", "")
				.text(next_state_button.text())
				.attr('data-status', next_state_button.data('status'))
				.data("target", next_state_button);
		else
			this.find(".action-status-referer")
				.hide()
				.removeData("target");
	}

	function dom_action_thumb(args) {
		var container = $("<div>")
			.addClass("action-thumb")
			.attr("data-id", args.id)
			.append(
				$('<div>')
					.addClass('actions')
					.html('<i class="fa fa-arrows dragger for-admin not-admin-hide"></i>'),
				$("<div>")
					.addClass("image")
					.append(
						$("<img>")
							.attr("alt", args.text)
							.attr("src", defaultImage)
					),
				$("<div>")
					.addClass("title")
					.text(args.text)
			).data({
				tags: args.tags ? args.tags : [],
				status: "#" + args.status,
				viewStatus: args.status,
				id: args.id,
				milestone: null
			});
		container.tooltip({
			title: args.text,
			placement: 'right',
			container: ".action-modal",
			html: true
		});
		return (container);
	}

	function dom_create_action_modal() {
		var statuses = ["discuter", "next", "todo", "tracking", "totest", "done"];
		var fields = {
			name: $("<input>")
				.addClass("form-control")
				.attr("name", "name"),
			min: $("<input>")
				.addClass("form-control")
				.attr("name", "min")
				.val(1),
			max: $("<input>")
				.addClass("form-control")
				.attr("name", "max")
				.val(1),
			is_contributor: $("<input>")
				.attr("type", "checkbox")
				.attr("name", "is_contributor")
				.prop("checked", true),
			status: $("<select>")
				.addClass("form-control")
				.attr("name", "status")
				.append(statuses.map(function (status) {
					return ($("<option>")
						.attr("value", status)
						.text(trad["act." + status].ucfirst()))
				})),
			milestone: $("<select>")
				.addClass("form-control")
				.attr("name", "milestone")
				.append($("<option>")
					.attr("value", "")
					.prop("selected", true))
				.append(php.milestones.map(function (milestone) {
					return ($("<option>")
						.attr("value", milestone.milestoneId)
						.text(milestone.name));
				}))
		};
		return (
			$("<div>")
				.addClass("modal-dialog")
				.append(
					$("<div>")
						.addClass("modal-content")
						.append(
							$("<div>")
								.addClass("modal-body")
								.append(
									$("<form>")
										.attr("autocomplete", "off")
										.append(
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Nom de l'action"),
													fields.name,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.text("Status"),
													fields.status
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.attr("for", "")
														.text("Jalon"),
													fields.milestone,
													$("<span>")
														.addClass("help-block")
												)
											,
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.attr("for", "")
														.text("Nombre minimal de contributeur"),
													fields.min,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("form-group")
												.append(
													$("<label>")
														.attr("for", "")
														.text("Nombre maximal de contributeur"),
													fields.max,
													$("<span>")
														.addClass("help-block")
												),
											$("<div>")
												.addClass("checkbox")
												.append(
													$("<label>")
														.append(
															fields.is_contributor,
															"Je participe à cette action"
														)
												),
											$("<button>")
												.addClass("btn")
												.addClass("btn-primary")
												.attr("type", "submit")
												.text("Valider")
										).on("submit", function (e) {
											e.preventDefault();
											var valid = true;
											var value;
											var key;

											for (key in fields) {
												fields[key].nextAll(".help-block").text("");
												fields[key].parents(".form-group").removeClass("has-error");
											}
											if (!fields.name.val()) {
												valid = false;
												fields.name.nextAll(".help-block").text("Ce champ est requis");
												fields.name.parents(".form-group").addClass("has-error");
											}
											if (!fields.min.val()) {
												valid = false;
												fields.min.nextAll(".help-block").text("Ce champ est requis");
												fields.min.parents(".form-group").addClass("has-error");
											} else if (!fields.min.val().toString().match(/^[0-9]{1,}$/)) {
												valid = false;
												fields.min.nextAll(".help-block").text("Veuillez indiquer un nombre valide et supérieur à 0");
												fields.min.parents(".form-group").addClass("has-error");
											}
											if (!fields.max.val()) {
												valid = false;
												fields.max.nextAll(".help-block").text("Ce champ est requis");
												fields.max.parents(".form-group").addClass("has-error");
											} else if (!fields.max.val().toString().match(/^[0-9]{1,}$/)) {
												valid = false;
												fields.max.nextAll(".help-block").text("Veuillez indiquer un nombre valide et supérieur à 0");
												fields.max.parents(".form-group").addClass("has-error");
											} else if (!isNaN(fields.min.val()) && fields.max.val() < fields.min.val()) {
												valid = false;
												fields.max.nextAll(".help-block").text("Le nombre de participant maximal ne peut être inférieur au nombre de participant minimal");
												fields.max.parents(".form-group").addClass("has-error");
											}
											if (valid) {
												value = {
													name: fields.name.val(),
													parentId: php.parent_id,
													parentType: php.parent_type,
													idParentRoom: php.parent_room,
													idParentRoom: php.parent_room,
													status: fields.status.val(),
													min: fields.min.val(),
													max: fields.max.val(),
													is_contributor: fields.is_contributor.is(':checked'),
													component: 'panel'
												};
												if (fields.milestone.val()) {
													value.milestone = php.milestones.find(function (m) {
														return (m.milestoneId === fields.milestone.val());
													});
													if (value.milestone.startDate)
														value.milestone.startDate = moment.unix(value.milestone.startDate.sec).format();
													if (value.milestone.endDate)
														value.milestone.endDate = moment.unix(value.milestone.endDate.sec).format();
												}
												if (W.wsCO && W.wsCO.id)
													value.emiter = W.wsCO.id;
												coInterface.actions.request_create_action(value).then(function (response) {
													var id = response._id.$id;
													var action_thumb = dom_action_thumb({
														id: id,
														text: fields.name.val(),
														status: value.status,
														other: {}
													});
													var status_group = $(".action-status-group[data-status=" + fields.status.val() + "]");
													var last_thumb_dom = $('.action-thumb').filter(function () {
														return ($(this).data('view-status') === fields.status.val());
													}).last();
													if (last_thumb_dom.length)
														last_thumb_dom.after(action_thumb);
													else
														status_group.after(action_thumb);
													action_thumb.trigger("click");
													$(".create-action-modal").modal("hide");
													coInterface.actions.update_action_position({
														action: id,
														group: fields.status.val(),
														emiter: W.wsCO && W.wsCO.id ? W.wsCO.id : null,
														component: 'panel',
														server_url: php.server_url
													}).then();
												});
											}
										})
								)
						)
				)
		);
	}

	function do_get_drag_after(container, y) {
		var draggable_dom = container.find(".action-thumb:not(.dragging),.action-status-group").toArray();
		var after = draggable_dom.reduce((closest, child) => {
			const box = child.getBoundingClientRect()
			const offset = y - box.top - box.height / 2
			if (offset < 0 && offset > closest.offset) {
				return {
					offset: offset,
					element: child
				}
			} else {
				return closest
			}
		}, {
			offset: Number.NEGATIVE_INFINITY,
			element: null
		});
		return ($(after.element));
	}

	function do_click_action(id) {
		$(".action-thumb").filter(function () {
			return ($(this).data("id") === id);
		}).trigger("click");
	}

	function do_init_socket(args) {
		var self = this;
		var socketConfigurationEnabled;
		if (typeof args !== "object")
			return (false);
		socketConfigurationEnabled = args.coWsConfig.enable && args.coWsConfig.serverUrl && args.coWsConfig.pingActionManagement;
		if (!socketConfigurationEnabled) {
			return (false);
		}
		args.wsCO = args.wsCO && args.wsCO.connected ? args.wsCO : (args.io ? args.io.connect(args.coWsConfig.serverUrl) : null);
		if (!args.wsCO)
			return (false);
		$.each(Object.keys(socket_handlers), function (i, event) {
			var fn = socket_handlers[event].bind(self);
			args.wsCO.on(event, fn);
			$(".action-modal").on("hide.bs.modal", function () {
				args.wsCO.off(event, fn);
			})
		});
	}

	function do_update_action_count() {
		var self = this;
		self.list.find('.action-status-group').each(function () {
			var group_dom = $(this);
			var length = self.list.find('.action-thumb').filter(function () {
				return ($(this).css('display') !== 'none' && $(this).data('view-status') === group_dom.data('status'));
			}).length;
			group_dom.text(group_dom.text().replace(/\((\d{1,})\)/, function (match, occ1) {
				return ('(' + length + ')');
			}));
		});
	}

	function handle_key_navigation() {
		var request_delay = -1;
		var self = this;
		return (function (e) {
			var event = e.originalEvent;
			var action_thumb = $('.action-thumb.selected.focused');
			if (event.key.toLowerCase() === "escape")
				$('.action-modal').modal("hide");
			else if (['ArrowUp', 'ArrowDown'].includes(event.key) && action_thumb.length > 0) {
				e.preventDefault();
				clearTimeout(request_delay);
				var next_dom = $('.action-thumb.selected.focused');
				var status_str = next_dom.data("view-status");
				var move_function;
				if (event.key === 'ArrowUp')
					move_function = (revert) => revert ? next_dom.next() : next_dom.prev();
				else
					move_function = (revert) => revert ? next_dom.prev() : next_dom.next();
				if (event.ctrlKey) {
					var after = null;
					var status;
					var id = action_thumb.data('id');
					var first_status_group_dom = self.list.find('.action-status-group:first');
					next_dom = move_function();
					// trouver la prochaine carte après celle sélectionné
					while (next_dom.css('display') === 'none')
						next_dom = move_function();
					if (next_dom.hasClass("action-status-group"))
						next_dom = move_function(true);
					if (next_dom.is(action_thumb))
						return (request_delay);
					if (next_dom.is(first_status_group_dom))
						next_dom.after(action_thumb);
					else if (event.key === 'ArrowUp')
						next_dom.before(action_thumb);
					else
						next_dom.after(action_thumb);
					next_dom = action_thumb.next();
					while (next_dom.length > 0 && !next_dom.hasClass('action-thumb'))
						next_dom = next_dom.next();
					if (next_dom.hasClass('action-thumb'))
						after = next_dom.data('id');
					next_dom = action_thumb.prev();
					while (next_dom.length > 0 && !next_dom.hasClass('action-status-group'))
						next_dom = next_dom.prev();
					status = next_dom.data('status');
					do_update_data_status(id, status);
					do_update_action_count.call(self);
					do_center_thumb(action_thumb);
					request_delay = setTimeout(function () {
						coInterface.actions.request_set_status({
							id: id,
							status: status,
							server_url: php.server_url,
							user: userId
						}).then(function () {
							request_get_action(id).then(function (p_action) {
								do_load_status_bar.call(self, p_action);
							});
						});
						coInterface.actions.update_action_position({
							action: id,
							after: after,
							group: status,
							emiter: wsCO && wsCO.id ? wsCO.id : null,
							component: 'panel',
							server_url: php.server_url
						}).then();
					}, 500);
					return (request_delay);
				}
				var next_dom = move_function();
				while (next_dom.length && (!next_dom.hasClass('action-thumb') || next_dom.css('display') === 'none'))
					next_dom = move_function();
				if (next_dom.hasClass('action-thumb')) {
					action_thumb.removeClass('selected').removeClass('focused');
					next_dom.addClass('selected').addClass('focused');
					action_thumb = next_dom;
					do_center_thumb(action_thumb);
				}
				request_delay = setTimeout(function () {
					action_thumb.trigger('click');
					if (action_thumb.data('id') !== $('.action-detail.co-scroll').data('id'))
						do_load_action.call(self, action_thumb.data('id'));
				}, 500);
			}
			return (request_delay);
		}).bind(this);
	}

	function do_get_thumb_hidden_y(thumb) {
		if (!(thumb instanceof $))
			thumb = $(thumb);
		var container_dom = thumb.parents('.action-list.co-scroll');
		var reference_top_dom = container_dom.find('.action-status-group:visible:first');
		var reference_bottom_dom = container_dom.find('.action-status-referer');
		var bounding_rect = {
			container: $.extend({}, container_dom.get(0).getBoundingClientRect()),
			reference_top: $.extend({}, reference_top_dom.get(0).getBoundingClientRect()),
			reference_bottom: $.extend({}, reference_bottom_dom.get(0).getBoundingClientRect()),
			thumb: $.extend({}, thumb.get(0).getBoundingClientRect())
		};
		if (bounding_rect.reference_bottom.top === 0)
			bounding_rect.reference_bottom.top = bounding_rect.container.top + container_dom.height();
		if (bounding_rect.reference_top.top + reference_top_dom.outerHeight(true) > bounding_rect.thumb.top)
			return (-1);
		if (bounding_rect.thumb.top + thumb.outerHeight(true) > bounding_rect.reference_bottom.top)
			return (1);
		return (0);
	}

	function do_center_thumb(thumb, animate) {
		var animation = typeof animate === 'boolean' ? animate : false;
		if (!(thumb instanceof $))
			thumb = $(thumb);
		if (!thumb.hasClass('action-thumb') && !thumb.hasClass('ghost-element'))
			return (true);
		var thumb_offset = do_get_thumb_hidden_y(thumb);
		if (thumb_offset === 0)
			return (true);
		var container_dom = thumb.parents('.action-list.co-scroll');
		var reference_top_dom = container_dom.find('.action-status-group:visible:first');
		var reference_bottom_dom = container_dom.find('.action-status-referer');
		var bounding_rect = {
			container: $.extend({}, container_dom.get(0).getBoundingClientRect()),
			reference_top: $.extend({}, reference_top_dom.get(0).getBoundingClientRect()),
			reference_bottom: $.extend({}, reference_bottom_dom.get(0).getBoundingClientRect()),
			thumb: $.extend({}, thumb.get(0).getBoundingClientRect())
		};
		if (bounding_rect.reference_bottom.top === 0)
			bounding_rect.reference_bottom.top = bounding_rect.container.top + container_dom.height();
		var thumb_relative = bounding_rect.thumb.top - bounding_rect.container.top;
		var start = (bounding_rect.reference_top.top - bounding_rect.container.top) + reference_top_dom.outerHeight(true);
		if (thumb_offset > 0)
			start = bounding_rect.reference_bottom.top - bounding_rect.container.top - thumb.outerHeight(true);
		var scroll = thumb_relative - start + container_dom.get(0).scrollTop;
		container_dom.scrollTop(scroll);
	}

	function do_open_task_view(id) {
		var module_container_dom = $('.action-modal .modules-container').empty().html('');
		var url = php.server_url + '/costum/project/action/request/task_html/';
		var action;
		if (typeof id === 'string')
			action = id;
		else
			action = this.list.find(".action-thumb.selected").data("id");
		var post = {
			action: action
		};
		ajaxPost(null, url, post, function (html) {
			module_container_dom.empty().html(html);
		}, null, 'text');
	}

	$(function () {
		/**
		 * BEGIN DECLARE
		 */
		var action_list_dom = $(".action-list.co-scroll");
		var action_filter_dom = $(".action-filter.co-scroll");
		var create_action_modal_dom = $("<div>")
			.addClass("create-action-modal")
			.addClass("modal")
			.addClass("fade");
		var action_thumb_ghost_dom = $("<div>")
			.addClass("ghost-element");
		/**
		 * END DECLARE
		 */
		/**
		 * BEGIN EVENT
		 */
		$(".action-detail .task-toggle").on("click", function (e) {
			e.stopPropagation();
			var properties;

			properties = {
				width: 0
			};
			$(".task-list").animate(properties, {
				step: function (now, fx) { },
				complete: function () {

				}
			});
		});
		$('.action-list .filter .search').on('input', function () {
			clearTimeout(searchTimeout);
			searchTimeout = setTimeout(function () {
				do_filter_action_list.call({
					list: action_list_dom,
					filter: action_filter_dom
				});
			}, 500);
		});

		$('.btn-participer').on('click', function () {
			self = $(this);
			ids = [];
			$(".contributor-selector .contributor-list .contributor-container.selected").each(function () {
				ids.push($(this).data("contributor").id);
			});
			var participate = ids.includes(userId) ? 0 : 1;
			var action = $(".action-thumb.selected").data("id");
			var post = {
				action: action,
				contributor: userId,
				participate: participate
			};
			coInterface.actions.request_participation(post).then(function () {
				do_load_contributors(action);
				if (participate)
					toastr.success("Vous avez choisi de participer a cette action!");
				else
					toastr.success("Vous avez choisi de ne plus participer a cette action!");
			});
		})

		function do_close_dropdown(e) {
			var target_dom = $(e.target);
			var dropdown = $(".contributor-list-dropdown .contributor-selector");
			if (dropdown.hasClass("open") && !target_dom.is(dropdown) && !target_dom.parents(".contributor-list-dropdown .contributor-selector").length) {
				dropdown.removeClass("open");
				$(".action-modal").off("click", do_close_dropdown);
			}
		}

		$(".action-modal").off("click", do_close_dropdown);
		$("[data-target=dropdown-fire]").on("click", function (e) {
			var action = $(".action-thumb.selected").data("id");
			do_load_contributors(action)
			var self;

			self = $(this);
			e.stopPropagation();
			if (self.parent().hasClass("disabled"))
				return (true);
			self.next(".contributor-selector").addClass("open");
			$(".action-modal").on("click", do_close_dropdown);
		});
		$(".hidden-info-button").on("mouseover", function (e) {
			var target = $(e.target);
			var self = $(this);
			if (target.is(self) || target.parents(".hidden-info-button").is(self))
				self.find(".button-hidden-info").addClass("open");
		}).on("mouseout", function (e) {
			var target = $(e.target);
			var self = $(this);
			if (target.is(self) || target.parents(".hidden-info-button").is(self))
				self.find(".button-hidden-info").removeClass("open");
		});
		$('.btn-tasks').on('click', function () {
			var self = $(this);

			if (!self.prop("disabled"))
				do_open_task_view.call({
					list: action_list_dom,
					filter: action_filter_dom
				});
		});
		$('.btn-invite-member').on('click', function (e) {
			var inviteDom = $('.invite-container').empty().html('');
			var url = php.server_url + '/costum/project/action/request/invite_html/';
			var action = action_list_dom.find(".action-thumb.selected").data("id");
			var post = {
				action: action
			};
			ajaxPost(null, url, post, function (html) {
				inviteDom.html(html);
				inviteDom.removeClass('d-none')
			}, null, 'text');
		});
		$('.btn-invite-outside').click(function () {
			smallMenu.openAjaxHTML(baseUrl + '/' + moduleId + '/' + 'element/invite/type/' + typeCostum + '/id/' + idCostum);
		})
		$('.openActionComment').click(function () {
			var action = action_list_dom.find(".action-thumb.selected").data("id");
			var title = action_list_dom.find(".action-thumb.selected").text()
			commentObj.openPreview('actions', action, action + '.comment', title, 'null', 'null', { notCloseOpenModal: true })
		})
		$('.close-panel-action').click(function () {
			$('#modal-preview-comment').hide()
		})
		$('.btn-delete-action').on('click', function (e) {
			var self = $(this);
			var selected_action_dom,
				action_id;
			if (self.hasClass("disabled"))
				return (true);
			selected_action_dom = action_list_dom.find(".action-thumb.selected");
			action_id = selected_action_dom.data("id");
			$.confirm({
				title: trad['Archive'],
				type: 'orange',
				content: trad['Please confirm archiving'],
				buttons: {
					no: {
						text: trad.no,
						btnClass: 'btn btn-default'
					},
					yes: {
						text: trad.yes,
						btnClass: 'btn btn-warning',
						action: function () {
							var params = {
								id: action_id,
								server_url: php.server_url
							};
							coInterface.actions.request_archive(params).then(function () {
								toastr.success("Action archivée");
								ajaxPost(null, coWsConfig.pingActionManagement, {
									event: '.archive' + php.parent_id, data: {
										action: action.id,
										emiter: '',
										column: action.header
									}
								}, null, null, { contentType: 'application/json' });
								$('.action-modal').modal('hide');
							});
						}
					}
				}
			});
		});
		$(".btn-cancel-action").on("click", function () {
			var self = $(this);
			var action_id;
			if (self.hasClass("disabled"))
				return (true);
			action_id = action_list_dom.find(".action-thumb.selected").data("id");
			$.confirm({
				title: coTranslate("Cancel"),
				type: 'orange',
				content: "Vous voulez annuler cette action ?",
				buttons: {
					no: {
						text: trad.no,
						btnClass: 'btn btn-default'
					},
					yes: {
						text: trad.yes,
						btnClass: 'btn btn-warning',
						action: function () {
							coInterface.actions.request_cancel(action_id).then(function () {
								toastr.success("Action annulée");
								$('.action-modal').modal('hide');
								ajaxPost(null, coWsConfig.pingActionManagement, {
									event: '.archive' + php.parent_id, data: {
										action: action.id,
										emiter: '',
										column: action.header
									}
								}, null, null, { contentType: 'application/json' });
							});
						}
					}
				}
			});
		});
		$(".action-status a").on("click", function (e) {
			e.preventDefault();
			var selected_dom = $(".action-thumb.selected");
			var id = selected_dom.data("id");
			var status = this.getAttribute("href");
			if (status === selected_dom.data('view-status'))
				return (true);
			var status_group_dom = $('.action-status-group[data-status=' + status + ']');
			var seek_after = status_group_dom.next();
			var after_id = null;
			while (seek_after.length && !seek_after.hasClass('action-thumb'))
				seek_after = seek_after.next();
			if (seek_after && seek_after.hasClass('action-thumb'))
				after_id = seek_after.data('id');
			status_group_dom.after(selected_dom);
			coInterface.actions.update_action_position({
				action: id,
				after: after_id,
				group: status,
				emiter: W.wsCO && W.wsCO.id ? W.wsCO.id : null,
				component: 'panel',
				server_url: php.server_url
			}).then();
			coInterface.actions.request_set_status({
				id: id,
				status: status,
				server_url: php.server_url,
				user: userId
			}).then(function () {
				request_get_action(id).then(function (p_action) {
					do_load_status_bar.call({
						list: action_list_dom,
						filter: action_filter_dom
					}, p_action);
				});
			});
		});
		$(".setting-button").on("click", function (e) {
			e.preventDefault();
			var selected_dom = $(".action-thumb.selected");
			var id = selected_dom.data("id");
			request_get_action(id).then(function (p_action) {
				dyFObj.openForm({
					jsonSchema: {
						title: "Modifier une action",
						properties: {
							name: {
								inputType: "text",
								label: "Titre",
								value: p_action.name,
								rules: {
									required: true
								}
							},
							tags: {
								inputType: "tags",
								label: "Tags",
								value: p_action.tags
							},
							description: {
								inputType: "textarea",
								markdown: true,
								label: "Description",
								value: dataHelper.htmlToMarkdown(p_action.description)
							},
							startDate: {
								inputType: "datetime",
								label: "Date du début",
								value: p_action.start_date ? moment(p_action.start_date).format("DD/MM/YYYY HH:mm") : ""
							},
							endDate: {
								inputType: "datetime",
								label: "Date de fin",
								value: p_action.end_date && moment(p_action.end_date).isValid() ? moment(p_action.end_date).format("DD/MM/YYYY HH:mm") : ""
							},
							credits: {
								inputType: "text",
								label: "Crédit",
								rules: {
									number: true
								},
								value: p_action.credits
							},
							max: {
								inputType: "text",
								label: "Nombre maximal de contributeur",
								rules: {
									number: true
								},
								value: typeof p_action.max === "number" ? p_action.max : 1
							},
							images: {
								inputType: "uploader",
								label: "Images",
								docType: "image",
								showUploadBtn: false,
								initList: p_action.images.map(function (m_image) {
									return m_image.full;
								})
							}
						},
						beforeBuild: function () {
							uploadObj.set("actions", id);
						},
						save: function (form_data) {
							delete form_data.collection;
							delete form_data.scope;
							var setType = [];
							var path2Value = {
								id: id,
								collection: "actions",
								path: "allToRoot",
								value: {}
							}
							for (var key in form_data) {
								path2Value.value[key] = form_data[key];
							}
							if (path2Value.value.startDate) {
								path2Value.value.startDate = moment(path2Value.value.startDate, "DD/MM/YYYY HH:mm").format();
								setType.push({
									path: "startDate",
									type: "isoDate"
								});
							}
							if (path2Value.value.endDate) {
								path2Value.value.endDate = moment(path2Value.value.endDate, "DD/MM/YYYY HH:mm").format();
								setType.push({
									path: "endDate",
									type: "isoDate"
								});
							}
							if (setType.length)
								path2Value.setType = setType;
							dataHelper.path2Value(path2Value, function (response) {
								dyFObj.commonAfterSave(response, function () {
									request_add_image_and_file(path2Value.id);
									dyFObj.closeForm();
									do_load_action.call({
										filter: action_filter_dom,
										list: action_list_dom
									}, id);
									// update la carte
									selected_dom.find(".title").text(path2Value.value.name);
									// notification socoket
									ajaxPost(null, W.coWsConfig.pingActionManagement, {
										event: "action-full-update",
										data: $.extend({}, path2Value.value, { id: path2Value.id })
									});
									return (true);
								});
								return (true);
							})
						}
					}
				});
			});
		});
		$(".image-upload .btn").on("click", function () {
			$(this).next("input[type=file]").trigger("click");
		});
		$(".image-upload input[type=file]").on("change", function () {
			var files = Array.from(this.files);
			var upload_list = [];
			var error_message_list = []
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				var extension_ok = ['jpeg', 'jpg', 'gif', 'png'].includes(file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase());
				var to_megabyte = file.size / 1024 / 1024;
				var max_filesize = 5;
				var filesize_ok = to_megabyte <= max_filesize;
				if (extension_ok && filesize_ok)
					upload_list.push(file);
				if (!extension_ok)
					error_message_list.push(' - ' + file.name + ' n\'est pas une image');
				if (!filesize_ok)
					error_message_list.push(' - ' + file.name + ' dépasse les ' + max_filesize + 'Mo');
			}
			if (upload_list.length > 0)
				do_upload_files(upload_list);
			if (error_message_list.length > 0)
				toastr.error('Quelques erreurs sont survenues. Raison :<br>' + error_message_list.join('<br>'))
		});
		if (typeof $.fn.datetimepicker === 'function')
			$(".hidden-datepicker").datetimepicker({
				format: 'd/m/Y H:i'
			});
		$(".hidden-datepicker").on("blur", function () {
			$(this).attr("hidden", "hidden");
			$(this).prev(".date-preview").show();
		}).on("focus", function () {
			$(this).prev(".date-preview").hide();
		}).on("change", function () {
			var self = $(this);
			var old_val = self.prev(".date-preview").text();
			var moment_format = "DD/MM/YYYY HH:mm";
			var self_moment = moment(self.val(), moment_format);
			self.prev(".date-preview").text(self_moment.isValid() ? self.val() : "__/__/____ __:__");
			var new_val = self.prev(".date-preview").text();
			if (old_val !== new_val)
				dataHelper.path2Value({
					id: $(".action-thumb.selected").data("id"),
					collection: "actions",
					path: self.parent("div").data("target"),
					value: self_moment.isValid() ? self_moment.format() : null,
					setType: "isoDate"
				}, function (response) {
					if (response.result && typeof response.text !== "undefined")
						toastr.success(response.msg);
				});
		});
		$(".action-duration .fa.fa-calendar, .action-duration .date-preview").on("click", function () {
			var self = $(this);
			if (self.hasClass("disabled"))
				return (true);
			self.parent("div").find(".hidden-datepicker").removeAttr("hidden").trigger("focus");
		});
		$(".action-detail").on("dragover", function (e) {
			e.preventDefault();
		}).on("drop", function (e) {
			e.preventDefault();
			var files = Array.from(e.originalEvent.dataTransfer.files);
			var upload_list = [];
			var error_message_list = []
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				var extension_ok = ['jpeg', 'jpg', 'gif', 'png'].includes(file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase());
				var to_megabyte = file.size / 1024 / 1024;
				var max_filesize = 5;
				var filesize_ok = to_megabyte <= max_filesize;
				if (extension_ok && filesize_ok)
					upload_list.push(file);
				if (!extension_ok)
					error_message_list.push(' - ' + file.name + ' n\'est pas une image');
				if (!filesize_ok)
					error_message_list.push(' - ' + file.name + ' dépasse les ' + max_filesize + 'Mo');
			}
			if (upload_list.length > 0)
				do_upload_files(upload_list);
			if (error_message_list.length > 0)
				toastr.error('Quelques erreurs sont survenues. Raison :<br>' + error_message_list.join('<br>'))
		});
		$(".image-upload input[type=text]").on("paste", function (e) {
			e.preventDefault();
			var files = Array.from(e.originalEvent.clipboardData.files);
			var upload_list = [];
			var error_message_list = []
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				var extension_ok = ['jpeg', 'jpg', 'gif', 'png'].includes(file.name.substr(file.name.lastIndexOf('.') + 1).toLowerCase());
				var to_megabyte = file.size / 1024 / 1024;
				var max_filesize = 5;
				var filesize_ok = to_megabyte <= max_filesize;
				if (extension_ok && filesize_ok)
					upload_list.push(file);
				if (!extension_ok)
					error_message_list.push(' - ' + file.name + ' n\'est pas une image');
				if (!filesize_ok)
					error_message_list.push(' - ' + file.name + ' dépasse les ' + max_filesize + 'Mo');
			}
			if (upload_list.length > 0)
				do_upload_files(upload_list);
			if (error_message_list.length > 0)
				toastr.error('Quelques erreurs sont survenues. Raison :<br>' + error_message_list.join('<br>'))
		});
		$(".action-description").on("click", function () {
			var self = $(this);
			if (self.hasClass("edit-mode") || self.hasClass("disabled"))
				return (true);
			self.addClass("edit-mode");
			var textarea_dom = $("<textarea>");
			var start_description = dataHelper.htmlToMarkdown(self.html()).trim();
			start_description = start_description !== empty_description_text ? start_description : "";
			textarea_dom.val(start_description);
			self.empty().append(textarea_dom);
			dataHelper.activateMarkdown(textarea_dom);
		});
		$(".save-description").on("click", function () {
			var editor_dom = $(".action-description.edit-mode");
			var textarea = editor_dom.find(".md-input").clone();
			var value = textarea.val() && textarea.val().trim() !== empty_description_text ? textarea.val() : "";
			editor_dom
				.empty()
				.html(dataHelper.markdownToHtml(value ? value : empty_description_text))
				.removeClass("edit-mode");
			dataHelper.path2Value({
				id: $(".action-thumb.selected").data("id"),
				collection: "actions",
				value: value,
				path: "description",
			}, function (response) {
				if (response.result && typeof response.text !== "undefined")
					toastr.success(response.msg);
			})
		});
		action_list_dom
			.on("click", ".action-status-group", function () {
				var self = $(this);
				var container_dom = self.parents('.action-list.co-scroll');
				var reference_top_dom = container_dom.find('.filter');
				var bounding_rect = {
					container: container_dom.get(0).getBoundingClientRect(),
					reference_top: reference_top_dom.get(0).getBoundingClientRect(),
					self: self.get(0).getBoundingClientRect()
				};
				var self_relative = bounding_rect.self.top - bounding_rect.container.top;
				var start = (bounding_rect.reference_top.top - bounding_rect.container.top) + reference_top_dom.outerHeight(true);
				var scroll = self_relative - start + container_dom.get(0).scrollTop;
				container_dom.scrollTop(scroll);
			})
			.on("scroll", do_reload_next_status_button.bind(action_list_dom))
			.on('mouseover', '.action-thumb', function () {
				var self = $(this);
				if (self.find('.dragger').is(':visible'))
					self.prop('draggable', true);
			})
			.on('mouseout', '.action-thumb', function () {
				$(this).prop('draggable', false);
			})
			.on("click", ".action-thumb", function () {
				var self = $(this);
				do_center_thumb(self);
				$(".action-thumb").removeClass('focused');
				self.addClass('focused');
				if (self.hasClass("selected"))
					return (true);
				$(".action-thumb").removeClass('selected');
				self.addClass('selected');
				// Load content
				do_load_action.call({
					filter: action_filter_dom,
					list: action_list_dom
				}, self.data('id'));
			})
			.on("mousedown", ".dragger", function () {
				var self = $(this);
				self.parents(".action-thumb").prop("draggable", true);
			}).on("mouseup", ".dragger", function () {
				$(this).parents(".action-thumb").prop("draggable", false);
			}).on("dragstart", ".action-thumb", function (e) {
				var self = $(this);
				var dragger_dom = self.find(".actions .dragger");
				var target = $(e.target);
				if (dragger_dom.is(":visible"))
					self.addClass("dragging");
				else
					return (false);
			}).on("dragend", ".action-thumb", function () {
				var self = $(this);
				var id = self.data("id");
				var selected_id = $(".action-thumb.selected").data("id");
				var status;

				self.removeClass("dragging");
				action_thumb_ghost_dom.before(self);
				self.css("display", "");
				action_thumb_ghost_dom.detach();
				setTimeout(function () {
					// getting status
					status = self.prev();
					while (!status.hasClass("action-status-group"))
						status = status.prev();
					status = status.data("status");
					if (status !== self.data("status").substr(1)) {
						// update tags
						do_update_data_status(id, status);
						do_update_action_count.call({ list: action_list_dom });
						do_reload_next_status_button.bind(action_list_dom);
						coInterface.actions.request_set_status({
							id: id,
							status: status,
							server_url: php.server_url,
							user: userId
						}).then(function (action) {
							if (id === selected_id) {
								request_get_action(id).then(function (action) {
									do_load_status_bar.call({
										list: action_list_dom,
										filter: action_filter_dom
									}, action);
								});
							}
						});
					}
					coInterface.actions.update_action_position({
						action: id,
						after: action_thumb_ghost_dom.data("id"),
						group: status,
						emiter: wsCO && wsCO.id ? wsCO.id : null,
						component: 'panel',
						server_url: php.server_url
					}).then();
				})
			}).on("dragover", function (e) {
				var self = $(this);
				var after_element_dom = do_get_drag_after(self, e.originalEvent.clientY);
				var dragging_dom = $(".action-thumb.dragging");
				dragging_dom.css("display", "");
				action_thumb_ghost_dom
					.height(dragging_dom.height())
					.width(dragging_dom.width());
				dragging_dom.hide();
				if (after_element_dom === null) {
					self.append(action_thumb_ghost_dom);
					action_thumb_ghost_dom.data("id", null);
				} else {
					var after_id = after_element_dom.data("id");
					if (after_element_dom.hasClass("action-status-group")) {
						if ($(".action-status-group").index(after_element_dom) === 0)
							return (true);
						var dom = after_element_dom.next();
						while (dom.length && (!dom.hasClass("action-thumb") || dom.data("id") === dragging_dom.data("id")))
							dom = dom.next();
						if (dom.length)
							after_id = dom.data("id");
						else
							after_id = null;
					}
					after_element_dom.before(action_thumb_ghost_dom);
					action_thumb_ghost_dom.data("id", after_id);
				}
				do_center_thumb(action_thumb_ghost_dom);
			});
		$(".action-tags").on("click", ".badge", function () {
			var self = $(this);
			var tag_dom = action_filter_dom.find("[name=tags]");
			var val = tag_dom.val();
			if (!val || val.length == 0)
				tag_dom.val([self.data("tag")]).trigger("change");
			else if (val.includes(self.data("tag"))) {
				val.splice(val.indexOf(self.data("tag")), 1);
				tag_dom.val(val).trigger("change");
			} else {
				val.push(self.data("tag"));
				tag_dom.val(val).trigger("change");
			}
		});
		$(".action-filter-tags").on("click", ".badge", function () {
			var self = $(this);
			var tag_dom = action_filter_dom.find("[name=tags]");
			var val = tag_dom.val();

			if (val && val.length && val.includes(self.data("tag")))
				val.splice(val.indexOf(self.data("tag")), 1);
			tag_dom.val(val).trigger("change");
		});
		$(".action-status-referer").on("click", function () {
			$(this).data("target").trigger("click");
		});
		$(".action-list .filter .input-group-addon").on("click", function () {
			action_list_dom.before(action_filter_dom).detach();
		});
		action_filter_dom.on("click", ".back-to-list", function () {
			action_filter_dom.before(action_list_dom).detach();
			setTimeout(do_reload_next_status_button.bind(action_list_dom), 500);
		}).on("change", "[name=status-filter], [name=tags], [name=milestones]", function () {
			do_filter_action_list.call({
				list: action_list_dom,
				filter: action_filter_dom
			});
		});
		$(".action-detail .show-action-list").on("click", function () {
			action_list_dom.addClass("open");
			action_filter_dom.addClass("open");
			$(".aside-overlay").addClass("show");
		});
		$(".aside-overlay").on("click", function () {
			$(this).removeClass("show");
			action_list_dom.removeClass("open");
			action_filter_dom.removeClass("open");
		});
		$(".action-detail .create-action").on("click", function () {
			create_action_modal_dom
				.empty()
				.append(dom_create_action_modal())
			if (!$.contains(document.body, create_action_modal_dom[0]))
				$(document.body).append(create_action_modal_dom);
			create_action_modal_dom.modal("show");
		});
		$(".milestone").on("click", function (e) {
			e.preventDefault();
			var self = $(this);
			var is_selected = self.parent().hasClass('active');
			var selected_action = $(".action-thumb.selected");
			var value = {};
			var path2value = {
				id: selected_action.data('id'),
				collection: 'actions',
				path: 'milestone'
			};
			if (is_selected) {
				W.dataHelper.unsetPath(path2value, function (response) {
					if (response.result) {
						update_milestone_view();
						selected_action.removeData('milestone');
					}
				});
			} else {
				value = self.data("all");
				value.startDate = moment.unix(value.startDate.sec);
				value.endDate = moment.unix(value.endDate.sec);
				value.startDate = value.startDate.format("YYYY-MM-DD");
				value.endDate = value.endDate.format("YYYY-MM-DD");
				path2value.value = value;
				path2value.setType = [{
					path: "startDate",
					type: "isoDate"
				},
				{
					path: "endDate",
					type: "isoDate"
				}];
				dataHelper.path2Value(path2value, function (response) {
					if (typeof response.text !== "undefined") {
						update_milestone_view(self.data("milestoneId"));
						selected_action.data('milestone', self.data('milestoneId'));
					}
				});
			}
		});

		handle_key_navigation = handle_key_navigation.call({
			list: action_list_dom,
			filter: action_filter_dom
		});
		function handle_document_click(e) {
			var target = $(e.target);
			if (!target.hasClass('action-thumb') && target.parents('.action-thumb').length === 0)
				$('.action-thumb.selected.focused').removeClass('focused');
		}
		$('.action-modal')
			.on('show.bs.modal', function () {
				$(document.documentElement).on('keydown', handle_key_navigation)
				$(document.documentElement).on('click', handle_document_click)
			})
			.on('shown.bs.modal', function () {
				$('*').css('cursor', '');
			})
			.on("hide.bs.modal", function () {
				create_action_modal_dom.empty();
				create_action_modal_dom.detach();
				$(document.documentElement).off('keydown', handle_key_navigation);
				$(document.documentElement).off('click', handle_document_click);
			})
			.on("hidden.bs.modal", function () {
				$("#project-action-modal").remove();
			});
		/**
		 * END EVENT
		 */

		/**
		 * BEGIN CALL
		 */
		$('.action-modal').modal('show');
		action_filter_dom.detach();
		do_init_socket.call({
			list: action_list_dom,
			filter: action_filter_dom
		}, {
			wsCO: W.wsCO,
			io: W.io,
			coWsConfig: W.coWsConfig
		})
		setTimeout(function () {
			php.action_list.map(function (consumer) {
				var dom = action_list_dom.find('.action-thumb[data-id=' + consumer.id + ']').tooltip({
					title: consumer.name,
					placement: 'right',
					container: ".action-modal",
					html: true
				});
				var tags = [];
				if (consumer.tags)
					tags = tags.concat(consumer.tags);
				dom.data("tags", tags);
				dom.data("status", "#" + consumer.view_status);
				dom.data("view-status", consumer.view_status);
				dom.data("id", consumer.id);
				dom.data("milestone", consumer.milestone);
			});
			action_filter_dom.find("[name=tags]").select2({
				width: "100%"
			});
		});
		setTimeout(function () {
			do_update_action_count.call({ list: action_list_dom });
			do_reload_next_status_button.call(action_list_dom);
			do_click_action(php.default_action);
		}, 500);
		/**
		 * END CALL
		 */
	});
}