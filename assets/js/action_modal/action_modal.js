function actionModal($, globals = {}, blockKey){
	var W = window;
	var assignSelectOption = {
		width : '100%',
		maximumSelectionSize : globals.max,
		formatResult : function(state){
			var originalOption = state.element;
			return dataHelper.printf('' +
				'<div class="contributor-selection">' +
				'   <div class="contributor-img-container">' +
				'       <img src="{{image}}" alt="{{text}}" />' +
				'   </div>' +
				'   <span>{{text}}</span>' +
				'</div>', {
				image : $(originalOption).data('image'),
				text : state.text
			});
		}
	}

	// Début fonctions
	function sendUpdateTask(options){
		return new Promise(function(resolve){
			var url = baseUrl + '/costum/project/action/request/socket_task_update';
			options = $.extend({action : globals.actionId}, options);
			if (typeof wsCO === 'object' && typeof wsCO.id === 'string' && wsCO.id.length) {
				options.emiter = wsCO.id;
			}
			ajaxPost(null, url, options, resolve);
		});
	}

	function updateAfterContribution(contributors, operation){
		$(`#detail_contribution_indicator${ blockKey }`).empty();
		$('.contributor_list .list-group-item:not(.active)').remove();
		var contributorHtml = $('.contributor_list').html();
		if (Object.keys(globals.contributors).length) {
			$.each(globals.contributors, function(_, oneContributor){
				contributorHtml += contributorHtmlTemplate(oneContributor);
			});
			$(`#detail_contribution_indicator${ blockKey }`).empty().html('<span class="fa fa-clock-o"></span> Vous participez à cette action');
		} else {
			contributorHtml += '<li class="list-group-item no-task"><span>Aucun participant</span></li>';
		}
		$('.contributor_list').empty().html(contributorHtml);
		if (Object.keys(globals.contributors).length >= globals.max) {
			$('#action-modal-participate').addClass('disabled');
		} else {
			$('#action-modal-participate').removeClass('disabled');
		}
		if (typeof globals.contributors[userConnected._id.$id] === 'object') {
			$('#action-modal-participate').parent('.btn-group').hide();
			$('#action-modal-no-longer-participate').parent('.btn-group').show();
		} else {
			$('#action-modal-participate').parent('.btn-group').show();
			$('#action-modal-no-longer-participate').parent('.btn-group').hide();
		}
		$('#assign-action-select').val(Object.keys(globals.contributors)).trigger('change');
		$('#assign-action-form').css('display', '');
		if (typeof operation === 'string') {
			pingContributor(globals.actionId, contributors, operation);
		}
	}


	/**
	 * SocketIO ajout contributeur
	 * @param {string} id Identifiant de l'action
	 * @param {string[]} contributors Liste d'identifiant des contributeurs
	 * @param {string} action Action à effectuer (suppression, ajout, modification)
	 * @return
	 */
	function pingContributor(id, contributors, action){
		var url = dataHelper.printf('{{baseUrl}}/costum/project/action/request/ping_contribution', {baseUrl : W.baseUrl});
		var post = {
			id : id,
			contributors : contributors,
			action : action
		};
		if(wsCO !== null && typeof wsCO.id === 'string') {
			post.emiter = wsCO.id;
		}
		W.ajaxPost(null, url, post);
		
		$(document.body).trigger('action.set-contributor', {action: globals.actionId, contributors: globals.contributors});
	}

	function uploadDocumentFiles(fileList){
		$.each(fileList, function(_, oneFile){
			var sizeMb = oneFile.size / 1024 / 1024;
			var maxFileSize = 7.5;
			if (sizeMb > maxFileSize) {
				toastr.error(`${ trad['Maximum upload size exceeded'] } : ${ maxFileSize }Mb`);
			} else if (isValidDocument(oneFile.name)) {
				var fileReader = new FileReader();
				var newid = Math.floor(Math.random() * Date.now() + 1);
				var targetDocumentDom = $(htmlDocumentTemplate({
					name : oneFile.name
				}, {upload : true, id : 'temp' + newid}));
				$('#document-container').before(targetDocumentDom);
				fileReader.onload = function(readerLoad){
					var file = {
						name : oneFile.name,
						data : readerLoad.target.result
					};
					saveMediaDocument([file]).then(function(messageList){
						var errorMessage = '';
						$.each(messageList, function(_, oneMessage){
							if (oneMessage.status) {
								targetDocumentDom.find('.fa').removeClass('fa-spinner').removeClass('fa-spin').addClass(getFileFAIcon(file.name));
								var badgeDom = $('a[href="#documentsTab"] .badge');
								badgeDom.text(parseInt(badgeDom.text(), 10) + 1);
							} else {
								errorMessage += oneMessage.msg + '<br>';
							}
						});
						if (notEmpty(errorMessage)) {
							toastr.error(errorMessage);
						}
					});
				};
				fileReader.readAsDataURL(oneFile);
			}
		});

	}

	function uploadImageFiles(fileList){
		$.each(fileList, function(_, oneFile){
			var sizeMb = oneFile.size / 1024 / 1024;
			var maxFileSize = 7.5;
			if (sizeMb > maxFileSize) {
				toastr.error(`${ trad['Maximum upload size exceeded'] } : ${ maxFileSize }Mb`);
			} else {
				var fileReader = new FileReader();
				var newid = Math.floor(Math.random() * Date.now() + 1);
				var targetImageDom = $(htmlImageTemplate({}, {upload : true, id : 'temp' + newid}));
				$('#image-upload-container').before(targetImageDom);
				fileReader.onload = function(readerLoad){
					saveMediaImage([readerLoad.target.result]).then(function(messageList){
						var errorMessage = '';
						$.each(messageList, function(_, oneMessage){
							if (oneMessage.status) {
								var badgeDom = $('a[href="#imagesTab"] .badge');
								badgeDom.text(parseInt(badgeDom.text(), 10) + 1);
								targetImageDom.find('.image-icon').remove();
								targetImageDom.append(dataHelper.printf('<a href="{{src}}" data-lightbox="{{action}}"><img src="{{src}}" /></a>', {
									src : oneMessage.url,
									action : globals.actionId
								}));
							} else {
								errorMessage += oneMessage.msg + '<br>';
							}
						});
						if (notEmpty(errorMessage)) {
							toastr.error(errorMessage);
						}
					});
				};
				fileReader.readAsDataURL(oneFile);
			}
		});

	}

	/**
	 * Générer un template HTML pour l'affichage des images
	 * @param {Object} image Image à afficher
	 * @param {string} image.url Url de l'image depuis la racine
	 * @param {string} image.name Nom du fichier
	 * @return {string}
	 */
	function htmlImageTemplate(image, options){
		var defaultOption = {upload : false};
		if (typeof options === 'undefined' || options === null) {
			options = {};
		} else {
			options = $.extend({}, defaultOption, options);
		}
		return dataHelper.printf('' +
			'<div class="image-element" {{id}}>' +
			'    {{mainContent}}' +
			'</div>',
			{
				mainContent : options.upload ? '<span class="image-icon fa fa-spinner fa-spin"></span>' : dataHelper.printf('<a href="{{url}}" data-lightbox="{{action}}"><img src="{{url}}" alt="{{name}}" /></a>', {
					action : globals.actionId,
					name : image.name,
					url : image.url ? image.url : ''
				}),
				id : options.id ? dataHelper.printf('id="{{id}}"', {id : options.id}) : ''
			}
		);
	}

	function contributorHtmlTemplate(contributor){
		return dataHelper.printf(
			'<li class="list-group-item">' +
			'    <div class="item-avatar" style="min-height: auto; padding: 0;">' +
			'        <img src="{{imageUrl}}" class="visible" alt="{{name}}" style="width: 50px; height: 50px;">' +
			'        <p style="margin: 0;text-overflow: ellipsis; overflow: hidden;">' +
			'            <span class="creator">{{name}}</span>' +
			'        </p>' +
			'    </div>' +
			'</li>',
			{
				imageUrl : baseUrl + contributor.image,
				name : contributor.name
			}
		);
	}

	/**
	 * @param {Object} document
	 * @param {string} document.id Identifiant du document
	 * @param {string} document.name Nom du document
	 * @param {string} document.url Lien du document
	 * @return {stirng}
	 */
	function htmlDocumentTemplate(document, options){
		var defaultOption = {upload : false};
		if (typeof options === 'undefined' || options === null) {
			options = {};
		} else {
			options = $.extend({}, defaultOption, options);
		}
		return dataHelper.printf('' +
			'<div class="document-element" {{id}}>' +
			'    <span class="document-icon fa {{faIcon}}"></span>' +
			'    <span class="document-name">{{name}}</span>' +
			'</div>',
			{
				faIcon : options.upload ? 'fa-spinner fa-spin' : getFileFAIcon(document.name),
				name : document.name,
				id : options.id ? dataHelper.printf('id="{{id}}"', {id : options.id}) : ''
			}
		);
	}

	/**
	 * Retourner l'icône associant au fichier
	 * @param {string} filename Nom du fichier
	 * @return {string}
	 */
	function getFileFAIcon(filename){
		var extension = filename.split('.');
		extension = extension.length > 0 ? extension[extension.length - 1].toLowerCase() : '';
		var output = 'fa-file-o';
		if (['ppt', 'pptx', 'odp'].includes(extension)) {
			output = 'fa-file-powerpoint-o';
		} else if (['doc', 'docx', 'odt'].includes(extension)) {
			output = 'fa-file-word-o';
		} else if (['xls', 'xlsx', 'ods'].includes(extension)) {
			output = 'fa-file-excel-o';
		} else if (['txt', 'csv'].includes(extension)) {
			output = 'fa-file-text-o';
		} else if (['pdf'].includes(extension)) {
			output = 'fa-file-pdf-o';
		}
		return output;
	}

	/**
	 * Vérifie si le format du documetn est adéquat
	 * @param {string} filename Nom du fichier
	 * @return {boolea}
	 */
	function isValidDocument(filename){
		var extensionList = ['pptx', 'ppt', 'odp', 'doc', 'docx', 'odt', 'xls', 'xlsx', 'ods', 'txt', 'csv', 'pdf'];
		var extension = filename.split('.');
		extension = extension.length > 0 ? extension[extension.length - 1].toLowerCase() : '';
		return extensionList.includes(extension);
	}

	/**
	 * Récupère le détail d'une action
	 * @param {string} __id identifiant de l'action à récupérer
	 * @return {Promise}
	 */
	function getAction(__id){
		return new Promise(function(__resolve){
			var url = baseUrl + '/costum/project/action/request/action_detail_data';
			var post = {
				id : __id
			};
			ajaxPost(null, url, post, __resolve);
		});
	}

	/**
	 * Enregistre des nouveaux contributeurs
	 * @param {stirng[]} contributors
	 * @return {Promise<{id: string, name: string, image: string}[]>}
	 */
	function setContributors(contributors){
		return new Promise(function(resolve){
			if (contributors.length) {
				var url = `${ baseUrl }/costum/project/action/request/set_contributors`;
				var post = {
					action : globals.actionId,
					contributors : contributors
				};
				ajaxPost(null, url, post, resolve);
			} else {
				dataHelper.unsetPath({
					id : globals.actionId,
					collection : 'actions',
					path : 'links.contributors'
				}, function(){
					resolve([]);
				});
			}
		});
	}

	/**
	 * Récupère le détail d'une tâche
	 * @param {Number} __range Ordre de la tâche à récupérer
	 * @param {String} __action Identifiant de l'action où on pourra trouver la tâche
	 * @return {Promise}
	 */
	function getTask(__action, __range){
		return new Promise(function(__resolve){
			var url = baseUrl + '/costum/project/action/request/task';
			var post = {
				action : __action,
				task : __range
			};
			ajaxPost(null, url, post, __resolve);
		});
	}

	/**
	 * Ajout d'une nouvelle tâche
	 * @param {string} task Le nom de tâche
	 * @return {Promise}
	 */
	function addNewtask(task){
		return new Promise(function(resolve){
			var value = {
				taskId : (new Date()).getTime(),
				userId : userConnected['_id']['$id'],
				task : task,
				createdAt : moment().format(),
				checked : false
			};

			var setType = [{
				path : 'checked',
				type : 'boolean'
			},
				{
					path : 'createdAt',
					type : 'isoDate'
				}
			];

			var params = {
				id : globals.actionId,
				collection : 'actions',
				path : 'tasks',
				arrayForm : true,
				edit : false,
				format : true,
				value,
				setType
			};

			dataHelper.path2Value(params, function(response){
				if (response.result && response.result === true) {
					resolve(response.text);
				}
			});
		});
	}

	/**
	 * Modifier la tâche
	 * @param {String} __value Le nom de tâche
	 * @param {Number} __range Rang de la tâche dans la le tableau
	 * @return {Promise}
	 */
	function update_task(__value, __range){
		return new Promise(function(__resolve){
			getTask(globals.actionId, __range).then(function(__task){
				__task.task = __value;
				var setType = [{
					path : 'checked',
					type : 'boolean'
				},
					{
						path : 'createdAt',
						type : 'isoDate'
					}
				];

				var params = {
					id : globals.actionId,
					collection : 'actions',
					path : 'tasks.' + __range,
					value : __task,
					setType
				};

				dataHelper.path2Value(params, function(__response){
					if (__response.result && __response.result === true) {
						__resolve(__response['text']);
					}
				});
			});
		});
	}

	/**
	 * Charger la tâche ajoutée
	 * @param {Object} __task L'objet task inséré dans la base de données
	 * @param {HTMLElement} __input le champ de saisie pour saisir le nom de la tâche qu'on va reinitialiser
	 */
	function post_add_task(__task, __input){
		__input.value = '';
		var task_html = taskHmlTemplate(__task);

		// Envoyer une notification rocket chat pour la nouvelle tâche
		var url = baseUrl + '/survey/answer/rcnotification/action/newtask2';
		var post = {
			projectname : globals.parentName,
			taskname : __task.task,
			actname : globals.actionName,
			channelChat : globals.parentSlug
		};
		ajaxPost(null, url, post);

		if ($('.task-group-container').length === 0) {
			$('.task_list .list-group-item:last-child').slideUp({
				complete : function(){
					$(this).remove();
					$('.task_list').append(task_html).find('.list-group-item').last().hide().slideDown();
				}
			});
		} else {
			$('.task_list').append(task_html).find('.list-group-item').last().hide().slideDown();
		}
		update_task_recap();
	}

	/**
	 * Charger la tâche modifiée
	 * @param {Object} __task L'objet task modifié dans la base de données
	 * @param {Number} __range Numéro d'ordre de la tâche dans le tableau
	 * @param {HTMLElement} __input le champ de saisie pour saisir le nom de la tâche qu'on va reinitialiser
	 */
	function post_update_task(__task, __range, __input){
		__input.value = '';
		var task_line_container = $('.task-line-container').eq(__range);
		var task_input_container = $('.new_task_form_container');

		task_line_container.find('.cpl-check').data('name', __task.task);
		task_input_container.find('input[type=hidden]').val('-1');
		task_input_container.find('button[type=submit] .fa').removeClass('fa-edit').addClass('fa-plus');
		task_line_container.find('label:not(.checkmark)').fadeOut({
			complete : function(){
				$(this).text(__task.task).fadeIn();
			}
		});
	}

	/**
	 * Mettre à jour la vue de récapitulation et la barre de progression
	 */
	function update_task_recap(){
		get_task_recap(globals.actionId).then(function(__recap){
			if (__recap['total'] === 0) {
				$('.task_list .task_number_recap').text('0').hide();
			} else {
				$('.task_list .task_number_recap').text(__recap['done'] + '/' + __recap['total']).show();
			}
			$('.task_list .progress-bar').attr('aria-valuenow', __recap['done']);
			$('.task_list .progress-bar').attr('aria-valuemax', __recap['total']);
			$('.task_list .progress-bar').css('width', __recap['percentage'] + '%');
		});
	}

	/**
	 * Récupère la récapitulation des tâches
	 * @param {String} __action Identifiant de l'action où on doit récupérer les tâches
	 * @return {Promise}
	 */
	function get_task_recap(__action){
		return new Promise(function(__resolve){
			var url = baseUrl + '/costum/project/action/request/task_recap';
			var post = {
				id : __action
			};
			ajaxPost(null, url, post, __resolve);
		});
	}

	/**
	 * Récupérer tous les détails concernant à une sous-tâche
	 * @param index Ordre de la sous tâche dans la vue
	 * @return {Promise<{id: string, name: string, checked: boolean, userId: Object, contributors: []}>}
	 */
	function getTaskDetail(index){
		return new Promise(function(resolve){
			var url = `${ baseUrl }/costum/project/action/request/task_detail`;
			var post = {
				action : globals.actionId,
				index
			};
			ajaxPost(null, url, post, resolve);
		})
	}

	/**
	 * Enregistrer la liste des images sélectionnées
	 * @param {string[]} images liste des images à enregistrer
	 * @return {Promise<{status: boolean, filename: stirng, [url]: string, msg: string}[]>}
	 */
	function saveMediaImage(images){
		return new Promise(function(resolve){
			var url = `${ baseUrl }/costum/project/action/request/add_image_media`;
			var post = {
				action : globals.actionId,
				images
			};
			ajaxPost(null, url, post, resolve);
		});
	}

	/**
	 * Enregistrer le document
	 * @param {Object[]} documents liste des images à enregistrer
	 * @return {Promise<{status: boolean, filename: stirng, [url]: string, msg: string}[]>}
	 */
	function saveMediaDocument(documents){
		return new Promise(function(resolve){
			var url = `${ baseUrl }/costum/project/action/request/add_document_media`;
			var post = {
				'action' : globals.actionId,
				'documents' : documents
			};
			ajaxPost(null, url, post, resolve);
		});
	}

	/**
	 * Créer un template html pour la liste des tâches
	 * @param {Object} task
	 * @return {String}
	 */
	function taskHmlTemplate(task, option = {}){
		task = {
			id : task.taskId ? task.taskId : (task.id ? task.id : '' + Math.round(Date.now())),
			name : task.task ? task.task : task.name,
			checked : typeof task.checked !== 'undefined' ? task.checked : false,
			user : task.userId ? task.userId : task.user,
			contributors : typeof task.contributors !== 'undefined' && Array.isArray(task.contributors) ? task.contributors : []
		};

		var isTaskContributor = userConnected !== null && typeof task.contributors[userConnected._id.$id] === 'object';

		var userCanCheckTask = globals.canAdmin || globals.isProjectContributor;
		var dataStatus = [];
		if (task.checked) {
			dataStatus.push('done');
		}
		if (isTaskContributor) {
			dataStatus.push('my-task');
		}

		var innerHtml = dataHelper.printf('' +
			'<div class="task-line-container" data-status="{{dataStatus}}">' +
			'   <div style="position: relative; margin-right: 10px;">' +
			'       <input class="cpl-check" type="checkbox" id="task{{taskId}}" data-id="{{taskId}}" data-name="{{taskName}}" {{checkedAttribute}} {{canCheckInput}}>' +
			'       <label class="checkmark {{canCheckLabel}}" for="task{{taskId}}"></label>' +
			'   </div>' +
			'   <div style="flex: 1 1 0%;">' +
			'       <label for="task{{taskId}}" class="{{disableClassIfUserCanCheck}} {{doneClassIfTaskChecked}}" style="user-select: none;">{{taskName}}</label>' +
			'       <span class="executor{{showIfTaskCheckedByUser}}">' +
			'           <span class="fa fa-check"></span>' + (task.user && task.user.name ? task.user.name : '') +
			'       </span>' +
			task.contributors.map(function(taskContributorMap){
				return '' +
					'<span class="contributor show">' +
					'   <span class="fa fa-lock"></span>' + (taskContributorMap.name ? taskContributorMap.name : '') +
					'</span>';
			}).join('') +
			'   </div>' +
			(globals.canAdmin || globals.isActionContributor ? ('' +
				'   <div class="dropdown">' +
				'       <button class="btn btn-default dropdown-toggle" type="button" id="action-list-{{taskId}}" data-toggle="dropdown">' +
				'           <span class="fa fa-cog"></span>' +
				'           <span class="caret"></span>' +
				'       </button>' +
				'       <ul class="dropdown-menu" role="menu" aria-labelledby="action-list-{{taskId}}">' +
				'           <li role="presentation"><a class="task-edit" role="menuitem" onclick="return false" tabindex="-1" href="!#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editer</a></li>' +
				'           {{userCanDeleteTask}}' +
				'           {{userCanParticipate}}' +
				'           <!-- <li role="presentation"><a class="task-invite" role="menuitem" onclick="return false" tabindex="-1" href="!#"><i class="fa fa-user" aria-hidden="true"></i> Assigner</a></li>-->' +
				'       </ul>' +
				'   </div>') : '') +
			'</div>', {
			dataStatus : dataStatus.join(','),
			taskId : task.id,
			taskName : task.name,
			checkedAttribute : task.checked ? 'checked="checked"' : '',
			canCheckInput : userCanCheckTask ? '' : 'disabled="disabled"',
			canCheckLabel : userCanCheckTask ? 'default' : 'disabled',
			disableClassIfUserCanCheck : !userCanCheckTask ? 'disabled' : '',
			doneClassIfTaskChecked : task.checked ? 'done' : '',
			showIfTaskCheckedByUser : task.checked && task.user ? ' show' : ' hide',
			userCanDeleteTask: globals.canAdmin || globals.isActionContributor ? '<li role="presentation"><a class="task-delete" role="menuitem" onclick="return false" tabindex="-1" href="!#"><i class="fa fa-trash" aria-hidden="true"></i> Supprimer</a></li>' : '',
			userCanParticipate: (globals.canAdmin || globals.isProjectContributor) && !isTaskContributor ? '<li role="presentation"><a class="task-assign-to-me" role="menuitem" onclick="return false" tabindex="-1" href="!#"><i class="fa fa-user" aria-hidden="true"></i> Participer</a></li>' : ''
		});
		return option.innerHtmlOnly ? innerHtml : dataHelper.printf('' +
			'<li class="list-group-item task-group-container" data-status="{{dataStatus}}">' +
			innerHtml +
			'</li>', {
			dataStatus : dataStatus.join(',')
		});
	}

	// Fin fonctions

	$(function(){
		$(`#action-modal${ blockKey }`).on('shown.bs.modal', function(){
			getAction(globals.actionId).then(function(action){
				var start_moment = moment(action.startDate);
				var end_moment = moment(action.endDate);

				// informations générales
				$(`#action-modal${ blockKey } .item-avatar img`).attr('src', baseUrl + action.creator.image)
				$(`#action-modal${ blockKey } .item-avatar .creator`).text(action.creator.name);
				$(`#begining_date${ blockKey }`).text(start_moment.format('LL'));
				$(`#detail_credit${ blockKey }`).text(action.credits);
				$(`#detail_full_begining${ blockKey }`).text(start_moment.format('LLL'));
				$(`#detail_duration${ blockKey }`).text(moment.duration(start_moment.diff(end_moment)).humanize());
				$(`#detail_contributor${ blockKey }`).text(action.contributor_number);
				if (action.contributor_number > 1) {
					$(`#detail_contributor_multiple${ blockKey }`).show();
				} else {
					$(`#detail_contributor_multiple${ blockKey }`).hide();
				}
				$(`#detail_contributor_min${ blockKey }`).text(action.min);
				if (action.min > 1) {
					$(`#detail_contributor_min_multiple${ blockKey }`).show();
				} else {
					$(`#detail_contributor_min_multiple${ blockKey }`).hide();
				}
				$(`#detail_contributor_max${ blockKey }`).text(action.max);
				if (action.max > 1) {
					$(`#detail_contributor_max_multiple${ blockKey }`).show();
				} else {
					$(`#detail_contributor_max_multiple${ blockKey }`).hide();
				}

				// tâches
				$('.task_list .task_number_recap').text('0').hide();
				var task_html = $('.task_list').html();
				if (action.tasks.length > 0) {
					var checkeds = action.tasks.filter(function(filterTask){
						return filterTask.checked
					});

					$('.task_list .task_number_recap').text(checkeds.length + '/' + action.tasks.length).show();
					$('.task_list .progress-bar').attr('aria-valuenow', checkeds.length);
					$('.task_list .progress-bar').attr('aria-valuemax', action.tasks.length);
					$('.task_list .progress-bar').css({
						width : Math.ceil(checkeds.length * 100 / action.tasks.length) + '%'
					});

					task_html = $('.task_list').html();
					$.each(action.tasks, function(_, oneTask){
						task_html += taskHmlTemplate(oneTask);
					});
				} else {
					task_html += '<li class="list-group-item no-task"><span>Aucune tâche associée</span></li>';
				}
				$('.task_list').empty().html(task_html);

				// participants
				if (notEmpty(userConnected) && typeof action.contributors[userConnected['_id']['$id']] !== 'undefined') {
					$(`#detail_contribution_indicator${ blockKey }`).empty().html('<span class="fa fa-clock-o"></span> Vous participez à cette action');
				}

				var contributor_html = $('.contributor_list').html();
				var contributor_keys = Object.keys(action.contributors);
				if (contributor_keys.length) {
					$.each(contributor_keys, function(_, oneContributorKey){
						var contributor = action.contributors[oneContributorKey];
						contributor_html += contributorHtmlTemplate(contributor);
					});
				} else {
					contributor_html += '<li class="list-group-item no-task"><span>Aucun participant</span></li>';
				}
				$('.contributor_list').empty().html(contributor_html);

				if (!notEmpty(userConnected) && notEmpty(userConnected._id) && notEmpty(userConnected._id.$id) || typeof action.allContributors[userConnected._id.$id] === 'undefined') {
					$('.new_task_form_container input[type=text]').prop('disabled', true);
					$('.new_task_form_container').hide();
				}

				// Afficher les images
				var imageHtml = '';
				var documentHtml = '';
				$.each(action.images, function(_, image){
					imageHtml += htmlImageTemplate(image);
				});
				$.each(action.documents, function(_, document){
					documentHtml += htmlDocumentTemplate(document);
				});
				$('#image-container').empty().html(imageHtml);
				$('.document-compact-list>.document-element').remove();
				$('.image-compact-list>.image-element').remove();
				documentHtml += $('.document-compact-list').html();
				imageHtml += $('.image-compact-list').html();
				$('.document-compact-list').empty().html(documentHtml);
				$('.image-compact-list').empty().html(imageHtml);
				$('a[href="#imagesTab"] .badge').text(action.images.length);
				$('a[href="#documentsTab"] .badge').text(action.documents.length);
			});
		});
		$(`#action-modal${ blockKey }`).modal('show');

		// Modification de tâche
		$('.task_list').on('click', '.task-edit', function(__e){
			if (notEmpty(userConnected)) {
				var index = $('.task-edit').index(this);
				var container = $(this).parents('.task-line-container');
				var input_container = $('.new_task_form_container');

				input_container.find('button[type=submit]').prop('disabled', false);
				input_container.find('button[type=submit] .fa').removeClass('fa-plus').addClass('fa-edit');
				input_container.find('input[type=text]').val(container.find('.cpl-check').data('name'));
				input_container.find('input[type=hidden]').val(index);
			}
		});

		// Suppression d'une tâche
		$('.task_list').on('click', '.task-delete', function(__e){
			var self = this;
			var index = $('.task-delete').index(this);

			if (notEmpty(userConnected)) {
				$.confirm({
					title : 'Supprimer',
					content : 'Supprimer cette tâche',
					buttons : {
						no : {
							text : 'Non',
							btnClass : 'btn btn-default'
						},
						yes : {
							text : 'Oui',
							btnClass : 'btn btn-danger',
							action : function(){
								var params = {
									id : globals.actionId,
									collection : 'actions',
									path : 'tasks.' + index,
									pull : 'tasks',
									value : null
								};

								dataHelper.path2Value(params, function(__response){
									if (__response.result && __response.result === true) {
										$(self).parents('.list-group-item').slideUp({
											complete : function(){
												var content = $(this).find('span:first-child').text();
												$(this).remove();

												// Quand il n'y a plus de sous-tâche
												if ($('.task-group-container').length === 0) {
													$('.task_list').append('<li class="list-group-item no-task" style="display:none;"><span>Aucune tâche associée</span></li>');
													$('.list-group-item:last-child').slideDown();
												}
												update_task_recap();
												sendUpdateTask({
													operation : 'delete',
													content : content,
													index : index
												}).then();
											}
										});
									}
								});
							}
						}
					}
				});
			}
		});

		// Assigner la sous-tâche à l'utilisateur connécté
		$('.task_list').on('click', '.task-assign-to-me', function(__e){
			var self = $(this);
			var index = $('.task-assign-to-me').index(this);

			if (notEmpty(userConnected)) {
				var params = {
					id : globals.actionId,
					collection : 'actions',
					path : 'tasks.' + index + '.contributors',
					value : {}
				};
				params.value[userConnected._id.$id] = {
					type : 'citoyens'
				};

				dataHelper.path2Value(params, function(response){
					if (response.result && response.result === true) {
						getTaskDetail(index).then(function(task){
							var html = taskHmlTemplate(task, {
								innerHtmlOnly : true
							});
							self.parents('.list-group-item.task-group-container').slideUp({
								complete(){
									$(this).empty().html(html).slideDown({
										complete(){
											this.dataset.status = this.firstElementChild.dataset.status;
										}
									});
								}
							});
						});
					}
				});
			}
		});

		// Participer à la sous-tâche à l'utilisateur connécté
		$('.task_list').on('click', '.task-participate', function(__e){
			var self = $(this);
			var index = $('.task-participate').index(this);

			if (notEmpty(userConnected)) {
				var params = {
					id : globals.actionId,
					collection : 'actions',
					path : `tasks.${ index }.contributors.${ userConnected._id.$id }`,
					value : {
						type : 'citoyens'
					}
				};

				dataHelper.path2Value(params, function(response){
					if (response.result && response.result === true) {
						getTaskDetail(index).then(function(task){
							var html = taskHmlTemplate(task, {
								innerHtmlOnly : true
							});
							self.parents('.list-group-item.task-group-container').slideUp({
								complete : function(){
									$(this).empty().html(html).slideDown();
								}
							});
						});
					}
				});
			}
		});

		// Ajout ou modification d'une tâche à partir de l'input
		$('.task_list').on('keydown', '.new_task_form_container input[type=text]', function(event){
			var fa = $('.new_task_form_container button[type=submit] .fa');
			var self = this;
			var buttonDom = $('.new_task_form_container button[type=submit]');
			var value = $(self).val();

			if (event.key === 'Enter' && value && notEmpty(userConnected)) {
				$(self).val('');
				// enregistrer une nouvelle tâche
				if (fa.hasClass('fa-plus')) {
					addNewtask(value).then(function(task){
						post_add_task(task, self);
						buttonDom.prop('disabled', true);
						sendUpdateTask({
							operation : 'insert',
							content : value
						}).then();
					});
				} else
					// modifier la tâche existante
				if (fa.hasClass('fa-edit')) {
					var range = $(this).prev().val();
					update_task(value, range).then(function(task){
						post_update_task(task, range, self);
						buttonDom.prop('disabled', true);
						sendUpdateTask({
							operation : 'update',
							content : value,
							index : range
						}).then();
					});
				}
			}
		});
		$('.task_list').on('input', '.new_task_form_container input[type=text]', function(){
			$(this).next('button[type=submit]').prop('disabled', $(this).val().length === 0);
		});
		// Ajout ou modification d'une tâche à partir du bouton
		$('.task_list').on('click', '.new_task_form_container button[type=submit]', function(__e){
			var fa = $('.new_task_form_container button[type=submit] .fa');
			var self = document.querySelector('.new_task_form_container input[type=text]');
			var target = $(this);
			if (self.value && notEmpty(userConnected)) {
				// enregistrer une nouvelle tâche
				if (fa.hasClass('fa-plus')) {
					addNewtask(self.value).then(function(__task){
						post_add_task(__task, self);
						target.prop('disabled', true);
						sendUpdateTask({
							operation : 'insert',
							content : value
						}).then();
					});
				} else
					// modifier la tâche existante
				if (fa.hasClass('fa-edit')) {
					var range = $(self).prev().val();
					update_task(self.value, range).then(function(__task){
						post_update_task(__task, range, self);
						target.prop('disabled', true);
						sendUpdateTask({
							operation : 'update',
							content : value,
							index : range
						}).then();
					});
				}
			}
		});

		// check d'une tâche
		$('.task_list').on('change', '.task-line-container .cpl-check', function(__e){
			var self = this;
			var index = $('.task-line-container .cpl-check').index(this);
			var parents = $(self).parents('.task-line-container,.task-group-container');
			var status = parents.eq(0).data('status').split(',');

			if ($(self).is(':checked') && status.push('done')) {
				;
			} else {
				status.splice(status.indexOf('done'), 1);
			}

			parents.each(function(){
				this.dataset.status = status.join(',');
			});

			var setType = [{
				path : 'checked',
				type : 'boolean'
			},
				{
					path : 'checkedAt',
					type : 'isoDate'
				},
				{
					path : 'createdAt',
					type : 'isoDate'
				}
			];

			getTask(globals.actionId, index).then(function(task){
				task.checked = self.checked;
				if (self.checked) {
					task.checkedAt = moment().format();
					task.userId = userConnected['_id']['$id'];
				} else {
					task.checkedAt = null;
				}

				var params = {
					id : globals.actionId,
					collection : 'actions',
					path : 'tasks.' + index,
					value : task,
					setType
				}

				dataHelper.path2Value(params, function(response){
					if (response.result && response.result === true) {
						var label = $(self).parents('.task-line-container').find('label:not(.checkmark)');
						var user = $(self).parents('.task-line-container').find('.executor');
						if (self.checked) {
							label.addClass('done');
							user.addClass('show').removeClass('hide').empty().html('<span class="fa fa-check"></span> ' + userConnected.name);
						} else {
							label.removeClass('done');
							user.addClass('hide').removeClass('show').empty().html('');
						}
						showHideElement();
						update_task_recap();
						sendUpdateTask({
							operation : 'update',
							content : task.task,
							index : index
						}).then();

						// Envoyer une notification rocket chat pour la tâche terminée
						var url = baseUrl + '/survey/answer/rcnotification/action/checktask2';
						var post = {
							projectname : globals.parentName,
							taskname : task.task,
							actname : globals.actionName,
							channelChat : globals.parentSlug,
						};
						ajaxPost(null, url, post);
					}
				});
			});
		});

		$('#action-status-switcher')
			.on('click', 'button[data-status]:not(.disabled)', function(){
				var self = $(this);
				var status = self.get(0).dataset.status;
				var newHtml = '';
				switch (status) {
					case 'done':
						newHtml = '' +
							'<div class="btn-group">' +
							'   <button type="button" class="btn btn-default" data-status="todo">' +
							'       <span class="fa fa-thumbs-up"></span> ' + coTranslate('Reopen') +
							'   </button>' +
							'</div>';
						break;
					case 'disabled':
						newHtml = '' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default" data-status="todo">' +
							'        <span class="fa fa-thumbs-up"></span> ' + coTranslate('Reopen') +
							'    </button>' +
							'</div>';
						break;
					case 'todo':
						newHtml = '' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default" id="action-modal-assign">' +
							'        <span class="fa fa-users"></span> ' + coTranslate('Assign') +
							'    </button>' +
							'</div>' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default ' + (Object.keys(globals.contributors).length >= globals.max ? 'disabled' : '') + '"' +
							'            id="action-modal-participate">' +
							'        <span class="fa fa-link"></span> ' + coTranslate('participate') +
							'    </button>' +
							'</div>' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default" id="action-modal-no-longer-participate">' +
							'        <span class="fa fa-unlink"></span> ' + coTranslate('notparticipateanymore') +
							'    </button>' +
							'</div>' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default" data-status="disabled">' +
							'        <span class="fa fa-times"></span> ' + coTranslate('cancel') +
							'    </button>' +
							'</div>' +
							'<div class="btn-group">' +
							'    <button type="button" class="btn btn-default" data-status="done">' +
							'        <span class="fa fa-thumbs-up"></span> ' + coTranslate('finished') +
							'    </button>' +
							'</div>';
						break;
				}

				var tags = globals.tags;
				['discuter', 'totest'].forEach(function(tag){
					var indexOf = tags.indexOf(tag);
					if (indexOf >= 0) {
						tags.splice(indexOf, 1);
						var paramDeleteArray = {
							collection : 'actions',
							id : globals.actionId,
							pull : "tags",
							path : "tags." + indexOf,
							value : null
						}
						dataHelper.path2Value(paramDeleteArray, function(){});
					}
				});

				var params = {
					collection : 'actions',
					id : globals.actionId,
					path : 'status',
					value : status
				};
				dataHelper.path2Value(params, function(response){
					// Object regroupant les différents status d'une action
					if (response.result) {
						$('#action-status-switcher').empty().html(newHtml);
						updateAfterContribution(globals.contributors);
						if (status === 'disabled') {
							if (wsCO !== null && coWsConfig !== null && typeof coWsConfig.pingActionManagement === 'string' && typeof wsCO.id === 'string') {
								ajaxPost(null, coWsConfig.pingActionManagement, {
									event : '.archive' + globals.parentId,
									data : {
										action : globals.actionId,
										emiter : ''
									}
								}, null, null, {contentType : 'application/json'});
							}
						} else {
							if (wsCO !== null && coWsConfig !== null && typeof coWsConfig.pingActionManagement === 'string' && typeof wsCO.id === 'string') {
								ajaxPost(null, coWsConfig.pingActionManagement, {
									event : '.aap-kanban-move' + globals.parentId,
									data : {
										action : globals.actionId,
										emiter : '',
										target : status,
										position : action.position
									}
								}, null, null, {contentType : 'application/json'});
							}
						}
					}
				});
			})
			.on('click', '#action-modal-participate:not(.disabled)', function(){
				var contributor = {};
				var self = $(this);
				if (notEmpty(userConnected)) {
					contributor[userConnected._id.$id] = {
						type : 'citoyens'
					}

					W.dataHelper.path2Value({
						id : globals.actionId,
						collection : 'actions',
						path : 'links.contributors',
						value : contributor
					}, function(__response){
						if (__response.result && __response.result === true) {
							W.toastr.success(__response.msg);
							var view_tabs = [
								'calendar{{blockKey}}',
								dataHelper.printf('vertical_timeline{{blockKey}}', {blockKey : blockKey}),
								'horizontal_timeline{{blockKey}}',
								'gantt{{blockKey}}'
							];
							if (!globals.isCostumAap) {
								params = {
									id : globals.parentId,
									collection : 'projects',
									path : dataHelper.printf('parent.{{contextId}}', {contextId : W.costum.contextId}),
									value : {
										type : W.costum.contextType,
										name : W.costum.title
									}
								};
								W.dataHelper.path2Value(params, function(){ });
							}
							$('#action-modal-participate').parent('.btn-group').hide();
							$('#action-modal-no-longer-participate').parent('.btn-group').show();
							if (Array.isArray(globals.contributors)) {
								globals.contributors = {};
							}
							globals.contributors[userConnected._id.$id] = {
								name : userConnected.name,
								image : userConnected.profilImageUrl ? userConnected.profilImageUrl : defaultImage
							};
							updateAfterContribution([userConnected._id.$id], 'add');
						}
					});
				}
			})
			.on('click', '#action-modal-no-longer-participate', function(){
				if (notEmpty(userConnected) && typeof globals.contributors[userConnected._id.$id] !== 'undefined') {
					dataHelper.unsetPath({
						id : globals.actionId,
						collection : 'actions',
						path : dataHelper.printf('links.contributors.{{contributor}}', {contributor : userConnected._id.$id})
					}, function(){
						delete globals.contributors[userConnected._id.$id];
						updateAfterContribution([userConnected._id.$id], 'delete');
					})
				}
			})
			.on('click', '#action-modal-assign', function(){
				var assignationContainerDom = $('#assign-action-form');
				if (assignationContainerDom.is(':visible')) {
					assignationContainerDom.hide();
				} else {
					assignationContainerDom.show();
				}
			});

		// Gestion d'upload de fichier
		$('#file-upload-paste').on('paste', function(e){
			e.preventDefault();
			var validImages = [
				'image/jpeg',
				'image/png',
				'image/gif',
				'image/bmp',
				'image/x-ms-bmp',
				'image/webp',
				'image/svg+xml',
				'image/tiff',
				'image/x-icon',
				'image/vnd.microsoft.icon'
			];
			var clipboardData = e.originalEvent.clipboardData;
			if (clipboardData && clipboardData.items) {
				$.each(clipboardData.items, function(_, oneItem){
					if (oneItem.kind === 'file' && validImages.includes(oneItem.type)) {
						uploadImageFiles([oneItem.getAsFile()]);
					} else {
						toastr.error('Type de fichier invalide');
					}
				});
			}
		});
		$('#file-upload-input').on('change', function(){
			var files = this.files;
			if (files.length > 0) {
				$.each(files, function(_, oneFile){
					if (oneFile.type.indexOf('image') >= 0) {
						readFileAsText(oneFile);
					}
				});
			}
		});

		$('#choose-file-button').on('click', function(){
			$('#file-upload-input').trigger('click');
		});

		$('#file-upload-save').on('click', function(){
			var fileToUploadList = [];
			$('.upload-image-container img').each(function(){
				fileToUploadList.push(this.src);
			});
			var size = fileToUploadList.reduce(function(accumulator, currentValue){
				return accumulator + atob(currentValue.split(',')[1]).length;
			}, 0);
			var sizeMb = size / 1024 / 1024;
			var maxFileSize = 7.5;
			if (sizeMb > maxFileSize) {
				toastr.error(`${ trad['Maximum upload size exceeded'] } : ${ maxFileSize }Mb`);
			} else {
				saveMediaImage(fileToUploadList).then(function(messageList){
					var errorMessage = '';
					$.each(messageList, function(_, oneMessage){
						var imageTemplateHtml = '';
						if (oneMessage.status) {
							imageTemplateHtml += htmlImageTemplate(oneMessage);
						} else {
							errorMessage += oneMessage.msg + '<br>';
						}
						var badgeDom = $('a[href="#imagesTab"] .badge');
						badgeDom.text(parseInt(badgeDom.text()) + 1);
						$('#image-container').append(imageTemplateHtml);
					});
					if (notEmpty(errorMessage)) {
						toastr.error(errorMessage);
					}
					$('.delete-upload-preview').trigger('click');
				});
			}
		});

		// Options
		function showHideElement(){
			var activeConditions = {
				mine : $('#show-my-tasks').is(':checked'),
				done : $('#show-done-tasks').is(':checked')
			};
			$('.task-group-container').each(function(){
				var status = this.dataset.status.split(',');
				var statusIncludesDone = status.includes('done');
				var statusIncludesMyTask = status.includes('my-task');
				var active = true;
				if (activeConditions.mine && !statusIncludesMyTask) {
					active = false;
				}
				if (!activeConditions.done && statusIncludesDone) {
					active = false;
				}
				if (active) {
					$(this).slideDown();
				} else {
					$(this).slideUp();
				}
			});
		}

		$('.task_list').on('change', '.option-task', showHideElement);
	});
	$.each(globals.projectContributors, function(contributorId, oneProjectContributor){
		var optionDom = $('<option>', {
			value : contributorId,
			text : oneProjectContributor.name,
			data : {
				image : oneProjectContributor.image
			}
		});
		if (notEmpty(globals.contributors[contributorId])) {
			optionDom.prop('selected', true);
		}
		$('#assign-action-select').append(optionDom);
	});
	$('#assign-action-select').select2(assignSelectOption);

	$('#assign-action-form>.btn').on('click', function(){
		var contributorList = Array.isArray($('#assign-action-select').val()) ? $('#assign-action-select').val() : [];
		setContributors(contributorList).then(function(contributorList){
			globals.contributors = {};
			contributorList.forEach(function(contributor){
				globals.contributors[contributor.id] = {
					name : contributor.name,
					image : contributor.image
				};
			});
			updateAfterContribution(contributorList, 'update');
			pingContributor(globals.actionId, contributorList.map(function(contributorMap){ return contributorMap.id; }), 'update');
		});
	});

	$('#description-editor-container [type=button]').on('click', function(){
		var textarea = $('#description-editor-container textarea');
		if (textarea.val()) {
			var path2Value = {
				id : globals.actionId,
				collection : 'actions',
				value : textarea.val(),
				path : 'description'
			}
			$('#description-content p').empty().html(textarea.val().replace(/\r?\n/g, '<br />')).show();
			$('.edit-description').show();
			$('#description-editor-container').hide();
			dataHelper.path2Value(path2Value, function(response){
				if (response.result) {
					toastr.success(response.msg);
				}
			});
		}
	});

	$('.edit-description').on('click', function(){
		$(this).hide();
		$('#description-editor-container').show();
		$('#description-content p').hide();
	});

	$('.document-compact-list').on('click', '.document-upload-form', function(event){
		$('#document-upload-input').trigger('click');
	});
	$('.image-compact-list').on('click', '.image-upload-form', function(event){
		$('#image-upload-input').trigger('click');
	});
	$('.document-compact-list:not(.disable), .image-compact-list:not(.disable)').on('dragover', function(e){
		e.preventDefault();
	}).on('drop', function(e){
		e.preventDefault();
		var fileList = e.originalEvent.dataTransfer.files;
		if (fileList.length > 0) {
			if ($(this).hasClass('document-compact-list')) {
				uploadDocumentFiles(fileList);
			} else {
				uploadImageFiles(fileList);
			}
		}
	});
	$('#document-upload-input').on('change', function(e){
		var fileList = e.target.files;
		if (fileList.length > 0) {
			uploadDocumentFiles(fileList);
		}
	});
	$('#image-upload-input').on('change', function(e){
		var fileList = e.target.files;
		if (fileList.length > 0) {
			uploadImageFiles(fileList);
		}
	});
	if (globals.mode === 'r') {
		$('#file-upload-paste').prop('disabled', true);
		$('.image-compact-list, .document-compact-list').addClass('disable');
		$('#image-upload-container, #document-container').remove();
	}
}