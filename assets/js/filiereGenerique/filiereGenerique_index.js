dyFObj.unloggedMode=true;

costum[costum.slug] = {
	init : function(){
		coInterface.bindButtonOpenForm = function(){

			$(".btn-open-form, #joinBtn").off().on("click",function(){
				setTimeout(function() {
					$('.addListLineBtncertificationsAwards').text(trad.addrow);
					if (costum.slug == "ries") {
						$('#btn-submit-form').attr("disabled", true);
					}
					$('.extraInfo-agreecheckboxSimple').click(function(){
						if ($('#extraInfo-agree').val() == "true")
							$('#btn-submit-form').attr("disabled", false);
						else
							$('#btn-submit-form').attr("disabled", true);
					});
					$(".typeselect").hide();

				}, 800);

				var typeForm = $(this).data("form-type");

				if(typeForm =="reseau"){
					getNetwork();
				}
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				if(contextData && contextData.type && contextData.id ){
					dyFObj.openForm(typeForm);
				}else{
					/*let dynFormCostumIn = {}
					if(typeof costum[costum.slug][typeForm]!="undefined" && typeof costum[costum.slug][typeForm].afterSave != "undefined"){
						dynFormCostumIn.afterSave = costum[costum.slug][typeForm]
					}*/
					dyFObj.openForm(typeForm);
				}
			});
		};
		coInterface.bindButtonOpenForm();
	},
	"organizations" : {
		formData : function(data){
			var certifications = [];
			$.each(data, function(e, v){
				if(e.indexOf("-") != -1) {
					ee = e.split("-").join("[")+"]";
					data[ee] = data[e];
					delete data[e]
				}

				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];

						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);
							});
						}
					}
					delete data[e];
				}
			});
			return data;
		},
		afterSave : function(orga){
			var data = {
				type : orga.map.type,
				id : orga.map._id["$id"],
				collection:"organizations",
				path : "allToRoot",
				value : {"type": orga.map.group}
			};
			dataHelper.path2Value( data, function(params) {
				urlCtrl.loadByHash("#@"+orga.map.slug+".view.detail");
			} );
		}
	}
}

costum.lists.budget = {
	"Da 1 a 25.000 euro" : "Da 1 a 25.000 euro",
	"Da 25.000 a 50.000 euro" : "Da 25.000 a 50.000 euro",
	"Da 50.000 a 100.000 euro" : "Da 50.000 a 100.000 euro",
	"Da 100.000 a 500.000 euro" : "Da 100.000 a 500.000 euro",
	"Oltre 500.000 euro" : "Oltre 500.000 euro"
}

costum.lists.types =  {
	"NGO" : trad.ong,
	"GAS" : "GAS",
	"Associazione":"Associazione",
	"Cooperative":"Cooperativa",
	"Consorzio":"Consorzio",
	"Impresa sociale":"Impresa sociale",
	"LocalBusiness" : trad.entreprise,
	"GovernmentOrganization" : trad.servicepublic,
	"university" : trad.university,
	"international organization" : trad.internationalorganization,
	"foundation" : trad.foundation,
	"entity association or religious community" : trad.entityassociationorreligiouscommunity,
	"self-propelled social space" : trad.selfpropelledsocialspace,
	"political collective" : trad.politicalcollective,
	"syndicate" : trad.syndicate,
	"party" : trad.politicalParty,
	"media" : "media",
	"Group" : "Altri",
}

/*
costum.lists.theme = {};

costum.lists.theme[tradCategory.food] =  {
	"agricultural production" : trad.agriculturalproduction,
	"GAS" : "GAS",
	"Shops of the world" : trad.shopsoftheworld,
	"CSA" : "CSA",
	"Urban gardens" : trad.urbangardens,
	"Farmers markets" : trad.farmersmarkets,
	"Catering and administration" : trad.cateringandadministration
}

costum.lists.theme[tradCategory.productionandservices] =  {
	"Carpenters" : trad.carpenters,
	"Social tailors" : trad.socialtailors,
	"Fair trade" : trad.fairtrade
}

costum.lists.theme[tradCategory.education] =  {
	"Cultural activities" : trad.culturalactivities,
	"Education and training" : trad.educationandtraining,
	"Research" : trad.research,
	"Information" : trad.information
}

costum.lists.theme[tradCategory.dwelling] =  {
	"Sustainable living" : trad.sustainableliving,
	"Territory care" : trad.territorycare,
	"Ciclofficine" : "Ciclofficine",
	"Other forms of sustainable mobility" : trad.otherformsofsustainablemobility,
	"Building" : trad.building
}

costum.lists.theme[trad.finance] =  {
	"Social coins" : trad.socialcoins,
	"MAG" : "MAG",
	"Shops of the world" : trad.shopsoftheworld
}

costum.lists.theme[tradCategory.health] =  {
	"Sport" : "Sport",
	"Health and care" : trad.healthandcare,
	"Counters for psychological listening" : trad.countersforpsychologicallistening,
	"Mutual aid benches" : trad.mutualaidbenches
}

costum.lists.theme[trad.energie] =  {
	"Energy supplies" : trad.energysupplies,
	"Energy production" : trad.energyproduction,
	"Wireless Networks" : trad.wirelessnetworks,
	"Repair café" : "Repair café",
	"Fablab" : "Fablab"
}

costum.lists.theme[tradCategory.citizen] =  {
	"Legal branches" : trad.legalbranches,
	"Advocacy and local policies" : trad.advocacyandlocalpolicies,
	"Neighborhood committees" : trad.neighborhoodcommittees
}

costum.lists.theme[trad.travelandhospitality] =  {
	"Travel and hospitality" : trad.travelandhospitality
}*/

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.group["label"] = trad.choosemoretypeoforganization
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.group["placeholder"] =  trad.select
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.theme["label"] = trad.themes
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.theme["placeholder"] =  trad.select

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["type"] = {
	"inputType" : "select",
	"labelInformation" : "typo",
	"class" : "hidden",
	"value" : "group"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"] =  {
	"inputType" : "select",
	"label" : trad.typeoforganization,
	"order" : "3",
	"noOrder" : true,
	"class" : "form-control",
	"labelInformation" : "groupo",
	"list" : "types"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["activityPlace"] = {
	"placeholder" : trad.select,
	"inputType" : "selectMultiple",
	"label" : trad.wheredoyoudoyourbusiness,
	"class" : "multi-select",
	"options" : {
		"head office" : trad.headoffice,
		"online" : trad.activityonline,
		"home" : trad.athome
	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo[contacts]"] = {
	"placeholder" : trad.addressedto,
	"inputType" : "selectMultiple",
	"label" : "Principali destinatari dell’attività",
	"class" : "multi-select contacts",
	"options" : {
		"Aimed at everyone" : trad.aimedateveryone,
		"Public institutions" : trad.publicinstitutions,
		"Religious bodies" : trad.religiousbodies,
		"Organization NGO" : trad.ong,
		"Companies" : trad.companies,
		"Inhabitants of the neighborhood" : trad.inhabitantsneighborhood,
		"Solidarity Purchase Groups" : trad.solidaritypurchasegroups
	}
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["certificationsAwards"] = {
	"label" : trad.certificationsorawards,
	"inputType" : "lists",
	"entries" : {
		"certAward" : {
			"label" : trad.certificationsorawards,
			"type" : "text"
		}
	}
}


costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["description"] = {
	"label" : tradDynForm.longDescription,
	"inputType" : "textarea",
	"order" : "13"
}


costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-reteofapartment"] = {
	"label" : trad.reteofapartment,
	"inputType" : "tags",
	"placeholder" : "RIES, RESS Roma, Equogarantito, Numeri Pari, ...	",
	"values" : []
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-dimensions"] = {
	"label" : trad.dimensions,
	"inputType" : "select",
	"class" : "form-control",
	"placeholder" : trad.select,
	"options" : {
		"Regional" : trad.regional,
		"National" : trad.national,
		"Local" : trad.local,
		"International" : trad.international
	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-transformativehorizons"] = {
	"label" : trad.transformativehorizons,
	"inputType" : "selectMultiple",
	"class" : "multi-select",
	"placeholder" : trad.select,
	"options" : {
		"Economia sociale e solidale": trad.socialandsolidarityeconomy,
		"Economia sociale" : trad.socialeconomy,
		"Ecological economics" : trad.ecologicaleconomics,
		"Non-violent economy" : trad.nonviolenteconomy,
		"Circular economy" : trad.circulareconomy,
		"Economy of the common good" : trad.economyofthecommongood,
		"Community economy" : trad.communityeconomy,
		"Gift economy" : trad.gifteconomy,
		"Collaborative economies" : trad.collaborativeeconomies,
		"Civil economy" : trad.civileconomy,
		"Ecofeminist economics" : trad.ecofeministeconomics,
		"Economies of degrowth" : trad.economiesofdegrowth,
		"Migrant protagonist" : trad.migrantprotagonist,
		"Inclusive economies" : trad.Inclusiveeconomies,
		"Ethical finance" : trad.ethicalfinance,
		"Mutualism" : trad.mutualism,
		"Agroecology" : trad.agroecology

	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-budget"] =  {
	"inputType" : "select",
	"label" : "Dimensione economica (budget)",
	"class" : "form-control",
	"labelInformation" : "Dimensione economica",
	"noOrder" : true,
	"list" : "budget"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-agree"] = {
	"label" : trad.readandacceptprivacyinformation,
	"inputType" : "checkboxSimple",
	"rules" : {
		"required" : true
	},
	"params" : {
		"onText" : trad.yes,
		"offText" : trad.no,
		"onLabel" : trad.yes+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>",
		"offLabel" : trad.no+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>"
	},
	"checked" : false
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"] = {
	"label" : trad.wouldliketoreceiveinformationfromtheobservatory,
	"inputType" : "checkboxSimple",
	"params" : {
		"onText" : trad.yes,
		"offText" : trad.no,
		"onLabel" : trad.yes,
		"offLabel" : trad.no
	},
	"checked" : false
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["people"] = {
	"inputType" : "custom",
	"order" : "16",
	"html" : "<label class='col-xs-12 text-left control-label no-padding'><i class='fa fa-chevron-down'></i>"+trad.howmanypeopleareinvolvedintheactivities+"</label><div class='col-xs-12 no-padding'><div class='listEntry col-xs-12 no-padding' data-num='0'><div class='col-md-5'><div class='padding-5 col-xs-12'><h6>"+trad.workers+" </h6></div><div class='space5'></div><input type='number' min='0' name='extraInfo[peopleinvolved][workers]' data-entry='workers' id='extraInfo[peopleinvolved][workers]' class=' form-control input-md col-xs-12 no-padding workers' value='' placeholder=''></div><div class='col-md-5'><div class='padding-5 col-xs-12'><h6>"+trad.volunteers+" </h6></div><div class='space5'></div><input type='number' min='0' name='extraInfo[peopleinvolved][volunteers]' data-entry='volunteers' id='extraInfo[peopleinvolved][volunteers]' class='form-control input-md col-xs-12 no-padding volunteers' value='' placeholder=''></div></div></div>"
}



/** Themes Editor */

var themesEditor = {
	dynForm : {},
	queryParams:{
		value:{}
	},
	macroThemeLength:0,
	init:function(te){
		te.initEvents(te);
	},
	openDynForm:function(te, btn){
		te.dynForm["costumThemesParamsData"] = {};
		te.dynForm["costumThemesParams"] = {
				"jsonSchema" : {
						"title" : "Editeur de thèmes",
						"description" : "",
						"icon" : "fa-cog",
						"noSubmitBtns":true,
						"properties" : {
								themes : {
									"inputType" : "custom",
									"html" : te.generateEditor(te)
								}
						}
				}
		}
		te.queryParams.id = btn.data("id");
		te.queryParams.collection = btn.data("collection");
		dyFObj.openForm( te.dynForm.costumThemesParams, null, {})
	},
	generateEditor:function(te){
		var themesEditorHTML = "<div class='row equal text-left'>";
		for (var macroTm in costum.lists.theme) {
			te.queryParams.value[macroTm] = {}
			themesEditorHTML+="<div class='col-md-4 padding-5 macroTm"+te.macroThemeLength+"'>";
			themesEditorHTML+="<div contenteditable class='textMicroTheme bg-blue padding-5 fs-14pt text-bolder text-white' data-path='costum.lists.theme' data-old='"+macroTm+"'>"+macroTm+"</div>"
			for (var microTm in costum.lists.theme[macroTm]) {
				if (costum.lists.theme[macroTm].hasOwnProperty(microTm)) {
					themesEditorHTML+="<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme'  data-macrotm='"+macroTm+"' data-oldvalue='"+microTm+"' style='border:1px dashed #eee'>"+microTm+"</div>"
					te.queryParams.value[macroTm][microTm] = microTm;
				}
			}
			themesEditorHTML+="<a class='btn btn-sm btn-success btnNewTm padding-right-20 padding-left-20' data-index='macroTm"+te.macroThemeLength+"' data-theme='"+macroTm+"'><i class='fa fa-plus'></i></a>"
			themesEditorHTML+="</div>";
			te.macroThemeLength ++;
		}
		themesEditorHTML+="<a href='javascript:;' class='col-md-4 padding-5 text-info text-center newColumn' data-index='"+te.macroThemeLength+"' style='font-size:40pt; border-radius:5px; border:1px dashed #eee'>+</a>";
		themesEditorHTML+="</div>";
		return themesEditorHTML;
	},
	initEvents:function(te){

		$(document).on("click", ".mega-menu-dropdown", function(e){
			if($(".btnEditMegaMenu").length==0 && userId && typeof costum.admins[userId] != "undefined" && costum.admins[userId].isAdmin==true){
				$(".mega-menu-container").prepend("<button class='btn bg-dark text-white btn-sm padding-right-20 padding-left-20 btnEditMegaMenu pull-right' data-id='"+costum.contextId+"' data-collection='"+costum.contextType+"'>"+trad.edit+"</button>");
			}
		});

		$(document).on("click", ".btnEditMegaMenu", function(e){
			te.openDynForm(te, $(this));
		});

		$(document).on("blur", ".textMicroTheme", function(e){
			te.helpers.saveChanges(te, $(this))
		});

		$(document).on("click", ".btnNewTm", function(e){
			$("<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme' data-macrotm='"+$(this).data("theme")+"' style='border:1px dashed #eee'></div>").insertBefore($(this));
		});

		$(document).on("click", ".newColumn", function(e){
			$(te.helpers.macroThemeHTML(te, $(this))).insertBefore($(this));
		});

		$(document).on("click", "#deleteTheme", function(e){

		})
	},
	helpers:{
		macroThemeHTML:function(te, element){
			//let te.macroThemeLength = element.data("index");
			let newColumn = "";
			te.queryParams["Macro theme "+te.macroThemeLength] = {};
			te.queryParams["Macro theme "+te.macroThemeLength]["New micro theme"] = "New micro theme";

			newColumn += "<div class='col-md-4 padding-5 macroTm"+te.macroThemeLength+"'>";
			newColumn += "<div contenteditable class='textMicroTheme bg-blue padding-5 fs-14pt text-bolder text-white' data-path='costum.lists.theme' data-old='Macro theme "+te.macroThemeLength+"'>Macro theme "+te.macroThemeLength+"</div>";
			newColumn += "<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme'  data-macrotm='Macro theme "+te.macroThemeLength+"' data-oldvalue='micro theme "+te.macroThemeLength+"' style='border:1px dashed #eee'>micro theme "+te.macroThemeLength+"</div>";
			newColumn += "<a class='btn btn-sm btn-success btnNewTm padding-right-20 padding-left-20' data-index='macroTm"+te.macroThemeLength+"' data-theme='Macro theme "+te.macroThemeLength+"'><i class='fa fa-plus'></i></a>";
			newColumn += "</div>";
			te.macroThemeLength++;
			return newColumn;
		},

		saveChanges:function(te, element){
			let macroTheme = element.data("macrotm");
			let theme = element.text();
			te.queryParams.path = element.data("path");

			if(theme!=""){
				if(typeof te.queryParams.value == "undefined"){
					te.queryParams.value = {};
				}

				if(typeof macroTheme!="undefined" && typeof te.queryParams.value[macroTheme]=="undefined"){
					te.queryParams.value[macroTheme] = {};
				}

				if(typeof macroTheme == "undefined"){
					te.queryParams.value[theme] = te.queryParams.value[element.data("old")];
					if(typeof te.queryParams.value[element.data("old")]=="undefined"){
						te.queryParams.value[theme] = {};
					}
					if(theme!=element.data("old")){
						$("[data-macrotm='"+element.data('old')+"']").each(function(i){
							$(this).data("macrotm", theme);
						});
						delete te.queryParams.value[element.data("old")];
					}
					element.data("old", theme);
				}else{
					te.queryParams.value[macroTheme][theme] = theme;
				}
			}else{
				if(typeof macroTheme == "undefined"){
					bootbox.dialog({
						message:'<div class="alert-white text-center"><br><strong><?php echo Yii::t("common","Voulez vous vraiement supprimer cette macro thème, ça va supprimer les micro thèmes correspondants") ?></strong><br><br></div>',
						buttons: {
			        cancel: {
			            label: "Non, Annuler",
			            className: 'btn-danger',
			        },
			        ok: {
			            label: "Je confirme",
			            className: 'btn-info',
			            callback: function(){
										delete te.queryParams.value[element.data("old")];
										te.helpers.update(te, "deletion successfully");
										element.parent().remove();
			            }
			        }
			    }
				});
					/*bootbox.confirm("lqkdjfmqkdfjqmdkfq", function(){
						delete te.queryParams.value[element.data("old")];
					})*/
				}else{
					delete te.queryParams.value[macroTheme][element.data("oldvalue")];
				}
			}

			if(typeof te.queryParams.value == undefined){
				toastr.error('value cannot be empty!');
			}else if(typeof macroThem=="undefined"){
				te.helpers.update(te, "Update successfully");
			}
		},

		update:function(te, message){
			//alert(JSON.stringify(te.queryParams));
			//te.queryParams.updatePartial=true;
			te.queryParams.removeCache=true;
			//te.queryParams.format=true;
			dataHelper.path2Value(te.queryParams, function(data) {
				if(data.result){
					costum.lists.theme = data.elt.costum.lists.theme;
					toastr.success(message);
				}
			});
		}
	}
}

//themesEditor.init(themesEditor);
