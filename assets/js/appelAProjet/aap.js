var selected_action = '';
var selected_project = '';
var selected_task = null;
var aapObject = {
    formParent: {},
    formConfig: {},
    formInputs: {},
    elementAap: {},
    allAnswers: {},
    aapStep1Inputs: {},
    useMap: (localStorage.getItem("useAapMap") === "true"),
    useMapInView: function () {
        var view = ["view=kanban-personal-action", "actionOf=", "view=kanbanaap","view=project_kanban"];
        var useMap = true;
        $.each(view, function (k, v) {
            if (location.hash.indexOf(v) != -1) {
                useMap = false;
            }
        })
        return useMap;
    },
    init: function () {
        aapObject.sections.common.events(aapObject);
        aapObject.sections.proposition.events(aapObject);
        let topContentHeight = $("#mainNav").height() + $(".container.no-padding.fixed-breadcrumb-filters").height() + 50;
        $("#show-filters-lg").css({ position: "sticky" });
        $("#filterContainerL").css({ position: "sticky" });
    },
    interventionAreaOld: {
        "Centre Ville": "Centre Ville",
        "Marcadet": "Marcadet",
        "Bas-de-la-Rivière": "Bas-de-la-Rivière",
        "Butor": "Butor",
        "Sainte-Clotilde": "Sainte-Clotilde",
        "Montagne 8ème": "Montagne 8ème",
        "Montagne 15 ème": "Montagne 15 ème",
        "Source": "Source",
        "Bellepierre": "Bellepierre",
        "Brûlé": "Brûlé",
        "Vauban": "Vauban",
        "Camélia": "Camélia",
        "Providence": "Providence",
        "Saint-François": "Saint-François",
        "Montgaillard": "Montgaillard",
        "Chaudron": "Chaudron",
        "Primat": "Primat",
        "Moufia 2": "Moufia 2",
        "Moufia": "Moufia",
        "Bois-de-Nèfle": "Bois-de-Nèfle",
        "Bretagne": "Bretagne",
        "Domenjod": "Domenjod"
    },
    tags: [
        "Salaire",
        "Parcours Insertion et Emploi",
        "Education",
        "Cadre de vie",
        "Culture",
        "Sport",
        "Jardins",
        "Santé",
        "Seniors",
        "Prévention de la délinquance",
        "Soutien aux initiatives",
        "Intégration",
        "Vie associative",
        "Animation de proximité",
        "Solidarité",
        "Citoyenneté",
        "Intergénérationel",
        "Transition Ecologique",
        "Accompagement dans les démarches administratives",
        "Jeunesse",
        "Egalité Femme - Homme"
    ],
    sections: {
        proposition: {
            activeFilters: {},
            communicationTools: {

            },
            events: function (obj) {
                $('.communication-tools').off().on('click', function () {
                    /*ajaxPost("", baseUrl+'/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.communicationTools',
                    {

                    },
                    function(){
                    },null,null,
                    {
                        async : false
                    });*/
                    smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.communicationTools/params/' + obj.formParent._id.$id);
                })
                $('.duplicate-prop-tools').off().on('click', function () {
                    smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.duplicatePropTools/params/' + obj.formParent._id.$id);
                })
                $("#filterContainerL .btn-filters-select").each(function (k, elmt) {
                    elmt.addEventListener("click", function () {
                        //localStorage.setItem(lObj.aapFiltersHistory,JSON.stringify(filterSearch.search.obj))
                        mylog.log("")
                    })
                })
                $('.email-table-stat').off().on('click', function () {
                    smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.maillist/params/' + obj.formParent._id.$id);
                })
            }
        },
        list: {
            events: function (obj) {

            },
            filters: {
                getUrlSearchParams: function (fObj, extraParams = ["nbPage"], checkForced = true) {
                    arrayToSearch = ["text", "types", "tags", "type", "section", "category", "subType", "priceMin", "priceMax", "devise", "source", "actionOf", "projectOf", "actionByproject", "actionByStatus", "interventionArea", "view", "projectid", "inproject", "association", "user", "showprojectpanel"];
                    if (notNull(extraParams)) { arrayToSearch = arrayToSearch.concat(extraParams); }
                    getStatus = "";
                    $.each(arrayToSearch, function (e, label) {
                        if (typeof fObj.search.obj[label] != "undefined") {
                            if (!checkForced || !fObj.search.forcedParams(fObj, label)) {
                                value = encodeURI((Array.isArray(fObj.search.obj[label])) ? fObj.search.obj[label].join(",") : fObj.search.obj[label]);
                                if (label == "text" && notEmpty(value))//>0 || fObj.search.obj[label]!= "")
                                    getStatus += ((getStatus != "") ? "&" : "") + label + "=" + value;
                                else if (label == "nbPage") {
                                    if (Number(value) != 0 && notEmpty(value))
                                        getStatus += ((getStatus != "") ? "&" : "") + label + "=" + (Number(value) + 1);
                                }
                                else if (label == "types" && typeof fObj.search.obj[label] != "undefined") {
                                    //fObj.search.objranges
                                    if (fObj.filters.actions.types.initList.length != fObj.search.obj.types.length && typeof fObj.search.obj.ranges == "undefined")
                                        getStatus += ((getStatus != "") ? "&" : "") + label + "=" + value;
                                    else if (typeof fObj.search.obj.ranges != "undefined") {
                                        var typesInRanges = [];
                                        $.each(fObj.search.obj.ranges, function (e, v) {
                                            typesInRanges.push(e);
                                        });
                                        if (fObj.filters.actions.types.initList.length != typesInRanges.length)
                                            getStatus += ((getStatus != "") ? "&" : "") + label + "=" + typesInRanges.join(",");
                                    }
                                }
                                else if ($.inArray(label, ["startDate", "endDate"]) >= 0) {
                                    if (!notEmpty(fObj.search.obj.text))
                                        getStatus += ((getStatus != "") ? "&" : "") + label + "=" + value;
                                } else if (notEmpty(value))
                                    getStatus += ((getStatus != "") ? "&" : "") + label + "=" + value;
                            }
                        }
                    });
                    return getUrlSearchLocality(getStatus, true);
                }
            }
        },
        listItem: {
            currentAnswer : "",
            events: function (obj) {
                $('.votant-modal-heart').off().on('click', function () {
                    var answerid = $(this).data("answerid");
                    ajaxPost('', baseUrl + "/co2/element/get/type/answers/id/" + answerid,
                        null,
                        function (data) {
                            var answers = data.map;

                            var votants = [];
                            if (exists(answers.vote)) {
                                $.each(answers.vote, function (k, v) {
                                    votants.push(k);
                                })
                            }
                            if (votants.length != 0)
                                ajaxPost('', baseUrl + "/co2/element/get/type/citoyens",
                                    { id: votants, fields: ["name", "profilMediumImageUrl"] },
                                    function (dataVotans) {
                                        var html = "<ul style='list-style-type:none'>";
                                        $.each(dataVotans.map, function (kv, vv) {
                                            html +=
                                                `<li>
                                    <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="50" height="50" style="border-radius: 100%;object-fit:cover"/>
                                    <span>${vv.name}</span>
                                </li>`;
                                        })
                                        html += "</ul>";
                                        bootbox.alert(html);
                                    })
                        }, null);
                })
                $('.votant-modal').off().on('click', function () {
                    var subtype = $(this).data("subtype");
                    var answerid = $(this).data("answerid");

                    ajaxPost('', baseUrl + "/co2/element/get/type/answers/id/" + answerid,
                        null,
                        function (data) {
                            var answers = data.map;
                            var votants = [];
                            if (exists(answers.answers) && exists(answers.answers.aapStep2) && exists(answers.answers.aapStep2.selection)) {
                                $.each(answers.answers.aapStep2.selection, function (k, v) {
                                    votants.push(k);
                                })
                            }
                            if (exists(answers.answers) && exists(answers.answers.aapStep2) && exists(answers.answers.aapStep2.admissibility)) {
                                $.each(answers.answers.aapStep2.admissibility, function (k, v) {
                                    votants.push(k);
                                })
                            }
                            if (votants.length != 0)
                                ajaxPost('', baseUrl + "/co2/element/get/type/citoyens",
                                    { id: votants, fields: ["name", "profilMediumImageUrl"] },
                                    function (dataVotans) {
                                        var html = "<ul style='list-style-type:none'>";
                                        $.each(dataVotans.map, function (kv, vv) {
                                            html +=
                                                `<li>
                                    <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="50" height="50" style="border-radius: 100%;object-fit:cover"/>
                                    <span>${vv.name}</span>
                                </li>`;
                                        })
                                        html += "</ul>";
                                        bootbox.alert(html);
                                    })
                        })
                })
                $('.view-contributors').off().on('click', function () {
                    var dataId = $(this).data("id");
                    var dataType = $(this).data("type");
                    var elem = $(this);
                    ajaxPost("",
                        baseUrl + "/co2/element/get/type/" + dataType + "/id/" + dataId,
                        null,
                        function (data) {
                            if (data.result) {
                                if (exists(data.map) && exists(data.map.links) && exists(data.map.links.contributors)) {
                                    var contributors = Object.keys(data.map.links.contributors);
                                    ajaxPost('', baseUrl + "/co2/element/get/type/citoyens",
                                        { id: contributors, fields: ["name", "profilMediumImageUrl"] },
                                        function (dataVotans) {
                                            var html = `<div class='row'>
                                            <div class="col-xs-12">
                                                <input type="text" class="margin-right-10 exclude-input" value="" id="search-person" placeholder="">
                                            </div>
                                        </div>`;
                                            $.each(dataVotans.map, function (kv, vv) {
                                                var roles = (exists(vv.links) && exists(vv.links.memberOf) && exists(vv.links.memberOf[aapObject.elementAap.id]) && exists(vv.links.memberOf[aapObject.elementAap.id]["roles"])) ? vv.links.memberOf[aapObject.elementAap.id]["roles"] : [];
                                                if (exists(aapObject.elementAap["el"]) && exists(aapObject.elementAap["el"]["collection"]) && aapObject.elementAap["el"]["collection"] === "projects") {
                                                    roles = (exists(vv.links) && exists(vv.links.projects) && exists(vv.links.projects[aapObject.elementAap.id]) && exists(vv.links.projects[aapObject.elementAap.id]["roles"])) ? vv.links.projects[aapObject.elementAap.id]["roles"] : [];
                                                }

                                                html +=
                                                    `<div class='row' id='row-${kv}'>
                                                <div class="col-xs-2">
                                                    <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="45" height="45" style="border-radius: 100%;object-fit:cover"/>
                                                </div>
                                                <div class="col-xs-8">
                                                    <span><small>${vv.name}</small></span><br/>
                                                    <span class="text-green"><small>${roles.join(',')}</small></span>
                                                </div>
                                                <div class="col-xs-1">
                                                <button class="btn btn-xs btn-danger remove-person" data-id="${kv}"> <i class="fa fa-times"></i></button>
                                                </div>
                                            </div>`;
                                            })
                                            var dialog = bootbox.dialog({
                                                title: trad.contributors + (!exists(data.map.links.contributors[userId]) ? "<button type='button' class='btn btn-sm bg-green-k hover-white btn-contribute pull-right'>" + ucfirst(trad.contribute) + "</button>" : ""),
                                                message: html
                                            });
                                            dialog.init(function () {
                                                $('.btn-contribute').off().on('click', function () {
                                                    dialog.modal('hide')
                                                    var params = {
                                                        parentId: dataId,
                                                        parentType: dataType,
                                                        listInvite: {
                                                            citoyens: {},
                                                            organizations: {}
                                                        }
                                                    }
                                                    params.listInvite.citoyens[userId] = userConnected.name
                                                    ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function (data) {
                                                        toastr.success(trad.saved);
                                                        elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text()) + 1)
                                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                            {
                                                                pers: params.listInvite.citoyens,
                                                                url: window.location.href
                                                            },
                                                            function (data) {

                                                            }, "html");
                                                    })
                                                })
                                                $('.remove-person').off().on('click', function () {
                                                    var btn = $(this);
                                                    var persId = $(this).data('id');
                                                    var persObj = {};
                                                    persObj[persId] = "";
                                                    links.disconnectAjax(dataType, dataId, persId, 'citoyens', 'contributors', null, function () {
                                                        btn.parent().parent().remove();
                                                        elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text()) - 1);
                                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + persId,
                                                            {
                                                                pers: persObj,
                                                                url: window.location.href
                                                            },
                                                            function (data) {

                                                            }, "html");
                                                    })
                                                })
                                                $("#search-person").select2({
                                                    minimumInputLength: 3,
                                                    //tags: true,
                                                    multiple: true,
                                                    placeholder: "Ajouter quelqu'un",
                                                    "tokenSeparators": [','],
                                                    createSearchChoice: function (term, data) {
                                                        if (!data.length)
                                                            return {
                                                                id: term,
                                                                text: term
                                                            };
                                                    },
                                                    initSelection: function (element, callback) {
                                                        /*var data = [];
                                                        $(element.val().split(",")).each(function () {
                                                            //data.push({id: this, text: this});_inviteObj
                                                            data.push({id: this, text: exists(_inviteObj.contributors[this].name) ? _inviteObj.contributors[this].name :""});
                                                        });
                                                        callback(data);*/
                                                    },
                                                    ajax: {
                                                        url: baseUrl + "/" + moduleId + "/search/globalautocomplete",
                                                        dataType: 'json',
                                                        type: "POST",
                                                        quietMillis: 50,
                                                        data: function (term) {
                                                            var prms = {
                                                                name: term,
                                                                searchType: ["citoyens"],
                                                                notSourceKey: true,
                                                                filters: {
                                                                    '$or': {}
                                                                }
                                                            };
                                                            prms.filters['$or']['links.projects.' + aapObject.elementAap.id] = { '$exists': true };
                                                            prms.filters['$or']['links.memberOf.' + aapObject.elementAap.id] = { '$exists': true };
                                                            return prms;
                                                        },
                                                        results: function (data) {
                                                            return {
                                                                results: $.map(Object.values(data.results), function (item) {
                                                                    return {
                                                                        text: item.name,
                                                                        id: item._id.$id
                                                                    }
                                                                })
                                                            };
                                                        }
                                                    }
                                                }).on('change', function (e) {
                                                    var params = {
                                                        parentId: dataId,
                                                        parentType: dataType,
                                                        listInvite: {
                                                            citoyens: {},
                                                            organizations: {}
                                                        }
                                                    }
                                                    if (exists(e.added)) {
                                                        params.listInvite.citoyens[e.added.id] = e.added.text
                                                        ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function (data) {
                                                            elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text()) + 1);
                                                            toastr.success(trad.saved);
                                                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + e.added.id,
                                                                {
                                                                    pers: params.listInvite.citoyens,
                                                                    url: window.location.href
                                                                },
                                                                function (data) {

                                                                }, "html");
                                                        })
                                                    }

                                                    if (exists(e.removed)) {
                                                        links.disconnectAjax(dataType, dataId, e.removed.id, 'citoyens', 'contributors', null, function () {
                                                            elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text()) - 1);
                                                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/rmperson/answerid/' + e.removed.id,
                                                                {
                                                                    pers: params.listInvite.citoyens,
                                                                    url: window.location.href
                                                                },
                                                                function (data) {

                                                                }, "html");
                                                        })
                                                    }
                                                });
                                                $("#s2id_search-person").addClass("col-xs-12 no-padding margin-bottom-15");
                                            });
                                        })
                                } else {
                                    if (notNull(userConnected)) {
                                        var dialog = bootbox.dialog({
                                            title: trad.noone + " " + trad.contributor,
                                            message: "<button type='button' class='btn btn-sm bg-green-k hover-white btn-block btn-contribute'>" + ucfirst(trad.contribute) + "</button>"
                                        });
                                        dialog.init(function () {
                                            $('.btn-contribute').off().on('click', function () {
                                                dialog.modal('hide')
                                                var params = {
                                                    parentId: dataId,
                                                    parentType: dataType,
                                                    listInvite: {
                                                        citoyens: {},
                                                        organizations: {}
                                                    }
                                                }
                                                params.listInvite.citoyens[userId] = userConnected.name
                                                ajaxPost("", baseUrl + '/' + moduleId + "/link/multiconnect", params, function (data) {
                                                    toastr.success(trad.saved);
                                                    elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text()) + 1)
                                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                        {
                                                            pers: params.listInvite.citoyens,
                                                            url: window.location.href
                                                        },
                                                        function (data) {

                                                        }, "html");
                                                })
                                            })
                                        });
                                    }
                                }
                            }
                        },
                        null,
                        "json"
                    );
                })
                $('.btn-search-association').off().on('click', function () {
                    var txt = $(this).text();
                    $('[data-field="association"]').val(txt).trigger('keyup');
                })
                $(".btn-download-pdf").on("click", function () {
                    answerid = $(this).data("id");

                    var activeForm = {
                        "jsonSchema": {
                            "title": "Choisir les champs à imprimer",
                            "type": "object",
                            onLoads: {
                                onload: function (data) {
                                    $(".parentfinder").css("display", "none");
                                    $.each(keyInput, function (kIn, valIn) {
                                        $("#" + kIn).change(function () {
                                            if ($("#" + kIn).is(":checked") == true) {
                                                activeForm.jsonSchema.properties[kIn].value = true;
                                            } else {
                                                activeForm.jsonSchema.properties[kIn].value = false;
                                            }
                                        })

                                    })
                                }
                            },
                            "properties": {
                            },
                            save: function (data) {

                                tplCtx.value = {};
                                tplCtx.path = "champPrint";
                                tplCtx.collection = "forms";
                                tplCtx.id = aapObject.formParent._id.$id;
                                $.each(activeForm.jsonSchema.properties, function (k, val) {
                                    tplCtx.value[k] = activeForm.jsonSchema.properties[k].value;
                                });
                                if (typeof tplCtx.value == "undefined")
                                    toastr.error('value cannot be empty!');
                                else {
                                    dataHelper.path2Value(tplCtx, function (params) {
                                        dyFObj.commonAfterSave(params, function () {
                                            toastr.success("Élément bien ajouter");
                                            $("#ajax-modal").modal('hide');
                                            window.open(baseUrl + '/co2/aap/exportpdf/formid/' + aapObject.formParent._id.$id + '/answerid/' + answerid);
                                        });
                                    });
                                }
                            }
                        }
                    };

                    $.each(keyInput, function (kIn, valIn) {
                        activeForm.jsonSchema.properties[kIn] = {
                            "inputType": "checkbox",
                            "placeholder": valIn,
                            "checked": (typeof aapObject.formParent.champPrint != "undefined" && typeof aapObject.formParent.champPrint[kIn] != "undefined" && aapObject.formParent.champPrint[kIn] == "true") ? true : false,
                        }
                        if (kIn == "association" ||
                            kIn == "titre" ||
                            kIn == "aapStep1l0ru4g66a6r7lfwtay" ||
                            kIn == "interventionArea" ||
                            kIn == "aapStep1l0kythybqx6i2gldi1" ||
                            kIn == "description" ||
                            kIn == "nameProjectManager" ||
                            kIn == "phoneProjectManager" ||
                            kIn == "aapStep1l0m5iq15701exxuxwq" ||
                            kIn == "startDate" ||
                            kIn == "plannedDuration" ||
                            kIn == "exactLocationAction" ||
                            kIn == "numberStakeholdersProject" ||
                            kIn == "totalNumberBeneficiaries" ||
                            kIn == "aapStep1l1lzd7y6jak23ytnjpd" ||
                            kIn == "budget" ||
                            kIn == "depense" ||
                            kIn == "aapStep1l1xfy8ox73zuib530a3" ||
                            kIn == "aapStep1l1xfp33q5bg1bz8mwbg" ||
                            kIn == "aapStep1l1xft16v6ohm6m5o0wu"
                        )
                            activeForm.jsonSchema.properties[kIn].value = true;

                    });

                    dyFObj.openForm(activeForm);


                });
                $('.load-pdf-panel').off().on('click', function () {
                    smallMenu.openAjaxHTML(baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.exportation-pdf-cosindnismarterre/params/' + $(this).data('ans'));
                })
                $('.rater-admin').each(function (i, obj) {
                    var starRatingAdmin = raterJs({
                        rating: (typeof $(obj).data("rating") == "number" ? $(obj).data("rating") : 0),
                        starSize: 20,
                        readOnly: true,
                        showToolTip: false,
                        element: obj,
                    });
                });
                $.each($(".list-markdown"), function (k, v) {
                    descHtml = dataHelper.markdownToHtml($(v).html(), {
                        parseImgDimensions: true,
                        simplifiedAutoLink: true,
                        strikethrough: true,
                        tables: true,
                        openLinksInNewWindow: true
                    });
                    $(v).html(descHtml);
                });
                $('.collapse.collapse-detailed').on('shown.bs.collapse', function() {
                    $('[href=#'+$(this).attr('id')+']').html('<i class="fa fa-chevron-up"></i>');
                });

                $('.collapse.collapse-detailed').on('hidden.bs.collapse', function() {
                    $('[href=#'+$(this).attr('id')+']').html('<i class="fa fa-chevron-down"></i>');
                });
                const arr = document.querySelectorAll('img.lzy_img')
                arr.forEach((v) => {
                    imageObserver.observe(v);
                });
            },
            reloadPanel : function(view){
                var prms = {
                    answerId : aapObject.sections.listItem.currentAnswer,
                    formId : aapObject.formParent._id.$id,
                    formConfigId : aapObject.formConfig._id.$id,
                    el : {
                        'id' : aapObject.elementAap.id,
                        'type' : aapObject.elementAap.el.collection,
                    },
                    view : view,
                    slug : notEmpty(costum) ? costum.slug : contextData.slug,
                    timezone : Intl.DateTimeFormat().resolvedOptions().timeZone,
                }
                ajaxPost(
                    null,
                    baseUrl+"/co2/aap/reloadpanel/",
                    prms,
                    function(data){
                        $( "#list-"+prms.answerId ).replaceWith(data);
                        var top = {
                            a : $(".fixed-breadcrumb-filters").height() != null ? $(".fixed-breadcrumb-filters").height() : 0,
                            b : $("#mainNav").height() != null ? $("#mainNav").height() : 0
                        }
                        $('html,body').animate({
                            scrollTop: $("#list-"+prms.answerId).offset().top - (top.a + top.b +15)
                        }, 1000);
                        aapObject.init();
                        aapObject.sections.listItem.events(aapObject);
                        aapObject.sections.listItem.select2(aapObject,'.statustags[data-id="'+prms.answerId+'"]');
                    },null,null,{async:false}
                );
            },
            select2 :function(obj,selector){
                var formatState = function (state) {
                    if (!state.id) { return state.text; }
                    var colori = "statusicon";
                    if(state.id && state.id == "abandoned")
                        colori = "statusicondanger";
                    var $state = jQuery(
                        '<span><i class="'+colori+' fa fa-'+obj.statusicon[state.id]+' +"></i> ' + state.text + '</span>'
                    );
                    return $state;
                };
                /*$.getScript("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js", function(data, textStatus, jqxhr) {
                $("<link/>", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
                }).appendTo("head");*/
                    let optionSelect2 = {
                        formatSelection: formatState,
                        closeOnSelect: false,
                        placeholder: "Aucun statut",
                        containerCssClass : 'aapstatus-container',
                        dropdownCssClass: "aapstatus-dropdown"
                    };
                    let $select2 = $(selector).select2(optionSelect2);
                //});
            }
        },
        kanbanProject: {
            events: function (obj) {
                alert("andrana");
            }
        },
        kanbanPersonalAction: {
            events: function (obj) {
                alert("andrana");
            }
        },
        common: { // secion commune
            events: function (obj) {
                var fixMenu = function () {
                    if ($("#menuTopCenter .to-menu-top-center").length == 0) {
                        $('.to-menu-top-center').appendTo('#menuTopCenter');
                        $('.menu-aap-container').parent().parent().remove();
                    } else {
                        $('.menu-aap-container').parent().parent().remove();
                    }
                    var menuTopHeight = ($('#mainNav').height() + parseInt($('#mainNav').css("padding-top")) + parseInt($('#mainNav').css("padding-bottom")));
                    var menuAapHeight = 0;/*parseInt($('.aap-menu').height());*/
                    var breadcrumbHeight = parseInt($('.fixed-breadcrumb-filters').height());
                    var filterContainerLHeight = 0;//isNaN(parseInt($('#filterContainerL').height())) ? 0 : parseInt($('#filterContainerL').height()) ;
                    var coformTopHeight = ($("#mainNav").height() + 76);
                    $('.aap-menu').css('top', menuTopHeight + "px");
                    $('.fixed-breadcrumb-filters').css('top', (menuTopHeight + menuAapHeight) + "px");
                    $('#filterContainerL').css("top", (menuTopHeight + menuAapHeight + breadcrumbHeight + 16 + aapObject.formParent.subType ? 0 : coformTopHeight) + "px");
                    $("#show-filters-lg").css({ position: "sticky", top: menuTopHeight + menuAapHeight + breadcrumbHeight + 16 + aapObject.formParent.subType ? 0 : coformTopHeight });
                    $('.list-aap-container').css('margin-top', (menuAapHeight + breadcrumbHeight + filterContainerLHeight) + 'px');
                    $(".openFilterParams").css({ position: "sticky", top: menuTopHeight + menuAapHeight + breadcrumbHeight + 16 + aapObject.formParent.subType ? 0 : coformTopHeight });
                }
                //var optionsToBurgerMenu = function () {
                    //if($("#menuTopRight .dropdown-menu.arrow_box").length == 0){
                    //mylog.log($('.to-menu-top-center .options .options-admin-dropdown').children(), 'childrenko')
                    //$('.options-admin-dropdown').children().appendTo('#menuTopRight .dropdown-menu.arrow_box')
                    //}
                //}
                //setTimeout(() => { /*fixMenu(),*/ optionsToBurgerMenu(); }, 500);
                var switchFilter = function () {
                    if ($(window).width() < 769) {
                        if($("#menu-top-profil-social").is(':visible')) {
                            if($("#menu-top-btn-group").is(":visible")) {
                                $(".menu-verticale.searchObjCSS").detach().appendTo("#menu-top-profil-social #menu-top-btn-group");
                                $("#filterContainerInside").hide();
                            }
                        } else {
                            $(".menu-verticale.searchObjCSS").detach().appendTo("#menuTopLeft");
                            $("#filterContainerInside").hide()
                        }
                    } else {
                        $(".menu-verticale.searchObjCSS").detach().appendTo("#filterContainerAapOceco");
                        $("#filterContainerInside").show()
                    }
                }
                $(window).resize(function (event) {
                    event.stopImmediatePropagation();
                    /*fixMenu();*/
                    switchFilter()
                });
                $('.btnaapp_link').off().on("click", function () {
                    var url = $(this).data("url")
                    if (location.hash.indexOf('.null.') > 0 || location.hash.indexOf('..') > 0)
                        urlCtrl.loadByHash("#welcome");
                    else {
                        history.pushState('', document.title, baseUrl+url);
                        urlCtrl.loadByHash(location.hash);
                    }

                    $(this).parent().parent().parent().removeClass("in");
                    let burgerBtnIcon = $(this).parent().parent().parent().parent().children("#menuTopLeft").find(".open-xs-menu-collapse").children("i")
                    burgerBtnIcon.removeClass("fa-times");
                    burgerBtnIcon.addClass("fa-bars");

                    if ($(window).width() < 769) {
                        if($("#menu-top-profil-social").is(':visible')) {
                            if($("#menu-top-btn-group").is(":visible")) {
                                $(".menu-verticale.searchObjCSS").detach().appendTo("#menu-top-profil-social #menu-top-btn-group");
                                $("#filterContainerInside").hide();
                            }
                        } else {
                            $(".menu-verticale.searchObjCSS").detach().appendTo("#menuTopLeft");
                        }
                        if (($("#filterContainerAapOceco").is(":visible"))) {
                            $(".menu-verticale.searchObjCSS").detach().appendTo("#filterContainerAapOceco");
                            $(".menu-verticale.searchObjCSS").hide()
                        }
                    }
                });
                if (location.hash.indexOf("actionOf=") != -1)
                    $('.btnaapp_link[data-action=kanban-personal-action]').parent().addClass('active');
                else
                    $('.btnaapp_link[data-action=kanban-personal-action]').parent().removeClass('active');


                /*$(".aapgoto").off().on("click", function() {
                    if (isUserConnected == "unlogged")
                        return $("#modalLogin").modal();
                    mylog.log("azeee", $(this).data("url"));
                    window.location.href = $(this).data("url");
                    urlCtrl.loadByHash(location.hash);
                });*/

                $(".st-enable-btn").on("click", function () {
                    var thisbtn = $(this);
                    if (thisbtn.data('loc') == 'edit') {
                        thisbtn.data('loc', 'save');
                        thisbtn.html('<i class="fa fa-save"></i> Enregistrer');
                        $("select[data-id='" + thisbtn.data('id') + "']").prop("disabled", false);
                    } else if (thisbtn.data('loc') == 'save') {
                        thisbtn.data('loc', 'edit');
                        thisbtn.html('<i class="fa fa-pencil"></i> Modifier status');
                        $("select[data-id='" + thisbtn.data('id') + "']").prop("disabled", true);
                        var tplCtx = {};
                        tplCtx.id = thisbtn.data('id');
                        tplCtx.collection = "answers";
                        tplCtx.path = "status";
                        tplCtx.value = $("select[data-id='" + thisbtn.data('id') + "']").val();
                        dataHelper.path2Value(tplCtx, function (params) {
                            tplCtx.path = "statusUpdated";
                            tplCtx.value = "updatedTime";
                            dataHelper.path2Value(tplCtx, function (params) {
                                aapObject.sections.listItem.currentAnswer = params.elt._id.$id;
                                aapObject.sections.listItem.reloadPanel(viewParams.view);
                            })
                            toastr.success(trad.saved);
                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatus/answerid/' + thisbtn.data('id'), {
                                status: $("select[data-id='" + thisbtn.data('id') + "']").val(),
                                url: window.location.href
                            },
                                function (data) {

                                }, "html");
                        });
                    }
                });

                $(".st-save-btn").on("click", function () {
                    var thisbtn = $(this);
                    thisbtn.removeClass('st-save-btn');
                });

                $('.aapcommunity').on('click', function (e) {
                    e.stopPropagation();
                    var thisid = $(this).data('id');
                    var ansid = $(this).data('id');
                    var ansform = $(this).data('form');
                    var ansformstep = $(this).data('formstep');
                    var ansinputid = $(this).data('inputid');
                    /*ajaxPost(".aapinputcontent", baseUrl+'/survey/answer/getinput/id/'+ansid+'/form/'+ansform+'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid,
                        null,
                        function(res){

                        },null);*/

                    smallMenu.open("<div id='central-container'></div>");
                    contextData = contextDataAap;
                    pageProfil.params.dir = (!notEmpty(pageProfil.params.dir)) ? links.connectType[costum.contextType] : pageProfil.params.dir;
                    pageProfil.directory.init();
                });

                $('.aapgetinputsa').on('click', function (e) {
                    e.stopPropagation();
                    var thisid = $(this).data('id');
                    coInterface.showLoader(".aapinputcontent");
                    var ansid = $(this).data('id');
                    var ansform = $(this).data('form');
                    var ansformstep = $(this).data('formstep');
                    var ansinputid = $(this).data('inputid');
                    /*ajaxPost(".aapinputcontent", baseUrl+'/survey/answer/getinput/id/'+ansid+'/form/'+ansform+'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid,
                        null,
                        function(res){

                        },null);*/
                    smallMenu.openAjaxHTML(baseUrl + '/survey/answer/getinput/id/' + ansid + '/form/' + ansform + '/mode/w/formstep/' + ansformstep + '/inputid/' + ansinputid);
                    if(notNull(aapObject) && notNull(aapObject.sections) && notNull(aapObject.sections.listItem)) {
                        $('.close-modal').off().on('click',function(){
                            if(notEmpty(aapObject.sections.listItem.currentAnswer)){
                                aapObject.sections.listItem.reloadPanel(viewParams.view);
                            }else{
                                aapObject.sections.listItem.currentAnswer = ansid;
                                aapObject.sections.listItem.reloadPanel(viewParams.view);
                            }
                        })
                    }

                });
                $('.open-kanban').off().on('click', function () {
                    $("body").prepend(`<div class="portfolio-modal" id="kanban-project-modal"></div>`);
                    $('#kanban-project-modal').css({
                        position: "fixed", inset: 0, zIndex: "100000", background: "#fff", paddingTop: ($('#mainNav').height() + 10) + "px"
                    }).append(`<div class="close-modal" data-dismiss="modal">
                                <div class="lr">
                                    <div class="rl">
                                    </div>
                                </div>
                            </div>`);
                    $('#kanban-project-modal .close-modal').css("top", ($('#mainNav').height() + 10) + "px")
                    $('#kanban-project-modal .close-modal').on('click', function () {
                        $('#kanban-project-modal').remove();
                    })
                    let ansid = $(this).data('id');
                    let projectid = $(this).data('project');
                    ajaxPost('',
                        baseUrl + '/co2/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.kanban.kanban_project_list',
                        {
                            showProjectPanel: "false",
                            arrAnswers: [ansid],
                        }, function (content) {
                            //smallMenu.open(content,null);
                            $('#kanban-project-modal').append(content);
                        }, "html");
                })

                $('.aapgetobservatory').on('click', function (e) {
                    e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansid = $(this).data('id');

                    smallMenu.openAjaxHTML(baseUrl + '/survey/answer/ocecoformdashboard/answerid/' + ansid);
                });

                $('.aapgetaapview2').off().on('click', function (e) {
                    /*e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansurl = $(this).data('url');
                    //alert(baseUrl+'/survey/form/getaapview/urll'+ansurl);
                    if (typeof formStandalone != "undefined" && formStandalone) {
                        ansurl += "/slugConfig/" + contextDataAap.slug
                    }
                    smallMenu.openAjaxHTML(baseUrl + '/survey/form/getaapview/urll/' + ansurl);*/
                    $(".modal").modal("hide");
                    $("#aapViewFormModal .modal-content .container").html('<h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>');
                    $("#aapViewFormModal").modal("show");
                    aapObject.sections.listItem.currentAnswer = $(this).data("id");
                    ajaxPost(null, baseUrl+'/survey/answer/index/id/'+$(this).data("id")+'/form/'+$(this).data("elformid")+'/mode/w',
                    { url : window.location.href },
                    function(data){
                        ajaxPost(
                            null,
                            baseUrl + '/survey/answer/countvisit/answerid/' + aapObject.sections.listItem.currentAnswer + '/formId/' + aapObject.formParent._id.$id,
                            null,
                            function(data) {
                                if(typeof propostionObj != "undefined")
                                    propostionObj.refreshNewCounter(aapObject.sections.listItem.currentAnswer);
                            },
                            "json"
                        );
                        setTimeout(() => {
                            $("#aapViewFormModal .modal-content .container").html(data);
                        }, 900);
                    },"html");
                });
                $('.aapgetaapview').off().on('click', function (e) {
                    e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansurl = $(this).data('url');
                    //alert(baseUrl+'/survey/form/getaapview/urll'+ansurl);
                    if (typeof formStandalone != "undefined" && formStandalone) {
                        ansurl += "/slugConfig/" + contextDataAap.slug
                    }
                    smallMenu.openAjaxHTML(baseUrl + '/survey/form/getaapview/urll/' + ansurl);
                });

                $(".aapmodalform").off().on('click', function (e) {
                    e.stopPropagation();
                    $("#mainNav").css("z-index", 99998);
                    $(".aapformmodal").fadeIn("slow");
                    var ansurl = $(this).data('url');
                    coInterface.showLoader(".aapformmodal #aapformdynamic");
                    ajaxPost(".aapformmodal #aapformdynamic" , baseUrl + '/survey/form/getaapview/urll/' + ansurl,
                        null,
                        function(){

                        },"html");
                });

                $('.getcoremutable').on('click', function(e) {
                    e.stopPropagation();
                    var depenseid = $(this).data('depenseid');
                    smallMenu.openAjaxHTML( baseUrl+'/survey/form/coremudashboard/tableonly/true/depenseid/'+depenseid );
                });

                $(".aapformmodal__close").click(function () {
                    $(".aapformmodal").fadeOut("slow");
                    $(".aapformmodal #aapformdynamic").html("");
                });

                $('.aapclosemodalform').off().on('click', function (e) {
                    $(".formpopup").removeClass('active');
                });

                $('.btnaapp_plusdinfo,.aap_plusdinfo').off().on("click", function () {
                    window.location.href = $(this).data("url");
                    urlCtrl.loadByHash(location.hash);
                });
                $('.evaluator-panel').click(function (e) {
                    e.stopPropagation();
                });
                $('.tooltips').tooltip();

                $('.btn-retain-reject').on('click', function (e) {
                    e.stopPropagation();
                    var btn = $(this);
                    if (btn.prop("nodeName").toLocaleLowerCase() != "button") {
                        btn = btn.prev();
                    }
                    var id = btn.data("id");
                    var retain = btn.text();
                    mylog.log("button value", btn);
                    var params = {
                        id: id,
                        collection: "answers",
                        path: "allToRoot",
                        format: true,
                        value: {
                            acceptation: ((retain == "Retenu") ? "rejected" : "retained")
                        }
                    }
                    btn.html('<i class="fa fa-refresh fa-spin "></i>');
                    dataHelper.path2Value(params, function (res) {
                        if (res.result) {
                            if (exists(res.elt) && exists(res.elt.acceptation)) {
                                if (res.elt.acceptation == "retained") {
                                    btn.html("<i class='fa fa-hand-rock-o'></i><span class='hidden'>Retenu</span>").fadeIn(500);
                                    btn.attr("data-original-title", "Retenu");
                                    btn.attr("data-retain", "true");
                                    btn.parent().find("span.btn-retain-reject").text('Retenu')
                                    btn.tooltip();
                                    //btn.removeClass("btn-default").addClass("btn-success");
                                } else {
                                    btn.html("<i class='fa fa-thumbs-down'></i><span class='hidden'>Non retenu</span>").fadeIn(500);
                                    btn.attr("data-original-title", "Non retenu");
                                    btn.attr("data-retain", "false");
                                    btn.parent().find("span.btn-retain-reject").text('Non retenu')
                                    btn.tooltip();
                                    //btn.removeClass("btn-success").addClass("btn-default");
                                }
                            }
                        }
                        mylog.log("retenu", res);
                    });
                });

                $('.btn-delete-answer').off().on("click", function (e) {
                    e.stopPropagation();
                    $this = $(this);
                    id = $(this).data("id");
                    projectId = $(this).data("project-id");
                    bootbox.dialog({
                        title: trad.confirm,
                        message: "<span class='text-red bold'><i class='fa fa-warning'></i>"+trad.actionirreversible+"</span>",
                        buttons: [{
                            label: "Ok",
                            className: "btn btn-primary pull-left",
                            callback: function () {
                                getAjax("", baseUrl + "/survey/co/delete/id/" + id, function (data) {
                                    if (data.result) {
                                        toastr.success("Proposition supprimé avec succès !");
                                        $("#list-" + id).remove();
                                        var currentCount = $('.folderCount').text() * 1;
						                currentCount > 0 ? $('.folderCount').html(currentCount - 1) : ''
                                        ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/deleteproposition/answerid/' + $this.data('id'), {
                                            proposition: $this.data('proposition'),
                                            url: window.location.href
                                        },
                                            function (data) { })
                                        if (projectId != "") {
                                            bootbox.dialog({
                                                title: "Supprimer le projet associer ?",
                                                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                                                buttons: [{
                                                    label: "Ok",
                                                    className: "btn btn-primary pull-left",
                                                    callback: function () {
                                                        var url = baseUrl + "/" + moduleId + "/element/delete/id/" + projectId + "/type/projects";
                                                        var param = new Object;
                                                        param.reason = ""
                                                        ajaxPost(
                                                            null,
                                                            url,
                                                            param,
                                                            function (data) {
                                                                if (data.result) {
                                                                    toastr.success(data.msg);
                                                                } else
                                                                    toastr.error(data.msg);
                                                            }
                                                        );
                                                    }
                                                },
                                                {
                                                    label: "Annuler",
                                                    className: "btn btn-default pull-left",
                                                    callback: function () {
                                                        urlCtrl.loadByHash(location.hash);
                                                    }
                                                }
                                                ]
                                            });
                                        }
                                    }
                                }, "html");


                            }
                        },
                        {
                            label: "Annuler",
                            className: "btn btn-default pull-left",
                            callback: function () { }
                        }
                        ]
                    });
                });
                $('.btn-transfer-answer').off().on("click", function (e) {
                    e.stopPropagation();
                    $this = $(this);
                    var id = $(this).data("id");
                    smallMenu.openAjaxHTML(baseUrl + '/survey/answer/transferanswer/id/' + id);
                });
                $('.btn-notification-history').off().on("click", function (e) {
                    e.stopPropagation();
                    $this = $(this);
                    var id = $(this).data("id");
                    ajaxPost('', baseUrl + "/co2/aap/commonaap/action/notification_history_by_anwerid",
                        {
                            answerId : id
                        },
                        function (data) {
                            smallMenu.open(data)
                        }, null
                    );
                });
                $('.like-project').on('click', function () {
                    $btn = $(this);
                    $val = $btn.data('value').toString();
                    var tplCtx = {
                        id: $btn.data("ans"),
                        collection: "answers",
                        format: true,
                        path: "publicRating." + userId,
                    }
                    if ($val == "false") {
                        $btn.data('value', "true");
                        $btn.find('i').removeClass("fa-heart-o").addClass("fa-heart");
                        var count = $btn.find('.counter').text();
                        $btn.find('.counter').text(parseInt(count) + 1);
                        tplCtx.value = $btn.data('value').toString();
                        ajaxPost(null, baseUrl + '/co2/aap/publicrate',
                            tplCtx,
                            function (res) {
                                // if(res.result)
                                //     toastr.success(res.msg)
                                // else
                                //     toastr.error(res.msg)
                            }, null);
                    } else if ($val == "true") {
                        $btn.data('value', "false");
                        $btn.find('i').removeClass("fa-heart").addClass("fa-heart-o");
                        var count = $btn.find('.counter').text();
                        $btn.find('.counter').text(parseInt(count) - 1);
                        tplCtx.value = $btn.data('value').toString();
                        ajaxPost(null, baseUrl + '/co2/aap/publicrate',
                            tplCtx,
                            function (res) {
                                //    if(res.result)
                                //         toastr.success(res.msg)
                                //     else
                                //         toastr.error(res.msg)
                            }, null);
                    }
                })

                $(".aapgoto").off().on("click", function () {
                    if (isUserConnected == "unlogged")
                        return $("#modalLogin").modal();

                    if ($(this).data('communecter') == true)
                        window.open(baseUrl + $(this).data("url"), '_blank');
                    else {
                        history.pushState('', document.title, baseUrl+$(this).data("url"));
                        urlCtrl.loadByHash(location.hash);
                    }
                });
                $('.new-prop-counter').off().on("click", function (e) {
                    e.stopPropagation();
                    if(!location.hash.includes("aappage.list")){
                        var new_hash = `#oceco.slug.${aapObject.elementAap.el.slug}.formid.${aapObject.formParent._id.$id}.aappage.list?view=detailed&inproject=false`
                        history.pushState('', document.title, location.origin+location.pathname+new_hash);
                        urlCtrl.loadByHash(location.hash,null,function(){
                            htmlExists('.btn-filters-select[data-key=notSeen]',function(){
                                $('.btn-filters-select[data-key=notSeen]').trigger('click');
                            })
                        });
                    }else{
                        $('.btn-filters-select[data-key=notSeen]').trigger('click');
                    }
                    
                })
                $('body').on('click', function () {
                    setTimeout(() => {
                        if ($('.options-admin-dropdown').is(":visible")) {
                            $('#filterContainerL').hide(800)
                        } else {
                            $('#filterContainerL').show(800);
                        }
                    }, 500);
                })
                $('.list-aap-map').off().on('click', function () {
                    if ($(this).hasClass('active-view')) {
                        $(this).removeClass('active-view');
                        localStorage.setItem("useAapMap", "false");
                        urlCtrl.loadByHash(location.hash);
                    } else {
                        $(this).addClass('active-view');
                        localStorage.setItem("useAapMap", "true");
                        urlCtrl.loadByHash(location.hash);
                    }
                })
                if (obj.useMap && obj.useMapInView()) {
                    $('.list-aap-map').addClass('active-view')
                } else if (!obj.useMap && obj.useMapInView()) {
                    //setTimeout(() => {
                    $('.map-aap-container').remove();
                    /*$('.bodySearchContainer').removeClass("col-xs-6").addClass('col-xs-12');
                    $('.list-aap-container').removeClass('container-fluid').addClass('container');*/
                    //}, 500);
                } else {
                    //setTimeout(() => {
                    $('.map-aap-container,.list-aap-map').remove();
                    $('.bodySearchContainer').removeClass("col-xs-6").addClass('col-xs-12');
                    $('.list-aap-container').removeClass('container-fluid').addClass('container');
                    //}, 500);
                }
                // $(".list-group-item").click(function(e){
                //     e.stopPropagation();
                //     var pos = $(this).data('pos');
                //     alert(pos);
                //     mapAnsw.getOptions().markerList[pos].openPopup()
                // })
                $('.config-participatory-budget').off().on('click', function () {
                    var updateConfigParticipatoryBudget = (notNull(aapObject.formParent.params) && notNull(aapObject.formParent.params.voteConfig)) ? aapObject.formParent.params.voteConfig : {};
                    $.each(["startDate", "endDate"], function (k, v) {
                        if (notNull(updateConfigParticipatoryBudget[v]) && notNull(updateConfigParticipatoryBudget[v]["sec"])) {
                            var d = new Date(parseInt(updateConfigParticipatoryBudget[v]["sec"] * 1000));
                            mylog.log(d, "datiko")
                            updateConfigParticipatoryBudget[v] = d.getDate() + '/' + (d.getUTCMonth() + 1) + '/' + d.getUTCFullYear()
                        }
                    })
                    dyFObj.openForm(
                        {
                            jsonSchema: {
                                title: trad.participativefinancing,
                                icon: "fa-key",
                                type: "object",
                                onLoads: {
                                    initUpdateInfo: function () {

                                    },
                                    onload: function (data) {
                                        var fparticipatif = function () {
                                            if ($('#activeFor').val() == "date")
                                                $('.startDatedate,.endDatedate').fadeIn();
                                            else
                                                $('.startDatedate,.endDatedate').fadeOut();
                                            if ($('#restricted').val() == "roles")
                                                $('.rolestags').fadeIn();
                                            else
                                                $('.rolestags').fadeOut();
                                        }
                                        fparticipatif();
                                        $('#activeFor,#restricted').on('change', function () {
                                            fparticipatif();
                                        })

                                    }
                                },
                                beforeSave: function () {
                                },
                                save: function (data) {
                                    delete data.collection; delete data.scope;
                                    var tplCtx = {
                                        id: aapObject.formParent._id.$id,
                                        path: "params.voteConfig",
                                        collection: "forms",
                                        value: {},
                                        setType: [
                                            {
                                                "path": "startDate",
                                                "type": "isoDate"
                                            },
                                            {
                                                "path": "endDate",
                                                "type": "isoDate"
                                            }
                                        ]
                                    }
                                    $.each(data, function (k, v) {
                                        if (exists(data[k])) {
                                            tplCtx.value[k] = $("#" + k).val();
                                        }
                                    });
                                    if (typeof tplCtx.value == "undefined")
                                        toastr.error('value cannot be empty!');
                                    else {
                                        dataHelper.path2Value(tplCtx, function (params) {
                                            if (params.result)
                                                toastr.success(trad.saved)
                                            $('.modal').hide();
                                            urlCtrl.loadByHash(location.hash);
                                        });
                                    }
                                },
                                properties: {
                                    "activeFor": {
                                        inputType: "select",
                                        label: "Activer pour status à voter",
                                        options: {
                                            "status": "Status à voter",
                                            "date": "Entre deux dates",
                                        }
                                    },
                                    "restricted": {
                                        inputType: "select",
                                        label: trad.restrictaccess,
                                        options: {
                                            "all": trad.all,
                                            "members": trad.members,
                                            "roles": trad.roleroles,
                                        }
                                    },
                                    "roles": {
                                        inputType: "tags",
                                        label: "Ajouter des roles",
                                    },
                                    "startDate": {
                                        inputType: "date",
                                        label: tradDynForm.startDate,
                                    },
                                    "endDate": {
                                        inputType: "date",
                                        label: tradDynForm.endDate,
                                    }
                                }
                            }
                        }, null, updateConfigParticipatoryBudget
                    )
                })
                $('.config-budget').off().on('click', function () {
                    var configBudget = jsonHelper.notNull("aapObject.formParent.params.globalBudget") ? aapObject.formParent.params.globalBudget : {}
                    dyFObj.openForm(
                        {
                            jsonSchema: {
                                title: "Config "+trad.globalBudget,
                                icon: "fa-key",
                                type: "object",
                                onLoads: {
                                    initUpdateInfo: function () {

                                    },
                                    onload: function (data) {

                                    }
                                },
                                beforeSave: function () {
                                },
                                save: function (data) {
                                    delete data.collection; delete data.scope;
                                    var tplCtx = {
                                        id: aapObject.formParent._id.$id,
                                        path: "params.globalBudget",
                                        collection: "forms",
                                        value: {},
                                        setType: [
                                            {
                                                "path": "max",
                                                "type": "int"
                                            }
                                        ]
                                    }
                                    $.each(data, function (k, v) {
                                        if (exists(data[k])) {
                                            tplCtx.value[k] = $("#" + k).val();
                                        }
                                    });
                                    if (typeof tplCtx.value == "undefined")
                                        toastr.error('value cannot be empty!');
                                    else {
                                        dataHelper.path2Value(tplCtx, function (params) {
                                            if (params.result)
                                                toastr.success(trad.saved)
                                            $('.modal').hide();
                                            urlCtrl.loadByHash(location.hash);
                                        });
                                    }
                                },
                                properties: {
                                    "max": {
                                        inputType: "text",
                                        label: "Budget global",
                                        rules:{
                                            number :true,
                                            min: 0
                                        }
                                    }
                                }
                            }
                        }, null, configBudget
                    )
                })

                setTimeout(() => {
                    if (location.hash.indexOf(".page.agenda") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '#eee');
                    } else if (location.hash.indexOf(".page.details") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url*=".page.details"]').css('background', '#eee')
                    } else if (location.hash.indexOf("inproject=false") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url*="inproject=false"]').css('background', '#eee')
                    } else if (location.hash.indexOf("inproject=true") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url*="inproject=true"]').css('background', '#eee')
                    } else if (location.hash.indexOf("view=project_kanban&user=") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url*="view=project_kanban&user="]').css('background', '#eee')
                    } else if (location.hash.indexOf(".page.multidashboard") != -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url*=".page.multidashboard"]').css('background', '#eee')
                    } else if (location.hash.indexOf(".page.form") != -1 && location.hash.indexOf(".page.form.answer.") == -1) {
                        $('.to-menu-top-center .btnaapp_link.lbh-default,#btn-new-proposition').css('background', '');
                        $('[href*=".page.agenda"]').css('background', '');
                        $('[data-url$=".page.form"]').css('background', '#eee')
                    }
                }, 400);

                $(".openFilterParams").off().on("click", function () {
                    var inputsText = {};
                    var inputsSelect = {};
                    let dataInputs = {};
                    let dataParamsInputs = {};
                    const inputSelectType = ["tpls.forms.cplx.radioNew", "tpls.forms.cplx.checkboxNew", "tpls.forms.select"];
                    const paramsKeys = aapObject.formParent.params ? Object.keys(aapObject.formParent.params) : [];
                    if (aapObject.formInputs) {
                        for (const [index, elem] of Object.entries(aapObject.formInputs)) {
                            const key = Object.keys(elem)[0];
                            if (elem[key].inputs) {
                                for (const [k, v] of Object.entries(elem[key].inputs)) {
                                    dataInputs[k] = index;
                                    if (v.type) {
                                        if (v.type == "text") {
                                            inputsText[k] = v.label;
                                        }
                                        if (inputSelectType.indexOf(v.type) > -1) {
                                            inputsSelect[k] = v.label;
                                            const indexFinded = paramsKeys.findIndex(e => e.includes(k));
                                            const inputParamsKey = indexFinded > -1 ? paramsKeys[indexFinded] : "";
                                            inputParamsKey != "" ? dataParamsInputs[k] = inputParamsKey : "";
                                        }
                                    } else {
                                        inputsText[k] = v.label;
                                    }
                                }
                            }
                        }
                    }
                    sectionDyfFilter = {
                        "jsonSchema": {
                            "title": trad.configureFilter,
                            "icon": "cog",
                            "text": "Sélectionnez la réponse d'un champ text pour filtrer par titre",
                            "properties": {
                                labelForText: {
                                    inputType: "text",
                                    label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
                                    placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
                                    rules: {
                                        required: true
                                    },
                                    value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
                                        aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.labelForText : null
                                    ) : null) : null,
                                },
                                inputForText: {
                                    inputType: "select",
                                    label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
                                    placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
                                    noOrder: true,
                                    options: inputsText,
                                    rules: {
                                        required: true
                                    },
                                    value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
                                        aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.inputForText.split('.')[2] : null
                                    ) : null) : null
                                }
                            },
                            onLoads: {},
                            save: function () {
                                let dataToSend = {
                                    id: aapObject.formParent["_id"]["$id"],
                                    path: "params.filterParams",
                                    collection: 'forms',
                                    value: {
                                    }
                                }
                                $.each(sectionDyfFilter.jsonSchema.properties, function (k, val) {
                                    if ($("#" + k).val()) {
                                        const keyForGroup = k.substring(k.indexOf('For') + 3).toLocaleLowerCase();
                                        if (!dataToSend.value[keyForGroup]) dataToSend.value[keyForGroup] = {};
                                        switch (k) {
                                            case "inputFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1): {
                                                if (dataInputs[$("#" + k).val()])
                                                    dataToSend.value[keyForGroup][k] = 'answers.' + dataInputs[$("#" + k).val()] + '.' + $("#" + k).val();
                                                if (dataParamsInputs[$("#" + k).val()])
                                                    dataToSend.value[keyForGroup]["paramsKeyFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1)] = dataParamsInputs[$("#" + k).val()]
                                            } break;

                                            default: dataToSend.value[keyForGroup][k] = $("#" + k).val();
                                                break;
                                        }
                                    }
                                });

                                mylog.log("save tplCtx", dataToSend);
                                if (typeof dataToSend.value == "undefined") {
                                    toastr.error('value cannot be empty!');
                                } else {
                                    dataHelper.path2Value(dataToSend, function (params) {
                                        urlCtrl.loadByHash(location.hash)
                                    });
                                }

                            }
                        }
                    };
                    sectionDyfFilter.jsonSchema.properties = aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (Object.keys(aapObject.formParent.params.filterParams).length > 0 ? {} : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties;
                    if (aapObject.formParent.params) {
                        if (aapObject.formParent.params.filterParams) {
                            for (const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
                                for (const [k, v] of Object.entries(value)) {
                                    let label = trad.textFilter;
                                    !key.includes("text") ? label = trad.selectFilter : "";
                                    if (k.includes("label")) {
                                        sectionDyfFilter.jsonSchema.properties[k] = {
                                            inputType: "text",
                                            label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + label,
                                            placeholder: trad.EnterText + " " + trad.for + " " + label,
                                            value: v
                                        }
                                    } else if (k.includes("input")) {
                                        sectionDyfFilter.jsonSchema.properties[k] = {
                                            inputType: "select",
                                            label: trad.SelectField + " " + trad.for + " " + " " + label,
                                            placeholder: trad.SelectField + " " + trad.for + " " + label,
                                            options: key.includes("text") ? inputsText : inputsSelect,
                                            value: v.split(".")[2]
                                        }
                                    }
                                }
                            }
                        }
                    }
                    sectionDyfFilter.jsonSchema.onLoads.onload = function () {
                        let objectForArrayGroup = {};
                        for (const [key, value] of Object.entries(sectionDyfFilter.jsonSchema.properties)) {
                            if (key.includes('label') || key.includes('input')) {
                                const keyForGroup = key.substring(key.indexOf('For') + 3);
                                if (!objectForArrayGroup[keyForGroup]) {
                                    objectForArrayGroup[keyForGroup] = {
                                        groupType: keyForGroup.includes("ext") ? trad.text.charAt(0).toUpperCase() + trad.text.slice(1) : trad.selection.charAt(0).toUpperCase() + trad.selection.slice(1),
                                        inputToGroup: []
                                    };
                                }
                                objectForArrayGroup[keyForGroup].inputToGroup.push(key + value.inputType);
                            }
                        }
                        for (const [key, value] of Object.entries(objectForArrayGroup)) {
                            alignInput2(value.inputToGroup, key, 6, 12, null, null, value.groupType, "#3f4e58", "");
                            $(".fieldset" + key).append(`<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-propertiesToRemove="labelFor${key},inputFor${key}" data-inputSelector=".fieldset${key}"><i class="fa fa-times"></i></button>`);
                        }

                        $(`
                            <div class="form-group mb-0 d-flex justify-content-center flex-xs-column filter-actions-group">
                                <button type="button" class="btn btn-xs btn-primary addTextFilter">${trad.Add + " " + trad.textFilter}</button>
                                <button type="button" class="btn btn-xs btn-info addSelectFilter">${trad.Add + " " + trad.selectFilter}</button>
                            </div>
                        `).insertBefore(".form-actions");
                        let inputsTextOptions = "";
                        for (const [key, value] of Object.entries(inputsText)) {
                            inputsTextOptions += `<option value="${key}" data-value="">${value}</option>`;
                        }
                        let inputsSelectOptions = "";
                        for (const [key, value] of Object.entries(inputsSelect)) {
                            inputsSelectOptions += `<option value="${key}" data-value="">${value}</option>`;
                        }
                        $(".addTextFilter").off().on("click", function () {
                            const fieldSet = "Text" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
                            let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
                            let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
                            $(`
                                <div class="col-xs-12 fieldset fieldset${fieldSet}">
                                    <div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
                                        <label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
                                            <i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter}
                                        </label>
                                        <input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.textFilter}">
                                    </div>
                                    
                                    <div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
                                        <label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
                                            <i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.textFilter}
                                        </label>
                                        <select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}">
                                            <option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.textFilter}</option>
                                            ${inputsTextOptions}
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            `).insertAfter(lastField).hide().fadeIn(300);
                            styleWrapped = `
                                .modal-content .fieldset${fieldSet}:before {
                                    content: "${trad.text.charAt(0).toUpperCase() + trad.text.slice(1)}";
                                    color: #3f4e58;
                                    position: absolute;
                                    top: -15px;left: 14px;
                                    background: white;
                                    font-size: medium;
                                    font-weight: bold;
                                    padding: 0 15px;
                                }
                            `;
                            $("head style").last().append(styleWrapped);
                            sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
                                inputType: "text",
                                label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
                                placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
                                value: ""
                            };
                            sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
                                inputType: "select",
                                label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
                                placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
                                options: inputsText,
                                value: ""
                            }
                        });
                        $(".addSelectFilter").off().on("click", function () {
                            const fieldSet = "Select" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
                            let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
                            let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
                            $(`
                                <div class="col-xs-12 fieldset fieldset${fieldSet}">
                                    <div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
                                        <label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
                                            <i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter}
                                        </label>
                                        <input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.selectFilter}">
                                    </div>
                                    
                                    <div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
                                        <label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
                                            <i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.selectFilter}
                                        </label>
                                        <select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}">
                                            <option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.selectFilter}</option>
                                            ${inputsSelectOptions}
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            `).insertAfter(lastField).hide().fadeIn(300);
                            styleWrapped = `
                                .modal-content .fieldset${fieldSet}:before {
                                    content: "${trad.selection}";
                                    color: #3f4e58;
                                    position: absolute;
                                    top: -15px;left: 14px;
                                    background: white;
                                    font-size: medium;
                                    font-weight: bold;
                                    padding: 0 15px;
                                }
                            `;
                            $("head style").last().append(styleWrapped);
                            sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
                                inputType: "text",
                                label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter,
                                placeholder: trad.EnterText + " " + trad.for + " " + trad.selectFilter,
                                value: ""
                            };
                            sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
                                inputType: "select",
                                label: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
                                placeholder: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
                                options: inputsSelect,
                                value: ""
                            }
                        });
                        $(document).on("click", ".removeFilterOption", function (e) {
                            e.stopImmediatePropagation();
                            const propertiesToRemove = $(this).attr("data-propertiesToRemove") ? $(this).attr("data-propertiesToRemove").split(",") : [];
                            propertiesToRemove.forEach(e => {
                                if (sectionDyfFilter.jsonSchema.properties[e]) delete sectionDyfFilter.jsonSchema.properties[e]
                            })
                            $($(this).attr("data-inputSelector")).fadeOut(300, function () {
                                $(this).remove()
                            });
                        });
                    }
                    dyFObj.openForm(sectionDyfFilter);
                });

                $("#filterContainerL").on('shown.bs.collapse', '.collapse', function () {
                    $("#filterContainerInside").find(".collapse.in").each(function () {
                        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                        $(this).removeClass("in");
                    });
                    $(this).addClass("in");
                    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                }).on('hidden.bs.collapse', function () {
                    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                });

                $("#mainNav").on('shown.bs.collapse', '.collapse', function () {
                    let self = this;
                    $(this).find(".btnaapp_link").each(function () {
                        $(this).click(function () {
                            let burgerBtnIcon = $("#mainNav").find(".open-xs-menu-collapse").children("i");
                            burgerBtnIcon.removeClass("fa-times");
                            burgerBtnIcon.addClass("fa-bars");
                            $(self).removeClass('in')
                        });
                    });
                });
                $(".aap-lists-btn-2").on("click",function(){
                    if(location.hash.indexOf("#oceco")!=-1){
                        var aapDialog = bootbox.dialog({
                            title: '',
                            message: '<div id="exists-aap-lists"></div>',
                            size: 'large',
                            onEscape: true,
                            backdrop: true,
                        })
                        aapDialog.init(function(){
                            aapDialog.attr("id","exists-aap-lists-dialog");
                            $("#exists-aap-lists-dialog").css("background-color","rgb(0 0 0 / 66%)");
                            if(costum != null){
                                ajaxPost('#exists-aap-lists', baseUrl + "/co2/aap/listaap/slug/"+costum.slug,
                                    null,
                                    function (data) {

                                    }, null
                                );
                            }
                        });
                    }
                })

                $("#centerFilters-active").mouseenter(function() {
                    $("#centerBtnSaveFilter").addClass('canactive')
                }).mouseleave(function() {
                    // $("#centerBtnSaveFilter").removeClass('canactive')
                });
            }
        }
    },
    aap: {

    },
    ocecoform: {

    },
    readMoreLess: function (selector, showChar = 400, moretext = "Lire la suite", lesstext = "Lire moins") {
        var ellipsestext = "...";
        $(selector).each(function () {
            var content = $(this).html();
            var textcontent = $(this).text();

            if (textcontent.length > showChar) {
                var c = textcontent.substr(0, showChar);
                //var h = content.substr(showChar-1, content.length - showChar);

                var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

                $(this).html(html);
                $(this).after('<a href="" class="moreLinkaap btn-xs btn btn-default">' + moretext + '</a>');
            }
        });

        $(".moreLinkaap").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
                $(this).prev().children('.morecontent').fadeToggle(500, function () {
                    $(this).prev().fadeToggle(500);
                });

            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
                $(this).prev().children('.container').fadeToggle(500, function () {
                    $(this).next().fadeToggle(500);
                });
            }

            return false;
        });
    }

}

$(document).ready(function () {
    $(document).on("click", ".aapgoto", function (event) {
        event.stopImmediatePropagation();
        $(this).parent().parent().parent().removeClass("in");
        let burgerBtnIcon = $(this).parent().parent().parent().parent().children("#menuTopLeft").find(".open-xs-menu-collapse").children("i")
        burgerBtnIcon.removeClass("fa-times");
        burgerBtnIcon.addClass("fa-bars");
    })
    $(document).on("click", ".open-xs-menu-collapse", function (event) {
        event.stopImmediatePropagation();
        $(this).children('i').toggleClass("fa-bars"); $(this).children('i').toggleClass("fa-times");
        if ($("#show-filters-xs").children("i").hasClass("fa-times")) {
            $("#show-filters-xs").children("i").toggleClass("fa-times");
            $("#show-filters-xs").children("i").toggleClass("fa-filter");
            $("#filterContainerInside").hide()
        }
    });
    $(document).on("click", ".showHide-filters-xs", function (event) {
        event.stopImmediatePropagation();
        if ($(".open-xs-menu-collapse").children('i').hasClass("fa-times")) {
            $(".open-xs-menu-collapse").children('i').toggleClass("fa-times");
            $(".open-xs-menu-collapse").children('i').toggleClass("fa-bars");
            $("#menuTopCenter").toggleClass("in")
        }
        if ($(window).width() > 768) {
            if ($(".menu-filters-lg").is(":visible")) {
                $(this).find("i").removeClass("fa-times").addClass("fa-filter");
                $(".menu-filters-lg").hide('slide', {
                    direction: 'left'
                }, 600);
                $(".openFilterParams").hide('fade', {
                }, 600)
            } else {
                $(this).find("i").removeClass("fa-filter").addClass("fa-times");
                $(".menu-filters-lg").show('slide', {
                    direction: 'right'
                }, 600);
                $(".openFilterParams").show('fade', {
                }, 600);
            }
        }
    })
    if ($(window).width() < 769) {
        // $(".menu-verticale.searchObjCSS").detach().appendTo("#menuTopLeft");
    };
    $('#show-filters-lg').off().on('click', function () {
        /* if($(".menu-filters-lg").is(":visible")) {
            $(this).find("i").removeClass("fa-times").addClass("fa-filter");
            $(".menu-filters-lg").hide('slide', {
                direction: 'left'
            }, 600)
        } else {
            $(this).find("i").removeClass("fa-filter").addClass("fa-times");
            $(".menu-filters-lg").show('slide', {
                direction: 'right'
            }, 600);

        } */
    });
})


// venant du fichier agenda.php
function invert_color(hex) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    // invert color components
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    // pad each with zeros and return
    return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#ffffff';
}

function get_form_id() {
    var hash = document.location.hash.split('.');
    return hash[hash.indexOf('formid') + 1];
}

function get_actions_from($projects = [], $is_array = true) {
    var url = `${baseUrl}/co2/aap/agenda/request/get_actions`;
    var data = {
        form: get_form_id(),
        project: $projects,
        is_array: $is_array
    };
    return new Promise(function (resolve) {
        ajaxPost(null, url, data, function (results) {
            resolve(results)
        }, null, 'json');
    });
}

function get_action($id) {
    var url = `${baseUrl}/co2/aap/agenda/request/get_action`;
    var data = {
        action: $id,
        form: get_form_id()
    };
    return new Promise(function (resolve) {
        ajaxPost(null, url, data, function (result) {
            resolve(result);
        }, null, 'json');
    });
}

function get_project($id) {
    var url = `${baseUrl}/co2/aap/agenda/request/get_project`;
    var data = {
        project: $id
    };
    return new Promise(function (resolve) {
        ajaxPost(null, url, data, function (result) {
            resolve(result);
        }, null, 'json');
    });
}

function get_projects($ids = []) {
    var url = `${baseUrl}/co2/aap/agenda/request/get_projects`;
    var data = {
        form: get_form_id(),
        ids: $ids
    };
    return new Promise(function (resolve) {
        ajaxPost(null, url, data, function (result) {
            resolve(result)
        }, function(arg0, arg1, arg2) {
            mylog.log('aap error', arg0, arg1, arg2);
        }, 'json');
    });
}

function get_project_names() {
    return new Promise(resolve => {
        ajaxPost(null, `${baseUrl}/co2/aap/agenda/request/get_project_names`, {
            form: get_form_id()
        }, function (result) {
            resolve(result);
        }, null, 'json');
    });
}

function send_task_added($task) {
    var url = `${baseUrl}/co2/aap/agenda/request/task_added`;
    var data = {
        task: $task,
        action: selected_action
    };
    return new Promise(function (resolve) {
        ajaxPost(null, url, data, function(response) {
        }, null, 'json');
    })
}

function send_task_checked($task,$checked) {
    var url = `${baseUrl}/co2/aap/agenda/request/task_status`;
    var data = {
        task: $task,
        checked: $checked,
        action: selected_action
    };
    return new Promise(function (resolve, reject) {
        ajaxPost(null, url, data, resolve, reject, 'json');
    })
}

async function open_action_detail($action) {
    var waiters = $('body, .fc-widget-content, .fc-content');

    if(waiters.css('cursor') === 'wait')
        return false;
    waiters.css('cursor', 'wait');

    var modal = document.getElementById('action_detail');

    // créer le  modal s'il n'existe pas encore
    if(!modal) {
        modal = document.createElement('div');
        modal.classList.add('modal', 'fade');
        modal.setAttribute('id', 'action_detail');
        var modal_html_content = `
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" style="text-transform: unset;"><span class="project_name"></span><span class="action_name"></span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="item-avatar">
                            <img src="" data-src="" class="visible">
                            <p style="margin: 0;">
                                Auteur : <span class="creator"></span>
                            </p>
                        </div>
                        <br>
                        <span>Action à réaliser commençant le <span id="begining_date"></span>
                            <!-- il y a <span id="month_begining_computed"></span> -->
                        </span>
                        <table>
                            <tbody>
                                <tr>
                                    <td><i class="fa fa-credit-card" aria-hidden="true"></i></td>
                                    <td>Vous gagnerez <strong id="detail_credit"></strong> crédit</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-calendar" aria-hidden="true"></i></td>
                                    <td>Début le <span id="detail_full_begining"></span> </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-clock-o" aria-hidden="true"></i></td>
                                    <td>Durée <span id="detail_duration"></span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-male" aria-hidden="true"></i></td>
                                    <td>Il y a <span id="detail_contributor"></span> participant<span id="detail_contributor_multiple">(s)</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></td>
                                    <td>Pour un besoin minimum de <span id="detail_contributor_min"></span> participant<span id="detail_contributor_min_multiple">(s)</span></td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></td>
                                    <td>Et un maximum de <span id="detail_contributor_max"></span> participant<span id="detail_contributor_max_multiple">(s)</span></td>
                                </tr>
                            </tbody>
                        </table>
                        <snap id="detail_contribution_indicator"></snap>
                        <br />
                        <ul class="list-group task_list">
                            <li class="list-group-item active">Liste de tâches</li>
                            <li class="list-group-item new_task_form_container">
                                <span class="fa fa-tasks"></span>
                                <input type="text" placeholder="Tâche...">
                                <button type="button">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </li>
                            <li class="list-group-item">
                                <span class="task_number_recap"></span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <br>
                        <ul class="list-group contributor_list">
                            <li class="list-group-item active">Ils participent à cette action</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        `;
        modal.innerHTML = modal_html_content;
        document.body.appendChild(modal);
    }
    var action = await get_action($action);
    var project = [];
    if(typeof action['parent'] != "undefined")
        project = await get_project(action['parent']);
    tasks = action['tasks'];
    selected_action = action['id'];
    var start_date = new Date(action['start_date']);
    var end_date = new Date(action['end_date']);
    var moment_start = moment(start_date);
    moment_start.locale('fr');
    var moment_end = moment(end_date);
    moment_end.locale('fr');

    $.extend(modal.style, {
        fontFamily: '"Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif',
        fontWeight: 'normal',
        userSelect: 'none',
        MozUserSelect: 'none',
        webkitUserSelect: 'none'
    });

    [...modal.querySelectorAll('table>tbody>tr>td:nth-child(2)')].forEach(function (td) {
        $.extend(td['style'], {
            padding: '5px 10px'
        })
    });
    if(typeof project['name'] != "undefined")
        modal.querySelector('.project_name').textContent = project['name'];
    modal.querySelector('.action_name').textContent = `: ${action['name']}`;

    modal.querySelector('.creator').textContent = action['creator']['name'];
    modal.querySelector('img').src = action['creator']['image'];
    if(typeof project['creator'] != "undefined" && typeof project['creator']['image'] != "undefined")
        modal.querySelector('img').dataset.src = project['creator']['image'];
    document.getElementById('begining_date').textContent = moment_start.format('DD MMMM YYYY HH:mm');
    document.getElementById('detail_credit').textContent = action['credit'];
    document.getElementById('detail_full_begining').textContent = moment_start.format('ddd DD MMMM YYYY');
    document.getElementById('detail_duration').textContent = moment.duration(moment_end.diff(moment_start)).humanize();
    document.getElementById('detail_contributor').textContent = action['contributors'].length;
    document.getElementById('detail_contributor_min').textContent = action['min'];
    document.getElementById('detail_contributor_max').textContent = action['max'];
    document.getElementById('detail_contributor_min_multiple').style.display = action['min'] !== 1 ? 'inline' : 'none';
    document.getElementById('detail_contributor_max_multiple').style.display = action['max'] !== 1 ? 'inline' : 'none';
    document.getElementById('detail_contributor_multiple').style.display = action['contributors'].length !== 1 ? 'inline' : 'none';

    if (userConnected && Array.isArray(action['contributors']) && action['contributors'].some(some => some['id'] === userConnected['_id']['$id']))
        document.getElementById('detail_contribution_indicator').textContent = 'Vous participez à cette action';
    else document.getElementById('detail_contribution_indicator').textContent = '';

    var contributor_list = modal.querySelector('.contributor_list');
    var contributor_items = [...modal.querySelectorAll('.contributor_list .list-group-item')];
    var contributor_line_to_spare = 1;
    while (contributor_items.length > contributor_line_to_spare) contributor_items.pop().remove();

    create_task_list_detail();

    var contributors = action['contributors'];
    if (contributors.length > 0) {
        for (var contributor of contributors) {
            var tag_contributor_item = document.createElement('li');
            tag_contributor_item.classList.add('list-group-item');
            tag_contributor_item.innerHTML = `
            <div class="item-avatar">
                <img src="${contributor['image']}" class="visible" style="width: 50px; height: 50px;">
                <p style="margin: 0;text-overflow: ellipsis; overflow: hidden;">
                    <span class="creator">${contributor['name']}</span>
                </p>
            </div>
            `;
            contributor_list.appendChild(tag_contributor_item);
        }
    } else {
        var tag_contributor_item = document.createElement('li');
        tag_contributor_item.classList.add('list-group-item');
        tag_contributor_item.textContent = 'Aucun participant associé';
        contributor_list.appendChild(tag_contributor_item);
    }
    contributor_list.lastChild.style.cssText = 'border-bottom: 1px solid #ddd !important';
    var add_task_input = $(modal).find('.new_task_form_container>input');
    add_task_input.off('keypress').on('keypress', event => {
        if (event['originalEvent']['code'] === 'Enter' && add_task_input.val()) insert_update_task().then(() => create_task_list_detail());
    });
    $(modal).find('.new_task_form_container>button').off('click').on('click', () => {
        if (add_task_input.val()) insert_update_task().then(() => create_task_list_detail());
    });
    add_task_input.val('');
    $(modal).find('.new_task_form_container>button').innerHTML = '<i class="fa fa-plus"></i>';
    selected_task = null;
    $(modal).modal('show');
    waiters.css('cursor', '');
}

function insert_update_task() {
    // var action_last_index = js_tasks.map(map => map['action']).lastIndexOf(selected_action);
    var start_date = new Date();
    var start_moment = moment(start_date);
    var input = document.getElementById('action_detail').querySelector('.new_task_form_container>input');
    var button = document.getElementById('action_detail').querySelector('.new_task_form_container>button');

    var task_id = selected_task ? selected_task['element']['taskId'] : start_moment.format('X');
    var params = {
        id: selected_action,
        collection: `actions`,
        value: {
            taskId: task_id,
            task: input.value,
            userId: userConnected['_id']['$id']
        },
        setType: [{
            path: 'createdAt',
            type: 'isoDate'
        }, {
            path: 'startDate',
            type: 'isoDate'
        },
        {
            path: 'checked',
            type: 'boolean'
        }
        ]
    }

    if (selected_task) {
        params['path'] = `tasks.${selected_task['index']}`;
        params['value']['checked'] = selected_task['element']['checked'];
        params['value']['createdAt'] = selected_task['element']['createdAt'];
        params['value']['checkedAt'] = selected_task['element']['checkedAt'];
        params['value']['startDate'] = selected_task['element']['startDate'];
        if (selected_task['element']['checked']) {
            params['value']['endDate'] = moment().format();
            params['setType'].push({
                path: 'endDate',
                type: 'isoDate'
            }, {
                path: 'checkedAt',
                type: 'isoDate'
            },);
        }
    } else {
        params['path'] = 'tasks';
        params['arrayForm'] = true;
        params['edit'] = false;
        params['format'] = true;
        params['value']['checked'] = false;
        params['value']['createdAt'] = start_moment.format();
        params['value']['startDate'] = start_moment.format();
    }

    return new Promise(resolve => {
        dataHelper.path2Value(params, () => {
            send_task_added(input.value);
            input.value = '';
            button.innerHTML = '<i class="fa fa-plus"></i>'
            toastr.success('La modification est enregistrée');
            selected_task = null;
            resolve();
        });
    });
}

function create_task_list_detail() {
    var modal = document.getElementById('action_detail');
    var progress_bar = modal.querySelector('.progress-bar');
    var task_recap = modal.querySelector('.task_number_recap');
    var tag_task_list = modal.querySelector('.task_list');
    var task_items = [...modal.querySelectorAll('.task_list .list-group-item')];
    var add_task_input = modal.querySelector('.new_task_form_container>input');
    var add_task_button = modal.querySelector('.new_task_form_container>button');
    var task_line_to_spare = 3;
    while (task_items.length > task_line_to_spare) task_items.pop().remove();

    var tag_task_item = document.createElement('li');
    tag_task_item.classList.add('list-group-item');
    tag_task_item.textContent = 'Aucune tâche associée';
    tag_task_list.appendChild(tag_task_item);
    task_items.push(tag_task_item);

    progress_bar.style.width = `0%`;
    progress_bar.textContent = ``;
    task_recap.textContent = '';
    ajaxPost(null, `${baseUrl}/co2/aap/agenda/request/get_tasks`, {
        action: selected_action
    }, tasks => {
        if (tasks.length > 0) {
            while (task_items.length > task_line_to_spare) task_items.pop().remove();
            for (var i = 0; i < tasks.length; i++) tag_task_list.appendChild(task_item_template(i, tasks[i]));

            var number_of_done_task = tasks.filter(filter => filter['checked']).length;
            var percentage = parseInt(number_of_done_task * 100 / tasks.length);
            progress_bar.style.width = `${percentage}%`;
            progress_bar.textContent = `${percentage}%`;
            task_recap.textContent = `${number_of_done_task}/${tasks.length}`;
        }
        tag_task_list.lastChild.style.cssText = 'border-bottom: 1px solid #ddd !important';
    }, null, 'json');
}

function set_task_status() {
    var typedefs = [{
        path: 'startDate',
        type: 'isoDate'
    },
    {
        path: 'createdAt',
        type: 'isoDate'
    },
    {
        path: 'checked',
        type: 'boolean'
    }
    ];
    if (selected_task['element']['checked']) {
        selected_task['element']['userId'] = userConnected['_id']['$id'];
        selected_task['element']['checkedAt'] = moment().format();
        selected_task['element']['endDate'] = moment().format();
        selected_task['element']['checkedUserId'] = userConnected['_id']['$id'];
        typedefs.push({
            path: 'endDate',
            type: 'isoDate'
        }, {
            path: 'checkedAt',
            type: 'isoDate'
        });
    } else['checkedUserId', 'endDate', 'checkedAt'].forEach(field => delete selected_task['element'][field]);

    return new Promise(resolve => {
        dataHelper.path2Value({
            id: selected_action,
            collection: `actions`,
            path: `tasks.${selected_task['index']}`,
            value: selected_task['element'],
            setType: typedefs
        }, () => {
            toastr.success('Mise à jour enregistrée');
            send_task_checked(selected_task['element']['task'], selected_task['element']['checked']);
            selected_task = null;
            resolve();
        });
    });
}

function task_item_template(index, task) {
    var tag_task_item = document.createElement('li');
    var tag_task_name = document.createElement('label');
    var tag_check_task_container = document.createElement('div');
    var tag_check_task_input = document.createElement('input');
    var tag_check_task_checkmark = document.createElement('label');
    var tag_task_item_container = document.createElement('div');
    var tag_task_edit = document.createElement('button');
    var tag_task_delete = document.createElement('button');

    tag_task_edit.innerHTML = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
    tag_task_delete.innerHTML = '<i class="fa fa-trash" aria-hidden="true"></i>';

    tag_task_edit.classList.add('btn');
    tag_task_edit.classList.add('btn-default');
    tag_task_edit.classList.add('btn-sm');
    $.extend(tag_task_edit.style, {
        marginRight: '5px'
    });

    tag_task_delete.classList.add('btn');
    tag_task_delete.classList.add('btn-default');
    tag_task_delete.classList.add('btn-sm');
    $.extend(tag_check_task_container.style, {
        position: 'relative',
        marginRight: '10px'
    });
    tag_check_task_checkmark.classList.add('checkmark');
    tag_check_task_checkmark.classList.add('default');
    tag_check_task_input.classList.add('cpl-check');
    tag_check_task_input.setAttribute('type', 'checkbox');
    tag_check_task_input.setAttribute('id', `detail_${task['taskId']}`);
    tag_check_task_input.checked = task['checked'];
    tag_task_item.classList.add('list-group-item');
    $.extend(tag_task_item_container.style, {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    });
    tag_check_task_checkmark.setAttribute('for', `detail_${task['taskId']}`);
    tag_task_name.setAttribute('for', `detail_${task['taskId']}`);
    $.extend(tag_task_name.style, {
        userSelect: 'none',
        MozUserSelect: 'none',
        webkitUserSelect: 'none',
    });
    $.extend(tag_task_name.style, {
        flex: '1'
    });

    if (task['checked']) tag_task_name.classList.add('done');

    tag_check_task_input.addEventListener('change', event => {
        selected_task = {
            element: task,
            index
        };
        selected_task['element']['checked'] = event.target.checked;
        set_task_status().then(() => create_task_list_detail());
    });
    tag_task_edit.addEventListener('click', event => {
        selected_task = {
            element: task,
            index
        };
        if (!task['checked']) delete selected_task['element']['endDate'];
        document.querySelector('.new_task_form_container>input').value = task['task'];
        document.querySelector('.new_task_form_container>button').innerHTML = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
    });
    tag_task_delete.addEventListener('click', event => {
        $.confirm({
            icon: 'fa fa-tasks',
            theme: 'material',
            closeIcon: true,
            animation: 'rotateY',
            type: 'red',
            title: `Supprimer une tâche`,
            content: `Êtes-vous sûr de supprimer la tâche <strong>"${task['task']}"</strong>?`,
            animationBounce: 1.5,
            animationSpeed: 300,
            buttons: {
                confirm: {
                    text: 'Supprimer',
                    btnClass: 'btn-danger',
                    keys: ['enter'],
                    action: () => {
                        dataHelper.path2Value({
                            id: selected_action,
                            collection: `actions`,
                            path: `tasks.${index}`,
                            pull: 'tasks',
                            value: null
                        }, () => create_task_list_detail());
                    }
                }
            }
        });
    });

    tag_task_name.textContent = task['task'];

    tag_check_task_container.appendChild(tag_check_task_input);
    tag_check_task_container.appendChild(tag_check_task_checkmark);

    tag_task_item_container.appendChild(tag_check_task_container);
    tag_task_item_container.appendChild(tag_task_name);
    tag_task_item_container.appendChild(tag_task_edit);
    tag_task_item_container.appendChild(tag_task_delete);

    tag_task_item.appendChild(tag_task_item_container);
    return tag_task_item;
}
