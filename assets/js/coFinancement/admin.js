adminPanel.views.collectifLocaux = function(){
  var data={
    title : "Porteurs de communs",
    types : ["organizations"],
    table : {
      name: {
        name : "Nom",
        preview : true
      },
      description : {
        name : "Description"
      },
      scope:{
        name : "Ville"
      }
    },
      actions:{
         validateSource : true,
         updateCollectif : true,
         delete : true,
         update : true
      },
      paramsFilter : {
          container : "#filterContainer",
          defaults : {
          types : [ "organizations" ]
        },
        filters : {
          text : true
        }
      }
      
  };
  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");

};

adminPanel.views.communs = function(){
  var data={
    title : "Les communs co-financés",
    types : ["projects"],
    table : {
      name: {
        name : "Nom",
        preview : true
      },
      shortDescription:{
        name : "Description courte"
      }
    },
      actions:{
         delete : true,
         update : true
      },
      paramsFilter : {
          container : "#filterContainer",
          defaults : {
          types : [ "projects" ]
        },
        filters : {
          text : true
        }
      }
      
  };
  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");

};

adminPanel.views.campaign = function(){
  var data={
    title : "Les campagnes",
    types : ["crowdfunding"],
    table : {
      name: {
        name : "Nom",
        preview : true
      },
      description:{
        name : "Description"
      }
    },
      actions:{
         validateSource : true,
         updateCollectif : true,
         delete : true,
         update : true
      },
      paramsFilter : {
          container : "#filterContainer",
          defaults : {
          types : [ "crowdfunding" ],
          filters : {
            type : "campaign"
          }
        },
        filters : {
          text : true
        }
      }
      
  };
  ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");

};

adminPanel.views.pledge = function(){

      var data={
        title : "Promesses de don",
        //types : ["crowdfunding"],
        table : {
          name: {
            name : "Nom",
            preview : true
          },
          amount : {
            name : "Montant",
            sum : true
          },
          type : {
            name : "type",
            preview : true
          }
          // validateField :{
          //   field : "type",
          //   value : "donation"
          // }

        },  
        actions:{
           delete : true,
           validatePledge : true
        },
        paramsFilter : {
            container : "#filterContainer",
            options : {
          tags : {
            verb : '$all'
          }
        },
            defaults : {
              types : [ "crowdfunding" ],
              //type : "pledge"
              filters : {
                type : "pledge"
              }
            },
            filters : {
          publicData : {
            view : "dropdownList",
            type : "filters",
            name : "Données publiques",
            //event : "selectList",
            field : "publicDonationData",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Données publiques",
              "false" : "Données privées"
            }
          },
          invoice : {
            view : "dropdownList",
            type : "filters",
            name : "Demande de facture",
            //event : "selectList",
            field : "invoice",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Facture demandée",
              "false" : "Facture non demandée"
            }

            
          }
        }
          },
          csv : [
              {
                  url : baseUrl+'/co2/export/csv/',
                  defaults : {
                          indexStep : 0,
                          fields : [
                              "name","behalf","type","civility","surname","donatorName","email","amount","invoice","invoiceName","invoiceAddress","invoiceSiret","publicDonationData"
                          ]
                  }
              }
          ]
          
      };
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
    };
    adminPanel.views.donation = function(){

      var data={
        title : "Dons validés",
        //types : ["crowdfunding"],
        table : {
          name: {
            name : "Nom",
            preview : true
          },
          amount : {
            name : "Montant",
            sum : true
          },
          type : {
            name : "type",
            preview : true
          }
        },  
        actions:{
           delete : true
        },
        options : {
        tags : {
          verb : '$all'
        }
      },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
              types : [ "crowdfunding" ],
              //type : "pledge"
              filters : {
                type : "donation"
              }
            },
            filters : {
          publicData : {
            view : "dropdownList",
            type : "filters",
            name : "Données publiques",
            field : "publicDonationData",
            action : "filters",
            event : "filters",
            keyValue : false,
            list : {
              "true" : "Données publiques",
              "false" : "Données privées"
            }
          },
          invoice : {
            view : "dropdownList",
            type : "filters",
            name : "Demande de facture",
            //event : "selectList",
            field : "invoice",
            action : "filters",
                  event : "filters",
            keyValue : false,
            list : {
              "true" : "Facture demandée",
              "false" : "Facture non demandée"
            }

            
          }
        }
        },
        csv : [
              {
                  url : baseUrl+'/co2/export/csv/',
                  defaults : {
                          indexStep : 0,
                          fields : [
                              "name","civility","surname","donatorName","email","amount","invoice","behalf","invoiceName","invoiceAddress","invoiceSiret","publicDonationData"
                          ]
                  }
              }
          ]
          
      };
      ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
    };

// adminPanel.views.pledge = function(){

//   var data={
//     title : "Promesses de don",
//     //types : ["crowdfunding"],
//     table : {
//       name: {
//         name : "Nom"
//       },
//       amount : {
//         name : "Montant",
//         sum : true
//       },
//       type : {
//         name : "type"
//       },
//       validateField :{
//         field : "type",
//         value : "donation"
//       }

//     },  
//     actions:{
//        delete : true,
//        validatePledge : true
//     },
//     paramsFilter : {
//         container : "#filterContainer",
//         defaults : {
//           types : [ "crowdfunding" ],
//           //type : "pledge"
//           filters : {
//             type : "pledge"
//           }
//         }
//       }
      
//   };
//   ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
// };

// adminDirectory.bindCostum = function(aObj){
//   mylog.log("validatePledgeBtn");
//     $(".validatePledgeBtn").off().on("click", function(){
//       $(this).find("i").removeClass("fa-check").addClass("fa-spinner fa-spin");
//       var $this=$(this);
//       var params={
//         id:$(this).data("id"),
//         type:$(this).data("type")
//       };
//       ajaxPost(
//         null,
//         baseUrl+'/co2/crowdfunding/validatepledge/type/'+params.type+'/id/'+params.id,
//         params,
//         function(data){ 
//               mylog.log("validatepledge callback",data);
//               if(data.result==true){
//                 toastr.success(data.msg);
//                 urlCtrl.loadByHash(location.hash);
//               }
//               else{
//                 toastr.error(trad.somethingwentwrong);
//               }
//         }
//       );
//   });
// };



// adminDirectory.actions.validatePledge = function(e, id, type, aObj){
//   mylog.log("adminDirectory.actions validatePledge", e, id, type, aObj);
//   var str="";
//   if(typeof e.type != "undefined" && e.type == "pledge"){
//     str = '<button data-id="'+id+'" data-type="'+type+'" class="margin-right-5 validatePledgeBtn btn bg-green-k text-white"><i class="fa fa-check"></i> Valider (reçu)</button>';
//   }
//   return str;
// };



