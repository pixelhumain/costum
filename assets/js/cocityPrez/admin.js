adminPanel.views.cocityAdmin=function(){
    var data = {
        title: "Liste des Cocity",
        subTitle : "Supervisez tous les cocity en un coup d'œil : activité, statistiques de visite et administration simplifiée.",
        //types : [ "citoyens" ],
        table: {
            name: {
                name: "Nom",
                /*sort : {
                    field : "name",
                    up : true,
                    down : true
                }*/
            },
            slug: {
                name: "Slug"
            },
            numberOfVisit : {
                name : "Nombre de visite",
                sort : {
                    field : "costum.numberOfVisit",
                    up : true,
                    down : true
                }
            }

        },
        actions: {
            statCostum : true,
            sizeCostum : true,
            visitCostum : true
        },
        paramsFilter: {
            container: "#filterContainer",
            defaults: {
                notSourceKey : true,
                types: ["organizations"],
                fields: ["name", "email", "collection","created","slug","costum"],
                sort: {name: 1},
                filters : {
                    $or : {
                        $and : [
                            {"costum.slug" :{'$not':{'$regex':"templateCocity"}}},
                            {"costum.cocity" :{'$exists':true}},
                            {"costum.slug" :{'$exists':true}}
                        ]
                    },
                },
                indexStep : 30
            },
            filters: {
                text: true
            }
        }
    };

    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminPanel.views.analyseCocity = function() {
    ajaxPost('#content-view-admin', baseUrl+'/costum/cocity/analyse', null, function(){});
}