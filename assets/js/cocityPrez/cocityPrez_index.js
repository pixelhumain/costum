costum.lists.theme[tradTags.food] = {
    "agriculture": "agriculture",
    "alimentation": "alimentation",
    "nourriture": "nourriture",
    "AMAP": "AMAP"
}
costum.lists.theme[tradTags.health] = {
    "santé": "santé"
}


costum.lists.theme[tradTags.waste] = {
    "déchets": "déchets"
}


costum.lists.theme[tradTags.transport] = {
    "Urbanisme": "Urbanisme",
    "transport": "transport",
    "construction": "construction"
}


costum.lists.theme[tradTags.education] = {
    "éducation": "éducation",
    "petite enfance": "petite enfance"
}

costum.lists.theme[tradTags.citizenship] = {
    "citoyen": "citoyen",
    "society": "society"
}


costum.lists.theme[tradTags.economy] = {
    "ess": "ess",
    "économie social solidaire": "économie social solidaire"
}

costum.lists.theme[tradTags.energy] = {
    "énergie": "énergie",
    "climat": "climat"
}


costum.lists.theme[tradTags.culture] = {
    "culture": "culture",
    "animation": "animation"
}


costum.lists.theme[tradTags.environment] = {
    "environnement": "environnement",
    "biodiversité": "biodiversité",
    "écologie": "écologie"
}

costum.lists.theme[tradTags.numeric] = {
    "informatique": "informatique",
    "tic": "tic",
    "internet": "internet",
    "web": "web"
}

costum.lists.theme[tradTags.sport] = {
    "sport": "sport"
}

costum.lists.theme[tradTags.associations] = {
    "associations" : "association"
}

costum.lists.theme[tradTags.pact] = {
    "pacte" : "pacte"
}

costum.lists.theme[tradTags.thirdplaces] = {
    "TiersLieux" : "TiersLieux"
}

var allThem = {
    "alimentation" : {
        "name" : "Food",
        "icon" : "fa-cutlery",
        "tags" : [ 
            "agriculture", 
            "alimentation", 
            "nourriture", 
            "AMAP"
        ]
    },
    "santé" : {
        "name" : "Health",
        "icon" : "fa-heart-o",
        "tags" : [ 
            "santé"
        ]
    },
    "déchets" : {
        "name" : "Waste",
        "icon" : "fa-trash-o ",
        "tags" : [ 
            "déchets"
        ]
    },
    "transport" : {
        "name" : "Transport",
        "icon" : "fa-bus",
        "tags" : [ 
            "Urbanisme", 
            "transport", 
            "construction"
        ]
    },
    "éducation" : {
        "name" : "Education",
        "icon" : "fa-book",
        "tags" : [ 
            "éducation", 
            "petite enfance"
        ]
    },
    "citoyenneté" : {
        "name" : "Citizenship",
        "icon" : "fa-user-circle-o",
        "tags" : [ 
            "citoyen", 
            "society"
        ]
    },
    "Économie" : {
        "name" : "Economy",
        "icon" : "fa-money",
        "tags" : [ 
            "ess", 
            "économie social solidaire"
        ]
    },
    "énergie" : {
        "name" : "Energy",
        "icon" : "fa-sun-o",
        "tags" : [ 
            "énergie", 
            "climat"
        ]
    },
    "culture" : {
        "name" : "Culture",
        "icon" : "fa-universal-access",
        "tags" : [ 
            "culture", 
            "animation"
        ]
    },
    "environnement" : {
        "name" : "Environnement",
        "icon" : "fa-tree",
        "tags" : [ 
            "environnement", 
            "biodiversité", 
            "écologie"
        ]
    },
    "numérique" : {
        "name" : "Numeric",
        "icon" : "fa-laptop",
        "tags" : [ 
            "informatique", 
            "tic", 
            "internet", 
            "web",
            "numérique"
        ]
    },
    "sport" : {
        "name" : "Sport",
        "icon" : "fa-futbol-o",
        "tags" : [ 
            "sport"
        ]
    },
    "tiers lieux" : {
        "name" : "Third places",
        "icon" : "fa-globe",
        "tags" : [
            "TiersLieux"
        ]
    },
    "pacte" : {
        "name" : "Pact",
        "icon" : "fa-hand-o-up",
        "tags" : [
            "pacte"
        ]
    },
    "associations" : {
        "name" : "Associations",
        "icon" : "fa-chain",
        "tags" : [
            "associations"
        ]
    }
};
var allSugThematic = [
    tradTags["food"],
    tradTags["health"] ,
    tradTags["associations"],
    tradTags["pact"],
    tradTags["third places"],
    tradTags["transport"] ,
    tradTags["education"],
    tradTags["citizenship"],
    tradTags["economy"],
    tradTags["energy"],
    tradTags["culture"],
    tradTags["environment"],
    tradTags["numeric"],
    tradTags["sport"],
    tradTags["waste"] 
];


function useTemplate(tpl_params, loadpage, progress = 0, progressBar = '', progressPercentage = '') {
    return new Promise((resolve, reject) => {
        var params = {
            "id"        : tpl_params.idTplSelected,
            "page"      : "",
            "newCostum" : true,
            "action"    : tpl_params.action,
            "collection": tpl_params.collection,
            "parentId"  : tpl_params.contextId,
            "parentSlug": tpl_params.contextSlug,
            "parentType": tpl_params.contextType
        };
        ajaxPost(
            null,
            baseUrl + "/co2/template/use",
            params,
            function (data) {
                if(progress && progress > 0) {
                    progressBar.style.width = progress + '%';
                    progressPercentage.textContent = Math.round(progress) + '%';
                } 
                if (loadpage) {
                    window.location = baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug;
                }
                resolve();
            },
            { async: false }
        );
    });
}


function createOrgaFiliere(params, paramscocity, progress, progressBar, progressPercentage) {
    var costumize = paramscocity.thematic == "Tiers lieux" ? "reseauTierslieux" : "costumize";
    return new Promise((resolve, reject) => {
        ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
            function(result){
                var data = {
                    collection : 'organizations',
                    id: result.id,
                    path: 'allToRoot',
                    value : {
                        "costum.slug" : costumize,
                        "cocity" : paramscocity.cocity, 
                        "thematic" : paramscocity.thematic, 
                        "ville" : paramscocity.ville,
                        "source.key" : paramscocity.keys,
                        "source.keys" : [paramscocity.keys],
                        "costum.typeCocity" : paramscocity.typeZone
                    }
                };

                if(paramscocity.address) data.value["address"] = paramscocity.address;
                
                // dataHelper.path2Value(data, function(){
                //     // let tpl_params = {"idTplSelected" : paramscocity.idTemplatefiliere , "collection" : "templates", "action" : "", "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
                //     let loadpage = false;
                //     var params = {"thematic" : paramscocity.thematic , "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
                //     costum.importContent(params, loadpage, progress, progressBar, progressPercentage);
                //     // useTemplate(tpl_params, loadpage, progress, progressBar, progressPercentage);
                //     resolve();
                // });

                dataHelper.path2Value(data, function(){
                    // let tpl_params = {"idTplSelected" : paramscocity.idTemplatefiliere , "collection" : "templates", "action" : "", "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
                    let loadpage = false;
                    let themat = (paramscocity.thematic).toLowerCase();
                    var paramsImport = {"thematic" : themat , "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};                    
                    co.importCostumContent(paramsImport, loadpage, progress, progressBar, progressPercentage);
                    // useTemplate(tpl_params, loadpage, progress, progressBar, progressPercentage);
                    resolve();
                });
            },
            function(){
                toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
                reject();
            },
            "json"
        );
    });
}

function path2ValueCity(params, tpl_params, loadpage) {
    ajaxPost(
        null,
        baseUrl+"/costum/cocity/update",
        params,
        function(results) { 
            useTemplate(tpl_params, loadpage);
        }
    )
}