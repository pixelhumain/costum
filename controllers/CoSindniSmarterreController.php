<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class CoSindniSmarterreController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions()
	{ 
	    return array(
            'observatory' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coSindniSmarterre\ObservatoryAction::class,
	    );
	}

}