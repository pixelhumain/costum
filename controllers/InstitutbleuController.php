<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class InstitutbleuController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'elementhome'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\HomeAction::class,
        	'deletewithsource'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\DeleteWithSourceAction::class,
        	'fichepdf' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\FicheActeurAction::class,
        	//'invitationhandler' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\InvitationHandlerAction::class,
        	'updatevalue' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\UpdateValueAction::class,
        	'connect' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu\ConnectAction::class,
        );
    }
}