<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers;
    
    use CommunecterController;
    
    class LacompagniedestierslieuxController extends CommunecterController {
        
        
        public function beforeAction($action) {
            //parent::initPage();
            return parent::beforeAction($action);
        }
        
        public function actions() {
            return array(
                'importcontact'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux\ImportContactAction::class,
                'gestionformation' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux\GestionFormationAction::class,
                'gestionsession'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux\GestionSessionAction::class,
                'inviteinsession'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux\InviteInSessionAction::class,
                'register'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux\RegisterAction::class,
            );
        }
        
    }
