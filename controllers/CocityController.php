<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use City;
use CommunecterController;
use Organization;
use PHDB;
use Yii;

class CocityController extends CommunecterController {

	public function beforeAction($action) {
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getcityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetCityAction::class,
			'getlistcocityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetListCocityAction::class,
			'getfiliere' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetFiliereAction::class,
			'checkdatafiliere' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\CheckDataFiliereAction::class,
			'checkzone' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\CheckZoneAction::class,
			'getorgafiliere' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetOrgaFiliereAction::class,
			'getcmscocityaction' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCmsCocityAction::class,
			'graph' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GraphAction::class,
			'generatecocity' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GenerateCocityAction::class,
			'update' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\CocityUpdateValueAction::class,
			'souszone' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetSousZoneAction::class,
			'classdatathematique' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\CheckDataThemaAction::class,
			'addmember' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\AddMemberUpdateAction::class,
			'navigatshapemap' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\NavigateShapMapAction::class,
			'getdataoddzone' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetDataOddZonesAction::class,
			'getcitiscommunes' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\ExternDataCityShapeAction::class,
			'analyse' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\AnalyseCocityAction::class,
			'getnbrelmbycity' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity\GetNuberElementByCityAction::class,
		);
			
	}
	public function actionupdatePC(){
		$allCocity = PHDB::find(Organization::COLLECTION, array( "costum.slug" => "cocity"));
		foreach($allCocity as $kCocity => $vCocity){
			if($vCocity["address"] != null){
				$city = City::getById($vCocity["address"]["localityId"]);
				$retour = '';
				foreach($city["postalCodes"] as $kpc => $vp){
					if($vCocity["name"] == $vp["name"] && $vCocity["address"]["postalCode"] !== $vp["postalCode"]){
						if (!is_string($vp["geo"]["latitude"]))
							$latitude = strval($vp["geo"]["latitude"]);
						else
							$latitude = $vp["geo"]["latitude"];
						if (!is_string($vp["geo"]["longitude"]))
							$longitude = strval($vp["geo"]["longitude"]);
						else 
							$longitude = $vp["geo"]["longitude"];
						$geo = [
							'@type' => $vp["geo"]["@type"],
							'latitude' => $latitude,
							'longitude' => $longitude
						];
						$assetUrl =  Yii::app()->createUrl('/costum');
						$retour .= "Mise à jour de la code postale du cocity <a href='".$assetUrl."/co/index/slug/".$vCocity["slug"]."'  target='_blank'>".$vCocity["name"]."</a></br>";
						
						PHDB::update(Organization::COLLECTION, 
							array('slug' =>$vCocity["slug"]), 
							array('$set' => ["geoPosition"=>$vp["geoPosition"], "geo"=> $geo,"address.postalCode"=>$vp["postalCode"] ]));
					}
				}
				return $retour;
			} 			
		}
		
	} 
}
