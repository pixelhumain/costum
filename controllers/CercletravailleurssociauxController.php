<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class CercletravailleurssociauxController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'elementhome'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cercletravailleurssociaux\HomeAction::class,       
	        'addtochat'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cercletravailleurssociaux\AddToChatAction::class	       
        );
    }
}