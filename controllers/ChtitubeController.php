<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class ChtitubeController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'references'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\chtitube\ReferencesAction::class,
	        'validate'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\chtitube\ValidateAction::class,
	    );
	}

}
