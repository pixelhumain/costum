<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class CostumizeController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'importelm'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumize\ImportelmAction::class,
            'saveimport'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\costumize\SaveImportAction::class
	    );
	}

}
