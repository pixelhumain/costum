<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class PacteController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'register'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\RegisterAction::class,
	        'checkexist'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\CheckExistAction::class,
	        'validategroup'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\ValidateGroupAction::class,
	        'importcontrat'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\ImportcontratAction::class,
	        'importcollectif'  => \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\ImportcollectifAction::class,
	        'savecontrat'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\SaveContratAction::class,
	        'savecollectif'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\SaveCollectifAction::class,
	        'searchsignatures'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\SearchSignaturesAction::class,
	        'mapsearch'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\MapSearchAction::class,
	        "collectifsignedcsv"=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte\CollectifSignedCsvAction::class,

	    );
	}

}
