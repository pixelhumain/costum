<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class Essconnect9711Controller extends CommunecterController
{
    public function actions(){
        return array(
            'validategroup' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711\ValidateGroupAction::class,
            'references' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711\ReferencesAction::class,
            'register' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711\RegisterAction::class,
            'setsource' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ManageReferencesAction::class,
            'validateinvitationbymail' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ValidateInvitationByMailAction::class,
            "upload-save" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\UploadSaveAction::class,
            "getelementanswers" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711\GetElementAnswersAction::class,
        );
    }
}