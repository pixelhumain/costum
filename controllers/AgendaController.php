<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers;
    
    use CommunecterController;
    use PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda\EventAction;
    use PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda\PersonAction;
    use PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda\ProjectAction;
    
    class AgendaController extends CommunecterController {
        public function beforeAction($action) {
            return parent::beforeAction($action);
        }
        
        public function actions() {
            return [
                'events'   => EventAction::class,
                'projects' => ProjectAction::class,
                'persons'  => PersonAction::class
            ];
        }
    }
