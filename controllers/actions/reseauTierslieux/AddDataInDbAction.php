<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ReseauTierslieux;
use CAction;
use Import;
use Rest;
use Yii;
use PixelHumain\PixelHumain\components\Action;
use ReseauTierslieux;

class AddDataInDbAction extends Action
{
    public function run(){
    	
    	$controller=$this->getController();
    	$result = ReseauTierslieux::addDataInDb($_POST);
    	
    	return Rest::json( $result );
    }
}