<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\reseautierslieux\pdf;

use CAction, ReseauTierslieux;
class AllContactPdfAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null, $page="base", $expertise = null,$territorialCercle = null,$trainer = null,$interestingTraining = null, $ptlConsideredTraining = null, $competence = null, $text = null) {
		$controller=$this->getController();
		$params["query"] = [];
		if($expertise !== null)
			$params["query"]["expertise"] = $expertise;
		if($territorialCercle !== null)
			$params["query"]["territorialCercle"] = $territorialCercle;
		if($territorialCercle !== null)
			$params["query"]["territorialCercle"] = $territorialCercle;
		if($trainer !== null)
			$params["query"]["trainer"] = $trainer;
		if($interestingTraining !== null)
			$params["query"]["interestingTraining"] = $interestingTraining;
		if($ptlConsideredTraining !== null)
			$params["query"]["ptlConsideredTraining"] = $ptlConsideredTraining;
		if($competence !== null)
			$params["query"]["competence"] = $competence;
		$params["slug"] = $slug; 
		$params["page"] = $page; 
		$params["controller"] = $controller; 
		ReseauTierslieux::pdfContact($params);

	}
}