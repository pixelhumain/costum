<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cercletravailleurssociaux;
use MongoId;
use PHDB;

class HomeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
		$controller = $this->getController();
    	$params=array();
    	$el= PHDB::findOne($type,
                array("_id" => new MongoId($id)));
        if(isset($el["creator"])){
            $creator= PHDB::findOne("citoyens",
                array("_id" => new MongoId($el["creator"])),["username"]);
            $params["creator"] = $creator["username"] ; 
        }
        $params["element"] = $el;
    	return $controller->renderPartial("costum.views.custom.CercleTravailleursSociauxLiberauxLaPlumeSociale.element.fiche",$params,true);
    }
}