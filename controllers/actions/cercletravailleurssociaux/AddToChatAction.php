<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cercletravailleurssociaux;
use MongoId;
use PHDB;
use RocketChat;
use Rest;

class AddToChatAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
		
		$room = $_POST["room"]??null;
		$contextSlug = $_POST["costumSlug"]??null;
		$contextType = $_POST["costumType"]??null;
		$contextId = $_POST["costumId"]??null;
		$username = $_POST["username"] ?? null;
		$response = array();
		$response = RocketChat::inviteGroup($room, $username);
		return Rest::json($response);
    }
}