<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterritoireautre;

use CAction, Smarterritoireautre, Rest;
class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Smarterritoireautre::getCommunity($_POST["contextId"],$_POST["contextType"]);
        
        return Rest::json($params);
    }
}