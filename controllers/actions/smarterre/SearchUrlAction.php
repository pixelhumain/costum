<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre;

use CAction, Smarterre, Rest;
class SearchUrlAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id=null,$type=null,$slug=null, $view=null,$page=null){
		$controller = $this->getController();
		$params = Smarterre::searchUrl($_POST);


	return Rest::json($params);
	}
}