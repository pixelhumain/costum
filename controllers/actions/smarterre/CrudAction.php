<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre;
use CAction;
use Folder;
use Person;
use Yii;

class CrudAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($action = "new"){
        $res = array("result" => false, "msg" => Yii::t("common","Something when wong"));
        if(!Person::logguedAndValited()) return array("result"  => false, "msg" =>  Yii::t("common","Please Login First"));
        else{
            try{
                //On ne mettra que l'action crée pour la temps.
                if($action == "create") $res = Folder::create(@$_POST["targetId"], @$_POST["name"], "bookmarks", @$_POST["folderId"]);
            } catch(Exception $e){
                $res = array("result" => false, "msg" => $e);
            }
        }
    }
}