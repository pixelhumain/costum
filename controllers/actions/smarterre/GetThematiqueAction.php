<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\smarterre;

use CAction, Smarterre, Rest;
class GetThematiqueAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Smarterre::getThematique($_POST);
        
        return Rest::json($params);
    }
}