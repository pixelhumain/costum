<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\hubmednum;
use CAction;
use hubMedNum;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = hubMedNum::getCommunity($_POST["contextId"],$_POST["contextType"]);
        
        return Rest::json($params);
    }
}