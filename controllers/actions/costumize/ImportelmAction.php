<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumize;

use Yii;
class ImportelmAction extends \PixelHumain\PixelHumain\components\Action {
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$params = array() ;
		$page = "importelm";
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial("/custom/costumize/".$page,$params,true);
	}
}