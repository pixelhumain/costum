<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumize;

use CAction, Costumize, Rest;
class SaveImportAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M'); 
        $params = Costumize::saveImport($_POST); 
        return Rest::json($params);
    }
}
?>