<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux;

use Yii;
use Mail;
use Rest;
use Slug;
use MongoDate;
use Preference;
use CacheHelper;
use CTKException;
use Role;
use Person;
use Element;
/**
   * Register a new user for the application
   * Data expected in the post : name, email, postalCode and pwd
   * @return Array as json with result => boolean and msg => String
   */
class RegisterAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();

        $name = (!empty($_POST['name'])) ? $_POST['name'] : "";
        $username = (!empty($_POST['username'])) ? $_POST['username'] : "";
        $telephone = (!empty($_POST['telephone'])) ? $_POST['telephone'] : "";
        $surname = (!empty($_POST['surname'])) ? $_POST['surname'] : "";
        $firstName = (!empty($_POST['firstName'])) ? $_POST['firstName'] : "";
        $structure= (!empty($_POST['structure'])) ? $_POST['structure'] : "";
        // $situation = (!empty($_POST['situation'])) ? $_POST['situation'] : "";
        // $arrondissement = (!empty($_POST['arrondissement'])) ? $_POST['arrondissement'] : "";
        // $gender = (!empty($_POST['gender'])) ? $_POST['gender'] : "";
		$email = (!empty($_POST['email'])) ? $_POST['email'] : "";
		$pwd = (!empty($_POST['pwd'])) ? $_POST['pwd'] : "";
		$address = (!empty($_POST['address'])) ? $_POST['address'] : null;
		$pendingUserId = (!empty($_POST['pendingUserId'])) ? $_POST['pendingUserId'] : "";
		$newsletterSub = (!empty($_POST['newsletter'])) ? $_POST['newsletter'] : "";

        // var_dump($structure);

		$newPerson = array(
			'name'=> $name,
			'username'=> $username,
			'telephone'=> array("mobile"=>[$telephone]),
			// 'gender'=> $gender,
			// 'situation'=> $situation,
			// 'arrondissement'=> $arrondissement,
			"newsletterSubscription" => (!empty($newsletterSub)) ? "true" : null,
			'pwd'=>$pwd,
			'structure' => $structure,
			'surname' => $surname,
			"firstName" => $firstName
		);

		$inviteCode = (@$_POST['inviteCode']) ? $_POST['inviteCode'] : null;
		$sessionRegister = (isset($_POST['sessionRegister'])) ? $_POST['sessionRegister'] : null;

		if (isset($_POST['mode'])) 
			$mode = $_POST["mode"];
		else 
			$mode = Person::REGISTER_MODE_NORMAL;
	//	if(isset($_POST['loginAfter']))
	//		$newPerson["firstConnect"]=true;
		// Deprecated but keep it for Rest calls.
		// => @Bouboule : I comment to use import process 
		/*if ($mode == Person::REGISTER_MODE_NORMAL) {
			$newPerson['city'] = @$_POST['city'];
			$newPerson['postalCode'] = @$_POST['cp'];
			$newPerson['geoPosLatitude'] = @$_POST['geoPosLatitude'];
			$newPerson['geoPosLongitude'] = @$_POST['geoPosLongitude'];
		}*/

		$pendingUserId = Person::getPendingUserByEmail($email);
		
		//The user already exist in the db (invitation process) : the data should be updated
		if ($pendingUserId != "") {
			$res = Person::updateMinimalData($pendingUserId, $newPerson);
			if (! $res["result"]) {
				return Rest::json($res);
				exit;
			} 
		//New user
		} else {
			try {
				$account = Person::getPersonByEmail($email);
    		  	if ($account) {
    		  		throw new CTKException(Yii::t("person","Problem inserting the new person : a person with this email already exists in the plateform"));
    		  	}
    			$person = $newPerson;

				$person['email'] = $email;

				$person["roles"] = Role::getDefaultRoles();
			  	//Person::addPersonDataForBetaTest($person, $mode, $inviteCode);
			  	if(!empty($forced)){
			  		$person=array_merge($person,$forced);
			  	}
			  	$person["created"] = new MongoDate(time());
			  	$person["collection"] = Person::COLLECTION;
			  	//$person["preferences"] = array("seeExplanations"=> true);
			  	$person["preferences"] = Preference::initPreferences(Person::COLLECTION);
			  	$person["seePreferences"] = true;
				$person["slug"]=Slug::checkAndCreateSlug($person["username"]);
				$costum = CacheHelper::getCostum();
			  	if(@$costum && @$costum["slug"]){
			  		$person["source"]=array("origin"=>"costum", "key"=>$costum["slug"], "keys"=>[$costum["slug"]]);
			  	}
			  	Yii::app()->mongodb->selectCollection(Person::COLLECTION )->insert( $person);
		        if (isset($person["_id"])) {
		        	$newpersonId = (String) $person["_id"];
		        	if (! empty($pwd)) {
			        	//Encode the password
					  	$encodedpwd = $this->hashPassword($newpersonId, $pwd);
					  	Person::updatePersonField($newpersonId, "pwd", $encodedpwd, $newpersonId);
					} 
			        Slug::save(Person::COLLECTION,(string)$person["_id"],$person["slug"]);
			    } else {
			    	throw new CTKException("Problem inserting the new person");
			    }

				//A mail is sent to the admin
				//Mail::notifAdminNewUser($person);
				$res = array("result"=>true, "msg"=>"You are now communnected", "id"=>$newpersonId, "person"=>$person);

				if(!empty($person["invitedBy"]))
					$res['invitedBy'] = $person["invitedBy"];
	    









				//$res = Person::insert($newPerson, $mode,$inviteCode);
				$newPerson["_id"]=$res["id"];
			} catch (CTKException $e) {
				$res = array("result" => false, "msg"=>$e->getMessage());
				return Rest::json($res);
				exit;
			}
		}

		//Try to login with the user
		$res2 = Person::login($email,$pwd,true, true);
		if(isset($person)){
			Mail::validatePerson($person);
		}
		
		// var_dump(Yii::)
		if(isset($_POST["isInvitation"]))
			$res["isInvitation"]=true;

		if ($res["result"]) {
		    array("result"=>true, "msg"=> Yii::t("login","Congratulation your account is created !")."<br>".Yii::t("login","Last step to enter : we sent you an email, click on the link to validate your mail address."), "id"=>(string)$newPerson["_id"], "person"=>$newPerson );		
		} else if ($res["msg"] == "betaTestNotOpen") {
			$newPerson["_id"] = $pendingUserId;
			$newPerson['email'] = $email;
			//send betatest information mail
			Mail::betaTestInformation($newPerson);
			$res = array("result"=>true, 
					"msg"=> Yii::t("login","You are now communnected !")."<br>".Yii::t("login","Our developpers are fighting to open soon ! Check your mail that will happen soon !")."<br>".Yii::t("login","If you really want to start testing the platform before, send us an email and we'll consider your demand :)"), 
					"id"=>$pendingUserId); 
		} 
	
		return Rest::json($res);
		exit;
    }
	
	private static function hashPassword($personId, $pwd) {
		return hash('sha256', $personId.$pwd);
	}
}

