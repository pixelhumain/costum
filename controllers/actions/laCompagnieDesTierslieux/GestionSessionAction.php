<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux;
    
    use DateTime;
    use DateTimeZone;
    use Event;
    use Person;
    use PHDB;
    use Project;
    use PixelHumain\PixelHumain\components\Action;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    
    class GestionSessionAction extends Action {
        public function run($sessionId, $mode = 'w') {
            $controller = $this->getController();
            $sessionFieldsList = [
                'name',
                'parent',
                'timeZone',
                'links.attendees',
                'links.organizer',
                'visioDate',
                'gatheringDates',
                'interestedLink'
            ];
            $sessionDb = PHDB::findOneById(Event::COLLECTION, $sessionId, $sessionFieldsList);
            
            // var_dump($sessionDb['links']['organizer']);exit;
            if(!empty($sessionDb['links']['organizer'])){
                foreach ($sessionDb['links']['organizer'] as $idOrga=>$org){
                    if($org['type']==Project::COLLECTION){
                        $parentId=$idOrga;
                        break;
                    }
                }
            }else if(!empty($sessionDb['parent']) && sizeof($sessionDb['parent'])>0){
                $parentId=array_keys($sessionDb['parent'])[0];
            }
            // var_dump($parentId);exit;
           
            $formationDb = PHDB::findOneById(\Project::COLLECTION, $parentId, ['name']);
            $formation = [
                'id'   => (string)$formationDb['_id'],
                'name' => $formationDb['name'],
            ];

            $session = [
                'id'   => (string)$sessionDb['_id'],
                'name' => $sessionDb['name'],
                'visioDates' =>[],
                'gatheringDates'=> [],
                'interestedLink'=>$sessionDb['interestedLink'] ?? ''
            ];
            if(isset($sessionDb['gatheringDates'])){
                $session["gatheringDates"]=$sessionDb['gatheringDates'];
            }

            if(isset($sessionDb["visioDate"]["alldates"]) && !empty($sessionDb["visioDate"]["alldates"])){
                // $allvisioDate=[];
                foreach($sessionDb["visioDate"]["alldates"] as $date=>$hours){
                    foreach($hours as $id=>$hour){
                        // $date=str_replace("/","-",$date);
                        // $timeStampDate=strtotime($date." ".$hour);
                        $session["visioDates"][]=$date." ".$hour;

                    }
                }
            }
            // var_dump($formation["visioDates"]);
            // exit;
            
            $attendees = [];
            $attendeesDbList = $sessionDb['links']['attendees'] ?? [];
            $personsDbList = array_keys($attendeesDbList);
            $personsDbList = PHDB::findByIds(Person::COLLECTION, $personsDbList);
            $dataList = [];
            foreach ($attendeesDbList as $attendeesId => $attendeesDb) {
                // empty persons and not admin
                if (empty($personsDbList[$attendeesId]) || empty($personsDbList[$attendeesId]['name']) || !empty($personsDbList[$attendeesId]["links"]["events"][$sessionId]["isAdmin"])) {
                    continue;
                }
                $dateFieldsist = ['dateLastContact', 'visioDate'];
                foreach ($dateFieldsist as $dateField) {
                    $dateFieldDateTimeVar = $dateField . 'DateTime';
                    if (!empty($attendeesDb[$dateField])) {
                        $$dateFieldDateTimeVar = new DateTime();
                        $$dateFieldDateTimeVar->setTimestamp(UtilsHelper::get_as_timestamp([$dateField], $attendeesDb));
                        if (!empty($sessionDb['timeZone'])) {
                            $$dateFieldDateTimeVar->setTimezone(new DateTimeZone($sessionDb['timeZone']));
                        }
                    }
                    $$dateField = !empty($$dateFieldDateTimeVar) ? $$dateFieldDateTimeVar->format('d/m/Y') : '';
                }
                $dataList[] = [
                    'id'                  => $attendeesId,
                    'name'                => !empty($personsDbList[$attendeesId]['firstName']) || !empty($personsDbList[$attendeesId]['lastName']) ? ($personsDbList[$attendeesId]['firstName'] ?? '') . ' ' . ($personsDbList[$attendeesId]['lastName'] ?? '') : $personsDbList[$attendeesId]['name'],
                    'motivation'          => $attendeesDb['motivation'] ?? '',
                    'actionToDo'          => $attendeesDb['actionToDo'] ?? '',
                    'dateLastContact'     => $dateLastContact,
                    'contactTypology'     => $attendeesDb['contactTypology'] ?? '',
                    'lastExchangeSummary' => $attendeesDb['lastExchangeSummary'] ?? '',
                    'informationOrigin'   => $attendeesDb['informationOrigin'] ?? '',
                    'visioFollowed'       => $attendeesDb['visioFollowed'] ?? '',
                    'visioDate'           => $visioDate,
                    'funding'             => $attendeesDb['funding'] ?? '',
                    'otherComments'       => $attendeesDb['otherComments'] ?? ''
                ];
            }
            foreach ($personsDbList as $personId => $personDb) {
                if (empty($personDb['name'])) {
                    continue;
                }
                $defaultPerson = [
                    'email'     => '',
                    'firstName' => '',
                    'lastName'  => '',
                    'address'   => null,
                    'geo'       => null,
                    'telephone' => [
                        'mobile' => [],
                        'fixe'   => []
                    ]
                ];
                $attendees[$personId] = array_replace_recursive($defaultPerson, $personDb);
            }
            return $controller->renderPartial('costum.views.custom.laCompagnieDesTierslieux.gestionSession', [
                'sessionsList' => $dataList,
                'parent'       => $formation,
                'session'      => $session,
                'attendees'    => $attendees,
                'mode'         => $mode
            ]);
        }
    }