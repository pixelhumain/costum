<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux;

use CAction, Yii, HtmlHelper, PHDB, Rest;


class GestionFormationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        // $res=PHDB::find("projects",array("parent.5d36ad9340bb4ea9625e0915"=>array('$exists'=>1)));
        // return Rest::json($res);
        return $controller->renderPartial("costum.views.custom.laCompagnieDesTierslieux.gestionFormation",array(), true);


    }
} 

?>