<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\laCompagnieDesTierslieux;
    
    use DateTime;
    use DateTimeZone;
    use Event;
    use Person;
    use PHDB;
    use Project;
    use Yii;
    use InvitationLink;
    use Element;
    use PixelHumain\PixelHumain\components\Action;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    
    class InviteInSessionAction extends Action {
        public function run($type=null, $id =null) {
            $controller = $this->getController();
            if(!empty(Yii::app()->session["userId"])){
                $params = array(
                    "parentType" => ( empty($type) ? Person::COLLECTION : $type ) ,
                    "parentId" => ( empty($id) ? Yii::app()->session["userId"] : $id ) ,
                    "search" => true,
                    // "invitationLinks" => InvitationLink::getLinks($type, $id)
                );
    
                if($id && $type){
                    $params["parent"] = Element::getByTypeAndId($type, $id);
                }
    
                if(!empty($params["parentType"]) && $params["parentType"] != Person::COLLECTION){
                    $parent = Element::getElementById($id, $type, null, array("links", "id"));
                    $params["parentLinks"] = ( !empty($parent["links"]) ? $parent["links"] : array() );
                    $params["id"] = (empty($parent["id"]) ? $id : $parent["id"]);
                }
    
                //Rest::json($params); exit ;
    
                if( !empty( $controller->costum ) && !empty( $controller->costum["roles"])){
                    $params["roles"] = $controller->costum["roles"] ;
                } else
                    $params["roles"] = array();
                
                if(Yii::app()->request->isAjaxRequest)
                    return $controller->renderPartial('costum.views.custom.laCompagnieDesTierslieux.inviteInSession',$params,true);
                else 
                   return $controller->render( 'costum.views.custom.laCompagnieDesTierslieux.inviteInSession' ,$params);
            }
            
            // return $controller->renderPartial('costum.views.custom.laCompagnieDesTierslieux.inviteInSession',array("parentType"=>$type,"parentId"=>$id));
        }
    }