<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\deal;

use CAction, Rest, Yii;
use PixelHumain\PixelHumain\modules\costum\models\Deal;

class AddDataZouritAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	
    	$controller=$this->getController();
    	$result = Deal::addDataZourit($_POST);
    	
    	return Rest::json( $result );
    }
}