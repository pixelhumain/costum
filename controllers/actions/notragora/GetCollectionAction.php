<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora;

use InflectorHelper;
use MongoRegex;
use PHDB;
use Rest;
use SearchNew;

class GetCollectionAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run($type = null)
    {
        $name=$_POST["name"];

		// var_dump($_POST,$type);

    	$id="";
        $res=[];
    	// if($type!=="poi"){
            
            // $name=str_replace("-", "[\s'-]", $name);
			$name=urldecode($name);
			// $name=preg_replace_callback(
			// 			'/%[A-Z](\d+)/',
			// 			function($matches) {
			// 				var_dump(hex2bin($matches[0]));exit;
			// 				var_dump(hex2bin($matches[0]));exit;
			// 				return hex2bin($matches[1]);
			// 			},
			// 			$name
			// 		);
            $textRegExp = SearchNew::accentToRegex($name);
			// $textRegExp2= str_replace("\-", "[',-]?\s?", $textRegExp);
			$textRegExp2= str_replace("\-", "[^a-zA-Z0-9]{0,3}", $textRegExp);

			$searchSlug=null;
			$searchSlug=PHDB::findOne($type,array("slug" => new MongoRegex("/{$textRegExp2}.*/i")),array("_id","name"));
			if(empty($searchSlug)){
				PHDB::findOne($type,array("slug" => new MongoRegex("/.*{$textRegExp2}.*/i")),array("_id","name"));
			}
			
			// var_dump($search);exit;

			if(!empty($searchSlug)){
				$res=$searchSlug;
			}else{
				$res = PHDB::findOne($type,array("name" => new MongoRegex("/.*{$textRegExp2}.*/i")),array("_id","name"));
			}
		
			
    		
			
			// var_dump($name,$textRegExp,$textRegExp2,$res);exit;

    	// }else{
    	// 	$collec=PHDB::find($type,array(),array("name"));
        //     $name=urldecode($name);
		// 	$name=preg_replace_callback(
		// 		'/%u(\d+)/',
		// 		function($matches) {
		// 			return mb_convert_encoding('&#'.hexdec($matches[1]).';', 'UTF-8', 'HTML-ENTITIES');
		// 		},
		// 		$name
		// 	);
    	// 	$name=InflectorHelper::slugifyLikeJs($name);

		//     $nameDb=[];
    	// 	foreach ($collec as $key => $value) {
    	// 		$valueName=InflectorHelper::slugifyLikeJs(htmlspecialchars_decode($value["name"]));
    	// 		array_push($nameDb,$valueName);
		// 		if(strrpos($valueName,$name)===false){
		// 			continue;
		// 		}
		// 		else{
		// 			$id=$key;
		// 			// break;
		// 		}
    	// 	}
		// 	var_dump($name,$nameDb);exit;
    	// 	$res=$collec[$id] ?? null;
    	// }	
		
		return Rest::json($res);
    }

}