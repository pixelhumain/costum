<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use CAction, Yii, Rest, PHDB;
class SaveVoteAction extends \PixelHumain\PixelHumain\components\Action {
    public function run($spot, $date, $time, $vote){
        $userId = Yii::app()->session[ "userId" ];
        $vote = intval($vote);
        if(!$userId){
            return Rest::json([
                "status"=>false,
                "message"=>"No user in the session"
            ]);
        }else{
            $spotData = PHDB::findOne(
                '--meteolamer-forecast',
                [
                    "spot"=>$spot,
                    "date"=>strtotime($date)
                ]
            );

            if(!$spotData){
                return Rest::json([
                    "status"=>false,
                    "message"=>"Spot not exist"
                ]);
            }else{
                $totalVote = (isset($spotData['data']["h$time"]['votes'])) ? 
                            $spotData['data']["h$time"]['votes']['total']:0;

                $userVote = PHDB::findOne(
                    '--meteolamer-forecast',
                    [
                        "_id"=>$spotData['_id'],
                        "data.h$time.votes.users.id"=>$userId
                    ], 
                    ["data.h$time.votes.users.$"=>1]
                );

                $where=[
                    "_id" => $spotData['_id']
                ];
                if(!$userVote){
                    $totalVote += $vote;

                    $data = [
                        '$set' => [
                            "data.h$time.votes.total" => $totalVote
                        ],
                        '$push' => [
                            "data.h$time.votes.users" => [
                                "id"=>$userId,
                                "vote"=>$vote
                            ]
                        ]
                    ];
                }else{
                    $userVote = $userVote["data"]["h$time"]["votes"]["users"][0];
                    $totalVote = ($totalVote - $userVote["vote"]) + $vote;
                    
                    $where = [
                        "_id" => $spotData['_id'],
                        "data.h$time.votes.users.id" => $userId
                    ];
                    $data = [
                        '$set' => [
                            "data.h$time.votes.total"=>$totalVote,
                            "data.h$time.votes.users.$.vote"=>$vote
                        ]
                    ];
                }

                PHDB::update('--meteolamer-forecast', $where, $data);
                
                return Rest::json(["status"=>true]);
            }
        }
    }
}