<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use CAction, PHDB, Rest;
class GetSpotsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($merge=FALSE){
        $spots = [];
        if($merge){
            $spots = PHDB::find('--meteolamer-spots');
        }else{
            $spots['spot'] = PHDB::findAndSort('--meteolamer-spots',  ["type" => "spot"], ["label"=>1]);
            $spots['region'] = PHDB::findAndSort('--meteolamer-spots',  ["type" => "region"], ["label"=>1]);
        }
        return Rest::json($spots);
    }
}