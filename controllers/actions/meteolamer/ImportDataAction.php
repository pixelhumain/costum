<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;
use CAction;
use Exception;
use Meteolamer;
use Rest;
use Yii;

/**
 * ImportDataMeteolamerAction.php
 *
 * this action allows to import the data coming 
 * from the meteolamer FTP server into the CODB:--meteolamer
 * 
 * @author: Yorre Rajaonarivelo
 * Date: 20/20/2020
 */

class ImportDataAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($key, $start_date=NULL, $end_date=NULL){
        $start_date = ($start_date !== NULL) ? $start_date : date("Y-m-d");
        $end_date = ($end_date !== NULL) ? $end_date : date("Y-m-d", strtotime($start_date." +6 day"));

        if($key !== Meteolamer::$API_KEY)
            return Rest::json(["status"=>0, "message"=>"La clé est invalide."]);
        try{
            Meteolamer::importData($start_date, $end_date);
            return Rest::json(["status"=>1]);
        }catch(Exception $e){
            return Rest::json(["status"=>0, "message"=>$e->getMessage()]);
        }
    }
}