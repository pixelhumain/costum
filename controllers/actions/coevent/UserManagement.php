<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;

use ActStr;
use Event;
use Notification;
use Person;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use Slug;
use Yii;

class UserManagement extends \PixelHumain\PixelHumain\components\Action
{
	public function run($request)
	{
		$response	= [
			'success'	=> false,
			'data'		=> null
		];
		switch ($request)
		{
			case 'create_temporary':
				$db_user = Person::getPersonByEmail($_POST['email']);
				$n_user = [
					'email'		=> $_POST['email'],
					'name'		=> $_POST['name'],
					'firstname'	=> $_POST['firstname'],
					'lastname'	=> $_POST['lastname'],
					'collection' => Person::COLLECTION,
					'created'	=> time(),
					'updated'	=> time()
				];
				if (!empty($db_user))
				{
					$n_user = $db_user;
					PHDB::update(Person::COLLECTION, ['_id' => $db_user['_id']], [
						'$set' => [
							'firstname'	=> $_POST['firstname'],
							'lastname'	=> $_POST['lastname'],
							'updated'	=> time(),
						]
					]);
				}
				else
				{
					Yii::app()->mongodb
						->selectCollection(Person::COLLECTION)
						->insert($n_user);
					$id = (string) $n_user['_id'];
					$slug = Slug::checkAndCreateSlug($n_user['name']);
					Slug::save(Person::COLLECTION, $id, $slug);
					PHDB::update(Person::COLLECTION, ['_id' => $n_user['_id']], [
						'$set' => [
							'slug' => $slug
						]
					]);
				}
				$response['data'] = $n_user;
				if (!empty($_POST['memberOf']) && !empty($n_user['_id']))
				{
					$db_community = PHDB::findOneById($_POST['memberOf']['type'], $_POST['memberOf']['id']);
					$db_community_link = $db_community['links'] ?? [];
					$db_community_link['members'] = $db_community_link['members'] ?? [];
					$db_community_link['members'][(string) $n_user['_id']] = $db_community_link['members'][(string) $n_user['_id']] ?? ['type' => Person::COLLECTION];
					$db_user_link = $n_user['links'] ?? [];
					$db_user_link['memberOf'] = $db_user_link['memberOf'] ?? [];
					$db_user_link['memberOf'][$_POST['memberOf']['id']] = $db_user_link['memberOf'][$_POST['memberOf']['id']] ?? ['type'	=> $_POST['memberOf']['type']];
					if (!empty($_POST['roles']))
					{
						if (!empty($db_community_link['members'][(string) $n_user['_id']]['roles']))
						{
							foreach ($_POST['roles'] as $role) :
								UtilsHelper::push_array_if_not_exists($role, $db_community_link['members'][(string) $n_user['_id']]['roles']);
							endforeach;
						}
						else
							$db_community_link['members'][(string) $n_user['_id']]['roles'] = $_POST['roles'];
						if (!empty($db_user_link['memberOf'][$_POST['memberOf']['id']]['roles']))
						{
							foreach ($_POST['roles'] as $role) :
								UtilsHelper::push_array_if_not_exists($role, $db_user_link['memberOf'][$_POST['memberOf']['id']]['roles']);
							endforeach;
						}
						else
							$db_user_link['memberOf'][$_POST['memberOf']['id']]['roles'] = $_POST['roles'];
					}
					if (!empty($db_community_link))
						PHDB::update($_POST['memberOf']['type'], ['_id' => $db_community['_id']], [
							'$set'	=> [
								'links'	=> $db_community_link
							]
						]);
					if (!empty($db_user_link))
						PHDB::update(Person::COLLECTION, ['_id' => $n_user['_id']], ['$set' => [
							'links' => $db_user_link
						]]);
				}
				if (!empty($n_user['_id']))
					Person::saveUserSessionData($n_user);
				break;
		}
		return (Rest::json($response));
	}
}
