<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;

use ActStr;
use Event;
use Notification;
use Person;
use PHDB;
use Rest;

class JoinAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run()
	{
		$response	= [
			'success'	=> false,
			'data'		=> null
		];
		$required_post_list = ['event', 'user'];
		$got_post_list = array_filter(array_keys($_POST), fn($post) => (in_array($post, $required_post_list)));
		if (count($got_post_list) === count($required_post_list))
		{
			$db_author = PHDB::findOneById(Person::COLLECTION, $_POST['user'], ['name']);
			$notif_author = [
				'id'	=> $_POST['user'],
				'name'	=> $db_author['name']
			];
			$notif_target = [
				'id'	=> $_POST['event'],
				'type'	=> Event::COLLECTION
			];
			$notif_object = [
				'type'	=> Event::COLLECTION,
				'id'	=> $_POST['event']
			];
			Notification::constructNotification(ActStr::COEVENT_JOIN, $notif_author, $notif_target, $notif_object, Event::COLLECTION);
			$response['success'] = true;
			$response['data'] = 'JOIN';
		}
		return (Rest::json($response));
	}
}
