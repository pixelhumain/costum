<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;

use Citoyen;
use DateTime;
use DateTimeZone;
use Event;
use MongoDate;
use MongoId;
use Normalizer;
use Organization;
use PHDB;
use PixelHumain\PixelHumain\components\Action as CAction;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Project;
use Rest;
use Yii;

class EventAction extends CAction {

	public function run($request = '', $id = null, $type = Event::COLLECTION) {
		$output = [];
		switch ($request) {
			case 'actors':
				$output = $this->get_actors_of($id, $type);
				break;
			case "element_event":
				$args = [];
				if (!empty($_POST["id"]))
					$args["id"] = $_POST["id"];
				if (!empty($_POST["type"]))
					$args["type"] = $_POST["type"];
				$output = $this->get_events($args);
				break;
			case 'subevents':
				$output = $this->get_subevents($id, $type);
				break;
			case 'categories':
				$output = $this->get_categories($id, $type);
				break;
			case 'dates':
				$output = $this->get_subevent_dates($id, $type);
				break;
			case 'event':
				$output = $this->get_event($id, $type);
				break;
			case 'link_tl_to_event':
				$set = [];
				$keys = ['address', 'geo', 'geoPosition'];
				$db_address = PHDB::findOneById(Organization::COLLECTION, $_POST['tl'], $keys);
				foreach ($keys as $key) {
					if (!empty($db_address[$key]))
						$set[$key] = $db_address[$key];
				}
				PHDB::update(Event::COLLECTION, ['_id' => new MongoId($_POST['event'])], [
					'$set' => $set
				]);
				$output = $set;
				break;
			case 'load_context_tag':
				$tags = [];
				$where_sub = [
					"parent." . $_POST['event']   => ['$exists' => 1],
					'_id'               => ['$ne' => new \MongoId($_POST['event'])]
				];
				$db_subevents = PHDB::find(Event::COLLECTION, $where_sub, ['tags', 'parent']);
				$db_context = PHDB::findOneById(Event::COLLECTION, $_POST['event'], ['parent', 'parentId', 'parentType', 'tags']);
				foreach ($db_subevents as $db_subevent) {
					$db_tags = !empty($db_subevent['tags']) && is_array($db_subevent['tags']) ? $db_subevent['tags'] : [];
					foreach ($db_tags as $db_tag) {
						$search = $_POST['search'] ?? '';
						if (empty($search) || preg_match("/$search/i", $db_tag))
							UtilsHelper::push_array_if_not_exists($db_tag, $tags);
					}
				}
				while (!empty($db_context)) {
					$db_tags = !empty($db_context['tags']) && is_array($db_context['tags']) ? $db_context['tags'] : [];
					foreach ($db_tags as $db_tag) {
						$search = $_POST['search'] ?? '';
						if (empty($search) || preg_match("/$search/i", $db_tag))
							UtilsHelper::push_array_if_not_exists($db_tag, $tags);
					}
					$parent = [];
					if (!empty($db_context['parent'])) {
						$parent_key = array_keys($db_context['parent'])[0];
						$parent['type'] = $db_context['parent'][$parent_key]['type'];
						$parent['id'] = $parent_key;
					} else if (!empty($db_context['parentId']) && !empty($db_context['parentType'])) {
						$parent['type'] = $db_context['parentType'];
						$parent['id'] = $db_context['parentId'];
					}
					$db_context = null;
					if (!empty($parent))
						$db_context = PHDB::findOneById($parent['type'], $parent['id'], ['parent', 'parentId', 'parentType', 'tags']);
				}
				$output = UtilsHelper::array_sort_by($tags, function ($a, $b) {
					$a = preg_replace('/[\x{0300}-\x{036f}]/u', '', Normalizer::normalize(mb_strtolower($a, 'UTF-8'), Normalizer::FORM_D));
					$b = preg_replace('/[\x{0300}-\x{036f}]/u', '', Normalizer::normalize(mb_strtolower($b, 'UTF-8'), Normalizer::FORM_D));
					return (strcmp($a, $b) > 0);
				});;
				break;
		}
		return Rest::json($output);
	}

	private function get_subevent_dates($id, $type) {
		$where = ["parent.$id.type" => $type, '_id' => ['$ne' => new MongoId($id)]];
		if ($type != \Event::COLLECTION) {
			$elements = [\Organization::COLLECTION => [], \Project::COLLECTION => []];
			$db_links = \PHDB::findOneById($type, $id, ["links"]);
			$db_links = isset($db_links["links"]) && is_array($db_links["links"]) ? $db_links["links"] : [];
			foreach ($db_links as $link_type => $links) {
				if (!in_array($link_type, ["projects", "contributors", "memberOf", "members"]))
					continue;
				foreach ($links as $key => $link) {
					if (array_key_exists($link["type"], $elements))
						$elements[$link["type"]][] = $key;
				}
			}
			$where = [
				'$or' => [
					["parent.$id.type" => $type],
				]
			];
			$in = [];
			foreach ($elements as $group => $ids) {
				$db_events = \PHDB::findByIds($group, $ids, ["links.events"]);
				foreach ($db_events as $db_event) {
					$ids = $db_event["links"]["events"] ?? [];
					$in = array_merge($in, array_keys($ids));
				}
			}
			if (count($in) > 0)
				$where['$or'][] = ["_id" => ['$in' => array_map(fn($i) => new \MongoId($i), $in)]];
		}
		if (!empty($_POST['type']))
			$where['type'] = $_POST['type'];
		if (!empty($_POST['tags']))
			$where['tags'] = ['$in' => $_POST['tags']];
		if (!empty($_POST['regions']))
			$where['address.level3'] = ['$in' => array_map(fn($region) => $region['id'], $_POST['regions'])];
		$thisTime = new MongoDate(strtotime(date('Y-m-d')));
		$where['startDate'] = ['$gte' => $thisTime];
		$fromNow = [];
		$this->processOrderedDateSubEvents($where, $fromNow);
		$where['startDate'] = ['$lt' => $thisTime];
		$before = [];
		$this->processOrderedDateSubEvents($where, $before);
		$output = ['fromNow' => $fromNow, 'before' => $before];
		return $output;
	}

	private function processOrderedDateSubEvents(array $where, array &$indexed_outputs) {
		$db_subevents = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, ['startDate', 'timeZone']);
		foreach ($db_subevents as $db_subevent) {
			$date = $this->get_formated(['startDate'], $db_subevent, null, $db_subevent['timeZone'] ?? '');
			$datetime = new \DateTime($date);
			$key = $datetime->format('d.m.Y');
			if (!array_key_exists($key, $indexed_outputs)) {
				$indexed_outputs[$key] = [
					'date_group' => $key,
					'year'       => $datetime->format('Y'),
					'month'      => $datetime->format('F'),
					'date'       => $datetime->format('d'),
					'day'        => $datetime->format('l'),
				];
			}
		}
		$output = [];
		foreach ($indexed_outputs as $indexed_output) $output[] = $indexed_output;
		$indexed_outputs = $output;
	}

	private function get_actors_of($id, $type) {
		if (!UtilsHelper::is_valid_mongoid($id))
			return ([]);
		$output = [];
		$indexed_outputs = [];
		$link_types = $_POST['types'] ?? [];
		$fields = [
			'name'
		];
		$where = [
			'$or' => [['_id' => new MongoId($id)]]
		];
		if ($type != \Event::COLLECTION) {
			$elements = [\Organization::COLLECTION => [], \Project::COLLECTION => []];
			$db_links = \PHDB::findOneById($type, $id, ["links"]);
			$db_links = isset($db_links["links"]) && is_array($db_links["links"]) ? $db_links["links"] : [];
			foreach ($db_links as $link_type => $links) {
				if (!in_array($link_type, ["projects", "contributors", "memberOf", "members"]))
					continue;
				foreach ($links as $key => $link) {
					if (array_key_exists($link["type"], $elements))
						$elements[$link["type"]][] = $key;
				}
			}
			$in = [];
			foreach ($elements as $group => $ids) {
				$db_events = \PHDB::findByIds($group, $ids, ["links.events"]);
				foreach ($db_events as $db_event) {
					$ids = $db_event["links"]["events"] ?? [];
					$in = array_merge($in, array_keys($ids));
				}
			}
			if (count($in) > 0)
				$where['$or'][] = ["_id" => ['$in' => array_map(fn($i) => new \MongoId($i), $in)]];
		}
		foreach ($link_types as $link_type)
			$fields[] = $link_type;
		if (empty($_POST['parent_only']) || !filter_var($_POST['parent_only'], FILTER_VALIDATE_BOOLEAN))
			$where['$or'][] = ["parent.$id.type" => $type];
		$db_events = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, $fields);
		foreach ($db_events as $id_event => $db_event) {
			$actor_fields = [
				'name',
				'profilImageUrl'
			];
			$attendees = !empty($db_event['links']) && !empty($db_event['links']['attendees']) ? $db_event['links']['attendees'] : [];
			$organizers = !empty($db_event['links']) && !empty($db_event['links']['organizer']) ? $db_event['links']['organizer'] : [];
			$animators = !empty($db_event['animator']) ? $db_event['animator'] : [];
			$creators = !empty($db_event['links']) && !empty($db_event['links']['creator']) ? $db_event['links']['creator'] : [];

			$creator = $db_event['creator'] ?? [];
			if (!empty($creator))
				$creators = array_merge($creators, [$creator => ['type' => 'citoyens']]);
			if (!empty($db_event['organizer']))
				$organizers = array_merge($organizers, $db_event['organizer']);
			foreach ($organizers as $id => $content) {
				if (!array_key_exists($id, $indexed_outputs)) {
					$actor = PHDB::findOneById($content['type'], $id, $actor_fields);
					$indexed_outputs[$id] = [
						'id'    => $id,
						'name'  => $actor['name'] ?? "(Sans nom)",
						'type'  => $content['type'],
						'image' => $actor['profilImageUrl'] ?? Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_' . $content['type'] . '.png',
						'roles' => [
							$id_event => [
								'id'   => $id_event,
								'name' => $db_event['name'],
								'role' => 'organizer'
							]
						]
					];
				} else
					$indexed_outputs[$id]['roles'][$id_event] = [
						'id'	=> $id_event,
						'name'	=> $db_event['name'],
						'role' => 'organizer'
					];
			}
			foreach ($attendees as $id => $content) {
				if (!array_key_exists($id, $indexed_outputs)) {
					$actor = PHDB::findOneById($content['type'], $id, $actor_fields);
					if (!empty($actor)) {
						$indexed_outputs[$id] = [
							'id'    => $id,
							'name'  => $actor['name'],
							'type'  => $content['type'],
							'image' => $actor['profilImageUrl'] ?? Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_' . $content['type'] . '.png',
							'roles' => [
								$id_event => [
									'id'   => $id_event,
									'name' => $db_event['name'],
									'role' => !empty($content['isAdmin']) && $content['isAdmin'] ? 'speaker' : 'attendee'
								]
							]
						];
					}
				} else
					$indexed_outputs[$id]['roles'][$id_event] = [
						'id'    => $id_event,
						'name'  => $db_event['name'],
						'role'  => 'attendee'
					];
			}

			foreach ($creators as $id => $content) {
				if (!array_key_exists($id, $indexed_outputs)) {
					$actor = PHDB::findOneById($content['type'], $id, $actor_fields);
					if (!empty($actor)) {
						$indexed_outputs[$id] = [
							'id'    => $id,
							'name'  => $actor['name'],
							'type'  => $content['type'],
							'image' => $actor['profilImageUrl'] ?? Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_' . $content['type'] . '.png',
							'roles' => [
								$id_event => [
									'id'   => $id_event,
									'name' => $db_event['name'],
									'role' => $id_event === $id ? 'creator' : 'speaker'
								]
							]
						];
					}
				} else
					$indexed_outputs[$id]['roles'][$id_event] = [
						'id'    => $id_event,
						'name'  => $db_event['name'],
						'role'  => $id_event === $id ? 'creator' : 'speaker'
					];
			}
			foreach ($animators as $id => $content) {
				if (!array_key_exists($id, $indexed_outputs)) {
					$actor = PHDB::findOneById($content['type'], $id, $actor_fields);
					if (!empty($actor)) {
						$indexed_outputs[$id] = [
							'id'    => $id,
							'name'  => $actor['name'],
							'type'  => $content['type'],
							'image' => $actor['profilImageUrl'] ?? Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumb/default_' . $content['type'] . '.png',
							'roles' => [
								$id_event => [
									'id'   => $id_event,
									'name' => $db_event['name'],
									'role' => 'animator'
								]
							]
						];
					}
				} else
					$indexed_outputs[$id]['roles'][$id_event] = [
						'id' => $id_event,
						'name' => $db_event['name'],
						'role' => 'animator'
					];
			}


			if (!empty($db_event['organizerName'])) {
				$organizer = $db_event['organizerName'];
				if (!array_key_exists($organizer, $indexed_outputs)) {
					$indexed_outputs[$organizer] = [
						'id'    => $organizer,
						'name'  => $organizer,
						'type'  => 'organizations',
						'image' => Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumbnail-default.jpg',
						'roles' => [
							$id_event => [
								'id'   => $id_event,
								'name' => $organizer,
								'role' => 'organizer'
							]
						]
					];
				} else
					$indexed_outputs[$organizer]['roles'][$id_event] = [
						'id'	=> $id_event,
						'name'	=> $organizer,
						'role'	=> 'organizer'
					];
			}
		}

		// remove indexes
		foreach ($indexed_outputs as $indexed_output) $output[] = $indexed_output;
		return $output;
	}

	private function get_categories($id, $type) {
		$output = [];
		$categories = PHDB::distinct(Event::COLLECTION, 'type', ["parent.$id.type" => $type]);
		foreach ($categories as $category) $output[] = $category;
		return $output;
	}

	private function get_subevents($parent, $type) {
		if (!UtilsHelper::is_valid_mongoid($parent))
			return ([]);
		$output = [];
		$fields = [
			'description',
			'shortDescription',
			'endDate',
			'name',
			'startDate',
			'timeZone',
			'slug',
			'type',
			'links',
			'address',
			'addresses',
			'timeZone',
			'geo',
			'tags',
			'geoPosition',
			'profilRealBannerUrl',
			'organizerName',
			'profilImageUrl',
			'profilMarkerImageUrl',
			'url',
			'organizer',
			'email'
		];
		$where = [
			'_id' => ['$ne' => new MongoId($parent)],
			"parent.$parent.type" => $type,
			"name" => ['$exists' => 1],
			"type" => ['$exists' => 1],
			"slug" => ['$exists' => 1],
		];
		$is_from_today = !empty($_POST['fromToday']) && filter_var($_POST['fromToday'], FILTER_VALIDATE_BOOLEAN);
		$and_where = [];
		if (!empty($_POST['type']))
			$where['type'] = $_POST['type'];
		if (!empty($_POST['tags']))
			$where['tags'] = ['$in' => $_POST['tags']];
		if (!empty($_POST['regions']))
			$where['address.level3'] = ['$in' => array_map(fn($region) => $region['id'], $_POST['regions'])];
		if (!empty($_POST['date'])) {
			$dates = [];
			if (is_string($_POST['date']) && $_POST['date'] === 'begining') {
				$where['startDate'] = [
					'$gte' => new MongoDate(strtotime(date('Y-m-d')))
				];
				$first = PHDB::findAndFieldsAndSortAndLimitAndIndex(Event::COLLECTION, $where, [
					'startDate',
					'timeZone'
				], ['startDate' => 1], 1, 0);
				if (count($first) === 0) {
					unset($where['startDate']);
					$first = PHDB::findAndFieldsAndSortAndLimitAndIndex(Event::COLLECTION, $where, [
						'startDate',
						'timeZone'
					], ['startDate' => 1], 1, 0);
				}
				unset($where['startDate']);
				foreach ($first as $one) {
					$start_formated = $this->get_formated(['startDate'], $one, null, $db_event['timeZone'] ?? '');
					$dates['gt'] = new DateTime($start_formated, new DateTimeZone($_POST['timezone']));
					$dates['lt'] = new DateTime($start_formated, new DateTimeZone($_POST['timezone']));
					$dates['lt']->modify('+ 1 day');
					$and_where[] = ['startDate' => ['$gte' => new MongoDate($dates['gt']->getTimestamp())]];
					$and_where[] = ['startDate' => ['$lt' => new MongoDate($dates['lt']->getTimestamp())]];
				}
			} else if (is_array($_POST['date']) && !empty($_POST['date']["between"]["start"]) && !empty($_POST['date']["between"]["end"])) {
				$start = new \DateTime($_POST['date']["between"]["start"]);
				$end = new \DateTime($_POST['date']["between"]["end"]);
				$and_where[] = [
					'$or' => [
						['startDate' => ['$lte' => new MongoDate($start->getTimestamp())]],
						['$and' => [
							['startDate' => ['$gt' => new MongoDate($start->getTimestamp())]],
							['startDate' => ['$lt' => new MongoDate($end->getTimestamp())]],
						]],
					]
				];
				$and_where[] = [
					'$or' => [
						['endDate' => ['$gte' => new MongoDate($start->getTimestamp())]],
						['$and' => [
							['endDate' => ['$gt' => new MongoDate($start->getTimestamp())]],
							['endDate' => ['$lt' => new MongoDate($end->getTimestamp())]],
						]],
					]
				];
			} else {
				$dates['gt'] = new DateTime($_POST['date'], new DateTimeZone($_POST['timezone']));
				$dates['lt'] = new DateTime($_POST['date'], new DateTimeZone($_POST['timezone']));
				$dates['lt']->modify('+ 1 day');
				$and_where[] = ['startDate' => ['$gte' => new MongoDate($dates['gt']->getTimestamp())]];
				$and_where[] = ['startDate' => ['$lt' => new MongoDate($dates['lt']->getTimestamp())]];
			}
		}
		if ($is_from_today)
			$and_where[] = ['startDate' => ['$gte' => new MongoDate(strtotime(date('Y-m-d')))]];
		if (!empty($and_where))
			$where['$and'] = $and_where;
		$db_subevents = PHDB::findAndSort(Event::COLLECTION, $where, ['startDate' => 1], 0, $fields);
		foreach ($db_subevents as $id => $db_subevent) {
			$subevent = [
				'id'                => $id,
				'name'              => $db_subevent['name'],
				'slug'              => $db_subevent['slug'],
				'start_date'        => $this->get_formated(['startDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
				'end_date'          => $this->get_formated(['endDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
				'full_description'  => $db_subevent['description'] ?? '',
				'short_description' => $db_subevent['shortDescription'] ?? '',
				'image'             => $db_subevent['shortDescription'] ?? '',
				'profile'           => $db_subevent['profilImageUrl'] ?? '',
				'banner'            => $db_subevent['profilRealBannerUrl'] ?? '',
				'marker'            => $db_subevent['profilMarkerImageUrl'] ?? '',
				'collection'		=> Event::COLLECTION,
				'tags'				=> !empty($db_subevent['tags']) && is_array($db_subevent['tags']) ? $db_subevent['tags'] : [],
				'communities'       => [],
				'links'             => [],
				'url'               => $db_subevent['url'] ?? '',
				'organizer'			=> $db_subevent['organizer'] ?? [],
				'type'				=> $db_subevent['type'] ?? '',
				'addresses'			=> $db_subevent['addresses'] ?? [],
				'email'				=> 	$db_subevent['email'] ?? "",
				'address'			=> $db_subevent['address'] ?? [],
				'timeZone'			=> $db_subevent['timeZone'] ?? "",

			];

			$keys = ['address', 'geo', 'geoPosition'];
			foreach ($keys as $key) {
				if (!empty($db_subevent[$key]))
					$subevent[$key] = $db_subevent[$key];
			}
			if (!empty($db_subevent['links'])) {
				$attendees = $db_subevent['links']['attendees'] ?? [];
				$organizers = $db_subevent['links']['organizers'] ?? [];

				foreach (array_keys($attendees) as $attendee) {
					if (empty($attendees[$attendee]['type']))
						continue;
					$subevent['links']['attendees'][$attendee] = $attendees[$attendee]['type'];
					if (!in_array($attendee, $subevent['communities'])) $subevent['communities'][] = $attendee;
				}
				foreach (array_keys($organizers) as $organizer) {
					if (empty($organizers[$organizer]['type']))
						continue;
					$subevent['links']['organizers'][$organizer] = $organizers[$organizer]['type'];
					if (!in_array($organizer, $subevent['communities'])) $subevent['communities'][] = $organizer;
				}
			}
			$output[$id] = $subevent;
		}
		if ($type != \Event::COLLECTION) {
			$elements = [\Organization::COLLECTION => [], \Project::COLLECTION => []];
			$db_links = \PHDB::findOneById($type, $parent, ["links"]);
			$db_links = isset($db_links["links"]) && is_array($db_links["links"]) ? $db_links["links"] : [];
			foreach ($db_links as $link_type => $links) {
				if (!in_array($link_type, ["projects", "contributors", "memberOf", "members"]))
					continue;
				foreach ($links as $key => $link) {
					if (array_key_exists($link["type"], $elements))
						$elements[$link["type"]][] = $key;
				}
			}
			foreach ($elements as $group => $ids) {
				$db_events = \PHDB::findByIds($group, $ids, ["links.events"]);
				foreach ($db_events as $child) {
					$child = $child["links"]["events"] ?? [];
					$child = array_keys($child);
					$this->get_element_events($child, $output);
				}
			}
		}
		$output = array_values($output);
		return $output;
	}

	private function get_element_events($ids, &$output) {
		$fields = [
			'description',
			'shortDescription',
			'endDate',
			'name',
			'startDate',
			'timeZone',
			'slug',
			'type',
			'links',
			'address',
			'addresses',
			'timeZone',
			'geo',
			'tags',
			'geoPosition',
			'profilRealBannerUrl',
			'organizerName',
			'profilImageUrl',
			'profilMarkerImageUrl',
			'url',
			'organizer',
			'email'
		];
		$output_ids = array_keys($output);
		$ids = array_values(array_filter($ids, fn($i) => !in_array($i, $output_ids)));
		$db_where = [
			"name" => ['$exists' => 1],
			"type" => ['$exists' => 1],
			"slug" => ['$exists' => 1],
		];
		if (!is_array($ids) || count($ids) === 0)
			return [];
		$db_where = array_merge($db_where, ["_id" => ['$in' => array_map(fn($i) => new \MongoId($i), $ids)]]);
		$is_from_today = !empty($_POST['fromToday']) && filter_var($_POST['fromToday'], FILTER_VALIDATE_BOOLEAN);
		$and_where = [];
		if (!empty($_POST['type']))
			$db_where['type'] = $_POST['type'];
		if (!empty($_POST['tags']))
			$db_where['tags'] = ['$in' => $_POST['tags']];
		if (!empty($_POST['regions']))
			$db_where['address.level3'] = ['$in' => array_map(fn($region) => $region['id'], $_POST['regions'])];
		if (!empty($_POST['date'])) {
			$dates = [];
			if ($_POST['date'] === 'begining') {
				$db_where['startDate'] = [
					'$gte' => new MongoDate(strtotime(date('Y-m-d')))
				];
				$first = PHDB::findAndFieldsAndSortAndLimitAndIndex(Event::COLLECTION, $db_where, [
					'startDate',
					'timeZone'
				], ['startDate' => 1], 1, 0);
				if (count($first) === 0) {
					unset($db_where['startDate']);
					$first = PHDB::findAndFieldsAndSortAndLimitAndIndex(Event::COLLECTION, $db_where, [
						'startDate',
						'timeZone'
					], ['startDate' => 1], 1, 0);
				}
				unset($db_where['startDate']);
				foreach ($first as $one) {
					$start_formated = $this->get_formated(['startDate'], $one, null, $db_event['timeZone'] ?? '');
					$dates['gt'] = new DateTime($start_formated, new DateTimeZone($_POST['timezone']));
					$dates['lt'] = new DateTime($start_formated, new DateTimeZone($_POST['timezone']));
					$dates['lt']->modify('+ 1 day');
					$and_where[] = ['startDate' => ['$gte' => new MongoDate($dates['gt']->getTimestamp())]];
					$and_where[] = ['startDate' => ['$lt' => new MongoDate($dates['lt']->getTimestamp())]];
				}
			} else if (is_array($_POST['date']) && !empty($_POST['date']["between"]["start"]) && !empty($_POST['date']["between"]["end"])) {
				$start = new \DateTime($_POST['date']["between"]["start"]);
				$end = new \DateTime($_POST['date']["between"]["end"]);
				$and_where[] = [
					'$or' => [
						['startDate' => ['$lte' => new MongoDate($start->getTimestamp())]],
						['$and' => [
							['startDate' => ['$gt' => new MongoDate($start->getTimestamp())]],
							['startDate' => ['$lt' => new MongoDate($end->getTimestamp())]],
						]],
					]
				];
				$and_where[] = [
					'$or' => [
						['endDate' => ['$gte' => new MongoDate($start->getTimestamp())]],
						['$and' => [
							['endDate' => ['$gt' => new MongoDate($start->getTimestamp())]],
							['endDate' => ['$lt' => new MongoDate($end->getTimestamp())]],
						]],
					]
				];
			} else {
				$dates['gt'] = new DateTime($_POST['date'], new DateTimeZone($_POST['timezone']));
				$dates['lt'] = new DateTime($_POST['date'], new DateTimeZone($_POST['timezone']));
				$dates['lt']->modify('+ 1 day');
				$and_where[] = ['startDate' => ['$gte' => new MongoDate($dates['gt']->getTimestamp())]];
				$and_where[] = ['startDate' => ['$lt' => new MongoDate($dates['lt']->getTimestamp())]];
			}
		}
		if ($is_from_today)
			$and_where[] = ['startDate' => ['$gte' => new MongoDate(strtotime(date('Y-m-d')))]];
		if (!empty($and_where))
			$db_where['$and'] = $and_where;
		$db_subevents = PHDB::findAndSort(Event::COLLECTION, $db_where, ['startDate' => 1], 0, $fields);
		foreach ($db_subevents as $id => $db_subevent) {
			$subevent = [
				'id'                => $id,
				'name'              => $db_subevent['name'],
				'slug'              => $db_subevent['slug'],
				'start_date'        => $this->get_formated(['startDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
				'end_date'          => $this->get_formated(['endDate'], $db_subevent, null, $db_subevent['timeZone'] ?? ''),
				'full_description'  => $db_subevent['description'] ?? '',
				'short_description' => $db_subevent['shortDescription'] ?? '',
				'image'             => $db_subevent['shortDescription'] ?? '',
				'profile'           => $db_subevent['profilImageUrl'] ?? '',
				'banner'            => $db_subevent['profilRealBannerUrl'] ?? '',
				'marker'            => $db_subevent['profilMarkerImageUrl'] ?? '',
				'collection'		=> Event::COLLECTION,
				'tags'				=> !empty($db_subevent['tags']) && is_array($db_subevent['tags']) ? $db_subevent['tags'] : [],
				'communities'       => [],
				'links'             => [],
				'url'               => $db_subevent['url'] ?? '',
				'organizer'			=> $db_subevent['organizer'] ?? [],
				'type'				=> $db_subevent['type'] ?? '',
				'addresses'			=> $db_subevent['addresses'] ?? [],
				'email'				=> 	$db_subevent['email'] ?? "",
				'address'			=> $db_subevent['address'] ?? [],
				'timeZone'			=> $db_subevent['timeZone'] ?? "",

			];

			$keys = ['address', 'geo', 'geoPosition'];
			foreach ($keys as $key) {
				if (!empty($db_subevent[$key]))
					$subevent[$key] = $db_subevent[$key];
			}
			if (!empty($db_subevent['links'])) {
				$attendees = $db_subevent['links']['attendees'] ?? [];
				$organizers = $db_subevent['links']['organizers'] ?? [];

				foreach (array_keys($attendees) as $attendee) {
					if (empty($attendees[$attendee]['type']))
						continue;
					$subevent['links']['attendees'][$attendee] = $attendees[$attendee]['type'];
					if (!in_array($attendee, $subevent['communities'])) $subevent['communities'][] = $attendee;
				}
				foreach (array_keys($organizers) as $organizer) {
					if (empty($organizers[$organizer]['type']))
						continue;
					$subevent['links']['organizers'][$organizer] = $organizers[$organizer]['type'];
					if (!in_array($organizer, $subevent['communities'])) $subevent['communities'][] = $organizer;
				}
			}
			$output[$id] = $subevent;
		}
		return $output;
	}

	private function get_event($id, $type) {
		if (!UtilsHelper::is_valid_mongoid($id))
			return ([]);
		$output = [];
		$fields = [
			'description',
			'shortDescription',
			'endDate',
			"created",
			'name',
			'startDate',
			'timeZone',
			'type',
			'links.organizer',
			'links.attendees',
			'profilRealBannerUrl',
			'address.streetAddress',
			'address.addressLocality',
			'address.postalCode',
			'organizerName',
			'slug'
		];
		$links = [
			Organization::COLLECTION => [],
			Project::COLLECTION      => [],
			Citoyen::COLLECTION      => [],
		];
		$db_parent = PHDB::findOneById($type, $id, $fields);
		$dates = [
			"start"	=> new DateTime(),
			"end"	=> new DateTime(),
		];
		if (!empty($db_parent["startDate"]))
			$dates["start"] = $db_parent["startDate"]->toDateTime();
		else
			$dates["start"]->setTimestamp($db_parent["created"] ?? 1325376000);
		if (!empty($db_parent["endDate"]))
			$dates["end"] = $db_parent["endDate"]->toDateTime();
		else
			$dates["end"]->modify("+1 months");
		$output['id'] = $id;
		$output['name'] = $db_parent['name'];
		$output['full_description'] = $db_parent['description'] ?? '';
		$output['short_description'] = $db_parent['shortDescription'] ?? '';
		$output['start_date'] = $dates["start"]->format("c");
		$output['end_date'] = $dates["end"]->format("c");
		$output['links'] = ['attendees' => [], 'organizers' => []];
		$output['communities'] = [];

		if (!empty($db_parent['address'])) {
			$address_grouped = [];
			if (!empty($db_parent['address']['streetAddress'])) $address_grouped[] = $db_parent['address']['streetAddress'];
			if (!empty($db_parent['address']['postalCode'])) $address_grouped[] = $db_parent['address']['postalCode'];
			if (!empty($db_parent['address']['addressLocality'])) $address_grouped[] = $db_parent['address']['addressLocality'];
			$output['address'] = implode(', ', $address_grouped);
		}

		$organizers = $db_parent['links']['organizer'] ?? [];
		$attendees = $db_parent['links']['attendees'] ?? [];

		foreach ($organizers as $id => $organizer) {
			if (array_key_exists($id, $links[$organizer['type']])) continue;
			$db_organizer = PHDB::findOneById($organizer['type'], $id, ['name', 'email', 'profilImageUrl']);
			if (!empty($db_organizer)) {
				$links[$organizer['type']][$id] = [
					'id'    => $id,
					'name'  => $db_organizer['name'],
					'email' => $db_organizer['email'] ?? '',
					'image' => $db_organizer['profilImageUrl'] ?? ''
				];
				$output['links']['organizers'][] = $links[$organizer['type']][$id];
				if (!in_array($id, $output['communities'])) $output['communities'][] = $id;
			}
		}

		if (!empty($db_parent['organizerName'])) $output['communities'][] = $db_parent['organizerName'];

		foreach ($attendees as $id => $attendee) {
			if (array_key_exists($id, $links[$attendee['type']])) continue;
			$db_attendee = PHDB::findOneById($attendee['type'], $id, ['name', 'email', 'profilImageUrl']);
			if (!empty($db_attendee)) {
				$links[$attendee['type']][$id] = [
					'id'    => $id,
					'name'  => $db_attendee['name'],
					'email' => $db_attendee['email'],
					'image' => $db_attendee['profilImageUrl'] ?? ''
				];
				$output['links']['attendees'][] = $links[$attendee['type']][$id];
				if (!in_array($id, $output['communities'])) $output['communities'][] = $id;
			}
		}
		return $output;
	}

	private function get_formated($fields, $array, $default = null, $timezone = '') {
		foreach ($fields as $field) {
			if (!empty($array[$field])) {
				$date = $array[$field];
				$exploded = explode('/', $date);
				$date = count($exploded) === 3 ? $exploded[2] . '-' . $exploded[1] . '-' . $exploded[0] : $date;
				$date = !is_integer($date) ? (is_string($date) ? strtotime($date) : (is_array($date) && array_key_exists('sec', $date) ? intval($date['sec']) : $date->sec)) : $date;
				$datetime = new DateTime();
				$datetime->setTimestamp($date);
				if (!empty($timezone)) $datetime->setTimezone(new DateTimeZone($timezone));
				if ($date > 0) return $datetime->format('c');
			}
		}
		$date = $default ?? date('Y-m-d');
		$date = count(explode('/', $date)) === 3 ? explode('/', $date) : $date;
		$date = is_array($date) ? $date[2] . '-' . $date[1] . '-' . $date[0] : $date;
		$date = !is_integer($date) ? (is_string($date) ? strtotime($date) : $date->sec) : $date;
		$datetime = new DateTime();
		$datetime->setTimestamp($date);
		if (!empty($timezone)) $datetime->setTimezone(new DateTimeZone($timezone));
		return $datetime->format('c');
	}

	private function get_events(array $args): array {
		if (count($args) !== 2)
			return ([]);
		$db_request = PHDB::findOneById($args["type"], $args["id"], ["links.events"]);
		$events  = !empty($db_request["links"]["events"]) && is_array($db_request["links"]["events"]) ? array_keys($db_request["links"]["events"]) : [];
		$where = [
			'$and' => [
				["startDate" => ['$exists' => 1]],
				[
					"\$or" => [
						["parentId" => $args["id"], "parentType" => $args["type"]],
						["_id" => ["\$in" => array_map(function ($id) {
							return (new MongoId($id));
						}, $events)]]
					]
				]
			]
		];
		return (PHDB::findAndSort(Event::COLLECTION, $where, ["startDate" => -1], 0, ["name", "startDate", "endDate"]));
	}
}
