<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\pdf;

use Pdf;
use cebe\markdown\Markdown;
use DateTimeZone;
use Event;
use MongoDate;
use MongoId;
use PHDB;
use Rest;
use Yii;

class GetPdfAction extends \PixelHumain\PixelHumain\components\Action {
	public function run() {
		$request_body = json_decode(file_get_contents('php://input'), 1);
		$controller = $this->getController();
		$parser = new Markdown();
		$parser->html5 = true;
		$parent_id = $_GET['id'];
		$event_field = [
			'name',
			'shortDescription',
			'description',
			'profilMediumImageUrl',
			'profilRealBannerUrl'
		];
		$db_parent = PHDB::findOneById($_GET["type"], $parent_id, $event_field);
		$view = 'costum.views.custom.coevent.pdf';
		$subevents = [];
		$render_data = [
			'context'	=> [
				'name'				=> $db_parent['name'],
				'url'				=> urldecode($request_body['url']),
				'logo'				=> $db_parent['profilMediumImageUrl'] ?? '',
				'shortDescription'	=> !empty($db_parent['shortDescription']) ? $db_parent['shortDescription'] : '',
				'description'		=> !empty($db_parent['description']) ? $parser->parse($db_parent['description']) : '',
				'banner'			=> $db_parent['profilRealBannerUrl'] ?? ''
			]
		];
		// subevents
		$sub_where = [
			"parent.$parent_id" => ['$exists' => 1],
			'_id' => ['$ne' => new MongoId($parent_id)]
		];
		if ($_GET["type"] !== \Event::COLLECTION) {
			$elements = [\Organization::COLLECTION, \Project::COLLECTION];
			$in = [];
			foreach ($elements as $element) {
				$db_events = \PHDB::find($element, ["parent.$parent_id.type" => $_GET["type"]], ["links.events"]);
				foreach ($db_events as $db_event) {
					$ids = $db_event["links"]["events"] ?? [];
					$in = array_merge($in, array_keys($ids));
				}
			}
			if (count($in) > 0) {
				$sub_where = [
					'$or' => [
						$sub_where,
						["_id" => ['$in' => array_map(fn($i) => new \MongoId($i), $in)]]
					]
				];
			}
		}
		$sub_where["name"] = ['$exists' => 1];
		if (!empty($request_body['regions']))
			$sub_where['address.level3'] = ['$in' => $request_body['regions']];
		if (!empty($request_body['tags']))
			$sub_where['tags'] = ['$all' => $request_body['tags']];
		if (!empty($request_body['type']))
			$sub_where['type'] = $request_body['type'];
		if (!empty($_GET['startDate']))
			$sub_where['startDate'] = ['$gte' => new MongoDate(strtotime($_GET['startDate']))];
		$sub_fields = [
			'name',
			"shortDescription",
			"description",
			"startDate",
			"endDate",
			'profilRealBannerUrl',
			'timeZone',
			'profilMediumImageUrl',
			"address"
		];
		$db_subevents = PHDB::findAndSort(Event::COLLECTION, $sub_where, ['startDate' => 1], 0, $sub_fields);
		foreach ($db_subevents as $db_subevent) {
			$subevent = [
				'name'				=> $db_subevent['name'],
				'shortDescription'	=> $db_subevent['shortDescription'] ?? '',
				'fullDescription'	=> !empty($db_subevent['description']) ? $parser->parse($db_subevent['description']) : '',
				'startDate'			=> $db_subevent['startDate']->toDateTime(),
				'endDate'			=> $db_subevent['endDate']->toDateTime(),
				'background'		=> $db_subevent['profilRealBannerUrl'] ?? '',
				'logo'				=> $db_subevent['profilMediumImageUrl'] ?? '',
				"address"			=> "",
				"marker"			=> Yii::app()->getModule("co2")->getAssetsUrl() . "/images/sig/markers/MARKER.png"
			];
			if (!empty($db_subevent["tiemZone"])) {
				$subevent["startDate"]->setTimeZone(new DateTimeZone($db_subevent["tiemZone"]));
				$subevent["endDate"]->setTimeZone(new DateTimeZone($db_subevent["tiemZone"]));
			}
			$address_field = ['streetAddress', 'postalCode', 'addressLocality', 'addressCountry'];
			foreach ($address_field as $field) {
				if (!empty($db_subevent['address'][$field]))
					$subevent['address'] .= $db_subevent['address'][$field] . ' ';
			}
			$subevents[] = $subevent;
		}
		$render_data['subevents'] = $subevents;
		if (!empty($db_parent["costum"]["pdf"]["events"]))
			$view = $db_parent["costum"]["pdf"]["events"];
		$html = $controller->renderPartial($view, $render_data, true);
		$params = [
			'header'					=> false,
			'footer'					=> false,
			'textShadow'				=> false,
			'docName'					=> $db_parent['name'] . ' coevent.pdf',
			'html'						=> $html,
			'ReducedMarginParagraph'	=> true,
			'saveOption'				=> 'D'
		];
		Pdf::createPdf($params);
	}
}
