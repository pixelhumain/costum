<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere;

use PHDB;
use Yii;
use Rest;

class PopulateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $elementType = $_POST['elementType'];
        $elementSlug = $_POST['elementSlug'];

        $values = [
            "source"=>$_POST['source']
        ];

        PHDB::update($elementType, 
                        array('slug' => $elementSlug), 
                        array('$set' => $values));

        return Rest::json(array('response' => true));
    }
}