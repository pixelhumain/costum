<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\filiere;
use Organization;
use PHDB;
use Slug;
use Yii;
use Rest;

class GenerateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        // cms blocks (block à améliorer "article.actualiteTimeLine")
        $defaultBlock = ["header.header","annuaire.annuaireDropdown", "text.textWithButton", "textImg.imgLeftWithBtnRight" , "elements.standardElement", "footer.footerWithCollabo"];
        //$defaultBlock = ["header.headerEducation","text.textColumnsEducation", "map.basicmaps", "elements.standardElement", "elements.standardElement"];

        foreach($defaultBlock as $key => $value){
            $arrayInsert = array(
                "name" => "actualite",
                "path" => "tpls.blockCms.".$value,
                "page" => "welcome",
                "type" => "blockCopy",
                "parent" => [
                    (string)$_POST['map']['_id']['$id'] => [
                        "type" => $_POST['map']['collection'],
                        "name" => isset($_POST['map']['name'])?$_POST['map']['name']:$_POST['slug']
                    ]
                ],
                "haveTpl" => "false",
                "creator" => $_POST['map']['creator'],
                "position" => $key,
                "paddingBottom" => "0",
                "paddingLeft" => "0",
                "paddingRight" => "0",
                "paddingTop" => "0"
            );

            if($key==0){
                $mybaseUrl = Yii::app()->baseUrl;
                $myAssetsUrl = Yii::app()->getModule("costum")->assetsUrl;

                $arrayInsert["logo"] = $myAssetsUrl.'/images/filiereGenerique/'.$_POST["thematic"].'.png';

                $arrayInsert["titleBlock"] = $_POST['map']['name'];

                $arrayInsert["blockCmsColorTitle1"] = "white";
                $arrayInsert["blockCmsColorTitle2"] = "white";
                $arrayInsert["blockCmsTextSizeTitle1"] = 90;

            }

            Yii::app()->mongodb->selectCollection("cms")->insert($arrayInsert);
            
        }

        /*
        $formArray = array(
            "name" => "Formulaire membre",
            "what" => "Membre",
            "parent" => [
                (string)$_POST['map']['_id'] => [
                    "type" => $_POST['map']['collection'],
                    "name" => $_POST['map']['name']
                ]
            ],
            "creator" => $_POST['map']['creator'],
            "subForms" => [ 
                "FormActeur2012021_1031_1", 
                "FormActeur2012021_1031_2", 
                "FormActeur2012021_1032_3", 
                "FormActeur2012021_1032_4"
            ],
            "description" => "",
            "active" => "true",
            "private" => "false",
            "canReadOtherAnswers" => "true",
            "startDate" => "",
            "endDate" => "31/12/2025",
            "anyOnewithLinkCanAnswer" => "true",
            "oneAnswerPerPers" => "true",
            "canModify" => "true",
            "showAnswers" => "true"
        );
        Yii::app()->mongodb->selectCollection("forms")->insert($formArray);*/
        $menuLeft = [];
        $menuTopRight = [];
        $searchBar = [];
        $slugFiliere = "cocity";
        $urlExtraObs =  "../co2/app/view/page/observatory/url/costum.views.custom.filiereGenerique.observatory";
        $hashObs = "#";
        $filterObj = "";
        if (isset($_POST['cocity']) && $_POST['cocity'] == "") {
            $menuLeft = [
                "buttonList" => [
                    "app" => [
                        "label" => false,
                        "icon" => true,
                        "spanTooltip" => false,
                        "buttonList" => [
                            "#observatory" => true,
                            "#search" => true,
                            "#projets" => true,
                            "#live" => true,
                            "#agenda" => true
                        ]
                    ],
                    "add" => [
                        "construct" => "createElement",
                        "label" => false,
                        "icon" => "plus-circle",
                        "id" => "show-bottom-add",
                        "class" => "show-bottom-add bg-green text-white",
                        "spanTooltip" => "Add something"
                    ]
                ],
                "addClass" => "bg-default align-middle"
            ];
            $slugFiliere = "filiereGenerique";
            $menuTopRight = [
                "buttonList" => [
                    "app" => [
                        "label" => false,
                        "icon" => true,
                        "spanTooltip" => false,
                        "buttonList" => [
                            "#dashboard" => true,
                            "#search" => true,
                        ]
                    ],
                    "networkFloop" => true,
                    "notifications" => true,
                    "chat" => true,
                    "login" => true,
                    "register" => true,
                    "userProfil" => [
                        "name" => true,
                        "img" => true
                    ],
                    "dropdown" => [
                        "buttonList" => [
                            "admin" => true,
                            "logout" => true
                        ]
                    ]
                ]
            ];
            $searchBar = [
                "construct" => "searchBar",
                "dropdownResult" => false,
                "class" => "pull-left margin-top-5 margin-left-15 hidden-xs"
            ];
            $filterObj = "costum.views.custom.filiereGenerique.filters";
            $urlExtraObs = "/page/observatory/url/costum.views.custom.filiereGenerique.observatory";
            $hashObs = "#app.view";
        }
        $htmlConstruct = array(
            "header" => [
                "menuTop" => [
                    "left" => [
                        "buttonList" => [
                            "logo" => [
                                "height" => "70"
                            ]
                        ]
                    ],
                    "right" => $menuTopRight
                ]
            ]
        );
        if (count($searchBar) != 0) {
            $htmlConstruct["header"]["menuTop"]["left"]["buttonList"]["searchBar"] =$searchBar;
        }
        if (count($menuLeft) != 0) {
            $htmlConstruct["menuLeft"]=$menuLeft;
        }

        // default typeObj
        $typeObj = array(
            "organization"=>[
                "color" => "azure",
                "name" => "Organization",
                "createLabel" => "Orga",
                "icon" => "users",
                "add" => true
            ],
            "projects" => [
                "color" => "blue",
                "name" => "Project",
                "createLabel" => "Projet",
                "icon" => "lightbulb-o",
                "add" => true
            ],
            "events" => [
                "color" => "turq",
                "name" => "Events",
                "createLabel" => "Evénement",
                "icon" => "calendar-o",
                "add" => true
            ]);

        // Default app
        $app = array("#observatory" => [
                "isTemplate" => true,
                "staticPage" => true,
                "hash" => $hashObs,
                "icon" => "tachometer",
                "urlExtra" => $urlExtraObs,
                "subdomainName" => "observatory"
            ],
            "#search" => [
                "useFilter" => true,
                "hash" => "#app.search",
                "icon" => "search",
                "urlExtra" => "/page/search",
                "subdomainName" => "SEARCH",
                "results" => [
                    "renderView" => "directory.elementPanelHtml"
                ],
                "filters" => [
                    "types" => [ 
                        "events", 
                        "organizations", 
                        "poi", 
                        "projects", 
                        "classifieds"
                    ]
                ],
                "results"  => [
                    "smartGrid"  => "",
                    "renderView"  => "directory.elementPanelHtml"
                ]
            ],
            "#projects" => [
                "hash" => "#app.search",
                "subdomainName" => "Projets",
                "icon" => "lightbulb-o",
                "urlExtra" => "/page/projects",
                "useFilter" => true,
                "searchObject" => [
                    "indexStep" => "0"
                ],
                "filters" => [
                    "types" => [ 
                        "projects"
                    ]
                ]
            ],
            "#live" => [
                "useFilter" => true,
                "subdomainName" => "LIVE",
                "icon" => "newspaper-o"
            ],
            "#agenda" => [
                "useFilter" => true,
                "hash" => "#app.agenda",
                "icon" => "calendar",
                "urlExtra" => "/page/agenda",
                "subdomainName" => "agenda"
            ]);


        if ($filterObj != "") {
            $app["#search"]["filterObj"] = $filterObj;
            $app["#projects"]["filterObj"] = $filterObj;
        }

        $values = [
            "cocity"=>$_POST['cocity'], 
            "ville"=>$_POST['ville'], 
            "thematic"=>$_POST['thematic'],
            "source"=>$_POST['source'],
            "costum"=>[
                "slug"=>$slugFiliere,
                "htmlConstruct"=>$htmlConstruct, 
                "app"=>$app, 
                "typeObj"=>$typeObj
            ]
        ];

        /**if(isset($_POST['map']['geo']) && isset($_POST['map']['geoPosition']) && isset($_POST['map']['address'])){
            $values["geo"] = $_POST['map']['geo'];
            $values["geoPosition"] = $_POST['map']['geoPosition'];
            $values["address"] = $_POST['map']['address'];
        }*/
 
        // base config costum
        if (isset($_POST["afterSave"]))
            PHDB::update($_POST['map']["collection"], 
                array('slug' =>$_POST["afterSave"]["organization"]["slug"]), 
                array('$set' => $values));
        else {
            PHDB::update($_POST['map']['collection'], 
                array('slug' => $_POST['map']["slug"]), 
                array('$set' => $values));

            return Rest::json(array('url' => Yii::app()->baseUrl.'/costum/co/index/slug/'.isset($_POST['map']['slug'])?$_POST['map']['slug']:$_POST['slug']));
        }
    }
}
