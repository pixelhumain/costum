<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu;
use MongoId;
use PHDB;

class HomeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
		  $controller = $this->getController();
    	$params=array();
    	$el= PHDB::findOne($type,
                array("_id" => new MongoId($id)));
      $params["element"] = $el;
    	return $controller->renderPartial("costum.views.custom.institutBleu.ficheActeur",$params,true);
    }
}