<?php 
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu;
use  Yii, CTKException, Rest, InstitutBleu;

class DeleteWithSourceAction extends \PixelHumain\PixelHumain\components\Action
{

    public function run(){
        $controller = $this->getController(); 
        $params = array();
        $params=$_POST;

        $res = array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!"));
        try {
            InstitutBleu::removeSourceFromElement($params);
            $res = array("result"=>true);
        } catch (CTKException $e) {
            $res = array("result"=>false, "msg"=>$e->getMessage());
        }
        return Rest::json($res);
    }

}