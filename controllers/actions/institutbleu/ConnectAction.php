<?php
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu;
    use Rest,PHDB,MongoId,InstitutBleu;
    

    class ConnectAction extends \PixelHumain\PixelHumain\components\Action{
        public function run() {
            $res = InstitutBleu::connect($_POST);
            return Rest::json($res);
        }
    }