<?php
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu;
    use Yii,Pdf,Element,PHDB,MongoId;
    

    class FicheActeurAction extends \PixelHumain\PixelHumain\components\Action{
        public function run($id = null, $tags = null, $type=null) {
            $controller = $this->getController();
            $tag = str_replace("_2"," ",$tags);
            $allTags = explode(",", $tag);
            $where = [];
            $where["_id"] = new MongoId($id);
           
            $element = PHDB::findOne($type,$where);
            $params=[         
                "element" => $element,
                "docName" => "Fiche acteur ".$element["name"].".pdf",
                "textShadow" => false,
                "header" => false,
                "footer" => false,
                "saveOption" => "D"
            ];
            $server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
            $params["html"] = $controller->renderPartial("costum.views.custom.institutBleu.fichePdf", $params, true);
            $res=Pdf::createPdf($params);
        }
    }