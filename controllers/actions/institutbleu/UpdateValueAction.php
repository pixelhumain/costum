<?php
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\institutbleu;
    use Yii,Rest,PHDB,MongoId;
    

    class UpdateValueAction extends \PixelHumain\PixelHumain\components\Action{
        public function run() {
            $params=[
                "id" => $_POST["id"],
                "collection" => $_POST["collection"],
                "path" => $_POST["path"],
                "value" => $_POST["value"]
            ];

            $res = PHDB::update(
                $params["collection"], 
                array("_id" => new MongoId($params["id"])),
                array($params["path"] => $params["value"]));
            
            return Rest::json($res);
        }
    }