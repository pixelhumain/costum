<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711;


use Authorisation;
use Element;
use MongoId;
use Person;
use PHDB;
use Rest;
use Yii;

class ReferencesAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $costumUserArray["admins"]= Element::getCommunityByTypeAndId($controller->costum["contextType"], $controller->costum["contextId"], Person::COLLECTION,"isAdmin");
        if(isset($_POST["elementId"]) && isset($_POST["elementType"])){
            $elt = PHDB::findOneById($_POST["elementType"], $_POST["elementId"]);
            if(isset($elt["reference"]["costum"])){
                array_push($elt["reference"]["costum"], $controller->costum["contextSlug"]);
            }else{
                $elt["reference"]["costum"] = [$controller->costum["contextSlug"]];
            }
            if(isset($costumUserArray["admins"][Yii::app()->session["userId"]])){
                $ekitia = PHDB::findOne($controller->costum["contextType"], array("slug" => $controller->costum["contextSlug"]));
                $elt["links"]["memberOf"][(string)$controller->costum["contextId"]] = array("type"=>$controller->costum["contextType"]);
                $ekitia["links"]["members"][$_POST["elementId"]] = array("type" => $_POST["elementType"]);
                $query = array("links.memberOf" => $elt["links"]["memberOf"], "reference" => $elt["reference"]);
                $queryEkitia = array("links.members" => $ekitia["links"]["members"]);
            }else{
                $elt["preferences"]["toBeValidated"][$controller->costum["slug"]] = true;
                $query=array("preferences" => $elt["preferences"], "reference" => $elt["reference"]);
            }

            $res = PHDB::update( $_POST["elementType"],
                array("_id" => new MongoId($_POST["elementId"])),
                array('$set'=>$query));
            if(isset($queryEkitia)){
                $resEktia = PHDB::update( $controller->costum["contextType"],
                    array("_id" =>  new MongoId($controller->costum["contextId"])),
                    array('$set'=>$queryEkitia));
            }
            return Rest::json(array(
                'result' => true,
                'elt' => $elt
            ));
        }
    }
}