<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\essconnect9711;

use MongoId;
use Organization;
use SearchNew;
use Yii, PHDB, Person, Rest, Link;
use Form;

class GetElementAnswersAction  extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
        $controller=$this->getController();
        
        // $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
        $fields= array(
            "name", "collection", "slug", "fromActivityPub", "attributedTo", "objectId", "created", "updated", "profilThumbImageUrl",
            "profilImageUrl", "preferences", "profilMediumImageUrl", "shortDescription", "parent", "tags", "type", "section", "category", "startDate", "endDate", "openingHours",
            "address","addresses", "scope", "geo", "geoPosition", "links", "description", "isSpam", "userSpamDetector", "dateSpamDetector"
        );
        $res = SearchNew::querySearchAll($_POST, $fields, true);
        // var_dump($res);exit;
        $elts=$res["results"];
        $query=["context.".$controller->costum["contextId"]=>array('$exists'=>1)];
        
        foreach($elts as $id=>$elt){
            if(!isset($query['$or'])){
                $query['$or']=[];
            }
            array_push($query['$or'],array("links.organizations.".$id=>array('$exists'=>1)));
            $elts[$id]["answers"]=[];

        }
        $answers=PHDB::find(Form::ANSWER_COLLECTION,$query);
        $formsId=[];
        foreach($answers as $ida=>$answ){
            if(isset($answ["links"]["organizations"])){
                $idLinkedOrga=array_keys($answ["links"]["organizations"])[0];
                $elts[$idLinkedOrga]["answers"][$ida]=$answ;
                if(!in_array($answ["form"],$formsId)){
                    array_push($formsId,new mongoId($answ["form"]));
                }
    
            }
            
        }

        $res["results"]=$elts;

        $res["forms"]=PHDB::find(Form::COLLECTION,array("_id"=>array('$in'=>$formsId)));
        // var_dump($answers);exit;



        return Rest::json($res);
    }
}