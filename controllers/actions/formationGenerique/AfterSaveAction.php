<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique;
use PHDB;
use MongoId;
class AfterSaveAction extends  \PixelHumain\PixelHumain\components\Action
{
    
    public function run($type="formation") {
        if($type == "formation"){
            $idFormation = $_POST["data"]["_id"]['$id'];
            $typeFormation = $_POST["data"]["collection"];
            if(isset($_POST["data"]["parent"])){
                foreach($_POST["data"]["parent"] as $kp => $vParent){
                    $tagsUp = "";
                    if($vParent["type"] == "organizations"){
                        $tagsUp = "Centre de formation";
                    }
                    if($vParent["type"] == "citoyens"){
                        $tagsUp = "Formateur";
                    }
                    $parentElt = PHDB::findOne($vParent["type"],array("_id" =>new MongoId($kp)),array("tags","sources","reference"));
                    /* Mise à jour parent */
                    if($parentElt){
                        $set = [];
                        if(isset($parentElt["tags"])){
                            if(!in_array($tagsUp,$parentElt["tags"])){
                                $parentElt["tags"][] = $tagsUp;
                                $set["tags"] = $parentElt["tags"]; 
                            }
                        } 
                        if(isset($parentElt["sources"]["keys"]) && !in_array($_POST["costumSlug"],$parentElt["sources"]["keys"])){
                            if(isset($parentElt["reference"]["costum"]) && !in_array($_POST["costumSlug"],$parentElt["reference"]["costum"])){
                                $set["reference"]["costum"][] = $_POST["costumSlug"];
                            }
                            if(!isset($parentElt["reference"]["costum"])){
                                $set["reference"]["costum"] = [$_POST["costumSlug"]];
                            }
                        }
                        if(!isset($parentElt["sources"]["keys"])){
                            if(isset($parentElt["reference"]["costum"]) && !in_array($_POST["costumSlug"],$parentElt["reference"]["costum"])){
                                $set["reference"]["costum"][] = $_POST["costumSlug"];
                            }
                            if(!isset($parentElt["reference"]["costum"])){
                                $set["reference"]["costum"] = [$_POST["costumSlug"]];
                            }
                        }
                        $set["links.projects.".$idFormation.".roles"] = [$tagsUp];
                        if($set){
                            PHDB::update( $vParent["type"], 
                                array("_id" => new MongoId($kp)) , 
                                array('$set' => $set)
                            ); 
                        }
                        /* Mise à jour Formation */
                        PHDB::update( $typeFormation, 
                            array("_id" => new MongoId($idFormation)) , 
                            array('$set' => array("links.contributors.".$kp.".roles" => [$tagsUp]))
                        ); 
                    }
                }    
            }
        }else if($type == "organismeFormation"){
            // $idOF = $_POST["data"]["_id"]['$id'];
            // $typeOF = $_POST["data"]["collection"];
            // $of = PHDB::findOne($typeOF,array("_id" =>new MongoId($idOF)),array(tags,trainingQualification));
            // $set = [];
            // if(isset($of["tags"])){
            //     if(!in_array("Organisme de formation",$of["tags"])){
            //         $of["tags"][] = "Organisme de formation";
            //         $set["tags"] = $of["tags"]; 
            //     }
            // } 
            // if(isset($of["trainingQualification"])){
            //     /* Ajouter dans la tags */
            //     $trainingQualification = explode(",",$of["trainingQualification"]);
            //     foreach($trainingQualification as $k => $v){
            //         if(isset($of["tags"])){
            //             if(!in_array($v,$of["tags"])){
            //                 if(isset($set["tags"])){
            //                     array_push($set["tags"],$v);
            //                 }else{
            //                     $set["tags"] = $of["tags"];
            //                     $set["tags"][] = $v;
            //                 }
            //             }
            //         }else{
            //             if(isset($set["tags"])){
            //                 $set["tags"][] = $v;
            //             }else{
            //                 $set["tags"] = [];
            //                 $set["tags"][] = $v;
            //             }
            //         }

                    
            //     }
            // }
            // if($set){
            //     PHDB::update( $typeOF, 
            //         array("_id" => new MongoId($idOF)) , 
            //         array('$set' => $set)
            //     ); 
            // }
        }else if($type == "tiersLieux"){
            // $idTl = $_POST["data"]["_id"]['$id'];
            // $typeTl = $_POST["data"]["collection"];
            // $tl = PHDB::findOne($typeTl,array("_id" =>new MongoId($idTl)),array(tags));
            // $set = [];
            // if(isset($tl["tags"])){
            //     if(!in_array("Tiers lieux",$tl["tags"])){
            //         $tl["tags"][] = "Tiers lieux";
            //         $set["tags"] = $tl["tags"]; 
            //     }
            // } 
            // if($set){
            //     PHDB::update( $typeTl, 
            //         array("_id" => new MongoId($idTl)) , 
            //         array('$set' => $set)
            //     ); 
            // }
        }else if($type == "sessionFormation"){
            
        }
    }
}