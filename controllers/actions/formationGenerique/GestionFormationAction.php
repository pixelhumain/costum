<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique;

use CAction, Yii, HtmlHelper, PHDB, Rest;


class GestionFormationAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($category=null)
    {
        $controller=$this->getController();
        // $res=PHDB::find("projects",array("parent.5d36ad9340bb4ea9625e0915"=>array('$exists'=>1)));
        // return Rest::json($res);
        // var_dump($category);exit;
        return $controller->renderPartial("costum.views.custom.formationGenerique.gestionFormation",array(), true);


    }
} 

?>