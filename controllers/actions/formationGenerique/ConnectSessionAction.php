<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique;

use DataValidator, Yii, Person, Rest;
use Element;
use Event;
use Link;
use PHDB;
use Organization;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use Project;
use CacheHelper;
use Costum;
class ConnectSessionAction extends Action
{
	 /**
	 * TODO Clement : La PHPDOC
	 */
    public function run($ref=null) {
    	if($ref){
			
			return $this->handleInvitationByLink($ref);
		}else{
			if(DataValidator::missingParamsController($_POST, ["childType","parentId","parentType"]) )
    		return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));

			$result = array("result"=>false, "msg"=>Yii::t("common", "Incorrect request"));
			if ( ! Person::logguedAndAuthorized() ) {
				return array("result"=>false, "msg"=>Yii::t("common", "You are not loggued or do not have acces to this feature "));
			}
			
			$currentRoles=[];
			if($_POST["parentType"] == Organization::COLLECTION && !empty($_POST["childId"])){
				$currentRoles = Person::getPersonLinksByPersonId($_POST["childId"]);
				$currentRoles = isset($currentRoles[Organization::COLLECTION][$_POST['parentId']]["roles"]) ? $currentRoles[Organization::COLLECTION][$_POST['parentId']]["roles"] :[] ;
			}
			if($_POST["parentType"] == Project::COLLECTION && !empty($_POST["childId"])){
				$currentRoles = Person::getPersonLinksByPersonId($_POST["childId"]);
				$currentRoles = isset($currentRoles[Project::COLLECTION][$_POST['parentId']]["roles"]) ? $currentRoles[Project::COLLECTION][$_POST['parentId']]["roles"] :[] ; 
			}
			if($_POST["parentType"] == Event::COLLECTION && !empty($_POST["childId"])){
				$currentRoles = Person::getPersonLinksByPersonId($_POST["childId"]);
				$currentRoles = isset($currentRoles[Event::COLLECTION][$_POST['parentId']]["roles"]) ? $currentRoles[Event::COLLECTION][$_POST['parentId']]["roles"] :[] ; 
			}
				
			$roles =  @$_POST["roles"];
			if(is_array($roles))
				$roles = array_merge($roles,$currentRoles);

			$child = array(
				"childId" => @$_POST["childId"],
				"childType" => $_POST["childType"],
				"childName" => @$_POST["childName"],
				"childEmail" => @$_POST["childEmail"]
			);
			$parentId = $_POST["parentId"];
			$parentType = $_POST["parentType"];
			$isConnectingAdmin = @$_POST["connectType"];
			
			if ($isConnectingAdmin=="admin"){
				$isConnectingAdmin=true;
			} else {
				$isConnectingAdmin=false;
			}

            $result=array("result"=>false);
			if(Costum::isSameFunction("connectElement")){
				// var_dump("here");
				$extraParams=(!empty($_GET))? $_GET : [];
				$costumRes = Costum::sameFunction("connectElement", array("type"=>$parentType,"id"=>$parentId,"child"=>$child,"extraParams"=>$extraParams, "roles"=>$roles));
				
				$parentData = Element::getElementSimpleById($parentId, $parentType);
				$result=array("result"=>true,"parent"=>$parentData,"parentType"=>$parentType);
				// var_dump($result);exit;
			}else{
				// var_dump("else");
				$result = Link::connectParentToChild($parentId, $parentType, $child, $isConnectingAdmin, Yii::app()->session["userId"], $roles);
			}
			// exit;
			return Rest::json($result);
		}
    }

	private function handleInvitationByLink($ref){
		$controller = $this->getController();
		$baseUrl = Yii::app()->baseUrl;

		if(Yii::app()->session["userId"]){
			$res = Link::connectByUrl($ref);
			if($res["result"]){
				$invitationLink = PHDB::findOne(InvitationLink::COLLECTION, ["ref" => $ref]);
                unset($_COOKIE['invitationLinkRef']);
				setcookie('invitationLinkRef', null, -1, '/');

				$redirectURL = ["/#page.type.".$invitationLink["targetType"].".id.".$invitationLink["targetId"]];

				if(isset($invitationLink["urlRedirection"]) && trim($invitationLink["urlRedirection"]) != "") {
					$redirectURL = $invitationLink["urlRedirection"];
				} else if($invitationLink["targetType"] === Organization::COLLECTION){
					$element = PHDB::findOneById(Organization::COLLECTION, $invitationLink["targetId"]);
					if(isset($element["costum"]))
						$redirectURL = ["/costum/co/index/slug/".$element["slug"]];
				}

				$costum=CacheHelper::getCostum();
				$costum=(empty($costum) && isset($invitationLink["source"]["key"])) ? CacheHelper::getCostum($invitationLink["source"]["key"])  : false;
                if(!empty($costum) && isset($costum["class"]["function"])){
					if(in_array("connectElement",$costum["class"]["function"])){
						$slugContext = isset($costum["assetsSlug"]) ? $costum["assetsSlug"] : $costum["slug"];
						$slugContext = ucfirst($slugContext);
						if(class_exists($slugContext)){
							$extraParams=(!empty($_GET))? $_GET : [];
							$opt=(!empty($invitationLink["targetId"]) && !empty($invitationLink["targetType"])) ? array("source"=>@$invitationLink["source"],"extraParams"=>$extraParams,"id"=>$invitationLink["targetId"], "type"=>$invitationLink["targetType"], "roles"=>$invitationLink["roles"]) : [];
							$params = $slugContext::connectElement($opt);
						}
					}
                    if(in_array("handleInvitationByLink",$costum["class"]["function"])){
                        $slugContext = isset($invitationLink["source"]["key"]) ? $invitationLink["source"]["key"] : $costum["slug"];
                        $slugContext = ucfirst($slugContext);
                        if(class_exists($slugContext)){
                            $params = $slugContext::handleInvitationByLink(array("userId" => Yii::app()->session["userId"], "targetId" => $invitationLink["targetId"], "targetType" => $invitationLink["targetType"]));
                        }
                    }

                }
				// var_dump($redirectURL);exit;
				return $this->getController()->redirect($redirectURL);
			}else{
				return $this->getController()->redirect(['/']);
			}
		}else{
			$params = [
				"title" => "Lien d'invitation expirer ou invalide",
				"description" => "",
				"redirectUrl" => $baseUrl,
				"image" => $baseUrl."/themes/CO2/assets/img/1+1=3.jpg"
			];

			$extraUrlParams="?".$_SERVER['QUERY_STRING'];
			
			$invitationLink = PHDB::findOne(InvitationLink::COLLECTION, ["ref" => $ref]);
			if($invitationLink){
				$element = Element::getByTypeAndId($invitationLink["targetType"], $invitationLink["targetId"]);
				$params["title"] = Yii::t("invite", "Click to join").": ".$element["name"];
				$params["description"] = isset($element["shortDescription"])?$element["shortDescription"]:"";
				$params["redirectUrl"] = $baseUrl."/#panel.box-login";

				if($invitationLink["targetType"] === Organization::COLLECTION){
					$element = PHDB::findOneById(Organization::COLLECTION, $invitationLink["targetId"]);
					if(isset($element["costum"]))
						$params["redirectUrl"] = $baseUrl."/costum/co/index/slug/".$element["slug"]."/#panel.box-login";
				}

				if(isset($element["profilThumbImageUrl"]) && $element["profilThumbImageUrl"] !== "")
					$params["image"] = $baseUrl.$element["profilThumbImageUrl"];

				$ONE_HOUR = 60*60;
				setcookie("invitationLinkRef", $ref.$extraUrlParams, time()+$ONE_HOUR , '/');
			}
			return $controller->renderPartial("invitation-link-meta", $params);
		}
	}

}