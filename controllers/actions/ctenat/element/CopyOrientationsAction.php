<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element;

use MongoId;
use Badge;
use PHDB;
use Project;
use Rest;
class CopyOrientationsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $type=null)
    {
    	$controller = $this->getController();
    	$params=array();
    	$el= PHDB::findOne($type, ["_id" => new MongoId($id)] );
        $params["el"]=$el;
        
        if( isset( $el["copyOf"] ) ){
            $params["copyOf"] = $el["copyOf"]["id"];
            
            $badges=Badge::getByWhere(array("parent.".$el["copyOf"]["id"] => array('$exists'=>true)));
            $params["badges"]=[];
            foreach($badges as $k => $v)
            {
                PHDB::update( Badge::COLLECTION, ["_id" => new MongoId($k) ], 
                    ['$set'=>["parent.".$id => ["type"=>Project::COLLECTION, "name"=>$el["name"] ] ] ] );       
                $params["badges"][] = "badge ".$k." updated";
            }
            $params["result"] = true;
        } else 
            $params["copyOf"] = null;
        return Rest::json($params);
    }
}
