<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\admin;
use CAction;
use Ctenat;
use Rest;

class SaveOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
		//Rest::json($_POST); exit;
        $params = Ctenat::saveOrga($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}
