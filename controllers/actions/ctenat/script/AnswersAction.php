<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\script;
use Answer;
use CAction;
use Form;
use PHDB;
use Project;
use Yii;

class AnswersAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null) {
		
		$controller=$this->getController();
		
		$answers = PHDB::find( Answer::COLLECTION, [
			"source.key"=>"ctenat",  
			"answers"=>['$exists' => 1]], ["formId","answers"]  );
		
		$form = PHDB::findOne( Form::COLLECTION , [ "id" => "ctenatForm" ], [ "subForms", "parent"]  );
		$cterArray = array();
		$nb = 0;

		//Rest::json(count($answers) ); exit;
		if(!empty($form) )
		{
			$formId = implode("|", $form["subForms"]);

			foreach ($answers as $key => $value) 
			{
				$newContext = $form["parent"];
				if(!empty($cterArray[$value["formId"]])){
					$cter = $cterArray[$value["formId"]];
				}else{
					$cter = PHDB::findOne(Project::COLLECTION, array( "slug" => $value["formId"] ), array("name"));
					$cterArray[$value["formId"]] = $cter;
				}

				$newContext[(string)$cter["_id"]] = array("type" => Project::COLLECTION, "name" => $cter["name"]);
				$action = [];
				if(isset($value["answers"][ $value["formId"] ]["answers"]["project"])){
					// $action["answers.action"] = [ 
					// 	"project" => [ "0"=>$value["answers"][ $value["formId"] ]["answers"]["project"]] ];

					$action["project"][] = $value["answers"][ $value["formId"] ] ["answers"]["project"];
				}

				if(isset($value["answers"][ $value["formId"] ]["answers"]["organization"] )){
					// $action["answers.action"]["parents"] = [ $value["answers"][ $value["formId"] ]["answers"]["organization"]];
					$action["parents"] = [ $value["answers"][ $value["formId"] ]["answers"]["organization"]];
					if(isset($value["answers"][ $value["formId"] ]["answers"]["organizations"]["orgasLinked"] )) {
						foreach ($value["answers"][ $value["formId"] ]["answers"]["organizations"]["orgasLinked"] as $i => $o) {
							$action["parents"][] = $o;
						}
					}
				}
				$answer = $value["answers"][ $value["formId"] ]["answers"];
				if( count( array_keys($action))>0)
					$answer["action"] = $action;
				
				$answers[$key] = [
					"answers" => $answer ,
					"cterSlug" => $value["formId"],
					"context" => $newContext,
					"formId" => $formId,
					"form" => (string)$form["_id"]
				];
				PHDB::update( Answer::COLLECTION, 
					[ "_id"=> $value["_id"] ], 
					['$set'=> $answers[$key] ]);
				$nb++;
			}

			//Rest::json(count($cterArray) ); exit;
			return $nb." answers update";

		}
		else {
			return "<h1 style='color:red'>YOU MUST INSERT CTENAT FORMS <br/>from data/forms/cte3Forms.json</h1>";
		}
		
		

		Yii::app()->end();
	}
}