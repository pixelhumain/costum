<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;

use CAction, PHDB, Project, Organization, MongoId, Yii, Notification, ActStr, Rest, Link;
class NotifinancerAction extends \PixelHumain\PixelHumain\components\Action{
	public function run() {
		
		$res = false;
		//launch action linking project to orga with role Financeur + orga to project
		// and createNotification to the financer
		$cter = PHDB::findOne(Project::COLLECTION, [ "slug" => $_POST["cter"] ], ["links","slug"] );
		//check financer is part of community and role Financer
		if( $cter && isset( $cter["links"]["contributors"][$_POST["financer"]] ) && in_array("Financeur", $cter["links"]["contributors"][$_POST["financer"]]["roles"]) )
		{
			$org = PHDB::findOne(Organization::COLLECTION, [ "_id" => new MongoId($_POST["financer"]) ], ["slug"] );
			if( $cter && $org ){
                Link::connect($_POST["project"], Project::COLLECTION, $_POST["financer"], Organization::COLLECTION, Yii::app()->session["userId"], "contributors",false,false,false,false, ["Financeur"]);
                            //links.projects rattaché le projet à l'orga porteuse 
                Link::connect($_POST["financer"], Organization::COLLECTION, $_POST["project"], Project::COLLECTION, Yii::app()->session["userId"], "projects",false,false,false, false);
		        
		        Notification::constructNotification( ActStr::VERB_ADD, 
		        		array( "id" => Yii::app()->session["userId"],
		        			   "name"=> Yii::app()->session["user"]["name"]), 
		        		array( "type"=>Project::COLLECTION,
		        			   "id"=> $_POST["project"]), 
		        		array( "id"=>$_POST["financer"],
		        			   "type"=> Organization::COLLECTION), Organization::COLLECTION);

				$res = true;
			}
		}	
        return Rest::json([ "result"=> $res ]);

	}
}