<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;

use CAction, PHDB, Poi, Answer;
class SelectindicateurAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null,$key=null,$formId =null)
    {
        
        $poilistPrinci = PHDB::findAndSort(Poi::COLLECTION, [
                        "type"=>"indicator",
                        'isIndicateurPrincipale'=>['$exists'=>1] ],["name"=>1],0,["name"] );
        $poilist = PHDB::findAndSort(Poi::COLLECTION, [
                        "type"=>"indicator",
                        'isIndicateurPrincipale'=>['$exists'=>0] ],["name"=>1],0,["name"] );

        $answer = PHDB::findOneById( Answer::COLLECTION, $id, ["answers.murir.results","answers.caracter"] );
        
        $poilistDA = [];

        if(isset($answer["answers"]["caracter"]["actionPrincipal"] ))
            $poilistDA = PHDB::findAndSort(Poi::COLLECTION, [
                        "type"=>"indicator",
                        "domainAction" => $answer["answers"]["caracter"]["actionPrincipal"] 
                    ],["name"=>1],0, ["name"] );

        $results = [];
        if(isset($answer["answers"]["murir"]["results"])){
            foreach ($answer["answers"]["murir"]["results"] as $k => $v ) {
                $results[ $v["indicateur"] ] = 1;
            }
        }
        

        //var_dump($results);exit;
        $params = [
            "results"=>$results,    
            "poilist"=>$poilist,
            "poilistDA"=>$poilistDA,
            "poilistPrinci"=>$poilistPrinci,
            "key" => $key ,
            "formId" => $formId
        ];
        return $this->getController()->renderPartial("survey.views.tpls.forms.costum.ctenat.selectIndic", $params);

        
    }
}
