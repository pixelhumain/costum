<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use Ctenat; 
use Rest;

class GetAnswerByContextAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($context,$project){
        $answer = Ctenat::getAnswerByContext($context,$project); 
        $res = []; 
        if($answer){
            $res["answerId"] = (String)$answer["_id"]; 
        }
        return Rest::json($res);
    }
}