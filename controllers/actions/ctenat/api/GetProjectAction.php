<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use CAction;
use Project;
use Rest;

class GetProjectAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id){
        $project = Project::getById($id);
        $res = [];
        if($project){
            /* if(isset($project["links"]))
                $res = Element::getAllLinks($project["links"], Project::COLLECTION, $id);
            else */
            $res[$id] = $project;
        }
        Rest::json(["results"=>$res]);
    }
}