<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf;

use CTKAction, PHDB, Badge, MongoId, Element, Form, Organization, Document, Pdf, Yii, Rest;
class GenerateOrientationAction extends CTKAction{
	public function run($id){
		// $id = "cte" ;
		// $user = "5ac4c5536ff9928b248b458a";
		// $session = "1";
		$controller=$this->getController();
		ini_set('max_execution_time',1000);

		$badge = PHDB::findOne( Badge::COLLECTION, array("_id"=>new MongoId($id)));
		if(isset($badge["parent"])){
			foreach($badge["parent"] as $k=>$v){
				$projetParent=Element::getElementSimpleById( $k, $v["type"],null, array("slug", "name"));
			}
		}
		$answerList=array();
		
		if(isset($badge["links"]) && isset($badge["links"]["projects"]) && !empty($badge["links"]["projects"]) && isset($projetParent) && !empty($projetParent)){


			foreach($badge["links"]["projects"] as $k=>$v){

				// $addEntry=array();
				//echo "answers.".$projetParent["slug"].".answers.project.id.".$k;

				// $ans=PHDB::findOne(Form::ANSWER_COLLECTION, array("answers.project.id"=>$k));

				// if(empty($ans) || $ans == null){
					$ans=PHDB::findOne(Form::ANSWER_COLLECTION, array("answers.action.project.0.id"=>$k));
				// }
				
				if(!empty($ans)){
					$ansfFormId = $ans["form"];
					$ans=$ans["answers"];
				}

				$elt=Element::getElementSimpleById( $k, $v["type"],null, array("slug", "name", "links"));
				if (!empty($ans)) {
					$frmprt=PHDB::findByIds(Form::COLLECTION, [$ansfFormId]);
					if(isset($frmprt[$ansfFormId]["params"]["period"])){
						$from = $frmprt[$ansfFormId]["params"]["period"]["from"];
						$to = $frmprt[$ansfFormId]["params"]["period"]["to"];
						
					}
				}


				$addEntry["name"]=$elt["name"];
				$strPorter="";

				$strPartner="";
				$totalBudg=0;

				// if(isset($elt["links"]) && !empty($elt["links"]) && isset($elt["links"]["contributors"]) && !empty($elt["links"]["contributors"])){	
				// 	foreach($elt["links"]["contributors"] as $e => $p){
				// 		if((isset($p["isAdmin"]) || (isset($p["roles"]) && in_array("Porteur d'action", $p["roles"]))) && $p["type"]==Organization::COLLECTION){
				// 			$porter=Element::getElementSimpleById( $e,$p["type"], null, array("name"));
				// 			$strPorter.=(!empty($strPorter)) ? ", ".$porter["name"] : $porter["name"];
				// 		}
				// 	}
				// }

				if( !empty($ans) && 
                    !empty($ans["action"]) &&
                    !empty($ans["action"]["parents"]) 
                ) {

                    foreach ($ans["action"]["parents"] as $keyP => $valueP) {

                            $actparent = PHDB::findOneById( Organization::COLLECTION , $valueP["id"], array("name", "typeStruct") ) ;
                            if (!empty($actparent)) {
	                            if (!empty($actparent["typeStruct"]) && $actparent["typeStruct"] == "associe") {
	                                $strPartner .= (!empty($strPartner)) ? ", ".$actparent["name"] : $actparent["name"];
	                            } else {
	                            	$strPorter .= (!empty($strPorter)) ? ", ".$actparent["name"] : $actparent["name"];
	                            }
                        }
                    }
                }


				
				// $strPartner="";
				// $totalBudg=0;
				if(isset($ans["murir"]) && !empty($ans["murir"])){
					
					if(isset($ans["murir"]["budget"]) && !empty($ans["murir"]["budget"])){
						foreach ($ans["murir"]["budget"] as $q => $a) {
							$t = 0;
							if(!isset($a["valid"]) || $a["valid"]!="refused"){
								if(isset($from)){
									for ($i = $from; $i <= $to ; $i++) { 
										if (isset($a["amount".$i])) {
											$t += intval($a["amount".$i]);
										}
									}
								}	
							}
							$totalBudg = $totalBudg + $t;
						}
					}
				}
				$addEntry["porteur"]=$strPorter;
				$addEntry["partner"]=$strPartner;
				$addEntry["budget"]=$totalBudg;
				array_push($answerList, $addEntry);
			}
		}
			
		//var_dump($answerList);exit;
		$params = array(
			//"author" => @$answer["name"],
			"idBadge"=>$id,
			"badge"=>$badge,
			"saveOption"=>"F",
			//"urlPath"=>$res["uploadDir"],
			"title" => $badge["name"]."_".date("d-m-Y"),
			"date" => date("d-m-Y"),
			"subject" => "CTE",
			//"custom" => $form["custom"],
			"footer" => true,
			"tplData" => "cteDossier",
			"projetParent"=> $projetParent,
			"answerList"=> $answerList

		);
	    $res=Document::checkFileRequirements([], null, 
			array(
				"dir"=>"communecter",
				"typeEltFolder"=> Badge::COLLECTION,
				"idEltFolder"=> $id,
				"docType"=> "file",
				"nameUrl"=>"/pdf.pdf",
				"sizeUrl"=>1000
			)
		);
		$params["urlPath"]=$res["uploadDir"];
		$params["docName"]="orientation_".str_replace(["'", "/"], "", $badge["name"])."_".date("d-m-Y").".pdf";
			
		$html= $controller->renderPartial('costum.views.custom.ctenat.pdf.orientation', $params, true);

		$params["html"] = $html ;
		$res=Pdf::createPdf($params);
			$kparams = array(
                "id" => $id,
                "type" => Badge::COLLECTION,
                "folder" => Badge::COLLECTION."/".$id."/".Document::GENERATED_FILE_FOLDER,
                "moduleId" => "communecter",
                "name" => $params["docName"],
                "size" => "",
                "contentKey" => "",
                "doctype"=> "file",
                "author" => Yii::app()->session["userId"]
            );
            $res2 = Document::save($kparams);
            return Rest::json(array("res"=>true, "id"=>(string)$res2["id"], "docPath"=>"/upload/".$kparams["moduleId"]."/".$kparams["folder"]."/".$params["docName"], "fileName"=>$params["docName"]));
	   
	}
}