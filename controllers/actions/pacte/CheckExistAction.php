<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, City, PHDB, Organization, Rest;
class CheckExistAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){ 	
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);

		if(!empty($_POST["scope"])){
			$name = "";
			foreach ($_POST["scope"] as $key => $value) {
				$checkScope=$key;
				$postalCode=$value["postalCode"];
				$city = City::getById($value["city"], array("postalCodes"));
			}

			if(!empty($city) && !empty($city["postalCodes"][0]["postalCode"]) ){
				$name = trim($name);
				$where = array( "source.key" => "siteDuPactePourLaTransition",
								"scope.".$checkScope => array('$exists'=>true),
								//"email" => "pacte-".mb_strtolower($city["postalCodes"][0]["postalCode"])."@listes.transition-citoyenne.org",
								"source.toBeValidated" => array('$exists' => false )  );

				
				$exist = PHDB::findOne(Organization::COLLECTION, $where);

				if(!empty($exist)){
					$res["exist"] = true;
					$res["elt"] = $exist;
				/*	
					if (!isset($exist["category"]) || $exist["category"]=="soutien") {
						$whereCategory = array( "source.key" => "siteDuPactePourLaTransition",
								"scope.".$checkScope => array('$exists'=>true) );
						//Nonbre de collectif soutien
						$contCategory = PHDB::count(Person::COLLECTION, $whereCategory);
						$res["numberSoutien"]= $contCategory;
					}elseif ($exist["category"]=="actifSigned") {
						$res["checkIdPoi"]=array_keys($exist["links"]["contract"]);

						//$whereElue = array("_id" => $res["checkIdPoi"][0]);
						//Nonbre de mesure dans  contract
						$findMesure = PHDB::find(Poi::COLLECTION, array("_id" => new MongoId((string)$res["checkIdPoi"][0]))) ;
						$res["numberMesure"]= count($findMesure[$res["checkIdPoi"][0]]["localMeasures"]);
					}
				*/
					//$res["elt"] = $exist;
				}else{
					$res["exist"] = false;
				}
			}
			
		}


		return Rest::json($res) ;
	}
}