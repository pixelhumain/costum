<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, SiteDuPactePourLaTransition, Rest;
/**
 * 
 */
class GetElueAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$params = SiteDuPactePourLaTransition::getElue($_POST["idPoi"]);

		return Rest::json($params);
	}
}

?>