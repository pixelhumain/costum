<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii, Rest, PHDB, MongoId;

class SubscribeToNewsLetterAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        
        $contextId = $_POST['contextId']??"";
        $contextCollection = $_POST['contextType']??"";

        $mailToSubscribe = $_POST['mailToSubscribe']??"";
        $userName = $_POST['userName']??"";

        $response = array('ok' => false, 'status'=>'error', 'msg' => "Une erreur s'est produite" );

        if($mailToSubscribe==""){
            $response["msg"] = "Une adresse mail est obligatoire";
            $response["status"] = "error";
        }else{
            $element = PHDB::findOneById($contextCollection, $contextId);
            $isSubscribed = false;
            if(isset($element["links"]) && isset($element["links"]["newsletter"])){
                foreach ($element["links"]["newsletter"] as $key => $value) {
                    if($value["email"] == $mailToSubscribe){
                        $isSubscribed = true;
                    }
                }
            }

            if($isSubscribed){
                $response["msg"] = "L'addresse mail ".$mailToSubscribe." est déjà inscrit dans notre newsletter. Merci";
                $response["status"] = "info";
            }else{
                $res = PHDB::update(
                    $contextCollection, 
                    array('_id' => new MongoId($contextId)),
                    array(
                        '$push'=>array(
                            'links.newsletter' => array(
                                'name' => $userName, 
                                'email' => $mailToSubscribe 
                            )
                        )
                    )
                );

                if($res["ok"]==1 && $res["nModified"]==1){
                    $response["ok"] = true;
                    $response["msg"] = "Merci pour votre inscription, vous recevrez des nouvelles de nous sur l'adresse ".$mailToSubscribe;
                    $response["status"] = "success";
                } 
            }
        }
        
        return Rest::json($response);
    }
}