<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii, Cms, PHDB, Element, Rest, CacheHelper, Project;

class SetProjectCategoriesAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();
        //var_dump($_POST);exit;
        $contextId=$_POST["costumId"];
        $projects=PHDB::find(Project::COLLECTION,array('parent.'.$contextId=>array('$exists'=>1)));
        //var_dump($projects);exit;
        $params = [
            "projects" => $projects,
           //  "modalrender" => true
        ];
        return $this->getController()->renderPartial("costum.views.custom.tiersLieuxGenerique.projectCategories", $params,true );
    }
}


//smallMenu.openAjaxHTML(baseUrl + '/costum/tierslieuxgenerique/setprojectcategories')

// ajaxPost(
//                 null,
//                 baseUrl + "/co2/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/projects",
//                 {},
//                 function (res) {
                    
//                 });