<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\tierslieuxgenerique;

use Yii, Form, PHDB, Element, Rest, Cron, Mail, CacheHelper;

class DescribeProjectAction extends \PixelHumain\PixelHumain\components\Action {
    public function run() {
        $controller = $this->getController();

        // get posted data
        $prjData = $_POST['map'];
        $prjId = $_POST['id'];
        
        $cid = $_POST['cid'];
        $cname = $_POST['cname'];
        $ctype = $_POST['ctype'];

        $asAnswer = array(
            'tiersLieuxGenerique2672022_952_0' => array(
                'tiersLieuxGenerique2672022_952_0l64j6zq0ar0soa7rp1e' => [
                    "0" => array(
                        'id' => $prjId,  
                        'type' => $prjData['collection'],
                        'slug' => $prjData['slug']
                    )]
            )
        );

        $answerData = array(
            "collection" => Form::ANSWER_COLLECTION,
            "user" => $_SESSION["userId"],
            "cterSlug" => $_POST["costumSlug"],
            "parent" => [
                $cid => [
                    "type" => $ctype,
                    "name" => $cname
                ]
            ],
            "form" => "62df8d35afe92e169c5b2a78", // formId 
            "formId" => [ 
                0
            ],
            "formList" => 1,
            'answers' => $asAnswer
        );

        Yii::app()->mongodb->selectCollection(Form::ANSWER_COLLECTION)->insert( $answerData );

        // send email with answers link
        $bU = Yii::app()->getRequest()->getBaseUrl(true);
        
        $currentAnswer = PHDB::findOne(Form::ANSWER_COLLECTION, array('answers.tiersLieuxGenerique2672022_952_0.tiersLieuxGenerique2672022_952_0l64j6zq0ar0soa7rp1e.0.id' => $prjId));

        if(!empty($currentAnswer)){
            
            Mail::createAndSendSingle(
                array(
                    "tpl" => "basic",
                    "tplMail" => $_SESSION["userEmail"],
                    "to" => $_SESSION["userEmail"],
                    "from" => Yii::app()->params['adminEmail'],
                    "tplFrom" => Yii::app()->params['adminEmail'],
                    "tplObject" => "Décrir votre projet en détail - Communecter",
                    'html' => 'Vous venez de créer un projet, veuillez le renseigner en détail en cliquant sur le lien ci-dessous <br/> <a href="'.$bU.'/#@tiersLieuxGenerique.view.forms.dir.answer.'.((string)$currentAnswer['_id']).'" target="_blank"> Détailler mon projet  </a>',
                    'forceMailSend' => true
                )
            );

            // return toast message
            return Rest::json(
                array(
                    'result' => true, 
                    'msg' => "Vous venez de créer un projet. Nous vous invitons à en détailler les caractéristiques (objectifs, critères d'évaluation, nature des actions, personnes impliquées), de manière à en faciliter la suivi et à terme son établir un bilan.",
                    'answerId' => ((string)$currentAnswer['_id']),
                    'action' => "Détailler Maintenant"
                )
            );
        }else{
            throw new Exception("Une erreur s'est produite", 1);
        }

        //return $controller->renderPartial("costum.views.custom.tiersLieuxGenerique.badge-finder", ["email" => $email]);
    }
}