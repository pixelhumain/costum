<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\hubultranumerique;
use CAction;
use hubUltraNumerique;
use Rest;

class GetEventCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = hubUltraNumerique::getEventCommunity($_POST["contextId"],$_POST["contextType"]);
        
        return Rest::json($params);
    }
}