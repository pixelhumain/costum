<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia;

use DataValidator;
use MongoId;
use PHDB;
use PixelHumain\PixelHumain\components\Action;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use Rest;
use Yii;
use CTKException;

class ManageReferencesAction extends Action
{
    public function run($action=null, $set=null)
    {
        $controller = $this->getController();
        $params = array();
        $params=$_POST;
        if(DataValidator::missingParamsController($_POST, ["id", "type","sourceKey"]))
            return array("result"=>false, "msg"=>Yii::t("common", "Something went wrong : please contact your admin"));
        $res = array("result"=>false, "msg"=>Yii::t("common", "Something went wrong!"));
        try {
            if(!empty($action)){
                $sourceKey = $_POST['sourceKey'];
                if(isset(Yii::app()->session["costum"])
                    && isset(Yii::app()->session["costum"]["contextSlug"]))
                    $sourceKey=Yii::app()->session["costum"]["contextSlug"];

                $elt = PHDB::findOneById($_POST["type"], $_POST["id"]);
                $ekitia = PHDB::findOne($controller->costum["contextType"], array("slug" => $controller->costum["contextSlug"]));
                if($action=="add"){
                    $res = Admin::addSourceInElement($_POST["id"], $_POST["type"], $sourceKey, $set);
                    $elt["links"]["memberOf"][(string)$controller->costum["contextId"]] = array("type"=>$controller->costum["contextType"]);
                    $ekitia["links"]["members"][$_POST["id"]] = array("type" => $_POST["type"]);
                    $query = array("links.memberOf" => $elt["links"]["memberOf"]);
                    $queryEkitia = array("links.members" => $ekitia["links"]["members"]);
                }
                if($action=="remove"){
                    $res = Admin::removeSourceFromElement($_POST["id"], $_POST["type"], $sourceKey, $set);
                    unset($elt["links"]["memberOf"][(string)$controller->costum["contextId"]]);
                    unset($ekitia["links"]["members"][$_POST["id"]]);
                    if(isset($elt["links"]["memberOf"])){
                        $linkUpToDate = $elt["links"]["memberOf"];
                        if(count($linkUpToDate)==0){
                            $linkUpToDate = null;
                        }
                        $query = array("links.memberOf" => $linkUpToDate);
                    }
                    if(isset($ekitia["links"]["members"])){
                        $linkUpToDate = $ekitia["links"]["members"];
                        if(count($linkUpToDate)==0){
                            $linkUpToDate = null;
                        }
                        $queryEkitia = array("links.members" => $linkUpToDate);
                    }
                }
                if(isset($query)){
                    PHDB::update( $_POST["type"],
                        array("_id" => new MongoId($_POST["id"])),
                        array('$set'=>$query));
                }
                if(isset($queryEkitia)){
                    PHDB::update( $controller->costum["contextType"],
                        array("_id" =>  new MongoId($controller->costum["contextId"])),
                        array('$set'=>$queryEkitia));
                }
            }
        } catch (CTKException $e) {
            $res = array("result"=>false, "msg"=>$e->getMessage());
        }
        return Rest::json($res);
    }
}