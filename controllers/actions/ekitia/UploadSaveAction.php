<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia;

use MongoId;
use Yii, Person, Document, Rest;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\handlers\Handler;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\type\core\Activity;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\Activitypub;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\activitypub\ActivitypubActivityCreator;
use Preference;

class UploadSaveAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($dir, $folder = null, $ownerId = null, $input, $contentKey = false, $docType = false, $rename = false, $folderId = null, $surveyId = null, $subDir = null, $subKey = null, $forcedUnloggued = null)
    {
        $user = Yii::app()->session["user"];
        $res = array('result' => false, 'msg' => Yii::t("document", "Something went wrong with your upload!"));
        if (Person::logguedAndAuthorized() || !empty($forcedUnloggued)) {
            if (strtolower($_SERVER['REQUEST_METHOD']) != 'post') {
                $res = array('result' => false, 'error' => Yii::t("document", "Error! Wrong HTTP method!"));
            }
            //{"result":false,"msg":"Le chargement du document ne s'est pas deroul\u00e9 correctement",
            //"file":{ "qqfile":{"name":"compo.jpg","type":"","tmp_name":"","error":6,"size":0}}}
            $file = [];
            if (array_key_exists($input, $_FILES)) {
                if (intval($_FILES[$input]['error']) === 0) {
                    $file = $_FILES[$input];
                    $res['result'] = true;
                } else {
                    $errors = [
                        'The uploaded file exceeds the upload_max_filesize directive in php.ini',
                        'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
                        'The uploaded file was only partially uploaded',
                        'No file was uploaded',
                        'Missing a temporary folder',
                        'Failed to write file to disk',
                        'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help'
                    ];
                    $res = [
                        'result' => false,
                        'msg' => $errors[intval($_FILES[$input]['error']) - 1],
                    ];
                }
            } else {
                error_log("WATCH OUT ! - ERROR WHEN UPLOADING A FILE ! CHECK IF IT'S NOT AN ATTACK");
                $res = array('result' => false, 'msg' => Yii::t("document", "Something went wrong with your upload!"));
            }
            if (!$res['result']) return Rest::json($res);

            $res['file'] = $file;
            $pathArray = array(
                "dir" => $dir,
                "typeEltFolder" => $folder,
                "idEltFolder" => $ownerId,
                "contentKey" => @$contentKey,
                "docType" => @$docType,
                "folderId" => @$folderId,
                "subDir" => @$subDir
            );
            if (isset($_POST["restricted"]))
                $pathArray["restricted"] = true;
            if (isset($_POST["qqfilename"]))
                $pathArray["nameUrl"] = $_POST["qqfilename"];

            $res = Document::checkFileRequirements($file, $input, $pathArray);
            if ($res["result"]) {
                $nameUrl = (isset($pathArray["nameUrl"])) ? $pathArray["nameUrl"] : null;
                $res = Document::uploadDocument($file, $res["uploadDir"], $input, $rename, $nameUrl, null, $forcedUnloggued);
                if ($res["result"]) {
                    $res = array(
                        'resultUpload' => true,
                        "success" => true,
                        'name' => $res["name"],
                        //'dir'=> $res["uploadDir"],
                        "docPath" => $res["uploadDir"] . $res["name"],
                        'size' => (int)$res["size"]
                    );
                }
            }
            $res2 = array();

            if (@$res["resultUpload"]) {
                //error_log("resultUpload xxxxxxxxxxxxxxxx");
                /*if($contentKey==false){
                    if(@$_POST["contentKey"]) $contentKey=$_POST["contentKey"];
                    else $contentKey=Document::IMG_PROFIL;
                }*/
                $subFolder = "";
                if (@$_POST["formOrigin"])
                    $subFolder = "/" . $_POST["formOrigin"];
                if ($contentKey == Document::IMG_SLIDER)
                    $subFolder = "/" . Document::GENERATED_ALBUM_FOLDER;
                if (@$docType && $docType == Document::DOC_TYPE_FILE) {
                    $subFolder = "/" . Document::GENERATED_FILE_FOLDER;
                    if (@$subDir)
                        $subFolder .= "/" . str_replace(".", "/", $subDir);
                }
                $folderPath = $folder . "/" . $ownerId;
                if (isset($_POST["restricted"])) $folderPath .= "/restricted";
                $folderPath .= $subFolder;
                $params = array(
                    "id" => $ownerId,
                    "type" => $folder,
                    "folder" => $folderPath,
                    "moduleId" => "communecter",
                    "name" => $res["name"],
                    "size" => (int) $res['size'],
                    "contentKey" => $contentKey,
                    "doctype" => $docType,
                    "author" => (isset(Yii::app()->session["userId"])) ? Yii::app()->session["userId"] : "",
                    "forcedUnloggued" => $forcedUnloggued
                );
                if (isset($_POST["subKey"]))
                    $params["subKey"] = $_POST["subKey"];
                else if (!empty($subKey)) {
                    $params["subKey"] = $subKey;
                }
                if (isset($_POST["cryptage"]))
                    $params["cryptage"] = $_POST["cryptage"];
                if (isset($_POST["restricted"]))
                    $params["restricted"] = $_POST["restricted"];
                if (isset($surveyId))
                    $params["surveyId"] = $surveyId;
                if (isset($folderId))
                    $params["folderId"] = $folderId;
                if (isset($_POST["parentType"]))
                    $params["parentType"] = $folder;
                if (isset($_POST["parentId"]))
                    $params["parentId"] = $ownerId;
                if (isset($_POST["formOrigin"]))
                    $params["formOrigin"] = $_POST["formOrigin"];
                if (isset($_POST["cropX"]))
                    $params["crop"] = array("cropX" => $_POST["cropX"], "cropY" => $_POST["cropY"], "cropW" => $_POST["cropW"], "cropH" => $_POST["cropH"]);
                if(isset($params["subKey"])){
                    Document::removeDocumentByWhere(["id" => $ownerId,
                        "type" => $folder, "subKey" => $params["subKey"]]);
                }
                $res2 = Document::save($params);
                // if($res2['result']){
                //     $data = explode("-", $params["subKey"]);
                //     $path = $data[0];
                //     $key = $data[1];

                //     $projects = PHDB::findOneById($folder, $ownerId, [$path]);
                //     $projects[$path][$key]["image"] = $res2["docPath"];

                //     PHDB::update($folder, ["_id" => new MongoId($ownerId)], ['$set' => [$path => $projects[$path]]]);
                // }
                if (@$res2["survey"] && $res2["survey"]) {
                    $res2["survey"] = substr($subDir, strrpos($subDir, '.') + 1);
                }
            }
        } else
            $res2 = array("result" => false, "msg" => Yii::t("common", "Please Log in order to update document !"));

        $res = array_merge($res, $res2);
        $userId = Yii::app()->session["userId"];
        $isBanner = $input == "modalDashprofil_avatar" || $input == "profil_avatar";
        if (
            $userId &&
            ($userId == $ownerId) &&
            ($input == "modalDashprofil_avatar" || $input == "profil_avatar") &&
            isset($res["src"]["changes"])
        ) {
            $changes = $res["src"]["changes"];

            $user["profilImageUrl"] = $changes["profilImageUrl"];
            $user["profilMarkerImageUrl"] = $changes["profilMarkerImageUrl"];
            $user["profilMediumImageUrl"] = $changes["profilMediumImageUrl"];
            $user["profilThumbImageUrl"] = $changes["profilThumbImageUrl"];
            Yii::app()->session["user"] = $user;


            //federate avatar update if activitypub activate
            if (Preference::isActivitypubActivate($user["preferences"])){
                $activity = ActivitypubActivityCreator::createUpdateUploadActivity($ownerId,$res,$params);
                if( $activity instanceof Activity){
                    Handler::handle($activity);
                }
            }


        }else{
            $res['isBanner'] = $contentKey == 'banner';

            if (Preference::isActivitypubActivate($user["preferences"])){
                $activity = ActivitypubActivityCreator::createUpdateUploadActivity($ownerId,$res,$params);
                if( $activity instanceof Activity){
                    Handler::handle($activity);
                }
            }
        }

        return Rest::json($res);
    }
}