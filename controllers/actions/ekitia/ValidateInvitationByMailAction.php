<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia;

use MongoId;
use Organization;
use Yii, PHDB, Person, Rest, Link;

class ValidateInvitationByMailAction  extends \PixelHumain\PixelHumain\components\Action
{
    public function run($userId, $targetType, $targetId, $answer, $redirect=null, $costum=null ) {
        $controller=$this->getController();
        $res = array( "result" => false , "msg" => Yii::t("common","Something went wrong!" ));
        $user = PHDB::findOneById(Person::COLLECTION, $userId, array("roles","links", "pending"));
        $target = PHDB::findOneById($targetType,$targetId, array("links", "slug"));

        if( !empty($user) &&
            !empty($target)){
            $registerAction=false;
            $urlElt = "#page.type.".$targetType.".id.".$targetId ;
            if( !empty($user["roles"]) &&
                !empty($user["roles"]["tobeactivated"]) &&
                $user["roles"]["tobeactivated"] == true ){
                $validationKey =Person::getValidationKeyCheck($userId);
                $registerAction=true;
                $urlElt = "/co2/person/validateinvitation/user/".$userId.'/validationKey/'.$validationKey.'/invitation/1';
            }

            if($answer ==  "true"){
                //$res = Link::validateLink($targetId, $targetType, $userId, Person::COLLECTION, Link::IS_INVITING, $userId);
                if($target["slug"] != "ekisphere"){
                    $ekitia = PHDB::findOne(Organization::COLLECTION, array("slug" => "ekisphere"));
                    $ekitia["links"]["members"][$userId] = array("type" => Person::COLLECTION);
                    $user["links"]["memberOf"][(string) $ekitia['_id']] = array("type" => Organization::COLLECTION);
                    $queryEkitia = array("links.members" => $ekitia["links"]["members"]);
                    $query= array("links.memberOf" => $user["links"]["memberOf"]);
                    $resEktia = PHDB::update( Organization::COLLECTION,
                        array("slug" => "ekisphere"),
                        array('$set'=>$queryEkitia)
                    );
                    $resUser = PHDB::update( Person::COLLECTION,
                        array("_id" => new MongoId($userId)),
                        array('$set'=>$query)
                    );
                }
            }else if($answer ==  "false"){
                $res = Link::disconnect($userId, Person::COLLECTION, $targetId, $targetType, $userId, Link::$linksTypes[Person::COLLECTION][$targetType]);
                $res = Link::disconnect($targetId, $targetType, $userId, Person::COLLECTION, $userId, Link::$linksTypes[$targetType][Person::COLLECTION]);
            }

            $urlRedirect=Yii::app()->createUrl($urlElt);
            if($registerAction){
                if(!empty($redirect))
                    $urlRedirect.="/redirect/".$redirect;

                if(!empty($costum)){
                    $controller->redirect(Yii::app()->createAbsoluteUrl($urlElt."/costum/true") );
                }else{
                    $controller->redirect($urlRedirect);
                }
            }else{
                if(!empty($redirect)){
                    if(strrpos($redirect, "survey") !== false || strrpos($redirect, "costum") !== false) {
                        $redirect=str_replace(".", "/", $redirect);
                        $urlRedirect=Yii::app()->createUrl($redirect.$urlElt);
                        $controller->redirect($urlRedirect);
                    }
                } else if(!empty($costum)){
                    $controller->redirect(Yii::app()->createAbsoluteUrl($urlElt) );
                }else{
                    $controller->redirect($urlRedirect);
                }
            }
        } else {
            $controller->redirect(Yii::app()->createUrl(""));
        }
        return Rest::json($res);
    }
}