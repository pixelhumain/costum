<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia;

use MongoId;
use PHDB;
use Rest;

class ValidateGroupAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null, $type = null){
        $controller = $this->getController();
        if($_POST["id"] && $_POST["type"]){
            $elt = PHDB::findOneById($_POST["type"], $_POST["id"]);
            $ekitia = PHDB::findOne($controller->costum["contextType"], array("slug" => $controller->costum["contextSlug"]));
            if($_POST["valid"] == "true"){
                if( !empty($elt["preferences"]["toBeValidated"]) ) {
                    unset($elt["preferences"]["toBeValidated"][$controller->costum["slug"]]);
                    if(count($elt["preferences"]["toBeValidated"]) == 0)
                        unset($elt["preferences"]["toBeValidated"]);
                }
                if(isset($elt["links"]["memberOf"][(string)$controller->costum["contextId"]]["toBeValidated"])){
                    unset($elt["links"]["memberOf"][(string)$controller->costum["contextId"]]["toBeValidated"]);
                }else{
                    $elt["links"]["memberOf"][(string)$controller->costum["contextId"]] = array("type"=>$controller->costum["contextType"]);
                }
                if(isset($ekitia["links"]["members"][$_POST["id"]]["toBeValidated"])){
                    unset($ekitia["links"]["members"][$_POST["id"]]["toBeValidated"]);
                }else{
                    $ekitia["links"]["members"][$_POST["id"]] = array("type" => $_POST["type"]);
                }
            }else{
                if( empty($elt["preferences"]["toBeValidated"]) )
                    $elt["preferences"]["toBeValidated"] = array();
                $elt["preferences"]["toBeValidated"][$controller->costum["slug"]] = true ;
                if(empty($elt["links"]["memberOf"]))
                    $elt["links"]["memberOf"] = array();
                $elt["links"]["memberOf"][(string)$controller->costum["contextId"]]["toBeValidated"] = true;
                if(empty($ekitia["links"]["members"]))
                    $$ekitia["links"]["members"] = array();
                $ekitia["links"]["members"][$_POST["id"]]["toBeValidated"] = true;
            }
            $query=array("preferences" => $elt["preferences"], "links" => $elt["links"]);
            $res= PHDB::update( $_POST["type"],
                array("_id" => new MongoId($_POST["id"])),
                array('$set'=>$query));
            $ekitiaQuery = array("links.members" => $ekitia["links"]["members"]);
            $resEkitia= PHDB::update($controller->costum["contextType"],
                array("_id" => new MongoId($controller->costum["contextId"])),
                array('$set'=>$ekitiaQuery));
        }
        return Rest::json(array(
            "result"=>true,
            "elt" => $elt,
            "ekitia" => $ekitia
        ));
    }
}