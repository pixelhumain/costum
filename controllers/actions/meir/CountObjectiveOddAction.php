<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meir;
use MongoId;
use PHDB;
use Rest;
use Organization;

class CountObjectiveOddAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id=null, $slug, $field = null)
    {
        $results = array();
        $query = array(
            '$or'=> array (
                array("source.keys" => $slug),
                array("links.memberOf.".$id.".type"=>Organization::COLLECTION)
            )
        );
        if(isset($_POST["odd"])){
            foreach ($_POST['odd'] as $key => $value) {
                $count = 0;
                if($field == "tags") {
                    $query["tags"] = $value;
                } else {
                    $query["objectiveOdd"] = $value;
                    $query["category"] = "startup";
                }
                if(isset($_POST["elements"])) {
                    foreach ($_POST["elements"] as $value) {
                        $count += PHDB::count($value, $query);
                    }
                } else {
                    $count = PHDB::count(Organization::COLLECTION, $query);
                }
                array_push($results, $count);
            }
        }
        return Rest::json($results);
    }
}