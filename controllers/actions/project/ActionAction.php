<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\project;

use Action;
use Authorisation;
use DateTime;
use DateTimeZone;
use MongoDate;
use Person;
use Document;
use Event;
use Lists;
use MongoId;
use MongoRegex;
use PHDB;
use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Project;
use Rest;
use RocketChat;
use Room;
use Yii;
use yii\helpers\Markdown;

class ActionAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($request = '', $scope = '', $mode = 'w') {
		$userId = $_POST['connectedUser'] ?? (Yii::app()->session['userId'] ?? '');
		$output = null;
		switch ($request) {
			case "cancel":
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Empty action id"
					]));
				$set = [
					"status"	=> "closed"
				];
				$unset = [
					"tracking"	=> 0
				];
				PHDB::update(Action::COLLECTION, ["_id" => new MongoId($_POST["id"])], [
					'$set'		=> $set,
					'$unset'	=> $unset,
					'$pull'		=> [
						"tags"	=> "totest"
					]
				]);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> PHDB::findOneById(Action::COLLECTION, $_POST["id"])
				]));
				break;
			case 'archive':
				$output = ['success' => false];
				if (empty($_POST['id']))
					$output['data'] = 'Empty field id';
				else {
					$update = [
						'$set' => ['status' => 'disabled'],
						'$unset' => ['tracking' => '']
					];
					PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['id'])], $update);
					$output = [
						'success'	=> true,
						'data'		=> $_POST['id']
					];
					self::emitActionEvent('action-archive', [
						'id'	=> $_POST['id']
					]);
				}
				$output = Rest::json($output);
				break;
			case 'new':
				$required_list = ['name', 'status', 'parentId', 'parentType'];
				if (count(array_filter($required_list, fn($required) => in_array($required, $_POST))) === count($required_list))
					return Rest::json([
						'success'	=> false,
						'content'	=> 'Required fields empty'
					]);
				$user = \Yii::app()->session["userId"] ?? '';
				if (!empty($_POST["user"]))
					$user = $_POST["user"];
				if (!Person::logguedAndAuthorized($user))
					return \Rest::json([
						"success" => false,
						"content" => "You are not allowed to do this action",
					]);
				$max = 0;
				$min = 0;
				if (!empty($_POST['min']))
					$min = intval($_POST['min']);
				if (!empty($_POST['max']))
					$max = intval($_POST['max']);
				$max = $min > $max ? $min : $max;
				$n_action = [
					'name'			=> $_POST['name'],
					'status'		=> 'todo',
					'credits'		=> 0,
					'created'		=> time(),
					'parentId'		=> $_POST['parentId'],
					'parentType'	=> $_POST['parentType'],
					'creator'		=> $user,
					'idUserAuthor'	=> $user,
					'idParentRoom'	=> !empty($_POST['idParentRoom']) ? $_POST['idParentRoom'] : Room::getOrCreateActionRoom($_POST),
					'max'			=> $max,
					'min'			=> $min,
					'tags'			=> [],
				];
				if (!empty($_POST["urls"]))
					$n_action['urls'] = $_POST["urls"];
				if (!empty($_POST["milestone"])) {
					$n_action["milestone"] = $_POST["milestone"];
					if (!empty($n_action["milestone"]["startDate"])) {
						$n_action["milestone"]["startDate"] = new DateTime($n_action["milestone"]["startDate"]);
						$n_action["milestone"]["startDate"] = new MongoDate($n_action["milestone"]["startDate"]->setTimezone(new DateTimeZone("UTC"))->getTimestamp());
					}
					if (!empty($n_action["milestone"]["endDate"])) {
						$n_action["milestone"]["endDate"] = new DateTime($n_action["milestone"]["endDate"]);
						$n_action["milestone"]["endDate"] = new MongoDate($n_action["milestone"]["endDate"]->setTimezone(new DateTimeZone("UTC"))->getTimestamp());
					}
				}
				if (!empty($_POST['is_contributor']) && filter_var($_POST['is_contributor'], FILTER_VALIDATE_BOOL)) {
					$n_action['links'] = [
						'contributors'	=> [
							$user	=> [
								'type'		=> Person::COLLECTION,
								'isAdmin'	=> true
							]
						]
					];
				}
				if (!empty($_POST['assign'])) {
					$n_action['links'] = [
						'contributors'	=> [
							$_POST['assign']	=> [
								'type'		=> Person::COLLECTION,
								'isAdmin'	=> true
							]
						]
					];
				}
				
				if (!empty($_POST['mentions'])) {
					$db_persons = PHDB::find(Person::COLLECTION, ['username' => ['$in' => $_POST['mentions']]], ['_id']);
					$n_action['links'] = [
						'contributors'	=> [],
					];
					$db_persons = array_keys($db_persons);
					foreach ($db_persons as $id)
						$n_action['links']['contributors'][$id] = [
							'type'	=> Person::COLLECTION
						];
				}
				switch ($_POST['status']) {
					case 'done':
						$n_action['status'] = $_POST['status'];
						$n_action['startDate'] = new MongoDate(strtotime(date('Y-m-d h:i')));
						$n_action['endDate'] = new MongoDate(strtotime(date('Y-m-d h:i') . ' + 1 minutes'));
						break;
					case 'tracking':
						$n_action['tracking'] = true;
						$n_action['startDate'] = new MongoDate(strtotime(date('Y-m-d h:i')));
						break;
					case 'discuter':
					case 'totest':
					case 'next':
						$n_action['tags'] = [$_POST['status']];
						if ($_POST['status'] === 'totest')
							$n_action['startDate'] = new MongoDate(strtotime(date('Y-m-d h:i') . ' - 1 minutes'));
						break;
					default:
						$n_action['status'] = 'todo';
						break;
				}
				if (!empty($_POST['tags']))
					$n_action['tags'] = array_merge($n_action['tags'], $_POST['tags']);
				if (!empty($_POST['startDate']) || !empty($_POST['start_date']))
					$n_action['startDate'] = new MongoDate(strtotime($_POST['startDate'] ?? $_POST['start_date']));
				if (!empty($_POST['endDate']) || !empty($_POST['end_date']))
					$n_action['endDate'] = new MongoDate(strtotime($_POST['endDate'] ?? $_POST['end_date']));
				Yii::app()->mongodb->selectCollection(Action::COLLECTION)->insert($n_action);
				$db_parent = PHDB::findOneById($_POST['parentType'], $_POST['parentId'], ['name', 'slug', 'tools.chat.int']);
				$rc_message = ' @' . Yii::app()->session['user']['username'] . ' a ajouté une action **' . $_POST['name'] . '** dans **' . $db_parent['name'] . '**';
				$n_action['rc'] = [];
				if (!empty($db_parent['tools']['chat']['int'])) :
					foreach ($db_parent['tools']['chat']['int'] as $url) :
						if (is_array($url) && !empty($url['url']) && !empty($url['name'])) {
							$position = strpos($url['url'], 'channel');
							$position = $position === false ? -1 : $position;
							if (isset(Yii::app()->params['rocketchatServiceEnabled']))
								$n_action['rc'][] = RocketChat::post($url['name'], $rc_message, '', in_array($position, [0, 1]));
						} else if (is_string($url)) {
							$position = strpos($url, 'channel');
							$position = $position === false ? -1 : $position;
							if (isset(Yii::app()->params['rocketchatServiceEnabled']))
								$n_action['rc'][] = RocketChat::post($db_parent['slug'], $rc_message, '', in_array($position, [0, 1]));
						}
					endforeach;
				endif;
				$ws_parameter = [
					'component'	=>	$_POST['component'] ?? 'undefined',
					'emiter'	=>	$_POST['emiter'] ?? null,
					'group'		=>	$_POST['status'],
					'id'		=>	(string)$n_action['_id'],
				];
				self::emitActionEvent('.action-create-' . $_POST['parentId'], $ws_parameter);
				self::emitActionEvent('.cd-int-create', $ws_parameter);
				return Rest::json([
					'success' => true,
					'content' => $n_action,
				]);
				break;
			case "socket-action-create":
				if (empty($_POST["id"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Empty action id"
					]));
				$parent = $_POST["parent"] ?? null;
				if (empty($parent)) {
					$parent = PHDB::findOneById(Action::COLLECTION, $_POST["id"], ["parentId"]);
					$parent = $parent["parentId"];
				}
				return (Rest::json([
					"success"	=> 1,
					"content"	=> self::emitActionEvent(".action-create-${parent}", [
						'component'	=>	$_POST['component'] ?? 'undefined',
						'emiter'	=>	$_POST['emiter'] ?? null,
						'group'		=>	$_POST['status'] ?? "todo",
						'id'		=>	$_POST['id'],
					])
				]));
				break;
			case 'actions':
				// Récupère la liste des actions à partir du filtre
				$where = $_POST['filters'] ?? [];
				if (!empty($_POST['text'])) {
					$text = $_POST['text'];
					$special_commands = [':mine', ':other', ':all'];
					$first_text = substr($text, 0, 1);
					if ($first_text === '#' || $first_text === ':') {
						// Supprimer les espaces de trop
						$text = preg_replace('/\s+/', ' ', trim($text));
						// Normaliser les espaces
						$text = str_replace(',#', ', #', $text);
						$text = str_replace(',:', ', :', $text);
						// Diviser en tableau selon la normalisation
						$splited = explode(', ', $text);
						foreach ($splited as $split) {
							if (in_array($split, $special_commands)) {
								$split = strtolower($split);
								if ($split === ':mine') $where['links.contributors.' . $userId] = ['$exists' => true];
								else if ($split === ':other') $where['links.contributors.' . $userId] = ['$exists' => false];
								else if ($split === ':all') unset($where['urls']);
							} else {
								$search = substr($split, 1);
								if (empty($where['$or'])) $where['$or'] = [['status' => ['$in' => []]], ['tags' => ['$in' => []]]];
								$where['$or'][0]['status']['$in'][] = $search;

								// échaper les caractères spéciaux
								foreach (
									[
										'.',
										'+',
										'*',
										'?',
										'^',
										'$',
										'(',
										')',
										'[',
										']',
										'{',
										'}',
										'|',
										'\\',
									] as $special
								) $text = str_replace($special, '\\' . $special, $text);
								$where['$or'][1]['tags']['$in'][] = new MongoRegex('/' . $search . '/i');
							}
						}
					} else $where['name'] = new MongoRegex("/$text/i");
				}
				$where['status'] = ['$nin' => ['disabled', "closed"]];

				if (!empty($scope)) {
					switch ($scope) {
						case 'mine';
							$where['links.contributors.' . $userId] = ['$exists' => true];
							unset($where['urls']);
							break;
						case 'all':
							unset($where['urls']);
							break;
					}
				}

				$action_parent = [
					'id'	=> $where['parentId'],
					'type'	=> Project::COLLECTION
				];
				$db_actions = PHDB::findAndSort(Action::COLLECTION, $where, [
					"updated"	=> 1,
					'startDate'	=> 1,
					'created'	=> 1
				], 0, [
					'name',
					'tags',
					'medias',
					'status',
					'tracking',
					'links.contributors',
					'startDate',
					'endDate',
					'created'
				]);
				$db_actions = Lists::arrange_action_by_status($action_parent, $db_actions, ['discuter', "next", 'todo', 'tracking', 'totest', 'done']);
				$actions = [];
				foreach ($db_actions as $db_action) {
					if (empty($db_action['name'])) continue;
					$is_contributor = !empty($db_action['links']) && !empty($db_action['links']['contributors']) && !empty($db_action['links']['contributors'][$userId]);
					$actions[] = [
						'id'             => $db_action['id'],
						'name'           => $db_action['name'],
						'status'         => $db_action['status'] ?? 'todo',
						'tags'           => $db_action['tags'] ?? [],
						'tracking'       => !empty($db_action['tracking']) ? UtilsHelper::parse_values($db_action['tracking']) : false,
						'is_contributor' => $is_contributor,
						'startDate'      => $db_action['startDate'] ?? null,
						'endDate'        => $db_action['endDate'] ?? null,
						'created'        => $db_action['created'] ?? null,
					];
				}
				$output = Rest::json($actions);
				break;
			case 'media':
				$db_medias = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['media.images', 'mediaFile.files']);
				$medias = [
					"images" => [],
					"files"  => [],
				];
				$docImages = (!empty($db_medias['media']) && !empty($db_medias['media']['images'])) ? $db_medias['media']['images'] : [];
				$docFiles = (!empty($db_medias['mediaFile']) && !empty($db_medias['mediaFile']['files'])) ? $db_medias['mediaFile']['files'] : [];
				$docImageAndFiles = array_merge($docImages, $docFiles);
				if (!empty($docImageAndFiles)) {
					$documentsWhere = [
						'_id' => [
							'$in' => array_map(function ($__id) {
								return new MongoId($__id);
							}, $docImageAndFiles),
						],
					];
					$docs = Document::getListDocumentsWhere($documentsWhere);
					foreach ($docs as $key => $value) {
						$docType = $value['doctype'] ?? $value['docType'];
						if ($docType === "image") {
							$medias["images"][$key] = $value;
							$medias["images"][$key]["docPath"] = Document::getDocumentPath($value, true);
						}
						if ($docType === "file") {
							$medias["files"][$key] = $value;
							$medias["files"][$key]["docPath"] = Document::getDocumentPath($value, true);
						}
					}
				}

				$output = Rest::json($medias);
				break;
			case 'add_image_file':
				if (empty($_POST["action"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Need for action id"
					]));
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST["action"], ["media.images"]);
				$image_ids = $db_action["media"]["images"] ?? [];
				$images = PHDB::find(Document::COLLECTION, ["id" => $_POST['action'], "doctype" => "image"], ["_id"]);
				$files = PHDB::find(Document::COLLECTION, ["id" => $_POST['action'], "doctype" => "file"], ["_id"]);
				$image_keys = array_keys($images);
				foreach ($image_keys as $image_key) {
					if (!in_array($image_key, $image_ids))
						$image_ids[] = $image_key;
				}
				if (!empty($image_ids))
					PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['action'])], [
						'$set' => ["media.images" => $image_ids],
					]);
				if (!empty($files))
					PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['action'])], [
						'$set' => ["mediaFile.files" => array_keys($files)],
					]);
				return (Rest::json([
					"success"	=> 1,
					"content"	=> [
						"img"	=> $image_ids,
						"file"	=> array_keys($files)
					]
				]));
				break;
			case 'action':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action']);
				$output = Rest::json($db_action);
				break;
			case 'tags':
				$db_tags = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['tags']);
				$tags = [];
				if (!empty($db_tags) && !empty($db_tags['tags'])) $tags = $db_tags['tags'];
				$output = Rest::json($tags);
				break;
			case 'action_detail_data':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id']);
				$dbParent = PHDB::findOneById($db_action['parentType'], $db_action['parentId'], [
					'links.contributors',
					'creator',
					'created',
					'startDate',
				]);
				$allContributorsList = [];
				$creatorId = $db_action['creator'] ?? $dbParent['creator'];
				$parentFields = [];
				$actionimage_list = [];
				$actionDocumentsList = [];
				// Problème d'une action qui n'a pas de créateur
				if (empty($db_action['creator']) && !empty($db_action['parentType']))
					$parentFields[] = 'creator';
				// Contributeurs parents
				$actionContributorsList = !empty($db_action['links']['contributors']) && is_array($db_action['links']['contributors']) ? $db_action['links']['contributors'] : [];
				$parentContributorsList = !empty($dbParent['links']['contributors']) && is_array($dbParent['links']['contributors']) ? $dbParent['links']['contributors'] : [];
				foreach ($actionContributorsList as $id => $oneActionContributor) {
					if (!empty($oneActionContributor['type']) && $oneActionContributor['type'] === Person::COLLECTION)
						UtilsHelper::push_array_if_not_exists($id, $allContributorsList);
				}
				foreach ($parentContributorsList as $id => $oneParentContributor) {
					if (!empty($oneParentContributor['type']) && $oneParentContributor['type'] === Person::COLLECTION)
						UtilsHelper::push_array_if_not_exists($id, $allContributorsList);
				}
				if (!empty($dbParent['creator'])) {
					UtilsHelper::push_array_if_not_exists($dbParent['creator'], $allContributorsList);
					$creatorId = $creatorId ?? $dbParent['creator'];
				}
				if (!empty($db_action['creator'])) UtilsHelper::push_array_if_not_exists($db_action['creator'], $allContributorsList);

				// action sans date de départ
				if (empty($db_action['startDate']) && empty($db_action['created']) && !empty($db_action['parentType'])) {
					$parentFields[] = 'startDate';
					$parentFields[] = 'created';
				}

				if (!empty($parentFields)) {
					$creatorId = $db_action['creator'] ?? $dbParent['creator'];
					$db_action['created'] = $db_action['created'] ?? ($dbParent['created'] ?? null);
					$db_action['startDate'] = $db_action['startDate'] ?? ($dbParent['startDate'] ?? null);
				}
				$dbPersonsList = PHDB::findByIds(Person::COLLECTION, $allContributorsList, ['name', 'profilThumbImageUrl']);

				// Récupérer les images de l'action
				$documentIdsList = [];
				$documentIdsList = array_merge($documentIdsList, $db_action['media']['images'] ?? []);
				$documentIdsList = array_merge($documentIdsList, $db_action['media']['files'] ?? []);

				if (!empty($documentIdsList)) {
					$dbDocumentsList = PHDB::findByIds(Document::COLLECTION, $documentIdsList, ['folder', 'name', 'doctype', 'docType']);
					foreach ($dbDocumentsList as $dbDocumentId => $dbOneDocument) {
						$docType = $dbOneDocument['doctype'] ?? $dbOneDocument['docType'];
						switch ($docType) {
							case 'image':
								$actionimage_list[] = [
									'id'   => $dbDocumentId,
									'name' => $dbOneDocument['name'],
									'url'  => '/upload/communecter/' . $dbOneDocument['folder'] . '/' . $dbOneDocument['name'],
								];
								break;
							default:
								$actionDocumentsList[] = [
									'id'   => $dbDocumentId,
									'name' => $dbOneDocument['name'],
									'url'  => '/upload/communecter/' . $dbOneDocument['folder'] . '/' . $dbOneDocument['name'],
								];
								break;
						}
					}
				}

				$startDatetime = new DateTime();
				$endDatetime = new DateTime();

				$start_date = UtilsHelper::get_as_timestamp(['startDate', 'created'], $db_action);
				$startDatetime->setTimestamp($start_date);

				$now = strtotime(date('Y-m-d H:i'));
				$addition = max($now, strtotime($startDatetime->format('c') . ' + 1day'));

				$endDate = UtilsHelper::get_as_timestamp(['endDate'], $db_action, $addition);
				$endDatetime->setTimestamp($endDate);

				$contributorsList = [];
				$allContributorsList = [];
				foreach ($dbPersonsList as $personId => $onePerson) {
					$allContributorsList[$personId] = [
						'name'  => $onePerson['name'],
						'image' => $onePerson['profilThumbImageUrl'] ?? Yii::app()
							->getModule('co2')
							->getAssetsUrl() . '/images/thumb/default_citoyens.png',
					];
				}
				if (!empty($db_action['links']['contributors'])) {
					foreach ($db_action['links']['contributors'] as $id => $contributor) {
						$contributorsList[$id] = [
							'name'  => $allContributorsList[$id]['name'],
							'image' => $allContributorsList[$id]['image'],
						];
					}
				}

				$tasks = [];
				$task_person_list = [];
				if (!empty($db_action['tasks']) && is_array($db_action['tasks'])) {
					foreach ($db_action['tasks'] as $task) {
						$user = null;
						$this_task_contributor_list = [];
						$personQueryIds = [];
						$is_contributor_exists = !empty($task['contributors']) && is_array($task['contributors']);

						// Préparation des requêtes vers la base de donénes
						if (!empty($task['userId']) && empty($task_person_list[$task['userId']])) UtilsHelper::push_array_if_not_exists($task['userId'], $personQueryIds);
						if ($is_contributor_exists) {
							$filteredContributorKeyList = array_filter($task['contributors'], function ($id) use ($task_person_list) {
								return !array_key_exists($id, $task_person_list);
							}, ARRAY_FILTER_USE_KEY);
							$filteredContributorKeyList = array_keys($filteredContributorKeyList);
							foreach ($filteredContributorKeyList as $oneFilteredContributorKey) UtilsHelper::push_array_if_not_exists($oneFilteredContributorKey, $personQueryIds);
						}

						// Charger la liste des personnes
						$db_person_list = PHDB::findByIds(Person::COLLECTION, $personQueryIds, ['name']);
						foreach ($db_person_list as $dbPersonId => $dbOnePerson) {
							$task_person_list[$dbPersonId] = [
								'id'   => $dbPersonId,
								'name' => $dbOnePerson['name'] ?? '',
							];
						}

						// Récupération des valeurs
						if (!empty($task['userId'])) $user = $task_person_list[$task['userId']];
						if ($is_contributor_exists) {
							$this_task_contributor_list = array_map(function ($id) use ($task_person_list) {
								return $task_person_list[$id];
							}, array_keys($task['contributors']));
						}

						// Construire la structure de la sortie
						$tasks[] = [
							'id'           => $task['taskId'] ?? '',
							'name'         => $task['task'] ?? '(No name)',
							'user'         => $user,
							'checked'      => !empty($task['checked']) ? filter_var($task['checked'], FILTER_VALIDATE_BOOL) : false,
							'contributors' => $this_task_contributor_list,
						];
					}
				}

				$action = [
					'id'                 => $_POST['id'],
					'name'               => $db_action["name"],
					'creator'            => [
						'name'  => $dbPersonsList[$creatorId]['name'],
						'image' => $dbPersonsList[$creatorId]['profilThumbImageUrl'] ?? Yii::app()
							->getModule('co2')
							->getAssetsUrl() . '/images/thumb/default_citoyens.png',
					],
					'startDate'          => $startDatetime->format('c'),
					'endDate'            => $endDatetime->format('c'),
					'credits'            => $db_action['credits'] ?? 0,
					'contributor_number' => !empty($db_action['links']) && !empty($db_action['links']['contributors']) ? count($db_action['links']['contributors']) : 0,
					'min'                => $db_action['min'] ?? 0,
					'max'                => $db_action['max'] ?? max(count($contributorsList), 1),
					'contributors'       => $contributorsList,
					'allContributors'    => $allContributorsList,
					'tasks'              => $tasks,
					'images'             => $actionimage_list,
					'documents'          => $actionDocumentsList,
				];
				$output = Rest::json($action);
				break;
			case "task_basic_info":
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['tasks']);
				$db_tasks = $db_action['tasks'] ?? [];
				$tasks = [
					"done"  => 0,
					"total" => count($db_tasks),
					"data"  => [],
				];
				foreach ($db_tasks as $db_task) {
					$tasks["data"][] = [
						"name"         => $db_task["task"] ?? "undefined",
						"contributors" => [],
					];
				}
				$output = Rest::json($tasks);
				break;
			case "all_contributors":
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], [
					"links.contributors",
					"parentType",
					"parentId",
					"parent"
				]);
				$db_parent = [];
				$collectionType = Action::COLLECTION;
				if (!empty($db_action["parentId"]) && !empty($db_action["parentType"]))
					$db_parent = PHDB::findOneById($db_action["parentType"], $db_action["parentId"], ["links.contributors", "links.attendees"]);
				if (empty($db_action) && isset($_POST['id'])) {
					$db_parent = PHDB::findOneById(Project::COLLECTION, $_POST['id'], ["links.contributors"]);
					if (!empty($db_parent))
						$collectionType = Project::COLLECTION;
				}
				if ($collectionType == Project::COLLECTION)
					$db_parent = $db_parent["links"]["contributors"] ?? [];
				else
					$db_parent = $db_action["parentType"] === Project::COLLECTION ? $db_parent["links"]["contributors"] ?? [] : $db_parent["links"]["attendees"] ?? [];
				$db_action = $db_action["links"]["contributors"] ?? [];
				$db_parent = array_filter($db_parent, fn($consumer) => ($consumer["type"] === Person::COLLECTION));
				$db_action = array_filter($db_action, fn($consumer) => ($consumer["type"] === Person::COLLECTION));
				$db_parent = array_keys($db_parent);
				$db_action = array_keys($db_action);
				$contributors_id = [];
				foreach ($db_parent as $contributor)
					UtilsHelper::push_array_if_not_exists($contributor, $contributors_id);
				foreach ($db_action as $contributor)
					UtilsHelper::push_array_if_not_exists($contributor, $contributors_id);
				$db_contributors = PHDB::findByIds(Person::COLLECTION, $contributors_id, ["name", "profilImageUrl"]);
				$output = [];
				foreach ($db_contributors as $id => $contributor)
					$output[] = [
						"id" => $id,
						"name" => $contributor["name"],
						"image" => $contributor["profilImageUrl"] ?? Yii::app()->getModule("co2")->assetsUrl . "/images/thumb/default_citoyens.png",
						"action_contributor" => in_array($id, $db_action)
					];
				$output = Rest::json($output);
				break;
			case "action_parent": 
				$action_id = $_POST['id'];
				$db_action = PHDB::findOneById(Action::COLLECTION, $action_id, [
					"name",
					"creator",
					"parentId",
					"parentType"
				]);
				$action = [
					"id" => $action_id,
					"name" => $db_action["name"] ?? "undefined",
					"creator" => $db_action["creator"] ?? "undefined",
					"parentId" => $db_action["parentId"] ?? "undefined",
					"parent" => isset($db_action["parentId"]) ? 
						[ 
							$db_action["parentId"] => [ 
								"id" => $db_action["parentId"] ?? "undefined",
								"type" => $db_action["parentType"] ?? "undefined"
							]
						] : []
				];
				$output = Rest::json($action);
				break;
			case "action_basic_info":
				$action_id = $_POST['id'];
				$user_can_edit = false;
				$userid = $_POST["user"] ?? Yii::app()->session["userId"];
				$db_action = PHDB::findOneById(Action::COLLECTION, $action_id, [
					"name",
					"creator",
					"shortDescription",
					"description",
					"media.images",
					"startDate",
					"noStartDate",
					"created",
					"endDate",
					"links.contributors",
					"credits",
					"tracking",
					"status",
					"timeZone",
					"tags",
					'tasks',
					"milestone.milestoneId",
				]);
				// can edit if creator
				$user_can_edit = !empty($userid) && !empty($db_action["creator"]) && $db_action["creator"] === $userid;
				$datetime = new DateTime();
				$contributors_id = $db_action["links"]["contributors"] ?? [];
				$contributors_id = array_filter($contributors_id, fn($consumer) => ($consumer["type"] == Person::COLLECTION), ARRAY_FILTER_USE_BOTH);
				$contributors_id = array_keys($contributors_id);
				// can edit if contributor
				$user_can_edit =  !$user_can_edit && in_array($userid, $contributors_id);
				if (!empty($db_action["timeZone"]))
					$datetime->setTimezone(new DateTimeZone($db_action["timeZone"]));
				$start_date = $db_action["startDate"] ?? null;
				$end_date = $db_action["endDate"] ?? null;
				if (is_numeric($start_date)) {
					$datetime->setTimestamp($start_date);
					$start_date = $datetime->format("c");
				} else if ($start_date instanceof MongoDate)
					$start_date = $start_date->toDateTime()->format("c");
				else if (filter_var($db_action["noStartDate"] ?? false, FILTER_VALIDATE_BOOLEAN))
					$start_date = null;
				if ($end_date instanceof MongoDate)
					$end_date = $end_date->toDateTime()->format("c");
				else if ($start_date === null)
					$end_date = null;
				$dbCreator = PHDB::findOneById(Person::COLLECTION, $db_action['creator'] ?? null, ["name", "profilImageUrl"]);
				$db_contributors = PHDB::findByIds(Person::COLLECTION, $contributors_id, ["name", "profilImageUrl"]);
				$creator = [
					"name"  => $dbCreator["name"] ?? Yii::t("common", "undefined"),
					"image" => $dbCreator["profilImageUrl"] ?? Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_citoyens.png',
				];
				$contributors = array_map(function ($consumer) {
					return [
						"id"    => (string)$consumer["_id"],
						"name"  => $consumer["name"],
						"image" => $consumer["profilImageUrl"] ?? Yii::app()->getModule("co2")->getAssetsUrl() . "/images/thumb/default_citoyens.png",
					];
				}, $db_contributors);
				$contributors = array_values($contributors);
				$image_ids = !empty($db_action["media"]["images"]) && is_array($db_action["media"]["images"]) ? $db_action["media"]["images"] : [];
				$db_images = PHDB::find(Document::COLLECTION, [
					"_id"    => [
						"\$in" => array_map(function ($consumer) {
							return new MongoId($consumer);
						}, $image_ids),
					],
					"name"   => ["\$exists" => 1],
					"folder" => ["\$exists" => 1],
				]);
				$images = [];
				foreach ($db_images as $id => $db_image) {
					$db_image["docPath"] = Document::getDocumentPath($db_image, true);
					$images[] = [
						"src"	=> "upload/communecter/" . $db_image["folder"] . "/" . $db_image["name"],
						"name"	=> $db_image["name"],
						"id"	=> $id,
						"full"	=> $db_image
					];
				}
				$action = [
					"id"                => $action_id,
					'name'              => $db_action['name'],
					'creator'           => $creator,
					"images"            => $images,
					"description"		=> $db_action["description"] ?? "",
					"start_date"        => $start_date,
					"end_date"          => $end_date,
					"contributors"      => $contributors,
					"credits"           => $db_action["credits"] ?? null,
					"tags"				=> $db_action["tags"] ?? [],
					"status"			=> $db_action["status"] ?? "todo",
					"tracking"			=> filter_var($db_action["tracking"] ?? false, FILTER_VALIDATE_BOOLEAN),
					"editable"			=> $user_can_edit || Authorisation::canEdit($userid, $action_id, Action::COLLECTION),
					"milestone"			=> $db_action["milestone"]["milestoneId"] ?? null,
					'has_tasks'			=> !empty($db_action['tasks']) && is_array($db_action['tasks'])
				];
				$action["description"] = Markdown::process($action["description"]);
				$output = Rest::json($action);
				break;
			case "last_image_of":
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST["id"], [
					"name",
					"media.images",
				]);
				$last_image = $db_action["media"]["images"] ?? [];
				$last_image = end($last_image);
				$output = Rest::json([
					"name" => $db_action["name"],
					"url"  => Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumbnail-default.jpg',
				]);
				if (!empty($last_image)) {
					$db_image = PHDB::findOneById(Document::COLLECTION, $last_image);
					$output = Rest::json([
						"name" => $db_image["name"],
						"url"  => Document::getDocumentPath($db_image, true),
					]);
				}
				break;
			case "milestone_data":
				if (empty($_POST["project"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Need for project id"
					]));
				$db_project = PHDB::findOneById(Project::COLLECTION, $_POST["project"], ["oceco.milestones"]);
				$milestones = [];
				if (!empty($db_project["oceco"]["milestones"])) {
					foreach ($db_project["oceco"]["milestones"] as &$milestone) {
						if (!empty($milestone["startDate"]))
							$milestone["startDate"] = $milestone["startDate"]->toDateTime()->format("c");
						if (!empty($milestone["endDate"]))
							$milestone["endDate"] = $milestone["endDate"]->toDateTime()->format("c");
						$milestones[] = $milestone;
					}
				}
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $milestones
				]));
				break;
			case "action_create_data":
				if (empty($_POST["project"]))
					return (Rest::json([
						"success"	=> 0,
						"content"	=> "Need for project id"
					]));
				$content = [
					"tags"			=> [],
					"milestones"	=> []
				];
				$db_project = PHDB::findOneById(Project::COLLECTION, $_POST["project"], ["oceco.milestones", "parent", "tags"]);
				$db_parent = null;
				if (!empty($db_project["tags"]) && is_array($db_project["tags"])) {
					foreach ($db_project["tags"] as $tag)
						UtilsHelper::push_array_if_not_exists(trim($tag), $content["tags"]);
				}
				if (!empty($db_project["oceco"]["milestones"])) {
					foreach ($db_project["oceco"]["milestones"] as &$milestone) {
						if (!empty($milestone["startDate"]))
							$milestone["startDate"] = $milestone["startDate"]->toDateTime()->format("c");
						if (!empty($milestone["endDate"]))
							$milestone["endDate"] = $milestone["endDate"]->toDateTime()->format("c");
						$content["milestones"][] = $milestone;
					}
				}
				if (!empty($db_project["parent"])) {
					$db_parent = array_keys($db_project["parent"]);
					$db_parent = end($db_parent);
					$db_parent = PHDB::findOneById($db_project["parent"][$db_parent]["type"], $db_parent, ["oceco.tags"]);
					if (!empty($db_parent["oceco"]["tags"]) && is_array($db_parent["oceco"]["tags"])) {
						foreach ($db_parent["oceco"]["tags"] as $tag)
							UtilsHelper::push_array_if_not_exists(trim($tag), $content["tags"]);
					}
				}
				return (Rest::json([
					"success"	=> 1,
					"content"	=> $content
				]));
				break;
			case 'action_detail_html':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], [
					'name',
					'parentId',
					'parentType',
					'parent',
					'creator',
					'status',
					'links.contributors',
					'description',
					'tags',
					'max',
				]);
				$action_parent = [];
				if (!empty($db_action["parentId"]) && !empty($db_action["parentType"]))
					$action_parent = [
						"id"		=>	$db_action["parentId"],
						"type"		=>	$db_action["parentType"]
					];
				else {
					$parent = array_keys($db_action["parent"])[0];
					$action_parent = [
						"id"		=>	$parent,
						"type"		=>	$db_action["parent"][$parent]["type"]
					];
				}
				$db_project = PHDB::findOneById($action_parent['type'], $action_parent['id'], ["name", "parentId", "parentType", "parent", "oceco.milestones"]);
				$milestones = !empty($db_project["oceco"]["milestones"]) && is_array($db_project["oceco"]["milestones"]) ? $db_project["oceco"]["milestones"] : [];
				$db_actions = Action::selectOrderedByStatus($action_parent, [], [
					'name',
					'media',
					'status',
					'tags',
					'tracking',
					"milestone"
				]);
				$actions = [];
				$db_action_media = [];
				foreach ($db_actions as $db_action) {
					$actions[] = [
						'id'		=> $db_action["_id"],
						'name'		=> $db_action['name'],
						'status'	=> isset($db_action['status']) ? $db_action['status'] : 'todo',
						'tags'		=> isset($db_action['tags']) ? $db_action['tags'] : [],
						'tracking'	=> isset($db_action['tracking']) ? $db_action['tracking'] : '',
						'image'		=> !empty($db_action['media']['images']) ? end($db_action['media']['images']) : null,
						"milestone"	=> !empty($db_action["milestone"]["milestoneId"]) ? $db_action["milestone"]["milestoneId"] : null
					];
					if (!empty($db_action['media']['images']))
						UtilsHelper::push_array_if_not_exists(end($db_action['media']['images']), $db_action_media);
				}
				$db_image_list = PHDB::findByIds(Document::COLLECTION, $db_action_media, ['folder', 'name']);
				$db_image_list = array_values($db_image_list);
				foreach ($actions as $id => $action) {
					$image = array_filter($db_image_list, fn($image_item) => ($action['image'] === (string) $image_item['_id']));
					$image = !empty($image) ? end($image) : [];
					$action['image'] = [
						'name' => $image['name'] ?? $action['name'],
						'url'  => !empty($image['folder']) && !empty($image['name']) ? "/upload/communecter/" . $image['folder'] . "/" . $image['name'] : Yii::app()->getModule('co2')->getAssetsUrl() . '/images/thumbnail-default.jpg',
					];
					$action["view_status"] = ACtion::get_action_status($action);
					$actions[$id] = $action;
				}
				$actions = Lists::arrange_action_by_status($action_parent, $actions, ["discuter", "next", "todo", "tracking", "totest", "done"]);
				$output = $this->getController()->renderPartial('costum.views.custom.appelAProjet.project_action_modal', [
					'parent_id'		=>	$action_parent["id"],
					'server_url'	=>	$_POST['server_url'] ?? Yii::app()->getBaseUrl(),
					'parent_type'	=>	$action_parent["type"],
					'parent_name'	=>	$db_project["name"],
					'milestones'	=>	$milestones,
					'action_list'	=>	$actions,
					'selected'		=>	$_POST['id'],
					'parent_room'	=>	Room::getOrCreateActionRoom(['parentId' => $action_parent['id'], 'parentType' => $action_parent["type"]]),
					"user"			=>	$_POST["connectedUser"] ?? Yii::app()->session["userId"]
				]);
				break;
			case 'task':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['tasks']);
				$task = $db_action['tasks'][$_POST['task']];

				if (!empty($task['createdAt'])) {
					$created_timestamp = new DateTime();
					$created_timestamp->setTimestamp(UtilsHelper::get_as_timestamp(['createdAt'], $task));
					$task['createdAt'] = $created_timestamp->format('c');
				}

				if (!empty($task['checkedAt'])) {
					$checked_timestamp = new DateTime();
					$checked_timestamp->setTimestamp(UtilsHelper::get_as_timestamp(['checkedAt'], $task));
					$task['checkedAt'] = $created_timestamp->format('c');
				}
				$output = Rest::json($task);
				break;
			case 'task_html':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['tasks']);
				$output = $this->getController()->renderPartial('costum.views.custom.appelAProjet.tasks_list', [
					'tasksList' => $db_action,
					'selected'   => $_POST['action'],
				]);
				break;
			case 'mindmap_html': {
				$output = $this->getController()->renderPartial('costum.views.tpls.blockCms.map.mindmap', [
					'mind_id' => $_POST['id'],
					'mind_type' => $_POST['type'],
					'mind_slug'   => $_POST['slug'],
					'blockKey' => $_POST['id'],
					'blockCms' => array(
						'_id' => $_POST['id'],
					),
					'kunik' => 'mindmap'.$_POST['id']
				]);
				break;
			}
			case 'task_recap':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['id'], ['tasks']);
				$recap = [
					'total'      => !empty($db_action['tasks']) ? count($db_action['tasks']) : 0,
					'done'       => 0,
					'percentage' => 0,
				];
				if (!empty($db_action['tasks'])) {
					foreach ($db_action['tasks'] as $task) {
						if ($task['checked']) $recap['done']++;
					}
				}
				$recap['percentage'] = $recap['total'] === 0 ? 0 : ceil($recap['done'] * 100 / $recap['total']);
				$output = Rest::json($recap);
				break;
			case 'task_detail':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['tasks']);
				$db_task = $db_action['tasks'][$_POST['index']];

				$user = null;
				$task_person_list = [];
				$this_task_contributor_list = [];
				$personQueryIds = [];
				$is_contributor_exists = !empty($db_task['contributors']) && is_array($db_task['contributors']);

				// Préparation des requêtes vers la base de donénes
				if (!empty($db_task['userId'])) UtilsHelper::push_array_if_not_exists($db_task['userId'], $personQueryIds);
				if ($is_contributor_exists) {
					$filteredContributorKeyList = array_filter($db_task['contributors'], function ($id) use ($personQueryIds) {
						return !in_array($id, $personQueryIds);
					}, ARRAY_FILTER_USE_KEY);
					$filteredContributorKeyList = array_keys($filteredContributorKeyList);
					foreach ($filteredContributorKeyList as $oneFilteredContributorKey) UtilsHelper::push_array_if_not_exists($oneFilteredContributorKey, $personQueryIds);
				}

				// Charger la liste des personnes
				$db_person_list = PHDB::findByIds(Person::COLLECTION, $personQueryIds, ['name']);
				foreach ($db_person_list as $db_person_id => $db_person_item) {
					$task_person_list[$db_person_id] = [
						'id'   => $db_person_id,
						'name' => $db_person_item['name'] ?? '',
					];
				}

				// Récupération des valeurs
				if (!empty($db_task['userId'])) $user = $task_person_list[$db_task['userId']];
				if ($is_contributor_exists)
					$this_task_contributor_list = array_map(fn($id) => ($task_person_list[$id]), array_keys($db_task['contributors']));
				$task = [
					'id'           => $db_task['taskId'] ?? '',
					'name'         => $db_task['task'] ?? '(No name)',
					'user'         => $user,
					'checked'      => !empty($db_task['checked']) ? filter_var($db_task['checked'], FILTER_VALIDATE_BOOL) : false,
					'contributors' => $this_task_contributor_list,
				];
				$output = Rest::json($task);
				break;
			case 'invite_html':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['parentId', 'parentType', 'links']);
				$output = $this->getController()->renderPartial('costum.views.custom.appelAProjet.invite_on_project', [
					'selected'   => $_POST['action'],
					'info' => $db_action
				]);
				break;
			case 'invite_to_project_html':
				$db_action = PHDB::findOneById(Project::COLLECTION, $_POST['project'], ['parentId', 'parentType', 'links']);
				$db_action["parentId"] = $_POST['project'];
				$db_action["parentType"] = Project::COLLECTION;
				$output = $this->getController()->renderPartial('costum.views.custom.appelAProjet.invite_on_project', [
					'selected'   => $_POST['project'],
					'info' => $db_action
				]);
				break;
			case 'contribution_rocker_chat':
				$messages = [];
				if (!empty($_POST['action']) && !empty($_POST['project_name']) && !empty($_POST['contributors'])) {
					$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['name']);
					foreach ($_POST['contributors'] as $contributor) {
						$dbPersonsList = PHDB::findOneById(Person::COLLECTION, $contributor, ['username']);
						$message_list = ' @' . $dbPersonsList['username'] . ' participe à l\'action **' . $db_action['name'] . '** de **' . $_POST["project_name"] . '**';
						$messages[] = $message_list;
						if (isset(Yii::app()->params['rocketchatServiceEnabled']))
							RocketChat::post($_POST['channel'], $message_list, '', true);
					}
				}
				$output = Rest::json(['status' => true, 'msg' => $messages]);
				break;
			case 'new_action_rocker_chat':
				$messages = [];
				if (!empty($_POST['action_name']) && !empty($_POST['project'])) {
					$db_project = PHDB::findOneById(Project::COLLECTION, $_POST['project'], ['name', 'slug']);
					$message_list = ' @' . Yii::app()->session['user']['username'] . ' a ajouté une action **' . $_POST['action_name'] . '** dans **' . $db_project['name'] . '**';
					$messages[] = $message_list;
					if (isset(Yii::app()->params['rocketchatServiceEnabled']))
						RocketChat::post($db_project['slug'], $message_list, '', true);
				}
				$output = Rest::json(['status' => true, 'msg' => $messages]);
				break;
			case 'actions_by_project_id':
				if (Authorisation::isElementMember($userId, Project::COLLECTION, $_POST["id"])) {
					$where = ["parentId" => $_POST["id"]];
					if (!empty($_POST['personal'])) {
						$where['$or'] = [
							["creator" => $_POST['personal']],
							["links.contributors." . $_POST['personal'] => ['$exists' => true]],
							["tasks.contributors." . $_POST['personal'] => ['$exists' => true]],
						];
					}
					$output = PHDB::find(Action::COLLECTION, $where);
					$output = Rest::json(
						["result" => true, "data" => $output]
					);
				} else {
					$output = Rest::json(
						["result" => false, "msg" => Yii::t("form", "Visible only to the project's contributors")]
					);
				}
				break;
			case 'add_image_media':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['parentId', 'parentType', 'media']);
				$db_action_media = [
					'type'        => 'gallery_images',
					'countImages' => 0,
					'images'      => $db_action['media']['images'] ?? [],
					'files'       => $db_action['media']['files'] ?? [],
				];
				$data_type_map = [
					'image/jpeg' => 'jpg',
					'image/png'  => 'png',
					'image/webp' => 'webp',
				];
				$base64_list = array_map(function ($image) {
					$b64_tag = ';base64,';
					$tag_pos = strpos($image, $b64_tag);
					$start = strpos($image, 'data:') + strlen('data:');
					return [
						'content' => substr($image, $tag_pos + strlen($b64_tag)),
						'type'    => substr($image, $start, $tag_pos - $start),
					];
				}, $_POST['images']);
				$upload_path = "upload/communecter/actions/" . $_POST['action'];
				$rootDirectory = $_SERVER['DOCUMENT_ROOT'] . '/' . $upload_path;
				if (!is_dir($rootDirectory)) mkdir($rootDirectory, 0755);
				$message_list = [];
				foreach ($base64_list as $listIndex => $oneBase64) {
					$decoded = base64_decode($oneBase64['content']);
					$extension = $data_type_map[$oneBase64['type']];
					$filename = uniqid() . '-' . $listIndex . '.' . $extension;
					$filepath = "$rootDirectory/$filename";
					file_put_contents($filepath, $decoded);
					if (file_exists($filepath)) {
						$message_list[] = [
							'status' => true,
							'name'   => $filename,
							'url'    => "$upload_path/$filename",
							'msg'    => "$filename créé avec succès",
						];
						// Enregistrer dans document
						$n_document = [
							'id'         => $_POST['action'],
							'type'       => "action",
							'folder'     => "actions/" . $_POST['action'],
							'moduleId'   => 'communecter',
							'author'     => $userId,
							'name'       => $filename,
							'size'       => strlen($decoded),
							'doctype'    => 'image',
							'created'    => time(),
							'collection' => Document::COLLECTION,
						];
						Yii::app()->mongodb->selectCollection(Document::COLLECTION)->insert($n_document);
						$db_action_media['images'][] = (string)$n_document['_id'];
						$db_action_media['countImages'] = count($db_action_media['images']);
					} else
						$message_list[] = [
							'status' => false,
							'name'   => $filename,
							'msg'    => "Erreur d'écriture dans le chemin : $filepath",
						];
				}
				PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['action'])], ['$set' => ['media' => $db_action_media]]);
				// update image list
				$image_list = [];
				$db_image_list = PHDB::findByIds(Document::COLLECTION, $db_action_media['images'], ['name', 'folder']);
				foreach ($db_image_list as $id => $db_image_item) {
					$image_list[] = [
						'id'   => $id,
						'name' => $db_image_item['name'],
						'url'  => 'upload/communecter/' . $db_image_item['folder'] . '/' . $db_image_item['name'],
					];
				}
				$parameter = ['action' => $_POST['action'], 'images' => $image_list];
				if (!empty($_POST['emiter'])) {
					$parameter['emiter'] = $_POST['emiter'];
				}
				Self::emitActionEvent('.update-images' . $db_action['parentId'], $parameter);
				Self::emitActionEvent('.action-image', $parameter);
				$output = Rest::json($message_list);
				break;
			case 'add_document_media':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['parentId', 'parentType', 'media']);
				$db_action_media = [
					'type'        => 'gallery_images',
					'countImages' => 0,
					'images'      => $db_action['media']['images'] ?? [],
					'files'       => $db_action['media']['files'] ?? [],
				];
				$base64_list = array_map(function ($oneDocumentMap) {
					$b64_tag = ';base64,';
					$tag_pos = strpos($oneDocumentMap['data'], $b64_tag);
					$start = strpos($oneDocumentMap['data'], 'data:') + strlen('data:');
					return [
						'content' => substr($oneDocumentMap['data'], $tag_pos + strlen($b64_tag)),
						'type'    => substr($oneDocumentMap['data'], $start, $tag_pos - $start),
						'name'    => str_replace(' ', '-', $oneDocumentMap['name']),
					];
				}, $_POST['documents']);
				$rootDirectory = $_SERVER['DOCUMENT_ROOT'] . '/upload/communecter/actions/' . $db_action['parentId'] . '/';
				if (!is_dir($rootDirectory)) mkdir($rootDirectory, 0755);

				$message_list = [];
				foreach ($base64_list as $listIndex => $oneBase64) {
					$decoded = base64_decode($oneBase64['content']);
					$filename = $oneBase64['name'];
					$filepath = "$rootDirectory/$filename";
					$writeFile = file_put_contents($filepath, $decoded);
					if (file_exists($filepath)) {
						$message_list[] = [
							'status' => true,
							'name'   => $filename,
							'url'    => '/upload/communecter/actions/' . $db_action['parentId'] . '/' . $filename,
							'msg'    => "$filename créé avec succès",
						];

						// Enregistrer dans document
						$n_document = [
							'id'         => $db_action['parentId'],
							'type'       => 'actions',
							'folder'     => 'actions/' . $db_action['parentId'],
							'moduleId'   => 'communecter',
							'author'     => $userId,
							'name'       => $filename,
							'size'       => strlen($decoded),
							'doctype'    => 'file',
							'created'    => time(),
							'collection' => Document::COLLECTION,
						];
						Yii::app()->mongodb->selectCollection(Document::COLLECTION)->insert($n_document);
						$db_action_media['files'][] = (string)$n_document['_id'];
						$db_action_media['countImages'] = count($db_action_media['images']);
					} else {
						$message_list[] = [
							'status' => false,
							'name'   => $filename,
							'msg'    => "Erreur d'écriture dans le chemin : $filepath",
						];
					}
				}
				PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['action'])], ['$set' => ['media' => $db_action_media]]);

				// update document list
				$documentsList = [];
				$dbDocumentsList = PHDB::findByIds(Document::COLLECTION, $db_action_media['files'], ['name', 'folder']);
				foreach ($dbDocumentsList as $documentId => $dbDocument) {
					$documentsList[] = [
						'id'   => $documentId,
						'name' => $dbDocument['name'],
						'url'  => '/upload/communecter/' . $dbDocument['folder'] . '/' . $dbDocument['name'],
					];
				}
				$parameter = ['action' => $_POST['action'], 'documents' => $documentsList];
				if (!empty($_POST['emiter'])) {
					$parameter['emiter'] = $_POST['emiter'];
				}
				Self::emitActionEvent('.update-documents' . $db_action['parentId'], $parameter);
				$output = Rest::json($message_list);
				break;
			case 'set_contributors':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['links.contributors', 'parentId', "min", "max"]);
				$contributorList = !empty($db_action['links']['contributors']) && is_array($db_action['links']['contributors']) ? $db_action['links']['contributors'] : [];
				// remove existing
				$join = null;
				$quit = [];
				$contributor_post = array_keys(array_filter($contributorList, fn($c) => $c['type'] === Person::COLLECTION));
				if (isset($_POST['contributors']))
					$contributor_post = is_array($_POST['contributors']) ? $_POST['contributors'] : [];
				else if (!empty($_POST['contributor'])) {
					$participate = intval($_POST['participate']);
					if ($participate) 
						$join = $_POST['contributor'];
					else
						$quit[] = $_POST['contributor'];
				}
				
				foreach ($contributorList as $id => $c) {
					if ($c['type'] === Person::COLLECTION && !in_array($id, $contributor_post))
						$quit[] = $id;
				}
				// set non existing
				foreach ($contributor_post as $c) {
					if (empty($contributorList[$c]))
						$contributorList[$c] = ['type' => Person::COLLECTION];
				}
				foreach ($quit as $q)
					unset($contributorList[$q]);
				
				if (!empty($join) && empty($contributorList[$join])) {
					$contributorList[$join] = ['type' => Person::COLLECTION];
				}
				if (isset($db_action["min"]) && intval($db_action["min"]) > count($contributorList))
					return \Rest::json([
						"success" => false,
						"content" => "You didn't reach the minimal number of contributor",
					]);
				if (isset($db_action["max"]) && intval($db_action["max"]) < count($contributorList))
					return \Rest::json([
						"success" => false,
						"content" => "You have exceed the max authorized of contributor number",
					]);
				$updatedAction = PHDB::update(Action::COLLECTION, ['_id' => new MongoId($_POST['action'])], [
					'$set' => ['links.contributors' => $contributorList],
				]);
				$outputPersonList = [];
				if (filter_var($updatedAction['ok'], FILTER_VALIDATE_BOOLEAN)) {
					$dbPersons = PHDB::findByIds(Person::COLLECTION, array_keys($contributorList), ['name', 'profilThumbImageUrl']);
					foreach ($dbPersons as $personId => $onePerson) {
						$outputPersonList[] = [
							'id'    => $personId,
							'name'  => $onePerson['name'],
							'image' => $onePerson['profilThumbImageUrl'] ?? Yii::app()
								->getModule('co2')
								->getAssetsUrl() . '/images/thumb/default_citoyens.png',
						];
					}
					$output = \Rest::json([
						"success" => true,
						"content" => $outputPersonList,
					]);
				} else
					$output = \Rest::json([
						"success" => true,
						"content" => [],
					]);
				$parameter = [
					'action'       => $_POST['action'],
					'contributors' => $outputPersonList,
				];
				if (!empty($_POST['emiter']))
					$parameter['emiter'] = $_POST['emiter'];
				Self::emitActionEvent('.set-contributors' . $db_action['parentId'], $parameter);
				Self::emitActionEvent('.cd-int-contributors', $parameter);
				break;
			case "get_tags":
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST["id"], ["tags"]);
				$output = !empty($db_action["tags"]) && is_array($db_action["tags"]) ? $db_action["tags"] : [];
				$output = Rest::json($output);
				break;
			case "set_status":
				$user = !empty($_POST['user']) ? $_POST['user'] : Yii::app()->session['userId'];
				if (empty($user))
					return (Rest::json([
						'success'	=> false,
						'content'	=> "Aucun utilisateur n'est désigné",
					]));
				$db_user = PHDB::findOneById(Person::COLLECTION, $user, ['username']);
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST["id"], [
					'name',
					'parent',
					'parentId',
					'parentType',
					"tags",
					"status",
					"startDate",
					"endDate",
					'updateStatus'
				]);
				$update_status = isset($db_action['updateStatus']) && is_array($db_action['updateStatus']) ? $db_action['updateStatus'] : [];
				$set = [
					"status" => $db_action["status"] ?? "todo",
					"tags" => isset($db_action["tags"]) && is_array($db_action["tags"]) ? $db_action["tags"] : [],
					"tracking" => false,
					"updated" => time(),
				];
				$status_tags = ["discuter", "next", "totest"];
				$update_status[] = [
					'status'	=>	$_POST['status'],
					'author'	=>	$user,
					'update'	=>	new MongoDate()
				];
				$set['updateStatus'] = $update_status;
				switch ($_POST["status"]) {
					case "discuter":
					case "totest":
					case "next":
						foreach ($status_tags as $a_status) {
							$index = array_search($a_status, $set["tags"]);
							if ($index !== false)
								unset($set["tags"][$index]);
						}
						$set["tags"][] = $_POST["status"];
						$set["status"] = "todo";
						if ($_POST["status"] !== "totest")
							$unset = [
								"startDate" => null,
								"endDate" => null,
							];
						break;
					case "tracking":
						foreach (["discuter", "totest"] as $a_status) {
							$index = array_search($a_status, $set["tags"]);
							if ($index !== false)
								unset($set["tags"][$index]);
						}
						$set["status"] = "todo";
						$set["tracking"] = true;
						$set["startDate"] = new MongoDate();
						$unset = [
							"endDate" => null,
						];
						break;
					case "todo":
					case "done":
						$set["status"] = $_POST["status"];
						foreach ($status_tags as $a_status) {
							$index = array_search($a_status, $set["tags"]);
							if ($index !== false)
								unset($set["tags"][$index]);
						}
						if ($_POST["status"] === "done" && empty($set["endDate"]))
							$set["endDate"] = new MongoDate();
						else if ($_POST["status"] === "todo")
							$unset = [
								"startDate" => null,
								"endDate" => null,
							];
						break;
				}
				$set["tags"] = array_values($set["tags"]);
				$options = [];
				if (isset($set))
					$options['$set'] = $set;
				if (isset($unset))
					$options['$unset'] = $unset;
				PHDB::update(Action::COLLECTION, ["_id" => new MongoId($_POST["id"])], $options);
				$message = ' @' . $db_user['username']. ' a mis à jours le status de l\'action **' . str_replace('*', "\*", $db_action['name']) . '** à **' . \Yii::t('common', 'act.' . $_POST['status']) . '**';
				$set['message'] = $message;
				if (!empty($db_action['parent']) && is_array($db_action['parent'])) :
					foreach ($db_action['parent'] as $parent_key => $parent) :
						$db_parent = PHDB::findOneById($parent['type'], $parent_key, ['slug', 'tools.chat.int']);
						$this->do_send_rc_message($db_parent, $message);
					endforeach;
				elseif (!empty($db_action['parentType']) && !empty($db_action['parentId'])) :
					$db_parent = PHDB::findOneById($db_action['parentType'], $db_action['parentId'], ['slug', 'tools.chat.int']);
					$this->do_send_rc_message($db_parent, $message);
				endif;
				$output = Rest::json([
					'success'	=> 1,
					'content'	=> $options,
				]);
				break;
			case 'socket_task_update':
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST['action'], ['tasks', 'parentId']);
				$recap = [
					'done'  => 0,
					'total' => 0,
				];
				$db_tasks = !empty($db_action['tasks']) && is_array($db_action['tasks']) ? $db_action['tasks'] : [];
				$recap['total'] = count($db_tasks);
				foreach ($db_tasks as $db_task) {
					if (filter_var($db_task['checked'] ?? false, FILTER_VALIDATE_BOOLEAN))
						$recap['done']++;
				}
				$parameter = [
					'action'    => $_POST['action'],
					'operation' => $_POST['operation'],
					'content'   => $_POST['content'],
					'index'     => $_POST['index'] ?? $recap['total'] - 1,
					'recap'     => $recap,
				];
				if (!empty($_POST['emiter'])) {
					$parameter['emiter'] = $_POST['emiter'];
				}
				$output = Self::emitActionEvent('.update-task' . $db_action['parentId'], $parameter);
				$output = Self::emitActionEvent('.cd-int-task', $parameter);
				break;
			case 'ping_contribution':
				$actionId = $_POST['id'];
				$updateContributorsList = !empty($_POST['ids']) && is_array($_POST['ids']) ? $_POST['ids'] : [];
				$action = $_POST['action'];
				$db_action = PHDB::findOneById(Action::COLLECTION, $actionId, ['name', 'parentId', 'max', 'links.contributors']);
				$parentId = $db_action['parentId'] ?? '';
				$ids = $db_action['links']['contributors'] ?? [];
				$ids = array_filter($ids, function ($contributor) {
					return !empty($contributor['type']) && $contributor['type'] === Person::COLLECTION;
				}, ARRAY_FILTER_USE_BOTH);
				$ids = array_keys($ids);
				$dbPersons = PHDB::findByIds(Person::COLLECTION, $ids, ['name', 'profilImageUrl']);
				$contributors = [];
				foreach ($dbPersons as $personId => $dbPerson) {
					if (empty($dbPerson['name'])) {
						continue;
					}
					$contributors[] = [
						'id'    => $personId,
						'name'  => $dbPerson['name'],
						'image' => $dbPerson['profilImageUrl'] ?? Yii::app()
							->getModule('co2')
							->getAssetsUrl() . '/images/thumb/default_citoyens.png',
					];
				}
				$parameter = [
					'target'       => $actionId,
					'contributors' => $contributors,
					'updates'      => $updateContributorsList,
					'action'       => $action,
					'info'         => [
						'name' => $db_action['name'],
						'max'  => intval($db_action['max'] ?? 1),
					],
				];
				if (!empty($_POST['emiter'])) {
					$parameter['emiter'] = $_POST['emiter'];
				}
				$output = Rest::json(Self::emitActionEvent(".set-contributors$parentId", $parameter));
				break;
			case 'aap-pnotify-project':
				$projectId = $_POST['project'];
				$actionId = $_POST['action'];
				$statuses = [
					'discuter'	=>	Yii::t("common", "act.discuter"),
					'next'	=>	Yii::t("common", "act.next"),
					'todo'		=>	Yii::t("common", "act.todo"),
					'tracking'	=>	Yii::t("common", "act.tracking"),
					'totest'	=>	Yii::t("common", "act.totest"),
					'done'		=>	Yii::t("common", "act.done"),
				];
				$dbProject = PHDB::findOneById(Project::COLLECTION, $projectId, ['links.contributors', 'parent']);
				$dbProjectParent = PHDB::findOneById(array_values($dbProject['parent'])[0]['type'], array_keys($dbProject['parent'])[0], ['oceco.kanbanColumns.' . Yii::app()->language]);
				foreach ($statuses as $index => $status) {
					$statuses[$index] = $dbProjectParent['oceco']['kanbanColumns'][Yii::app()->language][$index] ?? $status;
				}
				$db_action = PHDB::findOneById(Action::COLLECTION, $actionId, ['name']);
				$dbContributorList = array_keys($dbProject['links']['contributors'] ?? []);
				$data = [
					'action'		=> $actionId,
					'actionName'	=> $db_action['name'],
					'authorId'		=> Yii::app()->session['userId'],
					'authorName'	=> Yii::app()->session['user']['name'],
					'target'		=> $_POST['target'],
					'targetText'	=> $statuses[$_POST['target']],
					'position'		=> $_POST['position'],
					'publics'		=> $dbContributorList,
					'component'		=> $_POST['component'] ?? null,
					'after'			=> $_POST["after"] ?? null,
				];
				if (!empty($_POST['emiter']))
					$data['emiter'] = $_POST['emiter'];

				$output = Rest::json([
					self::emitActionEvent(".aap-kanban-move$projectId", $data),
					self::emitActionEvent(".panel-action-move$projectId", $data),
					self::emitActionEvent(".cd-int-move", $data),
				]);
				break;
			case "set_room":
				$db_action = PHDB::findOneById(Action::COLLECTION, $_POST["id"], ["parentId", "parentType"]);
				$db_parent = PHDB::findOneById($db_action["parentType"], $db_action["parentId"]);
				if ((empty($db_parent["parentType"]) || empty($db_parent["parentId"])) && !empty($db_parent["links"]["organizer"])) {
					$first_organizer_key = array_keys($db_parent["links"]["organizer"])[0];
					$db_parent["parentType"] = $db_parent["links"]["organizer"][$first_organizer_key]["type"];
					$db_parent["parentId"] = $first_organizer_key;
				}
				$db_exists = PHDB::findOne(Room::COLLECTION, ['parentId' => $db_action["parentId"], 'parentType' => $db_action["parentType"]], ["_id"]);
				$room_id = !empty($db_exists["_id"]) ? (string) $db_exists["_id"] : "";
				if (empty($db_exists)) $room_id = (string) Room::insert($db_parent, $db_action["parentType"])["_id"];
				$set = [
					"idParentRoom"	=>	$room_id
				];
				PHDB::update(Action::COLLECTION, ["_id" => new MongoId($_POST["id"])], ["\$set" => $set]);
				$output = Rest::json($room_id);
				break;
			default:
				$output = Rest::json(['status' => false, 'msg' => 'Bad request']);
				break;
		}
		Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
		Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
		Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
		return $output;
	}

	public static function emitActionEvent(string $event, $data = null) {
		$coWs = Yii::app()->params['cows'];
		$payload = [
			'event' => $event,
		];
		if (!empty($data)) $payload['data'] = $data;
		if (empty($coWs['pingActionManagement']))
			return (false);
		$curl = curl_init($coWs['pingActionManagement']);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($payload));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		return (curl_exec($curl));
	}

	private function do_send_rc_message(array &$parent, string $msg) {
		if (!empty($parent['tools']['chat']['int']) && is_array($parent['tools']['chat']['int'])) {
			foreach ($parent['tools']['chat']['int'] as $url) :
				if (is_array($url) && !empty($url['url']) && !empty($url['name'])) {
					$position = strpos($url['url'], 'channel');
					$position = $position === false ? -1 : $position;
					RocketChat::post(
						$url['name'],
						$msg, '',
						in_array($position, [0, 1]),
						"oceco",
						"https://oce.co.tools/avatar.png",
						"Lien oceco",
						true,
					);
				}
				else if (is_string($url) && !empty($parent['slug'])){
					$position = strpos($url, 'channel');
					$position = $position === false ? -1 : $position;
					RocketChat::post(
						$parent['slug'],
						$msg,
						'',
						in_array($position, [0, 1]),
						"oceco",
						"https://oce.co.tools/avatar.png",
						"Lien oceco",
						true,
					);
				}
			endforeach;
		} else if (!empty($parent['slug']))
			RocketChat::post($parent['slug'],
				$msg,
				'',
				true,
				"oceco",
				"https://oce.co.tools/avatar.png",
				"Lien oceco",
				true);
	}
}
