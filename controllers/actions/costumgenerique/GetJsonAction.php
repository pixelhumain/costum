<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique;
use CAction;
use CostumGenerique;
use Rest;

/**
 * 
 */
class GetJsonAction extends \PixelHumain\PixelHumain\components\Action
{
	
	public function run($id=null, $type=null, $slug=null, $view=null, $page=null)
	{
		$controller = $this->getController();
		$params = CostumGenerique::getJson($_POST["slug"]);
		return Rest::json($params);
	}
}
?>