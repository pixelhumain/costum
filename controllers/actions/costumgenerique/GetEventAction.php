<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique;
use CAction;
use CostumGenerique;
use Rest;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CostumGenerique::getEvent($_POST);
        return Rest::json($params);
    }
}