<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meuseCampagnes\admin;

use CAction, Meusecampagnes, Rest;
class SaveOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	$params = $_POST;
     // $params = Import::newStart($params);
      $res = Meusecampagnes::saveOrga($params);
      return Rest::json($res);
    }
}
?>