<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph;

use CAction, Form, Cms, Zone, PHDB, MongoId, Yii, Rest, Organization, DashboardData;
use Authorization, CacheHelper;

class GetDashboardDataAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($isExport=false, $format="csv", $levelId=null){
        //if(Yii::app()->session["userId"]){
            $id = $_POST["costumId"]??"";
            $type = $_POST["costumType"]??"";
            $slug = $_POST["costumSlug"]??"";
            $levelId = $_POST["scopeId"]??($levelId??null);
            $isByQuestion = $_POST["countByQuestion"]??null;
            $specificBlock = $_POST["specificBlock"]??[];
            $extraFilter = $_POST["extraFilter"]??null;

            $scopeQuery = array("coform" => array("shareToChildren" => true));
            if(isset($extraFilter) && is_array($extraFilter)){
                $scopeQuery["answers"] = $extraFilter;
            }
            $isSharedForm = false;
            $inScope = array();
            $communityIds = array();

            if(!is_array($specificBlock)){
                $specificBlock = [];
            }

            if($levelId!=null && $levelId!=""){
                $orga = Yii::$app->cache->get('zonesId'.$slug);
                if(!$orga){
                    $inScope = PHDB::find( Organization::COLLECTION , array(
                        "address.level3" => $levelId,
                        '$or' => array(
                            array("reference.costum"=>"franceTierslieux"),
                            array("source.keys"=>"franceTierslieux")
                        ) ), ["name", "collection"] );
                        
                    $orga = array();
                    foreach ($inScope as $key => $item) {
                        unset($item["_id"]);
                        array_push($communityIds, $key);
                        $item["type"] = $item["collection"];
                        unset($item["collection"]);
                        array_push($orga, [$key => $item]);
                    }
                    Yii::$app->cache->set('zonesId'.$slug, $orga, 3600);
                }
                if(isset($scopeQuery["answers"])){
                    $scopeQuery["answers"] = array("links.organizations" => array('$in'=>$orga) );
                }
            }
            
            // Init data to cache
            $cachedData = [];
            $cachedData = Yii::$app->cache->get('dashboardData'.$slug);

            $response = array();

            if($cachedData!=false && isset($cachedData["global"]) && isset($cachedData["data"]) && count($cachedData["data"])!=0){
                $response = $cachedData;
            }else{
                // Get context children forms and inputs 
                $ccQuery = array('$or'=>array(
                    array("parent.".$id=>['$exists'=>true])
                ) /*,"active"=>"true"*/);

                $costum = CacheHelper::getCostum();

                if( isset($scopeQuery["coform"]) && isset($levelId)){
                    array_push($ccQuery['$or'], $scopeQuery["coform"]);
                }
                
                $contextCoforms = PHDB::find(Form::COLLECTION, $ccQuery, ["name", "type", "config", "shareToChildren", "params", "subForms"]);

                // Get subforms from duplicated forms
                $mainFormsIds = array();
                foreach($contextCoforms as $formKey => $formValue){
                    if( isset($formValue["config"]) && (!isset($formValue["type"]) || (isset($formValue["type"]) && $formValue["type"]!="aap")) ){
                        array_push($mainFormsIds, $formValue["config"]);
                    }
                }

                if( count($mainFormsIds) > 0 ){
                    $mainForms = PHDB::findByIds(Form::COLLECTION, $mainFormsIds, ["name", "subForms", "params", "shareToChildren"]);
                    foreach($contextCoforms as $formKey => $formValue){
                        if( isset($formValue["config"]) && isset($mainForms[$formValue["config"]]) && (!isset($formValue["type"]) || (isset($formValue["type"]) && $formValue["type"]!="aap"))){
                            $contextCoforms[$formKey]["subForms"] = $mainForms[$formValue["config"]]["subForms"];
                            $contextCoforms[$formKey]["params"] = $mainForms[$formValue["config"]]["params"];
                        }
                    }
                }

                // Get subforms from duplicated forms
                $formInputs = [];
                $formList = [];

                foreach ($contextCoforms as $formKey => $formValue) {
                    $formList[$formKey] = !empty($formValue["name"]) ? $formValue["name"] : "";                
                    if((empty($formValue["type"])) || (isset($formValue["type"]) && ($formValue["type"]!="aap" || $formValue["type"]!="aapConfig") )){
                        if(isset($formValue["subForms"]) && is_array($formValue["subForms"]) && count($formValue["subForms"])!=0){
                            $subFormId = array('$in' => $formValue["subForms"]);
                        }
                        if(!empty($formValue["subForms"]) && is_string($formValue["subForms"]) && $formValue["subForms"] !=""){
                            $subFormId = $formValue["subForms"];
                        }
                        if(isset($subFormId) && is_array($subFormId) && $subFormId!=""){
                            $subForms = [];
                            try {
                                $subForms = PHDB::find(Form::COLLECTION, array('id' => $subFormId));
                            } catch (\Throwable $th) {}

                            if(count($subForms)!=0){
                                foreach ($subForms as $subFormKey => $subFormValue) {
                                    if(isset($subFormValue["inputs"])){
                                        $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                                    }
                                }
                            }
                        }
                    }
                }

                $response = array(
                    'global' => array(
                        'formTL' => $contextCoforms,
                        'coformList' => $formList,
                        'coformInputs' => $formInputs,
                    ),
                    'data' => [],
                    'exportable' => []
                );

                // get params from block cms
                $queryCms = array(
                    "parent.".$id=>['$exists'=>true],
                    '$or' => [
                        array('coform' => ['$exists' => true]),
                        array('path' => array('$in' => $specificBlock))
                    ]
                );

                if(isset($_POST["page"]) && $_POST["page"]!=""){
                    $queryCms["page"] = $_POST["page"];
                }
                
                $blockParams = PHDB::find(Cms::COLLECTION, $queryCms);
                $answersCountByForm = array();
                
                foreach ($blockParams as $key => $params) {
                    $path_parts = array_reverse(explode(".", $params['path']));
                    $name = isset($path_parts[0]) ? $path_parts[0]:"";
                    $group = isset($path_parts[1]) ? $path_parts[1]:"";

                    $target_classname = "PixelHumain\PixelHumain\modules\costum\components\blockCms\\".$group."\\".ucfirst($name)."Widget";
                    if(isset($params["coform"]) && !class_exists($target_classname)){
                        $sharedFormRequest = $scopeQuery;

                        if(!isset($contextCoforms[$params["coform"]]["shareToChildren"])){
                            unset($sharedFormRequest["answers"]);
                        }
                        
                        if(!isset($answersCountByForm[$params["coform"]])){
                            $aCountQuery = array('form' => $params["coform"], "answers"=>['$exists'=>true], "draft"=>['$exists'=>false]);
                            if(isset($sharedFormRequest["answers"])){
                                $aCountQuery = array_merge($aCountQuery, $sharedFormRequest["answers"]);
                            }
                            $answersCountByForm[$params["coform"]] = PHDB::count(Form::ANSWER_COLLECTION, $aCountQuery);
                        }
                        $answersCount = $answersCountByForm[$params["coform"]];
                        if($answersCount==0){
                            $answersCount = 1;
                        }
                        
                        if(isset($isByQuestion) && $isByQuestion){
                            $params["isByQuestion"] = true;
                        }

                        if(isset($params["path"])){
                            if(isset($params["answerPath"]) && strpos($params["answerPath"], "nbTLgeomapped")!==false){
                                $response["data"][$key] = DashboardData::nbTLgeomapped($id, count($inScope));
                            }else if(isset($params["answerPath"]) && strpos($params["answerPath"], "countByElementTags")!==false){
                                $response["data"][$key] = DashboardData::countByElementTags($id, $params, $levelId);
                            }else if(isset($params["answerPath"]) && strpos($params["answerPath"], "percentageByRegion")!==false){
                                $response["data"][$key] = DashboardData::percentageByRegion($params, $key, $levelId);
                            }else if(isset($params["answerPath"]) && strpos($params["answerPath"], "countQPV")!==false){
                                $response["data"][$key] = DashboardData::countQPV($id, $params, $levelId);
                            }else if(isset($params["answerPath"]) && strpos($params["answerPath"], "countByCodeInsee")!==false && strpos($params["path"], "progressCircle") !== false){
                                $response["data"][$key] = DashboardData::countByCodeInsee($id, $params["answerValue"], $levelId);
                            }else if(isset($params["answerPath"]) && strpos($params["answerPath"], "bail36mois")!==false){
                                $response["data"][$key] = DashboardData::bail36mois($params["coform"], $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "horizontalBar") !== false || strpos($params["path"], "chartForCoform") !== false || strpos($params["path"], "pourcentage") !== false || strpos($params["path"], "progressCircleMultiple") !== false){
                                $response["data"][$key] = DashboardData::horizontalBar($id, $answersCount, $params, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "progressCircle") !== false){
                                $response["data"][$key] = DashboardData::progressCircle($id, $answersCount, $params, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "progressBar") !== false){
                                $response["data"][$key] = DashboardData::progressBar($id, $answersCount, $params, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "textWithValue") !== false){
                                $response["data"][$key] = DashboardData::simpleValue($id, $answersCount, $answersCountByForm[$params["coform"]], $params, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "simplePie") !== false){
                                $response["data"][$key] = DashboardData::simplePie($id, $answersCount, $params, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "answerByMembers") !== false){
                                $response["data"][$key] = DashboardData::answerByMembers($id, $slug, $answersCount, $sharedFormRequest["answers"]??null);
                            }else if(strpos($params["path"], "crossedDoubleProgressBar") !== false){
                                $response["data"][$key] = DashboardData::doubleProgressBar($id, $params, $sharedFormRequest["answers"]??null);
                            }
                        }
                    }
                }

                if($isExport==true){
                    foreach ($blockParams as $key => $params) {
                        if(isset($params["coform"])){
                            try {
                                //[$params["answerPath"]]
                                $p = explode(".", str_replace(["checkboxNew", "radioNew", "multiCheckboxPlus", "text", "number","multiRadio", "checkboxcplx"], "", $params["answerPath"]));
                                $question = $response["global"]["coformInputs"][$params["coform"]][$p[0]][$p[1]]["label"];
                                if($format=="csv"){
                                    $exportData = array();
                                }else{
                                    $exportData = array(
                                        "question" => $question??$params["question"]??"C'est pas une question", 
                                        "label" => $params["name"]??"Label non spécifié",
                                        "type" => $params["chartType"]??"chiffre_cle",
                                        "isPercent" => true,
                                        "spotlight" => false, 
                                        "answers" => array()
                                    );
                                }
                                if(isset($response["data"][$key]["percentageArray"])){
                                    $answerData = $response["data"][$key]["percentageArray"];
                                    $exportData["type"] = "doughnut";
                                    foreach ($answerData as $k => $v){
                                        if($format=="csv"){
                                            $exportData = array(
                                                "question" => $question??$params["question"]??"C'est pas une question", 
                                                "label" => $params["blockName"]??"",
                                                "options" => $v["label"],
                                                "valeur" => $v["value"],
                                                "format" => "pourcentage"
                                            );
                                            array_push($response["exportable"], $exportData);
                                        }else{
                                            array_push($exportData["answers"], array(
                                                "label" => $v["label"],
                                                "value" => $v["value"],
                                                "legend" => $v["label"]
                                            ));
                                        }
                                    }
                                    if($format=="json"){
                                        array_push($response["exportable"], $exportData);
                                    }
                                }else if(isset($response["data"][$key]["value"])){
                                    $v = $response["data"][$key];
                                    if($format=="csv"){
                                        $exportData["question"] = $question??$params["question"]??$params["blockName"]??"C'est pas une question";
                                        $exportData["label"] = $params["blockName"]??"";
                                        $exportData["options"] = isset($params["answerValue"])?implode(',', $params["answerValue"]):"";
                                        $exportData["valeur"] = (isset($v["allCount"]) && $v["allCount"]!=0)?round(($v["value"]/$v["allCount"])*100):0;
                                        $exportData["format"] = str_contains($params["path"], "progressCircle")?"pourcentage":"nombre";
                                    }else{
                                        $exportData["isPercent"] = true;
                                        $exportData["spotlight"] = true;
                                        $exportData["answers"]["label"] = $params["name"]??"";
                                        $exportData["answers"]["value"] = (isset($v["allCount"]) && $v["allCount"]!=0)?round(($v["value"]/$v["allCount"])*100):0;
                                        $exportData["answers"]["legend"] = $params["name"]??"";
                                    }
                                    array_push($response["exportable"], $exportData);
                                }else if(is_string($response["data"][$key]) || is_float($response["data"][$key])){
                                    $v = $response["data"][$key];
                                    if($format=="csv"){
                                        $exportData["question"] = $question??$params["question"]??$params["blockName"]??"C'est pas une question";
                                        $exportData["label"] = $params["blockName"]??"";
                                        $exportData["options"] = $v["label"]??"";
                                        $exportData["valeur"] = (isset($v["allCount"]) && $v["allCount"]!=0)?round(($v["value"]/$v["allCount"])*100):0;
                                        $exportData["format"] = "nombre";
                                    }else{
                                        $exportData["isPercent"] = false;
                                        $exportData["spotlight"] = true;
                                        $exportData["answers"]["label"] = $params["name"]??"";
                                        $exportData["answers"]["value"] = (isset($v["allCount"]) && $v["allCount"]!=0)?round(($v["value"]/$v["allCount"])*100):0;
                                        $exportData["answers"]["legend"] = $params["name"]??"";
                                    }
                                    array_push($response["exportable"], $exportData);
                                }
                                //, self::formatKeyValue($response["data"][$key]["percentageArray"], "chart", $question));
                            } catch (\Throwable $th) {
                                //throw $th;
                            }
                        }
                    }
                }

                Yii::$app->cache->set('dashboardData'.$slug, $response, 3600);
            }

            if($isExport==true){
                if($format=="csv"){
                    return Rest::json(["header" =>array("question", "label", "options", "valeur", "format"), "body"=>$response["exportable"]]);
                }else{
                    return Rest::json(["header" =>array("question", "label", "answers", "type"), "body"=>$response["exportable"]]);
                }
            }
            return Rest::json($response);
        /*}else{
            return Rest::json(array("msg" => "Vous n'êtes pas connecter"));
        }*/
    }
}