<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph;

use Form;
use PHDB;
use Rest;

class GetFormsDataAction  extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $id = $_POST["costumId"]??"";
        $type = $_POST["costumType"]??"";
        $slug = $_POST["costumSlug"]??"";
        $ccQuery = array('$or'=>array(
            array("parent.".$id=>['$exists'=>true]),
            array("shareToChildren" => true)
        ) /*,"active"=>"true"*/);
        $contextCoforms = PHDB::find(Form::COLLECTION, $ccQuery, ["name", "type", "config", "shareToChildren", "params", "subForms"]);
        $mainFormsIds = array();
        foreach($contextCoforms as $formKey => $formValue){
            if( isset($formValue["config"]) && (!isset($formValue["type"]) || (isset($formValue["type"]) && $formValue["type"]!="aap")) ){
                array_push($mainFormsIds, $formValue["config"]);
            }
        }
        if( count($mainFormsIds) > 0 ){
            $mainForms = PHDB::findByIds(Form::COLLECTION, $mainFormsIds, ["name", "subForms", "params", "shareToChildren"]);
            foreach($contextCoforms as $formKey => $formValue){
                if( isset($formValue["config"]) && isset($mainForms[$formValue["config"]]) && (!isset($formValue["type"]) || (isset($formValue["type"]) && $formValue["type"]!="aap"))){
                    $contextCoforms[$formKey]["subForms"] = $mainForms[$formValue["config"]]["subForms"];
                    $contextCoforms[$formKey]["params"] = $mainForms[$formValue["config"]]["params"];
                }
            }
        }

        // Get subforms from duplicated forms
        $formInputs = [];
        $formList = [];

        foreach ($contextCoforms as $formKey => $formValue) {
            $formList[$formKey] = !empty($formValue["name"]) ? $formValue["name"] : "";                
            if((empty($formValue["type"])) || (isset($formValue["type"]) && ($formValue["type"]!="aap" || $formValue["type"]!="aapConfig") )){
                if(isset($formValue["subForms"]) && is_array($formValue["subForms"]) && count($formValue["subForms"])!=0){
                    $subFormId = array('$in' => $formValue["subForms"]);
                }
                if(!empty($formValue["subForms"]) && is_string($formValue["subForms"]) && $formValue["subForms"] !=""){
                    $subFormId = $formValue["subForms"];
                }
                if(isset($subFormId) && is_array($subFormId) && $subFormId!=""){
                    $subForms = [];
                    try {
                        $subForms = PHDB::find(Form::COLLECTION, array('id' => $subFormId));
                    } catch (\Throwable $th) {}

                    if(count($subForms)!=0){
                        foreach ($subForms as $subFormKey => $subFormValue) {
                            if(isset($subFormValue["inputs"])){
                                $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                            }
                        }
                    }
                }
            }
        }
        $response = array(
            'global' => array(
                'formTL' => $contextCoforms,
                'coformList' => $formList,
                'coformInputs' => $formInputs,
            )
        );
        return Rest::json($response);
    }
}
