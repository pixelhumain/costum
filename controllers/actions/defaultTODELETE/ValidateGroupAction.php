<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\defaultv;

use CAction, PHDB, MongoId, Person, Classified, Rest;
class ValidateGroupAction extends \PixelHumain\PixelHumain\components\Action{
	public function run(){

		$controller = $this->getController();
		
		if( !empty($controller->costum["slug"]) && 
			!empty($_POST["type"]) && 
			!empty($_POST["id"]) && 
			isset($_POST["valid"]) ){
			
			$elt = PHDB::findOneById( $_POST["type"], $_POST["id"], array("source", "links"));
			if($_POST["valid"] == "true"){
				if( !empty($elt["source"]["toBeValidated"]) ) {
					unset($elt["source"]["toBeValidated"][$controller->costum["slug"]]);
					if(count($elt["source"]["toBeValidated"]) == 0)
						unset($elt["source"]["toBeValidated"]);
				}
			}else{
				if( empty($elt["source"]["toBeValidated"]) )
					$elt["source"]["toBeValidated"] = array();
				$elt["source"]["toBeValidated"][$controller->costum["slug"]] = true ;
				
			}
			$query=array("source" => $elt["source"]);
			$res= PHDB::update( $_POST["type"], 
								array("_id" => new MongoId($_POST["id"])), 
								array('$set'=>$query));

			$sub = array();
			if(!empty($elt["links"])){
				foreach ($elt["links"] as $kL => $vL) {
					foreach ($vL as $key => $value) {
						if(!empty($value["type"]) && $value["type"] != Person::COLLECTION){
							$where = array(	"_id" => new MongoId($key),
											"source.keys" => array('$in' => array($controller->costum["slug"] ) ) );
							$subElt = PHDB::findOne($value["type"], $where, array("name","source", "links"));

							if(!empty($subElt)){
								if($_POST["valid"] == "true"){
									if( !empty($subElt["source"]["toBeValidated"]) ) {
										unset($subElt["source"]["toBeValidated"][$controller->costum["slug"]]);
										if(count($subElt["source"]["toBeValidated"]) == 0)
											unset($subElt["source"]["toBeValidated"]);
									}
								}else{
									if( empty($subElt["source"]["toBeValidated"]) )
										$subElt["source"]["toBeValidated"] = array();
									$subElt["source"]["toBeValidated"][$controller->costum["slug"]] = true ;
									
								}

								//$sub[$key] = $subElt;
								$querySub=array("source" => $subElt["source"]);
								$sub[$key] = PHDB::update( $value["type"], 
													array("_id" => new MongoId($key)), 
													array('$set'=>$querySub) );
							}
							
							

							
						}
					}
				}
			}
			
			$whereCl = array("parent.".$_POST["id"] => array('$exists' => true));
			$classifieds = PHDB::find( Classified::COLLECTION, $whereCl, array("name","source", "links"));
			//Rest::json($classifieds);
			if(!empty($elt["links"])){
				foreach ($classifieds as $key => $valC) {
					if($_POST["valid"] == "true"){
						if( !empty($valC["source"]["toBeValidated"]) ) {
							unset($valC["source"]["toBeValidated"][$controller->costum["slug"]]);
							if(count($valC["source"]["toBeValidated"]) == 0)
								unset($valC["source"]["toBeValidated"]);
						}
					}else{
						if( empty($valC["source"]["toBeValidated"]) )
							$valC["source"]["toBeValidated"] = array();
						$valC["source"]["toBeValidated"][$controller->costum["slug"]] = true ;
						
					}
					//$sub[$key] = $valC;
					$querySub=array("source" => $valC["source"]);
					$sub[$key] = PHDB::update( Classified::COLLECTION, 
										array("_id" => new MongoId($key)), 
										array('$set'=>$querySub) );
				}
			}


			return Rest::json(array("result"=>true, "elt" => $elt, "sub" => $sub));
		}
	}
}
?>