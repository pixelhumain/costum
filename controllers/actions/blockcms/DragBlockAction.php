<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Person, Authorisation, Yii, CTKException, PHDB, MongoId, Rest;
class DragBlockAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        if ( Person::logguedAndValid() ) 
        {
            if (isset($_POST["idAndPosition"]) && is_array($_POST["idAndPosition"])) {
            	$id = $_POST["id"];
                $collection = $_POST["collection"];
                $path = "position";
                if ( !Authorisation::isInterfaceAdmin($id, $collection) && 
                    !Authorisation::isParentAdmin($id, $collection , Yii::app()->session["userId"]) &&
                    !Authorisation::canEditItemOrOpenEdition($id, $collection, Yii::app()->session['userId']) && 
                    !Authorisation::specificCondition() ) {
                    throw new CTKException(Yii::t("common","Can not update the element : you are not authorized to update that element !"));
                }
                
                try{
                	foreach ($_POST["idAndPosition"] as $key => $value) {
						$blockId = $value;
						$blockCollection = "cms";
						$blockPosition = $key;
						PHDB::update( $blockCollection,
                                    [ "_id" => new MongoId($blockId) ], 
                                    [ '$set' => [$path => $blockPosition] ]);
                	}
                    return Rest::json(array("result"=>true, "msg"=> "Move saved",$path));
                }catch (CTKException $e) {
                    return Rest::json(array("result"=>false, "msg"=>$e->getMessage(), $path));
                }
            }else{
            	return Rest::json(array("result"=>false, "msg"=>"invalid parameter"));
            }
        } else 
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));

    }
}