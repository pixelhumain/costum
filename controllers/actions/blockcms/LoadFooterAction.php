<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest,CacheHelper;
class LoadFooterAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
        $controller=$this->getController();
        $costum = CacheHelper::getCostum();
        $useFooter = false;
        $footerCMS = Cms::getCmsFooter($costum["contextId"]);
        if(!empty($footerCMS))
            $useFooter = true;        

        $resp = array("html" => $controller->renderPartial("../../../../pixelhumain/ph/themes/CO2/views/layouts/footer",array("useFooter" => $useFooter)), "footerCMS" => $useFooter);
        return Rest::json($resp);
    }
}