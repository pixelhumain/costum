<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest;
class GetCmsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        if ($_POST["action"] == "duplicate") { 

           $template = Element::getElementById($_POST["tplParent"],Cms::COLLECTION);
           // $contextCostum = CacheHelper::getCostum();
           $allCmsId = [];
           if($_POST['cmsList'] != ""){  
             $allCmsId = $_POST['cmsList'];
           }else if (!empty($template["cmsList"])) {
             $allCmsId = $template["cmsList"]; 
           }

           /**Duplicate block**/       
                Cms::duplicateBlock($allCmsId, "", array(
                    "parentId"   => $_POST['parentId'], 
                    "parentType" => $_POST["parentType"],
                    "parentSlug" => $_POST['parentSlug'],
                    "page"       => $_POST['page']))  ;
        	// $array_result = [];
        	// foreach ($_POST["ide"] as $key => $value) {
        	// 	$params = Element::getElementById($value,Cms::COLLECTION);
        	// 	$idParent = [];
        	// 	unset($params["_id"]);
        	// 	$idParent[$_POST["parentId"]] =  ["type" => $_POST["parentType"], "name" => $_POST["parentSlug"]];
        	// 	$params["page"] = $_POST["page"];
        	// 	$params["parent"] = $idParent;
         //        if ($_POST["tplParent"] !== "") {                
         //            $params["tplParent"] = $_POST["tplParent"];
         //        }else{
         //            $params["haveTpl"] = "false";
         //            unset($params["tplParent"]);
         //        }
        	// 	$array_result[] = $params;
        	// }
        	// PHDB::batchInsert(Cms::COLLECTION,$array_result);
         //    $res=array("result"=>true, "msg"=>Yii::t("common","Information saved"));
         //    Rest::json($res);
        }elseif($_POST["action"] == "removestat"){
          foreach ($_POST["ide"] as $key => $value) {
            $params = Element::getElementById($value,Cms::COLLECTION);
            $params["haveTpl"] = true;
            $params["tplParent"] = $_POST["tplId"];
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($value) ],['$set'=>$params]);
          }
        }elseif($_POST["action"] == "tplEmpty"){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],[ '$unset' => ["cmsList" =>true ] ]);          
        }elseif($_POST["action"] == "upCmsList"){
            PHDB::update(Cms::COLLECTION,[ "_id" => new MongoId($_POST["tplId"]) ],['$set'=> ["cmsList" => $_POST["ide"]]]);          
        }elseif($_POST["action"] == "delete"){
            foreach ($_POST["ide"] as $key => $value) {
            $res = Element::deleteSimple($value,"cms", Yii::app()->session["userId"]); 
         }   
         return Rest::json($res);    
        }
    }
}
