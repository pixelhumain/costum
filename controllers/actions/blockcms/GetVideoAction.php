<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;
use CAction;
use Cms;
use Rest;

class GetVideoAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
		$controller = $this->getController();
		$params = Cms::getVideo($_POST);

		return Rest::json($params);
	}
}
