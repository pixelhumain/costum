<?php
	
	namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda;
	
	use CacheHelper;
	use DateTime;
	use DateTimeZone;
	use Event;
	use MongoId;
	use Organization;
	use PHDB;
	use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
	use Project;
	use Rest;
	use Slug;
	use Yii;
	
	class EventAction extends \PixelHumain\PixelHumain\components\Action
	{
		private $_excludeIdsList = [];
		
		public function run($request = '', $reorder = 0)
		{
			$output = [];
			switch ($request)
			{
				case 'all':
					$traduction_map = [
						'getTogether'       => 'Get together',
						'participativeWork' => 'Participative work',
					];
					$subElementEventIds = [];
					$dbElementEvent = PHDB::findOneById($_POST['costumType'], $_POST['costumId'], ['links.events', 'agendas.communecter']);
					
					// get sub organizations
					$dbSubElements = PHDB::find(Organization::COLLECTION, ['parent.' . $_POST['costumId'] . '.type' => $_POST['costumType']], ['links.events']);
					foreach ($dbSubElements as $subElement) {
						$dbSubElementsEvents = $subElement['links']['events'] ?? [];
						foreach (array_keys($dbSubElementsEvents) as $eventKey)
							UtilsHelper::push_array_if_not_exists($eventKey, $subElementEventIds);
					}
					foreach (array_keys($dbSubElements) as $eventKey)
						UtilsHelper::push_array_if_not_exists($eventKey, $subElementEventIds);
					
					// get sub projects
					$dbSubElements = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['costumId'] . '.type' => $_POST['costumType']], ['links.events']);
					foreach ($dbSubElements as $subElement) {
						$dbSubElementsEvents = $subElement['links']['events'] ?? [];
						foreach (array_keys($dbSubElementsEvents) as $eventKey)
							UtilsHelper::push_array_if_not_exists($eventKey, $subElementEventIds);
					}
					
					$events = $this->getAllEvents($subElementEventIds, ['subevents' => true]);
					if (!empty($dbElementEvent['links']) && !empty($dbElementEvent['links']['events']))
					{
						$events = array_merge($events, $this->getAllEvents(array_keys($dbElementEvent['links']['events']), ['subevents' => true]));
					}
					
					// Agenda interne co
					if (!empty($dbElementEvent['agendas']['communecter']))
					{
						$idsList = [Organization::COLLECTION => [], Project::COLLECTION => [], Event::COLLECTION => []];
						foreach ($dbElementEvent['agendas']['communecter'] as $elementId => $commentcterElement)
							UtilsHelper::push_array_if_not_exists($elementId, $idsList[$commentcterElement['type']]);
						$eventIdsList = [];
						foreach ($idsList as $elementType => $ids) {
							$dbElementsList = PHDB::findByIds($elementType, $ids, ['links.events', 'links.subEvent', 'links.subEvents']);
							foreach ($dbElementsList as $dbElement) {
								if (!empty($dbElement['links']['events'])) $eventIdsList = array_merge($eventIdsList, array_keys($dbElement['links']['events']));
								else if (!empty($dbElement['links']['subEvent'])) $eventIdsList = array_merge($eventIdsList, array_keys($dbElement['links']['subEvent']));
								else if (!empty($dbElement['links']['subEvents'])) $eventIdsList = array_merge($eventIdsList, array_keys($dbElement['links']['subEvents']));
							}
						}
						$events = array_merge($events, $this->getAllEvents($eventIdsList));
					}
					$ids = [];
					foreach ($events as $event) $ids[] = new MongoId($event['id']);
					
					// search by key
					$dbElementEvent = PHDB::find(Event::COLLECTION, [
						'source.key' => $_POST['costumSlug'],
						'_id'        => ['$not' => ['$in' => $ids]],
					], ['_id']);
					$events = array_merge($events, $this->getAllEvents(array_keys($dbElementEvent), ['subevents' => true]));
					
					$groups = [];
					$color_cache = CacheHelper::get('color_cache') ?? [];
					foreach ($events as $event) {
						$type = $event['type'];
						if (!array_key_exists($type, $groups)) {
							if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$type] ?? ucfirst($type))))
								continue;
							$color_cache["event$type"] = $color_cache["event$type"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
							$groups[$type] = [
								'value' => $type,
								'color' => $color_cache["event$type"],
								'count' => 1,
							];
						}
						else
							$groups[$type]['count']++;
					}
					$groups = array_values($groups);
					CacheHelper::set('color_cache', $color_cache);
					
					if (!empty($_POST['exclude'])) {
						$temporary = [];
						foreach ($events as $event) {
							if (!in_array($event['type'], $_POST['exclude'])) {
								if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']))))
									continue;
								$event['color'] = $color_cache['event' . $event['type']];
								$event['group_id'] = $event['type'];
								$event['group'] = Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']));
								$temporary[] = $event;
							}
						}
						$events = $temporary;
					} else {
						$temporary = [];
						foreach ($events as $event)
						{
							if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']))))
							{
								continue;
							}
							$event['color'] = $color_cache['event' . $event['type']];
							$event['group'] = Yii::t('category', $traduction_map[$event['type']] ?? ucfirst($event['type']));
							$event['group_id'] = $event['type'];
							$temporary[] = $event;
						}
						$events = $temporary;
					}
					
					if ($reorder) $events = $this->reorder_by_date('startDate', $events);
					
					$output = Rest::json(['groups' => $groups, 'data' => $events]);
					break;
				case 'all_actions':
					$color_cache = is_array(CacheHelper::get('color_cache')) ? CacheHelper::get('color_cache') : [];
					$element_from_slug = Slug::getBySlug($_POST['costumSlug']);
					$element_id = $element_from_slug["id"];
					$db_element_events = PHDB::findOneById($element_from_slug["type"], $element_from_slug["id"], ["links.events"]);
					$db_element_events = !empty($db_element_events["links"]["events"]) ? array_keys($db_element_events["links"]["events"]) : [];
					foreach ([Organization::COLLECTION, Project::COLLECTION] as $element) {
						$db_subelements = PHDB::find($element, ["parent.$element_id.type" => $element_from_slug["type"]], ["links.events"]);
						foreach ($db_subelements as $db_subelement) {
							$links_events = !empty($db_subelement["links"]["events"]) ? array_keys($db_subelement["links"]["events"]) : [];
							foreach ($links_events as $links_event)
								UtilsHelper::push_array_if_not_exists($links_event, $db_element_events);
						}
					}
					$events = $this->getAllEvents($db_element_events, ["subevents" => true, "name" => $_POST["filter"]["text"]]);
					$found_events = array_map(fn($e) => $e["id"], $events);
					$db_actions = PHDB::find(\Action::COLLECTION, [
						"parentType" => Event::COLLECTION,
						"parentId"   => [
							'$in' => $found_events,
						],
						"status"     => ['$ne' => "disabled"],
					]);
					foreach ($events as $index => $event)
					{
						$id = $event["id"];
						$color_cache["event$id"] = $color_cache["event$id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
						$events[$index] = [
							"value" => $event["id"],
							"name"  => $event["name"],
							"color" => $color_cache["event$id"],
							"count" => count(array_filter($db_actions, function ($consumer) use ($event)
								{
									return $consumer["parentId"] == $event["id"];
								})
							),
						];
					}
					$actions = [];
					foreach ($db_actions as $id => $db_action)
					{
						if (!empty($_POST["filter"]["exclude"]) && in_array($db_action["parentId"], $_POST["filter"]["exclude"]))
							continue;
						$startDate = UtilsHelper::get_iso_date_from(["fields" => ["startDate", "created"], "provider" => $db_action, "default" => '', "timezone" => '']);
						$endDate = UtilsHelper::get_iso_date_from(["fields" => ["endDate", "created"], "provider" => $db_action, "default" => '', "timezone" => '']);
						$actions[] = [
							"id"               => $id,
							"slug"             => $db_action["slug"] ?? strval(rand(0, time())),
							"name"             => $db_action["name"],
							"startDate"        => $startDate,
							"endDate"          => $endDate,
							"color"            => $color_cache["event" . $db_action["parentId"]],
							"group"            => $db_action["parentId"],
							"group_id"         => $db_action["parentId"],
							"contributors"     => [],
							"subs"             => [],
							"profilImageUrl"   => "",
							"shortDescription" => $db_action["shortDescription"] ?? "",
							"description"      => $db_action["description"] ?? "",
						];
					}
					$output = Rest::json(["groups" => $events, "data" => $actions]);
					CacheHelper::set('color_cache', $color_cache);
					break;
			}
			return $output;
		}
		
		private function getAllEvents($ids, $options = ['subevents' => false])
		{
			$default = [
				"subevents" => false,
				"name"      => "",
			];
			$options = array_replace_recursive($default, $options);
			if (empty($ids)) return [];
			$fields = [
				'creator',
				'name',
				'description',
				'shortDescription',
				'startDate',
				'endDate',
				'slug',
				'timeZone',
				'type',
				'links.subEvents',
				'links.subEvent',
				'profilImageUrl',
			];
			$where = [
				'$or'    => [
					['preferences.private' => ['$exists' => false]],
					['preferences.private' => false],
				],
				"status" => ['$nin' => ["uncomplete", "deleted", "deletePending"]],
				"_id"    => [
					'$in' => array_map(function ($consumer)
					{
						return new MongoId($consumer);
					}, $ids),
				],
			];
			if (!empty($options["name"]))
				$where = array_merge_recursive($where, [
					"name" => UtilsHelper::mongo_regex($options["name"]),
				]);
			$dbCurrentEventsList = PHDB::find(Event::COLLECTION, $where, $fields);
			$dbActions = PHDB::findAndSort(\Action::COLLECTION, [
				'parentType' => Event::COLLECTION,
				'parentId'   => [
					'$in' => array_keys($dbCurrentEventsList),
				],
			], ['startDate' => 1], 0, ['parentId', 'name', 'max', 'links.contributors']);
			$events = [];
			$subEventsIdsList = [];
			foreach ($dbCurrentEventsList as $currentEventId => $dbCurrentEvent)
			{
				$eventActions = [];
				foreach ($dbActions as $actionId => $dbAction)
				{
					if ($dbAction['parentId'] === $currentEventId)
					{
						$eventActions[] = [
							'id'           => $actionId,
							'name'         => $dbAction['name'],
							'contributors' => $dbAction['links']['contributors'] ?? [],
							'max'          => intval($dbAction['max'] ?? 1),
						];
					}
				}
				$start = new DateTime();
				$end = new DateTime();
				$timezone = $dbCurrentEvent['timeZone'] ?? date_default_timezone_get();
				$start->setTimestamp(UtilsHelper::get_as_timestamp(['startDate'], $dbCurrentEvent));
				$start->setTimezone(new DateTimeZone($timezone));
				$end->setTimestamp(UtilsHelper::get_as_timestamp(['endDate'], $dbCurrentEvent));
				$end->setTimezone(new DateTimeZone($timezone));
				$events[] = [
					'id'               => $currentEventId,
					'slug'             => $dbCurrentEvent['slug'] ?? '',
					'type'             => $dbCurrentEvent['type'],
					'name'             => $dbCurrentEvent['name'],
					'shortDescription' => $dbCurrentEvent['shortDescription'] ?? '',
					'description'      => $dbCurrentEvent['description'] ?? '',
					'profilImageUrl'   => $dbCurrentEvent['profilImageUrl'] ?? '',
					'startDate'        => $start->format('c'),
					'endDate'          => $end->format('c'),
					'contributors'     => [],
					'maxContributor'   => 0,
					'actions'          => $eventActions,
				];
				$this->_excludeIdsList[] = $currentEventId;
				if (!empty($dbCurrentEvent['links']['subEvents'])) $subEventsIdsList = array_merge($subEventsIdsList, array_keys($dbCurrentEvent['links']['subEvents']));
				if (!empty($dbCurrentEvent['links']['subEvent'])) $subEventsIdsList = array_merge($subEventsIdsList, array_keys($dbCurrentEvent['links']['subEvent']));
			}
			foreach ($this->_excludeIdsList as $exclude)
			{
				if (in_array($exclude, $subEventsIdsList))
				{
					$index = array_search($exclude, $subEventsIdsList);
					array_splice($subEventsIdsList, $index, 1);
				}
			}
			if ($options['subevents'] && !empty($subEventsIdsList))
			{
				$events = array_merge($events, $this->getAllEvents($subEventsIdsList, $options));
			}
			return $events;
		}
		
		private function reorder_by_date($field, $input)
		{
			$output = $input;
			$length = count($input);
			$has_permutation = true;
			while ($has_permutation)
			{
				$has_permutation = false;
				for ($i = 0; $i < $length - 1; $i++)
				{
					if (UtilsHelper::get_as_timestamp([$field], $output[$i]) > UtilsHelper::get_as_timestamp([$field], $output[$i + 1]))
					{
						$has_permutation = true;
						$temporary = $output[$i];
						$output[$i] = $output[$i + 1];
						$output[$i + 1] = $temporary;
					}
				}
			}
			return $output;
		}
	}
