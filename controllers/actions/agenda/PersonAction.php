<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda;
    
    use MongoId;
    use Person;
    use PHDB;
    use PixelHumain\PixelHumain\components\Action;
    use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
    use Rest;
    use Yii;
    
    class PersonAction extends Action {
        public function run($request = '') {
            $output = '';
            switch ($request) {
                case 'memberOf':
                    $find = $_POST['filter'];
                    if (array_key_exists('_id', $find) && array_key_exists('$in', $find['_id'])) {
                        $find['_id']['$in'] = array_map(function ($id) {
                            return new MongoId($id);
                        }, $find['_id']['$in']);
                    }
                    
                    $collection = $_POST['element'];
                    $dbElements = PHDB::find($collection, $find, ['idUserAuthor', 'creator', 'links.contributors']);
                    $ids = [];
                    
                    foreach ($dbElements as $dbElement) {
                        if (!empty($dbElement['idUserAuthor'])) UtilsHelper::push_array_if_not_exists($dbElement['idUserAuthor'], $ids);
                        if (!empty($dbElement['creator'])) UtilsHelper::push_array_if_not_exists($dbElement['creator'], $ids);
                        
                        $dbLinks = !empty($dbElement['links']) && is_array($dbElement['links']) ? $dbElement['links'] : [];
                        foreach ($dbLinks as $dbLink) {
                            $links = !empty($dbLink) && is_array($dbLink) ? $dbLink : [];
                            foreach ($links as $id => $link) {
                                if (!empty($link['type']) && $link['type'] === Person::COLLECTION) UtilsHelper::push_array_if_not_exists($id, $ids);
                            }
                        }
                    }
                    
                    $dbPersons = PHDB::findByIds(Person::COLLECTION, $ids, ['name', 'profilImageUrl']);
                    $output = [];
                    foreach ($dbPersons as $id => $person) {
                        $output[] = [
                            'id'    => $id,
                            'name'  => $person['name'],
                            'image' => Yii::app()->getBaseUrl(true) . (!empty($person['profilImageUrl']) ? $person['profilImageUrl'] : Yii::app()->getModule('survey')->assetsUrl . '/images/thumbnail-default.jpg')
                        ];
                    }
                    $output = Rest::json($output);
                    break;
            }
            return $output;
        }
    }