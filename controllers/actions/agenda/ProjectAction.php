<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\agenda;

use Person;
use PHDB;
use Action;
use CacheHelper;
use Project;
use DateTime;
use Lists;
use MongoDate;
use \PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use Rest;
use Yii;

class ProjectAction extends \PixelHumain\PixelHumain\components\Action {
	public function run($request = '', $reorder = 0) {
		$output = [];
		switch ($request) {
			case 'all':
				$fields = [
					'slug',
					'name',
					'shortDescription',
					'description',
					'created',
					'startDate',
					'endDate',
					'profilImageUrl',
					'links.contributors',
					"oceco.milestones"
				];
				$sort_criteria = ['startDate' => 1, 'created' => 1];

				if (!empty($_POST['ids']))
					$where = ['_id' => ['$in' => array_map(fn($__id) => new \MongoId($__id), $_POST['ids'])]];
				else
					$where = ['$or' => [['parent.' . $_POST['costumId'] => ['$exists' => true]], ['parentId' => $_POST['costumId']]]];
				$where['$and'] = $where['$and'] ?? [];
				$where['$and'][] = [
					'state' => [
						'$nin' => [
							'uncomplete',
							'deleted'
						]
					]
				];
				$where['$and'][] = [
					'status' => [
						'$nin' => [
							'uncomplete',
							'deleted',
							'deletePending'
						]
					]
				];
				$where['name'] = ['$exists' => true];
				$db_projects = PHDB::findAndSort(Project::COLLECTION, $where, $sort_criteria, 0, $fields);
				$projects = [];
				$actionWhere = [
					'created'		=> ['$exists' => 1],
					'parentId'		=> ['$in' => array_keys($db_projects)],
					'parentType'	=> Project::COLLECTION,
					"status"    	=> ['$nin' => ["disabled", "closed"]]
				];
				if (!empty($_POST['members'])) {
					$actionWhere['$or'] = [['idUserAuthor' => ['$in' => $_POST['members']]]];
					$actionWhere['$or'] = array_merge($actionWhere['$or'], array_map(fn($member) => (["links.contributors.$member" => ['$exists' => 1]]), $_POST['members']));
				}
				if (!empty($_POST['agendaContentText']))
					$actionWhere['name'] = UtilsHelper::mongo_regex($_POST['agendaContentText']);
				$actionFields = [
					'name',
					'parentId',
					'slug',
					'description',
					'shortDescription',
					'profilImageUrl',
					'created',
					'startDate',
					'endDate',
					'status',
					'tasks',
					'tracking',
					'max',
					'tags',
					'links.contributors',
					'parentId',
					'creator',
					'media.images',
					'media.files',
					'commentCount',
					"milestone",
				];
				$dbActionsList = PHDB::findAndSort(Action::COLLECTION, $actionWhere, [
					'startDate' => 1,
					'created' => 1
				], 0, $actionFields);
				// Récupérer les images de l'action
				$documentIdsList = [];
				$dbActionsList = array_map(function ($action) {
					$action["id"] = (string)$action["_id"];
					return ($action);
				}, $dbActionsList);
				foreach ($dbActionsList as $dbAction) {
					$filesList = [];
					$filesList = array_merge($filesList, $dbAction['media']['images'] ?? []);
					$filesList = array_merge($filesList, $dbAction['media']['files'] ?? []);
					foreach ($filesList as $file)
						UtilsHelper::push_array_if_not_exists($file, $documentIdsList);
				}
				$dbDocumentsList = PHDB::findByIds(\Document::COLLECTION, $documentIdsList, ['folder', 'name', 'doctype', 'docType']);
				$groups = [];
				// cache de couleur
				$color_cache = CacheHelper::get('color_cache') ?? [];
				// cache de couleur
				foreach ($db_projects as $db_id => $db_project) {
					$contributorsList = [];
					$projectContributorsList = $db_project['links']['contributors'] ?? [];
					foreach ($projectContributorsList as $contributorId => $oneProjectContributor) {
						if (!empty($oneProjectContributor['type']) && $oneProjectContributor['type'] === Person::COLLECTION)
							UtilsHelper::push_array_if_not_exists($contributorId, $contributorsList);
					}
					if (!empty($_POST['text']) && !preg_match("/" . preg_quote($_POST['text']) . "/i", $db_project['name']))
						continue;
					$color_cache["projectproj$db_id"] = $color_cache["projectproj$db_id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
					$project_start = new DateTime();
					$project_end = new DateTime();
					$project_start_time = UtilsHelper::get_as_timestamp([
						'startDate',
						'created'
					], $db_project);
					$project_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_project, strtotime(date('c', $project_start_time) . ' +1day'));
					if (date('H:i:s', $project_end_time) === '00:00:00') $project_end_time -= 1;

					$currentDbActionList = array_filter($dbActionsList, fn($db_action_item) => ($db_action_item['parentId'] === $db_id), ARRAY_FILTER_USE_BOTH);
					usort($currentDbActionList, function ($a, $b) {
						$current = UtilsHelper::get_as_timestamp(['startDate', 'created'], $a);
						$next = UtilsHelper::get_as_timestamp(['startDate', 'created'], $b);
						return ($current - $next);
					});
					$actions = [];
					$currentDbActionList = Lists::arrange_action_by_status(["id" => $db_id, "type" => Project::COLLECTION], $currentDbActionList, ["discuter", "next", "todo", "tracking", "totest", "done"]);
					foreach ($currentDbActionList as $dbAction) {
						$actionContributorsList = $dbAction['links']['contributors'] ?? [];
						foreach ($actionContributorsList as $contributorId => $oneActionContributor) {
							if (!empty($oneActionContributor['type']) && $oneActionContributor['type'] === Person::COLLECTION)
								UtilsHelper::push_array_if_not_exists($contributorId, $contributorsList);
						}
						if (!empty($dbAction['creator']))
							UtilsHelper::push_array_if_not_exists($dbAction['creator'], $contributorsList);
						$currentDbImagesList = array_filter($dbDocumentsList, function ($dbDocument, $documentId) use ($dbAction) {
							if (empty($dbDocument['name']))
								return false;
							$docType = $dbDocument['doctype'] ?? $dbDocument['docType'];
							return !empty($dbAction['media']['images']) && in_array($documentId, $dbAction['media']['images']) && $docType === 'image';
						}, ARRAY_FILTER_USE_BOTH);
						$currentDbDocumentsList = array_filter($dbDocumentsList, function ($dbDocument, $documentId) use ($dbAction) {
							if (empty($dbDocument['name']))
								return false;
							$docType = $dbDocument['doctype'] ?? $dbDocument['docType'];
							return !empty($dbAction['media']['files']) && in_array($documentId, $dbAction['media']['files']) && $docType !== 'image';
						}, ARRAY_FILTER_USE_BOTH);
						$currentDbImagesList = array_map(function ($currentDbImage) {
							return Yii::app()->getBaseUrl(true) . '/upload/communecter/' . $currentDbImage['folder'] . '/' . $currentDbImage['name'];
						}, array_values($currentDbImagesList));
						$currentDbDocumentsList = array_map(function ($currentDbDocument) {
							return Yii::app()->getBaseUrl(true) . '/upload/communecter/' . $currentDbDocument['folder'] . '/' . $currentDbDocument['name'];
						}, array_values($currentDbDocumentsList));
						$action_start = $dbAction['created'];
						if (!empty($dbAction['startDate']) && $dbAction['startDate'] instanceof MongoDate)
							$action_start = $dbAction['startDate']->sec;
						$action_end = strtotime(date('Y-m-d H:i', $action_start) . ' + 30 minutes');
						if (!empty($dbAction['endDate']) && $dbAction['endDate'] instanceof MongoDate)
							$action_end = $dbAction['endDate']->sec;
						$action_start_time = UtilsHelper::get_as_timestamp([
							'startDate',
							'created'
						], $dbAction);

						$now = strtotime(date('Y-m-d H:i'));
						$addition = max($now, strtotime(date('c', $action_start_time) . ' + 1day'));

						$action_end_time = UtilsHelper::get_as_timestamp(['endDate'], $dbAction, $addition);
						if (date('H:i:s', $action_end_time) === '00:00:00') $action_end_time -= 1;

						$tasks = [];
						if (!empty($dbAction['tasks'])) {
							foreach ($dbAction['tasks'] as $db_task) {
								if (!is_array($db_task))
									continue;
								$taskContributorsList = $db_task['contributors'] ?? [];
								foreach ($taskContributorsList as $contributorId => $oneTaskContributor) {
									if (!empty($oneTaskContributor['type']) && $oneTaskContributor['type'] === Person::COLLECTION) UtilsHelper::push_array_if_not_exists($contributorId, $contributorsList);
								}

								$task_start = new DateTime();
								$task_end = new DateTime();

								$task_start_time = UtilsHelper::get_as_timestamp([
									'startDate',
									'createdAt',
									'created'
								], $db_task, $action_start_time);
								$task_end_time = UtilsHelper::get_as_timestamp(['checkedAt'], $db_task, strtotime(date('c', $task_start_time) . ' + 30 minutes'));
								if (date('H:i:s', $task_end_time) === '00:00:00') $task_end_time -= 1;

								$task_start->setTimestamp($task_start_time);
								$task_end->setTimestamp($task_end_time);
								$tasks[] = [
									'id'        => 'task' . ($db_task['taskId'] ?? sha1(strval(time()))),
									'name'      => $db_task['task'] ?? '',
									'checked'   => !empty($db_task['checked']) ? filter_var($db_task['checked'], FILTER_VALIDATE_BOOLEAN) : false,
									'startDate' => $task_start->format('c'),
									'endDate'   => $task_end->format('c'),
									'color'     => $color_cache["projectproj$db_id"]
								];
							}
							if ($reorder) {
								$tasks = $this->reorder_by_date('startDate', $tasks);
								if (count($tasks) > 0)
									$action_end = empty($dbAction['endDate']) ? max(strtotime(end($tasks)['endDate']), $action_end) : $action_end;
							}
						}
						$actions[] = [
							'id'               => 'act' . $dbAction['id'],
							'slug'             => $dbAction['slug'] ?? '',
							'name'             => $dbAction['name'],
							'group'            => $db_project['name'],
							'shortDescription' => $dbAction['shortDescription'] ?? '',
							'description'      => $dbAction['description'] ?? '',
							'profilImageUrl'   => $dbAction['profilImageUrl'] ?? '',
							'startDate'        => date('c', $action_start),
							'endDate'          => date('c', $action_end),
							'status'           => $dbAction['status'] ?? 'todo',
							'tags'             => !empty($dbAction['tags']) && is_array($dbAction['tags']) ? $dbAction['tags'] : [],
							'tracking'         => filter_var($dbAction['tracking'] ?? false, FILTER_VALIDATE_BOOLEAN),
							'color'            => $color_cache["projectproj$db_id"],
							'images'           => $currentDbImagesList ?? [],
							'documents'        => $currentDbDocumentsList ?? [],
							"milestone"			=> $dbAction["milestone"] ?? null,
							'commentCount'     => !empty($dbAction['commentCount']) ? $dbAction['commentCount'] : 0,
							'contributors'     => $dbAction['links']['contributors'] ?? [],
							'maxContributor'   => intval($dbAction['max'] ?? 1),
							'creator'          => $dbAction['creator'] ?? null,
							'subs'             => $tasks
						];
					}
					if ($reorder) {
						if (count($actions) > 0) {
							$project_start_time = min(strtotime($actions[0]['startDate']), $project_start_time);
							$project_end_time = max(strtotime(end($actions)['endDate']), $project_end_time);
						}
					}

					$project_start->setTimestamp($project_start_time);
					$project_end->setTimestamp($project_end_time);

					$dbProjectContributors = PHDB::findByIds(Person::COLLECTION, $contributorsList, [
						'name',
						'profilImageUrl',
						"username",
					]);
					$contributors = array_map(function ($oneContributorMap) {
						return [
							'name'  => $oneContributorMap['name'],
							"username" => $oneContributorMap['username'],
							'image' => Yii::app()->getBaseUrl(true) . ($oneContributorMap['profilImageUrl'] ?? Yii::app()->getModule('co2')->assetsUrl . '/images/thumbnail-default.jpg')
						];
					}, $dbProjectContributors);
					$project = [
						'id'               => "proj$db_id",
						'slug'             => $db_project['slug'] ?? '',
						'name'             => $db_project['name'],
						'shortDescription' => $db_project['shortDescription'] ?? '',
						'description'      => $db_project['description'] ?? '',
						'profilImageUrl'   => $db_project['profilImageUrl'] ?? '',
						'startDate'        => $project_start->format('c'),
						'endDate'          => $project_end->format('c'),
						'color'            => $color_cache["projectproj$db_id"],
						'original_created' => $db_project['created'] ?? 0,
						'contributors'		=> $contributors,
						'subs'				=> $actions,
						"milestones"		=> !empty($db_project["oceco"]["milestones"]) && is_array($db_project["oceco"]["milestones"]) ? array_values($db_project["oceco"]["milestones"]) : [],
					];

					$groups[] = [
						'value' => $db_id,
						'name'  => $project['name'],
						'color' => $color_cache["projectproj$db_id"],
						'count' => count($project['subs']),
						'info'  => $project['name']
					];
					$projects[] = $project;
				}
				// cache de couleur
				CacheHelper::set('color_cache', $color_cache);
				// cache de couleur
				if ($reorder) $projects = $this->reorder_by_date('startDate', $projects);

				if (!empty($_POST['exclude'])) {
					$temporary = [];
					foreach ($projects as $project) {
						if (!in_array(substr($project['id'], 4), $_POST['exclude'])) {
							$project['color'] = $color_cache['project' . $project['id']];
							$temporary[] = $project;
						}
					}
					$projects = $temporary;
				} else {
					$temporary = [];
					foreach ($projects as $project) {
						$project['color'] = $color_cache['project' . $project['id']];
						$temporary[] = $project;
					}
					$projects = $temporary;
				}
				$output = Rest::json(['groups' => $groups, 'data' => $projects]);
				break;
			case 'actions':
				$db_project_ids = [];
				// cache de couleur
				$color_cache = CacheHelper::get('color_cache') ?? [];
				// cache de couleur

				if (!empty($_POST['costumType']) && $_POST['costumType'] === 'projects') $db_project_ids[$_POST['costumId']] = PHDB::findOneById(Project::COLLECTION, $_POST['costumId'], ['name']);
				else $db_project_ids = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['costumId'] => ['$exists' => true]], ['name']);

				$ids = array_keys($db_project_ids);
				$wheres = [];
				if (!empty($_POST['members'])) {
					$wheres['$or'] = [['idUserAuthor' => ['$in' => $_POST['members']]]];
					$wheres['$or'] = array_merge($wheres['$or'], array_map(function ($id) {
						return ["links.contributors.$id" => ['$exists' => 1]];
					}, $_POST['members']));
				}
				$in = [];
				foreach ($ids as $id) {
					$in[] = $id;
					$color_cache["projectproj$id"] = $color_cache["projectproj$id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
				}
				if (!empty($_POST['agendaContentText'])) {
					$wheres['name'] = UtilsHelper::mongo_regex($_POST['agendaContentText']);
				}
				$wheres['parentId'] = ['$in' => $in];

				// cache de couleur
				CacheHelper::set('color_cache', $color_cache);
				// cache de couleur

				$dbActionsList = PHDB::findAndSort(Action::COLLECTION, ['$or' => $wheres], ['startDate' => 1, 'created' => 1], 0, [
					'name',
					'slug',
					'description',
					'shortDescription',
					'profilImageUrl',
					'created',
					'startDate',
					'endDate',
					'status',
					'parentId',
					'max',
					'links.contributors'
				]);
				$actions = [];
				foreach ($dbActionsList as $dbAction) {
					$start = new DateTime();
					$end = new DateTime();

					$start_time = UtilsHelper::get_as_timestamp([
						'startDate',
						'created'
					], $dbAction);
					$end_time = UtilsHelper::get_as_timestamp(['endDate'], $dbAction, strtotime(date('c', $start_time) . ' +1day'));
					if (date('H:i:s', $end_time) === '00:00:00') $end_time -= 1;

					$start->setTimestamp($start_time);
					$end->setTimestamp($end_time);

					$actions[] = [
						'id'               => (string)$dbAction['_id'],
						'slug'             => $dbAction['slug'] ?? '',
						'name'             => $dbAction['name'],
						'group'            => $db_project_ids[$dbAction['parentId']]['name'],
						'shortDescription' => $dbAction['shortDescription'] ?? '',
						'description'      => $dbAction['description'] ?? '',
						'profilImageUrl'   => $dbAction['profilImageUrl'] ?? '',
						'startDate'        => $start->format('c'),
						'color'            => $color_cache['projectproj' . $dbAction['parentId']],
						'contributors'     => $dbAction['links']['contributors'] ?? [],
						'maxContributor'   => intval($dbAction['max'] ?? 1),
						'endDate'          => $end->format('c')
					];
				}
				if ($reorder) $actions = $this->reorder_by_date('startDate', $actions);
				$output = Rest::json($actions);
				break;
			case 'actions_tasks':
				$db_project_ids = [];
				// cache de couleur
				$color_cache = CacheHelper::get('color_cache') ?? [];
				// cache de couleur

				if ($_POST['costumType'] === 'projects') $db_project_ids[$_POST['costumId']] = PHDB::findOneById(Project::COLLECTION, $_POST['costumId'], ['name']);
				else $db_project_ids = PHDB::find(Project::COLLECTION, ['parent.' . $_POST['costumId'] => ['$exists' => true]], ['name']);

				$ids = array_keys($db_project_ids);
				$wheres = [];
				foreach ($ids as $id) {
					$wheres[] = ['parentId' => $id];
					$color_cache["projectproj$id"] = $color_cache["projectproj$id"] ?? '#' . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2) . substr('0' . strval(base_convert(rand(0, 255), 10, 16)), -2);
				}

				// cache de couleur
				CacheHelper::set('color_cache', $color_cache);
				// cache de couleur

				$currentDbActionList = PHDB::findAndSort(Action::COLLECTION, ['$or' => $wheres], ['startDate' => 1, 'created' => 1], 0, [
					'name',
					'slug',
					'description',
					'shortDescription',
					'profilImageUrl',
					'created',
					'startDate',
					'endDate',
					'status',
					'parentId',
					'links.contributors',
					'max',
					'tasks'
				]);
				$actions = [];
				foreach ($currentDbActionList as $dbAction) {
					$action_start = new DateTime();
					$action_end = new DateTime();

					$action_start_time = UtilsHelper::get_as_timestamp([
						'startDate',
						'created'
					], $dbAction);
					$action_end_time = UtilsHelper::get_as_timestamp(['endDate'], $dbAction, strtotime(date('c', $action_start_time) . ' +1day'));
					if (date('H:i:s', $action_end_time) === '00:00:00') $action_end_time -= 1;
					$tasks = [];
					if (!empty($dbAction['tasks'])) {
						foreach ($dbAction['tasks'] as $db_task) {
							$task_start = new DateTime();
							$task_end = new DateTime();

							$task_start_time = UtilsHelper::get_as_timestamp([
								'startDate',
								'createdAt',
								'created'
							], $db_task);
							$task_end_time = UtilsHelper::get_as_timestamp(['endDate'], $db_task, strtotime(date('c', $task_start_time) . ' +1day'));
							if (date('H:i:s', $task_end_time) === '00:00:00') $task_end_time -= 1;

							$task_start->setTimestamp($task_start_time);
							$task_end->setTimestamp($task_end_time);

							$tasks[] = [
								'id'        => 'task' . ($db_task['taskId'] ?? sha1(strval(time()))),
								'name'      => $db_task['task'],
								'checked'   => filter_var($db_task['checked'], FILTER_VALIDATE_BOOLEAN),
								'startDate' => $task_start->format('c'),
								'endDate'   => $task_end->format('c'),
								'color'     => $color_cache['projectproj' . $dbAction['parentId']]
							];
						}
						if ($reorder) $tasks = $this->reorder_by_date('startDate', $tasks);
					}

					if (count($tasks) > 0) {
						$action_end_time = empty($db_action['endDate']) ? max(strtotime(end($tasks)['endDate']), $action_end_time) : $action_end_time;
					}

					$action_start->setTimestamp($action_start_time);
					$action_end->setTimestamp($action_end_time);

					$actions[] = [
						'id'               => 'act' . (string)$dbAction['_id'],
						'slug'             => $dbAction['slug'] ?? '',
						'name'             => $dbAction['name'],
						'group'            => $db_project_ids[$dbAction['parentId']]['name'],
						'shortDescription' => $dbAction['shortDescription'] ?? '',
						'description'      => $dbAction['description'] ?? '',
						'profilImageUrl'   => $dbAction['profilImageUrl'] ?? '',
						'startDate'        => $action_start->format('c'),
						'endDate'          => $action_end->format('c'),
						'contributors'     => $dbAction['links']['contributors'] ?? [],
						'maxContributor'   => intval($dbAction['max'] ?? 1),
						'color'            => $color_cache['projectproj' . $dbAction['parentId']],
						'subs'             => $tasks
					];
				}
				if ($reorder) $actions = $this->reorder_by_date('startDate', $actions);
				$output = Rest::json($actions);
				break;
		}
		return $output;
	}

	private function reorder_by_date($field, $input) {
		$output = $input;
		$length = count($input);
		$has_permutation = true;
		while ($has_permutation) {
			$has_permutation = false;
			for ($i = 0; $i < $length - 1; $i++) {
				if (UtilsHelper::get_as_timestamp([$field], $output[$i]) > UtilsHelper::get_as_timestamp([$field], $output[$i + 1])) {
					$has_permutation = true;
					$temporary = $output[$i];
					$output[$i] = $output[$i + 1];
					$output[$i + 1] = $temporary;
				}
			}
		}
		return $output;
	}
}
