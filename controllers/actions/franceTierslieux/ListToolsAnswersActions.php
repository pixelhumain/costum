<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use Rest;
use PHDB;
use Form;
use Answer;

class ListToolsAnswersActions extends \PixelHumain\PixelHumain\components\Action {
  public function run() {
    $results = array();
    if (isset($_POST["finderPath"]) && isset($_POST["step"]) && isset($_POST["formId"])) {
        $answerFinderPath = $_POST["finderPath"];
        $getForm = PHDB::findOne (Form::COLLECTION, array("id"=> $_POST["step"] ) );
        $toolsTitleResults = array();
        $answerContent = array();
        $criteriasFromAnswers = array();
        $criteriaCount = array();
        $suplFormId = $_POST["formId"];
        $countAnswers = array();
        $finderAnswer = array();
        $finderContent = array();
        $finderContentKey = "";
        if (isset($getForm["inputs"])) {
            foreach ($getForm["inputs"] as $key => $value) { 
                $criteriaKey = "criterias" . $key;
                $yesNoCriteriaKey = "yesOrNo" . $key;
                $allSelfCriterias = PHDB::find(Form::ANSWER_COLLECTION,array(
                    "form" => (string)$suplFormId,
                    "answers." . $criteriaKey => ['$exists'=> true],
                    ),
                    array("answers." . $criteriaKey)
                );
                foreach ($allSelfCriterias as $kcrit => $vcrit) {
                    if(!empty($vcrit['answers'][$criteriaKey])){
                        foreach($vcrit['answers'][$criteriaKey] as $k => $v){
                            $criteriasFromAnswers[$value['label']][$k] = $v;
                            $criteriaCount[$k] = "answers." . $yesNoCriteriaKey . "." . $k;
                        }
                    }
                }
                $criterias = $criteriasFromAnswers;
            }
            foreach ($criterias as $kcrit => $vcrit) {
                $criterias[$kcrit]["count"] = 0;
                foreach ($vcrit as $key => $value) {
                    $countAnswers = PHDB::find(Form::ANSWER_COLLECTION,array($criteriaCount[$key] => array('$exists' => true), $answerFinderPath => array('$exists' => true) ),array($criteriaCount[$key], $answerFinderPath));
                    if (count(array_values($countAnswers)) > 0 && isset(array_values($countAnswers)[0]) && isset(array_values($countAnswers)[0]['answers']['lesCommunsDesTierslieux10112022_1423_0'])) {
                        $finderAnswer = array_values($countAnswers)[0]['answers']['lesCommunsDesTierslieux10112022_1423_0'];
                        if (isset(array_values($finderAnswer)[0]) && isset(array_keys(array_values($finderAnswer)[0])[0])) {
                            $finderContent = array_values($finderAnswer)[0];
                            $finderContentKey = array_keys($finderContent)[0];
                            $criterias[$kcrit][$key]['trAnswers'][] = $countAnswers;
                            if (count($countAnswers) > 0) 
                                $criterias[$kcrit][$key]["count"] = count($countAnswers);
                            
                            $criterias[$kcrit]["count"] += count($countAnswers);
                        }
                    } else {
                        unset($criterias[$kcrit][$key]);
                    }
                }
                if ($criterias[$kcrit]["count"] == 0)
                    unset($criterias[$kcrit]);
            }
            $results = $criterias;
        }
    }
    return Rest::json($results);
  }
}

?>



