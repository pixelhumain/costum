<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use Rest;
use PHDB;
use Answer;
use Form;
use Organization;
use SearchNew;
use FranceTierslieux;

class GetallsuplanswersAction extends \PixelHumain\PixelHumain\components\Action {
  public function run($subForm=null) {
    $returnValue = [];
    if ($subForm == "all") {
      $subforms = $_POST["subformArray"];
      $countSubform = count($subforms);
      $query = ['$or' => []];
      $show = ["inputs"];
      for ($i=0; $i < $countSubform; $i++) { 
        $query['$or'][] = ["id" => $subforms[$i]];
      }
      $returnValue = PHDB::find(Form::COLLECTION, $query, $show);
    } else {
      $res = [];
      $organisation = [];
      $_POST["show"] = $_POST["show"] ?? [];
      if (isset($_POST["suplFormId"])) {
        foreach ($_POST["suplFormId"] as $key => $value) {
          if (gettype($value) == "array") {
            $value = ["form" => $key, $value[0] => ['$exists' => true], "user" => $value[1]];
          } else {
            $value = ["form" => $key, $value => ['$exists' => true]];
          }
          $getAnswer = PHDB::find(Answer::COLLECTION, $value, $_POST["show"]);
          // var_export($value);
          $formsData = PHDB::findOneById(Form::COLLECTION, $key, ["subForms"]);
          $allInputs = [];
          if (isset($formsData["subForms"]) && count($formsData["subForms"]) > 0) {
            foreach ($formsData["subForms"] as $keyForm => $valueForm) {
              $inputData = PHDB::findOne(Form::COLLECTION, array("id" => $valueForm), ["inputs"]);
              array_push($allInputs, $inputData["inputs"]);
            }
          }
          $res[$key] = ["allAnswers" => $getAnswer,"form" => $formsData,"inputs" => $allInputs];
        }
      }
      if (isset($_POST["orgaId"])) {
        $_POST["showInOrga"] = $_POST["showInOrga"] ?? [];
        $organisation = PHDB::findOneById(Organization::COLLECTION, $_POST["orgaId"], $_POST["showInOrga"]);
      }
      $returnValue = ["results" => $res, "orgaData" => $organisation];
    }
    return Rest::json($returnValue);
  }
}





