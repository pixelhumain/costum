<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, Element, Rest, Link, Yii, PHDB, Organization, mongoId;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;

class MultiReferenceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type= null){
        $controller = $this->getController();
        // if(isset($controller->costum["contextType"]) && isset($controller->costum["contextId"])){
        //     $ctx=PHDB::findOne(Organization::COLLECTION,array("_id"=>new MongoId($controller->costum["contextId"])),array("links"));
        //     $members=(isset($ctx["links"]) && isset($ctx["links"]["members"])) ? array_keys($ctx["links"]["members"]) : array() ;
            
        // }
        // foreach($members as $ind=>$id){
        //     $members[$ind]=new MongoId($id);
        // }
        // //  var_dump($members);exit;
         $extraQuery=(isset($members)) ? array("_id" => array('$nin'=>$members))  : array() ;
        
        $elts = Element::getBySourceTagsAndZone($type,$_POST["tags"],null,$_POST["zoneNb"],$_POST["zoneId"],"franceTierslieux",$extraQuery);
        // var_dump($elts);exit;
        //    var_dump(Yii::$app->cache);exit;
        $counterS=0;
        $counterL=0;
        $countE=sizeof(array_keys($elts));
        foreach ($elts as $id => $value){
            if((!isset($value["reference"]) && 
                !in_array($_POST["costumSlug"],$value["source"]["keys"])) || 
                (isset($value["reference"]["costum"]) && !in_array($_POST["costumSlug"],$value["reference"]["costum"]))){
                    Admin::addSourceInElement($id,$value["collection"],$_POST["costumSlug"],"reference");
                    $counterS++;
                    $elts[$id]["referenced"]=true;
            }
            if(isset($controller->costum["contextType"]) && isset($controller->costum["contextId"]) && 
               (!isset($value["links"]) || (isset($value["links"]) && !isset($value["links"]["memberOf"])) || 
                    (isset($value["links"]) && isset($value["links"]["memberOf"]) && !isset($value["links"]["memberOf"][$controller->costum["contextId"]])) ) ){
                
                $child = array(
                        "childId" => (string)$value["_id"],
                        "childType" => $value["collection"],
                        "childName" => $value["name"],
                        "childEmail" => $value["email"] ?? ""
                    );
                $counterL++;
                $elts[$id]["linked"]=true;    

                Link::connectParentToChild($controller->costum["contextId"], $controller->costum["contextType"], $child, false, Yii::app()->session["userId"]);
            }
        }
        

        $res=array("referenced"=>$counterS,"linked"=>$counterL,"NbTiersLieux"=>$countE,"tiersLieux"=>$elts);
        // var_dump($res);exit;
        return Rest::json($res);
    }
    	
}
    	
