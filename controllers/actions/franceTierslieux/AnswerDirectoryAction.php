<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use Answer;
use Rest;
use FranceTierslieux;
use Organization;
use Citoyen;
use MongoRegex;
use Project;
use SearchNew;
use Event;
/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class AnswerDirectoryAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run($source=null, $form=null, $loadMap=null, $answerPath="", $addOrga = false){
		$controller = $this->getController();
		ini_set('memory_limit', '-1');
		$res=array();
		$res = SearchNew::globalAutoComplete($_POST);
		if (isset($_POST["searchType"][0]) && isset($res) ) {
			if ($_POST["searchType"][0] == Project::COLLECTION || $_POST["searchType"][0] == Event::COLLECTION) 
				$res["results"] = FranceTierslieux::addOrgaInElement($res["results"] ,$_POST["searchType"][0] ,$_POST["params"]);
			if ($_POST["searchType"][0] == Answer::COLLECTION){
				if($addOrga){
					$res["results"] = FranceTierslieux::addOrgaInElement($res["results"], Organization::COLLECTION, []);
				}
			}
		} 
		if (isset($_POST["searchType"][0]) && $_POST["searchType"][0] == Organization::COLLECTION) {
			$searchParams = $_POST;
			if ($loadMap != null) {
				$searchParams["fields"] = array (
					'links.organizations' => 1,
					'geo' => 1,
					'name' => 1
				);	
			}
			if ($answerPath != null && $answerPath != "" || $loadMap != null) {
				$searchParams["fields"] = array (
					[$answerPath] => 1,
					'links.organizations' => 1,
				);	
			}
			$answers= FranceTierslieux::globalAutocomplete($form, $searchParams);
			if(isset($answers["count"])) {
				$res["count"]=$answers["count"];
			}
			$res["results"] = $answers["results"];
			$res["results"] = FranceTierslieux::addAnswerAndPerson($res["results"]);
			$res["results"] = FranceTierslieux::addMobilized($res["results"]);
		}
		return Rest::json( $res );
	}
}
