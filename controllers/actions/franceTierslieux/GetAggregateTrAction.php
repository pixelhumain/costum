<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use Rest;
use PHDB;
use Answer;
use Organization;
use Citoyen;

class GetAggregateTrAction extends \PixelHumain\PixelHumain\components\Action {
  public function run() {
    $gatewayPath = $_POST["allPaths"] ?? [];
    $scoreCounterArray = [
        '$add' => []
    ];
    $scoreCounterString = [
        '$add' => []
    ];
    $result = Rest::json(["anyResults" => false, "profileResults" => []]);
    foreach ($gatewayPath as $key => $value) {
        if (isset($value)) {
            if (is_array($value)) {
                
                foreach ($value as $key2 => $value2) {
                    $pathValue = (is_array($value2) && isset(array_keys($value2)[0])) ? array_keys($value2)[0] : $value2;
                    $scoreCounterArray['$add'][] = [
                        '$cond' => [
                            ['$isArray' => '$' . $key],
                            [
                                '$cond' => [
                                    ['$in' => [$pathValue, '$' . $key . '.' . $pathValue . '.value']],
                                    1,
                                    0
                                ]
                            ],
                            0
                        ]
                    ];
                }
            } else if (is_string($value)) {
                $scoreCounterString['$add'][] = [
                    '$cond' => [
                        ['$eq' => ['$' . $key, $value]],
                        1,
                        0
                    ]
                ];
            }
        }
    }

    $projectStage = [
        '$project' => [
            'user' => 1,
            "created" => 1,
            'links.organizations' => 1,
            'totalScoreArrayResults' => 1,
            'totalScoreStringResults' => 1,
            'totalScoreAllScore' => 1,
            'answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb' => 1
        ]
    ];
    $scoreStorageArrayInput = [
        '$addFields' => [
            'totalScoreArrayResults' => ['$add' => $scoreCounterArray['$add']] // Use the $add array directly
        ]
    ];

    $scoreStorageStringInput = [
        '$addFields' => [
            'totalScoreStringResults' => ['$add' => $scoreCounterString['$add']] // Use the $add array directly
        ]
    ];

    $limitStage = ['$limit' => (isset($_POST["limit"]) && is_numeric($_POST["limit"])) ? intval($_POST["limit"]) : 5];
    $sortStage = [
        '$sort' => [
            // 'totalScoreAllScore' => -1,
            'totalScoreStringResults' => -1,
            'totalScoreArrayResults' => -1
        ]
    ];
    $totalScoreStage = [
        '$addFields' => [
            'totalScoreAllScore' => ['$add' => ['$totalScoreArrayResults', '$totalScoreStringResults']]
        ]
    ];
    $pipeline = [$scoreStorageArrayInput, $projectStage, $scoreStorageStringInput, $totalScoreStage ,$sortStage, $limitStage];
   
    $results = PHDB::aggregate(Answer::COLLECTION, $pipeline);
    //  $result = Rest::json(["anyResults" => true, "profileResults" => $results["result"], "markerData" => $mapMarkerdata, "chartData" => $chartdata]);
    if (isset($results)) {
        $result = Rest::json(["anyResults" => true, "profileResults" => $results["result"]]);
    }
    return $result;
  }
}

?>



