<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, FranceTierslieux, Rest;
class GetNetworkAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = FranceTierslieux::getNetwork($_POST["costumSlug"]);
        
        return Rest::json($params);
    }
    	
}
    	
