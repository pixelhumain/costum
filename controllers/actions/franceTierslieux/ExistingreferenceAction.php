<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, Yii, HtmlHelper;


class ExistingreferenceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($slug=null,$tag=null,$action=null,$format=null)
    {

$cs = Yii::app()->getClientScript();
$cssAnsScriptFilesModule = array(
		'/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
		'/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js'
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
$userId = Yii::app()->session["userId"] ;
$userId = Yii::app()->session["userId"] ;

$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';
//header + menu
/*$this->renderPartial($layoutPath.'header', 
                    array(  "layoutPath"=>$layoutPath , 
                            "page" => "admin") );*/
?>
<style>
	.dropdown-menu{
		width: 100%;
	}
	.btn-add-to-directory{
		font-size: 14px;
		margin-right: 0px;
		border-radius: 6px;
		color: #666;
		border: 1px solid rgba(188, 185, 185, 0.69);
		margin-left: 3px;
		float: left;
		padding: 1px;
		width: 24px;
		margin-top: 15px;
	}
  .img-logo {
    height: 290px;
  }
  .btn-filter-type{
    height:35px;
    border-bottom: 3px solid transparent;
  }
  .btn-filter-type.active{
    height:35px;
    border-bottom: 3px solid #383f4e;
  }
  .btn-filter-type:hover{
    height:35px;
    border-bottom: 3px solid #383f4e;
  }
  .btn-scope{
    display: inline;
  }
  .lbl-scope-list {
    top: 255px;
  }
  .btn-tag{
    font-weight:300;
    padding-left: 0px;
  }
  .btn-tag.bold{
    font-weight:600;
  }
  	.searchEntity{
	    padding: 10px;
	}
  	.searchEntity:hover{
	    background-color: #d9d9d9;
	}

	#searchLink{
		margin-top: 10px;
		margin-bottom: 10px;
	}

	#searchLink .entityName{
		font-size: 14px;
	}

	.vcenter {
	    display: inline-block;
	    vertical-align: middle;
	    float: none;
	}
  
  @media screen and (max-width: 1024px) {
    #menu-directory-type .hidden-sm{
     display:none;
    }
  }

@media screen and (max-width: 767px) {
  .searchEntity{
        /*margin-left: 25px !important;*/
  }
  	
	  #searchBarText{
    font-size:13px !important;
    margin-right:-30px;
  }
  /*.btn-add-to-directory {
      position: absolute;
      right: 0px;
      z-index:9px !important;
  }*/
}


</style>

<div class="col-lg-offset-1 col-lg-10 col-xs-12 no-padding">
	<div class="col-xs-12 no-padding ">
		<h4>Obligatoire</h4>
		
		<div class="col-sm-4 col-xs-12">
			<label for="fileImport">Fichier csv data_a_verifier :</label>
			<input type="file" id="fileImport" name="fileImport" accept=".csv">
		</div>
	</div>
	

	<div class="col-xs-12 center">
		<br/><a href="javascript:;" class="btn btn-primary col-sm-3" id="sumitVerification">Ajouter</a>
	</div>
	
		
</div>

		
	
	


<script type="text/javascript">
var file = "" ;
var extensions = ["csv"];
var nameFile = "";
var typeFile = "";
var typeElement = "";
var searchType = [ "persons" ];
jQuery(document).ready(function() 
{
	
	bindAddData();

});



function bindAddData(){

	$("#fileImport").change(function(e) {
    	var nameFileSplit = $("#fileImport").val().split("."); 
  		if(extensions.indexOf(nameFileSplit[nameFileSplit.length-1]) == -1){
  			toastr.error("Vous devez sélectionner un fichier csv");
  			return false ;
  		}
  		nameFile = nameFileSplit[0];
		typeFile = nameFileSplit[nameFileSplit.length-1];
		file = "";
		if (e.target.files != undefined) {
			var reader = new FileReader();
			reader.onload = function(e) {
				file = e.target.result;
				mylog.log("file ok",file);
			};
			reader.readAsText(e.target.files.item(0));
			mylog.log("reader ok",file);
		}
		return false;

	});

	$("#sumitVerification").off().on('click', function(e){
  		if(file == ""){
  			toastr.error("Vous devez sélectionner un fichier");
  			return false ;
  		}

  		mylog.log("file params",file);

  		var params = {
        		file : file        	
        };

        


  		ajaxPost(
	        null,
	        baseUrl+"/"+moduleId+"/datamigration/referencingFtl",
	        params,
	        function(data){ 
				alert(data);
	        
		  		}
		  		
	        	
	    );
		 		

		
		
		return false;
  		
  	});

}










</script>

<?php }} ?>