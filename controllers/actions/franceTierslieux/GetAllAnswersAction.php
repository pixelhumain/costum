<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use PHDB;
use Rest;
use Organization;
use Answer;
use MongoId;
use FranceTierslieux;
use Form;
use MongoRegex;

class GetAllAnswersAction extends \PixelHumain\PixelHumain\components\Action {
  public function run($type=Answer::COLLECTION,$form=null, $showOrga="false", $orgaIdInStart="false", $getMarkers="false", $getForm="false", $network="false", $subkey=null) { 
    $controller = $this->getController();
    $res = [];
    $showInputs = [];
    $where = [];
    $mergeOrgaAns = [];
    $formParam = (isset($_POST["formParam"])) ? $_POST["formParam"] : ["subForms"];
    if ($subkey != null) {
      $results = array("result" => false, "allDocuments" => []);
      $allDocument = PHDB::find($type, array("subKey" => $subkey));
      if (isset($allDocument)) {
        foreach ($allDocument as $key => $value) {
          $value["answerContent"] = [];
          if (isset($value["id"])) {
            $getAnswer = PHDB::findOneById(Answer::COLLECTION, $value["id"], $_POST["show"] ?? []);
            $value["answerContent"] = $getAnswer;
            $newKey = $getAnswer["answers"][$_POST["nameStep"]][$_POST["nameInput"]] . "-" . $key;
            $results["allDocuments"][$newKey] = $value;
          }
        }
        $results["result"] = true;
      }
      return Rest::json($results); 
    }
    if (isset($_POST["show"])) {
      foreach ($_POST["show"] as $key => $value) {
        $showInputs [$value] = 1;
        $mergeOrgaAns [$value] = 1;
      }
    }
    if (isset($_POST["inputs"])) {
      foreach ($_POST["inputs"] as $key => $value) {
        $showInputs [$key] = 1;
      }
    }
    try {
      if ($form != null) 
        $where["form"] = $form;
      
      if (isset($_POST["where"])) 
        $where = $_POST["where"]; 

      if ($getMarkers == "false") {
        if ($showOrga == "true") {
          $allAnswersId = PHDB::find($type, $where, $mergeOrgaAns);
          $answerWithOrga = [];
          foreach ($allAnswersId as $key => $value) {
            if (isset($value["links"]["organizations"]) && isset(array_keys($value["links"]["organizations"])[0])) {
              $orgaAnswers = [];
              $organizationId = array_keys($value["links"]["organizations"])[0];
              $showInOrga = [];
              if (isset($_POST["showInOrga"])) {
                $showInOrga = $_POST["showInOrga"];
              }  
              $organization = PHDB::findOne(Organization::COLLECTION, array("_id" => new MongoId ( $organizationId )), $showInOrga);
              if ($orgaIdInStart == "true") {
                $orgaAnswers[$organizationId] = $organization;             
              } else {
                $orgaAnswers = $organization;
              }
              $answerWithOrga[$key] = array_merge($orgaAnswers, $value);
            }
          }       
          $res = array("result"=>true, "allAnswers" => $answerWithOrga);
        } else {
          $res = array("result"=>true, "allAnswers" => PHDB::find($type, $where, $showInputs));
          if(array_key_exists("answers.franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab", $showInputs)){
            $data = $where['$or'];
            $ids = array_map(function($key){
              $cle = array_keys($key)[0];
              return explode(".", $cle)[2];
            }, array_values($data));
            $res["organizations"] = PHDB::findByIds(Organization::COLLECTION, $ids);
          }
        }
        
      } else {
        $arrayFormPath = (isset($_POST["arrayFormPath"])) ? $_POST["arrayFormPath"] : [];
        $showAnswers = (count($arrayFormPath) == 0) ? false : ((isset($_POST["showAnswers"]) && $_POST["showAnswers"] == "true" || $_POST["showAnswers"] == true) ? true : false);
        $results = FranceTierslieux::getMarkersAnswers($where, $arrayFormPath, $showAnswers, $network);
        $res = array("result"=>true, "markers" => $results['markerData'], "suplAnswers" => $results['supplementAnswer']);
      }
      if ($getForm == "true" && isset($form)) {
        $formData = PHDB::findOneById(Form::COLLECTION, $form, $formParam);
        $allInputs = [];
        if (isset($formData["subForms"]) && count($formData["subForms"]) > 0) {
          foreach ($formData["subForms"] as $key => $value) {
            $inputData = PHDB::findOne(Form::COLLECTION, array("id" => $value), ["inputs"]);
            array_push($allInputs, $inputData["inputs"]);
          }
        }
        $res = array_merge($res, array("form" => $formData,"inputs" => $allInputs));
      }
    } catch (CTKException $e) {
      $res = array("result"=>false, "msg"=>$e->getMessage());
    }
  return Rest::json($res);
  }

}

?>