<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, FranceTierslieux, Rest, PHDB, MongoId, organization;
class AddInseeDensityAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $id=$_POST["id"] ?? null;
        $element=$_POST["element"] ?? null;
        if(isset($_POST["element"]["address"])){
            $res=FranceTierslieux ::addInseeDensityToAddress($id,$element);
        }else{
            $element=PHDB::findOne(Organization::COLLECTION,array("_id"=>new MongId($id)));
            if(isset($element["address"])){
                $res=FranceTierslieux ::addInseeDensityToAddress($id,$element);
            }else{
                $res=array("result"=>false, "msg"=>"L'élément n'a pas d'adresse pour ajouter un densité Insee");
            }

        }    
        
        return Rest::json($res);
    }
    	
}