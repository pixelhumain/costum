<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator;

use CacheHelper;
use DataValidator;
use Lists;
use MongoDate;
use PHDB;
use Rest;
use Yii;

class IconsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $data = DataValidator::clearUserInput($_POST);
        $costum = CacheHelper::getCostum();
        $lists = PHDB::findOne(Lists::COLLECTION, array("name" => "navigatorIcons"));
        if($data["type"] == "fetch"){
            $icons = $lists["list"] ?? [];
        }else{
            if($lists){
                $icons = $lists["list"];
                $icons[$data["label"]] = $data["icon"];
                PHDB::update(Lists::COLLECTION, array("name" => "navigatorIcons"), array('$set' => array("list" => $icons)));
            }else{
                $icons = array($data["label"] => $data["icon"]);
                PHDB::insert(Lists::COLLECTION, array(
                    "name" => "navigatorIcons",
                    "list" => $icons,
                    "created" => new MongoDate(),
                    "creator" => Yii::app()->session["userId"],
                    "parent" => [
                        $costum["contextId"] => [
                            "type" => $costum["contextType"]
                        ]
                    ]
                ));
            }
        }
        return Rest::json(array("results" => true, "msg" => $data["type"] == "fetch" ? "Icons fetched" : "Icons updated" , "data" => $icons));
    }
}

