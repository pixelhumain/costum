<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator;

use CacheHelper;
use DataValidator;
use Lists;
use MongoDate;
use NavigatorCriteria;
use PHDB;
use Rest;
use Yii;

class CriteriaDescriptionAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $data = DataValidator::clearUserInput($_POST);
        $costum = CacheHelper::getCostum();
        $lists = PHDB::find(NavigatorCriteria::COLLECTION, [], ["name", "description"]);
        $criteria = [];
        foreach ($lists as $key => $list) {
            $criteria[$list["name"]] = $list["description"];
        }
        return Rest::json(array("results" => true, "msg" => "Criteria descriptions fetched", "data" => $criteria));
    }
}

