<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator;

use CacheHelper;
use DataValidator;
use Lists;
use MongoDate;
use NavigatorCriteria;
use PHDB;
use Rest;
use Yii;

class SaveCriteriaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $data = DataValidator::clearUserInput($_POST);
        $costum = CacheHelper::getCostum();
        if(isset($data["name"]) && isset($data["content"])){
            $criteria = array(
                "name" => $data["name"],
                "collection" => NavigatorCriteria::COLLECTION,
                "description" => $data["content"],
                "created" => new MongoDate(time()),
                "updated" => new MongoDate(time()),
                "parent" => [
                    $costum["contextId"] => [
                        "type" => $costum["contextType"]
                    ]
                ],
                "creator" => Yii::app()->session["userId"]
            );

            PHDB::insert(NavigatorCriteria::COLLECTION, $criteria);            
            return Rest::json(array("results" => true, "msg" => "Criteria saved successfully", "data" => $criteria));
        }
        return Rest::json(array("results" => false, "msg" => "Criteria not saved"));
    }
}

