<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction;
use FranceTierslieux;
use Rest;
use Organization;
use Yii;
use News;
use \PixelHumain\PixelHumain\modules\news\controllers\actions\GetAction;

class GetLikedNewsAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type=Organization::COLLECTION){
        $controller = $this->getController();
        $getNewByElement = new GetAction($this->controller->id, Yii::$app);
        $allNews = array();
        $likedElement = $_POST["elements"];
        foreach ($likedElement as $value) {
            if (isset($value)) {
                $currentNews = $getNewByElement -> run($type, $value, null, null, false, false,true)["news"];
                if (count($allNews) == 0)
                    $allNews = $currentNews; 
                else 
                    $allNews = array_merge($allNews, $currentNews);
            }
        }
        if (isset($allNews) && count($allNews) > 0) {
            $allNews = News::sortNews($allNews, array('updated' => SORT_DESC));
            $params= array(
    			"news"=> $allNews, 
    			"actionController"=>"save",
    			"canManageNews"=>false,
    			"canPostNews"=>false,
                "nbCol" => 1,
                "endStream"=>false,
                "pair" => false);
		    return $controller->renderPartial("news.views.co.timelineTree", $params, true);
        } else {
            return "";
        }
    }
}