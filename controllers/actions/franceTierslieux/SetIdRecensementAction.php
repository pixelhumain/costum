<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, FranceTierslieux, Rest, PHDB, MongoId, organization;
class SetIdRecensementAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($id = null,$type= null,$form = null, $view = null, $page =null){
        $controller = $this->getController();
        $orga=PHDB::findOne($type,array("_id"=>new MongoId($id)));
        $res=[];
        if(empty($orga["idRecensement23"])){
            $idsRec=PHDB::findAndSort(Organization::COLLECTION,array("idRecensement23"=>array('$exists'=>1)),array("idRecensement23"=>1));
            // var_dump(array_keys($idsRec)[count($idsRec)-1]);
            $nextId=(end($idsRec)["idRecensement23"]+1);
            $res["msg"]="Nouvel id du recensement";
            $res["idRecensement"]=$nextId;
            $res["toSet"]=true;
            PHDB::update($type,array("_id"=>new MongoId($id)),array('$set'=>array("idRecensement23"=>$nextId)));
            // var_dump($nextId);exit;
            // exit;
        }else{
            $res["msg"]="L'id du recensement existe déjà";
            $res["idRecensement"]=$orga["idRecensement23"];
            $res["toSet"]=false;
        }

        
        return Rest::json($res);
    }
    	
}