<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use Rest;
use SearchNew;
use PHDB;
use Document;
use Answer;

/**
 * Display the directory of back office
 * @param String $id Not mandatory : if specify, look for the person with this Id.
 * Else will get the id of the person logged
 * @return type
 */
class AutoGlobalThematicNtwrkAction extends \PixelHumain\PixelHumain\components\Action
{
    private $thematicArray = [];
    private $thematicResults = [];
	public function run(){
		$controller = $this->getController();
        $result = [];
        if(isset($_POST["params"])) {
            $paramsGlobal = $_POST;
            $data = SearchNew::globalAutoComplete($paramsGlobal);
            $paramsGlobal["path"] = $paramsGlobal["params"]["thematicPath"];
            if(isset($paramsGlobal["params"]["finderPath"])){
                $paramsGlobal["finderPath"] = $paramsGlobal["params"]["finderPath"];
            }
            $result = $this->sortElement($paramsGlobal, $data);
        }else {
            $paramsGlobal = array(
                "searchType" => ["answers"],
                "indexMin" => 0,
                "indexStep" => 10000,
                "count" => false,
                "fediverse" => false,
                "sortBy" => array("updated" => -1),
                "notSourceKey" => true,
                "initType" => "",
                "locality" => "",
                "fields" => [],
                "filters" => ['$or' => []]
            );
            $searchData = $_POST["searchedData"];
            array_map(function($key) use (&$paramsGlobal, $searchData){
                array_push($paramsGlobal["fields"], "answers.".str_replace("radioNew", "", str_replace("checkboxNew", "", $searchData[$key]["path"])));
                if(isset($searchData[$key]["finderPath"])){
                    array_push($paramsGlobal["fields"], $searchData[$key]["finderPath"]);
                }else{
                    array_push($paramsGlobal["fields"], "links.organizations");
                }
                //array_push($paramsGlobal["filters"]['$or'], "answers.".str_replace("radioNew", "", str_replace("checkboxNew", "", $searchData[$key]["path"])));
                $paramsGlobal["filters"]['$or']["answers.".str_replace("radioNew", "", str_replace("checkboxNew", "", $searchData[$key]["path"]))] = ['$exists' => true];
            },array_keys($searchData));
            $paramsGlobal["fields"] = array_unique($paramsGlobal["fields"]);
            $data = SearchNew::globalAutoComplete($paramsGlobal);
            foreach ($_POST["searchedData"] as $keyAnswer => $answer) {
                $answer["fields"] = [$answer["path"]];
                if(isset($answer["finderPath"])){
                    array_push($answer["fields"], $answer["finderPath"]);
                }
                $result[$keyAnswer] = $this->sortElement($answer, $data);
            }
            // $result["distinctElements"] = $this->thematicResults["distinctElements"] ?? [];
        }
        return Rest::json($result);
	}

    private function sortElement($data, $res){
        $this->thematicResults = [];
        $this->thematicArray = [];
        $thematicPath = explode(".",$data["path"]) ?? [];
        $thematicFinderPath = isset($data["finderPath"]) ? explode(".",$data['finderPath']) : [];
        if(count($thematicFinderPath) <= 0){
            $thematicFinderPath = explode(".", "links.organizations");
            array_push($data["fields"], "links.organizations");
        }
        if (isset($res["results"]) && isset($thematicPath[0]) && isset($thematicPath[1])) {
            if(str_contains($thematicPath[1], "multiCheckboxPlus")){
                $input = str_replace('multiCheckboxPlus', '', $thematicPath[1]);
                foreach ($res["results"] as $key => $value) {
                    if (isset($value['answers'][$thematicPath[0]][$thematicPath[1]]) && count($value['answers'][$thematicPath[0]][$thematicPath[1]]))
                        $thematicValue = $value['answers'][$thematicPath[0]][$thematicPath[1]];
                    if (isset($thematicValue)) {
                        for ($i=0; $i < count($thematicValue); $i++) { 
                            if (isset(array_keys($thematicValue[$i])[0])) {
                                $networkThematic = array_keys($thematicValue[$i])[0];
                                $this->addElements($networkThematic, $value, $thematicFinderPath, $input, $data);
                            }
                        } 
                    }     
                }
            }else if(str_contains($thematicPath[1], "radioNew") || str_contains($thematicPath[1], "checkboxNew")){
                $input = str_replace("checkboxNew", "", str_replace('radioNew', '', $thematicPath[1]));
                foreach ($res["results"] as $key => $value) {
                    if (isset($value['answers'][$thematicPath[0]][$input]) && count($value['answers'][$thematicPath[0]][$input]))
                        $thematicValue = $value['answers'][$thematicPath[0]][$input];
                    if (isset($thematicValue)) {
                        if(gettype($thematicValue) == "string"){
                            $this->addElements($thematicValue, $value, $thematicFinderPath, $input, $data);
                        } else {
                            for ($i=0; $i < count($thematicValue); $i++) { 
                                if (isset($thematicValue[$i])) {
                                    $networkThematic = $thematicValue[$i];
                                    $this->addElements($networkThematic, $value, $thematicFinderPath, $input, $data);
                                }
                            }
                        }
                    }     
                }
            }else if(str_contains($thematicPath[1], "multiRadio")){
                foreach ($res["results"] as $key => $value) {
                    if (isset($value['answers'][$thematicPath[0]][$thematicPath[1]]) && count($value['answers'][$thematicPath[0]][$thematicPath[1]]))
                        $thematicValue = $value['answers'][$thematicPath[0]][$thematicPath[1]];
                    if (isset($thematicValue)) {
                        $networkThematic = $thematicValue["value"];
                        $this->addElements($networkThematic, $value, $thematicFinderPath, $thematicPath[1], $data);
                    }     
                }
            }else if(str_contains($thematicPath[1], "finder")){
                foreach ($res["results"] as $key => $value) {
                    if (isset($value['answers'][$thematicPath[0]][$thematicPath[1]])) {
                        $this->addElements("Oui", $value, $thematicFinderPath, $thematicPath[1], $data);
                    }     
                }
            }
            $res["results"] = $this->thematicResults;
            $res["count"]["answers"] = count($this->thematicArray);
            return $res;
        }
        return ["results" => [], "count" => ["answers" => 0]];
    }

    private function addElements($value, $element, $finderPath, $input, $data){
        // var_dump($element);exit;
        if(!isset($this->thematicResults["distinctElements"])){
            $this->thematicResults["distinctElements"]=[(string)$element["_id"]];
        }else if(!in_array((string)$element["_id"], $this->thematicResults["distinctElements"])){
            array_push($this->thematicResults["distinctElements"],(string)$element["_id"]);
        }
        if (!in_array($value, $this->thematicArray, true)) {
            array_push($this->thematicArray, $value);
            $keyRef = strtolower(preg_replace('/[^a-z0-9]+/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $value)));
            $keyRef = $input.$keyRef;
            $image = PHDB::findOne(Document::COLLECTION, ["contentKey" => $keyRef, "type" => Answer::COLLECTION]);
            $this->thematicResults[$value] = [];
            $this->thematicResults[$value]["image"] = (@$image) ? Document::getDocumentPath($image, true) : "";
            $this->thematicResults[$value]["name"] = $value;
            $this->thematicResults[$value]["orgaNameArray"] = [];
        }
        if(!in_array("links.organizations", $data["fields"])){
            if (isset($element[$finderPath[0]][$finderPath[1]][$finderPath[2]]) && count(array_keys($element[$finderPath[0]][$finderPath[1]][$finderPath[2]])) > 0) {
                $finderContent = array_keys($element[$finderPath[0]][$finderPath[1]][$finderPath[2]])[0] ?? [];
                if (isset($finderContent)) {
                    array_push($this->thematicResults[$value]["orgaNameArray"], $finderContent);
                }
            }
        }else{
            if (isset($element[$finderPath[0]][$finderPath[1]]) && count(array_keys($element[$finderPath[0]][$finderPath[1]])) > 0) {
                $finderContent = array_keys($element[$finderPath[0]][$finderPath[1]])[0] ?? [];
                if (isset($finderContent)) {
                    array_push($this->thematicResults[$value]["orgaNameArray"], $finderContent);
                }
            }
        }
    }
}
