<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;
use FranceTierslieux, Rest;
class GetNetworksWereListAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
        $controller = $this->getController();
        $res = FranceTierslieux::getAllNetworkWithCondition();
        return Rest::json($res);
    }
    	
}
    	
