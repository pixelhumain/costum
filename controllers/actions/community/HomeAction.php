<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\community;

use CAction, Yii;
class HomeAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($tpl=null)
    {
    	$controller = $this->getController();    	


    	
        $params = [];
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    	
    }
}