<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coSindniSmarterre;

use Action;
use CAction, Rest,Form,PHDB;
use Citoyen;
use DateTime;
use DateTimeZone;
use Exception;
use MongoDate;
use MongoId;
use Organization;
use Zone;
use SearchNew,MongoRegex;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
use Project;
use Slug;

class ObservatoryAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($request){
        $controller = $this->getController();
        $results = $this->$request();
        return Rest::json($results);
    }


    private function dataCosindni(){
        $form = $_POST["form"];
        $results = [
            "quartiers" => array(),
            "financers" => array(),
            "tags" => array(),
        ];

        $results["quartiers"] = PHDB::distinct(Form::ANSWER_COLLECTION,"answers.aapStep1.interventionArea",array(
            "form" => $form,
            "answers.aapStep1.titre" => ['$exists' => true]
        ));
        $results["quartiers"] = array_map(function($q){
            if(Api::isValidMongoId($q))
                return new MongoId($q);
        },$results["quartiers"]);
        $results["quartiers"] = PHDB::find(Zone::COLLECTION,array("_id" => ['$in'=> $results["quartiers"]]),array("name"));


        $results["financers"] = PHDB::distinct(
            Form::ANSWER_COLLECTION,
            "answers.aapStep1.depense.financer.id",
            array("form" => $_POST["form"])
        );
        $results["financers"] = array_map(function($v){
            return new MongoId($v);
        },$results["financers"]);
        if(!empty($results["financers"])){
            $results["financers"] = PHDB::find(Organization::COLLECTION,array('_id' => ['$in' => $results["financers"]]),array("name"));
            usort($results["financers"], function($a, $b) {
                return $a['name'] > $b['name'];
            });
        }
            

        $results["tags"] = PHDB::distinct(
            Form::ANSWER_COLLECTION,
            "answers.aapStep1.tags",
            array("form" => $_POST["form"])
        );
        
        return $results;
    }

    /********************************************* NOMBRE DE PROPOSITION ****************************************************/
    private function numberOfActionPerYear(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => ['$exists' => true],
                )
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.year',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        
        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return (int)$a['_id'] - (int)$b['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerThematique(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.tags" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array(
                '$unwind' => '$answers.aapStep1.tags'
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.tags',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerPilier(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.principalDomaine" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array(
                '$unwind' => '$answers.aapStep1.principalDomaine'
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.principalDomaine',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        
        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerAssos(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.association" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.association',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerFinancer(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => ['$exists' => true],
                    "answers.aapStep1.depense.financer.name" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array('$unwind' => '$answers.aapStep1.depense'),
            array('$unwind' => '$answers.aapStep1.depense.financer'),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.depense.financer.name',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerAxis(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.axesTFPB" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.axesTFPB',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        
        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function numberOfActionPerSpecificGoals(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
        array(
            array(
                '$match' => array(
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.aapStep1l1xfdtuazek4jhgcco" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session],
                )
            ),
            array(
                '$unwind' => '$answers.aapStep1.aapStep1l1xfdtuazek4jhgcco'
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.aapStep1l1xfdtuazek4jhgcco',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        
        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });
        return $proposals["result"];
    }

    private function combineNumberOfAction(){
        $results = array(
            "numberOfActionPerYear" => $this->numberOfActionPerYear(),
            "numberOfActionPerThematique" => $this->numberOfActionPerThematique(),
            "numberOfActionPerPilier" => $this->numberOfActionPerPilier(),
            "numberOfActionPerAssos" => $this->numberOfActionPerAssos(),
            "numberOfActionPerFinancer" => $this->numberOfActionPerFinancer(),
            "numberOfActionPerAxis" => $this->numberOfActionPerAxis(),
            "numberOfActionPerSpecificGoals" => $this->numberOfActionPerSpecificGoals()
        );
        return $results;
    }
    /********************************************* END NOMBRE DE PROPOSITION **************************************************/

    /********************************************* FINANCEMENTS ****************************************************/
    public function financementPerYear(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $query = array();

        $results = [];

        $query = [
            [
                '$match'=> array(
                    'form'=> $form,
                    'answers.aapStep1.depense.financer'=> array('$exists'=>true),
                    "answers.aapStep1.interventionArea" => ['$exists' => true],
                )
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.financer'
            ],
            [
                '$project'=> [
                    '_id'=> 0,
                    'date'=> '$answers.aapStep1.year',
                    'amount' => '$answers.aapStep1.depense.financer.amount'
                ],

            ]
        ];
        if(!empty($quartiers)){
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        }
        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $financer = PHDB::aggregate(Form:: ANSWER_COLLECTION,$query);
        
        $results = $this->getCreatedYears($form);

        foreach ($financer["result"] as $k => $v) {
            if(!empty($v["date"]) && !empty($v["amount"])){
                $results[$v["date"]] += (int)$v["amount"];
            }
        }

        return $results;
    }

    public function financementPerThematique(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.interventionArea" => ['$exists' => true],
                        "answers.aapStep1.tags" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array(
                    '$unwind' => '$answers.aapStep1.tags'
                ),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.tags',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });

        foreach ($proposals["result"] as $k => $v) {
            if(!empty($v["amount"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += array_sum($v["amount"][0]);
                }else{
                    $result[$v["_id"]] = array_sum($v["amount"][0]);
                }
            }
        }
        return $result;
    }

    public function financementPerPilier(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.interventionArea" => ['$exists' => true],
                        "answers.aapStep1.principalDomaine" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array(
                    '$unwind' => '$answers.aapStep1.principalDomaine'
                ),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.principalDomaine',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);

        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });

        foreach ($proposals["result"] as $k => $v) {
            if(!empty($v["amount"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += array_sum($v["amount"][0]);
                }else{
                    $result[$v["_id"]] = array_sum($v["amount"][0]);
                }
            }
        }

        return $result;
    }

    public function financementPerAssoc(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.interventionArea" => ['$exists' => true],
                        "answers.aapStep1.association" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array(
                    '$unwind' => '$answers.aapStep1.association'
                ),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.association',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);

        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });

        foreach ($proposals["result"] as $k => $v) {
            if(!empty($v["amount"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += array_sum($v["amount"][0]);
                }else{
                    $result[$v["_id"]] = array_sum($v["amount"][0]);
                }
            }
        }

        return $result;
    }

    public function financementPerFinancer(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.interventionArea" => ['$exists' => true],
                        "answers.aapStep1.depense.financer.name" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array('$unwind' => '$answers.aapStep1.depense'),
                array('$unwind' => '$answers.aapStep1.depense.financer'),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.depense.financer.name',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        usort($proposals["result"], function($a, $b) {
            return @$b['_id'] < @$a['_id'];
        });
        foreach ($proposals["result"] as $k => $v) {
           if(!empty($v["amount"]) && !empty($v["_id"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += $v["amount"];
                }else{
                    $result[$v["_id"]] = $v["amount"];
                }
            }
        }

        return $result;
    }

    public function financementPerAxis(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.axesTFPB" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array(
                    '$unwind' => '$answers.aapStep1.axesTFPB'
                ),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.axesTFPB',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);

        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });

        foreach ($proposals["result"] as $k => $v) {
            if(!empty($v["amount"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += array_sum($v["amount"][0]);
                }else{
                    $result[$v["_id"]] = array_sum($v["amount"][0]);
                }
            }
        }

        return $result;
    }

    public function financementPerSpecificGoals(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.aapStep1l1xfdtuazek4jhgcco" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array(
                    '$unwind' => '$answers.aapStep1.aapStep1l1xfdtuazek4jhgcco'
                ),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.aapStep1l1xfdtuazek4jhgcco',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        
        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);

        usort($proposals["result"], function($a, $b) {
            return $b['_id'] < $a['_id'];
        });

        foreach ($proposals["result"] as $k => $v) {
            if(!empty($v["amount"])){
                if(!empty($result[$v["_id"]])) {
                    $result[$v["_id"]] += array_sum($v["amount"][0]);
                }else{
                    $result[$v["_id"]] = array_sum($v["amount"][0]);
                }
            }
        }

        return $result;
    }

    public function financementPerQuartier(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query =
            array(
                array(
                    '$match' => array(
                        "form" => $form,
                        "answers.aapStep1.titre" => ['$exists' => true],
                        "answers.aapStep1.interventionArea" => ['$exists' => true],
                        "answers.aapStep1.depense.financer.name" => ['$exists' => true],
                        "answers.aapStep1.year" => ['$in'=>$session],
                    )
                ),
                array('$unwind' => '$answers.aapStep1.interventionArea'),
                array('$unwind' => '$answers.aapStep1.depense'),
                array('$unwind' => '$answers.aapStep1.depense.financer'),
                [
                    '$project'=> [
                        "_id" => '$answers.aapStep1.interventionArea',
                        'amount' => '$answers.aapStep1.depense.financer.amount'
                    ],

                ]
            );
        if(!empty($quartiers))
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];

        if(!empty($financersFilters))
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];

        if(!empty($tagsFilters))
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];

        $result = [];

        $proposals = PHDB::aggregate(Form::ANSWER_COLLECTION,$query);
        foreach ($proposals["result"] as $k => $v) {
           if(!empty($v["amount"]) && !empty(trim($v["_id"]))){
                if(!empty($result[trim($v["_id"])])) {
                    $result[trim($v["_id"])] += $v["amount"];
                }else{
                    $result[trim($v["_id"])] = $v["amount"];
                }
            }
        }

        return $result;
    }

    private function combineFinancement(){
        $results = array(
            "financementPerYear" => $this->financementPerYear(),
            "financementPerThematique" => $this->financementPerThematique(),
            "financementPerPilier" => $this->financementPerPilier(),
            "financementPerAssoc" => $this->financementPerAssoc(),
            "financementPerFinancer" => $this->financementPerFinancer(),
            "financementPerAxis" => $this->financementPerAxis(),
            "financementPerSpecificGoals" => $this->financementPerSpecificGoals(),
            "financementPerQuartier" => $this->financementPerQuartier(),
        );
        return $results;
    }
    /********************************************* END FINANCEMENTS ****************************************************/


    /********************************************* METRICS ****************************************************/
    private function projectMetrics(){
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $projectIdFromAnswers = $this->getProjectByFormId($formId, ["answers.aapStep1.year" => ['$in' => $session]]);
        // $projectIdFromContext = [];
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers,$projectIdFromContext);
        $projectId = array_values(array_unique($projectId));

        $projectsMongoId = [];
        foreach ($projectId as $key => $value) {
            array_push($projectsMongoId, new MongoId($value));
        }
        $finishedQuery = [
            '$and' => [
                [
                    "_id" => ['$in' => $projectsMongoId]
                ],
                [
                    '$or' => [
                        ["parent" => ['$exists' => true]],
                        ["parentId" => ['$exists' => true]]
                    ]
                ],
                [
                    "properties.avancement" => ['$in' => ['finished', 'abandoned']]
                ],
            ]
        ];
        $inprogressQuery = [
            '$and' => [
                [
                    "_id" => ['$in' => $projectsMongoId]
                ],
                [
                    '$or' => [
                        ["parent" => ['$exists' => true]],
                        ["parentId" => ['$exists' => true]]
                    ]
                ],
                [
                    '$or' => [
                        ["properties.avancement" => ['$in' => ['idea', 'concept', 'started', 'development', 'testing', 'mature']]],
                        ["properties.avancement" => ['$exists' => false]]
                    ]
                ]
            ]
        ];
        $allQuery = [
            '$and' => [
                [
                    "_id" => ['$in' => $projectsMongoId]
                ],
                [
                    "parent" => ['$exists' => true]
                ]
            ]
        ];
        // if(!empty($quartiers)){
        //     array_push($finishedQuery, [
        //         "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
        //     ]);
        //     array_push($inprogressQuery, [
        //         "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
        //     ]);
        //     array_push($allQuery, [
        //         "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
        //     ]);
        // }
        $finished = PHDB::findAndSort(
            Project:: COLLECTION,
            $finishedQuery,
            [
                "updated" => -1
            ],
            0,
            ["_id"]
        );
        $inprogress = PHDB::findAndSort(
            Project:: COLLECTION,
            $inprogressQuery,
            [
                "updated" => -1
            ],
            0,
            ["_id"]
        );
        $results = [
            // "total" => PHDB::count(
            //                 Project:: COLLECTION,
            //                 $allQuery
            //             ),
            "total" => count($finished) + count($inprogress),
            "finished" => count($finished),
            "inprogress" => count($inprogress),
            "finishedData" => $finished,
            "inprogressData" => $inprogress
        ];

        return  $results;
    }
    private function actionMetrics(){
        $formId = $_POST["formId"];
        $contexts = $_POST["context"];
        $currentDateTime = (new DateTime())->getTimestamp();
        $projectIdFromAnswers = $this->getProjectByFormId($formId);
        $projectIdFromContext = $this->getProjectByContextId($contexts);
        $projectId = array_merge($projectIdFromAnswers,$projectIdFromContext);
        $projectId = array_values(array_unique($projectId));


        $results = [
            "total" => 0,
            "todo" => 0,
            "running" => 0,
            "done"=> 0,
            "deadlinePassed" => 0,
        ];

        if(!empty($projectId )){
            $results["total"] = PHDB::count(Action:: COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => ['$ne' => 'disabled']),
                    )
                )
            );
            $results["todo"] = PHDB::count(Action:: COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array(
                            '$or' => [
                                array("tracking" => false),
                                array("tracking" => ['$exists' => false])
                            ]
                        ),
                    )
                )
            );
            $results["running"] = PHDB::count(Action:: COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array("tracking" => true),
                    )
                )
            );
            $results["done"] = PHDB::count(Action:: COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "done"),
                    )
                )
            );
            $results["deadlinePassed"] = PHDB::count(Action:: COLLECTION,
                array(
                    '$and' => array(
                        array("parentId" => ['$in' => $projectId]),
                        array("status" => "todo"),
                        array(
                            '$or' => [
                                array("endDate" => ['$lt'=> new MongoDate($currentDateTime)]),
                                /*array('$expr' => [
                                    '$lt' => [
                                      ['$toDate'=> "endDate" ],
                                      $currentDateTime
                                    ]
                                ])*/
                            ]
                        )
                    )
                )
            );
        }

        return  $results;
    }
    private function proposalMetrics(){
        $formId = $_POST["formId"];
        $context = Slug::getBySlug($_POST["context"]);
        $quartiers = $_POST["quartiers"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $session = $_POST["session"];
        $results = [
            "total" => 0,
            "vote" => 0,
            "toBefinanced" => 0,
            "inFinancing"=> 0
        ];
        $voteQuery = [
            '$and' => [
                ["form" => $formId],
                ['status'=> "vote"],
                ['answers.aapStep1.titre' => ['$exists'=> true]],
                ['project.id' => ['$exists' => false]],
                ["answers.aapStep1.year" => ['$in' => $session]]
            ]
        ];
        $toBefinancedQuery = [
            '$and' => [
                ["form" => $formId],
                ['answers.aapStep1.titre' => ['$exists'=> true]],
                ['answers.aapStep1.depense' => ['$exists'=> true]],
                ['answers.aapStep1.depense.financer' => ['$exists'=> false]],
                ["answers.aapStep1.year" => ['$in' => $session]]
            ]
        ];
        $inFinancingQuery = [
            '$and' => [
                ["form" => $formId],
                ['answers.aapStep1.titre' => ['$exists'=> true]],
                ['answers.aapStep1.depense.financer' => ['$exists'=> true]],
                ["answers.aapStep1.year" => ['$in' => $session]]
            ]
        ];
        if(!empty($quartiers)){
            array_push($voteQuery['$and'], [
                "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
            ]);
            array_push($toBefinancedQuery['$and'], [
                "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
            ]);
            array_push($inFinancingQuery['$and'], [
                "answers.aapStep1.interventionArea" => ['$in' => $quartiers]
            ]);
        }
        if(!empty($financersFilters)){
            array_push($voteQuery['$and'], [
                "answers.aapStep1.depense.financer.id" => ['$in' => $financersFilters]
            ]);
            array_push($toBefinancedQuery['$and'], [
                "answers.aapStep1.depense.financer.id" => ['$in' => $financersFilters]
            ]);
            array_push($inFinancingQuery['$and'], [
                "answers.aapStep1.depense.financer.id" => ['$in' => $financersFilters]
            ]);
        }

        if(!empty($tagsFilters)){
            array_push($voteQuery['$and'], [
                "answers.aapStep1.tags" => ['$in' => $tagsFilters]
            ]);
            array_push($toBefinancedQuery['$and'], [
                "answers.aapStep1.tags" => ['$in' => $tagsFilters]
            ]);
            array_push($inFinancingQuery['$and'], [
                "answers.aapStep1.tags" => ['$in' => $tagsFilters]
            ]);
        }

        $proposalData = [
            "vote" => PHDB::findAndSort(Form:: ANSWER_COLLECTION,
                $voteQuery,
                ['updated' => -1],
                0,
                ["_id"]
            ),
            "toBefinanced" => PHDB::findAndSort(Form:: ANSWER_COLLECTION,
                $toBefinancedQuery,
                ["updated" => -1],
                0,
                ["_id"]
            ),
            "inFinancing" => PHDB::findAndSort(Form:: ANSWER_COLLECTION,
                $inFinancingQuery,
                ["updated" => -1],
                0,
                ["_id"]
            )
        ];

        if(!empty($formId )){
            $results["total"] = count($proposalData["vote"]) + count($proposalData["toBefinanced"]) + count($proposalData["inFinancing"]);
            $results["vote"] = count($proposalData["vote"]);

            $results["toBefinanced"] = count($proposalData["toBefinanced"]);

            $results["inFinancing"] = count($proposalData["inFinancing"]);

            $results["tobevotedData"] = $proposalData["vote"];
            $results["tobefinancedData"] = $proposalData["toBefinanced"];
            $results["infinancingData"] = $proposalData["inFinancing"];
        }

        return  $results;
    }

    private function financingMetrics(){
        $formId = $_POST["formId"];
        $context = Slug::getBySlug($_POST["context"]);
        $quartiers = $_POST["quartiers"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $session = $_POST["session"];

        $commonMatch = [
            'form'=> $formId,
            'answers.aapStep1.titre'=> array('$exists'=>true),
            "answers.aapStep1.interventionArea" => ['$exists' => true],
            "answers.aapStep1.year" => ['$in' => $session]
        ];

        $results = [
            "amountRequested" => 0,
            "amountFinanced" => 0,
            "amountSpent" => 0,
            "amountRemaining" => 0,
        ];

        if(!empty($formId )){
            $amountRequestedQuery = [
                [
                    '$match'=> $commonMatch
                ],
                [
                    '$unwind'=> '$answers.aapStep1.depense'
                ],
                /*[
                    '$match'=> [
                        "answers.aapStep1.depense.price"=> [ '$type'=> "number" ] // Filter out non-numeric "price" values
                    ]
                ],*/
                /*[
                    '$addFields'=> [
                        "answers.aapStep1.depense.price"=> [
                            '$cond'=> [
                                'if'=> [ '$eq'=> [[ '$type'=> '$answers.aapStep1.depense.price' ], "string"] ],
                                'then'=> [ '$toInt'=> '$answers.aapStep1.depense.price' ],
                                'else'=> '$answers.aapStep1.depense.price'
                            ]
                        ]
                    ]
                ],*/
                [
                    '$group'=> [
                        '_id'=> '$answers.aapStep1.year',
                        'totalDepense'=> [ '$sum'=> '$answers.aapStep1.depense.price']
                    ]
                ]
            ];

            $amountFinancedQuery = [
                [
                    '$match' => $commonMatch
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense'
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense.financer'
                ],
                /*[
                    '$addFields' => [
                        'answers.aapStep1.depense.financer.amount' => [
                            '$cond' => [
                                'if' => [
                                    '$eq' => [
                                        ['$type' => '$answers.aapStep1.depense.financer.amount'],
                                        'string'
                                    ]
                                ],
                                'then' => ['$toInt' => '$answers.aapStep1.depense.financer.amount'],
                                'else' => '$answers.aapStep1.depense.financer.amount'
                            ]
                        ]
                    ]
                ],*/
                [
                    '$group' => [
                        '_id' => '$answers.aapStep1.year',
                        'totalAmountFinanced' => ['$sum' => '$answers.aapStep1.depense.financer.amount']
                    ]
                ]
            ];
            $amountSpentQuery = [
                [
                    '$match' => $commonMatch
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense'
                ],
                [
                    '$unwind' => '$answers.aapStep1.depense.payement'
                ],
                [
                    '$addFields' => [
                        'answers.aapStep1.depense.payement.amount' => [
                            '$cond' => [
                                'if' => [
                                    '$eq' => [
                                        ['$type' => '$answers.aapStep1.depense.payement.amount'],
                                        'string'
                                    ]
                                ],
                                'then' => ['$toInt' => '$answers.aapStep1.depense.payement.amount'],
                                'else' => '$answers.aapStep1.depense.payement.amount'
                            ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'totalAmountSpent' => ['$sum' => '$answers.aapStep1.depense.payement.amount']
                    ]
                ]
            ];
            if(!empty($quartiers)){
                $amountRequestedQuery[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
                $amountFinancedQuery[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
                $amountSpentQuery[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
            }
            if(!empty($financersFilters)){
                $amountRequestedQuery[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
                $amountFinancedQuery[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
                $amountSpentQuery[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
            }
            if(!empty($tagsFilters)){
                $amountRequestedQuery[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
                $amountFinancedQuery[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
                $amountSpentQuery[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
            }

            $results["amountRequested"] = PHDB::aggregate(Form:: ANSWER_COLLECTION, $amountRequestedQuery);
            $results["amountRequested"] = isset($results["amountRequested"]["result"][0]["totalDepense"]) ? $results["amountRequested"]["result"][0]["totalDepense"] : 0;

            $results["amountFinanced"] = PHDB::aggregate(Form:: ANSWER_COLLECTION,$amountFinancedQuery);
            $results["amountFinanced"] = isset($results["amountFinanced"]["result"][0]["totalAmountFinanced"]) ? $results["amountFinanced"]["result"][0]["totalAmountFinanced"] : 0;

            $results["amountSpent"] = PHDB::aggregate(Form:: ANSWER_COLLECTION,$amountSpentQuery);
            $results["amountSpent"] = isset($results["amountSpent"]["result"][0]["totalAmountSpent"]) ? $results["amountSpent"]["result"][0]["totalAmountSpent"] : 0;

            $results["amountRemaining"] = ($results["amountRequested"] - $results["amountFinanced"]);
        }

        return  $results;
    }

    private function combineMetrics(){
        $results = [
            "projectMetrics" => $this->projectMetrics(),
            "actionMetrics" => $this->actionMetrics(),
            "proposalMetrics" => $this->proposalMetrics(),
            "financingMetrics" => $this->financingMetrics(),
        ];
        return $results;
    }

    public function totalFinanceByYear()
    {
        $formId = $_POST["form"];
        $quartiers = $_POST["quartiers"];
        //$context = Slug::getBySlug($_POST["context"]);
        //$financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $key = "year";// allTime : year, months, this week, today, given two date
        $query = array();
        $labels = array();
        //$months = [1,2,3,4,5,6,7,8,9,10,11,12];

        $results = [
            "depense" => [],
            "financement" => [],
            "payment" => [],
        ];
        $query_depense = [
            [
                '$match'=> array(
                    'form'=> $formId,
                    'answers.aapStep1.depense'=> array('$exists'=>true)
                )
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$project'=> [
                    '_id'=> 0,
                    'date'=> '$answers.aapStep1.year',
                    'amount' => '$answers.aapStep1.depense.price'
                ]
            ]
        ];

        $query_finance = [
            [
                '$match'=> array(
                    'form'=> $formId,
                    'answers.aapStep1.depense.financer'=> array('$exists'=>true)
                )
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.financer'
            ],
            [
                '$project'=> [
                    '_id'=> 0,
                    'date'=> '$answers.aapStep1.year',
                    'amount' => '$answers.aapStep1.depense.financer.amount'
                ]
            ]
                ];


        $query_payement = [
            [
                '$match'=> array(
                    'form'=> $formId,
                    'answers.aapStep1.depense.payement'=> array('$exists'=>true)
                )
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.payement'
            ],
            [
                '$project'=> [
                    '_id'=> 0,
                    'date'=> '$answers.aapStep1.year',
                    'amount' => '$answers.aapStep1.depense.payement.amount'
                ]
            ]
        ];

        if(!empty($quartiers)){
            $query_depense[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
            $query_finance[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
            $query_payement[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        }
        if(!empty($financersFilters)){
            $query_depense[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
            $query_finance[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
            $query_payement[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        }
        if(!empty($tagsFilters)){
            $query_depense[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
            $query_finance[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
            $query_payement[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        }

        $depense = PHDB::aggregate(Form:: ANSWER_COLLECTION, $query_depense);
        $financer = PHDB::aggregate(Form:: ANSWER_COLLECTION,$query_finance);
        $payement = PHDB::aggregate(Form:: ANSWER_COLLECTION, $query_payement);



        if($key == "year"){
            $labels = $this->getCreatedYears($formId);

            $depenses = [
                "label" => "Financement demandé",
                "data" => $labels,
                "backgroundColor" => "#f96e3f",
                "stack" => "stack 0"
            ];

            foreach ($depense["result"] as $k => $v) {

                    if(!empty($v["date"]) && !empty($v["amount"])){
                        $depenses["data"][$v["date"]] += (int)$v["amount"];
                    }


            }

            uksort($depenses["data"], fn($a, $b) => $a <=> $b);

            $depenses["data"] = array_values($depenses["data"]);

            $financements = [
                "label" => "Financé",
                "data" => $labels,
                "backgroundColor" => "#78d0c0",
                "stack" => "stack 1"
            ];

            foreach ($financer["result"] as $k => $v) {
               /* if(!empty($v["date"]) && !empty($v["amount"])){
                     $results["financement"][$v["date"]] += (int)$v["amount"];
                }*/
            }

            uksort($financements["data"], fn($a, $b) => $a <=> $b);
            $financements["data"] = array_values($financements["data"]);

            $payments= [
                "label" => "Payement",
                "data" => $labels,
                "backgroundColor" => "#007aff",
                "stack" => "stack 2"
            ];

            foreach ($payement["result"] as $k => $v) {
                if(!empty($v["date"]) && !empty($v["amount"])){
                    $results["payment"][$v["date"]] += (int)$v["amount"];
                }
            }
            uksort($financements["data"], fn($a, $b) => $a <=> $b);
            $payments["data"] = array_values($payments["data"]);
        }
        uksort($labels, fn($a, $b) => $a <=> $b);
        $results = ["years" => array_keys($labels), "data" => ["depenses" => $depenses, "financements" => $financements, "payements" => $payments]];
        return $results;
    }

    private function financingPerFunder() {
        $formId = $_POST["form"];
        $quartiers = $_POST["quartiers"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $session = $_POST["session"];
        $query = [
            [
                '$match' => [
                    "form" => $formId,
                    "answers.aapStep1.depense.financer.name" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => ['$exists' => true],
                    "answers.aapStep1.depense.financer.amount" => ['$exists' => true],
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session]
                ]
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.financer'
            ],
            [
                '$project'=> [
                    //_id => 0, Exclure le champ "_id" du résultat si nécessaire
                    'name'=> '$answers.aapStep1.depense.financer.name',
                    'id'=> '$answers.aapStep1.depense.financer.id',
                    'amount' => '$answers.aapStep1.depense.financer.amount',
                    'proposal' => '$answers.aapStep1.titre',
                ]
            ],
        ];
        $queryWithPayement = [
            [
                '$match' => [
                    "form" => $formId,
                    "answers.aapStep1.depense.payement" => ['$exists' => true],
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session]
                ]
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.payement'
            ],
            [
                '$project'=> [
                    //_id => 0, Exclure le champ "_id" du résultat si nécessaire
                    'name'=> '$answers.aapStep1.depense.payement.beneficiary.name',
                    'id'=> '$answers.aapStep1.depense.payement.beneficiary.id',
                    'amount' => '$answers.aapStep1.depense.payement.amount',
                    'proposal' => '$answers.aapStep1.titre',
                ]
            ]
        ];

        if(!empty($quartiers)){
            $query[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
            $queryWithPayement[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        }

        if(!empty($financersFilters)){
            $query[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
            $queryWithPayement[0]['$match']["answers.aapStep1.depense.financer.id"] = ['$in' => $financersFilters];
        }

        if(!empty($tagsFilters)){
            $query[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
            $queryWithPayement[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        }
        
        $answersAndFinancers = PHDB::aggregate(Form:: ANSWER_COLLECTION, $query);
        $answersAndPayement = PHDB::aggregate(Form:: ANSWER_COLLECTION, $queryWithPayement);

        $results = [
            "colors" => array(),
            "labels" => array(),
            "flows" => array()
        ];

        $results["check data"] = $answersAndFinancers;

        if(!empty($answersAndFinancers["result"])){
            foreach($answersAndFinancers["result"] as $key => $val){
                if(!empty($val["name"]) && $val["proposal"] && $val["amount"]){
                    $val["name"] = ucfirst($val["name"])." (FINANCEUR)";
                    $val["proposal"] = ucfirst($val["proposal"])." (PROPOSITION)";

                    $index = $val["name"]."---".$val["proposal"];
                    if(!isset($results["flows"][$index]["flow"])){
                        $results["flows"][$index] = array( 'from'=> $val["name"], 'to'=> $val["proposal"], 'flow'=> (int)$val["amount"]);
                    }else{
                        $results["flows"][$index]["flow"] = $results["flows"][$index]["flow"] + (int)$val["amount"];
                    }

                    if(!in_array($val["name"],$results["labels"])){
                        $results["labels"][] = $val["name"];
                        $results["colors"][$val["name"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }

                    if(!in_array($val["proposal"],$results["labels"])){
                        $results["labels"][] = $val["proposal"];
                        $results["colors"][$val["proposal"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }

                }
            }
        }

        if(!empty($answersAndPayement["result"])){
            foreach($answersAndPayement["result"] as $key => $val){
                if(!empty($val["name"]) && $val["proposal"] && $val["amount"]){
                    $val["name"] = ucfirst($val["name"])." (EXECUTANT)";
                    $val["proposal"] = ucfirst($val["proposal"])." (PROPOSITION)";

                    $index = $val["proposal"]."---".$val["name"];
                    if(!isset($results["flows"][$index]["flow"])){
                        $results["flows"][$index] = array( 'from'=>$val["proposal"] , 'to'=> $val["name"], 'flow'=> (int)$val["amount"]);
                    }else{
                        $results["flows"][$index]["flow"] = $results["flows"][$index]["flow"] + (int)$val["amount"];
                    }

                    if(!in_array($val["name"],$results["labels"])){
                        $results["labels"][] = $val["name"];
                        $results["colors"][$val["name"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }

                    if(!in_array($val["proposal"],$results["labels"])){
                        $results["labels"][] = $val["proposal"];
                        $results["colors"][$val["proposal"]] = sprintf("#%02X%02X%02X", mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
                    }
                }
            }
        }
        return $results;
    }
    /********************************************* END METRICS ****************************************************/


    /******************************************** MAPS ***********************************************************/
    private function globalautocompletequartier(){
        $params = $_POST;
        $query = array("insee" =>"97411");
        if(!empty($params["name"])){
            $q = array("name" => new MongoRegex("/.*{$params["name"]}.*/i"));
            $query = SearchNew::addQuery( $query ,$q);
        }
        
        $queryAnswer = 
        array(
            array(
                '$match' => array(
                    "form" => $params["filters"]["formId"],
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => ['$exists' => true],
                )
            ),
            array(
                '$unwind' => '$answers.aapStep1.interventionArea'
            ),
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.interventionArea',
                    "count" => ['$sum' => 1]
                ]
            )
        );
        $quaritierInAnswer = PHDB::distinct(Form::ANSWER_COLLECTION,"answers.aapStep1.interventionArea");
        $quaritierInAnswer = array_map(function($v){
            if(Api::isValidMongoId(trim($v)))
                return new MongoId(trim($v));
        },$quaritierInAnswer);
        
        $query  = SearchNew::addQuery( $query ,array("_id"=>['$in'=> $quaritierInAnswer]));

        $quartiers = PHDB::findAndFieldsAndSortAndLimitAndIndex(Zone::COLLECTION,$query,array("name"),array("updated" => -1), @$params["indexStep"], @$params["indexMin"]);
        $quariesCount = PHDB::count(Zone::COLLECTION,$query);

        $proposalsCounter = PHDB::aggregate(Form::ANSWER_COLLECTION,$queryAnswer);
        $proposalsCounter = $proposalsCounter["result"];

        foreach ($proposalsCounter as $key => $value) {
            $idQuartier = trim((string)$value["_id"]);
            if(!empty($quartiers[$idQuartier]))
                $quartiers[$idQuartier]["actions"] = $value["count"];
        }

        return Rest::json(array(
            "results" => $quartiers,
            "count" => [
                "zones" => $quariesCount,
                "dd" => $quaritierInAnswer
            ]
        ));
    }

    private function mapProposalData(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query = [
            array(
                '$match' => [
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => ['$in' => $quartiers],
                    "answers.aapStep1.adress" => ['$exists' => true],
                    "answers.aapStep1.year" => ['$in'=>$session]
                ]
            ),
            array(
                '$project' => [
                    "geo" => '$answers.aapStep1.adress.geo',
                    "name" => array('$concat' => ['<h5 class="text-center">Association: ','$answers.aapStep1.association','</h5>',"Projet: ",'$answers.aapStep1.titre']),
                ]
            )
        ];
        if(empty($quarties))
            unset( $query[0]['$match']["answers.aapStep1.interventionArea"]);
        $proposal = PHDB::aggregate(Form:: ANSWER_COLLECTION,$query);
        return $proposal["result"];
    }
    private function mapQuartierData(){
        $form = $_POST["form"];
        $financersFilters = $_POST["financers"];
        $tagsFilters = $_POST["tags"];
        $quartiers = $_POST["quartiers"];
        $session = $_POST["session"];
        $query = [
            array(
                '$match' => [
                    "form" => $form,
                    "answers.aapStep1.titre" => ['$exists' => true],
                    "answers.aapStep1.interventionArea" => [
                        '$in' => $quartiers,
                    ],
                    "answers.aapStep1.year" => ['$in'=>$session]
                ]
            ),
            array(
                '$unwind' => '$answers.aapStep1.interventionArea'
            ),
            array(
                '$addFields' => [
                    //"interventionArea" => ['$toObjectId' => '$answers.aapStep1.interventionArea'],
                    "interventionArea"=> [
                        '$cond' => [
                            'if'=> [
                            '$regexMatch'=> [
                                'input'=> '$answers.aapStep1.interventionArea',
                                'regex'=> new MongoRegex("/^[0-9a-fA-F]{24}$/"),
                            ]
                            ],
                            'then'=> [ '$toObjectId'=> '$answers.aapStep1.interventionArea' ],
                            'else'=> '$answers.aapStep1.interventionArea'
                        ]
                    ]
                ]
            ),
            array(
                '$lookup' => [
                    "from" => "zones",
                    "localField" => 'interventionArea',
                    "foreignField" => '_id',
                    "as" => "quartier",
                ]
            ),
            array(
                '$addFields' => [
                    "nameQuartier" => ['$arrayElemAt' => array('$quartier.name',0)]
                ]
            ),
            array(
                '$project' => [
                    "_id"=>1,
                    "name" => array('$concat' => [
                        '<h5 class="text-center">Quartier : ','$nameQuartier','</h5>',

                        '<h5 class="text-center">Association: ','$answers.aapStep1.association','</h5>',

                        "Projet: ",'$answers.aapStep1.titre'
                    ]),
                    "geo" => ['$arrayElemAt' => array('$quartier.geo',0)],
                    "idQuartier" => ['$arrayElemAt' => array('$quartier._id',0)],
                    /*"quartier.name" => 1,
                    "quartier.geo" => 1,
                    "quartier.geoPosition" => 1,*/
                    //"quartier.geoShape" => 1*/
                ]
            )
        ];
        if(empty($quarties))
            unset( $query[0]['$match']["answers.aapStep1.interventionArea"]);
        $proposal = PHDB::aggregate(Form:: ANSWER_COLLECTION,$query);
        if(!empty($quartiers))
            $proposal["result"] = array_filter($proposal["result"], function ($res) use ($quartiers) {
                if(!empty($res["idQuartier"]))
                    return in_array((string)$res["idQuartier"],$quartiers);
            });

        $proposal["result"] = array_map(function($v){
            return array(
                "geo" => [
                    "@type" => $v["geo"]["@type"],
                    "latitude" => $v["geo"]["longitude"],
                    "longitude" => $v["geo"]["latitude"],
                ],
                "idQuartier" => $v["idQuartier"],
                "name" => $v["name"],
                "_id" => $v["_id"],
            );
        },$proposal["result"]);

        return $proposal["result"];
    }
    /********************************************END MAPS ***********************************************************/

    /******************************************** TABLEAU ***********************************************************/
    private function fundingByYearAndFinancor(){
        $formId = $_POST["form"];
        $quartiers = $_POST["quartiers"];
        $tagsFilters = $_POST["tags"];
        $key = "year";
        $yearsArray = PHDB::distinct(Form::ANSWER_COLLECTION,"answers.aapStep1.year",['form'=> $formId]);
        $commonMatch = array(
            'form'=> $formId,
            'answers.aapStep1.titre'=> array('$exists'=>true),
            'answers.aapStep1.depense.financer'=> array('$exists'=>true),
            "answers.aapStep1.interventionArea" => ['$exists' => true],
            "answers.aapStep1.year" => ['$exists' => true],
        );
        
        $amountFinancedQuery = [
            [
                '$match'=> $commonMatch
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense.financer'
            ],
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.year',
                    "financed" => ['$sum' => '$answers.aapStep1.depense.financer.amount'],
                    "financers" => [
                        '$push' => array(
                            "name" => '$answers.aapStep1.depense.financer.name',
                            "value" => '$answers.aapStep1.depense.financer.amount'
                        )
                    ]
                ]
            ),
        ];

        $amountRequestedQuery = [
            [
                '$match'=> $commonMatch
            ],
            [
                '$unwind'=> '$answers.aapStep1.depense'
            ],
            [
                '$group'=> [
                    '_id'=> '$answers.aapStep1.year',
                    'requested'=> [ '$sum'=> '$answers.aapStep1.depense.price']
                ]
            ]
        ];

        
        if(!empty($quartiers)){
            $amountFinancedQuery[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
            $amountRequestedQuery[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        }

        if(!empty($tagsFilters))
        {
            $amountFinancedQuery[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
            $amountRequestedQuery[0]['$match']["answers.aapStep1.tags"] = ['$in' => $tagsFilters];
        }
            

        $amountFinanced = PHDB::aggregate(Form:: ANSWER_COLLECTION,$amountFinancedQuery);
        $amountRequested = PHDB::aggregate(Form:: ANSWER_COLLECTION,$amountRequestedQuery);

        $query_actions = [
            [
                '$match'=> array(
                    'form'=> $formId,
                    'answers.aapStep1.titre'=> array('$exists'=>true),
                    'answers.aapStep1.interventionArea' => ['$exists' => true],
                )
            ],
            array(
                '$group'=> [
                    "_id" => '$answers.aapStep1.year',
                    "actions" => ['$sum' => 1],
                ]
            ),
        ];

        if(!empty($quartiers)){
            $query_actions[0]['$match']["answers.aapStep1.interventionArea"] = ['$in' => $quartiers];
        }
        $actions = PHDB::aggregate(Form:: ANSWER_COLLECTION,$query_actions);

        foreach ($actions["result"] as $ka => $va) {
            foreach ($amountFinanced["result"] as $kf => $vf) {
                if($va["_id"] === $vf["_id"])
                    $amountFinanced["result"][$kf]["actions"] = $va["actions"];
            }
        }

        $results = $amountFinanced["result"];
        $amountRequested = $amountRequested["result"];
        //$amountSpent = $amountSpent["result"];
        //return Rest::json($amountSpent);exit;
        foreach ($results as $key => $value) {
            if(!isset($results[$key]["funders"]))
                $results[$key]["funders"] = [];
            foreach($value["financers"] as $keyFinancor => $valueFinancor){
                if(!empty($valueFinancor["name"])){
                    $name = $valueFinancor["name"];
                    $val = (float)$valueFinancor["value"];
                    if(isset($results[$key]["funders"][$name])){
                        $results[$key]["funders"][$name] = (float)$results[$key]["funders"][$name] + $val;
                    }else{
                        $results[$key]["funders"][$name] = $val;
                    }
                }
            }

            foreach ($amountRequested as $kReq => $vRep) {
                if($vRep['_id'] == $value['_id'])
                    $results[$key]["requested"] = $vRep['requested'];
            }

            /*foreach ($amountSpent as $kSpent => $vSpent) {
                if($vSpent['_id'] == $value['_id'])
                    $results[$key]["spent"] = $vSpent['spent'];
            }*/
            $results[$key]["rest"] = $results[$key]["requested"] - $results[$key]["financed"];
            unset($results[$key]["financers"]);
            unset($amountFinanced["result"][$key]["financers"]);

        }

        usort($results, function($a, $b) {
            return (int)$a['_id'] - (int)$b['_id'];
        });

        return $results;
    }

    private function combineTable(){
        return array(
            "fundingByYearAndFinancor" => $this->fundingByYearAndFinancor()
        );
    }
    /******************************************** END TABLEAU ***********************************************************/

    /******************************************** UTILS ***********************************************************/
    private function getProjectByFormId($formId, $moreQuery = []){
        $projectId = [];
        $query = array("form" => $formId,"answers.aapStep1.titre"=>['$exists'=>true],"project.id"=>['$exists'=>true]);
        // if(!empty($moreQuery)) { 
        //     foreach ($moreQuery as $key => $value) {
        //        $query[$key] = $value;
        //     }
        // }
        $answers = PHDB::find(Form::ANSWER_COLLECTION, $query,array("project"));
        foreach ($answers as $kans => $vans) {
            $projectId[] = $vans["project"]['id'];
        }
        return $projectId;
    }

    private function getProjectByContextId($contextIds){
        $projectIds = [];
        $or = array();
        foreach ($contextIds as $key => $value) {
            $or[] = array("parent.".$value => ['$exists'=>true]);
        }
        $projects = PHDB::find(Project::COLLECTION,array('$or' => $or),array("_id"));
        if(!empty($projects)) $projectIds = array_keys($projects);
        return $projectIds;
    }

    private function getMembersContextIds($contextIds){
        $members = [];
        $contextIds =  array_map(function($v){
            return new MongoId($v);
        },$contextIds);

        $elementOrg = PHDB::find(Organization::COLLECTION,array("_id"=>['$in'=> $contextIds ]),array("links"));
        $elementProject = PHDB::find(Project::COLLECTION,array("_id"=>['$in'=> $contextIds ]),array("links"));
        $elements = array_merge($elementOrg,$elementProject);
        foreach ($elements as $key => $value) {
            if(!empty($value["links"]["members"])){
                foreach ($value["links"]["members"] as $kl => $vl) {
                    if(!isset($vl["toBeValidated"])){
                        $members[] = new MongoId($kl);
                    }
                }
            }
            if(!empty($value["links"]["contributors"])){
                foreach ($value["links"]["contributors"] as $kl => $vl) {
                    if(!isset($vl["toBeValidated"])){
                        $members[] = new MongoId($kl);
                    }
                }
            }
        }

        $members = PHDB::findAndSort(Citoyen::COLLECTION,array("_id"=> ['$in'=>$members]),array("name"=>1),null,array("name","slug"));
        return $members;
    }
    private function compareDates($a, $b) {
        $dateA = DateTime::createFromFormat('d/m/Y', $a);
        $dateB = DateTime::createFromFormat('d/m/Y', $b);

        if ($dateA === false || $dateB === false) {
            throw new Exception("Invalid date format");
        }

        return $dateA <=> $dateB;
    }
    private function timestampOfEachDayOnCurrentWeek($tmsp){
        $today = date('Y-m-d',$tmsp); // Get the current date
        $dayOfWeek = date('N', strtotime($today)); // Get the numeric representation of the day of the week (1 = Monday, 7 = Sunday)

        $timestamps = [];

        for ($i = 1; $i <= 7; $i++) {
            $beginTimestamp = strtotime("$today -" . ($dayOfWeek - $i) . " days 00:00:00");
            $endTimestamp = strtotime("$today -" . ($dayOfWeek - $i) . " days 23:59:59");

            $timestamps[date('l', $beginTimestamp)] = [
                'begin' => $beginTimestamp,
                'end' => $endTimestamp,
            ];
        }
        return $timestamps;
    }

    private function isBetween($number, $min, $max) {
        return ((int)$number >= (int)$min && (int)$number <= (int)$max);
    }
    /* Y/m/d H:i */
    private function timestampOfDay($dateStr=null){
        $timezone = new DateTimeZone($this->getTimezone());
        if(empty($dateStr)){
            $now = new DateTime('now', $timezone);
            $dateStr = $now->format('Y/m/d H:i');
        }

        $currentDate = new DateTime($dateStr, $timezone);
        $currentDateTime = $currentDate;
        $currentDate->setTime(0, 0, 0);

        $endDate = clone $currentDate;
        $endDate->setTime(23, 59, 59);

        $startTimestamp = $currentDate->getTimestamp();
        $endTimestamp = $endDate->getTimestamp();
        $currentDateTime = $currentDateTime->getTimestamp();
        return array(
            "startTimestamp" => $startTimestamp,
            "endTimestamp" => $endTimestamp,
            "currentTimestamp" => $currentDateTime
        );
    }

    function getDayStartAndEndTimestamps($date) {
        $dateTime = new DateTime($date);
        $dateTime->setTime(0, 0, 0);
        $startTimestamp = $dateTime->getTimestamp();
        $dateTime->setTime(23, 59, 59);
        $endTimestamp = $dateTime->getTimestamp();
        return [
            'start' => $startTimestamp,
            'end' => $endTimestamp
        ];
    }

    private function getDataInsee()
    {
        $quartiers = $_POST["quartiers"];
        $array_quartiers = array_map(function($val){
            return new MongoId($val);
        },$quartiers);

        $years = $_POST['years'];
        $field = $this->setListField($years[0]);
        $les_champs =  $this->renameField($field, $_POST['inqpv'], $years); //liste des champs QPV
        $outqpv = $_POST['outqpv']; // list des champs hors codeQPV
        $les_champs =  array_merge( $les_champs , $this->setFieldToArray($outqpv));
        
        $query = [
            [
                '$match'=> array("_id" => ['$in'=> $array_quartiers], "codeQPV" => ['$exists' => true])
            ],
            [
                '$project'=> $les_champs
            ]
        ];
        

       $result =  PHDB::aggregate(Zone::COLLECTION, $query)['result'];
       
       $result =  $this->prettyOutput($outqpv, $result, $years);

       

        if(count($years) < 2)
        {   
            
            $data_int = array();
            $year = "_".$years[0];
            $list_column = array_keys($result[0][$year]);
            foreach($list_column as $key)
            {

                $data_int = array_merge($data_int , array($key => 0));
            }
            $data_total = array("name" =>"TOTAL", $year => $data_int);
            foreach ($result as $cle ) {
                foreach($list_column as $key)
                {
                    if(isset($cle[$year][$key]) && is_numeric($cle[$year][$key]))
                        $data_total[$year][$key] += $cle[$year][$key];
                }
            }
            return array_merge($result, array($data_total));
        }

        return $result;
        

    }

    private function prettyOutput($champ1, $result, $years)
    {
        foreach ($result  as $document) {
             $newDocument = array(
                 
             );
            
             foreach($champ1 as $key)
             {
                 if($key !== "_id")
                     $newDocument = array_merge($newDocument, array( $key => $document[$key]));
             }
             foreach($years as $key)
             {  
                $arr = array();
                for($i = 0; $i < count(array_keys($document['_'+$key])); $i++)
                {
                    $arr = array_merge($arr, array_values($document[$key])[$i]);
                }
                $newDocument = array_merge($newDocument, array( '_'.$key =>  $arr));
             }
         
             // Merge emploi and revn subfields into the newDocument
             $outputData[] = $newDocument;
         }
        return $outputData;
    }

    private function setListField($year)
    {
        $field["revn"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".revn", array("insee" => "97411"))[0]);
        $field["insertionPro"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".insertionPro", array("insee" => "97411"))[0]);
        $field["logement"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".logement", array("insee" => "97411"))[0]);
        $field["educ"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".educ", array("insee" => "97411"))[0]);
        $field["tissuEco"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".tissuEco", array("insee" => "97411"))[0]);
        $field["demo"] =  array_keys(PHDB::distinct(Zone::COLLECTION, $year.".demo", array("insee" => "97411"))[0]);

        return $field;
    }

    private function getListField($field, $le_champs, $year)
    {
        
        foreach (array_keys($field) as $key ) {
            foreach($field[$key] as $champ)
            { 
              if($champ == $le_champs)
              {
                return  $year.".".$key.".".$champ;
              }
            }
        }
        return "demo.EPCI_1";
    }

    private function setFieldToArray($fields)
    {
        $new_array_string = array();
        foreach ($fields as $key) {
            $new_array_string = array_merge($new_array_string, array($key => 1));
        }

        return $new_array_string;
    }

    private function renameField($field, $fields, $years)
    {

        $new_array_string = array();
        foreach ($fields as $key) {
            foreach($years as $year){
                $listField = $this->getListField($field, $key, $year);
                $new_array_string = array_merge($new_array_string, array($listField => 1));
            }
        }

        return $new_array_string;
    }

    private function getCreatedYears($form){
        $year = PHDB::distinct(Form::ANSWER_COLLECTION,"answers.aapStep1.year", ["form" => $form]);
        //$minimumYear = array_values($minimumYear)[0]["created"];
        //$minimumYear = (int)date('Y', $minimumYear);
        //$maximumYear =  (int)date('Y');
        $years = [];
        foreach ($year as $y){
            $years[$y] = 0;
        }
        return $years;
    }
}
