<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom;

use CAction, PHDB, Form, Yii;
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
class TerritoryAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($answerId=null)
    {
    	$controller = $this->getController();    	

    	$answers = null;
    	$tpl = "costum.views.custom.sommom.territory";    	
    	if ($answerId != null) {
    		$answers = PHDB::findByIds( Form::ANSWER_COLLECTION, array($answerId));
    	}

		$params = [
    		"answers" => $answers
    	];	
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    	
    }
}
