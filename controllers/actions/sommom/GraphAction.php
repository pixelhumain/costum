<?php
//http://127.0.0.1/ph/costum/co/dashboard/sk/laCompagnieDesTierslieux
namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\sommom;
use CAction;
use Ctenat;
use Form;
use PHDB;
use Yii;
use function ctype_digit;

class GraphAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($tpl="costum.views.custom.sommom.graph", $aj="false", $territoires="[0]", $firstkey = "", $ter="")
    {
    	if(Yii::app()->request->isAjaxRequest){
    		if(null !== (Yii::app()->request->getPost("territoires"))){
    			$territoires = Yii::app()->request->getPost("territoires");
    		}
    	}

    	$controller = $this->getController();  	
    	$formId =PHDB::find( Form::COLLECTION,  array( "parent.".$controller->costum["contextId"] => array('$exists'=>1) ) );

    	reset($formId);
		

		//if($aj == "false"){
			$firstkey = key($formId);
		//}

    	$answers = PHDB::find( Form::ANSWER_COLLECTION, [ "form" => $firstkey ]);
    	
    	$blocks = [];
    	$title = "Observatoire des cétacés";

    	$territoires1 = [];
    	$havevalue = false;
    	$to = [];
    	$too = [];

    	$oooa = explode('"', $territoires);
    	foreach ($oooa as $key => $value) {
    		if (ctype_digit($value)) {
    			array_push($territoires1, $value);
    		}
    	}

    	if(count($territoires1) != 0){
    		$havevalue = true;
    		$territoires = $territoires1;
    	} else {
    		foreach($answers as $key => $value){
				if(isset($value["answers"])){
						if(isset($value["answers"]["sommomForm1"]["sommomForm120"])){
							array_push($to, $value["answers"]["sommomForm1"]["sommomForm120"]);
						}else {
							array_push($to,"non défini");
						}
				}
			}

			for ($i=0; $i < count($to); $i++) { 
				array_push($too, $i);
			}

			$territoires = $too;

    	}

    	
    	

			$lists = [];
			// *********** List ************
			$formName = [];	
			$totalChamp = 0;
			$totalValide = 0;
			$totalRep = 0;

			

			for($i = 1; $i < 6; $i++){
				${"sommomForm" . $i ."Label"} = [];
				${"sommomForm" . $i ."Count"} = [];
				${"sommomForm" . $i ."Variable"} = [];
				${"sommomForm" . $i ."Data"} = [];
				${"form". $i} = PHDB::findOne(Form::COLLECTION, array('id' => 'sommomForm'.$i));
			}
		
			for($i = 1; $i < 6; $i++){
				if(!is_null(${"form". $i})){
					foreach(${"form". $i}["inputs"] as $key => $value){
						array_push(${"sommomForm" . $i ."Label"}, $value["label"]);
						array_push(${"sommomForm" . $i ."Variable"}, $key);
						$totalChamp++;
					}
					array_push($formName, ${"form". $i}["name"]);
				}
			}

			for($i = 1; $i < 6; $i++){
				for($t = 0; $t < count(${"sommomForm" . $i ."Label"}); $t++){
					${"sommomForm" . $i ."Count"}[$t] = 0;
					${"sommomForm" . $i ."Data"}[$t] = [];
				}
			}

			foreach($answers as $key => $value){
				if(isset($value["answers"])){
	                foreach ($value["answers"] as $kn => $vn) {
						foreach ($vn as $k => $v) {
							for($i = 1; $i < 6; $i++){
								for($t = 0; $t < count(${"sommomForm" . $i ."Variable"}); $t++){
									if(strpos($k, ${"sommomForm" . $i ."Variable"}[$t]) !== false){
										$ck = count($v);
										${"sommomForm" . $i ."Count"}[$t] = ${"sommomForm" . $i ."Count"}[$t] + $ck;
										$totalValide++;
										$totalRep = $totalRep + $ck;

										//ajoute les data
										$j = 0;
										for($j = 0; $j < count($v); $j++){
											if(isset($v[$j])){
												array_push(${"sommomForm" . $i ."Data"}[$t], $v[$j]);
											}
										}
									}
								}
							}
						}
	                }
	            }
			}

			$acteur = [];
			$zoneobservation = [];
			$planconservation = [];
			$reglementation = [];
			$label = [];
			$espece = [];
			$opmaritime = [];
			$opterrestre = [];
			$opaerien = [];
			$zoneobs = [];
			$espobs = [];

			$users = [];
			$ids = [];
			$links = [];

			$counter = 0;
			$territoire = [];

			$acteurtr = [];

			$accord = [];
			$eq = [];
			$cod = [];

			foreach($answers as $key => $value){
				if(isset($value["answers"])){
					array_push($users, $value["user"]);
					array_push($ids, $key);
						if(isset($value["links"]["answered"])){
							array_push($links, $value["links"]["answered"]);
						}else {
							array_push($links,[]);
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm120"])){
							array_push($territoire, $value["answers"]["sommomForm1"]["sommomForm120"]);
						}else {
							array_push($territoire,"non défini");
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm122"])){
							array_push($acteur, $value["answers"]["sommomForm1"]["sommomForm122"]);
							
						}else {
							array_push($acteur,[]);
						}

						if(isset($value["answers"]["sommomForm2"]["sommomForm21"])){
							array_push($acteurtr, $value["answers"]["sommomForm2"]["sommomForm21"]);
							
						}else {
							array_push($acteurtr,[]);
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm12"])){
							array_push($zoneobservation, $value["answers"]["sommomForm1"]["sommomForm12"]);
						}else {
							array_push($zoneobservation,[]);
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm12"])){
							array_push($planconservation, $value["answers"]["sommomForm1"]["sommomForm12"]);
						}else {
							array_push($planconservation,[]);
						}


						if(isset($value["answers"]["sommomForm2"]["sommomForm22"])){
							array_push($reglementation, $value["answers"]["sommomForm2"]["sommomForm22"]);
						}else {
							array_push($reglementation, []);
						}

						if(isset($value["answers"]["sommomForm2"]["sommomForm25"])){
							array_push($label, $value["answers"]["sommomForm2"]["sommomForm25"]);
						}else {
							array_push($label,[]);
						}

						if(isset($value["answers"]["sommomForm1"]["sommomForm13"])){
							array_push($espece, $value["answers"]["sommomForm1"]["sommomForm13"]);
						}else {
							array_push($espece,[]);
						}

						if(isset($value["answers"]["sommomForm3"]["sommomForm31"])){
							array_push($opmaritime, $value["answers"]["sommomForm3"]["sommomForm31"]);
						}else {
							array_push($opmaritime,[]);
						}

						if(isset($value["answers"]["sommomForm3"]["sommomForm32"])){
							array_push($opterrestre, $value["answers"]["sommomForm3"]["sommomForm32"]);
						}else {
							array_push($opterrestre,[]);
						}

						if(isset($value["answers"]["sommomForm3"]["sommomForm33"])){
							array_push($opaerien, $value["answers"]["sommomForm3"]["sommomForm33"]);
						}else {
							array_push($opaerien,[]);
						}
						// if(isset($value["answers"]["sommomForm1"]["sommomForm128"][0]["pointGPS"])){
						// 	array_push($gpsterritoire, $value["answers"]["sommomForm1"]["sommomForm128"][0]["pointGPS"]);
						// }else {
						// 	array_push($gpsterritoire, []);
						// }
						if(isset($value["answers"]["sommomForm1"]["sommomForm12"])){
							array_push($zoneobs, $value["answers"]["sommomForm1"]["sommomForm12"]);
						}else {
							array_push($zoneobs, []);
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm13"])){
							array_push($espobs, $value["answers"]["sommomForm1"]["sommomForm13"]);
						}else {
							array_push($espobs, []);
						}
						if(isset($value["answers"]["sommomForm1"]["sommomForm15"])){
							array_push($accord, $value["answers"]["sommomForm1"]["sommomForm15"]);
						}else {
							array_push($accord, []);
						}
						if(isset($value["answers"]["sommomForm5"]["sommomForm55"])){
							array_push($eq, $value["answers"]["sommomForm5"]["sommomForm55"]);
						}else {
							array_push($eq, []);
						}
						if(isset($value["answers"]["sommomForm2"]["sommomForm24"])){
							array_push($cod, $value["answers"]["sommomForm2"]["sommomForm24"]);
						}else {
							array_push($cod, []);
						}

						for($i = 1; $i < 6; $i++){
							${"sommomForm" . $i ."Data".$counter} = [];
							${"sommomForm" . $i ."Count".$counter} = [];
						}

						for($i = 1; $i < 6; $i++){
							for($t = 0; $t < count(${"sommomForm" . $i ."Label"}); $t++){
								${"sommomForm" . $i ."Count".$counter}[$t] = 0;
								${"sommomForm" . $i ."Data".$counter}[$t] = [];
							}
						}

						foreach ($value["answers"] as $kn => $vn) {
						foreach ($vn as $k => $v) {
							for($i = 1; $i < 6; $i++){
								for($t = 0; $t < count(${"sommomForm" . $i ."Variable"}); $t++){
									if(strpos($k, ${"sommomForm" . $i ."Variable"}[$t]) !== false){
										$ck = count($v);
										${"sommomForm" . $i ."Count".$counter}[$t] = ${"sommomForm" . $i ."Count".$counter}[$t] + $ck;
										$totalValide++;
										$totalRep = $totalRep + $ck;

										//ajoute les data
										$j = 0;
										for($j = 0; $j < count($v); $j++){
											if(isset($v[$j])){
												array_push(${"sommomForm" . $i ."Data".$counter}[$t], $v[$j]);
											}
										}
									}
								}
							}
						}
	                }

	                $counter++;
				}

			}
			$acteur1 = [];
			$zoneobservation1 = [];
			$planconservation1 = [];
			$reglementation1 = [];
			$label1 = [];
			$espece1 = [];
			$opmaritime1 = [];
			$opterrestre1 = [];
			$opaerien1 = [];
			$zoneobs1 = [];
			$espobs1 = [];

			$acteurtr1 = [];

			$accord1 = [];
			$eq1 = [];
			$cod1 = [];

		foreach ($territoires as $key => $value) {
			$acteur1 = array_merge($acteur1 , $acteur[$value]);
			$zoneobservation1 = array_merge($zoneobservation1 , $zoneobservation[$value]);
			$planconservation1 = array_merge($planconservation1 , $planconservation[$value]);
			$reglementation1 = array_merge($reglementation1 , $reglementation[$value]);
			$label1 = array_merge($label1 , $label[$value]);
			$espece1 = array_merge($espece1 , $espece[$value]);
			$opmaritime1 = array_merge($opmaritime1 , $opmaritime[$value]);
			$opterrestre1 = array_merge($opterrestre1 , $opterrestre[$value]);
			$opaerien1 = array_merge($opaerien1 , $opaerien[$value]);
			$zoneobs1 = array_merge($zoneobs1 , $zoneobs[$value]);
			$espobs1 = array_merge($espobs1 , $espobs[$value]);

			$acteurtr1 = array_merge($acteurtr1 , $acteurtr[$value]);

			$accord1 = array_merge($accord1 , $accord[$value]);
			$eq1 = array_merge($eq1 , $eq[$value]);
			$cod1 = array_merge($cod1 , $eq[$value]);
		}




       $gr = [$users ,$ids ,$links ,$acteur1 ,$zoneobservation1 ,$planconservation1 ,$reglementation1 ,$label1 ,$espece1 ,$opmaritime1 ,$opterrestre1 ,$opaerien1 ,$zoneobs1 ,$espobs1, $acteurtr1, $territoire, $accord1, $eq1, $cod1 ];

		


			$hgraph = [];

			for($k = 0; $k < count($users); $k++){
				${"list".$k} = [];
			}

			$iconList = ["fa-map-o","fa-cogs", "fa-flag-o", "fa-flask", "fa-graduation-cap"];

			for($k = 0; $k < count($users); $k++){
			${"list".$k} += array(
				"chiffresDoss" =>[
					"title"=>"<i class='fa fa-2x fa-folder-open-o'></i><br/>ANALYSE DES REPONSES SUR L'OBSERVATOIRE DES CETACES ",
					"blocksize"=>"4 col-xs-6", 
					"bgColor" => Ctenat::$COLORS[0],
					"color" => "#fff",
					"data" => [
						["data"=>count($answers),"name"=>"Nombre d'acteur qui ont répondu","icon"=>"folder-open","type"=>"success"],
						["data"=>$totalRep,"name"=>"Nombre de réponse valide","icon"=>"folder-open","type"=>"default"],	
						["data"=>$totalValide ,"name"=>"Nombre de champ renseigné","icon"=>"folder-open","type"=>"default"],			
						["data"=> ($totalChamp * count($answers)) - $totalValide ,"name"=>"Nombre de champ non renseigné","icon"=>"folder-open","type"=>"default"]			
					],
					"tpl"  => "costum.views.tpls.list"
				]
				);
			}

			// for($k = 0; $k < count($users); $k++){
			// for($i = 1; $i < 6; $i++){
			// 	$preData = [];
			// 	for($t = 0; $t < count(${"sommomForm" . $i ."Label"}); $t++){
			// 		array_push($preData, ["data" => ${"sommomForm" . $i ."Count".$k}[$t], "name" => ${"sommomForm" . $i ."Label"}[$t], "icon" => "success", "type" => "danger"]);
			// 	}
			// 	${"list".$k} += array( "sommomForm".$i.$k  => [
			// 		"title" => "<i class='fa fa-2x ".$iconList[$i -1]."'></i><br/>".$formName[$i - 1]." ".$territoire[$k], 
			// 		"blocksize"=>"4 col-xs-6", 
			// 		"bgColor" => Ctenat::$COLORS[0],
			// 		"color" => "#fff",
			// 		"data" => $preData,
			// 		"tpl"  => "costum.views.tpls.list",
			// 		"id" => $i
			// 	]);
			// 	// };
			// }
			// }

			// *********** Graph ************

			//frequence
			$monthLabel = ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"];

			$monthtrueLabel = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];


			for($i = 0; $i < 12; $i++){
				${$monthLabel[$i]} = 0;
					//${$monthLabel[$i]."Obs".$k} = 0;
			}

			
			foreach ($zoneobservation1 as $ke => $va) {
				$debutp = "";
				$finp = "";

				if(isset($va['startDate'])){
					$debutp = $va['startDate'];
				} 

				if(isset($va['endDate'])){
					$finp = $va['endDate'];
				} 

				if($debutp != "" and $finp != ""){
					for($m = 0; $m < count($monthLabel); $m++){
						if($debutp == $monthtrueLabel[$m]){
							for($mm = $m; $mm < count($monthLabel); $mm++){
								${$monthLabel[$mm]}++;
								if($monthtrueLabel[$mm] == $finp){
									break;
								}
							}
						}
					}
				}


			}

			$periodeacti = [];
			
			for($i = 0; $i < 12; $i++){
				$periodeacti[$i] = ${$monthLabel[$i]};
			}


			$graphi = [];

			$graphi += ["periodeactivite" => [
				"title"=>"PÉRIODE D’ACTIVITÉ PAR ZONE D’OBSERVATION",
				"data" => $periodeacti,
				"lbls" => $monthtrueLabel,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.barMany",
				"yAxesLabel" => "Nombre de ports de départ"
			]];

			
			$statut =  ["Espèce disparue (EX)", "Espèce disparue, survivant uniquement en élevage (EW)","Espèce en danger critique d'extinction (CR)","Espèce en danger (EN)","Espèce vulnérable (VU)","Espèce quasi menacée (NT)","Préoccupation mineure (LC)","Données insuffisantes (DD)","Non évalué (NE)"];

			$zonelabel = [];
			$zonedata = [];

			for($i = 0; $i < count($statut); $i++){
				${"zsp".$i} = 0;
			}

			
			foreach ($espece1 as $ke => $va) {
						
					for($m = 0; $m < count($statut); $m++){
						if($va['statut'] == $statut[$m]){
							${"zsp".$m}++;
						}
					}

			}

			$especeobserve = [];
			foreach ($statut as $key => $value) {
				$especeobserve[$key] = ${"zsp".$key};
			}


			$graphi += ["especeobserve" => [
				"title"=>"Espèces ciblées",
				"data" => $especeobserve,
				"lbls" => $statut,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.pieMany"
			]];


			$roleact = [
				  "Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation",
				  "Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien",
				  "Organisme impliqué dans la science et la recherche sur l’activité d’observation",
				  "Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation"
			];

			$actelabel = [];
			$actedata = [];

			for($i = 0; $i < count($roleact); $i++){
				${"or".$i} = 0;
			}

			
			foreach ($acteur1 as $ke => $va) {
						
					// for($m = 0; $m < count($roleact); $m++){
					// 	foreach ($va['role'] as $keyy => $valu) {
					// 		if($valu == $statut[$m]){
					// 			${"or".$m}++;
					// 		}
					// 	}
					// }
				foreach ($va['role'] as $keyy => $valu) {
					for($m = 0; $m < count($roleact); $m++){
						if($valu == $roleact[$m]){
					 			${"or".$m}++;
					 	}
					}
				}

			}

			$acteurimpl = [];
			foreach ($roleact as $key => $value) {
				$acteurimpl[$key] = ${"or".$key};
			}


			$graphi += ["acteurimpl" => [
				"title"=>"Acteurs impliqués dans l'activité",
				"data" => $acteurimpl,
				"lbls" => $roleact,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.pieMany"
			]];

			$opm = [];
			for ($i=0; $i < count($territoire); $i++) { 
				$opm[$i] = count($opmaritime[$i]);
			}


			$nomterritoire = [];

			foreach ($territoire as $key => $value) {
				$nomterritoire[$key] = $value;
			}

			$graphi += ["opm" => [
				"title"=>"Répartition des opérateurs maritimes selon les territoires",
				"data" => $opm,
				"lbls" => $nomterritoire,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.barMany"
			]];

			for($i = 0; $i < count($territoire); $i++){
				${"dstatopm".$i} = 0;
				${"dstatopmnage".$i} = 0;
			}

			$datastatopm = [];
			$datastatopmnage = [];

			// var_dump($opmaritime);
			for ($i=0; $i < count($territoire); $i++) {
				foreach ($opmaritime[$i] as $key => $value) {
					if(isset($value["pratique"])){
					if($value["pratique"] == "Embarquée" or $value["pratique"] == "deux"){
						${"dstatopm".$i}++;
					} 

					if($value["pratique"] == "Nage avec" or $value["pratique"] == "deux"){
						${"dstatopmnage".$i}++;
					} 
				}
				}
			}

			foreach ($territoire as $key => $value) {
				$datastatopm[$key] = ${"dstatopm".$key};
				$datastatopmnage[$key] = ${"dstatopmnage".$key};
			}


			$graphi += ["embarqueopm" => [
				"title"=>"Observation embarquée",
				"data" => $datastatopm,
				"lbls" => $nomterritoire,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.barMany"
			]];

			$graphi += ["nageavecopm" => [
				"title"=>"Observation en nage avec les cétacés",
				"data" => $datastatopmnage,
				"lbls" => $nomterritoire,
				"url"  => "/graph/co/dash/g/graph.views.co.sommom.barMany"
			]];





		foreach ($graphi as $ki => $list) 
			{
				$kiCount = 0;
				foreach ($list["data"] as $ix => $v) {
					if(is_numeric($v))
						$kiCount += $v;
					else 
						$kiCount ++;
				}
				$blocks[$ki] = [
					"title"   => $list["title"],
					"counter" => $kiCount,
				];
				if(isset($list["tpl"])){
					$blocks[$ki] = $list;
				}
				else 
					$blocks[$ki]["graph"] = [
						"url"=>$list["url"],
						"key"=>"pieMany".$ki,
						"data"=> [
							"datasets"=> [
								[
									"data"=> $list["data"],
									"backgroundColor"=> Ctenat::$COLORS
								]
							],
							"labels"=> $list["lbls"]
						]
					];

					if (isset($list["yAxesLabel"])) {
						$blocks[$ki]["graph"]["data"]["yAxesLabel"] = $list["yAxesLabel"];
					}

		}

	// var_dump($blocks);

		$params = [
			"title" => $title,
    		"blocks" => $blocks,
    		"gr" => $gr,
    		"aj" => $aj, 
    		"nbgraph" => count($territoires),
    		"firstkey" => $firstkey,
    		"ter" => $ter

    	];	
    	if(Yii::app()->request->isAjaxRequest){
    		if(null !== (Yii::app()->request->getPost("aj"))){
    			$params["aj"] = Yii::app()->request->getPost("aj");
    			$params["ter"] = Yii::app()->request->getPost("territoires");
    		}
            return $controller->renderPartial($tpl,$params,true);
    	}else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    	
    }
}
