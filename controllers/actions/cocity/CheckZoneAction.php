<?php

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

    use MongoId;
    use PHDB;
    use Rest;
    use Zone;

    class CheckZoneAction extends \PixelHumain\PixelHumain\components\Action{
        public function run(){
            $zone = PHDB::findOne(Zone::COLLECTION, array(
                "_id" => new MongoId($_POST["idZone"])
            ));

            return Rest::json($zone);
        }
    }