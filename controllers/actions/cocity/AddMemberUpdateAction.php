<?php 
    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

    use MongoId;
    use Organization;
    use PHDB;
    use Project;
    use Rest;

    class AddMemberUpdateAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $_id = $_POST["id"];
            $_value = $_POST["value"];
            $_collection = $_POST["collection"];
            $element = PHDB::findOne($_collection::COLLECTION,array("_id" => new MongoId($_id)));
            $sourceKeys = isset($element['source']['keys']) ? $element['source']['keys'] : [];

            if (!in_array($_value, $sourceKeys)) {
                array_push($sourceKeys, $_value);
            }

            $params = array(
                "source.keys" => $sourceKeys
            );

            $result["result"] = PHDB::update($_collection::COLLECTION, array("_id" => new MongoId($_id)), array('$set' => $params));
            $result["message"] = "Element Updated";
            $result["data"] = $_POST["value"];   
            return Rest::json($result);
        }
    }

?>