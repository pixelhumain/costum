<?php

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use Organization;
use PHDB;
use Rest;
use Zone;

    class GetDataOddZonesAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $results = array();
            $addressParams = [];
            $address = [];
            if(isset($_POST['address']))
                $address = $_POST['address'];
            
            $idAddress = [];
        
            if(isset($_POST['typezone'])) {
                switch ($_POST['typezone']) {
                    case "region":
                        if (isset($address["level3"])) {
                            $addressParams[] = ['address.level3' => $address["level3"]];
                        }
                        break;
                    case "ville":
                        if (isset($address["localityId"])) {
                            $addressParams[] = ['address.localityId' => $address["localityId"]];
                        }
                        break;
                    case "departement":
                    case "district":
                        if (isset($address["level4"])) {
                            $addressParams[] = ['address.level4' => $address["level4"]];
                        }
                        break;
                    case "epci":
                        $_zones = PHDB::findOne(
                            Zone::COLLECTION,
                            ["name" => $address["level5Name"]],
                            ['cities']
                        );
                        $_cities = $_zones['cities'];
                        if ($_cities && count($_cities) > 0) {
                            $addressParams[] = ['address.localityId' => array('$in' => $_cities)];
                        }
                        break;
                    default:
                        $idAddress = null;
                }
            }
        
            $orConditions = [
                array("source.keys" => $_POST['slug']),
                array("links.memberOf.".$_POST['_id'].".type" => Organization::COLLECTION)
            ];
        
            if (!empty($addressParams)) {
                $orConditions = array_merge($orConditions, $addressParams);
            }
        
            $initialQuery = array(
                '$and' => array(
                    array(
                        '$or' => $orConditions
                    )
                )
            );
        
            if (isset($_POST["odd"])) {
                foreach ($_POST['odd'] as $key => $value) {
                    $count = 0;
        
                    $query = $initialQuery;
        
                    $query['$and'][] = array(
                        'preferences.toBeValidated.'.$_POST['slug'] => array('$exists' => false)
                    );
                    $query['$and'][] = array(
                        '$or' => array(
                            array('objectiveOdd' => $value),
                            array("tags" => $value)
                        )
                    );

                    if(isset($_POST["elements"])) {
                        foreach ($_POST["elements"] as $value) {
                            $count += PHDB::count($value, $query);
                        }
                    } else {
                        $count = PHDB::count(Organization::COLLECTION, $query);
                    }

                    array_push($results, $count);
                }
            }
        
            return Rest::json($results);
        }
    }
?>