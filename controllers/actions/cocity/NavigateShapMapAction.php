<?php

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

    use City;
    use MongoId;
    use MongoRegex;
    use Organization;
    use PHDB;
    use Rest;
    use Zone;

    class NavigateShapMapAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $results = [];
            $zones = [];
            $fields = ["geo", "geoShape", "name", "level1", "level1Name", "level3", "level3Name", "level4Name", "level4", "cocityId", "country", "countryCode", "level", "cities"];
            $where = [
                "costum.cocity" => ['$exists' => true],
            ];
            if(isset($_POST['name'])){
                if($_POST['collection'] == 'zones') {
                    $_where = [
                        "level" => array($_POST['level']),
                        "name" => new MongoRegex("/".preg_quote($_POST['name'], '/')."/i")
                    ];
                    switch ($_POST['level']) {
                        case 3:
                            $where["costum.typeCocity"] = "region";
                            break;
                        case 5:
                            $where["costum.typeCocity"] = "epci";
                            break;
                    }
                    $zones = PHDB::find(Zone::COLLECTION, $_where, $fields);
                } else {
                    $_where = [
                        "name" => new MongoRegex("/".preg_quote($_POST['name'], '/')."/i")
                    ];
                    $where = [
                        "costum.cocity" => ['$exists' => true],
                        "costum.typeCocity" => 'ville'
                    ];
                    $zones = PHDB::find(City::COLLECTION, $_where, $fields);
                }
            } else if($_POST['collection'] == 'zones') {
                $_where = [
                    "countryCode" => $_POST['countryCode'],
                    "level" => array($_POST['level'])
                ];
                if(isset($_POST['idgetlevel']) && $_POST['idgetlevel'] != "") {
                    $_where["level".$_POST['getLevel']] = $_POST['idgetlevel'];
                }

                switch ($_POST['level']) {
                    case 3:
                        $where["address.addressCountry"] = $_POST['countryCode'];
                        $where["costum.typeCocity"] = "region";
                        break;
                
                    case 4:
                        $where["address.level3"] = $_POST['idgetlevel'];
                        $where["costum.typeCocity"] = $_POST['countryCode'] == "MG" ? "district" : "departement";
                        break;
                
                    case 5:
                        $where["address.level4"] = $_POST['idgetlevel'];
                        $where["costum.typeCocity"] = "epci";
                        break;
                
                }
               
                $zones = PHDB::find(Zone::COLLECTION, $_where, $fields);
            } else {
                if($_POST['typeZone'] == 'EPCI') {
                    $zone = PHDB::findOne(Zone::COLLECTION, array("_id" => new MongoId($_POST['idgetlevel'])), array('cities'));
                    $_cities = $zone['cities'];
                    $zones = PHDB::find(City::COLLECTION, array("_id" => ['$in' => array_map(fn($id) => new MongoId($id), $_cities)]), $fields);
                    $where = [
                        "address.localityId" => ['$in' => $_cities],
                        "costum.cocity" => ['$exists' => true],
                        "costum.typeCocity" => 'ville'
                    ];
                } else {
                    $zones = PHDB::find(City::COLLECTION, array('level4' => $_POST['idgetlevel']), $fields);
                    $where = [
                        "address.level4" => $_POST['idgetlevel'],
                        "costum.cocity" => ['$exists' => true],
                        "costum.typeCocity" => 'ville'
                    ];
                }
            }

            $_sousCocity = PHDB::find(Organization::COLLECTION, $where);

            $results['zones'] = $zones;
            $results['sousCocity'] = $_sousCocity;
            $results['where'] = $where;

            return Rest::json($results);
        }
    }
?>