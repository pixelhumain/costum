<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use CAction, Cocity, Rest;
class GetFiliereAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cocity::getFiliere($_POST);
        
        return Rest::json($params);
    }
}