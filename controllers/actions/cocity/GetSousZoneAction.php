<?php

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

    use MongoId;
    use PHDB;
    use Rest;
    use Zone;

    Class GetSousZoneAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            $level = $_POST["level"];
            $levelNumber = $level == '3' ? 'level1' : 'level3'; 
            $zone = PHDB::find(Zone::COLLECTION, array(
                $levelNumber => $_POST["zoneId"],
                "level" => [$level]
            ));
            return Rest::json($zone);
        }
    }
?>