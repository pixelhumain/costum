<?php

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use City;
use MongoRegex;
use PHDB;
use Rest;

    class ExternDataCityShapeAction extends \PixelHumain\PixelHumain\components\Action {

        public function run() {

            $fields = ["geoShape", "name", "level1", "level1Name", "level3", "level3Name", "level4Name", "level4", "country"];
            $communes = $_POST['communes'];
            $shapes = [];

            $filter = array(
                "name" => array(
                    '$in' => array_map(function($commune) {
                        return new MongoRegex("/^".preg_quote($commune, '/')."$/i");
                    }, $communes)
                )
            );
            
            $shapes = PHDB::find(City::COLLECTION, $filter, $fields);

            return Rest::json($shapes);
        }
    }
?>