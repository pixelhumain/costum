<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use Cocity, Rest;
class CheckDataFiliereAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $address = isset($_POST["address"]) ? $_POST["address"] : [];
        $params = Cocity::checkDataFiliere($_POST["thematic"], $address, $_POST["typeCity"]);
        return Rest::json($params);
    }
}