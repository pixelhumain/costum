<?php


namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;


use PixelHumain\PixelHumain\components\Action;
use Yii, Authorisation;

class AnalyseCocityAction extends Action {
    public function run(){
        $controller = $this->getController();
        if(Authorisation::isInterfaceAdmin()){
            $params = array();
            return $controller->renderPartial("costum.views.custom.cocityPrez.admin.analyseCocity",$params,true);
        }
        else
            return $controller->renderPartial("co2.views.error.error",array("error"=>array("code"=>"303", "message"=>Yii::t("common","You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system"))),true);
    }
}