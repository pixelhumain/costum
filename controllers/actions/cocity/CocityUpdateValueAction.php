<?php 

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

    use MongoId;
    use Organization;
    use PHDB;
    use Rest;

    class CocityUpdateValueAction extends \PixelHumain\PixelHumain\components\Action {
        public function run() {
            $_id = $_POST["id"];
            if(isset($_POST["value"]["geoPosition"]["coordinates"])){
                $_POST["value"]["geoPosition"]["coordinates"] = array_map(function($coordinate){
                    return doubleval($coordinate);
                }, $_POST["value"]["geoPosition"]["coordinates"]);
            }
            
            $result["result"] = PHDB::update(Organization::COLLECTION, array("_id" => new MongoId($_id)), array('$set' => $_POST["value"]));
            $result["message"] = "Element Updated";
            $result["data"] = $_POST["value"];   
            return Rest::json($result);
        }
    }

?>