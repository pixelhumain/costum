<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use CAction, Rest;
use Citoyen;
use Event;
use Organization;
use PHDB;
use Poi;
use Project;
use Person;
use Zone;
use City;
use MongoId;
class GetNuberElementByCityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = [];
        if(isset($_POST["level"] ) && $_POST["level"] != "") {
            if(isset($_POST["country"]) && $_POST["country"] == "MG") {
                $where = array("level4" => $_POST["epci"] , "cocityId" => array('$exists'=> false));
                $params = PHDB::find(City::COLLECTION, $where);
                
                // $_cities = $params['cities'];
                // 		($_cities && count($_cities) > 0) ? $addressParams[] = ['address.localityId' => array('$in' => $_cities)];

                $types = array(Organization::COLLECTION, Project::COLLECTION, Poi::COLLECTION, Event::COLLECTION,Person::COLLECTION);
                foreach ($params as $key=>$value) {
                    $params[$key]["TotalElement"] = 0;
                    foreach ($types as $keyType => $type) {
                        $condition = array("address.localityId" => $key);
                        $element = PHDB::count($type, $condition);
                        $params[$key]["nbr".$type] = $element;
                        $params[$key]["TotalElement"] = $params[$key]["TotalElement"]+$element;
                    }
                    //$params[$key]["TotalElement"] = $params["nbrorganizations"] + $params["nbrprojects"]  + $params["nbrpoi"] + $params["nbrevents"]  + $params["nbrcitoyens"] ;
                }
                uasort($params, function ($a, $b) {
                    return $b['TotalElement'] <=> $a['TotalElement'];
                });
            }
            else {
                if (isset($_POST["departement"]))  {
                    $where = array("level" => $_POST["level"], "level3" => $_POST["departement"] , "cocityId" => array('$exists'=> false));
                }elseif(isset($_POST["epci"])) {
                    $where = array("level" => $_POST["level"], "level4" => $_POST["epci"] , "cocityId" => array('$exists'=> false));
                }else {
                    $where = array("level" => $_POST["level"],"cocityId" => array('$exists'=> false));
                }

                $params = PHDB::find(Zone::COLLECTION, $where);
                
                // $_cities = $params['cities'];
                // 		($_cities && count($_cities) > 0) ? $addressParams[] = ['address.localityId' => array('$in' => $_cities)];

                $types = array(Organization::COLLECTION, Project::COLLECTION, Poi::COLLECTION, Event::COLLECTION,Person::COLLECTION);
                foreach ($params as $key=>$value) {
                    $params[$key]["TotalElement"] = 0;
                    foreach ($types as $keyType => $type) {
                        if (isset($_POST["epci"])) {
                            $_cities = $value['cities'];
                            $condition = array(/*"address.level".$_POST["level"] => $key,*/'address.localityId' => array('$in' => $_cities));
                        } else {
                            $condition = array("address.level".$_POST["level"] => $key);
                        }
                        $element = PHDB::count($type, $condition);
                        $params[$key]["nbr".$type] = $element;
                        $params[$key]["TotalElement"] = $params[$key]["TotalElement"]+$element;
                    }
                    //$params[$key]["TotalElement"] = $params["nbrorganizations"] + $params["nbrprojects"]  + $params["nbrpoi"] + $params["nbrevents"]  + $params["nbrcitoyens"] ;
                }
                uasort($params, function ($a, $b) {
                    return $b['TotalElement'] <=> $a['TotalElement'];
                });  
                
            } 
        } elseif (isset($_POST["cities"])){
            $where = array("_id" => new MongoId($_POST["cities"])/*,"cocityId" => array('$exists'=> false)*/);
            $paramsZone = PHDB::findOne(Zone::COLLECTION, $where);
            $types = array(Organization::COLLECTION, Project::COLLECTION, Poi::COLLECTION, Event::COLLECTION,Person::COLLECTION);
            if (isset($paramsZone)) {
                foreach ($paramsZone['cities'] as $k => $localityId) {
                    $paramsCities = PHDB::findOne(City::COLLECTION, array("_id" => new MongoId($localityId),"cocityId" => array('$exists'=> false)));
                    $paramsCities["TotalElement"] = 0;
                    foreach ($types as $keyType => $type) {
                        $element = PHDB::count($type, array('address.localityId' => $localityId));
                        $paramsCities["nbr".$type] = $element;
                        $paramsCities["TotalElement"] = $paramsCities["TotalElement"]+$element;
                    };
                    $params[$localityId] = $paramsCities;
                }
            }
            uasort($params, function ($a, $b) {
                return $b['TotalElement'] <=> $a['TotalElement'];
            }); 
        }
        return Rest::json($params);
    }
}