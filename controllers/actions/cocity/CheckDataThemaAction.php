<?php 

    namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use Citoyen;
use Event;
use Organization;
use PHDB;
use Poi;
use Project;
use Rest;

    class CheckDataThemaAction extends \PixelHumain\PixelHumain\components\Action {
        public function run(){
            $themas = isset($_POST["thematiques"]) ? $_POST["thematiques"] : [];
            $cityId = isset($_POST["cityId"]) ? $_POST["cityId"] : [];

            $allData = [];

            foreach ($themas as $themaKey => $thema) {
                $ucTags = array_map('ucfirst', $thema["tags"]);

                $where = array(
                    "address.level1" => array('$in' => $cityId)
                );

                if ($ucTags == ["TiersLieux"] || $ucTags == ["Pacte"]) {
                    unset($where['tags']);
                    $sourceKey = ($ucTags == ["TiersLieux"]) ? "franceTierslieux" : "siteDuPactePourLaTransition";
                    $where['$or'] = array(
                        ['source.keys' => $sourceKey],
                        ['reference.costum' => $sourceKey],
                        ['tags' => array('$in' => $ucTags)]
                    );
                } else {
                    unset($where['$or']);
                    $where['tags'] = array('$in' => $ucTags);
                }
        
                $organisations = PHDB::count(Organization::COLLECTION, $where);
                $projets = PHDB::count(Project::COLLECTION, $where);
                $events = PHDB::count(Event::COLLECTION, $where);
                $citoyens = PHDB::count(Citoyen::COLLECTION, $where);
                $poi = PHDB::count(Poi::COLLECTION, $where);
        
                $somme = $organisations + $projets + $events + $citoyens + $poi;
                $tempData = array(
                    "organization" => $organisations,
                    "projet" => $projets,
                    "event" => $events,
                    "citoyen" => $citoyens,
                    "poi" => $poi,
                    "icon" => $thema["icon"],
                    "somme" => $somme
                );
                $allData[$themaKey] = $tempData;
            }

            return Rest::json($allData);
        }
    }  
?>