<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class CommunityController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\community\DashboardAction::class,
	        'home'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\community\HomeAction::class,
	    );
	}

}