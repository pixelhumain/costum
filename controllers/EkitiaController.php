<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class EkitiaController extends CommunecterController
{
    public function actions(){
        return array(
            'validategroup' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ValidateGroupAction::class,
            'references' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ReferencesAction::class,
            'register' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\RegisterAction::class,
            'setsource' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ManageReferencesAction::class,
            'validateinvitationbymail' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\ValidateInvitationByMailAction::class,
            "upload-save" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\ekitia\UploadSaveAction::class,
        );
    }
}