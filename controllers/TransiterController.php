<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class TransiterController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	       'deletetags'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\transiter\DeleteTagsAction::class,
        );
    }
}