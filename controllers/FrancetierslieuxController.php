<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class FrancetierslieuxController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
			'getlikednewsaction' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetLikedNewsAction::class,
	        'answerdirectory'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\AnswerDirectoryAction::class,
			'autoglobalthematicntwrk' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\AutoGlobalThematicNtwrkAction::class,
			'getaggregatetr' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetAggregateTrAction::class,
			'getallsuplanswers' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetallsuplanswersAction::class,
			'listtoolsanswers' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ListToolsAnswersActions::class,
			'getallanswers'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetAllAnswersAction::class,
			'getnetworkswerelist' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetNetworksWereListAction::class,
	        
			'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\DashboardAction::class,
	        'existingreference' =>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ExistingreferenceAction::class,
	        'getnetwork'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetNetworkAction::class,
	        'multireference' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\MultiReferenceAction::class,
	        'dumprecencement2023' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ObservatoireDumpAction::class,
			'notfinishedinforecensement2023' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\NotFinishedInfoRecensement2023Action::class,
	        'diagnoanswers' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\DiagnoAnswersAction::class,
			'setidrecensement' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\SetIdRecensementAction::class,
			'addinseedensity' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\AddInseeDensityAction::class,

			// Francki Navigator For Icons
			'icons' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator\IconsAction::class,
			'criteria' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator\CriteriaDescriptionAction::class,
			'savecriteria' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\navigator\SaveCriteriaAction::class,
	    );
	}

}
