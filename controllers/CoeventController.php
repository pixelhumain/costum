<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
use PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\UserManagement;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoeventController extends CommunecterController {


	public function beforeAction($action) {
		//parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions() {
		return array(
			'getpdfaction'	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\pdf\GetPdfAction::class,
			'get_events'	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\EventAction::class,
			'join_event'	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\JoinAction::class,
			'user_manager'	=> UserManagement::class,
		);
	}
}
