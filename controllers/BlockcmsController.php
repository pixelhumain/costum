<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class BlockcmsController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getpoiaction'    => 'costum.controllers.actions.blockcms.GetPoiAction',
			'geteventsaction' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetEventsAction::class,
			'getcollection'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCollectionAction::class,
			'geteventaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetEventAction::class,
			'getcmsstaticexistaction' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCmsStaticExistAction::class,
			'getusersaction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetUsersAction::class,
			'getlisttemplatestaticaction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetListTemplateStaticAction::class,
			'getnamecreator' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetNameCreatorAction::class,
			'getvideo' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetVideoAction::class,
			'dragblock' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\DragBlockAction::class,
			// 'gettemplatebycategory' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetTemplateByCategoryAction::class,
			'getdocumentationaction' =>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetDocumentationAction::class,
			'getcmscocityaction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCmsCocityAction::class,
			'loadbloccms'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\LoadBlocAction::class,
			'loadfootercms'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\LoadFooterAction::class,
			'loadbtnedit'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\loadBtnBlocAction::class,
			'saveposition'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\SavePositionAction::class,
			'getcmsbywhere'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCmsByWhereAction::class,
			'getcms'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms\GetCmsAction::class,
			"getformsdata" => \PixelHumain\PixelHumain\modules\costum\controllers\actions\blockgraph\GetFormsDataAction::class
		);
	}
}
?>