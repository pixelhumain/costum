<?php
    
    namespace PixelHumain\PixelHumain\modules\costum\controllers;
    
    use CommunecterController;
    
    class FormationgeneriqueController extends CommunecterController {
        
        
        public function beforeAction($action) {
            //parent::initPage();
            return parent::beforeAction($action);
        }
        
        public function actions() {
            return array(
                'importcontact'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\ImportContactAction::class,
                'gestionformation' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\GestionFormationAction::class,
                'gestionsession'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\GestionSessionAction::class,
                'inviteinsession'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\InviteInSessionAction::class,
                'connectsession'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\ConnectSessionAction::class,
                'register'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\RegisterAction::class,
                'aftersave'   => \PixelHumain\PixelHumain\modules\costum\controllers\actions\formationGenerique\AfterSaveAction::class,
            );
        }
        
    }
