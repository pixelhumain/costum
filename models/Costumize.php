<?php

class Costumize {
    const COLLECTION = "costums";
    const CONTROLLER = "costum";
    const MODULE = "costum";
    public static function saveImport($params){
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $costum = CacheHelper::getCostum();
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
            foreach ($res["elementsObj"] as $key => $value) {
                if(isset($value["thematic"])){
                    $thematic = array();
                    foreach(explode(",",$value["thematic"]) as $kt => $vt) {
                        $thematic[] = ucfirst(trim($vt));
                    }
                    $value["thematic"] = $thematic;
                }
                if(isset($value["telephone"]["mobile"])){
                    $mobile = array();
                    foreach(explode("/",$value["telephone"]["mobile"]) as $km => $vm) {
                        $mobile[] = trim($vm);
                    }
                    $value["telephone"]["mobile"] = $mobile;
                }
                if(isset($value["telephone"]["fixe"])){
                    $fixe = array();
                    foreach(explode("/",$value["telephone"]["fixe"]) as $kf => $vf) {
                        $fixe[] = trim($vf);
                    }
                    $value["telephone"]["fixe"] = $fixe;
                }
                $saveaddress =  array();
                if(isset($value["address"])){
                    if(isset($value["address"]["localityId"])){
                        $locality = PHDB::findOne("cities",array(
                            '_id' => new MongoId($value["address"]["localityId"])
                        ));
                        if($locality){
                            $value["address"]["@type"] =  "PostalAddress";
                            if(isset($locality["insee"]))
                                $value["address"]["codeInsee"] =  $locality["insee"];
                            if(isset($value["address"]["streetAddress"]))
                                $value["address"]["streetAddress"] =  $value["address"]["streetAddress"];
                            else
                                $value["address"]["streetAddress"] =  ""; 
                            $value["address"]["postalCode"] =  "";  
                            $value["address"]["addressLocality"] =  $locality["name"];  
                            $value["address"]["level1"] =  $locality["level1"];  
                            $value["address"]["level1Name"] =  $locality["level1Name"];  
                            $value["address"]["addressCountry"] =  $locality["country"]; 
                            $value["address"]["level2"] =  $locality["level2"];  
                            $value["address"]["level2Name"] =  $locality["level2Name"];  
                            $value["address"]["level3"] =  $locality["level3"];  
                            $value["address"]["level3Name"] =  $locality["level3Name"];  
                            $value["address"]["level4"] =  $locality["level4"];  
                            $value["address"]["level4Name"] =  $locality["level4Name"]; 
                            $value["address"]["localityId"] =  (String)$locality["_id"]; 
                            if(isset($locality["geo"])){
                                $value["geo"] = $locality["geo"];
                            }
                            if(isset($locality["geoPosition"])){
                                $value["geoPosition"] = $locality["geoPosition"];
                            }
                        }else{
                            $saveaddress["address"] = $value["address"];
                            $saveaddress["address"]["@type"] =  "PostalAddress";
                            $saveaddress["address"]["codeInsee"] =  "";
                            if(isset($value["address"]["level4"])){
                                $level4 = PHDB::findOne("zones",array(
                                    '_id' => new MongoId($value["address"]["level4"])
                                ),array("geo","geoPosition"));
                                if($level4){
                                    if(isset($level4["geo"])){
                                        $saveaddress["geo"] = $level4["geo"];
                                    }
                                    if(isset($level4["geoPosition"])){
                                        $saveaddress["geoPosition"] = $level4["geoPosition"];
                                    }
                                }
                            }
                            unset($value["address"]);
                        }
                    }else{
                        $saveaddress["address"] = $value["address"];
                        $saveaddress["address"]["@type"] =  "PostalAddress";
                        $saveaddress["address"]["codeInsee"] =  "";
                        if(isset($value["address"]["level4"])){
                            $level4 = PHDB::findOne("zones",array(
                                '_id' => new MongoId($value["address"]["level4"])
                            ),array("geo","geoPosition"));
                            if($level4){
                                if(isset($level4["geo"])){
                                    $saveaddress["geo"] = $level4["geo"];
                                }
                                if(isset($level4["geoPosition"])){
                                    $saveaddress["geoPosition"] = $level4["geoPosition"];
                                }
                            }
                        }
                        unset($value["address"]);
                    }
                }               
                $value["collection"] = $params["typeElement"];
                if($params["typeElement"] == "organizations")
                    $value["type"] = "NGO";
                $value["creator"] = Yii::app()->session["userId"];
                $results = Element::save($value);
                if($saveaddress )
                    $up = PHDB::update( $params["typeElement"],  array("_id" => new MongoId($results["id"])), 
                        array('$set' => $saveaddress ));
                $child = array(
                    "childId" => $results["id"],
                    "childType" => $params["typeElement"],
                    "childName" => $value["name"]
                );
                $parentType = $params["costumType"];
                $parentId = $params["costumId"];
                $resElt = array(
                    "add" => true,
                    "name" => @$value["name"]
                );
                Link::connectParentToChild($parentId, $parentType, $child, false,Yii::app()->session["userId"], []);
                $resultImport[] = $resElt;
            }
            
        }
        $res["resultImport"] = $resultImport;
        
        return Rest::json($res);
    }
    public static function elementBeforeSave($params){
		if($_POST["costumSlug"] == "transiter"){
            $res=$params["elt"];
            if($params["collection"]==Person::COLLECTION){
                $costum = CacheHelper::getCostum();
                $elem=PHDB::findOneById($params["collection"],$params["id"]);
                
                
                if(empty($elem)){
                    $newPerson = array(
                        "name" => $params["elt"]["name"],
                        "email" => $params["elt"]["email"],
                        "invitedBy" => Yii::app()->session["userId"]
                    );
     
                    $person = Person::createAndInvite($newPerson, @$value["msg"]);
                    Mail::validatePerson($person["person"]);
                    $elem=($person["result"]==true) ? $person["person"] : $params["elt"];
                    $elem["id"]=$person["id"] ?? null;
                    // var_dump($elem);exit;
                    $params["elt"]["id"]=$elem["id"] ?? null;
                    $res = $params;
                }
                 
                if(isset($params["elt"]["mobile"])){
                    element::updateField($params["collection"], $params["id"], "mobile", $params["elt"]["mobile"]);
                }
            }
            return $res;

        }
    }
   
}