<?php

class Ekisphere
{
    const COLLECTION = "costums";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function handleInvitationByLink($params){
        $user = PHDB::findOneById(Person::COLLECTION, $params["userId"], array("links"));
        $target = PHDB::findOneById( $params["targetType"], $params["targetId"], array("slug"));
        if($target["slug"] != "ekisphere"){
            $ekitia = PHDB::findOne(Organization::COLLECTION, array("slug" => "ekisphere"));
            $ekitia["links"]["members"][$params["userId"]] = array("type" => Person::COLLECTION);
            $user["links"]["memberOf"][(string) $ekitia['_id']] = array("type" => Organization::COLLECTION);
            $queryEkitia = array("links.members" => $ekitia["links"]["members"]);
            $query= array("links.memberOf" => $user["links"]["memberOf"]);
            $resEktia = PHDB::update( Organization::COLLECTION,
                array("slug" => "ekisphere"),
                array('$set'=>$queryEkitia)
            );
            $resUser = PHDB::update( Person::COLLECTION,
                array("_id" => new MongoId($params["userId"])),
                array('$set'=>$query)
            );
        }
    }

    public static function canAccessAnswer($params){
        $canAccess = false;
        $userSession = Yii::app()->session["userId"];

        if($userSession){
            $canAccess = true;
        }
        
        return $canAccess;
    }
}