<?php
    use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
    class CoSindniSmarterre {
        const COLLECTION = "costum";
        const CONTROLLER = "costum";
        const MODULE = "costum";

        public static function interventionArea($entries){
            $results = array();
            if(!empty($entries["formid"])){
                $results = PHDB::distinct(Form::ANSWER_COLLECTION,"answers.aapStep1.interventionArea",array(
                    "form" => $entries["formid"],
                    "answers.aapStep1.titre" => ['$exists' => true]
                ));
                $results = array_map(function($q){
                    if(Api::isValidMongoId($q))
                        return new MongoId($q);
                },$results);
                $results = PHDB::find(Zone::COLLECTION,array("_id" => ['$in'=> $results["quartiers"]]),array("name"));
            }
            return $results;
        }
    }
?>