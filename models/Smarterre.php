<?php
/*
Note : source.key est mis n'est pas encore utilisée, je l'ai mis en cas besoin;
*/
class Smarterre{
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";

    //Récupère l'ensemble de tous les thematiques
    public static function getAllUrl(){
        $params = array(
            "result"    =>  false
        );


        $source = array(
            "source.key"    =>  "smarterre");

        $where = array("thematique" => array(
                                            "education",
                                            "dechets",
                                            "commun",
                                            "citoyennete",
                                            "transport",
                                            "construction",
                                            "economie"
        ));

        $allUrl = PHDB::find(Bookmark::COLLECTION, $where);
        
        if($allUrl) return $params = array("result" => true, $allThematique);

        return $params;
    }

    //Récupère les smarterritoire
    public static function getSmarterritories(){
        $parametres = array(
           "result" => false
       );

        date_default_timezone_set('UTC');
       
       $where = array(
            "tags" =>    array("smarterritoire")
            );

        $allsmarterritories = PHDB::findAndLimitAndIndex(Organization::COLLECTION, $where,3);

        // var_dump($allsmarterritories);

       if(isset($allsmarterritories)){

           $results = array();
           
           $parametres = array(
               "result" =>  true
           );

           $results = self::createResultSmarterritories($allsmarterritories);
           return array_merge($parametres,$results);
       }
       return $parametres;
    }

    private static function createResultSmarterritories($params){

       $res["element"] = array();
       foreach($params as $key => $value){
        $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
        $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
        $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"], 
               "type"             =>  Yii::t("organization",$value["type"]),
               "imgMedium"        =>  $imgMedium,
               "img"              =>  $img,
               "slug"             =>  $value["slug"]
           ));
       }
       return $res;
   }

    //Récupère tous les thematiques
    public static function getThematique($post){
        $params = array(
            "result"    =>  false);

        $source = array(
            "source.key"    =>  "smarterre"
        );

        $where = array("docType" => "smarterre.".$post['thematique']);
        $allThematique = PHDB::find(Folder::COLLECTION, $where);

        // var_dump($allThematique);

        //El famoso tableau
        $res = array();
        if(@$allThematique){
            $params = array(
                "result"    =>  true
            );
            $res = self::createFile($allThematique);
        } 

        return array_merge($params,$res);
        return $params;        
    }

    //Permet l'affichage de l'URL quand on clique sur une thematique
    public static function searchUrl($post){

		$params = array(
			"result" => false);
		$source = array(
            "source.key" => "smarterre");
            
		if(@$post){
			$tab = array();
            $where = array("category" => $post["thematique"], "parentId" => $post["parentId"]);
            $url = PHDB::find(Bookmark::COLLECTION, $where);

            if($url) return $params = array("result" => true, "urls" => self::getUrl($url));
        }
		return $params;
    }
    

    //Récupère les liens et les tris
    public static function getUrl($params){
        $res = array();
        // var_dump($params);
        foreach($params as $key => $value){
            array_push($res, array("name"   =>   $value["name"],
                                    "url"   =>   $value["url"],
                                    "tags"  =>   $value["tags"]));
        }
        return $res;
    }

    //Création du tableau qui sera envoyé pour afficher les thematiques
    private static function createFile($params){
        // var_dump($params);
        $res["element"] = array();
        $tab = array();
        $i = 1;
        foreach($params as $key => $value){
            if(!isset($value["parentId"])){
                $parent = (String) $value["_id"];
                $tab[$parent] = array("title"  =>  $value["name"] , "description" => array(), "count" => $i);
                $i++;
                foreach($params as $k => $v){
                    if(isset($v["parentId"]) && $parent == (String) $v["parentId"]){
                        array_push($tab[$parent]["description"], array(
                                "name"      =>      $v["name"],
                                "sous_menu" =>      self::getSubMenu($v["_id"]),
                                "id"        =>      (String) ($v["_id"])
                            ));
                    } 
                }
                array_push($res["element"], $tab[$parent]);
            }
        }
        // var_dump($res);
        return $res;
    }

    private static function getSubMenu($id){
        $params = array(
            "result"        =>  false);

        $where = array("parentId" => (String) new MongoId ($id));
        $subMenu = PHDB::find(Folder::COLLECTION, $where);
        
        // var_dump($subMenu);

        if(@$subMenu){
            $res = array();
            foreach($subMenu as $key => $value){
                array_push($res, array(
                    "name" => $value["name"],
                    "id"   => (String) $value["_id"]));
            }
            return $res;
        }

        return $params;
    }
    
}