<?php
/**
 * 
 */
class hubUltraNumerique{
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
  const MODULE = "costum";

    public static function getEventCommunity($id,$type){
        $communityForEvent = Element::getCommunityByTypeAndId($type, $id, "all", null , "mednum",null);

        if (isset($communityForEvent)) {
            $results = self::getDataEventCommunity($communityForEvent);
        }
        return $results;
    }

    private static function getDataEventCommunity($data){
        $params = array(
            "result" => false
        );

        $elements = [];
        $tabres = [];

        foreach ($data as $k => $v) {
            $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("name", "profilImageUrl","profilRealBannerUrl","links","slug"));

            $tabres += PHDB::find(Event::COLLECTION,array(
                                                        "source"    => array(
                                                        "insertOrign" =>    "costum",
                                                        "keys"         =>    array(
                                                        $elements[$k]["slug"]),
                                                        "key"          =>  $elements[$k]["slug"])));
        }

        if (isset($tabres)) {
            $params = array(
                "result" => true
            );

            $results  = self::createResultEventCommunity($tabres);

            return array_merge($params,$results);
        }
        return $results;
    }

    private static function createResultEventCommunity($params){

        $res["element"] = array();

        foreach($params as $key => $value){

            $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $resume = (@$value["shortDescription"] ? $value["shortDescription"] : "");

            array_push($res["element"], array(
            "id"               => (String) $value["_id"],
            "name"             =>  $value["name"],
            "startDate"        =>  date(DateTime::ISO8601, $value["startDate"]->sec),
            "type"             =>  Yii::t("event",$value["type"]),
            "sourceKey"        =>  $value["source"]["keys"],
            "imgMedium"        =>  $imgMedium,
            "img"              =>  $img,
            "resume"           =>  $resume,
            "slug"             =>  $value["slug"]
            ));
        }
        return $res;
    }

    public static function getCommunity($id,$type){
        $community = Element::getCommunityByTypeAndId($type, $id, "all", null , "mednum",null);
        //var_dump($community);exit;

        if (isset($community)) {
            $results = self::getDataCommunity($community);
        }

        return $results;
    }

    private static function getDataCommunity($data){
        $params = array(
            "result" => false
        );

        $elements = [];

        foreach ($data as $k => $v) {
        $elements[$k] = Element::getElementSimpleById($k, $v["type"], null, array("_id","name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" )); 
        }
    // var_dump($elements);

        if (isset($elements)) {
            $params = array(
                "result" => true
            );

            $results  = self::createResultCommunity($elements);
            return array_merge($params,$results);
        }
        return $results;
    }

    private static function createResultCommunity($params){

        $res["elt"] = array();

        foreach($params as $key => $value){
            if(@$value["type"] != "")
                $value["typeOrga"] = $value["type"];
                    $value["type"] = "organizations";
                    $value["typeSig"] = Organization::COLLECTION;
                    
            $imgMedium = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $imgBanner = (@$value["profilRealBannerUrl"] ? $value["profilRealBannerUrl"] : "none");
            $countActus = PHDB::count(News::COLLECTION,array("source.key" => $value["slug"]));
            $countOrga= PHDB::count(Organization::COLLECTION,array("source.key" => $value["slug"]));
            $countAllEvents= PHDB::count(Event::COLLECTION,array("source.key" => $value["slug"]));
            $countAllProjects = PHDB::count(Project::COLLECTION,array("source.key" => $value["slug"]));

            
            $res["elt"][$key] = array(
            "_id"               => $value["_id"],
            "name"             =>  $value["name"],
            "imgMedium"        =>  $imgMedium,
            "img"              =>  $img,
            "imgBanner"        =>  $imgBanner,
            "type"             => $value["typeSig"],
            "profilMarkerImageUrl" => @$value["profilMarkerImageUrl"],
            "address"          =>  @$value["address"],
            "geo"              =>  @$value["geo"],
            "geoPosition"      =>  @$value["geoPosition"],
            "profilThumbImageUrl" => @$value["profilThumbImageUrl"],
            "slug"             =>  $value["slug"],  
            "countEvent"       =>  count(@$value["links"]["events"]),
            "countActeurs"     =>  count(@$value["links"]["members"]),
            "countProjet"      =>  count(@$value["links"]["projects"]),
            "countActus"       =>  @$countActus,
            "countOrga"        =>  @$countOrga,
            "countAllEvents"   =>  @$countAllEvents,
            "countAllProjects" =>  @$countAllProjects
            );
        }
    return $res;
    }
}
?>