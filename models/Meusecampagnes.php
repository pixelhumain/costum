<?php

class Meusecampagnes {
    public static function saveOrga($params){
        $params = Import::newStart($params);
        $res = Import::previewData($params, false,true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"])){

                    $where = array( "name" => $value["name"],
                                    "address.localityId" => @$value["address"]["localityId"],
                                    "source.key" => "meuseCampagnes"
                                );
                    $org = PHDB::findOne(Organization::COLLECTION, $where);
                    if(empty($org)){
                        $value["collection"]=Organization::COLLECTION;
                        Element::save($value);
                       // Yii::app()->mongodb->selectCollection(Organization::COLLECTION)->insert( $value );
                        $resElt = array(
                            "update" => false,
                            "id" => false,
                            "name" => @$value["name"]
                        );
                        $resultImport[] = $resElt; 
                    }
                    
                }   
            } 
        }
        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }
    public static function saveEvent($params){
        $params = Import::newStart($params);
        $res = Import::previewData($params, false, true);
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["name"])){

                    $where = array( "name" => $value["name"],
                                    "address.localityId" => @$value["address"]["localityId"],
                                    "startDate" => @$value["startDate"],
                                    "source.key" => "meuseCampagnes"
                                );
                    $event = PHDB::findOne(Event::COLLECTION, $where);
                    if(empty($event)){
                        $value["collection"]=Event::COLLECTION;
                        Element::save($value);
                       // Yii::app()->mongodb->selectCollection(Event::COLLECTION)->insert( $value );
                        $resElt = array(
                            "update" => false,
                            "id" => false,
                            "name" => @$value["name"]
                        );
                        $resultImport[] = $resElt;
                    }
                    
                }   
            } 
        }
        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }
}
?>