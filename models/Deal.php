<?php

namespace PixelHumain\PixelHumain\modules\costum\models;
use Answer;
use CacheHelper;
use Cron;
use Element;
use Form;
use Link;
use Mail;
use Organization;
use Person;
use PHDB;
use Yii;

class Deal {

    const INPUT_OPERATOR = "deal128";


    public static function addDataZourit($params){
       // var_dump($params["file"]);exit;
            //$parsed=Import::parsingCSV($params["file"]);
            //var_dump($parsed);exit;

             $mapping = [
                "id" => null,
                "collection" => "answers",
                "data" => [
                    ["Nom de l'accedant" => "Nom de l'accedant",
                    "path"=>"answers.deal1.deal121",
                    "value" => null
                    ],
                    ["Prenom de l'accedant" => "Prenom de l'accedant",
                    "path"=>"answers.deal1.deal122",
                    "value" => null
                    ],
                    ["Sit Fam" => ""],
                    ["Date de naissance" => "Date de naissance",
                    "path"=>"answers.deal1.deal123",
                    "value" => null
                    ],
                    ["Identifiant de la personne" => ""],
                    ["Reve Mensuel" => ""],
                    ["Nombre Occupant du logement" => ""],
                    ["Al Mensuelle Moyenne" => ""],
                    ["Ressources Menages" => "Ressources Menages",
                    "path" => "answers.deal1.deal135b.totalZourit",
                    "value" => "null"
                    ],
                    ["Numéro Allocataire" => "Numéro Allocataire",
                    "path"=>"answers.deal1.deal130",
                    "value" => null],
                    ["Section Cadastrale Diffus" => ""],
                    ["Numero de Parcelle Diffus" => "Numero de Parcelle Diffus",
                    "path"=>"answers.deal1.deal148",
                    "value" => null
                    ],
                    ["Adresse de l'accédant" => "Adresse de l'accédant",
                    "path"=>"answers.deal1.deal114",
                    "value" => null
                    ],
                    ["Opérateur financement" => "Opérateur financement",
                    "path" => ""
                    ]
                ]


            ];

            $mappingOpe=[
                "SOLIHA RÉUNION" => "5ea7dacc8b509caa108b456a",
                "SUD HABITAT CONSEIL" => "5ea7dd388b509caa108b4597",
                "SICA HR" => "5ea7dc168b509caa108b457e",
                "ARCHIPEL BOIS HABITAT" => "5ea7dde28b509caa108b45a4"
            ];
            $nameOpe = array_keys($mappingOpe);
           // var_dump($nameOpe);exit;

            $form = PHDB::findOneById( Form::COLLECTION , "5ea7d3a1275640c4999297ae");
            $params=explode("\n",$params["file"]);
            //var_dump($params);exit;


            $lines[]=[];
            $count=0;
            foreach ($params as $line) {
             $answer = Answer::generateAnswer($form, false, null);

            $colonne=explode(";", $line);

                    foreach($colonne as $key => $element){
                        if(isset($mapping["data"][$key]["path"])){
                            $mapping["data"][$key]["value"]=$element;
                            if(isset($mapping["data"][$key]["Opérateur financement"])){
                                if(in_array($element, $nameOpe)){
                                    $mapping["data"][$key]["path"]="links.operators.".$mappingOpe[$element];
                                    $mapping["data"][$key]["value"]=1;

                                }   
                            } 
                            $result=Element::updatePathValue(Answer::COLLECTION, $answer["_id"],  $mapping["data"][$key]["path"], $mapping["data"][$key]["value"]);
                            
                        }    
                           
                    }
            $count++;
            }


            return "<span>Les ".$count." dossiers provenant de Zourit ont été importés dans la plate-forme Améliore Out' Kaz. La première étape du formulaire est donc partiellement pré-remplie. Nous vous invitons à la compléter. <a class='lbh-menu-app' data-hash='#dossiers' href='#dossiers'>Accéder au répertoire des dossiers</a>.</span>";

            

           
        
    }

    public static function getCheckCustomUser($params){

        $links=$params["userLinks"];
        $communityLinks=$params["communityLinks"]["links"];

        if( isset($links["links"][Link::$linksTypes[Person::COLLECTION][ Organization::COLLECTION]] ) && isset($communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]]))
        { 
                //var_dump($links["links"]['memberOf']);

            // on boucle sur tous les liens de l'utilisateur concernant les organisations (memberOf)
                foreach($links["links"][Link::$linksTypes[Person::COLLECTION][ Organization::COLLECTION]] as $k =>$v){
                    // if(!empty($v["isAdmin"]) && !isset($v["isAdminPending"]) && isset($communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){

                    // on regarde si l'utilisateur n'est pas en attente de validation simple pour être membre 
                    // Et si l'organisation appartient à la communauté de la DEAL et qu'elle présente des rôles
                    if(!isset($v["toBeValidated"]) && isset($communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){

                        // si orga "opérateur" le membre hérite des droits opérateurs
                        if(in_array("Opérateur", $communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){     
                            $elt=Element::getElementById($k, $v["type"], null, array("name","slug", "profilThumbImageUrl"));
                            $operator=$elt;
                            $params["costumUserArray"]["hasRoles"] = ["Opérateur"];

                        }

                        // si orga "financeur" le membre hérite des droits opérateurs
                        if(in_array("Financeur", $communityLinks[Link::$linksTypes[$params["el"]["type"]][Person::COLLECTION]][$k]["roles"])){     
                            $elt=Element::getElementById($k, $v["type"], null, array("name","slug", "profilThumbImageUrl"));
                            $financor=$elt;
                            $params["costumUserArray"]["hasRoles"] = ["Financeur"];
                        }
                    }
                }
            
        }
        if(isset($operator))
            $params["costumUserArray"]["operatorOf"]=$operator;
        if(isset($financor))
            $params["costumUserArray"]["financorOf"]=$financor;
        //var_dump($params["costumUserArray"]);
        return $params["costumUserArray"];
    }
    public static function canAccessAnswer($ans){
        return Form::canAccess(array("roles"=>["Opérateur", "Financeur"]));
    }
    public static function canEditAnswer($ans){
        $params=@$_POST;
        $costum = CacheHelper::getCostum();
        if(!empty($params)){
            if(isset($params["path"]) && isset($params["value"])){
                if(strrpos($params["path"], "links.operators") !== false && $params["value"]==0 && isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])){
                    return true;
                }
            }
            if(self::isFinancor())
                return true;
            if(self::isOperatorOfAnswer($ans, $costum))
                return true;
        }
    }
    public static function canAdminAnswer($ans){
        $params=@$_POST;
        $costum = CacheHelper::getCostum();
        // if(!empty($params)){
            if(isset($params["path"]) && isset($params["value"])){
                if(strrpos($params["path"], "links.operators") !== false && $params["value"]==0 && isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"])){
                    return true;
                }
            }
            if(self::isFinancor())
                return true;
            if(self::isOperatorOfAnswer($ans, $costum))
                return true;
        // }
    }
    public static function isFinancor(){
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) && in_array("Financeur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]))
            return true;
        return false;
    }
    public static function isOperatorOfAnswer($ans, $costum){
        $res=false;
        // if(isset($ans["answer"]["links"]) 
        //     && isset($ans["answer"]["links"]["operators"]) 
        //     && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"])
        //     && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])
        //     && isset($ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]])
        //     && $ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]]==1)
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) 
            && in_array("Opérateur", Yii::app()->session["costum"][$costum["slug"]]["hasRoles"]) 
            && isset($ans["answer"]["links"]["operators"]) 
            // && isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"])
            // && isset($ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]])
            // && $ans["answer"]["links"]["operators"][(string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]]==1
        ) 
            $res=true;
        return $res;
    }
    public static function generateAnswerBeforeSave($params){
        $costum = CacheHelper::getCostum();
        if(isset(Yii::app()->session["costum"][$costum["slug"]]["operatorOf"])){
            $params["answer"]["links"]["operators"]=array((string)Yii::app()->session["costum"][$costum["slug"]]["operatorOf"]["_id"]=>1);
            $params["answer"]["answers"]["deal1"][self::INPUT_OPERATOR]=date("Y-m-d");
             
        }

        $params["answer"]["step"]="deal1";
       
       
        return $params;
    }
    public static function AnswerMailProcess($params){
        $answer=$params["answer"];
        if(isset($answer) && isset($answer["links"]["operators"]) && $params["tpl"]=="validation"){
            $nameAnsw=(isset($answer["mappingValues"]["name"])) ? $answer["mappingValues"]["name"] : "";
            foreach($answer["links"]["operators"] as $k => $state){
                $community=Element::getCommunityByTypeAndId(Organization::COLLECTION, $k,Person::COLLECTION, "isAdmin");
                if(is_array($state)){
                    foreach($community as $k => $v){
                        $people=Element::getElementById($k, Person::COLLECTION, null, array("email","name","slug") );
                        if($params["step"] == "deal1") $subject="Vous avez été choisi comme opérateur sur la réponse ".$nameAnsw;
                        else if($params["step"] == "deal12") $subject="Le dossier ".$nameAnsw." vous a été attribué en tant qu'opérateur";
                        else $subject="Une nouvelle étape a été validée sur le dossier ".$nameAnsw;
                        $params = array (
                            "type" => Cron::TYPE_MAIL,
                            "tpl"=>Answer::$mailConfig[$params["tpl"]]["tpl"],
                            "subject" => "[".Mail::getAppName()."] ".$subject,
                            "from"=>Yii::app()->params['adminEmail'],
                            "to" => $people["email"],
                            "tplParams" => array(  "user"=> $people ,
                                                    "title" => Mail::getAppName() ,
                                                    "answer" => $answer,
                                                    "form"=>$params["form"],
                                                    "msg"=>"En tant qu'opérateur de ce dossier, nous avons le plaisir de informer que le dossier a été validé"));                                        
                        $params=Mail::getCustomMail($params); 
                        Mail::schedule($params);
                    }
                }else if($state=="0" && @$params["step"]=="deal1"){
                    foreach($community as $k => $v){
                        $people=Element::getElementById($k, Person::COLLECTION, null, array("email","name","slug") );
                        $subject="Vous n'avez été pas été retenu sur le dossier ".$nameAnsw;
                        
                        $params = array (
                            "type" => Cron::TYPE_MAIL,
                            "tpl"=>"answer.rejected",
                            "subject" => "[".Mail::getAppName()."] ".$subject,
                            "from"=>Yii::app()->params['adminEmail'],
                            "to" => $people["email"],
                            "tplParams" => array(  "user"=> $people ,
                                                    "title" => Mail::getAppName() ,
                                                    "answer" => $answer,
                                                    "form"=>$params["form"],
                                                    "msg"=>"Vous avez postulé en tant qu'opérateur. Nous avons le regret de vous informer que vous n'avez pas été retenu pour prendre en charge la suite de ce dossier"));                                        
                        $params=Mail::getCustomMail($params); 
                        Mail::schedule($params);
                    }
                }
            }

        }
    }

    
    

}
?>