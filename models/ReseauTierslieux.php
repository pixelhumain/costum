<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;

class ReseauTierslieux {

	public static function connectElement($params){
		if($params("type")==Event::COLLECTION){
		
					if(isset($params["extraParams"]["rest"])){
						unset($params["extraParams"]["rest"]);
					}
					if(isset($params["extraParams"]["ref"])){
						unset($params["extraParams"]["ref"]);
					}

					// $updateParams=["type"=>"citoyens"];
					if(!empty($params["roles"])){
						$updateParams["links.attendees.".Yii::app()->session["userId"].".roles"]=$params["roles"];
					}
					foreach($params["extraParams"] as $k => $v){
						if($k=="visioDate"){
							$stampVisioDate=strtotime($v);
							$mongoVisioDate=new MongoDate($stampVisioDate);
							$v=$mongoVisioDate;
						}
						$updateParams["links.attendees.".Yii::app()->session["userId"].".".$k]=$v;

					}
					$user=PHDB::findOne(Person::COLLECTION,array("_id"=>New MongoId(Yii::app()->session["userId"])),array("email"));
					$interestedUser=array(
						"name"=>Yii::app()->session["user"]["name"],
						"email" =>$user["email"],
						"id"=>Yii::app()->session["userId"]
					);
					Link::connect((string)$params["id"], Event::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "attendees");
                    Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Event::COLLECTION, Yii::app()->session["userId"], "events");
					// var_dump($params,$updateParams);exit;
					if(!empty($updateParams)){
					PHDB::update(
						$params["type"],
						["_id" => new MongoId($params["id"])],
						['$set' => $updateParams]
					);
				    }

					$session=PHDB::findOne($params["type"],array("_id" => new MongoId($params["id"])));

					$visioDatePreSub=(isset($stampVisioDate) && !empty(date("Y-m-d", $stampVisioDate))) ? date("d-m-Y", $stampVisioDate)  : "";
					$visioHourPreSub=(isset($stampVisioDate) && !empty(date("H:i", $stampVisioDate))) ? date("H:i", $stampVisioDate)  : "";

					$paramsMail = array(
						"tpl" => "basic",
						"tplObject" => "Pré-inscription à la visioconférence d'information - Formation ". $session['name'] ,
						"tplMail" => $interestedUser["email"],
						"html" => "Vous nous avez communiqué votre intérêt pour la formation. Vous vous êtes pré-inscrit.e à la visioconférence d'information du ". $visioDatePreSub . " à ".$visioHourPreSub ." concernant la session de formation intitulée " . $session['name'] .".",
						// "community" => <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" )
						"btnRedirect" => array(
							"hash" => "#panel.modal-account",
							"label" => "Accéder au récapitulatif"
						),
						// "tplParams" =>[
						// 	"costumTpl"=>"sessionInterest",							 
						// 	"logo"=> Yii::app()->params["logoUrl"],
						// 	"logo2" => Yii::app()->params["logoUrl2"],
						// 	"baseUrl" => Yii::app()->getRequest()->getBaseUrl(true)."/"
							
						// ]
					);	
					// var_dump($source);exit;
					// if(isset($source["key"]))
					    // $costum=cacheHelper::getCostum();
						// // var_dump($costum);exit;
						// if(isset($costum)){
						// 	$params["tplParams"]["logo"]=$costum["logo"];
						//     $params["tplParams"]["logo2"]="";
						//     $params["tplParams"]["logoHeader"]=(isset($costum["mailsConfig"]["logo"])) ? $costum["mailsConfig"]["logo"] : $costum["logo"] ;
						// 	$paramsCostum["source"] = [];
						// 	$paramsCostum["source"]["key"]=isset($costum["contextSlug"]) ? $costum["contextSlug"] : $costum["slug"];
						//     if(@$costum && @$costum["title"]){
						// 		$params["tplParams"]["title"]=$costum["title"];
						// 		$params["expeditor"]=$costum["title"];
						// 	}
						// }	
						// }

					// $paramsMailCostum=array($paramsMail,$paramsCostum);
					$res=Mail::CreateAndSend($paramsMail);
					return $res;
	    }else{
			$res=Link::connectParentToChild($id, $type, $child, $isConnectingAdmin, Yii::app()->session["userId"], $roles);
			return $res;
		}		
				
	}

	public static function elementAfterSave($data){
		
		$costum = CacheHelper::getCostum();
		$elt=Element::getElementById($data["id"], $data["collection"]);
  
  
    	if($data["collection"]==Organization::COLLECTION){ 
            // add source FTL
			$sources=["franceTierslieux","tiersLieux"];
			foreach ($sources as $i => $source){
	    		Admin::addSourceInElement($data["id"],$data["collection"],$source,"reference");
	        }	    	
	    	$isInviting=false;
	    	$child=[];
	    	$child["childId"]=$data["id"];
        	$child["childType"]=$data["collection"];
			// var_dump($controller->costum["contextId"]);exit;
	        
			// Link third place to parent costum (network)
			Link::connect($costum["contextId"], $costum["contextType"], $child["childId"], $child["childType"], Yii::app()->session["userId"], "members");
            Link::connect($child["childId"], $child["childType"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
			// -- replace by doubled connect to avoid notification process.
			// Link::connectParentToChild($costum["contextId"], $costum["contextType"], $child, false, Yii::app()->session["userId"]);
	        		
			
			// $net=Element::getElementById($costum["contextId"],Organization::COLLECTION);
	        		// if($net){

		        	// 	Link::connect($net["_id"],$net["collection"],$data["id"],$data["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	// 	Link::connect($data["id"],$data["collection"],$net["_id"],$net["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		        	// }

			// size of third place from absolute to filter criterias in tags
			$size="";
			$value=$data["params"]["buildingSurfaceArea"];
			preg_match_all("/\s/",$value,$space);

			if(intval($data["params"]["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($data["params"]["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else if(intval($data["params"]["buildingSurfaceArea"])>200){
				$size="Plus de 200m²";
			}
			if(!empty($size) && !isset($space[0][0])){
				$commonVal=[];

				if(isset($data["params"]["tags"])){
					$commonVal=array_intersect($data["params"]["tags"],["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]);
				}
				if(!empty($commonVal)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$pull' => array("tags"=> array('$in'=>["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]))));
				}				
				PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$push' => array("tags"=> $size )));
			}
			// set idCartoTL single id for third places in map
			if(empty($elt["idCartoTL"])){
				$idsRec=PHDB::findAndSort(Organization::COLLECTION,array("idCartoTL"=>array('$exists'=>1)),array("idCartoTL"=>1));
				
				$nextId=(end($idsRec)["idCartoTL"]+1) ?? null;
				if(!empty($nextId)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$set' => array("idCartoTL"=> $nextId )));
				}
				// var_dump(count($idsRec),array_keys($idsRec)[count($idsRec)-1],end($idsRec)["idCartoTL"],$nextId,$idsRec);exit;
			}  			
	    }


		//Trainer et partner sessions de formation et formations ajouter dans participants + notif mail
		if(($data["collection"]==Project::COLLECTION && isset($data["params"]["category"]) && $data["params"]["category"]=="formation")||($data["collection"]==Event::COLLECTION && isset($data["params"]["category"]) && $data["params"]["category"]=="sessionFormation")){
			$children=array();
			$links=array();
			if(!empty($data["params"]["trainer"])){
				$links["citoyens"]=$data["params"]["trainer"];			    
			}if(!empty($data["params"]["partner"])){
				$links["organizations"]=$data["params"]["partner"];			    
			}
			
			if(!empty($links)){
				$element=PHDB::findOne($data["collection"],array("_id"=>new MongoId($data["id"])),array("links"));
				foreach($links as $collec=>$obj){
					$isConnectingAdmin=($collec==Person::COLLECTION) ? true : false ;
					
					foreach($obj as $id=>$val){
						$existingRoles=(isset($element["links"][Link::$linksTypes[$data["collection"]][$collec]][$id]["roles"])) ? $element["links"][Link::$linksTypes[$data["collection"]][$collec]][$id]["roles"]: [];
					    $userRole=($collec==Person::COLLECTION) ? array_merge($existingRoles,["Formateur.ice"]) : [] ;
					    // var_dump($existingRoles,$userRole);exit;
						Link::connect($data["id"], $data["collection"], $id, $collec, Yii::app()->session["userId"], Link::$linksTypes[$data["collection"]][$collec], $isConnectingAdmin, false, false, false, $userRole);
                        Link::connect($id, $collec, $data["id"], $data["collection"], Yii::app()->session["userId"], Link::$linksTypes[$collec][$data["collection"]], $isConnectingAdmin, false, false, false, $userRole);
					    if($collec==Person::COLLECTION){
							$tplObject="";
							$html="";
							$btnLabel="";
							$btnHash="";
							if($data["collection"]==Project::COLLECTION){
							    $tplObject="Formation" .$data["params"]["name"]. " - Formateur.ice / Administrateur.ice"; 
							    $html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la formation " .$data["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-formations?preview=".$data["collection"].".".$data["id"]."'>espace dédié</a>. Vous pourrez alors y créer les sessions à venir.";
							    $btnLabel="Consulter les informations liées à la formation";
							    $btnHash=Yii::app()->baseUrl."/#listing-formations?preview=".$data["collection"].".".$data["id"];
							}else if($data["collection"]==Event::COLLECTION){
							    $tplObject="Session de formation " .$data["params"]["name"]. " - Formateur.ice / Administrateur.ice";
								$html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la session de formation " .$data["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-sessions?preview=".$data["collection"].".".$data["id"]."'>espace dédié</a> et gérer le listing des stagiaires potentiels.";
							    $btnLabel="Consulter les informations liées à la session de formation";
							    $btnHash=Yii::app()->baseUrl."/#listing-sessions?preview=".$data["collection"].".".$data["id"];
							}	
						    $user = PHDB::findOne($collec,array("_id"=>new MongoId($id)),array("email")); 
						    $emailUser=$user["email"];
						    $paramsmail = array(
							    "tpl" => "basic",
							    "tplObject" => $tplObject,
							    "tplMail" => $emailUser,
							    "html" => $html,
							    // "community" => <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" )
							    "btnRedirect" => array(
								    "hash" => $btnHash,
								    "label" => $btnLabel
							    )
						    );	
						    Mail::createAndSend($paramsmail);
					    }					
				    }
				}	
			}
		}

		if($data["collection"]==Event::COLLECTION){
			// var_dump($data);exit;
			if(isset($data["params"]["category"]) && $data["params"]["category"]=="sessionFormation"){
				
				$invitLink=array(
					"targetType" => $data["collection"],
					"targetId" => (string)$data["params"]["_id"],
					"roles" => ["Intéressé.e"],
					"isAdmin" => false,
					"urlRedirection" => Yii::app()->baseUrl."/costum/co/index/slug/laCompagnieDesTierslieux#sessions-publics?preview=".$data["collection"].".".(string)$data["params"]["_id"],
					"invitorId" =>  Yii::app()->session["userId"],
                    "ref" => uniqid(),
                    "createOn" => time()
				);	
				// var_dump($invitLink);exit;
                PHDB::insert(InvitationLink::COLLECTION, $invitLink);
				PHDB::update( Event::COLLECTION, 
					  array("_id"=>$data["params"]["_id"]),
					array('$set' => array("interestedLink"=> "/co2/link/connect/ref/".$invitLink["ref"])));
			}
			

		}

		
	       
    }

    public static function elementAfterUpdate($params){

		//Trainer et partner sessions de formation et formations ajouter dans participants + notif mail
		if(($params["collection"]==Project::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="formation")||($params["collection"]==Event::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="sessionFormation")){
			$children=array();
			$links=array();
			if(!empty($params["params"]["trainer"])){
				$links["citoyens"]=$params["params"]["trainer"];			    
			}if(!empty($params["params"]["partner"])){
				$links["organizations"]=$params["params"]["partner"];			    
			}
			// var_dump($params);exit;
			if(!empty($links)){
				$element=PHDB::findOne($params["collection"],array("_id"=>new MongoId($params["id"])),array("links"));
				foreach($links as $collec=>$obj){
					$isConnectingAdmin=($collec==Person::COLLECTION) ? true : false ;
					
					foreach($obj as $id=>$val){
					  if(!isset($element["links"][Link::$linksTypes[$params["collection"]][$collec]][$id])){
						$existingRoles=(isset($element["links"][Link::$linksTypes[$params["collection"]][$collec]][$id]["roles"])) ? $element["links"][Link::$linksTypes[$params["collection"]][$collec]][$id]["roles"]: [];
					    $userRole=($collec==Person::COLLECTION) ? array_merge($existingRoles,["Formateur.ice"]) : [] ;
					    // var_dump($existingRoles,$userRole);exit;
						Link::connect($params["id"], $params["collection"], $id, $collec, Yii::app()->session["userId"], Link::$linksTypes[$params["collection"]][$collec], $isConnectingAdmin, false, false, false, $userRole);
                        Link::connect($id, $collec, $params["id"], $params["collection"], Yii::app()->session["userId"], Link::$linksTypes[$collec][$params["collection"]], $isConnectingAdmin, false, false, false, $userRole);
					    if($collec==Person::COLLECTION){
							$tplObject="";
							$html="";
							$btnLabel="";
							$btnHash="";
							if($params["collection"]==Project::COLLECTION){
							    $tplObject="Formation" .$params["params"]["name"]. " - Formateur.ice / Administrateur.ice"; 
							    $html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la formation " .$params["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-formations?preview=".$params["collection"].".".$params["id"]."'>espace dédié</a>. Vous pourrez alors y créer les sessions à venir.";
							    $btnLabel="Consulter les informations liées à la formation";
							    $btnHash=Yii::app()->baseUrl."/#listing-formations?preview=".$params["collection"].".".$params["id"];
							}else if($params["collection"]==Event::COLLECTION){
							    $tplObject="Session de formation " .$params["params"]["name"]. " - Formateur.ice / Administrateur.ice";
								$html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la session de formation " .$params["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-sessions?preview=".$params["collection"].".".$params["id"]."'>espace dédié</a> et gérer le listing des stagiaires potentiels.";
							    $btnLabel="Consulter les informations liées à la session de formation";
							    $btnHash=Yii::app()->baseUrl."/#listing-sessions?preview=".$params["collection"].".".$params["id"];
							}	
						    $user = PHDB::findOne($collec,array("_id"=>new MongoId($id)),array("email")); 
						    $emailUser=$user["email"];
						    $paramsmail = array(
							    "tpl" => "basic",
							    "tplObject" => $tplObject,
							    "tplMail" => $emailUser,
							    "html" => $html,
							    // "community" => <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" )
							    "btnRedirect" => array(
								    "hash" => $btnHash,
								    "label" => $btnLabel
							    )
						    );	
						    Mail::createAndSend($paramsmail);
					    }					
				      }
				    }	
					
				}	
			}
		}

		$user=Person::getById(Yii::app()->session['userId']);
		$costum = CacheHelper::getCostum();
		$source=PHDB::findOneById($params["collection"],$params["id"],array("source","reference"));
		$userOrgaAdmin=(isset($user["links"]) && isset($user["links"]["memberOf"]) && isset($user["links"]["memberOf"][$params["id"]]) && isset($user["links"]["memberOf"][$params["id"]]["isAdmin"])) ? true : false;
		if($params["collection"]==Organization::COLLECTION && !$userOrgaAdmin){	
		    $isAdmin=true;
		    Link::connect((string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "members", $isAdmin);
            Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf", $isAdmin);
        }

		

		
		// var_dump($source);exit;
		if($params["collection"]==organization::COLLECTION){
			// var_dump($params);exit;
			if(isset($params["params"]["address"]) && isset($params["params"]["address"]["level3Name"])){
				$network=PHDB::findOne(Organization::COLLECTION,array("network"=>"ANCTNetwork","address.level3Name"=>$params["params"]["address"]["level3Name"]));
				$child=[];		
				$child["childId"]=$params["id"];
				$child["childType"]=$params["collection"];
				$elt=PHDB::findOne($child["childType"],array("_id"=>New MongoId( $child["childId"])));
				if(!empty($network)){
					if(!isset($elt["links"]) || (isset($elt["links"]) && !isset($elt["links"]["memberOf"])) || (isset($elt["links"]) && isset($elt["links"]["memberOf"]) && !isset($elt["links"]["memberOf"][$network]["id"]))){
						Link::connect((string)$network["_id"], $network["collection"], $child["childId"], $child["childType"], Yii::app()->session["userId"], "members");
						Link::connect($child["childId"], $child["childType"], (string)$network["_id"], $network["collection"], Yii::app()->session["userId"], "memberOf");
					}
				}		
			}
			if((!isset($source["source"]) || (isset($source["source"]) && $source["source"]["key"]!="franceTierslieux")) && (!isset($source["reference"]) || 
		    (isset($source["reference"]) && isset($source["reference"]["costum"]) && array_search("franceTierslieux",$source["reference"]["costum"])===false))){
				Admin::addSourceInElement($params["id"],$params["collection"],"franceTierslieux","reference");
		    }

			$size="";
			$value=$params["params"]["buildingSurfaceArea"];
			preg_match_all("/\s/",$value,$space);

			if(intval($params["params"]["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($params["params"]["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else if(intval($params["params"]["buildingSurfaceArea"])>200){
				$size="Plus de 200m²";
			}
			if(!empty($size) && !isset($space[0][0])){
				$commonVal=[];

				if(isset($params["params"]["tags"])){
					$commonVal=array_intersect($params["params"]["tags"],["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]);
				}
				if(!empty($commonVal)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($params["id"])),
					array('$pull' => array("tags"=> array('$in'=>["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]))));
				}				
				PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($params["id"])),
					array('$push' => array("tags"=> $size )));
			}
		}

		if($params["collection"]==Organization::COLLECTION && (!isset($params["params"]["tags"]) || (isset($params["params"]["tags"]) && array_search("TiersLieux",$params["params"]["tags"])===false))){
			PHDB::update( $params["collection"], 
									  		array("_id"=>new MongoId($params["id"])),
				                        	array('$push' => array("tags"=> "TiersLieux")));

		}

		// if($params["collection"]==Person::COLLECTION){
		if((!isset($source["source"]) || (isset($source["source"]) && $source["source"]["key"]!=$costum["contextSlug"])) && (!isset($source["reference"]) || 
		    (isset($source["reference"]) && isset($source["reference"]["costum"]) && array_search($costum["contextSlug"],$source["reference"]["costum"])===false))){
			Admin::addSourceInElement($params["id"],$params["collection"],$costum["contextSlug"],"reference");
		}
		// }
		if($params["collection"]==Person::COLLECTION){
			// exit;
			// var_dump("afterupdaSav perosne");exit;
			
			// $res1=Link::connect($costum["contextId"], $costum["contextType"], $params["id"],$params["collection"], Yii::app()->session["userId"], "members");
            // $res2=Link::connect($params["id"],$params["collection"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
			// if($res1["result"] && $res2["result"]){
			$elt=PHDB::findOne($params["collection"],array("_id"=>New MongoId($params["id"])));	
			// var_dump($costum["contextId"],$elt["links"]["memberOf"][$costum["contextId"]]);exit;
			if(!isset($elt["links"]["memberOf"][$costum["contextId"]])){
				$res1=Link::connect($costum["contextId"], $costum["contextType"], $params["id"],$params["collection"], Yii::app()->session["userId"], "members");
                $res2=Link::connect($params["id"],$params["collection"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
                // var_dump($res1,$res2);exit;
			}
			if(isset($elt["links"]["memberOf"][$costum["contextId"]])){
				// $res1=Link::connect($costum["contextId"], $costum["contextType"], $params["id"],$params["collection"], Yii::app()->session["userId"], "members");
                // $res2=Link::connect($params["id"],$params["collection"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
				
				// var_dump($params["params"]["adherent"],$params["params"]["keyMoment"]);exit;
				$adherent=(!empty($params["params"]["adherent"])) ? (is_array($params["params"]["adherent"]) ? $params["params"]["adherent"] : [$params["params"]["adherent"]]) : [] ;
				$trainer=(isset($params["params"]["trainer"])  && $params["params"]["trainer"]=="true") ? ["Formateur.ice"] : [] ;
				$territorialCercle=(isset($params["params"]["territorialCercle"])) ? $params["params"]["territorialCercle"] : [] ;
                $newsletterSubscribtion=(isset($params["params"]["newsletterSubscription"]) && $params["params"]["newsletterSubscription"]=="true") ? ["Newsletter"] : [] ;
                $supportCercle=(isset($params["params"]["supportCercle"]) && $params["params"]["supportCercle"]=="true") ? ["Cercle des accompagnements"] : [] ;
                $actionContributor=(isset($params["params"]["actionContributor"]) && $params["params"]["actionContributor"]=="true") ? ["Contributeur.ices"] : [] ;
                $referentTerritorial=(isset($params["params"]["referentTerritorial"]) && $params["params"]["referentTerritorial"]=="true") ? ["Référent.e teritorial"] : [] ;
                // var_dump($adherent);exit;
                
				$potentialRoles=array_merge($adherent, $trainer, $territorialCercle,$newsletterSubscribtion,$supportCercle,$actionContributor,$referentTerritorial);
				// var_dump($potentialRoles);exit;
				
				$roles2Attribute=(isset($elt["links"]["memberOf"][$costum["contextId"]]["roles"])) ? $elt["links"]["memberOf"][$costum["contextId"]]["roles"] : [];
                // foreach($potentialRoles as $in=>$role){
				// 	if(!empty($params["params"][$role])){
				// 		$roles2Attribute[]=$role;
				// 	}
				// }

				$roles2Attribute=array_merge($roles2Attribute,$potentialRoles);
				$roles2Attribute=array_values(array_unique($roles2Attribute));

				// var_dump($roles2Attribute);exit;
				// $roles2Attribute=(isset($params["params"]["adherent"])) ? array_merge($params["params"]["adherent"],$roles2Attribute) : $roles2Attribute; 
                if(!empty($roles2Attribute)){
				    $resRoles = Link::removeRole($costum["contextId"], $costum["contextType"], $params["id"], $params["collection"], $roles2Attribute, Yii::app()->session['userId'], "members");
			    // var_dump($resRoles);exit;
				}
			}

			// connect person to attached 
			if(isset($params["params"]["linkedThirdPlace"])){
				$parentId = array_keys($params["params"]["linkedThirdPlace"])[0] ?? "";
				$parentType = $params["params"]["linkedThirdPlace"][$parentId]["type"] ?? "";
				
				$child["childId"]=$params["id"];
				$child["childType"]=$params["collection"];
				if(!empty($parentId) && !empty($parentType) && !isset($elt["links"]["memberOf"][$parentId])){
					// var_dump($params["params"]["linkedThirdPlace"]);exit;
					Link::connectParentToChild($parentId, $parentType, $child, false, Yii::app()->session["userId"], "");
				}
			}
			if(isset($params["params"]["otherLinkedStructure"])){

				$parentId = array_keys($params["params"]["otherLinkedStructure"])[0] ?? "";
				$$parentType = $params["params"]["otherLinkedStructure"][$parentId]["type"] ?? "";
				$child["childId"]=$params["id"];
				$child["childType"]=$params["collection"];
				if(!empty($parentId) && !empty($parentType) && !isset($elt["links"]["memberOf"][$parentId])){
					Link::connectParentToChild($parentId, $parentType, $child, false, Yii::app()->session["userId"], "");
				}	
			}


			
		}
		// var_dump("after update");exit;

		
	}

	public static function prepDataForUpdate($data){
		// var_dump($data["images"]);
		$typeEl=(in_array($data["collection"], [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($data["collection"]) : $data["collection"]; 
		  		$where=array(
		  			"id"=>(string)$data["_id"], "type"=>$typeEl, "doctype"=>"image", 
		  			"contentKey"=>"slider"
		  		);
		  		$data["photo"] = Document::getListDocumentsWhere($where, "image");

		// var_dump($data["images"]);exit; 	
		
		if($data["collection"]==Person::COLLECTION){
			if(!isset($data["email"])){
				$email=PHDB::findOne(Person::COLLECTION,array("_id"=>$data["_id"]),array("email","telephone","address","geo","geoPosition"));
				$data["email"]=$email["email"];
				if(isset($email["telephone"])){
					if(isset($email["telephone"]["mobile"])){
						$data["mobile"]=implode(",",$email["telephone"]["mobile"]);
					}
				}
				if(isset($email["address"])){
					$data["address"]=$email["address"];
					$data["geo"]=$email["geo"];
					$data["geoPosition"]=$email["geoPosition"];

				}
				
			}

			
		}

		return $data;  		

		  		
	}

	public static function elementBeforeSave($params){
		 
		$res=$params["elt"];
		if($params["collection"]==Person::COLLECTION){
		    $costum = CacheHelper::getCostum();
		    $elem=PHDB::findOneById($params["collection"],$params["id"]);
			
			
		    if(empty($elem)){
				// correspond à l'ajout par un admin
			    
				// if(isset($params["elt"]["inviteMail"])){
				// 	unset($params["elt"]["inviteMail"]);
				// }
				// $new=array("name"=>$params["elt"]["name"],"mail"=>$params["elt"]["email"],"profilThumbImageUrl" => "/assets/b62f46e1/images/thumb/default_citoyens.png");
			    // $list["invites"]=array();
			    // array_push($list["invites"],$new);
			    // $parent=array("parentType"=>$costum["contextType"],"parentId"=>$costum["contextId"]);
               
                //LIER et personnes au contexte et envoyé invitatoin personnalisée !!!!

				$newPerson = array(
					"name" => $params["elt"]["name"],
					"email" => $params["elt"]["email"],
					"invitedBy" => Yii::app()->session["userId"]
				);

				$person = Person::createAndInvite($newPerson, @$value["msg"]);
				// var_dump($person);exit;
				Mail::validatePerson($person["person"]);
				// if(!$person["result"]){
				// 	var_dump($params,$person);exit;
				// }
				$elem=($person["result"]==true) ? $person["person"] : $params["elt"];
				$elem["id"]=$person["id"] ?? null;;
				// var_dump($elem);exit;
				$params["elt"]["id"]=$elem["id"] ?? null;
				
				 

			    // $invit = Link::invite($list, $parent);
			    // $idElem=(isset($invit["invites"]) && isset($invit["invites"][0])) ? $invit["invites"][0]["newElement"]["id"] : null;
			    // $params["elt"]["id"]=$idElem;
			    $res = $params;
		    }
			
			// --- Condition de voir si jamais un contact déjà créer n'est pas liée à la CIE - A voir si ça arrive encore
			
			// if(!isset($elem["links"]) || (isset($elem["links"]) && !isset($elem["links"]["memberOf"][$costum["contextId"]]))){
			// 	// if(!isset($elem["email"])){
			// 	// 	var_dump($elem);exit;
			// 	// }
			// 	// var_dump($costum);exit;
			// 	$elem["id"]= $elem["id"] ?? (string)$elem["_id"] ?? null;
			// 	$isInviting=(!empty(Yii::app()->session["userId"])) ? true : false;
			// 	Link::connect($costum["contextId"], $costum["contextType"], $elem["id"], Person::COLLECTION, Yii::app()->session["userId"], "members", null, null, null,$isInviting);
            //     Link::connect($elem["id"], Person::COLLECTION, $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf",null, null, null,$isInviting);
			// 	$paramsmail = array(
			// 		"tpl" => "invitation",
			// 		"tplObject" => "Rejoignez la dynamique de la plateforme réseau pour faciliter nos interactions !" ,
			// 		"tplMail" => $elem["email"],
			// 		"msg" => "Vous avez été invité.e à rejoindre la plateforme numérique pour faire grandir nos liens !",
			// 		// "community" => <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" )
			// 		// "btnRedirect" => array(
			// 		// 	"hash" => "#panel.modal-account",
			// 		// 	"label" => "Accéder au récapitulatif"
			// 		// ),
			// 		"invitedUserId" => $elem["id"],
			// 		"target" => ["type"=>$costum["contextType"],"id"=>$costum["contextId"],"name"=>$costum["title"]],
			// 		"language" =>"FR",
			// 		"invitorUrl"=>"#page.type.citoyens.id".Yii::app()->session["userId"],
			// 		"invitorName" =>Yii::app()->session["user"]["name"] ?? null
			// 	);
			// 	// commented form import Cie
			// 	// var_dump($elem);exit;
			// 	$resMail=Mail::createAndSend($paramsmail);
			// }
			
			if(isset($params["elt"]["mobile"])){
				if(is_string($params["elt"]["mobile"])){
					$set = array("telephone.mobile" => [$params["elt"]["mobile"]]);
					$set["modified"] = new MongoDate(time());
					$set["updated"] = time();
				} 
				$resUpdate = PHDB::update( $params["collection"], array("_id" => new MongoId($params["id"])), 
			                          array('$set' => $set));

				// element::updateField($params["collection"], $params["id"], "mobile", $params["elt"]["mobile"]);
			}
	    }
		return $res;
	}

    public static function getNetwork($costumSlug){
		//var_dump($costumSlug);
		$network=PHDB::find(Organization::COLLECTION,array("category" => "network"));
		return $network;
	}
    public static function pdfContact($params) {
		$query = [];
		//var_dump($params["query"]);exit;
		$controller = $params["controller"];
		if(isset($params["slug"]))
			$query["tags"] = "Contact".$params["slug"];
		if(isset($params["query"])){
			foreach($params["query"] as $kQuery => $vQuery){
				$query[$kQuery] = array('$in' => explode(",",$vQuery));
			}
		}		
		$contactList = PHDB::find( Person::COLLECTION, $query);
		$paramsPdf=[         
			"contactList" => $contactList,
			"docName" => "Contact.pdf",
			"textShadow" => false,
			"orientation" => "L",
			"page" => $params["page"],
			"header" => false,
			"query" => $query,
			"footer" => false,
			"saveOption" => "D"
		];
		$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
		$paramsPdf["html"] = $controller->renderPartial("costum.views.custom.reseauTierslieux.pdf.allContact", $paramsPdf, true);
		$res = Pdf::createPdf($paramsPdf);

	}

	public static function prepImportData($data){
		if(empty($data["value"])){
			// var_dump($data);exit;
		}
		if($data["valueConfig"]["valueAttributeElt"]=="name"){
				// $firstName=$data["valueFile"][$data["valueConfig"]["idHeadCSV"]+1];
				// var_dump($data["value"]);exit;
				$data["element"]["surname"]=$data["value"];
				
			//    var_dump($valueData);exit;
		}else if($data["valueConfig"]["valueAttributeElt"]=="firstName"){
			$data["element"]["name"]=trim($data["element"]["name"]." ".$data["value"]);
		}
		
		if(!isset($data["element"]["tagsCie"])){
				$data["element"]["tagsCie"]=[];
		}else if(!in_array("ContactlaCompagnieDesTierslieux",$data["element"]["tagsCie"])){
			$data["element"]["tagsCie"][]="ContactlaCompagnieDesTierslieux";
		}

		// var_dump($element);
                                

                               
        //                         // var_dump($value["valueAttributeElt"]);
        //                         if(in_array($value["valueAttributeElt"],["tags","roles"])){
        //                             $specData=explode(",",$valueData);
                                    
        //                             foreach($specData as $val){
        //                                 if(!isset($element[$value["valueAttributeElt"]])){
        //                                     $element[$value["valueAttributeElt"]]=[];
        //                                 }
        //                                 if(!empty($val)){
        //                                     array_push($element[$value["valueAttributeElt"]],trim($val));
        //                                 }
        //                                 $valueData=trim($val);    
        //                                 // $i++;
        //                                 // var_dump($element);
        //                                 // exit;
        //                             }
                                    
        //                         // }else if(in_array($value["valueAttributeElt"],["linkedStructure"])){
        //                         //     $query=SearchNew::searchText($valueData,[]);
                                    

        //                         //     $linkedStructure=PHDB::find(Organization::COLLECTION,$query,array("_id","name","collection","slug"));
        //                         //     if(count($linkedStructure)==1){
        //                         //         $id=array_keys($linkedStructure)[0];
        //                         //         $structure=[];
        //                         //         $structure[$id]=array("type"=>$linkedStructure[$id]["collection"],"name"=>$linkedStructure[$id]["name"]);
        //                         //     }
        //                         //     // var_dump($linkedStructure);

        //                         }else if(str_contains($value["valueAttributeElt"],"links.")){
        //                             $searchEl=explode(".",$value["valueAttributeElt"]);
        //                             // var_dump($searchEl);exit;
        //                             $collecSearchEl=$searchEl[1];

        //                             $multiElements=explode(",",$valueData);
        //                             foreach($multiElements as $nameElem){
        //                                 $res=self::getLinksImport($nameElem,$collecSearchEl,$elem,$searchEl,$value,$element);
        //                                 // var_dump($res);
        //                                  $elem=array_merge($elem,$res["elem"]);
        //                                  $element=array_merge($element,$res["element"]);
        //                                 //  $element[$searchEl[0]]=array_merge($element[$searchEl[0]],$res["element"][$searchEl[0]]);
        //                                 //  $element["msgError"]=$res["element"]["msgError"];
        //                             }
                                    
        //                             if($value["idHeadCSV"]=="supportedStructures"){
        //                                 // var_dump($element);exit;
                            
        //                             } 

                                    
                                    
                                

        //                         }
          
		// var_dump($data["element"]);exit;
		return $data["element"];
	}

	public static function prepData($params){
		// var_dump($params);exit;
		if(($params["collection"]==Project::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="formation")||($params["collection"]==Event::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="sessionFormation")){
			$params["links"]=(isset($params["links"])) ? $params["links"] : [];
			// if(isset($params["trainer"])){
			// 	$params["links"][Person::COLLECTION]=$params["trainer"];
			// 	// foreach($params["trainer"] as $id=>$val){
			// 	// 	"childId" => $id,
			// 	// 	"childType" => $val["type"],
			// 	// 	// "childName" => $value["name"],
			// 	// 	// "roles" => (empty($value["roles"]) ? array() : $value["roles"]),
			// 	// 	"connectType" => "admin"

			// 	// }
			// 	// =array_merge($params["links"],$params["trainer"]);
			// }
			// if(isset($params["partner"])){
			// 	$params["links"][organization::COLLECTION]=$params["partner"];
			// }
			// var_dump($params);exit;
		}
		if($params["collection"]==Person::COLLECTION){
			if(isset($params["invitedBy"])){
				unset($params["invitedBy"]);
			}
		}
		return $params;
	}

	public static function addDataInDb($post){
        $jsonString = $post["file"];
        $typeElement = $post["typeElement"];
        /*$pathFolderImage = $post["pathFolderImage"];
        $sendMail = ($post["sendMail"] == "false"?null:true);
        $isKissKiss = ($post["isKissKiss"] == "false"?null:true);
        $invitorUrl = (trim($post["invitorUrl"]) == ""?null:$post["invitorUrl"]);*/
        
        if(substr($jsonString, 0,1) == "{")
            $jsonArray[] = json_decode($jsonString, true) ;
        else
            $jsonArray = json_decode($jsonString, true) ;
        if(isset($jsonArray)){
            $resData =  array();
            foreach ($jsonArray as $key => $value){
                try{
                    if($typeElement == City::COLLECTION){
                        $exist = City::alreadyExists($value, $typeElement);
                        if(!$exist["result"]) {
                            $res = City::insert($value, Yii::app()->session["userId"]);
                            $element["name"] = $value["name"];
                            $element["info"] = $res["msg"];
                        }else{
                            $element["name"] = $exist["city"]["name"];
                            $element["info"] = "La ville existe déjà";
                        }
                    }else{


						if( (isset($value["address"]) && !empty($value["address"])) || $typeElement==Person::COLLECTION ) {
							$good = true ;
							if(isset($value["address"]["osmID"]) && !empty($value["address"]["osmID"])){
								$city = City::getByOsmId($value["address"]["osmID"]);
                                // $city = (isset($value["address"]["postalCode"]) && isset($value["address"]["addressCountry"])) ? City::getByPostalCode($value["address"]["postalCode"], $value["address"]["addressCountry"]) : $city ;

								if(!empty($city)){
									$value["address"] = self::getAddressConform($city, $value["address"]);
									$resGeo = self::getLatLonBySIG($value["address"]);
									if(!empty($resGeo)){
										$value["geo"] = SIG::getFormatGeo($resGeo["lat"], $resGeo["lon"]);
										$value["geoPosition"] = SIG::getFormatGeoPosition($resGeo["lat"], $resGeo["lon"]);
									}

                                    if(!empty($value["geo"])){
                                        if(gettype($value["geo"]["latitude"]) != "string" )
                                            $value["geo"]["latitude"] = strval($value["geo"]["latitude"]);
                                        if(gettype($value["geo"]["longitude"]) != "string" )
                                            $value["geo"]["longitude"] = strval($value["geo"]["longitude"]);
                                    }
								}
								else{
                                    // var_dump($value);exit;
									$good = false ;
									$element["name"] = $value["name"];
									$element["info"] = "La commune n'existe pas, pensez à l'ajouter avant"; 
								}

                            }
                            if($typeElement==Person::COLLECTION){
                                $value["geoCie"]=$value["geo"] ?? null;
                                $value["geoPositionCie"]=$value["geoPosition"] ?? null;
                                $value["addressCie"]=$value["address"] ?? null;
                                unset($value["geo"]);
                                unset($value["geoPosition"]);
                                unset($value["address"]);
                            }
                            if($good == true){
                                $updateProcess=true;
                                $exist = Element::alreadyExists($value, $typeElement);
                                // var_dump($exist);exit;
                                if(!$exist["result"]){
                                    if(!empty($post["isLink"]) && $post["isLink"] == "true"){
                                        if($post["typeLink"] == Event::COLLECTION && $typeElement == Event::COLLECTION){
											$value["parentId"] = $post["idLink"];
											$value["parentType"] = $post["typeLink"];
                                        }
                                        else{
											$paramsLink["idLink"] = $post["idLink"];
											$paramsLink["typeLink"] = $post["typeLink"];
											$paramsLink["role"] = $post["roleLink"];
                                        }
                                        
                                    }
                                    if(!empty($value["urlImg"])){
                                        $paramsImg["url"] =$value["urlImg"];
                                        $paramsImg["module"] = "communecter";
                                        $split = explode("/", $value["urlImg"]);
                                        $paramsImg["name"] = $split[count($split)-1];
                                        unset($value["urlImg"]);
                                    }
                                    if(!empty($value["startDate"])){
                                        $startDate = DateTime::createFromFormat('Y-m-d H:i:s', $value["startDate"]);
                                        $value["startDate"] = $startDate->format('d/m/Y H:i');
                                    }
                                    if(!empty($value["endDate"])){
                                        $endDate = DateTime::createFromFormat('Y-m-d H:i:s', $value["endDate"]);
                                        $value["endDate"] = $endDate->format('d/m/Y H:i');
                                    }
                                    $value["collection"] = $typeElement ;
                                    $value["key"] = Element::getControlerByCollection($typeElement);
                                    $value["paramsImport"] = array( "link" => (empty($paramsLink)?null:$paramsLink),
                                                                    "img" => (empty($paramsImg)?null:$paramsImg ),
                                                                    // "links" => (empty($paramsLinks)? null : $paramsLinks)
                                                            );
                                    if($typeElement!==Person::COLLECTION){
                                        $value["preferences"] = array(  "isOpenData"=>true, 
                                                                    "isOpenEdition"=>true);

                                    }                        
                                    if($typeElement == Organization::COLLECTION)
                                        $value["role"] = "creator";
                                    if($typeElement == Event::COLLECTION && empty($value["organizerType"]))
                                        $value["organizerType"] = Event::NO_ORGANISER;
                                    
                                    if(!empty($value["organizerId"])){
                                        $eltSimple = Element::getElementSimpleById($value["organizerId"], @$value["organizerType"]);
                                        if(empty($eltSimple)){
                                            unset($value["organizerId"]);
                                            if(!empty($value["organizerType"])) 
                                                $value["organizerType"] = Event::NO_ORGANISER;
                                        }
                                    }
                                    $element = array();
                                    $res = Element::save($value);
                                    $element["name"] =  $value["name"];
                                    $element["info"] = $res["msg"];
                                    $element["type"] = $typeElement;
                                    if(!empty($res["id"])){
                                        $element["url"] = "/#page.type.".$typeElement.".id.".$res["id"] ;
                                        $element["id"] = $res["id"] ;
                                    }
                                    
                                } else {
                                    if(count($exist["element"])>1){
                                        // var_dump($exist,$exist["where"]['$or']);exit;
                                        $element["info"]="Plusieurs éléments au nom très proche";
                                        $element["type"] = $typeElement ;
                                        $element["url"]= "";
                                        $element["name"] = "";
                                        $element["email"] = "";                              
                                        $element["id"] ="";
                                        foreach($exist["element"] as $kd => $vd){
                                            $ld='#page.type.'.$vd["collection"].'.id.'.$kd;
                                            $element["url"]= (empty($element["url"])) ? $ld : $element["url"].", ".$ld;
                                            $element["name"] = (empty($element["name"])) ? $vd["name"] : $element["name"].", ".$vd["name"];
                                            $element["email"] = (empty($element["email"])) ? $vd["email"] : $element["email"].", ".$vd["email"];                                            
                                            $element["id"] = (empty($element["id"])) ? $kd : $element["id"].", ".$kd ;
                                            // $element["url"] = 

                                        }
                                    }else{
                                    $keyId=array_keys($exist["element"])[0];
                                    if(isset($exist["element"][$keyId]["collection"])==Person::COLLECTION){
                                        if($value["source"]){
                                            if(isset($exist["element"][$keyId]["reference"]["costum"])){
                                                array_push($exist["element"][$keyId]["reference"]["costum"], $value["source"]); 
                                            }else{
                                                $exist["element"][$keyId]["reference"]=["costum"=>[$value["source"]]];                                       
                                            }
                                            unset($value["source"]);
                                        }
                                        if($value["name"]){
                                            unset($value["name"]);
                                        }
                                        if($value["email"]){
                                            unset($value["email"]);
                                        }
                                        $value["id"]=$keyId;
                                        $value["collection"]=$exist["element"][$keyId]["collection"];
                                        if(!empty($value["tags"]) && !empty($exist["element"][$keyId]["tags"])){
                                            $value["tags"]=array_merge($value["tags"],$exist["element"][$keyId]["tags"]);
                                        }
                                        // $exist["element"][$keyId]["id"]=$keyId;
                                        // $mergeElem=array_merge_recursive($exist["element"][$keyId],$value);
                                        // $mergeElem=array_unique($mergeElem);
                                    // $newElem=[];
                                    //     array_walk($mergeElem, function(&$v) {
                                    //         $newElem[$v] = array_map('array_unique', $v);
                                    //     });
                                        // var_dump($value,$exist["element"][$keyId]);exit;
                                        Element::save($value);
                                    }
                                    $element["name"] = $exist["element"][$keyId]["name"];
                                    $element["info"] = "L'élément existe déjà";
                                    $element["url"] = "/#page.type.".$typeElement.".id.".(String)$exist["element"][$keyId]["_id"] ;
                                    $element["type"] = $typeElement ;
                                    $element["id"] = (String)$exist["element"][$keyId]["_id"] ;
                                    $element["data"] = [
                                        "former" => $exist["element"][$keyId],
                                        "new" => $value
                                    ];
                                    }
                                }
                            }
                        }else{
                            $element["name"] = @$value["name"];
                            if($typeElement !== Person::COLLECTION)
                                $element["info"] = "L'élément n'a pas d'adresse.";  
                        }
                    }
                }
                catch (CTKException $e){
                    $element["name"] =  $value["name"];
                    $element["info"] = $e->getMessage();
                }
                $resData[] = $element;     
            }
            $params = array("result" => true, 
                            "resData" => $resData);
        }
        else
        {
            $params = array("result" => false); 
        }
      
        return $params;
    }

}
