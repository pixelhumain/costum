<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;

class FranceTierslieux {


	
	// public static function csvElement($params){
	// 	if(empty($costum)){
	// 		$costum=CacheHelper::getCostum();
	// 		if (isset($params["fields"])) {
	// 			foreach($costum["lists"] as $e=>$v) {
	// 				$constructDataForEdit = [];
	// 				foreach($v as $i=>$tag) {
	// 					if (in_array($tag,$params["fields"]["tags"])!==false) {
	// 						$constructDataForEdit.push($tag);
	// 				array_splice(		$params["fields"]["tags"](data.map.tags.indexOf(tag), 1);
	// 					}
	// 				}
	// 				data.map[e] = constructDataForEdit;
	// 			});
	// 		}
	// 	}
		
	// }

	public static function orgaRules($field,$value,$costum=null){
		if(empty($costum)){
			$costum=CacheHelper::getCostum();
		}
		if($field=="manageModel")
		    //On vérifie si la valeur 
			foreach($value as $ind=>$tag){
				$pos=array_search($tag,$costum["lists"][$field]);
				if($pos==false){
					unset($value[$ind]);	
				}
			}
		    if(is_array($value) && sizeof($value)>1){
			// on prend la dernière valeur si plusieurs
			$value=array($value[sizeof($value)-1]);
			// voir in_array pour valeur non conforme
		}
		return $value;
	}
	public static function answerRules($inputKey,$value){
		// bénévoles
		// var_dump($inputKey);exit;

		//ETP gestion lieu
		// all number inputs
		$nbInputs=[
			"franceTierslieux1522023_1549_3le5si0qhpywv18p94ne",
			"franceTierslieux1522023_1549_3le5si0qhkwg9705sfr",
			"franceTierslieux1822023_1721_4lea64x9vdukt4t2p92",
			"franceTierslieux1822023_1721_4lf2b0atwtu662i9uor",
			"franceTierslieux1822023_1721_4lea64xa0lop8kirjb8",
			"franceTierslieux1922023_1231_5lebb61gnmph1vnin1a",
			"franceTierslieux1922023_1231_5lebb61gosevh7nembco",
			"franceTierslieux1922023_1231_5lebb61goil3uftiqkwo",
			"franceTierslieux1922023_1231_5lebb61go0yi3ov9tzuve",
			"franceTierslieux1922023_1231_6lebdout8uyagpr9ehyg",
			"franceTierslieux1922023_1231_6lebdout9qov9grn8e08",
			"franceTierslieux1922023_1231_6leh2y401jm987cubh8a",
			"franceTierslieux1922023_1231_6lebdoutagu828mxc7dd",
			"franceTierslieux1922023_1231_6lebdoutam5ehxcno6gm",
			"franceTierslieux1922023_1417_7lf3yrvyxn59y74gq51i",
			"franceTierslieux1922023_1417_7lebeyrj9zthl8xraoim",
			"franceTierslieux2022023_732_9lecfwuxw4ol3ib2zcyc",
			"franceTierslieux2022023_732_9lecfwuxx9q1jro8u6xf",
			"franceTierslieux2022023_732_9lecfwuxx2a0p6im4uhn",
			"franceTierslieux2022023_732_9lecfwuxz95rar0lzc65",
			"franceTierslieux2022023_732_9lecfwuxzsgkob33wl6d"
		];
		if(in_array($inputKey,$nbInputs)!==false){
			$value=str_replace(",",".",$value);
			$value=floatval($value);
			$value=abs($value);
		}

		

		if(str_contains($inputKey,"franceTierslieux1822023_1721_4lea64x9zefdbjqbhl7l")){
			// var_dump($value);exit;
			$length=count(array_keys($value));
			$indexLoyer=-1;
			$loyer="";
			foreach($value as $ind=>$opt){
				
				if(isset($value[$ind]["Loyer / Participation aux frais"]) && isset($value[$ind]["Loyer / Participation aux frais"]["textsup"])){
					$indexLoyer=$ind;
					$loyer=$value[$ind]["Loyer / Participation aux frais"]["textsup"];
				   break;
				}
			}
			
			if(!preg_match('~[0-9]+~', $loyer)){
				// var_dump($loyer);exit;
				$loyer="";
			}
		
			// $correc=[];
			
		
			if(!empty($loyer)){
				// var_dump($loyer);exit;
				// var_dump(preg_match_all("/[0-9]+/",$val,$out));
				
				// $correc[$ind]=[];
				
				if(preg_match('(m2|mètre carré|m2/mois|m²)', $loyer) === 1){
					$meter=["m2","mètre carré","m2/mois","m²"];
					foreach($meter as $mind => $m){
						if(str_contains($loyer,$m)){
							$sub=explode($m,$loyer);
						//    var_dump($sub);
							preg_match("/[0-9]+/",$sub[0],$out);
							if(preg_match("/[0-9]+/",$sub[0],$out)!==0){
								// var_dump($out[0]);
								 $len=sizeof($out[0]);
								$area=15;
								$coef=1;
								// $cost=$out[0][$len-1];
								$cost=$out[0];
								$month=12;
		
								if($cost<=20){
									$coef==1;
		
								}elseif($cost>20){
									$coef=1/12;
								}
								$loyer=$out[0][$len-1];
								// $correc[$ind]["coût m2"]=$cost;
								// $correc[$ind]["coût mois"]=$cost*$area*$coef;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(mois|MOIS|/m)', $loyer) === 1) { 
					$mois=["mois","MOIS","m"];
					foreach($mois as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							preg_match_all("/[0-9]+/",$sub[0],$out);
							if(preg_match_all("/[0-9]+/",$sub[0],$out)!==0){
								$len=sizeof($out[0]);
								$loyer=$out[0][$len-1];
								// $correc[$ind]["coût mois"]=$out[0][$len-1];
								// // $correc[$ind]["coût mois"]=$out[0][$len-1]*30;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(jour|journée|/j)', $loyer) === 1) { 
					// var_dump($loyer);exit;
					$day=["jour","journée","/j"];
					foreach($day as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							preg_match_all("/[0-9]+/",$sub[0],$out);
							if(preg_match_all("/[0-9]+/",$sub[0],$out)!==0){
								$len=sizeof($out[0]);
								$loyer=$out[0][$len-1];

								$loyer=$loyer*20;
								// var_dump($loyer);exit;
								// $correc[$ind]["coût jour"]=$out[0][$len-1];
								// $correc[$ind]["coût mois"]=$out[0][$len-1]*30;
								// $correc[$ind]["original"]=$value;
								// var_dump($correc[$ind]);
							}    
						}
					}
				}else if(preg_match('(heure|/h|/H)', $loyer) === 1) { 
					$hour=["heure","/h","/H"];
					foreach($hour as $jind => $j){
						if(str_contains($loyer,$j)){
							$sub=explode($j,$loyer);
						   // var_dump($sub);
							// preg_match("[+-]?[0-9]+(\.[0-9]+)?([Ee][+-]?[0-9]+)?",$sub[0],$out);
                            $preg = preg_match("[+-]?[0-9]+(\.[0-9]+)?([Ee][+-]?[0-9]+)?",$sub[0],$out);
                            if($preg !== false && $preg !==0) {
                                $len=sizeof($out[0]);
                                $loyer=$out[0][$len-1];
                                
                                $loyer=$loyer*7*20;
                                // var_dump($ind, $out[0][$len-1]);
                                // $correc[$ind]["coût heure"]=$out[0][$len-1];
                                // $correc[$ind]["coût mois"]=$out[0][$len-1]*7*30;
                                // $correc[$ind]["original"]=$value;
                                // var_dump($correc[$ind]);
                            }
						}
					}
				}else if(preg_match_all("/[0-9]+/",$loyer,$out)){
					if($loyer!="100"){
					// var_dump($loyer);exit;
					}
					$len=sizeof($out[0]);
					$loyer=$out[0][$len-1];
					if($loyer<=100){
						$loyer=$loyer*20;

					}
					// $match[$ind]=array($out[0],$value[$ind]);
					// array_push($match,$out);
		
				}else if(preg_match("/[0-9]+/",$loyer,$out)!==1){
				    //   var_dump($loyer);exit;
				}

				
				
			    
				// var_dump($value);exit;
			}
			if($indexLoyer>-1){ 
			    $value[$indexLoyer]["Loyer / Participation aux frais"]["textsup"]=$loyer;
			}	
			// var_dump($value);exit;
		}

		if(is_float($value)){
			$value=strval($value);
		    $value=str_replace(".",",",$value);
		}

		
		
		return $value;
		
	}
	
	public static function getSortedZone(){
		$orderBy = array("countryCode" => 1, "name" => 1);

		return $orderBy;
	}

	public static function getNetwork($costumSlug){
		//var_dump($costumSlug);
		$network=PHDB::find(Organization::COLLECTION,array("network"=>"ANCTNetwork"));
		return $network;
	}

	public static function referenceFromFtl($costumSlug){
		$elements=PHDB::findByIds(Organization::COLLECTION, $ids);			

				 if(!empty($elements)){
				 	$countRef=0;
				 	$countNew=0;
				 	foreach($elements as $data){
				 			$newreference = array();
				 			if(!empty($data["reference"])){	 				
								$newreference["costum"] = $data["reference"]["costum"];
									array_push($newreference["costum"],"franceTierslieux");
									try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array(	"reference.costum" => $newreference["costum"])));
									} catch (MongoWriteConcernException $e) {
										echo("Erreur à la mise à jour de la référence existante de ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countRef++;

				 				
				 			}
				 			else{
				 				try {
										$res = PHDB::update( Organization::COLLECTION, 
									  		array("_id"=>new MongoId($data["_id"])),
				                        	array('$set' => array("reference.costum"=> array("franceTierslieux"))));
										$countNew++;
									} catch (MongoWriteConcernException $e) {
										echo("Erreur de la création de la référence à jour de l'élément ".Organization::COLLECTION." avec l'id ".$data);
										die();
									}
				 					$countNew++;
				 			}
				 	}
				 	echo $countNew ." tiers-lieux avec nouvelle référence !";
				 	echo "\n".$countRef." tiers-lieux mis a jour avec référence en plus !" ;
				}
	}


	public static function elementAfterUpdate($params){
		// var_dump($params);exit;
		$elt=Element::getElementById($params["id"], $params["collection"]);
		$user=Person::getById(Yii::app()->session['userId']);
		$userOrgaAdmin=(isset($user["links"]) && isset($user["links"]["memberOf"]) && isset($user["links"]["memberOf"][$params["id"]]) && isset($user["links"]["memberOf"][$params["id"]]["isAdmin"])) ? true : false;
		if($params["collection"]==Organization::COLLECTION && !$userOrgaAdmin){
		    $isAdmin=true;
		    Link::connect((string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "members", $isAdmin);
            Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Organization::COLLECTION, Yii::app()->session["userId"], "memberOf", $isAdmin);
        }

		$child=[];
		
	    $child["childId"]=$params["id"];
        $child["childType"]=$params["collection"];
		if(isset($params["params"]["address"]) && isset($params["params"]["address"]["level3Name"])){
			$network=PHDB::findOne(Organization::COLLECTION,array("network"=>"ANCTNetwork","address.level3Name"=>$params["params"]["address"]["level3Name"]));
			// var_dump($network);exit;
			if(!empty($network)){
			    if(!isset($elt["links"]) || (isset($elt["links"]) && !isset($elt["links"]["memberOf"])) || (isset($elt["links"]) && isset($elt["links"]["memberOf"]) && !isset($elt["links"]["memberOf"][$network]["id"]))){
				    Link::connect((string)$network["_id"], $network["collection"], $child["childId"], $child["childType"], Yii::app()->session["userId"], "members");
                    Link::connect($child["childId"], $child["childType"], (string)$network["_id"], $network["collection"], Yii::app()->session["userId"], "memberOf");
			    }
				if(!isset($elt["reference"]) || (isset($elt["reference"]) && isset($elt["reference"]["costum"]) && array_search($network["slug"],$elt["reference"]["costum"])===false)){
					Admin::addSourceInElement($params["id"],$params["collection"],$network["slug"],"reference");
				}	
			}			
		}

		if((!isset($elt["source"]) || (isset($elt["source"]) && $elt["source"]["key"]!="franceTierslieux")) && (!isset($elt["reference"]) || 
		    (isset($elt["reference"]) && isset($elt["reference"]["costum"]) && array_search("franceTierslieux",$elt["reference"]["costum"])===false))){
				Admin::addSourceInElement($params["id"],$params["collection"],"franceTierslieux","reference");

		}
		if($params["collection"]==Organization::COLLECTION && (!isset($elt["tags"]) || (isset($elt["tags"]) && array_search("TiersLieux",$elt["tags"])===false))){
			PHDB::update( $params["collection"], 
									  		array("_id"=>new MongoId($params["id"])),
				                        	array('$push' => array("tags"=> "TiersLieux")));

		}

		if($params["collection"]==Organization::COLLECTION){
			$size="";
			$value=$params["params"]["buildingSurfaceArea"];
			preg_match_all("/\s/",$value,$space);

			if(intval($params["params"]["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($params["params"]["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else if(intval($params["params"]["buildingSurfaceArea"])>200){
				$size="Plus de 200m²";
			}
			if(!empty($size) && !isset($space[0][0])){
				$commonVal=[];

				if(isset($params["params"]["tags"])){
					$commonVal=array_intersect($params["params"]["tags"],["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]);
				}
				if(!empty($commonVal)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($params["id"])),
					array('$pull' => array("tags"=> array('$in'=>["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]))));
				}				
				PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($params["id"])),
					array('$push' => array("tags"=> $size )));
			}
                
			if(!isset($params["params"]["address"]["inseeDensity"])){
				self::addInseeDensityToAddress($params["id"],$params["params"]);
			}
		}


	}

	public static function prepDataForUpdate($data){
		// var_dump($data["images"]);
		$typeEl=(in_array($data["collection"], [Event::CONTROLLER, Project::CONTROLLER, Organization::CONTROLLER])) ? Element::getCollectionByControler($data["collection"]) : $data["collection"]; 
		  		$where=array(
		  			"id"=>(string)$data["_id"], "type"=>$typeEl, "doctype"=>"image", 
		  			"contentKey"=>"slider"
		  		);
		  		$data["photo"] = Document::getListDocumentsWhere($where, "image");

		// var_dump($data["images"]);exit; 		

		return $data;  		

		  		
	}			

	public static function elementAfterSave($data){
        
		
    	 $elt=Element::getElementById($data["id"], $data["collection"]);
    	 $isInviting=false;
    	if($data["collection"]==Organization::COLLECTION && isset($elt["category"]) && $elt["category"]=="network"){ 

	    	$where=array("tags"=>$elt["name"]);
	    	$community=PHDB::find(Organization::COLLECTION,$where);
	        
	        if($data["collection"]==Organization::COLLECTION && isset($community)){
	        	foreach ($community as $key => $val){
	        			Link::connect($data["id"],$data["collection"],$val["_id"],$val["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        			Link::connect($val["_id"],$val["collection"],$data["id"],$data["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
	        		
				}

			
	        }
	    } 
	    else if($data["collection"]==Organization::COLLECTION){ 
			// $mappingLevel3ToNetwork=[];
			// $networks=PHDB::find(Organization::COLLECTION,array("network"=>"ANCTNetwork"),array("address.level3"));
			// foreach($networks as $id=>$net){
			// 	$mappingLevel3ToNetwork[$net["address.level3"]]=$net["slug"];
			// }

			// if(!empty($data["params"]["address"]) && isset($data["params"]["address"]["level3"])){
				

			// }

	    	$costum = CacheHelper::getCostum();
			// $networks=$costum["lists"]["network"];
	    	$isInviting=false;
	    	$child=[];
	    	$child["childId"]=$elt["_id"];
        	$child["childType"]=$elt["collection"];
			if(isset($elt["address"]) && isset($elt["address"]["level3Name"])){
				$network=PHDB::findOne(Organization::COLLECTION,array("network"=>"ANCTNetwork","address.level3Name"=>$elt["address"]["level3Name"]));
                    Link::connect((string)$network["_id"], $network["collection"], $child["childId"], $child["childType"], Yii::app()->session["userId"], "members");
                    Link::connect($child["childId"], $child["childType"], (string)$network["_id"], $network["collection"], Yii::app()->session["userId"], "memberOf");
				    Admin::addSourceInElement($data["id"],$data["collection"],$network["slug"],"reference");
			} 

			Admin::addSourceInElement($data["id"],$data["collection"],"tiersLieux","reference");
	    	//$network=PHDB::find(Organization::COLLECTION,$where);
	        // foreach($elt["tags"] as $tag){
	        // 	if(in_array($tag, $networks)){
	        // 		$where=array("name"=>$tag);
	        // 		$net=Element::getElementByWhere(Organization::COLLECTION,$where);
	        // 		if($net){
	        // 		// Link::connectParentToChild($net["_id"],$net["collection"],$child,false,Yii::app()->session["userId"]);
		    //     		Link::connect($net["_id"],$net["collection"],$elt["_id"],$elt["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		    //     		Link::connect($elt["_id"],$elt["collection"],$net["_id"],$net["collection"],Yii::app()->session["userId"],"members",false,false,false,$isInviting);
		    //     	}	
		    //     }
	        // }

			// size of third place from absolute to filter criterias in tags
			$size="";
			$value=$data["params"]["buildingSurfaceArea"];
			preg_match_all("/\s/",$value,$space);

			if(intval($data["params"]["buildingSurfaceArea"])<60){
				$size="Moins de 60m²";
			}else if(intval($data["params"]["buildingSurfaceArea"])<=200){
				$size="Entre 60 et 200m²";
			}else if(intval($data["params"]["buildingSurfaceArea"])>200){
				$size="Plus de 200m²";
			}
			if(!empty($size) && !isset($space[0][0])){
				$commonVal=[];

				if(isset($data["params"]["tags"])){
					$commonVal=array_intersect($data["params"]["tags"],["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]);
				}
				if(!empty($commonVal)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$pull' => array("tags"=> array('$in'=>["Moins de 60m²","Entre 60 et 200m²","Plus de 200m²"]))));
				}				
				PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$push' => array("tags"=> $size )));
			}  

			if(!isset($data["params"]["address"]["inseeDensity"])){
				self::addInseeDensityToAddress($data["id"],$data["params"]);
			}

			// set idCartoTL single id for third places in map
			if(empty($elt["idCartoTL"])){
				$idsRec=PHDB::findAndSort(Organization::COLLECTION,array("idCartoTL"=>array('$exists'=>1)),array("idCartoTL"=>1));
				
				$nextId=(end($idsRec)["idCartoTL"]+1) ?? null;
				if(!empty($nextId)){
					PHDB::update( Organization::COLLECTION, 
					array("_id"=>new MongoId($data["id"])),
					array('$set' => array("idCartoTL"=> $nextId )));
				}
				// var_dump(count($idsRec),array_keys($idsRec)[count($idsRec)-1],end($idsRec)["idCartoTL"],$nextId,$idsRec);exit;
			}	
	    }

    
    }

	public static function getInputsOrElement ($collection="", $form="", $field=[]) {
		$results = [];
		if ($collection == Form::COLLECTION) {
			$getForm =  PHDB::findOneById($collection,$form) ?? [];		
			if (isset($getForm["subForms"])) {
				foreach ($getForm["subForms"] as $subFormsKey => $subFormsValue) {
					$getOneStepOrElement = PHDB::findOne($collection, array("id" => $subFormsValue)) ?? [];
					$idStep = (string) $getOneStepOrElement["_id"];
					$results[$subFormsValue] = array($idStep => $getOneStepOrElement);
				}
			}
		} else {
			$results = PHDB::findOneById($collection, $form, $field) ?? [];
		}
		return $results;
	} 

    public static function globalAutocomplete($form, $searchParams){
		$form = PHDB::findOneById(Form::COLLECTION,$form);
		$searchParams["indexMin"] = (isset($searchParams["indexMin"])) ? $searchParams["indexMin"] : 0;
		$searchParams["indexStep"] = (isset($searchParams["indexStep"])) ? $searchParams["indexStep"] : 100;
		$query = array();


		if(!empty($searchParams["userId"]))
			$query = SearchNew::addQuery( $query , array("user" => $searchParams["userId"] ));

		if(!empty($searchParams["name"])){
			if(!empty($searchParams["textPath"]))
				$query = SearchNew::searchText($searchParams["name"], $query, array("textPath"=>$searchParams["textPath"]));
			else
				$query = SearchNew::searchText($searchParams["name"], $query);
		}

		if(!empty($searchParams['filters'])){
			$query = SearchNew::searchFilters($searchParams['filters'], $query);
		}

		if(!empty($searchParams['sortBy'])){
			if(Api::isAssociativeArray($searchParams['sortBy'])){
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$key] = (int)$value;
				}
				$searchParams["sortBy"] = $sortBy;
			}else{
				$sortBy = array();
				foreach ($searchParams["sortBy"] as $key => $value) {
					$sortBy[$value] = 1;
				}
				$searchParams["sortBy"] = $sortBy;
			}
		}else
			$searchParams['sortBy'] = array("updated"=> -1);

		if(!isset($searchParams['fields']))
			$searchParams['fields'] = [];

		$res= array();
		if(is_string($searchParams["fields"])) $searchParams["fields"] = [];
		if(!empty($searchParams["indexStep"]) && $searchParams["indexStep"] == 30){
			$res["results"] = PHDB::findAndSort($searchParams["searchType"][0],$query,$searchParams['sortBy'], 0,  $searchParams["fields"]);
			if(isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
		}else{
			$res["results"] = PHDB::findAndFieldsAndSortAndLimitAndIndex($searchParams["searchType"][0],$query, $searchParams["fields"],$searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
			if(isset($searchParams["count"]))
				$res["count"][$searchParams["searchType"][0]] = PHDB::count( $searchParams["searchType"][0] ,$query);
		} 

		if (isset($searchParams["implRegion"]) && gettype($searchParams["implRegion"]) == "string" && $searchParams["implRegion"] == "true") {
			$query = array("what" => "Réseau de tiers-lieux");
			if(!empty($searchParams["name"])){
				if(!empty($searchParams["textPath"]))
					$query = SearchNew::searchText($searchParams["name"], $query, array("textPath"=>$searchParams["textPath"]));
				else
					$query = SearchNew::searchText($searchParams["name"], $query);
			}
			$listOfImplRegion = PHDB::find(Form::COLLECTION, $query, array("name" => 1, "_id" => 0));
			$networkArray = [];
			foreach ($listOfImplRegion as $key => $value) {
				if (isset($value["name"])) {
					$networkArray[] = $value["name"];
				}
			}
			$res["results"] = $networkArray;
			$res["results"] = array_unique(array_map(function($item) {
				return preg_replace('/\([^)]+\)/', '', $item);
			}, $res["results"]));

			// if aucun match 
			if ((!empty($searchParams["name"]) && preg_match("/au/i", $searchParams["name"])) || empty($searchParams["name"])) {
				array_push($res["results"], "Aucun");
			}
			if(isset($searchParams["count"])) {
				$res["count"][$searchParams["searchType"][0]] = count($res["results"]);
			}
		}
		
		return $res;
	}

	public static function addMobilized($data = []){
		$result = [];
		if(count($data) > 0){
			foreach ($data as $key => $value) {
				$answer = PHDB::count(Answer::COLLECTION, array("answers.lesCommunsDesTierslieux1462024_1313_0.finderlesCommunsDesTierslieux1462024_1313_0lxeldfuzo8xtrxheuwo.$key" => ['$exists' => true]));
				if($answer > 0){
					$value["mobilized"] = true;
				}
				$result[$key] = $value;
			}
		}
		return $result;
	}

	public static function addAnswerAndPerson ($data=[]) {
		$result = [];
		$newArray = [];
		if (count($data) > 0) {
			$orgaId = "";
			$user = "";
			$getOrgaImage = [];
			$getUserName = [];
			$moreContent = [];
			foreach ($data as $key => $value) {
				$value["networkName"] = "";
				$value["networkSlug"] = "";
				if (isset($value["address"]["level3"])) {
					$getNetworkOrga = PHDB::findOne(Organization::COLLECTION, array("address.level3" => $value["address"]["level3"], "tags" => array( '$in' => array('RéseauTiersLieux'))), array("name" => 1, "slug" => 1));
					$value["networkName"] = $getNetworkOrga["name"] ?? "";
					$value["networkSlug"] = $getNetworkOrga["slug"] ?? "";
				}
				$answer = PHDB::findOne(Answer::COLLECTION, array("form" => "63e0a8abeac0741b506fb4f7", "links.organizations.$key" => array('$exists' => true)));
				if($answer){
					$user = (isset($answer["user"])) ? $answer["user"] : "";
					if ($user != "") {
						$getUserName = PHDB::findOneById(Citoyen::COLLECTION, $user, array("name" => 1, "_id" => 0));
						$value["nameAndUser"] = $getUserName;
					}
					$value["answers"] = $answer;
				}
				// $result[] = $value;
				// // var_export($moreContent, true);
				$newArray[$key] = $value;
				// $result = [];
			}
		}
		return $newArray;
	}

	public static function addOrgaInElement ($data=[], $type, $params=[]) {
		$res = $data;
		$links = [];
		if (count($data) > 0) {
			foreach ($data as $key => $value) {
				if ($type == Project::COLLECTION && count($params) > 0) {
					if (isset($value["links"]["contributors"])) {
						$links = array_keys($value["links"]["contributors"]);
						$orgaId = array_filter($links, function ($key) use ($params) {
							return in_array($key, $params);
						});
						foreach ($orgaId as $orgaKey => $orgaValue) {
							$value["organizations"][$orgaValue] = PHDB::findOneById(Organization::COLLECTION, $orgaValue, array("name" => 1));
						}
						unset($value["links"]["contributors"]);
					}
				}
				if ($type == Event::COLLECTION && count($params) > 0) {
					
					if (isset($value["links"]["attendees"])) {
						$links = array_keys($value["links"]["attendees"]);
						$orgaId = array_filter($links, function ($key) use ($params) {
							return in_array($key, $params);
						});
						foreach ($orgaId as $orgaKey => $orgaValue) {
							$value["organizations"][$orgaValue] = PHDB::findOneById(Organization::COLLECTION, $orgaValue, array("name" => 1));
						}
						unset($value["links"]["attendees"]);
					}
					if (isset($value["links"]["organizer"])) {
						$links = array_keys($value["links"]["organizer"]);
						$orgaId = array_filter($links, function ($key) use ($params) {
							return in_array($key, $params);
						});
						foreach ($orgaId as $orgaKey => $orgaValue) {
							$value["organizations"][$orgaValue] = PHDB::findOneById(Organization::COLLECTION, $orgaValue, array("name" => 1));
						}
						unset($value["links"]["attendees"]);
					}
				}
				if($type == Organization::COLLECTION){
					if(isset($value["links"]["organizations"]) && isset(array_keys($value["links"]["organizations"])[0])){
						$orgaId = array_keys($value["links"]["organizations"])[0];
						$value["organizations"] = PHDB::findOneById(Organization::COLLECTION, $orgaId, array("name" => 1, "geo" => 1, "geoPosition" => 1, "address" => 1, "addresses" => 1));
					}
				}
				$res[$key] = $value;
			}
		}
		return $res;
	}

	public static function getMarkersAnswers ($where=[], $arrayFormPath=[], $showAnswers=false, $network="false") {
		$markerData = [];
		$getAllAnswers = [];
		$condition = $where['$or'];
		$newCondition = [];
		if ($network == "true") {
			$getAllanswerTest = PHDB::find(Answer::COLLECTION, $where , array(
				"links.organizations" => 1, 
				"_id" => 1, 
			));
			foreach ($getAllanswerTest as $key => $value) {
				if (isset(array_keys($value["links"]["organizations"])[0])) {
					$organizationId = array_keys($value["links"]["organizations"])[0];
					$markerData[$key][$organizationId] = PHDB::findOneById(Organization::COLLECTION, $organizationId);
				}
			}
			foreach ($condition as $orga) {
				$orgaKey = explode(".", array_key_first($orga))[2];
				$orCondition = ['$or' => []];
				foreach ($arrayFormPath as $keyPath => $valuePath) {
					$andCondition = ['$and' => []];
					$andCondition['$and'][] = ["form" => $keyPath];
					$andCondition['$and'][] = [$valuePath . "." . $orgaKey => ['$exists' => true]];
					$orCondition['$or'][] = $andCondition;
				}
				$newCondition['$or'][] = $orCondition;
			}
			$getAllAnswers = PHDB::find(Answer::COLLECTION, $newCondition, ["answers"]);
		} else {
			$markerData = PHDB::find(Organization::COLLECTION, $where ,
			array(
				'geoPosition' => 1,
				'geo' => 1,
				'name' => 1
			));
		}		
		return array ("markerData" => $markerData, "supplementAnswer" => $getAllAnswers);
	}

	public static function csvElement($post){
		if(isset($post["extraFilters"])){
			if(empty($post["filters"])){
				$post["filters"]=$post["extraFilters"];
			}
			else if(is_array($post["extraFilters"]) && is_array($post["filters"])){
			    $post["filters"]=array_merge_recursive($post["filters"],$post["extraFilters"]);
		    }
		}
		unset($post['extraFilters']);
		$res=SearchNew::searchAdmin($post);
		return $res;
		
	}

	public static function getAllNetworkWithCondition () {
		$res = [];
		$getOptions = PHDB::find( Organization::COLLECTION,["tags" => "RéseauTiersLieux" ], ['name',"profilMediumImageUrl", 'address', "slug"]) ?? [];
		foreach ($getOptions as $key => $value) {
			$inScope = PHDB::find(Organization::COLLECTION , array(
			"address.level3" => $value['address']['level3'],
			'$or' => array(
				array("reference.costum"=>"franceTierslieux"),
				array("source.keys"=>"franceTierslieux")  
			) ), ["name", "collection", "profilImageUrl"] );
			$orga = array();
			$whereAnswers = array();
			$arrayName = array();
			$networkImage = "";
			foreach ($inScope as $key => $item) {
				unset($item["_id"]);
				array_push($communityIds, $key);
				$item["type"] = $item["collection"];
				unset($item["collection"]);
				unset($item["profilImageUrl"]);
				array_push($orga, [$key => $item]);
				array_push($whereAnswers, ["links.organizations.".$key => ['$exists' => true]]);
				array_push($arrayName, $item["name"]);
				$networkImage = (isset($item["profilImageUrl"])) ? $item["profilImageUrl"] : "";
			}
			$where[$value['name']] = array('whereAnswers' => ['$or' => $whereAnswers],"where" => array("links.organizations" => array('$in'=>$orga)), "count" => count($orga), "level3Key" => $value['address']['level3'], "arrayName" => $arrayName, "imageUrl" => $networkImage);
		}
		$getPrevOptions = array_map(function($item) {
			return array($item["slug"] => array("name" => $item["name"], "profilMediumImageUrl" => @$item["profilMediumImageUrl"], "address" => $item["address"]));
		}, array_values($getOptions));
		$getPrevOptions = array_reduce($getPrevOptions, 'array_merge', array());
		$getOptions = array_map(function($item) {
		  return array($item["address"]["level3"] => preg_replace('/\([^)]+\)/', '', $item["name"]));
		}, array_values($getOptions));
		$getOptions = array_reduce($getOptions, 'array_merge', array());
		$res = ["previewOptions" => $getPrevOptions ?? [], "simpleOptions" => $getOptions ?? [], "whereLists" => $where ?? []];
		return $res;
	}

	public static function addInseeDensityToAddress($id,$org){

		$res= array("result"=>false, "msg"=>Yii::t("common", "Problème à l'ajout de la densité Insee"));
		$mapping=PHDB::FindOne(Mapping::COLLECTION,array("name"=>"Decoupage INSEE"),array("fields"));
		$inseeCodes=array_keys($mapping["fields"]);
		$cities=PHDB::find(City::COLLECTION,array("insee"=>array('$in'=>$inseeCodes)));
		$highDensity=array("Paris","Lyon","Marseille");
		if(!empty($org["address"]["codeInsee"])){
			if(isset($mapping["fields"][$org["address"]["codeInsee"]])){
				// var_dump("yoyo",$mapping["fields"]);
				$density=$mapping["fields"][$org["address"]["codeInsee"]];
				// var_dump($density);exit;
			}else{
				// var_dump("here");exit; 
				$city=PHDB::findOne(City::COLLECTION,array("_id"=> new MongoId($org["address"]["localityId"])));
				if(isset($city["density"]["degréDensité"])){
					$density=$city["density"]["degréDensité"];
				}else{
				// (array_search( new MongoRegex("/" . explode(" ",$org["address"]["addressLocality"])[0] . "/i"),$highDensity)){
					
					$matches=preg_grep('/'.explode(" ",$org["address"]["addressLocality"])[0].'/i',$highDensity);
					// var_dump($matches);
					if(!empty($matches)){
						$density=4;
					}elseif(!array_search($org["address"]["localityId"],array_keys($arr))){
							array_push($arr,array($org["address"]["localityId"]=>$org["address"]["addressLocality"]));
						
					}
						// explode(" ",$org["address"]["addressLocality"])[0])
					// ,array_search( new MongoRegex("/" . explode(" ",$org["address"]["addressLocality"])[0] . "/i"),$highDensity));exit;
				// }elseif(isset($city["densite"])){
				// 	// if($de)
				}
					
			
			}
			if(!empty($density)){
				PHDB::Update(
					Organization::COLLECTION,
					array("_id"=>new MongoId($id)),
					array('$set'=>
						array("address.inseeDensity"=>$density)
					)
				);
				$res= array("result"=>true, "msg"=>Yii::t("common", "Densité Insee bien ajouté à l'adresse"));
			}else{
				$res["msg"]="Aucune densité Insee n'a été trouvée";

			}	
		}else{
			$res["msg"]="Code insee introuvable";
		}
		return $res;
	// }

	}

	public static function existingAnswerBooleanExceptions($params){
			
		$exceptions=array(
			// RH Contrat
			"franceTierslieux1922023_1417_7lebeyrj8fchnm9ehoxv",
			"franceTierslieux1922023_1417_7lebeyrj8jkinjym2mmb",
			"franceTierslieux1922023_1417_7lf3yrvyxn59y74gq51i",
			// simple radio fields
			// multiradio
			"franceTierslieux1522023_146_2le5osg2zot8o4trt4",
			"franceTierslieux1522023_146_2le5osg302vw1u14cpwy",
			"franceTierslieux1822023_1721_4lea64xa0rhrlpidw01",
			"franceTierslieux1922023_1231_6lebdouta0in8w4sa9iua",
			"franceTierslieux1922023_1231_6lebdoutaof95ocsytqb",
			"franceTierslieux2022023_732_9lecfwuxz1ro1kkcljy7",
			"franceTierslieux1922023_1231_6leh388ss6s98rnld2xe",
			"franceTierslieux1522023_1341_0lf2j30fgrikequtany",
			// radionew
			"franceTierslieux1522023_146_2le5osg2yojldzvooar9",
			"franceTierslieux1522023_146_2le5osg2y8vqgazoigl",
			"franceTierslieux1522023_146_2le5osg2z48o1yquwcv",
			"franceTierslieux1522023_146_2le5osg2zq3m3vw4ymqp",
			"franceTierslieux1822023_1721_4lea64x9t2q7t5h4sxsl",
			"franceTierslieux1822023_1721_4lea64x9vsar6wu8yht",
			"franceTierslieux1822023_1721_4lea64x9yarsh39l6hae",
			"franceTierslieux1922023_1231_6lebdoutae8cgtmhdyx7",
			"franceTierslieux1522023_1341_0lepon6voqlg8iqevhy",
			"franceTierslieux1522023_1341_0lepox9dlj3t69fq2t5",
			"franceTierslieux1522023_1341_0lepoy0hi2q2vbl2x4fc",
			"franceTierslieux1922023_1231_5lezmol4bnz3uvjedajn",
			"franceTierslieux1522023_146_2lf263wkqxf4w97gyqs"
		);
		// if($params["key"]=="franceTierslieux1522023_146_2le5osg2z48o1yquwcv"){
		// 	// var_dump($params,array_search($params["key"],$exceptions));exit;

		// }
		
		// var_dump($fieldKey,array_search($fieldKey,$exceptions));exit;
		if(array_search($params["key"],$exceptions)===false){
			$res=true;
		}else{
			$res=false;
		}

		if(is_numeric($params["value"]) || is_string($params["value"])){
			// var_dump($params);exit;
			$res=false;
		}

		return $res;

	}

	

	// public static function getNameOrUrl ($type, $key) {
	// 	$result = [];
	// 	$getElement = PHDB::
	// }

}


?>