<?php
class InstitutBleu {
    const COLLECTION = "costum";
    const CONTROLLER = "costum";
    const MODULE = "costum";
    public static function removeSourceFromElement($params){
        $elt=Element::getElementById($params["id"], $params["type"]);
        $costum = CacheHelper::getCostum();
        if(isset($elt["tags"])){ 
            foreach($costum["lists"]["family"] as $kF => $vT){                
                if (($key = array_search($kF, $elt["tags"])) !== false) {
                    unset($elt["tags"][$key]);
                } 
            }
            foreach($costum["lists"]["objectiveOdd"] as $kT => $vT){                
                if (($keyT = array_search($kT, $elt["tags"])) !== false || ($keyT = array_search(str_replace("&","&amp;",$kT), $elt["tags"])) !== false) {
                    unset($elt["tags"][$keyT]);
                } 
            }
            $unset = [
                "legalStatus" => true,
                "mobile" => true,
                "mobile" => true,
                "thematic" => true,
                "referentId" => true,
                "objectiveOdd" => true,
                "acronym" => true,
                "preferences.toBeValidated".$costum["slug"] => true,
                "category" => true
            ];
    
            $set = [
                "tags"=> $elt["tags"]
            ];
            if(isset($elt["source"])){
                if($elt["source"]["key"] == $costum["slug"]) 
                    $unset["source"] = true;
                if($elt["source"]["key"] != $costum["slug"] && ($keyS = array_search($costum["slug"], $elt["source"]["keys"]) !== false)){
                    unset($set["source"]["keys"][$keyS]);
                    $set["source.keys"] =  $elt["source"]["keys"];
                }    
            }
            PHDB::update($params["type"], array( "_id" => new MongoId($params["id"])),array('$unset' => $unset,'$set' => $set ));  
        }

    }
    public static function connect($params){
        $child = PHDB::findOne(Organization::COLLECTION, array("_id" => new MongoId($params["idChild"])));
        // $parent =  PHDB::findOne(Organization::COLLECTION, array("_id" => new MongoId($params["idParent"])),array("link"));
        $data = [
            'parentId' => $params["idParent"],
            'parentType' => Organization::COLLECTION,
            'listInvite' => [
                Organization::COLLECTION => [
                    $params["idChild"] => [
                        "name" => $child["name"],
                        "profilThumbImageUrl" => isset($child["profilThumbImageUrl"]) ? $child["profilThumbImageUrl"] : Yii::app()->getModule("co2")->getAssetsUrl().'/images/thumb/default_organizations.png',
                    ]
                ]
            ]
        ];

        $res = Link::invite($data["listInvite"], $data);
        return $res;
        // if(!isset($child["links.memberOf.".$params["idParent"]])){
        //     $queryChild = array("links.memberOf.".$params["idParent"] => array(
        //         "type" => "organizations",
        //         "date" => new MongoDate(time())
        //     )); 
        //     $resChild = PHDB::update( Organization::COLLECTION,
        //         array("_id" => new MongoId($params["idChild"])),
        //         array('$set'=>$queryChild)
        //     );
        // }
        // if(!isset($parent["links.members.".$params["idChild"]])){
        //     $queryParent = array("links.members.".$params["idChild"]  => array(
        //         "type" => "organizations",
        //         "date" => new MongoDate(time())
        //     ));
        //     $resParent = PHDB::update( Organization::COLLECTION,
        //         array("_id" => new MongoId($params["idParent"])),
        //         array('$set'=>$queryParent)
        //     ); 
        // }
    }
    public static function prepDataForUpdate($data){
        $costum = CacheHelper::getCostum();
        if( $data["collection"] == Organization::COLLECTION){
            if(isset($data["tags"])){
                foreach($data["tags"] as $k => $tag){
                    if(in_array($tag, array_keys($costum["lists"]["family"]))){
                        unset($data["tags"][$k]);
                    }
                }
            }
        }
		return $data;
	}	
}