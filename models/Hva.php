<?php

class Hva {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	public static function prepData($params){
        $costum = CacheHelper::getCostum();
        
        if(!empty($costum ) ) {
            if( !empty($params["preferences"]) && !empty($params["preferences"]["toBeValidated"]) ) {

            	if(!empty($params["preferences"]["toBeValidated"][$costum["slug"]]))
            		unset($params["preferences"]["toBeValidated"][$costum["slug"]]);
                $validated = true;
                if( !empty($costum["typeObj"]) && 
                    ( (!empty($costum["typeObj"][$params["collection"]]) &&
                        !empty($costum["typeObj"][$params["collection"]]["validatedParent"]) ) ||
                        (!empty($costum["typeObj"][$params["key"]]) &&
                        !empty($costum["typeObj"][$params["key"]]["validatedParent"]) ) ) ) {

                    if($params["collection"] == Event::COLLECTION && !empty($params["organizer"])) {
                        $parent = "organizer";
                    }else{
                        $parent = "parent";
                    }
                    $validated = false;
                    foreach ($params[$parent] as $key => $value) {
                        if(!empty($value["type"])){
                            $eltParent = PHDB::findOneById($value["type"],$key, array('name','preferences'));
                            // var_dump( $eltParent);
                            if( !empty($eltParent) && 
                                !empty($eltParent["preferences"]) &&
                                !empty($eltParent["preferences"]["toBeValidated"]) &&
                                !empty($eltParent["preferences"]["toBeValidated"][$costum["slug"]]) &&
                                $eltParent["preferences"]["toBeValidated"][$costum["slug"]] )
                                $validated = true;
                        }else
                            $validated = true;
                        
                    }
                }
                //var_dump( $validated);
                if($validated === true)
                    $params["preferences"]["toBeValidated"] = array($costum["slug"] => true);
            }
        }
        //Rest::json($params); exit;
        return $params;
    }

    
    public static function elementAfterSave($data){
        //WARNING Yii::app()->session["costum"] est ce que l'on veut la data en cache
        $costum = CacheHelper::getCostum();
        // if(!empty(Yii::app()->session["costum"] ) ) {
        if(!empty($costum) ) {
            if($data["collection"] == Event::COLLECTION){
                if(!empty($data["params"]) && !empty($data["params"]["creator"]) ){
                    //var_dump($data["params"]["creator"]); exit;

                    Link::connect($data["id"], $data["collection"], $data["params"]["creator"], Person::COLLECTION, Yii::app()->session["userId"], "attendees", true, false, false, false, "");
                    Link::connect($data["params"]["creator"], Person::COLLECTION, $data["id"], $data["collection"], Yii::app()->session["userId"], "events", true, false, false, false, "");


                    // connect($originId, $originType, $targetId, $targetType, $userId, $connectType,$isAdmin=false,$pendingAdmin=false,$isPending=false, $isInviting=false, $role="", $settings=false)
                }
            }
        }
        //Rest::json($params); exit;
        return $data;
    }
}
?>