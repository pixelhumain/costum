<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;

class Transiter {

	public static function connectElement($params){
        if(isset($params["extraParams"]["rest"])){
            unset($params["extraParams"]["rest"]);
        }
        if(isset($params["extraParams"]["ref"])){
            unset($params["extraParams"]["ref"]);
        }

        // $updateParams=["type"=>"citoyens"];
        if(!empty($params["roles"])){
            $updateParams["links.attendees.".Yii::app()->session["userId"].".roles"]=$params["roles"];
        }
        foreach($params["extraParams"] as $k => $v){
            if($k=="visioDate"){
                $stampVisioDate=strtotime($v);
                $mongoVisioDate=new MongoDate($stampVisioDate);
                $v=$mongoVisioDate;
            }
            $updateParams["links.attendees.".Yii::app()->session["userId"].".".$k]=$v;

        }
        $user=PHDB::findOne(Person::COLLECTION,array("_id"=>New MongoId(Yii::app()->session["userId"])),array("email"));
        $interestedUser=array(
            "name"=>Yii::app()->session["user"]["name"],
            "email" =>$user["email"],
            "id"=>Yii::app()->session["userId"]
        );
        Link::connect((string)$params["id"], Event::COLLECTION, Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"], "attendees");
        Link::connect(Yii::app()->session["userId"], Person::COLLECTION, (string)$params["id"], Event::COLLECTION, Yii::app()->session["userId"], "events");
        if(!empty($updateParams)){
        PHDB::update(
            $params["type"],
            ["_id" => new MongoId($params["id"])],
            ['$set' => $updateParams]
        );
        }

        $session=PHDB::findOne($params["type"],array("_id" => new MongoId($params["id"])));

        $visioDatePreSub=(isset($stampVisioDate) && !empty(date("Y-m-d", $stampVisioDate))) ? date("d-m-Y", $stampVisioDate)  : "";
        $visioHourPreSub=(isset($stampVisioDate) && !empty(date("H:i", $stampVisioDate))) ? date("H:i", $stampVisioDate)  : "";

        $paramsMail = array(
            "tpl" => "basic",
            "tplObject" => "Pré-inscription à la visioconférence d'information - Formation ". $session['name'] ,
            "tplMail" => $interestedUser["email"],
            "html" => "Vous nous avez communiqué votre intérêt pour la formation. Vous vous êtes pré-inscrit.e à la visioconférence d'information du ". $visioDatePreSub . " à ".$visioHourPreSub ." concernant la session de formation intitulée " . $session['name'] .".",
            "btnRedirect" => array(
                "hash" => "#listing-sessions?preview=events.".$params["id"],
                "label" => "Accéder au récapitulatif"
            ),
        );	
        $res=Mail::CreateAndSend($paramsMail);
        return $res;
				
	}

    public static function linkAndNotifyTrainerOrPartner($data){
        $children=array();
        $links=array();
        if(!empty($data["params"]["trainer"])){
            $links["citoyens"]=$data["params"]["trainer"];			    
        }if(!empty($data["params"]["partner"])){
            $links["organizations"]=$data["params"]["partner"];			    
        }
        if(!empty($links)){
            $element=PHDB::findOne($data["collection"],array("_id"=>new MongoId($data["id"])),array("links"));
            foreach($links as $collec=>$obj){
                $isConnectingAdmin=($collec==Person::COLLECTION) ? true : false ;
                
                foreach($obj as $id=>$val){
                    $existingRoles=(isset($element["links"][Link::$linksTypes[$data["collection"]][$collec]][$id]["roles"])) ? $element["links"][Link::$linksTypes[$data["collection"]][$collec]][$id]["roles"]: [];
                    // var_dump(Link::$linksTypes[$data["collection"]],$collec,$obj,$existingRoles,array_intersect($existingRoles,["Formateur.ice"]));exit;
                    if(empty(array_intersect($existingRoles,["Formateur.ice"]))){
                        $userRole=($collec==Person::COLLECTION) ? array_merge($existingRoles,["Formateur.ice"]) : [] ;
                        Link::connect($data["id"], $data["collection"], $id, $collec, Yii::app()->session["userId"], Link::$linksTypes[$data["collection"]][$collec], $isConnectingAdmin, false, false, false, $userRole);
                        Link::connect($id, $collec, $data["id"], $data["collection"], Yii::app()->session["userId"], Link::$linksTypes[$collec][$data["collection"]], $isConnectingAdmin, false, false, false, $userRole);
                        if($collec==Person::COLLECTION){
                            $tplObject="";
                            $html="";
                            $btnLabel="";
                            $btnHash="";
                            if($data["collection"]==Project::COLLECTION){
                                $tplObject="Formation" .$data["params"]["name"]. " - Formateur.ice / Administrateur.ice"; 
                                $html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la formation " .$data["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-formations?preview=".$data["collection"].".".$data["id"]."'>espace dédié</a>. Vous pourrez alors y créer les sessions à venir.";
                                $btnLabel="Consulter les informations liées à la formation";
                                $btnHash=Yii::app()->baseUrl."/#listing-formations?preview=".$data["collection"].".".$data["id"];
                            }else if($data["collection"]==Event::COLLECTION){
                                $tplObject="Session de formation " .$data["params"]["name"]. " - Formateur.ice / Administrateur.ice";
                                $html="Vous avez été ajouté.e en tant que formateur.ice / administrateur.ice de la session de formation " .$data["params"]["name"]. ". Vous pouvez modifier les informations relatives à la formation dans l'<a href='".Yii::app()->baseUrl."/#listing-sessions?preview=".$data["collection"].".".$data["id"]."'>espace dédié</a> et gérer le listing des stagiaires potentiels.";
                                $btnLabel="Consulter les informations liées à la session de formation";
                                $btnHash=Yii::app()->baseUrl."/#listing-sessions?preview=".$data["collection"].".".$data["id"];
                            }	
                            $user = PHDB::findOne($collec,array("_id"=>new MongoId($id)),array("email")); 
                            $emailUser=$user["email"];
                            $paramsmail = array(
                                "tpl" => "basic",
                                "tplObject" => $tplObject,
                                "tplMail" => $emailUser,
                                "html" => $html,
                                // "community" => <?php echo (!empty($selectedContact["roles"]) ? json_encode($selectedContact) : "[]" )
                                "btnRedirect" => array(
                                    "hash" => $btnHash,
                                    "label" => $btnLabel
                                )
                            );	
                            Mail::createAndSend($paramsmail);
                        }					
                    }
                }    
            }	
        }    

    }

    public static function addInvitationAndInterestedLinks($data){
        $baseUrl = Yii::app()->baseUrl;
        $costum = CacheHelper::getCostum();
        if(!isset($costum["host"])){
            $baseUrl=$baseUrl."/costum/co/index/slug/".$costum["slug"];
        }
        $invitLink=array(
            "targetType" => $data["collection"],
            "targetId" => (string)$data["params"]["_id"],
            "roles" => ["Intéressé.e"],
            "isAdmin" => false,
            "urlRedirection" => $baseUrl."#sessions-publics?preview=".$data["collection"].".".(string)$data["params"]["_id"],
            "invitorId" =>  Yii::app()->session["userId"],
            "ref" => uniqid(),
            "createOn" => time()
        );	
        // var_dump($invitLink);exit;
        PHDB::insert(InvitationLink::COLLECTION, $invitLink);
        PHDB::update( Event::COLLECTION, 
              array("_id"=>$data["params"]["_id"]),
            array('$set' => array("interestedLink"=> "/co2/link/connect/ref/".$invitLink["ref"])));


    }

	public static function elementAfterSave($data){
		
		$costum = CacheHelper::getCostum();
    	
		//Trainer et partner sessions de formation et formations ajouter dans participants + notif mail
		if(($data["collection"]==Project::COLLECTION && isset($data["params"]["category"]) && $data["params"]["category"]=="formation")||($data["collection"]==Event::COLLECTION && isset($data["params"]["category"]) && $data["params"]["category"]=="sessionFormation")){
			self::linkAndNotifyTrainerOrPartner($data);
		}

		if($data["collection"]==Event::COLLECTION){
			// var_dump($data);exit;
			if(isset($data["params"]["category"]) && $data["params"]["category"]=="sessionFormation"){
                self::addInvitationAndInterestedLinks($data);
			}
		}

    }

    public static function elementAfterUpdate($params){
        // var_dump("ok");exit;
		//Trainer et partner sessions de formation et formations ajouter dans participants + notif mail
		if(($params["collection"]==Project::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="formation")||($params["collection"]==Event::COLLECTION && isset($params["params"]["category"]) && $params["params"]["category"]=="sessionFormation")){
			self::linkAndNotifyTrainerOrPartner($params);
		}		
	}
    public static function deletetags($params){
        $elt=Element::getElementById($params["id"], $params["type"]);
        // $tags = $params["tags"];
        $tagsDelete = [
            "Formateur",
            "Centre de formation",
            "Formation"
        ];
        $set = [];
        $unset = [];
        if(isset($elt["tags"])){    
            foreach($tagsDelete as $tags){
                if (($key = array_search($tags, $elt["tags"])) !== false) {
                    unset($elt["tags"][$key]);
                }
                $set = [
                    "tags"=> $elt["tags"]
                ];
               }                       
        }
        if(isset($elt["category"]) && $elt["category"] == "formation"){
            $unset = [
                "category"=> true
            ];
        }
        $upd = [
            '$set'=> $set
        ];
        if(!empty($unset)){
            $upd['$unset'] = $unset;
        }
        PHDB::update($params["type"], array( "_id" => new MongoId($params["id"])),$upd);
            
    }
    public static function removeSourceFromElement($params){
        $elt=Element::getElementById($params["id"], $params["type"]);

        $tagsDelete = [
            "Formateur",
            "Centre de formation",
            "Formation"
        ];
        $set = [];
        $unset = [];
        if(isset($elt["tags"])){   
            foreach($tagsDelete as $tags){                       
                if (($key = array_search($tags, $elt["tags"])) !== false) {
                    unset($elt["tags"][$key]);
                }
                $set = [
                    "tags"=> $elt["tags"]
                ];
            }
        } 
        if(isset($elt["category"]) && $elt["category"] == "formation"){
            $unset = [
                "category"=> true
            ];
        }
        $upd = [
            '$set'=> $set
        ];
        if(!empty($unset)){
            $upd['$unset'] = $unset;
        }

        PHDB::update($params["type"], array( "_id" => new MongoId($params["id"])),$upd); 
    }

	public static function prepDataForUpdate($data){

        $costum = CacheHelper::getCostum();
		if($data["collection"]==Person::COLLECTION){
			if(!isset($data["email"])){
				$email=PHDB::findOne(Person::COLLECTION,array("_id"=>$data["_id"]),array("email","telephone","address","geo","geoPosition"));
				$data["email"]=$email["email"];				
			}

            $data["files"] = Document::getListDocumentsWhere(
                array(
                    "id"=> (String)$data["_id"],
                    "type"=>'citoyens',
                    "contentKey"=>'pdf',
                    "source.key"=>$costum["slug"]
                ), "file"
            );
		}
		return $data;  		

		  		
	}
	public static function getElement($data){
		if( isset($data["collection"] ) && $data["collection"] ==Person::COLLECTION){
			if(!isset($data["email"])){
				$email=PHDB::findOne(Person::COLLECTION,array("_id"=>$data["_id"]),array("email","telephone","address","geo","geoPosition"));
				$data["email"]=$email["email"];				
			}
		}
		return $data;  				  		
	}

	public static function canAddDocuments($params){ 
        $res = false;
        $elt=Element::getElementById($params["elementId"], $params["elementType"]);if(isset($elt["tags"]))                        
        if (($key = array_search("Formateur", $elt["tags"])) !== false && Authorisation::isCostumAdmin()) {
            $res = true;
        }
        return $res;
    }   
}
