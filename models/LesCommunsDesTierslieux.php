<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use ReseauTierslieux;
use Organization;
use CacheHelper;
use Event;
use Notification;
use MongoId;
use ActStr;
use Poi;
use Link;

class LesCommunsDesTierslieux {

    const COLLECTION = "costums";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static function elementAfterSave($data){
        $costum=CacheHelper::getCostum();
        $elem=PHDB::findOne($data["collection"],array("_id"=>new MongoId($data["id"])));
        if($data["collection"]==Organization::COLLECTION){
            // var_dump("here");exit;
            if(isset($data["params"]["mainTag"]) && $data["params"]["mainTag"]=="TiersLieux"){
                $tlnetwork=PHDB::findOne(Organization::COLLECTION,array("network"=>"ANCTNetwork","address.level3"=>$data["params"]["address"]["level3"]));
                if(!(isset($data["params"]["reference"]["costum"]) && in_array($tlnetwork["slug"],$data["params"]["reference"]["costum"]))){
                    ReseauTiersLieux::elementAfterSave($data);
                    //add reference in network
                    Admin::addSourceInElement($data["id"],$data["collection"],$tlnetwork["slug"],"reference");
                    //avoid adding the third place in lakou's community by default
                    Link::disconnect($costum["contextId"], $costum["contextType"], $data["id"], $data["collection"], Yii::app()->session["userId"], "members");
                    Link::disconnect( $data["id"], $data["collection"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
                    
                    // Assure adding the third place in lakou's reseauRegionalDesTierslieuxGuadeloupe network by default
                    Link::connect((string)$tlnetwork["_id"], $tlnetwork["collection"], $data["id"], $data["collection"], Yii::app()->session["userId"], "members");
                    Link::connect( $data["id"], $data["collection"], (string)$tlnetwork["_id"], $tlnetwork["collection"], Yii::app()->session["userId"], "memberOf");
                    Admin::addSourceInElement($data["id"],$data["collection"],$tlnetwork["slug"],"reference");
                }
            }
        }
	}

}
