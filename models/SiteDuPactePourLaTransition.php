<?php

class SiteDuPactePourLaTransition {
    const COLLECTION = "costums";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    public static $dataBinding_allPerson  = [
        "name"      => ["valueOf" => "name"],
        "email"      => ["valueOf" => "email"],
        "tags"      => ["valueOf" => "tags"],
        "Code Postal"     => array("parentKey"=>"scope","valueOf"=>array("postalCode"=> array("valueOf" => "postalCode"))),
    ];

    public static $dataBinding_allOrganization  = array(
        "id"     => array("valueOf" => "id"),
        "name"      => array("valueOf" => "name"),
        // "postalcode"     => array("parentKey"=>"address", 
        //                      "valueOf" => array("postalCode" => array("valueOf" => "postalCode"))),
        // "ville"     => array("parentKey"=>"address", 
        //                      "valueOf" => array("addressLocality" => array("valueOf" => "addressLocality"))),
        "Candidat"     => "",
        "Code Postal"     => array("parentKey"=>"scope","valueOf"=>array("postalCode"=> array("valueOf" => "postalCode"))),
        "Description du candidat"     => "",
        "Mesures"     => "",
        // "address"   => array("parentKey"=>"address", 
        //                      "valueOf" => array(
        //                             "@type"             => "PostalAddress", 
        //                             "streetAddress"     => array("valueOf" => "streetAddress"),
        //                             "postalCode"        => array("valueOf" => "postalCode"),
        //                             "addressLocality"   => array("valueOf" => "addressLocality"),
        //                             "codeInsee"         => array("valueOf" => "codeInsee"),
        //                             "addressRegion"     => array("valueOf" => "addressRegion"),
        //                             "addressCountry"    => array("valueOf" => "addressCountry")
        //                             )),
        // "postalcode"   => array("parentKey"=>"geo", 
        //                      "valueOf" => array(
        //                             "@type"             => "GeoCoordinates", 
        //                             "postalcode"          => array("valueOf" => "latitude"),
        //                             "longitude"         => array("valueOf" => "longitude")
        //                             )),
        // "startDate"     => array("valueOf" => "startDate"),
        // "endDate"       => array("valueOf" => "endDate"),
        // "geo"   => array("parentKey"=>"geo", 
        //                      "valueOf" => array(
        //                             "@type"             => "GeoCoordinates", 
        //                             "latitude"          => array("valueOf" => "latitude"),
        //                             "longitude"         => array("valueOf" => "longitude")
        //                             )),

    );

    // public static $dataHead_allOrganization  = array(
    //     "type"     => "Type",
    //     "name"      => "Nom",
    //     "siren"     => "Siren",
    //     "why"     => "Pourquoi souhaitez-vous réaliser un CTE ? Quelles sont vos attentes vis-à-vis d’un CTE ?",
    //     "nbHabitant"     => "Nombre d'habitants concernés?",
    //     "dispositif"     => "Quels sont les dispositifs actuellement mis en place sur tout ou partie du territoire (contractuels et/ou en lien avec la transition écologique) ?",
    //     "planPCAET"     => "Avez élaboré un Plan climat-air-énergie territorial (PCAET) sur votre territoire ?",
    //     "singulartiy"     => "Qu'est ce qui constitue la singularité de votre territoire ? Quel est le contexte économique et quels sont ses enjeux en termes de transition écologique ?",
    //     "caracteristique"     => "Quelles sont les caractéristiques de votre territoire sur lesquelles vous agissez/vous souhaiteriez agir dans le cadre du CTE ? (exemples : problèmes majeurs/risques auxquels vous êtes confrontés, thèmes sur lesquels vous vous êtes engagés à agir)",
    //     "actions"     => "Quelles sont les actions envisagées dans votre projet de CTE ? Décrivez au moins 3 pistes d’actions.",
    //     "economy"     => "Quels acteurs socio-économiques prévoyez-vous de mobiliser pour le portage d’actions du CTE ?",
    //     "inCharge"     => "Qui serait en charge du projet au sein de la collectivité (chargé de mission, équipe dédiée, élu...) ?",
    //     "autreElu"     => "Autres élus engagés politiquement dans la réussite du projet (député, sénateur, maires) ?",
    //     "contact"     => "Nom du référent local, Adresse postale, numéro de téléphone et adresse mail ?",
    //     // "url.communecter"     => "Url ctenat",
    //     // "url.pdf"     => "PDF",
    //     "url.website"     => "Site internet",
    //     "address.streetAddress"     => "Rue",
    //     "address.postalCode"     => "Code Postal",
    //     "address.addressLocality"     => "Ville",
    //     "address.codeInsee"     => "Insee",
    //     "address.addressCountry"     => "Code Pays",
    //     "geo.latitude"     => "Latitude",
    //     "geo.longitude"     => "Longitude",

    // );

    public static function saveContrat($params){

        //var_dump("HelloThere"); exit;
        PHDB::remove(Poi::COLLECTION, array("source.key"=>"siteDuPactePourLaTransition", "type"=>"contract"));
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        $measuresPacte=PHDB::find(Poi::COLLECTION, array( "source.key" => "siteDuPactePourLaTransition",
                                        "type" => "measure"), array("name"));
        $resultImport = array();
       // var_dump($res["elementsObj"]); exit;
        if(!empty($res) && !empty($res["elementsObj"])){
           foreach ($res["elementsObj"] as $key => $value) {
                if(!empty($value["ownerList"])){
                    $value["name"]=$value["ownerList"];
                    unset($value["ownerList"]);
                    // TODO : A modifier si besoin updater l'orga
                    $set = array("category"=>"actifSigned");
                    $value["type"] = "contract";
                    $value["creator"] = Yii::app()->session["userId"];
                    $value["created"] = time();
                    if(strlen(@$value["postalCode"])==4){
                        $value["postalCode"]="0".$value["postalCode"];
                    }
                    $measures = null ;
                    if(!empty($value["mesuresPacte"])){
                        $measures = explode(",", $value["mesuresPacte"]);
                        unset($value["mesuresPacte"]);
                    }
                    $insee = null ;
                    $mainKeyScope=null;
                    if(!empty($value["insee"])){
                        $insee = $value["insee"];
                        $city=City::getCityByInsee($insee);
                        if(empty($city)){
                            if(strlen($insee)==4){
                                $tryNewInsee="0".$insee;
                                $city=City::getCityByInsee($tryNewInsee);
                                if(empty($city)){
                                    $getCities=City::getByPostalCode(@$value["postalCode"]);
                                    if(!empty($getCities)){
                                        foreach($getCities as $k=> $v){
                                            $city=$v;
                                        }
                                        $value["insee"]=$city["insee"];
                                        $insee=$city["insee"];
                                    }
                                }
                            }
                        }
                        $prepScope=array();
                        if(isset($city["level1"])){
                            $prepScope["level1Name"] = $city["level1Name"];
                            $prepScope["level1"] = $city["level1"];
                        }

                        if(isset($city["level2"])){
                            $prepScope["level2Name"] = $city["level2Name"];
                            $prepScope["level2"] = $city["level2"];
                        }

                        if(isset($city["level3"])){
                            $prepScope["level3Name"] = $city["level3Name"];
                            $prepScope["level3"] = $city["level3"];
                        }

                        if(isset($city["level4"])){
                            $prepScope["level4Name"] = $city["level4Name"];
                            $prepScope["level4"] = $city["level4"];
                        }
                        else if(isset($city["country"]))
                            $prepScope["country"] = $city["country"];
                    
                        $prepScope["postalCode"] = $city["postalCodes"][0]["postalCode"];
                        $prepScope["cityName"] = $city["postalCodes"][0]["name"];
                        $prepScope["city"] = (string)$city["_id"];
                        $prepScope["type"] ="cities";
                        if(count($city["postalCodes"])>1)
                            $key=(string)$city["_id"]."cities".$city["postalCodes"][0]["postalCode"];
                        else
                            $key=(string)$city["_id"]."cities";
                        $prepScope["key"]=$key;
                        $value["scope"]=array($key=>$prepScope);
                        $mainKeyScope=$key;
                    }

                    $postalCode = null ;
                    //if(!empty($value["postalCode"])){
                        $postalCode = $value["postalCode"];
                         $where = array( "source.key" => "siteDuPactePourLaTransition",
                                            '$or'=>array(
                                                array("email" => "pacte-".$postalCode."@listes.transition-citoyenne.org"),
                                                array("scope.".$mainKeyScope => array('$exists' => true))
                                            )
                                        );
                           
                            $orga = PHDB::findOne( Organization::COLLECTION, $where, array("name") );
                            if(!empty($orga))
                                $value["parent"]=array((string)$orga["_id"]=>array("type"=>Organization::COLLECTION));

                            /* if(isset($value["url"]) && !empty($value["url"])){
                                 /*PHDB::update(Organization::COLLECTION, 
                                     array("_id"=>$orga["_id"]), 
                                     array('$set'=>array("url"=>$value["url"]))
                                );
                                 unset($value["url"]);
                                 $set["url"] = $value["url"];

                             }*/
                            
                                 
                         
                    //}
                    $mesuresLocale=array();
                    for ($i=0; $i < 5; $i++) { 
                        # code...
                        $incMes=($i+1);
                        if(isset($value["mesureLocale".$incMes]) && !empty($value["mesureLocale".$incMes])){
                            $mesuresLocale[$i]=array("name"=>"Mesure locale ".$incMes, "description"=>$value["mesureLocale".$incMes]);
                            unset($value["mesureLocale".$incMes]);
                        }
                    }
                    if(!empty($mesuresLocale))
                        $value["localMeasures"]=$mesuresLocale;
                    $update = false;
                    $id = false;
                     if( !empty($measures) ){
                        $links=array();
                        $links["measures"]=array();
                        foreach ($measures as $keylO => $vallO) {
                            $mesLevel=explode(".", $vallO);
                            $keyMes=self::array_find($mesLevel[0], $measuresPacte);
                            if(!empty($keyMes) && !empty($mesLevel[1]) ){
                                $links["measures"][$keyMes]=array("type"=>Poi::COLLECTION, "level"=>$mesLevel[1]);
                            }
                        }
                        if(!empty($links["measures"]))
                            $value["links"]=$links;
                    }
                    $resElt = array(
                        "update" => false,
                        "id" => false,
                        "name" => @$value["name"]
                    );
                    $value["collection"]=Poi::COLLECTION;
                   // $contractExist=PHDB::findOne(Poi::COLLECTION, array("insee"=>$insee, "name"=>@$value["name"]));
                    //if(empty($contractExist)){
                        //var_dump($value);
                    Yii::app()->mongodb->selectCollection(Poi::COLLECTION)->insert( $value );
                    //var_dump($value);
                    if(!empty($orga)){
                       // $value["parent"]=array((string)$orga["_id"]=>array("type"=>Organization::COLLECTION));
                            PHDB::update(Organization::COLLECTION, 
                                array("_id"=>$orga["_id"]),
                                array('$set'=>$set, '$unset'=>array("links.contracts"=>""))
                            );
                            
                            Link::connect( (String)$orga["_id"], Organization::COLLECTION, (String)$value["_id"], Poi::COLLECTION, Yii::app()->session["userId"],"contracts",false,false,false,false, null);
                        
                    }
                    /*if(isset($value["urlContract"])){
                       // if(strrpos($value["urlContract"], "nextcloud.transition")) $value["urlContract"]=$value["urlContract"]."/download";
                        $res=Document::uploadDocumentFromURL("communecter",Poi::COLLECTION,(string)$value["_id"],"",false, $value["urlContract"]);
                        var_dump($value["urlContract"]);
                        var_dump($res);exit;
                          $params = array(
                            "id" => (string)$value["_id"],
                            "type" => Poi::COLLECTION,
                            "folder" => Poi::COLLECTION."/".(string)$value["_id"],
                            "moduleId" => "communecter",
                            "name" => $res["name"],
                            "size" => (int) $res['size'],
                            "contentKey" => "",
                            "doctype"=> "file",
                            "author" => Yii::app()->session["userId"],
                            "subKey"=>"contractPdf"
                        );
                        $res2 = Document::save($params);
                    }*/
                     



                    
                    //if( !empty($measures) ){
                        //$listOwnerRegex = array();
                     //   foreach ($measures as $keylO => $vallO) {
                     //       $mesLevel=explode(".", $vallO);
                     //       $listOwnerRegex = new MongoRegex("/.*{$mesLevel[0]}.*/i");   
                     //       $where = array( "source.key" => "siteDuPactePourLaTransition",
                       //                 "type" => "measure",
                         //               "name" => $listOwnerRegex );
                           // $measure = PHDB::findOne( Poi::COLLECTION, $where, array("name") );
                    //        if(!empty($measure) && !empty($mesLevel[1]) ){
                      //         // foreach ($measure as $keyM => $valM) {
                        //            Link::connect((String) $value["_id"], Poi::COLLECTION, (string)$measure["_id"], Poi::COLLECTION, Yii::app()->session["userId"], "measures",false,false,false,false, "",array("name"=>"level", "value"=>$mesLevel[1]));
                          //          Link::connect((string)$measure["_id"], Poi::COLLECTION, (String) $value["_id"], Poi::COLLECTION, Yii::app()->session["userId"], "contracts",false,false,false, false, "", array("name"=>"level", "value"=>$mesLevel[1]));
                                //}
                    //        }
                    //    }
                    //}*/
                    $resultImport[] = $resElt;
                  //  }
                }   
            } 
        }

        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }



    public static function saveCollectif($params){

        //var_dump("HelloThere"); exit;
        
        $params = Import::newStart($params);
        $res = Import::previewData($params, true, true, true);
        
        $resultImport = array();
        if(!empty($res) && !empty($res["elementsObj"])){

            foreach ($res["elementsObj"] as $key => $value) {
                if (empty($value["name"])) 
                    $value["name"] = "Collectif local du Pacte pour la Transition";
                   // var_dump($value["maininseeContract"]); 
                //if(!empty($value["name"])){
                   
                                 
                    //if(!empty($value["postalCode"])){
                    if(strlen(@$value["postalCode"])==4){
                        $value["postalCode"]="0".$value["postalCode"];
                    }
                    $postalCode = $value["postalCode"];
                    //$postalCode = null ;
                    if(isset($value["maininseeContract"]) && !empty($value["maininseeContract"])){
                                $value["maininseeContract"]=(strlen($value["maininseeContract"])==4) ? "0".$value["maininseeContract"] : $value["maininseeContract"];
                             
                    $scopes=array();
                    $mainCity=City::getCityByInsee($value["maininseeContract"]);
                    if(empty($mainCity)){
                        $query = array("postalCodes.postalCode" => array('$in' => array($postalCode)));
                        //var_dump($query);
                        $mainCity = PHDB::findOne(City::COLLECTION, $query);
                        //var_dump($postalCode)
                        //var_dump($mainCity);
                        //var_dump($value["maininseeContract"]);
                        //exit;
                    }
                    $res=self::formatCityToZone($mainCity);
                    $scopes[$res["key"]]=$res["val"];
                    $mainScopeKey=$res["key"];
                       /* $query = array("postalCodes.postalCode" => array('$in' => array($value["postalCode"])));
                        //var_dump($query);
                        $mainCity = PHDB::find(City::COLLECTION, $query);
                        if(count($mainCity) > 1){
                            $countCity = count($mainCity);
                            $i = 1;
                            foreach($mainCity as $k => $v){
                                if(isset($value["maininseeContract"]) && !empty($value["maininseeContract"])){
                                    $value["maininseeContract"]=(strlen($value["maininseeContract"])==4) ? "0".$value["maininseeContract"] : $value["maininseeContract"];
                                    if($value["maininseeContract"]==$v["insee"])
                                        $mainCity=$v;
                                    else if ($countCity == $i) {
                                        $mainCity = $v;
                                    }
                                }else{
                                    $mainCity=$v;break;
                                }
                                $i++;
                            }
                        }else {
                            foreach ($mainCity as $key => $value) {
                                $mainCity = $value;
                            }
                        }

                        if(!empty($mainCity)){
                            $res=self::formatCityToZone($mainCity);
                            $scopes[$res["key"]]=$res["val"];
                        }*/
                            
                        
                    for ($i=1; $i <= 50; $i++) { 
                        $incInsee=($i+1);
                        if(isset($value["INSEE".$incInsee]) && !empty($value["INSEE".$incInsee])){
                            $value["INSEE".$incInsee]=(strlen($value["INSEE".$incInsee])==4) ? "0".$value["INSEE".$incInsee] : $value["INSEE".$incInsee];
                            $city=City::getCityByInsee($value["INSEE".$incInsee]);
                             if(!empty($city)){
                                //  foreach ($city as $key => $value) {
                            //    $city=$value;
                            //}
                                $res=self::formatCityToZone($city);
                                if ($city["insee"] != $value["maininseeContract"] && $res["key"] != $mainScopeKey) {
                                    self::removeCollectifByScope($res["key"]); 
                                }

                                $scopes[$res["key"]]=$res["val"];
                            }
                        }
                    }       
                
                    $where = array( "source.key" => "siteDuPactePourLaTransition",
                                    "type"=>"Group",
                                    '$or'=>array(
                                        array("email" => "pacte-".$postalCode."@listes.transition-citoyenne.org"),
                                        array("scope.".$mainScopeKey => array('$exists' => true))
                                    )
                            );
                    
                    $collectifActif = PHDB::findOne(Organization::COLLECTION, $where, array("name") );

                    if(!empty($collectifActif)){
                        $value["parent"]=array((string)$collectifActif["_id"]=>array("type"=>Organization::COLLECTION));

                        $set = array("category"=>"actif");
                        // TODO : faire les tests : 
                        //Test si name
                        if(isset($value["name"]) && !empty($value["name"])){
                             $set["name"] = $value["name"];
                        };
                        if(isset($scopes) && !empty($scopes)){
                             $set["scope"] = $scopes;
                        };
                        if(isset($value["siteWeb"]) && !empty($value["siteWeb"])){
                            $set["url"]=$value["siteWeb"];
                        }
                        if(isset($value["description"]) && !empty($value["description"])){
                            $set["description"]=$value["description"];
                        }
                      
                        //Test si email 
                         if(isset($value["email"]) && !empty($value["email"])){
                             $set["email"] = $value["email"];
                         };
                        //TestmailReferent
                        if(isset($value["referentEmail"]) && !empty($value["referentEmail"])){
                             $set["referentEmail"] = $value["referentEmail"];
                        }
                        if(isset($value["assosMembers"]) && !empty($value["assosMembers"])){
                             $set["localMembers"] = $value["assosMembers"];
                        }
                        if(isset($value["nextEvents"]) && !empty($value["nextEvents"])){
                             $set["nextEvents"] = $value["nextEvents"];
                        }
                        $slug=Slug::checkAndCreateSlug($value["name"],Organization::COLLECTION,(string)$collectifActif["_id"]);
                        Slug::save(Organization::COLLECTION,(string)$collectifActif["_id"],$slug);
                        $set["slug"]=$slug;
                        PHDB::update(Organization::COLLECTION, 
                            array("_id"=>$collectifActif["_id"]),
                            array('$set'=>$set, '$unset'=>array("links"=>true))
                        ); 

                    }
                            
                }
                
                $resElt = array(
                    "update" => false,
                    "id" => false,
                    "name" => @$value["name"]
                );
               
                
                $resultImport[] = $resElt;
                  
                //}  
            } 

            //TODO : Manque la condition pour dire que l'org n'es pas de catégorie actif
            //$count = PHDB::count(Organization::COLLECTION, array( "source.key" => "siteDuPactePourLaTransition"));
            //if($count != null) {
                PHDB::updateWithOptions(Organization::COLLECTION,
                    array("source.key" => "siteDuPactePourLaTransition",
                        "category"=> array('$nin'=>array("actif","actifSigned"))),
                    array('$set' => array("name" => "Collectif local du Pacte pour la Transition","category" => "soutien" )),
                    array('multiple' => true)
                ); 
            //}
        }

        $res["resultImport"] = $resultImport;
        
        return Rest::json($res); exit;
    }
  
    public static function removeCollectifByScope($key){
        PHDB::remove(Organization::COLLECTION, array( 
                "source.key" => "siteDuPactePourLaTransition",
                "scope.".$key => array('$exists'=>true),
                '$or'=>array(
                    array("category"=>array('$exists'=>false)),
                    array("category"=>"soutien")
                )
            ) 
        );
    }                        
  
    public static function formatCityToZone($city){
        if (!isset($city["name"])) {
            var_dump($city);exit;
        }
        
        $nameScope= ucwords(strtolower($city["name"]));
        $keyScope=(string)$city["_id"]."cities";
        $geoScope=$city["geo"];
        $geoPosition=$city["geoPosition"];
        $scopeCp=$city["postalCodes"][0]["postalCode"];  
        if(count($city["postalCodes"]) > 1){
            $keyScope.=$city["postalCodes"][0]["postalCode"];
            $nameScope=ucwords(strtolower($city["postalCodes"][0]["name"]));
            $geoScope=$city["postalCodes"][0]["geo"];
            $geoPosition=$city["postalCodes"][0]["geoPosition"];
        
            /*foreach ($city["postalCodes"] as $val){
                
            }*/
        }
        $zone=array(
            "city"=>(string)$city["_id"],
            "cityName"=>$nameScope,
            "key"=>$keyScope,
            "geo"=>$geoScope,
            "geoPosition"=>$geoPosition,
            "postalCode"=>$scopeCp,
            "level1" => isset($city["level1"]) ? $city["level1"] : "",
            "level1Name" => isset($city["level1Name"]) ? $city["level1Name"] : "",
            "country" => $city["country"],
            "type"=>"cities"
        );
        if(!empty($city["level4"])){
             $zone["level4"] = $city["level4"];
             $zone["level4Name"] = $city["level4Name"];
        }
        
        if(!empty($city["level3"])){
             $zone["level3"] = $city["level3"];
             $zone["level3Name"] = $city["level3Name"];
        }

        if(!empty($city["level2"])){
             $zone["level2"] = $city["level2"];
             $zone["level2Name"] = $city["level2Name"];
        }
        return array("key"=>$keyScope, "val"=>$zone);
    }

    public static function elementActionBeforeSave($data){
        $sessionUser=PHDB::findOne(Person::COLLECTION, array("email"=>"pacte@transition-citoyenne.org"));
        if(!Authorisation::isInterfaceAdmin()){
            Person::saveUserSessionData($sessionUser);
        }
    }
    public static function prepData($params){
        if($params["collection"]==Organization::COLLECTION && !empty($params["scope"])){
            $postalCodeForEmail="";
            foreach ($params["scope"] as $key => $value) {
                //$name = (isset($value["cityName"]) && !empty($value["cityName"])) ? ucfirst(strtolower($value["cityName"])) : $value["name"];
                //checkScope=$key;
                if(empty($postalCodeForEmail))
                    $postalCodeForEmail=$value["postalCode"];
                $city = City::getById($value["city"], array("name","postalCodes", "geo", "geoPosition"));
                $params["scope"][$key]["geo"]=$city["geo"];
                $params["scope"][$key]["geoPosition"]=$city["geoPosition"];
                //$scopeToRegister=$_POST["scope"];
                //var_dump($city);
                //if(count($city["postalCodes"]) > 1){
                 //   $postalCode=$city["postalCodes"][0]["postalCode"];
                //  $name = (isset($city["name"])) ? $city["name"] : $name;
                 //   $checkScope=(string)$city["_id"]."cities".$postalCode;
                 //   $scopeToRegister=array($checkScope=>$value);
                //}
            }
            if(empty($params["email"]) || strrpos($params["email"], "@listes.transition-citoyenne.org") == false )
                $params["email"] = "pacte-".mb_strtolower($postalCodeForEmail)."@listes.transition-citoyenne.org";    
        }
        return $params;
    }
    public static function elementBeforeSave($params){
        if(in_array($params["collection"],[Poi::COLLECTION, Organization::COLLECTION]) && isset($params["elt"]["source"])){
            $exists = PHDB::findOne($params["collection"],array("_id"=>new MongoId($params["id"])));
            if(!empty($exists))
                unset($params["elt"]["source"]);
        }
        return $params;
       
    }
    public static function elementAfterUpdate($data){
        if($data["collection"]==Poi::COLLECTION && $data["params"]["type"]=="contract"){
            self::processAfterContract($data);
        }
    }
    public static function elementAfterSave($data){
        if($data["collection"]==Poi::COLLECTION && $data["params"]["type"]=="contract"){
            $elt=Element::getElementById($data["id"], $data["collection"], null,array("source", "parent"));
            $elt["source"]["toBeValidated"]=true;
            PHDB::update(Poi::COLLECTION,
                    array("_id"=>new MongoId($data["id"])),
                    array('$set'=>array("source"=>$elt["source"])));
            foreach($elt["parent"] as $k => $v){
                $collectif=Element::getElementById($k, Organization::COLLECTION,null,array("slug", "name"));
                break;    
            }
            $objMail="Un nouveau contrat a été ajouté ".$data["params"]["name"];
            $html = "<span>Un contrat a été ajouté sur la page du collectif <b>".$collectif["name"]."</b></span><br/><br/>".
                "<span>Validez le sur l’<a href='https://www.pacte-transition.org/#admin.view.contract' target='_blank' style='color:#5b2649'>espace admin</a> pour qu’il apparaisse sur la carte du Pacte.</span><br/><br/>".
                "<span>La page du collectif <a href='https://www.pacte-transition.org/#@".$collectif["slug"].".edit.".(string)$collectif["_id"]."' target='_blank' style='color:#5b2649'>(lien)</a></span><br/>";
            $paramsMails = array("tplMail" => "pacte@transition-citoyenne.org",
                                "tplObject" => $objMail,
                                "tpl" => "basic",
                                "html" => $html);
            Mail::createAndSend($paramsMails);
            self::processAfterContract($data);

            
        }
        if($data["collection"]==Organization::COLLECTION){
            $elt=Element::getElementById($data["id"], $data["collection"], null,array("source"));
            $elt["source"]["toBeValidated"]=true;
            PHDB::update(Organization::COLLECTION,
                    array("_id"=>new MongoId($data["id"])),
                    array('$set'=>array("category"=>"actif", "source"=>$elt["source"])));
            // Mail Au Admin du pacte pour valider le collectif
            $objMail="Nouveau collectif à valider ".$data["params"]["name"];
            $html = "<span>Une page collectif a été créée : ".$data["params"]["name"]."</span><br/><br/>".
            "<span>Vérifiez que la liste de diffusion <b style='color:red;'>".$data["params"]["email"]."<b/> [mail référent : ".$data["params"]["referentMail"]."] ait bien été créée, puis validez l’ajout de la page sur l’espace admin. <br/><br/></span>".
            "<span>La page provisoire du collectif <a href='https://www.pacte-transition.org/#page.type.".Organization::COLLECTION.".id.".$data["id"].".edit.".$data["id"]."' target='_blank' style='color:#5b2649'>(lien)</a></span><br/>";
            $paramsMails = array("tplMail" => "pacte@transition-citoyenne.org",
                                "tplObject" => $objMail,
                                "tpl" => "basic",
                                "html" => $html);
            Mail::createAndSend($paramsMails);
            // Mail au référent du collectif
            $objMail="[Pacte pour la transition] Lien de modification de votre collectif: ".$data["params"]["name"];
            $html = "<span>Nous avons bien pris en compte l’ajout de votre collectif actif sur le site du Pacte pour la Transition.</span><br/><br/>".
                    "<span>Nous allons le valider au plus vite afin qu’il apparaisse sur la carte interactive du site et sur ce lien :</span><br/>".
                    "<a href='https://www.pacte-transition.org/#@".$data["params"]["slug"]."' target='_blank' style='color:#5b2649'>www.pacte-transition.org/#@".$data["params"]["slug"]."</a><br/><br>".
                    "<span>En attendant, vous pouvez modifier votre page collectif grâce à ce lien <strong>(à conserver précieusement et à ne pas diffuser)</strong> : </span><br/>".
                    "<a href='https://www.pacte-transition.org/#@".$data["params"]["slug"].".edit.".$data["id"]."' target='_blank' style='color:#5b2649'>www.pacte-transition.org/#@".$data["params"]["slug"].".edit.".$data["id"]."</a><br/>";
                    
            $paramsMails = array("tplMail" => $data["params"]["referentMail"],
                                "tplObject" => $objMail,
                                "tpl" => "basic",
                                "html" => $html);
            Mail::createAndSend($paramsMails);
        
        }
        // var_dump($data);
    }
    public static function prepDataForUpdate($data){
        if($data["collection"]==Poi::COLLECTION && $data["type"]=="contract"){
            if(isset($data["links"]["measures"]) && !empty($data["links"]["measures"])){
                $measuresPacte=PHDB::find(Poi::COLLECTION, array( "source.key" => "siteDuPactePourLaTransition",
                                        "type" => "measure"), array("name"));
                $data["measures"]=array();
                foreach($data["links"]["measures"] as $k => $v){
                    $data["measures"][$k]=$v;
                    $data["measures"][$k]["name"]=$measuresPacte[$k]["name"];

                }
            }
        }
        return $data;
    }
    public static function processAfterContract($data){
        if(isset($data["params"]["measures"])){
            PHDB::update($data["collection"], 
                array("_id"=> new MongoId($data["id"])), 
                array('$set'=>array("links"=>array("measures"=>$data["params"]["measures"])), '$unset'=>array("measures"=>1)));

        }
        if(isset($data["params"]["parent"])){
            foreach($data["params"]["parent"] as $e => $v){
                $orga=Element::getElementById($data["id"], $data["collection"], null,array("links"));
                $links=array();
                if(!empty($orga["links"])){
                    if(isset($orga["links"]["contracts"]))
                        $orga["links"]["contracts"][$data["id"]]=array("type"=>Poi::COLLECTION);
                    else
                        $orga["links"]["contracts"]=array($data["id"]=>array("type"=>Poi::COLLECTION));
                    $links=$orga["links"];
                }else
                    $links=array("contracts"=>array($data["id"]=>array("type"=>Poi::COLLECTION)));
                PHDB::update(Organization::COLLECTION,
                    array("_id"=>new MongoId($e)),
                    array('$set'=>array("links"=>$links, "category"=>"actifSigned")));
                break;
            }
        }
        
    }

    
    public static function getActus($params){

        $results["dataArticles"] = false;

        $where = array(
            "source.key" => $params,
            "type"   =>  "article"
        );
        
        $actualite = PHDB::findAndLimitAndIndex(Poi::COLLECTION,$where,4);
        
        if (!empty($actualite)) {
            $results["dataArticles"] = true;

            $resActu = array();

            $resActu = self::createResultsActus($actualite);

            return array_merge($results,$resActu);
        }
        return $results;
    }

    private static function createResultsActus($actualite){
        $data["article"] = array();

        foreach ($actualite as $key => $value) {
            $imgMedium = (!empty($value["profilMediumImageUrl"])) ? $value["profilMediumImageUrl"] : "Aucune image pour cette actus"; 

            $data["article"][] = [
                "id"                =>  (string) $value["_id"],
                "name"              =>  $value["name"],
                "collection"        =>  $value["collection"],
                "shortDescription"  =>  $value["shortDescription"],
                "imgMedium"         =>  $imgMedium
            ]; 
        }
        return $data;
    }


    public static function array_find($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            if (false !== stripos($value["name"], $needle)) {
                return $key;
            }
        }
        return false;
    }
}
?>