<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\InvitationLink;
use ReseauTierslieux;
use Organization;
use CacheHelper;
use Event;
use Notification;
use MongoId;
use ActStr;
use Poi;
use Link;

class Lakou {

    const COLLECTION = "costums";
    const CONTROLLER = "costum";
    const MODULE = "costum";

    // public static function handleInvitationByLink($params){
    //     $user = PHDB::findOneById(Person::COLLECTION, $params["userId"], array("links"));
    //     $target = PHDB::findOneById( $params["targetType"], $params["targetId"], array("slug"));
    //     if($target["slug"] != "ekisphere"){
    //         $ekitia = PHDB::findOne(Organization::COLLECTION, array("slug" => "ekisphere"));
    //         $ekitia["links"]["members"][$params["userId"]] = array("type" => Person::COLLECTION);
    //         $user["links"]["memberOf"][(string) $ekitia['_id']] = array("type" => Organization::COLLECTION);
    //         $queryEkitia = array("links.members" => $ekitia["links"]["members"]);
    //         $query= array("links.memberOf" => $user["links"]["memberOf"]);
    //         $resEktia = PHDB::update( Organization::COLLECTION,
    //             array("slug" => "ekisphere"),
    //             array('$set'=>$queryEkitia)
    //         );
    //         $resUser = PHDB::update( Person::COLLECTION,
    //             array("_id" => new MongoId($params["userId"])),
    //             array('$set'=>$query)
    //         );
    //     }
    // }

    public static function canAccessAnswer($params){
        $canAccess = false;
        $userSession = Yii::app()->session["userId"];

        if($userSession){
            $canAccess = true;
        }
        
        return $canAccess;
    }




	// public static function prepImportData($data){
	// 	if(empty($data["value"])){
	// 		// var_dump($data);exit;
	// 	}
	// 	if($data["valueConfig"]["valueAttributeElt"]=="name"){
	// 			// $firstName=$data["valueFile"][$data["valueConfig"]["idHeadCSV"]+1];
	// 			// var_dump($data["value"]);exit;
	// 			$data["element"]["surname"]=$data["value"];
				
	// 		//    var_dump($valueData);exit;
	// 	}else if($data["valueConfig"]["valueAttributeElt"]=="firstName"){
	// 		$data["element"]["name"]=trim($data["element"]["name"]." ".$data["value"]);
	// 	}
		
	// 	if(!isset($data["element"]["tags"])){
	// 			$data["element"]["tags"]=[];
	// 	}else if(!in_array("ContactlaCompagnieDesTierslieux",$data["element"]["tags"])){
	// 		$data["element"]["tags"][]="ContactlaCompagnieDesTierslieux";
	// 	}
	// 	return $data["element"];
	// }

    public static function elementAfterSave($data){
        $costum=CacheHelper::getCostum();
        $elem=PHDB::findOne($data["collection"],array("_id"=>new MongoId($data["id"])));
        // var_dump($elem);exit;
        if($data["collection"]==Organization::COLLECTION){
            if(isset($data["params"]["mainTag"]) && $data["params"]["mainTag"]=="TiersLieux"){
                // var_dump("here");
                $tlnetwork=PHDB::findOne(Organization::COLLECTION,array("network"=>"ANCTNetwork","address.addressCountry"=>"GP"));
                if(!(isset($data["params"]["reference"]["costum"]) && in_array($tlnetwork["slug"],$data["params"]["reference"]["costum"]))){
                    // var_dump("inside"); 
                    ReseauTiersLieux::elementAfterSave($data);
                    //add reference in network
                    Admin::addSourceInElement($data["id"],$data["collection"],$tlnetwork["slug"],"reference");
                    //avoid adding the third place in lakou's community by default
                    Link::disconnect($costum["contextId"], $costum["contextType"], $data["id"], $data["collection"], Yii::app()->session["userId"], "members");
                    Link::disconnect( $data["id"], $data["collection"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
                    
                    // Assure adding the third place in lakou's reseauRegionalDesTierslieuxGuadeloupe network by default
                    Link::connect((string)$tlnetwork["_id"], $tlnetwork["collection"], $data["id"], $data["collection"], Yii::app()->session["userId"], "members");
                    Link::connect( $data["id"], $data["collection"], (string)$tlnetwork["_id"], $tlnetwork["collection"], Yii::app()->session["userId"], "memberOf");
                    // var_dump($tlnetwork);exit;
                    Admin::addSourceInElement($data["id"],$data["collection"],$tlnetwork["slug"],"reference");
                }
            }else{
                $child=[];
                $child["childId"]=$data["id"];
                $child["childType"]=$data["collection"];
                // Link::connect($costum["contextId"], $costum["contextType"], $child["childId"], $child["childType"], Yii::app()->session["userId"], "members");
                // Link::connect($child["childId"], $child["childType"], $costum["contextId"], $costum["contextType"], Yii::app()->session["userId"], "memberOf");
            }
            // exit;
        }

        if($data["collection"]!==Organization::COLLECTION){
            // var_dump($data["params"],$elem);exit;
            //set mail notifs towards members of Lakou for random events (lakou is not the organizer)
            if((!empty($data["params"]["parent"]) && array_keys($data["params"]["parent"])[0]!==$costum["contextId"]) || (!empty($data["params"]["organizer"]) && array_keys($data["params"]["organizer"])[0]!==$costum["contextId"])){
                $memberToNotif=Element::getCommunityByTypeAndId($costum["contextType"], $costum["contextId"], Person::COLLECTION, null, null, array("type" => "mails", "value" => "default"),array("_id","email","username","language"));
         		$memberEmails=[];
         		// foreach($memberToNotif as $ida=>$va){
                //     $memberEmails[$ida]=array("_id"=> new MongoId($ida),"email"=>$va["email"],"username"=>$va["username"],"language"=>$va["language"] ?? null);
         		// 	// array_push($memberEmails,$va["email"]);
         		// }
                

                //  (ActStr::VERB_ADD, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$targetType,"id"=> $targetId), array("id"=>$id,"type"=> $collection), $collection);


                $verb=ActStr::VERB_ADD;
                $levelType=$data["collection"];
                $notificationPart = Notification::$notificationTree[$verb];

                $notificationPart["verb"] = $verb;
                $notificationPart["target"] = array("type"=>$costum["contextType"],"id"=> $costum["contextId"]);
                $notificationPart["object"] = array("id"=>$data["id"],"type"=> $data["collection"]);
                $notificationPart["levelType"] = $levelType;
                $notificationPart["value"] = null;
                $notificationPart["author"] = array(
                    "id" => Yii::app()->session["userId"],
                    "name" => Yii::app()->session["user"]["name"],
                    "type" => Person::COLLECTION,
                    "profilThumbImageUrl" => Yii::app()->session["user"]["profilThumbImageUrl"] ?? null
                );
                $eltTarget = Element::getElementById($notificationPart["target"]["id"], $notificationPart["target"]["type"], null, array("name", "title", "slug", "profilThumbImageUrl"));
			    $notificationPart["target"] = array_merge($notificationPart["target"], $eltTarget);
                $eltObject = Element::getElementById($notificationPart["object"]["id"], $notificationPart["object"]["type"], null, array("name", "title", "text", "slug", "profilMediumImageUrl", "shortDescription"));
			    $notificationPart["object"] = array_merge($notificationPart["object"], $eltObject);
			    $object = $notificationPart["object"];
                $notificationPart["labelUpNotifyTarget"] = "author";
		        $notificationPart["notifyCommunity"] = true;
                $notificationPart["community"] = array("mails"=>$memberToNotif);
                $tpl = $notificationPart["tpl"] ?? null;

                // if (
                //     in_array("author", $notificationPart["labelArray"]) &&
                //     (!in_array($notificationPart["verb"], [Actstr::VERB_ADD]) ||
                //         (@$notificationPart["levelType"] && $notificationPart["levelType"] == "asMember"))
                // )
                // {
                //     $notificationPart["object"] = array("id" => $authorId, "type" => Person::COLLECTION, "name" => $author["name"], "profilThumbImageUrl" => @$eltauthor["profilThumbImageUrl"]);
                //     $notificationPart["author"] = array("id" => Yii::app()->session["userId"], "type" => Person::COLLECTION, "name" => "Test", "profilThumbImageUrl" => @Yii::app()->session["user"]["profilThumbImageUrl"]);
                //     $notificationPart["labelUpNotifyTarget"] = "object";
                // }
    
                // if ($notificationPart["verb"] == Actstr::VERB_COMMENT && $notificationPart["levelType"] == Comment::COLLECTION)
                //     $notificationPart["levelType"] = $notificationPart["target"]["type"];
    
                // //Check if the notification indicates a repeat system 
                // if ((@$notificationPart["repeat"] && $notificationPart["repeat"]) ||
                //     (@$notificationPart["type"] &&
                //         @$notificationPart["type"][$levelType] &&
                //         @$notificationPart["type"][$levelType]["repeat"])
                // ){
                //     $update = Notification::checkIfAlreadyNotifForAnotherLink($notificationPart);
                // }


    			if (isset($notificationPart["type"]) && isset($notificationPart["type"][$levelType]))
    			{
    				if (isset($notificationPart["type"][$levelType]["tpl"]))
    					$tpl = $notificationPart["type"][$levelType]["tpl"];
    				if (
    					$notificationPart["labelUpNotifyTarget"] == "object" &&
    					!empty($notificationPart["type"][$levelType][$notificationPart["object"]["type"]]) &&
    					!empty($notificationPart["type"][$levelType][$notificationPart["object"]["type"]]["tpl"])
    				)
    					$tpl = $notificationPart["type"][$levelType][$notificationPart["object"]["type"]]["tpl"];
    			}
    
                Mail::createNotification($notificationPart, $tpl);

            }


        }
        // var_dump($data);exit;
	}

    public static function mailCreateParamsTpl($params){
        $globalParams=$params["globalParams"];
        $dataObj=$params["dataObj"];
        $urlSuffix="";    

            foreach($dataObj["data"] as $ind=> $data){
                $urlSuffix="";
                if(!empty($data["object"]["type"])){
                    if($data["object"]["type"]==Event::COLLECTION){
                        $urlSuffix=".view.agenda";
                    }else if($data["object"]["type"]==Poi::COLLECTION){
                        $urlSuffix=".view.directory.dir.poi";
                    }
                    
                }else if(!empty($data["verb"]) && $data["verb"]==ActStr::VERB_POST){
                    $urlSuffix=".view.newspaper";
                }

                
                foreach($data["labelArray"]['{where}'] as $lab => $vallab){
                    if($vallab["type"]==Organization::COLLECTION){
                        if(str_contains($dataObj["data"][$ind]["labelArray"]['{where}'][$lab]["url"],$urlSuffix)===false){
                            $dataObj["data"][$ind]["labelArray"]['{where}'][$lab]["url"]=$dataObj["data"][$ind]["labelArray"]['{where}'][$lab]["url"].$urlSuffix;
                        }    
                    }
                }
                if($data["verb"]==ActStr::VERB_ADD){
                    foreach($data["labelArray"]['{what}'] as $lab => $vallab){
                        if(str_contains($vallab["url"],"?preview")===false){
                            $hashT=explode(".",$vallab["url"]);
                            $urlPreview="?preview=".$hashT[2].".".$hashT[4];
                            $hash="#welcome";
                            $base=explode("#",$vallab["url"]);
                            if($vallab["type"]==Event::COLLECTION){
                                $hash="#events";
                            }else if($vallab["type"]==Poi::COLLECTION){
                                $hash="#boites-a-outils";
                            }else if($vallab["type"]==Organization::COLLECTION){
                                $hash="#search";
                            }
                            $dataObj["data"][$ind]["labelArray"]['{what}'][$lab]["url"]=$base[0].$hash.$urlPreview;
                        }    
                    }

                }
            }    
          

        // }
        // var_dump($dataObj["data"][0]["labelArray"]['{where}']);exit;
        
        return array("globalParams"=>$globalParams,"dataObj"=>$dataObj);
        
    }

    const TRAD = array(
        "{who} added a new point of interest : {what} on {where}" => "{who} a ajouté une ressource ou un outil : {what} dans {where}",
        "{who} have added points of interest on {where}"=>"{who} ont ajouté des ressources ou des outils sur {where}",
        "{who} added points of interest on {where}"=>"{who} a ajouté des ressources ou des outils sur {where}",
    );

    public static function translateLabel($data){
        // var_dump($data);exit;
        $res = null;
        if( !empty($data["label"]) && array_key_exists($data["label"], self::TRAD) ) {
            $data["label"] = self::TRAD[ $data["label"] ];
        }
        return $data;
    }

	

}
