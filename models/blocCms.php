<?php 
class blocCms {
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	public static function getCms($post){
		$cms = Element::getElementById($post["ider"],Cms::COLLECTION);
		return $cms;
	}
	public static function getCmsExist($post){
		$cmsList = PHDB::find(Cms::COLLECTION, 
			array( "parent.".$post["contextId"] => array('$exists'=>1))
		);
		$res["element"] = array();
		foreach ($cmsList as $key => $value) {
			array_push($res["element"],array(
				"id"  =>  (String) $value["_id"]
			));
		}
		return array_merge($res);
	}
	public static function getProperties($post){
		$cmsListe = PHDB::find(Cms::COLLECTION, 
			array( "parent.".$post["contextId"] => array('$exists'=>1))
		);
		return $cmsListe;
	}
	public static function getCmsStaticExist($post){
		$cmsList = PHDB::find(Cms::COLLECTION, 
			array( "parent.".$post["contextId"] => array('$exists'=>1),
				"page" => $post["page"]
		)
		);
		$res["element"] = array();
		foreach ($cmsList as $key => $value) {
			array_push($res["element"],array(
				"id"  =>  (String) $value["_id"]
			));
		}
		return array_merge($res);
	}
	public static function getlistTemplateStatic($post){
		$listTemplateStatic = PHDB::find(Cms::COLLECTION, 
			array( "name"=>$post["nameTpl"],"parent.".$post["contextId"] => array('$exists'=>1),
				"tplParent" => $post["idTplParent"])
		);
		$res["element"] = array();
		foreach ($listTemplateStatic as $key => $value) {
			array_push($res["element"],array(
				"id"  =>  (String) $value["_id"]
			));
		}
		return array_merge($res);
	}

 	public static function getTemplateByCategory($post){
		$tplList = PHDB::find(Cms::COLLECTION, 
			array( "type" => $post["type"], "category" => $post["category"])
		);
		return $tplList;
	}

	public static function getNameCreator($post){
		$name = Element::getElementSimpleById($post["idCreator"],"citoyens");
		return $name["name"];
	}
	// public static function getVideo($post){
	// 	$name = Element::getElementSimpleById($post["idCreator"],"citoyens");
	// 	return $name["name"];
	// }


	
}
?>
