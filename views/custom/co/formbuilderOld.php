<?php 	
//var_dump($answer);
$saveOneByOne = (isset($saveOneByOne)) ? $saveOneByOne :false;
if( isset( $form["inputs"] ) ){ ?>
<form id="formQuest">
	<?php 
	$initValues = [];
	$ct = 1;
	
	$orderInputs = [];
	$orderInputsKeys = [];
	foreach ( $form["inputs"] as $key => $input) {
		if( stripos( $input["type"] , "tpls.forms.cplx" ) !== false )
			$saveOneByOne = true;
		if(isset($input["position"])){
			$orderInputs[(int)$input["position"]] = $input;
			$orderInputsKeys[(int)$input["position"]] = $key;
		}
	}


    $inV = $form["inputs"];
    foreach ( $form["inputs"] as $key => $input) {
        if(!isset($input["position"])){
            unset( $form["inputs"][$key]);
        }else{
            unset( $inV[$key]);
        }
    }

    //nouvelle repositionnement


    if(!function_exists('sortByPosOrder'))
    {
        function sortByPosOrder($a, $b) {
            return (int)$a['position'] - (int)$b['position'];
        }
    }

     uasort($form["inputs"], 'sortByPosOrder');



    $form["inputs"] = array_merge($form["inputs"], $inV);


    //fin nouvelle repositionnement


	echo "<ul class='questionList'>";
	foreach ( $form["inputs"] as $key => $input) { 

        var_dump($canEdit);
		$editQuestionBtn = ($canEdit) ? " <a class='btn btn-xs btn-danger editQuestion' href='javascript:;' data-form='".$form["id"]."' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='".$key."' data-path='inputs.".$key."'><i class='fa fa-pencil'></i></a>".
					" <a class='btn btn-xs btn-danger deleteLine' href='javascript:;' data-id='".$form["_id"]."' data-collection='".Form::COLLECTION."' data-key='question".$key."' data-path='inputs.".$key."'><i class='fa fa-trash'></i></a>" : "xx";

		$tpl = $input["type"];
		if(in_array($tpl, ["textarea","markdown","wysiwyg"]))
			$tpl = "tpls.forms.textarea";
		else if(empty($tpl) || in_array($tpl, ["text","button","color","date","datetime-local","email","image","month","number","radio","range","tel","time","url","week","tags"]))
			$tpl = "tpls.forms.text";
		else if(in_array($tpl, ["sectionTitle"]))
			$tpl = "tpls.forms.".$tpl;

		if( stripos( $tpl , "tpls.forms." ) !== false )
			$tplT = explode(".", $input["type"]);

		$kunikT = explode( ".", $input["type"]);
		$keyTpl = ( count($kunikT)>1 ) ? $kunikT[ count($kunikT)-1 ] : $input["type"];
		$kunik = $keyTpl.$key;
		$answers = null;
		$answerPath = "answers.".$kunik.".";
		if($wizard){
			$answerPath = "answers.".$form["id"].".".$kunik.".";
			if( isset($answer["answers"][$form["id"]][$kunik]) && count($answer["answers"][$form["id"]][$kunik])>0 )
				$answers = $answer["answers"][$form["id"]][$kunik];
		}
		else {
			if(isset($answer["answers"][$kunik]))
				$answers = $answer["answers"][$kunik];
		}
		// var_dump($keyTpl);
		// var_dump($answer['user']);

		$p = [ 
			"input" 	=> $input,
			"type"		=> $input["type"], 
			"answerPath" => "answers.".$kunik.".",
			"answer" 		=> $answer,
			"answers" 	=> $answers ,//sub answers for this input
		  	"label" 	=> $input["label"] ,//$ct." - ".$input["label"] ,
		  	"titleColor"=> "#16A9B1",
		  	"info" 		=> isset($input["info"]) ? $input["info"] : "" ,
		  	"placeholder" => isset($input["placeholder"]) ? $input["placeholder"] : "" ,
		  	"form"		=> $form,
			"key"		=> $key, 
			"kunik" 	=> $kunik,
			"canEdit"	=> $canEdit,
			"editQuestionBtn" => $editQuestionBtn,
			"saveOneByOne" => $saveOneByOne,
			"wizard" 	=> (isset($wizard) && $wizard == true) ,
			"el" => $el  ];

		if(isset($tplT[2]) && $tplT[2] == "select" && isset($input["options"]))
			$p["options"] = $input["options"];
    	
    	if($input["type"] == "tags") 
			$initValues[$key] = $input;
		
		echo "<li id='question".$key."' class='questionBlock'  data-id='".$form['_id']."' data-path='inputs.".$key.".position' data-key='".$key."' data-form='".$form["id"]."' style='margin-bottom:0px'>";
    		echo $this->renderPartial( "costum.views.".$tpl , $p );
    	echo "</li>";
		
	 	$ct++; 
	} 	
	echo "</ul>";
	$id = (!empty($answer)) ? " data-id='".$answer["_id"]."' " : "";
	
	//if form contains just one cplx input then all saves are made oneByone
	if(!$saveOneByOne){ ?>
	<a href="javascript:;" id="openFormSubmit" <?php echo $id ?> class=" btn btn-primary">Envoyer</a>
	<?php } ?>
</form>
<?php }

if ($canEdit) { ?>
<div class="text-center">   
	<a href="javascript:;" class="addQuestion btn btn-danger" data-form="<?php echo $formId ?>"  data-id="<?php echo $form['_id'] ?>" ><i class="fa fa-plus"></i> Ajouter Une Question</a>
</div>
<?php } ?>

<script type="text/javascript">

var initValues = <?php echo (!empty($initValues)) ?json_encode( $initValues ) : "null"; ?>;

jQuery(document).ready(function() {

    mylog.log("render","/modules/costum/views/custom/co/formbuilder.php");
    
 	<?php if(!$saveOneByOne){ 
 		 //if form contains just one cplx input then all saves are made oneByone?>
	$( "#openFormSubmit" ).off().on( "click", function( event ) {
	  event.preventDefault();
	  $("#openFormSubmit").html("<i class='fa fa-spin fa-spinner'></i>Veuillez Patienter nous traitons vos données")
	  var answer = {
	  	value:{
	  		formId : "<?php echo $formId ?>",
	  		type : "openForm",
	  		"answers":{},
	  		user : userId,
	  		parentSlug : "<?php echo $el["slug"] ?>"
	  	}
	  };
	  $("form#formQuest :input").each(function(){
	  	if(typeof $(this).attr("id") != "undefined" && $(this).val() != "" )
	  		answer.value.answers[ $(this).attr("id") ] = $(this).val();
		});
	  console.log("#openFormSubmit", answer );
	  answer.collection = "answers";
	  if($(this).data("id"))
	  	answer.id = $(this).data("id");
	  
	  dataHelper.path2Value( answer , function(params) { 
			$("form#formQuest").html("<h2>Merci de votre participation!!</h2><a href='javascript:;'>Voir toutes les réponses</a>");
			//location.reload();
		} );
	});
	<?php } else {
		//here answer allready exists is only completed by small updates
		?>

	$( ".saveOneByOne" ).off().on( "blur", function( event ) {
		event.preventDefault();
		//toastr.info('saving...'+$(this).attr("id"));
		//$('#openForm118').parent().children("label").children("h4").css('color',"green")
    	if( notNull(answerObj)){

			var answer = {
			  	collection : "answers",
			  	id : answerObj._id.$id,
			  	path : "answers."+$(this).attr("id")
			};

			if(answerObj.formList)
	  			answer.path = "answers."+$(this).data("form")+"."+$(this).attr("id");	  		
			  
			if($(this).attr("type") == "checkbox")
			  	answer.value = $(this).is(":checked");
			else
			  	answer.value = $(this).val();
			  
			console.log("saveOneByOne",$(this).attr("id"), answer );
			  
			dataHelper.path2Value( answer , function(params) { 
					toastr.success('saved');
				} );
		} else {
			toastr.error('answer cannot be empty, on saveOneByOne!');
		}
	});

	<?php } ?>

	if(initValues){
		$.each(initValues, function(k,inp) { 
			if(inp.type == "tags"){
				$("#"+k).select2({
				    "tags": inp.data ,
					"tokenSeparators": [','],
				    //"minimumInputLength" : (inp.limit) ? inp.limit : 3,
				    "placeholder" : inp.placeholder
				});
			}
		})
	}


<?php if($canEdit) { ?>
    var arrayPosition = [];
    var arrayMissingV = 0;
    var i = 1;
    arrayPosition.push(0);
    $.each(formInputs, function (index, value) {
        if(value.position !== undefined){
            arrayPosition.push(parseInt(value.position));
        } else {
            arrayMissingV++;
        }
    });

    var maxValueInArray = Math.max.apply(Math, arrayPosition);

    if(arrayMissingV !== 0){
        $.each(formInputs, function (index, value) {
            if(arrayMissingV !== 0){
                if(value.position === undefined){
                    value.position = (maxValueInArray + i).toString();
                    i++;
                    arrayMissingV--;
                }
            }
        });
    }

	//questions can be ordered by drag n drop
	$( ".questionList" ).sortable({
      	stop: function(event, ui) {
        	//alert("New position: " + ui.item.index()+ ui.item.data("key")+ ui.item.data("form"));
        	tplCtx.id = ui.item.data("id");
	        tplCtx.collection = "forms";            
	        tplCtx.path = ui.item.data("path");
	        tplCtx.key = ui.item.data("key");
	        tplCtx.form = ui.item.data("form");
	        tplCtx.value = ""+ui.item.index();

	        //apres la ligne 186
	        //il faut traverser tout les formInputs
	        //et si formInputs.xxx.position est >= tplCtx.value alors 
	        //formInputs.xxx.position = formInputs.xxx.position+1

			console.dir(tplCtx );
			  
			  dataHelper.path2Value( tplCtx , function(params) { 
					toastr.success('move saved!');


                  var distance =0;
                  if(parseInt(tplCtx.value) + 1 < parseInt(formInputs[tplCtx.key]["position"]) ){ // Go up
                      distance = parseInt(formInputs[tplCtx.key]["position"]) - parseInt(tplCtx.value);
                      distance = distance - 1;
                          $.each(formInputs, function (index, value) {
                              if (distance !== 0){
                                  if (value.position >= (parseInt(tplCtx.value) + 1).toString()) {
                                      value.position = (parseInt(value.position) + 1).toString();
                                      distance--;
                                  }
                              }
                          });
                      formInputs[tplCtx.key]["position"] = (parseInt(tplCtx.value)+1).toString();
                  }else { // Go down
                      distance = parseInt(tplCtx.value) - parseInt(formInputs[tplCtx.key]["position"]);
                      distance = distance + 1;
                      for (let [key, value] of Object.entries(formInputs).reverse()) {
                          if (distance !== 0){
                              if (value.position <= (parseInt(tplCtx.value) + 1).toString()) {
                                  value.position = (parseInt(value.position) - 1).toString();
                                  distance--;
                              }
                          }
                      }
                      formInputs[tplCtx.key]["position"] = (parseInt(tplCtx.value)+1).toString();
                  }



                  tplCtx.path = "inputs";
                  tplCtx.value = formInputs;
                  dataHelper.path2Value( tplCtx , function(params) {
                      toastr.success('edit saved!');
                  });


                  //formInputs[tplCtx.form][tplCtx.key]["position"] = tplCtx.value;
					var clearDoublePos = false;
					//if input exists on same position delete position attribute
					$.each( formInputs[tplCtx.form], function(k,inp) {
			    		console.log("incPositions",k,inp.position)
						if( k != tplCtx.key && inp.position == tplCtx.value ){
							clearDoublePos = true;

							tplCtx.path = "inputs."+k+".position";
							tplCtx.value = null;
							dataHelper.path2Value( tplCtx , function(params) { 
								delete formInputs[tplCtx.form][k]["position"];
							} );
						}
					})
					if(clearDoublePos)
						toastr.info('conflicting positions , please reload the page!');
				} );

    	}
    });

	$(".editQuestion").off().on("click",function() { 

		var activeForm = {
            "jsonSchema" : {
                "title" : "Edit Question config",
                "type" : "object",
                "properties" : {
                    
                }
            }
        };
    	formInputsHere = formInputs;
    	if( notNull(formInputs [ $(this).data("form")]) )
        	formInputsHere = formInputs[  $(this).data("form") ];
        // if( typeof formInputsHere [ $(this).data("key") ].position == "undefined")
        // 	formInputsHere [ $(this).data("key") ].position = "";

    	$.each( formInputsHere [ $(this).data("key") ] , function(k,val) { 
    		mylog.log("formInputs",k,val); 
    		activeForm.jsonSchema.properties[ k ] = {
    			label : k,
    			value : val
    		};
    	});

        
        mylog.log("tplCtx formInputs activeForm.jsonSchema.properties",formInputsHere,activeForm.jsonSchema.properties); 
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path");
        mylog.log("tplCtx",tplCtx); 
        //alert(tplCtx.collection+":"+tplCtx.path);
        activeForm.jsonSchema.save = function () {  
            tplCtx.value = {};
            $.each ( formInputsHere [ tplCtx.key ] , function(k,val) { 
        		tplCtx.value[k] = $("#"+k).val();
        	} );
            
            //alert("#"+tplCtx.key+" : "+$( "#"+tplCtx.key ).val());
            console.log("save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    location.reload();
                } );
            }
    	} 
        dyFObj.openForm( activeForm );
    });

    $(".addQuestion").off().on("click",function() { 
        var activeForm = {
            "jsonSchema" : {
                "title" : "Ajouter une Question",
                "type" : "object",
                "properties" : {
                    label : { label : "Titre de la Question" },
                    placeholder : { label : "Texte dans la Question" },
                    info : { 
                    	inputType : "textarea",
                    	label : "Information complémentaire sur la Question" },
                    type : { label : "Type de cette question",
                    		 inputType : "select",
                    		 options : <?php echo json_encode(Form::inputTypes()); ?>,
                    		 value : "text" }
                }
            }
        };          
        
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = "<?php echo Form::COLLECTION ?>";  
        tplCtx.form = $(this).data("form");           
        
        activeForm.jsonSchema.save = function () {  
        	var formInputsHere = formInputs;
        	if( notNull(formInputs [tplCtx.form]) )
        		formInputsHere = formInputs[ tplCtx.form ];

        	var inputCt = (formInputsHere == null) ? "1" : (Object.keys(formInputsHere).length+1);
        	if(notNull(formInputsHere[tplCtx.form+inputCt]))
        		inputCt = inputCt+"x";
        	//alert("inputs."+tplCtx.form+inputCt);
        	
            tplCtx.path = "inputs."+tplCtx.form+inputCt;
            tplCtx.value = {
                label : $("#label").val(),
                placeholder : $("#placeholder").val(),
                info : $("#info").val(),
                type : $("#type").val()
            };
            delete tplCtx.form;
            console.log("activeForm save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    //location.href = location.origin+location.pathname+"/form/<?php echo $formId ?>";
                    location.reload();
                } );
            }

    	}

        dyFObj.openForm( activeForm );
    });

    
<?php } ?>
    
});
</script>

