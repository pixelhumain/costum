<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);


?>

<div id="canvas-holder" style="margin:20px auto;width:100%">
	<canvas id="chart-area-finance"></canvas>
</div>
<?php if($size!="S") { ?>
	<div style="margin:0px auto;width:80%">
	</div>
<?php } ?>

<script>
var randomScalingFactor = function() {
	return Math.round(Math.random() * 100);

};

<?php 

    $financeTotal=Yii::$app->cache->get('ctenatfinanceTotalPie');
    $financeDataAll=Yii::$app->cache->get('ctenatfinanceDataAll');
    $financeLblsAll=Yii::$app->cache->get('ctenatfinanceLblsAll');

if($financeTotal===false || $financeDataAll===false || $financeLblsAll===false)
{
	$cters = PHDB::find( Project::COLLECTION, ["category"=>"cteR"], ["slug"] );
	
	$financeDataAll = [];
	$financeLblsAll = [];
	$financeKeyValue = [];
	$financeTotal = 0;
	$slugArray = [];
	foreach ($cters as $i => $cter) 
	{
	$slugArray[] = $cter["slug"];
	}

	$answers = PHDB::find(Form::ANSWER_COLLECTION,[
                                    "cterSlug"=> array('$in' => $slugArray),
                                    "priorisation" => ['$in'=>[ Ctenat::STATUT_ACTION_VALID,
                                                                Ctenat::STATUT_ACTION_COMPLETED,
                                                                Ctenat::STATUT_ACTION_CONTRACT ]]
                                    ], ["_id","answers.murir.planFinancement"] );
        $finance = [];
        $financeLbl = [];
        $res = ["data"=>[],"lbls"=>[],"total"=>0];
		$parentForm = PHDB::findOne(Form::COLLECTION,["id"=>Ctenat::PARENT_FORM ], ["_id", "id","params.period", "cterSlug"] );
		
              foreach ( $answers as $id => $ans ) {

                $daPrinci = @$ans["answers"]["caracter"]["actionPrincipal"];
                if( isset( $ssBadgeFamily[$daPrinci] )
                  && isset($ans["answers"]["action"]["project"][0]["id"]) ) 
                  $actionCount++;
              } 

                  foreach ($answers  as $i => $a) {
            if(isset($a["answers"]["murir"]["planFinancement"]))
            {
                $fin = $a["answers"]["murir"]["planFinancement"];
                
                foreach ($fin as $ix => $f) {

                    $cumul = 0;
                    if(isset($parentForm["params"]["period"])){
                        
                        $from = intval($parentForm["params"]["period"]["from"])+1;
                        $to = intval($parentForm["params"]["period"]["to"]);
                        while ( $from <= $to) {
                            if(!empty($f["amount".$from]))
                                $cumul += intval( $f["amount".$from] );
                            $from++;
                        }
                    }
                    if(isset($f["financerType"])){
	                    if(!isset($finance[$f["financerType"]])){
	                        $finance[ $f["financerType"] ] = $cumul;
	                        $financeLbl[ $f["financerType"] ] = (isset(Ctenat::$financerTypeList[$f["financerType"]])) ? Ctenat::$financerTypeList[$f["financerType"]] : $f["financerType"] ;
	                    }
	                    else 
	                        $finance[$f["financerType"]] += $cumul;
	                }
                }
            }

                foreach (array_keys($finance) as $k => $v) {
                $financeTotal += intval($finance[$v]);
				  }
				  
				  foreach ( $finance as $k => $v) {
			if(!isset($financeKeyValue[$k])){
                $financeKeyValue[ $k ] = $v;
            }
            else 
                $financeKeyValue[$k] += $v;
		}

                  $finance = [];
                $financeLbl = [];
			}
			
	/*foreach ($cters as $i => $cter) 
	{
		$financeData = [];
		$financeLbls = [];
		
		$finance = Ctenat::chiffreFinancementByType($cter["slug"],false);*/
		/*foreach ( $finance as $k => $v) {
			if(!isset($financeKeyValue[$k])){
                $financeKeyValue[ $k ] = $v;
            }
            else 
                $financeKeyValue[$k] += $v;
		}*/
	//}

	foreach ($financeKeyValue as $k => $v) 
	{
		if(round(intval($v)/1000000) != 0){
	        $financeDataAll[] = round(intval($v)/1000000);
	        $financeLblsAll[] = (isset(Ctenat::$financerTypeList[$k])) ? Ctenat::$financerTypeList[$k] : $k ;
	    }
    }
// var_dump($financeDataAll);
// var_dump($financeLblsAll);
Yii::$app->cache->set('ctenatfinanceTotalPie',$financeTotal, 720);
Yii::$app->cache->set('ctenatfinanceDataAll',$financeDataAll, 720);
Yii::$app->cache->set('ctenatfinanceLblsAll',$financeLblsAll, 720);
		 }
?>

jQuery(document).ready(function() {
	//alert("<?php echo round($financeTotal/1000000,1) ?>");
	mylog.log("render","/modules/costum/views/custom/ctenat/graph/pieFinance.php",<?php echo json_encode( $financeDataAll ) ?>,<?php echo json_encode( $financeLblsAll ) ?>);
		var config = {
			type: 'pie',
    		data: {
				datasets: [{
					data: <?php echo json_encode( $financeDataAll ) ?>,
					backgroundColor: <?php echo json_encode( Ctenat::$COLORS ) ?>,
				}],
				labels: <?php echo json_encode( $financeLblsAll ) ?>
			},
			options: {
				responsive: true,
				
			}
		};

			var ctxContainer = document.getElementById('chart-area-finance');
			var ctx = ctxContainer.getContext('2d');
			window.myPie = new Chart(ctx, config);

			ctxContainer.onclick = function(evt) {
		      var activePoints = myPie.getElementsAtEvent(evt);
		      if (activePoints[0]) {
		        var chartData = activePoints[0]['_chart'].config.data;
		        var idx = activePoints[0]['_index'];

		        var label = chartData.labels[idx];
		        var value = chartData.datasets[0].data[idx];

		        var url = "label=" + label + "&value=" + value;
		        //alert("todo sub graphs"+url);
		        smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/slug/terrinarchy');
		      }
		    };
		
	});
	</script>