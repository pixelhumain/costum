<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>



<div id="<?php echo $id?>-canvas-holder" style="margin:0px auto;width:<?php echo ($size == "S") ? "50" : "100";?>%;">
	<canvas id="<?php echo $id?>-chart-area"></canvas>
	
</div>
		
<script type="text/javascript">

var COLORS = (typeof <?php echo $id?>Colors != "undefined") ? <?php echo $id?>Colors  : <?php echo json_encode( Ctenat::$COLORS ) ?>;

jQuery(document).ready(function() {
	if(typeof <?php echo $id?>Data == "undefined")
		alert("<?php echo $id?>Data is undefined");
	if(!id)
		alert("<?php echo $id?> id cannot be null");
	mylog.log("graphs render <?php echo $id?>","/modules/costum/views/custom/ctenat/graph/pieMany.php","<?php echo $id?>Data",<?php echo $id?>Data);
	
	var config = {
		type: 'pie',
		data: <?php echo $id?>Data
	};

	
	var ctxContainer = document.getElementById('<?php echo $id?>-chart-area');
	var ctx = ctxContainer.getContext('2d');
	window.openPie<?php echo $id?> = new Chart(ctx, config);

	ctxContainer.onclick = function(evt) {
      var activePoints = openPie<?php echo $id?>.getElementsAtEvent(evt);
      if (activePoints[0]) {
        var chartData = activePoints[0]['_chart'].config.data;
        var idx = activePoints[0]['_index'];

        var label = chartData.labels[idx];
        var value = chartData.datasets[0].data[idx];
//alert(slugify( label ).toLowerCase());
		if( typeof fObj.search == "function" ){
			//alert("search label "+label);
			searchObject.tags.push(label);
			fObj.search();
			fObj.manage.addActive(fObj,"tags",label, null, null);
			$("#openModal").modal("hide");
		} else {
	        var classSel = slugify( label ).toLowerCase();
	        $(".dashElem").removeClass("hide");
	        $(".dashElem:not(."+classSel+")").addClass("hide");
	        $("#dashElemTitle").html("Projets "+label +" ("+$("."+classSel).length+")")
	    }
//alert(classSel);
      }
    };
});

</script>

<div class='col-xs-12'></div>