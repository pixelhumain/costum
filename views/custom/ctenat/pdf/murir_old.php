<?php

if (!function_exists('buildLineCopilComment')) {
	function buildLineCopilComment($idAnswer, $title, $dateComment, $step, $key, $ct){
		$str="";
		$res = Comment::buildCommentsTree($idAnswer, "answers", null, array("currentDay"=>$dateComment), $step.$key.$ct);
		$commentstr="";
		$countComment=count($res["comments"]);
		foreach($res["comments"] as $comment){
			$commentstr.= '<p style="color:#616164;font-size:13px;">'.$comment["author"]["name"].' :</p><p style="padding:10px;font-size:12px;">'.str_replace("\n","<br>",$comment["text"]).'</p><br/>';
			if(isset($comment["replies"]) && !empty($comment["replies"])){
				$countRep=count($comment["replies"]);
				$countComment+=$countRep;
				$commentstr.='<div style="border:1px solid #ccc;">';
				$inc=1;
				foreach($comment["replies"] as $rep){
					$commentstr.='<p style="color:#616164;font-size:12px"> &#x21B3; '.$rep["author"]["name"].' :</p><p style="padding:5px 10px 10px 10px;font-size:11px;">'.str_replace("\n","<br>",$rep["text"]).'</p>';
					if($inc != $countRep) $commentstr.='<br/>';
					$inc++;
				}
				$commentstr.='</div>';
			}
		}
		$strCountComment=($countComment>1) ? $countComment." commentaires" : $countComment." commentaire";
			$str.='<p style="color:#5EAC87;">'.$title.' <span style="font-size:12px; color:#ccc;font-style:italic;">('.$strCountComment.')</span></p>';
			if(!empty($commentstr))
				$str.=$commentstr;
		return $str;
	}
}
if ( !function_exists('renderArrayFormPropertiesHeader') ) {
	function renderArrayFormPropertiesHeader($df, $step,$key,$table=null, $canAdmin=false){
		$props =  (isset($df["properties"])) ? $df["properties"] : array();
		$title = (isset($df["title"])) ? @$df["title"] : "";
		echo '<h3 style="color:#16A9B1">'.$title.'</h3>';
		echo '<table class="table table-striped table-bordered table-hover directoryTable">';
			echo '<thead>';
			echo '<tr style="border:none">';
			foreach ( $props as $iik => $iiv) {
				echo "<th>".( ( isset($iiv["placeholder"]) ) ? $iiv["placeholder"] : $iik )."</th>";
			}
			if($key=="planFinancement"){
				echo "<th>Statuts</th>";	
			}
			echo '</tr>';
			echo '</thead><tbody class="directoryLines">';
	}
}
if (!function_exists('renderArrayFormPropertiesHeaderClose')) {
	function renderArrayFormPropertiesHeaderClose(){
		echo "</tbody></table>";
	}
}
if (!function_exists('renderArrayFormPropertiesRows')) {
	function renderArrayFormPropertiesRows($step,$key,$answer , $pos, $canAdmin=false)
	{
		//echo "<br/>renderArrayFormPropertiesRows : ".$step."|".$key;
		//var_dump($answer);
		$statusFinance=[
			"waiting"=>"En attente de validation",
			"validated"=>"Validé sans réserve",
			"refused"=>"Non validé",
			"reserved"=>"Validé avec réserves"
		];
		$financerTypeList = [
		    "acteursocioeco" => "Acteur Socio-économique",
		    "colfinanceur"  => "Communes / intercommunalités / Syndicats mixtes",
		    "departement"   => "Départements",
		    "region" 		=> "Région",
		    "europe"=>"Europe",
		    "etat" => "Etat - Services déconcentrés / préfecture",
		    "ademe" => "Etat - Ademe",
		    "cerema" => "Etat - Cerema",
		    "bdt"=> "Etat - Banque des territoires",
		    "bpi"=> "Etat - BPI",
		    "agenceLeau"=> "Etat - Agence / Office de l'eau",
		    "officeNatForet"=>"Etat - Office national des forêts",
		    "agenceBiodiv"=> "Etat - Office de la biodiversité",
		    "afd"=> "Etat - Agence Francaise de développement",
		    "vn2f"=>"Etat - Voies navigable de France",
		    "franceAgirMer"=>"Etat - FranceAgriMer",
		    "autre" => "Etat - Autre"
		];
		if(isset($answer) && is_array($answer))
		{
			echo '<tr>';
				$titleComment = $key." ".(intval($pos)+1);
				if($key=="planFinancement"){
					if(isset($answer["financerType"]) && !empty($answer["financerType"])){
						if(@$listLabels[$answer["financerType"]])
							$financerType=$listLabels[$answer["financerType"]];
						else
							$financerType=$answer["financerType"];
					}else
						$financerType= "";

					$financer = "" ;
					if(!empty($answer["financer"])){
						$orgF = PHDB::findOneById(Organization::COLLECTION, $answer["financer"], array("name"));
						if(!empty($orgF) && !empty($orgF["name"]))
							$financer = $orgF["name"] ;
					}


					echo "<td>".$financerType."</td>";
					echo "<td>".$financer."</td>";
					echo "<td>".$answer["title"]."</td>";
				}
				foreach ($answer as $sa => $sv) {
					if(!in_array($sa, ["financerType", "valid", "financer"]) && ($key!="planFinancement" || $sa!="title")){
						echo "<td>";
						if(is_array($sv))
							echo implode(",",$sv);
						else
							echo $sv;
						if(in_array($sa, ["poste", "qui"]))
							$titleComment.=" : ".$sv;
						echo "</td>";
					}
				}
				if($key=="planFinancement"){
					if(isset($answer["valid"]) && !empty($answer["valid"])){
						$status=$statusFinance[$answer["valid"]];
					}else
						$status=$statusFinance["waiting"];
					echo "<td>".$status."</td>";
				}				
			echo '</tr>';
		}
	}
}

//Yii::import("parsedown.Parsedown", true);
//$Parsedown = new Parsedown();
//var_dump($answers); exit;?>
<style type="text/css">
	
h1 {
	font-size: 24px;
}
.img-band{
margin-top: -50px;
}
.titleEncart{
width: 80%;border:1px solid #616161;margin:auto;text-align:center;
}
h3{
	color:#50b796;
	text-transform:uppercase;
}

.comment-text{
	white-space: pre-line;
}
.noborder{
	border:1px solid white;
}
.blue{
	color : #195391;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

table {
    color: #616161;
	text-align: center;
}

table td, table th {
    border: 1px solid #616161;
}
table th {
	border-width: 2px;
}
table tr td{
	font-size: 10pt;
}
table tr th{
	font-size: 12px;
	font-weight: bold;
}
table.no-boder th, table.no-boder td{
	border:0px solid white;
}
</style>
<div>
	<div>
		<!-- *******************************
					calendar -->
			<h3 style="color:#16A9B1;"><?php echo $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["calendar"]["title"]?></h3>
			<table>
				<thead>
				<?php 
					$dataClAns= (isset($answers["murir"]["calendar"])) ? $answers["murir"]["calendar"]:array();
					$dateSections = array("01/01/2018","01/07/2018","01/01/2019","01/07/2019","01/01/2020","01/07/2020","01/01/2021","01/07/2021","01/01/2022","01/07/2022","01/01/2023");
					//var_dump($dataClAns);exit;
					 if(count($dataClAns)>0){ ?>
					<tr>
						<th><br/>Actions<br/></th>
						<th>1er<br/>Sem<br/>2018</th>
						<th>2ème<br/>Sem<br/>2018</th>
						<th>1er<br/>Sem<br/>2019</th>
						<th>2ème<br/>Sem<br/>2019</th>
						<th>1er<br/>Sem<br/>2020</th>
						<th>2ème<br/>Sem<br/>2020</th>
						<th>1er<br/>Sem<br/>2021</th>
						<th>2ème<br/>Sem<br/>2021</th>
						<th>1er<br/>Sem<br/>2022</th>
						<th>2ème<br/>Sem<br/>2022</th>
					</tr>
					<?php } ?>
				</thead>
				<tbody class="directoryLines">	
					<?php 
					$ct = 0;
					if(!empty($dataClAns)){
						foreach ($dataClAns as $q => $a) {

							echo "<tr>".
								"<td>".@$a["step"]."</td>";
							$bgColor = "white";
							$td = "";
							$style="";
							foreach ($dateSections as $sa => $sv) {
								if( $bgColor == "white"){
									if( isset( $dateSections[$sa+1] ) && strtotime(str_replace('/', '-',@$a["startDate"])) < strtotime(str_replace('/', '-',$dateSections[$sa+1]))  ) {
										$style="line-height:100%;background-color:#5EAC87;color:white;";
										$bgColor = "#5EAC87";
										//$color="white";
										$td = "";
									}
								} else if( $bgColor == "#5EAC87"){
									if( strtotime(str_replace('/', '-',@$a["endDate"])) < strtotime(str_replace('/', '-',$sv))  ) {
										$style="";
										$bgColor = "";
										$td = "";
									}
								}
								if( $sa != sizeof( $dateSections ) - 1 )
									echo '<td style="'.$style.'">'.$td.'</td>';
							}
							$ct++;
							echo "</tr>";
						}
					}
				 ?>
				</tbody>
			</table> 
			<?php if(!empty($comment) && !empty($dataClAns)){ ?> 
			<div class="col-xs-12 commentPod">
				<h6>COMMENTAIRES</h6>
				<?php 
					$ct=0;
					foreach($dataClAns as $q => $a){
						echo buildLineCopilComment($idAnswer, $a["step"], $dateComment, "murir", "calendar", $ct);
        				$ct++;
					} ?>
			</div>
			<?php } ?>
		</div>


		<div class='col-xs-12'>
		<!-- *******************************
					partenaires -->
		<?php 
			$data = (isset($answers["murir"]["partenaires"])) ? $answers["murir"]["partenaires"] : array();
			renderArrayFormPropertiesHeader( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["partenaires"], "murir","partenaires",true, $canAdmin ) ;
			$ct = 0;
			 
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "partenaires", $a , $ct, $canAdmin );
				$ct++;
			}

			renderArrayFormPropertiesHeaderClose();
			?>
			<?php if(!empty($comment)){ ?> 
			<div class="col-xs-12 commentPod">
				<h6>COMMENTAIRES</h6>
			
			<?php  $ct=0;
				foreach ($data as $q => $a) 
				{
					echo buildLineCopilComment($idAnswer, $a["qui"], $dateComment, "murir", "partenaires", $ct);
					$ct++;
				}
			?>
			</div>
			<?php } ?>
		</div>

		<div class='col-xs-12'>
		<!-- *******************************
					budget -->
		<?php 
			$data = (isset($answers["murir"]["budget"])) ? $answers["murir"]["budget"] : array(); 
			
			renderArrayFormPropertiesHeader ( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["budget"], "murir","budget",true, $canAdmin) ;
			$ct = 0;
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "budget", $a , $ct, $canAdmin);
				$ct++;
			}
			//renderArrayFormPropertiesHeaderClose();
			
			echo $this->renderPartial( "survey.views.custom.ctenat.totalRow" , 
									array( "data" => $data, 
										   "label"=>"Budget",
											"span"  => 4) );
			//renderArrayFormPropertiesHeaderClose();
			$totalBudg = 0;
			foreach ($data as $q => $a) {
				$t = 0;
				if( isset($a["amount2019"]) )
					$t = $a["amount2019"];
				if( isset($a["amount2020"]) )
					$t = $t + $a["amount2020"];
				if( isset($a["amount2021"]) )
					$t = $t + $a["amount2021"];
				if( isset($a["amount2022"]) )
					$t = $t + $a["amount2022"];
				$totalBudg = $totalBudg + $t;
			}
			renderArrayFormPropertiesHeaderClose(); ?>
			<?php if(!empty($comment)){ ?> 
			<div class="col-xs-12 commentPod">
				<h6>COMMENTAIRES</h6>
			
				<?php 
				$ct=0;
				foreach ($data as $q => $a) 
				{
					echo buildLineCopilComment($idAnswer, $a["poste"], $dateComment, "murir", "budget", $ct);
					$ct++;
				} ?>
			</div>
			<?php } ?>
		</div>


		<div class='col-xs-12'>
		<!-- *******************************
					planFinancement -->
		<?php 
			$data = (isset($answers["murir"]["planFinancement"])) ? $answers["murir"]["planFinancement"] : array(); 
			
			renderArrayFormPropertiesHeader ( $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["planFinancement"], "murir","planFinancement",true, $canAdmin ) ;
			$ct = 0;
			foreach ($data as $q => $a) 
			{
				renderArrayFormPropertiesRows( "murir", "planFinancement", $a , $ct, $canAdmin);
				$ct++;
			}

			echo $this->renderPartial( "survey.views.custom.ctenat.totalRow" , 
									array( "data" => $data,
										"label"=>"Financement",
										"span"  => 5) );
			$totalFin = 0;
			foreach ($data as $q => $a) {
				if(!isset($a["valid"]) || $a["valid"]!="refused"){
					$t = 0;
					if( isset($a["amount2019"]) )
						$t = $a["amount2019"];
					if( isset($a["amount2020"]) )
						$t = $t + $a["amount2020"];
					if( isset($a["amount2021"]) )
						$t = $t + $a["amount2021"];
					if( isset($a["amount2022"]) )
						$t = $t + $a["amount2022"];
					if( isset($a["amount2023"]) )
						$t = $t + $a["amount2023"];
					$totalFin = $totalFin + $t;
				}
			}
			
			$percent = ($totalBudg) ? $totalFin*100/$totalBudg : 0;
			$delta = $totalFin - $totalBudg ;
			$col = "";
			$style="";
			$percol = "primary";
			if($delta >0){
				$style="color:green;";
				$col = "text-green";
				$percol = "success";
			}
			else if($delta < 0 ){
				$style="color:red;";
				$col = "text-red";
				$percol = "danger";
			}
			renderArrayFormPropertiesHeaderClose();
			
			?>
			<?php if(!empty($comment)){ ?> 
			<div class="col-xs-12 commentPod">
				<h6>COMMENTAIRES</h6>
				
				<div class="col-xs-12 commentPod">
					<?php 
					$ct=0;
					foreach ($data as $q => $a) 
					{
						echo buildLineCopilComment($idAnswer, $a["title"], $dateComment, "murir", "planFinancement", $ct);
						$ct++;
					} ?>
				</div>
			<?php } ?>
		</div>

		<div class='col-xs-12 '>
			<h3 style="color:#16A9B1">EQUILIBRE BUDGETAIRE</h3>
			<div class="progress"><br/>
			    <span class="sr-only"><?php echo round($percent) ?>% complet</span>
			  <!-- <div class="progress-bar progress-bar-warning" style="width: 20%">
			    <span class="sr-only">20% Complete (warning)</span>
			  </div>
			  <div class="progress-bar progress-bar-danger" style="width: 10%">
			    <span class="sr-only">10% Complete (danger)</span>
			  </div> -->
			</div>
			<table class="table table-striped table-bordered table-hover  directoryTable">
				<tbody class="directoryLines">
					<tr>
						<td>BUDGET prévisionnel</td>
						<td><?php echo trim(strrev(chunk_split(strrev($totalBudg),3, ' '))) ?>€</td>
					</tr>
					<tr>
						<td>Financements acquis</td>
						<td><?php echo trim(strrev(chunk_split(strrev($totalFin),3, ' '))) ?> €</td>
					</tr>
					<tr>
						<td>Delta</td>
						<td><?php 
						
						echo '<span style="'.$style.'" class="'.$col.'">'.trim(strrev(chunk_split(strrev($delta),3, ' '))).' € </span>' ?> </td>
					</tr>
				</tbody>
			</table>
		</div> 

		<div class='col-xs-12'>
		<!-- *******************************
					results -->
			<h3 style="color:#16A9B1">RÉSULTATS ATTENDUS DE L'ACTION</h3>
			<table class="table table-bordered table-hover  directoryTable" id="">
	
				<?php
				$df = $form["scenario"]["murir"]["json"]["jsonSchema"]["properties"]["results"];
				$indicateurs = Ctenat::getIndicator();
				$editBtnL = ($canAdmin) ? " <a href='javascript:;' data-form='".$step."' data-step='".$step."' data-q='".$key."' class='addAF btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";
				?>	
				<thead>	
					<tr>
						<th>Indicateur</th>
						<th>Objectif / Réalisé</th>
						<th>Réf. 2018</th>
						<th>Résultat 2019</th>
						<th>Résultat 2020</th>
						<th>Résultat 2021</th>
						<th>Résultat 2022</th>
					</tr>
				</thead>
				<tbody class="directoryLines">	
			<?php 
				$ct = 0;
		//always show indicateur emploi 
			$dataind = array();
			if(!empty($answers["murir"]) && !empty($answers["murir"]["results"]))
				$dataind = $answers["murir"]["results"];
			$emploiExists = false;
			foreach ($dataind as $q => $a) {
				if( isset($a["indicateur"]) && is_array($a["indicateur"]) &&  in_array("5d7fa1c540bb4e8f7b496afe", $a["indicateur"]) )
					$emploiExists = true;
				else if( isset($a["indicateur"]) && "5d7fa1c540bb4e8f7b496afe" == $a["indicateur"] )
					$emploiExists = true;
			}

			if( !$emploiExists ){
				$emploiIndic = [ "indicateur" => "5d7fa1c540bb4e8f7b496afe" ];
				$dataind[] = $emploiIndic;
			}
			//var_dump($indicateurs); //exit;
			foreach ($dataind as $q => $a) {
				if(isset($a["indicateur"])){
					//foreach ($a["indicateur"] as $kA => $valA) {
					echo "<tr>".
							//"<td id='indic".$ct."' rowspan=2 style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$valA]) ?  $indicateurs[$valA] : "" )."</td>".
							"<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indicateurs[$a["indicateur"]]) ?  $indicateurs[$a["indicateur"]] : "" )."</td>".
							"<td>Objectif</td>".
							"<td style='vertical-align : middle;text-align:center;'></td>".
							"<td class='editContent' data-key='res2019'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2019"])?$a["objectif"]["res2019"]:"")."</td>".
							"<td class='editContent' data-key='res2020'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2020"])?$a["objectif"]["res2020"]:"")."</td>".
							"<td class='editContent' data-key='res2021'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2021"])?$a["objectif"]["res2021"]:"")."</td>".
							"<td class='editContent' data-key='res2022'  data-indic='indic".$ct."' data-pos='".$q."' data-type='objectif'>".(isset($a["objectif"]["res2022"])?$a["objectif"]["res2022"]:"")."</td>";
					echo "</tr>";
					echo "<tr>".
							"<td></td>".	
							"<td>Réalisé</td>".	
							"<td class='editContent'data-key='res2018'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2018"]) ? $a["reality"]["res2018"]:"")."</td>".
							"<td class='editContent'data-key='res2019'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2019"]) ? $a["reality"]["res2019"]:"")."</td>".
							"<td class='editContent' data-key='res2020'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2020"]) ? $a["reality"]["res2020"]:"")."</td>".
							"<td class='editContent' data-key='res2021'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2021"]) ? $a["reality"]["res2021"]:"")."</td>".
							"<td class='editContent' data-key='res2022'  data-indic='indic".$ct."' data-pos='".$q."' data-type='reality'>".(isset($a["reality"]["res2022"]) ? $a["reality"]["res2022"]:"")."</td>";
					echo "</tr>";


					$ct++;
					//}
				}
			}
			?>

		</tbody>
	</table>
		</div>
	</div>
</div>
