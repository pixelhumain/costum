<?php

Yii::import("parsedown.Parsedown", true);

$cter = PHDB::findOne( Project::COLLECTION, ["slug" => $answer["cterSlug"] ] );

$Parsedown = new Parsedown();

// if( !empty($answer["answers"]) &&
// 				!empty($answer["formId"]) &&
// 				!empty($answer["answers"][$answer["formId"]]) &&
// 				!empty($answer["answers"][$answer["formId"]]["answers"]) ) {
// 				$answers = $answer["answers"][$answer["formId"]]["answers"];
			
// 	if(!empty($answers["organization"]) && !empty($answers["organization"]["id"]) ){
// 		$organization = PHDB::findOneById(Project::COLLECTION, $answers["organization"]["id"], array("name"));
// 	}
// 	if(!empty($answers["project"]) && !empty($answers["project"]["id"])  ){
// 		$project = PHDB::findOneById(Project::COLLECTION, $answers["project"]["id"], array("name"));
// 	}
// }

  $cssAnsScriptFilesModule = array(
        '/plugins/font-awesome/css/font-awesome.min.css',
        '/plugins/font-awesome-custom/css/font-awesome.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
?>
<style type="text/css">
	
h1 {
	font-size: 20px;
}

span, h4 , .pdfsiv {
	font-family: 'Baloo Tamma', cursive;
}

table {
	font-size: 14px;
}

h4 {
	font-size: 17px;
}

.blue{
	color : #2e3e50;
}

.lightgreen{
	color : #a9ce3f;
}

.darkgreen{
	color : #4db88c;
}

.pdftext{
	text-align: center;
}

.body { 
	font-family: Arial,Helvetica Neue,Helvetica,sans-serif; 
}

.pdftittlecolor {
	color: #195391 ;
}

.specialword {
	color: grey;
	font-weight: bold;
}

.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
}

.table>thead>tr {
    width: 100%;
    max-width: 100%;
    min-width: 100%;
}

.table-bordered>thead>tr>th, .table-bordered>thead>tr>td {
    border-bottom-width: 2px;
}

.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #ddd;
}

.calendar-table tr th:nth-child(1){
    width: 100px;
}

	.calendar-table {
		padding: 10px;
		width: 100%;
		table-layout: fixed;
		border-collapse: collapse;
		font-size: 15px;
	}

	.calendar-table th {
		border-bottom: 2px solid #195391; 
		text-align: center
		color : #59bba7;
	}

	.calendar-table tr th {
		color : #59bba7;
	}

	.calendar-table td {
		border-bottom:  2px solid #ddd;
	}

	.calendar-table tr th:nth-child(1){
       width: 100px;
    }

    .calendar-table tr td:nth-child(1){
       width: 100px;
       border-right: 3px solid #ddd;
    }

    .calendar-table tr td+td{
       font-size: 15px;
    }

    .calendar-table tr th:last-child{
       /*width: 50px;
    }

    .calendar-table .col-bordered {
    	border-right: 3px solid #ddd;
    }




</style>
<?php 

	$server = ((isset($_SERVER['HTTPS']) AND (!empty($_SERVER['HTTPS'])) AND strtolower($_SERVER['HTTPS'])!='off') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
	//$imgHead =  Yii::app()->createUrl(Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/city.png") ;
	//$imgHead =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/city.png" ;

    $imgHead1 =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/PictoCTE.png" ;
    $imgHead2 =  $server.Yii::app()->getModule("costum")->assetsUrl."/images/ctenat/Logo_republique-francaise.png" ;
?>
<table>
    <tr>
        <td><img class="img-responsive" style="height: 120px; float: right; text-align: right;" src="<?php echo $imgHead2 ?>"/>
        </td>
        <td style="width: 300px"></td>
        <td><img class="img-responsive" style="width: 120px;height: 120px; float: right; text-align: right;" src="<?php echo $imgHead1 ?>"/>
        </td>
    </tr>
</table>

<div class="body">
	<span style="text-align: center;">
		<h1 class="blue"> </h1>
        <?php if (!empty($statushtml)){
            if ($statushtml == "Descriptifs de toutes les « Action en maturation » "){
                $statushtml = "Descriptifs de toutes les « Actions en maturation » ";
            }
            ?>
            
        <?php } ?>
		<span class="darkgreen"><?php echo @$cter["name"]; ?></span>
		<span class="lightgreen"></span>
		<br/>
		<?php
			if(!empty($answer["project"])) { ?>
				<span class="">
					<?php 
					//echo "Action N°".$answer["project"]["id"]." : ";
					echo $answer["project"]["name"]; ?> 
				</span>
		<?php	} ?>
	</span>
	<br/><br/>
	<span style="text-align: left; font-size: 14px;">
		<b>Rattachée à l’orientation : </b>
		<?php 
			if( isset($answer["orientationsName"]) ){
				echo $answer["orientationsName"];
			}
		?>
		<br/>
		<?php //echo "Dernière date de mise à jour : ".date("d/m/Y", $answer["created"]); ?>
	</span>
	<div class='col-xs-12'>
		<?php
		//$structpor = "aucune";
		$structpor = "";

		if (isset($answer["answers"]["action"]["parents"])) {
			foreach ($answer["answers"]["action"]["parents"] as $strp => $strpv) {
					if (isset($strpv["name"])) {
						if (!isset($strpv["typeStruct"]) || $strpv["typeStruct"] != "associe") {
							$structpor .= $strpv["name"]."<br>";
						}
					}else if (isset($strpv["slug"])) {
						$el = Slug::getElementBySlug($strpv["slug"]);
						 if (!isset($el["el"]["typeStruct"]) || $el["el"]["typeStruct"] != "associe") {
							$structpor .= $el["el"]["name"]."<br>";
						 }
					}else if (isset($strpv["id"]) && isset($strpv["type"])) {
						$el = Element::getElementById($strpv["id"],$strpv["type"]);
						if (!isset($el["el"]["typeStruct"]) || $el["el"]["typeStruct"] != "associe") {
							$structpor .= $el["name"]."<br>";
						 }
					}
				}
		}

		if ($structpor == "") {
			$structpor = "aucune";
		}

		$str = "";
		if(!empty($answer["answers"]["organizations"])){
		$str .= '<div class="pdftext"><h4 class="h4 padding-20 blue" style="">'."Structures Porteuses".'</h4>';
			$str .= '<span>'.$structpor.'</span>';
		}

		if(!empty($answer["project"]["shortDescription"])){
			
			$str .= '<h4 class="padding-20 blue" style=""><i class="fas fa-arrow-circle-right"></i>'."Description courte".'</h4>';
			$str .= '<span>'.$answer["project"]["shortDescription"].'</span>';
			$str .= '</div>';
		}else {
			$str .= '</div>';
		}
		
		if(!empty($answer["project"]["description"])){
			$str .= '<h4 class="padding-20 blue" style="">'." Description longue".'</h4>';
			$str .= '<span>'.$Parsedown->text($answer["project"]["description"]).'</span>';
		}
		
		if(!empty($answer["project"]["tags"])){
			$str .= '<h4 class="padding-20 blue" style=""><i class="fa fa-arrow-circle-right"></i> '."Tags".'</h4>';
			$str .= '<span>';

			foreach ($answer["project"]["tags"] as $keyT => $valT) {
				$str.= '<span style="color :red">#'.$valT.'</span> ';
			}
			'</span>';
		}

		$str .= '<h4 class="padding-20 blue" style="">'."Attentes vis à vis du dispositif".'</h4>';
		
		if(!empty($answer["project"]["expected"])){
			$str .= '<span>'.$Parsedown->text($answer["project"]["expected"]).'</span>';
		}


		echo $str ;


		$params = array(
			//"author" => @$answer["name"],
			"idAnswer"=>$id,
			"answer" => $answer,
			//"saveOption"=>"F",
			//"urlPath"=>$res["uploadDir"],
			"title" => "TEST",
			"participants" =>"partici",
			//"dateComment" => "dateComment",
			"subject" => "CTE",
			//"custom" => $form["custom"],
			"footer" => true,
			"tplData" => "cteDossier",
			"form" => $form,
			"forms" => $forms,
			"canAdmin"=>null
		);

		//Rest::json($params); exit;
		if(isset($answer['answers'])){
			$params["answers"] = $answer['answers'];
		}
		
		echo $this->renderPartial('costum.views.custom.ctenat.pdf.murir', $params, true);          
		?>
	</div>
</div>