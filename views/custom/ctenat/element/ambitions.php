<div class="col-xs-12 text-center contentFilRouge no-padding badgesSt">
	<hr/>
		<h3 class="filRougeTitle col-xs-12 text-gray">Le fil rouge du dispositif et ses orientations stratégiques</h3>
		<div class="description-markdown-filRouge col-xs-12"><?php if(isset($el["filRouge"])) echo $el["filRouge"];  ?></div>
	<script type="text/javascript">var badges=<?php echo json_encode(@$badges); ?>;</script>
	
	<?php if(!empty($badges)){ ?>
	<?php
		echo $this->renderPartial("costum.views.custom.ctenat.element.badges",array("el"=>$el, "badges"=>@$badges, "htmlContainer"=>".badgesSt"),true); 
	} ?>
</div>


<div class="col-xs-12 text-center contentEDLE no-padding badgesSt">
	<hr/>
		<h3 class="EDLETitle col-xs-12 text-gray">État des lieux écologique</h3>
		<div class="description-markdown-EDLE col-xs-12"><?php if(isset($el["EDLE"])) echo $el["EDLE"];  ?></div>
	<script type="text/javascript">var badges=<?php echo json_encode(@$badges); ?>;</script>
	<?php 
			$keyTpl = "indecolo";
			$kunik = $keyTpl;
			$answerPath = "indecolo";

		echo $this->renderPartial("costum.views.custom.ctenat.element.tableEDLE",[
		"keyTpl"=>$keyTpl,
		"kunik" => $kunik,
		"answerPath" => $answerPath,
		"id" => $id,
		"type" => $type,
		"indecolos" => $indecolos,
		"indecolosActifs" => $indecolosActifs,
		"canEdit" => $canEdit,
		"editable" => false
		],true);
		?>
</div>