<?php 
    $cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        // MARKDOWN
        '/plugins/to-markdown/to-markdown.js'            
    );

    $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js',
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 
    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
 

    $blockCmss = array();

    $page = ( isset($page) && !empty($page) ) ? $page : "welcome";

    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
        $blockCmss = PHDB::find(Cms::COLLECTION, 
                  array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"]) );
        
        $blockCmsById = [];

        foreach ($blockCmss as $key => $value) {
            if (isset( $value["id"] )) {
                $blockCmsById[$value["id"]] = $value;
            }
        }
    }

    $bannerImg = @$el["profilRealBannerUrl"] ? Yii::app()->baseUrl.$el["profilRealBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/miks/no-banner.jpg";
    $logo = @$this->costum["logo"] ? $this->costum["logo"] : null;
    $shortDescription = @$this->costum["description"] ? $this->costum["description"] : null;
    $bkagenda = @$this->costum["tpls"]["blockevent"]["background"] ? $this->costum["tpls"]["blockevent"]["background"] : "white";
    $titlenews = @$this->costum["tpls"]["news"]["title"] ? $this->costum["tpls"]["news"]["title"] : "Actualité";
?>
<style>
    section {
        background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
    }

    .ch-1{
        background: #001146;
        background-image : url(<?= $bannerImg; ?>);
        width: 101%;
        height: auto;
        background-size: cover;
        padding-bottom: 58%;
    }

    .blockwithimg {
        margin-top : 2%;
        margin-bottom: 2%;
    }

    .actu{
        margin-top:3%;

    }

    .agenda{
        color : white; 
        margin-top :3%;
        background : <?= $bkagenda; ?>;
    }

    button {
        background-color: Transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        outline:none;
    }

    #newsstream .loader{
        display: none;
    }
    #shortDescription {
        text-shadow: black 0px 0px 8px;
        margin-left: 5.5%;
    }

    .tooltips-menu-btn{
        display : none !important;
    }

    .title {
        text-decoration: underline;
    }


    .logofn{
        width: 50%;
        margin-top: 5%;
        position: absolute;
        margin-left: 3%;
    }

    .agenda-h1{
        margin-left :12%;
        left: 4%;
        margin-top: 2%;
    }

    .plus-m {
        width : 2%;
    }

    .img-event{
        height: 250px;
        width: 500px;
    }

    .description{
        padding : 5%; 
        top: 5%;
    }

    @media (max-width:768px){
        .plus-m {
        width : 5%;
        }
        .ch-2-description{
            font-size: 2.5vw !important;
        }
        .img-event {
            height: 75px;
            width: 250px;
        }
        .ch-2-p-m{
            font-size: 2.5rem;
            margin-top: 2%;
            line-height: 19px;
        }
        .p-mobile-description{
            font-size : 1.5rem;
        }
        .description{
            padding : 5%; 
            margin-top: -3%;
        }
        .title-ch-2{
            font-size : 2.81vw;
        }
        .btn {
            padding: 3px 5px;
            font-size: 7px;
        }
        .card-mobile{
            margin-top : 3%;
        }
        .logofn{
            width: 100%;
            margin-top: 13%;
        }
    }
</style>

<div class="ch-1 row">
    <?php 
        $params = [  "tpl" => "miks","slug"=>$this->costum["slug"],"canEdit"=>true,"el"=>$el, "page" => @$page  ];
        echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
    ?>
    <div class="logofn col-md-9">
        <!-- Edit Banniere -->
        <?php 
        $params =  array(
            "type"=> @$el["type"], 
            "id"=>(string) @$el["_id"], 
            "name"=> @$el["name"],
            "canEdit" => $canEdit,
            "profilBannerUrl"=> @$el["profilRealBannerUrl"]);

        echo $this->renderPartial("costum.views.tpls.modalHeader", $params); 
        ?>

        <?php if(@$logo && $logo != null) : ?>
            <img src="<?= $logo; ?>" id="logoBanner" class="img-responsive" style="width: 65%;">
        <?php endif; ?>
    </div>
</div>

<!-- Description -->

<div class="ch-2 row">
    <div class="ch-2-h1 col-xs-6 col-sm-6 no-padding text-left">
        <p class="title-ch-2" id="shortDescription"><?php echo $shortDescription; ?></p>
    </div>
    <div class="ch-2-img col-xs-6 col-sm-6" >
       <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/filigram.png" class="img-responsive" style="margin-top: -60%; margin-left:3%; position: absolute; opacity: 0.7;">
    </div>
    <div class="ch-2-d col-xs-12 col-sm-12">
        <p class="p-mobile-description" id="descriptions"> 
            <?php 
            $paramsText = [
                "cmsList" => $blockCmss,
                "page" => @$page,
                "tag" => "descriptionsfil1"
            ];

            echo $this->renderPartial("costum.views.tpls.blockCms.textImg.text", $paramsText);
            ?>    
        </p>
    </div>
</div>

<div class="ch-3 row">
    <?php 
        echo $this->renderPartial("costum.views.custom.miks.tpls.btnother", array("idToPath" => "tpls.btnother" ,"canEdit" => $canEdit , "cmsList" => $blockCmss,"page" => @$page));
    ?>
</div>

<?php
foreach ($blockCmss as $e => $v) {
    if (!empty($v["type"]) && isset($v["type"]) && $v["type"] != "custom.miks.tpls.btnother") {
        $params = [
            "cmsList"   =>  $blockCmss,
            "blockCms"  =>  $v,
            "page"      =>  $page,
            "canEdit"   =>  $canEdit,
            "type"      =>  $v["type"]
        ];
        echo $this->renderPartial("costum.views.".$v["type"],$params);
    }
}
?>

<?php if($canEdit){ ?> 
<!-- ESPACE ADMIN --> 

<hr>
<center>
    <div class="container">
        <a href="javascript:;" class="addTpl btn btn-primary" data-key="blockevent"  data-collection="cms"><i class="fa fa-plus"></i> Ajouter une section</a>
    </div>
</center>
<?php } ?>

<script>
jQuery(document).ready(function(){
    mylog.log("render","/modules/costum/views/custom/miks/home.php");
    setTitle(costum.title);
    // showProjects();

    contextData = <?php echo json_encode($el); ?>;
    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName=<?php echo json_encode($el["name"]); ?>;
    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
});

function showProjects(){

    var paramsProjects = {
        contextType : costum.contextType,
        contextSlug : costum.contextSlug
    };

    ajaxPost(
        null,
        baseUrl+"/costum/miks/getprojectsaction",
        paramsProjects,
        function(data){ 
            mylog.log("success : ",data);
            costum.projectsMiks = data.projects;
        },
        function(data){
            mylog.log("error : ");
            mylog.dir(data);
        }
    );
}
</script>
