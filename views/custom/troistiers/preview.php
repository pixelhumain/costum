<style type="text/css">
	.similarLinkcustom{
		display: none !important;
	}
	.blockFontPreview {
		font-size: 14px;
		color: #ec5d85;
	}
	h2 {
		font-size: unset;
	}
	#socialNetwork {
		font-size: 20px;
	}
	.preview-element-info {
		margin-bottom: 50px;
	}
	.bg-primary, .bg-primary:hover{
		background-color: var(--primary-color) !important;
		color:white !important;
	}
</style>


<?php
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig = @$this->appConfig["preview"];
$auth = (Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;
$userId = Yii::app()->session["userId"]??null;

$iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);

$compagnonBool = (isset($this->costum["settings"]) && isset($this->costum["settings"]["compagnonFtl"])) ? $this->costum["settings"]["compagnonFtl"] : false;

//var_dump("<pre>",$element,"</pre>");
if (!empty($element['startDate'])) {
	$date = ['start' => $element['startDate']];
	if (!empty($element['endDate']))
		$date['end'] = $element['endDate'];
}

if (empty($element['type'])) {
	$element['type'] = "NGO";
}

?>
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10">
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
		<a href="#@<?php echo @$element["slug"] ?>" class="lbh btn bg-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
		<?php if(in_array($element["collection"], [Person::COLLECTION, Organization::COLLECTION])){ 
			$followed = (isset($element["links"]) && isset($element["links"]["followers"]) && isset($element["links"]["followers"][$userId]));
		?>
		<a id="btn-follow-tl" class="btn btn-primary pull-right margin-right-10" 
			data-toggle="tooltip" data-placement="left" data-original-title="Suivre"
			data-type="<?= $element["collection"] ?>" 
			data-id="<?= $element["_id"] ?>" 
			title="Suivre"
			data-userid="<?= $userId ?>"
			data-isfollowed="<?= $followed ?>"
			>
			<?= ($followed)?"Suivi(e)":"Suivre" ?>
		</a>
		<?php } ?>

		<?php if($element["collection"]!=Person::COLLECTION && (Authorisation::canEdit($userId, (string)$element["_id"], "poi")|| Authorisation::isCostumAdmin())){ ?>
			<button class="btn btn-primary pull-right margin-right-10 btn-edit-tl" data-type="<?php echo $element["collection"] ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>" title="Modifier">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>
		<?php if($element["collection"]!=Person::COLLECTION &&(Authorisation::canEdit($userId, (string)$element["_id"], "poi")|| Authorisation::isCostumAdmin())){ ?>
			<button class="btn btn-danger pull-right margin-right-10 btn-delete-tool-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" title="Supprimer">
				<i class="fa fa-trash"></i>
			</button>
		<?php } ?>
	</div>
	<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
		<?php
		if (isset($element["profilImageUrl"]) && !empty($element["profilImageUrl"])): ?>
			<div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 margin-top-10">
				<a href="<?php echo Yii::app()->createUrl('/' . $element["profilImageUrl"]) ?>">
					<img class="img-responsive" style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);margin: auto;max-height: 300px;" src="<?php echo Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) ?>" />
				</a>
			</div>
		<?php endif; ?>
		<?php
		if (isset($element["tags"]) && $compagnonBool){
			if (in_array("Compagnon Trois-Lieux", $element["tags"]??[])){ ?>
				<div class="badgeCompagnon">
					<span>Tiers-lieux Compagnon</span>
				</div>
		<?php } } ?>
		<div class="preview-element-info col-xs-12">
			<?php if (isset($element["name"])) { ?>
				<h3 class="text-center margin-top-40"><?php echo $element["name"] ?></h3>
			<?php } ?>
			<div class="col-xs-12 address">
				<?php if (isset($element["address"]["addressLocality"])) { ?>
					<div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
						<i class="fa fa-map-marker"></i>
						<?php
						echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"] . ", " : "";
						echo !empty($element["address"]["postalCode"]) ?
							$element["address"]["postalCode"] . ", " : "";
						echo $element["address"]["addressLocality"];
						?>
					</div>
				<?php } ?>
			</div>
			<?php
			if (isset($element["url"])) { ?>
				<div class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
					<?php $scheme = ((!preg_match("~^(?:f|ht)tps?://~i", $element["url"])) ? 'http://' : ""); ?>
					<a href="<?php echo $scheme . $element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo $element["url"] ?></a>
				</div>
			<?php } ?>
			<?php
			if (isset($element["socialNetwork"])) { ?>
				<div id="socialNetwork" class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
					<?php

					if (isset($element["socialNetwork"]["facebook"])) { ?>
						<span id="divFacebook" class="margin-right-10">
							<a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"]; ?>" target="_blank" id="facebookAbout">
								<i class="fa fa-facebook" style="color:#1877f2;"></i>
							</a>
						</span>
					<?php }

					if (isset($element["socialNetwork"]["twitter"])) { ?>
						<span id="divTwitter">
							<a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"]; ?>" target="_blank" id="twitterAbout">
								<i class="fa fa-twitter" style="color:#1da1f2;"></i>
							</a>
						</span>
					<?php } ?>
				</div>

			<?php }
			if (!empty($date)) { ?>
				<div class="col-xs-10 col-xs-offset-1 margin-top-10 prev-date">
					<h2> Date</h2>
					<span></span>
				</div>
			<?php }
			if (isset($element["shortDescription"])) { ?>
				<div class="col-xs-10 col-xs-offset-1 margin-top-10">
					<h2> Description courte</h2>
					<span class="col-xs-12 text-center" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
					</span>
				</div>
			<?php } ?>
			
			<?php $tabSpec = array(
				"typePlace" =>
				[
					"name" => "Typologie",
					"icon" => "briefcase"
				],
				"manageModel" =>
				[
					"name" => "Mode de gestion",
					"icon" => "adjust"
				],
                "networks" =>
				[
					"name" => "Réseau d'affiliation",
					"icon" => "users"
                ],
				"siteEnvironment" =>
				[
					"name" => "Milieu",
					"icon" => "expand"
                ],
				"buildingSurfaceArea" =>
				[
					"name" => "Taille de l'espace",
					"icon" => "expand",
					"unit" => "m²"
                ],
				"siteSurfaceArea" =>
				[
					"name" => "Taille du lieu",
					"icon" => "expand",
					"unit" => "hectares"
				],
				"commonTransportsAccess" =>
				[
					"name" => "Transport en commun",
					"icon" => "car"
				],
				"vehiculesAccess" =>
				[
					"name" => "Accessibilité",
					"icon" => "expand"
				],
				"personnesAccessPMR" =>
				[
					"name" => "Accès PMR",
					"icon" => "wheelchair"
				],
			);

			if (isset($element["tags"]) && $element["collection"]!=Person::COLLECTION) {
			?>
				<div class="col-xs-10 col-xs-offset-1 margin-top-20">
					<h2>Caractéristiques du lieu</h2>
					<table class='col-xs-12'>
						<?php foreach ($tabSpec as $k => $v) {
							$strTags = "";
							$tagsList = "";
							
							if(isset($this->costum["lists"][$k])){
								$listCurrent = $this->costum["lists"][$k];
								if (!empty($element["tags"])) {
									foreach ($element["tags"] as $tag) {
										if (in_array($tag, $listCurrent)) {
											$strTags .= (empty($strTags)) ? $tag : "<br>" . $tag;
										}
									}
								}
							}else{
								$strTags = isset($element[$k])?($element[$k]." ".(isset($v["unit"])?$v["unit"]:"")):"";
							}
							
							if (!empty($strTags)) { ?>
								<tr>
									<td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
										<?php
										if (isset($v["icon"])) {
											echo "<i class='fa fa-" . $v["icon"] . " tableIcone'></i>";
										}
										?>
									</td>
									<td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
										<?php
										if (isset($v["name"])) {
											echo $v["name"];
										}

										?>
									</td>
									<td class="col-xs-7" style="padding:8px;"><?php echo ((trim($strTags)==="true")?"Oui":((trim($strTags)==="false")?"Non":$strTags) ); ?></td>
								</tr>
						<?php
							}
						}
						?>
					</table>

				<?php
					foreach ($element["tags"] as $tag) {
						if (!in_array($tag, $listCurrent)) {
							$tagsList .= "#" . $tag . " ";
						}
					}
					?>
					<span class="tagList"><?php echo $tagsList ?></span>
				</div>
			<?php } ?>

			<div class="col-xs-12">
				<?php
				if (isset($element["links"]) && isset($element["links"]["members"])) {
					echo $this->renderPartial('co2.views.pod.listItems', array("title" => "Les membres", "links" => $element["links"]["members"], "connectType" => "members", "number" => 12, "titleClass" => "col-xs-12 title text-gray", "heightWidth" => 50, "containerClass" => "text-center no-padding margin-top-10 margin-bottom-10", "contextId" => $id, "contextType" => $element["collection"]));
				} ?>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var eltPreview = <?php echo json_encode($element); ?>;
		var typePreview = <?php echo json_encode($type); ?>;
		var idPreview = <?php echo json_encode($id); ?>;
		var php_date = <?= json_encode($date??null) ?>;

		function do_load_element_date() {
			if(php_date!=null){
				var keys, str;

				keys = Object.keys(php_date);
				keys.forEach(function(key) {
					php_date[key] = moment(php_date[key]);
				});
				if (keys.length === 1)
					str = 'Le ' + php_date[keys[0]].format('DD/MM/YYYY à HH:mm');
				else if (keys.length > 1) {
					if (php_date[keys[0]].format('DD/MM/YYYY') === php_date[keys[1]].format('DD/MM/YYYY'))
						str = 'Le ' + php_date[keys[0]].format('DD/MM/YYYY') + ' du ' + php_date[keys[0]].format('HH:mm') + ' à ' + php_date[keys[1]].format('HH:mm');
					else
						str = 'Du ' + php_date[keys[0]].format('DD/MM/YYYY HH:mm') + ' au ' + php_date[keys[1]].format('DD/MM/YYYY HH:mm');
				}
				$('.prev-date span').text('');
				if (str)
					$('.prev-date span').text(str);
			}
		}
		jQuery(document).ready(function() {
			var str = directory.getDateFormated(eltPreview, null, true);
			$(".event-infos-header").html(str);
			directory.bindBtnElement();
			coInterface.bindLBHLinks();
			coInterface.bindEvents();
			if(typePreview=="citoyens"){
				do_load_element_date();
			}
			$('.btn-delete-preview').on('click', function() {
				$.confirm({
					title: 'Suppression',
					type: 'red',
					content: 'Voulez-vous supprimer cet élément ?',
					buttons: {
						no: {
							text: trad.no,
							btnClass: 'btn btn-default'
						},
						yes: {
							text: trad.yes,
							btnClass: 'btn btn-danger',
							action: function() {
								dataHelper.deleteDocument({
									id: idPreview,
									collection: typePreview
								}).then(function(resp) {
									if (resp.result) {
										$('.btn-close-preview').click();
										urlCtrl.loadByHash(location.hash);
									}
								});
							}
						}
					}
				});
			});


			$(".btn-edit-tl, .btn-edit-element").off().on("click", function () {
				$("#modal-preview-coop").hide(300);
				$("#modal-preview-coop").html("");
				let thisEl = <?php echo json_encode($element); ?>;
				dyFObj.editMode = true;
				uploadObj.update = true;
				dyFObj.currentElement={type : thisEl.collection, id : $(this).data("id")};
				dyFObj.openForm(
					(thisEl.collection.substr(0, thisEl.collection.length-1)),
					null, 
					{...thisEl, id:$(this).data("id")}
				);
			});

			$("#btn-follow-tl").click(function(){
				if(userId!=""){
					let elementId = $(this).data("id");
					let elementType = $(this).data("type");
					let userId = $(this).data("userid");
					if($(this).data("isfollowed")=="1"){
						links.disconnectAjax( elementType, elementId, userId, "<?= Person::COLLECTION ?>", "followers", null, function(){
							$("#btn-follow-tl").html("<i class='fas fa-user-friends'></i> Suivre");
							$("#btn-follow-tl").data("isfollowed","0");
						});
					}else{
						links.follow( elementType, elementId, userId, "<?= Person::COLLECTION ?>", function(){
							$("#btn-follow-tl").html("<i class='fas fa-user-friends'></i> Suivi(e)");
							$("#btn-follow-tl").data("isfollowed","1");
						});
					}
				}else{
					$(".cosDyn-login").click();
				}
			});
		});
	</script>