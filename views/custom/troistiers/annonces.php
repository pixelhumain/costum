
<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}
	#classifield #nav a{
		background: var(--primary-color) !important;
	}
</style>
<script type="text/javascript">
	alert("here we are")
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	
	var typeClassified=<?php echo json_encode(@$_GET["type"]); ?>;
	var sectionClassified=<?php echo json_encode(@$_GET["section"]); ?>;
	if(notNull(costum) && typeof costum["app"]["#annonces"] != "undefined" && typeof costum["app"]["#annonces"]["filters"] != "undefined" &&  typeof costum["app"]["#annonces"]["filters"]["type"] != "undefined"){
		typeClassified=costum["app"]["#echanges"]["filters"]["type"];
	}
	sectionsClassified={};
	categoriesList={};
	if(notEmpty(typeClassified)){
		$.each(modules[typeClassified].categories.sections, function(k,v){
			sectionsClassified[k]=(typeof tradCategory[v.labelFront] != "undefined") ? tradCategory[v.labelFront] : v.label; 
		});

		$.each(modules[typeClassified].categories.filters, function(k,v){
			labelCat=(typeof tradCategory[k] != "undefined") ? tradCategory[k] : v.label;
			categoriesList[k]={label : labelCat, icon : v.icon};
			if(typeof v.subcat != "undefined"){
				categoriesList[k]["subList"]={};
				$.each(v.subcat, function(e,val){
					labelCat=(typeof tradCategory[e] != "undefined") ? tradCategory[e] : val.label;
					categoriesList[k].subList[e]={label : labelCat, icon : val.icon};
				});			
			}
		});
	}
	var paramsFilter= {
	 	container : "#filters-nav",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	results : {
	 		renderView : "directory.classifiedPanelHtml",
	 		smartGrid : true
	 	},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		section : {
	 			label : "Besoins",
	 			type : "section",
	 			view : "dropdownList",
	 			event : "filters",
	 			keyValue : false,
	 			list : sectionsClassified
	 		},
	 		category : {
 				view : "complexeSelect",
	 			name : "Catégories", 
	 			event : "selectList", 
	 			list : categoriesList,
	 			multiple:true,
	 			keyValue : false,
	 			levelOptions : [
	 				{
	 					level : 1,
	 					type : "category",
	 					separator : true,
	 					visible : true 
	 				},
	 				{
	 					level : 2,
	 					class : "col-xs-12 no-padding",
	 					type : "subType",
	 					visible : true 
	 				}
	 			]
	 		}
	 	}
	};
	if(notNull(typeClassified) && typeClassified=="classifieds"){
		paramsFilter.filters.price=true;
	}
	paramsFilter.defaults={
		fields : ["price","devise","description","contactInfo", "section", "subtype"],
		types : ["classifieds"],
		type : typeClassified,
		"source.keys":"troistiers",
		forced : {
			//type:typeClassified
		}
	};
	if(typeof appConfig.extendFilters != "undefined"){
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	}

	if(notNull(sectionClassified))
		paramsFilter.defaults.section=sectionClassified;
	var filterSearch={};
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});
</script>