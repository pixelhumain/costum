<style>
	.cmsbuilder-container #filters-nav {
        position: absolute !important;
    }
	@media (min-width: 768px) {
		#filters-nav{
			position: sticky !important;
		}
	}
	.headerSearchContainer, .bodySearchContainer{
		width: 100%;
		margin-left: 0px !important;
		padding-left: 5px !important;
		padding-right: 0px !important;
	}
	.main-container{
		padding-top: 106px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS{
		padding-top: 12px !important;
	}
	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
		width: 210px !important;
	}
	.dropdown-title{
		display: none;
	}
	.thin {
		padding: 1em;
		font-size: 16px;
		text-transform: uppercase;
		width: 100%;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
		height: 250px;
		
	}
	.basic-banner {
		width: 100%;
		height: 100%;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
		display:flex;
		align-items: center;
		justify-content: space-around;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-yellow-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.tab-outer{
		justify-content: space-around !important;
	}

	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	#filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn {
		margin-left: 0px !important;
	}
	.hidden-lg .initialisSearch{
		margin-top: 18px;
		width: 100%;
		margin-left: 0;
	}
	.initialisSearch{
		border-radius: 2px !important;
		border: 1px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 10px;
		padding: 10px 15px;
		font-size: 16px;
		border: 1px solid #6b6b6b;
		color: #6b6b6b;
		top: 0px;
		position: relative;
		border-radius: 20px;
		text-decoration: none;
		background: white;
		line-height: 20px;
		display: inline-block;
		margin-left: 5px;
		color: var(--main1);
	}
	@media (max-width: 767px)  {
		#filters-nav {
			position: fixed !important;
		}
		#activeFilters {
			margin-top: 15px !important;
		}
		.container-filters-menu .searchBar-filters .search-bar {
			width: 85% !important;
		}
		.container-filters-menu .btn-show-map {
			margin-top: 15px;
			width: 100% !important;
		}
		.searchObjCSS .container-filters-menu .searchBar-filters {
			width: 100% !important;
		}
		.container-filters-menu #right-side {
			 padding: 0px !important; 
		}
	}
</style>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filtersByPage = {};
	var filterFields = [];
	var pageDirectory = "directory.cards";//"directory.elementPanelHtmlSmallCard";
	var defaultFilters = {'$or':{}};
	var sortResultBy = {"name":1};
	// Organizations
	defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["parentId"] = costum.contextId;
	defaultFilters['$or']["source.keys"] = costum.slug;
	defaultFilters['$or']["reference.costum"] = costum.slug;
	defaultFilters['$or']["links.contributors."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
	defaultFilters["links.memberOf."+costum.contextId+".roles"] =  {'$nin':["codev"]};

    var affichage = "";

	// Not validated
	defaultFilters["toBeValidated"]={'$exists':false};
	
	if(pageApp.indexOf("?")!=-1){
		pageApp = pageApp.split("?")[0];
	}

	if(pageApp == "tierslieux" || pageApp == "cartographie"){
		// pageDirectory = "directory.logos";
		filtersByPage = {
			text : {
				placeholder : "Recherche de tiers-lieux"
			}
		}
		if(typeof costum.typeObj != "undefined" && 
			(typeof costum.typeObj.organizations != "undefined" || typeof costum.typeObj.organization!="undefined") && 
			costum.lists){

			var costumInputs = costum.typeObj.organizations||costum.typeObj.organization;
			if(costumInputs && costumInputs.dynFormCostum && costumInputs.dynFormCostum.beforeBuild && costumInputs.dynFormCostum.beforeBuild.properties){
				costumInputs = costumInputs.dynFormCostum.beforeBuild.properties
			}else if(costumInputs && costumInputs.dynFormCostum && costumInputs.dynFormCostum.beforeBuild){
				costumInputs = costum.typeObj.organization.dynFormCostum.beforeBuild.properties;
			}

			var orderedFilters = {};

			if(typeof appConfig.filtersOrder != "undefined"){
				$.each(appConfig.filtersOrder, function(index, filter){
					if(costumInputs[filter]){
						orderedFilters[filter] = costumInputs[filter];
					}
				});
			}else{
				orderedFilters = costumInputs;
			}

			$.each(orderedFilters, function(index, input){
				if(typeof input.list != "undefined" && costum.lists[input.list]){
					console.log("input key",input.list);
					let optionList = Array.isArray(costum.lists[input.list])?costum.lists[input.list].reduce((a, v) => ({ ...a, [v]: v}), {}):costum.lists[input.list];
					let filterView = "dropdownList";
					if(index.toLowerCase().includes("odd")){
						filterView = "largeMenuDropdown";
					}
					// to do add condition for specific filterView
					filtersByPage[input.list] = {
						view : filterView,
						type : "tags",
						trad:false,
						name : (input.placeholder.length < 30)?input.placeholder: ((input.label.length < 30)?input.label:index),
						event : "tags", 
						list : optionList
					}
				}
			});

			filtersByPage["openingHours"] = {
				view : "dropdownList",
				type : "openingHours",
				name : "Ouvert maintenant",
				field : "openingHours",
				event : "openingHours",
				list : {
					"openingHours" : "Ouvert maintenant"
				}
			}
		}
	}

    if(pageApp == "annuaire-global"){
		filtersByPage = {
			acteur : {
				view : "dropdownList",
				type : "filters",
				field : "collection",
				name : "Type",
				keyValue : false,
				event : "filters",
				list : {
					"citoyens" : "Acteurs"
				}
			},
			tiersLieux : {
				view : "dropdownList",
				type : "filters",
				field : "collection",
				name : "Type",
				keyValue : false,
				event : "filters",
				list : {
					"organizations" : "Tiers lieux"
				}
			},
			accompagnant : {
				view : "dropdownList",
				type : "filters",
				name : "Type",
				field : "links.memberOf."+costum.contextId+".roles",
				event : "inArray",
				list : [
					"Accompagnant.e.s"
				]
			},
			openingHours : {
				view : "dropdownList",
				type : "openingHours",
				name : "Ouvert maintenant",
				field : "openingHours",
				event : "openingHours",
				list : {
					"openingHours" : "Ouvert maintenant"
				}
			},
			text : true
		}
        if(typeof appConfig.filters == "undefined"){
            appConfig.filters = {};
        }
        appConfig.filters.types = ["citoyens","organizations"];
	}
    if(pageApp == "membres"){
		filtersByPage = {
			text : {
				placeholder : "Recherche des acteurs"
			}
		}
        if(typeof appConfig.filters == "undefined"){
            appConfig.filters = {};
        }

        appConfig.filters.types = ["citoyens"];
	}
    if(pageApp == "accompagnants" || pageApp == "membres"){
		// pageDirectory = "directory.logos";
		if(pageApp == "accompagnants"){
			defaultFilters["links.memberOf."+costum.contextId+".roles"]="Accompagnant.e.s";
		}
		let formatedSkills = {};
		if(typeof costum.lists.skills == "object"){
			for (const [key, value] of Object.entries(costum.lists.skills)) {
				if(typeof value == "object"){
					formatedSkills = {...formatedSkills , ...value};
				}
			}
		}
		filtersByPage = {
			skills: {
				view: "dropdownList",
				type: "filters",
				event: "filters",
				action: "filters",
				field: "skills",
				name: "Compétences",
				list: formatedSkills || costum.lists.skills
			},
			text : {
				placeholder : "Recherche d'accompagnants"
			}
		}
        if(typeof appConfig.filters == "undefined"){
            appConfig.filters = {};
        }

        appConfig.filters.types = ["citoyens"];
	}

	if(pageApp == "news"){
		pageDirectory = "directory.cards";
		filterFields = ["mediaImg", "text", "organizer", "date", "parent", "created"];
		// defaultFilters["type"]={'$ne':"activityStream"};
		defaultFilters["type"]={'$nin':["activityStream", "tool", "doc"]};
		filtersByPage = {
			tabs : {
	 			view : "tabs",
	 			type : "tags",
				event : "tabs",
	 			list : {
					"ACTUALITÉS" : {
						icon: "fa-calendar",
						type:"types",
						value:"news",
                        class: "active"
					},
					"ÉVÉNEMENTS" : {
						icon: "fa-calendar",
						type:"types",
						value:"events"
					}
				}
	 		}
		};
	}

	if(pageApp=="projects"){
		pageDirectory = "directory.cards";
	}

	if(pageApp=="citoyens"){
		pageDirectory = "directory.profils";
	}

	var paramsFilter = {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		header : {
			options :{
				left : {
					classes : 'col-xs-12 col-sm-4 col-md-4 no-padding',
					group : {
						count : true
					}
				},
				right : {
					classes : 'col-xs-12 col-sm-8 col-md-8 text-right no-padding',
					group : {
						map : true	
					}
				}
			}
		},
		defaults : {
			notSourceKey: true,
		 	types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	filters: defaultFilters,
			indexStep: 900,
			sortBy: sortResultBy
		},
		filters : filtersByPage,
	 	results : {
			renderView: pageDirectory,
		 	smartGrid : true
		}
	}

	if(filterFields.length!=0){
		paramsFilter.defaults["fields"] = filterFields;
	}
	var actualityData = {}
	 
	jQuery(document).ready(function(){
		searchObj.filters.views["tabs"] = function(k,v, fObj){
			str = `<div class="tab-outer"> 
						<div id="tabs-container" class="tabs left large-tab">`;
			if(v.list){
				$.each(v.list, function(key, item){
					let dataFilter = "";
					if(item.page){
						dataFilter =  'href="'+item.page+'"';
					}else{
						dataFilter = `data-type="${item.type??v.type??"filters"}" 
							data-key="${item.key??key}" 
							data-value="${item.value??item.label??key}" 
							data-field="${item.field??v.field??"tags"}" 
							`;
						//if(item.type && item.type=="filters"){
							dataFilter+=` data-label="${item.label??key}" `;
							dataFilter+=` data-event="${item.event??v.event??"filters"}" `;
						//}
					}
					let icon = `<span class="ico-circle"><i class="fa ${(item.icon!="")?item.icon:"fa-tag"}"></i></span>`;
					if(item.icon==null){
						icon="";
					}
					str+= `<div class="term-item ${item.class??""}">
						<a type="button" class="${item.page?"lbh":"btn-filters-select"} tabs" ${dataFilter} data-filterk="tabs">${icon} ${item.label||key}</a>
					</div>`;
				});
			}
			str+=`</div>
			</div>`;

			return str;
		}

		searchObj.filters.events["tabs"] = function(fObj, domFilters, k){
			$(".btn-filters-select").off().on("click", function(event){
				let type = $(this).data("type");
				let field = $(this).data("field");
				if(type!="types"){
					if(field=="tags"){
						fObj.search.obj.tags = [$(this).data("value")];
					}else{
						fObj.search.obj.filters[field] = $(this).data("value")
					}
					fObj.search.obj.types = appConfig.filters.types||["news", "events", "poi"];
					fObj.filters.actions.types.initList = appConfig.filters.types||["news", "events"];
				}else{
					fObj.search.obj.tags = [];
					fObj.search.obj.types = [$(this).data("value")];
					fObj.filters.actions.types.initList =  [$(this).data("value")];
				}
				$(".term-item").each(function(){
					$(this).removeClass("active");
				});
                if(pageApp == "news"){
                    affichage = "tml";
                }
				$(this).parent().addClass("active")
				fObj.search.init(fObj);
			});
		}

        if(pageApp == "accompagnants" || pageApp == "tierslieux"){
            searchObj.header.events.map = function(fObj){
                $(".btn-show-map").off().on("click", function(e){
                    fObj.helpers.toggleMapVisibility(fObj);
                    var text = filterSearch.results.map.active ? '<i class="fa fa-list-alt"></i> '+tradCms.directoryPage : '<i class="fa fa-map-marker"></i> '+trad.map;
                    $(".btn-show-map").html(text);
                });
            }
        }

		searchObj.search.sortAlphaNumeric = function(results){
    		mylog.log("searchObj.search.callBack tosort", results);
			let sorted = Object.values(results).sort((a, b) => {
				let fa = a.name.replace(" ", "").trim().toLowerCase(),
					fb = b.name.replace(" ", "").trim().toLowerCase();

				return ('' + fa).localeCompare(fb);
			});
			let sortedResults = {}
			$.each(sorted, function(i, val){
				sortedResults[val._id.$id] = val;
			});
			return sortedResults;
    	}

		searchObj["scroll"].render = function(fObj, results, data){
			results = searchObj.search.sortAlphaNumeric(results);
      		//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				// if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				// }else{
				// 	toastr.error("Aucun résultat trouvé. Essayez avec un terme plus précis.");					
				// }	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique);
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
					}
					imageObserver.observe(v);
				});
			}
		}

		filterSearch = searchObj.init(paramsFilter);

		$("#filterContainerInside").append("<div id='right-side'></div>");
		$(".searchBar-filters").append("<div class='text-right hidden-xs' style='padding-left: 24px;'><a href='javascript:;' class='initialisSearch btn '>Effacer tout</a></div>");
		$(".menu-filters-xs").append("<div class='text-right hidden-lg hidden-md  visible-xs' style=''><a href='javascript:;' class='initialisSearch btn '>Effacer tout</a></div>");
		

		$(".initialisSearch").on("click", function(){
			urlCtrl.loadByHash("#"+pageApp);
		})
		if(["news", "cartographie"].indexOf(pageApp)!=-1){
			$(".btn-show-map").hide();
			$(".filter-btn-hide-map").hide();
			$("#filters-nav").css("padding", "0px");
			$("#filters-nav").css("border-bottom", "0px");
			$("#filterContainerInside").css("padding", "0px");
		}else{
			$("#filters-nav").css("border-bottom", "1px solid rgba(100, 100, 100, 0.1)");
			$("#filters-nav").css("padding-right", "5px");
			$("#filters-nav").css("padding-bottom", "9px");
			$("#filters-nav").css("padding-left", "5px");
			$("#filters-nav").css("padding-top", "12px");
			$(".btn-show-map").addClass("btn-primary-ekitia btn-primary-outline-ekitia").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo("#right-side");
			$(".btn-show-map").removeClass("hidden-xs");
		}

		if($(".tab-outer").length!=0){
			if(appConfig && typeof appConfig.filters !="undefined" && typeof appConfig.filters.types!="undefined"){
				let addBtnDropdown = "";
				if(appConfig.filters.types.length==1){
					if(appConfig.filters.types[0]=="answers" && typeof costum.lists!="undefined" && costum.lists["numericServiceForms"]){
						addBtnDropdown = prepareAddBtnDropdown(costum.lists["numericServiceForms"]);
					}else{
						addBtnDropdown = '<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-open-form" data-form-type="'+appConfig.filters.types[0]+'">POSTER</button></div>';
					}
				}else if(appConfig.filters.types.length>1){
					addBtnDropdown = prepareAddBtnDropdown(appConfig.filters.types);
				}
				$(".tab-outer").append(addBtnDropdown);
				// if ($.inArray("poi", appConfig.filters.types) && pageApp == "news") {
				// 	$(".tab-outer").append('<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				// }
			}
		}else{
			$("#right-side").prepend("")
			$("#right-side").insertBefore("#activeFilters");
		}
		$("#activeFilters").hide();
		//$("#filterContainerInside").append("<button class='btn btn-primary-ekitia'>POSTER</button>")
		$(".activeFilter-label").remove();
		$(".headerSearchContainer").remove();
		
		$(".banner-outer").remove();

		// Events
		$(".btn-show-map, .btn-filters-select").on("click", function(){
			if(filterSearch.results.map.active){
				$("#bannerSearch").hide();
				$('html,body').animate({
					scrollTop: $("#filters-nav").offset().top},
				'slow');
			}else{
				$("#bannerSearch").show();
			}
            if($("#show-filters-xs").is(":visible")){
                $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
            }
			if ($("div.btn-event-actus-ctn").length > 0)
				$("div.btn-event-actus-ctn").remove();
			if ($(this).attr("data-value") == "events" && pageApp == "news") {
				$(".tab-outer").append('<div class="term-item btn-event-actus-ctn"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				$(".tab-outer button.btn-actuality-switch").click( function() {
					var viewDisplayed = $(this).attr("data-switch");
                    affichage = viewDisplayed;
					loadActualityView()[viewDisplayed](this);
				})
			}
				
		});
        $("#show-filters-xs").on("click", function(){
            $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
        })
		$(".filter-btn-hide-map").on("click", function(){
            $(".btn-show-map").html('<i class="fa fa-map-marker"></i> '+trad.map);
			$("#bannerSearch").show();
		})

		/**
		 * Manage timeline view
		 */
		function loadActualityView () {
			var views = {};
			views.change = function (selector, name="", viewType="tml", icon="fa-th-list") {
				$(selector).html('<i class="fa ' + icon + '"></i> ' + name);
				$(selector).attr("data-switch", viewType);
			};
			views.tml = function (selector) {
				views.change(selector, "Vue vignette", "crd", "fa-th-list");
				if (notEmpty(views.timelineConetent())) {
					$("#dropdown_search").html(`<div class="container-timeline"><div id="timeline" class="timeline-ctn"></div></div>`);
					$(".timeline-ctn").html(views.timelineConetent());
					coInterface.bindLBHLinks();
				}
			};
			views.crd = function (selector) {
				views.change(selector, "Vue timeline", "tml");
				filterSearch.results.renderView = "directory.cards";
				filterSearch.search.init(filterSearch);
			};
			views.timelineConetent = function () {
				var resultHtml = "";
				var direction = "";
				$.each(actualityData, function(k,v) {
					var hash = "";
					var image = (v.profilMediumImageUrl != undefined) ?  v.profilMediumImageUrl : "https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg";
					if (v.collection == "events") {
						resultHtml += `
							<div class="timeline-item">
								<div class="timeline-icon">
									<div class="date-timeline"> 
										<b>${moment.unix(v.created).local().locale('fr').format('DD')}</b>/${moment.unix(v.created).local().locale('fr').format('MM')} 
										<br><b>${moment.unix(v.created).local().locale('fr').format('YYYY')}</b>
									</div>
								</div>
								<div class="timeline-content ${direction}">
									<div class="image-with-title">
										<div class="image-timeline-content">
											<img width="150" height="150" src="${image}" style="border-radius: 5px;" class="img-responsive" alt="" decoding="async">
										</div>
										<div class="info-timeline">
											<h4>${(v.name != undefined) ? v.name : "Il n'y a pas de titre"}</h4>
											${(v.type != undefined) ? '<h5 class="type-timeline"><i class="fa fa-chevron-right"></i> ' + v.type + '</h5>' : ""} 
											<div class="interval-timeline"> 
												<i class="fa fa-calendar"></i> ${moment.unix(v.startDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
												<i class="fa fa-long-arrow-right"> </i> ${moment.unix(v.endDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
											</div>
										</div>
									</div>
									<div class="timeline-desc">`;
						if (jsonHelper.getValueByPath(v, "organizer") != undefined && Object.keys(jsonHelper.getValueByPath(v, "organizer")).length > 0) {
							var organizerId = Object.keys(jsonHelper.getValueByPath(v, "organizer"))[0];
							var organizerName = jsonHelper.getValueByPath(v, "organizer")[organizerId].name;
							hash = `#page.type.organizations.id.${organizerId}`;
						resultHtml += 	`<div> 
											<a href="${hash}" class="lbh-preview-element padding-5 timeline-prev"><span class="ico-circle"></span> ${organizerName}</a>
										</div>`
						}	
						resultHtml +=	`<p class="timeline-short-desc">
											${(v.shortDescription != undefined && !(v.shortDescription == "")) ? v.shortDescription : "<i>Description non renseignée</i>"}
										</p>`		
						if (v.tags != undefined && Array.isArray(v.tags) && v.tags.length > 0) {
						resultHtml += `<div class="tag-content">`;
							v.tags.forEach(function(tagContent) {
								resultHtml += `<p class="tag-element">#${tagContent}</p>`;
							})
						resultHtml += `</div>`;
						}	
						if (v._id.$id != undefined) {
							resultHtml += `
										<div class="link-bordered">
											<a href="#page.type.events.id.${v._id.$id}" class="lbh-preview-element">En savoir plus</a>
										</div>
									`;
						}
						resultHtml +=
									`</div>
								</div>
							</div>
						`;
						direction = (direction == "") ? "right" : "";
					}
				})
				return resultHtml;
			};
			return views;
		}

		function prepareAddBtnDropdown(config){
			let addBtnDropDown = `<div class="btn-group">
			<button class="btn btn-primary-ekitia btn-primary-outline-ekitia dropdown-toggle" type="button" data-toggle="dropdown">POSTER
			<span class="caret"></span></button>
			<ul class="dropdown-menu pull-right">`;
			$.each(config, function(index, type){
				/*if(type=="news"){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`#live`);" class="padding-10">'+(type)+'</a></li>';
				}else*/ if(typeof type=="object" && type!=null){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`'+type.value+'`);" class="padding-10">'+(type.name||type.label||"Formaulaire")+'</a></li>';
				}else{
					let label = type;
					if(type=="news"){
						type="poi"; label="Actualité"
					}
					if(type=="newsletter"){
						return;
					}
					if(label=="poi"){
						label=typeObj[label].name;
					}
					let t = ((typeObj[type] && typeObj[type].sameAs)?typeObj[type].sameAs:type);
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(``, `'+t+'`);" class="padding-10" data-form-type="'+t+'">'+label+'</a></li>';
				}
			});
			addBtnDropDown+="</ul></div>";
//co2/app/view/url/news.views.co.formCreateNews
			return addBtnDropDown;
		}
	})
</script>