<?php 
	$photos = Document::getListDocumentsWhere(
		array(
			"id"=> (string)$element['_id'],
			"type"=>$element["collection"],
			"contentKey"=>'photo',
		), "image"
	);

	$photoPath = "";

	foreach ($photos as $k => $doc) {
		$photoPath = $doc['imagePath'];
		break;
	}
?>

<style>
.section-badges{
	background-color: unset !important;
}

.menu-btn-follow{
	display:none !important;
}

.boxBtnLink{
	background-color: #FF286B;
	border:solid 1px #fff;
}

/*.boxBtnLink:hover{
	opacity:0.75;
}*/
.menu-linksBtn:hover{
	background-color: unset;
	opacity:unset;
}
.header-address-tags .badge {
    margin: 5px;
    width: fit-content;
    height: 20px;
    background: #ea4335;
    color: #fff;
}
.header-address-tags {
	height: 100%;
    display: flex;
    align-items: end;
}
.header-tags .badge {
    margin: 5px;
    width: fit-content;
    height: 20px;
    background: #fff;
}
@media (min-width: 768px) {
    .header-tags .badge {
        margin-left: 3px;
        font-size: 13px;
    }
}

.banner-outer {
	background-color: #f9f9f9;
	background-image: url("<?= $photoPath ?>");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	/*background-repeat: no-repeat;
	background-size: 25% auto;*/
}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	/*.basic-banner {
		padding-top: 70px;
		padding-bottom: 130px;
		background-image: url("<?= $photoPath ?>");
		background-repeat: no-repeat;
		background-position: center;
		background-size: cover;
		background-position: top left;
		background-size: 25% auto;
	}*/

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

    .title-banner h1::before {
        content: "";
        position: absolute;
        /* top: -30%; */
        z-index: -1;
        /* margin-left: -1em; */
        width: 2em;
        height: 2em;
        background-color: var(--primary-color);
        border-radius: 50%;
        /* top: -80%; */
        top: -50%;
        /* margin-left: -12px;*/
    }

	.contentHeaderInformation {
    	background-color: transparent !important;
		/*height: -webkit-fill-available;*/
		margin-bottom: 50px;
	}

	.backgroundHeaderInformation {
    	background: transparent !important;
		margin-bottom: 50px !important;
	}

	#menu-top-profil-social #menu-top-btn-group {
		margin-top: -53px !important;
	}

	.link-banner {
		margin: 0px 5px;
		font-weight: bold;
    	color: white;
	}

	.social-main-container, #central-container{
		min-height: 0px !important;
	}

	.btn-menu-citoyen{
		padding: 50px;
    	display: inline-table; 
    	padding: 5px 22px; 
    	font-size: 22px; 
    	margin-bottom: 45px !important; 
    	font-weight: 500; 
    	text-transform: uppercase; 
    	border-bottom: 1px solid #cccccc !important;
	}

	.border-bottom-ekitia{
		border-bottom: 4px solid #ba1c24 !important;
	}

	.bg-ekitia{
		background-color: var(--primary-color) !important;
	}

	.btn-edit-tags{
		color: #fff !important;
		background-color: var(--primary-color) !important;
		border-color: var(--primary-color) !important;
		border-radius: inherit;
	}

	.elementButtonHtml{
		text-transform: capitalize !important;
	}

	.tag-banner-element{
		background-color: var(--primary-color) !important;
		color:white !important;
		border-radius: 5px;
		margin-right: 2px;
	}


	a.ssmla.btn-menu-tooltips{
		font-size: 20px;
		margin-bottom: 20px !important;
		border: 2px solid white !important;
		font-weight: 500;
		background-color:white !important;
		color: var(--primary-color) !important;
		border-top-left-radius: 8px;
		border-top-right-radius: 8px;
		border-bottom-left-radius: 0px;
		border-bottom-right-radius: 0px;
		margin: 0.5px;
	}

	a.ssmla.btn-menu-tooltips.active,  #menu-top-btn-group #btn-start-detail.active{
		font-size: 20px;
		margin-bottom: 20px !important;
		border: 2px solid var(--primary-color) !important;
		font-weight: 500;
		background-color: var(--primary-color) !important;
		color:white !important;
	}

	#menu-top-btn-group #btn-start-detail.active span{
		color:white !important;
	}

	#menu-top-btn-group #btn-start-detail span{
		color:var(--primary-color) !important;
	}

	#social-header {
		min-height: 280px;
		padding: 0px;
		padding-top: 0px;
		background: linear-gradient(-90deg, var(--primary-color), rgba(2,173,231,0.2)), url("<?= $photoPath ?>") no-repeat;
		background-position: center;
		background-size: cover;
	}

	@media (min-width: 768px) and (max-width: 991px) {
		#menu-top-profil-image #fileuploadContainer {
			width: 155px !important;
			height: 155px !important;
			margin-top: -240px !important;
		}
	}

	#menu-top-profil-image #fileuploadContainer {
		margin-top: -240px !important;
	}
</style>

<div class="btn-group pull-left no-padding" >
    <!-- Btn Link -->
    <div class="pull-left no-padding">
        <?php
        if(@Yii::app()->session["userId"] && @Yii::app()->session["userId"] != (string)$element["_id"]){ ?>
            <div class="pull-left boxBtnLink no-padding bg-dark-green" style="border-radius: 30px; display: flex;">
                <?php  
				// echo $this->renderPartial('co2.views.element.menus.links',
                //     array(  "linksBtn" => $linksBtn,
                //         "element"   => $element,
                //         "openEdition" => $openEdition )
                // );
                ?>
            </div>
        <?php } ?>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 text-white pull-right margin-bottom-5">
		<?php if (@$element["status"] == "deletePending") { ?> 
			<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
		<?php } ?>
		<h4 class="text-left padding-left-15 pull-left no-margin">
			<span id="nameHeader">
				<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
					
				</div>
				<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
				<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
				<?php /* echo $this->renderPartial('co2.views.element.menus.links',
					array(  "linksBtn" => $linksBtn,
						"element"   => $element,
						"openEdition" => $openEdition )
					);*/
				?>
			</span>
			<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
					$typesList=Event::$types;
			?>
				<span id="typeHeader" class="margin-left-10 pull-left">
					<i class="fa fa-x fa-angle-right pull-left"></i>
					<div class="type-header pull-left">
						<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
					</div>
				</span>
			<?php } ?>
		</h4>	

		<!-- <?php if($edit){ ?>
			<button class="btn-danger text-white pull-right" style="border: none;padding: 5px 10px;border-radius: 5px;" onclick="directory.deleteElement('<?php echo $element["collection"] ?>', '<?php echo (string)$element["_id"] ?>');"><i class="fa fa-trash"></i> Supprimer</button>
		<?php } ?>	 -->

	<?php 
		$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" );

		if(!empty($cteRParent))
			$classAddress = "";
	?>

	<div class="header-address col-xs-12 margin-left-5">
		<?php if(!empty($element["address"]["addressLocality"])){ ?>
			<div class="header-address badge letter-white padding-5  margin-left-5 bg-red">
				<?php
					echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
					echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
					echo $element["address"]["addressLocality"];
				?>
			</div>
		<?php } ?>
	</div>
	
	<div class="header-tags pull-left  col-xs-12" style="margin-top: 10px; display:flex; flex-wrap:wrap" >

        <?php
			$typePlaces = array_reduce($this->costum["lists"]["typePlace"], function($v1, $v2){
				return array_merge($v1, $v2);
			}, []);
			$networks = $this->costum["lists"]["networks"];
			$access = $this->costum["lists"]["vehiculesAccess"];
			$sites = $this->costum["lists"]["siteEnvironment"];
			
			if(!empty($element["tags"])){
                $countTags=count($element["tags"]);
                $element["tags"] = array_unique($element["tags"]);
                $nbTags=1;
				$page = ($element["collection"]==Organization::COLLECTION)?"tierslieux":"membres";
                foreach ($element["tags"]  as $key => $tag) {
					if(strtolower($tag)!="autre"){
						$order = 0;
						switch ($tag) {
							case in_array($tag, $typePlaces):
								$order=0;
								break;
							case in_array($tag, $networks):
								$order = 1;
								break;
							case in_array($tag, $access):
								$order = 2;
								break;
							case in_array($tag, $sites):
								$order = 3;
								break;
							default:
								$order = 4;
								break;
						}
						if($nbTags <= 10){
							echo '<a href="#'.$page.'?tags='.$tag.'" class="badge tag-banner-element lbh letter-red bg-white " style="margin-top: 3px; order:'.$order.'"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</a>';
							$nbTags++;
						}else{
							if(($countTags-$nbTags) > 0) echo "<span class='badge indicate-more-tags letter-red bg-white' style='order:".$order."'>+".($countTags-$nbTags)."</span>";
							break;
						}
					}
                }
            } ?>
		</div>
	</div>

<?php 
	echo $this->renderPartial('co2.views.element.menus.answerInvite', 
		array( 
			"invitedMe" => $invitedMe,
			"element"   => $element
		) 
	); 

$userId = Yii::app()->session["userId"]??null;
$type = $element["collection"];

?>
</div>
<div class="padding-10">
	<?php if($element["collection"]!=Person::COLLECTION && (Authorisation::canEdit($userId, (string)$element["_id"], "poi")|| Authorisation::isCostumAdmin())){ ?>
		<button class="btn btn-light pull-right margin-right-10 btn-edit-tl" data-type="<?php echo $element["collection"] ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo ($element["type"]??$element["collection"]) ?>" title="Modifier">
			<i class="fa fa-pencil margin-right-5"></i> Modifier les informations
		</button>
	<?php } ?>
	<?php if(in_array($element["collection"], [Person::COLLECTION, Organization::COLLECTION])){ 
		$followed = (isset($element["links"]) && isset($element["links"]["followers"]) && isset($element["links"]["followers"][$userId]));
	?>
	<button id="btn-follow-tl" class="btn btn-light pull-right margin-right-10" 
		data-toggle="tooltip" data-placement="left" data-original-title="Suivre"
		data-type="<?= $element["collection"] ?>" 
		data-id="<?= $element["_id"] ?>" 
		title="Suivre"
		data-userid="<?= $userId ?>"
		data-isfollowed="<?= $followed ?>"
		>
		<i class="fa fa-bell margin-right-5"></i><?= ($followed)?"Suivi(e)":"Suivre" ?>
	</button>
	<?php } ?>
	<?php if($element["collection"]!=Person::COLLECTION && $element["collection"]!=Organization::COLLECTION &&(Authorisation::canEdit($userId, (string)$element["_id"], "poi")|| Authorisation::isCostumAdmin())){ ?>
		<button class="btn btn-danger pull-right margin-right-10 btn-delete-tool-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" title="Supprimer">
			<i class="fa fa-trash"></i> Supprimer
		</button>
	<?php } ?>
</div>

<script>
	jQuery(document).ready(function($){
		//$("#menu-top-profil-social").appendTo("#social-header")
		$('.btn-delete-preview').on('click', function() {
				$.confirm({
					title: 'Suppression',
					type: 'red',
					content: 'Voulez-vous supprimer cet élément ?',
					buttons: {
						no: {
							text: trad.no,
							btnClass: 'btn btn-default'
						},
						yes: {
							text: trad.yes,
							btnClass: 'btn btn-danger',
							action: function() {
								dataHelper.deleteDocument({
									id: idPreview,
									collection: typePreview
								}).then(function(resp) {
									if (resp.result) {
										$('.btn-close-preview').click();
										urlCtrl.loadByHash(location.hash);
									}
								});
							}
						}
					}
				});
			});

			$(".btn-edit-tl, .btn-edit-element").off().on("click", function () {
				$("#modal-preview-coop").hide(300);
				$("#modal-preview-coop").html("");
				let thisEl = <?php echo json_encode($element); ?>;
				dyFObj.editMode = true;
				uploadObj.update = true;
				dyFObj.currentElement={type : thisEl.collection, id : $(this).data("id")};
				dyFObj.openForm(
					(thisEl.collection.substr(0, thisEl.collection.length-1)),
					null, 
					{...thisEl, id:$(this).data("id")}
				);
			});

			$("#btn-follow-tl").click(function(){
				if(userId!=""){
					let elementId = $(this).data("id");
					let elementType = $(this).data("type");
					let userId = $(this).data("userid");
					if($(this).data("isfollowed")=="1"){
						links.disconnectAjax( elementType, elementId, userId, "<?= Person::COLLECTION ?>", "followers", null, function(){
							$("#btn-follow-tl").html("<i class='fa fa-bell margin-right-5'></i> Suivre");
							$("#btn-follow-tl").data("isfollowed","0");
						});
					}else{
						links.follow( elementType, elementId, userId, "<?= Person::COLLECTION ?>", function(){
							$("#btn-follow-tl").html("<i class='fa fa-bell margin-right-5'></i> Suivi(e)");
							$("#btn-follow-tl").data("isfollowed","1");
						});
					}
				}else{
					$(".cosDyn-login").click();
				}
			});
	})
</script>

