<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;
use yii\helpers\VarDumper;

$isAdmin = Authorisation::isElementAdmin($element['_id'], $element['collection'], Yii::app()->session["userId"]);
if ($element["collection"] == Person::COLLECTION) {
    $isAdmin = ((string) $element['_id']) == Yii::app()->session["userId"];
}
$cssAnsScriptFilesModule = array(
    //Data helper
    '/js/dataHelpers.js',
    '/js/default/editInPlace.js',
    '/css/element/about.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$sortableAssets = ['/plugins/jquery.sortable/Sortable.js', '/plugins/jquery.sortable/jquery-sortable.js'];
HtmlHelper::registerCssAndScriptsFiles($sortableAssets, Yii::app()->request->baseUrl);
$elementParams = @$this->appConfig["element"];
if (isset($this->costum)) {
    $cssJsCostum = array();
    if (isset($elementParams["js"]) && $elementParams["js"] == "about.js")
        array_push($cssJsCostum, '/js/' . $this->costum["slug"] . '/about.js');
    if (isset($elementParams["css"]) && $elementParams["css"] == "about.css")
        array_push($cssJsCostum, '/css/' . $this->costum["slug"] . '/about.css');
    if (!empty($cssJsCostum))
        HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule("costum")->getAssetsUrl());
}
$isFromFediverse = Utils::isFediverseShareLocal($element);
$isLocalActor = boolval(Utils::isLocalActor());
if (@$element["fromActivityPub"] && $element["fromActivityPub"] == true) {
    $activities = Utils::activitypubToElement($element["objectId"]);
    foreach ($activities as $activityKey => $activityValue) {
        $element[$activityKey] = $activityValue;
    }
}
?>

<!--
<div class='col-md-12 margin-bottom-15 text-dark-blue'>
    <i class="fa fa-info-circle fa-2x"></i><span class='Montserrat' id='name-lbl-title'> <?php echo Yii::t("common", "About") ?></span>
</div>
-->
<?php
if ($isFromFediverse) {
    if ($isLocalActor) {
        $edit = isset($_POST["canEdit"]) ? filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : $edit;
    } else {
        $edit = false;
    }
} else {
    $edit = isset($_POST["canEdit"]) ? filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : $edit;
}
?>
<style>
    .pod-info-projectsInProgress .text-nv-3{
        margin-top: 0px;
    }
    #ajax-modal .container .modal-header{
        color: white !important;
        padding: 15px !important;

    }
</style>
<script>

function redirigerVersPage(url) {
      window.open(url, "_blank");
    }
</script>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1">

    <div class="about-xs-completed visible-xs">
        <div class="about-avatar">
            <?php
            echo $this->renderPartial(
                'co2.views.pod.fileupload',
                array(
                    "podId" => "modalDash",
                    "element" => $element,
                    "edit" => $edit,
                    "openEdition" => false
                )
            ); ?>
        </div>
        <div class="well col-xs-12 net-well no-padding">
            <?php  //var_dump($params);exit; 
            ?>
            <div id="menuCommunity" class="col-md-12 col-sm-12 col-xs-12 numberCommunity">

            </div>

            <div class="animated bounceInRight">
                <?php if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] != $invitedMe["invitorId"]) { ?>
                <?php echo $this->renderPartial(
                        'co2.views.element.menus.answerInvite',
                        array(
                            "invitedMe"      => $invitedMe,
                            "element"   => $element
                        )
                    );
                } else if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] == $invitedMe["invitorId"]) { ?>
                    <div class="containInvitation">
                        <div class="statuInvitation">
                            <?php echo Yii::t("common", "Friend request sent") ?>
                            <?php
                            $inviteCancel = "Cancel";
                            $option = null;
                            $msgRefuse = Yii::t("common", "Are you sure to cancel this invitation");

                            echo
                            '<br>' .
                                '<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . Element::$connectTypes[$element["collection"]] . '\',null,\'' . $option . '\',\'' . $msgRefuse . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", "Not interested by the invitation") . '">' .
                                '<i class="fa fa-remove"></i> ' . Yii::t("common", $inviteCancel) .
                                '</a>';

                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>




            <div class="section-Community">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading no-padding" id="headingOne" role="tab">
                            <h4 class="panel-title">
                                <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Communauté<i class="pull-right fa fa-chevron-down"></i>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">

                            <div class="panel-body">
                                <?php if ($type == Person::COLLECTION) { ?>
                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["follows"])) {
                                                echo count($element["links"]["follows"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Follows") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["friends"])) {
                                                echo count($element["links"]["friends"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Friends") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["memberOf"])) {
                                                echo count($element["links"]["memberOf"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Organizations") ?> </span>
                                    </div>
                                <?php } ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>





        </div>
    </div>


    <div class="pod-info-Description" id="pod-info-Description">
        <?php if (Yii::app()->session["userId"] != null) { ?>
            <span class="text-nv-3">
                <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common", "Description") ?>
            </span>

            <?php
            if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                <button class="btn-update-descriptions btn pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update description") ?>" id="btn-descriptions">
                    <?php if ((!empty($element["shortDescription"])) || (!empty($element["description"]))) { ?>
                        <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                    <?php } else { ?>
                        <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
                    <?php } ?>
                </button>

            <?php } ?>

            <hr class="line-hr">
        <?php } else if ((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null)) { ?>
            <span class="text-nv-3 letter-blue">
                <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common", "Description") ?>
            </span>
            <hr class="line-hr">
        <?php } ?>

        <div id="contenuDesc">
            <?php if (!((empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) { ?>
                <div class="contentInformation margin-10 short-description-pod"  style="font-size: 18px; text-align:justify">
                    <p id="shortDescriptionAbout" name="shortDescriptionAbout" class=""><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; echo (!empty($element["description"])?" ...":""); ?></p>
                    <p id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit" class="hidden"><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></p>
                    <?php if(!empty($element["description"])){ ?>
                        <div><a id="btn-read-more-description" class="" href="javascript:;">Lira la suite</a></div>
                    <?php }?>
                </div>
            <?php }
            if (!((empty($element["description"])) && Yii::app()->session["userId"] == null)) { ?>
                <div class="contentInformation margin-10 full-description-pod"  style="font-size: 18px; text-align:justify; display:none">
                    <p id="descriptionAbout" name="descriptionAbout" class=""><?php echo (@$element["description"]) ? $element["description"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                    <div><a id="btn-read-less-description" class="" href="javascript:;">Lire moins</a></div>
                </div>
            <?php } ?>
        </div>
    </div>


    <div class="section light-bg pod-infoGeneral" id="pod-infoGeneral">
        <div class="row profil-title-informations">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span class="text-nv-3">
                    <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common", "General information") ?>
                </span>
                <?php if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                    <button class="btn-update-info btn btn-update-ig pad-2 pull-right  visible-xs tooltips" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update general information") ?>">
                        <i class="fa fa-edit"></i>
                    </button>
                <?php } ?>
            </div>
            <?php if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                <div class=" col-md-4 col-sm-4 col-xs-12 hidden-xs">
                    <button class="btn-update-info btn btn-update-ig pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update general information") ?>">
                        <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                    </button>
                </div>
            <?php } ?>

        </div>
        <hr class="line-hr">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card features">
                        <div class="card-body">

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-font gradient-fill"></i></span>
                                <div class="media-body">
                                 <h6>Nom et prénom</h6>
                                    <p class="padding-top-10" id="nameAbout"> <?php echo $element["name"]; ?> </p>
                                </div>
                            </div>
                                <!-- <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-user-secret gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10" id="usernameAbout"><?php echo (@$element["username"]) ? $element["username"] : '<i>' . Yii::t("common", "Not specified") . '</i>' ?></p>
                                    </div>
                                </div> -->
                                <?php if (Preference::showPreference($element, $type, "birthDate", Yii::app()->session["userId"])) { ?>
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-birthday-cake gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>Date de naissance</h6>
                                            <p class="padding-top-10" id="birthDateAbout"><?php echo (@$element["birthDate"]) ? $element["birthDate"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                                        </div>
                                    </div>

                                <?php }?>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card features">
                        <div class="card-body">
                            <?php if ($type != Person::COLLECTION /*&& $type != Organization::COLLECTION */) { ?>
                                <div class="media contentInformation" id="divParentAbout">
                                    <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                    <div class="media-body">
                                        <span class="card-title ttr-4"><?php echo Yii::t("common", "Carried by"); ?></span>
                                        <p class="padding-top-10" id="parentAbout">
                                            <?php
                                            if (!empty($element["parent"])) {
                                                $count = count($element["parent"]);
                                                foreach ($element['parent'] as $key => $v) {
                                                    $heightImg = ($count > 1) ? 35 : 25;
                                                    if(is_object($v)){
                                                        $v = get_object_vars($v);
                                                    }
                                                    if (isset($v['id'])) {
                                                        $page = $v['id'];
                                                    } else if(isset($v['type'])){
                                                        $page = "#page.type." . $v['type'] . ".id." . $key; 
                                                    }
                                                    if (isset($v['activitypub'])) {
                                                        $imgPath = @$v['profilThumbImageUrl'] ? $v['profilThumbImageUrl'] : "";

                                                    ?>
                                                        <div style="text-decoration:none; border:0; background-color: white;padding:0;cursor: pointer;color: #2C3E50" onClick= "javascript:redirigerVersPage('<?php echo $page ?>')"  <?php if ($count > 1) echo 'data-toggle="tooltip" data-placement="left" title="' . $v["name"] . '"' ?>>
                                                            <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                            <?php if ($count == 1) echo $v['name']; ?>
                                                        </div>
                                                    <?php

                                                    } else if(isset($v["type"]) && isset($v["name"])){
                                                        $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/' . $v["profilThumbImageUrl"]) : $this->module->assetsUrl . '/images/thumb/default_' . $v["type"]??$v["collection"]??"" . '.png';?>
                                                        <a href="<?php echo $page ?>" class="lbh tooltips" <?php if ($count > 1) echo 'data-toggle="tooltip" data-placement="left" title="' . $v["name"] . '"' ?>>
                                                            <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                            <?php if ($count == 1) echo $v['name']; ?>
                                                        </a>
                                                    <?php
                                                    }
                                                    ?>
                                                    
                                                    

                                            <?php
                                                }
                                            } else
                                                echo '<i>' . Yii::t("common", "Not specified") . '</i>';
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if ($type == Event::COLLECTION) { ?>
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                    <div class="media-body">
                                        <span class="card-title ttr-4"><?php echo Yii::t("common", "Organized by"); ?></span>
                                        <p class="padding-top-10" id="organizerAbout">
                                            <?php
                                            if (!empty($element["organizer"])) {
                                                $count = count($element["organizer"]);
                                                foreach ($element['organizer'] as $key => $v) {
                                                    $heightImg = ($count > 1) ? 35 : 25;
                                                    $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/' . $v["profilThumbImageUrl"]) : $this->module->assetsUrl . '/images/thumb/default_' . $v["type"] . '.png' ?>
                                                    <a href="#page.type.<?php echo $v['type']; ?>.id.<?php echo $key; ?>" class="lbh tooltips" <?php if ($count > 1) echo 'data-toggle="tooltip" data-placement="left" title="' . $v["name"] . '"' ?>>
                                                        <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                        <?php if ($count == 1) echo $v['name']; ?>
                                                    </a>
                                            <?php   }
                                            } else
                                                echo '<i>' . Yii::t("common", "Not specified") . '</i>';
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="card features hidden-xs margin-bottom-20">

                    </div>

                    <?php if (!empty($element["tags"])) { ?>
                        <div class="card features margin-bottom-20">
                            <div class="card-body">

                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-tags gradient-fill"></i></span>
                                    <div class="media-body">
                                        <ul class="tag-list no-padding">
                                            <?php
                                            if (!empty($element["tags"])) {
                                                foreach ($element["tags"]  as $key => $tag) {
                                                    echo '<li class="tag"><a href="#membres?tags=' . $tag . '"> <i class="fa fa-tag icon-before"></i>&nbsp;' . $tag . '</a></li>';
                                                }
                                            } else {
                                                echo '<i>' . Yii::t("common", "Not specified") . '</i>';
                                            } ?>

                                            <!--<li><a href=""><i class="fa fa-tag"></i> tag</a></li>-->
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 margin-bottom-15" style="padding-left:0px">
        <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
        
            <div class="row">
                <div class=" col-md-8 col-sm-8 col-xs-12">    
                    <div class="text-nv-3">
                        <i class="fa fa-file-text-o"></i> Compétences
                    </div>    
                </div>    
                <?php if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                    <div class=" col-md-4 col-sm-4 col-xs-12">
                        <button class="btn-update-competence btn btn-update-ig pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update general information") ?>">
                            
                        <?php if ((!empty($element["skills"]))) { ?>
                                <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                            <?php } else { ?>
                                <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
                            <?php } ?>
                        </button>
                    </div>
                <?php } ?>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20 col-md-12 col-sm-12 col-xs-12">
                    <div class="contentInformation margin-10">                    
                        <?php if(isset($element["skills"])){
                            if(is_array($element["skills"])){
                                foreach ($element["skills"] as $key => $skill) {  ?>
                                    <?php echo $skill?><br>
                                <?php }
                            }else {
                                echo $element["skills"];
                            }?>
                       <?php }?>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
    <?php if ($type != Event::COLLECTION) { ?>
        <?php
        $isEmpty = true;
        $isBadgeShowed = false;
        $isBadgeConfigurable = false;
        if (!empty($element["badges"])) {
            $badgeStr = '';
            $configureStr = '';
            $badges = $element["badges"];
            function sortByOrder($a, $b)
            {
                return (isset($a["order"]) ? $a["order"] : 99999) < (isset($b["order"]) ? $b["order"] : 99999) ? -1 : 1;
            }
            uasort($badges, "sortByOrder");
            $badgeAssigned = null;
            foreach ($badges as $idBadge => $badgeValue) {
                if (!isset($badgeValue["attenteEmetteur"]) && !isset($badgeValue["attenteRecepteur"])) {
                    if ($badgeAssigned == null) {
                        $badgeAssigned = [];
                    }
                    array_push($badgeAssigned, $idBadge);
                }
            }
            foreach ($badges  as $key => $value) {
                if (!isset($value['attenteEmetteur'])) {
                    if (!$isAdmin && isset($value['attenteRecepteur'])) {
                        continue;
                    } else {
                        $progress = '';
                        if (isset($value["isParcours"]) && $value["isParcours"] == "true") {
                            $progress .= " (";
                            $tree = Badge::getBadgeChildrenTree($key, $badgeAssigned);
                            $childrenLocked = $tree["childrenLocked"] ?? 1;
                            $childrenCount = $tree["childrenCount"] ?? 1;
                            $progressValue = (($childrenCount - $childrenLocked) / $childrenCount) * 100;
                            if ($childrenCount == 0) {
                                $progressValue = 0;
                            }
                            $progressValue = round($progressValue, 2);
                            $progress .= $progressValue;
                            $progress .= "%)";
                        }
                        $assertionImage = '';
                        if ((isset($value["byReference"]) && $value["byReference"] == true)) {
                            $assertionImage = PHDB::findOneById(Badge::COLLECTION, $key, ["assertion"])['assertion']["image"] ?? "";
                        } else {
                            if (!isset($value['attenteRecepteur'])) {
                                $assertionImage = '/upload/communecter/badges/' . $key . '/assertions/' . (string) $element["_id"] . '.png';
                            } else {
                                $assertionImage = PHDB::findOneById(Badge::COLLECTION, $key, ["profilMediumImageUrl"]);
                                if (isset($assertionImage['profilMediumImageUrl'])) {
                                    $assertionImage = $assertionImage['profilMediumImageUrl'];
                                }
                            }
                        }
                        if (is_array($assertionImage)) {
                            $assertionImage = '';
                        }
                        if (isset($value["show"]) && $value["show"] == "true") {
                            $isBadgeShowed = true;
                        }
                        $badgeStr .= '<div data-show="' . (isset($value["show"]) && $value["show"] == "true" ? "true" : "false") . '" style="display: ' . (isset($value["show"]) && $value["show"] == "true" ? "block" : "none") . '" class="lbh badge-item ' . (isset($value['attenteRecepteur']) ? 'confirmation ' : ' ') . ((isset($value['revoke']) && $value['revoke'] == "true") ? 'revoked' : '') . ' "  title="' . $value["name"] . '" data-hash="#page.type.badges.id.' . $key . '" data-id="' . $key . '"><img class="img-responsive" src="' . $assertionImage . '"/><h5>' . $value["name"] . $progress . '</h5>' . (isset($value['attenteRecepteur']) ? '<div class="btn-confirmations"><button data-confirm="false" data-id="' . $key . '" class="btn btn-danger btn-confirm" data-toggle="tooltip" data-placement="top" title="' . Yii::t("common", "refuse") . '"><i class="fa fa-times"></i> </button><button data-confirm="true" data-id="' . $key . '" class="btn btn-success btn-confirm" data-toggle="tooltip" data-placement="top" title="' . Yii::t("badge", "Confirm") . '"><i class="fa fa-check"></i> </button></div>' : '') . ((isset($value['revoke']) && $value['revoke'] == "true") ? '' : '<button class="btn btn-success btn-endorse" onclick="openEndorse(event,\'' . $key . '\')"><i class="fa fa-thumbs-o-up"></i> ' . Yii::t("badge", "Endorse") . '</button>') . '</div>';
                        if (($isEmpty && isset($value["show"]) && $value["show"] == "true")) {
                            $isEmpty = false;
                        }
                    }
                } else {
                    $assertionImage = '';
                    $assertionImage = PHDB::findOneById(Badge::COLLECTION, $key, ["profilMediumImageUrl"]);
                    if (isset($assertionImage['profilMediumImageUrl'])) {
                        $assertionImage = $assertionImage['profilMediumImageUrl'];
                    }
                    if (is_array($assertionImage)) {
                        $assertionImage = '';
                    }
                    $badgeStr .= '<div data-show="' . (isset($value["show"]) && $value["show"] == "true" ? "true" : "false") . '" style="display: ' . (isset($value["show"]) && $value["show"] == "true" ? "block" : "none") . '" class="lbh badge-item confirmation"  title="' . $value["name"] . '" data-hash="#page.type.badges.id.' . $key . '" data-id="' . $key . '"><img class="img-responsive" src="' . $assertionImage . '"/><h5>' . $value["name"] . ' <i class="fa fa-clock-o"></i></h5><div class="btn-confirmations text-center">' . Yii::t('badge', "Pending confirmation") . '</div></div>';
                    if (isset($value["show"]) && $value["show"] == "true") {
                        $isBadgeShowed = true;
                    }
                }
                $configureStr .= '<div class="row" data-id="' . $key . '"><div class="col-xs-1 col-md-1"><i style="cursor: move;" class="fa fa-arrows fa-2x"></i></div><div class="col-xs-2 col-md-2"><img class="img-responsive badge-image" src="' . $assertionImage . '"/></div><div class="col-xs-6 col-md-6">' . $value["name"] . '</div><div class="col-xs-3 col-md-3">';
                if (isset($value["show"]) && $value["show"] == "true") {
                    $configureStr .= '<button data-value="true" class="btn apropos-show">A propos</button>';
                } else {
                    $configureStr .= '<button data-value="false" class="btn apropos-show">A propos</button>';
                }
                if (isset($value["showBanner"]) && $value["showBanner"] == "true") {
                    $configureStr .= '<button data-value="true" class="btn baniere-show">Baniere</button>';
                } else {
                    $configureStr .= '<button data-value="false" class="btn baniere-show">Baniere</button>';
                }

                $configureStr .= '</div></div>';
                $isBadgeConfigurable = true;
            }
        } ?>
        <div class="pod-badges" id="pod-badges">
            <span class="text-nv-3">
                <i class="fa fa-bookmark"></i>&nbsp;<?php echo Yii::t("common", "Badges") ?>
            </span>
            <?php if ($isAdmin && $isBadgeConfigurable) { ?>
                <button class="btn btn-configure-badge pad-2 pull-right  visible-xs tooltips" data-toggle="tooltip" data-placement="top" title="" alt="" onclick="modeSettings()">
                    <i class="fa fa-cog"></i>
                </button>
                <button class="btn btn-configure-badge pad-2 pull-right tooltips hidden-xs" data-toggle="tooltip" data-placement="top" title="" alt="" onclick="modeSettings()">
                    <i class="fa fa-cog"></i>&nbsp;<?php echo Yii::t("common", "Configure") ?>
                </button>
            <?php } ?>
            <hr class="line-hr">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card features hidden-xs margin-bottom-20">
                </div>
                <div id="badge-card" class="card features margin-bottom-20">
                    <div class="card-body">

                        <div class="media contentInformation">
                            <div class="media-body">
                                <div class="badge-list no-padding">
                                    <?php
                                    $badgeMessage = Yii::t("badge", "This element doesn't have any badge");
                                    if ($element["collection"] == Person::COLLECTION && ((string) $element['_id']) == Yii::app()->session["userId"]) {
                                        $badgeMessage = Yii::t("badge", "You don't have any badge");
                                    }
                                    if ($element["_id"])
                                        echo (!$isBadgeShowed ? $badgeMessage : $badgeStr);
                                    ?>
                                </div>
                                <?php if ($isAdmin) { ?>
                                    <div style="display: none;" class="badge-configure-list">
                                        <div data-id="header" class="row badge-configure-list-header">
                                            <div class="col-xs-1 col-md-1"></div>
                                            <div class="col-xs-2 col-md-2">Image</div>
                                            <div class="col-xs-6 col-md-6">Nom</div>
                                            <div class="col-xs-3 col-md-3">Afficher dans</div>
                                        </div>
                                        <div class="row badge-configure-list-body">
                                            <?= empty($configureStr) ? "" : $configureStr ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                        </div>
                        <div id="mode-normal">
                            <button class="btn btn-success" onclick="openBadgeFinder(event)"><?php echo Yii::t("badge", "Assign badge to me") ?></button>
                        </div>
                        <?php if ($isAdmin) { ?>
                            <div id="mode-settings" style="display: none;">
                                <button class="btn btn-success" onclick="saveSettings()"><?php echo Yii::t("badge", "Save") ?></button>
                                <button class="btn btn-danger" onclick="quitModeSettings()"><?php echo Yii::t("badge", "Cancel") ?></button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="portfolio-modal modal fade in" id="modal-finder" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;">
                    <div class="modal-content padding-top-15">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>


                        <div class="container">
                            <div id="badge-finder-content" style="height: 100%; width: 100%">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="portfolio-modal modal fade in" id="modal-endosser" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; display: none;">
                    <div class="modal-content padding-top-15">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        <div id="endosser-content" style="height: 100%; width: 100%"></div>
                    </div>
                </div>

                <script>
                    function openEndorse(event, badgeId) {
                        event.stopPropagation();
                        event.preventDefault();
                        $("#modal-endosser").data("badge", badgeId);
                        $("#modal-endosser").data("elementId", contextId);
                        $("#modal-endosser").data("elementType", contextData.collection);
                        getAjax("#endosser-content", baseUrl + '/co2/badges/endorsement-modal/badge/' + badgeId + '/id/' + contextId + '/type/' + contextData.collection, null, 'html');
                        $("#modal-endosser").modal("show");
                    }

                    function stopPropagation(event) {
                        event.stopPropagation();
                    }

                    function openBadgeFinder(event) {
                        coInterface.showLoader("#badge-finder-content");
                        <?php $withEmail = PHDB::findOneById($element['collection'], $element['_id']); ?>
                        getAjax("#badge-finder-content", baseUrl + '/co2/badges/finder/email/<?= isset($withEmail["email"]) ? 'true' : 'false' ?>', null, 'html');
                        $("#modal-finder").modal("show")
                    }
                    $('#modal-finder').on('hide.bs.modal', () => {
                        if (assign) {
                            assign = false;
                        }
                    })
                    $("#badge-card .apropos-show, #badge-card .baniere-show").off().on('click', (event) => {
                        const value = $(event.currentTarget).attr("data-value") != "true"
                        $(event.currentTarget).attr("data-value", value);
                    })
                    var sortableBadge = null;
                    var initialSort = null;

                    function modeSettings() {
                        //Change widget show / hide
                        $('#badge-card .badge-list').hide();
                        $("#badge-card #mode-normal").css("display", "none");
                        $("#badge-card #mode-settings").css("display", "block");
                        $("#badge-card #mode-settings-btn").css("display", "none");
                        $('#badge-card .badge-configure-list').show()
                        //Activate sortable
                        sortableBadge = $("#badge-card .badge-configure-list-body").sortable({
                            animation: 150,
                            draggable: "div.row"
                        });
                        $("#badge-card .badge-configure-list-body").disableSelection();
                        initialSort = sortableBadge.sortable("toArray");
                    }

                    function saveSettings(params) {
                        $('#badge-card .badge-list').show();
                        $('#badge-card .badge-configure-list').hide();
                        $("#badge-card .show-badge-checkbox").css("display", "none");
                        $("#badge-card #mode-normal").css("display", "block");
                        $("#badge-card #mode-settings").css("display", "none");
                        $("#badge-card #mode-settings-btn").css("display", "inline");
                        if (sortableBadge) {
                            var order = sortableBadge.sortable("toArray");
                            const dataToSend = {};
                            for (let i = 0; i < order.length; i++) {
                                console.log(order[i]);
                                dataToSend[order[i]] = {
                                    order: i,
                                    show: $(`#badge-card .badge-configure-list div[data-id='${order[i]}'] button.apropos-show`).attr("data-value"),
                                    showBanner: $(`#badge-card .badge-configure-list div[data-id='${order[i]}'] button.baniere-show`).attr("data-value")
                                }
                            }
                            if (Object.keys(dataToSend).length > 0) {
                                mylog.log("SETTINGS", dataToSend);
                                $.ajax({
                                    url: baseUrl + '/' + moduleId + '/badges/settings/element/' + contextId + '/type/' + contextData["collection"],
                                    method: 'POST',
                                    data: {
                                        badges: dataToSend
                                    }
                                }).done(function(result) {
                                    if (result.result) {
                                        sortableBadge.sortable('destroy');
                                        toastr.success("Changes saved");
                                        urlCtrl.loadByHash(location.hash);
                                    } else {
                                        toastr.error("unknow error");
                                    }
                                })
                            }
                        }
                    }

                    function quitModeSettings(params) {
                        $('#badge-card .badge-list').show();
                        $('#badge-card .badge-configure-list').hide();
                        $("#badge-card .show-badge-checkbox").css("display", "none");
                        $("#badge-card #mode-normal").css("display", "block");
                        $("#badge-card #mode-settings").css("display", "none");
                        $("#badge-card #mode-settings-btn").css("display", "inline");
                        if (sortableBadge) {
                            sortableBadge.sortable('sort', initialSort)
                            sortableBadge.sortable('destroy')
                        }
                    }
                </script>
            </div>
        </div>
    <?php } ?>
    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit, "openEdition" => $openEdition)); ?>

</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2">
    <div id="pod-accessiblity">
        <span class="text-nv-3">
            <i class="fa fa-file-text-o"></i>&nbsp;Accessibilité
        </span>
        <hr class="line-hr">                
        <div class="card features">
            <div class="card-body">
                <div class="media contentInformation">                            
                    <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                    <div class="media-body">
                        <h6>Site internet</h6>
                        <p class="padding-top-10" id="webAbout">
                            <?php
                            if (@$element["url"]) {
                                //If there is no http:// in the url
                                $scheme = ((!preg_match("~^(?:f|ht)tps?://~i", $element["url"])) ? 'http://' : "");
                                echo '<a href="' . $scheme . $element['url'] . '" target="_blank" id="urlWebAbout" style="cursor:pointer;">' . $element["url"] . '</a>';
                            } else
                                echo '<i>' . Yii::t("common", "Not specified") . '</i>'; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php if (Preference::showPreference($element, $type, "email", Yii::app()->session["userId"])) { ?>
     
            <div class="card features">
                <div class="card-body">
                    <div class="media contentInformation">
                        <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                        <div class="media-body">
                        <h6>Adresse mail</h6>
                            <p class="padding-top-10" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div id="pod-contact">
        <span class="text-nv-3">
            <i class="fa fa-file-text-o"></i>&nbsp;Contact
        </span>
        <hr class="line-hr">
        <div class="card features">
            <div class="card-body">
                <div class="media contentInformation">
                    <span class="ti-2x mr-3"><i class="fa fa-phone gradient-fill"></i></span>
                    <div class="media-body">
                        <h6>NUMÉRO DE TELEPHONE FIXE</h6>
                        <p class="padding-top-10" id="fixeAbout">
                            <?php
                            $fixe = '<i>' . Yii::t("common", "Not specified") . '</i>';
                            if (!empty($element["telephone"]["fixe"]))
                                $fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);

                            echo $fixe;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card features">
            <div class="card-body">
                <div class="media contentInformation">
                    <span class="ti-2x mr-3"><i class="fa fa-mobile gradient-fill"></i></span>
                    <div class="media-body">
                    <h6>NUMÉRO DE TELEPHONE MOBILE</h6>
                        <p class="padding-top-10" id="mobileAbout">
                            <?php
                            $mobile = '<i>' . Yii::t("common", "Not specified") . '</i>';
                            if (!empty($element["telephone"]["mobile"]))
                                $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                            echo $mobile;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
    <div id="newSocialNetwork">
        <?php echo $this->renderPartial('co2.views.pod.newSocialNetwork', array("element" => $element, "type" => $type, "edit" => $edit, "openEdition" => $openEdition)); ?>
    </div>
</div>
<!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 about-section2" id="pod-address">
    </div>
   
<?php echo $this->renderPartial('../pod/whycommunexion', array()); ?>

<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function() {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function() {
        controleAffiche();
    });

    function controleAffiche() {
        mylog.log(contextData.shortDescription, contextData.description);
        if ((typeof contextData.shortDescription == "undefined") && (typeof contextData.description == "undefined")) {
            $("#contenuDesc").addClass("hidden");
            //$("#btn-descriptions").html('<i class="fa fa-plus"> Ajouter');

        };
    };


    function afficheMap() {
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if (typeof contextData.geo == "undefined") {
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container: "divMapContent",
            latLon: contextData.geo,
            activeCluster: false,
            zoom: 16,
            activePopUp: false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt: {
                id: contextData.id,
                type: contextData.type,
                geo: contextData.geo
            },
            center: true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };


    //var paramsPointeur[contextId] = contextData;
    var formatDateView = "DD MMMM YYYY à HH:mm";
    var formatDatedynForm = "DD/MM/YYYY HH:mm";

    jQuery(document).ready(function() {
        bindDynFormEditable();
        initDate();
        inintDescs();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function() {
            communecterUser();
        });

        $(".btn-update-geopos").click(function() {
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function() {
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function() {
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function() {
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function() {
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function() {
            findGeoPosByAddress();
        });

        $("#btn-read-more-description").on("click", function(){
            $(".short-description-pod").hide();
            $(".full-description-pod").show();
        })

        $("#btn-read-less-description").on("click", function(){
            $(".full-description-pod").hide()
            $(".short-description-pod").show();
        })

        coInterface.bindLBHLinks();

    });

    function inintDescs() {
        mylog.log("inintDescs");
        if ($("#descriptionAbout").length > 0) {
            if (canEdit == true || (typeof openEdition != "undefined" && openEdition == true))
                descHtmlToMarkdown();
            mylog.log("after");
            mylog.log("inintDescs", $("#descriptionAbout").html());
            var descHtml = "<i>" + trad.notSpecified + "</i>";
            if ($("#descriptionAbout").html().length > 0) {
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html());
            }
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
        }
    }

    function initDate() { //DD/mm/YYYY hh:mm

        formatDateView = "DD MMMM YYYY à HH:mm";
        formatDatedynForm = "DD/MM/YYYY HH:mm";

        mylog.log("formatDateView", formatDateView);
        //if($("#startDateAbout").html() != "")

        $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));

        $("#endDateAbout").html(moment(contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));
        
        if(notEmpty(contextData.birthDate))
            $("#birthDateAbout").html(moment(contextData.birthDate.sec * 1000).local().locale(mainLanguage).format("DD/MM/YYYY"));
        
        if ($("#birthDate").html() != "")
            $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
        $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
    }

    function AddReadMore() {
        var showChar = 300;
        var ellipsestext = "...";
        var moretext = "Lire la suite";
        var lesstext = "Lire moins";
        $('.more').each(function() {
            var content = $(this).html();
            var textcontent = $(this).text();

            if (textcontent.length > showChar) {

                var c = textcontent.substr(0, showChar);
                //var h = content.substr(showChar-1, content.length - showChar);

                var html = '<span class="container "><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

                $(this).html(html);
                $(this).after('<a href="" class="morelink">' + lesstext + '</a>');
            }

        });

        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(lesstext);
                $(this).prev().children('.morecontent').fadeToggle(100, function() {
                    $(this).prev().fadeToggle(100);
                });

            } else {
                $(this).addClass("less");
                $(this).html(moretext);
                $(this).prev().children('.container').fadeToggle(100, function() {
                    $(this).next().fadeToggle(100);
                });
            }
            //$(this).prev().children().fadeToggle();
            //$(this).parent().prev().prev().fadeToggle(500);
            //$(this).parent().prev().delay(600).fadeToggle(500);

            return false;
        });
    }
    $(function() {
        //Calling function after Page Load
        AddReadMore();
        var element = {
            init: function() {
                this.events.init();
            },
            events: {
                init: function() {
                    element.events.updateComplementDetail();
                },
                updateComplementDetail: function() {
                    element.events.updateModal(".btn-update-competence", {
                        skills: {
                            "label": "Compétence",
                            "inputType": "selectMultiple",
                            "isSelect2": true,
                            "list": "skills",
                            "placeholder": "Compétence",
                            "groupOptions" : true,
                            "groupSelected" : false,
                            "optionsValueAsKey" : false,
                            "value" : contextData.skills,
                            "select2" : {
                                "multiple" : true
                            }
                        }
                    });
                },
                updateModal: function(dom, propObj = {}, callBack = () => {}) {
                    var keyClassOrId = dom.replace(".btn-update-", "").replace(".btn-edit-", "");
                    $(dom).off().on("click", function() {
                        var form = {
                            saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
                            dynForm: {
                                jsonSchema: {
                                    title: trad["update"] + " / " + trad["Add"],
                                    icon: "fa-key",
                                    onLoads: {

                                        markdown: function() {
                                            dataHelper.activateMarkdown("#ajaxFormModal #description");
                                            $("#ajax-modal .modal-header").attr("style", "background: #BA1C24 !important");
                                            // form.dynForm.jsonSchema.onLoads.loadIfMultiSelect();
                                        }
                                    },
                                    afterSave: function(data) {
                                        mylog.dir(data);
                                        if (jsonHelper.getValueByPath(data, "resultGoods.values") != undefined) {
                                            if ($(".contentInformation #" + keyClassOrId).length > 0) {
                                                if (data.resultGoods.values[keyClassOrId] == "")
                                                    $(".contentInformation #" + keyClassOrId).html('<i>' + trad["notSpecified"] + '</i>');
                                                else
                                                    $(".contentInformation #" + keyClassOrId).html(data.resultGoods.values[keyClassOrId]);
                                            }
                                            dyFObj.closeForm();
                                            changeHiddenFields();
                                            pageProfil.views.detail();
                                            if (keyClassOrId == "tags")
                                                $("#btn-start-detail").click();
                                        } else {
                                            toastr.error('Erreur d\'enregistrement');
                                        }
                                    },
                                    properties: {
                                        block: dyFInputs.inputHidden(),
                                        typeElement: dyFInputs.inputHidden(),
                                        isUpdate: dyFInputs.inputHidden(true),
                                        ...propObj
                                    }
                                }
                            }
                        };
                        var dataUpdate = {
                            block: "companyProfile",
                            id: contextData.id,
                            name: contextData.name,
                            typeElement: contextData.type,
                        };

                        if (Object.keys(propObj).length == 1) {
                            if (keyClassOrId != "tags")
                                dataUpdate[keyClassOrId] = $(".contentInformation #" + keyClassOrId + "Edit").html();
                            else
                                dataUpdate[keyClassOrId] = <?= json_encode(@$element["tags"]) ?>;
                            dyFObj.openForm(form, "markdown", dataUpdate);
                        } else if (Object.keys(propObj).length > 1) {
                            callBack(form, "markdown", dataUpdate, propObj);
                        }
                    });
                },
            }
        }
        element.init();
    });


    <?php if ($isAdmin) { ?>

        function confirmBadge(event) {
            event.preventDefault();
            event.stopPropagation();
            var target = event.currentTarget;
            var value = $(target).data('confirm');
            var badgeId = $(target).data('id');
            var url = baseUrl + '/co2/badges/confirm/badge/' + badgeId + '/award/' + contextData._id["$id"] + '/type/' + contextData.type + '/confirm/' + value;
            $.ajax({
                type: "GET",
                url: url,
                success: function(data) {
                    var badgeHtmlElement = $(target).closest('.badge-item.lbh.confirmation');
                    if (value) {
                        badgeHtmlElement.removeClass("confirmation");
                        badgeHtmlElement.find(".btn-confirmations").remove();
                        toastr.success("Badges assigné avec succes");
                    } else {
                        badgeHtmlElement.remove();
                        toastr.success("Badges refusé avec succes");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    toastr.error("Erreur lors de l'assignation des badges");
                }
            });
        }
        $('.btn-confirmations button.btn-confirm').off().on('click', confirmBadge);
    <?php } ?>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>