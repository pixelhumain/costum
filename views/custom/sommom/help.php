
<style type="text/css">


.content {
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  max-width: 500px;
  /*padding: 35vh 20px 20px 20px*/
}

.logo {
  max-width: 300px;
}

p {
  /*font-family: sans-serif;*/
  text-align: center;
}

.message {
  margin-top: 40px;
  font-size: 30px;
  position: relative;
}

.message:after {
        content: '';
        position: absolute;
        margin: auto;
        right: 0;
        bottom: 0;
        left: 0;
        width: 50%;
        height: 2px;
        background-color: rgb(142, 191, 39);
      }

.message-text {
    margin: 10px;
    padding: 10px;
}

.message-text .text {
    font-weight: bold;
    text-align: center;
    font-size: 20px;
}

.btn-start {
    background-color: transparent;
    border: 2px solid #7d98c7;
    color: #1e56a2;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.btn-start:hover {
    background-color: #7d98c7;
    border: 2px solid #7d98c7;
    color: #fff;
    border-bottom-left-radius: 10px;
    font-weight: bold;
}

.s-center {
    margin: 0px;
    text-align: center;
    width: 100%;
    font-size: 40px;
    margin-bottom: 10px;
}


@import url('https://fonts.googleapis.com/css?family=Oswald|Roboto:400,700');

 p {
	 font-family: 'Roboto', sans-serif;
	 font-size: 0.8rem;
}
 .container {
	 max-width: 1024px;
	 width: 90%;
	 margin: 0 auto;
}
 .timeline-item {
	 padding: 0.1em 2em 1em;
	 position: relative;
	 color: rgba(0, 0, 0, .7);
	 border-left: 2px solid rgba(0, 0, 0, .3);
}
 .timeline-item p {
	 font-size: 2rem;
}
 .timeline-item::before {
	 content: attr(date-is);
	 position: absolute;
	 left: 2em;
	 font-weight: bold;
	 top: 1em;
	 display: block;
	 font-family: 'Roboto', sans-serif;
	 font-weight: 700;
	 font-size: 0.785rem;
}
 .timeline-item::after {
	 width: 15px;
	 height: 10px;
	 display: block;
	 top: 1em;
	 position: absolute;
	 left: -7px;
	 border-radius: 10px;
	 content: '';
	 border: 2px solid rgba(0, 0, 0, .3);
	 background: #8ebf27;
}
 .timeline-item:last-child {
	 border-image: linear-gradient(to bottom, rgba(0, 0, 0, .3) 60%, rgba(0, 0, 0, 0)) 1 100%;
}



</style>

<?php
  
  $adminStatus = false;


  if (isset($this->costum["admins"])) {
	if(is_array($this->costum["admins"])){
		foreach ($this->costum["admins"] as $key => $value) {
			if($key == Yii::app()->session["userId"]){
				$adminStatus = true;
			}
  
		}
	}
  }

?>

<div class="content">

  <p class="message">QUELS SONT LES OBJECTIFS DE LA PLATEFORME ?</p>

</div>

<div class="col-md-12">
    <div class="message-text">
      	<div class="container">
	
			<div class="timeline-item" >
				<h4> Réaliser un état des lieux de l’activité de whale watching en France dans ses territoires d'outre-mer </h4>
				<p>
					
				</p>
			</div>

			<div class="timeline-item" >
				<h4> Avoir une vision globale : </h4>
				<p>
					<ul>
                        <li>
                            des outils existants (codes de conduite, réglementation, zones protégées...)
                        </li>
                        <li>
                          des acteurs impliqués
                        </li>
                        <li>
                            de l'importance de l’activité (espace géographique, nombre d'opérateurs, de bateaux, de ports de départ...)
                        </li>
                        <li>
                            des espèces de mammifères marins ciblées par l’activité et leur vulnérabilité
                        </li>
                        <li>
                            des différents types de pratiques
                        </li>
                    </ul>
				</p>
			</div>

			<div class="timeline-item" >
				<h4> Mettre en évidence les particularités de chaque territoire </h4>
				<p>
					
				</p>
			</div>

			<div class="timeline-item" >
				<h4> Apporter des éléments de réflexion pour une meilleure gestion de l’activité aux territoires, et à l'échelle nationale </h4>
				<p>
					
				</p>
			</div>

			<div class="timeline-item" >
				<h4> Avoir un outil disponible et adaptable, qui permet d’être régulièrement et facilement actualisé et de suivre l’évolution de l’activité dans le temps </h4>
				<p>
					
				</p>
			</div>

		</div>
        
    </div>
</div>

<div class="col-md-12">
  <!-- <img class="logo" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/cdv-cedtm-blanc.png"/> -->
  
  <p class="message">COMMENT A ETE CONCUE LA PLATEFORME ?</p>


</div>

<div class="col-md-12">
    <div class="message-text">
    	<div class="container">
	
			<div class="timeline-item" >
				<p> La plateforme a été réalisée grâce à l'outil « COmmunecter ». </p>
				
			</div>

			<div class="timeline-item" >
				<p>
					COmmunecter se connecter à sa commune » (www.communecter.org), est une boîte à outil citoyenne collaborative, ouverte à tous, un réseau sociétal innovant, open source et gratuit, de développement de territoire avec une approche locale (quartier, arrondissement, commune, ville..). Un bien COmmun disponible sur des applications mobiles et internet permettant de participer à la vie de son territoire avec pour objectif de CO-construire la ville de demain : plus intelligente et interconnectée ! 
				</p>
			</div>

			<div class="timeline-item" >
				<p>
					 Les avantages de cet outil sont de permettre à chacun de référencer directement l'information qui concerne son propre territoire. Les territoires sont ensuite référencés et connectés afin
                          d’avoir une vision globale de filière, visualisable en un clin d'œil grâce à des modules
                          cartographiques et des indicateurs statistiques.
				</p>
			</div>

			<div class="timeline-item" >
				<p>
					 Cet outil « COmmunecter » dispose de nombreux autres outils potentiellement adaptables à
                          notre plateforme, qui pourront être utilisés à l’avenir selon l’évolution des besoins (exemple :
                          indicateurs socio-économiques, sciences participatives, événements, partage d’information...).
				</p>
			</div>
        </div>
    </div>
</div>

<script type="text/javascript">
	setTitle("SOMMOM: Documentations");
	
<?php 
  if(isset($_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]) || $_SESSION["costum"][$this->costum["contextSlug"]]["isAdmin"]){
 ?>
    if($("#btn-nextcloud").length == 0) {
      $("#menuLeft a:eq(3)").after(`
          <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank"> 
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
              <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
          </a>
      `);
    }
<?php } ?>
</script>
