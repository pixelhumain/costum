<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.css">

<?php
    $cssAnsScriptFilesTheme = array(
        "/plugins/jquery-counterUp/waypoints.min.js",
        "/plugins/jquery-counterUp/jquery.counterup.min.js",
        "/plugins/owl-carousel/owl-carousel2/owl.carousel.css",
        "/plugins/owl-carousel/owl-carousel2/owl.carousel.js"
    );
$cssJsCostum = array(
        "/css/sommom/sommom.css",
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>

<style type="text/css">
    <?php if( !isset($css["border"]) || $css["border"]==true ){ ?>
    .counterBlock{min-height:340px; display : flex;}
    .wborder{border : 2px solid #002861; border-radius : 20px;}
    .title2{font-size : 24px; font-weight : bold;}
    .equalHeight{display : flex; flex-wrap : wrap;}
    <?php }?>

    span.cardlist{
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        line-clamp: 1;
        -webkit-box-orient: vertical;
    }

    span.cardlist > a.toggle {
        position: absolute;
        top: 0;
        right: 0;
        line-height: inherit;
        padding: 0 10px;
        cursor: pointer;
        white-space: nowrap;
    }

    span.cardlist > a.toggle b {
        color: #0a6ebd;
    }

    span.cardlist.max-lines-visible {
        display: initial;
        overflow: initial;
    }
    span.cardlist.max-lines-visible > a.toggle {
        position: relative;
    }


</style>

<?php
   $paramsData = [
      "role" => [
          "Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation",
          "Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien",
          "Organisme impliqué dans la science et la recherche sur l’activité d’observation",
          "Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation"
      ],

      "statut" => [
          "Service représentant de l’Etat",
          "Collectivité",
          "Syndicat, fédération, représentant d’acteurs de la mer",
          "Structure privée commerciale",
          "Association de sport et loisir",
          "Association environnementale",
          "Institut de recherche",
          "Autre"
      ]
  ];

  $activegraph = null;
  $adminStatus = false;
  $membre = false;
  $contributeur = false;


  $cetacelist = PHDB::findOne(Lists::COLLECTION, array('name' => 'cetaces'));


  if (isset($this->costum["admins"])) {
      if(is_array($this->costum["admins"])){
          foreach ($this->costum["admins"] as $key => $value) {
              if($key == Yii::app()->session["userId"]){
                  $adminStatus = true;
              }

          }
      }
  }

  if(isset(Yii::app()->session["userId"]) and !$adminStatus and !$partenaire){
      $membre = true;
  }

if($activegraph == null){
?>

<!-- baner -->
<div
    class="col-md-12 col-sm-12 col-xs-12 sommom-baner"
    style="background: url('<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/bandeau1.jpg'); 0" >
    <div class="col-xs-12  ourvalues" style="text-align:center;">
        <h2 class="mst col-xs-12 text-center" style="padding-bottom: 40px; ">
            <span style="color: white;">
              L’observation des cétacés en France et dans les territoires français d’outre-mer
            </span>
        </h2>

        <p class="mst" style="color:#00FFFF; font-size: 18px; padding: 40px !important; font-family: 'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
            Cette plateforme a vocation d'élaborer et de tenir à jour un résumé de l’activité d’observation des cétacés pratiquée dans les territoires français.<br/>

            <?php
                if($adminStatus)
                {
            ?>
                    <div class="col-md-12 col-sm-12 col-xs-12" >
                      <a target="_blank" href="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/sommom#answer.index.id.new.form.<?php echo $formId; ?>" class='btn btn-primary pull-right btn-add-terr' id="showAnswerBtn"><i class="fa fa-plus"></i>  Ajouter un territoire </a>
                    </div>
            <?php
                }


            ?>

        </p>
    </div>
</div>

<!-- territoire -->
<div class="col-xs-12 col-sm-12 padding-20 terr-title" >

    <div class="col-md-12 col-sm-12 col-xs-12" >
    <a class="lbh-sommom-graph" href="#!graph" style="text-decoration: none;">
      <h1 class="sommom-center ">
        Les TERRITOIRES
      </h1>
    </a>

    </div>
</div>

<div class="col-xs-12 col-md-12 d-flex justify-content-around">
    <div
      class="MultiCarousel"
      data-items="1,2,2,3"
      data-slide="1"
      id="MultiCarousel"
      data-interval="1000">

        <div class="MultiCarousel-inner owl-carousel">

        <?php

          $ters = "";

            for($i = 0; $i < count($territoire[0]); $i++)
            {


        ?>

                <div class="item" id ="terrid<?php echo $territoire[8][$i];?>">
                    <div class="terr-card" >

                        <div
                          class="thumb"
                          style="
                              <?php
                                if($territoire[9][$i] != "")
                                {
                              ?>
                                    background: url('<?php echo Yii::app()->request->baseUrl ?>/upload/communecter/answers/<?php echo $territoire[8][$i]; ?>/<?php echo $territoire[9][$i]; ?>') !important;
                              <?php
                                }
                              ?>
                              background-size: auto 260px !important;">

                        </div>

                        <div class="infos">

                            <h3 class="title" style="color:#1B4F95" >
                              <?php
                              if($territoire[1][$i] == ""){
                                echo "non défini";
                              }else{
                                echo $territoire[1][$i];
                                $ters = $ters."".$territoire[1][$i].", ";
                              }?>
                            </h3>

                            <h3 class="date"> <span style="font-size: 16px;"><?php echo count($territoire[3][$i]);  ?></span> organisme(s) <?php if($territoire[2][$i] != ""){ ?>sur <span style="font-size: 16px;"> <?php echo $territoire[2][$i]; ?> </span>  entité(s) terrestre(s) <?php } ?>
                            </h3>

                            <?php
                                if(isset($territoire[5][$i]) and count($territoire[5][$i]) != 0)
                                {
                            ?>
                                    <span class="spanlistcont">
                                        <h5 style="margin-bottom: 1px;">
                                            zones d'observation :
                                        </h5>
                                        <span class="cardlist">
                                        <?php
                                          for ($j=0; $j < count($territoire[5][$i]); $j++)
                                          {
                                              if (isset($territoire[5][$i][$j]["localisation"]))
                                              {
                                                echo $territoire[5][$i][$j]["localisation"];

                                                if($i != count($territoire[5][$i]) - 1)
                                                {
                                                    echo ", ";
                                                }
                                              }; ?>

                                        <?php
                                          }
                                        ?>
                                        </span>
                                    </span>
                            <?php
                                }
                                if(isset($territoire[6][$i]) and count($territoire[6][$i]) != 0)
                                {
                            ?>
                                    <span class="spanlistcont">
                                        <h5 style="margin-bottom: 1px;">
                                            Espèces Observées :
                                            <?php
                                                $hph = 0;
                                            ?>

                                        </h5>

                                        <span class="cardlist">
                                        <?php
                                            foreach ($territoire[6][$i] as $espob)
                                            {

                                                if (ctype_digit($espob["especeRec"]))
                                                {
                                                    foreach ($cetacelist['list'] as $il => $inp)
                                                    {
                                                        if ($il == $espob["especeRec"])
                                                        {
                                                            if(isset($inp["nomVernaculaire"]))
                                                            {
                                                              echo $inp["nomVernaculaire"];
                                                            };
                                                        }
                                                    }
                                                } else {
                                                    echo $espob["especeRec"];
                                                }

                                                $hph++;

                                                if( $hph < count(array_keys($territoire[6][$i])))
                                                {
                                                    echo ", ";
                                                }

                                            }

                                            ?>
                                        </span>
                                </span>
                                <?php
                                }

                                if( $adminStatus or $partenaire )
                                {
                                  if ($adminStatus or isset($roleList[$territoire[1][$i]])) {
                            ?>


                                    <span style=" word-wrap: break-word; ">
                                        <h5 style="margin-bottom: 1px;">
                                            <a
                                                href="javascript:;"
                                                data-href="<?php echo Yii::app()->baseUrl?>/costum/co/index/slug/sommom#answer.index.id.<?php echo $territoire[8][$i];?>.mode.w"
                                                style="color: #4e958b; font-family: 'Grotesque Black', sans-serif;"
                                                class="editterritoire">
                                                  Acceder au formulaire
                                            </a>
                                    </span>

                            <?php } ?>

                                    <span style=" word-wrap: break-word; "> <h5 style="margin-bottom: 1px;">

                                        <a href="javascript:;" class="downloadpdf" data-href="<?php echo Yii::app()->baseUrl ?>/co2/export/pdfelement/id/<?php echo $territoire[8][$i];?>/type/answers/slug/sommom/" style="color: rgba(21, 37, 54, 0.7); font-family: 'Grotesque Black', sans-serif; text-transform: none">
                                            Consulter l’ensemble des informations
                            <?php
                                          if ($adminStatus or isset($roleList[$territoire[1][$i]])) {
                            ?>

                                            avec les coordonées des organismes

                            <?php } ?>
                                        </a>
                                    </span>
                                    <span style=" word-wrap: break-word; ">
                                      <h5 style="margin-bottom: 1px;">
                                        <a class="lbh-sommom-graph" href="#!graph.ter.<?php echo $i ?>" style="color: #4e958b; font-family: 'Grotesque Black', sans-serif;"> <i> Observatoire du territoire </i> </a>
                                      </h5>
                                    </span>

                            <?php
                                } elseif($membre)
                                {
                            ?>

                                    <span style=" word-wrap: break-word; ">

                                      <h5 style="margin-bottom: 1px;">


                                       <a href="javascript:;" class="downloadpdf" data-href="<?php echo Yii::app()->baseUrl ?>/co2/export/pdfelement/id/<?php echo $territoire[8][$i];?>/type/answers/slug/sommom/" style="color: rgba(21, 37, 54, 0.7); font-family: 'Grotesque Black', sans-serif; text-transform: none">
                                             Consulter l’ensemble des informations
                                        </a>

                                      </h5>

                                    </span>

                                    <span style=" word-wrap: break-word; ">

                                      <h5 style="margin-bottom: 1px;">

                                        <a class="lbh" href="#graph.ter.<?php echo $i ?>" style="color: #4e958b; font-family: 'Grotesque Black', sans-serif;"> <i> Observatoire du territoire </i> </a>

                                          </h5>


                                    </span>


                            <?php
                                } elseif(!$membre)
                                {
                            ?>

                                    <span style=" word-wrap: break-word; ">

                                      <h5 style="margin-bottom: 1px;">

                                        <a class="lbh" href="#graph.ter.<?php echo $i ?>" style="color: #4e958b; font-family: 'Grotesque Black', sans-serif;"> <i> Observatoire du territoire </i> </a>

                                          </h5>


                                    </span>



                                        <span style=" word-wrap: break-word; ">

                                      <h5 style="margin-bottom: 1px;">

                                        <a href="#membre" style="color: rgba(21, 37, 54, 0.7); font-family: 'Grotesque Black', sans-serif; text-transform: none" class="lbh"> <i> Devenir membre </i> </a>
                                      </h5>


                                    </span>

                            <?php
                                }
                                if($adminStatus)
                                {
                            ?>
                                    <a class="details deleteAnswer" href="javascript:;" data-id ="<?php echo $territoire[8][$i];?>" style="font-size: 12px; color: red !important;" >Supprimer ce territoire </a>

                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
        <?php
            }
        ?>

        </div>
    </div>
</div>

<?php if($activegraph == null){ ?>

<div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20" style="">
    <h1 style="margin:0px; margin-top: 50px" class="sommom-center"> Les Organismes </h1>
</div>

<div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20" style="" id="dashf">
    <div class="form-inline" id ="azerty">


            <?php
                $affs2 = false;
                for($i = 0; $i < count($territoire[0]); $i++)
                {
                    if($territoire[0][$i] != "")
                    {
                        if(Yii::app()->session["userId"] != "")
                        {
                            foreach ($territoire[10][$i] as $value)
                            {
                                if(Yii::app()->session["userId"] != "")
                                {
                                    if($value != "")
                                    {
                                        if($value == Yii::app()->session["userId"])
                                        {
                                            $affs2 = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($affs2 or $adminStatus)
                {
            ?>

                <div class="col-md-12 sommom-center" style="margin-bottom: 20px">
                    <div class="form-group">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Ajouter Organisme
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php
                                    for($i = 0; $i < count($territoire[0]); $i++){

                                      $affs = false;
                                      foreach ($territoire[10][$i] as $value2) {
                                        if(isset(Yii::app()->session["userId"])){
                                        if($value2 != ""){
                                        if($value2 == Yii::app()->session["userId"]){
                                          $affs = true;
                                        }
                                      }
                                    }
                                      }
                                      if($affs or $adminStatus){


                                ?>

                                <li class="button-checkbox btn-add-org addorganisme" data-color="default" data-id="<?php echo $territoire[8][$i];?>" data-collection='answers' data-path='answers.sommomForm1.sommomForm122.<?php echo count($territoire[3][$i])?>' data-len =''>
                                      <?php
                                          if(isset($territoire[1][$i])){
                                                 if($territoire[1][$i] == ""){ echo "non défini"; }else{ echo $territoire[1][$i]; }
                                                }
                                                 ?>
                                </li>

                                <?php }
                              }?>

                            </ul>
                        </div>
                    </div>
                </div>

            <?php
                }
            ?>

        <div class="col-md-12 sommom-center" >

            <div class="form-group ">
                <div class="dropdown sppec">

                    <button class="btn  dropdown-toggle" type="button" style="color: #1B4F95;" data-toggle="dropdown">
                        Filtrer par territoire
                        <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu">
                        <li class="button-checkbox" data-color="default">
                            <div class="org-choice">
                                <input type="button" class="btn unselectall" data-loc="territoire" value="Tout désélectionner">
                                <input type="button" class="btn selectall" data-loc="territoire" value="Tout sélectionner">
                            </div>
                        </li>
                        <?php
                            for($i = 0; $i < count($territoire[0]); $i++)
                            {
                        ?>

                                <li class="button-checkbox" data-color="default">
                                    <div class="org-choice">
                                        <input  class="checkbox1" type="checkbox" checked="" data-territoire="<?php echo $i; ?>" data-type="territoire">
                                        <label for="checkbox1">
                                          <?php
                                            if(isset($territoire[1][$i])){
                                             if($territoire[1][$i] == ""){ echo "non défini"; }else{ echo $territoire[1][$i]; }
                                            }
                                          ?>
                                         </label>
                                    </div>
                                </li>

                                <li role="separator" class="divider"></li>
                        <?php
                            }
                        ?>

                    </ul>
                </div>
            </div>

            <div class="form-group ">
                <div class="dropdown sppec">
                    <button class="btn dropdown-toggle" type="button" style="color: #1B4F95;" data-toggle="dropdown">Filtrer par statut
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li class="button-checkbox" data-color="default">
                            <div class="org-choice">
                                <input type="button" class="btn unselectall" data-loc="statut" value="Tout désélectionner">
                                <input type="button" class="btn selectall" data-loc="statut" value="Tout sélectionner">
                            </div>
                        </li>

                       <?php
                          foreach ($paramsData["statut"] as $value) {
                        ?>

                        <li class="button-checkbox" data-color="default">
                         <div class="org-choice">
                                  <input  class="checkbox1" type="checkbox" checked="" data-statut="<?php echo $value;?>" data-type="statut">
                                  <label for="checkbox1">
                                     <?php echo $value;?>
                                  </label>
                              </div>
                        </li>

                        <li role="separator" class="divider"></li>

                        <?php } ?>

                    </ul>
                </div>
            </div>

            <div class="form-group ">
                <div class="dropdown sppec">
                    <button class="btn dropdown-toggle" type="button" style="color: #1B4F95;" data-toggle="dropdown">Filtrer par rôle
                    <span class="caret"></span>
                    </button>

                    <ul class="dropdown-menu">
                        <li class="button-checkbox" data-color="default">
                            <div class="org-choice">
                                <input type="button" class="btn unselectall" data-loc="role" value="Tout désélectionner">
                                <input type="button" class="btn selectall" data-loc="role" value="Tout sélectionner">
                            </div>
                        </li>

                       <?php
                          foreach ($paramsData["role"] as $value) {
                        ?>

                        <li class="button-checkbox" data-color="default">
                         <div class="org-choice">
                                  <input  class="checkbox1" type="checkbox" checked="" data-role="<?php echo $value;?>" data-type="role">
                                  <label for="checkbox1">
                                     <?php echo $value;?>
                                  </label>
                              </div>
                        </li>

                        <li role="separator" class="divider"></li>

                        <?php } ?>

                    </ul>
                </div>
            </div>

        </div>

</div>
</div>
<?php } ?>

<div id="btn-show-map">
    <a href="javascript:;" class="shmap">Afficher la carte</a>
</div>


<?php  if($activegraph == null){ ?>

<div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20" id="orgaliste" style="background-color: #f7f6f3">

   <?php

   for($i = 0; $i < count($territoire[0]); $i++){
            if(count($territoire[3][$i]) > 0){
              $orgcount = 0;
              foreach ($territoire[3][$i] as $value) {

    ?>

    <div class="terr-container col-md-12 col-xs-12 col-sm-12" data-name="<?php echo $value["intitule"]; ?>" style="border-radius: 1rem; padding-left: 0;/*box-shadow: 0 .5px 2px rgba(0, 0, 0, 0.3);*/">
        <div class="col-md-2 col-sm-12 col-xs-12" style="padding-left: 20px;">
          <div class="sommom-center" >
            <!-- <img style="margin-top: 10px;" class="img-org" src="<?php //if($territoire[9][$i] != "") { echo Yii::app()->request->baseUrl ?>/upload/communecter/answers/<?php //echo $territoire[8][$i]; ?>/<?php //echo $territoire[9][$i]; ?> <?php //} else { ?> <?php //echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/bandeau1.jpg <?php //} ?>" > -->

            <?php

            $initAnswerFiles=Document::getListDocumentsWhere(array(
                    "id"=>(string)$territoire[8][$i],
                    "type"=>'answers',
                    "subKey"=>"answers.sommomForm1.sommomForm122.".$orgcount), "file");


                    //echo $this->renderPartial("co2.views.pod.docsList",array("edit"=>false, "documents"=>$initAnswerFiles,"docType"=>"image") );
                    if(!empty($initAnswerFiles)){
                        foreach ($initAnswerFiles as $key => $d) {
                            ?>

                                <img src="<?php echo $d["docPath"] ?>" style="margin-top: 10px; padding-right: 10px;" class="img-org">


                            <?php
                             }
                    } else {
                      ?>
                        <img style="margin-top: 10px; padding-right: 10px;" class="img-org" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/bandeau1.jpg" >
                      <?php

                    }
                    ?>

          </div>
        </div>

        <div class="col-md-10  col-sm-12 col-xs-12" data-territoire="" data-role="<?php //echo $value['role']; ?>" data-statut="<?php //echo $value['statut']; ?>">
          <h3 style="margin-top: 5px; font-weight: 100;" class="sommom-mobile-center"> <?php echo $value["intitule"]; ?> </h3>
            <div class="col sommom-mobile-center">
            <span class="blue-sommom " > Rôle : </span> <?php echo implode(",", $value["role"]);; ?>
            </div>
            <div class="col sommom-mobile-center">
              <span class="blue-sommom"> Statut : </span>  <?php echo $value["statut"]; ?>
            </div>

        </div>

    </div>

  <?php
  $orgcount ++;
   }
 }} ?>
 <div class="col-xs-9 col-sm-9 col-md-9 col-md-offset-1" id="orgaliste" >
<div class="pagination-section">
      <ul class="paginationm-ul paginationm-ul-style">
        <li><a id="prev" href="javascript:;" onclick="prevPage()">Prev</a></li>
        <span class="number" style="display: inline-flex;">
        <li><a href="javascript:;" style="">1</a></li>
        </span>
        <li><a id ="next" href="javascript:;" onclick="nextPage()" >Next</a></li>
      </ul>
</div>
</div>
</div>


<div class="col-xs-9 col-sm-9 col-md-8  " id="territoiremap" style="">

</div>

<!-- News -->


<?php
//$this->renderPartial("costum.views.tpls.blockCms.article.actualiteTimeLine", array('collection' => "organizations", 'kunik'=>"", 'blockKey'=>"" ));

  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    }
?>
<div class="text-center">
    <div class="col-xs-12 col-sm-12">
        <br><br><br><br>
        <div class="actu-titre col-xs-12 col-sm-12">
            <h1 class="sommom-center">Actualités</h1>
        </div>
        <br><br><br><br>
        <div class="actucard col-xs-12 col-sm-12 col-md-12">
            <div id="newsstream"></div>
        </div>

        <div class="col-xs-12 col-sm-12 text-center">
            <a href="javascript:;" data-hash="#news" class="lbh-menu-app h3">
                <i class="fa fa-plus circle"></i>
                <br>Voir plus <br>  d'actualités
            </a>
        </div>
    </div>
</div>
<!-- End News -->


<script type="text/javascript">

  var organisme = [];
  var obszone = [];
  var espgeo = [];
  var territoires = [];
  var totalMap = [];

  <?php for($i = 0; $i < count($territoire[0]); $i++){  ?>

  var t<?php echo $i; ?> = new Object();
      t<?php echo $i; ?>.name = "<?php echo $territoire[1][$i]?>";
      t<?php echo $i; ?>.show = true;
      t<?php echo $i; ?>.showmap = true;
      t<?php echo $i; ?>.id = "<?php echo $i; ?>";

  <?php if(count($territoire[4][$i]) > 0){ ?>
      t<?php echo $i; ?>.havemap = true;

      var contextDataTerritoire<?php echo $i ?> = {
          name : t<?php echo $i; ?>.name,
          type: "poi",
          geo: { "type": "GeoCoordinates", "latitude" : "<?php echo $territoire[4][$i]['latitude']?>", "longitude" : "<?php echo $territoire[4][$i]['longitude']?>"},
          typeObs : 'zone',
      };

      t<?php echo $i; ?>.gps = contextDataTerritoire<?php echo $i ?>;

      totalMap.push(contextDataTerritoire<?php echo $i ?>);

  <?php }else{ ?>
      t<?php echo $i; ?>.havemap = false;
      t<?php echo $i; ?>.gps = [];
  <?php } ?>



  territoires.push(t<?php echo $i; ?>);

  <?php
      if(count($territoire[3][$i]) > 0){
      foreach ($territoire[3][$i] as $value) {
  ?>

    var o<?php echo $i; ?> = new Object();
      o<?php echo $i; ?>.show = true;
      o<?php echo $i; ?>.name = "<?php echo $value["intitule"]; ?>" ;
      o<?php echo $i; ?>.statut = "<?php echo $value["statut"]; ?>" ;
      o<?php echo $i; ?>.role = <?php echo json_encode($value["role"]); ?> ;
      o<?php echo $i; ?>.territoire = <?php echo $i; ?>;

      <?php if(isset($value["pointGPS"])){ ?>
      o<?php echo $i; ?>.havemap = true;

      var contextDataOrganisme<?php echo $i ?> = {
          name : o<?php echo $i; ?>.name,
          type: "poi",
          geo: { "type": "GeoCoordinates", "latitude" : "<?php echo $value["pointGPS"]['latitude']?>", "longitude" : "<?php echo $value["pointGPS"]['longitude']?>"},
          typeObs : 'zone',
      };
      o<?php echo $i; ?>.gps = contextDataOrganisme<?php echo $i ?>;

      totalMap.push(contextDataOrganisme<?php echo $i ?>);

  <?php }else{ ?>
      o<?php echo $i; ?>.havemap = false;
      o<?php echo $i; ?>.gps = [];
  <?php }  ?>




      organisme.push(o<?php echo $i; ?>);


  <?php } ?>


  <?php
      if(count($territoire[5][$i]) > 0){
      foreach ($territoire[5][$i] as $value) {
  ?>

    var z<?php echo $i; ?> = new Object();
      z<?php echo $i; ?>.show = true;
      z<?php echo $i; ?>.showmap = true;
      z<?php echo $i; ?>.name = "<?php echo $value["localisation"]; ?>" ;
      z<?php echo $i; ?>.port = "<?php echo $value["portName"]; ?>" ;
      z<?php echo $i; ?>.territoire = <?php echo $i; ?>;

      <?php if(isset($value["pointGPS"])){ ?>
      z<?php echo $i; ?>.havemap = true;

      var contextDataobszone<?php echo $i ?> = {
          name : z<?php echo $i; ?>.name,
          type: "poi",
          geo: { "type": "GeoCoordinates", "latitude" : "<?php echo $value["pointGPS"]['latitude']?>", "longitude" : "<?php echo $value["pointGPS"]['longitude']?>"},
          typeObs : 'zone',
          port : z<?php echo $i; ?>.port

      };
      z<?php echo $i; ?>.gps = contextDataobszone<?php echo $i ?>;

      totalMap.push(contextDataobszone<?php echo $i ?>);

  <?php }else{ ?>
      z<?php echo $i; ?>.havemap = false;
      z<?php echo $i; ?>.gps = [];
  <?php }  ?>




      obszone.push(z<?php echo $i; ?>);


  <?php }} ?>



  <?php
      if(count($territoire[11][$i]) > 0){
      foreach ($territoire[11][$i] as $value) {
  ?>

    var e<?php echo $i; ?> = new Object();
      e<?php echo $i; ?>.show = true;
      e<?php echo $i; ?>.showmap = true;
      e<?php echo $i; ?>.name = "<?php echo $value["localisation"]; ?>" ;
      e<?php echo $i; ?>.territoire = <?php echo $i; ?>;

      <?php if(isset($value["coord"])){ ?>
      e<?php echo $i; ?>.havemap = true;

      var contextEspge<?php echo $i ?> = {
          name : e<?php echo $i; ?>.name,
          type: "poi",
          geo: { "type": "GeoCoordinates", "latitude" : "<?php echo $value["coord"]['latitude']?>", "longitude" : "<?php echo $value["coord"]['longitude']?>"},
          typeObs : 'zone'

      };
      e<?php echo $i; ?>.gps = contextEspge<?php echo $i ?>;

      totalMap.push(contextEspge<?php echo $i ?>);

  <?php }else{ ?>
      e<?php echo $i; ?>.havemap = false;
      e<?php echo $i; ?>.gps = [];
  <?php }  ?>




      espgeo.push(e<?php echo $i; ?>);


  <?php }} ?>




<?php }} ?>




  var current_page = 1;
  var records_per_page = 4;
  var listing = [];

  $(".checkbox1").on('change', function () {
      toogleT($(this).data($(this).data("type")),$(this).data("type"),$(this).is(':checked'));
      //ToogleMap();
      updateDisplay();
  });



  function callTerritoireMap(territoire){
    getAjax("graphSection",baseUrl+"/costum/sommom/dashboard/tpl/costum.views.custom.sommom.dashboard/answer/5e5e2470681c3376298b456b/activegraph/"+territoire, function(data){
              $('#graphSection').html(data);
            },"html");

    $([document.documentElement, document.body]).animate({
        scrollTop: $("#graphSection").offset().top
    }, 800);
  }


  function updateOrgaListe(){

    for (var i = 0; i < organisme.length; i++) {
        $('div[data-name="'+organisme[i]["name"]+'"]').hide();
        // $("div").find('[data-name="' + organisme[i]['name'] + '"]').hide();
    }
    listing = [];
    for (var i = 0; i < organisme.length; i++) {
      if(organisme[i]["show"] == true){
        listing.push(organisme[i]["name"]);
      }
    }
    changePage(1);

  }



  function updateMap(){
    mapCO.clearMap();
    mapCO.addElts(totalMap);
  }

  function ToogleMap(){
    totalMap = [];

    for (var i = 0; i < territoires.length; i++) {
      if(territoires[i]["showmap"] == true){
        if(territoires[i]["havemap"] == true){
            totalMap.push(territoires[i]["gps"]);
        }
      }
    }

    for (var i = 0; i < obszone.length; i++) {
      if(obszone[i]["showmap"] == true){
        if(obszone[i]["havemap"] == true){
            totalMap.push(obszone[i]["gps"]);
        }
      }
    }

    for (var i = 0; i < espgeo.length; i++) {
      if(espgeo[i]["showmap"] == true){
        if(espgeo[i]["havemap"] == true){
            totalMap.push(espgeo[i]["gps"]);
        }
      }
    }

  }

  function numPages(){
    j = listing.length;

    return Math.ceil(j / records_per_page);
  }

  function prevPage()
  {
      if (current_page > 1) {
          current_page--;
          changePage(current_page);
      }
  }

  function nextPage()
  {
      if (current_page < numPages()) {
          current_page++;
          changePage(current_page);
      }
  }

  function changePage(page)
  {
      if(page > 0 && page < numPages() + 1){
        current_page = page;
      }

      // var btn_next = $("#next");
      // var btn_prev = $("#prev");

      // Validate page
      if (page < 1) page = 1;
      if (page > numPages()) page = numPages();

      // for (var i = 0; i < organisme.length; i++) {
      //     $("div").find("[data-name='" + organisme[i]["name"] + "']").hide();

      // }
      for (var i = 0; i < organisme.length; i++) {
          $('div[data-name="'+organisme[i]["name"]+'"]').hide();
          // $("div").find('[data-name="' + organisme[i]["name"] + '"]').hide();
      }

      for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
          // listing_table.innerHTML += objJson[i].adName + "<br>";
        //for (var i = 0; i < organisme.length; i++) {
            $('div[data-name="'+listing[i]+'"]').show();
            // $("div").find('[data-name="' + listing[i] + '"]').show();

        //}
      }

      $(".number").html("");


      //for(let i = 1; i < page ; i++){

        if(i != page){
            if(current_page < 5){
                for(let i = 1; i < page ; i++){
                  $(".number").append("<li><a href='javascript:' onclick='changePage("+i+");'>"+i+"</a></li>");
                }
            } else {
                $(".number").append("<li><a href='javascript:' onclick='changePage(1);'>1</a></li>");
                $(".number").append("<li><a href='javascript:' onclick=''>...</a></li>");
                for(let i = page - 2; i < page ; i++){
                  $(".number").append("<li><a href='javascript:' onclick='changePage("+i+");'>"+i+"</a></li>");
                }
            }

        }
      //}

      $(".number").append("<li><a href='javascript:' style='color: teal !important;'>"+page+"</a></li>");

      if(numPages() <= page + 3){
          for(let i = page + 1 ; i < numPages() + 1 ; i++){
            if(i < numPages() + 1){
                $(".number").append("<li><a href='javascript:' onclick='changePage("+i+");'>"+i+"</a></li>");
            }
          }
      } else {
          $(".number").append("<li><a href='javascript:' onclick='changePage("+(page+1)+");'>"+(page+1)+"</a></li>");
          $(".number").append("<li><a href='javascript:' onclick='changePage("+(page+2)+");'>"+(page+2)+"</a></li>");
          $(".number").append("<li><a href='javascript:' onclick=''>...</a></li>");
          $(".number").append("<li><a href='javascript:' onclick='changePage("+numPages()+");'>"+numPages()+"</a></li>");

      }





      if (page == 1 || page == 0) {
          $("#prev").hide();
      } else {
           $("#prev").show();
      }

      if (page == numPages() || page == 0) {
          $("#next").hide();
      } else {
          $("#next").show();
      }
  }

  function updateDisplay(){
    updateOrgaListe();
    //updateMap();
  }



  function toogleT(key, type,  chkd){
      if(type=="territoire"){
        for (var i = 0; i < territoires.length; i++) {
          if(territoires[i]["id"] == key){
            if(chkd){
              territoires[i]["show"] = true;
            } else {
              territoires[i]["show"] = false;
            }
          }
         }
      }


      for (var i = 0; i < organisme.length; i++) {

        if(type != "role"){
          if(organisme[i][type] == key){
            if(chkd){
              organisme[i]["show"] = true
            } else {
              organisme[i]["show"] = false
            }
          }
        } else {

          var arrayChecked = [];

          $('input[data-role]').each(function () {
            var sThisVal = (this.checked ? $(this).data("role") : "");
            arrayChecked.push(sThisVal);
          });

          var same = false;

          for (j = 0; j < organisme[i]["role"].length; j++) {

            for (t = 0; t < arrayChecked.length; t++) {
                if(organisme[i]["role"][j] == arrayChecked[t]){
                  console.log(organisme[i]["role"][j]+"///"+arrayChecked[t]);
                  same = true;
                }
              }
            }


          organisme[i]["show"] = same;
        }


      }
  }

  function toogleValue(key, type,  chkd){
      if(type=="territoire"){
        for (var i = 0; i < territoires.length; i++) {
          if(territoires[i]["id"] == key){
            if(chkd){
              territoires[i]["showmap"] = true;
            } else {
              territoires[i]["showmap"] = false;
            }
          }
        }

        for (var i = 0; i < obszone.length; i++) {
          if(obszone[i]["territoire"] == key){
            if(chkd){
              obszone[i]["showmap"] = true;
            } else {
              obszone[i]["showmap"] = false;
            }
          }
        }

        for (var i = 0; i < espgeo.length; i++) {
          if(espgeo[i]["territoire"] == key){
            if(chkd){
              espgeo[i]["showmap"] = true;
            } else {
              espgeo[i]["showmap"] = false;
            }
          }
        }


      }



  }

  $(document).ready(function(){
      updateOrgaListe();
  });

</script>

<?php }} ?>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20 dashboard app-<?php echo @$page ?>">

<style>
  /*body{margin-top:50px;}*/
  .glyphicon { margin-right:10px; }
  .panel-body { padding:0px; }
  .panel-body table tr td { padding-left: 15px }
  .panel-body .table {margin-bottom: 0px; }
  .panel-body { font-size: 15px; }
  .s19{list-style: none;}
  .s19 li:before{
      content: '\f0a9';
      margin-right: 15px;
      font-family: FontAwesome;
      color: #d9534f;
  }
  .s19li {
      font-size: 15px;
  }
  .container {
    max-width: 970px;
  }

  div[class*='col-'] {
    padding: 0 30px;
  }

  .wrap {
    box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
    border-radius: 4px;
  }

  .panel {
    border-width: 0 0 1px 0;
    border-style: solid;
    border-color: #fff;
    background: none;
    box-shadow: none;
  }

  .panel:last-child {
    border-bottom: none;
  }

  .panel-group > .panel:first-child .panel-heading {
    border-radius: 4px 4px 0 0;
  }

  .panel-group .panel {
    border-radius: 0;
  }

  .panel-group .panel + .panel {
    margin-top: 0;
  }

  .panel-heading {
    background-color: #009688;
    border-radius: 0;
    border: none;
    color: #fff;
    padding: 0;
  }

  .panel-title a {
    display: block;
    color: #fff;
    padding: 15px;
    position: relative;
    font-size: 16px;
    font-weight: 900;
  }

  .acteurtitle {
     background-color: #f0ad4e !important;
  }

  .panel-body {
    background: #fff;
  }

  .panel:last-child .panel-body {
    border-radius: 0 0 4px 4px;
  }

  .panel:last-child .panel-heading {
    border-radius: 0 0 4px 4px;
    transition: border-radius 0.3s linear 0.2s;
  }

  .panel:last-child .panel-heading.active {
    border-radius: 0;
    transition: border-radius linear 0s;
  }


  /* #accordion rotate icon option */
  #bs-collapse .panel-heading a:before {
    content: '\f078';
    font-size: 18px;
    position: absolute;
    font-family: FontAwesome;
    right: 5px;
    top: 10px;
    /*transform: rotate(180deg);*/
    transition: all 0.5s;
  }

  #bs-collapse .panel-heading.active a:before {
    content: '\f078';
    font-size: 18px;
    position: absolute;
    font-family: FontAwesome;
    right: 5px;
    top: 10px;
    /*transform: rotate(180deg);*/
    transition: all 0.5s;
  }


  /*#dribbble {
    position: fixed;
    display: block;
    right: 70px;
    bottom: 16px;
    svg {
      display: block;
      width: 76px;
      height: 24px;
      fill: rgba(#929cd0, .8);
    }
  }*/

  .beyondthemap {
      font-size: 25px;
      z-index: 10;
    position: absolute;
      /* border: 1px solid black; */
      top : 30px;
      left: 100px;
     display: flex;
    justify-content: center;
    align-items: center;
  }

  .activ-Z {
      background-color: #285e61 !important ;
      color : #fff !important ;
  }

  .btn-zone {
      font-weight: 600;
      border-radius: 9999px;
      color: #4a5568;
      background-color: #e2e8f0;
      transition-duration: .7s;
      padding-left: 1rem;
      padding-right: 1rem;
      padding-top: .5rem;
      padding-bottom: .5rem;
      margin: 5px;
  }

  .activ-A {
      background-color: #f0ad4e !important;
      color : #fff !important;
  }

  .btn-acteur {
      font-weight: 600;
      border-radius: 9999px;
      color: #4a5568;
      background-color: #e2e8f0;
      transition-duration: .7s;
      padding-left: 1rem;
      padding-right: 1rem;
      padding-top: .5rem;
      padding-bottom: .5rem;
      margin: 5px;
  }

  /*.cta {
     display: flex;
    justify-content: center;
    align-items: center;
    width: 66px;
    height: 66px;
     border: 1px solid #fff;
    background: #f1f3f6;
    border-radius: 20px;

    .icon {
      color: #9dabc0;
      height: 30px;
      width: 30px;
    }
  }*/

  .footer {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    background-color: #efefef;
    text-align: center;
  }

</style>

   <div classs="col-xs-12" style="">
        <?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?>
        <div class="col-xs-12 equalHeight" id="graphSection">
                <?php
                $blocksize = (!empty($blocks)) ? floor(12/count($blocks)) : 12;
                $colCount = 0;
                $fontSize = "24px;font-weight:bold;";

                 foreach ($blocks as $id => $d)
                 {
                    $borderClass = (isset($d["noborder"])) ? "" : "wborder";
                    if( ( isset($d["tpl"]) || isset($d["graph"])) && count($blocks) > 3){
                        //OnlyForTest $blocksize = 4;
                        $blocksize = 6;
                        $borderClass =  "wborder";
                        $fontSize = "24px;font-weight:bold;";
                    }
                    else if( isset($d["html"]) ){
                        $blocksize = 2;
                        $fontSize = "18px;";
                    }
                    else if( isset($d["offset"]) ){
                        $blocksize = 1;
                    }else if( isset($d["col12"]) ){
                        $blocksize = 12;
                    }

                    ?>



                    <?php
                    if(isset($d["tpl"]))
                    {
                        $style = "style='";
                        if(isset($d["bgColor"]))
                            $style .= "background-color:".$d["bgColor"].";";
                        if(isset($d["color"]))
                            $style .= "color:".$d["bgColor"].";";
                        $style .= "'";?>
                        <div class="col-md-<?php echo ( isset($d["blocksize"]) ) ? $d["blocksize"] : $blocksize ?>  text-center padding-10 margin-top-20"   >
                            <div style='font-size:<?php echo $fontSize?>'><?php echo $d["title"]?></div>
                            <div <?php echo $style?> class="counterBlock <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                                <?php echo $this->renderPartial($d["tpl"],
                                            [ "data" => $d["data"],
                                            "color"  => (isset($d["color"])) ? $d["color"] : "black" ],true);   ?>
                            </div>
                        </div>
                    <?php
                    }
                    else if( isset($d["col12"]) )  { ?>
                        <div class="col-md-<?php echo $blocksize?> text-center padding-10 "  >
                            <?php echo $d["col12"]?>
                        </div>
                    <?php } else if( isset($d["map"]) )  { ?>
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <div id='map<?php echo $id?>' class='col-xs-12' style='height: 420px;'></div>
                            <script type="text/javascript">
                                mylog.log("map","id:map<?php echo $id?>","var js :mapDash<?php echo $id?>");
                                var mapDash<?php echo $id?> = {};
                                jQuery(document).ready(function() {
                                    mapDash<?php echo $id?> =  mapObj.init({
                                        container : "map<?php echo $id?>",
                                        latLon : [ 39.74621, -104.98404],
                                        activeCluster : true,
                                        zoom : 16,
                                        activePopUp : true,
                                        menuRight : false
                                    });
                                    mapDash<?php echo $id?>.addElts(<?php echo json_encode( $elements) ?>, true);
                                });
                                //.clearMap
                            </script>
                        </div>

                    <?php } elseif( isset($d["indicator"])  )  {
                        ?>
                        <div class="col-md-12 text-center padding-10 "  >
                        <h1><?php echo $d["title"] ?></h1>
                        </div>
                        <?php
                        foreach ($d["indicator"] as $ix => $ind) {
                        ?>
                        <div class=" col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <div class="<?php echo $borderClass; ?>">
                                <h4 style="color:#18a47d"><?php echo $ind["title"];?></h4>

                                <h5>Engagement : <?php echo $ind["obj"];?></h5>
                                <h5>Réalisé : <?php echo $ind["done"];?></h5>
                            </div>
                        </div>

                        <?php }


                     } elseif( isset($d["graph"]) || isset($d["html"])  )  { ?>
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <h1><span class="<?php echo (!empty($d["counter"]))? "counter" : ""?>"><?php echo (!empty($d["counter"]))?$d["counter"] : "&nbsp;"?></span></h1>
                            <div style='font-size:<?php echo $fontSize?>'><?php echo $d["title"]?></div>
                            <div class="counterBlock <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                                <?php if( isset($d["html"]) )
                                    echo $d["html"];?>
                            </div>
                        </div>
                     <?php

                     } ?>
                <?php
                     $colCount = $colCount + $blocksize;
                     // if($colCount == 12){
                     //    echo '<div class="col-xs-12"></div>';
                     //    $colCount = 0;
                     // } ?>
             <?php }?>
        </div>
   </div>

</div>

<script type="text/javascript">

<?php
foreach ($blocks as $id => $d) {
    if( isset($d["graph"]) ) {
        ?>
        var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
        mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
<?php }
} ?>


jQuery(document).ready(function() {
    $('.collapse.in').prev('.panel-heading').addClass('active');
    $('#accordion, #bs-collapse').on('show.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').addClass('active');
    }).on('hide.bs.collapse', function(a) {
        $(a.target).prev('.panel-heading').removeClass('active');
    });

    setTitle("<?php echo strip_tags($title).': Observation Dauphin Baleine à '.$ters ?>");

    mylog.log("render graph","/modules/costum/views/custom/ctenat/dashboard.php");

    <?php  foreach ($blocks as $id => $d) {
        if( isset($d["graph"]) ) { ?>
            mylog.log('url graphs',' <?php echo $d["graph"]["url"]?>');
            ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
    <?php }
    } ?>

    $('.counter').counterUp({
      delay: 10,
      time: 2000
    });

    $('.counter').addClass('animated fadeInDownBig');
    $('h3').addClass('animated fadeIn');



    mapCO.clearMap();

    var mapCO_options = mapCO.getOptions();
    mapCO_options.mapCustom.getPopup = function (data) {
        if(data["typeObs"] !== undefined){
            if(data["typeObs"] == "zone"){
              return '<h4>'+data["name"]+'</h4>';

            }

        } else {
            return mapCustom.popup.default(data, this);
        }
    }
    mapCO.setOptions(mapCO_options)

    mapCO.addElts(totalMap);

    mapCO.getMap().invalidateSize();

  $('.shmap').off().click(function(){
    if(typeof mapCO != 'undefined'){
          showMap();
           $('#mapContent').append( '<div class="py-4 px-4 beyondthemap"></div>');
           $('.beyondthemap').html(
              '<div class="form-group ">'+
              '<div class="dropdown sppec">'+
                '<button class="btn  dropdown-toggle" type="button" style="color: #1B4F95;" data-toggle="dropdown">Filtrer par territoire'+
                  '<span class="caret"></span></button>'+
                    '<ul class="dropdown-menu">'+
                        '<li class="button-checkbox" data-color="default">'+
                            '<div class="org-choice">'+
                                '<input type="button" class="btn unselectall" data-loc="territoire" value="Tout désélectionner">'+
                                '<input type="button" class="btn selectall" data-loc="territoire" value="Tout sélectionner">'+
                            '</div>'+
                        '</li> '+
                    <?php
                        for($i = 0; $i < count($territoire[0]); $i++){
                    ?>

                    '<li class="button-checkbox" data-color="default">'+
                        '<div class="checkbox checkbox-primary">'+
                            '<input  class="checkbox2" type="checkbox" checked="" data-territoire="<?php echo $i; ?>" data-type="territoire" style="margin-left: 0px;"> '+
                                '<label for="checkbox2">'+

                                    '<?php
                                    if(isset($territoire[1][$i])){
                                     if($territoire[1][$i] == ""){ echo "non défini"; }else{ echo $territoire[1][$i]; }
                                    }
                                     ?>'+


                                '</label>'+
                        '</div>'+
                    '</li> '+

                    <?php } ?>

                '</ul>'+
           '</div>'+
        '</div>'
            );

        $(".checkbox2").on('change', function () {
            //alert('aaa');
             toogleValue($(this).data($(this).data("type")),$(this).data("type"),$(this).is(':checked'));
             ToogleMap();
             updateMap();
            //updateDisplay();
        });

        $(".unselectall").off().on("click",function() {
    $("input[data-type='"+$(this).data('loc')+"']").each(function(){
            // alert(JSON.stringify($(this)));
            $(this).prop('checked', false).change();
        });
    });

    $(".selectall").off().on("click",function() {
        $("input[data-type='"+$(this).data('loc')+"']").each(function(){
            // alert(JSON.stringify($(this)));
            $(this).prop('checked', true).change();
        });
    });


        }
  });


});

</script>




<?php
if(isset($elements)){
?>
<style type="text/css">
    .dashElem{
        /*height:275px;*/
        overflow:hidden;
        /*border: 1px solid #bbb;*/
    }
    .grayimg{
        opacity:0.2;
    }
    .openDashModal{
        font-size: 10px;
    }
</style>

<?php
    $badgeColor = [];
    $colorCt = 0;
    $colorList = array_slice(Ctenat::$COLORS, 3);
    foreach ($this->costum["lists"]["domainAction"] as $key => $value) {
        $badgeColor[$key] = $colorList[$colorCt];
        $colorCt++;
    }

    function getBadgeFamily($tag, $badgeColor){
        foreach ($this->costum["lists"]["domainAction"] as $key => $childBadges) {
            foreach ($childBadges as $ic => $cb) {
                if($tag == $cb)
                    return array(
                        "color"  => $badgeColor[$key],
                        "parent" => $key );
            }

        }
    }

    echo "<div class='col-xs-12'>";
        $slugData = (isset($slug)) ? " data-slug='".$slug."'" : "";
        echo "<div class='col-xs-12 margin-bottom-20'>";
            foreach ($badgeColor as $badge => $color) {

                echo "<a href='javascript:;' data-badge='".$badge."' ".$slugData." class='openDashModal btn btn-xs' style='margin-left:5px; background-color:".$color."'>#".$badge."</a>";
            }
        echo "</div>";
        echo "<h1 class='text-center' id='dashElemTitle'>Projets ".$blocks[ array_keys( $blocks)[0] ]["title"]." (".count($elements).")</h1>";
        $elCount = 0;
        foreach ($elements as $id => $p) {
            // echo $tag;
            //  echo $el["type"];
            if( ($elCount % 4) == 0)
                echo "<div class='col-xs-12 margin-top-20'></div>";
            $tagClasses = "";
            if(isset($p["tags"])){
                foreach ( $p["tags"] as $i => $t ) {
                   $tagClasses .= InflectorHelper::slugifyLikeJS($t)." ";
                }
            }
            echo "<div class='col-xs-12 col-sm-3 dashElem ".$tagClasses."'>";
                echo "<div style='height:150px;overflow:hidden;'>";
                if(isset($p["profilMediumImageUrl"])){
                    echo "<img style='margin:auto; width:100%;' src='".Yii::app()->createUrl($p["profilMediumImageUrl"])."' class='img-responsive'/>";
                } else {
                    echo '<img class="img-responsive grayimg"  style="margin:auto; width:100%;" src="'.Yii::app()->getModule("costum")->assetsUrl.'/images/ctenat/action.png">';
                }
                echo "</div>";
                // les badges
                echo "<div>";
                   echo "<a href='#@".$p["slug"]."' class='lbh-preview-element text-dark' data-id='".$id."' data-type='projects'> ".$p["name"]." </a>";
                    if(isset($p["tags"])){
                        foreach ( $p["tags"] as $i => $t ) {
                           $badgeParent = (getBadgeFamily($t, $badgeColor)) ? getBadgeFamily($t, $badgeColor) : array("parent"=>"","color"=>"#ccc");

                           if( !empty($badgeParent["parent"] ))
                                echo "<br/><a href='javascript:;' data-badge='".$p["domaineAction"]["family"]."'  ".$slugData." class='btn btn-xs openDashModal' style='background-color:".$badgeColor[$p["domaineAction"]["family"]]."'>#".$t."</a>";
                            // CibleDD :  else echo "<br/><span class='openDashModal' style='color:red;padding:3px; background-color:".$badgeParent["color"]."'>#".$t."</span>";
                        }
                    }
                echo "</div>";
                //Cterr
                if(isset($p["links"]["projects"])){
                    $cterId = array_keys($p["links"]["projects"]);

                    if(isset($cterId[0])){
                        $cter = PHDB::findOne(Project::COLLECTION, array("_id"=>new MongoId($cterId[0])),array("name","slug"));
                        echo "<div>";

                            echo "<a href='#@".$cter["slug"]."' class='lbh-preview-element text-dark' data-id='".$cterId[0]."' data-type='project'> <i class='fa fa-map-marker'></i> ".$cter["name"]." </a>";
                        echo "</div>";
                    }
                }

        echo "</div>";
        $elCount++;
    }
    echo "</div>";
}

?>
<script type="text/javascript">

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/ctenat/dashboard.php");
        coInterface.bindLBHLinks();
        $('.openDashModal').off().click(function() {
            tag = $(this).data("badge");
            slug = ($(this).data("slug")) ? "/slug/"+$(this).data("slug") : "";
            smallMenu.openAjaxHTML( baseUrl+'/costum/ctenat/dashboard/tag/'+$(this).data("badge")+slug );
        });

    });
</script>

<?php if($activegraph == null){ ?>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20">
<hr>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 footersdisclaimer bg-light border " >
    <div class="container">
        <div class="row ">
            <div class="col-md-12 py-2">
                <p class="mst" style="text-align: justify;">
                Cette plateforme est réalisée dans le cadre du projet SOMMOM “Suivi et encadrement de l’activité d’observation des cétacés dans les territoires français d’outre-mer”, financé par l’Office Français de la Biodiversité (OFB).
                Le projet est porté par l’équipe Quiétude du CEDTM mise en place au titre des mesures compensatoires du projet Nouvelle Route du Littoral financé par la Région Réunion, l’Etat et l’Union Européenne.
                </p>

            </div>
        </div>
    </div>
    <style type="text/css">
      .nav-logo {
      display: flex;
      align-items: center;
      }

      .logofoot{
        height: 150px;
        padding-left: 20px;
      }

      @media only screen and (max-width: 1000px) {
         .logofoot{
            height: 100px
        }
      }

      @media only screen and (max-width: 600px) {
         .logofoot{
            height: 70px
        }
      }

      hr {
  margin-top: 1rem;
  margin-bottom: 1rem;
  border: 0;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
}
    </style>

    <div class="container ">
       <div class="row nav-logo">
          <div class="">

           <a href="https://ofb.gouv.fr"><img  class="logofoot"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/logo-ofb-actualite.jpg" ></a>
          </div>

           <div class="">
            <a href="https://cedtm-asso.org/">
           <img class="logofoot" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/cdv-cedtm-blanc.png" >
           </a>
          </div>

           <div  class="">
            <a href="https://www.regionreunion.com/">
            <img class="logofoot"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/logor.png" >
            </a>

          </div>
           <div class="">
            <a href="http://www.nouvelleroutedulittoral.re/">
            <img class="logofoot"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/sommom/images.png" >
            </a>

          </div>


       </div>
   </div>
</div>

<?php } ?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/js/uikit.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.40/js/uikit-icons.min.js"></script>

<script type="text/javascript">
      jQuery(document).ready(function() {
             $('.owl-carousel').owlCarousel({
              loop:false,
            stagePadding: 15,
              margin:10,
              nav:true,
            navText : ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
              responsive:{
                  0:{
                      items:1
                  },
                  640:{
                      items:1
                  },
                960:{
                      items:2
                  },
                  1200:{
                      items:3
                  }
              }
    });

             $(".owl-next").attr('style', "padding: 2px 5px 0px 5px !important;");
             $(".owl-prev").attr('style', "padding: 2px 5px 0px 5px !important;");
           });

</script>

<script type="text/javascript">
jQuery(document).ready(function() {
    const viewMoreText = "...<b>Voir plus </b></a>";
    const viewLessText = "<b> Voir moins</b></a>";

    document.querySelectorAll("span.cardlist").forEach((paragraph) => {
        var paragraphIsOverflowing =
            paragraph.clientHeight < paragraph.scrollHeight ? true : false;
        if (!paragraphIsOverflowing) return;

        var theToggleLink = document.createElement("A");
        theToggleLink.classList.add("toggle");
        theToggleLink.innerHTML = viewMoreText;

        theToggleLink.onclick = () => {
            if (paragraph.classList.contains("max-lines-visible")) {
                paragraph.classList.remove("max-lines-visible");
                theToggleLink.innerHTML = viewMoreText;
                return;
            }
            paragraph.classList.add("max-lines-visible");
            theToggleLink.innerHTML = viewLessText;
        };

        paragraph.closest('span.spanlistcont').append(theToggleLink);
    });

  $(".lbh-sommom-graph").on("click", function(){
    var href = window.location.href.split("#")[0];
    href += $(this).attr('href');
    window.location = href;
    window.location.reload()
  });

  $("header").append("<meta name='robots' content='index, follow'>");

  <?php
  if(isset($_SESSION["costum"][$this->costum["contextSlug"]]["isMember"]) || $_SESSION["costum"][$this->costum["contextSlug"]]["isAdmin"]){

 ?>
    if($("#btn-nextcloud").length == 0) {
      $("#menuLeft a:eq(3)").after(`
          <a id="btn-nextcloud" class="text-center text-dark menu-app hidden-xs btn btn-link menu-button btn-menu btn-menu-tooltips pull-left" href="https://conextcloud.communecter.org/" style="text-decoration : none;" target="_blank">
              <i class="fa fa-file-text-o" aria-hidden="true"></i>
              <span class="tooltips-menu-btn" style="display: none;">Partage de documents</span>
          </a>
      `);
    }

<?php } ?>

<?php if(isset($adminStatus)) { ?>
  <?php if($adminStatus) { ?>


    $('.deleteAnswer').off().click( function(){
      id = $(this).data("id");
      bootbox.dialog({
          title: trad.confirmdelete,
          message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
          buttons: [
            {
              label: "Ok",
              className: "btn btn-primary pull-left",
              callback: function() {
                getAjax("", baseUrl+"/survey/co/delete/id/"+id , function(){
                  //urlCtrl.loadByHash(location.hash);

                  $("#terrid"+id).hide();
                },"html");
              }
            },
            {
              label: "Annuler",
              className: "btn btn-default pull-left",
              callback: function() {}
            }
          ]
      });
    });


<?php } }?>

tplCtx = {};

sectD = {
  "jsonSchema":{
      "title":"Les organismes impliqués dans l’activité",
      "icon":"fa-globe",
      "text":"",
      "properties":{
          "intitule":{
              "label":"Nom de l’organisme",
              "placeholder":"Intitulé",
              "inputType":"text",
              "rules":{"required":true}},
              "role":{
                  "label":"Rôles",
                  "placeholder":"Choisir parmis les rôles de la liste",
                  "inputType":"selectMultiple",
                  "noOrder":true,
                  "options":{"Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation":"Organisme impliqué dans le suivi, l’encadrement de l’activité d’observation","Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien":"Organisme impliqué dans la pratique de l’activité d’observation : Opérateur en mer, opérateur aérien","Organisme impliqué dans la science et la recherche sur l’activité d’observation":"Organisme impliqué dans la science et la recherche sur l’activité d’observation","Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation":"Organisme impliqué dans l’éducation et la sensibilisation sur l’activité d’observation","Organisme impliqué dans la protection de l’environnement et la conservation des espèces":"Organisme impliqué dans la protection de l’environnement et la conservation des espèces"},
                  "rules":{"required":true}
                },
                  "statut":{"label":"Statut","placeholder":"Statut","inputType":"select","noOrder":true,"options":{"Service représentant de l’Etat":"Service représentant de l’Etat","Collectivité":"Collectivité","Syndicat, fédération, représentant d’acteurs de la mer":"Syndicat, fédération, représentant d’acteurs de la mer","Structure privée commerciale":"Structure privée commerciale","Association de sport et loisir":"Association de sport et loisir","Association environnementale":"Association environnementale","Institut de recherche":"Institut de recherche","Autre":"Autre"},"rules":{"required":true}
                },
                "contact":{"label":"contact","placeholder":"Adresse mail","inputType":"text","rules":{"required":false}},
                "file" : { "label" : "logo", "placeholder" : "logo", "inputType" : "uploader", "rules" : { "required" : false },
                    "docType" : "file"
                }
              },
              save : function () {
                        var today = new Date();
                        tplCtx.value = { date : today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear() };
                        $.each( sectD.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });

                        mylog.log("save tplCtx",tplCtx);
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                               $("#ajax-modal").html("<div class='text-center'><i class='fa fa-spin fa-spinner'></i></div>");//$("#ajax-modal").modal('hide');
                                location.reload();
                            });
                        }

                    }
            }
}

$(".addorganisme").off().on("click",function() {
    tplCtx.id = $(this).data("id");
    tplCtx.collection = $(this).data("collection");
    tplCtx.path = $(this).data("path");
    dyFObj.openForm( sectD );
});

$('.editterritoire').off().on("click", function(e) {
    e.preventDefault();
    window.open($(this).data('href'));
    return false;
} );

$('.downloadpdf').off().on("click", function(e) {
    e.preventDefault();
    window.open($(this).data('href'));
    return false;
} );

$(".devenirmembre").off().on("click",function() {
    $('#modalRegister').modal("show");
});

$(".unselectall").off().on("click",function() {
    $("input[data-type='"+$(this).data('loc')+"']").each(function(){
        // alert(JSON.stringify($(this)));
        $(this).prop('checked', false).change();
    });
});

$(".selectall").off().on("click",function() {
    $("input[data-type='"+$(this).data('loc')+"']").each(function(){
        // alert(JSON.stringify($(this)));
        $(this).prop('checked', true).change();
    });
});

$("a[data-target='#modalRegister']").on("click",function(e) {
  e.stopPropagation();
  urlCtrl.loadByHash("#membre");
});

});


  $("#newsstream").empty();
  // News
  let urlNews = "co2/app/live";
  urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/isLive/false/formCreate/false/nbCol/2";
  // Get News
  ajaxPost("#newsstream",baseUrl+"/"+urlNews,{}, function(news){}, function(error){}, "html");
</script>
