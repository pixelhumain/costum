<style>
    #modal-preview-coop{
        overflow: auto;
    }
    .short-description{
        font-size:20px;
        text-align: justify;
    }

    .description-preview.activeMarkdown p,
    .area-preview.activeMarkdown p,
    .description-preview.activeMarkdown li,
    .area-preview.activeMarkdown li{
        font-size: 14px !important;
    }
    .heading img{
        width: 10%;
    }
    .preview-info{
        padding: 5%;
        background:#fff;
        box-shadow: 0px 0px 4px 0px #b0b3b7;
        margin-top: 10px;
        margin-bottom: 20px;
    }
    .preview-image{
        text-align:center;
    }
    .preview-content{
        text-align:left;
    }
    .preview-content .title-preview {
        font-weight:600;
        text-transform: none;
    }
    .preview-content .preview-shortDescription {
        font-size: 18px;
    }

    .preview-content .tags a{
        padding: 8px;
        color: #1A2660;
        background: #f3f3f3;
        display: inline-block;
        font-size: 13px;
        line-height: 11px;
        border-radius: 10px;
        margin-bottom: 5px;
        margin-right: 2px;
        text-decoration: none;
    }
    .preview-content .tags a:hover{
        background-color: #1A2660;
        color: white;
    }

    @media (max-width: 1199px) {

        .preview-content{
            text-align:center;
        }
        .preview-content .title-preview {
            text-align: center;
            font-size: 24px;
        }
        .preview-content .preview-shortDescription {
            text-align: center;
        }
    }

    .transition-timer-carousel .carousel-caption {
        background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 4%, rgba(0,0,0,0.5) 32%, rgba(0,0,0,1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(4%,rgba(0,0,0,0.1)), color-stop(32%,rgba(0,0,0,0.5)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.1) 4%,rgba(0,0,0,0.5) 32%,rgba(0,0,0,1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
        width: 100%;
        left: 0px;
        right: 0px;
        bottom: 0px;
        text-align: left;
        padding-top: 5px;
        padding-left: 15%;
        padding-right: 15%;
    }
    .transition-timer-carousel .carousel-caption .carousel-caption-header {
        margin-top: 10px;
        font-size: 24px;
    }

    .transition-timer-carousel .carousel-indicators {
        bottom: 0px;
        margin-bottom: 5px;
    }
    .transition-timer-carousel .carousel-control {
        z-index: 11;
    }
    .transition-timer-carousel .transition-timer-carousel-progress-bar {
        height: 5px;
        background-color: #5cb85c;
        width: 0%;
        margin: -5px 0px 0px 0px;
        border: none;
        z-index: 11;
        position: relative;
    }
    .transition-timer-carousel .transition-timer-carousel-progress-bar.animate{
        /* We make the transition time shorter to avoid the slide transitioning
        before the timer bar is "full" - change the 4.25s here to fit your
        carousel's transition time */
        -webkit-transition: width 4.25s linear;
        -moz-transition: width 4.25s linear;
        -o-transition: width 4.25s linear;
        transition: width 4.25s linear;
    }
</style>
<?php

if(isset($element)){
    $element = $element;
}else{
    $element=PHDB::findOne($collection,array("_id"=> new MongoId($id)));
}
//var_dump($element);
?>
<div class="margin-top-25 margin-bottom-50 col-xs-12">
    <div class="col-xs-12 no-padding toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview" >
            <i class="fa fa-times"></i>
        </button>
    </div>

    <div class="col-xs-12 preview-info">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xl-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="preview-image">
                            <!--<img src="<?php /*echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) */?>" alt="image" />-->
                            <?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?>
                                <a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
                                   class="thumb-info"
                                   data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
                                   data-lightbox="all">
                                    <img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
                                </a>
                            <?php }else{ ?>
                                <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xl-12">
                <div class="preview-content">
                    <h1 class="title-preview"><?php echo $element["name"]; ?></h1>
                    <?php if(isset($element["description"]) && !empty($element["description"]) ) echo "<div class='col-xs-12 preview-shortDescription markdown-text'>".$element["description"]."</div>"; ?>
                </div>
            </div>
        </div>

        <!-- The carousel -->
        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel margin-top-30" data-ride="carousel" >
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#transition-timer-carousel" data-slide-to="1"></li>
                <li data-target="#transition-timer-carousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <!-- <div class="item active">
                    <img src="http://placehold.it/1200x400" />
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="http://placehold.it/1200x400/AAAAAA/888888" />
                    <div class="carousel-caption">
                    </div>
                </div>

                <div class="item">
                    <img src="http://placehold.it/1200x400/888888/555555" />
                    <div class="carousel-caption">
                    </div>
                </div> -->
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>

            <!-- Timer "progress bar" -->
            <hr class="transition-timer-carousel-progress-bar animate" />
        </div>


    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var previewProductPrez = {
            initScreenShot : function(tplData) {
                var completedItems = 4;
                var screenShotItem = "";
                ajaxPost(
                    null,
                    baseUrl+"/"+moduleId+"/cms/getlistfile",
                    {costumType : "templates", costumId : '<?= $id ?>', subKey: "tplAlbum", doctype : "image"},
                    function (screenData) {
                        completedItems -= screenData.result.length
                        console.log("completedItems",screenData.result.length)
                        if (screenData.result.length != 0) {
                            for (var i = 0; i < 4; i++) {
                                if (typeof screenData.result[i] != "undefined") {
                                 screenShotItem += `
                                 <div class="item ${ i == 0 ? "active": ""}">
                                 <img src="${screenData.result[i].imagePath}" />
                                 <div class="carousel-caption">
                                 </div>
                                 </div>`
                             }
                         }
                         $(".carousel-inner").append(screenShotItem)
                     }else{                        
                        $("#transition-timer-carousel").remove()
                     }
                   }                                             
                   );
            }
        };
        previewProductPrez.initScreenShot()
        //Events that reset and restart the timer animation when the slides change
        $("#transition-timer-carousel").on("slide.bs.carousel", function(event) {
            //The animate class gets removed so that it jumps straight back to 0%
            $(".transition-timer-carousel-progress-bar", this)
                .removeClass("animate").css("width", "0%");
        }).on("slid.bs.carousel", function(event) {
            //The slide transition finished, so re-add the animate class so that
            //the timer bar takes time to fill up
            $(".transition-timer-carousel-progress-bar", this)
                .addClass("animate").css("width", "100%");
        });

        //Kick off the initial slide animation when the document is ready
        $(".transition-timer-carousel-progress-bar", "#transition-timer-carousel")
            .css("width", "100%");
    });
</script>



