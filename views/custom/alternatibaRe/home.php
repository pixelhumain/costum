<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );

	$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);

	$cssAnsScriptFilesModuleMap = array( 
		'/leaflet/leaflet.css',
		'/leaflet/leaflet.js',
		'/css/map.css',
		'/markercluster/MarkerCluster.css',
		'/markercluster/MarkerCluster.Default.css',
		'/markercluster/leaflet.markercluster.js',
		'/js/map.js',
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
     
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

	$logo = $this->costum["logo"];

	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

  		$blockCmss = PHDB::find(Cms::COLLECTION, 
                  array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                         "page" => "welcome") );
  		// var_dump($blockCmss);exit;
  		$blockCmsById = [];

  		foreach ($blockCmss as $key => $value) {
  			if (isset( $value["id"] )) {
  				$blockCmsById[$value["id"]] = $value;
  			}
  		}
  	}
  	$page = "welcome";

    $params = [  "tpl" => "alternatibaRe","slug"=>$this->costum["slug"],"canEdit"=>false,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );
?>

<style type="text/css">
	
	@font-face{
	    font-family: "Helvetica";
	    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/alternatibaRe/Helvetica.otf")
	}

	.cls-1 {
    	fill: #098048 !important;
	}

  	#dropdown {
	    background-color: whitesmoke;
	    width: 55% !important;
	    z-index: 1000;
	}

	.article-plus {
	    width: 16% !important;
	    margin-left: 1%;
	}

	.footerActus {
		display: none !important;
	}
</style>

<div class="one col-xs-12">
	<center>
		<img class="img-responsive img-one" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/logo1.svg">
		<div style="margin-left: 18vw;margin-top: 1vw;" class="hidden-xs col-xs-8" id="searchBar">
			<a data-type="filters" href="javascript:;">
    			<span id="second-search-bar-addon-alternatibaRe" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
        			<i id="fa-search" class="fa fa-search"></i>
    			</span>
			</a>
			<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Une recherche">
		</div>

		<!-- Dropdown -->
	    <div style="margin-left: 22.7vw;margin-top: -0.1%;" id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
	    </div>
	</center>
</div>

<img class="img-responsive seconde-img-one" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/banner.png">

<p style="margin-top: 2vw;margin-bottom: 2vw;" class="text-one col-xs-10 col-lg-5 col-sm-8">ENSEMBLE NOUS SOMMES UNE FORCE</p>
<!-- SearchBar -->
<div class="col-xs-12">
	<center>
    	<div class="col-xs-7 col-lg-4 col-sm-7 section-two">
	    	<p class="text-two col-xs-10 col-sm-7">DES ALTERNATIVES</p>
	    	<br>
	    	<p class="text-two col-xs-10 col-sm-7">À CHAQUE</p>
	    	<br>
	    	<p class="text-two col-xs-10 col-sm-7">COIN DE RUE !!</p>
    	</div>
	</center>
    <img class="img-responsive" style="width: 100%;height: auto;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/photo-01.jpg">
</div>
<!-- Actualités -->
<div style="background-color: #ededed;" class="col-xs-12 text-center">
	<!-- <div>
		<h1 style="font-family:'Helvetica';color: #098048;">
			<i class="fa fa-newspaper-o" aria-hidden="true"></i>  Actualités
		</h1>
	</div> -->
	<?php

	$paramsArticles = [
		"canEdit"=>true, 
		"blockCms" => @$blockCmsById["alternatibaRe.welcome.blockarticles.0"],
		"idToPath" => "alternatibaRe.welcome.blockarticles.0",
		"page" => $page, 
		"limit" => 4,
		"el"=>$el
	];

	echo $this->renderPartial( "costum.views.tpls.blockCms.article.blockarticles", $paramsArticles ); 
	?>
</div>
<!-- Cartographie -->
<center>
	<img class="img-responsive illustration" style="" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/illustration-01.svg">
</center>

<div style="background-color:#098048;" class="col-xs-12 text-center">
	<h1 style="font-family:'Helvetica';color: white;">
		<i class="fa fa-map-marker" aria-hidden="true"></i>  Cartographie
	</h1>
	<center>
		<div style="z-index: 1;" class="col-xs-12 mapBackground no-padding" id="mapAlternatiba">
		</div>
	</center>
</div>
   
<!-- Agenda -->
<div style="background-color: #ededed;" class="col-xs-12 text-center">
	<!-- <div>
		<h1 style="font-family:'Helvetica';color: #098048;">
			<i class="fa fa-calendar" aria-hidden="true"></i>  Agenda
		</h1>
	</div> -->
	<?php

		$paramsEvents = [
			"canEdit"=>true,
			"blockCms" => @$blockCmsById["alternatibaRe.welcome.blockeventdescription.0"],
			"idToPath" => "alternatibaRe.welcome.blockeventdescription.0",
			"page" => $page, 
			"el"=>$el
		];

		echo $this->renderPartial( "costum.views.tpls.blockCms.events.blockeventdescription", $paramsEvents ); 
	?>
</div>

<!-- Partenaires -->
<h1 style="color: #098048;">
	Nos partenaires : 
</h1>

<div class="col-xs-12" style="display: block ruby">
	<a href="https://leanature.com/la-fondation/la-fondation-lea-nature-jardin-bio/">
		<img class="img-responsive" style="width:14%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/lea_nature_logo.png">
	</a>
	<a href="https://lagedefaire-lejournal.fr/">
	<img class="img-responsive" style="width:16%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/agedefaire.png">
	</a>
	<a href="https://www.larevuedurable.com/fr/">
	<img class="img-responsive" style="width:16%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/la_revue_durable.jpg">
	</a>
</div>

<div class="col-xs-12" style="display: block ruby;">
	<a href="https://transition-citoyenne.org/">
		<img class="img-responsive" style="width:12%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/transition.png">
	</a>
	<a href="https://reporterre.net/">
		<img class="img-responsive" style="width:16%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/Reporterre.png">
	</a>
	<a href="https://www.mediapart.fr/">
		<img class="img-responsive" style="width:9%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/mediapart.png">
	</a>
	<a href="https://www.fondationdefrance.org/fr">
	<img class="img-responsive" style="width:7%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/Fondation-de-France.png">
	</a>
</div>

<div class="col-xs-12" style="display: block ruby;">
	<a href="https://www.bastamag.net/">
		<img class="img-responsive" style="width:12%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/Bastamag.png">
	</a>
	<a href="https://europeanclimate.org/">
		<img class="img-responsive" style="width:16%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/European-Climate-Foundation.jpg">
	</a>
	<a href="http://www.fph.ch/index_fr.html">
		<img class="img-responsive" style="width:9%;" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/partenaires/Fondation-CLM2.jpg">
	</a>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

	setTitle("Alternatiba Réunion");

	var mapAlternatibaHome = {};
	var paramsMapAlternatiba  = {};

	initAlternatibaMapView();

	contextData = <?php echo json_encode($el); ?>;
    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName=<?php echo json_encode($el["name"]); ?>;
    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
});

$("#second-search-bar").off().on("keyup",
	function(e){ 
	    $("#input-search-map").val($("#second-search-bar").val());
	    $("#second-search-xs-bar").val($("#second-search-bar").val());
	    if(e.keyCode == 13){
            mylog.log("searchObject.text",searchObject.text);
            searchObject.text=$(this).val();
            searchObject.sourceKey=[costum.contextSlug];
            myScopes.type="open";
            myScopes.open={};
            startGlobalSearch(0, indexStepGS);
            $("#dropdown").css('display','block');
	    }  
	}
);

$("#second-search-xs-bar").off().on("keyup",
	function(e){ 
        $("#input-search-map").val($("#second-search-xs-bar").val());
        $("#second-search-bar").val($("#second-search-xs-bar").val());
	    if(e.keyCode == 13){
	        mylog.log("searchObject.text",searchObject.text);
	        searchObject.text=$(this).val();
	        searchObject.sourceKey=[costum.contextSlug];
	        myScopes.type="open";
	        myScopes.open={};
	        startGlobalSearch(0, indexStepGS);
	        $("#dropdown").css('display','block');            
	    }
	}
);

$("#second-search-bar-addon-alternatibaRe, #second-search-xs-bar-addon").off().on("click", 
	function(){
        $("#input-search-map").val($("#second-search-bar").val());
        mylog.log("searchObject.text",searchObject.text);
        searchObject.text=$("#second-search-bar").val();
        searchObject.sourceKey=[costum.contextSlug];
        myScopes.type="open";
        myScopes.open={};
        startGlobalSearch(0, indexStepGS);
        $("#dropdown").css('display','block');
	}
);	

function initAlternatibaMapView(){
	alternatiba.initScopeObj();

	paramsMapAlternatiba = {
		zoom : 4,
		container : "mapAlternatiba",
		activePopUp : true,
		tile : "maptiler",
		menuRight : false,
		mapOpt:{
			latLon : ["-21.115141", "55.536384"],
		}
	};

	mapAlternatibaHome = mapObj.init(paramsMapAlternatiba);
	dataSearchAlternatiba=searchInterface.constructObjectAndUrl();
	dataSearchAlternatiba.searchType = ["organizations","events"];
	dataSearchAlternatiba.indexStep=0;
	alternatiba.mapDefault();
	dataSearchAlternatiba.private = true;
	dataSearchAlternatiba.sourceKey = costum.contextSlug;
	ajaxPost(
		null,
		baseUrl+'/'+moduleId+'/search/globalautocomplete',
		dataSearchAlternatiba,
      	function(data){ 
	        mylog.log(">>> success autocomplete search !!!! ", data);
			if(!data){ 
				toastr.error(data.content); 
			}else{
				mapAlternatibaHome.addElts(data.results, true);
				setTimeout(function(){
					mapAlternatibaHome.map.panTo([-21.115141,55.536384]);
					mapAlternatibaHome.map.setZoom(10);
				},2000);
			}
      	},
      	function(data){
            mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);
			loadingData = false; 
        }
    );
}

var alternatiba={
	initScopeObj : function(){
		$(".content-input-scope-alternatiba").html(scopeObj);
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["RE"]
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}

				if (data.type == "events") {
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Consulter l'évènement";
					popup += '</div></a>';
				popup += '</div>';
				}

				if (data.type == "organizations") {
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Accéder a l'organization";
					popup += '</div></a>';
				popup += '</div>';
				}
			popup += '</div>';
			return popup;
		};
	}
}
</script>