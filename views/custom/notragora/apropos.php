<style type="text/css">
.apropos h2{
	text-align: -webkit-left;
	text-align: left;
    font-size: 25px;
    font-weight: 400;
}

.apropos h4{
	text-align: -webkit-left;
	text-align: left;
    font-size: 15px;
    font-weight: 400;
}

.apropos h3 {
    font-size: 17px;
    line-height: 1.2;
    font-weight: 400;
}

.apropos span{
	color: #8b91a0;
    font-size: 13px;
}

</style>
<div class="col-md-12 apropos">
	<h2 class="text-brown">Concepteurs « Hors Cadre » (40 personnes)</h2>
	<h4 class="text-black">Idée Originale et Conception :</h4>
	<span>Nicolas Huguenin, Thomas Dulu</span>
	<h4 class="text-black">Direction Editoriale :</h4>
	<span>Nicolas Huguenin, Directeur de l'association Hors Cadre</span>
	<h4 class="text-black">Developpement Informatique :</h4>
	<span><a href="https://communecter.org" target="_blank">@Communecter</a></span>
	<h2 class="text-brown">Production</h2>
	<h3 class="text-brown">Apports Collaboratifs </h3>
	<h4 class="text-black">Equipe salariée :</h4>
	<span>Nicolas Huguenin, Thomas Dulu, Bruno Duriez, Marc Le Piouff</span>
	<h4 class="text-black">Equipe associative :</h4> 
	<span>Sandino Peña Casanova, Anne-Sophie Lefebvre, Dominique Garret, <br/>Philippe Deschamps, Benjamin Massart, Delphine Bon, Bastien Sander, <br/>Emmanuel Dhullu, Sylvie Leducq</span>
	<h4 class="text-black">Ergonomie / Fonctionnalités :</h4>
	<span>Thomas Dulu, Bruno Duriez, Bastien Sander</span>
	<h4 class="text-black">Design / Graphisme :</h4>
	<span>Thomas Dulu, Bruno Duriez, Laure Colas, Anne-Sophie Lefebvre, <br/>François-Xavier Muselet, Benjamin Massart, Norah Benamara, Noémie Chantome,<br/> Sylvie Leducq, Cécile Foudi</span>
	<h4 class="text-black">Apports Artistiques et Techniques :</h4>
	<span>Bruno Duriez, Guillaume Grelardon, Claire Jeandroz, Marie-Noëlle Boutin, <br/>Thomas Bousquet, Cléa Coudsi, Olivier Malfait, Eric Herbin,<br/> Hugues Rougerie, Mathieu Dumont, François Engrand, François Fairon, Arnaud Feret,<br/> Jean Thomé, Thomas Bousquet, Fédéric Vermeersch</span>
	<h3 class="text-black">Apports Participatifs</h3>
	<h4 class="text-black">Gestion des Usages et Usagers :</h4>
	<span>Nicolas Huguenin, Bruno Duriez</span>
	<h4 class="text-black">Sensibilisation et formation :</h4>
	<span>Nicolas Huguenin, Bruno Duriez, Thomas Dulu, Cécile Foudi,<br/> Sarah Rousseau, Élise Chevalier</span>
	<h4 class="text-black">Community Management :</h4>
	<span>Nicolas Huguenin, Thomas Dulu, Bruno Duriez, Ysé Lenglet, <br/>Lilian Boute, Cécile Foudi, Benjamin Massart,<br/> Noémie Chantome, Élise Chevalier</span>
</div>