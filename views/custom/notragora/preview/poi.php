<style>
	#modal-preview-coop{
		overflow: auto;
	}
	#poi .title{
		padding-bottom: 15px !important;
    	border-bottom: 1px solid rgba(150,150,150, 0.3);
	}
	.short-description{
		font-size:20px;
		text-align: justify;
	}
	.description-preview{
		text-align: justify;
	}
	.description-preview.activeMarkdown p, .description-preview.activeMarkdown li{
		font-size: 14px !important;
	}
	.tags-content .badge.tags-poi-preview{
		padding: 3px 15px!important;
	    margin-right: 5px;
	}
	.tags-poi-preview{
		pointer-events : none; 
	}
	#ajax-modal.portfolio-modal.modal{
		z-index:1000000 !important;
	}
</style> 
<div class="margin-top-25 margin-bottom-50 col-xs-12">
	<div class="col-xs-12 no-padding">
		<div class="pull-right" style="margin-top:-15px;">
			<a href="#poi.<?= InflectorHelper::slugifyLikeJS($element["name"]) ?>" class="btn btn-default lbh" style="margin-right:5px;">
				<i class="fa fa-share" aria-hidden="true"></i> Aller sur la page
            </a>
			<button class="btn btn-default btn-close-preview">
				<i class="fa fa-times"></i>
			</button>
		</div>
		<?php


		if( $element["creator"] == Yii::app()->session["userId"] || 
				  Authorisation::canEditItem( Yii::app()->session["userId"], "poi", $id, @$element["parentType"], @$element["parentId"] ) ){ ?>
			
			<button class="btn btn-default pull-right margin-right-10 text-red deleteThisBtn" 
					data-type="poi" data-id="<?php echo $id ?>" style="margin-top:-15px;">
				<i class=" fa fa-trash"></i>
			</button>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-prod" data-type="poi" data-id="<?php echo $id ?>" 
			data-subtype="<?php echo $element["type"] ?>" style="margin-top:-15px;">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>
		<div id="poi" class="<?php echo @$element["type"]; ?>">
			<h2 class="col-xs-12 title text-dark no-padding"><?php echo $element["name"] ?></h2>
			<?php if(!empty($this->costum["slug"])) { ?>
				<div class="col-xs-12 poiCategory no-padding">	
					<span>Type : </span> 
					<span style="font-weight:bold;"><?php echo Yii::t("category",$element["type"]) ?></span>
				</div>
				<div class="col-xs-12 auhtor-poi no-padding margin-bottom-10">
					<?php if(@$element["parent"]["name"]){ ?>
						<span class="font-montserrat col-xs-12">
							<i class="fa fa-angle-down"></i> <i class="fa fa-address-card"></i> 
							<?php echo Yii::t("common", "{what} publiée par {who}", 
								array("{what}"=>Yii::t("common","Production"),
									"{who}"=>"<a href='#page.type.".@$element["parentType"].".id.".@$element["parentId"]."' class='lbh'>".
												@$element["parent"]["name"].
											"</a>")
								);
							 ?> 
						</span>
					<?php }else if(@$element["parent"]){ ?>
						<span class="font-montserrat letter-blue">
							<?php echo Yii::t("common", "{what} publiée par ",array("{what}"=>ucfirst(Yii::t("common","Production"))));  ?>
						</span>	
						<?php foreach($element["parent"] as $key => $v){ 
								$imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?> 
								<a href='#page.type.<?php echo $v["type"] ?>.id.<?php echo $key ?>' class='lbh'>
									<img src='<?php echo $imgPath ?>' class='img-circle' width='25' height='25'/>
									<?php echo $v["name"] ?>
								</a> 
						
					<?php	}
					} ?>
				</div>
			<?php }  ?>
			<?php if(@$element["shortDescription"]){ ?>
			<span class="col-xs-12 short-description margin-bottom-15 no-padding activeMarkdown"><?php echo $element["shortDescription"] ?></span>
			<?php } ?>
			<?php 	
			// var_dump($element["images"]);
			if(!isset($element["medias"]) && isset($element["urls"][0]) && isset($element["profilMediumImageUrl"])){
				$element["medias"]= array(
    				array(
    					"content" => array(
    						"type" => "video_link",
    						"url" => $element["urls"][0],
    						"image" => $element["profilMediumImageUrl"],
    						"imageSize" => "small",
    						"videoLink" => str_replace ( "vimeo.com" , "player.vimeo.com/video",$element["urls"][0])
    					)
    				)
				);	
    				// $element[]
				// array_push(,$mediaObj);
			}
			// var_dump($element["medias"]);
			echo $this->renderPartial('../pod/sliderMedia',
								array(
									  "medias"=>$element["medias"] ?? [],
									  "images" => (isset($element["medias"])) ? [] : ($element["images"] ?? [])
									//   "images" => @$element["images"],
									  ) ); 
									    
				if(!empty($element["files"])){ 
					echo $this->renderPartial('../pod/docsList',
								array("documents"=>$element["files"], "edit"=>false) );
					?> 
				<!--<div class="col-xs-12 content-files-doc margin-top-10 margin-bottom-10-10">
				<?php /* foreach($element["files"] as $k => $v){ ?>
						<div class='col-xs-12 padding-5'>
							<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files col-xs-6 col-xs-offset-3 text-center bg-orange btn"><?php echo $v["name"] ?></a>
						</div>
				<?php } */ ?>-->
				<?php } 
				?>
			<?php if(@$element["type"]=="article"){
				if(isset($element["tags"])){ ?>
					<div class="tags-content text-center col-xs-12 no-padding margin-bottom-10">
					<?php foreach($element["tags"] as $v){ 
						
						echo "<a href='javascript:;' data-tags = \"".$v."\" class='badge tags-poi-preview'>#".$v."</a>";
					} ?>
					</div>
				<?php }
					echo $this->renderPartial('../poi/preview/article',
								array("params"=>$element) );
				}
				else if(@$element["type"]=="measure"){
					echo $this->renderPartial('../poi/preview/measure',
								array("params"=>$element) );
				}else if(@$element["type"]=="forum"){ ?>
					<div id="commentElement-preview" class="col-xs-12 no-padding"></div>
			<?php	
			} else {
				if(isset($element["tags"])){ ?>
					<div class="tags-content text-center col-xs-12 no-padding margin-bottom-10">
					<?php foreach($element["tags"] as $v){ 
						echo "<a href='javascript:;' data-tags = \"".$v."\" class='badge tags-poi-preview'>".$v."</a>";
					} ?>
					</div>
				<?php }
				if(isset($element["description"])){ ?>
					<div id="description" class="description-preview col-xs-12 no-padding activeMarkdown"><?php echo $element["description"] ?></div>
				<?php } 
			}
		
				
			?>

		</div>
		
        <!--<a href="javascript:;" onclick="dySObj.openSurvey('octosource','json')" class="btn btn-primary col-xs-12"  style="width:100%">C'est parti <i class="fa fa-arrow-circle-right fa-2x "></i></a>-->
	</div>
</div>

<script type="text/javascript">

	var poiAlone=<?php echo json_encode($element); ?>;

	jQuery(document).ready(function() {	
	
		$(".btn-edit-prod").off().on("click",function(){
			
			var dyFProd = jQuery.extend(true, {}, costum.typeObj.poi.dynFormCostum);
			dyFProd.prepData=function(data){
		        // alert("here");
    			mylog.log("prepData poi production",data);
    			var newData=jQuery.extend(true, {}, data);
    			var listObj=costum.lists;
				mylog.log("listObj",listObj);
    		    // delete listObj.network;
    		    $.each(listObj, function(e, v){
    		        constructDataForEdit=[];
    		        $.each(v, function(i, tag){
    		            if($.inArray(tag, newData.map.tags) >=0){
							// alert(tag);
    		                constructDataForEdit.push(tag);
    		             
    		                newData.map.tags.splice(newData.map.tags.indexOf(tag),1);
    		            }
    		        });
    		        newData.map[e]=constructDataForEdit;
    		    });
    			return newData;
    		};
			dyFProd.afterBuild=function(dataAB){
				costum[costum.slug].poi.afterBuild(dataAB);
				urlCtrl.closePreview();
			};

    		dyFObj.editElement("poi",poiAlone['_id']['$id'],null,dyFProd);

		});
		setTitle("", "", poiAlone.name);
			
		poiAlone["typePoi"] = poiAlone.type;
		poiAlone["type"] = "poi";
		poiAlone["typeSig"] = "poi";
		mylog.log("preview poiAlone", poiAlone);
		poiAlone["id"] = poiAlone['_id']['$id'];
		//if(typeof poi != "undefined" && notNull(poi) && typeof poi.filters != "undefined")
		if(typeof tradCategory[poiAlone.typePoi] != "undefined")
			tradCat=tradCategory[poiAlone.typePoi];
		else if(typeof poi.filters != "undefined" && typeof poi.filters[poiAlone.typePoi] != "undefined" && typeof poi.filters[poiAlone.typePoi].label != "undefined"){
			if(typeof tradCategory[poi.filters[poiAlone.typePoi].label] != "undefined")
				tradCat=tradCategory[poi.filters[poiAlone.typePoi].label];
			else
				tradCat=poi.filters[poiAlone.typePoi].label;
		}else
			tradCat=poiAlone.typePoi;
		$(".poiCategory .category").html(tradCat);
		//if($.inArray(poiAlone.typePoi, ["article", "measure", "forum"])<0){
		//	var html = directory.preview(poiAlone);
		  //	$("#poi").html(html);
		//}
	  	directory.bindBtnElement();
	  	if($("#poi #container-element-accordeon").length > 0){
	  		params={
	  			"images": [],
	  			"medias": []
	  		};
	  		if(typeof poiAlone.images != "undefined")
	  			params.images=poiAlone.images;
	  		if(typeof poiAlone.medias != "undefined")
	  			params.medias=poiAlone.medias;
	  		ajaxPost("#poi #container-element-accordeon", baseUrl+'/'+moduleId+'/pod/slidermedia', params, function(){});
	  	}
		$.each($(".activeMarkdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html()); 
            $(v).html(descHtml);
        }); 
	  	if($("#commentElement-preview").length>0){
	  	 	coInterface.showLoader("#commentElement-preview", "En cours de chargement");
	  	 	getAjax(null,baseUrl+"/"+moduleId+"/comment/index/type/poi/id/"+poiAlone['_id']['$id'],
				function(data, id){
					$("#commentElement-preview").html(data);
				},"html");
	  	}
	  	$("#modal-preview-coop .btn-close-preview, .deleteThisBtn").click(function(){
			setTitle("", "", "");
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
		});	
		if(typeof poiAlone["category"] != "undefined" &&  poiAlone["category"] == "fullScreen"){
			$("#modal-preview-coop").css("left","0%");
			$("#poi").addClass("col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 fullScreen")
		}else{
			$("#modal-preview-coop").css("left","40%");
			if($("#poi").hasClass("col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 fullScreen"))
				$("#poi").removeClass("col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 fullScreen")
		}	
	  	//poi["sections"] = <?php echo json_encode(CO2::getContextList("poi")); ?>

	  	//Sig.showMapElements(Sig.map, new Array(poiAlone));
	  	//mapCO.addElts(new Array(poiAlone));

		if(costum && costum.editMode){
			$(".btn-share-link-preview").css({ display:"none" })
		}
		$(".tags-poi-preview").click(function(){			
			var tagsF =  JSON.parse(localStorage.getItem("fitersTagspoi"+page));
			var tagsFilterActif = {};
			if(tagsF){	
				tagsFilterActif = tagsF;
				if(typeof tagsF[$(this).data("tags")] == "undefined"){
					tagsFilterActif[$(this).data("tags")] =  $(this).data("tags")
				}		
			}else{
				tagsFilterActif[$(this).data("tags")] =  $(this).data("tags")
			}
			$("#modal-preview-coop .btn-close-preview").trigger("click")
			localStorage.setItem("fitersTagspoi"+page, JSON.stringify(tagsFilterActif));
			urlCtrl.loadByHash(location.hash);			
		})
	});
</script>