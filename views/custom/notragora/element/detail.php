<?php
$isGroup = false; 
if(!empty($element["categoryNA"]) && in_array("group", $element["categoryNA"])){
	$isGroup = true;
}
$type=$element["collection"];
$id=(string)$element["_id"];
$canEdit=$edit;
?>

<div id="section-home" class="col-md-12">
	<div class="col-xs-12 text-center">
		<h2 class="section-title">
			<span class="sec-title">Présentation</span><br>
			<i class="fa fa-angle-down"></i>
		</h2>
	</div>
	<div class="col-xs-8 col-xs-offset-2  no-padding item-desc" id="descriptionAbout"><?php echo !empty($element['description']) ? @$element['description'] : "<span class='padding-10'><center><i>- Pas de présentation -</center></i></span>"; ?></div>
	<div class="col-xs-8 col-xs-offset-2  no-padding item-desc hidden" id="descriptionMarkdown"><?php echo !empty($element['description']) ? @$element['description'] : ""; ?></div>
				
</div>


<script type="text/javascript">


	jQuery(document).ready(function() {
		
		
		if(canEdit == true)
			descHtmlToMarkdown();

		var descHtml = "<span class='padding-10'><center><i>- Pas de présentation -</center></i></span>";
		if($("#descriptionAbout").length > 0){
			descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
		}
		$("#descriptionAbout").html(descHtml);
		directory.bindBtnElement();

		$('#btn-edit-user').off().click(function(){
			var form = {
				saveUrl : baseUrl+"/"+moduleId+"/element/updateblock/",
				dynForm : {
					jsonSchema : {
						title : trad["Update general information"],
						icon : "fa-key",
						type: "object",
						onLoads : {
							initUpdateInfo : function(){
								mylog.log("initUpdateInfo");
								$(".emailOptionneltext").slideToggle();
								$("#ajax-modal .modal-header").removeClass("bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");

								dataHelper.activateMarkdown("#ajaxFormModal #description");
								$("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
											  					  .addClass("bg-dark");
							}
						},
						beforeSave : function(){
							mylog.log("beforeSave");
							removeFieldUpdateDynForm(contextData.type);
					    },
						afterSave : function(data){
							mylog.dir(data);
							// if(data.result&& data.resultGoods && data.resultGoods.result){

							// 	if(typeof data.resultGoods.values.name != "undefined"){
							// 		contextData.name = data.resultGoods.values.name;
							// 		$(".element-name").html(contextData.name);
							// 	}
							// }
							dyFObj.closeForm();
							urlCtrl.loadByHash( location.hash );
							//changeHiddenFields();
						},
						properties : {
							block : dyFInputs.inputHidden(),
							name : dyFInputs.name(contextData.type),
							similarLink : dyFInputs.similarLink,
							typeElement : dyFInputs.inputHidden(),
							email : dyFInputs.email(tradDynForm.mainemail, tradDynForm.mainemail, { email: true, required : true }),
							description : dyFInputs.textarea(tradDynForm["longDescription"], "..."),
							isUpdate : dyFInputs.inputHidden(true)
						}
					}
				}
			};
			
			var dataUpdate = {
				block : "info",
		        id : contextData.id,
		        typeElement : contextData.type,
		        name : contextData.name
			};

			// if(contextData.type == typeObj.person.col ){
			// 	if(notNull(contextData.username) && contextData.username.length > 0)
			// 		dataUpdate.username = contextData.username;
			// 	if(notEmpty(contextData.birthDate))
			// 		dataUpdate.birthDate = moment(contextData.birthDate).local().format("DD/MM/YYYY");
			// }
			
			//mylog.log("ORGA ", contextData.type, typeObj.organization.col, dataUpdate.type);
			
			

			if($.inArray(contextData.type, [typeObj.organization.col, typeObj.person.col, typeObj.project.col, typeObj.event.col]) > -1 ){
				//mylog.log("test email", contextData, contextData.email);
				if(notEmpty(contextData.email)) {
					//mylog.log("test email2", contextData, contextData.email);
					dataUpdate.email = contextData.email;
				}
			}

			if(notEmpty(contextData.description)) {
				//mylog.log("test description", contextData, contextData.description);
				//dataUpdate.description = dataHelper.htmlToMarkdown($(".contentInformation #descriptionAbout").html());
				dataUpdate.description = contextData.description;
			}
			
			//mylog.log("dataUpdate", dataUpdate);
			dyFObj.openForm(form, "initUpdateInfo", dataUpdate);
		});

		$('#follows').click(function(){
			var id = $(this).data("id");
			var isco = $(this).data("isco");
			if(isco == false){
				links.connectAjax(contextData.type,contextData.id,userId,'citoyens','members', null, function(){
					$('#follows').html("Désinscrire");
					$('#follows').data("isco", true);
				});
			}else{
				links.disconnectAjax(contextData.type,contextData.id ,userId,'citoyens','members', null, function(){
					$('#follows').html("S'inscrire");
					$('#follows').data("isco", false);
				});
			}
		});

		// $(".deleteThisBtn").off().on("click",function (){
		// 	mylog.log("deleteThisBtn click");
	    //     $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
	    //     var btnClick = $(this);
	    //     var id = $(this).data("id");
	    //     var type = $(this).data("type");
	    //     var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;

	    //     bootbox.confirm("Etes vous sur de vouloir supprimer cet élément ?",
        // 	function(result)
        // 	{
		// 		if (!result) {
		// 			btnClick.empty().html('<i class="fa fa-trash"></i>');
		// 			return;
		// 		} else {
		// 			ajaxPost(
		// 				null,
		// 				urlToSend,
		// 				{},
		// 				function(data){ 
		// 					if ( data && data.result ) {
		// 			        	toastr.info("Cet élément a été effacé avec succès.");
		// 			        	$("#"+type+id).remove();
		// 			        	loadByHash("#"+parentType.substr(0, parentType.length - 1)+".detail.id."+parentId);
		// 			        } else {
		// 			           toastr.error("Une erreur est survenue : ".data.msg);
		// 			        }
		// 				}
		// 			);
		// 		}
		// 	});

		// });
		// $(".editThisBtn").off().on("click",function (){
	    //     $(this).empty(t).html('<i class="fa fa-spinner fa-spin"></i>');
	    //     var btnClick = $(this);
	    //     var id = $(this).data("id");
	    //     var type = $(this).data("type");
	    //     elementLib.editElement(type,id);
		// });

		

	});
</script>

