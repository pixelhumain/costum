<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    '/plugins/spectrum-colorpicker2/spectrum.min.js',
      '/plugins/spectrum-colorpicker2/spectrum.min.css'), null);
?>
<style>
    .html-editor-hexagone{
        display: flex;
        flex-direction: row;
        width: 100%;
        height: <?php echo (isset($_POST["preview"]) && ($_POST["preview"] == "true" || $_POST["preview"] == true)) ? "calc(90vh - 150px)" : "calc(100vh - 70px)"; ?>;
        margin-top: 20px;
    }
    .hexagone-editor-block{
        width: 300px;
        height: 100%;
        color: white;
        background-color:#3A3A3A;
    }
    .hexagone-editor-block-title, .hexagone-editor-style-title{
        width: 100%;
        text-align: center;
        border-bottom: 1px solid white;
        background-color: #2C2C2C;
        padding: 15px;
    }
    .hexagone-editor-block-title h3, .hexagone-editor-style-title h3 {
        margin: 0;
    }
    #hexagone-inputs{
        padding: 10px;
    }
    #hexagone-inputs input{
        background: #3a3a3a00;
        color: white;
    }
    #hexagone-inputs select {
        background: #3a3a3a00 !important;
        color: white !important;
    }
    #hexagone-inputs option {
        background: #4d4e4e;
    }

    #hexagone-inputs .units-form select {
        border: none;
        border-bottom: 1px solid #ccc;
        box-shadow: none;
        margin-bottom: 5px;
        border-radius: 0;
        padding: 0;
    }
    .hexagone-editor-style{
        width: 300px;
        height: 100%;
        color: white;
        background-color: #3A3A3A;
    }
    .hexagone-editor-content{ 
        width: calc(100% - 640px);
        overflow-y: auto;
        margin: 0 auto;
        padding-top: 20px;
    }
    .hexagone-editor-block-items{
        padding: 20px;
    }
    .hexagone-editor-block-item{
        height: 100px;
        background-color: #313131;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        color: white;
        margin-bottom: 10px;
        padding: 10px;
        cursor: grabbing;
        white-space: nowrap;
        min-width: 100px;
    }
    .hexagone-editor-block-item img {
        width: 40px;
        height: 40px;
    }
    .d-none {
        display: none !important;
    }
    .hexagone-editor-dropzone {
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: rgb(24, 144, 255, .8);
        opacity: 0;
        transition: .2s;
    }

    .hexagone-editor-dropzone img {
        height: 80%;
        max-height: 90px;
        opacity: 0;
    }

    .hexagone-editor-dropzone.dragover {
        opacity: 1;
    }

    .hexagone-editor-dropzone.dragover img {
        opacity: 1;
    }

    .hexagone-editor-dropzone-container {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 88888;
    }

    .hexagone-editor-dropzone-page {
        width: 100%;
        height: 60px;
        position: relative;
        opacity: .2;
    }

    .hexagone-editor-dropzone-page.dragover {
        height: 60px;
    }

    .hexagone-block-section {
        min-height: 100px;
        position: relative;
    }
    .hexagone-actions-wrapper {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 1.5px solid #1890FF;
        pointer-events: none;
        background-color: rgb(24, 144, 255, .15);
        display: block;
        z-index: 9999;
    }
    .hexagone-block-action-label {
        top: -20px;
        position: absolute;
        color: rgb(255, 255, 255);
        z-index: 9999999;
        line-height: 18px;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
        padding: 0px 10px;
        font-size: 16px;
        font-weight: bold;
        padding: 0px 10px;
        background-color: #1890FF;
    }
    .hexagone-block-action {
        align-items: center;
        height: 30px;
        width: fit-content;
        pointer-events: all !important;
        position: absolute;
        bottom: -30px;
        left: 0;
        z-index: 9999999;
        background-color: #1890FF;
        display: flex;
    }
    .hexagone-block-action-list ul {
        margin: 0;
        padding: 0;
        display: flex;
    }
    .hexagone-block-action-list ul li {
        list-style: none;
        display: flex;
        border-right: 1px solid #B1B1B1;
        cursor: pointer;
        overflow: hidden;
        max-width: 35px;
        transition: .4s;
        color: white;
    }
    .hexagone-block-action-list ul li span:first-child {
        width: 35px;
        text-align: center;
    }
    .hexagone-block-action-list ul li span {
        flex: none;
    }
    .block-actions-wrapper .hexagone-block-action-list li[data-action="deselect"], .block-actions-wrapper .cmsbuilder-block-action-list li[data-action="questionIA"] {
        display: none !important;
    }
    .block-actions-wrapper.selected .hexagone-block-action-list li[data-action="deselect"], .block-actions-wrapper.selected .cmsbuilder-block-action-list li[data-action="questionIA"] {
        display: flex !important;
    }

    .hexagone-block img{
        width: 100%;
        height: 100%;
    }
    #btn-save-container {
        padding-top: 20px;
        padding-right: 20px;
        text-align: right;
    }
    <?php if(isset($_POST["preview"]) && ($_POST["preview"] == "true" || $_POST["preview"] == true)){?>
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
    <?php } ?>
</style>

<div class="col-xs-12 padding-10">
    <span class="pull-left">
        <h4>Interface d'édition <?= isset($_POST["criteria"]) ? "de ".$_POST["criteria"] : (isset($_POST["title"]) ? "de ".$_POST["title"] : "") ?></h4>
    </span>
    <?php if(isset($_POST["preview"]) && ($_POST["preview"] == "true" || $_POST["preview"] == true)){?>
    <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <?php } ?>
</div> 
<div class="html-editor-hexagone">
    <div class="hexagone-editor-block">
        <div class="hexagone-editor-block-title">
            <h3>Element</h3>
        </div>
        <div class="hexagone-editor-block-items">
            <div class="hexagone-editor-block-item ui-draggable ui-draggable-handle" data-name="section" draggable="true">
                <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl() ?>/cmsBuilder/img/blocks/section.png" alt="" draggable="false">
                <span>Section</span>
            </div>
            <div class="hexagone-editor-block-item ui-draggable ui-draggable-handle" data-name="texte" draggable="true">
                <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl() ?>/cmsBuilder/img/blocks/text.png" alt="" draggable="false">
                <span>Texte</span>
            </div>
            <div class="hexagone-editor-block-item ui-draggable ui-draggable-handle" data-name="titre" draggable="true">
                <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl() ?>/cmsBuilder/img/blocks/title.png" alt="" draggable="false">
                <span>Titre</span>
            </div>
            <div class="hexagone-editor-block-item ui-draggable ui-draggable-handle" data-name="image" draggable="true">
                <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl() ?>/cmsBuilder/img/blocks/image.png" alt="" draggable="false">
                <span>Photo</span>
            </div>
        </div>
    </div>

    <div class="hexagone-editor-content">
        <?php
            if(isset($_POST["data"]) && $_POST["data"] != ""){
                echo $_POST["data"];
            }
        ?>
    </div>
    <div class="hexagone-editor-style">
        <div class="hexagone-editor-style-title">
            <h3>Style</h3>
        </div>
        <div class="row" id="hexagone-inputs">
        </div>
    </div>

</div>

<div id="btn-save-container">
    <button type="button" class="btn btn-primary btn-save-hexagone-edition"><i class="fa fa-save"></i> Enregistrer</button>
</div>
<script>
    var dataPosted = <?= json_encode($_POST) ?>;
    var hexagoneDetailEditor = {
        dragstart: false,
        criteriaId: null,
        init: function(){
            this.initDraggable();
            this.editor.bindMouseOver();
            this.editor.save();
        },
        initDraggable: function(){
            var autoScroller = hexagoneDetailEditor.helpers.getAutoScroller(".hexagone-editor-content")
            $('.hexagone-editor-block-item').draggable({
                helper: 'clone',
                appendTo: 'body',
                zIndex: 1000,
                scroll: true,
                cursor: 'move',
                start: function(event, ui){
                    hexagoneDetailEditor.dragstart = true;
                    var name = $(this).data("name");
                    if(name == "section"){
                        hexagoneDetailEditor.dropzone.addPage();
                    }else{
                        $(".hexagone-editor-block-droppable").each(function() {
                            var $element = $(this);
                            if (hexagoneDetailEditor.helpers.isInViewport($element[0],0)) {
                                $element.addClass("dragstart");
                            }
                        });
                        hexagoneDetailEditor.dropzone.addContainer()
                    }
                    hexagoneDetailEditor.dropzone.launch();
                },
                stop: function(event, ui){
                    hexagoneDetailEditor.dragstart = false
                    //remove space and border to column block on drag end
                    $(".hexagone-editor-block-droppable").removeClass("dragstart")
                    $(".hexagone-editor-dropzone").remove()
                
                    //cmsConstructor.builder.events.dropzone.remove()
                    autoScroller.stop()
                },
                drag: function(e){
                    autoScroller.start(e)
                }
            });
        },
        dropzone : {
            addPage : function(){
                if ($(".hexagone-editor-content > .hexagone-block[data-type='section']").length < 1) {
                    // No 'section' blocks available, add dropzone
                    $(".hexagone-editor-content").append(`
                        <div class="hexagone-editor-dropzone hexagone-editor-dropzone-page" data-position="in" style="height:${$(".hexagone-editor-content").height()}px !important;">
                            <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                        </div>
                    `);
                } else {
                    //add section dropzone
                    $(".hexagone-editor-content > .hexagone-block[data-type='section']").each(function(index){
                        var getDropzone = function(id, kunik, position){
                            return (`
                                <div class="hexagone-editor-dropzone hexagone-editor-dropzone-page" data-position="${position}" data-id="${id}" data-kunik="${kunik}">
                                    <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                                </div>
                            `)
                        }

                        if (hexagoneDetailEditor.helpers.isInViewport(this,1000)){
                            $(this).before(getDropzone($(this).data("id"), $(this).data("kunik"), "before"))
                            if((index+1) === $(".hexagone-block[data-type='section']").length)
                                $(this).after(getDropzone($(this).data("id"), $(this).data("kunik"), "after"))
                        }
                    })
                }
            },
            addContainer:function(){
                // $(".block-cms-dropzone").remove()
                $(".hexagone-editor-dropzone").remove();
                //add column dropzone
                $(".hexagone-block:not([data-blockType='element'])").each(function(){
                    var dropzoneContainer = $(`
                        <div class="hexagone-editor-dropzone hexagone-editor-dropzone-container" data-kunik="${$(this).data("kunik")}">
                            <img src="${assetPath}/cmsBuilder/img/drop-block.png"/>
                        </div>
                    `)

                    $(this).prepend(dropzoneContainer)
                })

            },
            launch : function(){
                $(".hexagone-editor-block-droppable").droppable({
                    greedy:true,
                    over:function(e){
                        $(".hexagone-editor-dropzone").removeClass("dragover")
                        $(this).find("> .hexagone-editor-dropzone").addClass("dragover")
                    },
                    out:function(){
                        $(this).find("> .hexagone-editor-dropzone").removeClass("dragover")
                    },
                    drop:function(e, ui){
                        e.stopPropagation()
                        
                        var blockName = $(ui.draggable).data("name"),
                            parentType = $(e.target).data("blocktype");
                        if(parentType == "section" && blockName == "section"){
                            bootbox.alert({
                                message: `<br>
                                <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                <p class="padding-left-20">${tradCms.alertSecinSec}</p>
                                </div>`
                                ,
                                size: 'medium'
                            });
                        }else if(parentType == "column" && blockName == "section"){
                            bootbox.alert({
                                message: `<br>
                                <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
                                <p class="padding-left-20">${tradCms.alertSecinColumn}</p>
                                </div>`
                                ,
                                size: 'medium'
                            });
                        }else{
                            var parent = {
                                id:$(e.target).data("id"),
                                kunik:$(e.target).data("kunik")
                            }
                            var block = {
                                name: blockName,
                                subtype: $(ui.draggable).data("subtype"),
                                path: $(ui.draggable).data("path")
                            }
                            
                            hexagoneDetailEditor.dropzone.insertBlock(parent, block);
                        }
                    }
                })
                $(".hexagone-editor-dropzone-page").droppable({
                    greedy:true,
                    hoverClass:"dragover",
                    drop:function(){
                        hexagoneDetailEditor.dropzone.insertSection($(this).data("position"), $(this))
                    }
                })
            },
            insertSection: function(position, element){
                switch (position){
                    case "in":
                        $(".hexagone-editor-content").append(`
                            <div class="hexagone-block hexagone-block-section hexagone-editor-block-droppable" data-kunik="section-${Date.now()}" data-type="section">
                                <div class="hexagone-editor-dropzone" data-type="section"></div>
                            </div>
                        `)
                        break;
                    case "before":
                        $(element).before(`
                            <div class="hexagone-block hexagone-block-section hexagone-editor-block-droppable" data-kunik="section-${Date.now()}" data-type="section">
                                <div class="hexagone-editor-dropzone" data-type="section"></div>
                            </div>
                        `)
                        break;
                    case "after":
                        $(element).after(`
                            <div class="hexagone-block hexagone-block-section hexagone-editor-block-droppable" data-kunik="section-${Date.now()}" data-type="section">
                                <div class="hexagone-editor-dropzone" data-type="section"></div>
                            </div>
                        `)
                        break;
                }
                hexagoneDetailEditor.editor.bindMouseOver();
            },
            insertBlock: function(parent, block){
                let kunik = `${block.name}-${Date.now()}`;
                if(block.name == "texte"){
                    var blockHtml = `
                        <div class="hexagone-block hexagone-block-texte" data-kunik="${kunik}" data-type="texte" data-blockType="texte">
                            <div class="hexagone-texte editable sp-text" data-field="text" data-kunik="${kunik}">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.
                            </div>
                        </div>
                    `
                }else if(block.name == "titre"){
                    var blockHtml = `
                        <div class="hexagone-block hexagone-block-titre" data-kunik="${kunik}" data-type="titre" data-blockType="titre">
                            <h3>Ajouter un titre</h3>
                        </div>
                    `
                }else if(block.name == "image"){
                    var blockHtml = `
                        <div class="hexagone-block hexagone-block-image" data-kunik="${kunik}" data-type="image" data-blockType="image">
                            <img src="${assetPath}/cmsBuilder/img/blocks/image.png" alt="">
                        </div>
                    `
                }
                $(".hexagone-block[data-kunik='"+parent.kunik+"']").append(blockHtml)
                hexagoneDetailEditor.editor.bindMouseOver();
            }
        },
        editor: {
            init: function(){

            },
            clickBlock : function(){
                $(".hexagone-block").on("click", function(e){
                    e.stopImmediatePropagation();
                    var $block = $(this);
                    var kunik = $block.data("kunik");
                    var type = $block.data("type");
                });
            },
            image: function(kunik){
                let block = $(".hexagone-block[data-kunik='"+kunik+"']");
                var myInputs = {
                    container: "#hexagone-inputs",
                    inputs: [
                        {
                            type: "inputFileImage",
                            options: {
                                name: "image",
                                label : tradCms.image || "Image",
                                collection: "documents",
                                class: "imageUploader col-sm-12",
                                endPoint: "/subKey/hexagondetail",
                                domElement: "image",
                                filetype: ["jpeg", "jpg", "gif", "png"],
                                defaultValue: block.find("img").attr("src")
                            }
                        },
                        {
                            type: "colorPicker",
                            options: {
                                name: "background",
                                class: "col-sm-12",
                                label: "Couleur du fond",
                                defaultValue: block.css("background")
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "padding",
                                label: tradCms.padding || "Padding",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "padding-top" : block.css("padding-top"),
                                    "padding-left" : block.css("padding-left"),
                                    "padding-bottom" : block.css("padding-bottom"),
                                    "padding-right" : block.css("padding-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "padding-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "margin",
                                label: tradCms.margin || "Margin",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "margin-top" : block.css("margin-top"),
                                    "margin-left" : block.css("margin-left"),
                                    "margin-bottom" : block.css("margin-bottom"),
                                    "margin-right" : block.css("margin-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "margin-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "border-radius",
                                label: tradCms.borderRadius || "Border radius",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "border-bottom-left-radius" : block.css("border-bottom-left-radius"),
                                    "border-bottom-right-radius" : block.css("border-bottom-right-radius"),
                                    "border-top-left-radius" : block.css("border-top-left-radius"),
                                    "border-top-right-radius" : block.css("border-top-right-radius"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "border-bottom-left-radius",
                                        label: tradCms.bottomLeft || "Bottom left",
                                    },
                                    {
                                        type: "number",
                                        name:"border-bottom-right-radius",
                                        label: tradCms.bottomRight || "Bottom right",
                                    },
                                    {
                                        type: "number",
                                        name:"border-top-left-radius",
                                        label: tradCms.topLeft || "Top left",
                                    },
                                    {
                                        type: "number",
                                        name:"border-top-right-radius",
                                        label: tradCms.topRight || "Top right",
                                    },
                                ]
                            }
                        },
                        
                    ],
                    onchange: function(name, value, payload){
                        if(name == "image"){
                            if(!value.includes("images/thumbnail-default.jpg")){
                                $(".hexagone-block[data-kunik='"+kunik+"'] > img").attr("src", value);
                            }
                        }
                        if(typeof value == "string"){
                            $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                [name]: value
                            });
                        }
                        if(typeof value == "object" && !Array.isArray(value)){
                            $.each(value, function(k, v){
                                if(v != ""){
                                    if(name == "border-radius"){
                                        $(".hexagone-block[data-kunik='"+kunik+"'] img").css({
                                            [k]: v
                                        });
                                    }else{
                                        $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                            [k]: v
                                        });
                                    }
                                }
                            })
                        }
                    }
                }

                new CoInput(myInputs);
            },
            section: function(kunik){
                let block = $(".hexagone-block[data-kunik='"+kunik+"']");
                var myInputs = {
                    container: "#hexagone-inputs",
                    inputs: [
                        {
                            type: "colorPicker",
                            options: {
                                name: "background",
                                class: "col-sm-12",
                                label: "Couleur du fond",
                                defaultValue: block.css("background")
                            }
                        },
                        {
                            type: "inputNumberRange",
                            options : {
                                name: "width",
                                label: tradCms.width || "Width",
                                defaultValue: block.css("width"),
                                class: "col-sm-12",
                                minRange: 0,
                                maxRange: window.innerWidth,
                                units: ["px", "%", "vw"],
                                filterValue: function(value, unit = ""){
                                    if(isNaN(value))
                                        return value;
                                    else if (value == "")
                                        return "";
                                    else
                                        return value+""+unit;
                                },
                            }
                        },
                        {
                            type: "inputNumberRange",
                            options : {
                                name: "height",
                                label: tradCms.height || "Height",
                                defaultValue: block.css("height"),
                                class: "col-sm-12",
                                minRange: 0,
                                maxRange: window.innerHeight,
                                units: ["px", "%", "vh"],
                                filterValue: function(value, unit = ""){
                                    if(isNaN(value))
                                        return value;
                                    else if (value == "")
                                        return "";
                                    else
                                        return value+""+unit;
                                },
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "padding",
                                label: tradCms.padding || "Padding",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "padding-top" : block.css("padding-top"),
                                    "padding-left" : block.css("padding-left"),
                                    "padding-bottom" : block.css("padding-bottom"),
                                    "padding-right" : block.css("padding-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "padding-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "margin",
                                label: tradCms.margin || "Margin",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "margin-top" : block.css("margin-top"),
                                    "margin-left" : block.css("margin-left"),
                                    "margin-bottom" : block.css("margin-bottom"),
                                    "margin-right" : block.css("margin-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "margin-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },{
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "border-radius",
                                label: tradCms.borderRadius || "Border radius",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "border-bottom-left-radius" : block.css("border-bottom-left-radius"),
                                    "border-bottom-right-radius" : block.css("border-bottom-right-radius"),
                                    "border-top-left-radius" : block.css("border-top-left-radius"),
                                    "border-top-right-radius" : block.css("border-top-right-radius"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "border-bottom-left-radius",
                                        label: tradCms.bottomLeft || "Bottom left",
                                    },
                                    {
                                        type: "number",
                                        name:"border-bottom-right-radius",
                                        label: tradCms.bottomRight || "Bottom right",
                                    },
                                    {
                                        type: "number",
                                        name:"border-top-left-radius",
                                        label: tradCms.topLeft || "Top left",
                                    },
                                    {
                                        type: "number",
                                        name:"border-top-right-radius",
                                        label: tradCms.topRight || "Top right",
                                    },
                                ]
                            }
                        },
                        
                    ],
                    onchange: function(name, value, payload){
                        if(typeof value == "string"){
                            $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                [name]: value
                            });
                        }
                        if(name == "width" || name == "height"){
                            $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                [name]: (value == "0px" || value == "0%" || value == "0vw" || value == "0vh") ? "auto" : value+"px"
                            });
                        }
                        if(typeof value == "object" && !Array.isArray(value)){
                            $.each(value, function(k, v){
                                if(v != ""){
                                    $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                        [k]: v
                                    });
                                }
                            })
                        }
                    }
                }

                new CoInput(myInputs);
            },
            texte: function(kunik){
                let block = $(".hexagone-block[data-kunik='"+kunik+"']");
                if(block.find(".hexagone-texte").prop("contenteditable") == "inherit" || block.find(".hexagone-texte").prop("contenteditable") == "false"){
                    block.find(".hexagone-texte").prop("contenteditable", true);
                    block.find(".hexagone-texte").focus();
                }
            },
            titre: function(kunik){
                let block = $(".hexagone-block[data-kunik='"+kunik+"']");
                var myInputs = {
                    container: "#hexagone-inputs",
                    inputs: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "title",
                                class : "col-sm-12",
                                label : tradCms.image || "Image",
                                defaultValue: block.find("h3").text()
                            }
                        },
                        {
                            type: "number",
                            options : {
                                label: tradCms.fontSize || "Font size",
                                name: "font-size",
                                units: ["px"],
                                class : "col-sm-12",
                                defaultValue: block.find("h3").css("font-size"),
                                filterValue : function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                            }
                        },
                        {
                            type: "groupButtons",
                            options: {
                                name: "text-align",
                                class : "col-sm-12",
                                label: tradCms.textalignment || "Text alignment",
                                defaultValue: block.find("h3").css('text-align'),
                                options : [
                                    {
                                        value:"left",
                                        icon:"align-left"
                                    },
                                    {
                                        value:"center",
                                        icon:"align-center"
                                    },
                                    {
                                        value:"right",
                                        icon:"align-right"
                                    }
                                ]
                            }
                        },
                        {
                            type: "select",
                            options : {
                                name: "text-transform",
                                class: "col-sm-12",
                                label: tradCms.textTransform || "Text transform",
                                defaultValue: block.find("h3").css("text-transform"),
                                options : ["none","capitalize","uppercase","lowercase","initial","inherit"]
                            }
                        },
                        {
                            type: "colorPicker",
                            options: {
                                name: "color",
                                class: "col-sm-12",
                                label: "Couleur du texte",
                                defaultValue: block.find("h3").css("color")
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "padding",
                                label: tradCms.padding || "Padding",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "padding-top" : block.css("padding-top"),
                                    "padding-left" : block.css("padding-left"),
                                    "padding-bottom" : block.css("padding-bottom"),
                                    "padding-right" : block.css("padding-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "padding-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "padding-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },
                        {
                            type: "inputGroup",
                            options: {
                                classes: "col-sm-12",
                                name: "margin",
                                label: tradCms.margin || "Margin",
                                linkValue: true,
                                units: ["px", '%'],
                                defaultValue : {
                                    "margin-top" : block.css("margin-top"),
                                    "margin-left" : block.css("margin-left"),
                                    "margin-bottom" : block.css("margin-bottom"),
                                    "margin-right" : block.css("margin-right"),
                                },
                                filterValue: function(values, unit = ""){
                                    if(typeof values =="object"){
                                        $.each(values, function(e,v){
                                            if(isNaN(v))
                                                values[e]=v;
                                            else if (v == "")
                                                values[e]="";
                                            else
                                                values[e]=v+""+unit;
                                            });//.join(" ");
                                    }else{
                                        if (isNaN(values))
                                            values=values;
                                        else if (values == "")
                                            values="";
                                        else
                                            values=values+""+unit;
                                    }
                                    //if(!res.replace(/\s/g, ""))
                                    //  return "";
                                    return values;
                                },
                                responsive: false,
                                inputs: [
                                    {
                                        type: "number",
                                        name: "margin-top",
                                        label: tradCms.top || "Top",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-bottom",
                                        label: tradCms.bottom || "Bottom",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-left",
                                        label: tradCms.left || "Left",
                                    },
                                    {
                                        type: "number",
                                        name: "margin-right",
                                        label: tradCms.right || "right",
                                    },
                                ]
                            }
                        },
                        
                    ],
                    onchange: function(name, value, payload){
                        if(name == "title"){
                            $(".hexagone-block[data-kunik='"+kunik+"'] > h3").text(value);
                        }
                        if(typeof value == "string"){
                            $(".hexagone-block[data-kunik='"+kunik+"'] > h3").css({
                                [name]: value
                            });
                        }
                        if(typeof value == "object" && !Array.isArray(value)){
                            $.each(value, function(k, v){
                                if(v != ""){
                                    $(".hexagone-block[data-kunik='"+kunik+"']").css({
                                        [k]: v
                                    });
                                }
                            })
                        }
                    }
                }

                new CoInput(myInputs);
            },
            bindMouseOver : function(){
                $(".hexagone-block").on("mouseover", function(e){
                    e.stopImmediatePropagation();
                    var $block = $(this);
                    var blockActionIsNotExist = $block.find("> .block-actions-wrapper").length < 1,
                    isSelected = $block.find("> .block-actions-wrapper.selected").length > 0;
                    if(isSelected)
                        $(".block-actions-wrapper:not(.selected)").remove()
                    if(!hexagoneDetailEditor.dragstart && blockActionIsNotExist){
                        var kunik = $block.data("kunik");
                        var type = $block.data("type");
                        hexagoneDetailEditor.editor.addAction(type, $block);
                        hexagoneDetailEditor.editor.bindOnClickActionItem();
                    }
                });
            },
            addAction: function(type, $el, isSelected){
                actions = [
                    [
                        {
                            name:"edit",
                            icon:"pencil",
                            label: tradCms.edit
                        },
                        {
                            name:"delete",
                            icon:"trash-o",
                            label: tradCms.delete,
                            className:"bg-red"
                        },
                        {
                            name:"deselect",
                            icon:"ban",
                            label: tradCms.deselect,
                            className:"bg-orange"
                        }
                    ]
                ]
                var cmsOptions = `
                <div class="hexagone-block-action">
                    <div class="hexagone-block-action-list"  data-kunik="${$el.data('kunik')}" ></div>
                </div>`
                var $actions = $(`<div class="block-actions-wrapper">${cmsOptions}</div>`);

                actions.forEach(function(actionsGroup){
                    var $ul = $(`<ul></ul>`)
                    actionsGroup.forEach(function(action){
                        if ( action.name === "insertSection" ){
                            $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span data-toggle="tooltip"  title="${action.label}" ><img src="${assetPath}/cmsBuilder/img/drop-block.png"/ style="width: 25px;"></span>
                            </li>
                        `)
                        }  else if (type !== "texte") {
                        $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}">
                                <span data-toggle="tooltip" title="${action.label}" ><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                            </li>
                        `)
                        }else if (action.name !== "edit") {
                            $ul.append(`
                            <li data-type="${type}" data-action="${action.name}" class="${action.className ? action.className:""}" >
                                <span data-toggle="tooltip" title="${action.label}" ><i class="fa fa-${action.icon}" aria-hidden="true"></i></span>
                            </li>
                        `)
                        }
                    })
                    $actions.find(".hexagone-block-action-list").append($ul)
                    $actions.prepend(`<div class="hexagone-block-action-label" data-kunik="${$el.data('kunik')}" >${type}</div>`);
                    $(".block-actions-wrapper").not(".selected").remove()
                    $el.css({ overflow:"visible" })
                    $el.prepend($actions) 

                })
            },
            bindOnClickActionItem : function(){
                var self = this;
                $(".block-actions-wrapper li").off("click").on("click", function(e){
                    e.stopPropagation();
                    var $owner = $($(this).parents(".hexagone-block")[0]),
                        kunik = $owner.data("kunik"),
                        ownerType = $owner.data("type")

                   switch($(this).data("action")){
                        case "edit":
                            $("#hexagone-inputs").empty();
                            $("div[contenteditable=true]").prop("contenteditable", false);
                            $(".block-actions-wrapper").removeClass("selected")
                            $(this).closest(".block-actions-wrapper").addClass("selected")
                            // if(ownerType == "section"){
                            //     hexagoneDetailEditor.editor.section(kunik);
                            // }
                            if(typeof hexagoneDetailEditor.editor[ownerType] != "undefined" && typeof hexagoneDetailEditor.editor[ownerType] == "function"){
                                hexagoneDetailEditor.editor[ownerType](kunik)
                            }
                            break;
                        case "delete":
                            $("#hexagone-inputs").empty();
                            $(".hexagone-block[data-kunik='"+kunik+"']").remove();
                            break;
                        case "select_parent":
                            console.log("Select parent", kunik, ownerType)
                            break;
                        case "deselect":
                            $("#hexagone-inputs").empty();
                            $(".block-actions-wrapper").removeClass("selected")
                            break;
                    }
                })
                $(".hexagone-block").off("click").on("click", function(e){
                    e.stopImmediatePropagation();
                    $("div[contenteditable=true]").prop("contenteditable", false);
                    var $hexagoneBlock = $(this).closest(".hexagone-block"),
                        kunik = $hexagoneBlock.data("kunik"),
                        ownerType = $hexagoneBlock.data("type")
                    $(".block-actions-wrapper").removeClass("selected")
                    $hexagoneBlock.find("> .block-actions-wrapper").addClass("selected")
                    $("#hexagone-inputs").empty();
                    // if(ownerType == "section"){
                    //     hexagoneDetailEditor.editor.section(kunik);
                    // }

                    if(typeof hexagoneDetailEditor.editor[ownerType] != "undefined" && typeof hexagoneDetailEditor.editor[ownerType] == "function"){
                        hexagoneDetailEditor.editor[ownerType](kunik)
                    }
                })
            },
            save: function(){
                $(".btn-save-hexagone-edition").off("click").on("click", function(){
                    $(".block-actions-wrapper").remove();
                    var data = {
                        content: $(".hexagone-editor-content").html(),
                        name: "<?= @$_POST["criteria"] ?>",
                    }
                    if("<?= $_POST['type'] ?>" == 'navigatorHexagone'){
                        hexagoneDetailEditor.editor.saveNavigator(data);
                    }else if("<?= $_POST['type'] ?>" == 'blockHexagone'){
                        hexagoneDetailEditor.editor.saveBlockCms(data.content);
                    }
                });
            },
            saveNavigator: function(data){
                ajaxPost(
                    null,
                    baseUrl+"/costum/francetierslieux/savecriteria",
                    data,
                    function(data){
                        if(data.results){
                            if(typeof dataPosted.callback != "undefined" && typeof window[dataPosted.callback] != "undefined"){
                                if(typeof dataPosted.paramsCallback != "undefined"){
                                    window[dataPosted.callback](dataPosted.paramsCallback)
                                }else{
                                    window[dataPosted.callback]()
                                }
                            }

                            toastr.success("Criteria sauvegardé avec succès")
                        }else{
                            toastr.error("Erreur de sauvegarde")
                        }
                    }
                )
            },
            saveBlockCms: function(data){
                if(typeof dataPosted.callback != "undefined" && typeof window[dataPosted.callback] != "undefined"){
                    if(typeof dataPosted.paramsCallback != "undefined"){
                        if(dataPosted.paramsCallback == "htmlGenerate"){
                            window[dataPosted.callback](data)
                        }
                    }else{
                        window[dataPosted.callback]()
                    }
                }
            }
        },
        helpers: {
            getAutoScroller:function(container, params={}){
                var $container = $(container);
            
                var config = Object.assign({}, {
                    distance:100, timer:80, step:50
                }, params)
                var offset = $container.offset(),
                    offsetHeight = offset.top + $container.height()

                var handlers = { 
                    top:null, 
                    bottom:null,
                    clear:function(){
                        clearInterval(this.top),
                        clearInterval(this.bottom)
                    }
                }

                return {
                    start:function(event){
                        var isMoving = false
        
                        if((event.pageY - offset.top) <= config.distance){
                            isMoving = true
                            handlers.clear()
                            handlers.top = setInterval(function(){
                                $container.scrollTop($container.scrollTop() - config.step)
                            }, config.timer)
                        }
                        //bottom
                        if(event.pageY >= (offsetHeight - config.distance)){
                            isMoving = true
                            handlers.clear()
                            handlers.bottom = setInterval(function(){
                                $container.scrollTop($container.scrollTop() + config.step)
                            }, config.timer)
                        }
                        if(!isMoving)
                            handlers.clear()
                    },
                    stop:function(){
                        handlers.clear();
                    }
                }
            },
            isInViewport:function(el,marge) {
                const rect = el.getBoundingClientRect();
                return (
                    rect.top >= 0 &&
                    rect.left >= 0 &&
                    rect.bottom-marge <= (window.innerHeight || document.documentElement.clientHeight ) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth )
                );
            },
        }
    }
    hexagoneDetailEditor.init();
</script>