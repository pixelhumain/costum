
<?php
$cssAnsScriptFilesTheme = array(
	'/plugins/moment/min/moment.min.js',
	'/plugins/moment/min/moment-with-locales.min.js',
	'/plugins/moment-timezone/moment-timezone-with-data.js' 
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>
<style type="text/css">
	.img-center{
		margin: 0 auto;
	}

	.bg-bleu-claire{
		background-color: #01A7E3;
	}
	.b-claire{
		color: #01A7E3;
	}
	.v-cress{
		color: #70AD47;
	}

	.o-cress{
		color: #FF8A3B;
	}
	.bg-orange-claire{
		background-color: #FF8A3B;
	}
	.b-foncer{
		color: #3498DB;
	}
	.r-cress{
		color: #F600B6;
	}
	.uppercase-none{
		text-transform: none;
	}
	#annuaireHome {
		border-radius: 30px;
		background-color: white; 
		font-size: 1.1em
	}

	.mapBackground{
        /*background-image: url(/ph/assets/449afa38/images/city/cityDefaultHead_BW.jpg);*/
        background-color: #79a8ea;
        background-repeat: no-repeat;
        background-position: 0px 0px;
        background-size: 100% auto;
        height: 400px;
    }

    .btn-orange-cress, .btn-orange-cress:hover{
    	border-color: #FF8A3B;
    	font-size: 1.3em;
    	background-color: #FF8A3B;
    }

    .bloc-plus{
    	display: table-cell;
        vertical-align: middle;
        float: none;
        height: inherit;
    	width: inherit;
    	font-size: 50px;
    }

    .fc-event{
    	color : white !important;
    }
</style>

<div class="col-xs-12 no-padding">
	<div id="bannerHome" class="col-xs-12 no-padding">
		<img class="img-responsive no-padding" style="width: 100%;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Bandeau_ARESS.png'>
	</div>
	<div class="col-xs-12 padding-20 bg-bleu-claire">
		<div class="col-xs-12 text-center" id="annuaireHome" style="">
			<div class="col-xs-12 margin-top-10">
				<span class="b-claire" style="font-size: 1.4em"><span class="v-cress"><b>A</b></span>nnuaire <span class="b-foncer"><b>R</b></span>égional de l'<span class="r-cress"><b>ESS</b></span></span>
			</div>
			<div class="col-xs-12 col-md-4 col-md-offset-4 no-padding">
				<hr style="border-color:#FF8A3B; border-width: 3px 0;" />
			</div>
			<div class="col-xs-12 margin-bottom-20">
				Une plateforme sociale et solidaire que vous alimentez, que vous partagez et que vous utilisez afin de faciliter votre quotidien et de consolider les liens dans votre quartier, dans votre ville et sur l'ensemble de l'île... 
			</div>

			<div class="col-xs-12 no-padding">
				<div class="col-xs-12 col-md-4 text-center">
					<img class="img-responsive img-center" style="height: 90px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_identification.PNG' >
					Identifier les actions et structures sociales et solidaires de notre territoire
				</div>
				<div class="col-xs-12 col-md-4 text-center">
					<img class="img-responsive img-center" style="height: 90px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_agir_ensemble.PNG'>
					Agir ensemble pour notre territoire
				</div>
				<div class="col-xs-12 col-md-4 text-center">
					<img class="img-responsive img-center" style="height: 90px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_decouvrir_agenda_solidaire.PNG'>
					Découvrir l'agenda solidaire de La Réunion
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-md-offset-4 no-padding ">
				<hr style="border-color:#FF8A3B; border-width: 3px 0; " />
			</div>
			<div class="col-xs-12 margin-bottom-20 margin-top-5 b-claire " style="font-size: 1.2em">
				<b>Tous les acteurs que vous retrouverez sur cette plateforme possèdent des valeurs communes et répondent aux besoins du territoires :</b>
			</div>
			<div class="col-xs-12 no-padding margin-top-5 margin-bottom-10">
				<div class="col-xs-12 col-md-3 text-center">
					<img class="img-responsive img-center" style="height: 190px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_finalite_sociale.png' >
				</div>
				<div class="hidden-xs col-md-1" style="height: 190px">
					<div class="bloc-plus r-cress">
						<i class="fa fa-plus"></i>
					</div>
				</div>
				<div class="col-xs-12 col-md-3 text-center">
					<img class="img-responsive img-center" style="height: 190px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_gouvernance_democratique.png' >
				</div>
				<div class="hidden-xs col-md-1" style="height: 190px">
					<div class="bloc-plus r-cress">
						<i class="fa fa-plus"></i>
					</div>
				</div>
				<div class="col-xs-12 col-md-3 text-center">
					<img class="img-responsive img-center" style="height: 190px;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/Icone_lucrativité.png' >
				</div>
			</div>

		</div>


		<div class="col-xs-12 margin-top-20 padding-10">
			<div class="col-xs-12 col-md-6 no-padding">
				<a href="#mapcress" class="lbh-menu-app"><div id="mapHomeCress" class="col-xs-12 mapBackground no-padding" style="max-height:350px; min-height:350px">
					<img class="img-responsive img-center" style="max-height:350px; min-height:350px" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/home_map.png' >
				</div></a>
				<div class="col-xs-12 padding-10 text-center">
					<a href="#mapcress" class="btn btn btn-primary btn-orange-cress lbh-menu-app">Consultez le mapping solidaire </a>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<a href="#agenda" class="lbh-menu-app"><div id="agendaHomeCress" class="col-xs-12 no-padding">
					<img class="img-responsive img-center" style="max-height:350px; min-height:350px" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cressReunion/calendrier.png' >
				</div></a>
				 <?php //echo $this->renderPartial('co2.views.app.calendar', array()); ?> 
				<div class="col-xs-12 padding-10 text-center">
					<a href="#agenda" class="btn btn-primary btn-orange-cress lbh-menu-app">Consultez l'agenda solidaire</a>
				</div>
			</div>

		</div>
		
	</div>
	
</div>
<script type="text/javascript">
/*function showOrganisationInCitoyensPage(){
 	var filterAddType = ["organization"] ;
	 if (typeof contextData != "undefined" && typeof contextData.type != "undefined" && contextData.type == "citoyens") {
		typeObj.buildCreateButton(".elementCreateButton", false, {
			addClass:"col-xs-6 col-sm-6 col-md-4 col-lg-4 uppercase btn-open-form",
			bgIcon:true,
			textColor:true,
			inElement:true,
			allowIn:true,
			contextType: contextData.type,
			bgColor : "white",
			explain:true,
			inline:false
		}, null, filterAddType);
	pageProfil.init();	
  	}
}*/
 

var mapCressHome = {};
var dataSearchCC = {} ;
var STARTDATE = new Date();
var ENDDATE = new Date();
jQuery(document).ready(function() {
    setTitle("ARESS");
    
    // calculateAgendaWindow(0);
    // $("#repertory").css("height", "400px");
    // calendar.init("#profil-content-calendar");
    // searchObject.sourceKey = "cressReunion";
    // searchObject.initType = "events";
    // searchObject.text="";
    // var endDate = moment(STARTDATE).set("month", moment(STARTDATE).get("month")+1).valueOf();
    // var secondEndDate = Math.floor(endDate / 1000);
    // calendar.searchInCalendar(searchObject.startDate, secondEndDate);







    
    //searchObject.initType = "";

    // dataSearchCC=searchInterface.constructObjectAndUrl(true);
    // dataSearchCC.searchType = ["organizations"];
    // //dataSearchCC.sourceKey = "cressReunion";
    // dataSearchCC.indexStep = 0;

  //   var paramsMapCR = {
		// container : "mapHomeCress",
		// activePopUp : true,
		// tile : "maptiler",
		// menuRight : {
  //           close : false,
  //           btnClose : {
  //               right : "280px"
  //           }
  //       },
  //       activePreview : true,
  //       doubleClick : true
  //   };
  //   mapCressHome = mapObj.init(paramsMapCR);
  //   allMaps.maps[paramsMapCR.container]=mapCressHome;
  //   $.ajax({
  //       type: "POST",
  //       url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
  //       data: dataSearchCC,
  //       dataType: "json",
  //       error: function (data){
		// 	mylog.log(">>> error autocomplete search"); 
		// 	mylog.dir(data);   
		// 	$("#dropdown_search").html(data.responseText);
		// 	loadingData = false;
  //       },
  //       success: function(data){ 
  //           mylog.log(">>> success autocomplete search data in cc!!!! ", data);
  //           if(!data){ 
  //             toastr.error(data.content); 
  //           } else { 
		// 		mapCressHome.addElts(data.results);
		// 		//allMaps.maps[paramsMapCR.container]=mapCressHome;
  //           }
  //       }
  //   });
 // showOrganisationInCitoyens();
 
 /*setInterval(function(){
 		 $(".ssmla[data-action='create']").removeAttr("onClick");
         $(".ssmla[data-action='create']").attr("onClick",'showOrganisationInCitoyensPage();');
	},2000);*/
 });
</script>