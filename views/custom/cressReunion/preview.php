<style type="text/css">
	.blockFontPreview{
		font-size: 14px;
		color: #777;
	}

	#filters-nav{
		padding-top: .6em !important;
	}

	.fa-preview{
		background-color: #ddd;
		color: orange;
		border-radius: 50%;
		padding: .8em;
	}
</style>
<?php 
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig=@$this->appConfig["preview"];
$auth=(Authorisation::canEditItem( @Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

$iconColor=(isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
?> 
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>">
	<div class="col-xs-12 padding-10">
		
		<?php if(isset($previewConfig["toolBar"]["close"]) && $previewConfig["toolBar"]["close"]){ ?> 
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
		<?php } 
		if(isset($previewConfig["toolBar"]["goToItem"]) && $previewConfig["toolBar"]["goToItem"]){ ?>
			<a href="#@<?php echo @$element["slug"] ?>" target="_blank" class="btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
		<?php } 
		if(isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth){ ?>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" 
			data-subtype="<?php echo $element["type"] ?>">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>	
	</div>

	<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
		<?php 
		if(isset($previewConfig["banner"]) && !empty($previewConfig["banner"])){
			if (isset($element["profilBannerUrl"]) && !empty($element["profilBannerUrl"])){ 
				$url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]);
			}else{
				 if(!empty($this->costum) && isset($this->costum["htmlConstruct"]) 
					&& isset($this->costum["htmlConstruct"]["element"])
						&& isset($this->costum["htmlConstruct"]["element"]["banner"]["img"]))
					$url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
				else
					$url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
			}
			?> 
			<div class="col-xs-12 no-padding" id="col-banner" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
				<?php 
				if(isset($previewConfig["banner"]["edit"]) && $previewConfig["banner"]["edit"] && $auth){ ?> 
					<?php echo $this->renderPartial("co2.views.element.modalBanner", array(
							"type"=>$type, 
							"id"=>(string)$element["_id"], 
							"name"=>$element["name"],
							"edit" => $canEdit,
							"openEdition" => $openEdition,
							"profilBannerUrl"=>@$element["profilBannerUrl"])); 
				} ?> 
				<div id="contentBanner" class="col-xs-12 no-padding">
					<?php $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
						alt="'.Yii::t("common","Banner").'" 
						src="'.$url.'">';
					if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
						$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
									class="thumb-info"  
									data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
									data-lightbox="all">'.
									$imgHtml.
								'</a>';
					}
					echo $imgHtml; 
					?>	
				</div>
			</div>
			<?php if(isset($previewConfig["banner"]["imgProfil"]) && $previewConfig["banner"]["imgProfil"]){ ?>
				<div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
				<?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?> 
						<a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
						class="thumb-info"  
						data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
						data-lightbox="all">
							<img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);margin: auto;max-height: 300px;" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
						</a>
				<?php }else{ ?>
						<img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="margin: auto;box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumb/default_<?php echo $type ?>.png"/>
				<?php } ?>
				</div>
		<?php } 
		} ?>
		<div class="preview-element-info col-xs-12">
		<?php 
			if(isset($previewConfig["body"])){  
				if(is_string($previewConfig["body"])){
					 echo $this->renderPartial($previewConfig["body"], array("type"=>$type, "element"=>$element, "edit"=>$auth)); 
				}else{ 
					if(isset($previewConfig["body"]["name"]) && $previewConfig["body"]["name"]){ ?>
						<h3 class="text-center"><?php echo $element["name"] ?></h3>
					<?php } 
					if(isset($previewConfig["body"]["shortDescription"]) && $previewConfig["body"]["shortDescription"]) { ?>
						<div class="col-xs-10 col-xs-offset-1 margin-top-20">
							<span class="col-xs-12 text-center" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
							</span>	
						</div>
					<?php } 
					if(isset($previewConfig["body"]["type"]) && $previewConfig["body"]["type"]){ ?>
						<span class="col-xs-12 text-center blockFontPreview margin-top-20"> 
							<span class="text-<?php echo $iconColor; ?> uppercase"><?php echo Yii::t("common", Element::getControlerByCollection($type)); ?></span>
							<?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){ 
								if($type==Organization::COLLECTION)
									$typesList=Organization::$types;
								else{
									$typesList=array(
										"workshop" => "Atelier",
										"tasting" => "Dégustation",
										"secondHandStoreJumbleSale" => "Dépôt-vente, Braderie",
										"radioShow" => "Emission de radio",
										"exhibition"=> "Exposition",
										"trainingAwareness" => "Formation, sensibilisation",
										"forumMeetingTradeFair" => "Forums, Recontres, salons",
										"openHouse" => "Portes-ouvertes",
										"filmProjection" => "Projection de film",
										"others"=> "Autres",
										"competition" => "Competition",
								        "concert" => "Concert",
								        "contest" => "Concours",
								        "exhibition" => "Exposition",
								        "festival" => "Festival",
								        "getTogether" => "Rencontre",
								        "market" => "Marché",
									    "meeting" => "Réunion",
									    "course"=>"Formation",
										"conference"=>"Conférence",
										"debate"=>"Débat",
										"film"=>"Film",
										"stand"=>"Stand",
										"crowdfunding"=>"Financement Participatif",
										"internship" => "Stage",
								        "spectacle" =>  "Spectacle",
										"protest" => "Manifestation",
										"fair" => "Foire"
									);
								}
								$typeCat=(isset($typesList[$element["type"]])) ? $typesList[$element["type"]] : $element["type"];
								?>
									<i class="fa fa-x fa-angle-right margin-left-10"></i>
									<span class="margin-left-10"><?php 
									if(isset($typesList[$element["type"]])) 
										echo Yii::t("category", $typesList[$element["type"]]); 
									?></span>
							<?php } ?>
							
						</span>
					<?php }
					if(isset($previewConfig["body"]["locality"]) && $previewConfig["body"]["locality"] && !empty($element["address"]["addressLocality"]) && $type!="events"){ ?>
						<div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
							<i class="fa fa-map-marker"></i> 
							<?php
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? 
										$element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
						</div>
					<?php } ?>

					<?php if($type == "events"){ ?>
						<br>
						<br>
						<div class="row">
						<?php if(isset($element["startDate"]) && isset($element["endDate"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-calendar"></i>
								  	<b>DATE</b> 
								  	<?php 
								  	if(date("D d M Y",strtotime($element["startDate"])) == date("D d M Y",strtotime($element["endDate"]))){
								  		echo date("D d M Y",strtotime($element["startDate"]));
								  	}else{
								  		echo date("D d M Y",strtotime($element["startDate"]))." à ".date("D d M Y",strtotime($element["endDate"]));
								  	}
								  	?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["thematicWeek"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-clock-o"></i> 
								  	<b>SEMAINE THÉMATIQUE</b> 
								  	<?php echo $element["thematicWeek"]; ?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["startDate"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-clock-o"></i> 
								  	<b>HORAIRE</b> 
								  	<span id="startDatePreview"></span>
								  	 à 
								  	<span id="endDatePreview"></span>
								  	<?php //echo date("H:i",strtotime($element["startDate"]))." à ".date("H:i",strtotime($element["endDate"])); ?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["isPaying"]) && isset($element["amount"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-tag"></i> 
								  	<b>TARIF</b> 
								  	<?php echo $element["amount"] ?>€
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["organizer"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-building-o"></i> 
								  	<b>STRUCTURE</b> 
								  	<?php 
								  	$index=1;
								  	foreach ($element["organizer"] as $key => $value) {
								  		echo $value["name"].(($index==count($element["organizer"]))?"":", ");
								  		$index++;
								  	}
								  	?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["tergetPublic"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-bullseye"></i> 
								  	<b>CIBLE</b> 
								  	<?php echo $element["tergetPublic"]; ?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["format"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-time"></i> 
								  	<?php echo $element["format"]; ?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["lowMobilityAccess"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-wheelchair"></i>
								  	<?php echo (($element["lowMobilityAccess"]=="true")?"A":"Non A")."ccessible au personne à mobilité reduite"; ?>
								  </li>
								</ul>
							</div>
						<?php } ?>

						<?php if(isset($element["address"])){ ?>
							<div class="col-md-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  	<i class="fa fa-preview fa-map-marker"></i> 
								  	<?php
									echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
									echo !empty($element["address"]["postalCode"]) ? 
											$element["address"]["postalCode"].", " : "";
									echo $element["address"]["addressLocality"];
								?>
								  </li>
								</ul>
							</div>
						<?php } ?>
						</div>
					<?php } ?>

					<?php
					if(isset($previewConfig["body"]["tags"]) && $previewConfig["body"]["tags"]){ ?>
						<div class="header-tags col-xs-12 text-center blockFontPreview margin-top-20">
								<?php 
								if(@$element["tags"]){ 
									foreach ($element["tags"] as $key => $tag) { ?>
										<a  href="javascript:;"  class="letter-red" style="vertical-align: top;">#<?php echo $tag; ?></a>
									<?php } 
								} ?>
						</div>
					<?php } ?>
				</div>
				<?php if(isset($previewConfig["body"]["eventInfos"]) && $previewConfig["body"]["eventInfos"] && $type==Event::COLLECTION){ ?>
					<div class="event-infos-header text-center margin-top-10 col-xs-12 blockFontPreview margin-top-20"  style="font-size: 14px;font-weight: none;"></div>
				<?php } 
				if(isset($previewConfig["body"]["description"]) && $previewConfig["body"]["description"]){ ?>
					<div class="col-xs-10 col-xs-offset-1 margin-top-20">
						<span class="pull-left text-center" id="descriptionHeader"><?php echo ucfirst(substr(trim(@$element["description"]), 0, 180)); ?>
						</span>	
					</div>
				<?php }
				if(isset($previewConfig["body"]["url"]) && $previewConfig["body"]["url"] && isset($element["url"])){ ?>
					<div class="col-xs-10 col-xs-offset-1 margin-top-20 text-center">
						<?php $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ; ?>
						<a href="<?php echo $scheme.$element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo $element["url"] ?></a>
					</div>
				<?php } 
				if(isset($previewConfig["body"]["shareButton"]) && $previewConfig["body"]["shareButton"]){ ?>
					<div class="social-share-button-preview col-xs-12 text-center margin-top-20 margin-bottom-20"></div>
		<?php 	}
			} 
		} ?>
	</div>
</div>

<script type="text/javascript">

	var eltPreview=<?php echo json_encode($element); ?>;
 	var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
	var socialBarConfig=<?php echo json_encode(@$previewConfig["body"]["shareButton"]); ?>;
	jQuery(document).ready(function() {	
		var str = directory.getDateFormated(eltPreview, null, true);
		$(".event-infos-header").html(str);
		$(".social-share-button-preview").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : socialBarConfig, "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
		coInterface.bindLBHLinks();
		resizeContainer();
		directory.bindBtnElement();

		$("#startDatePreview").html(moment(eltPreview.startDateDB).local().locale(mainLanguage).format("HH:mm"));

        $("#endDatePreview").html(moment(eltPreview.endDateDB).local().locale(mainLanguage).format("HH:mm"));
	});
</script>