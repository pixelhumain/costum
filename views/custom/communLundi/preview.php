<?php
    // var_dump($element);exit;
    $costum = !empty($this->costum) ? $this->costum : $this->context->config['costum'];
	function dateToFrench($date, $format) 
	{
		$english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
		$french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
		$english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		$french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
		return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date) ) ) );
	}


?>

<style type="text/css">
	.blockFontPreview{
		font-size: 14px;
		color: #777;
	}

	#filters-nav{
		padding-top: .6em !important;
	}

	.fa-preview{
		background-color: <?=$costum["css"]["color"]["orange"]?>;;
		color: white;
		border-radius: 50%;
		padding: .5em;
	}
    .list-group-item{
        border-color:<?=$costum ["css"]["color"]["blue"]?>;
    }
	.list-group-item .title-info{
        display: inline-block;
        width: 100%;
        color: <?=$costum ["css"]["color"]["blue"]?>;
    }

	.list-group-item .content-info{
		width: 100%;
    display: inline-block;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    }
	
    .header-tags span{
        vertical-align: top;
        color:<?=$costum ["css"]["color"]["blue"]?>;
    }

	#descriptionDetail ul{
		list-style: initial;
	}
	#descriptionDetail img{
		max-width:100%;
	}
</style>
<?php 
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig=@$this->appConfig["preview"];
$auth=(Authorisation::canEditItem( @Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

// var_dump($previewConfig);
$iconColor=(isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
?> 
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>">
	<div class="col-xs-12 padding-10">
		
		<?php if(isset($previewConfig["toolBar"]["close"]) && $previewConfig["toolBar"]["close"]){ ?> 
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
		<?php } 
		if(isset($previewConfig["toolBar"]["goToItem"]) && $previewConfig["toolBar"]["goToItem"]){ ?>
			<a href="#@<?php echo @$element["slug"] ?>" target="_blank" class="btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
		<?php } 
		if(isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth){ ?>
			
		<?php } ?>
		<?php 
		if (isset($canEdit) && $canEdit) { 
            ?>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" 
			data-subtype="<?php echo (isset($element["type"])) ? $element["type"] : "" ?>">
				<i class="fa fa-pencil"></i>
			</button>
            <button id="delete-element" class="btn btn-default pull-right margin-right-10 btn-delete-element-preview bg-red text-white" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo ($type==Event::COLLECTION && isset($element["category"])) ? $element["category"] : $type ?>">
				<i class="fa fa-trash"></i> 
			</button>
		<?php }?>	
	</div>

	<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
		
		<div class="preview-element-info col-xs-12" style="margin-bottom:90px;">
		<?php 
			if(isset($previewConfig["body"])){  
				if(is_string($previewConfig["body"])){
					 echo $this->renderPartial($previewConfig["body"], array("type"=>$type, "element"=>$element, "edit"=>$auth)); 
				}else{ 
					if(isset($previewConfig["body"]["name"]) && $previewConfig["body"]["name"]){ ?>
						<h3 class="text-center"><?php echo $element["name"] ?></h3>
					<?php } 
					if(isset($previewConfig["body"]["shortDescription"]) && $previewConfig["body"]["shortDescription"]) { ?>
						<div class="col-xs-10 col-xs-offset-1 margin-top-20">
							<span class="col-xs-12 text-center" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 200)); ?>
							</span>	
						</div>
					<?php } 
					if(isset($previewConfig["body"]["type"]) && $previewConfig["body"]["type"]){ ?>
						<span class="col-xs-12 text-center blockFontPreview margin-top-20"> 
							
							<?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){
							?>	 
								<span class=" uppercase">Format</span>
							<?php	
								if($type==Organization::COLLECTION)
									$typesList=Organization::$types;
								else{
									$typesList=array(
										"workshop" => "Atelier",
										"tasting" => "Dégustation",
										"secondHandStoreJumbleSale" => "Dépôt-vente, Braderie",
										"radioShow" => "Emission de radio",
										"exhibition"=> "Exposition",
										"trainingAwareness" => "Formation, sensibilisation",
										"forumMeetingTradeFair" => "Forums, Recontres, salons",
										"openHouse" => "Portes-ouvertes",
										"filmProjection" => "Projection de film",
										"others"=> "Autres",
										"competition" => "Competition",
								        "concert" => "Concert",
								        "contest" => "Concours",
								        "exhibition" => "Exposition",
								        "festival" => "Festival",
								        "getTogether" => "Rencontre",
								        "market" => "Marché",
									    "meeting" => "Réunion",
									    "course"=>"Formation",
										"conference"=>"Conférence",
										"debate"=>"Débat",
										"film"=>"Film",
										"stand"=>"Stand",
										"crowdfunding"=>"Financement Participatif",
										"internship" => "Stage",
								        "spectacle" =>  "Spectacle",
										"protest" => "Manifestation",
										"fair" => "Foire"
									);
								}
								$typeCat=(isset($typesList[$element["type"]])) ? $typesList[$element["type"]] : $element["type"];
								?>
									<i class="fa fa-x fa-angle-right margin-left-10"></i>
									<span class="margin-left-10 text-<?php echo $iconColor; ?>"><?php 
									if(isset($typesList[$element["type"]])) 
										echo Yii::t("category", $typesList[$element["type"]]); 
									?></span>
							<?php } ?>
							
						</span>
					<?php }
					
					if(isset($previewConfig["body"]["locality"]) && $previewConfig["body"]["locality"] && !empty($element["address"]["addressLocality"]) && $type!="events"){ ?>
						<div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
							<i class="fa fa-map-marker"></i> 
							<?php
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? 
										$element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
						</div>
					<?php } ?>

					<?php if($type == "events"){ ?>
						<br>
						<br>
						<div class="col-xs-12 margin-top-30">
						<?php if(isset($element["startDate"]) && isset($element["endDate"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
									<span class="title-info">
								  	<i class="fa fa-preview fa-calendar"></i>
								  	Date
						            </span>
									<span class="content-info"> 
								  	<?php 
								  	if(date("D d M Y",strtotime($element["startDate"])) == date("D d M Y",strtotime($element["endDate"]))){
								  		echo dateToFrench($element["startDate"],"l d F Y");

										
								  	}else{
								  		echo date("D d M Y",strtotime($element["startDate"]))." à ".date("D d M Y",strtotime($element["endDate"]));
								  	}
								  	?>
									</span>
								  </li>
								</ul>
							</div>
						<?php } ?>



						<?php if(isset($element["startDate"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-clock-o"></i> 
								  	Horaire
						          </span>
								  <span class="content-info"> 
								  	<span id="startDatePreview"></span>
								  	 à 
								  	<span id="endDatePreview"></span>
								  	<?php //echo date("H:i",strtotime($element["startDate"]))." à ".date("H:i",strtotime($element["endDate"])); ?>
						          </span>  
								</li>
								</ul>
							</div>
						<?php } ?>
						<?php if(isset($element["url"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-at"></i> 
								  	Lien Visio
						         </span>
								 <span class="content-info"> 	
								  	<?php echo "<a href='".$element["url"]."' target='_blank'>".$element["url"]."</a>"; ?>
						        </span> 	
								  </li>
								</ul>
							</div>
						<?php } ?>
						<?php if(isset($element["documentation"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-file"></i> 
								  	Lien documentation
						          </span>
								  <span class="content-info"> 
								  	
								  	<?php echo "<a href='".$element["documentation"]."' target='_blank'>".$element["documentation"]."</a>"; ?>
									</span>  
								</li>
								</ul>
							</div>
						<?php } ?>
						<?php if(isset($element["organizer"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-building-o"></i> 
								  	Structure organisatrice 
						          </span>
								  <span class="content-info">
								  	<?php 
								  	$index=1;
								  	foreach ($element["organizer"] as $key => $value) {
								  		echo $value["name"].(($index==count($element["organizer"]))?"":", ");
								  		$index++;
								  	}
								  	?>
									 </span>
								  </li>
								</ul>
							</div>
						<?php } ?>
						<?php if(isset($element["email"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-envelope"></i> 
								  	Email animateur
						         </span>
								 <span class="content-info">
								  	
								  	<?php echo $element["email"]; ?>
						         </span>	
								  </li>
								</ul>
							</div>
						<?php } ?>
						<?php if(isset($element["common"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-creative-commons"></i> 
								  	Commun(s) utilisé(s) 
						          </span>
								  <span class="content-info">
								  	<?php 
								  	$index=1;
								  	foreach ($element["common"] as $key => $value) {
								  		echo $value["name"].(($index==count($element["common"]))?"":", ");
								  		$index++;
								  	}
								  	?>
									</span>
								  </li>
								</ul>
							</div>
						<?php } ?>	
						<?php if(isset($element["links"]) && isset($element["links"]["attendees"])){ ?>
							<div class="col-xs-12 col-sm-6">
								<ul class="list-group">
								  <li class="list-group-item">
								  <span class="title-info">
								  	<i class="fa fa-preview fa-users"></i> 
								  	Participant(e)s
						         </span> 
								 <span class="content-info">
								  	<?php 
								  	echo strval(count(array_keys($element["links"]["attendees"])));
								  	?>
								  </li>
					  	          </span>
								</ul>
							</div>	
						<?php } ?>

						</div>
					<?php } ?>
					<?php 
				   if(isset($element["description"]) && $element["description"]){ 
					$Parsedown = new Parsedown();
					//$Parsedown->text('Hello _Parsedown_!');
					$description=$Parsedown->text($element["description"]) ;
					?>
					<div class="col-xs-10 col-xs-offset-1 margin-top-20">
						<h4 style="text-decoration:underline">Détails : </h4>
						<div class="col-xs-12 no-padding" id="descriptionDetail">
							<?php echo $description; ?>
				        </div>	
					</div>
				<?php } ?>

				</div>
				
		<?php 	
			} 
		} ?>
	</div>
</div>

<script type="text/javascript">

	var eltPreview=<?php echo json_encode($element); ?>;
 	var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
	var socialBarConfig=<?php echo json_encode(@$previewConfig["body"]["shareButton"]); ?>;
	jQuery(document).ready(function() {	
        
		var str = directory.getDateFormated(eltPreview, null, true);
		$(".event-infos-header").html(str);
		$(".social-share-button-preview").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : socialBarConfig, "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
		coInterface.bindLBHLinks();
		resizeContainer();
		directory.bindBtnElement();
		$('#delete-element').on('click', function () {
            var self = $(this);
            $.confirm({
                title : trad.delete,
                content : 'Procéder à la suppression de l\'événement',
                buttons : {
                    confirm : {
                        text : 'Confirmer',
                        action() {
                            var url = baseUrl + '/' + moduleId + '/element/delete/id/' + self.data('id') + '/type/events';
                            var request_params = {reasons : 'none'};
                            ajaxPost(null, url, request_params, function (__data) {
                                if (__data.result) {
                                    toastr.success(__data.msg);
                                    $(document.body).trigger('co.event.delete');
                                    urlCtrl.closePreview();
									urlCtrl.loadByHash(location.hash);
                                } else {toastr.error(__data.msg);}
                            });
                        }
                    },
                    cancel : {
                        text : 'Annuler'
                    }
                }
            })
        }) 

		$("#startDatePreview").html(moment(eltPreview.startDateDB).local().locale(mainLanguage).format("HH:mm"));

        $("#endDatePreview").html(moment(eltPreview.endDateDB).local().locale(mainLanguage).format("HH:mm"));
	
	    $(".btn-edit-preview").off().on("click",function(){
			// alert("heure");
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
            var typeElem=$(this).data("type");
            var idElem=$(this).data("id");
            var subElem=$(this).data("subType");
            var ctrlElem=typeObj[typeObj[typeElem].sameAs].ctrl;
            var editPrev=jQuery.extend(true, {},costum.typeObj[typeElem].dynFormCostum);
            editPrev.onload.actions.hide.patternselect = 1;
			editPrev.onload.actions.hide.similarLinkcustom = 1;
            // if(costum.isCostumAdmin){
                editPrev.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
                    "onText": trad.yes,
                    "offText": trad.no,
                    "onLabel": tradDynForm.public,
                    "offLabel": tradDynForm.private,
                    "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
                    "labelInformation": tradDynForm.explainvisibleevent
                })
            // }  
			mylog.log("editPrev",editPrev);     

                        
            dyFObj.editElement(ctrlElem,idElem, subElem, editPrev);      

        });
	});
</script>