<?php
$banniere = [
	'banner' => Yii::app()->getModule("costum")->getAssetsUrl() . '/images/coevent/no-banner.jpg',
	'logo' => Yii::app()->getModule("costum")->getAssetsUrl() . '/images/coevent/logo.png'
];

$currentEvent = PHDB::findOneById(Event::COLLECTION, new MongoId($_GET['event']));

$documents = Document::getListDocumentsWhere(
	[
		'source.key' => $currentEvent['slug'],
		// 'type' => 'cms',
	],
	'image'
);

foreach ($documents as $document) {
	switch ($document['subKey']) {
		case 'logo':
			$banniere[$document['subKey']] = $document['imagePath'];
		case 'banner':
			$banniere[$document['subKey']] = $document['imagePath'];
			// break;
	}
}

// var_dump($banniere);exit;


$fields = ["name", "shortDescription", "description", "startDate", "endDate", 'profilRealBannerUrl', 'timeZone', 'profilMediumImageUrl','organizer', 'url', 'email'];
// $startRange=new MongoDate(1694383200);
// $endRange=new MongoDate(strtotime("2023-09-11T22:00:00.000Z"));

$query=array('parent.' . $_GET['event'] => array('$exists' => true),'_id' => array('$ne' => new MongoId($_GET['event'])));
if(!empty($_GET["startDate"])){
	$startDate=(is_string($_GET["startDate"])) ? new MongoDate(strtotime($_GET["startDate"])) : new MongoDate($_GET["startDate"]);
	$query['startDate']=array('$gte'=>$startDate);
}
if(!empty($_GET["endDate"])){
	$endDate=(is_string($_GET["endDate"])) ? new MongoDate(strtotime($_GET["endDate"])) : new MongoDate($_GET["endDate"]);
	$query['endDate']=array('$lt'=>$endDate);
}



// $rangeEvents = PHDB::findAndSort(Event::COLLECTION, $query,);
// var_dump($query);exit;
$subEventsDatabase = PHDB::findAndSort(Event::COLLECTION, $query,array('startDate' => 1),0,$fields);
$subEventsCode = [];
// var_dump($subEventsDatabase);exit;
foreach ($subEventsDatabase as $subEventDatabase) {
	$tempSubEvent = [];
	$tempSubEvent["name"] = $subEventDatabase["name"];
	$tempSubEvent["shortDescription"] = isset($subEventDatabase["shortDescription"]) ? $subEventDatabase["shortDescription"] : "";
	$tempSubEvent["fullDescription"] = isset($subEventDatabase["description"]) ? $subEventDatabase["description"] : "";
	$tempSubEvent["organizer"] = isset($subEventDatabase["organizer"]) ? $subEventDatabase["organizer"]: "";
	$tempSubEvent["startDate"] = $subEventDatabase["startDate"]->toDateTime();
	$tempSubEvent["endDate"] = $subEventDatabase["endDate"]->toDateTime();
	$tempSubEvent["background"] = $subEventDatabase["profilRealBannerUrl"] ?? '';
	$tempSubEvent["url"] = $subEventDatabase["url"] ?? '';
	$tempSubEvent["email"] = $subEventDatabase["email"] ?? '';
	if (!empty($subEventDatabase["timeZone"])) {
		$tempSubEvent["endDate"]->setTimeZone(new DateTimeZone($subEventDatabase["timeZone"]));
		$tempSubEvent["startDate"]->setTimeZone(new DateTimeZone($subEventDatabase["timeZone"]));
	}
	$tempSubEvent["logo"] = $subEventDatabase["profilMediumImageUrl"] ?? '';

	$subEventsCode[] = $tempSubEvent;
}
$parser = new Parsedown();
$structField = "structags";
$cssAnsScriptFilesModule = array(
	'/plugins/font-awesome/css/font-awesome.min.css',
	'/plugins/font-awesome-custom/css/font-awesome.css'
);


$color1="#8ac4c5";
$color2="#f28437";

$contextDescription =(!empty($currentEvent["costum"]["description "])) ? $currentEvent["costum"]["description "] : ( (!empty($current["shortDescription"])) ? $current["shortDescription"] : null) ;
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
?>
<style> 
    html { 
		-webkit-print-color-adjust: exact; 
    }
	p {
	    /* line-height: 1; */
		margin-top:20px;
    }
	div{
		/* margin:0px; */
	}
</style>
<img src="<?= $banniere['banner'] ?>" alt="banniere" style="width: 800px; height: 300px;display: block; ">
<h1 style="font-family: sans-serif; position: fixed; top: 15px; left: 15px;"><?= $currentEvent["name"] ?></h1>
<?php if (!empty($contextDescription)) { ?>
	<p style="font-size: 16px; font-family: sans-serif;"><span style="font-size: 16px;">&nbsp;&nbsp;</span><strong><em><?= $contextDescription ?></em></strong></p>
<?php } ?>
<?php if (!empty($currentEvent["description"])) { ?>
	<p style="font-size: 16px; font-family: sans-serif;">&nbsp;&nbsp;<?= $parser->text(@$currentEvent["description"]) ?></p>
<?php } ?>

<?php
$previousDay="";
// var_dump($subEventsCode);exit;
foreach ($subEventsCode as $subEventCode) {
?>
	
	<?php
	
	$start_date = [
		'datetime' => $subEventCode['startDate'],
		'coded_date' => $subEventCode['startDate']->format('Y-m-d'),
		'formated_date' => $subEventCode['startDate']->format('d') . ' ' . Yii::t('translate', $subEventCode['startDate']->format('F')) . ' ' . $subEventCode['startDate']->format('Y'),
		'formated_hour' => $subEventCode['startDate']->format('H:i')
	];
	// var_dump($start_date);exit;
	$end_date = [
		'datetime' => $subEventCode['endDate'],
		'coded_date' => $subEventCode['endDate']->format('Y-m-d'),
		'formated_date' => $subEventCode['endDate']->format('d') . ' ' . Yii::t('translate', $subEventCode['endDate']->format('F')) . ' ' . $subEventCode['endDate']->format('Y'),
		'formated_hour' => $subEventCode['endDate']->format('H:i')
	];
	// $previousDay=(empty($previousDay)) ? $start_date['coded_date'] : $previousDay;

	if($start_date['coded_date'] != $previousDay){
		echo '<hr/>';
		echo '<div>';
		echo '<h2 style="display:block; margin:0; background-color:'.$color1.';">Le '.$start_date['formated_date'].'</h2>';
		echo '</div>';
	} ?>
	<div style="margin-top:3em">
	<p style="font-family: sans-serif;">
	<?php
	if ($start_date['coded_date'] == $end_date['coded_date']) {
	?>
		<b><span style="font-family: sans-serif;"><?= $start_date['formated_hour'] ?></span> - <span style="font-family: sans-serif;"><?= $end_date['formated_hour'] ?> : </span></b>
	<?php
	} else {
	?>
		<span style="font-family: sans-serif;">Le <?= $start_date['formated_date'] ?></span> à <span style="font-family: sans-serif;"><?= $start_date['formated_hour'] ?></span> jusqu'au <?= $end_date['formated_date'] ?> à <span style="font-family: sans-serif;"><?= $end_date['formated_hour'] ?></span>
	<?php
	} ?>
	<span style="font-family: sans-serif; text-transform: uppercase;"><b><?= $subEventCode["name"] ?></b></span>
	
	<?php
	if (!empty($subEventCode["organizer"])) { ?>
		<!-- <div style="font-family: sans-serif;">&nbsp; -->
		 <span style="font-family: sans-serif;">(Avec
	    <?php
		    $index=1;
	        foreach ($subEventCode["organizer"] as $key => $value) {
				echo $value["name"].(($index==count($subEventCode["organizer"]))?"":", ");
				$index++;
			}
		?>)
		</span>
		
		
		<!-- </div> -->
	<?php } ?>
	</p>
	<?php
	if (!empty($subEventCode["shortDescription"])) { ?>
	<p style="font-family: sans-serif;">
		<span style="font-size:16px; font-style:italic;"><?= $subEventCode["shortDescription"] ?></span>
		</p>
	<?php } ?>
	
	<?php
	if (!empty($subEventCode["url"])) { ?>
	<p style="font-family: sans-serif;">
		<span style="font-family:zapfdingbats;">+</span> <span style="font-size: 16px;"><a href="<?= $subEventCode["url"] ?>" target="_blank">Lien visio</a></span><span>&nbsp;&nbsp;&nbsp;(<?= $subEventCode["url"] ?>)</span>
	</p>
	<?php } ?>
	
	
	<?php
	if (!empty($subEventCode["email"])) { ?>
	<p style="font-family: sans-serif;">
	<span style="font-family:zapfdingbats;">)</span> <span style="font-size: 16px;"><?=  $subEventCode["email"] ?></span>
	</p>
	<?php } ?>
	

	
	
	
	<?php 
	if (!empty($subEventCode["background"])) { ?>
		<p style="text-align: center;"><img style="width: 500px; display: block;" src="<?= $subEventCode["background"] ?>"></p>
	<?php }
	if (!empty($subEventCode["fullDescription"])) { ?>
		<!-- <div style="font-family: sans-serif;">&nbsp;<?= $parser->text($subEventCode["fullDescription"]) ?></div> -->
	<?php } 
	?>
	</div>
	
	
	
<?php
    $previousDay=$start_date['coded_date'];
}
?>