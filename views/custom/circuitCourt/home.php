
<div class="no-padding col-xs-12">


    <style type="text/css">
      #customHeader{
        margin-top: 0px;
      }
      #costumBanner{
       /* max-height: 375px; */
      }
      #costumBanner h1{
        position: absolute;
        color: white;
        background-color: rgba(0,0,0,0.4);
        font-size: 29px;
        bottom: 0px;
        padding: 20px;
        text-align:center;
      }
      #costumBanner h1 span{
        color: #eeeeee;
        font-style: italic;
      }
      #costumBanner img{
        min-width: 100%;
      }
      .btn-main-menu{
        background: #3595a8;
        border-radius: 20px;
        padding: 20px !important;
        color: white;
        cursor: pointer;
        border:3px solid transparent;
        /*min-height:100px;*/
      }
      .btn-main-menu:hover{
        border:2px solid #3595a8;
        background-color: white;
        color: #1b7baf;
      }
      .ourvalues img{
        height:70px;
      }
      .main-title{
        color: #3595a8;
      }

      .ourvalues h3{
        font-size: 25px;
      }
      .box-register label.letter-black{
        margin-bottom:3px;
        font-size: 13px;
      }

       .participate {
    background-color: white;
      }
     
       .participate i{
      
    float: left;
    margin-right: 20px;
    color: #ef5b2b ;

      }

      .participate-content{
        font-size: 17px;
      }
     

      @media screen and (min-width: 450px) and (max-width: 1024px) {
        .logoDescription{
          width: 60%;
          margin:auto;
        }
      }

      @media (max-width: 1024px){
        #customHeader{
          margin-top: -1px;
        }
      }
      @media (max-width: 768px){

      }

    .mapBackground{
        /*background-image: url(/ph/assets/449afa38/images/city/cityDefaultHead_BW.jpg);*/
        background-color: #f6f6f6;
        background-repeat: no-repeat;
        background-position: 0px 0px;
        background-size: 100% auto;
        height: 500px;
    }

    #mapCircuit #menuRightmapCircuit{
        position: absolute;
    }

    #mapCircuit .closeMenuRight{
        position: absolute;
        right: 280px;
    }

    #listCategory.affix{
        top:58px;
        z-index:30000;
        background-color: white; 
    }

    </style> 

    <!-- <div class="col-sm-12 col-md-12 col-xs-12 no-padding"> -->
        
          
    <div class="no-padding col-xs-12 " style="background-color: #f6f6f6;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left no-padding shadow2" style="background-color: #fff;">
            <div class="col-xs-12 padding-10" style="font-size:18px; text-align:justify">
                <center>
                    Ce site est une plateforme participative <b>d'initiatives</b> de circuits courts d’alimentation et de production locales à la Réunion réunissant des producteurs locaux, des <b><a href="https://www.economie.gouv.fr/ess/amap-cest-quoi" class="" target="_blanc">AMAP</a></b>,  des épiceries,  des jardins partagés, des <b><a href="https://www.cohesion-territoires.gouv.fr/tiers-lieux" class="" target="_blanc">tiers lieux</a></b>, des marchés, des offres de livraisons à domicile et des points relais de distribution dans les quartiers.
                </center>
                <br/>
                A l’heure du confinement dû au Covid-19 et afin que la population puisse trouver près de chez elle des sources d’approvisionnement alimentaires, en respectant un minimum de déplacement et de contacts, ce site à toute son importance !
                <br/><br/>
                Il est également important que chacun soit acteur en partageant et en participant à ces initiatives : s’approvisionner auprès des producteurs locaux, faire vivre les AMAP,  participer à des épiceries collaboratives, rejoindre des jardins partagés, collaborer dans des tiers lieux, gérer des groupements d’achat citoyens, se définir en tant que points relais de distribution dans son quartier (association, citoyen, ..., autre) et bénéficier de livraisons à domicile.
                Ce référencement n'est pas exhaustif et sera mis à jour au fur et à mesure, si vous connaissez des initiatives dans ces domaines, 

                <br/>
                <center><a href="javascript:;" data-form-type="organization" class="btn-add-home btn btn-default btn-open-form bg-green margin-bottom-10"><span>Ajouter une initiative</span></a></center>
                <br/>
                La plateforme est en évolution constante, merci de votre compréhension.
            </div>
                        
            <div class="col-xs-12 padding-10 text-center" id="listCategory">
				<button class="btn btn-default tagCC bg-red text-white " data-tag="all">Tous</button>
				<?php
                    $category = PHDB::find( Badge::COLLECTION, array("source.key" => "circuitCourt") );
                    if(!empty($category)){
                        foreach ($category as $keyC => $valC) {
                            echo '<button class="btn btn-default bg-dark text-white tagCategory" data-idtag="'.$keyC.'">'.$valC['name'].'</button>';
                        }
                    }
                ?>
            </div>
            <div class="col-xs-12 mapBackground no-padding" id="mapCircuit"></div>
            <!-- </div> -->
        </div>
    </div>

</div>

<script type="text/javascript">
var mapCircuitHome = {};
var dataSearchCC = {} ;

var category = <?php echo json_encode($category) ; ?> ;
jQuery(document).ready(function() {
    setTitle("Circuit Court");
    dataSearchCC=searchInterface.constructObjectAndUrl();
    dataSearchCC.searchType = ["projects", "organizations"];
    dataSearchCC.sourceKey = "circuitCourt";
    dataSearchCC.indexStep = 0;

    var paramsMapCC = {
		container : "mapCircuit",
		activePopUp : true,
		tile : "maptiler",
		forced : {
			latLon : [-21.135745255030592,55.52918591952641],
			zoom : 10
		},
		menuRight : {
            close : false,
            btnClose : {
                right : "280px"
            }
        },
        activePreview : true,
        doubleClick : true
    };
    mapCircuitHome = mapObj.init(paramsMapCC);
    allMaps.maps[paramsMapCC.container]=mapCircuitHome;

    ajaxPost(null, baseUrl+"/" + moduleId + "/search/globalautocomplete", dataSearchCC, 
    	function(data){ 
            mylog.log(">>> success autocomplete search data in cc!!!! ", data); //mylog.dir(data);
            if(!data){ 
              toastr.error(data.content); 
            } else { 
              mapCircuitHome.addElts(data.results);
              //allMaps.maps[paramsMapCC.container]=mapCircuitHome;
            }
        }, function (data){
			mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false;     
        });
   //  $.ajax({
   //      type: "POST",
   //      url: baseUrl+"/" + moduleId + "/search/globalautocomplete",
   //      data: dataSearchCC,
   //      dataType: "json",
   //      error: function (data){
			// mylog.log(">>> error autocomplete search"); 
			// mylog.dir(data);   
			// $("#dropdown_search").html(data.responseText);  
			// //signal que le chargement est terminé
			// loadingData = false;     
   //      },
   //      success: function(data){ 
   //          mylog.log(">>> success autocomplete search data in cc!!!! ", data); //mylog.dir(data);
   //          if(!data){ 
   //            toastr.error(data.content); 
   //          } else { 
   //            mapCircuitHome.addElts(data.results);
   //            allMaps.maps[paramsMapCC.container]=mapCircuitHome;
   //            //j {lat: -21.135745255030592, lng: 55.52918591952641}
   //            setTimeout(function(){
   //              //mapCircuitHome.map.setView([-21.135745255030592,55.52918591952641], 10);
   //            },1000);
   //          }
   //      }
   //  });
	//mapCircuitHome.map.setZoom(mapCircuitHome.mapOpt.zoom);
    $(".tagCC").click(function () {
		mylog.log(".tagCC", $(this).data("tag"));
		$(".tagCC").removeClass("bg-red").addClass("bg-dark");
		mapCircuitHome.filtres.tagsActived = [];
		if($(this).data("tag") != "all")
			mapCircuitHome.filtres.tagsActived.push($(this).data("tag"));
		$(this).removeClass("bg-dark").addClass("bg-red");
		mapCircuitHome.clearMap();
		mapCircuitHome.addElts(mapCircuitHome.data);
		
	});

    $(".tagCategory").click(function () {
        mylog.log(".tagCategory", $(this).data("idtag"));
        $(".tagCategory").removeClass("bg-red").addClass("bg-dark");
        mapCircuitHome.filtres.tagsActived = [];
        if($(this).data("idtag") != "all" && 
            typeof category[$(this).data("idtag")] != "undefined" && 
            typeof category[$(this).data("idtag")]["tags"] != "undefined"
            ){
        	mapCircuitHome.filtres.tagsActived = category[$(this).data("idtag")]["tags"];
        mapCircuitHome.filtres.tagsActived.push(category[$(this).data("idtag")]["name"])
        }
            
        $(this).removeClass("bg-dark").addClass("bg-red");
        mapCircuitHome.clearMap();
        mapCircuitHome.addElts(mapCircuitHome.data);
        
    });

    $(".btn-add-home").click(function(){
        var typeForm = $(this).data("form-type");
        mylog.log("test", $(this).data("form-subtype")),
        currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
        //alert(contextData.type+" && "+contextData.id+" : "+typeForm);
        if(contextData && contextData.type && contextData.id )
            dyFObj.openForm(typeForm,"sub");
        else
            dyFObj.openForm(typeForm);
    });
});
 

</script>


