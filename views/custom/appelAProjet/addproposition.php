<div class="col-md-10 form-control" >
    <label class="label text-black">
        Choisir le formulaire
    </label>
    <select id="ap_<?php echo $formData["el"]["_id"] ?>" class="">
        <option value=""> choisir </option>
        <?php
            if (isset($formData["forms"])) {
                foreach ($formData["forms"] as $fid => $fdata){
                    if (isset($fdata["type"]) && $fdata["type"] == "aap") {
        ?>
        <option value="<?php echo $fid ?>"> <?php echo $fdata["name"] ?></option>
        <?php
                    }
                }
            }
        ?>
    </select>
</div>

<div id="form_addproposition" class="col-md-offset-1 col-md-10" >

</div>

<script type="text/javascript">
    var formData = <?php echo json_encode($formData) ?>;

    jQuery(document).ready(function() {

        $('#ap_<?php echo $formData["el"]["_id"] ?>').off().on("change",function() {

            if ($('#ap_<?php echo $formData["el"]["_id"] ?>').val() != "") {
                coInterface.showLoader("#form_addproposition");
                $id=null, $answer=null, $mode=null, $form=null, $formstep=null, $inputid=null, $viewmode=null
                ajaxPost('#form_addproposition', baseUrl + '/survey/answer/getstep/id/new/mode/w/form/' +$('#ap_<?php echo $formData["el"]["_id"] ?>').val()+ '/formstep/aapStep1' ,
                    null,
                    function (data) {

                    }, "html");
            }
        });

    });
</script>
