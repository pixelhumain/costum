<div id="project-action-modal">
	<?php
	$statuses = [
		"discuter",
		"next",
		"todo",
		"tracking",
		"totest",
		"done"
	];
	$tags = [];
	$files = [
		'/plugins/xdan.datetimepicker/jquery.datetimepicker.min.css',
		'/plugins/xdan.datetimepicker/jquery.datetimepicker.full.min.js'
	];
	HtmlHelper::registerCssAndScriptsFiles($files, Yii::app()->getBaseUrl());
	$files = [
		'/js/action_modal/main.js',
		'/css/action_modal.d/style.css'
	];
	HtmlHelper::registerCssAndScriptsFiles($files, Yii::app()->getModule('costum')->getAssetsUrl());
	?>
	<div class="modal fade action-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close close-panel-action" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title"><?= $parent_name ?> <span class="action-modal-name"></span></h4>
				</div>
				<div class="modal-body">
					<aside class="action-list co-scroll">
						<div class="filter">
							<div class="input-group">
								<div class="input-group-addon">
									<span class="fa fa-filter"></span>
								</div>
								<input type="text" class="search form-control">
							</div>
							<div class="action-filter-tags"></div>
						</div>
						<?php
						foreach ($statuses as $status) :
						?>
							<div class="action-status-group" data-status="<?= $status ?>"> <?= Yii::t("common", "act.$status") ?> (0) </div>
							<?php
							$actions = array_filter($action_list, function ($action) use ($status) {
								return ($action["view_status"] === $status);
							});
							foreach ($actions as $action) :
								if (!empty($action["tags"]) && is_array($action["tags"])) {
									foreach ($action["tags"] as $tag) {
										if (!in_array($tag, $tags))
											$tags[] = $tag;
									}
								}
							?>
								<div class="action-thumb" data-id="<?= $action["id"] ?>">
									<div class="actions">
										<i class="fa fa-arrows dragger for-admin not-admin-hide"></i>
									</div>
									<div class="image">
										<img src="<?= $action['image']['url'] ?>" alt="<?= $action['image']['name'] ?>">
									</div>
									<div class="title">
										<?= $action['name'] ?>
									</div>
								</div>
						<?php
							endforeach;
						endforeach;
						?>
						<div class="action-status-referer"></div>
					</aside>
					<aside class="action-filter co-scroll">
						<div class="btn btn-default back-to-list pull-right">
							<span class="fa fa-list"></span>
						</div><br>
						<label>Status</label>
						<?php foreach ($statuses as $status) : ?>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="status-filter" value="<?= $status ?>" checked="checked">
									<?= ucfirst(Yii::t("common", "act.$status")) ?>
								</label>
							</div>
						<?php endforeach; ?>
						<label>Tags/Thématique</label>
						<select name="tags" multiple="multiple">
							<?php foreach ($tags as $tag) : ?>
								<option value="<?= $tag ?>"><?= $tag ?></option>
							<?php endforeach; ?>
						</select>
						<label><?= Yii::t("common", "Milestone") ?></label>
						<select name="milestones" class="form-control">
							<option value="" selected>Tous</option>
							<?php foreach ($milestones as $milestone) : ?>
								<option value="<?= $milestone["milestoneId"] ?>"><?= $milestone["name"] ?></option>
							<?php endforeach; ?>
						</select>
					</aside>
					<div class="aside-overlay"></div>
					<main class="action-detail co-scroll">
						<button class="btn btn-default show-action-list">
							<i class="fa fa-list"></i>
						</button>
						<button class="btn btn-primary create-action for-admin not-admin-disable">
							<i class="fa fa-plus"></i>
							Créer une action
						</button>
						<div class="dropdown milestone-dropdown">
							<button class="btn btn-default dropdown-toggle for-admin not-admin-disable" type="button" id="milestone-dropdown" data-toggle="dropdown">
								<span class="fa fa-flag"></span> <span class="selected-milestone"><?= Yii::t('common', 'Milestone') ?></span>
							</button>
							<ul class="dropdown-menu" role="menu" aria-labelledby="milestone-dropdown">
								<?php foreach ($milestones as $milestone) : ?>
									<li role="presentation" data-id="<?= $milestone["milestoneId"] ?>"><a role="menuitem" tabindex="-1" class="milestone" href="<?= $milestone["milestoneId"] ?>"><?= $milestone["name"] ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="action-header">
							<p class="action-title"></p>
							<div class="setting-container">
								<div class="credit-field"></div>
								<button class="setting-button for-admin not-admin-hide">
									<i class="fa fa-cog"></i>
								</button>
								<button class="btn-invite-member"><i class="fa fa-plus"></i> Invite</button>
								<button class="btn-invite-outside"><i class="fa fa-user"></i> Invite par mail</button>
								<div class="invite-container d-none"></div>
							</div>
							<div class="creator-image">
								<img src="" alt="">
							</div>
						</div>
						<div class="contributors-tasks-container">
							<div class="action-contributors">
								<button class="btn-participer" data-toggle="tooltip" title="Participer">
									<span class="fa fa-user-plus"></span>
								</button>
								<!-- <div class="hidden-info-button rounded" style="margin-right: 10px;" data-target="dropdown-fire">
									<span class="button-hidden-info">
										Participer
									</span>
									<div class="span button-icon">
										<span class="fa fa-link"></span>
									</div>
								</div> -->
								<div class="contributor-list-dropdown for-admin not-admin-disable">
									<div class="hidden-info-button rounded" data-target="dropdown-fire">
										<span class="button-hidden-info">
											Participants
										</span>
										<div class="span button-icon">
											<span class="fa fa-group"></span>
										</div>
									</div>
									<div class="contributor-selector">
										<div class="contributor-list"></div>
									</div>
								</div>
								<div class="contributors-container"></div>
							</div>
							<div class="task-btn-container">
								<button type="button" class="btn btn-primary openActionComment"><i class="fa fa-commenting-o"> </i> Commentaire</button>
								<button class="btn btn-primary btn-tasks"><span class="fa fa-tasks"></span> Tasks</button>
								<button class="btn btn-danger btn-delete-action for-admin not-admin-disable"><span class="fa fa-archive"></span> Archiver</button>
								<button class="btn btn-danger btn-cancel-action for-admin not-admin-hide"><span class="fa fa-undo"></span> Annuler</button>
							</div>
						</div>
						<div class="action-tags"></div>
						<div class="duration-status default-status">
							<div class="dropdown">
								<div class="dropdown-toggle for-admin not-admin-disable" data-toggle="dropdown">
									Status
								</div>
								<ul class="dropdown-menu action-status" role="menu">
									<li role="presentation"><a role="menuitem" tabindex="-1" href="discuter">À discuter</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="next">Next</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="todo">À faire</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="tracking">En cours</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="totest">À tester</a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1" href="done">Terminé</a></li>
								</ul>
							</div>
							<div class="action-duration">
								<div class="start" data-target="startDate">
									<span class="fa fa-calendar for-admin not-admin-disable"></span> Début : <span class="date-preview for-admin not-admin-disable">__/__/____ __:__</span>
									<input type="text" class="hidden-datepicker" hidden="hidden">
								</div>
								<div class="end" data-target="endDate">
									<span class="fa fa-calendar for-admin not-admin-disable"></span> Fin : <span class="date-preview for-admin not-admin-disable">__/__/____ __:__</span>
									<input type="text" class="hidden-datepicker" hidden="hidden">
								</div>
							</div>
						</div>
						<div class="content-container">
							<div class="action-description-container">
								<div class="image-upload for-admin not-admin-hide">
									<button class="btn btn-primary"><i class="fa fa-upload"></i> Importer des images</button>
									<input type="file" name="import-images" multiple>
									<input type="text" class="form-control" style="margin-top: 10px;" placeholder="Coller vos images ici">
								</div>
								<div class="image-compact-list"></div>
								<div class="action-description for-admin not-admin-disable"></div>
								<button class="btn btn-primary save-description">Enregistrer</button>
							</div>
							<div class="modules-container"></div>
						</div>
					</main>
				</div>
			</div>
		</div>
	</div>
	<script>
		var php = {
			action_list: JSON.parse(JSON.stringify(<?= json_encode($action_list) ?>)),
			server_url: JSON.parse(JSON.stringify(<?= json_encode($server_url) ?>)),
			parent_room: JSON.parse(JSON.stringify(<?= json_encode($parent_room) ?>)),
			parent_id: JSON.parse(JSON.stringify(<?= json_encode($parent_id) ?>)),
			parent_type: JSON.parse(JSON.stringify(<?= json_encode($parent_type) ?>)),
			milestones: JSON.parse(JSON.stringify(<?= json_encode($milestones) ?>)),
			default_action: JSON.parse(JSON.stringify(<?= json_encode($selected) ?>)),
			user: "<?= $user ?>",
		}
		action_modal(window, jQuery, php);
	</script>
</div>