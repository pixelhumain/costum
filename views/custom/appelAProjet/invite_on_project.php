<?php

?>
<style>
    .input-invite .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{
        color: #000 !important;
        border: 1px #fff solid !important;
    }
    .input-invite .select2-container {
        display: block !important;
    }
    .input-invite .form-group #search-persone {		
        border-bottom: 2px gray solid !important;
        box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 1px 0px, rgba(61, 59, 53, 0.16) 0px 0px 0px 1px, rgba(61, 59, 53, 0.08) 0px 2px 5px 0px !important;
        border-radius: 3px !important;
        padding: 0 12px !important;
    }
    .input-invite {
        width: 100%;
    }
    .input-invite label {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .close-invite-container {
        padding: 0;
        cursor: pointer;
        border: 0;
        font-size: 21px;
        font-weight: 700;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
    }
    .d-none {
        display: none !important;
    }

    .select2-drop-active {
        z-index: 99999999999999999;
    }
</style>

<div class="no-padding input-invite">
    <div class="form-group">
        <label for="">
            <h4 class="">
                Inviter des personnes à contribuer
            </h4>
            <span class="close-invite-container"><i class="fa fa-times"></i></span>
        </label>
        <input type="text" class="margin-right-10 exclude-input" value="" id="search-persone" placeholder="">
    </div>
</div>
<script>
    var activeAction = JSON.parse(JSON.stringify(<?= json_encode($selected) ?>));
    var info = JSON.parse(JSON.stringify(<?= json_encode($info) ?>));
    function getContributors(id) {
		return new Promise(function(resolve) {
			var url = baseUrl + "/costum/project/action/request/all_contributors";
			ajaxPost(null, url, {
				id: id
			}, resolve);
		});
	}
    getContributors(activeAction).then(function(contributors){
        var currentValue = [];
        var objectValue = {}
        contributors.map(function(contributor) {
            objectValue[contributor.id] = {
                id : contributor.id,
                name : contributor.name
            }
            currentValue.push(contributor.id)
        })
        $("#search-persone").val(currentValue.join(',')).select2({
            minimumInputLength: 3,
            //tags: true,
            multiple:true,
            "tokenSeparators": [','],
            createSearchChoice: function (term, data) {
                // if (!data.length)
                //     return {
                //         id: term,
                //         text: term
                //     };
            },
            initSelection : function (element, callback) {
                mylog.log('element',element.val())
                var data = [];
                $(element.val().split(",")).each(function () {
                    //data.push({id: this, text: this});_inviteObj
                    data.push({id: this, text: typeof objectValue[this] != 'undefined' ? objectValue[this].name : ''});
                });
                callback(data);
            },
            ajax: {
                url: baseUrl+"/"+moduleId+"/search/globalautocomplete",
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function (term) {
                    return {
                        name: term,
                        searchType : ["citoyens"],
                        filters : {
                            '$or' : {
                                "name" : {'$exists': true},
                            }
                        }
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(Object.values(data.results), function (item) {
                            return {
                                text: item.name,
                                id: item._id.$id
                            }
                        })
                    };
                }
            }
        }).on('change',function(e){
            if(exists(e.added)){
                var params = {
                    parentId : info.parentId,
                    parentType : info.parentType,
                    listInvite : {
                        citoyens :{},
                        organizations:{}
                    }
                }
                params.listInvite.citoyens[e.added.id] = e.added.text
                ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                    toastr.success(trad.saved);
                    if(info) {
                        ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",{
                            parentId : info.parentId,
                            parentType : info.parentType,
                            listInvite : {...params.listInvite}
                        },function(data){})
                    }
                })
            }
            if(exists(e.removed)){
                links.disconnect(info.parentType,info.parentId,e.removed.id,'citoyens','contributors', (data) => {
                    if(info) {
                        links.disconnectAjax(info.parentType,info.parentId,e.removed.id,'citoyens','contributors', null, () => {

                        }, {
                            showNotification : false
                        })
                    }
                })
            }
        });
    })
    $('.close-invite-container').click(function(){
        $('.input-invite').parent().addClass('d-none')
        $('.select2-drop-active').css('display','none')
    })
</script>
