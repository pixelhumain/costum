<style>
    .portfolio-modal .modal-content .aap-tpl-container {
        text-align: initial;
    }
    .aap-tpl-container .blogicon
    {
        font-size: 217px;
        color: #5CB85C;
    }

    .aap-tpl-container .ratetext
    {
        font-size: 37px;
        text-decoration: underline;
        padding-bottom: 10px;
    }

    .aap-tpl-container .votes
    {
        font-size: 47px;
        padding-right: 20px;
        color: #197BB5;
    }

    .aap-tpl-container a.list-group-item
    {
        height: auto;
        min-height: 150px;
    }

    .aap-tpl-container a.list-group-item:hover, a.list-group-item:focus
    {
        border-left: 10px solid #5CB85C;
        border-right: 10px solid #5CB85C;
    }

    .aap-tpl-container a.list-group-item
    {
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
    }

    .aap-tpl-container .btnquest{
        font-size: 14px;
        border-color: transparent;
        padding-top: 2px;
        padding-bottom: 2px;
        background: transparent;
    }
    .aap-tpl-container .btnquest:hover{
        color: #9fbd38;
    }
    .aap-tpl-container .popover-content small
    {
        text-transform: initial !important;
    }
    .aap-tpl-container .popover.bottom>.arrow:after {
        top: 1px !important;
    }
    .aap-tpl-container .popover.bottom>.arrow{
        top: -11px !important;
    }

    .aap-tpl-container h5.formtag
    {
        color: #9fbd38;
    }
    .aap-tpl-container .template-description {
        /* overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        /-webkit-line-clamp: 3;
        -webkit-box-orient: vertical;*/
        text-decoration: none;
        font-size: 14px;
    }
    .aap-tpl-container .btn-tag{
        background: #9fbd38;
    }
</style>
<?php
$el = PHDB::findOneById($context["type"],$context["id"]);
$templatesaap = PHDB::find(Form::COLLECTION, array(
    "parent." .(string)$el['_id'] => array(
        '$exists' => true
    ) ,
    "type" => "aapConfig"
));

$templates = PHDB::find(Form::COLLECTION, array(
    "parent." . (string)$el['_id'] => array(
        '$exists' => true
    ) ,
    "type" => "template"
));
$explainAAP =   Yii::t("survey","A call for projects is a funding vehicle, a selection process and a request for peer review. It is used by public authorities, institutional bodies or private funders to respond to a particular problem by providing financial support for its resolution"); 

?>
<div class="col-md-12 col-xs-12 aap-tpl-container margin-top-30">
    <div class="row">
        <h3><?php echo Yii::t("survey","Your templates") ?></h3>
        <span></span>
        <?php
        if (!empty($templatesaap))
        {
            foreach ($templatesaap as $templateid => $template)
            {
                $idParent = array_keys($template["parent"]) [0];
                $creator = Person::getSimpleUserById(@$template["creator"]);
                ?>
                <div class="row list-group-item margin-bottom-10">
                    <a href="javascript:;" class="margin-bottom-5" style="text-decoration: none;" data-form='<?=htmlspecialchars(json_encode($template) , ENT_QUOTES, 'UTF-8') ?>' >
                        <div class="media col-md-3">
                            <figure class="pull-left" style="width:100%">
                                <!-- <i class="fa fa-cloud-upload blogicon"></i> -->
                                <img src="<?=Yii::app()->getModule("co2")->assetsUrl . "/images/thumbnail-default.jpg"; ?>" alt="" style="height:auto;width:100%"/>
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <h4 class="list-group-item-heading"><?=$template["name"] ?></h4>
                            <h5 class="formtag margin-top-5 margin-bottom-5"><?php echo Yii::t("survey","Template call for proposals") ?>
                                <button class="btn btn-sm btn-default btnquest " title="Info" data-toggle="popover" data-placement="bottom" data-html="true" data-trigger="focus" data-animation="true" data-content="<small><?=$explainAAP ?></small>">
                                    <i class="fa fa-question-circle-o"></i>
                                </button>
                            </h5>
                            <h6 style="font-size: 12px;text-transform:initial" class="margin-top-5 margin-bottom-5"><i><?=!empty($creator["name"]) ? "Créer par <a href='#page.type.citoyens.id." . $template["creator"] . "' class='lbh-preview-element'>" . $creator["name"] . "</a>" : " " ?></i> le <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i:s ', @$template["created"]); ?></h6>
                            <h6 style="font-size: 12px;text-transform:initial" class="margin-top-5 margin-bottom-5"><i><?=!empty($template["parent"][$idParent]["name"]) ? "Parent: <a href='#page.type." . $template["parent"][$idParent]["type"] . ".id." . $idParent . "' class='lbh-preview-element'>" . $template["parent"][$idParent]["name"] . "</a>" : " " ?></i></h6>
                            <p class="list-group-item-text template-description">
                                <?=@$template["description"] ?>
                            </p>
                            <ul class="tag-list">
                                <?php
                                if (isset($template['tags']))
                                {
                                    foreach ($template['tags'] as $ktags => $vtags){ 
                                        if(!empty($vtags)){ ?>
                                            <a href="javascript:;" class="template-tag-panel" data-tag="<?=$vtags
                                            ?>">
                                                        <span class="badge bg-transparent btn-tag tag" data-tag-value="cte" data-tag-label="cte"><?= "#".$vtags
                                                            ?></span>
                                            </a>
                                            <?php
                                        }
                                    }
                                }

                                ?>
                            </ul>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <!-- <div class="ratetext">20 utilisateur</div> -->
                                    <!-- <i class="fa fa-thumbs-o-up votes"></i><i class="fa fa-thumbs-o-down votes"></i> -->
                                    <!-- <div class="stars"></div> -->
                                    <h5><a href="javascript:;" class="editAapConfig" data-edit="<?=(isset($template["subType"]) && $template["subType"] == "ocecoformConfig") ? "ocecoform" : "aap" ?>"><i class="fa fa-cog"></i> <?php echo Yii::t("survey","Configure information") ?></a></h5>
                                    <?php if (Authorisation::isElementAdmin((string)$el['_id'], $el["collection"], Yii::app()->session["userId"]))
                                    { ?>
                                        <h5><a href="javascript:;" class="configcoform" data-id="<?php echo $templateid ?>" ><i class="fa fa-cog"></i><?php echo Yii::t("survey","Configure questionnaire") ?> </a></h5>
                                        <?php
                                    }
                                    if (Authorisation::isElementAdmin((string)$el['_id'], $el["collection"], Yii::app()->session["userId"]) && Form::canBeDeleted($templateid))
                                    { ?>
                                        <h5><a href="javascript:;" class="deleteFormBtn" data-id="<?php echo $templateid ?>" ><i class="fa fa-trash"></i> <?php echo Yii::t("common","Remove") ?></a></h5>
                                        <?php
                                    } ?>
                                    <div id="rate1"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <?php
            }
        }
        if (!empty($templates))
        {
            foreach ($templates as $templateid => $template)
            {
                ?>
                <div class="row list-group-item margin-bottom-10">
                    <a href="javascript:;" class="margin-bottom-5" style="text-decoration: none;" data-form='<?=htmlspecialchars(json_encode($template) , ENT_QUOTES, 'UTF-8') ?>' >
                        <div class="media col-md-3">
                            <figure class="pull-left" style="width:100%">
                                <!-- <i class="fa fa-cloud-upload blogicon"></i> -->
                                <img src="<?=Yii::app()->getModule("co2")->assetsUrl . "/images/thumbnail-default.jpg"; ?>" alt="" style="height:auto;width:100%"/>
                            </figure>
                        </div>
                        <div class="col-md-6">
                            <h4 class="list-group-item-heading"><?=$template["name"] ?></h4>
                            <p class="list-group-item-text template-description">
                                <?=@$template["description"] ?>
                            </p>
                            <ul class="tag-list">
                                <?php
                                if (isset($template['tags']))
                                {
                                    foreach ($template['tags'] as $ktagg => $vtagg){ 
                                        if(!empty($vtags)){ ?>
                                            <a href="javascript:;" class="template-tag-panel" data-tag="<?=$vtagg
                                            ?>">
                                            <span class="badge bg-transparent btn-tag tag" data-tag-value="cte" data-tag-label="cte"><?="#".$vtagg
                                                ?></span>
                                            </a>
                                            <?php
                                        }
                                    }
                                }

                                ?>
                            </ul>
                        </div>
                        <div class="col-md-3 text-center">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <!-- <div class="ratetext">20 utilisateur</div> -->
                                    <!-- <i class="fa fa-thumbs-o-up votes"></i><i class="fa fa-thumbs-o-down votes"></i> -->
                                    <!-- <div class="stars"></div> -->
                                    <h5><a href="javascript:;" class="btnConfigForm" data-form='<?=htmlspecialchars(json_encode($template) , ENT_QUOTES, 'UTF-8') ?>'><i class="fa fa-cog"></i> <?php echo Yii::t("survey","Configure information") ?></a></h5>
                                    <?php if (Authorisation::isElementAdmin((string)$el['_id'], $el["type"], Yii::app()->session["userId"]))
                                    { ?>
                                        <h5><a href="javascript:;" class="configcoform" data-id="<?php echo $templateid ?>" ><i class="fa fa-cog"></i> <?php echo Yii::t("survey","Configure questionnaire") ?></a></h5>
                                        <?php
                                    }
                                    if (Authorisation::isElementAdmin((string)$el['_id'], $el["type"], Yii::app()->session["userId"]) && Form::canBeDeleted($templateid))
                                    { ?>
                                        <h5><a href="javascript:;" class="deleteFormBtn" data-id="<?php echo $templateid ?>" ><i class="fa fa-trash"></i> <?php echo Yii::t("common","Remove") ?></a></h5>
                                        <?php
                                    } ?>
                                    <div id="rate1"></div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <?php
            }
        }
        ?>

        <?php
        if (empty($templatesaap))
        {
            ?>
            <div class="col-xs-12 padding-top-5 padding-bottom-10">
                <button type="button" class="btn btn-block btn-lg btn-primary generateAapConfig"> <i class="fa fa-plus"></i> &nbsp; <?php echo Yii::t("survey","Create a template for your") ?> <?=$el["collection"] . " " . $el["name"] ?> </button>
            </div>
            <?php
        }
        ?>
    </div>
    <h3><?php echo Yii::t("survey","New coform from these templates") ?> :</h3>
    <div class="row">
        <div class="well">
            <div class="list-group">
                <?php //var_dump($contextta"])
                ?>
                <?php
                if (isset($results))
                {
                    if(is_array($results)){
                        $results = array_map(function($num){
                            return new MongoId($num);
                        },$results);
                        $results = PHDB::find(Form::COLLECTION,array("_id" => ['$in'=>$results]));
                        foreach ($results as $idtemp => $vtemp)
                        {
                            if (!empty($vtemp["parent"]) && empty($vtemp["notShowInListTemplate"])){
                                $idParent = array_keys($vtemp["parent"]) [0];                                $user = Person::getSimpleUserById(@$vtemp["creator"]);
                                $templateUserCounter = PHDB::count(Form::COLLECTION, ["config" => $idtemp]);
                                $name = @$vtemp["name"];
                                $stepValidation = intval(@$vtemp["hasStepValidations"]);
                                $description = !empty($vtemp["description"]) ? $vtemp["description"] : "Pas de description";
                                $tag = (!empty($vtemp["type"]) && $vtemp["type"] == "aapConfig") ? '<h5 class="formtag margin-top-5 margin-bottom-5"> Appel à projet 
                                                                                                            <button class="btn btn-sm btn-default btnquest " title="Info" data-toggle="popover" data-html="true" data-placement="bottom" data-trigger="focus" data-animation="true"  data-content="<small>' . $explainAAP . '</small>">
                                                                                                            <i class="fa fa-question-circle-o"></i>
                                                                                                        </button> ' : '<h5 class="formtag margin-top-5 margin-bottom-5">';
                                $tag .= !empty($vtemp["formtag"]) ? $vtemp["formtag"] . join(" ") : '';
                                $tag .= "</h5>";
                                $params = @$vtemp["params"];
                                $formParent = ["name" => $name, "description" => $description, "config" => $idtemp, "parent" => [(string)$el["_id"] => ["type" => $el["collection"], "name" => $el["name"]]], "subForms" => [

                                ],"params"=>$params,"hasStepValidations" => $stepValidation];

                                $jstag = "";

                                if (isset($vtemp["type"]) && $vtemp["type"] == "aapConfig")
                                {
                                    $jstag = "generateParentForm";
                                }
                                else
                                {
                                    $jstag = "generatetemplatechild";
                                }

                                //var_dump($formParent);
                                if (!empty($vtemp["subForms"]))
                                {
                                    foreach ($vtemp["subForms"] as $ksubF => $vsubF)
                                    {
                                        if (!empty($vsubF["active"]) && $vsubF["active"] == "true") $formParent["subForms"][] = $ksubF;
                                    }
                                }

                                $subForm = [];
                                //var_dump($vtemp["type"]);exit;
                                if (!empty($vtemp["subForms"]) && isset($vtemp["type"]) &&  $vtemp["type"]!= "aapConfig")
                                {
                                    foreach ($vtemp["subForms"] as $vtsid => $vts)
                                    {
                                        $formuniqid = $vts . $el["slug"] . uniqid();

                                        array_push($formParent["subForms"], $formuniqid);

                                        $sbform = PHDB::findOne(Form::COLLECTION, array(
                                            "id" => $vts
                                        ));

                                        $sbform["id"] = $formuniqid;

                                        unset($sbform->_id);

                                        $subForm[$formuniqid] = $sbform;
                                    }
                                }
                                ?>
                                <div class="row list-group-item margin-bottom-10">
                                    <a href="javascript:;" style="text-decoration: none;" class="margin-bottom-5 <?php echo $jstag ?> " data-parent-id="<?= $idParent ?>" data-form='<?=htmlspecialchars(json_encode($formParent) , ENT_QUOTES, 'UTF-8') ?>' data-template='<?=$idtemp ?>' data-subform='<?=htmlspecialchars(json_encode($subForm) , ENT_QUOTES, 'UTF-8') ?>' data-subType='<?=(isset($vtemp["subType"]) && $vtemp["subType"] == "ocecoformConfig") ? "ocecoform" : "aap" ?>'>
                                        <div class="media col-md-3">
                                            <figure class="pull-left" style="width:100%">
                                                <!-- <i class="fa fa-cloud-upload blogicon"></i> -->
                                                <img src="<?=Yii::app()->getModule("co2")->assetsUrl . "/images/thumbnail-default.jpg"; ?>" alt="" style="height:auto;width:100%"/>
                                            </figure>
                                        </div>
                                        <div class="col-md-6">
                                            <h4 class="list-group-item-heading"><?=$name ?></h4>
                                            <?=$tag
                                            ?>
                                            <h6 style="font-size: 12px;text-transform:initial" class="margin-top-5 margin-bottom-5"><i><?=!empty($user["name"]) ? "<?php echo Yii::t('form','Create by') ?> <a href='#page.type.citoyens.id." . $vtemp["creator"] . "' class='lbh-preview-element'>" . $user["name"] . "</a>" : " " ?></i> le <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i:s ', @$vtemp["created"]); ?></h6>
                                            <h6 style="font-size: 12px;text-transform:initial" class="margin-top-5 margin-bottom-5"><i><?=!empty($vtemp["parent"][$idParent]["name"]) ? "Parent: <a href='#page.type." . $vtemp["parent"][$idParent]["type"] . ".id." . $idParent . "' class='lbh-preview-element'>" . $vtemp["parent"][$idParent]["name"] . "</a>" : " " ?></i></h6>
                                            <p class="list-group-item-text template-description">
                                                <?=$description
                                                ?>
                                            </p>
                                            <ul class="tag-list">
                                                <?php
                                                if ( isset($vtemp['tags']) && !empty($vtemp['tags']))
                                                {
                                                    foreach ($vtemp['tags'] as $ktag => $vtag){
                                                    if(!empty($vtag))
                                                        { ?>
                                                            <a href="javascript:;" class="template-tag-panel" data-tag="<?=$vtag
                                                            ?>">
                                                                    <span class="badge bg-transparent btn-tag tag" data-tag-value="cte" data-tag-label="cte"><?= "#".$vtag
                                                                        ?></span>
                                                            </a>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </ul>
                                            <div class="">
                                                <?php
                                                if (!empty($vtemp["type"]) && $vtemp["type"] == "aapconfig")
                                                {
                                                    ?>
                                                    <button class="btn" type="button"><?php echo Yii::t("survey","Apply this call for proposals") ?></button>
                                                    <?php
                                                }
                                                elseif (!empty($vtemp["type"]) && $vtemp["type"] == "aapconfig")
                                                {
                                                    ?>
                                                    <button class="btn" type="button"><?php echo Yii::t("survey","Integrate this coform") ?></button>
                                                    <?php
                                                }
                                                if (!empty($vtemp["copyable"]) && $vtemp["copyable"] == "true")
                                                {
                                                    ?>
                                                    <button class="btn" type="button"><?php echo Yii::t("survey","Add a copy") ?></button>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <!-- <div class="ratetext">20 utilisateur</div> -->
                                                    <!-- <i class="fa fa-thumbs-o-up votes"></i><i class="fa fa-thumbs-o-down votes"></i> -->
                                                    <!-- <div class="stars"></div> -->
                                                    <h2><?=($templateUserCounter == 1 || $templateUserCounter == 0) ? $templateUserCounter . "<small> Utilisation</small>" : $templateUserCounter . "<small> Utilisations</small>" ?>  </h2>
                                                    <div id="rate1"></div>
                                                    <?php if (isset(Yii::app()->session["userId"]) && Authorisation::isElementAdmin((string)$el['_id'], $el["collection"], Yii::app()->session["userId"]) && Form::canBeDeleted($idtemp)){ ?>
                                                    <div class="col-xs-12">
                                                        <button class="btn btn-default deleteFormBtn" data-id="<?= $idtemp ?>"><i class="fa fa-trash"></i></button>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>
    var  copyFormObj =formObj.init();
    $(function(){
        copyFormObj.events.form(copyFormObj);

        $('.configcoform').click(function () {
            $('#openModal').hide();
        });
        $('.deleteFormBtn').off().click( function(){
            var btn= $(this);
            var idF = $(this).data("id");
            bootbox.dialog({
                title: trad.confirmdelete,
                message: "<span class='text-red bold'><i class='fa fa-warning'></i>"+ trad.actionirreversible +"</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            getAjax("",baseUrl+"/survey/form/delete/id/"+idF,function(res){
                                if(res.result){
                                    toastr.success(res.msg);
                                    $("[data-template='"+btn.data("id")+"']").parent().remove();
                                }
                                else 
                                    toastr.error(res.msg);
                            },"html");
                        }
                    },
                    {
                        label: trad.cancel,
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
		});

        $('.generateParentForm').off().on('click',function(){
            var $this = $(this);
            mylog.log(result,"govagova");
            var result = $this.data("subtype");
            if(result!=null){
                var formToGenerate = $this.data("form");
                formToGenerate["type"] = "aap";
                var tplCtx = {
                    collection: "forms",
                    value: formToGenerate,
                    format: true,
                    path: "allToRoot"
                };
            }
            if(result=="aap"){
                formToGenerate["subType"] = result;
                tplCtx.value = formToGenerate
                dataHelper.path2Value(tplCtx, function (params) {
                    if(params.result){
                        toastr.success(trad.formcreated);
                        var options = {
                            proposition : 1,
                            //caracterisation: 2,
                            evaluation:2,
                            financement:3,
                            suivie:4,
                            aap:true,
                            formParent: params.saved._id.$id
                        };
                        var  copyFormObj =formObj.init();

                        var generatedInputs = copyFormObj.aap.generateInputsDocument(copyFormObj, options);
                        if (Object.keys(generatedInputs).length != 0){
                            toastr.success(trad.generatedfields);
                            copyFormObj.dynForm.addDescription(copyFormObj,params.saved)
                        }
                    }
                });
            }else if(result == "ocecoform"){
                formToGenerate["subType"] = result;
                tplCtx.value = formToGenerate
                dataHelper.path2Value(tplCtx, function (params) {
                    if(params.result){
                        toastr.success(trad.formcreated);
                        var options = {
                            proposition : 1,
                            evaluation:2,
                            financement:3,
                            suivie:4,
                            formParent: params.saved._id.$id,
                            ocecoform: true
                        };
                        var  copyFormObj =formObj.init();

                        var generatedInputs = copyFormObj.aap.generateInputsDocument(copyFormObj, options);
                        if (Object.keys(generatedInputs).length != 0){
                            toastr.success(trad.generatedfields);
                            copyFormObj.dynForm.addDescription(copyFormObj,params.saved)
                        }
                    }
                });
            }
        });
        $('.generateAapConfig').off().click(function () {
            aapConfigObj.addConfig();
        });
        $('.editAapConfig').off().click(function () {
            aapConfigObj.editConfig($(this).data("edit"));
        });

        $('.generatetemplatechild').off().on('click',function(){
            var $this = $(this);
            bootbox.prompt({
                title: trad.changemadeinoriginal,
                /*message: '<p>Please select an option below:</p>',*/
                inputType: 'radio',
                inputOptions: [
                    {
                        text:trad.addformapplychange,
                        value: 'apply'
                    },
                    {
                        text: trad.addformwithoutchange,
                        value: 'copy'
                    }
                ],
                callback: function (result) {
                    if(result == "apply"){
                        var formToGenerate = $this.data("form");

                        formToGenerate["type"] = "templatechild";

                        var tplCtx = {
                            collection: "forms",
                            value: formToGenerate,
                            format: true,
                            path: "allToRoot"
                        };
                        dataHelper.path2Value(tplCtx, function (params) {
                            if(params.result){
                                toastr.success(trad.formcreated);
                                urlCtrl.loadByHash(location.hash);
                            }
                        });
                    }else if(result == "copy"){
                        var originId=$this.data("parent-id");
                        ajaxPost(
                            null,
                            baseUrl+"/survey/form/duplicatetemplate/type/template/templateCostumId/"+originId,
                            null,
                            function(data){
                            }
                        );
                        urlCtrl.loadByHash(location.hash);
                        // var formToGenerate = $this.data("form");
                        // var subformToGenerate = $this.data("subform");

                        // formToGenerate["copy"] = "true";

                        // var tplCtx = {
                        //     collection: "forms",
                        //     value: formToGenerate,
                        //     format: true,
                        //     path: "allToRoot"
                        // };
                        // dataHelper.path2Value(tplCtx, function (params) {

                        //     toastr.success(trad.formcreated);
                        //     var cntsub = 1;

                        //     $.each(subformToGenerate , function (index, value) {
                        //         delete subformToGenerate[index]["_id"];
                        //     });

                        //     $.each(subformToGenerate , function (index, value) {

                        //         var tplCtxaa = {
                        //             collection: "forms",
                        //             value: value,
                        //             format: true,
                        //             path: "allToRoot"
                        //         };
                        //         dataHelper.path2Value(tplCtxaa, function (params) {
                        //             // if(params.result){
                        //             toastr.success("étape crée");
                        //             // }
                        //         });
                        //         cntsub++;
                        //     });
                        //     urlCtrl.loadByHash(location.hash);

                        // });
                    }
                }
            });
        });

        var aapConfigObj = {
            aapConfig :function(bootboxResult){
                mylog.log(bootboxResult);
                //var stepsArray = ["Proposition","Caractérisation","Evaluation","Financement","Suivi"];
                var stepsArray = ["Proposition","Evaluation","Financement","Suivi"];
                if(bootboxResult=="ocecoform")
                    stepsArray = ["Proposition","Evaluation","Financement","Suivi"];
                var currentAapConfig = <?=json_encode($templatesaap); ?>;
                currentAapConfig = Object.values(currentAapConfig)[0];

                mylog.log(currentAapConfig,"currentAapConfig");
                var checkboxSimpleParams = {"onText": trad.yes,"offText": trad.no,"onLabel": trad.yes,"offLabel": trad.no,"labelText": "label"}
                var indexOfProposition = parseInt(stepsArray.indexOf("Proposition"))+1;
                var indexOfCharacterization = parseInt(stepsArray.indexOf("Caractérisation"))+1;
                var indexOfEvaluation = parseInt(stepsArray.indexOf("Evaluation"))+1;
                var indexOfFinancement = parseInt(stepsArray.indexOf("Financement"))+1;
                var indexOfSuivie = parseInt(stepsArray.indexOf("Suivi"))+1;

                if(exists(currentAapConfig))
                    jsonHelper.setValueByPath(
                        currentAapConfig,
                        "subForms-aapStep"+indexOfEvaluation+"-params-config-criterions",
                        jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.criterions")
                    );
                var aapConfigForm = {
                    jsonSchema: {
                        title: trad.configuretempatecall,
                        description: "",
                        icon: "fa-question",
                        properties: {
                            parent: {
                                inputType: "finder",
                                label: tradDynForm.whoiscarrytheproject,
                                multiple: true,
                                rules: { required: true, lengthMin: [1, "parent"] },
                                initType: ["organizations", "projects"],
                                openSearch: true
                            },
                            name: {
                                inputType : "text",
                                label: trad.nametemplate,
                                value: (notNull(currentAapConfig) && typeof currentAapConfig.name != "undefined" ? currentAapConfig.name : "")
                            },
                            description: {
                                inputType : "textarea",
                                label: trad.templatedescription,
                                value: (notNull(currentAapConfig) && exists(currentAapConfig.description) ? currentAapConfig.description : "")
                            },
                            type: {
                                inputType: "hidden",
                                value: "aapConfig"
                            },
                            subType:{
                                inputType : "hidden",
                                value: (notNull(currentAapConfig) && exists(currentAapConfig.subType) ? currentAapConfig.subType :
                                        (bootboxResult =="ocecoform" ? "ocecoformConfig" : "aapConfig")
                                )
                            },
                            projectGeneration : {
                                inputType : "select",
                                label : trad.projectgenerate,
                                options :{
                                    onStart : trad.atcreation,
                                    onRunning: trad.processing,
                                    onSubmit : trad.onsubmission
                                }
                            },
                            tags : {
                                inputType : "tags",
                                minimumInputLength : 0,
                                label : trad.addkeywords,
                                values : [],
                                value: (notNull(currentAapConfig) && exists(currentAapConfig.tags) ? currentAapConfig.tags : "")
                            },
                            active: {
                                label: trad.activate,
                                inputType: "checkboxSimple",
                                params: checkboxSimpleParams,
                                checked: (notNull(currentAapConfig) && exists(currentAapConfig.active) ? currentAapConfig.active : true)
                            },
                            private: {
                                label: trad.private,
                                inputType: "checkboxSimple",
                                params: checkboxSimpleParams,
                                checked: (notNull(currentAapConfig) && exists(currentAapConfig.private) ? currentAapConfig.private : true)
                            },
                            showMap: {
                                label: trad.showmaponproject,
                                inputType: "checkboxSimple",
                                params: checkboxSimpleParams,
                                checked: (notNull(currentAapConfig) && exists(currentAapConfig.showMap) ? currentAapConfig.showMap : true)
                            }
                        },
                        onLoads: {
                            onload: function (data) {
                                $(".parentfinder,.subForms-aapStep"+indexOfFinancement+"-params-config-linktext").hide();
                                alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep"+indexOfProposition, 4, 6, null, null, trad.step +" "+indexOfProposition+" ("+stepsArray[indexOfProposition-1]+")", "#3f4e58", "",{borderSize : "5px",borderType : "solid",borderColor: "blue"});
                                alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep"+indexOfCharacterization, 4, 6, null, null, trad.step +" "+indexOfCharacterization+" ("+stepsArray[indexOfCharacterization-1]+")", "#3f4e58", "",{borderSize : "5px",borderType : "solid",borderColor: "green"});
                                alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep"+indexOfEvaluation, 4, 6, null, null, trad.step +" "+indexOfEvaluation+" ("+stepsArray[indexOfEvaluation-1]+")", "#3f4e58", "",{borderSize : "5px",borderType : "solid",borderColor: "blue"});
                                alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep"+indexOfFinancement, 4, 6, null, null, trad.step +" "+indexOfFinancement+" ("+stepsArray[indexOfFinancement-1]+")", "#3f4e58", "",{borderSize : "5px",borderType : "solid",borderColor: "green"});
                                alignInput2(aapConfigForm.jsonSchema.properties, "subForms-aapStep"+indexOfSuivie, 4, 6, null, null, trad.step +" "+indexOfSuivie+" ("+stepsArray[indexOfSuivie-1]+")", "#3f4e58", "",{borderSize : "5px",borderType : "solid",borderColor: "blue"});
                                $('.subForms-aapStep'+indexOfEvaluation+'-params-config-criterionslists,.subForms-aapStep'+indexOfEvaluation+'-params-config-typeselect,.subForms-aapStep'+indexOfEvaluation+'-params-config-publicVoteselect,.subForms-aapStep'+indexOfEvaluation+'-params-config-linktext').removeClass('col-md-4 col-xs-6').addClass('col-md-10 col-md-offset-1 col-xs-12')

                                if(jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.type") == "noteCriterionBased" || jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.type") == "starCriterionBased")
                                    $('.subForms-aapStep'+indexOfEvaluation+'-params-config-criterionslists').show();
                                $('#subForms-aapStep'+indexOfEvaluation+'-params-config-type').off().on('change',function(){
                                    if($(this).val() == "noteCriterionBased" || $(this).val() == "starCriterionBased"){
                                        $('.subForms-aapStep'+indexOfEvaluation+'-params-config-criterionslists').fadeIn();
                                        //$("[data-entry=min]").val(0); $("[data-entry=max]").val(5);
                                    }else
                                        $('.subForms-aapStep'+indexOfEvaluation+'-params-config-criterionslists').fadeOut();
                                })

                                if(jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfFinancement+".params.config.type") != "ocecoform")
                                    $('.subForms-aapStep'+indexOfFinancement+'-params-config-link').show();
                                $('#subForms-aapStep'+indexOfFinancement+'-params-config-type').off().on('change',function(){
                                    if($(this).val() == "ocecoform")
                                        $('.subForms-aapStep'+indexOfFinancement+'-params-config-linktext').fadeOut();
                                    else
                                        $('.subForms-aapStep'+indexOfFinancement+'-params-config-linktext').fadeIn();
                                })

                            },
                        },
                        save: function (formData) {
                            mylog.log('save tplCtx formData', formData)
                            tplCtx = {
                                collection: "forms",
                                value: {},
                                format : true,
                                path: "allToRoot"
                            };

                            if (typeof currentAapConfig != "undefined" /*&& currentAapConfig.exist==true*/) // check if aapAlready exists ans make modification only
                                tplCtx.id = currentAapConfig._id.$id
                            $.each(aapConfigForm.jsonSchema.properties,function(k,v){
                                var kk = k.split("-").join("][");
                                if(k == "subForms-aapStep"+indexOfEvaluation+"-params-config-criterions")
                                    tplCtx.value[kk] = formData["subForms-aapStep"+indexOfEvaluation+"-params-config-criterions"];
                                else if(v.inputType == 'tags')
                                    tplCtx.value[kk] = $("#" + k).val().split(',');
                                else
                                    tplCtx.value[kk] = $("#" + k).val();
                            });

                            // $.each(aapConfigForm.jsonSchema.properties,function(k,v){
                            //     var kk = k.split("-").join("][");
                            //     if(k == "subForms-aapStep"+indexOfEvaluation+"-params-config-criterions")
                            //         tplCtx.value[kk] = formData["subForms-aapStep"+indexOfEvaluation+"-params-config-criterions"];
                            //     else if (k.split("-").length > 1 ){ 
                            //         var spltid = k.split("-");

                            //         var spltidobj = spltid.reverse().reduce(function(total, current, index){
                            //             var sp = {};
                            //             if (index == 0){
                            //                 sp[current] = $("#" + k).val();
                            //             }else {
                            //                 sp[current] = total;
                            //             }
                            //             return sp;
                            //         }, {});

                            //         /**
                            //          * Simple object check.
                            //          * @param item
                            //          * @returns {boolean}
                            //          */
                            //          function isObject(item) {
                            //             return (item && typeof item === 'object' && !Array.isArray(item));
                            //         }

                            //         /**
                            //          * Deep merge two objects.
                            //          * @param target
                            //          * @param ...sources
                            //          */
                            //          function mergeDeep(target, ...sources) {
                            //             if (!sources.length) return target;
                            //             const source = sources.shift();

                            //             if (isObject(target) && isObject(source)) {
                            //                 for (const key in source) {
                            //                     if (isObject(source[key])) {
                            //                         if (!target[key]) Object.assign(target, { [key]: {} });
                            //                         mergeDeep(target[key], source[key]);
                            //                     } else {
                            //                         Object.assign(target, { [key]: source[key] });
                            //                     }
                            //                 }
                            //             }

                            //             return mergeDeep(target, ...sources);
                            //         }



                            //         /*$.each(spltidobj, function(indx, val){
                            //             if (indx == "params"){
                            //                 $.each(val, function(indx2, val2){
                            //                     tplCtx.value[indx][indx2] = val2;
                            //                 });
                            //             }else{
                            //                 tplCtx.value[indx]= val;
                            //             }
                            //         });*/

                            //         $.each(spltidobj, function(indx, val){
                            //             if (notNull(currentAapConfig) && notNull(currentAapConfig[indx])){
                            //                 tplCtx.value[indx] = mergeDeep(currentAapConfig[indx], val);
                            //             }else{
                            //                 tplCtx.value[indx]= val;
                            //             }
                            //         });

                            //     } else {
                            //         if(v.inputType == 'tags')
                            //             tplCtx.value[k] = $("#" + k).val().split(',');
                            //         else
                            //             tplCtx.value[k] = $("#" + k).val();
                            //     }

                            // });
                            
                            tplCtx.value["mapping"] = {
                                "proposition" : "answers.aapStep1.titre",
                                "description" : "answers.aapStep1.opalProcess41",
                                "depense" : "answers.aapStep1.depense",
                                "murir" : "aapStep4"
                            };
                            mylog.log("goo",formData);
                            tplCtx.value["parent"] = formData.parent;
                            mylog.log("save tplCtx", tplCtx);

                            //mylog.log("zzeeee", currentAapConfig);

                            if (typeof tplCtx.value == "undefined")
                                toastr.error('value cannot be empty!');
                            else {
                                dataHelper.path2Value(tplCtx, function (params) {
                                    if (typeof currentAapConfig != "undefined" /*&& currentAapConfig.exist == true*/){
                                        urlCtrl.loadByHash(location.hash)
                                    }else{
                                        var insertedAaapConfig = params.saved;
                                        var id = insertedAaapConfig._id.$id;
                                        delete insertedAaapConfig._id;
                                        var opt= {
                                            proposition : 1,
                                            //caracterisation: 2,
                                            evaluation:2,
                                            financement:3,
                                            suivie : 4,
                                            aap: true
                                        };
                                        if(bootboxResult=="ocecoform")
                                            opt= {
                                                proposition : 1,
                                                evaluation: 2,
                                                financement:3,
                                                suivie : 4,
                                                ocecoform:true
                                            };
                                        var generatedInputs = copyFormObj.aap.generateInputsDocument(copyFormObj,opt); //generate inputs document
                                        mylog.log(generatedInputs,"govagova generatedInputs");


                                        $.each(generatedInputs, function (k, v) {
                                            if (typeof insertedAaapConfig.subForms != "undefined" && typeof insertedAaapConfig.subForms != "undefined" && typeof insertedAaapConfig.subForms["aapStep" + k] != "undefined" && typeof insertedAaapConfig.subForms["aapStep" + k].inputs != "undefined"){
                                                insertedAaapConfig.subForms["aapStep" + k].inputs = v._id.$id;
                                            }
                                        }); // assing generated inputs to subforms input
                                        dataHelper.path2Value({ // adn update aapConfig in forms collection
                                            id: id,
                                            collection: "forms",
                                            path: "allToRoot",
                                            format:true,
                                            value: insertedAaapConfig
                                        }, () => {
                                            copyFormObj.aap.generateCostum();
                                            /*bootbox.confirm({
                                                message: "<h5 class='text-center text-success'>Voulez-vous créer le costum ?</h5>",
                                                buttons: {
                                                    confirm: {label: 'OUI',className: 'btn-success'},
                                                    cancel: {label: 'NON',className: 'btn-primary'}
                                                },
                                                callback: function (result) {
                                                    if (result)
                                                        copyFormObj.aap.generateCostum();
                                                    else
                                                        urlCtrl.loadByHash(location.hash);
                                                }
                                            })*/
                                        });
                                    }

                                });
                            }

                        }
                    }
                };

                $.each(stepsArray,function(k,v){ //common properties
                    var i = k+1;
                    if (currentAapConfig){}else{
                        aapConfigForm.jsonSchema.properties['subForms-aapStep' + i + '-active'] = {
                            label: trad.activatestep +" " +i,
                            inputType: "checkboxSimple",
                            params: checkboxSimpleParams,
                            checked: typeof jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep"+i+".active") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig, "subForms.aapStep"+i+".active") : true
                        };
                    }
                    aapConfigForm.jsonSchema.properties["subForms-aapStep"+i+"-inputs"]= {  //get inputs 1 path from inputs collection
                        inputType: "hidden",
                        value: jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".inputs")
                    };
                    aapConfigForm.jsonSchema.properties["subForms-aapStep"+i+"-name"]= {
                        inputType: "text",
                        label : trad.activatestep +" "+i,
                        value: (notNull(currentAapConfig) && exists(currentAapConfig.subForms) &&
                        (exists(currentAapConfig.subForms['aapStep'+i]) &&
                            exists(currentAapConfig.subForms['aapStep'+i].name)) ? currentAapConfig.subForms['aapStep'+i].name : v)
                    };
                    aapConfigForm.jsonSchema.properties["subForms-aapStep"+i+"-params-haveEditingRules"]= {
                        inputType: "checkboxSimple",
                        label: trad.writingmode,
                        params: checkboxSimpleParams,
                        checked: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.haveEditingRules") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.haveEditingRules") :true
                    };
                    aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-haveReadingRules"] = {
                        inputType: "checkboxSimple",
                        label: trad.readingmode,
                        params: checkboxSimpleParams,
                        checked: jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.haveReadingRules")
                    };
                    aapConfigForm.jsonSchema.properties["subForms-aapStep"+i+"-params-canEdit"]= {
                        inputType : "tags",
                        minimumInputLength : 0,
                        label : trad.whocanedit,
                        values : ["Evaluateur","Financeur"],
                        value: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canEdit") != "undefined" && Array.isArray( jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canEdit"))  ?
                            jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canEdit").join() : ""
                    };
                    aapConfigForm.jsonSchema.properties["subForms-aapStep" + i + "-params-canRead"] = {
                        inputType : "tags",
                        minimumInputLength : 0,
                        label : trad.whocanread,
                        values : ["Evaluateur","Financeur"],
                        value: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canRead") != "undefined" && Array.isArray(jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canRead")) ?
                            jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+i+".params.canRead").join() : ""
                    };
                });

                aapConfigForm.jsonSchema.properties['subForms-aapStep'+indexOfEvaluation+'-params-config-publicVote'] = {
                    inputType: "select",
                    label : trad.publicvote,
                    options:{
                        "all" : tradCategory.all,
                        "members" : trad.membersonly
                    },
                    value: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.publicVote") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.publicVote") : "all"
                };

                aapConfigForm.jsonSchema.properties['subForms-aapStep'+indexOfEvaluation+'-params-config-type'] = {
                    inputType: "select",
                    label : trad.evaluationtype,
                    options:{
                        "noteCriterionBased" : trad.evaluationbynote,
                        "starCriterionBased" : trad.evaluationbystar,
                        "forOrAgainst" : trad.fororagainst
                    },
                    value: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.type") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfEvaluation+".params.config.type") : "noteCriterionBased"
                };
                aapConfigForm.jsonSchema.properties['subForms-aapStep'+indexOfEvaluation+'-params-config-criterions'] = {
                    inputType: "lists",
                    label: trad.evaluationcriteria,
                    "entries":{
                        "label":{
                            "type":"text",
                            "label" : trad.namecriteria +" {num}",
                            "class":"col-md-5 col-sm-5 col-xs-10"
                        },
                        "coeff":{
                            "label":"Coeff",
                            "type":"text",
                            "value":"1",
                            "class":"col-md-2 col-sm-5 col-xs-10"
                        },
                        "note":{
                            "label":"",
                            "type":"text",
                            "value":"0",
                            "class":"col-md-2 col-sm-5 col-xs-10 hidden"
                        }

                    }
                };
                aapConfigForm.jsonSchema.properties['subForms-aapStep'+indexOfFinancement+'-params-config-type'] = {
                    inputType: "select",
                    label : trad.typefinancing,
                    options:{
                        "ocecoform" : trad.financingline,
                        "crowdfunding" : trad.participativefinancing
                    },
                    value: typeof jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfFinancement+".params.config.type") != "undefined" ? jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfFinancement+".params.config.type") : "ocecoform"
                };
                aapConfigForm.jsonSchema.properties['subForms-aapStep'+indexOfFinancement+'-params-config-link'] = {
                    inputType: "text",
                    label : trad.participativefinancinglink,
                    placeholder : "http://",
                    value: jsonHelper.getValueByPath(currentAapConfig,"subForms.aapStep"+indexOfFinancement+".params.config.link")
                };

                dyFObj.openForm(aapConfigForm,null,currentAapConfig);
            },
            addConfig: function(){
                bootbox.prompt({
                    title: "<h5 class='text-center modal-title'>"+trad.callforprojectsorocecoform+"</h5>",
                    value : ["aap"],
                    inputType: 'select',
                    inputOptions: [{
                        text: trad.templatecallproposals,
                        value: 'aap',
                    },
                        {
                            text: 'Template ocecoform',
                            value: 'ocecoform',
                        }],
                    callback: function (bootboxResult) {
                        if(bootboxResult == null) return bootbox.hideAll();
                        aapConfigObj.aapConfig(bootboxResult);
                    }
                });
            },
            editConfig : function(bootboxResult){
                aapConfigObj.aapConfig(bootboxResult);
            }
        }


        $('body').on('click','.template-description',(e)=>{
            e.preventDefault();
            e.stopPropagation();
        })
        $('.template-description').moreAndLess({
            showChar : 200,
            ellipsestext : "...",
            moretext : "Voir plus >",
            lesstext : "Voir moins",
            class : "letter-green"
        })
        $(".template-tag-panel").off().on('click', function(e){
            var dataTag = $(this).data("tag");
            location.hash = "#search";
            if (location.hash.indexOf("tags") == -1 && location.hash.indexOf("?") != -1){
                location.hash = location.hash+"&tags="+$(this).data("tag");
            }
            else if (location.hash.indexOf("tags") == -1 && location.hash.indexOf("?") == -1) {
                location.hash = location.hash+"?tags="+$(this).data("tag");
            }
            else if (location.hash.indexOf("tags") != -1) {

                mylog.log("location.hash",location.hash.split("&"));
                location.hash = location.hash.split("&").map(function(item,index){
                    // mylog.log(item.split("="));
                    if(item.indexOf("tags") != -1){
                        var item = item.split("=");
                        var tagsvalue = item[1].split(",");
                        tagsvalue.push(dataTag);
                        var tagsStr = tagsvalue.join();
                        item = item[0] +"=" + tagsStr;
                    }
                    return item;
                }).join("&");

            }
            urlCtrl.loadByHash(location.hash);
        });
        setTimeout(()=>{
            $('.btnquest').on('click',function(e){
                e.preventDefault();e.stopPropagation();
                $('[data-toggle="popover"]').popover();
            })
            coInterface.bindLBHLinks();
        },1000)

    });


</script>
