<?php
$cssJS = array(
 '/plugins/gridstack/css/gridstack.min.css',
 '/plugins/gridstack/js/gridstack.js',
 '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>
<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }
</style>

<div id="sticky-anchor"></div>
<div id="sticky" class="container fixed-breadcrumb-filters">
    <div class="col-md-10">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
            <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details ">Observatoire globale</a></li>
        </ul>
    </div>

</div>

<div class="list-aap-container" id="aapformcont"></div>


<script type="text/javascript">
    var formparentid = "<?php echo (isset($elform['_id']) ? (string)$elform['_id'] : ''); ?>";
    $(document).ready(function() {
        $('.grid-stack').gridstack({
            cellHeight: 100,
            verticalMargin: 10,
            horizontalMargin: 10,
            disableResize: true,
            removable: true
        });
        ajaxPost("#aapformcont", baseUrl+'/survey/answer/ocecoorgadashboard/formparentid/'+formparentid,
        {
            urlR : "<?php echo $_SERVER['REQUEST_URI']?>"
        },
        function(data){

        },"html");
    });
</script>