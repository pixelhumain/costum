<?php
HtmlHelper::registerCssAndScriptsFiles(array(
    '/plugins/jsontotable/JSON-to-Table.min.1.0.0.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.min.js',
    '/plugins/jqueryTablesorter/jquery.tablesorter.widgets.js',
    '/plugins/jqueryTablesorter/widget-grouping.js'
), Yii::app()->request->baseUrl);

HtmlHelper::registerCssAndScriptsFiles(array(
    '/css/aap/aapGlobalDashboard.css'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

?>

<style type="text/css">
    /* extra css needed because there are 5 child rows */
    /* no zebra striping */
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
        /* with zebra striping */
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.even:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #d9d9d9;
    }
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td,
    .tablesorter-blue tbody > tr.odd:hover + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow + tr.tablesorter-childRow > td {
        background: #bfbfbf;
    }

    /* Grouping widget css;
     * contained in widget.grouping.css (added v2.28.4)
     */
    tr.group-header td {
        background: #eee;
    }
    .group-name {
        text-transform: uppercase;
        font-weight: bold;
    }
    .group-count {
        color: #999;
    }
    .group-hidden {
        display: none !important;
    }
    .group-header, .group-header td {
        user-select: none;
        -moz-user-select: none;
    }
    /* collapsed arrow */
    tr.group-header td i {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid #888;
        border-right: 4px solid #888;
        border-left: 4px solid transparent;
        margin-right: 7px;
        user-select: none;
        -moz-user-select: none;
    }
    tr.group-header.collapsed td i {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid #888;
        border-right: 0;
        margin-right: 10px;
    }

    .tablesorter .filtered {
        display: none;
    }

    /* ajax error row */
    .tablesorter .tablesorter-errorRow td {
        text-align: center;
        cursor: pointer;
        background-color: #e6bf99;
    }

    input.tablesorter-filter.form-control.disabled{
        display: none;
    }

    tr.group-header td {
        background: #eee;
    }
    .group-name {
        text-transform: uppercase;
        font-weight: bold;
    }
    .group-count {
        color: #999;
    }
    .group-hidden {
        display: none;
    }
    .group-header, .group-header td {
        user-select: none;
        -moz-user-select: none;
    }
    /* collapsed arrow */
    tr.group-header td i {
        display: inline-block;
        width: 0;
        height: 0;
        border-top: 4px solid transparent;
        border-bottom: 4px solid #888;
        border-right: 4px solid #888;
        border-left: 4px solid transparent;
        margin-right: 7px;
        user-select: none;
        -moz-user-select: none;
    }
    tr.group-header.collapsed td i {
        border-top: 5px solid transparent;
        border-bottom: 5px solid transparent;
        border-left: 5px solid #888;
        border-right: 0;
        margin-right: 10px;
    }

    .smalltextgrey{
        color: grey;
    }

    .pie {
        margin: 20px;
    }

    svg {
        float: left;
    }

    .legend {
        float: left;
        font-family: "Verdana";
        font-size: .7em;
    }

    .pie text {
        font-family: "Verdana";
        fill: #000;
    }

    .pie .name-text{
        font-size: .8em;
    }

    .pie .value-text{
        font-size: 3em;
    }
</style>

    <div class="col-md-offset-1 col-md-10 col-xs-12 aapdashboard tablefin" style="display: none">

        <div class="col-md-12 col-sm-12 col-xs-12 aaptilescont">
            <div class="col-md-12 col-sm-12 col-xs-12 aaptiles3">
                <span class="aaptilestitles3"> <h5> Tableau de bord</h5> </span>
                <div class="print button"></div>
                <div class="aapinnertiles3 col-md-12" id="financementTable" data-aapdashcallback="viewinfo" style="padding: 50px 0 50px 0;">

                </div>

            </div>
        </div>

    </div>

<script type="text/javascript">
    var tableDataFin = <?php echo ( !empty($tableAnswers) ? json_encode($tableAnswers) : "[]"); ?>;

    var mybarcolorfin = [
        "#b30040",
        "#50bd60",
        "#7c00e4",
        "#b67994",
        "#00c5e6",
        "#006b0f",
        "#f06f29",
    ];
    function setRowSpanFin(tableId, col){
        var rowSpanMapff1 = {};
        var rowSpanMapffB = {};
        var rowSpanMapf = {};

        $(tableId).find('tr').each(function(){

            var valueOfTheSpannableCell1 = $($(this).children('td')[1]).text();
            $($(this).children('td')[1]).attr('data-original-value1', valueOfTheSpannableCell1);
            rowSpanMapff1[valueOfTheSpannableCell1] = true;

            if(col != 1){
                $($(this).children('td')[col]).attr('data-original-value1', valueOfTheSpannableCell1);

                var valueOfTheSpannableCellBefore = $($(this).children('td')[col - 1]).text();
                $($(this).children('td')[col]).attr('data-original-valueB', valueOfTheSpannableCellBefore);
                rowSpanMapffB[valueOfTheSpannableCellBefore] = true;

                var valueOfTheSpannableCell = $($(this).children('td')[col]).text();
                $($(this).children('td')[col]).attr('data-original-value'+col+'', valueOfTheSpannableCell);
                rowSpanMapf[valueOfTheSpannableCell] = true;
            }

        });


        if(col == 1){
            $.each(rowSpanMapff1 , function(row, rowGroup) {
                var $cellsToSpan1 = $(tableId+' td[data-original-value1="' + row + '"]');
                var numberOfRowsToSpan1 = $cellsToSpan1.length;
                $cellsToSpan1.each(function (index) {
                    if (index == 0) {
                        $(this).attr('rowspan', numberOfRowsToSpan1);
                    } else {
                        $(this).hide();
                    }
                });
            });
        }else{
            $.each(rowSpanMapff1 , function(row1, rowGroup1) {
                $.each(rowSpanMapffB , function(rowB, rowGroupB) {
                    $.each(rowSpanMapf , function(row, rowGroup) {
                        var $cellsToSpan = $(tableId+' td[data-original-value1="'+row1+'"][data-original-valueB="'+rowB+'"][data-original-value'+col+'="'+row+'"]');
                        var numberOfRowsToSpan = $cellsToSpan.length;
                        $cellsToSpan.each(function(index){
                            if(index==0){
                                $(this).attr('rowspan', numberOfRowsToSpan);
                            }else{
                                $(this).hide();
                            }
                        });
                    });
                });
            });
        }

    }

    function createPageFin (tableDataFin) {
        totalDepense = 0;
        totalRecette = 0;
        $.each(tableDataFin, function(index,value){
            totalDepense += parseInt(value.depense);
            totalRecette += parseInt(value.recette);
        });

        tableDataFin.push({
            "poste de depense" : "TOTAL" ,
            "depense" : totalDepense ,
            "libelé financeur" : " ",
            "libelé financement" : " ",
            "recette" : totalRecette
        });

        $('#financementTable').createTable(tableDataFin , {
            borderWidth:'1px',
            borderStyle:'solid',
            borderColor:'#DDDDDD',
            fontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',

            thBg:'#F3F3F3',
            thColor:'#0E0E0E',
            thHeight:'30px',
            thFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
            thFontSize:'14px',
            thTextTransform:'capitalize',

            trBg:'#FFFFFF',
            trColor:'#0E0E0E',
            trHeight:'25px',
            trFontFamily:'Verdana, Helvetica, Arial, FreeSans, sans-serif',
            trFontSize:'13px',

            tdPaddingLeft:'10px',
            tdPaddingRight:'10px'
        });

        setRowSpanFin('#financementTable table', 1);
        setRowSpanFin('#financementTable table', 2);

        $("#financementTable table tr:nth-child(1) th:nth-child(2)").addClass('filter-select');
        $("#financementTable table tr:nth-child(1) th:nth-child(5)").addClass('filter-select');
        $("input.tablesorter-filter.form-control[data-column='0']").remove();
        $( "#financementTable table tr:nth-child(1) th:nth-child(2)" ).addClass('group-word');



        $.tablesorter.themes.bootstrap = {
            // these classes are added to the table. To see other table classes available,
            // look here: http://getbootstrap.com/css/#tables
            table        : 'table table-bordered table-striped',
            caption      : 'caption',
            // header class names
            header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
            sortNone     : '',
            sortAsc      : '',
            sortDesc     : '',
            active       : '', // applied when column is sorted
            hover        : '', // custom css required - a defined bootstrap style may not override other classes
            // icon class names
            icons        : '', // add "bootstrap-icon-white" to make them white; this icon class is added to the <i> in the header
            iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
            iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
            iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
            filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
            footerRow    : '',
            footerCells  : '',
            even         : '', // even row zebra striping
            odd          : ''  // odd row zebra striping
        };

        $("#financementTable table").tablesorter({
            // this will apply the bootstrap theme if "uitheme" widget is included
            // the widgetOptions.uitheme is no longer required to be set
            theme : "bootstrap",

            widthFixed: true,

            headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

            // widget code contained in the jquery.tablesorter.widgets.js file
            // use the zebra stripe widget if you plan on hiding any rows (filter widget)
            widgets : [ "uitheme", "filter", "columns", "zebra" , "print" , "group"],

            widgetOptions : {
                columns: [ "primary", "secondary", "tertiary" ],
                print_now        : true,
                filter_reset : ".reset",

                // extra css class name (string or array) added to the filter element (input or select)
                filter_cssFilter: "form-control",

                group_collapsible : true,
                group_collapsed   : true,
                group_count       : true,
                filter_childRows  : true,
                filter_columnFilters   : true,
                headerTitle_useAria    : true,

                group_count       : " ({num})",

                group_forceColumn : [],   // only the first value is used; set as an array for future expansion
                group_enforceSort : true,

                // set the uitheme widget to use the bootstrap theme class names
                // this is no longer required, if theme is set
                // ,uitheme : "bootstrap"

            },

            headers: {
                3 : { sorter: 'priceparser' },
                7 : { filter_cssFilter : 'a'}
            },

            group_callback    : function($cell, $rows, column, table) {
                // callback allowing modification of the group header labels
                // $cell = current table cell (containing group header cells ".group-name" & ".group-count"
                // $rows = all of the table rows for the current group; table = current table (DOM)
                // column = current column being sorted/grouped
                if (column === 3) {
                    var subtotal = 0;
                    $rows.each(function() {
                        subtotal += parseFloat( $(this).find("td").eq(column).text() );
                    });
                    $cell.find(".group-count").append("; subtotal: " + subtotal );
                }
            },
            // event triggered on the table when the grouping widget has finished work
            group_complete    : "groupingComplete",

            on_filters_loaded: function(o){
                o.SetFilterValue(paramCol, paramCriteria);
                o.Filter();
            }

        });
    }

    jQuery(document).ready(function() {

        createPageFin (tableDataFin);

        if(!notNull(wsCO) || !wsCO.hasListeners('refresh-coform-answer')) {
            var wsCO = null;

            var socketConfigurationEnabled = coWsConfig.enable && coWsConfig.serverUrl && coWsConfig.pingRefreshCoformAnswerUrl;
            if (socketConfigurationEnabled) {
                wsCO = io(coWsConfig.serverUrl, {
                    query: {
                        answerId: answerId
                    }
                });

                wsCO.on('refresh-coform-answer', function () {
                    ajaxPost("", baseUrl + '/survey/form/coremufinancement/tableonly/true/jsononly/true/depenseid/' + depenseid, {},
                        function (data) {
                            tableData = data["tableAnswers"];
                            createPageFin(data["tableAnswers"]);
                        }, "json"
                    );
                });
            }
        }

    });

</script>

