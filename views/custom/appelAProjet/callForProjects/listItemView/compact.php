<?php 
    $name = $ansVar["name"];
    $description = $ansVar["description"];
    $completedFields = $ansVar["completedFields"];
    $percentageTasks = $ansVar["percentageTasks"];
    $dataPer = $ansVar["dataPer"];
    $iconTasks = $ansVar["iconTasks"];
    $haveDepense = $ansVar["haveDepense"];
    $nbFinancedDepense  = $ansVar["nbFinancedDepense"];
    $seen = $ansVar["seen"];
    $haveProject = $ansVar["haveProject"];
    $countTask = $ansVar["countTask"];
    $imgAnsw = $ansVar["imgAnsw"];
    $retain = $ansVar["retain"];
    $actions  = $ansVar["actions"];
    $urgentState  = $ansVar["urgentState"];
    $cummulMean  = $ansVar["cummulMean"];
    $votantCounter  = $ansVar["votantCounter"];
    $actions = $ansVar["actions"];
    $userAnswer = isset(Yii::app()->session['userId']) ? ($answer["user"] == Yii::app()->session['userId'] ? true : false) : false;
?>
<div class="col-xs-12 compact-parent col-md-4 col-sm-6" id="list-<?= (string) $answer["_id"] ?>" data-id="<?= (string) $answer["_id"] ?>">
    <?php if($is_coform) {
        $answerNameTitle = '';
            if(isset($data["elform"]["params"]["inputForAnswerName"]["title"])) {
                $answerName = $data["elform"]["params"]["inputForAnswerName"]["title"];
                if($answer["answers"]) {
                    $ownAnswers = array_values($answer["answers"]);
                    foreach($ownAnswers as $keyOAnswer => $itemOAnswer) {
                        if(isset($itemOAnswer[$answerName])) {
                            $answerNameTitle = $itemOAnswer[$answerName];
                        }
                    }
                }
            }
    ?>
        <div class="div compact-container padding-compact-coform <?php echo ($seen ? "" : "unseen-proposition bdgdiv"); ?>" data-label="nouv.">
            <?php echo $answerNameTitle != '' ? '<h6>'.$answerNameTitle.'</h6>' : '' ?>
            <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$data["users"][$answer["user"]]["name"] ?></i></b></small>
            <small>Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b></small>
            <div class="margin-top-10" style="position: relative;display: flex;justify-content: center;width: 100%;font-size: 23px;">
                <?php echo Aap::listItemCoFormButtons($p_active, $adminRight, $canEditEachOtherAnswer, $canReadEachOtherAnswer, (string) $answer["_id"], $userAnswer) ?>
            </div>
            <div class="margin-top-10" style="position: relative;display: flex;justify-content: center;width: 100%;font-size: 23px;">
                <?php echo Aap::heartVote($answer,$data["el"],$data["elform"],$data["me"]) ?>
                <?php if(isset(Yii::app()->session['userId'])){ ?>
                    <a href="javascript:;" id="btn-comment-<?= (string)$answerid ?>" class="btn margin-right-5 openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :23px;border:0">
                        <small style="font-size: 58% !important;"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?></small> <i class='fa fa-commenting'></i> 
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php 
    } else {
    ?>
        <div data-url = 'slug.<?php echo $data["el"]["slug"]; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string) $answer["_id"] ?>'  class="aapgetaapview margin-bottom-25" data-id="<?= (string) $answer["_id"] ?>">
            <div class="div compact-container radius-5  <?php /*echo ($seen ? "" : "unseen-proposition bdgdiv");*/ ?>" data-label="nouv." style="<?= $haveProject['is'] ? 'background-color:#7da53d42' :'' ?>">
                <div class="row" style="height:100%">
                    <div class="col-xs-4 padding-left-25 padding-top-10 padding-bottom-10" style="height:100%">
                        <img class="lzy_img img-responsive radius-5" data-src="<?=$imgAnsw?>" src="<?php echo Yii::app()->controller->module->assetsUrl ?>/images/thumbnail-default.jpg" alt="projet" style="width:100%;height:100%;object-fit: cover;"/>
                    </div>
                    <div class="col-xs-8" style="height:100%">
                        <h6><?= $name ?></h6>
                        <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$data["users"][$answer["user"]]["name"] ?></i></b></small>
                        <small>Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b></small>
                        <div class="margin-top-10" style="position: relative;display: flex;justify-content: center;width: 100%;font-size: 23px;">
                            <?php echo Aap::heartVote($answer,$data["el"],$data["elform"],$data["me"]) ?>
                            <?php if(isset(Yii::app()->session['userId'])){ ?>
                                <a href="javascript:;" id="btn-comment-<?= (string)$answerid ?>" class="margin-right-5 openAnswersComment tooltips" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :23px;border:0">
                                    <small style="font-size: 58% !important;"><?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?></small> <i class='fa fa-commenting'></i> 
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.listItemButtons",array("answer" => $answer,"data" => $data, "ansVar"=>$ansVar)) ?>
    <?php
    }
    ?>
</div>