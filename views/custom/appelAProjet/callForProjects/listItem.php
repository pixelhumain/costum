<?php
if(empty($allAnswers)){
    echo "<b>".Yii::t("common","No data")."</b>";
    return;
}
$data = Aap::prepareData($allAnswers,$types,$el,$el_form_id,$el_configform_id);unset($el);

?>
<style>
    .star-rating .star-value{
        background: url("<?=$data["starbg"]?>") !important;
        background-size: 20px !important;
        background-repeat: repeat no-repeat !important;
    }

    .or-spacer {
        margin-top: 10px;
        /*margin-left: 100px;*/
        /*width: 400px;*/
        position: relative;
    }
    .or-spacer .mask {
        overflow: hidden;
        height: 20px;
    }
    .or-spacer .mask:after {
        content: '';
        display: block;
        margin: -25px auto 0;
        width: 100%;
        height: 25px;
        border-radius: 125px / 12px;
        box-shadow: 0 0 8px black;
    }
    .or-spacer span {
        width: 50px;
        height: 50px;
        position: absolute;
        bottom: 100%;
        margin-bottom: -25px;
        left: 50%;
        margin-left: -25px;
        border-radius: 100%;
        box-shadow: 0 2px 4px #999;
        background: white;
    }
    .or-spacer span i {
        position: absolute;
        top: 4px;
        bottom: 4px;
        left: 4px;
        right: 4px;
        border-radius: 100%;
        border: 1px dashed #aaa;
        text-align: center;
        line-height: 40px;
        font-style: normal;
        color: #999;
    }

    .or-spacer-vertical {
        display: inline-block;
        /*margin-top: 100px;
        margin-left: 100px;
        width: 100px;*/
        position: relative;
    }
    .or-spacer-vertical .mask {
        overflow: hidden;
        width: 10px;
        height: 50px;
        margin-left: 10px;
    }
    .or-spacer-vertical.left .mask:after {
        content: '';
        display: block;
        margin-left: -20px;
        width: 20px;
        height: 100%;
        border-radius: 6px / 60px;
        box-shadow: 0 0 2px black;
    }
    .or-spacer-vertical.right .mask:before {
        content: '';
        display: block;
        margin-left: 20px;
        width: 20px;
        height: 100%;
        border-radius: 6px / 60px;
        box-shadow: 0 0 2px black;
    }

    .portfolio-modal.modal .modal-content,
    /*.portfolio-modal.modal #openModalContent,*/
    .portfolio-modal.modal .list-aap-container.aappageform.container,
    .portfolio-modal.modal #customHeader,
    .portfolio-modal.modal .col-xs-12.text-center.bg-white{
        background-color: #00000000 !important;
    }
    .portfolio-modal.modal #openModalContent{
        border-radius :5px;
    }
    .portfolio-modal.modal{
        background-color: #000000a3 !important;
    }
    .portfolio-modal.modal #wizardcontainer{
        background-color: #fff !important;
        border-radius: 5px;
    }
    .portfolio-modal.modal .aap-footer,
    .portfolio-modal.modal .col-xs-12.text-center.bg-white hr,
    .portfolio-modal.modal #modeSwitch{
        display: none;
    }
    .portfolio-modal .close-modal .lr .rl,
    .portfolio-modal .close-modal .lr {
        background-color: #fff ;
    }
</style>
<div class="portfolio-modal modal fade" id="formInputM" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class=" container ">
            <ul class="aapinputcontent questionList col-xs-12  no-padding">

            </ul>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="aapViewFormModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class=" container ">
            <h1 class="text-center"><i class="fa fa-spinner fa-spin"></i></h1>
        </div>
    </div>
</div>

<div class="item-container-aap <?=@$data["configform"]["showMap"] ? "cx12 col-xs-12" : "container" ?>">
    <div class="row">
        <div class="list-group proposition_list">
            <?php
            if(empty($data["allAnswers"])) $data["allAnswers"]=[];
            if (is_array($data["allAnswers"]))
            {
                if($view=="kanbanaap"){
                    echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.kanban.kanban_proposition_list",array(
                        "allAnswers"=>$data["allAnswers"],
                        "el_slug"=> $data["el"]["slug"],
                        "el_form_id"=> $el_form_id
                    ));
                }elseif($view=="table"){
                    echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.table",array(
                       "el_slug"=> $data["el"]["slug"],
                        "el_form_id"=> $el_form_id,
                        "el" => $data["el"],
                        "elform" => $data["elform"],
                        "allAnswers" => $data["allAnswers"],
                        "canAdmin" => $data["canAdmin"] ,
                        "canEvaluate" => $data["canEvaluate"],
                        "canSeeDetail" => $data["canSeeDetail"],
                        "dataUsers" => isset($data["users"]) ? $data["users"] : array()
                    ));
                }elseif($view=="ppt"){
                    echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.powerpoint.powerpoint",array(
                        "el_slug"=> $data["el"]["slug"],
                        "el_form_id"=> $el_form_id,
                        "el" => $data["el"],
                        "data" => $data,
                    ));
                }elseif($view=="project_kanban"){
                    echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.kanban.kanban_project_list",array(
                        "el_slug"=> $data["el"]["slug"],
                        "el_form_id"=> $el_form_id,
                        "el" => $data["el"],
                        "canAdmin" => $data["canAdmin"] ,
                        "arrAnswers"=>array_keys($data["allAnswers"]),
                    ));
                }
                else{
                    $pos = 0;
                    foreach ($data["allAnswers"] as $answerid => $answer){
                        if($data["isActiveSousOrga"] && !empty($answer["form"])){
                            $cac = $data["cacs"][$answer["form"]];
                            $cacId = array_keys($cac["parent"])[0];
                            $cacValue = $cac["parent"][$cacId];
                        }
                        if (!empty($answer["answers"]))
                        {
                            $ansVar = Aap::answerVariables($answer,$data,$data["stepEval"]);
                            $coremu = false;
                            $parameters = array(
                                "pos" => $pos,
                                "view" => $view,
                                "answer" => $answer,
                                "is_coform" => $is_coform,
                                "el_form_id" => $el_form_id,
                                "data" => $data,
                                "answerid" => $answerid,
                                "ansVar" => $ansVar,
                                "slug" => $slug,
                                "urgentState " => $ansVar["urgentState"],
                                "coremu" => $coremu,
                                "p_active" => $p_active,
                                "adminRight" => $adminRight,
                                "canEditEachOtherAnswer" => $canEditEachOtherAnswer,
                                "canReadEachOtherAnswer" => $canReadEachOtherAnswer,
                                "timezone" => $timezone
                            );
                            unset($parameters["data"]["allAnswers"]);
                            //unset($parameters["data"]["configform"]);
                            unset($parameters["data"]["oceco"]);
                            unset($parameters["data"]["me"]["links"]);
                            unset($parameters["data"]["el"]["links"]);
                            unset($parameters["data"]["el"]["costum"]);
                            unset($parameters["data"]["el"]["oceco"]);
                            if (!empty($data["elform"]['coremu']) && $data["elform"]['coremu'] == true){
                                $anscoremuVar = Aap::answerCoremuVariables($answer);
                                $parameters["anscoremuVar"] = $anscoremuVar;
                            }

                            if($view === "compact"){
                                echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.listItemView.compact",$parameters);
                            }elseif ($view === "minimalist") {
                                echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.listItemView.minimalist",$parameters);
                            }elseif ($view === "detailed") {
                                echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.listItemView.detailed",$parameters);
                            }
                        }
                        $pos++;
                    }
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
$keyInput = [];
if(!empty($data["inputs"]["inputs"])){
    foreach($data["inputs"]["inputs"] as $kinput => $vinput){
        if($vinput["label"] != "" && $vinput["type"] != "tpls.forms.uploader")
            $keyInput[$kinput] = $vinput["label"];
    }
}
?>

<script type="text/javascript">
    jQuery(function ($) {
		aapObject.init();
        aapObject.sections.listItem.events(aapObject);
        aapObject.sections.listItem.select2(aapObject,".statustags");
        $(".dropdown-toggle").dropdown();

        $('.close-modal').off().on('click',function(){
            if(notEmpty(aapObject.sections.listItem.currentAnswer)){
                aapObject.sections.listItem.reloadPanel(viewParams.view);
            }
        })
        $(".main-search-bar.search-bar").on("blur", function(){
            listAapObj.activeFilters["text"][ $(this).data("text-path") ] = $(this).val();
        });
    });
</script>
