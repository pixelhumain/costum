<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }
    .st-enable-btn{
        border: 0 !important;
        background-color: #7da53d !important;
        color : #fff;
        display: none;
    }
    .st-enable-btn:hover{
        color: #7da53d !important;
        background-color: #fff !important;
        border:1px solid #7da53d !important;
    }
    .main-menu.st-actionContainer.right-bottom{
        display: none;
    }
    .fixed-breadcrumb-filters .select2-container-multi.aapstatus-container .select2-choices .select2-search-choice {
        border: 0 !important;
        background: 0 !important;
        display: inline !important;
        font-size: 12px !important;
        font-weight: bold;
        float: none !important;
        display: inline !important;
        padding: 5px 5px 5px 5px;
        webkit-box-shadow : none;
        box-shadow: none;
        color:#000;
        cursor: pointer !important;
    }
    .fixed-breadcrumb-filters .select2-input.select2-default{
        cursor: pointer !important;
    }
    
    .fixed-breadcrumb-filters .aapstatus-container{
        width: auto;
    }
    .fixed-breadcrumb-filters .select2-container-multi.aapstatus-container .select2-choices .select2-search-choice:hover {
        color:#7da53d !important;
    }
    .fixed-breadcrumb-filters .select2-container-multi.aapstatus-container .select2-choices{
        display: flex;
        cursor: pointer !important;
    }
    .fixed-breadcrumb-filters  .st-enable-btn{
        display: inline;
    }
    .fixed-breadcrumb-filters .select2-container-multi.aapstatus-container .select2-choices:hover .st-enable-btn{
        display: block;
    }
    .wizard-banner #col-banner{
        min-height: auto !important;
    }
    .list-aap-container.aappageform{
        margin-top: 0px !important;
    }
    
    .portfolio-modal.modal.vertical .modal-content,
    .portfolio-modal.modal.vertical #openModalContent,
    .portfolio-modal.modal.vertical .list-aap-container.aappageform.container,
    .portfolio-modal.modal.vertical #customHeader,
    .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white{
        background-color: #00000000 !important;
    }
    .portfolio-modal.modal.vertical{
        background-color: #000000a3 !important;
    }
    .portfolio-modal.modal.vertical #wizardcontainer{
        background-color: #fff !important;
        border-radius: 5px;
    }
    .portfolio-modal.modal .aap-footer,
    .portfolio-modal.modal.vertical .col-xs-12.text-center.bg-white hr,
    .portfolio-modal.modal.vertical #modeSwitch{
        display: none;
    }
    .portfolio-modal.vertical .close-modal .lr .rl,
    .portfolio-modal.vertical .close-modal .lr {
        background-color: #fff ;
    }
</style>

<?php
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
$canEdit=false;
if(!empty($answerid)) {
    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid);
    //$canEdit = Authorisation::isUserSuperAdmin((string) @$me["_id"]) || Form::canAdmin((string)@$elform["_id"]) || (string) @$me["_id"] == $answer["user"];
    ?>
    <?php if(!empty($modalrender) && !$modalrender){ ?>
    <div class="container fixed-breadcrumb-filters">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
            <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string)$answer["_id"] ?>" >edition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
            <li class="active" style="display: inline;">
                <a href="javascript:;" class="label padding-0" >
                <select data-id="<?= $answerid ?>" class="statustags" multiple disabled>
                    <option disabled></option>
                    <?php
                    foreach (Aap::$badgelabel as $bdgid => $bdglbl){
                        ?>
                        <option <?php if (!empty($answer["status"]) && in_array($bdgid, $answer["status"])) { echo "selected"; } ?> value="<?php echo $bdgid ?>"><?php  echo $bdglbl ?></option>
                    <?php } ?>

                </select>
                </a>
                <button type="button" class="btn btn-xs btn-success st-enable-btn" data-loc="edit" data-id="<?= $answerid ?>">Modifier le status</button>
            </li>
        </ul> 
    </div>
    <?php } ?>

    <?php if (empty($pageedit) ||  ( !empty($pageedit) && !$pageedit) ){ ?>
        <?php if(!empty($modalrender) && !$modalrender){ ?>
        <div class="container hidden">
            <div class="aapconfigdiv">
                <div class="text-center">
                    <button type="button"  class="aapgoto resume aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string)$answer["_id"] ?>"> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                    <button type="button" class="aapgoto  observatory aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.formdashboard.answer.<?php echo (string)$answer["_id"] ?>"><i class="fa fa-plus-square-o"></i> Observatoire </button>
                </div>
            </div>
        </div>
        <?php } ?>
<?php } ?>

<?php } else { ?>
    <div class="container fixed-breadcrumb-filters">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
            <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details "><?php echo ((empty($pageedit) || !$pageedit) ? "nouvelle proposition" : 'parametre pour '.$el["el"]["name"]); ?></a></li>
        </ul>
    </div>
    <?php
    if (empty($pageedit) ||  ( !empty($pageedit) && !$pageedit)){
        ?>

        <div class="col-md-4 list-aap-container hidden">
            <div class="aapconfigdiv">
                <div class="text-center">
                    <button type="button"  class="aapgoto resume aap-breadrumbbtn newsheet" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer."> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                    <button type="button" class="aapgoto observatory aap-breadrumbbtn newdashboard" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.formdashboard."><i class="fa fa-plus-square-o"></i> Observatoire </button>
                </div>
            </div>
        </div>

        <?php
    }
}
?>
<div class="list-aap-container aappageform container text-center" id="aapformcont">
    <i class="fa fa-spinner fa-3x fa-spin" ></i>
    <!-- <div class="col-xs-12 text-center">
        <h1>Votre proposition n'a pas retenu par l'administrateur. Merci</h1>
    </div> -->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var newresaap = true;
        const answer = <?php echo (!empty($answer) ? json_encode($answer) : "null"); ?>;

        var answerid =  "<?php echo (!empty($answerid) ? $answerid : ''); ?>";
        aapObject.sections.listItem.currentAnswer = answerid;
        if (answerid != ''){
            newresaap = false;
        }

        var formedit = <?php echo (!empty($pageedit) ? $pageedit : 'false'); ?>;

        var el_form = "<?php echo (isset($elform["_id"]) ? (string)$elform["_id"] : ''); ?>";

        var el_form_id = "<?php echo (isset($el_form_id) ? (string)$el_form_id : ''); ?>";

        var sendnotif = true;
        var validation = function(){
            if( typeof $('#titre').val() != "undefined" && typeof $('#description').val() != "undefined" &&  ($('#titre').val().trim()=='' || $("#description").val().trim()=="")){
                $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').hide();
            }
            $('#titre,#description').on('blur',()=>{
                if(typeof $('#titre').val() != "undefined" && typeof $('#description').val() != "undefined" &&  ($('#titre').val().trim()=='' || $("#description").val().trim()==""))
                    $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').hide();
                else
                    $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').show();
            })
        }
        //$canEdit = "<?= $canEdit ?>";
        if(newresaap && el_form_id != "" && !formedit){
            ajaxPost("#aapformcont", baseUrl+'/survey/answer/index/id/new/form/'+el_form_id,
                { url : window.location.href },
                function(data){
                    validation();
                    if (notEmpty(answerId) ){
                        const hrefForm = window.location.href + ".answer." + answerId;
                        window.history.pushState(null,null, hrefForm);
                        if (typeof answerId != "undefined"){
                            $('.newsheet').data('url', $('.newsheet').data('url')+answerId);
                            $('.newdashboard').data('url', $('.newdashboard').data('url')+answerId);
                        }

                        $("li#questiontitre.questionBlock input#titre").focusout( function( event ) {
                            var tthis = $(this);
                            if (tthis.val() != "" && sendnotif) {
                                ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newproposition/answerid/' + answerId,
                                    {
                                        proposition : tthis.val(),
                                        url : window.location.href
                                    },
                                    function (data) {
                                        sendnotif = false;
                                    }, "html");
                            }
                        });
                    }
                },"html");
        }else if (formedit && el_form_id != ""){
            ajaxPost("#aapformcont", baseUrl+'/survey/form/edit/id/'+el_form_id,
                null,
                function(){
                    validation();
            },"html");
        }
        else if(answerid != '' && !newresaap) {
                ajaxPost("#aapformcont", baseUrl+'/survey/answer/index/id/'+answerid+'/form/'+el_form_id+'/mode/w',
                    { url : window.location.href },
                function(){
                    validation();
                    if(notEmpty(answer) && notEmpty(answer.project) && notEmpty(answer.project.id)){
                        $('#aapStep1 .sectionStepTitle,#aapStep1stepper .stepDesc').text("Projet");
                        $(".sectionStepTitle").css("background","#9fbd38");
                        $('label[for="titre"]').children('h4').text("Nom du projet");
                        $('label[for="description"]').children('h4').text("Description du projet");
                        $('label[for="uploaderimage"]').children('h4').text("Ajouter une image pour votre projet");
                    }
                    
                    ajaxPost(
                        null,
                        baseUrl+'/survey/answer/countvisit/answerid/'+answerid+'/formId/'+el_form_id,
                        null,
                        function(data){
                            propostionObj.refreshNewCounter(answerid);
                        },
                        "json"
                    );
                },"html");
        }
        var statusicon = <?= json_encode(Aap::$statusicon) ?>;
        var formatState = function (state) {
            if (!state.id) { return state.text; }
            var colori = "statusicon";
            if(state.id && state.id == "abandoned")
                colori = "statusicondanger";
            var $state = jQuery(
                '<span><i class="'+colori+' fa fa-'+statusicon[state.id]+' +"></i> ' + state.text + '</span>'
            );

            return $state;
        };
        $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js", function( data, textStatus, jqxhr ) {
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
            }).appendTo("head");

            let optionSelect2 = {
                formatSelection: formatState,
                closeOnSelect: false,
                //width: '80%',
                placeholder: "aucun statut",
                containerCssClass : 'aapstatus-container',
                dropdownCssClass: "aapstatus-dropdown"
            };
            let $select2 = $(".statustags").select2(optionSelect2);
            var edit = false;
            $('.st-enable-btn').hide();
            $('.select2-container-multi.aapstatus-container .select2-choices').on('click',function(){
                var btn = $(this);
                if(edit == false){
                    $('.st-enable-btn').trigger('click');
                    $('.st-enable-btn').show();
                    edit = true
                }
            })
            $('.st-enable-btn').on('click',function(){
                $(this).hide();
                edit =false;
            })
        })
    });

</script>
