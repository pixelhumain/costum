<style>
    .userNameAnswer {
        text-align: initial;
        font-size: 20px;
    }
    .tooltip {
        font-size: 20px ;
        position: relative;
        display: inline-block;
        /* border-bottom: 1px dotted black; */
        cursor: pointer;
        color: "black";
    }

    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: #333;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px;
        position: absolute;
        z-index: 1;
        bottom: 100%;
        left: 50%;
        margin-left: -60px;
        opacity: 0;
        transition: opacity 0.3s;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 1;
    }
</style>
<?php 
    $userName = [];
    $anserAction = [];
    if (isset($allAnswers)) {
        foreach($allAnswers as $key => $value) {
            $getName = PHDB::find("citoyens", array('_id' => new MongoId($value["user"])), ['name']);
            $userName[$value['user']] = $getName[$value["user"]]['name'];
            $anserAction[$key] = Aap::listItemCoFormButtons(false, true, false, false,  $key, false);
        }
    }

?> 

<table class="table">
  <thead>
    <tr id="stepName"></tr>
  </thead>
  <tbody id="stepScoreContent"></tbody>
</table>

<script>
    let isNotEmptyAnswer = (jsonHelper.pathExists('aapObject.allAnswers') && aapObject.allAnswers && Object.keys(aapObject.allAnswers).length > 0);
    let answerContent = <?= json_encode($allAnswers) ?>;
    let allAnswers = (isNotEmptyAnswer) ? aapObject.allAnswers : (answerContent && Object.keys(answerContent).length > 0) ? answerContent : "";
    let user = <?= json_encode($userName) ?>;
    let answerAction = <?= json_encode($anserAction) ?>;
    let show = "<?= $show ?>";

    // titre tableau
    let stepReference = [];
    setTimeout( function () { 
        if (show == "score") {
            if (jsonHelper.pathExists('aapObject.formInputs')) {
                let html = "";
                html += "<th scope='col'></th>";
                // html += "<th scope='col'>Nom</th>";
                $.each(aapObject.formInputs, function(stepRef, stepValue) {
                    stepReference.push(stepRef);
                    html += `<th class="step-${stepRef}" scope="col">${Object.values(stepValue)[0].name}</th>`;
                })
                // html += `<th class="step-${stepRef}" scope="col">Action</th>`;    
                $('#stepName').html(html);
            }
    

        // fonction qui calcule les checkbox et radio
        function scoreOutput (stepReference, content) {
            let total = 0;
            let list = "";
            if (jsonHelper.pathExists("aapObject.formInputs." + stepReference)) {
                let allInput = Object.values(aapObject.formInputs[stepReference])[0].inputs;
                $.each(content, function(k, v) {
                    let isNotUndefined = typeof allInput[k] != "undefined" && typeof allInput[k].type != "undefined";
                    if (isNotUndefined) {
                        if (allInput[k].type.includes("radioNew") && !isNaN(v)) {
                            total = total + Number(v);
                        }
                        if (allInput[k].type.includes("checkboxNew") && Array.isArray(v)) {
                            total = total + v.length;
                        }
                        mylog.log("sfkvhsdkv", allInput[k])
                        if (allInput[k].type.includes("text") || allInput[k].type.includes("date") || allInput[k].type.includes("number") || allInput[k].type.includes("email")) {
                            // mylog.log("sfkvhsdkv", v);
                            total = "NaN";
                            list += "<b>" +allInput[k].label + "</b> : " + v + "<br>";
                        }
                    }
                })
            }
            return (total != "NaN") ? total : list;
        }
        // content
        // if (jsonHelper.pathExists('aapObject.allAnswers')) {
            let htmlRender = ""; 
 
            $.each(allAnswers, function(answerReference, ansewContent) {
                htmlRender += "<tr>";
                htmlRender += "<td class='userNameAnswer'>"+user[ansewContent.user]+"</td>";
                let countResult = 0;
                let colRef = 0;
                $.each(stepReference, function(reference, value) {
                    if (typeof ansewContent.answers != "undefined" && typeof ansewContent.answers[value] != "undefined") {
                        scoreOutput(value, ansewContent.answers[value]);
                        countResult = scoreOutput(value, ansewContent.answers[value]);
                    } 
                    let isString = (typeof countResult == 'string');
                    htmlRender += `<td class='scoreContent' ${(isString) ? "style='text-align: center;'" : ""} >
                                    <div style="opacity: 1;" class="tooltip">
                                        ${(isString) ? `<div data-id="${answerReference}" data-value="${scoreOutput(value, ansewContent.answers[value])}" class="btn-showInformation social-btn flex-center"><i class='fa fa-sticky-note-o'></i></div>` : `<a class="getanswer" data-step="${value}" data-ansid="${answerReference}" >${countResult}</a>`}
                                    </div>
                                   </td>`;
                    countResult = 0;
                    colRef ++
                })
                
                htmlRender += `
                    <td class='scoreContent text-center'>
                        <div class="social-btn flex-center getanswer" data-mode="r" data-step="" data-ansid="${answerReference}">
                            <i class="fa fa-eye editdeleteicon"></i><span style="font-size: 14px;">Voir</span>
                        </div>
                    </td>`;
                htmlRender += "</tr>";  
            })
            $('#stepScoreContent').html(htmlRender);
            $(".btn-showInformation").on("click", function() {
                if ($(this).attr("class").includes('active')) {
                    $(this).html(`<div data-id="${$(this).data("id")}" data-value="${$(this).data("value")}" class="social-btn flex-center btn-showInformation"><i class='fa fa-sticky-note-o'></i></div>`);
                    $(this).attr("class", "btn-showInformation");
                } else {
                    $(this).html($(this).data("value"));
                    $(this).attr("class", "btn-showInformation active");
                }
            })
        // }
        } 

        $(".getanswer").on('click', function() {
            coInterface.showLoader("#mainDash");
            var answid = $(this).data("ansid");
            var stepRef = "#" + $(this).data("step");
            var getdatatype = "";
            // showStepForm(""+stepRef+"");
            if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
                ajaxPost("#mainDash", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                        null,
                        function(){
                            if (typeof hashUrlPage != "undefined") {
                                history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid+".contextId."+contextId+".contextType."+contextType);
                                showStepForm(stepRef);
                            }
                        },"html");
            }else{
                ajaxPost("#mainDash", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                        null,
                        function(){
                            if (typeof hashUrlPage != "undefined") {
                                history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid);
                                showStepForm(stepRef);
                            }
                        },"html");
            }
            
        });
    }, 100);

    
</script>

