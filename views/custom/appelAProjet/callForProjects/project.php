<style type="text/css">
    .aap-project .new-aap-project{
        margin-top: 5px;
        display: inline-block;
        float: left;
    }
    .aap-project .dayEvent h2 .date-label {
        color: #7da53d !important;
    }
    .aap-project .searchEntityContainer.events .entityName h4 a {
        color: #7da53d !important;
    }
    .aap-project .letter-orange {
        color: #7da53d!important;
    }
    .aap-project .dropdown .btn-menu, .list-aap-container #filterContainer .dropdown .btn-menu, .cfm .searchObjCSS .dropdown .btn-menu, .cfm .searchObjCSS .filters-btn {
        padding: 3px 11px;
        font-size: 11px;
    }
    .aap-project .searchBar-filters .search-bar {
        height: 29px;
        font-size: 11px;
    }
    .aap-project .searchBar-filters .main-search-bar-addon {
        height: 29px;
        font-size: 12px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .aap-project .container-filters-menu .searchBar-filters .input-group-addon {
        background-color: #7da53d!important;
    }
    .aap-project .container-filters-menu #input-sec-search .input-group-addon {
        height: 29px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .aap-project .searchObjCSS #input-sec-search .input-global-search {
        border-color: #7da53d !important;
        height: 29px !important;
        font-size: 11px !important;
    }
    .aap-project .new-aap-project{
        border-radius: 13px;
        border-color: #7da53d !important;
    }
</style>
<?php
    $elform = PHDB::findOneById(Form::COLLECTION,$el_form_id);

    $canAdmin = false;
    if((isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]))) || Form::canAdmin((string)$elform["_id"]))
        $canAdmin = true;

    $formsSousOrga = [];
	$allFormsSousOrga = [];
	$isActiveSousOrga = !empty($el["el"]["oceco"]["subOrganization"]) && $el["el"]["oceco"]["subOrganization"]==true ? true : false;
    $currentOrga = PHDB::findOneById(Organization::COLLECTION,$el["id"]);

    $params = array(
                '$or' => array(
                    ["_id"=> new MongoId($el["id"])],
                    ["_id" => new MongoId(@$currentOrga["parentId"])]
                )
            );
    if($isActiveSousOrga){
        $params['$or'][] = array("parentId" => @$el["id"]);
    }
    $organizationAndSousOrga = PHDB::find(Organization::COLLECTION,$params,array("name","slug"));
    $query = array();
    foreach ($organizationAndSousOrga as $kso => $vso) {
        $query["links.contributors.".$kso] = ['$exists' => true ];
        $query["parent.".$kso] = ['$exists' => true ];
    }
?>
<div class="container">
    <div class="row aap-project">
        <div class="col-xs-12 text-center">
            <div id='filterContainerAgenda' class='searchObjCSS'></div>
        </div>
        <div class="content-answer-search col-xs-12">
            <div class="headerSearchContainerL col-xs-12 no-padding"></div>
            <div class="col-xs-12 bodySearchContainer margin-top-10">
                <div class="no-padding col-xs-12" id="dropdown_searchL">
                    <p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>
                </div>
                <div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
    var el =  <?php echo json_encode(@$el); ?>;
    var query = <?php echo json_encode(@$query); ?>;
	var paramsFilterProjects= {
	 	container : "#filterContainerAgenda",
	 	/*urlData : baseUrl+"/co2/search/agenda",
	 	loadEvent: {
	 		default : "project"
	 	},*/
	 	defaults : {
            notSourceKey:true,
 			types : ["projects"],
 			filters : {
                '$or' : {
                      "parent.<?= @$el["id"] ?>" : {'$exists':1},
                      ...query                  
                    }
             }
 		}, 

 		header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						calendar : true,
						map : true
					}
				}
			}
	 	},
 		results : {
             dom: "#dropdown_searchL",
 			renderView : "directory.elementPanelHtml"
 		},
	 	filters : {
	 		text : true,
	 		scope : true,
	 	}
	};
    var el =  <?php echo json_encode(@$el); ?>;
    var canAdmin = <?php echo json_encode(@$canAdmin); ?>;
    var hashAap = location.hash.substring(1);
    var hashArray = hashAap.split('.');
    if(hashArray.includes("formid") && hashArray.includes("slug") && hashArray.includes("page")){
        var formid = hashArray.indexOf("formid")+1;
        var slugaap = hashArray.indexOf("slug")+1;
        $('[href="#agenda"].menu-app').attr("href", "#oceco.slug."+hashArray[slugaap]+".formid."+hashArray[formid]+".aappage.agenda");
    }else{
        bootbox.alert("Veullez choisir un formulaire", function(){ 
            urlCtrl.loadByHash("#welcome");
        }); 
    }

	if(notNull(appConfig) && typeof  appConfig.calendarOptions != "undefined"){
		paramsFilterProjects.agenda={
			options : {
				calendarConfig : appConfig.calendarOptions 
			}
		}
	}

	paramsFilterProjects.defaults.sourceKey= /*"proceco"*/el.el.slug;

	// if(notNull(appConfig) && typeof appConfig.filters != "undefined"){
	// 	if(typeof appConfig.filters.costumContext != "undefined" && appConfig.filters.costumContext)		
	// 		paramsFilterProjects.defaults.filters['organizer.'+costum.contextId] = {'$exists':1};
		
	// }

	if(notNull(appConfig) && typeof appConfig.results != "undefined")
		$.extend(paramsFilterProjects.results,appConfig.results);
	if(notNull(appConfig) && typeof appConfig.extendFilters != "undefined"){
		$.extend(paramsFilterProjects.filters, appConfig.extendFilters);
	}

	jQuery(document).ready(function() {
        var filterSearch={};
		filterSearch = searchObj.init(paramsFilterProjects);
        filterSearch.search.init(filterSearch);
        if(canAdmin)
            $("#filterContainerAgenda #costum-scope-search").after("<a href='javascript:;' class='margin-left-5 new-aap-project btn-sm btn btn-default'> <i class='fa fa-plus'></i> "+trad["addProject"]+"</a>")
        $("#filterContainerAgenda .container-filters-menu .new-aap-project").off().on("click",function(){
            var dyfProject={
                "beforeBuild":{
                    "properties" : {
                        /*organizer : {
                            inputType : "finder",
                            id : "organizer",
                            label : tradDynForm.whoorganizedevent,
                            initType: ["projects", "organizations"],
                            multiple : true,
                            search : {  
                                filters: {
                                    'slug' :aapObject.formParent._id.$id,
                                    'answers.aapStep1.titre' : {'$exists':true},
                                    'project.id' : {'$exists':true}
                                }
                            },
                            initBySearch : true,
                            initMe:false,
                            initContext:false,
                            initContacts:false,
                            openSearch :true,
                        },*/
                    }
                },
                "onload" : {
                    "actions" : {
                        "setTitle" : trad.addProject,
                        "src" : { 
                            "infocustom" : "Remplir le champ"
                        },
                        "hide" : {
                            "breadcrumbcustom" : 1,
                            "urlsarrayBtn" : 1,
                            "parentfinder" : 1,
                            "removePropLineBtn" : 1
                        }
                    }
                },
            }
            dyfProject.afterSave = function(data){
                mylog.log("beforeSaveAap",data);
                dyFObj.commonAfterSave(data, function(){
                    var tplCtx = {
                        id : data.id, 
                        collection : "projects"
                    }

                    tplCtx.value ={};
                    tplCtx.value[el.id] = {
                        name : aapObject.elementAap.el.name,
                        type : aapObject.elementAap.type
                    }
                    dataHelper.path2Value({ ...tplCtx, path : "parent"}, function(params) {
                        dataHelper.path2Value({ ...tplCtx, path : "links.contributors"}, function(params) {
                            toastr.success(trad.saved);
                            urlCtrl.loadByHash(location.hash);
                        })
                    })
                });
            }
            dyFObj.openForm('project',null, null,null,dyfProject);
        })
	});
</script>