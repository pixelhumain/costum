<style>
    .xdebug-var-dump{
        text-align: left;
    }
</style>
<?php 
    HtmlHelper::registerCssAndScriptsFiles(["/js/appelAProjet/row-merge-bundle.min.js"], Yii::app()->getModule('costum')->assetsUrl); 
    $cssAnsScriptFilesTheme = array(
        /*'/plugins/DataTables/media/css/DT_bootstrap.css',
        '/plugins/DataTables/media/js/jquery.dataTables.min.1.10.4.js',
        '/plugins/DataTables/media/js/DT_bootstrap.js',*/
        "/plugins/simplemde-markdown-editor/simplemde.min.css",
        '/plugins/simplemde-markdown-editor/simplemde.min.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    $initImage = Document::getListDocumentsWhere(
        array(
            "id"=> $params,
            "type"=>'form',
            "subKey"=>'imageSignatureEmail',
        ), "image"
    );
?>
<style>
    .table-all-email tr td{
        vertical-align: middle !important;
        font-weight: bold;
        border : 2px solid #ddd !important
    }
    .communication-tools-container .preview-item{
        position: relative;
        width: 100%;
        height: 100%;
        background-color: #dcdcdca8;
        margin-bottom: 5px;
        padding: 30px;
    }
    .communication-tools-container .preview{
        position: relative;
        width: 100%;
        max-height: 800px;
        overflow-y: auto;
    }

    .communication-tools-container .command-list{
        position: relative;
        width: 100%;
        max-height: 200px;
        overflow-y: auto;
        background-color: #dcdcdca8;
        padding: 15px;
        margin-bottom: 5px;
    }
    .communication-tools-container .mail-content{
        position: relative;
        background-color: #dcdcdca8;
        padding: 15px;
    }
    .communication-tools-container .editor-toolbar{
        z-index: 2 !important;
    }
    .communication-tools-container .popover,
    .communication-tools-container .popover-title,
    .communication-tools-container .popover-content{
        max-width: 100%;
        background: #f9f6f6 !important;
        color: #110101;
    }
    .communication-tools-container .popover{
        max-height: 400px;
        overflow-y: auto;
    }
    .communication-tools-container .popover .popover-content .btn{
        width: auto !important;
    }
    .communication-tools-container .preview-item .count{ 
        position: absolute;
        top: 4px;
        left: 7px;
        font-weight: bold;
    }
    .communication-tools-container .filters-activate{
        display: inline-block !important;
        padding: 5px 5px;
        background: #7da53d;
        margin: 0px 2px;
        border-radius: 5px;
        color: #1a1717;
        font-weight: bold;
    }
    .communication-tools-container .filters-activate .fa-times-circle{
        display: none;
    }
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-left communication-tools-container" >
  <div class="col-md-12 col-sm-12 col-xs-12" id="navigationAdmin">
    <div class="col-sm-12 col-xs-12 text-center">
      <h3>Dupliquer un ou plusieurs dossiers</h3>
    </div>
    <div class="row">
        <div class="col-xs-12 active-filters">
            <h3>1 - DOSSIERS SÉLECTIONNÉS (<span class="dossier-counter" class="text-green-k"></span> dossier(s))</h3>
            <div class="dossier-filters">Pas de filtre</div>
        </div>
        <div class="col-xs-12">
            <h3>2 - POUR QUEL ANNÉE ?</h3>
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="new-year"> <?php echo Yii::t("common","Year") ?> </label>
                    <select name="" id="new-year" class="form-control">
                        <option value=""></option>
                        <?php for ($i= ((int)date('Y') - 2); $i < ((int)date('Y')+10); $i++) {     
                            echo "<option value='".$i."'>".$i."</option>";
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <button class="btn btn-default btn-duplicate-prop" disabled="true">
                    <?php echo Yii::t("common","Duplicate") ?>
                </button>
            </div>
        </div>

        <div class="col-xs-12">
            <h3>3 - <?php echo Yii::t("common","results") ?> </h3>
            <div class="col-xs-12 col-md-6 col-sm-6">
                <label for="">Dupliqués <span id="duplicated-counter"></span></label>
                <table class="table">
                    <tbody id="tbody-duplicated" style="color:green">
                        <tr><td></td></tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6">
                <label for="">Non dupliqués  <small class="text-danger">(Raison : <?= Yii::t("common", "Already duplicated") ?>)</small> <span id="non-duplicated-counter"></span></label>
                <table class="table">
                    <tbody id="tbody-no-duplicated" style="color:red">
                        <tr><td></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
</div>
<script>
    $(function(){
        if( exists(aapObject.formParent.params) &&
            exists(aapObject.formParent.params.communicationToolsRoles) &&
            Array.isArray(aapObject.formParent.params.communicationToolsRoles) &&
            exists(aapObject.elementAap.el.links) &&
            exists(aapObject.elementAap.el.links.members) &&
            exists(aapObject.elementAap.el.links.members[userId]) &&
            exists(aapObject.elementAap.el.links.members[userId].roles) &&
            Array.isArray(aapObject.elementAap.el.links.members[userId].roles))
        {
            var filteredArray = aapObject.formParent.params.communicationToolsRoles.filter(function(n) {
                return aapObject.elementAap.el.links.members[userId].roles.indexOf(n) !== -1;
            });
            mylog.log(filteredArray,"filteredArray");
            if(filteredArray.length != 0){
                var duplicatePropObj = {
                    init :function(obj){
                        var copyObj =  Object.assign({}, obj);
                        copyObj.getData(copyObj);
                        copyObj.events(copyObj);
                        //copyObj.duplicateProposals(copyObj);
                        mylog.log(copyObj.proposition,"kratos");
                        $('.dossier-counter').text(copyObj.proposition.count.answers)
                    },
                    proposition : {},
                    depositors : {},
                    events : function(obj){
                        var htmlFilters = "";
                        if(aapObject.sections.proposition.activeFilters.text != ""){
                            htmlFilters = `<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" data-type="filters" data-key="vote" data-value="En evaluation" data-field="status" data-event="filters" data-filterk="status" data-original-title="" title=""><i class="fa fa-times-circle"></i><span class="activeFilters" data-type="filters" data-key="vote" data-value="En evaluation" data-field="status" data-event="filters" data-filterk="status">Contient : ${aapObject.sections.proposition.activeFilters.text}</span></div>`;
                        }
                        htmlFilters += $('#activeFilters').html();
                        $('.dossier-filters').html(htmlFilters);
                        
                        $('#new-year').off().on("change",function(){
                            if(notEmpty($(this).val())){
                                $(".btn-duplicate-prop").removeAttr("disabled")
                            }else
                                $(".btn-duplicate-prop").attr("disabled","true")
                        })

                        $(".btn-duplicate-prop").off().on('click',function(){
                            if($('#new-year').val() != ""){
                                obj.duplicateProposals(obj,$('#new-year').val());
                            }else{
                                toastr("<?= Yii::t("common","Invalid request") ?>");
                            }
                        })
                    },
                    getData : function(obj){
                        var params = aapObject.sections.proposition.activeFilters;
                        params.searchType = ["answers"];
                        params.name = params.text;
                        params.notIndexedQuery = true;
                        params.fields = ["_id"];
                        //params.fields = ["answers.aapStep1"]
                        params.indexStep = "30";
                        ajaxPost(
                        null,
                        baseUrl+"/co2/aap/directory/source/"+costum.slug+"/form/"+aapObject.formParent._id.$id,
                        params,
                        function(data){
                                mylog.log("govalos",data.results);
                                obj.proposition = data;
                            },null,null,
                            {async : false}
                        )
                    },
                    getDepositors : function(obj){
                        mylog.log(userTemp,"userTemp")
                        var userTemp = [];
                        $.each(obj.proposition.results,function(k,v){
                            userTemp.push(v.user)
                        })
                        mylog.log(userTemp,"userTemp")
                        ajaxPost('',baseUrl+"/co2/element/get/type/citoyens", 
                            {id : userTemp,fields:["name","profilMediumImageUrl"]},
                            function(data){
                                obj.depositors=data.map;
                            },null,null,
                            {async : false})
                    },
                    duplicateProposals : function(obj,year){
                        ajaxPost(
                            null,
                            baseUrl+"/co2/aap/duplicateproposition/",
                            {
                                "form" :  aapObject.formParent._id.$id,
                                "answers" : Object.keys(obj.proposition.results).join("-"),
                                "newYear" : year
                            },
                            function(data){
                                $("#tbody-no-duplicated,#tbody-duplicated").html("");
                                $("#duplicated-counter").text("("+Object.keys(data.duplicated).length+")");
                                $("#non-duplicated-counter").text("("+Object.keys(data.existsduplicated).length+")");
                                if(Object.keys(data.existsduplicated).length != 0)
                                    obj.tableResults(obj,"#tbody-no-duplicated",data.existsduplicated);
                                if(Object.keys(data.duplicated).length != 0)
                                    obj.tableResults(obj,"#tbody-duplicated",data.duplicated);
                            },null,null,{async:false}
                        );
                    },
                    tableResults : function(obj,appendSelector,data){
                        var html = "";
                        $.each(data,function(k,v){
                            html += `<tr><td>${v.answers.aapStep1.titre} (${v.answers.aapStep1.year})</td></tr>`;
                        })
                        $(appendSelector).html(html);
                    }
                }
                if(aapObject.sections.proposition.activeFilters.types[0] == "answers"){
                    duplicatePropObj.init(duplicatePropObj);
                }
            }else{
                $(".communication-tools-container").html("<h3 class='text-center text-red '>Vous n'êtes pas autorisé</h3>");
            }
        }
    })
</script>
<!-- 

Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée à titre provisoire pour calibrer une mise en page, le texte définitif venant remplacer le faux-texte dès qu'il est prêt ou que la mise en page est achevée. Généralement, on utilise un texte en faux latin, le Lorem ipsum ou Lipsum.
* Total : {depense.total}
* Financé : {depense.funded}
* Dépensé : {depense.spent}
* Subvention : {depense.subvention}
 -->