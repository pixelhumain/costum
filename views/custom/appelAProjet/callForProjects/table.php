<?php
    use CacheHelper;
    HtmlHelper::registerCssAndScriptsFiles(
        [
            '/plugins/handsometable/js/handsontable.full.min.js',
            '/plugins/handsometable/js/all.js',
            '/plugins/handsometable/css/handsontable.full.min.css',
           //'/plugins/handsometable/js/multi-select.js',
            /*'/plugins/handsometable/js/jquery.handsontable-controls.js',
            '/plugins/handsometable/js/jquery.multiselect.js',
            '/plugins/handsometable/css/jquery.handsontable-controls.css',*/
         //   '/plugins/handsometable/css/jquery.multiselect.css'
        ],
        Yii::app()->request->baseUrl);
    if(isset($elform['subType'])) {
        $subForms = PHDB::find(Form::INPUTS_COLLECTION , array("formParent" => (string)$elform["_id"]));
    } else {
        $subForms = PHDB::find(Form::COLLECTION , array("id" => array('$in' => $elform["subForms"])));
    }

?>
<style type="text/css">
    .list-aap-container {
        margin-right: 0 !important;
        margin-left: 0 !important;
        width: 100% !important;
    }
</style>
<style type="text/css">
    .portlet-header {
        margin-right: 30px;
        border: #c7c1c1 1px solid;
        padding: 2px 10px;
        border-radius: 6px;
    }

    .button-checkbox {
        border-bottom: #c7c1c1 .5px solid;
        margin: 10px;
    }

    .org-choice{
        font-size: 15px !important;
    }

    #aapHotTable THEAD TR:nth-child(1) TH {
        /*background: #5a5c5a; */
        border-right: #989e97 1px solid;
        border-color: #3f403f !important;
        font-weight: bold;
        color: #525050;
        font-size: 18px;
    }
    
    .tablebtn {
        font-size: 15px;
    }

    #aapHotTable TH,  #aapHotTable TD{
         border: 0px;
         border-right: #888b87 1px solid;
        font-size: 14px;
     }

    #aapHotTable table{
        border-collapse: separate;
        border-spacing: 0 10px;
        font-weight: bold;
    }

    #aapHotTable TD{
        background-color: #f6f6f9;
        padding: 10px;
        margin: 10px;
    }

    .handsometable tr:not(:nth-child(1))  {
        color: #2c323c !important;
        margin-bottom: 10px;
        -webkit-box-shadow: 0 5px 12px -12px rgba(0,0,0,.29);
        -moz-box-shadow: 0 5px 12px -12px rgba(0,0,0,.29);
        box-shadow: 0 5px 12px -12px rgba(0,0,0,.29);
    }
    

    #aapHotTable  {
        font-size: 20px !important;
    }


    #aapHotTable THEAD TR:nth-child(1) TH .collapsibleIndicator {
      box-shadow: 0px 0px 0px 6px #83c67b;
      -webkit-box-shadow: 0px 0px 0px 6px #69975f;
      background: #6aba73;
      border-color: #436829;
    }
    .handsontable .htDimmed {
        color: #040404 !important;
    }


    .aapHotTableFullScreen{
        position: fixed;
        top: 0;
        left: 0;
        right:0;
        bottom:0;
        z-index: 100000;
        background-color: #000;
    }
    #aapHotTable1{
        position:relative;
        width:100%;
        overflow-x: scroll; 
        overflow-y:hidden
    }
    #aapHotTable1 div{
        position:relative;
        height: 5px;
    }

    .handsontable .changeType {
        border: 2px solid #cecac5;
        border-radius: 6px;
        font-size: 11px;
        padding: 5px;
        background-color: #fff !important;
    }

    .handsontable .changeType:before {
        content: '\2630' !important;
    }

    .handsontable .htDisabled{
        display: none;
    }

    .simple-pagination.pageTable{
        display: none;
    }
</style>

<div class="container-fluid padding-top-10">
    <div id="hot-controls-custom"></div>
    <div id="hot-controls-callback"></div>
<!--    <button id="export-file" class="aap-breadrumbbtn btn-xs">Télécharger en CSV</button>
-->

    <div class="form-group ">
        <div class="dropdown sppec">

            <!--<button id="toogle-table-view" class="aap-breadrumbbtn btn-xs tablebtn">
                <i class="fa fa-expand"></i>
                Plein ecran
            </button>
-->
            <button class="aap-breadrumbbtn btn-xs dropdown-toggle tablebtn" style="color: #1B4F95;" data-toggle="dropdown">
                <i class="fa fa-filter"></i>
                Filtrer les colonnes
                <span class="caret"></span>
            </button>

            <ul class="dropdown-menu sortableselect" style="overflow: scroll;max-height: 350px;">
                    <li class="button-checkbox" data-color="default">
                        <div class="org-choice" style="font-size: 12px;">
                            <input type="button" class="btn unselectall" data-loc="territoire" value="Tout désélectionner">
                            <input type="button" class="btn selectall" data-loc="territoire" value="Tout sélectionner">
                        </div>
                    </li>
                    <li class="button-checkbox" data-color="default">
                        <div class="org-choice">
                            <input  class="checkbox1" type="checkbox" checked="" data-inputlabel="Titre" data-input="titre">
                            <label for="checkbox1">
                                titre
                            </label>
                            <label class="pull-right portlet-header"><i class="fa fa-arrows-v"></i></label>
                        </div>
                    </li>
                <li class="button-checkbox" data-color="default">
                    <div class="org-choice">
                        <input  class="checkbox1" type="checkbox" checked="" data-inputlabel="Status" data-input="status">
                        <label for="checkbox1">
                            status
                        </label>
                        <label class="pull-right portlet-header"><i class="fa fa-arrows-v"></i></label>
                    </div>
                </li>
                <?php
                foreach ($subForms as $sbid => $subform){
                    if (isset($subform["inputs"])){
                        foreach ($subform["inputs"] as $inp => $input){
                            if (!empty($input["type"]) && in_array($input["type"] , ["text" , "textarea" , "email" , "tel" ,
                                "date" , "tpls.forms.emailUser" , "hidden" ,
                                "number" , "tpls.forms.cplx.radioNew" , "tpls.forms.costum.cosindni.tags"]) ) {
                ?>

                            <li class="button-checkbox" data-color="default">
                                <div class="org-choice">
                                    <input  class="checkbox1" type="checkbox" checked="" data-inputlabel="<?= @$input["label"] ?>" data-input="<?= $inp ?>">
                                    <label for="checkbox1">
                                        <?php echo @$input["label"] ?>
                                    </label>
                                    <label class="pull-right portlet-header"><i class="fa fa-arrows-v"></i></label>
                                </div>
                            </li>
                <?php
                            }
                        }
                    }
                }
                ?>

            </ul>
        </div>
    </div>

    <div id="aapHotTable1">
        <div></div>
    </div>
    <div id="aapHotTable" class="form-hot"></div>
</div>



<script type="text/javascript">


    const aapHotTable = document.getElementById('aapHotTable');

    /*function calculateSize() {
        var offset = Handsontable.dom.offset(aapHotTable);
        var availableWidth = Handsontable.dom.innerWidth(document.body) - offset.left + window.scrollX;
        var availableHeight = Handsontable.dom.innerHeight(document.body) - offset.top + window.scrollY;

        aapHotTable.style.width = availableWidth + 'px';
        //aapHotTable.style.height = availableHeight + 'px';
    }
    Handsontable.dom.addEvent(window, 'resize', calculateSize);
*/


    const hotaapbutton = document.querySelector('#export-file');

    var hotData = <?php echo json_encode($allAnswers) ?>;

    var pathInputTitre = '';

    var hotElform = <?php echo json_encode($elform) ?>;

    var hotSubform = <?php echo json_encode($subForms) ?>;

    var el_slug = '<?php echo $el_slug; ?>';

    if(!hotElform['subType']) {
        if(hotElform['params']['inputForAnswerName']) {
            if(hotElform['params']['inputForAnswerName']['title']) {
                for(const [keySubForm, valueSubForm] of Object.entries(hotSubform)) {
                    if(valueSubForm['inputs']) {
                        if(valueSubForm['inputs'][hotElform['params']['inputForAnswerName']['title']]) {
                            pathInputTitre = valueSubForm['id'] + '.' + hotElform['params']['inputForAnswerName']['title'];
                        }
                    }
                }
            }
        }
    }
    mylog.log('path input title', pathInputTitre);

    var optionsList = [
        { 'id' : "progress" , 'text' : "En cours" },
        { 'id' : "underconstruction" , 'text' : "En construction" },
        { 'id' : "vote" , 'text' : "En evaluation" },
        { 'id' : "finance" , 'text' : "En financement" },
        { 'id' : "call" , 'text' : "Appel à participation" },
        { 'id' : "newaction" , 'text' : "nouvelle proposition" },
        { 'id' : "projectstate" , 'text' : "En projet" },
        { 'id' : "prevalided" , 'text' : "Pré validé" },
        { 'id' : "validproject" , 'text' : "Projet validé" },
        { 'id' : "voted" , 'text' : "Voté" },
        { 'id' : "finish" , 'text' : "Términé" },
        { 'id' : "suspend" , 'text' : "Suspendu" }
    ];

    var aapHotDataShema = {} , aapColHeaders = hotElform['subType'] ? [ "Titre" , "Status"] : ["Titre"],
        aapColumnsId = hotElform['subType'] ? [ "titre" , "status"] : ["titre"],
        aapColumns = hotElform['subType'] ? [
            {
                data: 'answers.aapStep1.titre',
                hideFromToggle: true,
                visible: true,
            },
            {
                data: getAapStatus('status'),
                //data: 'status',
                hideFromToggle: false,
                visible: true,
                editor: 'select2',
                renderer: customDropdownRenderer,
                select2Options: {
                    data: optionsList,
                    dropdownAutoWidth: true,
                    width: 'resolve',
                    multiple : true
                },
                /*renderer: MultiSelectRenderer,
                editor: 'mseditor',
                select: {
                    config: {
                        separator: ";",
                        valueKey: "value",
                        labelKey: "label"
                    },
                    options: [
                        { value: "SE", label: "Sweden" },
                        { value: "UK", label: "United Kingdom" }
                    ]
                },*/
                readOnly : false
            }
        ] : [
            {
                data: 'answers.' + pathInputTitre,
                hideFromToggle: true,
                visible: true,
            }
        ];

    var stringinputList = ["text" , "textarea" , "email" , "tel" ,
        "date" , "tpls.forms.emailUser" , "hidden" ,
        "number" , "tpls.forms.cplx.radioNew" , "tpls.forms.costum.cosindni.tags"];

    var arrayinputList = ["tpls.forms.cplx.checkboxNew" , "tpls.forms.tagsFix"];

    var hotaapsettings = {
        data: Object.values(hotData),
        dataSchema: { id: null, titre : null },
        colHeaders: aapColHeaders,
        fixedColumnsLeft: 1,
        height: 'auto',
        width: 'auto',
        colWidths: 300,
        columns: aapColumns,
        minSpareRows: 0,
        dropdownMenu: true,
        columnHeaderHeight: 50,
        bindRowsWithHeaders: true,
        fixedRowsBottom : true,
        collapsibleColumns: true,
        manualRowMove: true,
        filters: true,
        manualColumnResize: true,
        manualRowResize: true,
        contextMenu: {
            callback(key, selection, clickEvent) {
                // Common callback for all options
            },
            items: {
                "make_read_only": {
                    disabled : function () {
                        return true;
                    }
                },
                "row_above": {
                    disabled : false
                },
                "row_above": {
                    disabled : true
                },
                "clear_row" : {
                    disabled : true
                },
                "clear_column" : {
                    disabled : true
                },
                alignment: {
                    disabled : false
                },
                copy : {
                    name: 'Copier'
                },
                details : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-file"></i> Fiche de la proposition</b>';
                    },
                    callback(key, selection, clickEvent) {
                        coInterface.showLoader(".aapinputcontent");
                        var ansurl = hotconstructurl(hot.getSourceDataAtRow(selection[0].start.row) , "sheet");
                        smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl)
                    }
                },
                form : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-pencil-square"></i> Voir la proposition en details</b>';
                    },
                    callback(key, selection, clickEvent) {
                        <?php if ($canSeeDetail) { ?>
                        coInterface.showLoader(".aapinputcontent");
                        var ansurl = hotconstructurlgoto(hot.getSourceDataAtRow(selection[0].start.row) , "form");

                        if (isUserConnected){
                            window.open(baseUrl+ansurl, '_blank');
                        }
                        <?php } ?>
                    }
                },
                action : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-pencil-square"></i> Les actions </b>';
                    },
                    callback(key, selection, clickEvent) {
                        coInterface.showLoader(".aapinputcontent");
                        // console.log('kkkkeeeeey' , key, selection, clickEvent)
                        var ansurl = hotconstructurlinput(hot.getSourceDataAtRow(selection[0].start.row) , "aapStep4" , "suivredepense");

                        if (isUserConnected){
                            smallMenu.openAjaxHTML(baseUrl+ansurl);
                        }
                    }
                },
                selection : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-gavel"></i> Evaluation </b>';
                    },
                    callback(key, selection, clickEvent) {
                        coInterface.showLoader(".aapinputcontent");
                        var ansurl = hotconstructurlinput(hot.getSourceDataAtRow(selection[0].start.row) , "aapStep2" , "selection");

                        if (isUserConnected){
                            smallMenu.openAjaxHTML(baseUrl+ansurl);
                        }
                    }
                },
                financement : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-credit-card-alt"></i> Financement </b>';
                    },
                    callback(key, selection, clickEvent) {
                        <?php //if ($canEvaluate) { ?>
                        coInterface.showLoader(".aapinputcontent");
                        var ansurl = hotconstructurlinput(hot.getSourceDataAtRow(selection[0].start.row) , "aapStep3" , "financer");

                        if (isUserConnected){
                            smallMenu.openAjaxHTML(baseUrl+ansurl);
                        }
                        <?php //} ?>
                    }
                },
                supprimer : {
                    name() {
                        return '<b style="color: #d24c5c"><i class="fa fa-trash"></i> Supprimer </b>';
                    },
                    callback(key, selection, clickEvent) {
                        <?php //if ($canEvaluate) { ?>
                        bootbox.dialog({
                            title: "Confirmez la suppression",
                            message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                            buttons: [{
                                label: "Ok",
                                className: "btn btn-primary pull-left",
                                callback: function() {
                                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/deleteproposition/answerid/' + hot.getSourceDataAtRow(selection[0].start.row)["_id"]["$id"],
                                        {
                                            proposition: hot.getSourceDataAtRow(selection[0].start.row)["answers"]["aapStep1"]["titre"],
                                            url: window.location.href
                                        },
                                        function(data) {
                                            getAjax("", baseUrl + "/survey/co/delete/id/" + hot.getSourceDataAtRow(selection[0].start.row)["_id"]["$id"], function(data) {
                                                toastr.success("Proposition supprimé avec succès !");
                                                mylog.log(data, "datakoo");
                                                //urlCtrl.loadByHash(location.hash);
                                                //$this.parent().parent().parent().remove();
                                                if (typeof hot.getSourceDataAtRow(selection[0].start.row)["project"] != "undefined" && typeof hot.getSourceDataAtRow(selection[0].start.row)["project"]["id"] != "undefined" && hot.getSourceDataAtRow(selection[0].start.row)["project"]["id"] != "") {
                                                    bootbox.dialog({
                                                        title: "Supprimer le projet associer ?",
                                                        message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                                                        buttons: [{
                                                            label: "Ok",
                                                            className: "btn btn-primary pull-left",
                                                            callback: function() {
                                                                var url = baseUrl + "/" + moduleId + "/element/delete/id/" + hot.getSourceDataAtRow(selection[0].start.row)["project"]["id"] + "/type/projects";
                                                                var param = new Object;
                                                                param.reason = ""
                                                                ajaxPost(
                                                                    null,
                                                                    url,
                                                                    param,
                                                                    function(data) {
                                                                        if (data.result) {
                                                                            toastr.success(data.msg);
                                                                            hot.alter("remove_row" , selection[0].start.row)
                                                                        } else
                                                                            toastr.error(data.msg);
                                                                    }
                                                                );
                                                            }
                                                        },
                                                            {
                                                                label: "Annuler",
                                                                className: "btn btn-default pull-left",
                                                                callback: function() {
                                                                    hot.alter("remove_row" , selection[0].start.row)
                                                                }
                                                            }
                                                        ]
                                                    });
                                                } else {
                                                    hot.alter("remove_row" , selection[0].start.row)
                                                }
                                            }, "html");
                                        }, "html");


                                }
                            },
                                {
                                    label: "Annuler",
                                    className: "btn btn-default pull-left",
                                    callback: function() {}
                                }
                            ]
                        });
                        coInterface.showLoader(".aapinputcontent");

                        if (isUserConnected){

                        }
                        <?php //} ?>
                    }
                },
                comment : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-commenting"></i> commenter </b>';
                    },
                    callback(key, selection, clickEvent) {
                        commentObj.openPreview('answers', hot.getSourceDataAtRow(selection[0].start.row)["_id"]["$id"] , hot.getSourceDataAtRow(selection[0].start.row)["_id"]["$id"] , 'Commentaire');
                    }
                },
                transfer : {
                    name() {
                        return '<b style="color: #6cb234"><i class="fa fa-share"></i> transferer </b>';
                    },
                    callback(key, selection, clickEvent) {
                        smallMenu.openAjaxHTML(baseUrl + '/survey/answer/transferanswer/id/' + hot.getSourceDataAtRow(selection[0].start.row)["_id"]["$id"] );
                    }
                }
            }
        },
        allowInsertRow: false,
        allowInsertColumn: false,
        allowRemoveColumn: false,
        allowRemoveRow: false,
        afterOnCellMouseDown: function(event, coords) {
            //hot.selectRows(coords.row);
        },
        afterChange: function (change, source) {
                if (source === 'loadData') {
                    return; //don't save this change
                }
                // console.log("aaazzeee" , change);
                var sdataid = hot.getSourceDataAtRow(change[0][0])["_id"]["$id"];
                tplCtx = {
                    id : sdataid ,
                    collection : "answers",
                    path : change[0][1],
                    value : change[0][3]
                };
                if(tplCtx.path == "status"){
                    tplCtx.value = tplCtx.value.split(",")
                }

                dataHelper.path2Value( tplCtx, function(params) {
                    toastr.success('enregistré');
                } );
                //console.log("aaazzeee" , hot.getCellMeta(change[0][0],change[0][1]));
            },
        filters: true,
        language : 'fr-FR',
        licenseKey: 'non-commercial-and-evaluation'
    }

    function hotconstructurl(answer , page) {
        return 'slug.'+ el_slug + '.formid.' + answer.form + '.aappage.' + page + '.answer.' + answer["_id"]["$id"]
    }
    function hotconstructurlgoto(answer , page) {
        return '/costum/co/index/slug/'+ costum.slug +'#oceco.slug.'+ el_slug + '.formid.' + answer.form + '.aappage.' + page + '.answer.' + answer["_id"]["$id"]
    }
    function hotconstructurlinput(answer , ansformstep ,ansinputid ) {
        return '/survey/answer/getinput/id/'+ answer["_id"]["$id"] +'/form/'+answer.form +'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid;
    }

    function getAapStatus(inst1 , inst2 , inst3) {
        return function (arg1 , arg2 , arg3) {
            if (typeof arg1[inst1] != "undefined" && typeof arg2 == "undefined"){
                return arg1[inst1].join(',');
            } else if (typeof arg2 != "undefined") {
                return arg2;
            }
        }
    }

    function customDropdownRenderer(instance, td, row, col, prop, value, cellProperties) {
        var optionsListarr = {
            "progress" : "En cours" ,
            "underconstruction" : "En construction" ,
            "vote" : "En evaluation" ,
            "finance" : "En financement" ,
            "call" : "Appel à participation" ,
            "newaction" : "nouvelle proposition" ,
            "projectstate" : "En projet" ,
            "prevalided" : "Pré validé" ,
            "validproject" : "Projet validé" ,
            "voted" : "Voté" ,
            "finish" : "Términé" ,
            "suspend" : "Suspendu"
        };

        // console.log('eeeezz', value , instance , cellProperties);

        if (notNull(value)) {

            if (typeof value == "string") {
                var aartext = value.split(',');
                //aartext = aartext[0];
            }else{
                var aartext = value;
                //var aartext = value[0];
            }

            var rendertext = "";

            $.each(aartext, function (idst, st) {
                if (notNull(optionsListarr[st])) {
                    rendertext += optionsListarr[st]
                    if (idst != aartext.length - 1) {
                        rendertext += ',';
                    }
                }
            });

            value = rendertext;

            Handsontable.renderers.TextRenderer.apply(this, arguments);
        }

    }

    $.each(hotSubform, function (inputId , input) {
        if (typeof input.inputs != "undefined") {
            $.each(input.inputs, function (inp, inputData) {
                if (stringinputList.includes(inputData.type) && inputData.label != "titre") {
                    if (typeof inputData.label != "undefined") {
                        aapColHeaders.push(inputData.label);
                        let dataPath = "answers." + input.step + ".";
                        if(!hotElform['subType']) dataPath = "answers." + input.id + ".";
                        aapColumns.push({
                            data: dataPath + inp,
                            hideFromToggle: true,
                            visible: true,
                            readOnly : false
                        });

                        aapColumnsId.push(inp);

                    }
                } else if(arrayinputList.includes(inputData.type)){
                    /* if (typeof inputData.label != "undefined") {
                         aapColHeaders.push(inputData.label);

                         aapColumns.push({
                             data: 'answers.' + input.step + '.' + inp,
                             hideFromToggle: true,
                             visible: true
                         });
                     }*/
                } else if(inputData.type == "tpls.forms.cplx.address"){

                } else if(inputData.type == "tpls.forms.ocecoform.budget"){

                } else if(inputData.type == "tpls.forms.ocecoform.multiDecide") {

                }else if(inputData.type == "tpls.forms.ocecoform.financementFromBudget") {

                }else if(inputData.type == "tpls.forms.ocecoform.suiviFromBudget"){

                }

            });
        }
    });

    var hot = new Handsontable(aapHotTable, hotaapsettings);


    $(".unselectall").off().on("click",function() {
        $("input[data-type='"+$(this).data('loc')+"']").each(function(){
            // alert(JSON.stringify($(this)));
            $(this).prop('checked', false).change();
        });
    });

    $(".selectall").off().on("click",function() {
        $("input[data-type='"+$(this).data('loc')+"']").each(function(){
            // alert(JSON.stringify($(this)));
            $(this).prop('checked', true).change();
        });
    });


    var exportPlugin = hot.getPlugin('exportFile');
    var commentsPlugin = hot.getPlugin('comments');

    var config = {

        targetEl: $("#hot-controls-custom"),
        fileNamePrefix: "MyList",
        controls: [
            "<span style='padding: 0 20px 0 0; color:green;'>Control rendered below table, using targetEl</span>",
            "selector",
            "export",
            '<button class="btn">Selected columns</button>'

        ]
    };

/*    $(document).ready(function () {
        aapHotTable.handsontableControls($.extend({}, hotaapsettings, config));
    });*/

    /*export pdf*/
    /*hotaapbutton.addEventListener('click', () => {
        exportPlugin.downloadFile('csv', {
            bom: false,
            columnDelimiter: ';',
            columnHeaders: true,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Export_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\n',
            rowHeaders: false
        });
    });*/

    /*select2 config*/
    (function (Handsontable) {
        "use strict";

        var Select2Editor = Handsontable.editors.TextEditor.prototype.extend();

        Select2Editor.prototype.prepare = function (row, col, prop, td, originalValue, cellProperties) {

            Handsontable.editors.TextEditor.prototype.prepare.apply(this, arguments);

            this.options = {};

            if (this.cellProperties.select2Options) {
                this.options = $.extend(this.options, cellProperties.select2Options);
            }
        };

        Select2Editor.prototype.createElements = function () {
            this.$body = $(document.body);

            this.TEXTAREA = document.createElement('input');
            this.TEXTAREA.setAttribute('type', 'text');
            this.$textarea = $(this.TEXTAREA);

            Handsontable.dom.addClass(this.TEXTAREA, 'handsontableInput');

            this.textareaStyle = this.TEXTAREA.style;
            this.textareaStyle.width = 0;
            this.textareaStyle.height = 0;

            this.TEXTAREA_PARENT = document.createElement('DIV');
            Handsontable.dom.addClass(this.TEXTAREA_PARENT, 'handsontableInputHolder');

            this.textareaParentStyle = this.TEXTAREA_PARENT.style;
            this.textareaParentStyle.top = 0;
            this.textareaParentStyle.left = 0;
            this.textareaParentStyle.display = 'none';

            this.TEXTAREA_PARENT.appendChild(this.TEXTAREA);

            this.instance.rootElement.appendChild(this.TEXTAREA_PARENT);

            var that = this;
            this.instance._registerTimeout(setTimeout(function () {
                that.refreshDimensions();
            }, 0));
        };

        var onSelect2Changed = function (event) {
            this.close();
            this.finishEditing();
            // console.log("aaaazzz" , this , event);
        };
        var onSelect2Closed = function () {
            this.close();
            this.finishEditing();
        };
        var onBeforeKeyDown = function (event) {
            var instance = this;
            var that = instance.getActiveEditor();

            var keyCodes = Handsontable.helper.keyCode;
            var ctrlDown = (event.ctrlKey || event.metaKey) && !event.altKey; //catch CTRL but not right ALT (which in some systems triggers ALT+CTRL)

            //Process only events that have been fired in the editor
            if (!$(event.target).hasClass('select2-input') || event.isImmediatePropagationStopped()) {
                return;
            }
            if (event.keyCode === 17 || event.keyCode === 224 || event.keyCode === 91 || event.keyCode === 93) {
                //when CTRL or its equivalent is pressed and cell is edited, don't prepare selectable text in textarea
                event.stopImmediatePropagation();
                return;
            }

            var target = event.target;

            switch (event.keyCode) {
                case keyCodes.ARROW_RIGHT:
                    if (Handsontable.dom.getCaretPosition(target) !== target.value.length) {
                        event.stopImmediatePropagation();
                    } else {
                        that.$textarea.select2('close');
                    }
                    break;

                case keyCodes.ARROW_LEFT:
                    if (Handsontable.dom.getCaretPosition(target) !== 0) {
                        event.stopImmediatePropagation();
                    } else {
                        that.$textarea.select2('close');
                    }
                    break;

                case keyCodes.ENTER:
                    var selected = that.instance.getSelected();
                    var isMultipleSelection = !(selected[0] === selected[2] && selected[1] === selected[3]);
                    if ((ctrlDown && !isMultipleSelection) || event.altKey) { //if ctrl+enter or alt+enter, add new line
                        if (that.isOpened()) {
                            that.val(that.val() + '\n');
                            that.focus();
                        } else {
                            that.beginEditing(that.originalValue + '\n')
                        }
                        event.stopImmediatePropagation();
                    }
                    event.preventDefault(); //don't add newline to field
                    break;

                case keyCodes.A:
                case keyCodes.X:
                case keyCodes.C:
                case keyCodes.V:
                    if (ctrlDown) {
                        event.stopImmediatePropagation(); //CTRL+A, CTRL+C, CTRL+V, CTRL+X should only work locally when cell is edited (not in table context)
                    }
                    break;

                case keyCodes.BACKSPACE:
                case keyCodes.DELETE:
                case keyCodes.HOME:
                case keyCodes.END:
                    event.stopImmediatePropagation(); //backspace, delete, home, end should only work locally when cell is edited (not in table context)
                    break;
            }

        };

        Select2Editor.prototype.open = function (keyboardEvent) {
            this.refreshDimensions();
            this.textareaParentStyle.display = 'block';
            this.instance.addHook('beforeKeyDown', onBeforeKeyDown);

            this.$textarea.css({
                height: $(this.TD).height() + 4,
                'min-width': $(this.TD).outerWidth() - 4
            });

            //display the list
            this.$textarea.show();

            //make sure that list positions matches cell position
            //this.$textarea.offset($(this.TD).offset());

            var self = this;
            this.$textarea.select2(this.options)
                .on('change', onSelect2Changed.bind(this))
                .on('select2-close', onSelect2Closed.bind(this));

            self.$textarea.select2('open');

            if (keyboardEvent && keyboardEvent.keyCode) {
                var key = keyboardEvent.keyCode;
                var keyText = (String.fromCharCode((96 <= key && key <= 105) ? key-48 : key)).toLowerCase();
                self.$textarea.select2('search', keyText);
            }
        };

        Select2Editor.prototype.init = function () {
            Handsontable.editors.TextEditor.prototype.init.apply(this, arguments);
        };

        Select2Editor.prototype.close = function () {
            this.instance.listen();
            this.instance.removeHook('beforeKeyDown', onBeforeKeyDown);
            this.$textarea.off();
            this.$textarea.hide();
            Handsontable.editors.TextEditor.prototype.close.apply(this, arguments);
        };

        Select2Editor.prototype.val = function (value) {
            if (typeof value == 'undefined') {
                return this.$textarea.val();
            } else {
                this.$textarea.val(value);
            }
        };


        Select2Editor.prototype.focus = function () {

            this.instance.listen();

            // DO NOT CALL THE BASE TEXTEDITOR FOCUS METHOD HERE, IT CAN MAKE THIS EDITOR BEHAVE POORLY AND HAS NO PURPOSE WITHIN THE CONTEXT OF THIS EDITOR
            //Handsontable.editors.TextEditor.prototype.focus.apply(this, arguments);
        };

        Select2Editor.prototype.beginEditing = function (initialValue) {
            var onBeginEditing = this.instance.getSettings().onBeginEditing;
            if (onBeginEditing && onBeginEditing() === false) {
                return;
            }

            Handsontable.editors.TextEditor.prototype.beginEditing.apply(this, arguments);

        };

        Select2Editor.prototype.finishEditing = function (isCancelled, ctrlDown) {
            this.instance.listen();
            return Handsontable.editors.TextEditor.prototype.finishEditing.apply(this, arguments);
        };

        Handsontable.editors.Select2Editor = Select2Editor;
        Handsontable.editors.registerEditor('select2', Select2Editor);

    })(Handsontable);

    (function (Handsontable) {

        var MultiSelectEditor = Handsontable.editors.TextEditor.prototype.extend();

        MultiSelectEditor.prototype.prepare =  function (row, col, prop, td, originalValue, cellProperties) {
            Handsontable.editors.TextEditor.prototype.prepare.apply(this, arguments);

            const { type } = cellProperties
            this.type = type

            const selectOptions = R.prop('select', cellProperties)
            this.selectOptions = R.defaultTo({}, selectOptions)

            this.valueKey = getValueKey(this.selectOptions) || 'value'
            this.labelKey = getLabelKey(this.selectOptions) || 'label'

            if (type === 'numeric') {
                this.separator = '|'
                this.maxItemCount = 1
                this.originalValue = isNumber(originalValue) ? originalValue.toString() : originalValue
            } else {
                this.separator = getSeparator(this.selectOptions) || ','
                this.maxItemCount = getMaxItemCount(this.selectOptions) || -1
            }
        }

        MultiSelectEditor.prototype.createElements = function () {
            this.TEXTAREA = document.createElement('select')
            this.TEXTAREA.select = () => {}
            this.TEXTAREA.tabIndex = -1
            addClass(this.TEXTAREA, 'handsontableInput')
            this.TEXTAREA.setAttribute('multiple', true)

            this.TEXTAREA_PARENT = document.createElement('div')
            addClass(this.TEXTAREA_PARENT, 'handsontableInputHolder')

            if (hasClass(this.TEXTAREA_PARENT, this.layerClass)) {
                removeClass(this.TEXTAREA_PARENT, this.layerClass)
            }

            addClass(this.TEXTAREA_PARENT, EDITOR_HIDDEN_CLASS_NAME)

            this.textareaStyle = this.TEXTAREA.style
            this.textareaStyle.width = 0
            this.textareaStyle.height = 0
            this.textareaStyle.overflowY = 'visible'

            this.textareaParentStyle = this.TEXTAREA_PARENT.style

            this.TEXTAREA_PARENT.appendChild(this.TEXTAREA)
            this.instance.rootElement.appendChild(this.TEXTAREA_PARENT)
        }

        MultiSelectEditor.prototype.open = function () {
        this.refreshDimensions()

        this.instance.addHook('beforeKeyDown', (event) => this.onBeforeKeyDown(event))

        if (this.choices) this.choices.destroy()

        const choicesOptions = {
            classNames: classNamesOverride,
            delimiter: this.separator,
            removeItemButton: true,
            position: 'bottom',
            itemSelectText: '',
            maxItemCount: this.maxItemCount,
        }

        if (this.type === 'numeric') {
            Object.assign(choicesOptions, {
                sorter: numberComparator,
            })
        }
        this.choices = new Choices(this.TEXTAREA, choicesOptions)

        const getOptions = () => {
            const { options } = this.selectOptions
            const toResolve = typeof options === 'function'
                ? options()
                : options
            return Promise.resolve(toResolve)
                .then(((availableOptions) => {
                    const { originalValue } = this
                    if (R.isEmpty(originalValue) || R.isNil(originalValue)) {
                        return availableOptions
                    }

                    const selectedValues = originalValue.split(this.separator)
                    return R.map((item) => {
                        const label = `${R.prop(this.labelKey, item)}`

                        return R.any(R.identical(label), selectedValues)
                            ? R.assoc('selected', true, item)
                            : item
                    }, availableOptions)
                }))
        }

        this.choices.setChoices(getOptions, this.valueKey, this.labelKey, true)
        this.choices.passedElement.element.addEventListener('change', this.onChoicesChange.bind(this))

        this.choices.showDropdown()

        this.TEXTAREA.addEventListener('hideDropdown', this.close.bind(this))

        this.TEXTAREA_PARENT.querySelector('input').addEventListener('keydown', this.onBeforeKeyDownOnInput.bind(this))
    }

        MultiSelectEditor.prototype.close = function () {
            this.choices.hideDropdown()
            this.autoResize.unObserve()
            this.hideEditableElement()
            this.clearHooks()
        }

        MultiSelectEditor.prototype.focus = function () {
            this.instance.listen()
        }

        MultiSelectEditor.prototype.showEditableElement = function () {
            this.textareaParentStyle.height = ''
            this.textareaParentStyle.overflow = ''
            this.textareaParentStyle.position = ''
            this.textareaParentStyle.right = 'auto'
            this.textareaParentStyle.opacity = '1'
            this.textareaParentStyle.width = `${this.getEditedCell().getBoundingClientRect().width}px`

            this.textareaStyle.textIndent = ''
            this.textareaStyle.overflowY = 'hidden'

            const childNodes = this.TEXTAREA_PARENT.childNodes
            let hasClassHandsontableEditor = false

            rangeEach(childNodes.length - 1, (index) => {
                const childNode = childNodes[index]

                if (hasClass(childNode, 'handsontableEditor')) {
                    hasClassHandsontableEditor = true

                    return false
                }
            })

            if (hasClass(this.TEXTAREA_PARENT, EDITOR_HIDDEN_CLASS_NAME)) {
                removeClass(this.TEXTAREA_PARENT, EDITOR_HIDDEN_CLASS_NAME)
            }

            if (hasClassHandsontableEditor) {
                this.layerClass = EDITOR_VISIBLE_CLASS_NAME

                addClass(this.TEXTAREA_PARENT, this.layerClass)

            } else {
                this.layerClass = this.getEditedCellsLayerClass()

                addClass(this.TEXTAREA_PARENT, this.layerClass)
            }
        }

        MultiSelectEditor.prototype.onChoicesChange = function (event) {
            const { choices, maxItemCount } = this
            const selected = choices.getValue()

            if (maxItemCount !== -1 && selected.length >= maxItemCount) {
                this.close()
                this.finishEditing()
            }
        }

        MultiSelectEditor.prototype.onBeforeKeyDownOnInput = function (event) {
            switch (event.keyCode) {
                case KEY_CODES.ARROW_UP:
                case KEY_CODES.ARROW_DOWN:
                    event.preventDefault()
                    event.stopPropagation()
                    break

                default:
                    break
            }
        }

        MultiSelectEditor.prototype.onBeforeKeyDown = function (event) {
            const keyCodes = Handsontable.helper.KEY_CODES
            const ctrlDown = (event.ctrlKey || event.metaKey) && !event.altKey // catch CTRL but not right ALT (which in some systems triggers ALT+CTRL)

            // Process only events that have been fired in the editor
            if (event.target.tagName !== 'INPUT') {
                return
            }
            if (event.keyCode === 17 || event.keyCode === 224 || event.keyCode === 91 || event.keyCode === 93) {
                // when CTRL or its equivalent is pressed and cell is edited, don't prepare selectable text in textarea
                event.stopImmediatePropagation()
                return
            }

            const { target } = event

            switch (event.keyCode) {
                case keyCodes.ARROW_RIGHT:
                    if (Handsontable.dom.getCaretPosition(target) !== target.value.length) {
                        event.stopImmediatePropagation()
                    }
                    break

                case keyCodes.ARROW_LEFT:
                    if (Handsontable.dom.getCaretPosition(target) !== 0) {
                        event.stopImmediatePropagation()
                    }
                    break

                case keyCodes.ENTER:
                    event.stopImmediatePropagation()
                    event.preventDefault()
                    event.stopPropagation()
                    break

                case keyCodes.A:
                case keyCodes.X:
                case keyCodes.C:
                case keyCodes.V:
                    if (ctrlDown) {
                        event.stopImmediatePropagation() // CTRL+A, CTRL+C, CTRL+V, CTRL+X should only work locally when cell is edited (not in table context)
                    }
                    break

                case keyCodes.BACKSPACE:
                    event.stopImmediatePropagation()
                    break
                case keyCodes.DELETE:
                case keyCodes.HOME:
                case keyCodes.END:
                    event.stopImmediatePropagation() // backspace, delete, home, end should only work locally when cell is edited (not in table context)
                    break

                default:
                    break
            }
        }

        MultiSelectEditor.prototype.getValue = function () {
            const valueArray = this.choices.getValue()
            const formattedValues = R.pluck('label', valueArray)
            return formattedValues.join(this.separator)
        }

        Handsontable.editors.MultiSelectEditor = MultiSelectEditor;
        Handsontable.editors.registerEditor('mseditor', MultiSelectEditor);
    })(Handsontable);
    //})
    $(function(){
        var fullScreen = false;
        $(".list-aap-container").removeClass("container").addClass("container-fluid");
        var doubleScroll = function(){
            var containerWidth= $(".ht_master .wtHolder" ).width();
            var insideWidth = $(".handsontable .wtHider" ).width();
            $("#aapHotTable1").css("width",containerWidth);
            $("#aapHotTable1 div").css("width",insideWidth);

            $("#aapHotTable1").scroll(function(){
                $(".ht_master .wtHolder").scrollLeft($("#aapHotTable1").scrollLeft());
                $('.ht_master .wtHolder').css('height',(screen.height-200) );
            });
            $(".ht_master .wtHolder").scroll(function(){
                $("#aapHotTable1").scrollLeft($(".ht_master .wtHolder").scrollLeft());
                $('.ht_master .wtHolder').css('height',(screen.height-200) );
            });
        }
        doubleScroll();

        $('#toogle-table-view').off().on('click',function(){
            if(fullScreen===false){
                fullScreen=true;
                $('#toogle-table-view').text("Quitter plein ecran")
                $('body').css("overflow","hidden")
                $('#aapHotTable').parent().addClass('aapHotTableFullScreen');
                $('.ht_master .wtHolder').css('height',(screen.height-200) );
                setTimeout(() => {
                    doubleScroll();
                }, 300);
            }else{
                fullScreen=false;
                $('#toogle-table-view').text("Plein ecran")
                $('body').css("overflow","auto");
                $('#aapHotTable').parent().removeClass('aapHotTableFullScreen');
                $('.ht_master .wtHolder').css('height',"auto" );
                setTimeout(() => {
                    doubleScroll();
                }, 700);
            }
        })
    })

    $(document).ready(function () {
        $(document).on('click', '.sppec .dropdown-menu', function (e) {
            e.stopPropagation();
        });


        $( ".dropdown-menu.sortableselect" ).sortable({
            forcePlaceholderSize: true,
            //animation: 350,
            handle: ".portlet-header",
            update : function(event,ui){
                var valcol = aapColumnsId.indexOf($(this).data("input"));

                var intaapColumns = [];
                var intaapColHeaders = [];

                $(".checkbox1").each(function( index ) {
                    if ($(this).is(':checked')) {
                        var valcol = aapColumnsId.indexOf($(this).data("input"));
                        intaapColumns.push(aapColumns[valcol]);
                        intaapColHeaders.push(aapColHeaders[valcol]);
                    }
                });

                hot.updateSettings({
                    columns: intaapColumns,
                    colHeaders: intaapColHeaders
                });
            }
        });

        $(".checkbox1").on('change', function () {

            /*console.log($(this).data("input") , $(this).is(':checked').toString());
            console.log(aapColumns, aapColHeaders , aapColHeaders);
            console.log(aapColumnsId.indexOf($(this).data("input")));*/

            var valcol = aapColumnsId.indexOf($(this).data("input"));

            var intaapColumns = [];
            var intaapColHeaders = [];

            $(".checkbox1").each(function( index ) {
                if ($(this).is(':checked')) {
                    var valcol = aapColumnsId.indexOf($(this).data("input"));
                    intaapColumns.push(aapColumns[valcol]);
                    intaapColHeaders.push(aapColHeaders[valcol]);
                }
            });

            hot.updateSettings({
                columns: intaapColumns,
                colHeaders: intaapColHeaders
            });
            
        });
    });

</script>
