<?php
$answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$elform["_id"], "answers.aapStep1.titre" => ['$exists' => true]));
?>

<style>
    .w-95 {
        width: 95%;
    }
    .justify-content-center {
        justify-content: center;
    }
</style>

<div class="container w-95">
    <div class="col-xs-12">
        <div class="headerSearchleft col-xs-12 no-padding">
            <h4 class="pull-left countResults">
                <i class="fa fa-angle-down"></i> <?php echo count($answers) ?>  dossiers 
                <span class="letter-green">| Fin de la session : <?php echo $elform['endDate'] ?></span>
            </h4>
        </div>
    </div>
    <div class="col-xs-12 bodySearchContainer margin-top-10 no-padding">
<?php
foreach ($answers as $key => $value) {
    $user = PHDB::findOneById(Person::COLLECTION, $value['links']['answered'][0])
?>
    <div class="row list-group-item " style="margin-bottom:20px" data-label="nouv." id="list-62699a6931a1f263df1cfc23" data-id="62699a6931a1f263df1cfc23">
        <div class="">
            <div class="media col-md-4 col-lg-4 ">
                <div class="contain-img-left">
                    <figure class="">
                        <a href="javascript:;" data-url="slug.mieuxVoter.formid.6266d2f9f4861320bd12fa85.aappage.sheet.answer.62699a6931a1f263df1cfc23" class="aapgetaapview aaptitlegoto d-flex justify-content-center" data-id="62699a6931a1f263df1cfc23">
                            <img src="<?php if(isset($value['profilImageUrl'])) echo $value['profilImageUrl']; else echo '/assets/f04f76ee/images/thumbnail-default.jpg' ?>" alt="projet" class="img-responsive " data-url="/costum/co/index/slug/mieuxVoter#oceco.slug.mieuxVoter.formid.6266d2f9f4861320bd12fa85.aappage.form.answer.62699a6931a1f263df1cfc23" style="width: 80%;">
                        </a>
                    </figure>
                </div>
            </div>
            <div class="col-md-8 col-lg-8 col-xs-12 padding-top-5">
                <!-- <h4 class="list-group-item-heading">Blog Title</h4> -->
                <h4 class="list-group-item-heading">
                    <a href="javascript:;" data-url="slug.mieuxVoter.formid.6266d2f9f4861320bd12fa85.aappage.sheet.answer.62699a6931a1f263df1cfc23" class="aapgetaapview aaptitlegoto minimalist-name" data-id="62699a6931a1f263df1cfc23" style="display:inline"> <?php echo $value['answers']['aapStep1']['titre'] ?> </a>
                </h4>
                <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?php if($user) echo $user['name'] ?></i></b>&nbsp;&nbsp;&nbsp;&nbsp; Le <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y', $value['updated']) ?> </i></b>&nbsp; à &nbsp;<i class="fa fa-clock-o" style="color: #7da53d"></i> <b><i><?php echo date('h:i:s', $value['updated']) ?> </i></b> </small><br>
                <!-- <small class="end-project-date"><i class="fa fa-calendar" style="color: #7da53d"></i> Date fin : <b><i></i></b></small> <br> -->
                <hr style="border-top: 2px dashed #3f4e58;margin-bottom: 10px;">
                <!--?/*=$statusAction
                                                */?-->

                <!--<span class='label <?/*=$urgentState == "Urgent" ? "label-danger" : "hidden" */ ?> '><?/*=$urgentState */ ?></span>-->
                <small>
                    <i><b>
                        </b></i>
                </small>

                <p class="list-group-item-text project-description list-item-description list-markdown">
                <p><?php echo $value['answers']['aapStep1']['description'] ?></p>
                </p>
            </div>

            
        </div>
    </div>
<?php
}
?>
    </div>
</div>
<script type="text/javascript">
    let allanswers = <?php echo json_encode($answers); ?>;
    let user = <?php echo json_encode($elform); ?>;
    mylog.log('all answer ', allanswers);
    mylog.log('user', user);
</script>