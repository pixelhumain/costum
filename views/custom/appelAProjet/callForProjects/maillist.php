
<?php
use Aap;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Api;
$layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';

if(@Yii::app()->session["userId"])
	echo $this->renderPartial($layoutPath.'.rocketchat');


$cssAnsScriptFilesModule = array(
	'/plugins/jquery-simplePagination/jquery.simplePagination.js',
	'/plugins/jquery-simplePagination/simplePagination.css',
	'/plugins/select2/select2.min.js' ,
	'/plugins/select2/select2.css',
	'/plugins/underscore-master/underscore.js',
	'/plugins/jquery-mentions-input-master/jquery.mentionsInput.js',
	'/plugins/jquery-mentions-input-master/jquery.mentionsInput.css',
	'/plugins/jquery.dynForm.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));

$cssAnsScriptFilesModule = array( 
	'/js/eligible.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "survey" )->getAssetsUrl() );


$cssJS = array(
	'/js/dataHelpers.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl() );

$cssAnsScriptFilesModule = array(
	'/assets/js/comments.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);
$states = array();
?>
<?php 
	$results = PHDB::find('cron',array("tplParams.formId"=>$params));
	$answerIds = [];
	foreach($results as $kr => $vr){
		if(!empty($vr["tplParams"]["answerId"]) && !in_array($vr["tplParams"]["answerId"],$answerIds)){
			$answerIds[] = new MongoId($vr["tplParams"]["answerId"]);
		}elseif(!empty($vr["tplParams"]["html"])){
			$id = Aap::get_string_between($vr["tplParams"]["html"],"/attachedfile/answerid/","/template/");
			if(Api::isValidMongoId($id)){
				$answerIds[] = new MongoId($id);
				$results[$kr]["tplParams"]["answerId"] = $id;
				PHDB::update(Cron::COLLECTION,array("_id"=> new MongoId($kr)),array('$set' => ["tplParams.answerId" => $id]));
			}
		}
	}
	$answers = PHDB::find(Form::ANSWER_COLLECTION,array("_id" => ['$in' => $answerIds]),array("answers.aapStep1.titre" => 1));
	/*$inputs = PHDB::findOne(Form::INPUTS_COLLECTION,array("formParent"=>$el_form_id,"step"=>"aapStep1"));
	$emailFields = [];
	if(!empty($inputs['inputs'])){
		foreach($inputs['inputs'] as $kInp => $vInp){
			if(!empty($vInp["label"])){
				$vInp["label"] = strtolower($vInp["label"]);
				if(strpos($vInp["label"], "mail") !== false || strpos($vInp["label"], "mail") !== false){
					$emailFields[] = $vInp;
				}
			}
		}
	}
	var_dump($emailFields);*/
?>
<style type="text/css">
	.clickOpen{
		cursor: pointer;
		vertical-align: middle;
	}
	.round{
		border-radius: 100%;
		width: 250px;
		height: 250px;
		padding-top: 70px;
		border-color: #333;
	}
	.directoryLines td{
		vertical-align: middle;
	}
</style>
<div class="panel panel-white col-lg-offset-1 col-lg-10 col-xs-12 no-padding">
	
	<div class="col-md-12 col-sm-12 col-xs-12 ">
		<h1 class="text-center">Liste des emails </h1>
		<br/>
		<h5 class="">
			Filtres : 
			<a href="javascript:;" onclick="showType('line')" class="btn btn-xs btn-default">Tous</a>
			<a href="javascript:;" onclick="showType('pending')" class="btn btn-xs btn-default">Pending</a> 
			<a href="javascript:;" onclick="showType('update')" class="btn btn-xs btn-default">Update</a>
			<!-- <a href="javascript:;" onclick="showType('year')" class="btn btn-xs btn-default">Année</a> -->
		</h5>	

		<div style="width:80%;  display: -webkit-inline-box;">
			<input type="text" class="form-control" id="search" placeholder="Rechercher une information dans le tableau">
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20 text-center"></div>
</div>
<div class="panel panel-white col-lg-12 col-xs-12 no-padding">
	<div class="panel-body">
		<div>
			<!-- <a href="<?php //echo '#element.invite.type.'.Form::COLLECTION.'.id.'.(string)$form['_id'] ; ?>" class="btn btn-primary btn-xs pull-right margin-10 lbhp">Invite Admins & Participants</a> -->
			

			<span><b>Il y a <span id="nbLine"><?php echo count(@$results); ?></span> réponses</b></span>
			<br/><br/>
        
			<table class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin" style="table-layout: fixed; width:100%; word-wrap:break-word;">
				<thead>
					<tr>
						<th class="" style="width: 50px;">#</th>
						<th class="">Dossiers</th>
						<th class="">To</th>
						<th class="">Status</th>
						<th class="">Template</th>
						<th class="">Subject</th>
					</tr>
				</thead>
				<tbody class="directoryLines">
					<?php
						$nb = 0;
						foreach ($results as $k => $v) {
							$nb++;
						?>
						<tr data-id="<?php echo $k ?>" class="<?php echo @$v["status"]." ".@$v["tpl"]." " ?> line">
							<td class=" clickOpen " style="vertical-align: middle;"><?php echo @$nb ?></td>
							<td class=" clickOpen" style="vertical-align: middle;">
								<?= !empty($v["tplParams"]["answerId"]) && !empty($answers[$v["tplParams"]["answerId"]]["answers"]["aapStep1"]["titre"]) ? $answers[$v["tplParams"]["answerId"]]["answers"]["aapStep1"]["titre"] : "" ?>
							</td>
							<td class=" clickOpen" style="vertical-align: middle;"><?php echo @$v["to"] ?></td>
							<td class=" clickOpen" style="vertical-align: middle;"><?php echo @$v["status"] ?></td>
							<td class=" clickOpen" style="vertical-align: middle;"><?php echo @$v["tpl"] ?></td>
							<td class=" clickOpen" style="vertical-align: middle;"><?php echo @$v['subject'] ?></td>
							
						<?php
					} ?>
				</tbody>
			</table>
			
		</div>
	</div>
	<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>
</div>

<script type="text/javascript">

var results  = <?php echo json_encode($results); ?>;

function showType (type) { 
	$(".line").hide();
	$("."+type).show();
	countLine();
}

jQuery(document).ready(function() {
	
	$("#search").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$("#panelAdmin tr.line").filter( function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
			countLine();
		});
	});

	$(".clickOpen").off().on('click',function(){
		window.open("<?php echo Yii::app()->getRequest()->getBaseUrl(true)."/co2/test/displayMail/id/" ; ?>"+$(this).parent().data('id')) ;
	});
});

function countLine(){
	$("#nbLine").html($('#panelAdmin tr.line:visible').length);
}
</script> 

