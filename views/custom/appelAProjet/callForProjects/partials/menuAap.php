<?php if (!$modalrender && empty($formStandalone)) { ?>
    <style>
        .navbar-brand{
            padding: 15px 15px !important;
        }
        .navbar-default {
            background-color: #f8f8f8;
            border-color: #e7e7e700 !important;
        }
        .navbar-collapse li {
            font-size: 14px !important;
        }
    </style>
    <div class="container no-padding aap-menu">
        <div class="navbar navbar-default no-margin">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-aap">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar-aap" class="collapse navbar-collapse">
                <ul class="nav navbar-nav" style="width:100%">
                    <li class="<?php echo ((!empty($page) && $page == "details") ? "active" : ""); ?>"><a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details "><?php echo $paramsData["titleDetails"] ?></a></li>
                    <li class="<?php echo ((!empty($page) && $page == "list") ? "active" : ""); ?>">
                        <a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list?view=detailed&inproject=false" data-action="detaaap"> <?php echo $paramsData["titleList"] ?>
                            <span class="tooltips" data-toggle="tooltip" data-placement="right" data-original-title="Nouvelle(s) proposition(s)">
                                <span class="btn label label-success tooltips new-prop-counter" style="border-radius: 10px;display:none" data-toggle="tooltip" data-placement="right" data-original-title="">

                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="<?php echo ((!empty($page) && $page == "list") ? "active" : ""); ?>">
                        <a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list?view=detailed&inproject=true" data-action="">
                            En projets
                        </a>
                    </li>
                    <?php if (!empty(Yii::app()->session["userId"])) { ?>
                        <li class="<?php echo ((!empty($page) && $page == "list") ? "active" : ""); ?>">
                            <a href="javascript:;" id="kanban-personal-action" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list?view=project_kanban&user=<?= Yii::app()->session["userId"] ?>" data-action="kanban-personal-action">
                                <?php echo $paramsData["titleMyActions"] ?>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="<?php echo ((!empty($page) && $page == "agenda") ? "active" : ""); ?>"><a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.agenda ">Agenda</a></li>

                    <li class="<?php echo ((!empty($page) && $page == "dashboard") ? "active" : ""); ?>"><a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.dashboard"><?php echo $paramsData["titleObservatory"] ?></a></li>

                    <?php if (isset($elform["coremu"]) && $elform["coremu"]){ ?>
                        <li class="<?php echo ((!empty($page) && $page == "compact") ? "active" : ""); ?>"><a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.compact">Liste compact</a></li>
                    <?php } ?>

                    <li class="hidden <?php echo ((!empty($page) && $page == "dashboard") ? "active" : ""); ?>"><a href="javascript:;" class="btnaapp_link lbh-default" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.kanban_personal_action"></a></li>

                    <?php if (isset(Yii::app()->session['userId'])  && $elform["open"] == true) { ?>
                        <li><a href="javascript:;" id="btn-new-proposition" class="aapgoto lbh-default <?php echo ((!empty($page) && $page == 'form') ? "active" : ""); ?>" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.form"><i class="fa fa-plus-square-o"></i>&nbsp;<?php echo $paramsData["titleBtnNewProposition"] ?></a>
                        <li>
                        <?php } ?>

                        <li class="pull-right">
                            <div class="dropdown options">
                                <a href="javascript:;" class="dropdown-toggle aap-breadrumbbtn-2 options-admin margin-left-5 lbh-default" data-toggle="dropdown">
                                    <i class="fa fa-list"></i>
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu options-admin-dropdown" style="z-index:10000">
                                    <li style="display: block;">
                                        <a href="javascript:;" class="tooltips margin-top-10 aapgoto" data-communecter="true" target="_blank" data-url="/#@<?php echo $el_slug; ?>">
                                            <i class="fa fa-home" aria-hidden="true"></i> <?= Yii::t("common","Go to") ?> <?= htmlspecialchars(json_encode(@$el["el"]["name"]), ENT_QUOTES, 'UTF-8') ?>
                                        </a>
                                    </li>
                                    <?php if($isAdmin){ ?>
                                        <li>
                                            <a href="javascript:;" class="tooltips btnConfigForm" data-form='<?= htmlspecialchars(json_encode($elform), ENT_QUOTES, 'UTF-8') ?>' data-toggle="tooltip" data-placement="left" title="" data-original-title="<?= Yii::t("common","Configure the questionnaire") ?>">
                                                <i class="fa fa-cog" aria-hidden="true"></i> <?= Yii::t("common","Configure the questionnaire") ?>
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.form.editform.true">
                                                <i class="fa fa-edit" aria-hidden="true"></i> <?= Yii::t("common","Edit the questionnaire") ?>
                                            </a>
                                        </li>

                                        <li style="display: block;">
                                            <a href="javascript:;" class=" aapcommunity tooltips margin-top-10" data-url="">
                                                <i class="fa fa-users " aria-hidden="true"></i> <?= Yii::t("common","Manage the community") ?>
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class=" aapnotification tooltips margin-top-10" data-url="">
                                                <i class="fa fa-bell " aria-hidden="true"></i> <?= Yii::t("common","Manage notifications") ?>
                                            </a>
                                        </li>
                                        <?php if (!empty(Yii::app()->session["user"]["username"]) && in_array(Yii::app()->session["user"]["username"], ["Ramiandrison", "Anatole-Rakotoson", "oceatoon", 'Rinelfi'])) { ?>
                                            <li class="btn-breadcrumb" style="display: block;">
                                                <a href="javascript:;" class="btn-upload-csv margin-top-10"> <i class="fa fa-file"></i> Importer csv</a>
                                            </li>
                                        <?php } ?>
                                        <?php if (!empty($el["type"]) && !empty($el["id"])) { ?>
                                            <li class="inviter-membres" style="display: block;">
                                                <a href="#element.invite.type.<?= $el["type"] ?>.id.<?= $el["id"] ?>" class=" lbhp tooltips margin-top-10" data-url="">
                                                    <i class="fa fa-user-plus" aria-hidden="true"></i> <?= Yii::t("common","Invite members") ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <li style="display: block;">
                                        <a href="javascript:;" class="tooltips margin-top-10 copylinkstandalone">
                                            <i class="fa fa-link" aria-hidden="true"></i> <?= Yii::t("common","Standalone link") ?>
                                        </a>
                                    </li>
                                    <?php if($isAdmin){ ?>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 edit<?php echo $kunik ?>Params">
                                                <i class="fa fa-cog" aria-hidden="true"></i> <?= Yii::t("common","Change menu label") ?>
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 email-table-stat">
                                                <i class="fa fa-envelope" aria-hidden="true"></i> <?= Yii::t("common","Email lists") ?>
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 communication-tools">
                                                <i class="fa fa-copy" aria-hidden="true"></i> <?= Yii::t("common","Communication tools") ?>
                                            </a>
                                        </li>
                                        <?php if($el_slug == "coSindniSmarterre"){ ?>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 duplicate-prop-tools">
                                                <i class="fa fa-copy" aria-hidden="true"></i> <?= Yii::t("common","Dupliquer dossiers") ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10" data-toggle="modal" data-target="#csv-exportation">
                                                <i class="fa fa-file" aria-hidden="true"></i> <?= Yii::t("common","Export as") ?> CSV
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 btn-download-csv-financer">
                                                <i class="fa fa-file" aria-hidden="true"></i> <?= Yii::t("common","Export as") ?> CSV les financements
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 btn-download-csv-aap">
                                                <i class="fa fa-file" aria-hidden="true"></i> Export Appel à projet
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 config-budget">
                                                <i class="fa fa-money" aria-hidden="true"></i> <?= Yii::t("common","Configure the budget")." ". @$el["el"]["name"] ?>
                                            </a>
                                        </li>
                                        <li style="display: block;">
                                            <a href="javascript:;" class="tooltips margin-top-10 config-participatory-budget">
                                                <i class="fa fa-users" aria-hidden="true"></i> <?= Yii::t("common","Configure heart voting") ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <li>
                </ul>
            </div>
        </div>
    </div>

    <div class="aapformmodal">
        <label class="aapformmodal__bg" for="modal-1"></label>
        <div class="aapformmodal__inner">
            <label class="aapformmodal__close" for="modal-1"></label>

            <div id="aapformdynamic" class=""></div>
        </div>
    </div>
<?php } ?>
