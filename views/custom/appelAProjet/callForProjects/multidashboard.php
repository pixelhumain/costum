<?php
$cssJS = array(
    '/plugins/gridstack/css/gridstack.min.css',
    '/plugins/gridstack/js/gridstack.js',
    '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
?>
<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }
</style>

<div id="sticky-anchor"></div>
<div id="sticky">
    <div class="col-md-offset-1 col-md-10">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
            <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details ">Observatoire globale</a></li>
        </ul>
    </div>

</div>

<div class="" id="aapformcont"></div>


<script type="text/javascript">
    $(document).ready(function() {
        $(function () {
            $('.grid-stack').gridstack({
                cellHeight: 100,
                verticalMargin: 10,
                horizontalMargin: 10,
                disableResize: true,
                removable: true
            });
        });
    });

    var formparentid = "<?php echo (isset($elform['_id']) ? (string)$elform['_id'] : ''); ?>";

    $(document).ready(function() {
        ajaxPost("#aapformcont", baseUrl+'/survey/answer/multidashboard/formparentid/'+formparentid,
            null,
            function(data){

            },"html");
    });

</script>