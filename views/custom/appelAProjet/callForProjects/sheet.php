<style>
    .moreLinkaap.btn-xs.btn.btn-default {
        width: 80px;
    }
    .portfolio-modal .modal-content {
        text-align: left;
    }
    .video-app{
        display: flex;
        flex-wrap: wrap;
    }
</style>
<?php
function random_color_part()
{
    return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
}

function random_color()
{
    return random_color_part() . random_color_part() . random_color_part();
}

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css", "/js/blockcms/swiper/swiper-bundle.min.js"], Yii::app()
    ->getModule('costum')
    ->assetsUrl);

if (strpos($_SERVER['REQUEST_URI'], '/page/sheet/answer/') !== false) {
    $url = explode('/', $_SERVER['REQUEST_URI']);
    $answerIndex = array_search('answer', $url);
    $answerid = $url[$answerIndex + 1];
};

$answer = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerid);
$inputs = PHDB::find(Form::INPUTS_COLLECTION,array("formParent" => $answer["form"]));
$data = Aap::prepareData(array($answerid),Form::ANSWER_COLLECTION,$el,$el_form_id,(string)$el_configform["_id"]);
$el = $data["el"];

$paramsData = ["financerTypeList" => Ctenat::$financerTypeList, "limitRoles" => ["Financeur"], "openFinancing" => true];

if (!empty($idEl)) {
    $communityLinks = Element::getCommunityByParentTypeAndId($elform["parent"]);
} else {
    $communityLinks = [];
}
$organizations = Link::groupFindByType(Organization::COLLECTION, $communityLinks, ["name", "links"]);

$orgs = [];

foreach ($organizations as $id => $or) {
    $roles = null;

    if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

    if ($paramsData["limitRoles"] && !empty($roles)) {
        foreach ($roles as $i => $r) {
            if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
        }
    }
}

if (!empty($answer)) {
    $user = Person::getSimpleUserById(@$answer["user"]);
    $ansVar = Aap::answerVariables($answer,$data,$data["stepEval"]);
    $name= $ansVar["name"];
    $description= $ansVar["description"];
    $completedFields= $ansVar["completedFields"];
    $percentageTasks= $ansVar["percentageTasks"];
    $dataPer= $ansVar["dataPer"];
    $iconTasks= $ansVar["iconTasks"];
    $haveDepense= $ansVar["haveDepense"];
    $seen= $ansVar["seen"];
    $haveProject= $ansVar["haveProject"];
    $countTask= $ansVar["countTask"];
    $imgAnsw= $ansVar["imgAnsw"];
    $retain= $ansVar["retain"];
    $actions = $ansVar["actions"];
    $urgentState = $ansVar["urgentState"];
    $cummulMean = $ansVar["cummulMean"];
    $votantCounter = $ansVar["votantCounter"]
?>
    <style>
        .star-rating .star-value {
            background: url("<?= $data["starbg"] ?>") !important;
            background-size: 20px !important;
            background-repeat-y: no-repeat !important;
        }
    </style>
    
    <div id="sticky-anchor"></div>
    <div id="sticky">
        <?php if(!$modalrender){ ?>
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li><a><?php if (isset($el["el"]["name"])) {
                            echo $el["el"]["name"];
                        } ?></a></li>
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.list"><?php if (isset($elform["name"])) {
                                                                                                                                                                                                                                                                echo $elform["name"];
                                                                                                                                                                                                                                                            } ?> </a></li>
                <!-- <li ><a><?php if (isset($elform["name"])) {
                                    echo $elform["name"];
                                } ?></a></li> -->
                <!-- <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.details ">Fiche du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?></a></li> -->
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.sheet.answer.<?php echo (string)$answer["_id"] ?>">Fiche du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
            </ul>
        </div>
        <?php } ?>                     
        <div class="col-md-offset-1 col-md-10  col-sm-12 col-xs-12 ">
            <div class="aapconfigdiv ">
                <div class="text-center">
                    <?php //if ((!empty(Yii::app()->session["userId"]) && ($answer["user"] == Yii::app()->session["userId"]) || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]) )){ ?>
                        <button type="button" class="aapgetaapview2 aap-breadrumbbtn" data-id="<?= (string)$answer["_id"]; ?>" data-elformid="<?php echo $el_form_id ?>">
                        <i class="fa fa-plus-square-o"></i> 
                        Voir la proposition en détails
                    </button>
                    <?php //} ?>
                    
                    <?php if (!isset($elform["hidden"]["btnObservatory"]) || (isset($elform["hidden"]["btnObservatory"]) && $elform["hidden"]["btnObservatory"] == false)) { ?>
                        <button type="button" class="aapgoto aap-breadrumbbtn hidden" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.aappage.formdashboard.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Observatoire </button>
                        <!-- <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#oceco.slug.<?php echo $el_slug; ?>.aappage.timeline.answer.<?php echo (string)$answer["_id"] ?>"><i class="fa fa-plus-square-o"></i> Timeline </button> -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail margin-top-20">
        <div class="single_evaluate">
            <div class="row no-margin">
                <div class="col-xs-12 col-sm-7 col-md-7 padding-15">
                    <div class="evaluate_content">
                        <div class="row">
                            <div class="col-xs-7">
                                <?php echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.partials.listItemButtons",array("answer" => $answer,"data" => $data, "ansVar"=>$ansVar)); ?>
                                <h4> <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> <br>
                                    <small>Déposé par <b><i><?= @$user["name"] ?></i></b></small>
                                </h4>
                                <span class='margin-bottom-10 label <?= $urgentState == "Urgent" ? " urgent-state" : "hidden" ?> '><i class="fa fa-exclamation-triangle"></i>&nbsp;<?= $urgentState ?></span>
                            </div>
                            <div class="col-xs-5">
                                <div class="col-xs-12">
                                    <div class="rater-sheet pull-right text-center tooltips" data-rating="<?= is_numeric($cummulMean) ? $cummulMean : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?= htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . (is_numeric($cummulMean) ? round($cummulMean, 3) : 0) . " / 5" ?>"></div><br>
                                </div>
                                <div class="col-xs-12">
                                    <?php echo Aap::heartVote($answer,$data["el"],$elform,$data["me"]) ?>
                                    <?php if(isset(Yii::app()->session['userId'])){ ?>
                                        <span class="btn-group-custom-container pull-right">
                                            <a href="javascript:;" class="btn btn-sm btn-default margin-right-5 openAnswersComment tooltips btn-custom" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire" style="font-size :12px;border:0">
                                                <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?> <i class='fa fa-commenting'></i>
                                            </a>
                                        </span>
                                    <?php } ?>
                                    <a class="like-project pull-right tooltips margin-right-10" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre de visite">
                                        <i class="fa fa-eye"></i> <span id="countv"> <?php echo (isset($answer["visitor_counter"]["count"]) ? $answer["visitor_counter"]["count"] : __) ?> </span>
                                    </a>

                                    <!-- <a href="javascript:;" class="pull-right tooltips margin-right-10" data-toggle="modal" data-target="#modal<?= $votantCounter != 0 ? (string)$answer["_id"] : null ?>" >
                                        <i class="fa fa-gavel"></i> <span id ="countv"  style="font-size: 12px"> <?= $votantCounter ?> </span>
                                    </a> -->
                                    <a href="javascript:;" class="votant-modal pull-right tooltips margin-right-10" data-subtype="<?= $elform["subType"] ?>" data-answerid="<?= $answerid ?>">
                                        <i class="fa fa-gavel"></i> <span id="countv" style="font-size: 12px"> <?= $votantCounter ?> </span>
                                    </a>

                                </div>
                            </div>

                            <div class="col-xs-12 padding-0">
                                <div class="col-btn-group-vertical padding-0">
                                    <div>
                                        <select data-sheet-id="<?= $answerid ?>" class="statustags2" multiple disabled>
                                            <option disabled></option>
                                            <?php
                                            foreach (Aap::$badgelabel as $bdgid => $bdglbl) {
                                            ?>
                                                <option <?php if (!empty($answer["status"]) && in_array($bdgid, $answer["status"])) {
                                                            echo "selected";
                                                        } ?> value="<?php echo $bdgid ?>"><?php echo $bdglbl ?></option>
                                            <?php } ?>

                                        </select>
                                        <?php

                                        if (isset(Yii::app()->session['userId']) && isset($elform["_id"]) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]))) { ?>

                                            <a href="javascript:;" class="btn st-enable-btn2" data-loc="edit" data-id="<?= $answerid ?>" type="button"><i class="fa fa-pencil"></i> Modifier status</a>

                                        <?php } ?>
                                    </div>
                                    <!-- <?php //echo Aap::listItemButtons($answer,$elform,$el["el"],$actions,$data["canAdmin"],$retain,$haveDepense,$data["canEvaluate"],$stepAction,$data["stepEval"],$data["stepFinancor"],$name) ?> -->
                                </div>
                            </div>

                        </div>
                        
                        <div id="less-sheet-<?= (string) $answer["_id"] ?>" class="list-group-item-text project-description list-item-description sheet-description list-markdown margin-left-15"><?=substr($description, 0, 400) ?><?= strlen($description) > 400 ? '<b>. . .</b> <button onclick="document.getElementById(\'less-sheet-'.(string) $answer["_id"].'\').style.display = \'none\';document.getElementById(\'more-sheet-'.(string) $answer["_id"].'\').style.display = \'table\';" class="moreLinkaap btn-xs btn btn-default">'.Yii::t("form","Read more").'</button>' : "" ?>
                        </div>
                        <?php if(strlen($description) > 400){ ?>
                            <div id="more-sheet-<?= (string) $answer["_id"] ?>" style="display:none" class="list-group-item-text project-description list-item-description list-markdown margin-left-15"><?= $description ?><?=  '<button onclick="document.getElementById(\'less-sheet-'.(string) $answer["_id"].'\').style.display = \'table\';document.getElementById(\'more-sheet-'.(string) $answer["_id"].'\').style.display = \'none\';" class="btn btn-xs lessLinkaap btn-xs btn btn-default">'.Yii::t("common","Read less").'</button>' ?>
                            </div>
                        <?php } ?>
                        <div class="small col-xs-12 padding-bottom-0 padding-top-5 collapse-container" >
                            <?php echo Aap::contributors($answer)["html"];?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 padding-0 sheet-swipper" style="overflow: hidden;">
                    <?php
                    $initAnswerFiles = Document::getListDocumentsWhere(array(
                        "id" => (string)$answer["_id"],
                        "type" => 'answers',
                        "subKey" => "aapStep1.image"
                    ), "image");
                    if (!empty($initAnswerFiles)) { ?>
                        <div class="swiper mySwiper">
                            <div class="swiper-wrapper">
                                <?php foreach ($initAnswerFiles as $key => $d) { ?>
                                    <div class="swiper-slide">
                                        <img class="img-responsive" src="<?= !empty($d['imagePath']) ? $d['imagePath'] : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"  ?>">
                                    </div>
                                <?php
                                } ?>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-pagination"></div>
                        </div>
                    <?php
                    } else { ?>
                        <img class="img-responsive" src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/thumbnail-default.jpg">
                    <?php
                    } ?>
                </div>


            </div>
        </div>
        
        <div class="single_funding <?= empty($answer["answers"]["aapStep1"]["videos"]) ? "hidden" : "" ?>">
            <div class="funding_header padding-15">
                <h4>Videos
                    <span>
                        <i class="fa fa-youtube"></i>
                    </span>
                </h4>
            </div>
            <div class="funding_body">
                <div class="video-app margin-10">
                    <h1>hjkjhkjhkj</h1>
                </div>
            </div>
        </div>
        
        <div class="single_funding">
            <div class="funding_header padding-15">
                <h4>Plus d'informations
                    <span>
                        <i class="fa fa-info"></i>
                    </span>
                </h4>
            </div>
            
            <div class="funding_body">
                <div class="margin-10 padding-10">
                    <ul class="list-group">
                    <?php 
                        $hideInfo = ["titre","description","depense","budget","year"];
                        foreach ($inputs as $key => $value) {
                            if($value["step"] == "aapStep1"){
                                foreach ($value["inputs"] as $kinp => $vinp) {
                                    if(!empty($answer["answers"]["aapStep1"][$kinp]) && !in_array($kinp,$hideInfo)){
                                        if(gettype($answer["answers"]["aapStep1"][$kinp]) == "string"){
                                            echo '<li class="list-group-item"><b>'.$vinp["label"].": </b>".$answer["answers"]["aapStep1"][$kinp].'</li>';
                                        }elseif (
                                            gettype($answer["answers"]["aapStep1"][$kinp]) == "array" && 
                                            !(array_keys($answer["answers"]["aapStep1"][$kinp]) !== range(0, count($answer["answers"]["aapStep1"][$kinp]) - 1))
                                        ) {
                                            echo '<li class="list-group-item">';
                                            echo '<b>'.$vinp["label"].'</b><br>';
                                            foreach ($answer["answers"]["aapStep1"][$kinp] as $karr => $varr) {
                                                echo '- '.$varr.'<br>';
                                            }
                                            echo '</li>';
                                        }
                                        
                                    }
                                }
                            }
                        }
                    ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="single_funding">
            <div class="funding_header padding-15">
                <h4>Documents
                    <span>
                        <i class="fa fa-file"></i>
                    </span>
                </h4>
            </div>
            
            <div class="funding_body">
                <div class="margin-10 padding-10">
                <?php 
                    $documents = Document::getListDocumentsWhere(array(
                        "id" => (string)$answer["_id"],
                        "type" => 'answers',
                        "subKey" => ['$ne'=>"aapStep1.image"]
                    ), "file");
                    foreach ($inputs as $key => $value) {
                        echo "<h6>".$el_configform["subForms"][$value['step']]["name"]."</h6>";
                        $hasDoc = false;
                        foreach($value["inputs"] as $ki => $vi){
                            if(!empty($documents) && $vi["type"] == "tpls.forms.uploader"){  
                                $i = 1;                              
                                foreach ($documents as $kdoc => $vdoc) {
                                    if(!empty($vdoc["subKey"]) && !empty($vdoc['docPath']) && $vdoc["subKey"] == $value['step'].".".$ki){
                                        $hasDoc = true;
                                        echo "<a href=".Yii::app()->getRequest()->getBaseUrl(true).$vdoc['docPath']."> ".$i."- ".$vi['label']."(".$vdoc['name'].")</a> <br>";
                                    }
                                $i++;
                                }
                            }
                        }
                        if(!$hasDoc){
                            echo "<span>".Yii::t("common","No data")."</span>";
                        }
                    }
                ?>
                </div>
            </div>
        </div>

        <!--FINANCEMENT -->
        <div class="single_funding <?= empty($answer["answers"]["aapStep1"]["depense"]) ? "hidden" : "" ?>">
            <div class="funding_header padding-15">
                <h4>Financement
                    <span>
                        <i class="fa fa-euro"></i>
                    </span>
                </h4>
            </div>
            <div class="funding_body">
                <div class="panel-body no-padding">
                    <?php
                    if (!empty($answer["answers"]["aapStep1"]["depense"])) {
                        $icont = 1;
                        foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep) {
                    ?>

                            <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding  <?php echo $icont % 2 === 0 ? "line-light" : "line-dark"; ?>">
                                <div class="col-md-5 col-sm-6 hidden-xs labelFunding padding-10">
                                    <?php echo @$dep["poste"]; ?>
                                </div>
                                <div class="col-md-7 col-sm-6 col-xs-12 valueAbout padding-10 <?php echo $icont % 2 === 0 ? "line-dark" : "line-light"; ?>">
                                    <div class="visible-xs col-xs-12 no-padding"><?php echo @$dep["poste"] ?></div>
                                    <div class="col-xs-12 col-sm-12 col-md-5 text-center">
                                        <div class="media justify-center">
                                            <span class="text-simple bold">
                                                <?php
                                                $totalname = "";
                                                if (!empty($dep["financer"])) {
                                                    foreach ($dep["financer"] as $fiid => $fi) {
                                                        if (isset($fi["name"])) {
                                                            $totalname .= " " . $fi["name"] . " -";
                                                        }
                                                    }
                                                }
                                                echo $totalname;
                                                ?>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-7 no-padding">
                                        <div class="widget widget-black shadow">

                                            <div>
                                                <span class="pull-right text-dark">
                                                    <h3 class="widget-title"><?php if (!empty($dep["price"]) && is_numeric($dep["price"])) {
                                                                                    echo rtrim(rtrim(number_format($dep["price"], 3, ".", " "), '0'), '.');
                                                                                } ?> <i class="fa fa-euro"></i></h3>
                                                </span>
                                                <?php
                                                if (isset($dep["price"]) && is_numeric($dep["price"])) {
                                                    $totalfi = 0;
                                                    $totalde = 0;
                                                    if (!empty($dep["financer"])) {
                                                        foreach ($dep["financer"] as $fiid => $fi) {
                                                            if (isset($fi["amount"])) {
                                                                $totalfi += (int)$fi["amount"];
                                                            }
                                                        }
                                                    }

                                                    if (!empty($dep["payement"])) {
                                                        foreach ($dep["payement"] as $deid => $de) {
                                                            if (isset($de["amount"])) {
                                                                $totalde += (int)$de["amount"];
                                                            }
                                                        }
                                                    }

                                                    $percent = round(($totalfi * 100) / (int)$dep["price"]);
                                                    $depensed = round(($totalde * 100) / (int)$dep["price"]);
                                                } else {
                                                    $totalfi = 0;
                                                    $percent = 0;
                                                    $depensed = 0;
                                                }

                                                ?>

                                                <span class="pull-left text-dark">
                                                    <h3 class="widget-title"><?php if (!empty($dep["price"]) && is_numeric($totalfi)) {
                                                                                    echo rtrim(rtrim(number_format($totalfi, 3, ".", " "), '0'), '.');
                                                                                } ?><i class="fa fa-euro"></i>

                                                        (<?php if (!empty($dep["price"]) && is_numeric($totalde)) {
                                                                echo rtrim(rtrim(number_format($totalde, 3, ".", " "), '0'), '.');
                                                            } ?><i class="fa fa-euro"></i> dépensé)</h3>

                                                </span>

                                            </div>
                                            <div style="clear: both;"> </div>
                                            <div class="" style="max-height: 72px;">
                                                <?php
                                                if (!empty(Yii::app()->session['userId']) && ($answer["user"] == Yii::app()->session['userId'] || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]))) {
                                                    echo '<button style="background-color: #847e7e;" type="button" id="btnFinancer' . ($icont - 1) . '" data-id="' . $answer["_id"] . '" data-budgetpath="depense" data-form="aapStep1" data-pos="' . ($icont - 1) . '" data-name="' . @$dep["poste"] . '" class="btn btn-secondary btn-xs rebtnFinancer">Ajouter</button>';
                                                }
                                                ?>
                                                <div class="progress">
                                                    <?php
                                                    if ($depensed < $percent) {
                                                    ?>
                                                        <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($depensed > 100 ? 100 : $depensed) ?>%">
                                                            <small>Financé dépensé<?php echo $depensed ?> %</small>
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (($percent - $depensed) > 100 ? 100 : ($percent - $depensed)) ?>%">
                                                            <small>Financé non dépensé<?php echo $percent - $depensed ?> %</small>
                                                        </div>

                                                    <?php
                                                    } elseif ($depensed = $percent) {
                                                    ?>
                                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                            <small>Financé et depensé <?php echo $percent ?> %</small>
                                                        </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                            <small>Dépense Financé<?php echo $percent ?> %</small>
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (($depensed - $percent) > 100 ? 100 : $percent) ?>%">
                                                            <small>Dépense non financé <?php echo $depensed - $percent ?> %</small>
                                                        </div>

                                                    <?php
                                                    }
                                                    ?>

                                                    <div class="progress-bar progress-bar-warning" role="progressbar" style="width:<?php echo abs(100 - $percent) ?>%">
                                                        <small>Non financé <?php echo 100 - ($percent) ?> %</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="widget-controls hidden">
                                                <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                    <?php $icont++;
                        }
                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php
}
?>

<script>
    var statusicon = {
        progress: "hourglass",
        finished: "check-circle",
        abandoned: "times-circle",
        newaction: "plus-circle",
        call: "tag",
        projectstate: "rocket",
        finance: "euro",
        vote: "gavel",
        underconstruction: "flag",
        prevalided: "archive",
        validproject: "check",
        voted: "suitcase"
    };

    $(function() {
        var videosAap = <?php echo !empty($answer["answers"]["aapStep1"]["videos"]) ? json_encode($answer["answers"]["aapStep1"]["videos"]) : json_encode([]) ?>;
        if(notEmpty(videosAap) && Array.isArray(videosAap)){
            var counter = Object.keys(videosAap).length;
            var html = "";
            $.each(videosAap,function(k,v){
                var videoId = "";
                var src = "";
                

                if (v.url.indexOf("=") != -1) {
                    videoId = (((v.url).split("=")).reverse())[0];
                } else {
                    videoId = (((v.url).split("/")).reverse())[0];
                }

                if (v.url.indexOf("vimeo") != -1) {
                    src = "https://player.vimeo.com/video/"+videoId
                }else if (v.url.indexOf("youtu") != -1) {
                    src = "https://www.youtube.com/embed/"+videoId
                }else if (v.url.indexOf("dailymotion") != -1) {
                    src = "https://www.dailymotion.com/embed/video/"+videoId
                }else if (v.url.indexOf("peertube.communecter") != -1) {
                    src = "https://peertube.communecter.org/videos/embed/"+videoId
                }
                html+=`
                    <div class="frame-container-">
                        <p class="title padding-left-0 margin-bottom-0">${v?.title}`;

                html+=  `</p>
                        <iframe width="300" height="" src="${src}" allowfullscreen="allowfullscreen">
                        </iframe>
                    </div>
                `
            })
            $(".video-app").html(html);
        }

        var answerid = "<?php echo (!empty($answerid) ? $answerid : $answerid); ?>";
        aapObject.sections.listItem.currentAnswer = answerid;
        var formatState = function(state) {
            if (!state.id) {
                return state.text;
            }
            var colori = "statusicon";
            if (state.id && state.id == "abandoned")
                colori = "statusicondanger";
            var $state = jQuery(
                '<span><i class="' + colori + ' fa fa-' + statusicon[state.id] + ' +"></i> ' + state.text + '</span>'
            );

            return $state;
        };
        $.getScript("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js", function(data, textStatus, jqxhr) {
            $("<link/>", {
                rel: "stylesheet",
                type: "text/css",
                href: "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
            }).appendTo("head");
            let optionSelect2 = {
                formatSelection: formatState,
                closeOnSelect: false,
                //width: '80%',
                placeholder: "aucun statut",
                containerCssClass: 'aapstatus-container',
                dropdownCssClass: "aapstatus-dropdown"
            };
            let $select2 = $("select.statustags2").select2(optionSelect2);
        });

        $('p').each(function() {
            var $this = $(this);
            if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                $this.remove();
        });

        $('.rater-sheet').each(function(i, obj) {
            var starRatingAdmin = raterJs({
                rating: (typeof $(obj).data("rating") == "number" ? $(obj).data("rating") : 0),
                starSize: 20,
                readOnly: true,
                showToolTip: false,
                element: obj,
            });
        });

        $('.rebtnFinancer').off().click(function() {
            tplCtx.pos = $(this).data("pos");
            tplCtx.budgetpath = $(this).data("budgetpath");
            tplCtx.collection = "answers";
            tplCtx.id = $(this).data("id");
            tplCtx.form = $(this).data("form");
            prioModal = bootbox.dialog({
                message: $(".new-form-financer").html(),
                title: "Ajouter un Financeur",
                className: 'financerdialog',
                show: false,
                size: "large",
                buttons: {
                    success: {
                        label: trad.save,
                        className: "btn-primary",
                        callback: function() {

                            //var formInputsHere = formInputs;
                            // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                            tplCtx.path = "answers.aapStep1";

                            tplCtx.path = tplCtx.path + "." + tplCtx.budgetpath + "." + tplCtx.pos + ".financer." + financerList[tplCtx.pos].length;

                            // if( notNull(formInputs [tplCtx.form]) )
                            // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                            var today = new Date();
                            today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
                            tplCtx.value = {
                                line: $(".financerdialog .fi-fond").val(),
                                amount: $(".financerdialog .fi-montant").val(),
                                user: userId,
                                date: today
                            };

                            if ($(".financerdialog .fi-financer").val() != 0) {
                                tplCtx.value.id = $(".financerdialog .fi-financer").val();
                                tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                            } else if ($(".financerdialog .fi-name").val() != "") {
                                tplCtx.value.name = $(".financerdialog .fi-name").val();
                                tplCtx.value.email = $(".financerdialog .fi-mail").val();
                            }

                            delete tplCtx.pos;
                            delete tplCtx.budgetpath;
                            mylog.log("btnFinancer save", tplCtx);
                            dataHelper.path2Value(tplCtx, function(params) {
                                prioModal.modal('hide');
                                //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                prioModal.modal('hide');
                                //reloadInput("<?php //echo $key
                                                ?>//", "<?php //echo @$form["id"]
                                        ?>//");
                                // });
                                location.reload();
                            });
                        }
                    },
                    cancel: {
                        label: trad.cancel,
                        className: "btn-secondary",
                        callback: function() {}
                    }
                },
                onEscape: function() {
                    prioModal.modal("hide");
                }
            });
            prioModal.modal("show");

            var thisbtn = $(this);

            prioModal.on('shown.bs.modal', function(e) {

                $(".sup-btn").off().on("click", function() {
                    prioModal.modal("hide");
                    dyFObj.openForm("organization", function() {}, null, null, {
                        afterSave: function(data) {
                            var sendDataM = {
                                parentId: cntxtId,
                                parentType: cntxtType,
                                listInvite: {
                                    organizations: {}
                                }
                            };
                            sendDataM.listInvite.organizations[data.id] = {};
                            sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                            sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                            ajaxPost("",
                                baseUrl + "/co2/link/multiconnect",
                                sendDataM,
                                function(data) {
                                    dyFObj.closeForm();
                                    //reloadInput("<?php //echo $key
                                                    ?>//", "<?php //echo @$form["id"]
                                                ?>//");
                                    if (!standedAl) {
                                        reloadWizard();
                                    } else {

                                    }
                                },
                                null,
                                "json"
                            );
                            mylog.log("hey", data);
                        }
                    });
                });

                $(".switchcombtn").on("click", function() {
                    $(".financerdialog").find("." + $(this).data("switchactbtn")).addClass("active");
                    $(".financerdialog").find("." + $(this).data("removebtn")).removeClass("active");
                    $(".financerdialog").find("." + $(this).data("switchactdiv")).removeClass("hide");
                    $(".financerdialog").find("." + $(this).data("removediv")).addClass("hide");
                });

            });

        });

        $(".st-enable-btn2").on("click", function() {
            var thisbtn = $(this);
            if (thisbtn.data('loc') == 'edit') {
                thisbtn.data('loc', 'save');
                thisbtn.html('<i class="fa fa-save"></i> enregistrer');
                $("select[data-sheet-id='" + thisbtn.data('id') + "']").prop("disabled", false);
            } else if (thisbtn.data('loc') == 'save') {
                thisbtn.data('loc', 'edit');
                thisbtn.html('<i class="fa fa-pencil"></i> modifier status');
                $("select[data-sheet-id='" + thisbtn.data('id') + "']").prop("disabled", true);
                var tplCtx = {};
                tplCtx.id = thisbtn.data('id');
                tplCtx.collection = "answers";
                tplCtx.path = "status";
                tplCtx.value = $("select[data-sheet-id='" + thisbtn.data('id') + "']").val();
                dataHelper.path2Value(tplCtx, function(params) {
                    toastr.success(trad.saved);
                    ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/newstatus/answerid/' + thisbtn.data('id'), {
                        status: $("select[data-sheet-id='" + thisbtn.data('id') + "']").val(),
                        url: window.location.href
                    },
                    function (data) {

                    }, "html");
                });
            }
        });

        $(".st-save-btn2").on("click", function() {
            var thisbtn = $(this);
            thisbtn.removeClass('st-save-btn2');

        });
        
        var $el_form_id = "<?php echo $el_form_id ?>";
        ajaxPost(
            null,
            baseUrl + '/survey/answer/countvisit/answerid/' + answerid + '/formId/' + $el_form_id,
            null,
            function(data) {
                if(typeof propostionObj != "undefined")
                    propostionObj.refreshNewCounter(answerid);
            },
            "json"
        );

        $.each($(".sheet-markdown"), function(k, v) {
            descHtml = dataHelper.markdownToHtml($(v).html(), {
                parseImgDimensions: true,
                simplifiedAutoLink: true,
                strikethrough: true,
                tables: true,
                openLinksInNewWindow: true
            });
            $(v).html(descHtml);
        });


        $('.view-contributors').off().on('click',function(){
            var dataId = $(this).data("id");
            var dataType = $(this).data("type");
            var elem = $(this);
            ajaxPost("",
                baseUrl+"/co2/element/get/type/"+dataType+"/id/"+dataId,
                null,
                function(data) {
                    if(data.result){
                        if(exists(data.map) && exists(data.map.links) && exists(data.map.links.contributors)){
                            var contributors = Object.keys(data.map.links.contributors);
                            ajaxPost('',baseUrl+"/co2/element/get/type/citoyens", 
                            {id : contributors,fields:["name","profilMediumImageUrl"]},
                            function(dataVotans){
                                mylog.log(dataVotans,"votants");
                                var html = "";
                                $.each(dataVotans.map,function(kv,vv){
                                    var roles = (exists(vv.links) && exists(vv.links.memberOf) && exists(vv.links.memberOf[el["id"]]) &&  exists(vv.links.memberOf[el["id"]]["roles"])) ? vv.links.memberOf[el["id"]]["roles"] : [];
                                    if(exists(el["el"]) && exists(aapObject.elementAap["el"]["collection"]) && aapObject.elementAap["el"]["collection"] === "projects"){
                                        roles = (exists(vv.links) && exists(vv.links.projects) && exists(vv.links.projects[el["id"]]) &&  exists(vv.links.projects[el["id"]]["roles"])) ? vv.links.projects[el["id"]]["roles"] : [];
                                    }
                                    mylog.log(roles,"rolesko")
                                    html += 
                                    `<div class='row'>
                                        <div class="col-xs-2">
                                            <img src="${exists(vv.profilMediumImageUrl) ? vv.profilMediumImageUrl : defaultImage}" alt=""  width="45" height="45" style="border-radius: 100%;object-fit:cover"/>
                                        </div>
                                        <div class="col-xs-10">
                                            <span><small>${vv.name}</small></span><br/>
                                            <span class="text-green"><small>${roles.join(',')}</small></span>
                                        </div>
                                    </div>`;
                                })
                                var dialog = bootbox.dialog({
                                    title : trad.contributors+(!exists(data.map.links.contributors[userId]) ? "<button type='button' class='btn btn-sm bg-green-k hover-white btn-contribute pull-right'>"+ucfirst(trad.contribute)+"</button>":""),
                                    message: html
                                });
                                dialog.init(function(){
                                    $('.btn-contribute').off().on('click',function(){
                                        dialog.modal('hide')
                                        var params = {
                                            parentId : dataId,
                                            parentType : dataType,
                                            listInvite : {
                                                citoyens :{},
                                                organizations:{}
                                            }
                                        }
                                        params.listInvite.citoyens[userId] = userConnected.name
                                        ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                                            toastr.success(trad.saved);
                                            elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text())+1)
                                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                {
                                                    pers : params.listInvite.citoyens,
                                                    url : window.location.href
                                                },
                                                function (data) {

                                                }, "html");
                                        })
                                    })
                                });
                            })
                        }else{
                            if(notNull(userConnected)){
                                var dialog = bootbox.dialog({
                                    title : trad.noone+" "+trad.contributor,
                                    message: "<button type='button' class='btn btn-sm bg-green-k hover-white btn-block btn-contribute'>"+ucfirst(trad.contribute)+"</button>"
                                });
                                dialog.init(function(){
                                    $('.btn-contribute').off().on('click',function(){
                                        dialog.modal('hide')
                                        var params = {
                                            parentId : dataId,
                                            parentType : dataType,
                                            listInvite : {
                                                citoyens :{},
                                                organizations:{}
                                            }
                                        }
                                        params.listInvite.citoyens[userId] = userConnected.name
                                        ajaxPost("",baseUrl+'/'+moduleId+"/link/multiconnect",params,function(data){
                                            toastr.success(trad.saved);
                                            elem.find('.count-contributor').text(parseInt(elem.find('.count-contributor').text())+1)
                                            ajaxPost("", baseUrl + '/survey/answer/rcnotification/action/addperson/answerid/' + answerId,
                                                {
                                                    pers : params.listInvite.citoyens,
                                                    url : window.location.href
                                                },
                                                function (data) {

                                                }, "html");
                                        })
                                    })
                                });
                            }
                        }
                    }
                },
                null,
                "json"
            );
        })

        var swiper = new Swiper(".mySwiper", {
            pagination: {
                el: ".swiper-pagination",
                type: "fraction",
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });


        
        aapObject.sections.listItem.events(aapObject);
    })
</script>