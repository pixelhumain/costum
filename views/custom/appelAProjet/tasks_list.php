<style>
    .tasks-list-container {
        width: 300px;
    }
    .tasks-list {
        position: sticky;
		top: -10px;
    }

    .tasks-list-container .btn-close {
        margin-left: 43%;
        position: relative;
        cursor: pointer;
    }

    .task-list-header .list-group-item:last-child {
        border-radius: 0;
    }
    .task-list {
        overflow-y: auto;
        height: 69vh;
    }

    .task-list .list-group-item:first-child {
        border-radius: 0;
    }

    .task-item {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 300px;
        cursor: pointer;
        font-size: 15px;
    }
    .task-item:hover {
        background-color: whitesmoke;
    }

    .task-item:hover .pos-dropdown, .task-item:hover .pos-lock {
        display: block;
    }
    .popover {
        z-index: 99999999999999999 !important;
        margin-left: 80px;
    }
    .popover .popover-content {
        padding: 0 0 0 1px;
    }
    .popover.bottom .arrow {
        margin-left: 30px;
    }

    .search-container {
        display: flex;
    }

    .search-container .btn-newTask-container button {
        padding: 0;
        margin-left: 10px;
        border-radius: 50%;
        width: 34px;
        height: 34px;
        background-color: #337ab7;
        border-color: #337ab7
    }
    .search-container .btn-newTask-container button:hover {
        background-color: #172B4D;
        border-color: #172B4D
    }

    .new_task_form_container {
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
        padding-top: 10px;
    }

    .new_task_form_container > textarea {
        border: none;
        outline: none;
        flex: 1;
        font-size: 14px;
        height: 70px;
        border-bottom: 1px solid whitesmoke;
        border-right: 1px solid whitesmoke;;
    }

    .new_task_form_container > input:disabled {
        background: none;
        font-size: 14px;
    }

    .new_task_form_container > button {
        border: none;
        outline: none;
        background: transparent;
    }

    .new_task_form_container > button:disabled {
        color: #ddd;
    }

    .list-group {
        margin-bottom: 0;
    }

    .validator {
        font-size: 11px;
        color: #337ab7;
    }

    .pos-dropdown {
        position: absolute;
		top: 18%;
		right: 16px;
        border: none;
        overflow: hidden;
        border-radius: 50%;
        width: 22px;
        height: 22px;
        outline: none;
        z-index: 9999;
        text-align: center;
        font-size: 11px;
        color: white;
        background-color: #CC0000;
        padding-left: 5px;
        display: none;
    }
    .pos-lock {
        position: absolute;
		top: 18%;
		right: 40px;
        border: none;
        overflow: hidden;
        border-radius: 50%;
        width: 22px;
        height: 22px;
        outline: none;
        z-index: 999999;
        color: white;
        background-color: #172B4D;
        text-align: center;
        font-size: 11px;
        padding: 2px 0 0 2px;
        display: none;
    }
</style>
<div class="tasks-list">
    <div class="tasks-list-container">
        <div class="task-list-header">
            <ul class="list-group">
                <li class="list-group-item active">Liste des tâches <span class="fa fa-times btn-close"></span></li>
                <li class="list-group-item">
                    <div class="search-container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"> </i></span>
                            <input id="taskSearch" type="text" class="form-control" name="taskSearch" placeholder="">
                        </div>
                        <div class="btn-newTask-container">
                            <button class="btn btn-primary btn-tasks" disabled='true'><span class="fa fa-plus"></span></button>
                        </div>
                    </div>
                    <div class="collapse" id="newtaskinput">
                        <div class="new_task_form_container">
                            <input type="hidden" value="-1">
                            <textarea type="text" placeholder="<?= Yii::t('common', 'Type new task here') ?> ..."></textarea>
                            <button type="submit" disabled>
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <aside class="task-list co-scroll">
            <ul class="list-group"></ul>
        </aside>
    </div>
</div>
<script>
    $(document).ready(function(){
        var tasksList = JSON.parse(JSON.stringify(<?= json_encode($tasksList) ?>));
        console.log(tasksList, 'tasksList');
        $('[data-toggle="tooltip"]').tooltip(); 
        function getAction(__id){
            return new Promise(function(__resolve){
                var url = baseUrl + '/costum/project/action/request/action_detail_data';
                var post = {
                    id : __id
                };
                ajaxPost(null, url, post, __resolve);
            });
        }
        function getTask(__action, __range){
            return new Promise(function(__resolve){
                var url = baseUrl + '/costum/project/action/request/task';
                var post = {
                    action : __action,
                    task : __range
                };
                ajaxPost(null, url, post, __resolve);
            });
        }
        function onPopover(dom, id, index, text, lock) {
            $(dom).popover({
                html : true,
                placement : 'bottom',
                trigger : 'focus',
                content : function(){
                    return `<div class="list-group">
                                <a href="javascript:;" class="list-group-item btn-modifTask" data-id=${id} data-index=${index} data-text="${text}"><i class='fa fa-pencil'> </i> Modifier</a>
                                <a href="javascript:;" class="list-group-item btn-deleteTask" data-id=${id} data-index=${index} data-text="${text}"><i class='fa fa-archive'> </i> Supprimer</a>
                                <a href="javascript:;" class="${lock == true ? "list-group-item btn-assign-to-me" : "list-group-item btn-assign-to-me"}" data-id=${id} data-index=${index} data-text="${text}"><i class='fa fa-user-plus'> </i> Participer</a>
                            </div>`;
                }
            }).popover('show')
            $('.btn-deleteTask').click(function(e){
                var index = $(this).data('index')
                mylog.log('e target', index)
                deleteTask(index)
            })
            $('.btn-modifTask').click(function(e){
                if (notEmpty(userConnected)) {
                    var index = $(this).data('index')
                    var text = $(this).data('text')
                    var input_container = $('.new_task_form_container');

                    input_container.find('button[type=submit]').prop('disabled', false);
                    input_container.find('button[type=submit] .fa').removeClass('fa-plus').addClass('fa-edit');
                    input_container.find('textarea').val(text);
                    input_container.find('input[type=hidden]').val(index);
                    if (!$('#newtaskinput').hasClass('in') ) {
                        $('#newtaskinput').collapse('toggle');
                        $('.task-list').css('height', '55vh')
                    }
                }
            });
            $('.btn-assign-to-me').click(function(e){
                var self = $(this);
                var index = $(this).data('index')

                if (notEmpty(userConnected)) {
                    var params = {
                        id : tasksList._id.$id,
                        collection : 'actions',
                        path : 'tasks.' + index + '.contributors',
                        value : {}
                    };
                    params.value[userConnected._id.$id] = {
                        type : 'citoyens'
                    };

                    dataHelper.path2Value(params, function(response){
                        if (response.result && response.result === true) {
                            var btnLockDom = $(`<button type="button" data-id=${self.data('taskId')} data-index=${self.data('index')}  data-userId=${self.data('userId')} data-checked=${self.data('checked')} data-text="${self.data('task')}">`)
                                .addClass('pos-lock')
                            var lockIcon = $(`<i class="fa fa-lock"></i>`)
                            btnLockDom.append(lockIcon)
                            // $('.task-item[data-index='+index+']').append(btnLockDom)
                            // $('.task-item[data-index='+index+'] .pos-dropdown').data('lock', true)
                            self.addClass('hidden')
                            toastr.success("Vous avez choisi de participer a cette sous tache!");
                        }
                    });
                }
            });
        }
        function updateCheck(self, index, actionId) {
            var setType = 
            [
                {
                    path : 'checked',
                    type : 'boolean'
                },
                {
                    path : 'checkedAt',
                    type : 'isoDate'
                },
                {
                    path : 'createdAt',
                    type : 'isoDate'
                }
            ];

            getTask(actionId, index).then(function(task){
                task.checked = self.prop('checked');
                if (self.prop('checked')) {
                    task.checkedAt = moment().format();
                    task['checkedUserId'] = userConnected['_id']['$id'];
                } else {
                    task.checkedAt = null;
                }

                var params = {
                    id : actionId,
                    collection : 'actions',
                    path : 'tasks.' + index,
                    value : task,
                    setType
                }

                dataHelper.path2Value(params, function(response){
                    if (response.result && response.result === true) {
                        var parent = $(self).parents('.task-item')
                        if (self.prop('checked')) {
                            var userCheckDom = $(`<div>`)
                                .addClass('validator')
                                .text('Par : '+userConnected.name)
                            parent.append(userCheckDom)
                        } else {
                            parent.find('div').addClass('hidden')
                        }
                        // update_task_recap();
                        sendUpdateTask({
                            operation : 'update',
                            content : task.task,
                            index : index
                        }).then();

                        // Envoyer une notification rocket chat pour la tâche terminée
                        // var url = baseUrl + '/survey/answer/rcnotification/action/checktask2';
                        // var post = {
                        //     projectname : globals.parentName,
                        //     taskname : task.task,
                        //     actname : globals.actionName,
                        //     channelChat : globals.parentSlug,
                        // };
                        // ajaxPost(null, url, post);
                    }
                });
            });
        }
        function showTasks() {
            var listGroupDom = $('.task-list .list-group');
            getAction(tasksList._id.$id).then(function(action){
                console.log('La case à ete cocher est cochée', action);
                while (listGroupDom.children().length > 2) {
                    listGroupDom.children().eq(2).remove();
                }
                if (typeof tasksList != 'undefined' && typeof tasksList.tasks != 'undefined') {
                    for (var i = 0; i < tasksList.tasks.length; i++) {
                        var lock = false;
                        var listItemDom = $(`<li data-id=${tasksList.tasks[i].taskId} data-index=${i}  data-userId=${tasksList.tasks[i].userId} data-checked=${tasksList.tasks[i].checked} data-text="${tasksList.tasks[i].task}">`)
                            .addClass("list-group-item")
                            .addClass("task-item")    
                        var checkDom = $(`<input type="checkbox" value=""/>`)
                        var textDom = $(`<span data-index=${i} data-text="${tasksList.tasks[i].task}">`)
                            .addClass('task-text')
                            .text(" "+tasksList.tasks[i].task)
                        var btnDropdownDom = $(`<button type="button" data-toggle="tooltip" title="Supprimer" data-id=${tasksList.tasks[i].taskId} data-index=${i}  data-userId=${tasksList.tasks[i].userId} data-checked=${tasksList.tasks[i].checked} data-text="${tasksList.tasks[i].task}">`)
                            .addClass('pos-dropdown')
                        if(typeof tasksList.tasks[i].contributors != 'undefined' && Object.keys(tasksList.tasks[i].contributors).indexOf(userConnected._id.$id) > -1) {
                            var btnLockDom = $(`<span type="button" data-toggle="tooltip" title="Ne plus contribuer" data-id=${tasksList.tasks[i].taskId} data-index=${i}  data-userId=${tasksList.tasks[i].userId} data-checked=${tasksList.tasks[i].checked} data-text="${tasksList.tasks[i].task}">`)
                                .addClass('pos-lock')
                            var lockIcon = $(`<i class="fa fa-user-times"></i>`)
                        } else {
                            var btnLockDom = $(`<span type="button" data-toggle="tooltip" title="Contribuer" data-id=${tasksList.tasks[i].taskId} data-index=${i}  data-userId=${tasksList.tasks[i].userId} data-checked=${tasksList.tasks[i].checked} data-text="${tasksList.tasks[i].task}">`)
                                .addClass('pos-lock')
                            var lockIcon = $(`<i class="fa fa-user-plus"></i>`)
                        }
                        var iconBtn = $(`<i class="fa fa-trash"></i>`)
                        btnDropdownDom.append(iconBtn)
                        btnLockDom.append(lockIcon)
                        listItemDom.append(btnLockDom)
                        listItemDom.append(btnDropdownDom)
                        btnLockDom.on('click', (function(index) {
                            return function(e) {
                                var self = $(this);
                                var index = $(this).data('index')
                                participate(self, index);
                            };
                        })(i));
                        btnDropdownDom.on('click', (function(index) {
                            return function(e) {
                                var index = $(this).data('index')
                                mylog.log('e target', index)
                                deleteTask(index)
                                // onPopover($(this).parent('.task-item'), $(this).data('id'),$(this).data('index'),$(this).data('text'), $(this).data('lock'))
                            };
                        })(i));
                        textDom.on('click', (function(index) {
                            return function(e) {
                                if (notEmpty(userConnected)) {
                                    var index = $(this).data('index')
                                    var text = $(this).data('text')
                                    var input_container = $('.new_task_form_container');

                                    input_container.find('button[type=submit]').prop('disabled', false);
                                    input_container.find('button[type=submit] .fa').removeClass('fa-plus').addClass('fa-edit');
                                    input_container.find('textarea').val(text);
                                    input_container.find('input[type=hidden]').val(index);
                                    if (!$('#newtaskinput').hasClass('in') ) {
                                        $('#newtaskinput').collapse('toggle');
                                        $('.task-list').css('height', '55vh')
                                    }
                                }
                            };
                        })(i));
                        mylog.log(lock, 'lock')
                        if (tasksList.tasks[i].checked == true) {
                            checkDom.prop('checked', true)
                            listItemDom.append(checkDom)
                            listItemDom.append(textDom)
                            if (typeof action.allContributors != 'undefined') {
                                $.map(action.allContributors, function(val, key){
                                    if (typeof tasksList.tasks[i].checkedUserId != 'undefined' && key == tasksList.tasks[i].checkedUserId) {
                                        var userCheckDom = $(`<div>`)
                                            .addClass('validator')
                                            .text('Par : '+val.name)
                                        listItemDom.append(userCheckDom)
                                    }
                                })
                            }
                        } else {
                            checkDom.prop('checked', false)
                            listItemDom.append(checkDom)
                            listItemDom.append(textDom)
                        }
                        checkDom.on('click', (function(index){
                            return function(e) {
                                updateCheck($(this), index, tasksList._id.$id)
                            };
                        })(i));
                        listGroupDom.append(listItemDom);
                    }
                }
            })
           
            
        }
        showTasks()
        function sendUpdateTask(options){
            return new Promise(function(resolve){
                var url = baseUrl + '/costum/project/action/request/socket_task_update';
                options = $.extend({action : tasksList._id.$id}, options);
                if (typeof wsCO != 'undefined' && wsCO!= null && wsCO === 'object' && typeof wsCO.id === 'string' && wsCO.id.length) {
                    options.emiter = wsCO.id;
                }
                ajaxPost(null, url, options, resolve);
            });
        }
        function participate(self, index) {
            if (notEmpty(userConnected)) {
                var params = {
                    id : tasksList._id.$id,
                    collection : 'actions',
                    path : 'tasks.' + index + '.contributors',
                    value : {},
                };
                if($(self).find('i').hasClass('fa-user-plus')) {
                    params.value[userConnected._id.$id] = {
                        type : 'citoyens'
                    };

                    dataHelper.path2Value(params, function(response){
                        if (response.result && response.result === true) {
                            $(self).find('i').removeClass('fa-user-plus').addClass('fa-user-times')
							coInterface.actions.ping_update_task({
								action: tasksList._id.$id,
								index: index,
								content: params.value,
								operation: 'contribution'
							}).then();
                            toastr.success("Vous avez choisi de contribuer a cette sous tache!");
                        }
                    });
                } else {
                    var userId = userConnected._id.$id
                    delete tasksList.tasks[index].contributors[userId]
                    params.value = tasksList.tasks[index].contributors

                    dataHelper.path2Value(params, function(response){
                        if (response.result && response.result === true) {
                            $(self).find('i').addClass('fa-user-plus').removeClass('fa-user-times');
							coInterface.actions.ping_update_task({
								action: tasksList._id.$id,
								index: index,
								content: params.value,
								operation: 'contribution'
							}).then();
                            toastr.success("Vous avez choisi de ne plus contribuer a cette sous tache!");
                        }
                    });
                }
            }
        }
        function deleteTask(index) {
            var self = this;
            mylog.log(index, 'index')
			if (notEmpty(userConnected)) {
				$.confirm({
					title : 'Supprimer',
					content : 'Supprimer cette tâche',
					buttons : {
						no : {
							text : 'Non',
							btnClass : 'btn btn-default'
						},
						yes : {
							text : 'Oui',
							btnClass : 'btn btn-danger',
							action : function(){
								var params = {
									id : tasksList._id.$id,
									collection : 'actions',
									path : 'tasks.'+index,
									pull : 'tasks',
									value : null
								};

								dataHelper.path2Value(params, function(__response){
									if (__response.result && __response.result === true) {
										mylog.log(__response.result, '_response')
                                        $('.task-item[data-index='+index+']').remove()
                                        toastr.success("Sous tache supprimer!");
									}
								});
							}
						}
					}
				});
			}
        }
        function addNewtask(task, activeAction){
            return new Promise(function(resolve){
                var value = {
                    taskId : (new Date()).getTime(),
                    userId : userConnected['_id']['$id'],
                    task : task,
                    createdAt : moment().format(),
                    checked : false
                };

                var setType = [{
                    path : 'checked',
                    type : 'boolean'
                },
                    {
                        path : 'createdAt',
                        type : 'isoDate'
                    }
                ];

                var params = {
                    id : activeAction,
                    collection : 'actions',
                    path : 'tasks',
                    arrayForm : true,
                    edit : false,
                    format : true,
                    value,
                    setType
                };

                dataHelper.path2Value(params, function(response){
                    if (response.result && response.result === true) {
                        resolve(response.text);
                    }
                });
            });
        }
        function update_task(__value, __range, activeAction){
            return new Promise(function(__resolve){
                getTask(activeAction, __range).then(function(__task){
                    __task.task = __value;
                    var setType = [{
                        path : 'checked',
                        type : 'boolean'
                    },
                        {
                            path : 'createdAt',
                            type : 'isoDate'
                        }
                    ];

                    var params = {
                        id : activeAction,
                        collection : 'actions',
                        path : 'tasks.' + __range,
                        value : __task,
                        setType
                    };

                    dataHelper.path2Value(params, function(__response){
                        if (__response.result && __response.result === true) {
                            __resolve(__response['text']);
                        }
                    });
                });
            });
        }
        $('.tasks-list-container .btn-close').on('click', function(){
            $('.tasks-list-container').addClass('hidden')
        })
        $('.btn-newTask-container button').on('click', function(){
            var fa = $('.btn-newTask-container button .fa');
			var self = '#taskSearch';
			var buttonDom = $('.btn-newTask-container button');
			var value = $(self).val();
            var index = (typeof tasksList.tasks != 'undefined' ? tasksList.tasks.length : 0);
            var listGroupDom = $('.task-list .list-group');

			if ($(this).prop("disabled"))
				return ;
			if (value && notEmpty(userConnected)) {
				// enregistrer une nouvelle tâche
				if (fa.hasClass('fa-plus')) {
					addNewtask(value, tasksList._id.$id).then(function(task){
						buttonDom.prop('disabled', true);
                        var listItemDom = $(`<li data-id=${task.taskId} data-index=${index}  data-userId=${task.userId} data-checked=${task.checked }>`)
                            .addClass("list-group-item")
                            .addClass("task-item")    
                        var checkDom = $(`<input type="checkbox" value=""/>`)
                        var textDom = $(`<span data-index=${index} data-text="${$(self).val()}">`)
                            .addClass('task-text')
                            .text(" "+$(self).val())
                        checkDom.prop('checked', false)
                        listItemDom.append(checkDom)
                        listItemDom.append(textDom)

                        var btnDropdownDom = $(`<button type="button" data-id=${task.taskId} data-index=${index}  data-userId=${task.userId} data-checked=${task.checked } data-text=${task.task }>`)
                            .addClass('pos-dropdown')
                        var iconBtn = $(`<i class="fa fa-trash"></i>`)
                        btnDropdownDom.append(iconBtn)
                        listItemDom.append(btnDropdownDom)

                        var btnLockDom = $(`<span data-toggle="tooltip" title="Contribuer" data-id=${task.taskId} data-index=${index}  data-userId=${task.userId} data-checked=${task.checked } data-text=${task.task }>`)
                            .addClass('pos-lock')
                        var iconLock = $(`<i class="fa fa-user-plus"></i>`)
                        btnLockDom.append(iconLock)
                        listItemDom.append(btnLockDom)

                        checkDom.on('click', function(){
                            updateCheck($(this), index, tasksList._id.$id)
                        })
                        textDom.on('click', function(){
                            if (notEmpty(userConnected)) {
                                var index = $(this).data('index')
                                var text = $(this).data('text')
                                var input_container = $('.new_task_form_container');

                                input_container.find('button[type=submit]').prop('disabled', false);
                                input_container.find('button[type=submit] .fa').removeClass('fa-plus').addClass('fa-edit');
                                input_container.find('textarea').val(text);
                                input_container.find('input[type=hidden]').val(index);
                                if (!$('#newtaskinput').hasClass('in') ) {
                                    $('#newtaskinput').collapse('toggle');
                                    $('.task-list').css('height', '55vh')
                                }
                            }
                        })
                        btnDropdownDom.on('click', function(index) {
                            var pos = $(this).data('index')
                            mylog.log('e target', pos)
                            deleteTask(pos)
                        });
                        btnLockDom.on('click', function(index) {
                            var pos = $(this).data('index')
                            mylog.log('e target', pos)
                            participate(this, pos)
                        });
                        listGroupDom.append(listItemDom);
						sendUpdateTask({
							operation : 'insert',
							content : value
						}).then();
                        if (typeof tasksList.tasks != 'undefined') {
                            tasksList.tasks.push(task);
                        } else {
                            tasksList['tasks'] = [];
                            tasksList.tasks.push(task);
                        }
                        toastr.success("Sous tache ajouter avec succes!");
					});
				} else if (fa.hasClass('fa-edit')) {
                    // modifier la tâche existante
					var range = $(this).prev().val();
					update_task(value, range, tasksList._id.$id).then(function(task){
                        mylog.log('task update', task);
                        listGroupDom.find('li[data-id="'+task.taskId+'"] span').text(" "+task.task)
						buttonDom.prop('disabled', true);
                        fa.removeClass('fa-edit').addClass('fa-plus');
						sendUpdateTask({
							operation : 'update',
							content : value,
							index : range
						}).then();
                        $(self).val('');
                        toastr.success("Sous tache modifier avec succes!");
					});
				}
			}
            // if (!$('#newtaskinput').hasClass('in') ) {
            //     $('#newtaskinput').collapse('toggle');
            //     $('.task-list').css('height', '55vh')
            // } else {
            //     $('#newtaskinput').collapse('toggle');
            //     $('.task-list').css('height', '69vh')
            // }
        })
        $("#taskSearch").on("keydown", function(e) {
            var value = $(this).val().toLowerCase();
            var matchFound = false;
            $('.btn-newTask-container button').prop('disabled', false);

			if (e.originalEvent.key && e.originalEvent.key.toLowerCase() === "enter") {
				$('.btn-newTask-container button').trigger("click");
				return ;
			}
            $(".list-group .task-item").each(function() {
                var $taskItem = $(this);
                var taskText = $taskItem.text().toLowerCase();
                var normalizedValue = value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                var normalizedTaskText = taskText.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                
                if (normalizedTaskText.includes(normalizedValue)) {
                    $taskItem.show();
                    matchFound = true; 
				} else
                    $taskItem.hide();
            });

            // Si aucune correspondance n'a été trouvée
            if (matchFound)
                $('.btn-newTask-container button').prop('disabled', true);
        });
        $('#newtaskinput').on('input', '.new_task_form_container textarea', function(){
            var fa = $('.new_task_form_container button[type=submit] .fa');
            if ($(this).val().length === 0) {
                $('#newtaskinput').collapse('toggle');
                $('.task-list').css('height', '69vh')
                fa.removeClass('fa-edit').addClass('fa-plus');
                $(this).next('button[type=submit]').prop('disabled', $(this).val().length === 0);
            } else {
                $(this).next('button[type=submit]').prop('disabled', $(this).val().length === 0);
            }
		});

        $('#newtaskinput').on('keydown', '.new_task_form_container textarea', function(event){
			var fa = $('.new_task_form_container button[type=submit] .fa');
			var self = this;
			var buttonDom = $('.new_task_form_container button[type=submit]');
			var value = $(self).val();
            var index = typeof tasksList.tasks != 'undefined' ? tasksList.tasks.length : 0;
            var listGroupDom = $('.task-list .list-group');

			if (event.key === 'Enter' && value && notEmpty(userConnected)) {
				// enregistrer une nouvelle tâche
				if (fa.hasClass('fa-plus')) {
					addNewtask(value, tasksList._id.$id).then(function(task){
						buttonDom.prop('disabled', true);
                        var listItemDom = $(`<li data-id=${task.taskId} data-index=${index}  data-userId=${task.userId} data-checked=${task.checked }>`)
                            .addClass("list-group-item")
                            .addClass("task-item")    
                        var checkDom = $(`<input type="checkbox" value=""/>`)
                        var textDom = $(`<span>`)
                            .text(" "+$(self).val())
                        checkDom.prop('checked', false)
                        listItemDom.append(checkDom)
                        listItemDom.append(textDom)
                        var btnDropdownDom = $(`<button type="button" data-id=${task.taskId} data-index=${index}  data-userId=${task.userId} data-checked=${task.checked }>`)
                            .addClass('pos-dropdown')
                        var iconBtn = $(`<i class="fa fa-caret-down"></i>`)
                        btnDropdownDom.append(iconBtn)
                        listItemDom.append(btnDropdownDom)
                        checkDom.on('click', function(){
                            updateCheck($(this), index, tasksList._id.$id)
                        })
                        btnDropdownDom.on('click', function(index) {
                            onPopover($(this).parent(), $(this).data('id'),$(this).data('index'),$(this).text())
                            mylog.log('index', $(this).text());
                        });
                        listGroupDom.append(listItemDom);
						sendUpdateTask({
							operation : 'insert',
							content : value
						}).then();
                        if (typeof tasksList.tasks != 'undefined') {
                            tasksList.tasks.push(task);
                        } else {
                            tasksList['tasks'] = [];
                            tasksList.tasks.push(task);
                        }
                        $(self).val('');
                        toastr.success("Sous tache ajouter avec succes!");
					});
				} else if (fa.hasClass('fa-edit')) {
                    // modifier la tâche existante
					var range = $(this).prev().val();
					update_task(value, range, tasksList._id.$id).then(function(task){
                        $('#newtaskinput').collapse('toggle');
                        $('.task-list').css('height', '69vh')
                        mylog.log('task update', task);
                        listGroupDom.find('li[data-id="'+task.taskId+'"] .task-text').text(" "+task.task)
                        listGroupDom.find('li[data-id="'+task.taskId+'"] .task-text').data('text', task.task)
						buttonDom.prop('disabled', true);
                        fa.removeClass('fa-edit').addClass('fa-plus');
						sendUpdateTask({
							operation : 'update',
							content : value,
							index : range
						}).then();
                        $(self).val('');
                        toastr.success("Sous tache modifier avec succes!");
					});
				}
			}
		});
    });
</script>