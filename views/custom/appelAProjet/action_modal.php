<?php
	$blockKey = $blockKey ?? sha1(date('Y-m-d'));
	$css_js = [
		'/plugins/jquery-confirm/jquery-confirm.min.css',
		'/plugins/jquery-confirm/jquery-confirm.min.js',
		'/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.css',
		'/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.js',
		'/plugins/jQuery-contextMenu/dist/jquery.ui.position.min.js'
	];
	$isProjectContributor = !empty(Yii::app()->session['userId']) && !empty($projectContributors[Yii::app()->session['userId']]);
	HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
	$css_js = [
		'/js/action_modal/action_modal.js'
	];
	HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->getModule(Costum::MODULE)->getAssetsUrl());
?>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
<style>
    #action-modal<?= $blockKey ?> {
        --aap-primary-color: #337ab7;
    }

    #action-modal<?= "$blockKey " ?> {
        z-index: 100001;
        background-color: rgba(0, 0, 0, .4);
    }

    #action-modal <?= "$blockKey " ?>* {
        font-family: Raleway, sans-serif;
        font-size: 1.6rem;
    }

    #action-modal<?= "$blockKey " ?>.fa {
        font-family: FontAwesome;
    }

    #action-modal<?= "$blockKey " ?>.project_name {
        color: #e33551;
    }

    .list-group-item:last-child {
        border-bottom: 1px solid #ddd !important;
    }

    .task_list .list-group-item,
    .contributor_list .list-group-item {
        font-size: 14px;
        padding: 10px 15px;
        font-weight: bold;
    }

    .task-group-container {
        padding: 0 !important;
    }

    .task_list .list-group-item label.done {
        text-decoration: line-through;
        font-weight: normal;
    }

    .task_list .list-group-item.active,
    .contributor_list .list-group-item.active {
        font-weight: bold;
    }

    .item-avatar {
        min-height: 72px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        background-color: #fff;
        color: #444;
        position: relative;
        z-index: 2;
        margin: -1px;
        padding: 16px;
        font-size: 16px;
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
    }

    .item-avatar.bordered {
        border: 1px solid #ddd;
    }

    #action-modal<?= "$blockKey " ?>.item-avatar img {
        -webkit-user-drag: none;
        margin: 0;
        padding: 0;
        border: 0;
        width: 50px;
        height: 50px;
        vertical-align: baseline;
        font: inherit;
        font-size: 100%;
        border-radius: 50%;
    }

    #detail_contribution_indicator<?= $blockKey ?> {
        color: #ffc900;
        display: block;
        text-align: center;
        gap: 5px;
    }

    .task-line-container {
        display: flex;
        flex-direction: row;
        align-items: center;
        margin: 10px 15px !important;
    }

    .no-task {
        padding: 0 !important;
    }

    .no-task span {
        display: block;
        margin: 10px 15px;
    }

    .task-line-container .contributor,
    .task-line-container .executor {
        font-weight: normal;
    }

    .task-line-container .executor {
        color: #51e335;
    }

    .task-line-container .contributor {
        color: #ffc900;
    }

    .task-line-container .executor.show,
    .task-line-container .contributor.show {
        display: block;
    }

    .task-line-container .executor.hide,
    .task-line-container .contributor.hide {
        display: none;
    }

    #action-modal<?= "$blockKey " ?>tr > td:nth-child(n+2) {
        text-align: left;
        padding: 5px 10px;
    }

    .new_task_form_container {
        display: flex;
        flex-direction: row;
        align-items: center;
        gap: 10px;
    }

    .new_task_form_container > input {
        border: none;
        outline: none;
        flex: 1;
        font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
        font-weight: normal;
    }

    .new_task_form_container > input:disabled {
        background: none;
    }

    .new_task_form_container > button {
        border: none;
        outline: none;
        background: transparent;
    }

    .new_task_form_container > button:disabled {
        color: #ddd;
    }

    .cpl,
    #cpl-select {
        background-color: #f9f9f9;
        padding: 9px 10px;
        display: flex;
        align-items: center;
        gap: 5px;
    }

    #cpl-search {
        position: relative;
    }

    #cpl-search > input,
    #cpl-search > input:focus {
        border: 1px solid #cacaca;
        border-radius: 20px;
        line-height: 1.8rem;
        padding: 10px 10px 10px 35px;
        width: 100%;
        outline: none;
    }

    #cpl-search > .fa {
        position: absolute;
        top: 50%;
        left: 10px;
        transform: translateY(-50%);
        color: #cacaca;
        font-size: 2rem;
    }

    .cpl-text {
        -moz-user-select: none;
        -webkit-user-select: none;
        user-select: none;
        cursor: default;
        display: block;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .cpl-actions {
        display: flex;
        flex-direction: row;
        position: relative;
    }

    .checkmark {
        display: block;
        height: 25px;
        width: 25px;
        background-color: #fff;
    }

    .checkmark:after,
    .checkmark:before {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    input:checked ~ .checkmark:after {
        display: block;
    }

    input:indeterminate ~ .checkmark:before {
        display: block;
    }

    /* Style the checkmark/indicator */
    .checkmark:after {
        left: 10px;
        top: 7px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .checkmark:before {
        left: 50%;
        top: 50%;
        width: 15px;
        height: 1px;
        border: solid white;
        border-width: 0 0 3px 0;
        -webkit-transform: translateX(-50%) translateY(50%);
        -ms-transform: translateX(-50%) translateY(50%);
        transform: translateX(-50%) translateY(50%);
    }

    .checkmark.default:after,
    .checkmark.default:before {
        border-color: #000;
    }

    .checkmark.disabled:after,
    .checkmark.disabled:before {
        border-color: #ddd;
    }

    .cpl-check {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    .cpl-check ~ .checkmark {
        border: 3px solid #000;
    }

    .cpl-check ~ .checkmark.disabled {
        border: 3px solid #ddd;
    }

    .cpl-check:checked ~ .checkmark {
        background-color: #fff;
    }

    #action-status-switcher {
        margin-bottom: 20px;
    }

    label.disabled {
        font-weight: normal;
        color: #ddd;
    }

    #image-container img {
        width: 100%;
    }

    /** File upload */
    .file-upload-preview {
        width: 100%;
        position: relative;
        padding: 25px;
    }

    .file-upload-preview:nth-child(3) {
        margin-top: 50px;
    }

    .delete-upload-preview {
        position: absolute;
        right: 0;
        top: 0;
        display: block;
        width: 30px;
        height: 30px;
        background: #00000096;
        border-radius: 50%;
        border: none;
        outline: none;
        color: white;
    }

    .upload-image-container {
        width: 100%;
        overflow: hidden;
        border-radius: 10px;
    }

    .upload-image-container img {
        width: 100%;
    }

    #drag-file-area {
        margin: 5px 0;
        border: 2px dotted;
        border-radius: 10px;
        position: relative;
        min-height: 200px;
    }

    #choose-file-button {
        position: absolute;
        top: 10px;
        left: 50%;
        transform: translateX(-50%);
    }

    #drag-file-area-label {
        display: block;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        font-size: 29px;
        color: #939393;
        width: 100%;
        text-align: center;
    }

    #accordion-additional-setting {
        margin-top: 10px;
        font-weight: normal;
    }

    .contributor-selection {
        display: flex;
        flex-direction: row;
        max-width: 100%;
        align-items: center;
        gap: 10px;
    }

    .contributor-img-container {
        width: 50px;
        height: 50px;
        overflow: hidden;
        border-radius: 50%;
    }

    .contributor-img-container img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    #assign-action-form {
        display: none;
        margin: 0 0 20px 0;
    }

    #assign-action-form > .btn {
        margin-top: 5px;
    }

    /* Document tabs */
    #documentsTab {
        padding-top: 20px;
    }

    .document-upload-form,
    .image-upload-form {
        box-sizing: border-box;
        width: 120px;
        border: 1px solid #cbcaca;
        text-align: center;
        border-radius: 5px;
        transition: background-color .3s ease;
    }

    .document-upload-form:hover,
    .image-upload-form:hover {
        cursor: pointer;
        background: #ddd;
    }

    #document-upload-input,
    #image-upload-input {
        display: none;
    }

    .document-compact-list,
    .image-compact-list {
        margin-top: 20px;
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        justify-items: center;
    }

    .document-compact-list .document-element,
    .image-compact-list .image-element {
        text-align: center;
    }

    .document-compact-list .document-icon,
    .image-compact-list .image-icon {
        font-size: 100px;
        display: block;
    }

    .document-compact-list .document-name {
        width: 100%;
        height: auto;
        display: -webkit-box;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align: center;
    }

    .image-compact-list .image-element {
        width: 128px;
        height: 128px;
        overflow: hidden;
        box-sizing: border-box;
        margin: 5px;
        border-radius: 8px;
        border: 2px solid #ddd;
    }

    .image-compact-list .image-element img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .image-upload-form img,
    .document-upload-form img {
        width: 100%;
        display: inline-block;
        box-sizing: border-box;
        padding: 20px;
    }

    .co-nav-tabs-badge {
        position: absolute;
        display: block;
        top: -50%;
        right: 0;
    }
</style>
<div class="modal fade action-modal" id="action-modal<?= $blockKey ?>">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" style="text-transform: unset;"><span class="project_name"><?= $parent_name ?></span> : <span
							class="action_name"><?= $action_name ?></span></h4>
			</div>
			<div class="modal-body">
				<?php if ($isProjectContributor) : ?>
					<div class="btn-group btn-group-justified" id="action-status-switcher">
						<?php if (!empty($status) && in_array($status, ['todo'])) : ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default" id="action-modal-assign">
									<span class="fa fa-users"></span> Assigner
								</button>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-default<?= count($contributors) >= $max ? ' disabled' : '' ?>"
								        id="action-modal-participate">
									<span class="fa fa-link"></span> <?= Yii::t('common', 'Participate') ?>
								</button>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-default" id="action-modal-no-longer-participate">
									<span class="fa fa-unlink"></span> <?= Yii::t('common', 'Not participate anymore') ?>
								</button>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-default" data-status="disabled">
									<span class="fa fa-times"></span> <?= Yii::t('common', 'Cancel') ?>
								</button>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-default" data-status="done">
									<span class="fa fa-thumbs-up"></span> <?= Yii::t('common', 'Finish') ?>
								</button>
							</div>
						<?php endif;
							if (!empty($status) && in_array($status, ['done', 'disabled', 'closed'])) : ?>
								<div class="btn-group">
									<button type="button" class="btn btn-default" data-status="todo">
										<span class="fa fa-thumbs-up"></span> Réouvrir
									</button>
								</div>
							<?php endif ?>
					</div>
				<?php endif ?>
				<div id="assign-action-form">
					<select id="assign-action-select" multiple></select>
					<button type="button" class="btn btn-default">Confirmer</button>
				</div>
				<div class="item-avatar bordered">
					<img src="" data-src="" class="visible">
					<p style="margin: 0;">
						Auteur : <span class="creator"></span>
					</p>
				</div>
				<br>
				
				<ul class="nav nav-tabs" role="tablist">
					<li class="<?= empty($activeTab) || $activeTab === 'summary' ? ' active' : '' ?>">
						<a href="#summaryTab" role="tab" data-toggle="tab"><?= Yii::t('common', 'Summary') ?></a>
					</li>
					<li class="<?= !empty($activeTab) && $activeTab === 'image' ? ' active' : '' ?>">
						<a href="#imagesTab" role="tab" data-toggle="tab">
							<span class="badge">0</span> <?= Yii::t('common', 'Images') ?>
						</a>
					</li>
					<li class="<?= !empty($activeTab) && $activeTab === 'document' ? ' active' : '' ?>">
						<a href="#documentsTab" role="tab" data-toggle="tab">
							<span class="badge">0</span> <?= Yii::t('common', 'Documents') ?>
						</a>
					</li>
				</ul>
				
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane<?= empty($activeTab) || $activeTab === 'summary' ? ' active' : '' ?>" id="summaryTab">
                        <span>Action à réaliser commençant le <span id="begining_date<?= $blockKey ?>"></span>
	                        <!-- il y a <span id="month_begining_computed<?= $blockKey ?>"></span> -->
                        </span>
						<div id="description-section">
							<div id="description-content">
								<label><?= Yii::t('common', 'Description') ?></label>
								<?php if ($canAdmin || $isProjectContributor) : ?>
									<button class="btn btn-xs btn-primary edit-description"><span class="fa fa-pencil"></span></button><?php endif; ?>
								<br>
								<p><?php if (!empty($description)) echo nl2br($description);
									else echo Yii::t('common', 'No description'); ?></p>
							</div>
							<?php if ($canAdmin || $isProjectContributor) : ?>
								<div id="description-editor-container" style="display: none">
									<div class="form-group">
                                        <textarea class="form-control" style="resize: vertical" rows="3"
                                                  placeholder="<?= Yii::t('common', 'Put description here') ?>..."><?= $description ?? '' ?></textarea>
									</div>
									<button type="button" class="btn btn-default"><?= Yii::t('common', 'Save') ?></button>
								</div>
							<?php endif; ?>
						</div>
						<snap id="detail_contribution_indicator<?= $blockKey ?>"></snap>
						<br/>
						<ul class="list-group task_list">
							<li class="list-group-item active"><?= Yii::t('common', 'Tasks list') ?></li>
							<li class="list-group-item new_task_form_container">
								<span class="fa fa-tasks"></span>
								<input type="hidden" value="-1">
								<input type="text" placeholder="<?= Yii::t('common', 'Type new task here') ?> ...">
								<button type="submit" disabled>
									<i class="fa fa-plus"></i>
								</button>
							</li>
							<li class="list-group-item">
								<button class="btn btn-sm btn-default" data-toggle="collapse" href="#collapseOne">
									<span class="fa fa-cog"></span>
								</button>
								<span class="task_number_recap"></span>
								<div id="collapseOne" class="collapse">
									<div class="panel-group" id="accordion-additional-setting">
										<div class="panel panel-default">
											<div class="panel-heading">
												<p class="panel-title">
													Options
												</p>
											</div>
											<div class="panel-body">
												<div class="checkbox">
													<label>
														<input type="checkbox" class="option-task" id="show-my-tasks" data-condition="my-task">
														Afficher seulement mes tâches
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" class="option-task" id="show-done-tasks" checked="checked"
														       data-condition="done"> Afficher les tâches finies
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="progress">
									<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0"
									     aria-valuemin="0" aria-valuemax="100" style="width: 0%">
										<span class="sr-only">0% Complete (success)</span>
									</div>
								</div>
							</li>
						</ul>
						<br>
						<ul class="list-group contributor_list">
							<li class="list-group-item active">Ils participent à cette action</li>
						</ul>
						<table>
							<tbody>
							<tr>
								<td><i class="fa fa-credit-card" aria-hidden="true"></i></td>
								<td>Vous gagnerez <strong id="detail_credit<?= $blockKey ?>"></strong> crédit</td>
							</tr>
							<tr>
								<td><i class="fa fa-calendar" aria-hidden="true"></i></td>
								<td>Début le <span id="detail_full_begining<?= $blockKey ?>"></span></td>
							</tr>
							<tr>
								<td><i class="fa fa-clock-o" aria-hidden="true"></i></td>
								<td>Durée <span id="detail_duration<?= $blockKey ?>"></span></td>
							</tr>
							<tr>
								<td><i class="fa fa-male" aria-hidden="true"></i></td>
								<td>Il y a <span id="detail_contributor<?= $blockKey ?>"></span> participant<span
											id="detail_contributor_multiple<?= $blockKey ?>">s</span></td>
							</tr>
							<tr>
								<td><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></td>
								<td>Pour un besoin minimum de <span id="detail_contributor_min<?= $blockKey ?>"></span> participant<span
											id="detail_contributor_min_multiple<?= $blockKey ?>">s</span></td>
							</tr>
							<tr>
								<td><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></td>
								<td>Et un maximum de <span id="detail_contributor_max<?= $blockKey ?>"></span> participant<span
											id="detail_contributor_max_multiple<?= $blockKey ?>">s</span></td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="tab-pane<?= !empty($activeTab) && $activeTab === 'image' ? ' active' : '' ?>" id="imagesTab">
						<input type="file" id="image-upload-input" multiple="multiple">
						<h4 class="center"><?= Yii::t('common', 'You can drop the file directly below') ?></h4>
						<form autocomplete="off">
							<input type="text" class="form-control" id="file-upload-paste" placeholder="<?= Yii::t('common', 'Paste images here') ?>">
						</form>
						<div class="image-compact-list">
							<div id="image-upload-container">
								<div class="image-upload-form">
									<img src="<?= Yii::app()->getBaseUrl(true) . '/images/plus-icon.svg' ?>">
									<span><?= Yii::t('common', 'New images') ?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane<?= !empty($activeTab) && $activeTab === 'document' ? ' active' : '' ?>" id="documentsTab">
						<input type="file" id="document-upload-input" multiple="multiple">
						<h4 class="center"><?= Yii::t('common', 'You can drop the file directly below') ?></h4>
						<div class="document-compact-list">
							<div id="document-container">
								<div class="document-upload-form">
									<img src="<?= Yii::app()->getBaseUrl(true) . '/images/plus-icon.svg' ?>">
									<span><?= Yii::t('common', 'New documents') ?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div><!-- /.modal-content -->
	</div>
</div>

<script type="text/javascript">
	(function($, W){
		// Traductions
		trad['totest'] = 'À tester';

		// Début variable globale
		var actionId = '<?= $action ?>';
		var actionName = JSON.stringify(<?= json_encode($action_name) ?>);
		var parentSlug = '<?= $parent_slug ?>';
		var parentId = '<?= $parentId ?>';
		var parentName = JSON.stringify(<?= json_encode($parent_name) ?>);
		var projectContributors = JSON.parse(JSON.stringify(<?= json_encode($projectContributors) ?>));
		var max = JSON.parse(JSON.stringify(<?= json_encode($max) ?>));
		var contributors = JSON.parse(JSON.stringify(<?= json_encode($contributors) ?>))
		// Fin variable globale
		actionModal($, {
			actionId : actionId,
			actionName : actionName,
			parentId : parentId,
			parentSlug : parentSlug,
			parentName : parentName,
			projectContributors : projectContributors,
			contributors : contributors,
			max : max,
			mode : JSON.parse(JSON.stringify(<?= json_encode($mode ?? 'r') ?>)),
			tags : JSON.parse(JSON.stringify(<?= json_encode($tags ?? []) ?>)),
			canAdmin : JSON.parse(JSON.stringify(<?= json_encode($canAdmin) ?>)),
			isProjectContributor : JSON.parse(JSON.stringify(<?= json_encode($isProjectContributor) ?>)),
			isActionContributor : JSON.parse(JSON.stringify(<?= json_encode(!empty(Yii::app()->session['userId']) && !empty($contributors[Yii::app()->session['userId']])) ?>))
		}, '<?= $blockKey ?>');
		if (notEmpty(userConnected)) {
			if (typeof contributors[userConnected._id.$id] !== 'undefined') {
				$('#action-modal-participate').parent('.btn-group').hide();
			} else {
				$('#action-modal-no-longer-participate').parent('.btn-group').hide();
			}
		} else {
			$('#action-modal-participate').parent('.btn-group').hide();
			$('#action-modal-no-longer-participate').parent('.btn-group').hide();
		}
	})(jQuery, window);
</script>