<?php 
    $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 

<style type="text/css">
	body{
		background-image: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/images/eywa/background.png");
	}
	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	.events .searchEntity {
    	background-color: white !important;
	}

	.footerSearchContainer {
		display: none;
	}

	.smartgrid-slide-element .text-wrap div.counter {
		position: initial;
	}

	.smartgrid-slide-element .counter .list-dash {
		width : 100%;
	}

	#search-content{
		padding-top : 33px;
	}
	#filters-nav{
	    background: #fff !important;
    display:block !important;
  }
	}

	#filters-nav .container-filters-menu {
	    margin-left: 20px !important;
	}

	@media (min-width: 770px) and (max-width: 900px){
	    #filters-nav .container-filters-menu {
	        margin-left: 20px !important;
	    }
	}

	@media (min-width: 900px) and (max-width: 1099px){
	    #filters-nav .container-filters-menu {
	        margin-left: 20px !important;
	    }
	}

	@media (min-width: 1100px) and (max-width: 1320px){
	    #filters-nav .container-filters-menu {
	        margin-left: 20px !important;
	    }
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var eventList=<?php echo json_encode(Event::$types); ?>;

	var paramsFilter = {
		container : "#filters-nav",
	 	header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true,
						types : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						map : true
					}
				}
			}
	 	}
	};

	if (pageApp == "annuaire") {

		// costum.searchObject = { sortBy : "name" };

	 	paramsFilter.defaults = {
		 	types : ["organizations"],
		 	fields : ["typePlace"],
		 	indexStep : 0,
		 	sort : { "name": 1 }
		};

		paramsFilter.inteface = {
			events : {
				scroll : true,
				scrollOne : true
			}
		};

		paramsFilter.results = {
			renderView: "directory.elementPanelHtml",
		 	smartGrid : true
		};

	 	paramsFilter.filters = {
 			scope :{
 				name : "Code Postal",
	 			view : "scope",
	 			type : "scope",
	 			event : "scope",
	 			action : "scope"
	 		},
	 		types : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "type",
	 			name : "Organisation",
	 			event : "filters",
	 			list : {
					"NGO" : "Association", 
					"LocalBusiness": "Entreprise", 
					"Group" : "Groupe",
					"Cooperative" : "Cooperative", 
					"GovernmentOrganization" : "Service public"
	 			}
	 		},
	 		/*typePlace : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "typePlace",
	 			name : "Type de lieu",
	 			event : "filters",
	 			list : costum.lists.typePlace
	 		}*/
	 	}
	}

	if (pageApp == "jobs") {
		paramsFilter.inteface = {
			events : {
				scroll : true,
				scrollOne : true
			}
		};

		paramsFilter.results = {
			renderView: "directory.classifiedPanelHtml",
		 	smartGrid : true
		};

		paramsFilter.defaults = {
			types : ["classifieds"],
			fields : ["subtype"]
		}
	}

	if (pageApp == "projects") {
		paramsFilter.inteface = {
			events : {
				scroll : true,
				scrollOne : true
			}
		};

		paramsFilter.results = {
			renderView: "directory.elementPanelHtml",
		 	smartGrid : true
		};

		paramsFilter.defaults = {
			types : ["projects"]
		};
	}

	if (pageApp == "agenda") {
	 	paramsFilter.urlData = baseUrl+"/co2/search/agenda";
	 	paramsFilter.calendar = true;
	 	paramsFilter.loadEvent = {
	 		default : "agenda"
	 	};

	 	paramsFilter.defaults = {
 			types : ["events"]
 		};

 		paramsFilter.results = {
 			renderView : "directory.eventPanelHtml"
 		};

	 	paramsFilter.filters = {
	 		text : true,
	 		scope : true,
	 		type : {
	 			view : "dropdownList",
	 			event : "selectList",
	 			type : "type",
	 			list : eventList
	 		}
	 	};
	}


	filterSearch = {};

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});
</script>