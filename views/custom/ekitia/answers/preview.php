<?php 
HtmlHelper::registerCssAndScriptsFiles(array( 
  '/js/answer.js',
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

$form =  PHDB::findOneById( Form::COLLECTION , $element["form"]);

?>
<style type="text/css">
    .toolsMenu{
        box-shadow: 0px 1px 3px 0px #a3a3a3;
    }
    #modal-preview-coop{
        overflow : none;
    }

	#modal-preview-coop{
		box-shadow: -2px 10px 10px 0px rgba(0, 0, 0, 0.5) !important
	}
    .blockFontPreview {
		font-size: 14px;
		color: #ec5d85;
	}
	.step-vld{
		display: none;
	}
	#description{
		text-align: justify;
  		text-justify: inter-word;
	}

	#socialNetwork {
		font-size: 20px;
	}

	.mx-1 {
		margin: auto 1vh;
	}

	.space-top-prev {
		margin-top: 8vh;
	}

	.faw-size {
		font-size: xx-large;
	}

	@media (max-width: 628px) {

		.media-object {
			margin: auto 20%;
			width: 200px !important;
			height: 200px !important;
		}

		.media {
			margin: auto 8vh;
		}

		.media-left {
			display: inherit !important;
		}

		.space-top-prev {
			margin-top: 4vh;
		}
	}


	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-top: 80px;
		padding-bottom: 100px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	h4::before {
		content: "";
		position: absolute;
		left: 10px;
		z-index: -1;
		margin-top: -10px;
		margin-left: -10px;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.title-banner h2::before {
    content: "";
    position: absolute;
    top: 15%;
    z-index: -1;
    margin-top: -20px;
    margin-left: 50px;
    width: 2em;
    height: 2em;
    background-color: #F9B000;
    border-radius: 50%;
}

	.title-section {
		position: absolute;
	}

	.title-banner {
		position: sticky;
	}

	.m-title-contact {
		margin: auto 8vh;
    	margin-top: 2vh;
	}

	.dv-contact-content {
		margin-top: 3vh;
		margin-bottom: 10vh;
	}
</style>
<script type="text/javascript">
    var sectionDyf = {};
    var formInputs = {};
    var answerObj = <?php echo (!empty($element)) ? json_encode( $element ) : "null"; ?>;
</script>
<?php
if(!isset($costum)){
	$costum = CacheHelper::getCostum();
}
?>
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>">
    <div class="col-xs-12 padding-10 toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <!-- a href="#answer.index.id.<?php echo (string)$element["_id"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Open") ?></a-->
    </div>
    <div class="col-xs-12 no-padding container-preview">
		<?php if(Yii::app()->session["userId"] && Authorisation::isElementMember($costum["contextId"], $costum["contextType"], Yii::app()->session["userId"])){ ?>    

        <div class="banner-outer">
            <div class="basic-banner">
                <div class="basic-banner-inner">
                    <div class="container-banner max-950"> 
                        <div class="basic-info text-center">
							<?php 
								if(isset($element["links"]["organizations"])){
									foreach ($element["links"]["organizations"] as $key => $value) {
										echo '<a href="#page.type.'.$value["type"].'.id.'.$key.'" class="lbh-preview-element">
											<span class="ico-circle"><img width="50" height="50" src="/upload/communecter/'.$value["type"].'/'.$key.'/thumb/profil-resized.png"/></span> '.$value["name"].'</a>';
									}
								}
							 ?>
                            <div class="title-banner">
                                <h2 class="font-weight-100"><?php echo $form["name"] ?></h2>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-20">
			<?php 
				$params = [
					"form"=> $form,
					"canEdit" => @$canEdit,
					"answer"=>$element,
					"mode" => (isset($_GET["mode"])) ? $_GET["mode"] : "r",
					"showWizard"=>false,
					"preview"=>true,
					"wizid"=> $element["form"],
					"color1"=>"",
					"color2"=>""
				];
				if(isset($params["form"]["subForms"]) && count($params["form"]["subForms"]) > 1)
					$params["form"]["subForms"]=[$params["form"]["subForms"][0]];
				$params = Form::getDataForm($params);
				$params["parentForm"]=$params["form"];
				$params["canEditForm"]=false;
				$params["canAdminAnswer"] = false;

				echo $this->renderPartial("survey.views.tpls.forms.wizard",$params); 
			?>
    	</div>
		<?php }else{ ?>
			<div class="text-center text-danger margin-top-35">
				<i class="fa fa-lock fa-4x "></i>
				<h1>Accès Non Autorisé</h1>
				<p>Il faut être membre de la plateforme</p>
			</div>
		<?php } ?>

    </div>
</div>

<script>
    $(".title-header").remove()
</script>

		