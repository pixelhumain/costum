<style>
	#filters-nav-actus{
		position: sticky !important;
        padding: 16px 5px 9px 16px;
	}

	#filters-nav{
		display:none !important
	}

	.main-container{
		padding-top: 56px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS, #search-content{
		padding-top: 12px !important;
		border-top: none !important;
	}
	.searchObjCSS .dropdown .btn-menu, .searchBar-filters .search-bar, .searchBar-filters .input-group-addon, .btn-primary-ekitia{
		border-radius :  2px !important;
		border: 1px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.searchBar-filters .input-group-addon{
		background-color: var(--primary-color) !important;
		color: white !important;
		padding: 10px 14px !important;
		height: 44px;
	}

	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.banner-outer-tool {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
		height: 250px;
		
	}
	.basic-banner {
		width: 100%;
		height: 100%;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
		display:flex;
		align-items: center;
		justify-content: space-around;
		flex-wrap: wrap;
    	align-content: center;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-yellow-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer{
		justify-content: space-around !important;
	}
	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	.bootbox.modal .modal-body{
		padding: 0px;
	}

	#dropdownFilters{
		position:static;
		bottom:0;
	}
	.activeFilter-label{
		display: none !important;
	}
	.headerSearchContainer{
		display: none
	}
	#tab-page{
		margin-bottom: 12px;
	}
	#filters-nav-container{
		position: relative;
		z-index: 9;
	}

	@media (min-width:600px)  { 
		#filters-nav-container{
			padding-left: 35px;
		}

		#ethic-container{
			padding-left: 100px;
		}
	}
</style>

<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}
</style>

<style>
     
 #noMoreNews {
        position: relative;
        padding: 0px 40px;
        bottom: 0px;
        width: 100%;
        text-align: center;
        background: white;
    }
    #newsstream .loader,
    #noMoreNews{
       border-radius: 50px;
        margin-left: auto;
        margin-right: auto;
        display: table;
        padding: 15px;
        margin-top: 15px;
    }

    /* TIMELINE */
    .container-timeline, 
    .bodySearchContainer, .timeline-item, 
    .timeline-icon, .timeline-content, 
    .timeline-content h2 , .timeline-content p{
  box-sizing: border-box;
}
.container-timeline {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-family: helvetica, arial, tahoma, verdana;
  line-height: 20px;
  font-size: 14px;
  font-weight: 500;
  color: #726f77;
  width: 100%;
}

.bodySearchContainer {
   position: relative;
	width: 90%;
	margin: 30px auto;
	line-height: 1.5em;
	font-size: 14px;
	-webkit-transition: all 0.4s ease;
	-moz-transition: all 0.4s ease;
	-ms-transition: all 0.4s ease;
	transition: all 0.4s ease;
}

/* timeline vertical line */

.bodySearchContainer {
  list-style: none;
  position: relative;
}
.bodySearchContainer .direction-right:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 2px;
  left:0%;
  border-left: 2px dashed var(--primary-color);
  margin-left: -1.5px;
}

.bodySearchContainer .direction-left:after {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 2px;
  left:100%;
  border-left: 2px dashed var(--primary-color);
  margin-left: -1.5px;
}

.bodySearchContainer .clearFix {
  clear: both;
  height: 0;
}
.bodySearchContainer .timeline-badge {
  color: #fff;
  width: 66px;
  height: 64px;
  font-size: 1em;
  text-align: center;
  position: absolute;
  top: 20px;
  left:100%;
  margin-left: -30px;
  background-color: var(--primary-color);
  border-top-right-radius: 50%;
  border-top-left-radius: 5%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
  z-index: 28;
  padding: 8px;
}

.bodySearchContainer .col-sm-offset-6 .timeline-badge{
    left: 0%;
  border-top-right-radius: 5%;
  border-top-left-radius: 50%;
}



.bodySearchContainer .timeline-badge span.timeline-balloon-date-day {
  font-size: 1.4em;
  display: block;
}
.bodySearchContainer .timeline-badge span.timeline-balloon-date-month {
  font-size: .7em;
  position: relative;
  top: -10px;
}
.bodySearchContainer .timeline-badge.timeline-filter-movement {
  background-color: #ffffff;
  font-size: 1.7em;
  height: 35px;
  margin-left: -18px;
  width: 35px;
  top: 40px;
}
.bodySearchContainer .timeline-badge.timeline-filter-movement a span {
  color: var(--primary-color);
  font-size: 1.3em;
  top: -1px;
}
.bodySearchContainer .timeline-badge.timeline-future-movement {
  background-color: #ffffff;
  height: 35px;
  width: 35px;
  font-size: 1.7em;
  top: -16px;
  margin-left: -18px;
}
.bodySearchContainer .timeline-badge.timeline-future-movement a span {
  color: var(--primary-color);
  font-size: .9em;
  top: 2px;
  left: 1px;
}
.bodySearchContainer .timeline-movement-top {
  height: 60px;
}
.bodySearchContainer .timeline-item {
  padding: 20px 0;
}
.bodySearchContainer .timeline-item img{
  border-radius: 10px;
}
.bodySearchContainer .timeline-item .timeline-panel {
  border: 1px solid #d4d4d4;
  border-radius: 3px;
  background-color: #FFFFFF;
  color: #666;
  padding: 10px;
  position: relative;
  /*-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);*/
}
.bodySearchContainer .timeline-item .timeline-panel .timeline-panel-ul {
  list-style: none;
  padding: 0;
  margin: 0;
}
.bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul {
  text-align: right;
}
.bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul li {
  color: #666;
}
.bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul li span.importo {
  color: #468c1f;
  font-size: 1.3em;
}
.bodySearchContainer .timeline-item .timeline-panel.debits .timeline-panel-ul {
  text-align: left;
}
.bodySearchContainer .timeline-item .timeline-panel.debits .timeline-panel-ul span.importo {
  color: #e2001a;
  font-size: 1.3em;
}
.timeline-desc{
    text-align: justify;
    font-size: 12pt !important;
    word-wrap: break-word;
    white-space: pre-line;
}
.color-ekitia{
    color: var(--primary-color);
}

.bodySearchContainer{
    width: 100%;
}
</style>

<div id="tab-page" class="tab-outer bg-white" style="position:sticky; top:80px; z-index:99"> 
	<div id="tabs-container" class="tabs left large-tab">
		<div class="term-item actus tab-page">
			<a type="button" class="tabs lbh" data-type="all" href="#actus" data-filterk="tabs">
				<span class="ico-circle"><i class="fa fa-list"></i></span> Actualités
			</a>
		</div>
		<div class="term-item events tab-page" data-type="events" id="tab-event">
			<a type="button" class="tabs lbh" data-type="events" href="#events"  data-filterk="tabs">
				<span class="ico-circle"><i class="fa fa-file-text"></i></span> événement
            </a>
		</div>
	</div>
    <div class="tabs btn-group right">
        <div id="create-event-container" class="term-item right-content">
            <button id="add-event-button" onclick="costum[costum.slug].beforePost(``, `event`);" class="btn btn-primary-ekitia btn-primary-outline-ekitia">
                Créer un événement
            </button>
        </div>
        <div class="term-item btn-event-actus-ctn">
            <button id="btn-event-view-switcher" class="btn btn-primary-ekitia btn-primary-outline-ekitia" data-switch="card">
                <i class="fa fa-th-list"></i> Vue Vignette
            </button>
        </div>
    </div>
</div>

<div id="filters-nav-actus"></div>

<div id="live-container" class="row bg-white live-container margin-top-30">
    <div class="col-md-12 col-sm-12 col-xs-12 bg-white top-page" id="" style="padding-top:0px!important;">
    	<div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-right hidden-xs" id="sub-menu-left"></div>
    	<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 margin-top-10">
    		<div id="newsstream"></div>
    	</div>	
    </div>
</div>


<script type="text/javascript" >
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filtersByPage = {
        text : {
            placeholder : "Recherche",
            searchBy: "ALL"
        }
    };
	var filterFields = ["name", "mediaImg", "text", "organizer", "date", "parent", "created", "source"];
	var pageDirectory = "directory.timelineEvent";
	var defaultFilters = {'$or':{}};
	var sortResultBy = {"startDate":-1};
	
	// defaultFilters["type"]={'$ne':"activityStream"};
	defaultFilters["type"]={'$nin':["activityStream", "tool", "doc"]};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["parentId"] = costum.contextId;
	defaultFilters['$or']["source.keys"] = costum.slug;
    //searchObject.initType="news";
    var titlePage = appConfig.subdomainName;
    var liveParams = appConfig;


    directory.appKeyParam=(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;

    if(typeof pageApp=="undefined" || pageApp=="events"){
        $(".term-item.events").addClass("active");
    }else{
        $(".term-item.actus").addClass("active");
        $("#add-event-button").hide();
        $("#btn-event-view-switcher").hide();
    }


    var paramsFilter = {
        container : "#filters-nav-actus",
        options : {
            tags : {
                verb : '$all'
            }
        },
        loadEvent : {
            default : "scroll"
        },
        defaults : {
            notSourceKey: true,
            types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
            filters: defaultFilters,
            indexStep: 0,
            sortBy: sortResultBy
        },
        filters : filtersByPage,
        results : {
            renderView: pageDirectory
        }
    }

    if(filterFields.length!=0){
        paramsFilter.defaults["fields"] = filterFields;
    }

    var direction = "";

    directory.timelineEvent = function(v){
        var hash = "";
        var resultHtml = "";
        var image = (v.profilMediumImageUrl != undefined) ?  v.profilMediumImageUrl : "https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg";
        var shortDescriptionLink = "(pas de description)";
        if(typeof v.shortDescription !="undefined"){
            if(v.shortDescription.length > 200){
                let short = v.shortDescription.substr(0, 200);
                shortDescriptionLink = short.substr(0, short.lastIndexOf(" ")) + " ...";
            }else{
                shortDescriptionLink = v.shortDescription.replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, '<a href="$1" target="_blank" class="color-ekitia">$1</a> ');
            }
        }
        let eTypes = [];
        if(v.collection=="events" && v.type){
            if(typeof v.type == "object"){
                $.each(v.type, function(eIndex, eType){
                    if(typeof tradCategory[eType] != "undefined"){
                        eTypes.push(tradCategory[eType])
                    }else{
                        eTypes.push(eType)
                    }
                })
            }else if(typeof v.type == "string" && typeof tradCategory[v.type]!="undefined"){
                eTypes.push(tradCategory[v.type])
            }else{
                eTypes.push(v.type)
            }
        }
        resultHtml += `
            <div class="${direction} ${direction==""?"direction-left":"direction-right"} col-sm-6 timeline-item">
                <div class="timeline-badge">
                    <span class="timeline-balloon-date">${moment(v.startDate).local().locale('fr').format('DD')}</span>
                    <small class="timeline-balloon-date">${moment(v.startDate).local().locale('fr').format('MMM')}</small>
                    <span class="timeline-balloon-date-year">${moment(v.startDate).local().locale('fr').format('YYYY')}</span>
                </div>
                <div class="row no-padding">
                    <div class="${(direction=="")?"col-sm-offset-1 col-sm-10":"col-sm-offset-1 col-sm-10"}">
                        <div class="timeline-panel row">
                            <div class="timeline-panel-ul col-sm-8">
                                <h4 class="color-ekitia">${v.name}</h4>
                                <div>
                                    <span class="text-muted margin-right-10"><i class="glyphicon glyphicon-time"></i> ${moment(v.startDate).local().locale('fr').format('HH:mm')}</span>
                                    <span class="text-muted"><i class="glyphicon glyphicon-tag"></i> ${eTypes.toString()}</span>
                                </div>
                                <p class="timeline-desc markdown">${dataHelper.markdownToHtml(shortDescriptionLink)}</p>
                                <div class="link-bordered">
                                    <a href="#page.type.events.id.${v._id.$id}" class="lbh-preview-element">En savoir plus</a>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="${image}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        resultHtml+=""
        direction = (direction == "") ? "col-sm-offset-6" : "";
        return resultHtml;
    }

    jQuery(document).ready(function() {
        if(pageApp=="actus"){
            startNewsSearch(true);
        }else{
            $("#search-content").show();
            $("#live-container").hide()
            filterSearch =  searchObj.init(paramsFilter);
        }

        if(userConnected!=null && contextData!=null &&
            typeof userConnected.links != "undefined" && 
            typeof userConnected.links.memberOf!="undefined" && 
            (typeof userConnected.links.memberOf[contextData.id]!="undefined" && (typeof userConnected.links.memberOf[contextData.id].isAdmin=="undefined" || (typeof userConnected.links.memberOf[contextData.id].roles!="undefined" && userConnected.links.memberOf[contextData.id].roles.includes("Lecteur"))))
            ){
            $("#create-event-container").hide();
        }

        /*$.each(userConnected.links.memberOf, function(id, obj){
            if(obj){

            }
        });*/

        $("#btn-event-view-switcher").off().on("click", function(){
            if($(this).data("switch")=="tml"){
                $(this).data("switch", "card");
                $(this).html("<i class='fa fa-th-list'></i> Vue Vignette");
                direction = "";
                filterSearch.results.renderView = "directory.timelineEvent";
                filterSearch.search.init(filterSearch);
            }else{
                $(this).data("switch", "tml");
                $(this).html("<i class='fa fa-th-list'></i> Vue Timeline");
                filterSearch.results.renderView = "directory.cards";
                filterSearch.search.init(filterSearch);
            }
        })
    });

    function startNewsSearch(isFirst){
        $("#search-content").hide();
        var urlLive = "/news/co/index/type/organization/isLive/true/";
        var dataSearchLive={search:true};
        if(typeof liveParams != "undefined" && notNull(liveParams)){
            if(typeof liveParams.slug != "undefined" && notNull(costum)){
                urlLive = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId;
                dataSearchLive=null; 
            }else if(typeof contextData!="undefined" && notNull(contextData)){
                urlLive = "/news/co/index/type/"+contextData.collection+"/id/"+contextData._id.$id;
                dataSearchLive=null;
            }else if(typeof costum!="undefined" && notNull(costum)){
                urlLive = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId;
                dataSearchLive=null;
            }
            
            if(typeof liveParams.filters!="undefined" && typeof liveParams.filters.types!="undefined"){
                dataSearchLive = {searchTypes : liveParams.filters.types};
            }

            urlLive+="/nbCol/1"

            if(typeof liveParams.formCreate != "undefined"){
                urlLive += "/formCreate/false";
            }
            if(typeof liveParams.setParams != "undefined"){
                members = (typeof liveParams.setParams.members != "undefined") ? liveParams.setParams.members : false;
                myScopes.type="open";
                myScopes.open=[];
                /*if(typeof liveParams.setParams.source != "undefined") 
                    urlLive += "/source/"+liveParams.setParams.source;
                else*/
                urlLive += "/source/"+costum.contextSlug;
            }
        }
        coInterface.showLoader("#newsstream");
        coInterface.simpleScroll(0, 500);
        ajaxPost("#newsstream",baseUrl+"/"+urlLive, dataSearchLive, function(news){ 
            const arr = document.querySelectorAll('img.lzy_img')
            arr.forEach((v) => {
                imageObserver.observe(v);
            });
            spinSearchAddon();
        });
    }
</script>