<style type="text/css">
	#modal-preview-coop{
        left: 0% !important
    }   
    h2 {
		font-size: unset;
	}

	.content-desc {
		margin: auto 8vh;
		margin-top: 8vh;
	}

	.tags-poi-preview{
		background-color: #F9B000;
	}

	@media (max-width: 628px) {
		.media-object {
			margin: auto 20%;
			width: 200px !important;
			height: 100px !important;
		}

		.media {
			margin: auto 8vh;
		}

		.media-left {
			display: inherit !important;
		}
	}

	.banner-outer-preview {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-bottom: 20px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	.title-banner h1::before {
		content: "";
		position: absolute;
		/* left: 37%; */
		left: 40%;
		top: 100%;
		/* bottom: 50%; */
		z-index: -1;
		margin-left: -1em;
		width: 6em;
		height: 6em;
		background-color: transparent;
		border-radius: 50%;
		background-position-x: center;
		background-position-y: center;
		background-size: contain;
		border-radius: 0%;
	}

	.font-weight-100 {
		font-weight: 100;
	}

	.title-banner {
		position: sticky;
	}
	.title-section {
		position: relative;
	}

	.section-title {
		margin: auto 8vh;
    	margin-top: 2vh;
	}

	#results-container{
		display: flex;
		flex-wrap: wrap;
	}
</style>

<?php
	$cssAnsScriptFilesModule = array(
		'/js/default/preview.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

?>
	<div id="preview-elt-ethic" class="col-xs-12 no-padding">
		<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
			<div class="banner-outer-preview">
                <div class="col-xs-12 padding-10">
                    <div class="pull-left">
                        <h4 class="no-margin">PRINCIPE <span class="h2" style="color:var(--primary-color)"><?php echo $indexp ?></span> : <?php echo $principe ?></h4>
                    </div>
                    <button class="btn btn-default pull-right btn-close-preview">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
				<div class="basic-banner">
                    <div class="title-banner">
                        <h1> <?php echo $indexp.".".$indexi ?> <span style="color:var(--primary-color)"> <?php echo $name ?></h1>
                    </div>
				</div>
			</div>
            <div class="padding-10">
			    <?php echo $description ?></h1>
                <div class="searchObjCSS">
                    <div id="filter-container"></div>
                </div>
                <div id="results-container"></div>
            </div>
		<br/><br/><br/><br/>
	</div>
</div>
<script type="text/javascript">
    var paramsFilter = {
		container : "#filter-container",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
			fields: ["toolLevel", "toolType", "mainThematic"],
		 	types : ["poi"],
		 	filters: {
				"ethicCharte":"<?php echo $name ?>",
				"type":"tool"
			},
			indexStep: 900
		},
        filters : {
            niveau : {
                name:"Filtre par Niveau",
                type:"filters",
                view:"dropdownList",
                event:"filters",
                field:"toolLevel",
                list:costum.lists.toolLevel
            }
        },
	 	results : {
			dom: "#results-container",
			renderView: "directory.cards"
		}
	}

    var filterSearchPreview = searchObj;

    jQuery(document).ready(function(){
        filterSearchPreview = filterSearchPreview.init(paramsFilter);
		filterSearchPreview.search.init(filterSearchPreview);
		$(".filters-activate[data-type='preview']").click();

    })
</script>