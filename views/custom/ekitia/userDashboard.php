<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}
?>
<style>
    #filters-nav{
        display: none !important;
    }
    .banner-outer {
        background-color: #f9f9f9;
        background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
        background-position: bottom right;
        background-repeat: no-repeat;
        background-size: 25% auto;
        height: 250px;
        display:flex;
        align-items: center;
        justify-content: space-around;
    }


</style>
<script type="text/javascript">
    var appConfig=<?php echo json_encode(@$appConfig); ?>;

	var defaultFilters = {
        "$or":{
            "source.keys":costum.slug, 
            "reference.costum":costum.slug
        }
    }
    defaultFilters["links.members."+userId+""] = {'$exists':true};
    defaultFilters["slug"] = {'$ne': costum.slug};

	var paramsFilter = {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : ["organizations"],
		 	filters: defaultFilters,
			indexStep: 900,
			sortBy: {"name": 1}
		},
	 	results : {
			renderView: "directory.logos",
		 	smartGrid : true
		}
	}

	jQuery(document).ready(function(){
		searchObj.results.smartCallBack = function(fObj,timeOut, last){
				mylog.log($(".smartgrid-slide-element img.isLoading").length, "length img not loaded", $(".smartgrid-slide-element.last-item-loading").length);
				if($(".smartgrid-slide-element img.isLoading").length <= 0){
					fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element"});
				}else{
					setTimeout(function(){
						fObj.results.smartCallBack(fObj, (timeout+100));
					}, (timeout+100));
				}
				if($(".logo-item").length==1){
					$(".banner-outer").remove();
					$(".logo-item a").click();
				}

				if($(".logo-item").length==0){
					$("#subTitleDash").text("Vous n'êtes membre ou administrateur d'aucune structure pour le moment.")
				}
			}


		filterSearch = searchObj.init(paramsFilter);
		$(".headerSearchContainer").remove();

		$(".banner-outer").remove();
        $(`<div class="banner-outer ">
			<div class="basic-banner text-center">
                <h1 class="font-weight-100">${appConfig.bannerTitle||appConfig.subdomainName||appConfig.metaDescription||""}</h1>
                <p id="subTitleDash" class="lead">Vous êtes membre de ces structures et vous pouvez accéder à leur tableau de bord.</p>
				${(costum.isCostumAdmin==true)?'<a class="btn btn-primary-ekitia btn-open-form" href="javascript:;" data-form-type="organization" data-redirecturl=""> CRÉER UNE NOUVELLE STRUCTURE </a>':""}
			</div>
		</div>`).insertBefore("#filters-nav");
	});

</script>