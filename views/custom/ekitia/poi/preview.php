<style type="text/css">
	h2 {
		font-size: unset;
	}

	.content-desc {
		margin: auto 8vh;
		margin-top: 8vh;
		font-size: 15pt;
	}

	.tags-poi-preview{
		background-color: #F9B000;
	}

	@media (max-width: 628px) {
		.media-object {
			margin: auto 20%;
			width: 200px !important;
			height: 200px !important;
		}

		.media {
			margin: auto 8vh;
		}

		.media-left {
			display: inherit !important;
		}
	}


	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-top: 80px;
		padding-bottom: 100px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	.title-banner h1::before {
		content: "";
		position: absolute;
		/* left: 37%; */
		left: 40%;
		top: 100%;
		/* bottom: 50%; */
		z-index: -1;
		margin-left: -1em;
		width: 6em;
		height: 6em;
		background-size: 25% auto;
		background-image: url("<?php echo (isset($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>");
		background-repeat: no-repeat;
		background-color: transparent;
		border-radius: 50%;
		background-position-x: center;
		background-position-y: center;
		background-size: contain;
		border-radius: 0%;
	}

	.title-section h3::before {
		content: "";
		position: absolute;
		top: 2%;
		z-index: -1;
		margin-left: 0px;
		margin-top: -10px;
		width: 40px;
		height: 40px;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.font-weight-100 {
		font-weight: 100;
	}

	.title-banner {
		position: sticky;
	}
	.title-section {
		position: relative;
	}

	.section-title {
		margin: auto 8vh;
    	margin-top: 2vh;
	}
	.bootbox-body{
		padding: 10px;
		font-size: 18pt;
	}

	.description-preview{
		text-align: justify;
		white-space: pre-wrap;
	}
</style>

<?php
if(isset($element["_id"])){
	$cssAnsScriptFilesModule = array(
		'/js/default/preview.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
	$previewConfig = @$this->appConfig["preview"];
	$auth = (Authorisation::canEditItem(@$userId, $type, $element["_id"])) ? true : false;

	$iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
	$userId = Yii::app()->session["userId"]??null;
	if(!isset($costum)){
		$costum = CacheHelper::getCostum();
	}
?>
	<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding">
		<div class="col-xs-12 padding-10">
			<button class="btn btn-default pull-right btn-close-preview">
				<i class="fa fa-times"></i>
			</button>

			<?php if(@$element["type"]=="tool" && (Authorisation::canEdit($userId, (string)$element["_id"], "poi")|| Authorisation::isCostumAdmin())){ ?>
				<button class="btn btn-primary pull-right margin-right-10 btn-edit-tool-ekitia" data-type="<?php echo $element["type"] ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>" title="Modifier">
					<i class="fa fa-pencil"></i>
				</button>
				<button class="btn btn-danger pull-right margin-right-10 btn-delete-tool-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" title="Supprimer">
					<i class="fa fa-trash"></i>
				</button>
			<?php } ?>
			<?php /*
			if (isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth) { ?>
				<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>">
					<i class="fa fa-pencil"></i>
				</button>
			<?php }*/ ?>
		</div>
		
		<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
		<?php if($userId && Authorisation::isElementMember($costum["contextId"], $costum["contextType"], $userId)){ ?>  
		<div class="banner-outer ">
				<div class="basic-banner">
					<div class="basic-banner-inner">
						<div class="container-banner max-950"> 
							<div class="basic-info text-center">
								<div class="title-banner">
									<h1 class="font-weight-100"><?php echo $element["name"]??"" ?></h1>
									<p>
										Posté par : 
										<?php foreach ($element["parent"] as $key => $value) {
											echo $value["name"];
										} ?>
									</p>
									<p>
										Date de publication : 
										<?php 
											$datePublish = date( "d/m/Y", $element["created"]);
											echo $datePublish; 
										?>
									</p>
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div>

		<!-- Description -->
		<div class="title-section content-desc">
			<h3>Description</h3>
		</div>
		<div class="media content-desc padding-left-20" style="margin-top: 20px"> 
			<?php if(isset($element["description"])){ ?>
				<div id="description-preview" class="description-preview col-xs-12 no-padding activeMarkdown"><?php echo $element["description"] ?></div>
			<?php } ?>
			<?php if(isset($element["text"])){ ?>
				<div id="text-preview" class="description-preview col-xs-12 no-padding activeMarkdown"><?php echo $element["text"] ?></div>
			<?php } ?>
		</div>

		<!-- Caractéristique -->
		<?php if(isset($element["toolType"]) || isset($element["toolLevel"]) || isset($element["mainThematic"]) || isset($element["ethicCharte"])){ ?>
			<div class="title-section content-desc">
				<h3>Caractéristique</h3>
			</div>
			<div class="media content-desc padding-left-20 " style="margin-top: 20px">
				<?php echo (isset($element["mainThematic"])?'<span class="btn-ekitia-primary"><b>Thématique : </b>'.(is_string($element["mainThematic"])?$element["mainThematic"]:implode(",",$element["mainThematic"])).'</span>':"") ?><br>
				<?php echo (isset($element["toolType"])?'<span class="btn-ekitia-primary"><b>Type/Format : </b>'.$element["toolType"].'</span>':"") ?><br>
				<?php echo (isset($element["toolLevel"])?'<span class="btn-ekitia-primary"><b>Niveau : </b>'.(is_string($element["toolLevel"])?$element["toolLevel"]:implode(",", $element["toolLevel"])).'</span>':"") ?><br>
				<?php echo (isset($element["ethicCharte"])?'<span class="btn-ekitia-primary"><b>Charte Ethique : </b>'.(is_string($element["ethicCharte"])?$element["ethicCharte"]:implode(",",$element["ethicCharte"])).'</span>':"") ?>	
			</div>
		<?php } ?>

		<!-- Links and médias -->	
		<?php if($element["type"]=="link" || isset($element["medias"])){ ?>
			<div class="title-section content-desc">
				<h3>Lien</h3>
			</div>
			<div class="media content-desc padding-left-20 " style="margin-top: 20px"> 
				<?php 
				if(isset($element["medias"])){ 
					foreach($element["medias"] as $v){ ?>
						<a href="<?= $v["content"]["url"] ?>" target="_blank" class="no-padding">
							<div class="col-md-10 no-padding">
								<div class="col-md-4" style="padding-left: 0;">
								<?php if($v["content"]["image"]==NULL){?>
										<img src="<?= Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg" ?>" class="img-responsive" alt="image">
									<?php }else{ ?>
										<img src="<?= $v["content"]["image"] ?>" class="img-responsive" alt="image">
									<?php } ?>
								</div>
								<div class="col-md-6 no-padding">
									<h5><?= $v["name"] ?></h5>
									<div><?= $v["description"] ?>...</div>
								</div>
							</div>
						</a>
					<?php }
				} ?>
			</div>
		<?php } ?>

		<!-- Mot clés -->
		<?php if(isset($element["tags"])){ ?>
			<div class="title-section section-title"  style="margin-top: 20px">
				<h3>Mot clés</h3>
			</div>
			<div class="media content-desc " style="margin-top: 20px">
				<div class="col-xs-12">
					<?php foreach($element["tags"] as $v){ 
						echo "<span class='badge tags-poi-preview margin-right-5'>#".$v."</span>";
					} ?>
				</div>
			</div>
		<?php } ?>

		<!-- Person to contact -->
		<?php if(
			isset($element["adminEmail"]) || 
			isset($element["adminFirstName"]) || 
			isset($element["adminLastName"]) ){ ?>
			<div class="title-section content-desc">
				<h3>Contact</h3>
			</div>
			<div class="media content-desc padding-left-20 " style="margin-top: 20px">
				<b>Responsable : </b><span><?php echo ($element["adminFirstName"]??"")." ".($element["adminLastName"]??"") ?></span><br/>
				<?php if(isset($element["adminEmail"])){ ?><b>Email : </b><span><?php echo $element["adminEmail"] ?></span> <?php } ?>
			</div>
		<?php } ?>


		<br/><br/><br/><br/>
		<?php if($element["type"] == "forum"){ ?>
			<div id="commentElement-preview" class="col-xs-12 no-padding"></div>
		<?php }?>
		<?php }else{ ?>
			<div class="text-center text-danger margin-top-35">
				<i class="fa fa-lock fa-4x "></i>
				<h1>Accès Non Autorisé</h1>
				<p>Il faut être connecté et membre de la plateforme</p>
			</div>
		<?php } ?>
		<br/><br/><br/>
	</div>
</div>

<script>
	var eltPreview = <?php echo json_encode($element); ?>;
	$("#description-preview").html(dataHelper.markdownToHtml(eltPreview.description))
	$("#text-preview").html(dataHelper.markdownToHtml(eltPreview.text))

	$(".btn-delete-tool-ekitia").off().on("click", function () {
		$("#modal-preview-coop").hide(300);
		$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
		directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), null);
	});

	$(".btn-edit-tool-ekitia, .btn-edit-element").off().on("click", function () {
		$("#modal-preview-coop").hide(300);
		$("#modal-preview-coop").html("");
		let thisEl = <?php echo json_encode($element); ?>;
		dyFObj.editMode = true;
		uploadObj.update = true;
		dyFObj.currentElement={type : thisEl.collection, id : $(this).data("id")};
		
		dyFObj.openForm("tools", null, 
			{...thisEl, id:$(this).data("id")}
		);
	});
</script>
<?php }else{ ?>
	<script>
		$("#modal-preview-coop").hide();
	</script>
<?php } ?>