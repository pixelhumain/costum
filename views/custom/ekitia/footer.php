<footer class="text-center col-xs-12 padding-30" style="background-color: #242424;">
    <div class="container" style="color: white !important; font-size: 14pt">
        <div class="row">
            <div class="col-md-4">
                <img width="120" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/costumize/ekitia/EKITIA_BS_logo_RVB_2000.png" alt="logo d'EKITIA" class="margin-bottom-10">
                <img width="120" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/costumize/ekitia/La région occitanie.png" alt="logo de la région occitanie" class="margin-bottom-10">
                <div style="font-size: 12pt">Développée avec le soutien financier de la Région Occitanie</div>
            </div>
            <div class="col-md-4">
                <div class="margin-bottom-20"><a href="#mentions" class="lbh"  style="color:white">CGU</a></div>
                <div class="margin-bottom-20"><a href="#confidentialityRules" class="lbh"  style="color:white">Politique de confidentialité</a></div>
                <div><a href="https://chat.communecter.org/group/ssC49mPgERsKoFeTf" target="_blank"  style="color:white">Déclarer un bug ou incident</a></div>
            </div>
            <div class="col-md-4">
                <a href="https://www.linkedin.com/company/ekitia/" target="_blank" class="padding-5 text-white" style="background:#0A66C2;border-radius:2px"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</footer>
