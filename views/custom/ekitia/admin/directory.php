<style>
    #filterContainer #input-sec-search .input-global-search, .searchBar-filters .search-bar{
        border-color: #ba1c24;
        border-left-width: 1px;
    }
    .searchBar-filters .main-search-bar-addon{
        border-color: #ba1c24;
        background-color: #ba1c24 !important;
        padding: 12px 15px 12px 10px;
    }
    #filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn{
        border-color: #ba1c24;
    }
    #filterContainer .dropdown .btn-menu i{
        color: #ba1c24;
    }
    .searchObjCSS #input-sec-search .input-global-search{
        border-color: #ba1c24;
    }
    .searchObjCSS #input-sec-search .input-group-addon{
        border-color: #ba1c24;
        background-color: #ba1c24 !important;
        padding: 10px 10px 10px 5px;
        color: white;
    }
    #paginateDom .simple-pagination li i{
        margin-right: 10px;
        margin-left: 10px;
        font-size: 24px;
    }
    #paginateDom .simple-pagination li.disabled{
         display: none;
    }
    #paginateDom .simple-pagination li > a,#paginateDom .simple-pagination li > span{
        padding: 10px 20px;
        display: flex;
        align-items: center;
        text-transform: capitalize;
        font-size: 18px !important;
    }
    #paginateDom .simple-pagination li.active > a,#paginateDom .simple-pagination li.active > span{
        padding: 10px 15px;
        background: #ba1c24 !important;
        color: white!important;
        border: 2px solid #ba1c24;
        border-radius: 100%;
        display: flex;
        align-items: center;
        text-transform: capitalize;
        font-size: 18px !important;
    }
    .ekitia-directory .ekitia-directory-item{
        display: flex;
        align-items: center;
        margin: 10px 0 20px;
        box-shadow: 0 2px 20px 2px rgb(0 0 0 / 50%);

    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-logo{
        padding: 10px;
        width: 120px;
        margin: auto;
        display: block;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-name{
        font-size: 16px;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-address{
        font-size: 14px;
    }

    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-btn{
        padding: 8px 20px;
        background: #ba1c24;
        color: white;
        font-size: 14px;
        border: 2px solid transparent;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-btn i{
        font-size: 24px;
        margin-right: 10px;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-btn:hover{
        color: #ba1c24;
        border: 2px solid #ba1c24;
        background: transparent;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-tags span{
        margin-right: 5px;
    }
    .ekitia-directory .ekitia-directory-item .ekitia-directory-item-info {
        padding-bottom: 20px;
    }
</style>
<div id="adminDirectory" class="col-xs-12 no-padding searchObjCSS"></div>
<div class="col-md-12 col-sm-12 col-xs-12 bg-white shadow" id="paginateDom">
</div>
<div class="col-md-12 col-sm-12 col-xs-12 bg-white shadow ekitia-directory" id="search-content" style="min-height:700px;">
</div>
<script type="text/javascript">
    var contextElt = <?php echo json_encode(@$context)  ?>;
    var panelAdmin = <?php echo json_encode($panelAdmin) ?>;
    mylog.log("panelAdmin",panelAdmin);
    var filterAdmin = {};
    var paramsFilterAdmin= {};

    jQuery(document).ready(function() {
        adminDirectory.actions.addReference = function(e, id, type, aObj){
            var str='<button data-id="'+id+'" data-type="'+type+'" data-action="add" data-setkey="reference" class="margin-right-5 setSourceAdmin ekitia-directory-item-btn"><i class="fa fa-plus"></i><span>Ajouter comme membre d\'Ekitia</span></button>';
            return str;
        }
        adminDirectory.actions.removeSource = function(e, id, type, aObj){
            var str='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="source" class="margin-right-5 setSourceAdmin ekitia-directory-item-btn"><i class="fa fa-ban"></i> <span>Enlever la déclaration "Créer sur Ekitia"</span></button>';
            return str;
        }
        adminDirectory.actions.removeReference = function(e, id, type, aObj){
            var str='<button data-id="'+id+'" data-type="'+type+'" data-action="remove" data-setkey="reference" class="margin-right-5 setSourceAdmin ekitia-directory-item-btn"><i class="fa fa-ban"></i> <span>Enlever d\'Ekitia</span></button> ';
            return str;
        }
        directory.ekitiaPanel = function (params) {
            var action = "";
            $.each(panelAdmin.actions, function (key, value) {
                action += adminDirectory.actions[key](params, params._id.$id, params.collection);
            });

            return `<div class="row ekitia-directory-item" id="${params.collection+params._id.$id}">
                <div class="col-md-9 col-sm-9 row col-xs-12">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <img src="${params.profilMediumImageUrl}" class="ekitia-directory-item-logo" alt="Logo">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 ekitia-directory-item-info">
                        <h3 class="ekitia-directory-item-name">${adminDirectory.values.name(params, params._id.$id, params.collection,null, {preview: true})}</h3>
                        <p class="ekitia-directory-item-address">${adminDirectory.values.address(params)}</p>
                        ${adminDirectory.values.tags(params, params._id.$id, params.collection, null, "ekitia-directory-item-tags")}
                    </div>
                </div>
                <div class="col-md-3  col-sm-3 col-xs-12">
                    ${action}
                </div>
            </div>`;
        }
        paramsFilterAdmin = {
            footerDom: "#paginateDom",
            container : "#adminDirectory",
            // loadEvent : {
            // 	default : "scroll"
            // },
            results: {
                dom: "#search-content",
                renderView: "directory.ekitiaPanel",
                afterRenderingEvent: function(){
                    $.each(panelAdmin.actions, function (key, value) {
                        if(typeof value.event != "undefined" && adminDirectory.events[value.event] != "undefined")
                            adminDirectory.events[value.event](adminDirectory);
                        else if(typeof adminDirectory.events[key] != "undefined")
                            adminDirectory.events[key](adminDirectory);
                    });
                }
            },
            defaults : panelAdmin.paramsFilter.defaults,
            filters: panelAdmin.paramsFilter.filters,
            urlData : baseUrl+"/co2/search/globalautocompleteadmin"
        }
        // Configure searchObj for admin and construct central table
        filterAdmin = searchObj.init(paramsFilterAdmin);

        // Search results for admin initialisation
        var myResult = filterAdmin.search.init(filterAdmin);

    });
</script>
