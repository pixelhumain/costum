<?php
//var_dump("exit"); exit;
$cssAnsScriptFilesModule = array(
    '/js/admin/panel.js',
    '/js/admin/admin_directory.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
$adminConstruct = $this->appConfig["adminPanel"];
if (isset($this->costum)) {
    $slugContext = isset($this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"];
    $cssJsCostum = array();
    if (isset($adminConstruct["js"])) {
        $jsCostum = ($adminConstruct["js"] === true) ? "admin.js" : $adminConstruct["js"];
        array_push($cssJsCostum, '/js/' . $slugContext . '/' . $jsCostum);
    }
    array_push($cssJsCostum, '/css/ekitia/admin.css');
    array_push($cssJsCostum, '/js/costumize/ekitia/admin.js');
    if (!empty($cssJsCostum))
        HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule("costum")->getAssetsUrl());
}
$logo = (@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl . "/assets/img/LOGOS/CO2/logo-min.png";
?>
<style>
    #navigationAdmin {
        padding-top: 30px;
    }
</style>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" id="ekitia-admin-panel">
    <div class="row">
        <div class="ekitia-menu-xs <?= @$_GET['view'] != "" ? "menu-top" : "menu-left" ?>">
            <i class="fa fa-list-alt"></i> Menu d'administration
        </div>
        <?php if ($authorizedAdmin || Role::isSourceAdmin(Role::getRolesUserId(Yii::app()->session["userId"]))) {
            $title = (@Yii::app()->session["userIsAdmin"]) ? Yii::t("common", "Administration portal") : Yii::t("common", "Public administration portal"); ?>
            <div class="col-sm-3 col-xs-12 ekitia-admin-menu">
                <ul class="ekitia-admin-menu-lists">
                    <?php
                    if (!empty($menu)) {
                        foreach ($adminConstruct["menu"] as $key => $v) {
                            $show = (isset($v["show"])) ? $v["show"] : true;
                            $show = (isset($v["costumAdmin"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                            $show = (isset($v["super"]) && !Authorisation::isInterfaceAdmin()) ? false : $show;
                            if ($show) {
                                $dataAttr = "";
                                $dataAttr = (isset($v["dataHref"])) ? "data-href='" . $v["dataHref"] . "' " : "";
                                $dataAttr .= (isset($v["view"]) && !empty($v["view"])) ? "data-view='" . $v["view"] . "' " : "";
                                $dataAttr .= (isset($v["action"]) && !empty($v["action"])) ? "data-action='" . $v["action"] . "' " : "";
                    ?>
                                <li class="ekitia-admin-menu-item <?= @$v["view"] == $view ? "active" :  "" ?>">
                                    <a href="javascript:;" class="btnNavAdmin" id="<?php echo @$v["id"] ?>" <?php echo $dataAttr; ?> style="cursor:pointer;">
                                        <i class="fa fa-<?php echo @$v["icon"] ?> fa-2x"></i>
                                        <?php echo Yii::t("admin", @$v["label"]); ?>
                                    </a>
                                </li>
                    <?php
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-xs-12" id="content-admin-panel">
                <div id="navigationAdmin">
                    <div class="col-sm-12 col-xs-12 text-center ekitia-admin-title">
                        <!-- <img src="<?php echo $logo ?>" height="100"><br/> -->
                        <h3><?php echo $title ?></h3>
                    </div>
                    <?php if (isset($adminConstruct["add"]) && $adminConstruct["add"]) { ?>

                        <div class="col-xs-12 text-center col-sm-offset-1 col-sm-10 bg-white ">

                            <h5 class="text-center margin-top-15 infoPanelAddContent">
                                <i class="fa fa-plus-circle ekitia-add-icon"></i> Créer des nouveaux éléments pour votre communauté
                            </h5>
                            <button data-type="organizations" class="btn btn-primary edit-dynform margin-bottom-15 text-uppercase">Editer le formulaire de structure</button>
                            <button data-type="tools" class="btn btn-primary edit-dynform margin-bottom-15 text-uppercase">Editer le formulaire de ressource</button>
                            <!--button class="btn btn-primary btnEditMegaMenu margin-bottom-15 text-uppercase">Gestion des listes</button-->
                            <div class="col-md-12 col-sm-12 col-xs-12 contain-admin-add">
                                <hr>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a href="#@<?php echo @$this->costum["slug"] ?>.view.forms" class="btn btn-link ekitia-btn-link lbh col-xs-12 col-sm-6 col-md-4 col-lg-4 margin-bottom-10">
                                    <h6><i class="fa fa-wpforms"></i><br>FORMULAIRE</h6>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="goBackToHome">
                    <a href="javascript:;" class="btnNavAdmin col-md-12 col-sm-12 col-xs-12 padding-20 text-center" data-view="index" style="font-size:20px;"><i class="fa fa-home"></i> <?php echo Yii::t("common", "Back to admin home") ?></a>
                </div>
                <div id="content-view-admin"></div>
            </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    var superAdmin = "<?php echo @Yii::app()->session["userIsAdmin"] ?>";
    var sourceAdmin = "<?php echo @Yii::app()->session["userIsAdminPublic"] ?>";
    var authorizedAdmin = <?php echo json_encode(@$authorizedAdmin) ?>;
    var edit = true;
    var paramsAdmin = <?php echo json_encode($adminConstruct) ?>;
    var subView = <?php echo (isset($_POST["subView"])) ? json_encode(true) : json_encode(false); ?>;
    adminPanel.params = {
        hashUrl: "#admin",
        view: "<?php echo @$_GET['view']; ?>",
        action: null,
        dir: "<?php echo @$_GET['dir']; ?>",
        subView: false
    };
    if (notEmpty(subView)) {
        adminPanel.params.view = "<?php echo @$_GET['subview']; ?>";
        adminPanel.params.subView = true;
    }
    jQuery(document).ready(function() {
        mylog.log("render", "--- co2/views/admin/index.php", subView, authorizedAdmin, !authorizedAdmin);
        if (!authorizedAdmin) {
            //urlCtrl.loadByHash("");
            bootbox.dialog({
                message: '<div class="alert-danger text-center"><strong><?php echo Yii::t("common", "You are not authorized to acces adminastrator panel ! <br/>Connect you or contact us in order to become admin system") ?></strong></div>'
            });
        }
        adminPanel.init();
        $(".ekitia-menu-xs").off().on('click', function() {
            $(".ekitia-admin-menu").toggleClass("active");
        })
    });

    var dynFormEditor = {
        init: function() {

        },
        events: function() {

        }
    }

    $(".edit-dynform").on("click", function() {
        var type = $(this).data("type")
        var inputs = {};
        var properties = costum.typeObj[type].dynFormCostum.beforeBuild.properties;

        $.each(properties, function(index, props) {
            let pref ="";
            if(index=="description"){
                pref="p";
            }

            if(props.label){
                inputs[index+pref] = {
                    "label": "Label : ",
                    "value": props.label,
                    "inputType": "text"
                };
            }
            
            if (props.placeholder) {
                inputs[index+pref+"Placeholder"] = {
                    "label": "Placeholder : ",
                    "value": props.placeholder,
                    "inputType": "text"
                }
            }
        })

        sectionDyf.dynformEdit = {
            "jsonSchema": {
                "title": "MODIFICATION DU FORMULAIRE : "+(costum.typeObj[type].name||costum.typeObj[type].addLabel||""),
                "description": "",
                "icon": "fa-cog",
                "properties": {
                    "formTitle": {
                        "label": "Titre du formulaire",
                        "value": costum.typeObj[type]?.dynFormCostum?.onload?.actions?.setTitle||""
                    },
                    "formAddLabel": {
                        "label": "Nom de l'élement",
                        "value": costum.typeObj[type].addLabel
                    },
                    ...inputs
                },
                afterBuild: function() {
                    alignInput2(sectionDyf.dynformEdit.jsonSchema.properties ,"form",6,12,0,null,"Généralité","#777","");
                    $.each(sectionDyf.dynformEdit.jsonSchema.properties, function(index, prop) {
                        if(index.indexOf("Placeholder")==-1 && prop.inputType!="hidden" && index.indexOf("form")==-1){
                            alignInput2(sectionDyf.dynformEdit.jsonSchema.properties, index, 6, 12, 0, null, prop.value, "#777", "");
                        }
                    });
                },
                save: function(data) {
                    tplCtx.id = costum.contextId
                    tplCtx.collection = costum.contextType;
                    tplCtx.updateCache = true;
                    tplCtx.removeCache = true;
                    tplCtx.path = "allToRoot";
                    tplCtx.value = {};
                    const dynProps = sectionDyf.dynformEdit.jsonSchema.properties;
                    $.each(dynProps, function(k, val) {
                        if($("#" + k).val()!="" && dynProps[k].label!=$("#" + k).val() && k.indexOf("Placeholder")==-1 ){
                            if (k == "formTitle") {
                                tplCtx.value["costum.typeObj."+type+".dynFormCostum.onload.actions.setTitle"] = $("#" + k).val();
                            }else if(k=="formAddLabel"){
                                tplCtx.value["costum.typeObj."+type+".addLabel"] = $("#" + k).val();
                            }else if(k=="descriptionp"){
                                tplCtx.value["costum.typeObj."+type+".dynFormCostum.beforeBuild.properties.description.label"] = $("#" + k).val();
                                tplCtx.value["costum.typeObj."+type+".dynFormCostum.beforeBuild.properties.description.placeholder"] = $("#"+k+"Placeholder").val();
                            }else{
                                tplCtx.value["costum.typeObj."+type+".dynFormCostum.beforeBuild.properties."+k+".label"] = $("#" + k).val();
                                tplCtx.value["costum.typeObj."+type+".dynFormCostum.beforeBuild.properties."+k+".placeholder"] = $("#"+k+"Placeholder").val();
                            }
                        }
                    });
                    
                    dataHelper.path2Value(tplCtx, function(params) {
                        toastr.success("modification enregistré");
                        location.reload()
                    });
                }
            }
        };
        dyFObj.openForm(sectionDyf.dynformEdit, null, properties);
    })


    /** Themes Editor
    var themesEditor = {
        dynForm : {},
        queryParams:{
            value:{}
        },
        macroThemeLength:0,
        init:function(te){
            te.initEvents(te);
        },
        openDynForm:function(te, btn){
            te.dynForm["costumThemesParamsData"] = {};
            te.dynForm["costumThemesParams"] = {
                "jsonSchema" : {
                    "title" : "Editeur de thèmes",
                    "description" : "",
                    "icon" : "fa-cog",
                    "noSubmitBtns":true,
                    "properties" : {
                        themes : {
                            "inputType" : "custom",
                            "html" : te.generateEditor(te)
                        }
                    }
                }
            }
            te.queryParams.id = btn.data("id");
            te.queryParams.collection = btn.data("collection");
            dyFObj.openForm( te.dynForm.costumThemesParams, null, {})
        },
        generateEditor:function(te){
            var themesEditorHTML = "<div class='row equal text-left'>";
            for (var macroTm in costum.lists.theme) {
                te.queryParams.value[macroTm] = {}
                themesEditorHTML+="<div class='col-md-4 padding-5 macroTm"+te.macroThemeLength+"'>";
                themesEditorHTML+="<div contenteditable class='textMicroTheme bg-blue padding-5 fs-14pt text-bolder text-white' data-path='costum.lists.theme' data-old='"+macroTm+"'>"+macroTm+"</div>"
                for (var microTm in costum.lists.theme[macroTm]) {
                    if (costum.lists.theme[macroTm].hasOwnProperty(microTm)) {
                        themesEditorHTML+="<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme'  data-macrotm='"+macroTm+"' data-oldvalue='"+microTm+"' style='border:1px dashed #eee'>"+microTm+"</div>"
                        te.queryParams.value[macroTm][microTm] = microTm;
                    }
                }
                themesEditorHTML+="<a class='btn btn-sm btn-success btnNewTm padding-right-20 padding-left-20' data-index='macroTm"+te.macroThemeLength+"' data-theme='"+macroTm+"'><i class='fa fa-plus'></i></a>"
                themesEditorHTML+="</div>";
                te.macroThemeLength ++;
            }
            themesEditorHTML+="<a href='javascript:;' class='col-md-4 padding-5 text-info text-center newColumn' data-index='"+te.macroThemeLength+"' style='font-size:40pt; border-radius:5px; border:1px dashed #eee'>+</a>";
            themesEditorHTML+="</div>";
            return themesEditorHTML;
        },
        initEvents:function(te){
            $(document).on("click", ".mega-menu-dropdown", function(e){
                if($(".btnEditMegaMenu").length==0 && userId && typeof costum.admins[userId] != "undefined" && costum.admins[userId].isAdmin==true){
                    $(".mega-menu-container").prepend("<button class='btn bg-dark text-white btn-sm padding-right-20 padding-left-20 btnEditMegaMenu pull-right' data-id='"+costum.contextId+"' data-collection='"+costum.contextType+"'>"+trad.edit+"</button>");
                }
            });
            $(document).on("click", ".btnEditMegaMenu", function(e){
                te.openDynForm(te, $(this));
            });
            $(document).on("blur", ".textMicroTheme", function(e){
                te.helpers.saveChanges(te, $(this))
            });
            $(document).on("click", ".btnNewTm", function(e){
                $("<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme' data-macrotm='"+$(this).data("theme")+"' style='border:1px dashed #eee'></div>").insertBefore($(this));
            });
            $(document).on("click", ".newColumn", function(e){
                $(te.helpers.macroThemeHTML(te, $(this))).insertBefore($(this));
            });
            $(document).on("click", "#deleteTheme", function(e){

            })
        },
        helpers:{
            macroThemeHTML:function(te, element){
                //let te.macroThemeLength = element.data("index");
                let newColumn = "";
                te.queryParams["Macro theme "+te.macroThemeLength] = {};
                te.queryParams["Macro theme "+te.macroThemeLength]["New micro theme"] = "New micro theme";

                newColumn += "<div class='col-md-4 padding-5 macroTm"+te.macroThemeLength+"'>";
                newColumn += "<div contenteditable class='textMicroTheme bg-blue padding-5 fs-14pt text-bolder text-white' data-path='costum.lists.theme' data-old='Macro theme "+te.macroThemeLength+"'>Macro theme "+te.macroThemeLength+"</div>";
                newColumn += "<div contenteditable class='textMicroTheme padding-5' data-path='costum.lists.theme'  data-macrotm='Macro theme "+te.macroThemeLength+"' data-oldvalue='micro theme "+te.macroThemeLength+"' style='border:1px dashed #eee'>micro theme "+te.macroThemeLength+"</div>";
                newColumn += "<a class='btn btn-sm btn-success btnNewTm padding-right-20 padding-left-20' data-index='macroTm"+te.macroThemeLength+"' data-theme='Macro theme "+te.macroThemeLength+"'><i class='fa fa-plus'></i></a>";
                newColumn += "</div>";
                te.macroThemeLength++;
                return newColumn;
            },

            saveChanges:function(te, element){
                let macroTheme = element.data("macrotm");
                let theme = element.text();
                te.queryParams.path = element.data("path");

                if(theme!=""){
                    if(typeof te.queryParams.value == "undefined"){
                        te.queryParams.value = {};
                    }
                    if(typeof macroTheme!="undefined" && typeof te.queryParams.value[macroTheme]=="undefined"){
                        te.queryParams.value[macroTheme] = {};
                    }
                    if(typeof macroTheme == "undefined"){
                        te.queryParams.value[theme] = te.queryParams.value[element.data("old")];
                        if(typeof te.queryParams.value[element.data("old")]=="undefined"){
                            te.queryParams.value[theme] = {};
                        }
                        if(theme!=element.data("old")){
                            $("[data-macrotm='"+element.data('old')+"']").each(function(i){
                                $(this).data("macrotm", theme);
                            });
                            delete te.queryParams.value[element.data("old")];
                        }
                        element.data("old", theme);
                    }else{
                        te.queryParams.value[macroTheme][theme] = theme;
                    }
                }else{
                    if(typeof macroTheme == "undefined"){
                        bootbox.dialog({
                            message:'<div class="alert-white text-center"><br><strong><?php echo Yii::t("common","Voulez vous vraiement supprimer cette macro thème, ça va supprimer les micro thèmes correspondants") ?></strong><br><br></div>',
                            buttons: {
                        cancel: {
                            label: "Non, Annuler",
                            className: 'btn-danger',
                        },
                        ok: {
                            label: "Je confirme",
                            className: 'btn-info',
                            callback: function(){
                                            delete te.queryParams.value[element.data("old")];
                                            te.helpers.update(te, "deletion successfully");
                                            element.parent().remove();
                            }
                        }
                    }
                    });
                    }else{
                        delete te.queryParams.value[macroTheme][element.data("oldvalue")];
                    }
                }

                if(typeof te.queryParams.value == undefined){
                    toastr.error('value cannot be empty!');
                }else if(typeof macroThem=="undefined"){
                    te.helpers.update(te, "Update successfully");
                }
            },

            update:function(te, message){
                //alert(JSON.stringify(te.queryParams));
                //te.queryParams.updatePartial=true;
                te.queryParams.removeCache=true;
                //te.queryParams.format=true;
                dataHelper.path2Value(te.queryParams, function(data) {
                    if(data.result){
                        costum.lists.theme = data.elt.costum.lists.theme;
                        toastr.success(message);
                    }
                });
            }
        }
    }

    themesEditor.init(themesEditor);*/
</script>