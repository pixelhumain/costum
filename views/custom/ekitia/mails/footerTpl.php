<?php 
  $urlRedirect=(@$url && !empty($url)) ? $url : Yii::app()->getRequest()->getBaseUrl(true) ;
?>
<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
    <tr style="padding: 0;vertical-align: top;text-align: left;">
        <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 15px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                
                <tr style="padding: 0;vertical-align: top;text-align: left;">
                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                        <p style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px;"><?php echo Yii::t("mail","See you soon on {what}",array("{what}"=>'<a href="'.(isset($baseUrl)?$baseUrl:$urlRedirect).'" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;">'.(parse_url(isset($baseUrl)?$baseUrl:$urlRedirect, PHP_URL_HOST)).'</a>')) ?>, </p>
                        <p style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px;">L'équipe d'EKISPHERE.</p>
                    </th>
                </tr>
            </table>
        </th>
    </tr>

	<tr style="padding: 0;vertical-align: top;text-align: left;">
		<td style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 15px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
			<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;margin-top: 10px">
				
				<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px; font-style: italic;">
								<hr>
							</p>
						</th>
					</tr>
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px; font-style: italic;">
								<?php 
									$here = '<a href="'.$urlRedirect.'/#settings.redirect">'.Yii::t("mail","here").'</a>';
									echo Yii::t("mail","You can manage your notifications {here}", array("{here}" => $here) ) ; 
								?>
							</p>
						</th>
					</tr>
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px; font-style: italic;">
								<?php 
									$here = '<a href='.$urlRedirect.'/#element.askdata>'.Yii::t("common","this address").'</a>';
									echo Yii::t("common","If you are not the originator of this email. You can check your data and manage the sending of your email to {thisaddress}", array("{thisaddress}" => $here) ) ; 
								?>
							</p>
						</th>
					</tr>
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0px;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 15px; font-style: italic;">
								<hr>
							</p>
						</th>
					</tr>
			</table>
		</td>
	</tr>

</table>
   <!-- End main email content -->

 <table class="container text-center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: center;background: #fefefe;width: 580px;margin: 0 auto;">
 	<tbody>
 		<tr style="padding: 0;vertical-align: top;text-align: left;">
 			<td style="word-wrap: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 15px;border-collapse: collapse !important;"> <!-- Footer -->
			    <table class="row grey" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: #f0f0f0;width: 100%;position: relative;display: table;">
			    	<tbody>
			    		<tr style="padding: 0;vertical-align: top;text-align: left;">
			    			<th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
							<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
								<tr style="padding: 0;vertical-align: top;text-align: left;">
									<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
										<p class="text-center footercopy" style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 20px 0px;text-align: center;line-height: 19px;font-size: 12px;font-style: italic;"><?php echo Yii::t("mail","Mail send from") ?> <?php echo $urlRedirect; ?></p>
									</th>
									<th class="expander" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0 !important;margin: 0;text-align: left;line-height: 19px;font-size: 15px;visibility: hidden;width: 0;"></th>
								</tr>
							</table>
				        	</th>
				        </tr>
				    </tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
