<style type="text/css">
	.blockFontPreview {
		font-size: 14px;
		color: #ec5d85;
	}

	h2 {
		font-size: unset;
	}

	#description{
		text-align: justify;
  		text-justify: inter-word;
	}

	#socialNetwork {
		font-size: 20px;
	}

	div.community-text {
		font-size: 21px;
    	margin-top: 12vh;
		text-align: justify;
	}

	.media-body {
		width: auto !important;
	}

	.m-title-desc {
		margin: auto 8vh;
		margin-top: 1em;
	}

	.mx-1 {
		margin: auto 1vh;
	}

	.space-top-prev {
		margin-top: 8vh;
	}

	.faw-size {
		font-size: xx-large;
	}

	.flex-disp, #socialNetwork {
		display: flex;
		justify-content: center;
	}

	.font-21 {
		font-size: 21px;
	}

	.btn-network {
		display: inline-block;
		width: 50px;
		height: 50px;
		background: #fff/*#f1f1f1*/;
		margin: 5px;
		border-radius: 30%;
		color: #1da1f2;
		overflow: hidden;
		position: relative;
		border: 1px solid #1d3b58;
	}

	.btn-network i {
		line-height: 50px;
		font-size: 25px;
		transition: 0.2s linear;
	}


	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-top: 80px;
		padding-bottom: 100px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.event-banner{
		padding-top: 20px;
		padding-bottom: 20px;
		background-color: var(--primary-color);
		color:white;
	}

	.bg-ekitia{
		background-color: var(--primary-color);
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
		.community-text img {
			 position: relative;
			 border-radius: 5px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	.title-banner h1::before {
		content: "";
		position: absolute;
		/* left: 37%; */
		left: 40%;
		top: 100%;
		/* bottom: 50%; */
		z-index: -1;
		margin-left: -1em;
		width: 6em;
		height: 6em;
		background-image: url("<?php echo (!empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/".($type!="citoyens"?"thumbnail-default.jpg":"thumb/default_citoyens.png")) ?>");
		background-repeat: no-repeat;
		background-color: transparent;
		background-position-x: center;
		background-position-y: center;
		background-size: contain;
		border-radius: 0%;
	}

	.title-section h3::before {
		content: "";
		position: absolute;
		top: -13px;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.title-section {
		position: relative;
	}

	.font-weight-100 {
		font-weight: 100;
	}

	.title-banner {
		position: sticky;
	}

	.m-title-contact {
		margin: auto 8vh;
    	margin-top: 2vh;
	}

	.dv-contact-content {
		margin-top: 3vh;
		margin-bottom: 10vh;
	}
    .wrap-ekitia-btn-inscrire{
        margin-right: 20px;
        margin-top: 20px;
    }
    .wrap-ekitia-btn-inscrire .ekitia-btn-inscrire{
        background: #ba1c24;
        color: white;
        padding-top: 8px;
        padding-left: 40px;
        padding-right: 40px;
        padding-bottom: 8px;
        font-size: 18px;
        border: 2px solid transparent;
    }
    .wrap-ekitia-btn-inscrire .ekitia-btn-inscrire:hover{
        color: #ba1c24;
        border: 2px solid #ba1c24;
        background: transparent;
    }

    @media (max-width: 628px) {
        .media-body {
            width: auto !important;
            display: flex;
            margin-top: 20px
        }

        .media-object {
            margin: auto 20%;
            width: 200px !important;
            height: 200px !important;
        }

        .media {
            margin: 4vh 8vh auto;
        }

        p.community-text {
            font-size: initial;
            margin-top: 6vh;
        }

        .media-left {
            display: inherit !important;
        }

        .space-top-prev:first-child {
            margin-top: 8vh;
        }
        .space-top-prev {
            margin-top: 2vh;
        }
        .contact-title-content{
            margin-bottom: 10vh;
        }
        .title-banner h1::before{
            height: 5em;
            width: 5em;
            left: 45%;
        }
        .m-title-contact {
            margin: 2vh 4vh auto;
        }
    }
    @media screen and (max-width: 567px){
        .media {
            margin: 12vh 4vh auto;
        }
        .m-title-desc {
            margin: 14vh 4vh auto;
        }
    }

	.prev-description{
		word-wrap: break-word !important;
		white-space: pre-wrap;
	}
	.filters-activate{
		display: none !important;
	}
</style>

<?php
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig = @$this->appConfig["preview"];
$auth = (Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

$iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
$descriptionPrev = (isset($element["description"])) ? $element["description"]: (isset($element["shortDescription"]) ? $element["shortDescription"] : "<i>Il n'y a pas de description pour le moment.</i>") ;
$descriptionPrev = preg_replace('/\*\*(.*?)\*\*/', '<strong>$1</strong>', $descriptionPrev);
$descriptionPrev = preg_replace('/###(.*?)/', '<h2>$1</h2>', $descriptionPrev);
$descriptionPrev = preg_replace('/_(.*?)_/', '<em>$1</em>', $descriptionPrev);
$phone = (isset($element["telephone"]["fixe"][0])) ? ((isset($element["telephone"]["mobile"][0])) ? $element["telephone"]["mobile"][0] : $element["telephone"]["fixe"][0]) : "";
$userId = Yii::app()->session["userId"]??null;

if(isset($element["telephone"]) && is_string($element["telephone"])){
	$phone=$element["telephone"];
}

$isAdmin = (isset($_SESSION["userId"]))?Authorisation::isElementAdmin($element["_id"], $element["collection"], $_SESSION["userId"], false, false) || Authorisation::isUserSuperAdmin($_SESSION["userId"]):false;

if(!isset($costum)){
	$costum = CacheHelper::getCostum();
}

?>
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10 <?= ($element["collection"]=="events")?"bg-ekitia":"" ?>">
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
		<?php if(($element["collection"]=="events") && (Authorisation::canEdit($userId, (string)$element["_id"], "events")|| Authorisation::isCostumAdmin())){ ?>
			<button class="btn btn-primary pull-right margin-right-10 btn-edit-element-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Modifier">
				<i class="fa fa-pencil"></i>
			</button>
			<button class="btn btn-danger pull-right margin-right-10 btn-delete-element-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Supprimer">
				<i class="fa fa-trash"></i>
			</button>
		<?php } ?>

		<?php if(($element["collection"]=="citoyens") && $userId==(string)$element["_id"]){ ?>
			<button class="btn btn-primary btn-update-user-info pull-right margin-right-10" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" title="Modifier">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>

		<?php if($element["collection"]!="citoyens" && $element["collection"]!="events"){ ?>
			<a href="#@<?php echo @$element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
		<?php } ?>
		<?php
		if (isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth) { ?>
			<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>">
				<i class="fa fa-pencil"></i>
			</button>
		<?php } ?>
	</div>
	<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
		<?php if(Yii::app()->session["userId"] && Authorisation::isElementMember($costum["contextId"], $costum["contextType"], Yii::app()->session["userId"])){ ?>       
	
		<div class="banner-outer ">
            <div class="<?= ($element["collection"]=="events")?"event-banner":"basic-banner" ?>">
                <div class="basic-banner-inner">
                    <div class="container-banner max-950">
                        <div class="basic-info text-center">
                            <div class="<?= ($element["collection"]!="events")?"title-banner":"" ?>">
                                <h1 class="font-weight-100"><?php echo $element["name"] ?></h1>
								<!--p><?php /*if($element["collection"]=="events"){
									echo "<span style='background-color:var(--primary-color); color:white; display:inline-block; padding:5px 10px 0px 10px; border-radius: 20px'>".(Yii::t("category", ucfirst($element["type"])))."</span>"; //+ implode(",", $element["organizer"]);
									if(isset($element["organizer"]) && is_array($element["organizer"])){
										echo " partagé par ";
										foreach ($element["organizer"] as $key => $organizer) {
											echo $organizer["name"];
											if($organizer!=end($element["organizer"])){
												echo ", ";
											}
										}
									}
								}*/ ?></p -->
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(($element["collection"] == "organizations") && !(isset($element["preferences"]["toBeValidated"]["ekisphere"]) || (isset($element["reference"]["costum"]) && in_array("ekisphere", $element["reference"]["costum"])) || (isset($element["source"]["keys"]) && in_array("ekisphere", $element["source"]["keys"])))){ ?>
            <div class="text-right wrap-ekitia-btn-inscrire">
                <button class="ekitia-btn-inscrire">Rejoindre Ekitia</button>
            </div>
        <?php }?>

		<?php if($element["collection"]!="citoyens"){ ?>
			<div class="row community-text">
				<div class="<?= ($element["collection"]=="events")?"col-md-9 padding-right-0":"col-md-12" ?>">
					<div class="">
						<div class="title-section m-title-desc">
							<h3>
								<?php if(isset($element["startDate"])){ ?>
									<div id="eventDate"></div>
								<?php }else{
									echo "Description";
								} ?>
							</h3>
						</div>

						<div class="media m-title-desc">
							<div class="media-middle">
								<?php  if (isset($descriptionPrev) && !empty($descriptionPrev)) { ?>
									<p class="prev-description activeMarkdown"><?= preg_replace("/(http|https)?:\/\/([\w-]+\.)+[\w-]+(\/[\w\- .\/?%&=]*)?/", '<a href="$0" class="color-ekitia" target="_blank">$0</a> ', $descriptionPrev) ?></p>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<?php if($element["collection"]=="events"){ ?>
					<div class="col-md-3 no-padding">
						<div class="padding-right-20">
							<img class="img-responsive" src="<?php echo (!empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/".($type!="citoyens"?"thumbnail-default.jpg":"thumb/default_citoyens.png")) ?>" alt="image">
							<div class="margin-top-20">
								<?php if(isset($element["organizer"])){ ?>
									<h5>Organisateur(s) :</h5>
									<?php foreach($element["organizer"] as $orgId => $org){ ?>
										<img class="img-responsive" src="<?php echo $org["profilThumbImageUrl"] ?>" alt="image">
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
			
			<br/>
		<?php }else { ?>
			<div class="title-section m-title-desc">
				<h3>Fonction</h3>
			</div>
			<div class="media m-title-desc">
				<div class="media-body media-middle">
					<?php  if (isset($element["fonction"]) && !empty($element["fonction"])) { ?>
						<p class="community-text"><?= $element["fonction"] ?></p>
					<?php }else{ ?>
						<p class="community-text"><i>Fonction non renseigné.</i></p>
					<?php }?>
				</div>
			</div>
			<br/>
			<div class="title-section m-title-desc">
				<h3>Description</h3>
			</div>
			<div class="media m-title-desc">
				<div class="media-body media-middle description">
					<?php  if (isset($descriptionPrev) && !empty($descriptionPrev)) { ?>
						<p class="community-text activeMarkdown"><?= $descriptionPrev ?></p>
					<?php } ?>
				</div>
			</div>
		<?php }?>

		<?php if($element["collection"]=="events" && isset($element["url"])){ ?>
			<div class="title-section m-title-desc">
				<h3>
					<?php if (isset($element["address"]["streetAddress"]) || isset($element["address"]["postalCode"]) || isset($element["address"]["addressLocality"]) || isset($element["address"]["level4Name"])) { ?>
						<?= isset($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"] . ",<br>" : ""; ?>
						<?= isset($element["address"]["postalCode"])? $element["address"]["postalCode"] : ""; ?>
						<?= isset($element["address"]["addressLocality"]) ? $element["address"]["addressLocality"] . "<br>" : ""; ?>
						<?= isset($element["address"]["level4Name"]) ? $element["address"]["level4Name"] : "" ?>
					<?php } else if(isset($element["place"]) && $element["place"]!="") { ?>
						<?= $element["place"] ?>
					<?php } else { ?>
						<p><i>( Localisation non renseignée. )</i></p>
					<?php } ?>
				</h3>
			</div>
			<br/>
		<?php } ?>

		<!-- Mot clés -->
		<?php /* if($element["collection"]=="events" && isset($element["tags"])){ ?>
			<div class="title-section m-title-desc">
				<h3>Mots clés</h3>
			</div>
			<div class="media m-title-desc">
				<div class="media-body media-middle community-text">
					<?php foreach($element["tags"] as $v){ 
						echo "<span class='badge tags-poi-preview margin-right-5'>#".$v."</span>";
					} ?>
				</div>
			</div>
			<br/>
		<?php }*/ ?>
		
		<?php if($element["collection"]=="events" && isset($element["url"])){ ?>
			<div class="media m-title-desc">
				<div class="link-bordered">
					<a href="<?php echo $element["url"]; ?>" target="_blank">S'inscrire</a>
				</div>
			</div>
			<br/>
			<br/>
		<?php } ?>

		<?php if($element["collection"]!="citoyens" && $element["collection"]!="events"){ ?>
			<div class="title-section m-title-contact">
				<h3>Localisation & contacts</h3>
			</div>
		<?php } ?>

        <div class="col-md-12 dv-contact-content">
			<?php if($element["collection"]!="citoyens" && $element["collection"]!="events"){ ?>
				<div class="col-xs-12 col-md-6 space-top-prev">
					<div class="preview-adress flex-disp">
						<div class="localisation-icon">
							<i class="fa fa-map-marker faw-size"></i>
						</div>
						<div class="localisation-text mx-1">
							<?php if (isset($element["address"]["streetAddress"]) || isset($element["address"]["postalCode"]) || isset($element["address"]["addressLocality"]) || isset($element["address"]["level4Name"])) { ?>
								<p><?= isset($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"] . ",<br>" : ""; ?>
									<?= isset($element["address"]["postalCode"])? $element["address"]["postalCode"] : ""; ?>
									<?= isset($element["address"]["addressLocality"]) ? $element["address"]["addressLocality"] . "<br>" : ""; ?>
									<?= isset($element["address"]["level4Name"]) ? $element["address"]["level4Name"] : "" ?></p>
							<?php } else { ?>
								<p><i>Localisation non renseignée.</i></p>
							<?php } ?>
						</div>
					</div>

					<div id="socialNetwork">
						<?php if (isset($element["url"])) { ?>
							<span id="divUrl" class="margin-right-5">
								<a class="contentInformation text-center btn-network tooltips" href="<?php echo $element["url"]; ?>" target="_blank" id="urlAbout">
									<i class="fa fa-laptop" style="color:#1877f2;"></i>
								</a>
							</span>
						<?php } ?>

						<?php if (isset($element["socialNetwork"])) { ?>
							<?php
							if (isset($element["socialNetwork"]["facebook"])) { ?>
								<span id="divFacebook" class="margin-right-5">
									<a class="contentInformation text-center btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"]; ?>" target="_blank" id="facebookAbout">
										<i class="fa fa-facebook" style="color:#1877f2;"></i>
									</a>
								</span>
							<?php }

							if (isset($element["socialNetwork"]["twitter"])) { ?>
								<span id="divTwitter">
									<a class="contentInformation text-center btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"]; ?>" target="_blank" id="twitterAbout">
										<i class="fa fa-twitter" style="color:#1da1f2;"></i>
									</a>
								</span>
							<?php }

							if (isset($element["socialNetwork"]["instagram"])) { ?>
								<span id="divInstagram">
									<a class="contentInformation text-center btn-network tooltips" href="<?php echo $element["socialNetwork"]["instagram"]; ?>" target="_blank" id="instagramAbout">
										<i class="fa fa-instagram" style="color:#1da1f2;"></i>
									</a>
								</span>
							<?php }
						} ?>
					</div>
				</div>
			<br/>

			<?php }?>
			
			<?php if($element["collection"]!="events"){ ?>
            <div class="col-xs-12 col-md-6 space-top-prev">
                <div class="contact-title-content flex-disp">
                    <div class="contact-title-icon">
                        <i class="fa fa-address-card-o faw-size"></i>
                    </div>
                    <div class="contact-title-text mx-1">
                        <p class="font-21">contact :</p>
                        <div class="contact-content">
                            <?php if (isset($element["email"]) || (isset($phone) && !empty($phone))) { ?>
                                <?php if (isset($element["email"])) { ?>
                                    <div class="contact-mail">
                                        <p><?= $element["email"] ?></p>
                                    </div>
                                <?php }

                                if ((isset($phone) && !empty($phone))) { ?>
                                    <div class="contact-phone">
                                        <p><?= $phone ?></p>
                                    </div>
                                <?php } ?>
                            <?php } else { ?>
                                <div class="no-contact">
                                    <p><i>Contact non renseigné.</i></p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
			<?php } ?>
        </div>

		
		<?php }else{ ?>
			<div class="text-center text-danger margin-top-35">
				<i class="fa fa-lock fa-4x "></i>
				<h1>Accès Non Autorisé</h1>
				<p>Il faut être membre de la plateforme</p>
			</div>
		<?php } ?>
	</div>

	<script type="text/javascript">
		var eltPreview = <?php echo json_encode($element); ?>;
		var typePreview = <?php echo json_encode($type); ?>;
		var idPreview = <?php echo json_encode($id); ?>;
		jQuery(document).ready(function() {
			var str = directory.getDateFormated(eltPreview, null, true);
			if(typeof eltPreview.description == "undefined"){
				$(".activeMarkdown").html(dataHelper.markdownToHtml(eltPreview.shortDescription))
			}else{
				$(".activeMarkdown").html(dataHelper.markdownToHtml(eltPreview.description))
			}
			var reference = {
				load: function() {
					this.manageSourceData();
				},
				manageSourceData : function(){
					mylog.log("bindReferenceBtnEvents");
					$(".setSourceAdmin").off().on("click", function(){
						var $btnClick=$(this);
						if($btnClick.data("setkey")=="source" && $btnClick.data("action")=="remove"){
							bootbox.confirm("BE carefull, remove a source from an element is not revokable !!<br/>Are you sure to continue ?", function(result) {
								if (result) {
									reference.setSourceDataAction($btnClick);
								}
							});
						}else{
							reference.setSourceDataAction($btnClick);
						}	
					});
				},
				setSourceDataAction : function($btnClick){
					var action=$btnClick.data("action");
					var setKey=$btnClick.data("setkey");
					var params={
						id:$btnClick.data("id"),
						type:$btnClick.data("type")
					};
					if(typeof costum != "undefined" && notNull(costum)){
						params.origin="costum";
						params.sourceKey=(typeof costum.isTemplate !="undefined" && costum.isTemplate) ? costum.contextSlug : costum.slug;
					} 
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/admin/setsource/action/"+action+"/set/"+setKey,
						params,
						function(data){ 
							if ( data && data.result ) {
								toastr.success(data.msg);
								$(".btn-ref-" + params.id).fadeOut();
							} else {
							toastr.error("something went wrong!! please try again.");
							}

						}
					);
				}
			};
			if(eltPreview.startDate){
				$("#eventDate").append(`
					<div>${moment(eltPreview.startDate).locale("fr").format("dddd DD MMMM YYYY - HH:mm")}</div>
				`);
			}

			$(".btn-delete-element-ekitia").off().on("click", function () {
				$("#modal-preview-coop").hide(300);
				$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
				directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), function(data, type, id){
					location.href = baseUrl+"/costum/co/index/slug/"+costum.contextSlug+"#events";
				}, "Voulez vous vraiment supprimer cet événement ?", "Suppression éffectué");
			});

			$(".btn-edit-element-ekitia, .btn-edit-element").off().on("click", function () {
				$("#modal-preview-coop").hide(300);
				//$("#modal-preview-coop").html("");
				let thisEl = <?php echo json_encode($element); ?>;
				if(thisEl && thisEl.startDate){
					thisEl.startDate = moment(thisEl.startDate).locale("fr").format("DD/MM/YYYY HH:mm")
				}
				if(thisEl && thisEl.endDate){
					thisEl.endDate = moment(thisEl.endDate).locale("fr").format("DD/MM/YYYY HH:mm")
				}
				
				dyFObj.editMode = true;
				uploadObj.update = true;
				dyFObj.currentElement={type : thisEl.collection, id : $(this).data("id")};
		
				delete thisEl._id;
				delete thisEl.created;
				delete thisEl.creator;
				//delete thisEl.organizer;
				delete thisEl.modified;
				delete thisEl.source;
				delete thisEl.updated;
				delete thisEl.links;

				dyfCostum = {
					...dyFObj,
					beforeBuild: {
						properties : {
							shortDescription : dyFInputs.textarea(tradDynForm.shortDescription)
						}
					},
					afterSave: function(data){
						dyFObj.commonAfterSave(data, function(){
							urlCtrl.loadByHash(location.hash);
						})
					}
				}

				if(thisEl.collection=="events"){
					dyfCostum.beforeBuild.properties["place"] = {
						label:"Lieu de l'événement",
						placeholder: "En visio ou Sur site + le nom de la ville",
						"order" : 13
					}
					dyfCostum.beforeBuild.properties["url"] = {
						label:"Lien vers la page d'inscription",
						"order" : 12
					}
                    dyfCostum.beforeBuild.properties["type"] = {
						"label" : "Type d'événement",
						"inputType" : "selectMultiple",
						"isSelect2" : true,
						"optionsValueAsKey" : true,
						"list" : "eventTypes",
						"placeholder" : "Type d'événement",
						"rules" : {
							"required" : true
						}
					}

					dyfCostum["onload"] = {
						"actions" : {
                            "hide" : {
                                "locationlocation" : 1,
                                "formLocalityformLocality" : 1
                            }
                        }
					}
				}

				dyFObj.openForm(
					thisEl.collection.substring(0, thisEl.collection.length - 1), 
					null,
					{
						...thisEl, 
						id:$(this).data("id")
					},
					false,
					dyfCostum
				);
			});

			$(".btn-update-user-info").off().on("click", function() {
				$("#modal-preview-coop").hide(300);
				$("#modal-preview-coop").html("");
				var form = {
					saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
					dynForm: {
						jsonSchema: {
							title: "Mettre à jour vos informations",
							properties: {
								"fonction": {
									"label" : "Fonction"
								},
								"telephone": {
									"label" : "Téléphone",
									"inputType": "phone"
								},
								"email": {
									"label" : "Email",
									"inputType": "email"
								},
								"description": {
									"label" : "Description",
									"inputType": "textarea"
								}
							},
							save : function(data){
								delete data.collection;
								delete data.scope;
								
								let dataToUpDate = {
									id: contextData.id,
									collection: "citoyens",
									path:"allToRoot",
									value : data
								}
								dataHelper.path2Value(dataToUpDate, function(params) {
									urlCtrl.loadByHash(window.location.hash)
								});
							}
						}
					}
				}

				var dataUpdate = {
					"fonction" : contextData.fonction,
					"telephone" : contextData.telephone,
					"description" : contextData.description,
					"email" : contextData.email
				}
				dyFObj.openForm(form, null, dataUpdate);
			});

			reference.load();
			$(".event-infos-header").html(str);
			directory.bindBtnElement();

            $(".ekitia-btn-inscrire").off().on("click", function(){
                if((typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin)){
                    sendReferences();
                }else{
                    bootbox.confirm("Vous essayez de rejoindre la communauté d'Ekitia. Etes-vous sur ? ", function(result){
                        if(result){
                            sendReferences();
                        }
                    })
                }
            });
		});
        function sendReferences(){
            ajaxPost(
                null,
                baseUrl+"/costum/ekitia/references",
                {
                    elementType: typePreview,
                    elementId: idPreview,
                    isAdmin: (typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin)
                },
                function(result){
                    if(result.result){
                        dyFObj.closeForm();
                        var toastText = (typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin) ? "Structure ajoutée comme membre d'Ekitia" : "Votre demande pour rejoindre la communauté d'Ekitia a été envoyé aux administrateurs";
                        toastr.success(toastText);
                        $(".btn-close-preview").click();
                    }
                }
            );
        }
	</script>