<?php

use PixelHumain\PixelHumain\modules\citizenToolKit\components\activitypub\utils\Utils;

$costumCache = CacheHelper::getCostum();

$isAdmin = Authorisation::isElementAdmin($element['_id'], $element['collection'], Yii::app()->session["userId"]);
$isCostumMember = Authorisation::isElementMember($costumCache['contextId'], $costumCache['contextType'], Yii::app()->session["userId"]);

if ($element["collection"] == Person::COLLECTION) {
    $isAdmin = ((string) $element['_id']) == Yii::app()->session["userId"];
}
$isAdminOnlyTargetOrg = Authorisation::isElementAdmin($element['_id'], $element['collection'], Yii::app()->session["userId"], false, false);
$isMember = Authorisation::isElementMember($element['_id'], $element['collection'],  Yii::app()->session["userId"]);
$isContributor = Authorisation::hasRoles($element["collection"], $element["_id"], array( "roles"=> ["Contributeur"]));


if (!($isMember || $isAdminOnlyTargetOrg || $isAdmin))
    $element["showDetailSection"] = [];
$cssAnsScriptFilesModule = array(
    //Data helper
    '/js/dataHelpers.js',
    '/js/default/editInPlace.js',
    '/css/element/about.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

$sortableAssets = [
    '/plugins/jquery.sortable/Sortable.js',
    '/plugins/jquery.sortable/jquery-sortable.js',
    '/plugins/jlist/js/jplist.core.min.js',
    '/plugins/jlist/js/jplist.bootstrap-sort-dropdown.min.js',
    '/plugins/jlist/js/jplist.pagination-bundle.min.js',
    '/plugins/jlist/js/jplist.textbox-filter.min.js'
];
HtmlHelper::registerCssAndScriptsFiles($sortableAssets, Yii::app()->request->baseUrl);


if (isset(Yii::app()->session["userId"]) && ($isCostumMember || ($isMember || $isAdminOnlyTargetOrg || $isAdmin))) {

    $elementParams = @$this->appConfig["element"];
    if (isset($this->costum)) {
        $cssJsCostum = array();
        if (isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/' . $this->costum["slug"] . '/about.js');
        if (isset($elementParams["css"]) && $elementParams["css"] == "about.css")
            array_push($cssJsCostum, '/css/' . $this->costum["slug"] . '/about.css');
        if (!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule("costum")->getAssetsUrl());
    }
    $isFromFediverse = Utils::isFediverseShareLocal($element);
    $isLocalActor = boolval(Utils::isLocalActor());
    if (@$element["fromActivityPub"] && $element["fromActivityPub"] == true) {
        $activities = Utils::activitypubToElement($element["objectId"]);
        foreach ($activities as $activityKey => $activityValue) {
            $element[$activityKey] = $activityValue;
        }
    }

    // TO DO : à dynamiser
    $getFormationData = PHDB::find(Answer::COLLECTION, ["form" => "6538aed38312ccf36c042c95",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $getServicesData = PHDB::find(Answer::COLLECTION, ["form" => "6538adfc90b2f58f3503d8e3",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $getToolsData = PHDB::find(Answer::COLLECTION, ["form" => "6538ae1a8312ccf36c042c94",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $getCatalogsData = PHDB::find(Answer::COLLECTION, ["form" => "65389e3790b2f58f3503d8e2",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $getAlgo = PHDB::find(Answer::COLLECTION, ["form" => "659ceff39c9ae3090c6b3dce",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $getStorageCapacity = PHDB::find(Answer::COLLECTION, ["form" => "65b21195ac117c27326a1858",  "links.organizations." . (string)$element["_id"] => ['$exists' => true]]);
    $forms = PHDB::findByIds(Form::COLLECTION, [
        "65389e3790b2f58f3503d8e2",
        "659ceff39c9ae3090c6b3dce",
        "6538ae1a8312ccf36c042c94",
        "65b21195ac117c27326a1858",
        "6538adfc90b2f58f3503d8e3",
        "6538aed38312ccf36c042c95"], ["name"]);

    $countAnswer = [
        "65389e3790b2f58f3503d8e2" => count($getCatalogsData),
        "659ceff39c9ae3090c6b3dce" => count($getAlgo),
        "6538ae1a8312ccf36c042c94" => count($getToolsData),
        "65b21195ac117c27326a1858" => count($getStorageCapacity),
        "6538adfc90b2f58f3503d8e3" => count($getServicesData),
        "6538aed38312ccf36c042c95" => count($getFormationData)
    ];

    $sectionsPod = [
        "descriptions",
        "info",
        "services",
        "tools",
        "formation",
        "catalog",
        "address",
        "network",
        "fonction"
    ];

    foreach ($sectionsPod as $key => $value) {
        $element["showDetailSection"][$value] = isset($element["showDetailSection"][$value]) ? $element["showDetailSection"][$value] : false;
    }

    function shortDescription($description, $maxLength)
    {
        
        if (strlen($description) > $maxLength) {
            return preg_replace('/\W\w+\s*(\W*)$/', '$1', substr($description, 0, $maxLength)) . '...';
        }
        return $description;
    }

    if (isset($element["tags"])) {
        $element["tags"] = array_filter($element["tags"], function ($tags) {
            $activityLists = array_keys($this->costum["lists"]["activity"]);
            return in_array($tags, $activityLists);
        });
    }
    $initFiles = Document::getListDocumentsWhere(
        array(
            "id"=> (string) $element['_id'],
            "type"=>$element["collection"],
            "contentKey"=>'slider',
        ), "image"
    );

    /*
echo "<pre>";
    var_dump($initFiles);
    var_dump($element["projectsInProgress"]);
    echo "</pre>";

    foreach ($initFiles as $key => $value) {
        if($value["subKey"]){

        }
    }*/
?>

    <?php
    if ($isFromFediverse) {
        if ($isLocalActor) {
            $edit = isset($_POST["canEdit"]) ? filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : $edit;
        } else {
            $edit = false;
        }
    } else {
        $edit = isset($_POST["canEdit"]) ? filter_var($_POST["canEdit"], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : $edit;
    }

    if (isset(Yii::app()->session["costum"]["ekisphere"]["admins"][Yii::app()->session["userId"]])) {
        $edit = true;
        $isAdminOnlyTargetOrg = true;
    }

    ?>
    <script>
        function redirigerVersPage(url) {
            window.open(url, "_blank");
        }
    </script>
    <style>
        .btn-network::before {
            background: var(--primary-color) !important;
        }

        .btn-network {
            box-shadow: 0 5px 15px -5px var(--primary-color) !important;
            border: 1px solid var(--primary-color) !important;
        }

        .sub {
            font-size: 14pt !important
        }

        .bordered {
            border: 1px solid #eee;
            padding: 5px;
            margin-right: 2px;
            margin-left: 2px;
            margin-bottom: 30px;
        }
        .projectInProgressImg{
            width: 100%;
        }
        .border-bottom{
            border-bottom: 1px solid #ddd;
        }
        .fa.fa-times{
            color:white !important
        }
    </style>
    <div class="col-lg-offset-2 col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1">
        <div class="about-xs-completed visible-xs">
            <div class="about-avatar">
                <?php
                echo $this->renderPartial(
                    'co2.views.pod.fileupload',
                    array(
                        "podId" => "modalDash",
                        "element" => $element,
                        "edit" => $edit,
                        "openEdition" => false
                    )
                ); ?>
            </div>
            <div class="well col-xs-12 net-well no-padding">

                <div id="menuCommunity" class="col-md-12 col-sm-12 col-xs-12 numberCommunity">

                </div>

                <div class="animated bounceInRight">
                    <?php if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] != $invitedMe["invitorId"]) { ?>
                    <?php echo $this->renderPartial(
                            'co2.views.element.menus.answerInvite',
                            array(
                                "invitedMe"      => $invitedMe,
                                "element"   => $element
                            )
                        );
                    } else if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] == $invitedMe["invitorId"]) { ?>
                        <div class="containInvitation">
                            <div class="statuInvitation">
                                <?php echo Yii::t("common", "Friend request sent") ?>
                                <?php
                                $inviteCancel = "Cancel";
                                $option = null;
                                $msgRefuse = Yii::t("common", "Are you sure to cancel this invitation");

                                echo
                                '<br>' .
                                    '<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . Element::$connectTypes[$element["collection"]] . '\',null,\'' . $option . '\',\'' . $msgRefuse . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", "Not interested by the invitation") . '">' .
                                    '<i class="fa fa-remove"></i> ' . Yii::t("common", $inviteCancel) .
                                    '</a>';

                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>




                <div class="section-Community">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading no-padding" id="headingOne" role="tab">
                                <h4 class="panel-title">
                                    <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Communauté<i class="pull-right fa fa-chevron-down"></i>
                                    </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">

                                <div class="panel-body">
                                    <?php if ($type == Person::COLLECTION) { ?>
                                        <div class="col-xs-4 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["follows"])) {
                                                    echo count($element["links"]["follows"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Follows") ?> </span>
                                        </div>

                                        <div class="col-xs-4 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["friends"])) {
                                                    echo count($element["links"]["friends"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Friends") ?> </span>
                                        </div>

                                        <div class="col-xs-4 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["memberOf"])) {
                                                    echo count($element["links"]["memberOf"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Organizations") ?> </span>
                                        </div>
                                    <?php } ?>

                                    <?php if ($type == Organization::COLLECTION) { ?>
                                        <div class="col-xs-6 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["members"])) {
                                                    echo count($element["links"]["members"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Members") ?> </span>
                                        </div>

                                        <div class="col-xs-6 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["followers"])) {
                                                    echo count($element["links"]["followers"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                        </div>

                                    <?php } ?>

                                    <?php if ($type == Project::COLLECTION) { ?>
                                        <div class="col-xs-6 no-padding">
                                            <span class="number-Community">

                                                <?php
                                                if (isset($element["links"]["contributors"])) {
                                                    echo count($element["links"]["contributors"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Contributors") ?> </span>
                                        </div>

                                        <div class="col-xs-6 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["followers"])) {
                                                    echo count($element["links"]["followers"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                        </div>

                                    <?php } ?>

                                    <?php if ($type == Event::COLLECTION) { ?>
                                        <div class="col-xs-12 no-padding">
                                            <span class="number-Community">
                                                <?php if (isset($element["links"]["attendees"])) {
                                                    echo count($element["links"]["attendees"]);
                                                } else {
                                                    echo 0;
                                                } ?>
                                            </span><br>
                                            <span class="label-Community"> <?php echo Yii::t("common", "Attendees") ?> </span>
                                        </div>

                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Fonction section -->
        
        <div class="pod-info-Fonction content-section" id="pod-info-Description">
            <?php if (Yii::app()->session["userId"] != null) { ?>
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i><?php echo Yii::t("common", "Fonction") ?>
                </span>


                <?php

                if ($edit === true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                    <?php if ($isAdminOnlyTargetOrg || $element["showDetailSection"]["descriptions"]) {  ?>
                        <button class="btn-update-function btn pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update description") ?>" id="btn-descriptions">
                            <?php if ((!empty($element["shortDescription"])) || (!empty($element["description"]))) { ?>
                                <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                            <?php } else { ?>
                                <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
                            <?php } ?>
                        </button>
                    <?php }
                    if ($isAdminOnlyTargetOrg) {  ?>
                        <button class="btn-manage-Function btn pad-2 pull-right tooltips" data-block-key="descriptions" data-collection="<?= $element["collection"] ?>" data-element-id="<?= (string)$element["_id"] ?>" data-status="<?= ($element["showDetailSection"]["descriptions"]) ? "public" : "admin" ?>" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Gerer la visibilité de la modification ou de l'ajout" id="btn-descriptions">
                            <?php if (!$element["showDetailSection"]["fonction"]) { ?>
                                <i class="fa fa-eye-slash"></i>
                            <?php } else { ?>
                                <i class="fa fa-eye"></i>
                            <?php } ?>
                        </button>
                    <?php } ?>
                <?php } ?>

            <hr class="line-hr">

            <?php } else if ((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null)) { ?>
                <span class="text-nv-3 letter-blue">
                    <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common", "Descriptions") ?>
                </span>
                <hr class="line-hr">
            <?php } ?>

            <div id="contenuDesc">
                <?php if (!((empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) { ?>
                    <div class="contentInformation margin-10"  style="font-size: 18px; text-align:justify;">
                        <p id="fonction" name="fonction" class="bold"><?php echo (@$element["fonction"]) ? $element["fonction"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                        <p id="fonctionEdit" name="fonctionEdit" class="hidden"><?php echo (!empty($element["fonction"])) ? $element["fonction"] : ""; ?></p>
                    </div>
                <?php }
                if (!((empty($element["description"])) && Yii::app()->session["userId"] == null)) { ?>
                    <div class="contentInformation margin-10">
                        <div class="more no-padding" id="descriptionAbout" style="font-size:18px; text-align:justify; white-space:pre-wrap"><?php echo (@$element["description"]) ? $element["description"] : '<i></i>'; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    <!-- Description section -->
        <div class="pod-info-Description content-section" id="pod-info-Description">
            <?php if (Yii::app()->session["userId"] != null) { ?>
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i><?php echo Yii::t("common", "Description") ?>
                </span>


                <?php

                if ($edit === true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                    <?php if ($isAdminOnlyTargetOrg || $element["showDetailSection"]["descriptions"]) {  ?>
                        <button class="btn-update-descriptions btn pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update description") ?>" id="btn-descriptions">
                            <?php if ((!empty($element["shortDescription"])) || (!empty($element["description"]))) { ?>
                                <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                            <?php } else { ?>
                                <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
                            <?php } ?>
                        </button>
                    <?php }
                    if ($isAdminOnlyTargetOrg) {  ?>
                        <button class="btn-manage-Description btn pad-2 pull-right tooltips" data-block-key="descriptions" data-collection="<?= $element["collection"] ?>" data-element-id="<?= (string)$element["_id"] ?>" data-status="<?= ($element["showDetailSection"]["descriptions"]) ? "public" : "admin" ?>" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Gerer la visibilité de la modification ou de l'ajout" id="btn-descriptions">
                            <?php if (!$element["showDetailSection"]["descriptions"]) { ?>
                                <i class="fa fa-eye-slash"></i>
                            <?php } else { ?>
                                <i class="fa fa-eye"></i>
                            <?php } ?>
                        </button>
                    <?php } ?>
                <?php } ?>

            <hr class="line-hr">

            <?php } else if ((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null)) { ?>
                <span class="text-nv-3 letter-blue">
                    <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common", "Descriptions") ?>
                </span>
                <hr class="line-hr">
            <?php } ?>

            <div id="contenuDesc">
                <?php if (!((empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) { ?>
                    <div class="contentInformation margin-10"  style="font-size: 18px; text-align:justify;">
                        <p id="shortDescriptionAbout" name="shortDescriptionAbout" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                        <p id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit" class="hidden"><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></p>
                    </div>
                <?php }
                if (!((empty($element["description"])) && Yii::app()->session["userId"] == null)) { ?>

                    <div class="contentInformation margin-10">
                        <div class="more no-padding" id="descriptionAbout" style="font-size:18px; text-align:justify; white-space:pre-wrap"><?php echo (@$element["description"]) ? $element["description"] : '<i></i>'; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php if (in_array($type, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])) {
            $modeDate = "recurrency";
            $title = Yii::t("common", "When");
            $emptyval = Yii::t("common", "No date");
            if ($type == Organization::COLLECTION) {
                $title = Yii::t("common", "Opening hours");
                $emptyval = Yii::t("common", "No opening hours");
                $modeDate = "openingHours";
            } else if ($type == Project::COLLECTION) {
                $modeDate = "date";
            }
            // echo  $this->renderPartial('co2.views.pod.dateOH', array("element" => $element, "title" => $title, "emptyval" => $emptyval, "edit" => $edit, "openEdition" => $openEdition));
        }; ?>

        <div class="section content-section light-bg pod-infoGeneral" id="pod-infoGeneral">
            <div class="row profil-title-informations">
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <span class="text-nv-3">
                        <i class="fa fa-address-card-o"></i> Contact(s)
                    </span>
                    <?php if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                        <button class="btn-update-info-ekit btn btn-update-ig pad-2 pull-right  visible-xs tooltips" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update general information") ?>">
                            <i class="fa fa-edit"></i>
                        </button>
                    <?php } ?>
                </div>
                <?php if ($edit == true || ($openEdition == true && Yii::app()->session["userId"] != null)) { ?>
                    <div class=" col-md-4 col-sm-4 col-xs-12 hidden-xs">
                        <?php if ($isAdminOnlyTargetOrg || $element["showDetailSection"]["info"]) {  ?>
                            <button class="btn-update-info-ekit btn btn-update-ig pad-2 pull-right tooltips" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="<?php echo Yii::t("common", "Update general information") ?>">
                                <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                            </button>
                        <?php }
                        if ($isAdminOnlyTargetOrg) {  ?>
                            <button class="btn-manage-info btn pad-2 pull-right tooltips" data-block-key="info" data-collection="<?= $element["collection"] ?>" data-element-id="<?= (string)$element["_id"] ?>" data-status="<?= ($element["showDetailSection"]["info"]) ? "public" : "admin" ?>" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Gerer la visibilité de la modification ou de l'ajout">
                                <?php if (!$element["showDetailSection"]["info"]) { ?>
                                    <i class="fa fa-eye-slash"></i>
                                <?php } else { ?>
                                    <i class="fa fa-eye"></i>
                                <?php } ?>
                            </button>
                        <?php } ?>

                    </div>
                <?php } ?>

            </div>
            <hr class="line-hr">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
                    <?php
                    if ($type == Organization::COLLECTION) {
                        $resp = '<i>' . Yii::t("common", "Not specified") . '</i>';
                        $occupation = '<i>' . Yii::t("common", "Not specified") . '</i>';
                        $resp = (@$element["contacts"]["firstname"]) ? "" . $element["contacts"]["firstname"] : $resp;
                        $occupation = (@$element["contacts"]["function"]) ? $element["contacts"]["function"] : $occupation;
                        if (isset($element["contacts"]["lastname"]))
                            $resp = $resp . " " . $element["contacts"]["lastname"];
                    ?>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="card features">
                                <div class="card-body">
                                    <?php if ($type != Poi::COLLECTION) { ?>
                                        <div class="media contentInformation">
                                            <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                                            <div class="media-body">
                                                <h6>SITE WEB PRINCIPAL</h6>
                                                <p class="padding-top-10" id="webAbout">
                                                    <?php
                                                    if (@$element["url"]) {
                                                        //If there is no http:// in the url
                                                        $scheme = ((!preg_match("~^(?:f|ht)tps?://~i", $element["url"])) ? 'http://' : "");
                                                        echo '<a data-url="' . $element['url'] . '" href="' . $scheme . $element['url'] . '" target="_blank" id="url" style="cursor:pointer;">' . $element["url"] . '</a>';
                                                    } else
                                                        echo '<i>' . Yii::t("common", "Not specified") . '</i>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 margin-bottom-20">
                            <div class="card features">
                                <div class="card-body">
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>EMAIL PRINCIPAL</h6>
                                            <p class="padding-top-10" data-email="<?php echo (@$element["email"]) ? "" . $element["email"] : ""; ?>" id="email"><?php echo (@$element["email"]) ? $element["email"]  : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <span class="text-nv-3">
                        <i class="fa fa-address-card-o"></i> Contacts référent <?php /* echo (@$costum["name"]) ? "" . $costum["name"] : ""; */ ?>
                    </span>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="card features">
                                <div class="card-body">
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-user gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>RESPONSABLE</h6>
                                            <p class="padding-top-10" data-adminFirstname="<?= (@$element["contacts"]["firstname"]) ? "" . $element["contacts"]["firstname"] : ""; ?>" data-adminLastname="<?= (@$element["contacts"]["lastname"]) ? "" . $element["contacts"]["lastname"] : ""; ?>" id="adminName"><?php echo $resp; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="card features">
                                <div class="card-body">
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-suitcase gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>FONCTION</h6>
                                            <p class="padding-top-10" data-adminFunction="<?php echo (@$element["contacts"]["function"]) ? "" . $element["contacts"]["function"] : ""; ?>" id="adminFunction"><?php echo $occupation; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card features">
                            <div class="card-body">
                                <?php
                                if (($type == Person::COLLECTION &&
                                        Preference::showPreference($element, $type, "email", Yii::app()->session["userId"])) ||
                                    in_array($type, [Project::COLLECTION, Event::COLLECTION])
                                ) { ?>

                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>EMAIL</h6>
                                            <p class="padding-top-10" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                                        </div>
                                    </div>
                                <?php }
                                if ($type == Organization::COLLECTION) {  ?>
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>EMAIL</h6>
                                            <p class="padding-top-10" data-adminMail="<?php echo (@$element["contacts"]["mail"]) ? $element["contacts"]["mail"]  : ""; ?>" id="adminMail"><?php echo (@$element["contacts"]["mail"]) ? $element["contacts"]["mail"]  : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card features">
                            <div class="card-body">
                                <?php if ($type != Person::COLLECTION /*&& $type != Organization::COLLECTION */) { ?>
                                    
                                <?php } ?>

                                <?php
                                if ($type == Organization::COLLECTION) {
                                    $phone = "";
                                    if (isset($element["telephone"]) && isset($element["telephone"]["fixe"])){
                                        $phone = ArrayHelper::arrayToString($element["telephone"]["fixe"]);
                                    }
                                    if (isset($element["telephone"]) && isset($element["telephone"]["mobile"])){
                                        $phone = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                                    }
                                    if (!empty($element["contacts"]["phone"])){
                                        $phone = $element["contacts"]["phone"];
                                    }
                                ?>
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-phone gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>TELEPHONE</h6>
                                            <p data-adminPhone="<?php echo $phone; ?>" class="padding-top-10" id="adminPhone"> <?php echo ($phone!="")?$phone:'<i>' . Yii::t("common", "Not specified") . '</i>'; ?> </p>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->renderPartial('../pod/whycommunexion', array()); ?>

    <script type="text/javascript">
        //Affichage de la map
        $("#divMapContent").show(function() {
            afficheMap();

        });
        var mapAbout = {};

        $("#InfoDescription").show(function() {
            controleAffiche();
        });

        function controleAffiche() {
            mylog.log(contextData.shortDescription, contextData.description);
            if ((typeof contextData.shortDescription == "undefined") && (typeof contextData.description == "undefined")) {
                $("#contenuDesc").addClass("hidden");
                //$("#btn-descriptions").html('<i class="fa fa-plus"> Ajouter');

            };
        };


        function afficheMap() {
            mylog.log("afficheMap");

            //Si contextData.geo est undefined caché la carte
            if (typeof contextData.geo == "undefined") {
                $("#divMapContent").addClass("hidden");
            };

            var paramsMapContent = {
                container: "divMapContent",
                latLon: contextData.geo,
                activeCluster: false,
                zoom: 16,
                activePopUp: false
            };


            mapAbout = mapObj.init(paramsMapContent);

            var paramsPointeur = {
                elt: {
                    id: contextData.id,
                    type: contextData.type,
                    geo: contextData.geo
                },
                center: true
            };
            mapAbout.addMarker(paramsPointeur);
            mapAbout.hideLoader();


        };


        //var paramsPointeur[contextId] = contextData;
        var formatDateView = "DD MMMM YYYY à HH:mm";
        var formatDatedynForm = "DD/MM/YYYY HH:mm";

        jQuery(document).ready(function() {
            bindDynFormEditable();
            initDate();
            inintDescs();
            //changeHiddenFields();
            bindAboutPodElement();
            bindExplainLinks();

            $("#small_profil").html($("#menu-name").html());
            $("#menu-name").html("");

            $(".cobtn").click(function() {
                communecterUser();
            });

            $(".btn-update-geopos").click(function() {
                updateLocalityEntities();
            });

            $("#btn-add-geopos").click(function() {
                updateLocalityEntities();
            });

            $("#btn-update-organizer").click(function() {
                updateOrganizer();
            });
            $("#btn-add-organizer").click(function() {
                updateOrganizer();
            });

            $("#btn-remove-geopos").click(function() {
                removeAddress();
            });

            $("#btn-update-geopos-admin").click(function() {
                findGeoPosByAddress();
            });

            coInterface.bindLBHLinks();

        });

        function inintDescs() {
            mylog.log("inintDescs");
            if ($("#descriptionAbout").length > 0) {
                if (canEdit == true || openEdition == true)
                    descHtmlToMarkdown();
                mylog.log("after");
                mylog.log("inintDescs", $("#descriptionAbout").html());
                var descHtml = "<i>" + trad.notSpecified + "</i>";
                if ($("#descriptionAbout").html().length > 0) {
                    descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html());
                }
                $("#descriptionAbout").html(descHtml);
                //$("#descProfilsocial").html(descHtml);
                mylog.log("descHtml", descHtml);
            }
        }

        function initDate() { //DD/mm/YYYY hh:mm

            formatDateView = "DD MMMM YYYY à HH:mm";
            formatDatedynForm = "DD/MM/YYYY HH:mm";

            mylog.log("formatDateView", formatDateView);
            //if($("#startDateAbout").html() != "")

            $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));

            $("#endDateAbout").html(moment(contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));

            if ($("#birthDate").html() != "")
                $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
            $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
        }

        /*function AddReadMore() {
            var showChar = 300;
            var ellipsestext = "...";
            var moretext = "Lire la suite";
            var lesstext = "Lire moins";
            $('.more').each(function() {
                var content = $(this).html();
                var textcontent = $(this).text();

                if (textcontent.length > showChar) {

                    var c = textcontent.substr(0, showChar);
                    var lastIndex = c.lastIndexOf(" ");
                    c = c.substring(0, lastIndex);
                    //var h = content.substr(showChar-1, content.length - showChar);

                    var html = '<span class="container no-padding" style="font-size:18px; text-align:justify; white-space:pre-wrap"><p>' + c + '</p>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

                    $(this).html(html);
                    $(this).after('<a href="" class="morelink">' + lesstext + '</a>');
                }

            });

            $(".morelink").click(function() {
                if ($(this).hasClass("less")) {
                    $(this).removeClass("less");
                    $(this).html(lesstext);
                    $(this).prev().children('.morecontent').fadeToggle(100, function() {
                        $(this).prev().fadeToggle(100);
                    });

                } else {
                    $(this).addClass("less");
                    $(this).html(moretext);
                    $(this).prev().children('.container').fadeToggle(100, function() {
                        $(this).next().fadeToggle(100);
                    });
                }
                //$(this).prev().children().fadeToggle();
                //$(this).parent().prev().prev().fadeToggle(500);
                //$(this).parent().prev().delay(600).fadeToggle(500);

                return false;
            });
        }*/
        $(function() {
            //Calling function after Page Load
            //AddReadMore();
        });
        var countAnswer = <?php echo json_encode($countAnswer) ?>;
        var count = 0;
        directory.answerPanel = function(params) {
            count++;
            var changeBackground = (count % 2 == 0) ? "style='background: #f9f9f9;'" : "";
            var str = '';
            var btnHtml = '';

            btnHtml += `
        <div class="col-md-12">
            <div class="social-links justify-content-center"><div class="social-btn flex-center getanswer" data-mode="w" data-form="${params.form}" data-ansid="${params.id}">
                <i class="fa fa-pencil-square-o editdeleteicon"></i>
                <span style="font-size: 14px;">Éditer</span>
                </div><div class="social-btn flex-center getanswer" data-mode="r" data-form="${params.form}" data-ansid="${params.id}">
                <i class="fa fa-sticky-note-o editdeleteicon"></i><span style="font-size: 14px;">Lire</span>
                </div><div class="social-btn flex-center deleteanswer" data-mode="w" data-form="${params.form}" data-ansid="${params.id}">
                    <i class="fa fa-trash-o editdeleteicon" style="color: #ff5722"></i><span style="font-size: 14px;">Effacer</span>
                </div>
            </div>
        </div>`;

            str += `
            <div class="row card-vertical-container" ${changeBackground}>
                    <div class="col-md-4" align="center"> Reponse </div>
                    <div class="col-md-4" align="center"> ${directory.showDatetimePost(params.collection, params.id, params.created, 30)} </div>
                    <div class="col-md-4" align="center"> ${btnHtml} </div>
            </div>
        `;

            // if (/*userId != null && userId != '' && */typeof params.id != 'undefined' && typeof params.collection != 'undefined')
            // 	str += directory.socialToolsHtml(params);
            str += '</div>';
            str += '</div>';
            str += '</div>';
            str += '</div>';
            str += '</div>';
            return str;
        }
        var formDescription = {
            services: "answers.ekitia25102023_942_0.ekitia25102023_942_0lrqnxq5p4s7e1dodzk",
            tools: "answers.ekitia25102023_954_0.ekitia25102023_954_0lrqnbqdya6zirn5hyqm",
            formations: "",
            catalogs: "answers.ekitia2512024_845_0.ekitia2512024_845_0lsn2xv6nao2jhx5iot",
        }
        $(function() {
            var element = {
                init: function() {
                    this.views.init();
                    this.events.init();
                    this.actions.init();
                },
                views: {
                    init: function() {
                        element.views.addressVisibilityBtn();
                        element.views.networkVisibilityBtn();
                        element.views.manageEdit();
                    },
                    manageEdit: function() {
                        element.events.saveManagedEdit(".btn-manage-Description", "#pod-info-Description");
                        element.events.saveManagedEdit(".btn-manage-siren", "#pod-info-siren");
                        element.events.saveManagedEdit(".btn-manage-nombreETP", "#pod-info-nombreETP");
                        element.events.saveManagedEdit(".btn-manage-info", "#pod-infoGeneral");
                        element.events.saveManagedEdit(".btn-manage-activity", "#pod-info-activity");
                        element.events.saveManagedEdit(".btn-manage-projectsInProgress", "#pod-info-projectsInProgress");
                        element.events.saveManagedEdit(".btn-manage-services", "#pod-services");
                        element.events.saveManagedEdit(".btn-manage-tools", "#pod-tools");
                        element.events.saveManagedEdit(".btn-manage-formation", "#pod-formation");
                        element.events.saveManagedEdit(".btn-manage-catalog", "#pod-catalog");
                        element.events.saveManagedEdit(".btn-manage-address", "#pod-info-Address");
                        element.events.saveManagedEdit(".btn-manage-network", ".pod-network");
                    },
                    addressVisibilityBtn: function() {
                        var btnHtml = `
                        <?php if ($isAdminOnlyTargetOrg) {  ?>
                            <button class="btn-manage-address btn pad-2 pull-right tooltips" data-block-key="address" data-collection="<?= $element["collection"] ?>" data-element-id="<?= (string)$element["_id"] ?>" data-status="<?= ($element["showDetailSection"]["address"]) ? "public" : "admin" ?>" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Gerer la visibilité de la modification ou de l'ajout">
                                <?php if (!$element["showDetailSection"]["address"]) { ?>
                                    <i class="fa fa-eye-slash"></i>
                                <?php } else { ?>
                                    <i class="fa fa-eye"></i>
                                <?php } ?>
                            </button>
                        <?php } ?>
                    `;
                        $("#pod-info-Address .btn-update-geopos").after(btnHtml);
                    },
                    networkVisibilityBtn: function() {
                        var btnHtml = `
                        <?php if ($isAdminOnlyTargetOrg) {  ?>
                            <button class="btn-manage-network btn pad-2 pull-right tooltips" data-block-key="network" data-collection="<?= $element["collection"] ?>" data-element-id="<?= (string)$element["_id"] ?>" data-status="<?= ($element["showDetailSection"]["network"]) ? "public" : "admin" ?>" data-toggle="tooltip" data-placement="bottom" title="" alt="" data-original-title="Gerer la visibilité de la modification ou de l'ajout">
                                <?php if (!$element["showDetailSection"]["network"]) { ?>
                                    <i class="fa fa-eye-slash"></i>
                                <?php } else { ?>
                                    <i class="fa fa-eye"></i>
                                <?php } ?>
                            </button>
                        <?php } ?>
                    `;
                        $(".pod-network .btn-update-network").after(btnHtml);
                    }
                },
                events: {
                    init: function() {
                        element.events.clickCreateForm();
                        element.events.clickGetAnswer();
                        element.events.clickDeleteBtn();
                        element.events.updateComplementDetail();
                        element.events.updateNetwork();
                        element.events.updateProjectSInProgress();
                    },
                    saveManagedEdit: function(dom, parentDom) {
                        var visibleHtml = `<i class="fa fa-eye"></i>`;
                        var nonVisibleHtml = `<i class="fa fa-eye-slash"></i>`;
                        var key = $(dom).data("block-key");
                        var elementId = $(dom).data("element-id");
                        var collection = $(dom).data("collection");
                        $(parentDom).on('click', dom + "[data-status='admin']", function() {
                            var $this = $(this);
                            $this.attr("data-status", "public");
                            $this.html(visibleHtml);
                            element.actions.updateVisibility(elementId, collection, key, true);
                        });
                        $(parentDom).on('click', dom + "[data-status='public']", function() {
                            var $this = $(this);
                            $this.attr("data-status", "admin");
                            $this.html(nonVisibleHtml);
                            element.actions.updateVisibility(elementId, collection, key, false);
                        });
                    },
                    updateComplementDetail: function() {
                        var activityInput = {
                            "label": "Secteur(s) d'activité",
                            "inputType": "selectMultiple",
                            "isSelect2": true,
                            "optionsValueAsKey": true,
                            "list": "activity",
                            "placeholder": "Secteur(s) d'activité",
                            "rules": {
                                "required": true
                            }
                        };
                        element.events.updateModal(".btn-update-siren", {
                            siren: dyFInputs.text("SIREN/SIRET", "")
                        });
                        element.events.updateModal(".btn-update-nombreETP", {
                            nombreETP: dyFInputs.text("Nombre d'Equivalent Temps Plein", "Nombre d'Equivalent Temps Plein", {
                                required: true
                            })
                        });
                        element.events.updateModal(".btn-update-tags", {
                            tags: activityInput
                        });
                        element.events.updateModal(".btn-update-info-ekit", {
                                url: dyFInputs.inputUrl("URL de votre site web", ""),
                                email: dyFInputs.email("Email principal", ""),

                                adminFirstname: dyFInputs.text("Nom du responsable", ""),
                                adminLastname: dyFInputs.text("Prénom du responsable", ""),
                                adminFunction: dyFInputs.text("Fonction du responsable", ""),
                                adminMail: dyFInputs.email("Email du responsable", ""),
                                adminPhone: dyFInputs.text("Numero téléphone du responsable", ""),
                            },
                            function(form, onload, dataUpdate, propObj) {
                                element.events.showContactModal(form, onload, dataUpdate, propObj)
                            }
                        );
                    },
                    updateModal: function(dom, propObj = {}, callBack = () => {}) {
                        var keyClassOrId = dom.replace(".btn-update-", "");
                        $(dom).off().on("click", function() {
                            var form = {
                                saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
                                dynForm: {
                                    jsonSchema: {
                                        title: trad["update"] + " / " + trad["Add"],
                                        icon: "fa-key",
                                        onLoads: {

                                            markdown: function() {
                                                dataHelper.activateMarkdown("#ajaxFormModal #description");
                                                $("#ajax-modal .modal-header").attr("style", "background: #BA1C24 !important");
                                                // form.dynForm.jsonSchema.onLoads.loadIfMultiSelect();
                                            }
                                        },
                                        afterSave: function(data) {
                                            mylog.dir(data);
                                            if (jsonHelper.getValueByPath(data, "resultGoods.values") != undefined) {
                                                if ($(".contentInformation #" + keyClassOrId).length > 0) {
                                                    if (data.resultGoods.values[keyClassOrId] == "")
                                                        $(".contentInformation #" + keyClassOrId).html('<i>' + trad["notSpecified"] + '</i>');
                                                    else
                                                        $(".contentInformation #" + keyClassOrId).html(data.resultGoods.values[keyClassOrId]);
                                                }
                                                dyFObj.closeForm();
                                                changeHiddenFields();
                                                pageProfil.views.detail();
                                                if (keyClassOrId == "tags")
                                                    $("#btn-start-detail").click();
                                            } else {
                                                toastr.error('Erreur d\'enregistrement');
                                            }
                                        },
                                        properties: {
                                            block: dyFInputs.inputHidden(),
                                            typeElement: dyFInputs.inputHidden(),
                                            isUpdate: dyFInputs.inputHidden(true),
                                            ...propObj
                                        }
                                    }
                                }
                            };
                            var dataUpdate = {
                                block: "companyProfile",
                                id: contextData.id,
                                name: contextData.name,
                                typeElement: contextData.type,
                            };

                            if (Object.keys(propObj).length == 1) {
                                if (keyClassOrId != "tags")
                                    dataUpdate[keyClassOrId] = $(".contentInformation #" + keyClassOrId + "Edit").html();
                                else
                                    dataUpdate[keyClassOrId] = <?= json_encode($element["tags"]) ?>;
                                dyFObj.openForm(form, "markdown", dataUpdate);
                            } else if (Object.keys(propObj).length > 1) {
                                callBack(form, "markdown", dataUpdate, propObj);
                            }
                        });
                    },
                    showContactModal: function(form, onload = "", dataUpdate, propObj = {}) {
                        if (typeof propObj == "object" && Object.keys(propObj).length > 0) {
                            $.each(propObj, function(k, v) {
                                if (k == "adminFirstname" || k == "adminLastname")
                                    dataUpdate[k] = $(".contentInformation #adminName").data(k.toLowerCase());
                                else
                                    dataUpdate[k] = $(".contentInformation #" + k).data(k.toLowerCase());
                            })
                        }
                        dyFObj.openForm(form, onload, dataUpdate);
                    },
                    clickCreateForm: function() {
                        $(".btn-create-form").click(function() {
                            var name = $(this).data("name");
                            var formId = $(this).data("form-id");
                            var descKey = $(this).data("description-key");
                            var descInputKey = (jsonHelper.pathExists("formDescription." + descKey)) ? formDescription[descKey] : "";
                            var render = `
                            <div class="banner-outer ">
                                <div class="basic-banner">
                                    <div class="basic-banner-inner">
                                        <div class="container-banner max-950"> 
                                            <div class="basic-info text-center">
                                                <div class="title-banner">
                                                    <h1 class="font-weight-100" style="position: sticky;"><?php echo $element["name"]; ?></h1>
                                                    
                                                </div>	
                                                <h3 style="margin-top: 105px;">Déclarer votre ${name}</h3>                       
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="form-content">
                                <div id="element-form-content" class=""> 
                                    <p class="form-description"> Lorsque vous cliquez sur le bouton ci-dessous, il générera une nouvelle réponse pour vous. </p>
                                    <button class="btn btn-primary createForm btn-create-new">Acceder à un nouveau formulaire</button>
                                </div>
                            </div>
                        `;
                            smallMenu.open(render);
                            $(".btn-create-new").click(function() {
                                var formAns = element.actions.createForm("#form-content", formId, descKey);
                                if (typeof descInputKey == "string" && notEmpty(descInputKey) && descInputKey.includes("."))
                                    element.events.updateDesctiption(descInputKey, descKey, formAns);
                            })
                        })
                    },
                    updateDesctiption: function(descInputKey, descKey, formAns) {
                        var inputId = descInputKey.split(".")[2];
                        $("#" + inputId).on("blur", function() {
                            var newDesc = ($(this).val().trim().length > 0) ? $(this).val() : 'Il n\'y a pas de description';
                            newDesc = element.actions.generateShortDesc(newDesc, 60);
                            $('[data-desc-key="' + descKey + '-' + formAns + '"]').html("<b>" + newDesc + "</b>")
                        });
                    },
                    clickGetAnswer: function() {
                        var timeout = 500;
                        setTimeout(() => {
                            $(".getanswer").off().on("click", function() {
                                var formId = $(this).data("form");
                                var answerId = $(this).data("ansid");
                                var answerMode = $(this).data("mode");
                                var descKey = $(this).data("description-key");
                                var descInputKey = (jsonHelper.pathExists("formDescription." + descKey)) ? formDescription[descKey] : "";
                                var formRender = ``;
                                smallMenu.open(`<div class="answerClicked-content"><div>`);
                                formRender = element.actions.generateRenderForm(formId, answerId, answerMode);
                                $(".answerClicked-content").html(formRender);
                                if (descKey != undefined)
                                    element.events.updateDesctiption(descInputKey, descKey, answerId);
                            })
                        }, timeout);
                    },
                    clickDeleteBtn: function() {
                        var timeout = 500;
                        setTimeout(() => {
                            $(".deleteanswer").off().on("click", function() {
                                var answerId = $(this).data("ansid");
                                var formId = $(this).data("form");
                                element.actions.deleteAnswer(answerId, formId);
                            })
                        }, timeout);
                    },
                    updateNetwork: function() {
                        $(".btn-update-network").off().on("click", function() {
                            var form = {
                                saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
                                dynForm: {
                                    jsonSchema: {
                                        title: trad["Update network"],
                                        icon: "fa-key",
                                        onLoads: {
                                            sub: function() {
                                                $("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
                                                    .addClass("bg-dark");
                                                //bindDesc("#ajaxFormModal");
                                            }
                                        },
                                        beforeSave: function() {
                                            mylog.log("beforeSave", contextData["socialNetwork"]);
                                            //removeFieldUpdateDynForm(contextData.type);
                                            var SNetwork = ["telegram", "github", "gitlab", "twitter", "facebook", /*"gpplus",*/ "instagram", "diaspora", "mastodon", "signal"];
                                            $.each(SNetwork, function(key, val) {
                                                mylog.log("val", val);
                                                mylog.log("val2", $("#ajaxFormModal #" + val).val(), $("#ajaxFormModal #" + val).length);

                                                if (notNull(contextData["socialNetwork"]) &&
                                                    notNull(contextData["socialNetwork"][val]) &&
                                                    ($("#ajaxFormModal #" + val).length &&
                                                        $("#ajaxFormModal #" + val).val().trim() == contextData["socialNetwork"][val])) {
                                                    mylog.log("if", val);
                                                    $("#ajaxFormModal #" + val).remove();
                                                } else if ((!notNull(contextData["socialNetwork"]) ||
                                                        !notNull(contextData["socialNetwork"][val])) && $("#ajaxFormModal #" + val).length) {
                                                    mylog.log("else", val);
                                                    //$("#ajaxFormModal #"+val).remove();
                                                }

                                            });
                                        },
                                        afterSave: function(data) {
                                            mylog.dir(data);
                                            if (data.result && data.resultGoods && data.resultGoods.result) {

                                                //new entree
                                                if (!notEmpty(contextData.socialNetwork))
                                                    contextData.socialNetwork = {};

                                                if (typeof data.resultGoods.values.twitter != "undefined") {
                                                    contextData.socialNetwork.twitter = data.resultGoods.values.twitter.trim();
                                                    changeNetwork('#divTwitter', 'twitterAbout', contextData.socialNetwork.twitter, contextData.socialNetwork.twitter, 'Telegram', '<i class="fa fa-twitter"></i>');
                                                }

                                                if (typeof data.resultGoods.values.github != "undefined") {
                                                    contextData.socialNetwork.github = data.resultGoods.values.github.trim();
                                                    changeNetwork('#divGithub', 'githubAbout', contextData.socialNetwork.github, contextData.socialNetwork.github, 'Telegram', '<i class="fa fa-github"></i>');
                                                }

                                                if (typeof data.resultGoods.values.gitlab != "undefined") {
                                                    contextData.socialNetwork.gitlab = data.resultGoods.values.gitlab.trim();
                                                    changeNetwork('#divGitlab', 'gitlabAbout', contextData.socialNetwork.gitlab, contextData.socialNetwork.gitlab, 'Gitlab', '<img src="' + assetPath + '/images/gitlab_icon.png">');
                                                }
                                                if (typeof data.resultGoods.values.linkedin != "undefined") {
                                                    contextData.socialNetwork.linkedin = data.resultGoods.values.linkedin.trim();
                                                    changeNetwork('#divLinkedin', 'linkedinAbout', contextData.socialNetwork.linkedin, contextData.socialNetwork.linkedin, 'Linkedin', '<i class="fa fa-linkedin"></i>');
                                                }
                                            }
                                            dyFObj.closeForm();
                                            changeHiddenFields();
                                        },

                                        properties: {
                                            block: dyFInputs.inputHidden(),
                                            typeElement: dyFInputs.inputHidden(),
                                            isUpdate: dyFInputs.inputHidden(true),
                                            gitlab: dyFInputs.inputUrl(tradDynForm["linkGitlab"]),
                                            github: dyFInputs.inputUrl(tradDynForm["linkGithub"]),
                                            twitter: dyFInputs.inputUrl(tradDynForm["linkTwitter"]),
                                            linkedin: dyFInputs.inputUrl(tradCms["linkedinLink"]),
                                        }
                                    }
                                }
                            };

                            /*if(contextData.type == typeObj.person.col ){
                                form.dynForm.jsonSchema.properties.telegram = dyFInputs.inputText("Votre Speudo Telegram","Votre Speudo Telegram");
                            }*/

                            var dataUpdate = {
                                block: "network",
                                id: contextData.id,
                                typeElement: contextData.type,
                            };

                            if (notEmpty(contextData.socialNetwork)) {
                                if (notEmpty(contextData.socialNetwork.twitter))
                                    dataUpdate.twitter = contextData.socialNetwork.twitter;
                                if (notEmpty(contextData.socialNetwork.github))
                                    dataUpdate.github = contextData.socialNetwork.github;
                                if (notEmpty(contextData.socialNetwork.gitlab))
                                    dataUpdate.gitlab = contextData.socialNetwork.gitlab;
                                if (notEmpty(contextData.socialNetwork.linkedin))
                                    dataUpdate.linkedin = contextData.socialNetwork.linkedin;
                            }
                            dyFObj.openForm(form, "sub", dataUpdate);
                        });
                    },
                    updateProjectSInProgress: function(){
                        $(".btn-update-projectsInProgress").off().on("click", function() {
                            var form = {
                                saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
                                dynForm: {
                                    jsonSchema: {
                                        title: "Modifier / Ajouter votre Projets structurant en cours",
                                        beforeBuild: function() {
                                            uploadObj.basePath = baseUrl+"/costum/ekitia/upload-save";
                                            uploadObj.set(contextData.type,contextData.id);
                                            uploadObj.initUploader();
                                        },
                                        afterBuild: function(){
                                            $(".removeListLineBtn").off().on("click", function(){
                                                $(this).parents().eq(1).find(".qq-upload-delete-selector");
                                                $(this).parents().eq(1).fadeOut(200).remove();
                                            });
                                        },
                                        afterSave: function(data) {
                                            mylog.log("Data result", data)
                                            if (data.result && data.resultGoods && data.resultGoods.result) {
                                                dyFObj.commonAfterSave(data, function(result){});
                                            }
                                            // changeHiddenFields();
                                        },

                                        properties: {
                                            block: dyFInputs.inputHidden(),
                                            typeElement: dyFInputs.inputHidden(),
                                            isUpdate: dyFInputs.inputHidden(true),
                                            "projectsInProgress": {
                                                "label" : "Projets strucuturant en cours",
                                                "inputType" : "lists",
                                                "entries" : {
                                                    "photos" : {
                                                        "type" : "image",
                                                        "contentKey" : "slider",
                                                        "initList": <?php echo json_encode($initFiles) ?>
                                                    },
                                                    "description" : {
                                                        "type" : "textarea",
                                                        "label" : "Description"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            /*if(contextData.type == typeObj.person.col ){
                                form.dynForm.jsonSchema.properties.telegram = dyFInputs.inputText("Votre Speudo Telegram","Votre Speudo Telegram");
                            }*/

                            var dataUpdate = {
                                block: "companyProfile",
                                id: contextData.id,
                                typeElement: contextData.type,
                            };

                            if (notEmpty(contextData.projectsInProgress)) {
                                dataUpdate.projectsInProgress = contextData.projectsInProgress;
                            }
                            dyFObj.openForm(form, "sub", dataUpdate);
                        });
                    }
                },
                actions: {
                    init: function() {},
                    updateVisibility: function(elementId, collection, key, visiblity = false) {
                        let dataToSendLinks = {
                            id: elementId,
                            collection: collection,
                            path: "showDetailSection." + key,
                            value: visiblity
                        }
                        dataHelper.path2Value(dataToSendLinks, function(params) {});
                    },
                    getPerson: function(personId) {
                        var name = "";
                        ajaxPost(
                            null,
                            baseUrl + "/" + moduleId + "/element/get/type/citoyens", {
                                id: personId
                            },
                            function(data) {
                                if (data.result) {
                                    name = data.map.name;
                                }
                            },
                            null,
                            "json", {
                                async: false
                            }
                        );
                        return name
                    },
                    newAnswer: function(formId, descKey) {
                        var dataId = "";
                        ajaxPost(
                            null,
                            baseUrl + `/survey/answer/newanswer/form/${formId}`, {
                                "action": "new"
                            },
                            function(data) {
                                if (data._id) {
                                    dataId = data._id.$id;
                                    var dataToSendLinks = {
                                        id: data._id.$id,
                                        collection: "answers",
                                        path: "links.organizations.<?= (string)$element["_id"] ?>",
                                        value: {
                                            name: "<?= $element["name"] ?>",
                                            type: "<?= $element["collection"] ?>"
                                        }
                                    }
                                    element.actions.updateOrgaNumericService(formId, "CREATE");
                                    dataHelper.path2Value(dataToSendLinks, function(params) {});
                                    element.actions.addNewList(data.form, data._id.$id, data.created, data.collection, descKey, element.actions.getPerson(data.user));
                                }
                            },
                            null,
                            "json", {
                                async: false
                            }
                        )
                        return dataId;
                    },
                    addSelectedValues: function(data = []) {
                        var answerHtml = "";
                        if (Array.isArray(data), data.length > 0) {
                            for (let index = 0; index < data.length; index++) {
                                var result = data[index];
                                answerHtml += `<li class="select2-search-choice"><div>${result}</div><a href="#" class="select2-search-choice-close" tabindex="-1"></a></li>`;
                            }
                        }
                        return answerHtml;
                    },
                    generateShortDesc: function(description, maxLength) {
                        if (description.length > maxLength) {
                            return description.slice(0, maxLength) + '...';
                        }
                        return description;
                    },
                    addNewList: function(formId, ansId, created, collection, descKey, user) {
                        var listHtml = `
                        <div class="row card-vertical-container form-${formId} ans-${ansId}" ${!(($(".form-" + formId).length % 2 ) == 0) ? '': 'style="background-color: #f9f9f9;"'}>
                            <div class="col-md-4" data-desc-key="${descKey}-${ansId}" style="font-size: 16px;"> <b><i>Il n'y a pas de titre</i></b> </div>
                            <div class="col-md-4 time-${ansId}" align="center"> ${directory.showDatetimePost(collection, ansId, created, 30)} </div>
                            <div class="col-md-4 person" align="center"><i class="fa fa-user"></i><b> ${user}</b></div>
                            <div class="col-md-4" align="center"> 
                                <div class="col-md-12">
                                    <div class="social-links justify-content-center">
                                        <div class="social-btn flex-center getanswer" data-mode="w" data-description-key="${descKey}" data-form="${formId}" data-ansid="${ansId}">
                                            <i class="fa fa-pencil-square-o editdeleteicon"></i>
                                            <span style="font-size: 14px;">Éditer</span>
                                        </div>
                                        <div class="social-btn flex-center getanswer" data-mode="r" data-form="${formId}" data-ansid="${ansId}">
                                            <i class="fa fa-sticky-note-o editdeleteicon"></i><span style="font-size: 14px;">Lire</span>
                                        </div>
                                        <div class="social-btn flex-center deleteanswer" data-mode="w" data-form="${formId}" data-ansid="${ansId}">
                                            <i class="fa fa-trash-o editdeleteicon" style="color: #ff5722"></i><span style="font-size: 14px;">Effacer</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                        $(".content-" + formId).append(listHtml);
                        if ($(".empty-content-" + formId).length > 0) {
                            $(".empty-content-" + formId).remove();
                        }
                        $('[data-dismiss="modal"]').click(function() {
                            element.events.init();
                        })
                    },
                    createForm: function(formContainer, formId, descKey) {
                        var formAnswerId = element.actions.newAnswer(formId, descKey);
                        $(formContainer).html(element.actions.generateRenderForm(formId, formAnswerId));
                        return formAnswerId;
                    },
                    generateRenderForm: function(formId, answerId, mode = "w") {
                        var formAjaxPath = baseUrl + '/survey/answer/index/id/' + answerId + '/form/' + formId + '/mode/' + mode;
                        var renderHtml = "";
                        var formRender = "";
                        ajaxPost(
                            null,
                            formAjaxPath, {
                                url: window.location.href
                            },
                            function(data) {
                                let valueToSet = {};
                                renderHtml = data;
                            },
                            "html",
                            null, {
                                async: false
                            }
                        );
                        return renderHtml;
                    },
                    deleteAnswer: function(answerId, formId) {
                        bootbox.confirm(trad.areyousuretodelete,
                            function(result) {
                                if (result) {
                                    getAjax("", baseUrl + "/survey/co/delete/id/" + answerId, function() {
                                        element.actions.updateOrgaNumericService(formId, "DELETE");
                                    }, "html");
                                    $(".ans-" + answerId).remove();
                                    if ($(".form-" + formId).length == 0)
                                        $(".content-" + formId).html('<div class="more no-padding empty-content-' + formId + '"><p><i>Non renseigné(e)</i></p></div>');
                                }
                            });
                    },
                    updateOrgaNumericService:function(formId, action){
                        let valueToSet = {};
                        if(action=="DELETE"){
                            countAnswer[formId]=countAnswer[formId]-1;
                        }else if(action=="CREATE"){
                            countAnswer[formId] = countAnswer[formId]+1;
                        }
                        valueToSet["numericService"] = Object.keys(countAnswer).filter(function(v, k){return countAnswer[v]>0});
                        if(valueToSet["numericService"].length==0){
                            valueToSet["numericService"]=null;
                        }
                        dataHelper.path2Value({
                                id: contextData.id,
                                collection: contextData.collection,
                                path: "allToRoot",
                                value: valueToSet
                            }, function(){});
                    }
                }
            }
            element.init();
        })


        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $(".central-section").attr("class", "col-xs-12 col-lg-12 central-section");
        })
    </script>

<?php } else { ?>
    <div class="text-center text-danger margin-top-35">
        <i class="fa fa-lock fa-4x "></i>
        <h1>Accès Non Autorisé</h1>
        <p>Il faut être membre de la plateforme</p>
    </div>
<?php } ?>