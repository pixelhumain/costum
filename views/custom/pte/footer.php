<footer class="text-center col-xs-12 pull-left no-padding bg-purple">
     <div class="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center col-footer col-footer-step">
                    <a href="#mentions" class="lbh text-white"><?php echo Yii::t("home","Mentions légales") ?></a><br><br>
                    <a href="https://www.pacte-transition.org/" class="text-white">PACTE TRANSITION.org</a><br><br>
                </div>
                <div class="col-xs-12 col-sm-6 col-footer">
                    <span style="font-size:20px;"><a href="mailto:collectifecocitoyenmontreuil@initiative.place" style="color: #FFF !important;">Contactez nous</a></span>

                </div>
            </div>
        </div>
    </div>
</footer>
