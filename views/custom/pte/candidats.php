<style type="text/css">
    	.text-explain{
    		font-size: 22px;
    	}
    </style>
<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-10">Les candidats à la mairie de Montreuil-Sur-Mer</h3>
        	<hr>
        </div>

    <div class="col-xs-12">
        
        	<span class="col-xs-12 text-center">        
             <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/pte/banner.png'> 
    		</span>

    </div> 
     
     <div class="col-xs-12">
        
        	<span style="margin-top:20px;" class="col-xs-12 text-left text-explain">        

Ici sera prochainement listé l'ensemble des candidat.e.s à la mairie de Montreuil-Sur-Mer. Ces derni.er.ère.s devront statuer sur leur engagement et se positionner en faveur de la transition écologique en ratifiant le pacte co-écrit par 160 montreuillois ou non. Nous  vous tiendrons informé.e.s sur les engagements des candidats en rapport aux propositions faites par le collectif éco-citoyen.
<br/><br/>

    		</span>
    </div> 
        
    </div>
</div>
