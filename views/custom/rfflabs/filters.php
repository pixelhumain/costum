<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}

.subDropdown{
	position:absolute;
	left:100%;
	/*background: var(--main2);*/
	display:none;
	/*overflow-y: scroll;*/
	/*top: 0;*/
     bottom: 0; 
    max-height: 100%;
    overflow: auto;
    width:100%;
}

.subDropdown:hover{
	display: block;
}

.subDropdown button{
	width: 100% !important;
    background-color: white !important;
    color: var(--main2) !important;
    border-top: solid 1px var(--main2) !important;
    text-align:left;
}

.subDropdown button:hover{
    filter: brightness(0.9);
}


.mega-menu-container button:hover + .subDropdown{
    display: block;
}

/*.mega-menu-container button:hover .{
    position: absolute;
}
*/
/*.mega-menu-container button:hover + .subDropdown{
	display:block;
}
}*/
.mega-menu-container > button:hover{
	filter: brightness(0.7);
}	


/*.parent-button{
	display:block;
}

.subDropdown:hover{
	display:block;
}

.subDropdown:hover button{
	display:block;
	color:var(--main2);
}*/


.dropdown-menu.dropdown-menu-large{
	position:fixed !important;
    overflow: scroll !important;
    width: 94% !important;
    max-height: -webkit-fill-available;
    left : 65px !important;
    top:115px !important;
    height: 70%;
    padding:10px !important;
}    

.dropdown-menu-large > .col > div{
	    float: left;
    /* background-color: #c8caf1; */
    margin-bottom: 20px;
    height: -webkit-fill-available;
    padding: 3px;
    border: solid 2px #c8caf1;
}

#filters-nav .dropdown .dropdown-menu-large button{
	background-color: var(--main1);
	color:black;
	text-align: left;
}


#filters-nav .dropdown .dropdown-menu-large button:hover{
	background-color: #c8caf1;
}

</style>

<script type="text/javascript">
	var networkList=costum.lists.fablabNetwork.concat(costum.lists.thirdPlaceNetwork);

    var equipmentList={};
	$.each(costum.equipment,function(mat,mach){
		equipmentList[mat]={};
		$.each(mach,function(machK,machV){
			equipmentList[mat][machK] = {
			label : machV,
			field : "equipment."+mat+"."+machV,
			value : true
		    };
	    });	
	});


	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : true
            }
        },
        header:{
	 		options:{
	 			left:{
	 				classes :"col-xs-6 no-padding",
	 				group:{
						count : false
					}
	 			},
	 			right:{
	 				classes :"col-xs-5 no-padding",
	 				group:{
						map : true
					}
	 			}
	 		}
	 	},	
	 // 		views : {
	 // 			map : function(fObj,v){
		// 			return  '<button class="btn-show-map-search pull-right bg-main1" style="" title="yoyo" alt="yoyoyo">'+
		// 						'<i class="fa fa-map-marker"></i> heyyou</button>';
		// 		}
		// 	}
		// },	
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"],
	 		filters : {
	 			'$or' : {
	 			    updated : {'$gt':1668587064},
	 			    created : {'$gt':1668587064},
	 			    modified : {'$gt':1668587064}
	 			}    
	 		}
	 	},
	 	filters : {
	 		scope :true,
	 		scopeList : {
	 			params : {
	 				countryCode : ["FR"], 
	 				level : ["3"],
	 			}
	 		},
	 		equipement:{
				view : "subDropdown",
	 			type : "filters",
	 			field : "equipment",
	 			remove0: false,
	 			countResults: true,
	 			name : "Equipement",
	 			activateCounter:true,
                countFieldPath: "equipment",
	 			event : "exists",
	 			keyValue: true,
	 			typeList:'object',
	 			list : equipmentList
			},
	 		network : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Réseau d'appartenance",
	 			// action : "tags",
	 			event : "tags",
	 			list :  costum.lists.allNetwork
	 		},
	 		contributors : {
	 			view : "dropdownList",
	 		    type : "tags",
	 			name : "Participants au projet ...",
	 			// action : "filters",
	 			// typeList : "object",
	 			event : "tags",
	 			list : costum.nationalRffProjects
	 		}
	 		// services:{
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Services",
	 		// 	event : "tags",
	 		// 	list : costum.lists.services
	 		// }
	 		// // greeting : {
	 		// 	view : "selectList",
	 		// 	type : "tags",
	 		// 	name : "Accueil",
	 		// 	event : "tags",
	 		// 	list : costum.lists.greeting
	 		// },
	 		// manageModel : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Portage",
	 		// 	event : "tags",
	 		// 	list : costum.lists.manageModel
	 		// },
	 		// state : {
	 		// 	view : "selectList",
	 		// 	type : "tags",
	 		// 	name : "Etat",
	 		// 	event : "tags",
	 		// 	list : costum.lists.state
	 		// },
	 		// spaceSize : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Taille",
	 		// 	event : "tags",
	 		// 	list : costum.lists.spaceSize
	 		// },
	 		
	 		// compagnon : {
	 		// 	view : "tags",
	 		// 	type : "tags",
	 		// 	name : "Compagnons",
	 		// 	event : "tags",
	 		// 	list : costum.lists.compagnon
	 		// },
	 		// certification : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Lauréats Fabriques",
	 		// 	event : "tags",
	 		// 	list : costum.lists.certification
	 		// },
	 		// network : {
	 		// 	view : "tags",
	 		// 	type : "category",
	 		// 	name : "Réseaux",
	 		// 	event : "selectList",
	 		// 	field : "category"
	 		// }
	 	}
	};

	if (typeof upperLevelId !="undefined")
		paramsFilter.filters.scopeList.upperLevelId=upperLevelId;

	//function lazyFilters(time){
	  //if(typeof searchObj != "undefined" )
	    //filterGroup = searchObj.init(paramsFilter);
	  //else
	    //setTimeout(function(){
	      //lazyFilters(time+200)
	    //}, time);
	//}
var filterSearch={};
	jQuery(document).ready(function() {
        searchObj.filters.views.subDropdown=function(k,v, fObj){
        			var labelStr=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
        			if(!exists(fObj.filters.views.countFieldPath))
        				fObj.filters.views.countFieldPath = [];

        			if(exists(v.countFieldPath))
        				fObj.filters.views.countFieldPath.push(v.countFieldPath);
        			var megaMenu = '';
        			if(typeof v.list === 'object' && v.list !== null && !Array.isArray(v.list)){
        				megaMenu = '<div class="dropdown"><a href="javascript:;"  class="dropdown-toggle menu-button btn-menu parent-menu-subDropdown"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="theme" data-toggle="tooltip" data-placement="bottom">'+labelStr+'<b class="caret"></b></a><div class="dropdown-menu" style="overflow-y: auto;" aria-labelledby="dropdownTypes"><div class="mega-menu-container" style="width:100%">';
        				for (const [keyT, valueT] of Object.entries(v.list)) {
        		        	megaMenu += `<button type="button" class="col-xs-12">${keyT} ></button><div class="subDropdown">`;
        		        	for (const [keyChild, valueChild] of Object.entries(valueT)){
        		        		var newValue = {list:{}, event:v.event, activateCounter:(typeof v.activeCounter != "undefined")?v.activateCounter:true, remove0:(v.remove0)?v.remove0:false};
        						newValue.list[keyChild] = valueChild;
        		        		if(typeof v.field != "undefined"){
        		        			newValue["field"] = v.field;
        		        		}
        		        		if(typeof v.type != "undefined"){
        		        			newValue["type"] = v.type;
        		        		}
        		        		if(typeof v.typeList != "undefined"){
        		        			newValue["typeList"] = v.typeList;
        		        		}
        		        		mylog.log("newValue",newValue);
        			            megaMenu+=`${fObj.filters.views.buttonList(k, newValue, "", fObj)}`;
        			        }
        		        	megaMenu += '<br/></div>';
        			    }

        			    megaMenu += `</div></div></div>`;
        	        }else{

        				var classContainer= (typeof v.classDom != "undefined") ? v.classDom : "";
        				var classButton= (typeof v.classList != "undefined") ? v.classList : "col-xs-12";
        	            megaMenu+='<li class="dropdown">'+
        						'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
        							labelStr+' <i class="fa fa-angle-down margin-left-5"></i>'+
        						'</a>'+
        						'<div class="dropdown-menu arrow_box '+classContainer+'" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
        							'<div class="parent-dropdown">'+
        									fObj.filters.views.buttonList(k, v, classButton , fObj)+
        							'</div>'+
        						'</div>'+
        					'</li>';
        	        }
        			return megaMenu;        	
        };


		  filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l'annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();

		$(".subDropdown button").hover(
            function(){
            	//$(this).next().css("display","block");
            	$(this).parent().prev().css("filter","brightness(0.7)");
            },
            function(){
            	$(this).parent().prev().css("filter","");
            	//$(this).next().css("display","none");
            }  
	    );

		// var typeObjOrga=costum.typeObj;
		// delete typeObjOrga.organization;
		// delete typeObjOrga.projects;
		// delete typeObjOrga.events;
		// costum.init(typeObjOrga);

		
	//	lazyFilters(0);
		
	});

</script>



