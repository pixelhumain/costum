<?php
    $cssAnsScriptFilesModule = array(
        '/js/default/preview.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $auth = Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"]) ? true : false;
    $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
    $albumCostum = PHDB::findOne(Folder::COLLECTION,array("name" => "Costum présentation", "contextId" => (String) $element["_id"]) );
    $imageCostum =Array();
    // if (isset($albumCostum)) {
    //     $imageCostum = Document::getListDocumentsWhere(array(
    //     "id"=>(String) $element["_id"],
    //     "type"=>'organizations',
    //     "folderId"=>(String)$albumCostum["_id"]
    //     ),"image");
    // }  else {
    //     $imageCostum =[];
    // }  



    $imageCostum = Document::getListDocumentsWhere(
        array(
            "id"=>  (String)$element["_id"],
            "type"=> $element["collection"],
            "subKey"=>"costumPrez"
        ),"image"
    );
?>
<style>
    .contain-preview { 
        margin: 20px; 
    }

    .preview-element .timeline-block {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
        background-color: white;
        margin-bottom: 10px;
    }

    .preview-element .timeline-block:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: transparent; 
        left: 0%;
        margin-left: -1.5px;
        border-left: 2px dashed #8bc34a;
    }

    .preview-element .timeline-block > li {
        margin-bottom: 0;
        margin-right: 20px;
        position: relative;
    }

    .preview-element .timeline-block > li:before,
    .preview-element .timeline-block > li:after {
        content: " ";
        display: table;
    }

    .preview-element .timeline-block > li:after {
        clear: both;
    }

    .preview-element .timeline-block > li:before,
    .preview-element .timeline-block > li:after {
        content: " ";
        display: table;
    }

    .preview-element .timeline-block > li:after {
        clear: both;
    }

    .preview-element .timeline-block > li > .timeline-panel {
        width: 94%;
        float: left;
        border-radius: 2px;
        padding: 10px;
        position: relative;
        border: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .preview-element .timeline-block > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid red;
        border-right: 0 solid red;
        border-bottom: 15px solid transparent;
        content: " ";
    }
    .preview-element .timeline-block > li.timeline-inverted, .timeline-container-offset {
        float: none;
    }
    .preview-element .timeline-block > li > .timeline-badge {
        color: #8bc34a;
        width: 40px;
        height: 40px;
        line-height: 3px;
        font-size: 28px;
        padding: 5px;
        text-align: center;
        position: absolute;
        top: 25px;
        left: 0%;
        margin-left: -20px;
        background-color: white;
        border: 2px solid #8bc34a;
        z-index: 100;
        border-radius: 50%;
    }

    .preview-element .timeline-block > li.timeline-inverted > .timeline-panel {
        float: right;
    }
    .preview-element .timeline-block > li.timeline-inverted > .timeline-panel:before {
        display: none;
    }

    .preview-element .timeline-block > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }


    .preview-element  .timeline-block .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .preview-element .timeline-block .timeline-body > p,
    .preview-element .timeline-block .timeline-body > ul {
        margin-bottom: 0;
    }
    .preview-element .timeline-block .timeline-body {
        min-height: 70px;
    }

    .timeline-block .timeline-body > p  {
        font-size: 16px;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .timeline-block > li > .timeline-badge {
            color: #49cbe3;
            width: 20px;
            height: 20px;
            font-size: 32px;
            padding: 8px;
            margin-left: -24px;
        }
    }


    @media (max-width: 767px) {
        ul.timeline-block:before {
            left: 25px;
        }
        .timeline-block > li.timeline-inverted > .timeline-panel:before {
            display: block;
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }
        .timeline-block > li > .timeline-badge {
            width: 50px;
            height: 50px;
            display: block;
            line-height: 35px;
            font-size: 1.4em;
            margin-left: 12px!important;
            padding: 4px;
        }

        ul.timeline-block > li > .timeline-panel {
            width: calc(100% - 60px);
            width: -moz-calc(100% - 60px);
            width: -webkit-calc(100% - 60px);
        }

        ul.timeline-block > li > .timeline-badge {
            left: 0px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline-block > li > .timeline-panel {
            float: right;
        }

        ul.timeline-block > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline-block > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }
    .preview-element .timeline-block .timeline-heading h4.timeline-title, 
    .preview-element .timeline-block .timeline-body p  {
        color: #3f4d57;
        font-weight: bold;
        text-transform: none;
    }
    #modal-preview-coop{
        overflow-y: auto;
    }
    .preview-element   h3,
    .preview-element   h4{
        font-weight: bold ;
    }
    .preview-element .profil-element{
        width: 120px;
        height: auto;
        object-fit: contain;
    }
    .name-element{
        text-transform: none;
    }
    .preview-element .swiper-slide img{
        width: 90%;
        height: auto;
        max-height: 600px;
        object-fit: contain;
    }
    .preview-element .swiper-slide{
        align-self: center;
        text-align : center;
    }
    .contain-preview .swiper-container-horizontal>.swiper-pagination-bullets, 
    .contain-preview .swiper-pagination-custom, .swiper-pagination-fraction{
        bottom: -20px;
    }
    .preview-element .view-costum{
        color: white;
        background-color: #8bc34a;
        border-color: #8bc34a;
        font-weight: bold;
        font-size: 20px;
    }
    .preview-element .view-costum:hover{
        background-color:  #3f4d57;
        border-color: #3f4d57;
    }
    .preview-element .swiper-button-prev{
        left: -20px; 
    }
    .preview-element .swiper-button-prev:after,
    .preview-element .swiper-button-next:after {
        font-size: 25px;
    }
    .preview-element .btn-close-preview .fa,
    .preview-element .btn-edit-preview .fa{
        font-size: 20px;
    }
    .content-timeline{
        background : white; 
        min-height : 600px;
        position : relative;
        z-index : 10;
    }
</style>
<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="col-xs-12 no-padding preview-element">
    <div class="col-xs-12 padding-10 toolsMenu">
        <?php if(isset($element["costum"]["logo"])){?>
            <img class="profil-element" src="<?=$element["costum"]["logo"]?>">
        <?php } else {?>
            <img class="profil-element" src="<?= $element["profilImageUrl"]?>">
        <?php } ?>
        <button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <a href="<?= $baseUrl?>/costum/co/index/slug/<?= @$element["slug"]?>" target="_blank" class="btn pull-right margin-right-10 view-costum">Visiter le site</a>
        <?php if (isset($canEdit) && $canEdit) { ?>
            <button class="btn btn-default pull-right margin-right-10 text-black btn-edit-preview" data-type="<?php echo $element["collection"] ?>" data-id="<?php echo (String)$element["_id"] ?>">
                <i class="fa fa-pencil"></i>
            </button>
        <?php } ?>
    </div>
    <div class="col-xs-12 no-padding" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;"></div>
    <div class="col-xs-12 contain-preview  no-padding"> 
        <div class="col-xs-12 col-lg-6 no-padding">
            <h2 class="name-element">  <?= $element["name"]?></h2>
            <?php if(isset($element["shortDescription"])){?>
                <div class="markdown"><?= $element["shortDescription"]?></div> 
            <?php }?>
            <?php if(count($imageCostum) > 0 ){?>
            <div class="swiper-img-element">
                <div class="swiper-wrapper">
                    <?php foreach($imageCostum as $ki => $vi){?>
                        <div class="swiper-slide">
                            <img src="<?php echo $vi["imagePath"]?>">
                        </div>
                    <?php }?>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <?php }?>
        </div>
        <div class="col-xs-12 col-lg-6 no-padding">
            <h3> Fonctionalités</h3>
            <div class="content-timeline">
                <?php if(isset($element["features"])){ ?>
                    <ul class="timeline timeline-block z-index-10 ">
                            <?php foreach($element["features"] as $ff => $vf){?> 
                                <li class="timeline-inverted">
                                    <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title"><?php echo @$vf["title"]?></h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p class=""><?php echo @$vf["description"]?></p>
                                        </div>
                                    </div>
                                </li>   
                            <?php } ?>
                    </ul>
                <?php }else { ?>
                <ul class="timeline timeline-block z-index-10 ">
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Fonctionalité 1</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="">Add multiple feature items, set different icons or images for each feature and also give custom links if needed.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Fonctionalité 2</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="">Add multiple feature items, set different icons or images for each feature and also give custom links if needed.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Fonctionalité 3</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="">Add multiple feature items, set different icons or images for each feature and also give custom links if needed.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Fonctionalité 4</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="">Add multiple feature items, set different icons or images for each feature and also give custom links if needed.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge warning"><i class="fa fa-check"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="timeline-title">Fonctionalité 5</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="">Add multiple feature items, set different icons or images for each feature and also give custom links if needed.</p>
                            </div>
                        </div>
                    </li>
                </ul> 

                <?php } ?>

            </div>
        </div>
        <div class="col-xs-12 col-lg-12"> 
            <?php if(isset($element["description"])){?>
                <div class="markdown"><?php //echo $element["description"]?></div> 
            <?php }?>
        </div></div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
      
        $.each($(".markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html()); 
            $(v).html(descHtml);
        });
        var options = {
            onlyExternal: true,
            slidesPerView: 1,
            spaceBetween: 10,
            keyboard: {
                enabled: true,
            },
            autoplay : {
                delay: 3000,
                disableOnInteraction: true,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
            }
        };
        var swiper = new Swiper(".swiper-img-element", options);

        $(".btn-edit-preview").off().on("click",function(){
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
            var typeElem=$(this).data("type");
            var idElem=$(this).data("id"); 
			var dynfElmt = {
				"beforeBuild": {
					"properties": {
						"name": {
							"label": "Dénomination",
							"inputType": "text",
							"placeholder": "Raison Sociale de l’organisme"    
						},
						"costumPrez": {
							// "label":"Télécharger votre LOGO en Haute Définition (HD)",
							// "rules" : {
							// 	"required": true
							// }
                            "inputType" : "uploader",
                            "docType": "image",
                            "contentKey":"slider",
                            "endPoint": "/subKey/costumPrez",
                            "domElement" : "costumPrez",
                            "filetypes": ["jpeg", "jpg", "gif", "png"],
                            "label": "Photo de présentation du costum", 
                            "showUploadBtn": false,
                            initList : <?php echo json_encode($imageCostum) ?>
                            
						},
                        "features" : {
                            inputType : "lists",
                            label : "Fonctionalités",
                            entries:{
                                title:{
                                    "label":"Titre",
                                    "type":"text",
                                    "class":"col-xs-11"
                                },
                                description:{
                                    "label":"Description",
                                    "type":"textarea",
                                    "markdown":true,
                                    "class":"col-xs-11"
                                }
                            }                            
                        },
					}
				}, 
				"afterBuild" :{},
				"onLoads" : {
					"onload" :{}
				},
				"onload": {
					"actions": {
						"setTitle": "PRÉSENTEZ VOUS",
						"html": {
							"infocustom": "<br/><?php echo Yii::t('cms', 'Fill in the field')?>"
						},
						"hide": {
							"shortDescriptiontextarea": 1,  
							"nametext": 1,  
							"breadcrumbcustom" : 1,
							"parentfinder" : 1,                        
							"urltext": 1,
							"typeselect": 1,
							"tagstags": 1,
							"formLocalityformLocality": 1,
							"locationlocation": 1,
							"imageuploader": 1,
							"emailtext": 1,
							"roleselect": 1
						}
					}
				}
			}
            dyFObj.editElement(typeElem,idElem, null,dynfElmt);
                        
        });
    });
</script>