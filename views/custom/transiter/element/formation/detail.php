<?php 
    $cssAnsScriptFilesModule = array(
        '/css/element/about.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?>
<style>
    .btn-network::before {
        background: var(--primary-color) !important;
    }

    .btn-network {
        box-shadow: 0 5px 15px -5px var(--primary-color) !important;
        border: 1px solid var(--primary-color) !important;
    }

    .sub {
        font-size: 14pt !important
    }

    .bordered {
        border: 1px solid #eee;
        padding: 5px;
        margin-right: 2px;
        margin-left: 2px;
        margin-bottom: 30px;
    }
    .projectInProgressImg{
        width: 100%;
    }
    .border-bottom{
        border-bottom: 1px solid #ddd;
    }
    .fa.fa-times{
        color:white !important
    }
    #shortDescriptionAbout{
        word-wrap:break-word;
        text-align: justify;
    }

    .margin-bottom-0{
        margin-bottom: 0px
    }

    .gallery-image-view {
        position: relative;
        display: inline;
    }
    #image-visualizer {
        z-index: 99999999;
    }
    #image-visualizer img {
        width: 100%;
        height: auto;
    }

    @media (min-width: 768px){
        .central-section .modal-dialog{
            width: 60% !important;
            margin: auto;
            top: 20% !important;
            z-index: 100002;
        }
    }
    .img-round-profil{

        margin-top: 3px;
        height: 40px;
        width: 40px;
        object-fit: contain;
        border: 2px solid var(--primary-color);
        border-radius: 50%;
    }
    #pod-info-Description{
        margin-bottom: 0;
    }
    .text-nv-3 {
        font-size: 28px;
        font-weight: 500;
        letter-spacing: 0;
        line-height: 1.5em;
        padding-bottom: 15px;
        position: relative;
    }
    .text-nv-3:before {
        content: "";
        position: absolute;
        left: 0 !important;
        margin-left: 10px;
        color: var(--primary-color);
        bottom: 0;
        height: 5px;
        border-radius: 0;
        width: 55px;
        background-color: var(--primary-color);
    }
    .text-nv-3:after {
        content: "";
        position: absolute;
        left: 10px;
        bottom: 2px;
        height: 1px;
        width: 95%;
        max-width: 255px;
        background-color: var(--primary-color);
    }
    .content-section {
        box-shadow: 0px 4px 34px 0px rgba(0, 0, 28, 0.1);
        padding: 10px;
        margin: 10px auto;
    }
</style>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1">
    <!--<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 about-section1"> -->
    <div class="about-xs-completed visible-xs">
        <div class="about-avatar">
            <?php
            echo $this->renderPartial(
                'co2.views.pod.fileupload',
                array(
                    "podId" => "modalDash",
                    "element" => $element,
                    "edit" => $edit,
                    "openEdition" => false
                )
            ); ?>
        </div>
        <div class="well col-xs-12 net-well no-padding">

            <div id="menuCommunity" class="col-md-12 col-sm-12 col-xs-12 numberCommunity">

            </div>

            <div class="animated bounceInRight">
                <?php if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] != $invitedMe["invitorId"]) { ?>
                <?php echo $this->renderPartial(
                        'co2.views.element.menus.answerInvite',
                        array(
                            "invitedMe"      => $invitedMe,
                            "element"   => $element
                        )
                    );
                } else if (@Yii::app()->session["userId"] && @$invitedMe["invitorId"] && Yii::app()->session["userId"] == $invitedMe["invitorId"]) { ?>
                    <div class="containInvitation">
                        <div class="statuInvitation">
                            <?php echo Yii::t("common", "Friend request sent") ?>
                            <?php
                            $inviteCancel = "Cancel";
                            $option = null;
                            $msgRefuse = Yii::t("common", "Are you sure to cancel this invitation");

                            echo
                            '<br>' .
                                '<a class="btn btn-xs tooltips btn-refuse margin-left-5" href="javascript:links.disconnect(\'' . $element["collection"] . '\',\'' . (string)$element["_id"] . '\',\'' . Yii::app()->session["userId"] . '\',\'' . Person::COLLECTION . '\',\'' . Element::$connectTypes[$element["collection"]] . '\',null,\'' . $option . '\',\'' . $msgRefuse . '\')" data-placement="bottom" data-original-title="' . Yii::t("common", "Not interested by the invitation") . '">' .
                                '<i class="fa fa-remove"></i> ' . Yii::t("common", $inviteCancel) .
                                '</a>';

                            ?>
                        </div>
                    </div>
                <?php } ?>
            </div>




            <div class="section-Community">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading no-padding" id="headingOne" role="tab">
                            <h4 class="panel-title">
                                <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Communauté<i class="pull-right fa fa-chevron-down"></i>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">

                            <div class="panel-body">
                                <?php if ($type == Person::COLLECTION) { ?>
                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["follows"])) {
                                                echo count($element["links"]["follows"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Follows") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["friends"])) {
                                                echo count($element["links"]["friends"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Friends") ?> </span>
                                    </div>

                                    <div class="col-xs-4 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["memberOf"])) {
                                                echo count($element["links"]["memberOf"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Organizations") ?> </span>
                                    </div>
                                <?php } ?>

                                <?php if ($type == Organization::COLLECTION) { ?>
                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["members"])) {
                                                echo count($element["links"]["members"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Members") ?> </span>
                                    </div>

                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["followers"])) {
                                                echo count($element["links"]["followers"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                    </div>

                                <?php } ?>

                                <?php if ($type == Project::COLLECTION) { ?>
                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">

                                            <?php
                                            if (isset($element["links"]["contributors"])) {
                                                echo count($element["links"]["contributors"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Contributors") ?> </span>
                                    </div>

                                    <div class="col-xs-6 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["followers"])) {
                                                echo count($element["links"]["followers"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Followers") ?> </span>
                                    </div>

                                <?php } ?>

                                <?php if ($type == Event::COLLECTION) { ?>
                                    <div class="col-xs-12 no-padding">
                                        <span class="number-Community">
                                            <?php if (isset($element["links"]["attendees"])) {
                                                echo count($element["links"]["attendees"]);
                                            } else {
                                                echo 0;
                                            } ?>
                                        </span><br>
                                        <span class="label-Community"> <?php echo Yii::t("common", "Attendees") ?> </span>
                                    </div>

                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="pod-info-Description content-section" id="pod-info-Description">
        <span class="text-nv-3">
            <i class="fa fa-file-text-o"></i> Description
        </span>
        <div id="contenuDesc">
            <?php if (!((empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) { ?>
                <div class="contentInformation margin-20"  style="font-size: 18px; text-align:justify;">
                    <p id="shortDescriptionAbout" name="shortDescriptionAbout" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                    <p id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit" class="hidden"><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></p>
                </div>
            <?php }
            if (!((empty($element["description"])) && Yii::app()->session["userId"] == null)) { ?>

                <div class="contentInformation margin-20">
                    <div class="more no-padding" id="descriptionAbout" style="font-size:18px; text-align:justify; white-space:pre-wrap"><?php echo (@$element["description"]) ? $element["description"] : '<i></i>'; ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div> 
    <div style="display:flex;flex-direction: row; flex-wrap: wrap;">
        <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-left:0px">
            <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i> &nbsp;  Centres de formation                
                </span>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                    <div class="contentInformation margin-10" style="font-size: 12pt;"> 
                        <?php if(isset($element["parent"])){ ?>   
                            <?php foreach($element["parent"] as $k => $v){ ?>
                                    <a href="#page.type.<?php echo $v["type"] ?>.id.<?php echo $k; ?>" target="_blank" title="<?php echo $v["name"] ?>">
                                        <img class="img-round-profil" alt="image" src="<?php echo $v["profilThumbImageUrl"] ?>"><span><?php echo $v["name"]?></span><br>
                                    </a> 
                            <?php } ?>  
                        <?php } ?>     
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-right:0px">
            <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i> &nbsp;  Les partenaires                
                </span>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                    <div class="contentInformation margin-10" style="font-size: 12pt;">
                        <?php if(isset($element["partner"])){?>   
                            <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>(String)$element["_id"], "contextType"=>$element["collection"])); ?> 
                        <?php } ?>  
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-left : 0px">
            <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i> &nbsp;  Public cible                
                </span>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                    <div class="contentInformation margin-10" style="font-size: 12pt;">
                    <?php if (isset($element["publicCible"])){ ?>       
                        <?php echo $element["publicCible"] ?> 
                    <?php }?> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-right:0px">
            <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i> &nbsp;  Méthodes pédagogiques                
                </span>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                    <div class="contentInformation margin-10" style="font-size: 12pt;">
                        <?php if (isset($element["teachingMethods"])){ ?>       
                            <?php echo $element["teachingMethods"] ?>
                        <?php }?>  
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-left:0px">
            <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                <span class="text-nv-3">
                    <i class="fa fa-file-text-o"></i> &nbsp;  Evaluation                
                </span>
                <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                    <div class="contentInformation margin-10" style="font-size: 12pt;">   
                    <?php if (isset($element["evaluation"])){ ?>    
                        <?php echo $element["evaluation"] ?>  
                    <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (isset($element["certificationTraining"]) && $element["certificationTraining"] == "true" && isset($element["certificationsAwards"])){ ?>       
            <div class="col-md-6 col-xs-12 margin-bottom-15" style="padding-right:0px">
                <div class="section pod-info-projectsInProgress content-section" id="pod-info-projectsInProgress" style="height: 100%;">
                    <span class="text-nv-3">
                        <i class="fa fa-file-text-o"></i> &nbsp;  Nom de la certification                
                    </span>
                    <div id="contenuProjectsInProgress" class="left-2 margin-top-20">
                        <div class="contentInformation margin-10" style="font-size: 12pt;">
                                <?php echo $element["certificationsAwards"] ?> 
                        </div>
                    </div>
                </div> 
            </div>
        <?php }?> 
    </div>      
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2">
    <div class="section content-section light-bg pod-infoGeneral" id="pod-infoGeneral">
        <div class="row profil-title-informations">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span class="text-nv-3">
                    <i class="fa fa-address-card-o"></i> Accessibilité
                </span>
            </div>
        </div>
        <hr class="line-hr">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
                <?php
                    $resp = '<i>' . Yii::t("common", "Not specified") . '</i>';
                    $occupation = '<i>' . Yii::t("common", "Not specified") . '</i>';
                    $resp = (@$element["referentPersonFirstName"]) ? "" . $element["referentPersonFirstName"] : $resp;
                    $occupation = (@$element["function"]) ? $element["function"] : $occupation;
                    if (isset($element["referentPersonName"]))
                        $resp = $resp . " " . $element["referentPersonName"];
                ?>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card features">
                            <div class="card-body">
                                <?php if ($type != Poi::COLLECTION) { ?>
                                    <div class="media contentInformation">
                                        <span class="ti-2x mr-3"><i class="fa fa-desktop gradient-fill"></i></span>
                                        <div class="media-body">
                                            <h6>URL principale</h6>
                                            <p class="padding-top-10" id="webAbout">
                                                <?php
                                                if (@$element["url"]) {
                                                    $scheme = ((!preg_match("~^(?:f|ht)tps?://~i", $element["url"])) ? 'http://' : "");
                                                    echo '<a data-url="' . $element['url'] . '" href="' . $scheme . $element['url'] . '" target="_blank" id="url" style="cursor:pointer;">' . $element["url"] . '</a>';
                                                } else
                                                    echo '<i>' . Yii::t("common", "Not specified") . '</i>'; ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="card features">
                            <div class="card-body">
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-at gradient-fill"></i></span>
                                    <div class="media-body">
                                        <h6>EMAIL</h6>
                                        <p class="padding-top-10" data-email="<?php echo (@$element["email"]) ? "" . $element["email"] : ""; ?>" id="email"><?php echo (@$element["email"]) ? $element["email"]  : '<i>' . Yii::t("common", "Not specified") . '</i>'; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 margin-bottom-20">
                        <div class="card features">
                            <div class="card-body">
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-link gradient-fill"></i></span>
                                    <div class="media-body">
                                        <h6>Liens vers la documentation, le programme</h6>
                                        <?php 
                                        if(isset($element["urlsDoc"]) ){
                                            $urlsDoc="<ul class ='text-highlight' >";
                                            foreach($element["urlsDoc"] as $ind=>$l){
                                                if($l != ""){
                                                    $urlsDoc.="<li><a href='".$l."' target='_blank' class='content'>".$l."</a></li>";
                                                }

                                            } 
                                            $urlsDoc.="</ul>"?>; 
                                            
                                            <p class="padding-top-10"  id="urlsDoc"><?= $urlsDoc ?></p> 
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
    <div class="section content-section light-bg pod-address" id="pod-address">
        <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit, "openEdition" => $openEdition)); ?>
    </div>
</div>

<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function() {
        afficheMap();

    });
    var mapAbout = {};
    function afficheMap() {
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if (typeof contextData.geo == "undefined") {
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container: "divMapContent",
            latLon: contextData.geo,
            activeCluster: false,
            zoom: 16,
            activePopUp: false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt: {
                id: contextData.id,
                type: contextData.type,
                geo: contextData.geo
            },
            center: true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };
    jQuery(document).ready(function() {
        $(".listsItemsPod").removeClass("text-center");
        $(".listsItemsPod").removeClass("col-xs-12");
        inintDescs();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function() {
            communecterUser();
        });

        $(".btn-update-geopos").click(function() {
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function() {
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function() {
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function() {
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function() {
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function() {
            findGeoPosByAddress();
        });

        coInterface.bindLBHLinks();

    });

    function inintDescs() {
        mylog.log("inintDescs");
        if ($("#descriptionAbout").length > 0) {
            if (canEdit == true || openEdition == true)
                descHtmlToMarkdown();
            mylog.log("after");
            mylog.log("inintDescs", $("#descriptionAbout").html());
            var descHtml = "<i>" + trad.notSpecified + "</i>";
            if ($("#descriptionAbout").html().length > 0) {
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html());
            }
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
        }
    }

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $(".central-section").attr("class", "col-xs-12 col-lg-12 central-section");
    })
</script>
