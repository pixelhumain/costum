<style type="text/css">
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>

<?php 

    use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
    use Organization, Project, Event;

    // if(isset($context)){
    //     $element = $context;
    // }else if(!isset($costum)){
    //     $element = CacheHelper::getCostum();
    // }
    if(isset($context) && $context!= ""){
        $element = $context;
    }else if(!isset($costum)){
        $element = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
        if($element["type"] && in_array($element["collection"], [Project::COLLECTION, Event::COLLECTION, Organization::COLLECTION]))
            unset($element["type"]);
    }
    $cssAnsScriptFilesModule = array(
	    '/js/default/preview.js'
    );
    
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<div id="preview-graph" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10">
        <span class="pull-left">
            <h4>Visualisation graphique</h4>
        </span>
		<button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
	</div>
	<div class="container-preview padding-10" style="overflow-y: auto;">
    <?php   
         if(is_array($element["_id"]))
            $field_path = "links.memberOf.".($element["_id"]['$id']).".roles";
        else    
            $field_path = "links.memberOf.".((String)$element["_id"]).".roles";

        if(isset($element["type"]) && $element["type"] == Project::COLLECTION){
            $field_path = "links.".Project::COLLECTION.".".($element["_id"]['$id']).".roles";
        }else if(isset($element["type"]) && $element["type"] == Event::COLLECTION){
            $field_path = "badges";
        }else if(isset($element["type"]) && !in_array($element["type"], [Project::COLLECTION, Event::COLLECTION, Organization::COLLECTION])){
            $field_path = "tags";
        } 
        $params = array(
            'kunik' => "graphview",
            'el' => [],
            'blockKey' => "graphview",
            "blockCms" => array(
                "title"  => " ",
                "elementTypes" => "citoyens",
                "fields" => $field_path,
                "graphType" => $options["type"]??"circle",
                "height" => 450,
                "focus" => true
            ),
            'costum' => $element
        );
    if(isset($options["elementtypes"])){
        $params["blockCms"]["elementTypes"] = $options["elementtypes"];
    }
    echo $this->renderPartial('costum.views.tpls.blockCms.graph.graphOfElements', $params);
            /*BlockCmsWidget::widget([
                "path" => "tpls.blockCms.graph.graphOfElements",
                //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
                "notFoundViewPath" => "costum.views.tpls.blockCms.graph.graphOfElements", 
                "config" => [
                    'costum' => $costum,
                    'kunik' => "graph-view",
                    'el' => [],
                    'blockKey' => "graph-view",
                    "blockCms" => array(
                        "fields" => "links.memberOf."+$costum["contextId"]+".roles",
                        "graphType" => "circle"
                    )
                ]
            ]);*/
        ?>
	</div>
</div>