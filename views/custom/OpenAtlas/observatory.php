<style type="text/css">
    .content {
    position: relative;
    animation: animatop 0.9s cubic-bezier(0.425, 1.14, 0.47, 1.125) forwards;
    }

    .firstinfo {
    display: flex;
    align-items: left;
    }

    .card {
        /** width: 500px; */
        min-height: 120px;
        padding: 10px;
        border-radius: 3px;
        background-color: white;
        /**box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.2);*/
        position: relative;
        overflow: hidden;

    }
    .card:after {
    content: '';
    display: block;
    width: 170px;
    height: 300px;
    position: absolute;
    animation: rotatemagic 0.75s cubic-bezier(0.425, 1.04, 0.47, 1.105) 1s both;
    }

    .card-success:after {
        background: rgba(225,225,225, 0.91);/*border-right: 2px solid #3C763D; */
    }

    .card-danger:after {
        background: rgba(225,225,225, 0.91); /*border-right: 2px solid #A94442;*/   
    }

    .firstinfo {
    flex-direction: row;
    z-index: 2;
    position: relative;
    }
    .firstinfo img {
    border-radius: 50%;
    width: 70px;
    height: 70px;
    }
    .firstinfo .profileinfo {
    padding: 0px 20px;
    }
    .firstinfo .profileinfo h6 {
    font-size: 1.2em;
    color: #009688;
    font-style: italic;
    }
    .firstinfo .profileinfo p.bio {
    padding: 5px 0px;
    color: #5A5A5A;
    font-style: initial;
    }

    @keyframes animatop {
    0% {
        opacity: 0;
        bottom: -500px;
    }
    100% {
        opacity: 1;
        bottom: 0px;
    }
    }
    @keyframes animainfos {
    0% {
        bottom: 10px;
    }
    100% {
        bottom: -42px;
    }
    }
    @keyframes rotatemagic {
    0% {
        opacity: 0;
        transform: rotate(0deg);
        top: -24px;
        left: -253px;
    }
    100% {
        transform: rotate(-30deg);
        top: -24px;
        left: -78px;
    }
    }

    small{
        font-weight:bold;
    }

    .container, .row{
        padding-left: 2em !important;
        padding-top: 0em !important;
        padding-bottom: 0em !important;
        margin-top: 0em !important;
        margin-bottom: 0em !important;
    }

    .dbox {
        position: relative;
        background: rgb(255,255,255);
        background: -moz-linear-gradient(top, rgba(255, 255, 255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        background: -webkit-linear-gradient(top, rgba(255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        background: linear-gradient(to bottom, rgba(255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#ff5641', endColorstr='#fd3261', GradientType=0);
        border-radius: 4px;
        text-align: center;
        margin: 20px 0 50px;
    }
    .dbox-simple {
        position: relative;
        background: rgb(255,255,255);
        background: -moz-linear-gradient(top, rgba(255, 255, 255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        background: -webkit-linear-gradient(top, rgba(255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        background: linear-gradient(to bottom, rgba(255,255,255, 1) 0%, rgba(255,255,255, 1) 100%);
        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#ff5641', endColorstr='#fd3261', GradientType=0);
        border-radius: 4px;
        text-align: center;
        margin: 20px 0 20px;
    }
    .dbox__icon {
        position: absolute;
        transform: translateY(-50%) translateX(-50%);
        left: 50%;
    }
    .dbox__icon:before {
        width: 75px;
        height: 75px;
        position: absolute;
        background: #fda299;
        background: rgba(239,239,239, 0.91);
        content: '';
        border-radius: 50%;
        left: -17px;
        top: -17px;
        z-index: -2;
    }
    .dbox__icon:after {
        width: 60px;
        height: 60px;
        position: absolute;
        background: #f79489;
        background: rgba(225,225,225, 0.91);
        content: '';
        border-radius: 50%;
        left: -10px;
        top: -10px;
        z-index: -1;
    }
    .dbox__icon > i {
        background: #FFF;
        border-radius: 50%;
        line-height: 40px;
        width: 40px;
        height: 40px;
        font-size:22px;
    }
    .p-1{
        padding: .3em;
    }
    .dbox__body {
        padding: 10px 10px 40px 10px;
    }
    .dbox__body-simple {
        padding: 10px 10px 10px 10px;
    }
    .dbox__title {
        font-size: 14px;
        color: #888;
    }
    .dbox__action {
        transform: translateY(-50%) translateX(-50%);
        position: absolute;
        left: 50%;
    }
    .dbox__action__btn {
        border: none;
        background: #FFF;
        border-radius: 19px;
        padding: 7px 16px;
        text-transform: uppercase;
        font-weight: 500;
        font-size: 11px;
        letter-spacing: .5px;
        color: #003e85;
        box-shadow: 0 3px 5px #d4d4d4;
    }

    .mt-3{
        margin-top: 2em !important;
    }
    .p-1{
        padding-right: 0.5em !important;
        padding-left: 0.5em !important;
    }
    .py-2{
        padding-bottom: 1em !important;
        padding-top: 1em !important;
    }
    .text-style{
        word-break: break-all;
    }

    #search-field, #limit-field, #filter-type{
        padding: 10px 30px 8px 30px;
        border: none;
        font-size: 1.2em;
        border-radius: 4em;
        background: rgba(225,225,225, 0.91);
        width: 100%;
        font-weight: bolder;
    }

    a.btn-config {
        padding: 8px 20px 6px 20px;
        border: 2px solid #E54E5E;
        color: #E54E5E;
        margin-right: 0.5em;
        border-radius: 4em;
        font-weight: bolder;
    }

    .text-openatlas{
        color: #E54E5E;
    }

    #search-field:focus, #limit-field, #filter-type{
        outline:none;
    }
</style>


<?php
    if(!function_exists("count_distinct")){
        function count_distinct(&$array, $label){
            $exist = false;
            foreach ($array as $k => &$v) {
                if($v["answers"]==$label){
                $v["number"]++;
                $exist = true;
                }
            }

            if(!$exist){
                array_push($array, array("label"=>$label, "number"=>1));
            }
        }
    }

    $statistic_data = array();

    // Get Form
    $formParent = PHDB::findOne(
        Form::COLLECTION, 
        array(
            "name" => "Sociétaire de la coopérative",
            "parent.".$this->costum["contextId"]=> ['$exists'=>true]
        )
    );
    
    $form = $formParent["_id"]->{'$id'};
    
    $allAnswers = PHDB::find(
        Answer::COLLECTION, 
        array(
            "context.".$this->costum["contextId"]=> ['$exists'=>true], 
            "form"=>$form,
            "dreft"=>['$exists'=>false]
        )
    );

    $formParent = PHDB::findOneById(Form::COLLECTION, $form);

    $ids = array();

    foreach ($allAnswers as $key => $value) {
        array_push($ids, $value["user"]);
    }

    $userProfiles = PHDB::findByIds(Citoyen::COLLECTION, $ids, ["profilImageUrl"]);
?>

<div class="container-fluid" style="background-color: #efefef ; min-height: 100vh">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-6">
                    <h4>Statistique générale</h4>
                </div>
                <!--div class="col-md-5">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="month-addon">Month: </span>
                            <select name="control-month" class="form-control" aria-describedby="month-addon">
                                <option value="01">Janvier</option>   
                                <option value="02">Février</option>
                                <option value="03">Mars</option>
                                <option value="04">Avril</option>
                                <option value="05">Mai</option>
                                <option value="06">Juin</option>
                                <option value="07">Juillet</option>
                                <option value="08">Août</option>
                                <option value="09">Séptembre</option>
                                <option value="10">Octobre</option>
                                <option value="11">Novembre</option>
                                <option value="12">Décembre</option>
                            </select>
                            <span class="input-group-addon" id="year-addon">Year: </span>
                            <select name="control-year" class="form-control" aria-describedby="year-addon">
                                <option selected="selected">2017</option>                            
                                <option>2016</option>                            
                                <option>2015</option>                            
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" id="month-addon">Month: </span>
                            <select name="control-month" class="form-control" aria-describedby="month-addon">
                                <option value="01">Janvier</option>   
                                <option value="02">Février</option>
                                <option value="03">Mars</option>
                                <option value="04">Avril</option>
                                <option value="05">Mai</option>
                                <option value="06">Juin</option>
                                <option value="07">Juillet</option>
                                <option value="08">Août</option>
                                <option value="09">Séptembre</option>
                                <option value="10">Octobre</option>
                                <option value="11">Novembre</option>
                                <option value="12">Décembre</option>
                            </select>
                            <span class="input-group-addon" id="year-addon">Year: </span>
                            <select name="control-year" class="form-control" aria-describedby="year-addon">
                                <option selected="selected">2017</option>                            
                                <option>2016</option>                            
                                <option>2015</option>                            
                            </select>
                        </div>
                    </div>
                </div-->
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-3 text-right">
                    <?php if(Authorisation::isInterfaceAdmin()){ ?>
                        <a data-parentformid="<?php echo $form; ?>" href="#form.edit.id.<?php echo $form; ?>" class="btn btn-config lbh getformconfig"> <span class="fa fa-cogs"></span> &nbsp; CONFIGURER COFORM</a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="dbox">
                        <div class="dbox__body">
                            <h2 id="all" class="text-info">0</h2>
                            <h6 class="dbox__title text-info">
                                Demandes d'Associés
                            </h6>
                        </div>
                        <div class="dbox__action">
                            <a href="javascript:;" class="dbox__icon btn-statistic" data-status="">
                                <i class="fa fa-users text-info"></i>
                            </a>
                        </div>              
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dbox">
                        <div class="dbox__body">
                            <h2 id="paid" class="text-success">0</h2>
                            <h6 class="dbox__title">
                                Confirmés
                            </h6>
                        </div>
                        <div class="dbox__action">
                            <a href="javascript:;" class="dbox__icon  btn-statistic" data-status="true">
                                <i class="fa fa-check text-success"></i>
                            </a>
                        </div>              
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dbox">
                        <div class="dbox__body">
                            <h2 id="nopaid" class="text-danger">0</h2>
                            <h6 class="dbox__title">
                                Non Payés
                            </h6>
                        </div>
                        
                        <div class="dbox__action">
                            <a href="javascript:;" class="dbox__icon btn-statistic" data-status="false">
                                <i class="fa fa-exclamation-triangle text-danger"></i>
                            </a>
                        </div>              
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="dbox">
                        <div class="dbox__body">
                            <div id="paymentMethods" class="text-warning">
                            
                            </div>
                            <h6 class="dbox__title">Modes de paiement</h6>
                        </div>
                        
                        <div class="dbox__action">
                            <a target="_blank" href="https://www.mollie.com/dashboard/settings/profiles" class="dbox__icon">
                                <i class="fa fa-plus text-warning"></i>
                            </a>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                <canvas id="types-chart" class="dbox py-2" height="108%"></canvas>
                </div>
                <div class="col-md-3">
                    <div class="dbox-simple">
                        <div class="dbox__body-simple">
                            <h2 id="totalAmount" class="text-success">0 €</h2>
                            <h6 class="dbox__title py-2">
                                Montant Total
                            </h6>
                        </div>
                    </div>
                    <div class="dbox-simple">
                        <div class="dbox__body-simple">
                            <h2 id="nbPart" class="text-info">0</h2>
                            <h6 class="dbox__title py-2">
                                Nombre de part
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 py-2">
                    <h4>Liste d'associés &nbsp; <span class="text-info" id="infoAddon"></span></h4>
                </div>
                <div class="col-md-3 py-2">
                    <select id="filter-type">
                        <option value="" style="color:#999!important">filtrer par type</option>
                    </select>
                </div>
                <div class="col-md-3 py-2">
                    <input id="search-field" type="text" placeholder="Recherche">
                </div>
                <div class="col-md-2 py-2">
                    <input id="limit-field" type="number" min="5" placeholder="5 derniers">
                </div>
            </div>
            <br/>
        </div>
        
        <section id="team" class="container">
            <div id="answersList" class="row"></div>
        </section>
    </div>
</div>

<!-- Modal Answers-->
<div id="answerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Réponse</h4>
            </div>
            <div id="answerContent" class="modal-body">
            
            </div>
            <div class="modal-footer"  style="border-top: none;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<script src="/plugins/Chart-2.6.0/Chart.js"></script>
<script type="text/javascript">
    let paid = 0;
    let total = 0;
    let nbPart = 0;
    let types = [];
    let nopaid = 0;
    let refound = 0;
    let allAnswers = <?php echo json_encode($allAnswers); ?>;
    let profileImages = <?php echo json_encode($userProfiles); ?>;

    let subformId = "";

    $(document).ready(function () {

        showListAnswers(allAnswers, 50);
        
        // Handle limit
        $(document).on("change", "#limit-field", function(e){
            if($(this).val()!=""){
                showListAnswers(allAnswers, $(this).val());
            }else{
                showListAnswers(allAnswers, null);
            }
        });

        // Handle search
        $(document).on("keyup", "#search-field", function(e){
           search($(this).val(), "answerItem"); 
        });

        // Handle select by status
        $(document).on("click", ".btn-statistic", function(){
            var filterData = $(this).attr("data-status");
            if(filterData==""){
                showListAnswers(allAnswers, null)
                $("#search-field").val("");
                $("#infoAddon").text("");
            }else{
                
                let filtered = {};

                for (const [k, v] of Object.entries(allAnswers)) {
                    if(v.answers && v.answers[Object.keys(v.answers)[0]]){
                        if(filterData == ""+v.answers["isPaid"]){
                            filtered[k] = v;
                        }
                    }else{
                        if(filterData=="false"){
                            filtered[k] = v;
                        }
                    }
                }

                if(filterData=="true"){
                    $("#infoAddon").text("( Confirmés )");
                }else{
                    $("#infoAddon").text("( Non payés )");
                }

                showListAnswers(filtered, null);
            }
        });

        // Filter by type
        $(document).on("change", "#filter-type", function(){
            search($(this).val().substr(0, 15), "answerItem");
        })

        // Get form answer in modal
        $(".getanswer").on('click', function() {
            $("#answerContent").empty();
            coInterface.showLoader("#answerContent");
            var answid = $(this).data("ansid");
            var getdatatype = "";
            var contextId = "<?php echo $this->costum['contextId'] ?>";
            var contextType = "<?php echo $this->costum['contextType'] ?>";
        
            if (typeof $(this).data("type") != "undefined" && $(this).data("type") == "openform") {
                ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                  null,
                  function(){
                      if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid+".contextId."+contextId+".contextType."+contextType);
                      }
                  },"html");
            }else{
                ajaxPost("#answerContent", baseUrl+'/survey/answer/index/id/'+$(this).data("ansid")+'/mode/'+$(this).data("mode")+'/contextId/'+contextId+'/contextType/'+contextType, 
                null,
                function(){
                    if (typeof hashUrlPage != "undefined") {
                        history.replaceState(location.hash, "", hashUrlPage+".view.forms.dir.answer."+answid);
                    }
                },"html");
            }
        });

        // Get all payment methods from mollie
        getPaymentMethods("paymentMethods", subformId, "");

        //Initialize Chart
        initTypesChart();

        // Set Statistics
        $("#all").text(Object.entries(allAnswers).length);
        $("#paid").text(paid);
        $("#nopaid").text(nopaid);
        $("#totalAmount").text(total+" €");
        $("#nbPart").text(nbPart);
        
        for (let index = 0; index < types.length; index++) {
            $("#filter-type").append('<option value="'+types[index][0]+'">'+types[index][0]+'</option>');
        }
    });

    // Functions to display answers list with limit
    function showListAnswers(allAnswers, limit){
        let countLoaded = 0;

        $("#answersList").empty();

        for (const [keya, associe] of Object.entries(allAnswers)) {
            if(countLoaded==limit){
                break;
            }
            
            let [form, answers] = [null, null];

            if(associe.answers){
                [form, answers] = Object.entries(associe.answers)[0];
                subformId = form;
                if(associe.answers["isPaid"]==true){
                    paid++;
                    // Montant total
                    total += parseFloat(JSON.parse(atob(answers["type"])).droit);
                    //types.push(JSON.parse(atob(answers["type"])).label);
                    collect_graph_data(types, JSON.parse(atob(answers["type"])).label);
                    nbPart += parseInt(answers["nbPart"]);
                    // End
                }else{
                    if(associe.answers["status"]=="refound"){
                        refound++;
                    }else{
                        nopaid++;
                    }
                }
            }else{
                nopaid++;
            }

            const domItem = `<div class="col-xs-12 col-sm-6 answerItem col-md-3 py-2" data-status="${associe.answers && associe.answers["isPaid"]}">
                <div class="content">
                    <div class="card card-${(associe.answers && associe.answers["isPaid"])?"success":"danger"}">
                        <div class="firstinfo"><img class="img-responsive" src="${(associe.user && profileImages[associe.user]["profilImageUrl"])?profileImages[associe.user]["profilImageUrl"]:"<?= Yii::app()->getModule('co2')->assetsUrl; ?>/images/thumb/default_citoyens.png"}" alt="card image">
                            <div class="profileinfo">
                                <h5 class="text-info">
                                    ${(answers && answers.name)?answers.name:"Non validé"}
                                </h5>
                                <!--h6></h6-->
                                <div>
                                <small class="bio">${(answers && answers.type)?JSON.parse(atob(answers["type"])).label:"pas d'information"}</small></div>
                                <button type="button" class="btn btn-default btn-sm getanswer" data-mode="r" data-ansid="${keya}" data-toggle="modal" data-target="#answerModal">Voir réponse</button> &nbsp;
                                <span class="fa fa-${(associe.answers && associe.answers["isPaid"])?"check text-success":"exclamation-triangle text-danger"}"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;

            $("#answersList").append(domItem);

            countLoaded++;
        }
    }

    // Function to get payment methods
    function getPaymentMethods(elementId, sfId, sfAttr){
        ajaxPost(
            null, 
            baseUrl+"/survey/payment/getpaymentmethods",
            {param1:sfId, param2:sfAttr},
            function(response){
                for(const index in response) {
                    $("#"+elementId).append('<img width="40" src="'+response[index]["image"]["size2x"]+'" class="p-1">');
                }

                if(response.length==0){
                    $("#"+elementId).append('<button type="button" class="btn btn-sm" onclick="getPaymentMethods(\"paymentMethods\", subformId, \"\")"><i class="fa fa-refresh"></i>Refresh</button>')
                }
            },
            function(error){
                //toastr.warning("Veuillez recharger la page pour voir tout les methodes de payments");
            }
        );
    }

    // Function to collect and format statistic data from answers
    function collect_graph_data(array, label){
        let exist = false;

        for (let v of array) {
            if(v[0]==label){
                v[1]++;
                exist = true;
            }
        }

        if(!exist){
            array.push([label, 1]);
        }
    }

    // Initialize Camamber Chart from
    function initTypesChart(){
        var ctx = $("#types-chart");
        new Chart(ctx, {
			type: 'pie',
			data: {
				labels: types.map(function(v){return v[0]}),
				datasets: [
					{
						label: "Acteurs",
						backgroundColor: ["#E54E5E", "#CCE428", "#FEEF33", "#FBCBA9", "#F69854","#CCE428", "#e84c3d", "#f1c40f", "#F69854"],
						data: types.map(function(v){return v[1]})
					}
				]
			},
			options: {
				responsive: true,
				legend: { 
					display: true,
					position: "right",
					align: "left"
				},
				title: {
					display: true,
					text: "TYPE D'ACTEURS"
				},
				cutoutPercentage: 50
			}
		});
    }

    // Search function
    function search(critValue, itemClass){
        var count = 0;
        var all = $("."+itemClass).length;
        
        $("."+itemClass).each(function () {
            if ($(this).text().search(new RegExp(critValue, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });

        if(count < all){
            $("#infoAddon").text("( "+count+" Resultats )");
        }else{
            $("#infoAddon").text("");
        }
    }
</script>