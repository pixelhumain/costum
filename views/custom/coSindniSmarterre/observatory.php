<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
$graphLabels = Aap::aapGraph($slug);
$cssJS = array(
'/plugins/DataTables/media/css/DT_bootstrap.css',
 '/plugins/Chart.js/chart.4.4.0.min.js',
 "/plugins/Chart.js/chartjs-plugin-datalabels.2.0.0.js",
 '/plugins/Chart.js/chartjs-chart-sankey.0.12.0.js',
 '/plugins/DataTables/media/js/jquery.dataTables.min.1.10.4.js',
); 
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);
// $bodyStyle= "", $tag = "", $containerStyle = ""
function createChartPanel($idCanvas,$params = []){
    if(!empty($params["isActive"])){
        $tag = $params["tag"] ?? '';
        $containerStyle = $params["containerStyle"] ?? '';
        $bodyStyle = $params["bodyStyle"] ?? '';
        $html = $params["html"] ?? '';
        $col = $params["col"] ?? 6;
        $chartsExpand = <<<EOF
            <div class="col-md-1">
                <div class="panel-toolbar pull-right" role="menu">
                    <a href="javascript:;" class="btn btn-panel fullscreen" data-key="on" data-toggle="tooltip" data-original-title="Fullscreen">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="javascript:;" class="btn btn-panel fullscreen" data-key="off" data-toggle="tooltip" data-original-title="Fullscreen" style="display:none">
                        <i class="fa fa-compress"></i>
                    </a>
                </div>
            </div>
        EOF;

        return <<<EOF
            <div class="col-lg-$col col-md-$col col-sm-12 margin-top-10 $tag" id="$idCanvas-container">
                <div class="card card-stats" style="$containerStyle">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-11">
                                $html
                            </div>
                            $chartsExpand 
                        </div>
                    </div>
                    <div class="card-content" style="$bodyStyle">
                        <canvas id="$idCanvas"></canvas>
                    </div>
                </div>
            </div>
        EOF;
    }
    return "";
}
?>

<style>
    hr{
        margin-top: 1rem;
        margin-bottom: 1rem;
        border: 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
    }
    i.pourcentage{
        font-size: initial;
        color: forestgreen;
    }
    .tochecked{
        display: none;
    }
    .table td {
        text-align: center;
    }
    label.unchecked{
        background-color: #eee;
        color: #656565;
        font-weight: 700;
        padding: 2px;
        text-decoration: line-through;
    }

    .text-uppercase {
            text-transform: lowercase !important;
        }

    canvas{
        margin: 10px 10px 10px 10px;
    }
    .panel-toolbar {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .fullscreen-active{
        position : fixed;
        top : 0;left : 0;right : 0;bottom : 0;
        width : 100%;
        height : 100%;
        z-index : 100000
    }
    .canvas-fullscreen-active{
        width : 100%;
        height : 90%;
    }

    .card{
        border: 1px solid #eeefee !important;
    }
    .card-title{
        font-size: 16px;
    }

    .graphe.fullscreenActive{
        position:fixed;
        top:80px;
        left:0;
        align-items: center;
        justify-content: center;
        width:100%;
        height:100%;
        transition: visibility 0s, opacity 0.5s;
        z-index: 100;
    }

    a.fullscreenA{
        width: 2.3rem !important;
        height: 2.3rem !important;
    }

    canvas.fullscreenActive{
        height: 100% !important;
        width: 70% !important;
    }

    div.fullscreenActive{
        height: 100% !important;
        width:100% !important;
    }
    #expenditureFinancingPaymentHistory-filter{
        width: 112px;
    }

    .observatory-aap .historyName{
        text-align: left !important;
        width: 70%;
    }

    .observatory-aap .historyDate{
        text-align: right !important;
        width:30%
    }

    .observatory-aap .historyTable{
        border: 1px solid #ddd;
    }
    .observatory-aap #filterContainerInside{
        background-color: transparent;
    }
    .breadcrumb.aap-breadcrumb{
        display: none;
    }
    .observatory-aap .pointer{
        cursor: pointer;
    }
    .observatory-aap .date-between .form-group{
        position: relative;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
    }
    .observatory-aap .date-between{
        position: relative;
        display: flex;
        flex-direction: row;
        align-items: center;
        margin-top: 9px;
    }

    .switch.checkFilter input[type=checkbox]:checked+label::before {
        background-color: #9BC125;
    }

    #checkProject:hover + #hovercheckFilter{
        display:inline-block;
    }

    .chartAreaWrapper {
        position: relative;
        width: 600px;
        height: 100%;
        overflow-y: scroll;
        overflow-x: scroll;
    }
    .observatory-aap #map-container{
        position:relative;
        width:100%;
        height:700px ;
        z-index:1 !important;
    }
    .dropdown-menu-map{
        left: auto !important;
        right: 0 !important;
    }
    .searchObjCSS .dropdown .dropdown-menu.dropdown-menu-map:before,
    .searchObjCSS .dropdown .dropdown-menu.dropdown-menu-map:after{
        right: 19px !important;
        left: auto !important;
    }
    .observatory-aap .text-left{
        text-align:left;
    }
    .loader-observatory {
        position: absolute;
        background: #fff;
        height: 100vh;
        z-index: 9;
        top: 0;
        right: 0;
        left: 0;
        bottom: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
    }
    #modal-quartier-content
</style>
<style>
    .container-quartier-quartier hr
    {
        border: 1px solid #858585;
    }

    .container-quartier-quartier .team
    {
        margin-top: 25px;
    }

    .container-quartier-quartier .team h1
    {
        font-weight: normal;
        font-size: 22px;
        margin: 10px 0 0 0;
    }

    .container-quartier-quartier .team h2
    {
        font-size: 16px;
        font-weight: lighter;
        margin-top: 5px;
    }

    .container-quartier-quartier .team .img-box
    {
        opacity: 1;
        display: block;
        position: relative;
    }

    .container-quartier-quartier .team .img-box:after
    {
        content: "";
        opacity: 0;
        background-color: rgba(0, 0, 0, 0.75);
        position: absolute;
        right: 0;
        left: 0;
        top: 0;
        bottom: 0;
    }

    .container-quartier-quartier .img-box ul
    {
        position: absolute;
        z-index: 2;
        bottom: 50px;
        text-align: center;
        width: 100%;
        padding-left: 0px;
        height: 0px;
        margin: 0px;
        opacity: 0;
    }

    .container-quartier-quartier .team .img-box:after,.container-quartier-quartier .img-box ul, .img-box ul li
    {
        -webkit-transition: all 0.5s ease-in-out 0s;
        -moz-transition: all 0.5s ease-in-out 0s;
        transition: all 0.5s ease-in-out 0s;
    }

    .container-quartier-quartier .img-box ul i
    {
        font-size: 20px;
        letter-spacing: 10px;
    }

    .container-quartier-quartier .img-box ul li
    {
        width: 30px;
        height: 30px;
        text-align: center;
        border: 1px solid #fff;
        margin: 2px;
        padding: 5px;
        display: inline-block;
    }

    .container-quartier-quartier .img-box a
    {
        color: #fff;
    }

    .container-quartier-quartier .img-box:hover:after
    {
        /* opacity: 1; */
    }

    .container-quartier-quartier .img-box:hover ul
    {
        /* opacity: 1; */
    }

    .container-quartier-quartier .img-box ul a
    {
        -webkit-transition: all 0.3s ease-in-out 0s;
        -moz-transition: all 0.3s ease-in-out 0s;
        transition: all 0.3s ease-in-out 0s;
    }

    .container-quartier-quartier .img-box a:hover li
    {
        border-color: #FFEA05;
        color: #FFEA05;
    }

    .container-quartier-quartier .img-box a
    {
        color: #FFEA05;
    }

    .container-quartier-quartier .img-box a:hover
    {
        text-decoration: none;
        color: #519548;
    }
    .container-quartier-quartier .card-team{
        position: relative;
        /* height: 614px; */
        width: 100%;
        padding-top: 15px;
    }
    .container-quartier-quartier .team{
        box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
        flex: 0 0 calc(98%/3);
        padding: 5px 10px;
    }
    .container-quartier-quartier .bodySearchContainerQuartier{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .container-quartier-quartier .divEndOfresults{
        display: none;
    }
    .container-quartier-quartier .team .desc{
        word-break: break-word;
        line-height:120%;
        text-align: left !important;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp:3;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .container-quartier-quartier .img-responsive-quartier{
        width: 100%;
        height: 253px;
        object-fit: cover;
    }

    /* styles.css */
    .checkbox-box {
        display: none;
        position: fixed;
        background-color: #fff;
        border: 1px solid #ccc;
        box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.2);
        z-index: 1000;
        padding: 10px;
        width: auto;
        height: 250px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    i.delcol, i.addcol, i.updateindice{
        cursor: pointer;
    }

    .checkbox-box::-webkit-scrollbar, .dataTables_scrollBody::-webkit-scrollbar {
        width: 6px; /* Set the width of the scrollbar (for WebKit browsers) */
    }

    .checkbox-box::-webkit-scrollbar-thumb, .dataTables_scrollBody::-webkit-scrollbar-thumb {
        background-color: var(--aap-primary-color); /* Color of the scrollbar thumb (for WebKit browsers) */
        border-radius: 3px; /* Rounded corners for the thumb */
    }

    .checkbox-box::-webkit-scrollbar-thumb:hover, .dataTables_scrollBody::-webkit-scrollbar-thumb:hover {
        background-color: #555; /* Color of the thumb on hover (for WebKit browsers) */
    }

    .checkbox-box::-webkit-scrollbar-track, .dataTables_scrollBody::-webkit-scrollbar-track {
        background: #f1f1f1; /* Color of the scrollbar track (for WebKit browsers) */
        border-radius: 3px; /* Rounded corners for the track */
    }

    .checkbox-box::-webkit-scrollbar-corner, .dataTables_scrollBody::-webkit-corner {
        background: transparent; /* Hide the scrollbar corner (for WebKit browsers) */
    }

    .checkbox-box label {
        display: block;
        margin-bottom: 8px;
        padding: 2px;
    }

    .checkbox-box input[type="checkbox"] {
        margin-right: 5px;
    }

    @media only screen and (min-width : 320px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Extra Small Devices, Phones */ 
    @media only screen and (min-width : 480px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 100%;
            padding: 5px 10px;
        }
    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 49%;
            padding: 5px 10px;
        }
    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/2);
            padding: 5px 10px;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        .team{
            box-shadow: rgb(0 0 0 / 5%) 0px 6px 24px 0px, rgb(0 0 0 / 8%) 0px 0px 0px 1px;
            flex: 0 0 calc(98%/3);
            padding: 5px 10px;
        }
    }
</style>

<div class="container-fluid observatory-aap">
    <div class="row filter-container">
        <div class="padding-bottom-20">
            <div class="col-md-12 no-padding">
                <div id='filterContainerObs' class='searchObjCSS'></div>
                <div class='headerSearchIncommunityObs no-padding col-xs-12 hidden'></div>
                <div class='bodySearchContainerObs margin-top-30 hidden'>
                    <div class='no-padding col-xs-12' id='dropdown_search'>
                    </div>
                </div>
                <div class='padding-top-20 col-xs-12 text-left footerSearchContainer hidden'></div>
            </div>
        </div>
    </div>

    <div id="loader-observatory" class='loader-observatory'>
        
    </div>

    <div class="chart-and-metrics">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header">
                        <div class="row">
                                <div class="col-md-8 card-title"><i class="fa fa-lightbulb-o">&nbsp;</i><b>Proposition</b></div>
                                <div class="panel-toolbar pull-right" style="margin-right:4%">
                                <div class="switch checkFilter">
                                    <input id="checkProposition" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                    <label for="checkProposition"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table align-items-center mb-0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> En financement</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">A voter</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">A financer</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td ><h4 id="inFinancingProposal" class="metric-number cursor-pointer" data-action="infinancing">0</h4></td>
                                            <td ><h4 id="voteProposal" class="metric-number cursor-pointer" data-action="tobevoted">0</h4></td>
                                            <td ><h4 id="toBeFinancedProposal" class="metric-number cursor-pointer" data-action="tobefinanced">0</h4></td>
                                            <td ><h4 id="totalProposal">0</h4></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 card-title"><i class="fa fa-cogs"></i>&nbsp; <b>Projet</b></div>
                            <div class="panel-toolbar pull-right" style="margin-right:4%">
                                <div class="switch checkFilter">
                                    <input id="checkProject" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                    <label for="checkProject"></label>
                                    <span id="hovercheckFilter" class="tooltips-menu-btn" style="display: none;">Activer/désactiver</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Total</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">En cours</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Terminé</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td ><h4 id="totalProject">0</h4></td>
                                        <td ><h4 class="metric-number cursor-pointer" id="runningProject" data-action="inprogress">0</h4></td>
                                        <td ><h4 class="metric-number cursor-pointer" id="doneProject" data-action="finished">0</h4></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-8 card-title"><i class="fa fa-tasks">&nbsp;</i><b>Actions</b></div>
                            <div class="panel-toolbar pull-right" style="margin-right:4%">
                                <div class="switch checkFilter">
                                    <input id="checkActions" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                    <label for="checkActions"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Total</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">À faire</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">En cours</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Terminé</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Délai dépassé</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td ><h4 id="totalActions" class="metric-number pointer">0</h4></td>
                                        <td ><h4 id="todoActions" class="metric-number pointer">0</h4></td>
                                        <td ><h4 id="runningActions" class="metric-number pointer">0</h4></td>
                                        <td ><h4 id="doneActions" class="metric-number pointer">0</h4></td>
                                        <td ><h4 id="deadlinePassedActions" class="metric-number pointer">0</h4></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header">
                    <div class="row">
                            <div class="col-md-8 card-title"><i class="fa fa-money">&nbsp;</i><b>Financements (en euro)</b></div>
                            <div class="panel-toolbar pull-right" style="margin-right:4%">
                                <div class="switch checkFilter">
                                    <input id="checkFinancement" class="cmn-toggle cmn-toggle-round-flat checkbox" type="checkbox" checked>
                                    <label for="checkFinancement"></label>
                                </div>
                            </div>
                    </div>
                    </div>
                    <div class="card-content">
                        <div class="row">

                            <div class="col-md-12">
                            <table class="table align-items-center mb-0" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7"> Montant sollicité</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Montant financé</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Reste à financé </th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 hidden">Fonds dépensés</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td ><h4 id="amountRequested">0</h4></td>
                                        <td ><h4 id="amountFinanced">0</h4></td>
                                        <td ><h4 id="amountRemaining">0</h4></td>
                                        <td ><h4 class="hidden" id="amountSpent">0</h4></td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php echo createChartPanel("financementPerYear", [
                "bodyStyle" => "min-height: 300px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerYear"]) || !empty($config["financementPerYear"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerYear", [
                "bodyStyle" => "min-height: 300px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerYear"]) || !empty($config["numberOfActionPerYear"])
            ]) ?>
            <?php echo createChartPanel("financementPerThematique", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerThematique"]) || !empty($config["financementPerThematique"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerThematique", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerThematique"]) || !empty($config["numberOfActionPerThematique"])
            ]) ?>
            <?php echo createChartPanel("financementPerPilier", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerPilier"]) || !empty($config["financementPerPilier"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerPilier", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerPilier"]) || !empty($config["numberOfActionPerPilier"])
            ]) ?>
            <?php echo createChartPanel("financementPerAssoc", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerAssoc"]) || !empty($config["financementPerAssoc"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerAssos", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerAssos"]) || !empty($config["numberOfActionPerAssos"])
            ]) ?>

            <?php echo createChartPanel("financementPerAxis", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerAxis"]) || !empty($config["financementPerAxis"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerAxis", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerAxis"]) || !empty($config["numberOfActionPerAxis"])
            ]) ?>

            <?php echo createChartPanel("financementPerSpecificGoals", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerSpecificGoals"]) || !empty($config["financementPerSpecificGoals"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerSpecificGoals", [
                "bodyStyle" => "min-height: 400px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerSpecificGoals"]) || !empty($config["numberOfActionPerSpecificGoals"])
            ]) ?>

            <?php echo createChartPanel("financementPerFinancer", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerFinancer"]) || !empty($config["financementPerFinancer"])
            ]) ?>
            <?php echo createChartPanel("numberOfActionPerFinancer", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["numberOfActionPerFinancer"]) || !empty($config["numberOfActionPerFinancer"])
            ]) ?>
            <?php echo createChartPanel("financementPerQuartier", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["financementPerQuartier"]) || !empty($config["financementPerQuartier"])
            ]) ?>
            <?php echo createChartPanel("totalFinanceByYear", [
                "bodyStyle" => "min-height: 600px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 6,
                "isActive" => !isset($config["totalFinanceByYear"]) || !empty($config["totalFinanceByYear"])
            ]) ?>
            <?php echo createChartPanel("financingPerFunder", [
                "bodyStyle" => "min-height: 900px;",
                "containerStyle" => "max-height: 98vh; overflow-y: auto",
                "col" => 12,
                "isActive" => !isset($config["financingPerFunder"]) || !empty($config["financingPerFunder"])
            ])?>
        </div>
    </div>

    <div class="row" id="table-container" style="display:none">
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 $tag" id="fundingByYearAndFinancor">
            
        </div>
    </div>

    <div class="row" id="table-insee" style="display:none">
         <div class="col-lg-12 col-md-12 col-sm-12 ">
            <div class="btn-group">
            <button class="btn-thematique btn active"  data-value="abs">ABS</button>
                <button class="btn-thematique btn "  data-value="Demographie">Démographie</button>
                <button class="btn-thematique btn"  data-value="Revenus">Revenus </button>
                <button class="btn-thematique btn"  data-value="education">Éducation</button>
                <button class="btn-thematique btn"  data-value="inserstionpro">Insertion professionnelle</button>
                <button class="btn-thematique btn"  data-value="Logement">Logement</button>
                <button class="btn-thematique btn"  data-value="economie">Tissu économique</button>
               
            </div>
         </div>   
        
         <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables " id="getDataInseeabs" >

         </div>
                
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables " id="getDataInsee" style="display:none">
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables" id="getDataInseervn"  style="display:none">
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables" id="getDataInseeeduc" style="display:none">
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables" id="getDataInseeinsertionPro" style="display:none">
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables" id="getDataInseelogement" style="display:none">
            
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-top-10 content-tables" id="getDataInseetissuEco" style="display:none">
            
        </div>

    </div>

    <div class="row">
        <div id="map-container" style="display:none"></div>
    </div>

    <div id="list_box_checkbox"></div>
</div>

<script>
    var queryParams = aapObj.common.getQuery();
    var mychart = null;

    var objetAtraduire = {            
        "name":"Quartiers",
        "A":"DEFM catégorie A",
        "A ":"Nombre total d'allocataires",
        "AAAH":"Allocataires percevant l’Allocation Adulte Handicapé",
        "ABC":"DEFM catégories ABC",
        "ABCDE":"DEFM",
        "ABCDE_F":"DEFM femmes",
        "ABCDE_H":"DEFM hommes",
        "ABC_25":"DEFM catégories ABC moins de 26 ans",
        "ABC_25_F":"DEFM femmes catégories ABC moins de 26 ans",
        "ABC_25_H":"DEFM hommes catégories ABC moins de 26 ans",
        "ABC_50":"DEFM catégories ABC de 50 ans ou plus",
        "ABC_50_F":"DEFM femmes catégories ABC de 50 ans et plus",
        "ABC_50_H":"DEFM hommes catégories ABC de 50 ans et plus",
        "ABC_2649":"DEFM catégories ABC de 26 ans à moins de 50 ans",
        "ABC_2649_F":"DEFM femmes catégories ABC de 26 ans à moins de 50 ans",
        "ABC_2649_H":"DEFM hommes catégories ABC de 26 ans à moins de 50 ans",
        "ABC_BAC":"DEFM catégories ABC de niveau de formation BAC",
        "ABC_BAC_F":"DEFM femmes catégories ABC de niveau de formation BAC",
        "ABC_BAC_H":"DEFM hommes catégories ABC de niveau de formation BAC",
        "ABC_CAPBEP":"DEFM catégories ABC de niveau de formation CAP-BEP",
        "ABC_CAPBEP_F":"DEFM femmes catégories ABC de niveau de formation CAP-BEP",
        "ABC_CAPBEP_H":"DEFM hommes catégories ABC de niveau de formation CAP-BEP",
        "ABC_DUR1":"DEFM catégories ABC durée d'inscription de moins de 6 mois",
        "ABC_DUR1_F":"DEFM femmes catégories ABC durée d'inscription de moins de 6 mois",
        "ABC_DUR1_H":"DEFM hommes catégories ABC durée d'inscription de moins de 6 mois",
        "ABC_DUR2":"DEFM catégories ABC durée d'inscription de 6 mois à moins d'1 an",
        "ABC_DUR2_F":"DEFM femmes catégories ABC durée d'inscription de 6 mois à moins d'1 an",
        "ABC_DUR2_H":"DEFM hommes catégories ABC durée d'inscription de 6 mois à moins d'1 an",
        "ABC_DUR3":"DEFM catégories ABC durée d'inscription de 1 an à moins de 2 ans",
        "ABC_DUR3_F":"DEFM femmes catégories ABC durée d'inscription de 1 an à moins de 2 ans",
        "ABC_DUR3_H":"DEFM hommes catégories ABC durée d'inscription de 1 an à moins de 2 ans",
        "ABC_DUR4":"DEFM catégories ABC durée d'inscription d'au moins 2 ans",
        "ABC_DUR4_F":"DEFM femmes catégories ABC durée d'inscription d'au moins 2 ans",
        "ABC_DUR4_H":"DEFM hommes catégories ABC durée d'inscription d'au moins 2 ans",
        "ABC_Dur1":"DEFM catégories ABC d'ancienneté au chômage de moins de 6 mois",
        "ABC_Dur1_F":"DEFM femmes catégories ABC d'ancienneté au chômage de moins de 6 mois",
        "ABC_Dur1_H":"DEFM hommes catégories ABC d'ancienneté au chômage de moins de 6 mois",
        "ABC_Dur2":"DEFM catégories ABC d'ancienneté au chômage de 6 mois à moins d'1 an",
        "ABC_Dur2_F":"DEFM femmes catégories ABC d'ancienneté au chômage de 6 mois à moins d'1 an",
        "ABC_Dur2_H":"DEFM hommes catégories ABC d'ancienneté au chômage de 6 mois à moins d'1 an",
        "ABC_Dur3":"DEFM catégories ABC d'ancienneté au chômage de 1 an à moins de 2 ans",
        "ABC_Dur3_F":"DEFM femmes catégories ABC d'ancienneté au chômage de 1 an à moins de 2 ans",
        "ABC_Dur3_H":"DEFM hommes catégories ABC d'ancienneté au chômage de 1 an à moins de 2 ans",
        "ABC_Dur4":"DEFM catégories ABC d'ancienneté au chômage d'au moins 2 ans",
        "ABC_Dur4_F":"DEFM femmes catégories ABC d'ancienneté au chômage d'au moins 2 ans",
        "ABC_Dur4_H":"DEFM hommes catégories ABC d'ancienneté au chômage d'au moins 2 ans",
        "ABC_E":"DEFM catégories ABC nationalité étrangère",
        "ABC_F":"DEFM femmes catégories ABC",
        "ABC_FR":"DEFM catégories ABC nationalité française",
        "ABC_H":"DEFM hommes catégories ABC",
        "ABC_INFCAPBEP":"DEFM catégories ABC de niveau de formation inférieur au CAP-BEP",
        "ABC_INFCAPBEP_F":"DEFM femmes catégories ABC de niveau de formation inférieur au CAP-BEP",
        "ABC_INFCAPBEP_H":"DEFM hommes catégories ABC de niveau de formation inférieur au CAP-BEP",
        "ABC_Ins1":"DEFM catégories ABC dont le motif d'inscription est une 1ère entrée sur le marché du travail",
        "ABC_Ins2":"DEFM catégories ABC dont le motif d'inscription est un licenciement",
        "ABC_Ins3":"DEFM catégories ABC dont le motif d'inscription est une fin de CDD",
        "ABC_Ins4":"DEFM catégories ABC dont le motif d'inscription est une fin de mission d'intérim",
        "ABC_Ins5":"DEFM catégories ABC dont le motif d'inscription est autre",
        "ABC_RSA":"DEFM catégories ABC bénéficiaires du RSA",
        "ABC_RSA_F":"DEFM femmes catégories ABC bénéficiaires du RSA",
        "ABC_RSA_H":"DEFM hommes catégories ABC bénéficiaires du RSA",
        "ABC_SUPBAC":"DEFM catégories ABC de niveau de formation supérieur au BAC",
        "ABC_SUPBAC_F":"DEFM femmes catégories ABC de niveau de formation supérieur au BAC",
        "ABC_SUPBAC_H":"DEFM hommes catégories ABC de niveau de formation supérieur au BAC",
        "AC3ENF":"Allocataires couples avec au moins 3 enfants à charge",
        "ACAVENF":"Allocataires couples avec enfant(s)",
        "ACSSENF":"Allocataires couples sans enfant",
        "AI":"Allocataires isolés sans enfant",
        "AM":"Allocataires mono-parent",
        "APPA":"Allocataires percevant la prime d'activité",
        "ARSAS":"Allocataires percevant le Revenu de Solidarité Active socle",
        "AUTO_ENT":"Nombre de micro-entrepreneurs parmi les créations d'établissements",
        "AUTRE_SERV":"Nb. étab. service aux particuliers Autres activités de service",
        "A_ETUD":"Allocataires étudiants",
        "A_F":"DEFM femmes catégorie A",
        "A_H":"DEFM hommes catégorie A",
        "A_NETUD_24":"Allocataires de moins de 25 ans non étudiants",
        "B2":"Élèves scolarisés deux ans après la 3e",
        "B2_1_GEN":"Élèves en première générale",
        "B2_1_PRO":"Élèves en première professionnelle",
        "B2_1_TEC":"Élèves en première technologique",
        "B2_AUTR":"Élèves dans une autre formation (hors redoublements)",
        "B2_CAP2":"Élèves en seconde année de CAP",
        "B2_RET":"Élèves ayant redoublé un an après la 3e",
        "BREV":"Taux de bas revenus déclarés au seuil de 60% du revenu déclaré (%)",
        "C":"Population couverte bénéficiaire du régime général",
        "CA":"Contrats aidés",
        "CFA":"Centres de formation d'apprentis",
        "CFA_PRI":"Centres de formation d'apprentis privés",
        "COLL":"Collèges",
        "COLL_PRI":"Collèges privés",
        "COLL_REP":"Collèges en REP",
        "COLL_REP_P":"Collèges en REP plus",
        "COM_DETAIL":"Nb. étab. Commerce de détail hors automobiles et motocycles",
        "COM_GROS":"Nb. étab. Commerce de gros hors automobiles et motocycles",
        "COM_TRANSP":"Nb. étab. Commerce, Transport, Hébergement et Restauration",
        "CONSTR":"Nb. étab. Construction",
        "CREATION":"Créations d'établissements",
        "C_C2SNP":"Population couverte bénéficiaire de la C2S non participative (ex-CMUC)",
        "C_C2SP":"Population couverte bénéficiaire de la C2S participative (ex-ACS)",
        "DECUC_D1":"Premier décile du revenu déclaré (€)",
        "DECUC_D1D9":"Rapport interdécile (D9/D1) du revenu déclaré",
        "DECUC_D9":"Neuvième décile du revenu déclaré (€)",
        "DECUC_Q1":"1er quartile du revenu déclaré (€)",
        "DECUC_Q2":"Médiane du revenu déclaré (€)",
        "DECUC_Q3":"3ème quartile du revenu déclaré (€)",
        "DEFA_COLL":"Élèves au collège hors UPE2A, ULIS, SEGPA, 3e prépa métiers dont la PCS du représentant légal est défavorisée",
        "DEFA_LYC_GT":"Élèves au lycée général ou technologique dont la PCS du représentant légal est défavorisée",
        "DEFA_LYC_PRO":"Élèves au lycée professionnel dont la PCS du représentant légal est défavorisée",
        "DISP_D1":"Premier décile du revenu disponible (€)",
        "DISP_D1D9":"Rapport interdécile (D9/D1) du revenu disponible",
        "DISP_D9":"Neuvième décile du revenu disponible (€)",
        "DISP_Q1":"1er quartile du revenu disponible (€)",
        "DISP_Q2":"Médiane du revenu disponible (€)",
        "DISP_Q3":"3ème quartile du revenu disponible (€)",
        "EC_ELEM":"Écoles élémentaires",
        "EC_ELEM_PRI":"Écoles élémentaires privées",
        "EC_ELEM_REP":"Écoles élémentaires en REP",
        "EC_ELEM_REP_P":"Écoles élémentaires en REP plus",
        "EC_MAT":"Écoles maternelles",
        "EC_MAT_PRI":"Écoles maternelles privées",
        "EC_MAT_REP":"Écoles maternelles en REP",
        "EC_MAT_REP_P":"Écoles maternelles en REP plus",
        "EC_PRI":"Écoles primaires",
        "EC_PRI_PRI":"Écoles primaires privées",
        "EC_PRI_REP":"Écoles primaires en REP",
        "EC_PRI_REP_P":"Écoles primaires en REP plus",
        "EFF_COLL":"Élèves scolarisés dans une formation au collège hors UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_COLSEG":"Élèves scolarisés dans une formation au collège y compris UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_COLSEG_F":"Filles scolarisées dans une formation au collège y compris UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_COLSEG_PRI":"Élèves scolarisés dans une formation au collège dans un établissement privé y compris UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_COLSEG_REP":"Élèves scolarisés dans une formation au collège en REP y compris UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_COLSEG_REP_P":"Élèves scolarisés dans une formation au collège en REP+ y compris UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_DNB_PRI":"Nombre de présentés au brevet - établissements privés",
        "EFF_DNB_PRI_F":"Nombre de filles présentées au brevet - établissements privés",
        "EFF_DNB_PRI_H":"Nombre de garçons présentés au brevet - établissements privés",
        "EFF_DNB_PUB":"Nombre de présentés au brevet - établissements publics",
        "EFF_DNB_PUB_F":"Nombre de filles présentées au brevet - établissements publics",
        "EFF_DNB_PUB_H":"Nombre de garçons présentés au brevet - établissements publics",
        "EFF_E0_GT":"Élèves en terminale générale ou technologique",
        "EFF_E0_GT_F":"Filles en terminale générale ou technologique",
        "EFF_E0_PRO":"Élèves en terminale professionnelle",
        "EFF_E0_PRO_F":"Filles en terminale professionnelle",
        "EFF_E2_GT":"Élèves en 2de générale ou technologique",
        "EFF_E2_GT_F":"Filles en 2de générale ou technologique",
        "EFF_E2_PRO":"Élèves en 2de professionnelle",
        "EFF_E2_PRO_F":"Filles en 2de professionnelle",
        "EFF_E3":"Élèves en 3e hors UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_E3_F":"Filles en 3e hors UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "EFF_E6":"Élèves en 6e hors UPE2A, ULIS, SEGPA",
        "EFF_E6_F":"Filles en 6e hors UPE2A, ULIS, SEGPA",
        "EFF_ECO":"Écoliers",
        "EFF_ECO_3":"Écoliers de moins de 3 ans",
        "EFF_LYC_GT":"Élèves scolarisés dans une formation générale ou technologique des lycées",
        "EFF_LYC_GT_F":"Filles scolarisées dans une formation générale ou technologique des lycées",
        "EFF_LYC_GT_PRI":"Élèves scolarisés dans une formation générale ou technologique des lycées dans un établissement privé",
        "EFF_LYC_PRO":"Élèves scolarisés dans une formation professionnelle des lycées",
        "EFF_LYC_PRO_F":"Filles scolarisées dans une formation professionnelle des lycées",
        "EFF_PRE":"Écoliers en niveau préélémentaire",
        "EFF_REU_DNB_PRI":"Nombre de réussites au brevet - établissements privés",
        "EFF_REU_DNB_PRI_F":"Nombre de réussites des filles au brevet - établissements privés",
        "EFF_REU_DNB_PRI_H":"Nombre de réussites des garçons au brevet - établissements privés",
        "EFF_REU_DNB_PUB":"Nombre de réussites au brevet - établissements publics",
        "EFF_REU_DNB_PUB_F":"Nombre de réussites des filles au brevet - établissements publics",
        "EFF_REU_DNB_PUB_H":"Nombre de réussites des garçons au brevet - établissements publics",
        "EFF_SEGPA":"Élèves scolarisés dans une formation UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "ENF":"Enfants couverts par au moins une prestation CAF",
        "ENF_2":"Enfants de moins de 3 ans",
        "ENF_3_5":"Enfants de 3 à moins de 6 ans",
        "ENF_6_10":"Enfants de 6 à moins de 11 ans",
        "ENF_11_14":"Enfants de 11 à moins de 15 ans",
        "ENF_15_17":"Enfants de 15 à moins de 18 ans",
        "ENF_18_24":"Enfants de 18 à moins de 25 ans",
        "ENS_SANTE":"Nb. étab. service aux particuliers Enseignement, santé et action sociale",
        "EVOL_ABC_F_T4":"Évolution annuelle du nombre de DEFM femmes catégories ABC",
        "EVOL_ABC_H_T4":"Évolution annuelle du nombre de DEFM hommes catégories ABC",
        "EVOL_ABC_T4":"Évolution annuelle du nombre de DEFM catégories ABC",
        "EVOL_A_F_T4":"Évolution annuelle du nombre de DEFM femmes catégorie A",
        "EVOL_A_H_T4":"Évolution annuelle du nombre de DEFM hommes catégorie A",
        "EVOL_A_T4":"Évolution annuelle du nombre de DEFM catégorie A",
        "EVOL_CA_N_E":"Évolution annuelle du nombre de contrats aidés",
        "FAV_COLL":"Élèves au collège hors UPE2A, ULIS, SEGPA, 3e prépa métiers dont la PCS du représentant légal est favorisée",
        "FAV_LYC_GT":"Élèves au lycée général ou technologique dont la PCS du représentant légal est favorisée",
        "FAV_LYC_PRO":"Élèves au lycée professionnel dont la PCS du représentant légal est favorisée",
        "FINANCE":"Nb. étab. service aux entreprises Activités financières et d'assurance",
        "IMMO":"Nb. étab. service aux entreprises Activités immobilières",
        "IMPLANT":"Créations et transferts d'établissements",
        "INDUS":"Nb. étab. Industrie",
        "IND_JEUNE":"Indice de jeunesse",
        "INFO_COM":"Nb. étab. service aux entreprises Information et communication",
        "LIB_COM":"Libellé géographique de la (des) commune(s) englobante(s)",
        "LYC":"Lycées",
        "LYC_GT":"Lycées généraux ou technologiques",
        "LYC_GT_PRI":"Lycées généraux ou technologiques privés",
        "LYC_PRO":"Lycées professionnels",
        "LYC_PRO_PRI":"Lycées professionnels privés",
        "MOY_COLL":"Élèves au collège hors UPE2A, ULIS, SEGPA, 3e prépa métiers dont la PCS du représentant légal est moyenne",
        "MOY_LYC_GT":"Élèves au lycée général ou technologique dont la PCS du représentant légal est moyenne",
        "MOY_LYC_PRO":"Élèves au lycée professionnel dont la PCS du représentant légal est moyenne",
        "NBETAB":"Nombre d'établissements",
        "NOTE":"Note de diffusion",
        "NOTE":"Note de diffusion",
        "NOTE_ASP":"Note de diffusion ASP",
        "NOTE_ASP_E":"Note de diffusion ASP pour évolution",
        "NOTE_BASE_ELEVES":"Note de diffusion approche élève",
        "NOTE_BASE_ETABLI":"Note de diffusion approche établissement",
        "NOTE_CNAF":"Note de diffusion Cnaf",
        "NOTE_CNAM":"Note de diffusion Cnam",
        "NOTE_FILO":"Note de diffusion Filosofi",
        "NOTE_PE":"Note de diffusion Pôle Emploi",
        "NOTE_PE_T":"Note de diffusion Pôle Emploi trimestriel pour évolution",
        "NOTE_RP":"Note de diffusion RP",
        "PACT":"Part des revenus d'activités (%)",
        "PERCOU":"Personnes couvertes",
        "PIMPOT":"Part des impôts (%)",
        "PMIMP":"Part des ménages imposés (%)",
        "POP_COM":"Population municipale de la (des) commune(s) englobante(s)",
        "POP_COM_QP":"Population municipale en QPV de la (des) commune(s) englobante(s)",
        "POP_MUN":"Population municipale",
        "PPAT":"Part des revenus du patrimoine (%)",
        "PPEN":"Part des pensions, retraites et rentes (%)",
        "PPSOC":"Part de l'ensemble des prestations sociales (%)",
        "PartResPrincAch06_15":"Part des résidences principales achevées entre 2006 et 2015",
        "RET_E0_GT":"Retards en terminale générale ou technologique",
        "RET_E0_GT_F":"Retards des filles en terminale générale ou technologique",
        "RET_E0_PRO":"Retards en terminale professionnelle",
        "RET_E0_PRO_F":"Retards des filles en terminale professionnelle",
        "RET_E2_GT":"Retards en 2de générale ou technologique",
        "RET_E2_GT_F":"Retards des filles en 2de générale ou technologique",
        "RET_E2_PRO":"Retards en 2de professionnelle",
        "RET_E2_PRO_F":"Retards des filles en 2de professionnelle",
        "RET_E3":"Retards en 3e hors UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "RET_E3_F":"Retards des filles en 3e hors UPE2A, ULIS, SEGPA, 3e prépa métiers",
        "RET_E6":"Retards en 6e hors UPE2A, ULIS, SEGPA",
        "RET_E6_F":"Retards des filles en 6e hors UPE2A, ULIS, SEGPA",
        "SALA00NN":"Nb. étab. n'ayant pas de salarié",
        "SALA50PL":"Nb. étab. dont le nombre de salariés est supérieur ou égal à 50",
        "SALA0149":"Nb. étab. dont le nombre de salariés compris entre 1 et 49",
        "SERV_ENT":"Nb. étab. de service Tous services aux entreprises",
        "SERV_PAR":"Nb. étab. de service Tous services aux particuliers",
        "SERV_TOT":"Nb. étab. de service Tous services",
        "SPECIAL":"Nb. étab. service aux entreprises Activités spécialisées, scientifiques et techniques, services administratifs et de soutien",
        "TFAV_COLL":"Élèves au collège hors UPE2A, ULIS, SEGPA, 3e prépa métiers dont la PCS du représentant légal est très favorisée",
        "TFAV_LYC_GT":"Élèves au lycée général ou technologique dont la PCS du représentant légal est très favorisée",
        "TFAV_LYC_PRO":"Élèves au lycée professionnel dont la PCS du représentant légal est très favorisée",
        "TP60":"Taux de pauvreté au seuil de 60% du revenu disponible",
        "TRANSFERT":"Transferts d'établissements",
        "TX_ET_EDLIM":"Part des emplois à durée limitée parmi les emplois chez les étrangers",
        "TX_ET_EMPL":"Taux d'emploi des étrangers",
        "TX_F":"Part des femmes parmi la population",
        "TX_FAM_MONO":"Part des familles monoparentales",
        "TX_F_0A24":"Part des femmes de 0 à 24 ans parmi les femmes",
        "TX_F_60ETPLUS":"Part des femmes de 60 ans ou plus parmi les femmes",
        "TX_F_EDLIM":"Part des emplois à durée limitée parmi les emplois chez les femmes",
        "TX_F_EMPL":"Taux d'emploi des femmes",
        "TX_F_ET":"Part des étrangères parmi les femmes",
        "TX_NSNE_16A25":"Part des 16-25 ans non scolarisés et sans emploi",
        "TX_TOT_0A24":"Part des personnes de 0 à 24 ans parmi la population",
        "TX_TOT_60ETPLUS":"Part des personnes de 60 ans ou plus parmi la population",
        "TX_TOT_EDLIM":"Part des emplois à durée limitée parmi les emplois",
        "TX_TOT_EMPL":"Taux d'emploi des 15-64 ans",
        "TX_TOT_ET":"Part des étrangers parmi la population",
        "TX_TOT_MEN1":"Part des ménages de 1 personne parmi les ménages",
        "TX_TOT_MEN6":"Part des ménages de 6 personnes ou plus parmi les ménages",
        "medLoy":"Loyer par m² des logements sociaux -  Médiane",
        "moyLoy":"Loyer par m² des logements sociaux - Moyenne",
        "nbLsMes":"Nombre de logements sociaux mis en service dans l’année",
        "nbLsPls":"Nombre de logements du Parc Locatif Social",
        "nbPersResPrinc":"Nombre de personnes par résidence principale",
        "noteFid":"Note de diffusion Fideli",
        "noteRp":"Note de diffusion RP",
        "noteRpls1":"Note de diffusion RPLS",
        "noteRpls2":"Note de diffusion part LS",
        "partEntAutQp":"Dont part des entrants depuis un autre QPV",
        "partMenLoc":"Part des ménages locataires",
        "partMenLogGrat":"Part des ménages logés gratuitement",
        "partMenProp":"Part des ménages propriétaires",
        "partResPrinc":"Part des résidences principales",
        "partResPrinc1pi":"Part des résidences principales d'une pièce",
        "partResPrinc2pi":"Part des résidences principales de 2 pièces",
        "partResPrinc3pi":"Part des résidences principales de 3 pièces",
        "partResPrinc4pi":"Part des résidences principales de 4 pièces",
        "partResPrinc5pi":"Part des résidences principales de 5 pièces et plus",
        "partResPrinc30":"Part des résidences principales de moins de 30 m²",
        "partResPrinc30_40":"Part des résidences principales de 30 à moins de 40 m²",
        "partResPrinc40_60":"Part des résidences principales de 40 à moins de 60 m²",
        "partResPrinc60_80":"Part des résidences principales de 60 à moins de 80 m²",
        "partResPrinc80_100":"Part des résidences principales de 80 à moins de 100 m²",
        "partResPrinc100_120":"Part des résidences principales de 100 à moins de 120 m²",
        "partResPrinc120plus":"Part des résidences principales de 120 m² ou plus",
        "partResPrincAch19":"Part des résidences principales achevées avant 1919",
        "partResPrincAch19_45":"Part des résidences principales achevées entre 1919 et 1945",
        "partResPrincAch46_70":"Part des résidences principales achevées entre 1946 et 1970",
        "partResPrincAch71_90":"Part des résidences principales achevées entre 1971 et 1990",
        "partResPrincAch91_05":"Part des résidences principales achevées entre 1991 et 2005",
        "partResPrincApp":"Part des résidences principales de type appartement",
        "partResPrincEmm2":"Part des ménages ayant emménagé depuis moins de 2 ans",
        "partResPrincEmm2_4":"Part des ménages ayant emménagé entre 2 et 4 ans",
        "partResPrincEmm5_9":"Part des ménages ayant emménagé entre 5 et 9 ans",
        "partResPrincEmm10":"Part des ménages ayant emménagé depuis 10 ans ou plus",
        "partResPrincMai":"Part des résidences principales de type maison",
        "partResPrincSurocc":"Part des résidences principales hors studio de 1 personne suroccupées",
        "partResPrincSurocc2pi":"Part des résidences principales de 2 pièces suroccupées",
        "partResPrincSurocc3pi":"Part des résidences principales de 3 pièces suroccupées",
        "partResPrincSurocc4pi":"Part des résidences principales de 4 pièces suroccupées",
        "partResPrincSurocc5pi":"Part des résidences principales de 5 pièces et plus suroccupées",
        "partResSec":"Part des résidences secondaires",
        "partResVac":"Part des logements vacants",
        "partResoccas":"Part des logements occasionnels",
        "partSortAutQp":"Dont part de sortants vers un autre QPV",
        "q1Loy":"Loyer par m² des logements sociaux -  1er quartile",
        "q3Loy":"Loyer par m² des logements sociaux -  3ème quartile",
        "txEnt":"Taux d’entrants",
        "txLs01a13":"Part des logements sociaux construits entre 2001 et 2013",
        "txLs1p":"Part des logements sociaux d’une pièce",
        "txLs2p":"Part des logements sociaux de deux pièces",
        "txLs3p":"Part des logements sociaux de trois pièces",
        "txLs4p":"Part des logements sociaux de quatre pièces",
        "txLs5pp":"Part des logements sociaux de 5 pièces ou plus",
        "txLs30a40":"Part des logements sociaux de 30 m² à moins de 40 m²",
        "txLs40a60":"Part des logements sociaux de 40 m² à moins de 60 m²",
        "txLs49a75":"Part des logements sociaux construits entre 1949 et 1975",
        "txLs60a80":"Part des logements sociaux de 60 m² à moins de 80 m²",
        "txLs76a88":"Part des logements sociaux construits entre 1976 et 1988",
        "txLs80a100":"Part des logements sociaux de 80 m² à moins de 100 m²",
        "txLs89a00":"Part des logements sociaux construits entre 1989 et 2000",
        "txLs100a120":"Part des logements sociaux de 100 m² à moins de 120 m²",
        "txLs120p":"Part des logements sociaux de plus de 120 m²",
        "txLsAp13":"Part des logements sociaux construits après 2013",
        "txLsAv49":"Part des logements sociaux construits avant 1949",
        "txLsCol":"Part des logements sociaux collectifs",
        "txLsInd":"Part des logements sociaux individuels",
        "txLsPlai":"Part des logements sociaux financés en PLAI",
        "txLsPli":"Part des logements sociaux financés en PLI",
        "txLsPls":"Part des logements sociaux financés en PLS",
        "txLsPlusAp77":"Part des logements sociaux financés en PLUS (après 1977)",
        "txLsPlusAv77":"Part des logements sociaux financés en PLUS (avant 1977)",
        "txLsRp":"Part des logements sociaux parmi les résidences principales",
        "txLsm30":"Part des logements sociaux de moins de 30 m²",
        "txMobGlob":"Taux d’emménagement",
        "txMobInt":"Taux de mobilité interne",
        "txRot":"Taux de rotation des logements sociaux",
        "txSort":"Taux de sortants",
        "txVac":"Taux de vacance des logements sociaux",
        "txVac3m":"Taux de vacance de plus de 3 mois des logements sociaux"
        };

        $.each(objetAtraduire,function(k,v){
            
                trad[k] = v;
                
        })

    if(typeof formatNumber == "undefined") {
        function formatNumber(nStr) {
            let res = nStr
            if(!isNaN(nStr*1)) {
                res = (nStr*1).toLocaleString("fr-FR")
            }
            return res;
        }
    }

    var observatoryObj = {
        formId : queryParams.formid,
        context: queryParams.context,
        defaultMap: "assos-adress",
        listQuartierfiltre: {},
        projectData : {},
        proposalData : {},
        dataCosindni : {},
        list_table:{},
        list_ids:{"getDataInseeabs":[], "getDataInsee": [],"getDataInseervn": [],"getDataInseeeduc": [],"getDataInseeinsertionPro": [],"getDataInseelogement": [],"getDataInseetissuEco": []},
        graphLabels : <?= json_encode($graphLabels) ?>,
        init : function(obj){
            obj.commons.getDataCosindni(obj);
            obj.filters(obj);
            obj.commons.directoryQuartiers.filters(obj);
            obj.commons.mapQuartiers.initMap(obj);
            obj.events(obj);
        },
        initCharts : function(obj,params){
            this.listQuartierfiltre = params.quartiers;
            obj.charts.combineNumberOfAction(obj,params);
            obj.charts.combineFinancement(obj,params);
            obj.charts.totalFinanceByYear(obj, params);
            obj.charts.financingPerFunder(obj,params);
            obj.charts.getDataInsee(obj, params);

        },
        initMetrics : function(obj,params){
            obj.metrics.combineMetrics(obj,params);
        },
        initMap : function(obj,params){
            if(obj.defaultMap == "assos-adress")
                obj.commons.mapQuartiers.getCurrentProposalAdress(obj,params);
            else if(obj.defaultMap == "quartier-adress")
                obj.commons.mapQuartiers.getCurrentQuartierAdress(obj,params);
            $('.observatory-aap .switch-map').off().on('click',function(){
                const val = $(this).data('value');
                $('.chart-and-metrics,#table-container').hide();
                $('#map-container').show(500);
                if(val =="assos-adress")
                    obj.commons.mapQuartiers.getCurrentProposalAdress(obj,params);
                else if(val =="quartier-adress")
                    obj.commons.mapQuartiers.getCurrentQuartierAdress(obj,params);
                obj.defaultMap = val;
            })
        },
        initTable : function(obj,params){
            obj.table.combineTable(obj,params);
        },
        metrics : {
            "combineMetrics" : function(obj,params){
                ajaxPost(
                    "",
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/combineMetrics",
                    {
                        formId : params.formId,
                        context : params.context,
                        quartiers : params.quartiers,
                        financers : params.financers,
                        tags : params.tags,
                        session : params.session,
                    },
                    function (data) {
                        $("#totalProject").html(data.projectMetrics.total);
                        $("#runningProject").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.projectMetrics.inprogress);
                        $("#doneProject").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.projectMetrics.finished);
                        
                        if(data.projectMetrics.finished <= 0) {
                            $("#doneProject").off("click").removeClass("cursor-pointer")
                        }
                        if(data.projectMetrics.inprogress <= 0) {
                            $("#runningProject").off("click").removeClass("cursor-pointer")
                        }
                        observatoryObj.projectData = {
                            inprogress : data.projectMetrics.inprogressData,
                            finished : data.projectMetrics.finishedData
                        }

                        $("#totalActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.total);
                        $("#todoActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.todo);
                        $("#runningActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.running);
                        $("#doneActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.done);
                        $("#deadlinePassedActions").removeClass('pointer').addClass('pointer').html(data.actionMetrics.deadlinePassed);

                        $("#totalProposal").html(data.proposalMetrics.total);
                        $("#voteProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.vote);
                        $("#toBeFinancedProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.toBefinanced);
                        $("#inFinancingProposal").removeClass('cursor-pointer').addClass('cursor-pointer').html(data.proposalMetrics.inFinancing);
                        
                        if(data.proposalMetrics.vote <= 0)
                            $("#voteProposal").off("click").removeClass("cursor-pointer")
                        if(data.proposalMetrics.toBefinanced <= 0)
                            $("#toBeFinancedProposal").off("click").removeClass("cursor-pointer")
                        if(data.proposalMetrics.inFinancing <= 0)
                            $("#inFinancingProposal").off("click").removeClass("cursor-pointer")

                        if(data.actionMetrics.total <= 0)
                            $("#totalActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.todo <= 0)
                            $("#todoActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.running <= 0)
                            $("#runningActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.done <= 0)
                            $("#doneActions").off("click").removeClass("pointer")
                        if(data.actionMetrics.deadlinePassed <= 0)
                            $("#deadlinePassedActions").off("click").removeClass("pointer")

                        if (typeof aapObj?.form?.params.onlyAdminCanSeeList != "undefined" && aapObj.form?.params.onlyAdminCanSeeList == true) {
                            if(!aapObj.canEdit) {
                                $("#totalProject,#runningProject,#doneProject,#totalActions,#todoActions,#runningActions,#doneActions,#deadlinePassedActions,#totalProposal,#voteProposal,#toBeFinancedProposal,#inFinancingProposal").off("click").removeClass("cursor-pointer pointer")
                            }
                        }
                        observatoryObj.proposalData = {
                            infinancing : data.proposalMetrics.infinancingData,
                            tobevoted : data.proposalMetrics.tobevotedData,
                            tobefinanced : data.proposalMetrics.tobefinancedData
                        }

                        $("#amountRequested").html(addSeparatorMillier(data.financingMetrics.amountRequested)+' \u20ac');
                        $("#amountFinanced").html(addSeparatorMillier(data.financingMetrics.amountFinanced)+' \u20ac');
                        $("#amountSpent").html(addSeparatorMillier(data.financingMetrics.amountSpent)+' \u20ac');
                        $("#amountRemaining").html(addSeparatorMillier(data.financingMetrics.amountRemaining)+' \u20ac');
                    }
                );
            }
        },
        table : {
            "combineTable" : function(obj,params){
                ajaxPost(
                    "",
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/combineTable",
                    {
                        form : params.formId,
                        context : params.context,
                        quartiers : params.quartiers,
                        financers : params.financers,
                        tags : params.tags,
                        session : params.session,
                    },
                    function (data) {
                        $.each(data,function(k,v){
                            obj.table[k]?.(obj,k,v);
                        })
                    }
                );
            },
            "fundingByYearAndFinancor" : function(obj,k,v){
                let thead =`<th class="text-left h4">Budget du quartier</th>`;
                let tbodyTotalFinancers =`<tr><td class="text-left h4 bold">Montant investi par les financeurs</td>`;
                let tbodyTotalRequested =`<tr><td class="text-left h4 bold">Montant sollicité</td>`;
                let tbodyTotalRest =`<tr><td class="text-left h4 bold">Reste à financer</td>`;
                let tbodyTotalActions =`<tr><td class="text-left h4 bold">Actions</td>`;

                const arrayFinancer = [];
                const arrayYear = [];
                const label = "Tableau financement par année";
                const moneyUnity = " <i class='fa fa-euro'></i>";
                
                $.each(v,function(key,value){
                    thead += `<th class="h4">${value["_id"]}</th>`;
                    tbodyTotalFinancers += `<td class="text-left h4">${value?.['financed'] ? addSeparatorMillier(value['financed'])+moneyUnity:""}</td>`;
                    tbodyTotalRequested += `<td class="text-left h4">${value?.['requested'] ? addSeparatorMillier(value['requested'])+moneyUnity:""}</td>`;
                    tbodyTotalRest += `<td class="text-left h4">${value?.['rest'] ? addSeparatorMillier(value?.['rest'])+moneyUnity : ""}</td>`;
                    tbodyTotalActions += `<td class="text-left h4">${addSeparatorMillier(value['actions'])}</td>`
                    
                    arrayYear.push(value["_id"]);
                    $.each(value["funders"],function(kf,vf){
                        if(!arrayFinancer.includes(kf))
                            arrayFinancer.push(kf)
                    })
                })
                tbodyTotalFinancers+=`</tr>`;
                tbodyTotalRequested+=`</tr>`;
                tbodyTotalRest+=`</tr>`;
                tbodyTotalActions+=`</tr>`;

                let tr = ``;
                $.each(arrayFinancer,function(kf,vf){
                    tr += `<tr><td class="text-left h4">${vf}</td>`;
                    $.each(arrayYear,function(ky,vy){
                        var classs = slugify(vf+"-"+vy)?.replace(/[^a-zA-Z0-9]/g, '');
                        tr +=`<td class="${slugify(classs)} text-left h4"> </td>`;
                    })
                    tr += `<tr>`;
                })

                const tbody = tbodyTotalFinancers +tbodyTotalRequested+ tbodyTotalRest+ tbodyTotalActions +tr;
                const table = obj.tableType({thead,tbody,label:"Tableau de financement"});
                $('#'+k).html(table);

                $.each(v,function(key,value){
                    const year = value["_id"];
                    $.each(value["funders"],function(kf,vf){
                        var classs = slugify(kf+"-"+year)?.replace(/[^a-zA-Z0-9]/g, '');
                        $(`.${classs}`).html(addSeparatorMillier(vf)+moneyUnity);
                    })
                })
            }
        },
        charts : {
            "combineNumberOfAction" : function(obj,params){
                ajaxPost(
                    null,
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/combineNumberOfAction",
                    {
                        form : params.formId,
                        quartiers : params.quartiers,
                        financers : params.financers,
                        tags : params.tags,
                        session : params.session
                    },
                    function(data){
                        /*const chartsLabel = {
                            numberOfActionPerYear : "Nombre d'action par année",
                            numberOfActionPerThematique : "Nombre d'action par thématique",
                            numberOfActionPerPilier : "Nombre d'action par pilier",
                            numberOfActionPerAssos : "Nombre d'action par association",
                            numberOfActionPerFinancer : "Nombre d'action par financeur",
                            numberOfActionPerAxis : "Nombre d'action par Axe",
                            numberOfActionPerSpecificGoals : "Nombre d'action par objectif spécifique",
                        }*/
                        if(Object.values(data?.numberOfActionPerAssos).length > 0) {
                            $("#numberOfActionPerAssos-container .card-content").css("height", Object.values(data?.numberOfActionPerAssos).length *12)
                        } else
                            $("#numberOfActionPerAssos-container .card-content").css("height", 200)
                        $.each(data,function(keyChart,valueDataChart){
                            const dataLength = Object.values(valueDataChart).map(i => i.count).length;
                            // if(dataLength > 10)
                            //     $(`#${keyChart}-container .card-content`).css("min-height", dataLength *10)
                            if(aapObj.form.GraphObservatory[keyChart])
                                obj.chartTypes.bar({
                                    id: keyChart,
                                    labels: Object.values(valueDataChart).map(i => i._id),
                                    data: Object.values(valueDataChart).map(i => i.count),
                                    label: obj.graphLabels[keyChart],
                                });
                        })
                    }
                );
            },
            "combineFinancement" : function(obj,params){
                ajaxPost(
                    null,
                baseUrl+"/costum/coSindniSmarterre/observatory/request/combineFinancement",
                    {
                        form : params.formId,
                        quartiers : params.quartiers,
                        financers : params.financers,
                        tags : params.tags,
                        session : params.session
                    },
                    function(data){
                        /*const chartsLabel = {
                            financementPerYear : "Financement par année",
                            financementPerThematique : "Financement par thématique",
                            financementPerPilier : "Financement par pilier",
                            financementPerAssoc : "Financement par association",
                            financementPerFinancer : "Financement par financeur",
                            financementPerQuartier : "Financement par quartier",
                            financementPerAxis : "Financement par axes",
                            financementPerSpecificGoals : "Financement par objectifs spécifique"
                        }*/
                        if(Object.values(data?.financementPerAssoc).length > 0) {
                            $("#financementPerAssoc-container .card-content").css("height", Object.values(data?.financementPerAssoc).length *12)
                        } else
                            $("#financementPerAssoc-container .card-content").css("height", 200)
                        trad['65297ce233cb5d57cc6a4c48'] = 'Saint-Denis Sainte Clotilde'; // j'ai insérer directement la traduction mais il faudrait corriger la liste des quartiers dans le filtre
                        $.each(data,function(keyChart,valueDataChart){
                            if(aapObj.form.GraphObservatory[keyChart])
                                obj.chartTypes.bar({
                                    id: keyChart,
                                    labels: Object.keys(valueDataChart),
                                    data: Object.values(valueDataChart),
                                    label: obj.graphLabels[keyChart],
                                });
                        })
                    }
                    );
            },
            "totalFinanceByYear" : function (obj, params){
                ajaxPost(
                    "",
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/totalFinanceByYear",
                    {
                        form: params.formId,
                        context: params.context,
                        filter: params.filter,
                        financers : params.financers,
                        tags : params.tags,
                        quartiers : params.quartiers
                    },
                    function(data){
                        if(aapObj.form.GraphObservatory["totalFinanceByYear"])
                            expFinPayment = obj.chartTypes.barStacked({
                                id: "totalFinanceByYear",
                                labels: data.years,
                                data: Object.values(data.data),
                                label: obj.graphLabels["totalFinanceByYear"],
                            });
                    }
                );
            },

            "getDataInsee": function (obj, parames)
            {
                let quartiers = Object.keys(params.quartiers);
                if(Array.isArray(observatoryObj.listQuartierfiltre))
                {
                    quartiers = observatoryObj.listQuartierfiltre;
                }

                observatoryObj.list_table = JSON.parse(localStorage.getItem('listgraphe'));

                if(!observatoryObj.list_table)
                {
                    observatoryObj.list_table = {
                        "getDataInseeabs": {"POP_MUN" : [1, 1],"POP_COM_QP" : [1, 1],"POP_COM" : [1, 1],"TX_F" : [1, 1],"TX_TOT_0A24" : [1, 1],"TX_F_0A24" : [1, 1],"ACSSENF" : [1, 1],"ACAVENF" : [1, 1],"NOTE_FILO" : [1, 1],"EFF_DNB_PUB_F" : [1, 1],"EFF_REU_DNB_PUB_H" : [1, 1],"EFF_DNB_PUB_H" : [1, 1],"EFF_REU_DNB_PRI" : [1, 1],"EFF_DNB_PRI" : [1, 1],"EFF_REU_DNB_PRI_F" : [1, 1],"EFF_DNB_PRI_F" : [1, 1],"EFF_REU_DNB_PRI_H" : [1, 1],"EFF_DNB_PRI_H" : [1, 1],"B2" : [1, 1],"B2_1_GEN" : [1, 1],"B2_1_TEC" : [1, 1],"B2_1_PRO" : [1, 1],"B2_CAP2" : [1, 1],"B2_AUTR" : [1, 1],"B2_RET" : [1, 1],"NOTE_BASE_ETABLI" : [1, 1],"TX_NSNE_16A25" : [1, 1],"EFF_COLSEG" : [1, 1],"EFF_COLSEG_F" : [1, 1],"EFF_COLSEG_REP" : [1, 1],"EFF_COLSEG_REP_P" : [1, 1],"EFF_COLSEG_PRI" : [1, 1],"EFF_SEGPA" : [1, 1],"EFF_COLL" : [1, 1],"RET_E6" : [1, 1],"RET_E6_F" : [1, 1],"RET_E3" : [1, 1],"RET_E3_F" : [1, 1],"EFF_E6" : [1, 1],"EFF_E6_F" : [1, 1],"EFF_E3" : [1, 1],"EFF_E3_F" : [1, 1],"TFAV_COLL" : [1, 1],"FAV_COLL" : [1, 1],"MOY_COLL" : [1, 1],"DEFA_COLL" : [1, 1],"EFF_LYC_GT" : [1, 1],"EFF_LYC_GT_F" : [1, 1],"EFF_LYC_GT_PRI" : [1, 1],"RET_E2_GT" : [1, 1],"RET_E2_GT_F" : [1, 1],"RET_E0_GT" : [1, 1],"RET_E0_GT_F" : [1, 1],"EFF_E2_GT" : [1, 1],"EFF_E2_GT_F" : [1, 1],"EFF_E0_GT" : [1, 1],"EFF_E0_GT_F" : [1, 1],"TFAV_LYC_GT" : [1, 1],"FAV_LYC_GT" : [1, 1],"MOY_LYC_GT" : [1, 1],"DEFA_LYC_GT" : [1, 1],"EFF_LYC_PRO" : [1, 1],"EFF_LYC_PRO_F" : [1, 1],"RET_E2_PRO" : [1, 1],"RET_E2_PRO_F" : [1, 1],"RET_E0_PRO" : [1, 1],"RET_E0_PRO_F" : [1, 1],"txLs100a120" : [1, 1],"txLs120p" : [1, 1],"txLsAv49" : [1, 1],"txLs49a75" : [1, 1],"txLs76a88" : [1, 1],"txLs89a00" : [1, 1],"txLs01a13" : [1, 1],"txLsAp13" : [1, 1],"txLsPlai" : [1, 1],"txLsPlusAv77" : [1, 1],"txLsPlusAp77" : [1, 1],"txLsPls" : [1, 1],"txLsPli" : [1, 1],"moyLoy" : [1, 1],"q1Loy" : [1, 1],"medLoy" : [1, 1],"q3Loy" : [1, 1],"noteRpls1" : [1, 1],"txLsRp" : [1, 1],"noteRpls2" : [1, 1],"txMobGlob" : [1, 1],"txSort" : [1, 1],"partSortAutQp" : [1, 1],"txEnt" : [1, 1],"partEntAutQp" : [1, 1],"txMobInt" : [1, 1],"noteFid" : [1, 1], "NBETAB" : [1, 1],"INDUS" : [1, 1],"CONSTR" : [1, 1],"COM_TRANSP" : [1, 1],"COM_GROS" : [1, 1],"COM_DETAIL" : [1, 1],"INFO_COM" : [1, 1],"FINANCE" : [1, 1],"TRANSFERT" : [1, 1],"IMPLANT" : [1, 1],"AUTO_ENT" : [1, 1],"NOTE" : [1, 1]},
                        "getDataInsee": {"POP_MUN" : [1, 1],"POP_COM_QP" : [1, 1],"POP_COM" : [1, 1],"TX_F" : [1, 1],"TX_TOT_0A24" : [1, 1],"TX_F_0A24" : [1, 1],"TX_TOT_60ETPLUS" : [1, 1],"TX_F_60ETPLUS" : [1, 1],"IND_JEUNE" : [1, 1],"TX_TOT_MEN1" : [1, 1],"TX_TOT_MEN6" : [1, 1],"TX_FAM_MONO" : [1, 1],"TX_TOT_ET" : [1, 1],"TX_F_ET" : [1, 1],"NOTE_RP" : [1, 1],"PERCOU" : [1, 1],"AM" : [1, 1],"AI" : [1, 1],"ACSSENF" : [1, 1],"ACAVENF" : [1, 1],"AC3ENF" : [1, 1],"A_NETUD_24" : [1, 1],"A_ETUD" : [1, 1],"ENF" : [1, 1],"ENF_2" : [1, 1],"ENF_3_5" : [1, 1],"ENF_6_10" : [1, 1],"ENF_11_14" : [1, 1],"ENF_15_17" : [1, 1],"ENF_18_24" : [1, 1]},
                        "getDataInseervn": {"PMIMP" : [1, 1],"DECUC_Q2" : [1, 1],"DECUC_Q1" : [1, 1],"DECUC_Q3" : [1, 1],"BREV" : [1, 1],"DECUC_D1" : [1, 1],"DECUC_D9" : [1, 1],"DECUC_D1D9" : [1, 1],"DISP_Q2" : [1, 1],"DISP_Q1" : [1, 1],"DISP_Q3" : [1, 1],"TP60" : [1, 1],"DISP_D1" : [1, 1],"DISP_D9" : [1, 1],"DISP_D1D9" : [1, 1],"PACT" : [1, 1],"PPEN" : [1, 1],"PPSOC" : [1, 1],"PIMPOT" : [1, 1],"PPAT" : [1, 1],"NOTE_FILO" : [1, 1],"C" : [1, 1],"C_C2SNP" : [1, 1],"C_C2SP" : [1, 1],"NOTE_CNAM" : [1, 1],"A" : [1, 1],"APPA" : [1, 1],"ARSAS" : [1, 1],"AAAH" : [1, 1],"NOTE_CNAF" : [1, 1]},
                        "getDataInseeeduc": {"EC_MAT" : [1, 1],"EC_MAT_REP" : [1, 1],"EC_MAT_REP_P" : [1, 1],"EC_MAT_PRI" : [1, 1],"EC_ELEM" : [1, 1],"EC_ELEM_REP" : [1, 1],"EC_ELEM_REP_P" : [1, 1],"EC_ELEM_PRI" : [1, 1],"EC_PRI" : [1, 1],"EC_PRI_REP" : [1, 1],"EC_PRI_REP_P" : [1, 1],"EC_PRI_PRI" : [1, 1],"EFF_ECO" : [1, 1],"EFF_PRE" : [1, 1],"EFF_ECO_3" : [1, 1],"COLL" : [1, 1],"COLL_REP" : [1, 1],"COLL_REP_P" : [1, 1],"COLL_PRI" : [1, 1],"EFF_REU_DNB_PUB" : [1, 1],"EFF_DNB_PUB" : [1, 1],"EFF_REU_DNB_PUB_F" : [1, 1],"EFF_DNB_PUB_F" : [1, 1],"EFF_REU_DNB_PUB_H" : [1, 1],"EFF_DNB_PUB_H" : [1, 1],"EFF_REU_DNB_PRI" : [1, 1],"EFF_DNB_PRI" : [1, 1],"EFF_REU_DNB_PRI_F" : [1, 1],"EFF_DNB_PRI_F" : [1, 1],"EFF_REU_DNB_PRI_H" : [1, 1],"EFF_DNB_PRI_H" : [1, 1],"B2" : [1, 1],"B2_1_GEN" : [1, 1],"B2_1_TEC" : [1, 1],"B2_1_PRO" : [1, 1],"B2_CAP2" : [1, 1],"B2_AUTR" : [1, 1],"B2_RET" : [1, 1],"NOTE_BASE_ETABLI" : [1, 1],"TX_NSNE_16A25" : [1, 1],"EFF_COLSEG" : [1, 1],"EFF_COLSEG_F" : [1, 1],"EFF_COLSEG_REP" : [1, 1],"EFF_COLSEG_REP_P" : [1, 1],"EFF_COLSEG_PRI" : [1, 1],"EFF_SEGPA" : [1, 1],"EFF_COLL" : [1, 1],"RET_E6" : [1, 1],"RET_E6_F" : [1, 1],"RET_E3" : [1, 1],"RET_E3_F" : [1, 1],"EFF_E6" : [1, 1],"EFF_E6_F" : [1, 1],"EFF_E3" : [1, 1],"EFF_E3_F" : [1, 1],"TFAV_COLL" : [1, 1],"FAV_COLL" : [1, 1],"MOY_COLL" : [1, 1],"DEFA_COLL" : [1, 1],"EFF_LYC_GT" : [1, 1],"EFF_LYC_GT_F" : [1, 1],"EFF_LYC_GT_PRI" : [1, 1],"RET_E2_GT" : [1, 1],"RET_E2_GT_F" : [1, 1],"RET_E0_GT" : [1, 1],"RET_E0_GT_F" : [1, 1],"EFF_E2_GT" : [1, 1],"EFF_E2_GT_F" : [1, 1],"EFF_E0_GT" : [1, 1],"EFF_E0_GT_F" : [1, 1],"TFAV_LYC_GT" : [1, 1],"FAV_LYC_GT" : [1, 1],"MOY_LYC_GT" : [1, 1],"DEFA_LYC_GT" : [1, 1],"EFF_LYC_PRO" : [1, 1],"EFF_LYC_PRO_F" : [1, 1],"RET_E2_PRO" : [1, 1],"RET_E2_PRO_F" : [1, 1],"RET_E0_PRO" : [1, 1],"RET_E0_PRO_F" : [1, 1],"EFF_E2_PRO" : [1, 1],"EFF_E2_PRO_F" : [1, 1],"EFF_E0_PRO" : [1, 1],"EFF_E0_PRO_F" : [1, 1],"TFAV_LYC_PRO" : [1, 1],"FAV_LYC_PRO" : [1, 1],"MOY_LYC_PRO" : [1, 1],"DEFA_LYC_PRO" : [1, 1],"NOTE_BASE_ELEVES" : [1, 1]},
                        "getDataInseeinsertionPro": {"TX_TOT_EMPL" : [1, 1],"TX_F_EMPL" : [1, 1],"TX_ET_EMPL" : [1, 1],"TX_TOT_EDLIM" : [1, 1],"TX_F_EDLIM" : [1, 1],"TX_ET_EDLIM" : [1, 1],"CA" : [1, 1],"NOTE_ASP" : [1, 1],"EVOL_CA_N_E" : [1, 1],"NOTE_ASP_E" : [1, 1],"ABCDE" : [1, 1],"ABCDE_F" : [1, 1],"ABCDE_H" : [1, 1],"ABC" : [1, 1],"ABC_F" : [1, 1],"ABC_H" : [1, 1],"ABC_RSA" : [1, 1],"ABC_RSA_F" : [1, 1],"ABC_RSA_H" : [1, 1], "A_F" : [1, 1],"A_H" : [1, 1],"ABC_25" : [1, 1],"ABC_2649" : [1, 1],"ABC_50" : [1, 1],"ABC_FR" : [1, 1],"ABC_E" : [1, 1],"ABC_INFCAPBEP" : [1, 1],"ABC_CAPBEP" : [1, 1],"ABC_BAC" : [1, 1],"ABC_SUPBAC" : [1, 1],"ABC_INFCAPBEP_F" : [1, 1],"ABC_CAPBEP_F" : [1, 1],"ABC_BAC_F" : [1, 1],"ABC_SUPBAC_F" : [1, 1],"ABC_INFCAPBEP_H" : [1, 1],"ABC_CAPBEP_H" : [1, 1],"ABC_BAC_H" : [1, 1],"ABC_SUPBAC_H" : [1, 1],"ABC_DUR1" : [1, 1],"ABC_DUR2" : [1, 1],"ABC_DUR3" : [1, 1],"ABC_DUR4" : [1, 1],"ABC_DUR1_F" : [1, 1],"ABC_DUR2_F" : [1, 1],"ABC_DUR3_F" : [1, 1],"ABC_DUR4_F" : [1, 1],"ABC_DUR1_H" : [1, 1],"ABC_DUR2_H" : [1, 1],"ABC_DUR3_H" : [1, 1],"ABC_DUR4_H" : [1, 1],"NOTE_PE" : [1, 1],"EVOL_ABC_T4" : [1, 1],"EVOL_ABC_F_T4" : [1, 1],"EVOL_ABC_H_T4" : [1, 1],"EVOL_A_T4" : [1, 1],"EVOL_A_F_T4" : [1, 1],"EVOL_A_H_T4" : [1, 1],"NOTE_PE_T" : [1, 1]},
                        "getDataInseelogement": {"partResPrinc" : [1, 1],"partResSec" : [1, 1],"partResoccas" : [1, 1],"partResVac" : [1, 1],"partResPrincApp" : [1, 1],"partResPrincMai" : [1, 1],"partResPrinc1pi" : [1, 1],"partResPrinc2pi" : [1, 1],"partResPrinc3pi" : [1, 1],"partResPrinc4pi" : [1, 1],"partResPrinc5pi" : [1, 1],"partResPrinc30" : [1, 1],"partResPrinc30_40" : [1, 1],"partResPrinc40_60" : [1, 1],"partResPrinc60_80" : [1, 1],"partResPrinc80_100" : [1, 1],"partResPrinc100_120" : [1, 1],"partResPrinc120plus" : [1, 1],"partResPrincAch19" : [1, 1],"partResPrincAch19_45" : [1, 1],"partResPrincAch46_70" : [1, 1],"partResPrincAch71_90" : [1, 1],"partResPrincAch91_05" : [1, 1],"PartResPrincAch06_15" : [1, 1],"partResPrincEmm2" : [1, 1],"partResPrincEmm2_4" : [1, 1],"partResPrincEmm5_9" : [1, 1],"partResPrincEmm10" : [1, 1],"partMenProp" : [1, 1],"partMenLoc" : [1, 1],"partMenLogGrat" : [1, 1],"partResPrincSurocc" : [1, 1],"partResPrincSurocc2pi" : [1, 1],"partResPrincSurocc3pi" : [1, 1],"partResPrincSurocc4pi" : [1, 1],"partResPrincSurocc5pi" : [1, 1],"nbPersResPrinc" : [1, 1],"noteRp" : [1, 1],"nbLsPls" : [1, 1],"nbLsMes" : [1, 1],"txVac" : [1, 1],"txVac3m" : [1, 1],"txRot" : [1, 1],"txLsCol" : [1, 1],"txLsInd" : [1, 1],"txLs1p" : [1, 1],"txLs2p" : [1, 1],"txLs3p" : [1, 1],"txLs4p" : [1, 1],"txLs5pp" : [1, 1],"txLsm30" : [1, 1],"txLs30a40" : [1, 1],"txLs40a60" : [1, 1],"txLs60a80" : [1, 1],"txLs80a100" : [1, 1],"txLs100a120" : [1, 1],"txLs120p" : [1, 1],"txLsAv49" : [1, 1],"txLs49a75" : [1, 1],"txLs76a88" : [1, 1],"txLs89a00" : [1, 1],"txLs01a13" : [1, 1],"txLsAp13" : [1, 1],"txLsPlai" : [1, 1],"txLsPlusAv77" : [1, 1],"txLsPlusAp77" : [1, 1],"txLsPls" : [1, 1],"txLsPli" : [1, 1],"moyLoy" : [1, 1],"q1Loy" : [1, 1],"medLoy" : [1, 1],"q3Loy" : [1, 1],"noteRpls1" : [1, 1],"txLsRp" : [1, 1],"noteRpls2" : [1, 1],"txMobGlob" : [1, 1],"txSort" : [1, 1],"partSortAutQp" : [1, 1],"txEnt" : [1, 1],"partEntAutQp" : [1, 1],"txMobInt" : [1, 1],"noteFid" : [1, 1]},
                        "getDataInseetissuEco": {"NBETAB" : [1, 1],"INDUS" : [1, 1],"CONSTR" : [1, 1],"COM_TRANSP" : [1, 1],"COM_GROS" : [1, 1],"COM_DETAIL" : [1, 1],"INFO_COM" : [1, 1],"FINANCE" : [1, 1],"IMMO" : [1, 1],"SPECIAL" : [1, 1],"ENS_SANTE" : [1, 1],"AUTRE_SERV" : [1, 1],"SERV_ENT" : [1, 1],"SERV_PAR" : [1, 1],"SERV_TOT" : [1, 1],"SALA00NN" : [1, 1],"SALA0149" : [1, 1],"SALA50PL" : [1, 1],"CREATION" : [1, 1],"TRANSFERT" : [1, 1],"IMPLANT" : [1, 1],"AUTO_ENT" : [1, 1],"NOTE" : [1, 1]}
                    };

                    localStorage.setItem('listgraphe', JSON.stringify(observatoryObj.list_table));
                    
                }
                  
                let outqpv = ["name", "_id"];
                let list_box_checkbox = '';

                $.each(observatoryObj.list_table, function(index, value) {    
                    list_box_checkbox = obj.checkboxType({index:index, value:value});
                    obj.commons.getDatainseefunction(parames, value, outqpv, quartiers, index, list_box_checkbox);
                })

                $(".delcol").click(function(){
                    let valeur = $(this).data('value');
                    if (confirm('êtes-vous sûr de vouloir supprimer "'+trad[valeur]+'" du tableau ABS ?') == true) {
                        delete observatoryObj.list_table.getDataInseeabs[valeur]; 
                        localStorage.setItem('listgraphe', JSON.stringify(observatoryObj.list_table));
                        if($('#getDataInseeabs_'+valeur).is(":checked"))
                            $('#getDataInseeabs_'+valeur).click();
                        $(this).parent().parent().remove();
                    }
                })

                $(".addcol").click(function(){
                    let valeur = $(this).data('value');
                    if (confirm('êtes-vous sûr de vouloir ajouter "'+trad[valeur]+'" au tableau ABS ?') == true) {
                        observatoryObj.list_table.getDataInseeabs[valeur] = [1,1];
                        localStorage.setItem('listgraphe', JSON.stringify(observatoryObj.list_table));
                        $(this).parent().remove();
                    }
                })
                $(".updateindice").click(function(){
                    let valeur = $(this).data('value');
                    let ids = valeur.split('__')[0];
                    let value = valeur.split('__')[1];
                    observatoryObj['list_table'][ids][value]['1'] = (observatoryObj['list_table'][ids][value]['1']) ? 0 : 1;
                    localStorage.setItem('listgraphe', JSON.stringify(observatoryObj.list_table));
                    observatoryObj.computeTD(valeur);
                    observatoryObj.computeCriticity();
                    let asc = (observatoryObj['list_table'][ids][value]['1']) ? 'asc' : 'desc';
                    let color = (observatoryObj['list_table'][ids][value]['1']) ? 'primary' : 'warning';
                    let casc = (observatoryObj['list_table'][ids][value]['1']) ? 'desc': 'asc';
                    let ccolor = (observatoryObj['list_table'][ids][value]['1']) ?'warning'  : 'primary';
                    $("i.fa[data-value='"+valeur+"']").removeClass("fa fa-sort-numeric-"+casc+" text-"+ccolor+" updateindice").addClass("fa fa-sort-numeric-"+asc+" text-"+color+" updateindice");;
                })
                

                $("#list_box_checkbox").html(list_box_checkbox);
                $('.inputcheckbox').on('change', function(){
                    let value = ($(this).is(":checked")) ? 1 : 0;
                    $(this).parent().toggleClass("unchecked");
                    obj.updateCheckbox($(this).data('table'), $(this).data('column'), value);
                });

                $(".checkbox_all").click(function(){
                    var classe = $(this).data('value');
                    var checked = $(this).is(":checked");
                    if(checked){
                        $(".inputcheckbox[data-table='"+classe+"']:unchecked").click();
                    }
                    else{
                        $(".inputcheckbox[data-table='"+classe+"']:checked").click();
                    }
                });

                $('body').click(function(event){ 
                    $(".checkbox-box").hide();
                });

                observatoryObj.computeCriticity();
            },
            "financingPerFunder" : function(obj, params) {
                ajaxPost(
                    null, 
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/financingPerFunder",
                    {
                        form : params.formId,
                        quartiers : params.quartiers,
                        financers : params.financers,
                        tags : params.tags,
                        session : params.session
                    },
                    function(data){
                        if(Object.values(data.flows).length > 0) {
                            $("#financingPerFunder-container .card-content").css("height", Object.values(data.flows).length *12)
                        } else
                            $("#financingPerFunder-container .card-content").css("height", 200)
                        if(aapObj.form.GraphObservatory["financingPerFunder"])
                            obj.chartTypes.sankey({
                                id: "financingPerFunder",
                                labels: data.labels,
                                data: Object.values(data.flows),
                                colors: data.colors,
                                label: obj.graphLabels["financingPerFunder"],
                            });
                    }
                );
            } 
        },
        chartTypes : {
            bar : function(opt){
                var rectangleSet = false;
                const ctx = document.getElementById(opt.id);
                ctx.height = 500;
                const chartData = {
                    labels: opt.labels,
                    datasets: [{
                        label: opt.label,
                        data: opt.data,
                        borderWidth: 0,
                        backgroundColor : 'rgb(155 193 37)',
                    }]
                }


                const scales = {
                    beginAtZero: true,
                    ticks: {
                        callback: function(value, index, values) {
                            var val = chartData.labels?.[index];
                            val = trad[val] ?? val;
                            return val?.length > 20 ? val?.substring(0, 20)+'...' : val
                        }
                    }
                }

                let chartOptions = {
                    indexAxis: opt.indexAxis ?? 'y',
                    responsive: true,
                    //maintainAspectRatio: false,
                    scales: {
                        x: opt.indexAxis=='x' ? scales : {},
                        y: opt.indexAxis=='y' ? scales : {},
                    },
                    onAnimationComplete: function () {
                        var sourceCanvas = this.chart.ctx.canvas;
                        var copyHeight  = this.scale.yScalePaddingLeft - 5;
                        // the +5 is so that the bottommost y axis label is not clipped off
                        // we could factor this in using measureText if we wanted to be generic
                        var copyWidth = this.scale.endPoint + 5;
                        var targetCtx = document.getElementById("myChartAxis").getContext("2d");
                        targetCtx.canvas.height = copyHeight;
                        targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
                    },
                    maintainAspectRatio: false,
                    plugins : {
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                            /*position:"top",
                            align:"start"*/
                        },
                        datalabels: {
                            formatter: (value, ctx) => {
                                return formatNumber(value)
                            },
                            anchor: 'center',
                            align: 'end',
                            color: '#000',
                            font : {
                                //size : 25,
                                weight: 'bold'
                            }
                        },
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label: function(context) {
                                    var label = context.dataset.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += formatNumber(context.parsed.x);
                                    return label;
                                },
                                title: function (context) {
                                    const key = context[0].label.trim();
                                    return exists(trad[key]) ? trad[key] : key ; // Customize the tooltip title
                                },
                            }
                        }
                    }
                }
                chartOptions.scales = {
                        x: opt.indexAxis=='x' ? scales : {},
                        y: opt.indexAxis=='y' || chartOptions.indexAxis == "y" ? scales : {},
                }
                if(exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                /*if(Object.values(opt.data).length > 0) {
                    $("#"+opt.id+"-container .card-content").css("height", Object.values(opt.data).length *12)
                } else
                    $("#"+opt.id+"-container .card-content").css("height", 200)*/
                observatoryObj[opt.id] = new Chart(ctx, {
                    plugins: [ChartDataLabels],
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                });
                return observatoryObj[opt.id];
            },
            barStacked: function(opt) {
                const ctx = document.getElementById(opt.id);
                const chartData = {
                    labels: opt.labels,
                    datasets: opt.data
                };
                const chartOptions = {
                    indexAxis: 'x',
                    plugins: {
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                        },
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label: function(context) {
                                    var label =  context.dataset.label;
                                    label += ': ' + context.parsed.y;
                                    return label;
                                },
                                title: function(context) {
                                    const key = context[0].label;
                                    return exists(trad[key]) ? trad[key] : key; // Customize the tooltip title
                                },
                            }
                        }
                    },
                    responsive: true,
                    interaction: {
                        intersect: false,
                    },
                    scales: {
                        x: {
                            stacked: true,
                            ticks: {
                                callback: function(value, index, values) {
                                    var value = chartData.labels[index];
                                    return exists(trad[value]) ? trad[value] : value;
                                }
                            }
                        },
                        y: {
                            stacked: true,
                            
                        }
                    }
                }
                const config = {
                    type: 'bar',
                    data: chartData,
                    options: chartOptions
                };

                if (exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, config);
                return observatoryObj[opt.id];
            },
            line : function(opt){
                const ctx = document.getElementById(opt.id);
                const chartData = {
                    labels: opt.labels,
                    datasets: opt.data,
                };
                const chartOptions = {
                    responsive: true,
                    scales: {
                        x: {
                            beginAtZero: true,
                            ticks: {
                                callback: function(value, index, values) {
                                    var value = chartData.labels[index];
                                    return exists(trad[value]) ? trad[value] : value;
                                }
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: opt.label.toUpperCase(),
                            fullSize : true,
                            font: {
                                size: 16
                            }
                        },
                        tooltip: {
                            enabled: true,
                            callbacks: {
                                label: function(context) {
                                    var label = context.dataset.label || '';
                                    if (label) {
                                        label += ': ';
                                    }
                                    label += context.parsed.x;
                                    return label;
                                },
                                title: function (context) {
                                    const key = context[0].label;
                                    return exists(trad[key]) ? trad[key] : key ; // Customize the tooltip title
                                },
                            }
                        }
                    }
                };

                if(exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                observatoryObj[opt.id] = new Chart(ctx, {
                    type: 'line',
                    data: chartData,
                    options: chartOptions
                });
                return observatoryObj[opt.id];
            },
            sankey : function(opt){
                $("#"+opt.id).show();
                const ctx = document.getElementById(opt.id).getContext('2d');
                $("#"+opt.id).parent().find(".nodatamsg").remove()
                if(opt.data.length > 0) {
                    const colors = opt.colors;
                    function getColor(name) {
                        return colors[name] || "green";
                    }
                    const chartData = {
                        labels: opt.labels,
                        datasets: [
                            {
                                data: opt.data,
                                colorFrom: (c) => getColor(c.dataset.data[c.dataIndex].from),
                                colorTo: (c) => getColor(c.dataset.data[c.dataIndex].to),
                                borderWidth: 2,
                                borderColor: 'black',
                                size: 'max'
                            },
                        ],
                    };
                    const lastNodeX = 50; // Adjust the X-coordinate as needed
                    const lastNodeY = 80;
                    const options = {
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins : {
                            title: {
                                display: true,
                                text: opt.label.toUpperCase(),
                                fullSize : true,
                                font: {
                                    size: 16
                                }
                            },
                            sankey: {
                                node: {
                                    width: 30, // Adjust the width of nodes as needed
                                },
                            },
                        },
                        layout: {
                            padding: {
                                top: 10,   // Adjust the top padding for the last node
                                bottom: 10, // Adjust the bottom padding for the last node
                            },
                        },
                    };


                    
                    observatoryObj[opt.id] = new Chart(ctx, {
                        type: 'sankey',
                        data: chartData,
                        options: options,
                    });
                    return observatoryObj[opt.id];
                } else {
                    if(exists(observatoryObj[opt.id])) observatoryObj[opt.id].destroy();
                    $("#"+opt.id).hide();
                    $("#"+opt.id).parent().css("min-height", "")
                    $("#"+opt.id).parent().append(`
                        <div class="text-center nodatamsg col-xs-12 no-padding">
                            <h4 class="text-center">${opt.label}</h4> 
                            <h5 class="text-center">Aucun données à afficher</h5>
                        </div>
                    `);
                }
                
            }
        },
        tableType : function(params){
            return `
            <h4 class="text-center">${ params?.label ?? "Tableau"}</h4>
            <p class="hidden"></p>            
            <table class="table">
                <thead>
                <tr>
                    ${params?.thead}
                </tr>
                </thead>
                <tbody>
                    ${params?.tbody}
                </tbody>
            </table>
            `;
        },
        checkboxType: function(params){

            let input = '';
            
            $.each(params.value, function(index, value){
                let statu_aff = value[0];
                let statu_indc = value[1];
                let asc = (statu_indc) ? 'asc' : 'desc';
                let color = (statu_indc) ? 'primary' : 'warning';
                let checked = (statu_aff) ? "checked" : "";
                let classe = (statu_aff) ? "" : "unchecked";
                let del = (params.index == 'getDataInseeabs') ? '<i class="fa fa-trash delcol" data-value="'+index+'"></i>&nbsp;' : (observatoryObj.list_table.getDataInseeabs[index] === undefined)  ? '<i class="fa fa-plus addcol" data-value="'+index+'"></i>&nbsp;' : '' ;
                input += '<div class="row"><div class="col-sm-10"><label for="'+params.index+'_'+index+'" class="'+classe+'"><input style="display:none" class="inputcheckbox" type="checkbox" id="'+params.index+'_'+index+'" '+checked+' data-table="'+params.index+'" data-column="'+index+'" data-value="'+statu_aff+'"> '+trad[index]+'</label></div><div class="col-sm-1"><i class="fa fa-sort-numeric-'+asc+' text-'+color+' updateindice" data-value="'+params.index+'__'+index+'"></i></div><div class="col-sm-1">'+del+'</div></div>';
            });

            //let header = `<label for="all_${params.index}"><input class="checkbox_all" data-value="${params.index}" type="checkbox" id="all_${params.index}" > Tout</label><hr>`;
            let header = '';
            return `<div id="check_${params.index}" class="checkbox-box">${header+input}</div>`;    
        },
        computeTD: function(params){
            let table = [];
            let is_number = true;
            let parametre = params.split('__');
            let tableau = parametre[0];
            let column = parametre[1];
            $("td."+params).each(function(){
                let num = $(this).data('value');
                if(typeof num === 'undefined' || num === null || num == "" || num == 0)
                {
                    num = 0;
                }
                table.push(num);
            });


            let max_value = table[table.length-1];
            let table_p = {};
            for(i = 0; i < table.length; i++){
                var num = table[i]*100/max_value;
                table_p[table[i]] = Math.round((num + Number.EPSILON) * 100) / 100;
                delete table_p[max_value];    
            }
            table = table.filter(value => value !== 0);

            let color = '';
            if(observatoryObj['list_table'][tableau][column][1] == 1)
            {
                color = 'primary';
                table = table.slice().sort(function(a, b) {
                    return a - b;
                });
            }
            else{
                color = 'warning';
                table = table.slice().sort(function(a, b) {
                    return b - a;
                });
            }

            let table_s = [];
            $("td."+params).each(function(){
                let val = $(this).data('value');
                if(parseInt(val) != max_value || !isNaN(table_p[val]))
                {
                    let badge = table.indexOf(val)+1;
                    let span = ``;
                    
                    if(badge <= 3 && badge != 0){
                        span =  `<span class="label label-${color} badge-pill">${badge}</span>`;
                        table_s.push(badge);
                    }
                    else{
                        table_s.push(0);
                    }
                    let finale = `<div class="row"><div class="col-sm-12">${val} ${span}</div><div class="col-sm-12"><i class="pourcentage">${table_p[val]} %</i></div></div>`; 
                    $(this).html(finale);
                }
            });
            let ids = params.split("_")[0];
            observatoryObj.list_ids[ids].push(table_s);
        },
        computeCriticity: function(){
                let taille = $(".c_getDataInsee").length - 1;
                $.each(observatoryObj.list_ids, function(index, array){
                let final = Array.from({ length: taille }, (_, index) => 0);
                $.each(array, function(idx, val){
                    if(val.length == final.length){
                        for(i = 0; i < val.length; i++){
                        final[i] += val[i];
                        }
                    }
                        
                })
                $(".c_"+index).each(function(i, v){
                    $(this).html(final[i]);
                });
            })
        },
        updateCheckbox: function(table, column, value)
        {
            observatoryObj["list_table"][table][column]['0'] = value;
            localStorage.setItem('listgraphe', JSON.stringify(observatoryObj.list_table)); 
            if(value){
                $('.'+table+'__'+column).show(1);
            }else{
                $('.'+table+'__'+column).hide(1);
            }         
        },
        events : function(obj){
            obj.commons.fullScreenChart(obj);
            obj.commons.additionnalButtons(obj);
            obj.commons.switchMenu(obj);
            obj.commons.switchMenu2(obj);

            var listgraph = localStorage.getItem('listgraphCo');
            if (!listgraph) {
                let newlistgraph = {
                    "checkProject": [1, 1],
                    "checkActions": [1, 1],
                    "checkFinancement": [1, 1],
                    "checkProposition": 1
                };

                localStorage.setItem('listgraphCo', JSON.stringify(newlistgraph));
                //console.log(newlistgraph);
            } else {
                listgraph = JSON.parse(listgraph);
                $.each(listgraph, function(index, value) {
                    var bool = true;
                    if (value != 1) {
                        bool = false;
                    }
                    $("#" + index).prop('checked', bool);

                    if ($("#" + index).prop('checked')) {
                        $("." + index).removeClass("tochecked");
                        listgraph[index] = 1;

                    } else {

                        listgraph[index] = 0;
                        $("." + index).addClass("tochecked");
                    }
                })
            }
            htmlExists('.btn-filters-select', function (sel) {
                const buttonsFilter = document.querySelectorAll('.btn-filters-select');
                buttonsFilter.forEach(button => {
                    button.addEventListener('click', function(){
                        $("#totalProject,#runningProject,#doneProject,#totalActions,#todoActions,#runningActions,#doneActions,#deadlinePassedActions,#totalProposal,#voteProposal,#toBeFinancedProposal,#inFinancingProposal,#amountRequested,#amountFinanced,#amountSpent,#amountRemaining,#getDataInseeabs, #getDataInseetissuEco, #getDataInseelogement, #getDataInseeinsertionPro, #getDataInseeeduc, #getDataInseervn,#getDataInsee").html('<i class="fa fa-spinner fa-spin"></i>');
                        coInterface.showLoader("#loader-observatory");
                        $('#loader-observatory').show();
                    });
                });
            });
            htmlExists('.filters-activate', function (sel) {
                const buttonsFilter = document.querySelectorAll('.filters-activate');
                buttonsFilter.forEach(button => {
                    button.addEventListener('click', function(){
                        $("#totalProject,#runningProject,#doneProject,#totalActions,#todoActions,#runningActions,#doneActions,#deadlinePassedActions,#totalProposal,#voteProposal,#toBeFinancedProposal,#inFinancingProposal,#amountRequested,#amountFinanced,#amountSpent,#amountRemaining,#getDataInseeabs, #getDataInseetissuEco, #getDataInseelogement, #getDataInseeinsertionPro, #getDataInseeeduc, #getDataInseervn,#getDataInsee").html('<i class="fa fa-spinner fa-spin"></i>');
                        coInterface.showLoader("#loader-observatory");
                        $('#loader-observatory').show();
                    });
                });
            });

            $('.checkbox').click(function() {
                var listgraph = JSON.parse(localStorage.getItem('listgraphCo'));
                $.each(listgraph, function(index, value) {
                    if ($("#" + index).prop('checked')) {
                        $("." + index).removeClass("tochecked");
                        listgraph[index] = 1;

                    } else {

                        listgraph[index] = 0;
                        $("." + index).addClass("tochecked");
                    }
                })
                localStorage.setItem('listgraphCo', JSON.stringify(listgraph));
                //console.log(listgraph)

            });



            $('#annuaireActions').off().on('click', function(){
                const action = $(this).attr('data-action');
                let view = "funding";
                
                
                let answersId = {
                    infinancing : null,
                    tobevoted : null,
                    tobefinanced : null
                }
                const post = {
                    answerId : answersId.infinancing ? answersId.infinancing : aapObj?.defaultProposal?.["_id"]?.["$id"] ? aapObj.defaultProposal["_id"]["$id"] : null,
                    showFilter : true,
                    insideModal : true
                };
                let filterObj = {
                    obj : {
                        'answers.aapStep1.titre' : {
                            '$exists': true
                        },
                        'answers.aapStep1.depense.financer' : {
                            '$exists': true
                        }
                    },
                    toActivate : {
                        "status": {
                            "key": "funded",
                            "value": "funded",
                            "type": "filters",
                            "field": "status"
                        }
                    }
                };
        
                observatoryObj?.searchObj?.search?.obj?.filters?.quartiers ? filterObj.obj.quartiers = observatoryObj?.searchObj?.search?.obj?.filters?.quartiers : "";
                history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : view}, aapObj.common.getQueryStringObject()));
                const searchObjToActivate = {
                    obj : {
                            "text": "",
                            "nbPage": 0,
                            "indexMin": 0,
                            "indexStep": 20,
                            "count": false,
                            "scroll": true,
                            "scrollOne": false,
                            "tags": [],
                            "initType": "",
                            "filters": filterObj.obj,
                            "types": [
                                "answers"
                            ],
                            "countType": [],
                            "locality": {},
                            "forced": {
                                "filters": {}
                            },
                            "tagsPath": "answers.aapStep1.tags",
                            "fields": [],
                            "notSourceKey": true,
                            "sortBy": {
                                "updated": -1
                            }
                        }
                }
                aapObj.filterSearch.activeFilters = filterObj.toActivate
                if(!aapObj.filterSearch.proposal?.search) {
                    aapObj.filterSearch.proposal.search = searchObjToActivate
                } else {
                    aapObj.filterSearch.proposal.search.obj = searchObjToActivate.obj
                }
                const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProposal';
                dialogContent.open({
                    modalContentClass : "modal-custom-lg",
                    isRemoteContent : true,
                    shownCallback : () => {},
                    hiddenCallback : () => {
                        history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                        aapObj.filterSearch.proposal = {}
                        $("#dialogContent #dialogContentBody").empty()
                        $("#openModal .modal-content").removeClass("aap-modal-md");
                        $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                        $('#openModalContent').removeClass("container container-fluid").addClass("container");
                    }
                });
                ajaxPost(null, url, post,
                    function (data) {
                        $("#dialogContent #dialogContentBody").empty().html(data)
                    }, null, null
                );
            })
            $('.observatory-aap .filter-container').css({
                "position": "sticky",
                "top": ($("#mainNav").height() + 10) + "px",
                "z-index": 100,
                "background": '#fff'/*'var(--aap-primary-color)'*/,
                "margin-bottom": "13px",
                "border-bottom": "1px solid var(--aap-primary-color)"
            })
            
        },
        commons : {
            getDataCosindni : function(obj){
                ajaxPost(
                    null,
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/dataCosindni",
                    {
                        form : obj.formId
                    },
                    function(data){
                        obj.dataCosindni = data;
                    },null,null,{async:false}
                );
            },
            additionnalButtons : function(){
                $('#filterContainerInside').prepend("<h4>Observatoire Association de Saint Denis</h4>");
                    const additionFiltersButtons =
                    `<li class="dropdown ">
                        <a href="javascript:;" data-value="table-insee" class="dropdown-toggle menu-button btn btn-aap-primary additional-button insee-quartier" data-placement="bottom">
                        INSEE </i>
                        </a>
                    </li>
                    <li class="dropdown hidden">
                        <a href="javascript:;" class="dropdown-toggle menu-button btn btn-aap-primary additional-button" data-placement="bottom">
                        ABS(todo) <i class="fa fa-angle-down margin-left-5 hidden"></i>
                        </a>
                    </li>
                    <li class="dropdown hidden">
                        <a href="javascript:;" data-value="directory-actions" id="annuaireActions" class="dropdown-toggle menu-button btn btn-aap-primary additional-button" data-placement="bottom">
                        Annuaire actions <i class="fa fa-angle-down margin-left-5 hidden"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" data-value="directory-quartier" class="dropdown-toggle menu-button btn btn-aap-primary directory-quartier" data-placement="bottom">
                        Annuaire quartiers <i class="fa fa-angle-down margin-left-5 hidden"></i>
                        </a>
                    </li>
                    <li class="dropdown pull-right">
                        <a href="javascript:;" data-value="map-observatory" class="dropdown-toggle menu-button btn btn-aap-primary btn-map-observatory additional-button" data-placement="bottom" data-toggle="dropdown">
                            <i class="fa fa-map-marker margin-left-5"></i>&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-map">
                            <li><a href="javascript:;" data-value="assos-adress" class="switch-map">Par adresse de l'association</a></li>
                            <li><a href="javascript:;" data-value="quartier-adress" class="switch-map">Par quartier</a></li>
                        </ul>
                    </li>
                    <li class="dropdown pull-right">
                        <a href="javascript:;" data-value="table-observatory" class="dropdown-toggle menu-button btn btn-aap-primary additional-button" data-placement="bottom">
                        <i class="fa fa-table"></i>
                        </a>
                    </li>
                    <li class="dropdown pull-right">
                        <a href="javascript:;" data-value="chart-observatory" class="dropdown-toggle menu-button btn btn-aap-primary additional-button" data-placement="bottom">
                        <i class="fa fa-bar-chart "></i>
                        </a>
                    </li>`;

                    $('#filterContainerInside #activeFilters').before(additionFiltersButtons);
                    $('.searchObjCSS .dropdown .btn-menu').removeClass('btn-menu').addClass("btn btn-aap-primary");
            },
            fullScreenChart : function(){
                $(".btn-panel.fullscreen").off().on('click',function(){
                    if($(this).data("key") == "on"){
                        $(this).parent().parent().parent().parent().parent().addClass("fullscreen-active");
                        $(this).parent().parent().parent().parent().next().addClass("canvas-fullscreen-active");
                        $(this).hide();
                        $(this).next().show();
                    }else{
                        $(this).parent().parent().parent().parent().parent().removeClass("fullscreen-active");
                        $(this).parent().parent().parent().parent().next().removeClass("canvas-fullscreen-active");
                        $(this).hide();
                        $(this).prev().show();
                    }
                })
            },
            switchMenu : function(obj){
                $('.additional-button').on('click',function(){
                    const val = $(this).data('value');
                    switch (val) {
                        case "chart-observatory":
                            $('.chart-and-metrics').show();
                            $('#map-container').hide(500);
                            $('#table-container').hide();
                            $('#table-insee').hide();
                            break;
                        case "table-observatory":
                            $('.chart-and-metrics').hide();
                            $('#map-container').hide(500);
                            $('#table-container').show();
                            $('#table-insee').hide();
                            break;
                        case "table-insee":
                            $('#table-insee').show();
                            $('.chart-and-metrics').hide();
                            $('#map-container').hide(500);
                            $('#table-container').hide();
                        default:
                            break;
                    }
                })

                $('.map-action-quartier').off().on('click',function(){
                        //observatoryObj.map.addElts(data);
                        /*htmlExists('#map-container-btn-fullscreen-container button', function (sel) {
                            sel.trigger('click')
                        })*/
                    })
            },
            bindMetricEvents : function(obj) {
                $('#inFinancingProposal, #voteProposal, #toBeFinancedProposal').off('click').on('click', function() {
                    const action = $(this).attr('data-action');
                    let view = "funding";
                    
                    let answersId = {
                        infinancing : null,
                        tobevoted : null,
                        tobefinanced : null
                    }
                    if(typeof observatoryObj?.proposalData != "undefined") {
                        if(notEmpty(observatoryObj.proposalData?.infinancing) && Object.keys(observatoryObj.proposalData.infinancing).length > 0) {
                            answersId.infinancing = Object.keys(observatoryObj.proposalData.infinancing)[0]
                        }
                        if(notEmpty(observatoryObj.proposalData?.tobevoted) && Object.keys(observatoryObj.proposalData.tobevoted).length > 0) {
                            answersId.tobevoted = Object.keys(observatoryObj.proposalData.tobevoted)[0]
                        }
                        if(notEmpty(observatoryObj.proposalData?.tobefinanced) && Object.keys(observatoryObj.proposalData.tobefinanced).length > 0) {
                            answersId.tobefinanced = Object.keys(observatoryObj.proposalData.tobefinanced)[0]
                        }
                    }
                    const post = {
                        answerId : answersId.infinancing ? answersId.infinancing : aapObj?.defaultProposal?.["_id"]?.["$id"] ? aapObj.defaultProposal["_id"]["$id"] : null,
                        showFilter : true,
                        insideModal : true
                    };
                    const filters = observatoryObj.searchObj?.search?.obj?.filters
                    let filterObj = {
                        obj : {
                            'answers.aapStep1.titre' : {
                                '$exists': true
                            },
                            'answers.aapStep1.depense.financer' : {
                                '$exists': true
                            },
                            'form' : aapObj.form?._id?.$id,
                            'answers.aapStep1.year' : notEmpty(filters?.session) ? filters?.session : [aapObj.aapSession],
                            'answers.aapStep1.depense.financer.id' : notEmpty(filters?.financers) ? filters?.financers : [],
                            'answers.aapStep1.tags' : notEmpty(filters?.tags) ? filters?.tags : [],
                        },
                        toActivate : {
                            "status": {
                                "key": "funded",
                                "value": "funded",
                                "type": "filters",
                                "field": "status"
                            }
                        }
                    };
                    switch (action) {
                        case "tobevoted":
                            view = "evaluation";
                            answersId.tobevoted ? post.answerId = answersId.tobevoted : "";
                            filterObj.obj = {
                                'answers.aapStep1.titre' : {
                                    '$exists': true
                                },
                                'status' : "vote",
                                'form' : aapObj.form?._id?.$id,
                                'answers.aapStep1.year' : notEmpty(filters?.session) ? filters?.session : [aapObj.aapSession],
                                'answers.aapStep1.depense.financer.id' : notEmpty(filters?.financers) ? filters?.financers : [],
                                'answers.aapStep1.tags' : notEmpty(filters?.tags) ? filters?.tags : [],
                                "project.id" : {
                                    "$exists" : false
                                }
                            };
                            filterObj.toActivate = {
                                "status": {
                                    "key": "vote",
                                    "value": "vote",
                                    "type": "filters",
                                    "field": "status"
                                }
                            };
                            break;
                    
                        case "tobefinanced":
                            answersId.tobefinanced ? post.answerId = answersId.tobefinanced : "";
                            filterObj.obj = {
                                'answers.aapStep1.titre' : {
                                    '$exists': true
                                },
                                'answers.aapStep1.depense' : {
                                    '$exists': true
                                },
                                'form' : aapObj.form?._id?.$id,
                                'answers.aapStep1.year' : notEmpty(filters?.session) ? filters?.session : [aapObj.aapSession],
                                'answers.aapStep1.depense.financer.id' : notEmpty(filters?.financers) ? filters?.financers : [],
                                'answers.aapStep1.tags' : notEmpty(filters?.tags) ? filters?.tags : [],
                                'answers.aapStep1.depense.financer' : {
                                    '$exists': false
                                }
                            };
                            break;
                        default:
                            view = "funding"
                            break;
                    }
                    observatoryObj?.searchObj?.search?.obj?.filters?.quartiers ? filterObj.obj["answers.aapStep1.interventionArea"] = observatoryObj?.searchObj?.search?.obj?.filters?.quartiers : "";
                    history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : view}, aapObj.common.getQueryStringObject()));
                    const searchObjToActivate = {
                        obj : {
                                "text": "",
                                "nbPage": 0,
                                "indexMin": 0,
                                "indexStep": 20,
                                "count": false,
                                "scroll": true,
                                "scrollOne": false,
                                "tags": [],
                                "initType": "",
                                "filters": filterObj.obj,
                                "types": [
                                    "answers"
                                ],
                                "countType": [],
                                "locality": {},
                                "forced": {
                                    "filters": {}
                                },
                                "tagsPath": "answers.aapStep1.tags",
                                "fields": [],
                                "notSourceKey": true,
                                "sortBy": {
                                    "updated": -1
                                }
                            }
                    }
                    aapObj.filterSearch.activeFilters = filterObj.toActivate
                    if(!aapObj.filterSearch.proposal?.search) {
                        aapObj.filterSearch.proposal.search = searchObjToActivate
                    } else {
                        aapObj.filterSearch.proposal.search.obj = searchObjToActivate.obj
                    }
                    const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProposal';
                    dialogContent.open({
                        modalContentClass : "modal-custom-lg",
                        isRemoteContent : true,
                        shownCallback : () => {},
                        hiddenCallback : () => {
                            history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                            aapObj.filterSearch.proposal = {}
                            $("#dialogContent #dialogContentBody").empty()
                            $("#openModal .modal-content").removeClass("aap-modal-md");
                            $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                            $('#openModalContent').removeClass("container container-fluid").addClass("container");
                        }
                    });
                    ajaxPost(null, url, post,
                        function (data) {
                            $("#dialogContent #dialogContentBody").empty().html(data)
                        }, null, null
                    );
                });
                $('#runningProject, #doneProject').off('click').on('click', function() {
                    const action = $(this).attr('data-action');
                    let projectsId = {
                        inprogress : null,
                        finished : null
                    }
                    if(typeof observatoryObj?.projectData != "undefined") {
                        if(notEmpty(observatoryObj.projectData?.inprogress) && Object.keys(observatoryObj.projectData.inprogress).length > 0) {
                            projectsId.inprogress = Object.keys(observatoryObj.projectData.inprogress)[0]
                        }
                        if(notEmpty(observatoryObj.projectData?.finished) && Object.keys(observatoryObj.projectData.finished).length > 0) {
                            projectsId.finished = Object.keys(observatoryObj.projectData.finished)[0]
                        }
                    }
                    if($(this).html()*1 > 0) {
                        history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action"}, aapObj.common.getQueryStringObject()));
                        const post = {
                            projectId : projectsId.inprogress ? projectsId.inprogress : aapObj?.defaultProject?.["_id"]?.["$id"] ? aapObj.defaultProject["_id"]["$id"] : null,
                            answerId : "default",
                            showFilter : true,
                            insideModal : true
                        };
                        if(action != "inprogress" && projectsId.finished)
                            post.projectId = projectsId.finished
                        const searchObjToActivate = {
                            obj : {
                                    "text": "",
                                    "nbPage": 0,
                                    "indexMin": 0,
                                    "indexStep": 20,
                                    "count": false,
                                    "scroll": true,
                                    "scrollOne": false,
                                    "tags": [],
                                    "initType": "",
                                    "filters": {
                                        "form": aapObj?.form?.["_id"]?.["$id"] ? aapObj.form["_id"]["$id"] : "",
                                        "answers.aapStep1.titre": {
                                            "$exists": true
                                        },
                                        "properties.avancement": action == "inprogress" ? [
                                            "notSpecified",
                                            "idea",
                                            "concept",
                                            "started",
                                            "development",
                                            "testing",
                                            "mature"
                                        ] : [
                                            "finished",
                                            "abandoned"
                                        ]
                                    },
                                    "types": [
                                        "projects"
                                    ],
                                    "countType": [],
                                    "locality": {},
                                    "forced": {
                                        "filters": {}
                                    },
                                    "fediverse": false,
                                    "tagsPath": "answers.aapStep1.tags",
                                    "fields": [],
                                    "notSourceKey": true,
                                    "sortBy": {
                                        "updated": -1
                                    },
                                    "textPath": "answers.aapStep1.titre"
                                }
                        }
                        aapObj.filterSearch.activeFilters = action == "inprogress" ? {
                            "0notSpecified": {
                                "key": "notSpecified",
                                "value": "notSpecified",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "1idea": {
                                "key": "idea",
                                "value": "idea",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "2concept": {
                                "key": "concept",
                                "value": "concept",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "3started": {
                                "key": "started",
                                "value": "started",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "4development": {
                                "key": "development",
                                "value": "development",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "5testing": {
                                "key": "testing",
                                "value": "testing",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "6mature": {
                                "key": "mature",
                                "value": "mature",
                                "type": "filters",
                                "field": "properties.avancement"
                            }
                        } : {
                            "0abandoned": {
                                "key": "abandoned",
                                "value": "abandoned",
                                "type": "filters",
                                "field": "properties.avancement"
                            },
                            "1finished": {
                                "key": "finished",
                                "value": "finished",
                                "type": "filters",
                                "field": "properties.avancement"
                            }
                        }
                        if(!aapObj.filterSearch.project?.search) {
                            aapObj.filterSearch.project.search = searchObjToActivate
                        } else {
                            aapObj.filterSearch.project.search.obj = searchObjToActivate.obj
                        }
                        const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProject';
                        dialogContent.open({
                            modalContentClass : "modal-custom-lg",
                            isRemoteContent : true,
                            shownCallback : () => {},
                            hiddenCallback : () => {
                                history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                                aapObj.filterSearch.projectDetail = {}
                                $("#dialogContent #dialogContentBody").empty()
                                $("#openModal .modal-content").removeClass("aap-modal-md");
                                $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                                $('#openModalContent').removeClass("container container-fluid").addClass("container");
                            }
                        });
                        ajaxPost(null, url, post,
                            function (data) {
                                $("#dialogContent #dialogContentBody").empty().html(data)
                                coInterface.bindLBHLinks();
                            }, null, null, {
                                // async : false
                            }
                        )
                    }
                });
                $('#totalActions, #todoActions, #runningActions, #doneActions, #deadlinePassedActions').off("click").on("click", function() {
                    history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {aapview : "action"}, aapObj.common.getQueryStringObject()));
                    const action = $(this).attr('data-action');
                    const post = {
                        projectId : aapObj?.defaultProject?.["_id"]?.["$id"] ? aapObj.defaultProject["_id"]["$id"] : null,
                        showFilter : true,
                        insideModal : true,
                        answerId: "default"
                    };
                    const url = baseUrl + '/co2/aap/getviewbypath/path/co2.views.aap.detailProject';
                    dialogContent.open({
                        modalContentClass : "modal-custom-lg",
                        isRemoteContent : true,
                        shownCallback : () => {},
                        hiddenCallback : () => {
                            history.pushState({}, null, aapObj.common.buildUrlQuery(document.location.href, {projectId : "", aapview : "", answerId : "", userId: ""}, aapObj.common.getQueryStringObject()));
                            aapObj.filterSearch.projectDetail = {}
                            $("#dialogContent #dialogContentBody").empty()
                            $("#openModal .modal-content").removeClass("aap-modal-md");
                            $("#openModal .modal-content .close-modal").removeClass("aap-close-modal");
                            $('#openModalContent').removeClass("container container-fluid").addClass("container");
                        }
                    });
                    ajaxPost(null, url, post,
                        function (data) {
                            $("#dialogContent #dialogContentBody").empty().html(data)
                            coInterface.bindLBHLinks();
                        }, null, null
                    );
                })
            },
            directoryQuartiers : {
                modal : `
                <div class="portfolio-modal modal fade in" id="modal-quartier" tabindex="-1" role="dialog" aria-hidden="false" style="z-index: 99999; padding-left: 0px; top: 56px; display: none; left: 0px;background: #0000008f;">
                    <div class="modal-content padding-top-15 aap-modal-md radius-5">
                        <div class="close-modal" data-dismiss="modal">
                            <div class="lr">
                                <div class="rl">
                                </div>
                            </div>
                        </div>
                        

                        <div class="row">
                            <h2 class="text-center">Les quartiers</h2>
                            <div id="modal-quartier-content" style="height: 100%; width: 100%">

                            </div>
                        </div>
                    </div>
                </div>
                `,
                filtersHtml : `
                <div class="container padding-bottom-20 container-quartier-quartier">
                    <div class="col-md-12 no-padding">
                        <div id='filterContainerQuartier' class='searchObjCSS'></div>
                        <div class='headerSearchQuartier no-padding col-xs-12'></div>
                        <div class='bodySearchContainerQuartier margin-top-30'>
                            <div class='no-padding col-xs-12' id='dropdown_search_quartier'>
                            </div>
                        </div>
                        <div class='padding-top-20 col-xs-12 text-left footerSearchContainerQuartier'></div>
                    </div>
                </div>
                `,
                directoryHtml : function(params){
                    var str = `
                        <div class="team">
                            <div class="card-team">
                                <div class="img-box hidden">
                                    <img class="img-responsive-quartier" alt="Responsive Team Profiles" src="${exists(params.profilImageUrl) ? params.profilImageUrl : defaultImage}" />
                                    <ul class="text-center">
                                        <a href="#">
                                            <li><i class="fa fa-facebook"></i></li>
                                        </a>
                                        <a href="#">
                                            <li><i class="fa fa-twitter"></i></li>
                                        </a>
                                        <a href="#">
                                            <li><i class="fa fa-linkedin"></i></li>
                                        </a>
                                    </ul>
                                </div>
                                <h1>
                                    <a href="javascript:;" data-id="${params._id.$id}" data-quartier="${exists(params?.quartier) ? params.quartier.join(",") : "" }" class="tooltips see-quartier-quartier" data-toggle="tooltip" data-placement="bottom" data-original-title="Voir les quartier">
                                        ${params.name}
                                    </a>
                                </h1>
                                <h2 class="text-aap-primary">${params?.actions ? params?.actions+" action(s)" : "" } </h2>
                                <hr class="hidden" />
                                <p class="text-left desc">${exists(params.shortDescription) ? params.shortDescription : ""}</p>
                            </div>
                        </div>
                    `;
                    return str;
                },
                filters : function(obj){
                    var paramsFilterQuartier= {
                        urlData : baseUrl+"/costum/coSindniSmarterre/observatory/request/globalautocompletequartier",
                        container : "#filterContainerQuartier",
                        footerDom : ".footerSearchContainerQuartier",
                        interface : {
                            events : {
                                //page : true,
                                scroll : true,
                                //scrollOne : true
                            }
                        },
                        header : {
                            dom : ".headerSearchQuartier",
                            options : {
                                left : {
                                    classes : 'col-xs-8 elipsis no-padding',
                                    group:{
                                        count : true,
                                        types : true
                                    }
                                }
                            },
                        },
                        defaults : {
                            notSourceKey : true,
                            types : ["zones"],
                            indexStep: 10,
                            filters : {
                                formId : obj.formId
                            }
                        },
                        results : {
                            dom:".bodySearchContainerQuartier",
                            smartGrid : true,
                            renderView :"observatoryObj.commons.directoryQuartiers.directoryHtml",
                            map : {
                                active : false
                            },
                            events : function(fObj){
                                $(fObj.results.dom+" .processingLoader").remove();
                                coInterface.bindLBHLinks();
                                directory.bindBtnElement();
                                coInterface.bindButtonOpenForm();
                                $('.see-quartier-quartier').off().on('click',function(){
                                    const id = $(this).data('id');
                                    $(".btn-filters-select.quartier[data-key='"+id+"']").trigger('click');
                                    $('#modal-quartier').modal("hide");
                                })
                            },
                        },
                        filters : {
                            text : true
                        }
                    };
                    $('.observatory-aap').append(obj.commons.directoryQuartiers.modal);
                    $('#modal-quartier-content').html(obj.commons.directoryQuartiers.filtersHtml);
                    var filterQuartier = searchObj.init(paramsFilterQuartier);
                    filterQuartier.search.init(filterQuartier);
                    htmlExists('#modal-quartier-content #filterContainerInside .dropdown',function(sel){
                        sel.remove();
                    })

                    htmlExists('.directory-quartier',function(sel){
                        sel.off().on('click',function(){
                            $('#modal-quartier').modal("show");
                        })
                    })
                }
            },
            mapQuartiers : {
                getCurrentProposalAdress : function(obj,params){
                    if(observatoryObj?.map){
                        ajaxPost(
                            null,
                            baseUrl+"/costum/coSindniSmarterre/observatory/request/mapProposalData",
                            {
                                form : params.formId,
                                quartiers : params.quartiers,
                                financers : params.financers,
                                tags : params.tags,
                                session : params.session
                            },
                            function(data){
                                observatoryObj.map.clearMap(); 
                                observatoryObj.map.addElts(data);
                            }
                        );
                    }
                },
                getCurrentQuartierAdress : function(obj,params){
                    if(observatoryObj?.map){
                        ajaxPost(
                            null,
                            baseUrl+"/costum/coSindniSmarterre/observatory/request/mapQuartierData",
                            {
                                form : params.formId,
                                quartiers : params.quartiers,
                                financers : params.financers,
                                tags : params.tags,
                                session : params.session
                            },
                            function(data){
                                observatoryObj.map.clearMap();
                                observatoryObj.map.addElts(data);
                            }
                        );
                    }
                },
                initMap : function(obj,params){
                    const default_option = {
                        container : "#map-container",
                        activePopUp : true,
                        zoom : 0,
                        mapOpt : {
                            zoom : 0,
                            latLon : ["-21.115141", "55.536384"],
                        },
                        mapCustom : {
                            tile : "mapbox"
                        },
                        elts : [],
                        buttons : {
                            fullScreen : {
                                allow : true
                            }
                        }
                    };
                    observatoryObj.map = new CoMap(default_option);
                }
            },
            switchMenu2 : function(obj){
                
                $('.btn-thematique').on('click',function(){
                    $('.btn-thematique').removeClass("active");
                    $(this).addClass("btn-thematique btn active");
                    const val = $(this).data('value');
                    $('.content-tables').hide();
                    
                    switch (val) {
                        case "Demographie":
                            $('#getDataInsee').show();
                            break;
                        case "Revenus":
                            $('#getDataInseervn').show();
                            break;
                        case "education":
                            $('#getDataInseeeduc').show();
                            break;
                        case "inserstionpro":
                            $('#getDataInseeinsertionPro').show();
                            break;
                        case "Logement":
                            $('#getDataInseelogement').show();
                            break;
                        case "economie":
                            $('#getDataInseetissuEco').show();
                            break;
                        case "abs":
                            $('#getDataInseeabs').show();
                            break;
                        default:
                            break;
                    }
                })
            },
            getDatainseefunction : function(parames, inqpv, outqpv, quartiers, ids, list_box_checkbox){
                inqpv = Object.keys(inqpv);
                let years = parames.session;
                $('#table'+ids).html('<i class="fa fa-spinner fa-spin" style="font-size: 48px; color: var(--aap-primary-color); position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"></i>');
                ajaxPost(
                    null,
                    baseUrl+"/costum/coSindniSmarterre/observatory/request/getDataInsee",
                    {
                        form: parames.formId,
                        //context: params.context,
                        filter: parames.filter,
                        quartiers : quartiers,
                        inqpv: inqpv,
                        outqpv: outqpv,
                        years: years
                    },
                    function(data){
                        var configbtn = `<a href="javascript:;" id="config_${ids}" class="dropdown-toggle menu-button btn btn-aap-primary additional-button" ><i class="fa fa-gears"></i></a>${list_box_checkbox}`;
                        var table = '<h4 class="text-center">TABLEAU ANALYSE</h4>'+configbtn+'<table  id="table'+ids+'" class="table"> ';
                        var headerRow = '<thead><tr>';
                        // table headers
                        headerRow += '<th  class="text-left h4 bold " >'+trad['name']+'</th>'; // titre colonne Quartiers
                        if(years.length > 1)
                        {
                            headerRow += '<th  class="text-left h4 bold " >Années</th>';
                        }
                        else{
                            headerRow += '<th  class="text-left h4 bold " >Criticité</th>';
                        }

                        $.each(inqpv, function(key, value){
                                let status = observatoryObj["list_table"][ids][value];
                                let traduction = (trad[value]) ? trad[value] : value;
                                if ( status === undefined) {
                                    
                                    headerRow += '<th class="text-left h4 bold '+ids+'__'+value+' " style="max-width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" title="T'+trad[value]+'" >'+traduction+'</th>';
                                }
                                else{
                                    let status = observatoryObj["list_table"][ids][value];
                                    let display = (status) ? '' : 'style="display:none"';
                                    headerRow += '<th class="text-left h4 bold '+ids+'__'+value+'" style="max-width: 150px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" title="T'+trad[value]+'" '+display+' >'+traduction+'</th>';
                                }
                        });

                        headerRow += '</tr></thead>';
                        // Table body & rows
                        var row = '';
                            $.each(data, function(index, item) {
                                let qname = item['name'];
                                var i = 0;
                                $.each(years, function(yindx, yval){
                                    row += '<tr>';
                                        if(i < 1)
                                        {
                                            row += '<td class="text-left h4 bold" rowspan="'+years.length+'">'+qname+'</td>';
                                            
                                        }
                                        if(years.length > 1)
                                        {
                                                row += '<td class="text-left h4 bold ">'+yval+'</td>';
                                        }
                                        else(years.length > 1)
                                        {
                                                row += '<td class="text-left h4 bold c_'+ids.split("_")[0]+'"  >0</td>';
                                        }
                                        $.each(inqpv, function(idx, val){
                                                let status = observatoryObj["list_table"][ids][val][0];
                                                let display = (status) ? '' : 'style="display:none"';
                                                let value = (item['_'+yval][val]) ? item['_'+yval][val] : 0;
                                                row += '<td class="text-left h4 '+ids+'__'+val+'"  '+display+' data-value="'+value+'">'+value+'</td>';
                                                    
                                        });
                                    row += '</tr>';
                                    i++;           
                                });    
                        });
                        //table finale
                        table += headerRow+'<tbody>'+row+'</tbody></table>';
                        $('#'+ids).html(table);
                         //box de configuration pour l'affichage des colonnes 
                         $('#config_'+ids).on('click', function(e) {
                            e.stopPropagation();
                            const checkboxBox = $('#check_'+ids);
                            checkboxBox.toggle();
                            checkboxBox.css({
                                top: e.pageY + 'px',
                                left: e.pageX + 'px',
                            }); 
                        });
                        
                        if(years.length < 2)
                        {
                            // calcule pourcentage
                            $.each(Object.keys(data[0]['_'+years[0]]), function(index, value){
                                observatoryObj.computeTD(ids+'__'+value);
                            });
                            //datatable pour triage par colonne
                            $('#table'+ids).dataTable({
                                "info": false,
                                "language": {
                                    "previous": "",
                                    "next": ""
                                },
                                "searching": false,
                                "lengthChange": false,
                                "paging": false,
                                "columnDefs": [
                                    { type: 'num', targets: 0 },
                                ],
                                scrollY: '800px', // Set the maximum height before vertical scrolling is enabled
                                scrollX: true, // Enable horizontal scrolling
                                scrollCollapse: true, // Enable vertical scrolling
                                fixedHeader: {
                                    header: true,
                                    footer: false
                                } 
                            });
                        }

                       

                    },null,null,{async:false}
                );
            },
        },
        filters : function(obj){
            const quartierFilter = {};
            const financerFilter = {};
            const tagsFilter = {};
            $.each(aapObj.quartiers,function(k,v){
                if(v !=="Tous quartiers"){
                    quartierFilter[k] = v["name"];
                    trad[k] = v["name"];
                }
            });

            $.each(obj.dataCosindni["financers"],function(k,v){
                financerFilter[v["_id"]["$id"]] = v["name"];
                trad[v["_id"]["$id"]] = v["name"];
            });

            $.each(obj.dataCosindni["tags"],function(k,v){
                tagsFilter[v] = v;
            });



            mylog.log("obj.dataCosindni",financerFilter);
            if(aapObj?.allQuartiers)
                $.each(aapObj.allQuartiers,function(k,v){
                    if(v !=="Tous quartiers"){
                        q[k] = v["name"];
                        trad[k] = v["name"];
                    }
                });
            var paramsFilterObs = {
                urlData : "",
                container : "#filterContainerObs",
                header : {
                    dom : ".headerSearchIncommunityObs",
                    options : {
                        left : {
                            classes : 'col-xs-8 elipsis no-padding',
                            group:{
                                count : true,
                                types : true
                            }
                        }
                    },
                },
                defaults : {
                    filters:{
                    },
                },
                results : {
                    dom:".bodySearchContainerObs",
                    events : function(fObj){
                        const filters = fObj.search.obj.filters;
                        const defaultYear = $('.btn-filters-select.session[data-key="'+aapObj.aapSession+'"]');
                        const params = {
                            formId : obj.formId,
                            context : notEmpty(filters?.context) ? filters?.context : [aapObj.context.id],
                            quartiers : notEmpty(filters?.quartiers) ? filters?.quartiers : null,
                            financers : notEmpty(filters?.financers) ? filters?.financers : null,
                            tags : notEmpty(filters?.tags) ? filters?.tags : null,
                            session : notEmpty(filters?.session) ? filters?.session : [aapObj.aapSession]
                        }

                       /* if(!notEmpty(filters?.session)){
                            fObj.filters.manage.addActive(fObj, defaultYear, true);
                        }*else if(filters?.session?.includes(aapObj.aapSession)){
                            fObj.filters.manage.addActive(fObj, defaultYear, true);
                        }else if(filters?.session?.includes(aapObj.aapSession)){
                            fObj.filters.manage.removeActive(fObj, defaultYear, true);
                        }*/
                        obj.initCharts(obj,params);
                        obj.initMetrics(obj,params);
                        obj.initTable(obj,params);
                        obj.initMap(obj,params);
                        obj.commons.bindMetricEvents(obj);
                        $('#loader-observatory').hide();
                    },
                },
                filters : {
                    quartier : {
                        view : "dropdownList",
                        type : "filters",
                        multiple : false,
                        field : "quartiers",
                        name : "Quartiers",
                        event : "filters",
                        keyValue : false,
                        typeList: "object",
                        list : quartierFilter,
                    },
                    session : {
                        view : "dropdownList",
                        type : "filters",
                        multiple : false,
                        field : "session",
                        name : "Année",
                        event : "filters",
                        list : generateIntArray(2020,new Date().getFullYear()).map(v => v = v.toString()),
                    },
                    financers : {
                        view : "dropdownList",
                        type : "filters",
                        multiple : false,
                        field : "financers",
                        name : "Financeurs",
                        event : "filters",
                        keyValue : false,
                        typeList: "object",
                        list : financerFilter,
                    },
                    tags : {
                        view : "dropdownList",
                        type : "filters",
                        multiple : false,
                        field : "tags",
                        name : ucfirst(trad.Thematic),
                        event : "filters",
                        list : tagsFilter,
                    },
                }
            };
            observatoryObj["searchObj"] = searchObj.init(paramsFilterObs);
            observatoryObj["searchObj"].search.init(observatoryObj["searchObj"]);
        }
    }

    $(document).ready(function(){
        observatoryObj.init(observatoryObj);
    })
</script>
