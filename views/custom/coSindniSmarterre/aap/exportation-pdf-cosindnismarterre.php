<?php
    $user_session_id = strval(Yii::app()->session['userId']);
    $answerid = $params;
    $answer = PHDB::findOneById(Form::ANSWER_COLLECTION,$answerid,['form']);

    $formId = $answer['form'];
    $form = PHDB::findOneById(Form::COLLECTION,$formId,["config","pdfField.".$user_session_id]);

    $sub_forms = PHDB::findOneById(Form::COLLECTION,$form["config"])["subForms"];

    $allInputs = PHDB::find(Form::INPUTS_COLLECTION,array("formParent" => $formId));

    $steps = [];
    $not = [];
    
    // types d'inputs à exclure de l'exportation
    $input_exclusions = [
        "tpls.forms.titleSeparator",
        "tpls.forms.uploader"
    ];
    
    // reordonner les étapes et ne prendre que les champs intéressants
    foreach ($allInputs as $kinput => $vinput) {
        $temp_step = [
            "name" => $sub_forms[$vinput["step"]]["name"]
        ];
        $inputs = $vinput;
        $temp_inputs = [];
        $temp_ordered_inputs = [];
        $array_keys_input = array_keys($inputs["inputs"]);
        foreach ($inputs["inputs"] as $input_key => $input_value) {
            if (isset($input_value["type"])) {
                if (isset($input_value["positions"][$formId])) {
                    // sélectionner la position de l'élément dans le formulaire
                    $position = intval($input_value["positions"][$formId]);
                    
                    $temp_ordered_inputs[] = [
                        'id'         => (string)$inputs['_id'],
                        "clef"       => $input_key,
                        "label"      => isset($formParent['pdfLabels'][$user_session_id][$input_key]) ? $formParent['pdfLabels'][$user_session_id][$input_key] : (!empty($input_value["label"]) ? $input_value["label"] : $input_key),
                        "position"   => $position,
                        "step"       => $vinput["step"],
                        "type"       => $input_value["type"],
                        'exportable' => !in_array($input_value["type"], $input_exclusions)
                    ];

                    if($vinput["step"] === "aapStep2" &&  (count($array_keys_input)-2) === array_search($input_key, $array_keys_input)){
                        $temp_ordered_inputs[] = [
                            'id'         => "b2bkGyXRyHsGJXpkf5LDXbtM",
                            "clef"       => $input_key,
                            "label"      => "Note",
                            "position"   => -1,
                            "step"       => $vinput["step"],
                            "type"       => $input_value["type"],
                            'exportable' => !in_array($input_value["type"], $input_exclusions)
                        ];
                    }
                } else {
                    $temp_inputs[] = [
                        'id'         => (string)$inputs['_id'],
                        "clef"       => $input_key,
                        "label"      => isset($formParent['pdfLabels'][$user_session_id][$input_key]) ? $formParent['pdfLabels'][$user_session_id][$input_key] : (!empty($input_value["label"]) ? $input_value["label"] : $input_key),
                        "position"   => -1,
                        "step"       => $vinput["step"],
                        "type"       => $input_value["type"],
                        'exportable' => !in_array($input_value["type"], $input_exclusions)
                    ];
                    if($vinput["step"] === "aapStep2" &&  (count($array_keys_input)-2) === array_search($input_key, $array_keys_input)){
                        $temp_inputs[] = [
                            'id'         => "b2bkGyXRyHsGJXpkf5LDXbtM",
                            "clef"       => $input_key,
                            "label"      => "Note",
                            "position"   => -1,
                            "step"       => $vinput["step"],
                            "type"       => $input_value["type"],
                            'exportable' => !in_array($input_value["type"], $input_exclusions)
                        ];
                    }
                }
            }
        }
        
        $permutation = true;
        // reordonner selon les ordres fournis dans la base de données
        do {
            $permutation = false;
            $length = count($temp_ordered_inputs);
            for ($i = 0; $i < $length - 1; $i++) {
                if ($temp_ordered_inputs[$i]["position"] > $temp_ordered_inputs[$i + 1]["position"]) {
                    $permutation = true;
                    $_temp = $temp_ordered_inputs[$i];
                    $temp_ordered_inputs[$i] = $temp_ordered_inputs[$i + 1];
                    $temp_ordered_inputs[$i + 1] = $_temp;
                }
            }
        } while ($permutation);
        
        $temp_inputs = array_merge($temp_ordered_inputs, $temp_inputs);
        $temp_step["inputs"] = $temp_inputs;
        $steps[] = $temp_step;
    }
    $initImage = Document::getListDocumentsWhere(
        array(
            "id"=> $formId,
            "type"=>'form',
            "subKey"=>'imageSign',
        ), "image"
    );
    
?>
<style>
    .modal {
        z-index: 10000;
    }
    
    .tab-pane {
        padding: 5px;
        border: 1px rgba(0, 0, 0, .1) solid;
        border-top: transparent;
    }
    
    .reorderable-input-<?= $answerid?> {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        border: 1px solid transparent;
        box-sizing: border-box;
        border-radius: 6px;
        padding: 5px;
        margin: 0;
    }
    
    .reorderable-input-<?= $answerid?>:hover {
        background-color: rgba(0, 0, 0, .05);
    }
    
    .reorderable-input-<?= $answerid?>:hover .dragger_<?= $answerid?>,
    .reorderable-input-<?= $answerid?>:hover .editor-<?= $answerid?> {
        display: inline-block;
        margin-left: 5px;
    }
    
    .input-value,
    .dragger_<?= $answerid?>,
    .editor-<?= $answerid?> {
        align-self: center;
    }
    
    .dragger_<?= $answerid?>,
    .editor-<?= $answerid?> {
        display: none;
        justify-self: flex-end;
        padding: 5px;
        border-radius: 5px;
    }
    
    .dragger_<?= $answerid?> {
        background-color: rgba(0, 0, 0, .2);
    }
    
    .dragger_<?= $answerid?>:hover {
        cursor: grab;
    }
    
    .editor-<?= $answerid?> {
        background-color: #28a745;
        color: white;
        cursor: pointer;
    }
    
    .ghost-element-<?= $answerid?> {
        border: 3px rgba(0, 0, 0, .5) dashed;
        padding: 5px;
        border-radius: 6px;
    }
    
    .action-btn {
        display: flex;
        flex-direction: row;
        align-items: flex-start;
    }
    
    label.input-value {
        -moz-user-select: none;
        -webkit-user-select: none;
    }
</style>
<script>
    'use strict';
    function load_events_<?= $answerid?>() {
        // debut champs de remplacement pour l'édition de champ
        const input_group_field_editor_<?= $answerid?> = document.createElement('div');
        input_group_field_editor_<?= $answerid?>.classList.add('input-group');
        input_group_field_editor_<?= $answerid?>.style.flex = '1';
        
        const input_field_container_<?= $answerid?> = document.createElement('input');
        input_field_container_<?= $answerid?>.classList.add('form-control');
        input_field_container_<?= $answerid?>.classList.add('export-field-name-editor');
        input_field_container_<?= $answerid?>.type = 'text';
        const editor_button_container_<?= $answerid?> = document.createElement('div');
        editor_button_container_<?= $answerid?>.classList.add('input-group-btn');
        const cancel_editor_<?= $answerid?> = document.createElement('button');
        const confirm_editor_<?= $answerid?> = document.createElement('button');
        cancel_editor_<?= $answerid?>.classList.add('btn');
        cancel_editor_<?= $answerid?>.classList.add('btn-danger');
        
        const cancel_icon_<?= $answerid?> = document.createElement('i');
        cancel_icon_<?= $answerid?>.classList.add('fa');
        cancel_icon_<?= $answerid?>.classList.add('fa-times');
        cancel_editor_<?= $answerid?>.appendChild(cancel_icon_<?= $answerid?>);
        
        confirm_editor_<?= $answerid?>.classList.add('btn');
        confirm_editor_<?= $answerid?>.classList.add('btn-primary');
        
        const save_icon_<?= $answerid?> = document.createElement('i');
        save_icon_<?= $answerid?>.classList.add('fa');
        save_icon_<?= $answerid?>.classList.add('fa-save');
        confirm_editor_<?= $answerid?>.appendChild(save_icon_<?= $answerid?>);
        
        editor_button_container_<?= $answerid?>.appendChild(cancel_editor_<?= $answerid?>);
        editor_button_container_<?= $answerid?>.appendChild(confirm_editor_<?= $answerid?>);
        input_group_field_editor_<?= $answerid?>.appendChild(input_field_container_<?= $answerid?>);
        input_group_field_editor_<?= $answerid?>.appendChild(editor_button_container_<?= $answerid?>);
        // fin champs de remplacement pour l'édition de champ
        
        // début block fictif pour le déplacement d'un élément
        const ghost_reordable_input_<?= $answerid?> = document.createElement('div');
        ghost_reordable_input_<?= $answerid?>.classList.add('ghost-element-<?= $answerid?>');
        // fin block fictif pour le déplacement d'un élément
        
        const tab_pane_element_<?= $answerid?> = [...document.querySelectorAll('.tab-<?= $answerid?>')];
        const reorderable_input_containers_<?= $answerid?> = [...document.querySelectorAll('.reorderable-input-<?= $answerid?>')];
        const reorderable_inputs_<?= $answerid?> = [...document.querySelectorAll('.reorderable-input-<?= $answerid?> input[type=checkbox]:not(:disabled)')];
        const editor_button_elements_<?= $answerid?> = [...document.querySelectorAll('.editor-<?= $answerid?>')];
        const ragger_elements_<?= $answerid?> = [...document.querySelectorAll('.dragger_<?= $answerid?>')];
        const select_all_elements_<?= $answerid?> = [...document.querySelectorAll('.select-all-<?= $answerid?>')];
        
        let current_selected_tab_<?= $answerid?> = null;
        let y_mouse_location_<?= $answerid?> = 0;
        let after_element_<?= $answerid?> = null;
        const text_before = document.getElementById('text-before-<?= $answerid?>');
        const text_after = document.getElementById('text-after-<?= $answerid?>');
        
        reorderable_input_containers_<?= $answerid?>.forEach(reorderable_input => {
            reorderable_input.addEventListener('dragstart', function (e1) {
                reorderable_input.classList.add('dragging_<?= $answerid?>');
            });
            reorderable_input.addEventListener('dragend', function (e1) {
                current_selected_tab_<?= $answerid?>.removeChild(ghost_reordable_input_<?= $answerid?>);
                ragger_elements_<?= $answerid?>.forEach(dragger => dragger.removeAttribute('style'));
                reorderable_input_containers_<?= $answerid?>.forEach(element => element.removeAttribute('style'));
                reorderable_input.removeAttribute('draggable');
                reorderable_input.classList.remove('dragging_<?= $answerid?>');
                if (after_element_<?= $answerid?> == null) current_selected_tab_<?= $answerid?>.appendChild(this);
                else current_selected_tab_<?= $answerid?>.insertBefore(this, after_element_<?= $answerid?>);
                save_pdf_exportation_settings_<?= $answerid?>();
            });
        });
        
        reorderable_inputs_<?= $answerid?>.forEach(reorderable_input => {
            reorderable_input.addEventListener('change', function (e) {
                const is_checked = this.checked;
                const its_parent = document.querySelector(`[data-target="${this.dataset.parent}"]`);
                const children = [...document.querySelectorAll(`[data-parent="${this.dataset.parent}"]:not(:disabled)`)];
                const something_is_different = children.find(child => child.checked !== is_checked);
                if (typeof something_is_different === 'undefined') {
                    its_parent.checked = is_checked;
                    its_parent.indeterminate = false;
                } else {
                    its_parent.checked = false;
                    its_parent.indeterminate = true;
                }
                save_pdf_exportation_settings_<?php echo $answerid?>();
            });
        });
        
        select_all_elements_<?= $answerid?>.forEach(select_all_element => {
            select_all_element.addEventListener('change', function (e) {
                const parent = this.dataset.target;
                const targets = [...document.querySelectorAll(`[data-parent="${parent}"]:not(:disabled)`)];
                targets.forEach(target => target.checked = this.checked);
                save_pdf_exportation_settings_<?= $answerid?>();
            });
        });
        tab_pane_element_<?= $answerid?>.forEach(tab_pane => {
            tab_pane.addEventListener('dragover', function (e1) {
                e1.preventDefault();
                current_selected_tab_<?= $answerid?> = tab_pane;
                y_mouse_location_<?= $answerid?> = e1.clientY;
                after_element_<?= $answerid?> = get_drag_after_element_<?= $answerid?>_<?= $answerid?>(current_selected_tab_<?= $answerid?>, y_mouse_location_<?= $answerid?>);
                const dragging_element_<?= $answerid?> = document.querySelector('.dragging_<?= $answerid?>');
                reorderable_input_containers_<?= $answerid?>.forEach(reorderable_input => {
                    reorderable_input.style.background = 'transparent';
                    [...reorderable_input.childNodes].forEach(function (child) {
                        if (typeof child.classList !== 'undefined' && child.classList.contains('dragger_<?= $answerid?>')) {
                            child.style.display = 'none';
                        }
                    });
                });
                dragging_element_<?= $answerid?>.style.display = 'flex';
                ghost_reordable_input_<?= $answerid?>.style.height = dragging_element_<?= $answerid?>.offsetHeight + 'px';
                ghost_reordable_input_<?= $answerid?>.style.width = dragging_element_<?= $answerid?>.offsetWidth + 'px';
                dragging_element_<?= $answerid?>.style.display = 'none';

                if (after_element_<?= $answerid?> == null) tab_pane.appendChild(ghost_reordable_input_<?= $answerid?>);
                else tab_pane.insertBefore(ghost_reordable_input_<?= $answerid?>, after_element_<?= $answerid?>);

            });

        });
        
        function get_drag_after_element_<?= $answerid?>_<?= $answerid?>(container, y) {
            const draggable_elements = [...container.querySelectorAll('.reorderable-input-<?= $answerid?>:not(.dragging_<?= $answerid?>)')]
            
            return draggable_elements.reduce((closest, child) => {
                const box = child.getBoundingClientRect()
                const offset = y - box.top - box.height / 2
                if (offset < 0 && offset > closest.offset) {
                    return {
                        offset: offset,
                        element: child
                    }
                } else {
                    return closest
                }
            }, {
                offset: Number.NEGATIVE_INFINITY
            }).element
        }
        
        
        ragger_elements_<?= $answerid?>.forEach(dragger => {
            dragger.addEventListener('mousedown', () => {
                const parent = dragger.parentNode.parentNode;
                if (!parent.hasAttribute('draggable')) parent.setAttribute('draggable', true);
            });
            
            dragger.addEventListener('mouseup', () => {
                const parent = dragger.parentNode.parentNode;
                if (parent.hasAttribute('draggable')) parent.removeAttribute('draggable');
            });
        });
        function save_pdf_exportation_settings_<?= $answerid?>() {
            const form = '<?= $formId ?>pdf';
            const default_setting = {
                textbefore : document.getElementById('text-before-<?= $answerid?>').value,
                textafter : document.getElementById('text-after-<?= $answerid?>').value,
                fields: []
            };
            const settings = localStorage.getItem(form) == null ? default_setting : JSON.parse(localStorage.getItem(form));
            settings.fields = [];
            [...document.querySelectorAll('.pdf-field-export-<?= $answerid?>')].forEach(champ => {
                settings.fields.push({
                    id: champ.dataset.id,
                    parent: champ.dataset.parent,
                    step: champ.dataset.step,
                    label: champ.dataset.label,
                    type: champ.dataset.type,
                    exportable: !champ.hasAttribute('disabled'),
                    value: champ.value,
                    checked: champ.checked,
                });
            });
            settings.textbefore = document.getElementById('text-before-<?= $answerid?>').value;
            settings.textafter = document.getElementById('text-after-<?= $answerid?>').value;
            localStorage.setItem(form, JSON.stringify(settings));
            //toastr.success('Le paramètre est enregistré');
            return false;
        }

        editor_button_elements_<?= $answerid?>.forEach(editor => {
            editor.addEventListener('click', function (e) {
                const inputs = [...this.parentElement.parentElement.childNodes];
                const parent = this.parentElement.parentElement;
                const checkbox = this.parentElement.parentElement.firstElementChild.firstElementChild;
                inputs.forEach(input => {
                    input.remove();
                });
                
                input_field_container_<?= $answerid?>.value = checkbox.dataset.label;
                parent.appendChild(input_group_field_editor_<?= $answerid?>);
                input_field_container_<?= $answerid?>.focus();
                editor_button_elements_<?= $answerid?>.forEach(editorElement => editorElement.style.display = 'none');
                
                function handle_cancel_edition_<?= $answerid?>() {
                    input_group_field_editor_<?= $answerid?>.remove();
                    inputs.forEach(input => {
                        input_field_container_<?= $answerid?>.value = '';
                        parent.appendChild(input);
                    });
                    cancel_editor_<?= $answerid?>.removeEventListener('click', handle_cancel_edition_<?= $answerid?>);
                    confirm_editor_<?= $answerid?>.removeEventListener('click', handle_save_edition_<?= $answerid?>);
                    input_group_field_editor_<?= $answerid?>.removeEventListener('keydown', handle_input_keydown);
                    editor_button_elements_<?= $answerid?>.forEach(editorElement => editorElement.removeAttribute('style'));
                }
                
                function handle_save_edition_<?= $answerid?>() {
                    
                    const params = {
                        id: aapObj.form._id.$id,
                        collection: "forms",
                        path: `pdfLabels.<?= $user_session_id ?>.${checkbox.value}`,
                        value: input_field_container_<?= $answerid?>.value
                    };
                    
                    dataHelper.path2Value(params, function (data) {
                        inputs.forEach(input => {
                            checkbox.dataset.label = input_field_container_<?= $answerid?>.value;
                            checkbox.nextSibling.textContent = input_field_container_<?= $answerid?>.value;
                            input_group_field_editor_<?= $answerid?>.remove();
                            parent.appendChild(input);
                        });
                        cancel_editor_<?= $answerid?>.removeEventListener('click', handle_cancel_edition_<?= $answerid?>);
                        confirm_editor_<?= $answerid?>.removeEventListener('click', handle_save_edition_<?= $answerid?>);
                        input_group_field_editor_<?= $answerid?>.removeEventListener('keydown', handle_input_keydown);
                        editor_button_elements_<?= $answerid?>.forEach(editorElement => editorElement.removeAttribute('style'));
                        toastr.success('Mise à jour enregistrée');
                    });
                }
                
                function handle_input_keydown(event) {
                    if (event.code === 'Enter')
                        handle_save_edition_<?= $answerid?>();
                    else if (event.code === 'Escape')
                        handle_cancel_edition_<?= $answerid?>();
                }
                
                cancel_editor_<?= $answerid?>.removeEventListener('click', handle_cancel_edition_<?= $answerid?>);
                confirm_editor_<?= $answerid?>.removeEventListener('click', handle_save_edition_<?= $answerid?>);
                input_group_field_editor_<?= $answerid?>.removeEventListener('keydown', handle_input_keydown);
                
                
                cancel_editor_<?= $answerid?>.addEventListener('click', handle_cancel_edition_<?= $answerid?>);
                confirm_editor_<?= $answerid?>.addEventListener('click', handle_save_edition_<?= $answerid?>);
                input_group_field_editor_<?= $answerid?>.addEventListener('keydown', handle_input_keydown);
            });
        });

        function handle_blur_text() {
            save_pdf_exportation_settings_<?= $answerid?>();
        }
        
        text_before.removeEventListener('blur', handle_blur_text);
        text_before.addEventListener('blur', handle_blur_text);

        text_after.removeEventListener('blur', handle_blur_text);
        text_after.addEventListener('blur', handle_blur_text);
        
    }
    
    
    function load_settings_<?= $answerid?>() {
        const form = '<?= $formId ?>pdf';
        const default_setting = {
            textbefore : document.getElementById('text-before-<?= $answerid?>').value,
            textafter : document.getElementById('text-after-<?= $answerid?>').value,
            fields: []
        };
        const steps = JSON.parse(`<?= json_encode($steps) ?>`);
        const temporary_settings_pdf = [];
        for (const step of steps) {
            for (const input of step.inputs) {
                if(input.clef == "association" || 
                    input.clef == "titre"|| 
                    input.clef == "aapStep1l0ru4g66a6r7lfwtay"|| 
                    input.clef == "interventionArea"|| 
                    input.clef == "aapStep1l0kythybqx6i2gldi1"|| 
                    input.clef == "description"|| 
                    input.clef == "nameProjectManager"|| 
                    input.clef == "phoneProjectManager"|| 
                    input.clef == "aapStep1l0m5iq15701exxuxwq"|| 
                    input.clef == "startDate"|| 
                    input.clef == "plannedDuration"|| 
                    input.clef == "exactLocationAction"|| 
                    input.clef == "numberStakeholdersProject"|| 
                    input.clef == "totalNumberBeneficiaries"|| 
                    input.clef == "aapStep1l1lzd7y6jak23ytnjpd"|| 
                    input.clef == "budget"|| 
                    input.clef == "depense"|| 
                    input.clef == "aapStep1l1xfy8ox73zuib530a3"|| 
                    input.clef == "aapStep1l1xfp33q5bg1bz8mwbg"|| 
                    input.clef == "aapStep1l1xft16v6ohm6m5o0wu"
                    ){
                        temporary_settings_pdf.push({
                            id: input.id,
                            parent: step.name,
                            step: input.step,
                            label: input.label,
                            type: input.type,
                            checked : true,
                            exportable: input.exportable,
                            value: input.clef,
                        });
                    }else{
                        temporary_settings_pdf.push({
                            id: input.id,
                            parent: step.name,
                            step: input.step,
                            label: input.label,
                            type: input.type,
                            exportable: input.exportable,
                            value: input.clef,
                        });
                }
            }
        }
        

        const settings = localStorage.getItem(form) == null ? default_setting : JSON.parse(localStorage.getItem(form));
        const longest = settings.fields.length >= temporary_settings_pdf.length ? settings.fields : temporary_settings_pdf;
        const to_removes = [];
        const to_adds = [];
        
        for (let temporary of longest) {
            const setting_find = settings.fields.find(setting => setting.value === temporary.value);
            const temporary_find = temporary_settings_pdf.find(setting => setting.value === temporary.value);
            if (typeof setting_find === 'undefined') to_adds.push(temporary);
            if (typeof temporary_find === 'undefined') to_removes.push(temporary);
        }
        
        for (let to_add of to_adds) settings.fields.push(to_add);
        for (let to_remove of to_removes) {
            const index_to_remove = settings.fields.find(field => field.value === to_remove.value);
            settings.fields.splice(settings.fields.indexOf(index_to_remove), 1);
        }
        let html = {};
        let htmlall = {};
        let htmlinp = {};
        let last_parent = '';
        let is_checked = true;
        let is_above_checked = false;
        let is_indeterminate = false;
        let index = 0;
        $(settings.fields).each(function( kf,field ) {
                last_parent = field.parent;
                htmlall[last_parent] = `
                <div class="checkbox">
                    <label class="input-value">
                        <input type="checkbox" class="reorderable-input-<?= $answerid?> select-all-<?= $answerid?>" data-target="${last_parent}" checked>
                        <b>Sélectionner tout</b>
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor-<?= $answerid?>"></i>
                        <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
                    </div>
                </div>
                ` ;
            // if (last_parent === '') {
            //     last_parent = field.parent;
            //     html[last_parent] += `
            //     <div class="checkbox">
            //         <label class="input-value">
            //             <input type="checkbox" class="reorderable-input-<?= $answerid?> select-all-<?= $answerid?>" data-target="${last_parent}" checked>
            //             <b>Sélectionner tout</b>
            //         </label>
            //         <div class="action-btn">
            //             <i class="fa fa-edit editor-<?= $answerid?>"></i>
            //             <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
            //         </div>
            //     </div>
            //     `;
            // }
            // if (field.parent !== last_parent) {
            //    // document.getElementById(`${last_parent}-tabulation-pdf-<?= $answerid?>`).innerHTML = html[]; 
            //     last_parent = field.parent;
            //     html[last_parent] += `
            //     <div class="checkbox">
            //         <label class="input-value">
            //             <input type="checkbox" class="reorderable-input-<?= $answerid?> select-all-<?= $answerid?>" data-target="${last_parent}" checked>
            //             <b>Sélectionner tout</b>
            //         </label>
            //         <div class="action-btn">
            //             <i class="fa fa-edit editor-<?= $answerid?>"></i>
            //             <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
            //         </div>
            //     </div>
            //     `;
            //     is_checked = true;
            //     is_above_checked = false;
            //     is_indeterminate = false;
            //     index = 0;
            // }
            // if (index > 0 && field.exportable && !field.checked) is_checked = false;
            // if (index > 0 && field.exportable && field.checked !== is_above_checked) is_indeterminate = true;

            
            htmlinp[last_parent] += `
                <div class="checkbox reorderable-input-<?= $answerid?>">
                    <label class="input-value ${!field.exportable ? 'text-danger' : ''}">
                        <input class="pdf-field-export-<?= $answerid?>"  data-parent="${field.parent}" data-step="${field.step}" data-label="${field.label}" data-type="${field.type}" value="${field.value}" data-id="${field.id}" type="checkbox" ${!field.exportable ? 'disabled' : field.checked ? 'checked' : ''}>
                        ${field.label}
                    </label>
                    <div class="action-btn">
                        <i class="fa fa-edit editor-<?= $answerid?>"></i>
                        <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
                    </div>
                </div>
            `;
            if (field.exportable) is_above_checked = field.checked;
            html[last_parent] = htmlall[last_parent]+htmlinp[last_parent]
            index++;
        });
        
        if (typeof last_parent !== 'undefined' && last_parent !== '') {
            document.getElementById(`${last_parent}-tabulation-pdf-<?= $answerid?>`) && document.getElementById(`${last_parent}-tabulation-pdf-<?= $answerid?>`).innerHTML ?  document.getElementById(`${last_parent}-tabulation-pdf-<?= $answerid?>`).innerHTML = html[last_parent] : "";
            html = {};
        }
        
        document.getElementById('text-before-<?= $answerid?>').value = settings.textbefore;
        document.getElementById('text-after-<?= $answerid?>').value = settings.textafter;
        
    }
    
    function perform_pdf_exportation_<?= $answerid?>(element) {
        const spinner = element.firstElementChild;
        spinner.classList.add('fa');
        spinner.classList.add('fa-spinner');
        spinner.classList.add('fa-spin');
        
        const form = '<?= $formId ?>';
        const data = {
            form,
            formid : aapObj.form._id.$id ,
            answeridd :'<?= $answerid ?>',
            champs: [],
            //query : listAapObj.activeFilters
        };
        [...document.querySelectorAll('.pdf-field-export-<?= $answerid?>')].forEach(champ => {
            if (champ.checked) {
                data.champs.push({
                    step: champ.dataset.step,
                    label: champ.dataset.label,
                    type: champ.dataset.type,
                    value: champ.value
                });
            }
        });
        tplCtx = {};
        tplCtx.id = aapObj.form._id.$id;
        tplCtx.collection = "forms";
        tplCtx.path = "allToRoot";
        tplCtx.value = {
            textBeforePdf: document.getElementById('text-before-<?= $answerid?>').value,
            textAfterPdf : document.getElementById('text-after-<?= $answerid?>').value,
            "pdfField.<?= $user_session_id ?>" : data.champs
        };
        dataHelper.path2Value(tplCtx, function (data) {
            toastr.success('Mise à jour enregistrée');
        });
        var params = jQuery.param(data) 
        let dataToString = JSON.stringify(data);
        window.open(baseUrl + '/co2/aap/exportpdf/formid/'+aapObj.form._id.$id+'/answerid/<?= $answerid ?>');
        $("#openModal").modal('hide');
        return false;
    }
    
    $(document).ready(function () { 
        load_settings_<?= $answerid?>();
        load_events_<?= $answerid?>();
        let sectionDyf<?php echo $answerid ?>Params = {
            "jsonSchema" : {    
                "title" : "Importation d'image de signature",
                "icon" : "fa-cog",
                
                "properties" : {    
                    "image" :{
                        "inputType" : "uploader",
                        "label" : "image",
                        "docType": "image",
                        "contentKey" : "slider",
                        "itemLimit" : 1,
                        "endPoint": "/subKey/imageSign",
                        "domElement" : "image",
                        "filetypes": ["jpeg", "jpg", "gif", "png"],
                        "label": "Image :",
                        "showUploadBtn": false,
                        initList : <?php echo json_encode($initImage) ?>
                    },
                },
                beforeBuild : function(){
                    uploadObj.set("form","<?php echo $formId ?>");
                },
                save : function (data) {  
                    tplCtx.value = {};                
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                                $("#ajax-modal").modal('hide');
                            });
                        } );
                    }
                }
            }
        };

        $(".edit-img<?php echo $answerid ?>").off().on("click",function() {  
            tplCtx.subKey = "imageSign";
            tplCtx.id = "<?php echo $formId ?>";
            tplCtx.collection = "form";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf<?php echo $answerid ?>Params,null, null);
        });
    })
</script>
    <div class="container">
        <div class="row">
                <div class="col-xs-12">
                    <h4 class="modal-title">Exportation des données en PDF</h4>
                </div>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs text-left" role="tablist" id="pdf-export-headers-<?= $answerid ?>">
                        <?php foreach ($steps as $index => $step) { 
                            ?>
                            <li <?= $index == 0 ? "class=\"active\"" : "" ?>>
                                <a href="#<?= $step["name"] ?>-tabulation-pdf-<?= $answerid?>"
                                role="tab"
                                data-toggle="tab"><?= $step["name"] ?></a>
                            </li>
                        <?php } ?>
                        <li <?= $index == 0 ? "class=\"active\"" : "" ?>>
                            <a href="#setting-text-pdf-<?= $answerid?>"
                            role="tab"
                            data-toggle="tab">Texte</a>
                        </li>
                        <li <?= $index == 0 ? "class=\"active\"" : "" ?>>
                            <a href="#setting-other-pdf-<?= $answerid?>"
                            role="tab"
                            data-toggle="tab">Autres</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pdf-export-content-<?= $answerid ?>">
                        <?php foreach ($steps as $index => $step) { ?>
                            <div class="tab-pane tab-<?= $answerid?>  <?= $index == 0 ? "active" : "" ?>"
                                id="<?= $step["name"] ?>-tabulation-pdf-<?= $answerid?>">
                                <div class="checkbox">
                                    <label class="input-value">
                                        <input type="checkbox" class="reorderable-input-<?= $answerid?> select-all-<?= $answerid?>" data-target="<?= $step["name"] ?>" checked>
                                        <b>Sélectionner tout</b>
                                    </label>
                                    <div class="action-btn">
                                        <i class="fa fa-edit editor-<?= $answerid?>"></i>
                                        <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
                                    </div>
                                </div>

                                <?php foreach ($step["inputs"] as $step_key => $step_value) { 
                                    $display = "";
                                    $checked = "";
                                    if($step_value["clef"] == "association" || 
                                    $step_value["clef"] == "titre"|| 
                                    $step_value["clef"] == "aapStep1l0ru4g66a6r7lfwtay"|| 
                                    $step_value["clef"] == "interventionArea"|| 
                                    $step_value["clef"] == "aapStep1l0kythybqx6i2gldi1"|| 
                                    $step_value["clef"] == "description"|| 
                                    $step_value["clef"] == "nameProjectManager"|| 
                                    $step_value["clef"] == "phoneProjectManager"|| 
                                    $step_value["clef"] == "aapStep1l0m5iq15701exxuxwq"|| 
                                    $step_value["clef"] == "startDate"|| 
                                    $step_value["clef"] == "plannedDuration"|| 
                                    $step_value["clef"] == "exactLocationAction"|| 
                                    $step_value["clef"] == "numberStakeholdersProject"|| 
                                    $step_value["clef"] == "totalNumberBeneficiaries"|| 
                                    $step_value["clef"] == "aapStep1l1lzd7y6jak23ytnjpd"|| 
                                    $step_value["clef"] == "budget"|| 
                                    $step_value["clef"] == "depense"|| 
                                    $step_value["clef"] == "aapStep1l1xfy8ox73zuib530a3"|| 
                                    $step_value["clef"] == "aapStep1l1xfp33q5bg1bz8mwbg"|| 
                                    $step_value["clef"] == "aapStep1l1xft16v6ohm6m5o0wu"
                                    ){
                                        $checked = "checked";
                                    }
                                    if(!$step_value["exportable"]){
                                        $display = "none";
                                    }
                                    ?>
                                    <div class="checkbox reorderable-input-<?= $answerid?>">
                                        <label class="input-value <?= !$step_value["exportable"] ? 'text-danger' : '' ?>">
                                            <input class="pdf-field-export-<?= $answerid?>" data-parent="<?= $step["name"] ?>"
                                                data-step="<?= $step_value["step"] ?>"
                                                data-label="<?= $step_value["label"] ?>"
                                                data-type="<?= $step_value["type"] ?>"
                                                data-id="<?= $step_value['id'] ?>"
                                                data-position="<?= $step_key ?>"
                                                value="<?= $step_value["clef"] ?>" type="checkbox"  <?= !$step_value["exportable"] ? 'disabled' : $checked ?> >
                                            
                                                <?= $step_value["label"] ?>
                                        </label>
                                        <div class="action-btn">
                                            <i class="fa fa-edit editor-<?= $answerid?>"></i>
                                            <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>

                        <div class="tab-pane" id="setting-text-pdf-<?= $answerid?>">
                            <div class="form-group">
                                <label for="pdf-text-before-<?= $answerid?>">Texte avant :</label>                           
                                <textarea type="textarea" id="text-before-<?= $answerid?>" class="form-control text-before-<?= $answerid?>" rows="4" cols="50"></textarea> 
                            </div>
                            <div class="form-group">
                                <label for="pdf-text-after-<?= $answerid?>">Texte après :</label>                          
                                <textarea type="textarea" id="text-after-<?= $answerid?>" class="form-control text-after-<?= $answerid?>"  rows="4" cols="50"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="edit-img<?php echo $answerid ?> tooltips btn btn-primary" type="button" >Modifier l'image de signature
                                    <i class="fa fa-camera" aria-hidden="true"></i><br>
                                </button>
                            </div>
                        </div>

                        <div class="tab-pane" id="setting-other-pdf-<?= $answerid?>">
                            <div class="checkbox reorderable-input-<?= $answerid?>">
                                <label class="input-value">
                                    <input class="pdf-field-export-<?= $answerid?>" 
                                    data-parent="Proposition" 
                                    data-step="Other" 
                                    data-label="Comment" 
                                    data-type="" 
                                    data-id="" 
                                    data-position="1" 
                                    value="comment" 
                                    type="checkbox" 
                                    <?php
                                    if(!empty($form["pdfField"][$user_session_id])){
                                        foreach ($form["pdfField"][$user_session_id] as $key => $value) {
                                            if($value["value"] === "comment") echo "checked";
                                        }
                                    }
                                    ?>
                                    >
                                    <?= Yii::t("comment","comment") ?>
                                </label>                                      </label>
                                <div class="action-btn">
                                    <i class="fa fa-edit editor-<?= $answerid?>"></i>
                                    <i class="fa fa-arrows dragger_<?= $answerid?>"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-xs-12 padding-top-15">
                <button type="button" class="btn btn-primary pull-right" onclick="return perform_pdf_exportation_<?= $answerid?>(this)"><span></span> Exporter</button>
            </div>
        </div>
    </div>