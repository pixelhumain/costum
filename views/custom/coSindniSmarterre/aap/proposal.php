<style>
	:root {
		--aap-primary-color: #9BC125;
		--red-color: #E50000;
		--red-background: #F7B9B9;
	}

	.zone-column {
		flex: 1;
	}

	.proposition-container {
		padding: 15px;
		border-radius: 16px;
		margin-bottom: 20px;
		box-shadow: 0 0 10px rgba(0, 0, 0, .2);
		display: flex;
		flex-direction: row;
		gap: 10px;
	}

	.proposition-container-compact {
		width: calc(50% - 10px);
		display: inline-block;
		vertical-align: top;
	}

	.proposition-container-compact:nth-child(even) {
		margin-left: 10px;
	}

	.proposition-container-compact:nth-child(odd) {
		margin-right: 10px;
	}

	.proposition-name {
		text-transform: none;
		font-size: 19px;
		margin-top: 0;
	}

	.proposition-description {
		overflow: hidden;
		text-overflow: ellipsis;
		display: -webkit-box;
		-webkit-line-clamp: 6;
		-webkit-box-orient: vertical;
	}

	.proposition-image-container {
		border-radius: 8px;
		overflow: hidden;
		width: 170px;
		height: auto;
		max-height: 225px;
		position: relative;
	}

	.proposition-image-container img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.proposition-info-container {
		flex: 1;
		overflow: hidden;
	}

	.proposition-actions-container {
		display: flex;
		flex-direction: column;
		align-items: end;
	}

	.star-indicator {
		position: absolute;
		top: 9px;
		left: 9px;
		border-radius: 50%;
		background-color: white;
		width: 45px;
		height: 45px;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
	}

	.star-indicator .fa {
		font-size: 20px;
	}

	.star-indicator.active .fa {
		color: var(--aap-primary-color);
	}

	.parent-link,
	.parent-link:hover {
		color: var(--aap-primary-color);
		cursor: pointer;
	}

	.depositor-image {
		display: inline-block;
		overflow: hidden;
		width: 35px;
		height: 35px;
		border-radius: 50%;
		vertical-align: middle;
	}

	.depositor-image img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.deposition-date {
		margin-left: 28px;
	}

	.alert-actions {
		display: flex;
		flex-direction: row;
		gap: 10px;
		align-items: center;
	}

	.contributors {
		width: 150px;
		text-align: center;
	}

	.contributor-images {
		position: relative;
		width: 100%;
		height: 40px;
	}

	.contributor-images .contributor-image {
		position: absolute;
		top: 0;
		border: 2px solid white;
		border-radius: 50%;
		overflow: hidden;
		width: 35px;
		height: 35px;
	}

	.contributor-images .contributor-image img {
		object-fit: cover;
		width: 100%;
		height: 100%;
	}

	.more-contributor-image {
		position: absolute;
		right: 0;
		top: 50%;
		transform: translateY(-50%);
		background: white;
		padding: 0 8px;
		border-radius: 10px;
		border: 1px solid rgba(0, 0, 0, .2);
		cursor: pointer;
	}

	.proposition-detail {
		color: white;
		background-color: var(--aap-primary-color);
		padding: 8px 14px;
		border: none;
		outline: none;
		font-weight: bold;
		border-radius: 8px;
	}

	.proposition-alert {
		color: var(--red-color);
		background-color: var(--red-background);
		padding: 2px 8px;
		border-radius: 8px;
	}

	@media screen and (max-width: 768px) {
		.proposition-container-compact {
			width: calc(100% - 10px);
			display: inline-block;
			vertical-align: top;
		}
	}

	#mapOfResultsAnswL {
		width: 100vw;
		top: 0px;
		left: 0px;
		z-index: 999;
		position: sticky;
		height: calc(100vh - 81px);
	}

	.mapOfResults {
		position: sticky;
		top: 60px;
	}
</style>
<?php
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Aap;
$extraVars = array(
	"renderView" => ""
);
?>
<div class="container no-padding">
	<div class="row">
		<div class="col-xs-8">
			<h3 class="title"> 
				<span class="main"> Appel à projet - Nom de l’organisme/de l’appel à projet - </span>
				<span class="status"> session Dépôt ouverte </span>
			</h3>
		</div>
		<div class="col-xs-4 padding-top-20">
			<a href="javascript;" class="btn btn-primary pull-right">
				Déposer une proposition
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 padding-top-20">
			<div class="pull-right">
				Vue
				<button class="btn btn-default change-list-view border-none p-2" data-view="detailed">
					<i class="fa fa-th-list fs17"></i>
				</button>
				<button class="btn btn-default change-list-view border-none p-2" data-view="minimalist">
					<i class="fa fa-bars fs17 fw600 rotate90"></i>
				</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 no-padding">
			<div class="col-xs-3">
				<div id='filterContainerL' class='searchObjCSS'></div>
			</div>
			<button type="button" class="btn btn-default showHide-filters-xs hidden-xs" style="height: fit-content;" id="show-filters-lg">
				<span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success"></span>
				<i class="fa fa-filter"></i>
			</button>
			<div class="col-xs-8 filter-container">
				<div class='headerSearchContainerL no-padding col-xs-12'></div>
				<div class='bodySearchContainer margin-top-30'>
					<div class='col-xs-12 no-padding' id='dropdown_search'>
					</div>
					<div class='padding-top-20 col-xs-12 text-left footerSearchContainer'></div>
				</div>
			</div>
			<div class="col-xs-1 mapOfResults">
				<div id="mapOfResultsAnswL">

				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function() {
		var showMap = (exists(aapObj.form?.params?.showMap) && aapObj.form?.params?.showMap);

		if (showMap) {
			var mapAnsw = new CoMap({
				container: "#mapOfResultsAnswL",
				activePopUp: true,
				zoom: 0,
				mapOpt: {
					zoom: 0,
					latLon: ["-21.115141", "55.536384"],
				},
				mapCustom: {
					tile: "mapbox"
				},
				elts: [],
				buttons: {
					fullScreen: {
						allow: true
					}
				}
			});
		} else {
			$('.mapOfResults').remove();
			$('.filter-container').removeClass("col-xs-8").addClass("col-xs-9");
		}

		function proposition_init_filters() {
			var filterPrms = aapObj.paramsFilters.proposal(aapObj);
			mylog.log(filterPrms,"filterPrmss")
			var filterSearchL = searchObj.init(filterPrms);
			if (showMap) {
				filterSearchL.results.addToMap = function(fObj, results) {
					var elts = [];
					$.each(results, function(resultKey, result) {
						if (result.answers) {
							$.each(result.answers, function(k, answer) {
								if (exists(answer.adress) && exists(answer.adress.geo))
									elts.push({
										id: result._id.$id,
										geo: answer.adress.geo,
										collection: "organization",
										name: "Association:",
									})
							})
						}
					})
					mapAnsw.addElts(elts);
				};
			}
			filterSearchL.search.init(filterSearchL);
		}

		if (notEmpty(aapObj.form))
			proposition_init_filters(aapObj.form)
			aapObj.events.proposal(aapObj)
	})
</script>