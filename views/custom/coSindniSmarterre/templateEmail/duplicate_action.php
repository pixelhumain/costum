<p style="font-size:20px">
    Demande de duplication du dossier <i>{prop.title}</i> pour l'année <?= date("Y") ?>
</p>
<p>
    Clicker ici pour accepter : 
    <a target="_blank" href="<?= Yii::app()->getRequest()->getBaseUrl(true);?>/co2/aap/duplicateproposition/form/{prop.formid}/answerid/{prop.answerid}/year/<?= date("Y") ?>">
        Dupliquer
    </a>
</p>