<style>@media (min-width: 1200px){.email-template-container {width: 1170px;}}@media (min-width: 992px){.email-template-container {width: 970px;}}</style>
<div class="email-template-container" style="background-color:#fff;padding-right: 15px; padding:0 50px; margin: auto;">
    <table>
        <tbody>
        <tr>
            <td><img src="<?=  Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/logo.png" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/logo2.png" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/sedre.jpg" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/semader.png" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/shlmr.png" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/sidr.png" width="60" height="auto" alt="" /></td>
            <td><img src="<?= Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->getModule( "costum" )->assetsUrl ?>/images/aap/coSindniSmarterre/sodiac.jpg" width="138" height="auto" alt="" /></td>
        </tr>
        </tbody>    
    </table>
    <p style="font-size:9px;text-align:left;margin:0px">
        <br/>
        <b>Direction Générale Ville Citoyenne <br/>
        Direction du Développement des territoires <br/>
        Politique de la Ville/Contrat de Ville <br/><br/></b>
        Affaire suivie par : Magalie CESBRON <br/>
        politique.ville@saintdenis.re <br/>
        0262 40 45 30
    </p>
    <table style="width:100%">
        <tr>
            <td style="width: 50%;">&nbsp;</td>
            <td style="width: 50%;font-size:9px">
                <br/>
                A Saint-Denis le, {prop.today} <br/><br/>
                Madame la Présidente, <br/>
                Monsieur le Président, <br/>
                De l’association : {prop.association}
            </td>
        </tr>
    </table><br/>
    <p style="font-size:9px;margin:0px;">
        <br/>
        <u>Objet :</u> <b>Notification d’attribution de subvention dans le cadre de l’Appel à Projet Contrat de Ville    2022</b>
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Madame la Présidente,<br/>
        Monsieur le Président,
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Votre association a répondu à l’appel à projet lancé par la ville de Saint-Denis, l’Etat et les Bailleurs dans le cadre du  Contrat de Ville 2022. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Nous vous remercions de l’intérêt que vous portez, à nos côtés, au développement du territoire dionysien. <br/>
        Ce sont plus de 400 projets qui ont été déposés et analysés avec l’ensemble des cofinanceurs.
    </p>
    <p style="font-size:9px;margin:0px;">
       <br/>
        Le projet <b><i>"{prop.title}"</i></b> proposé par votre association a retenu toute notre attention et  a eté validé lors du Comité de Pilotage du 9 juin 2022.
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        En effet, celui-ci correspond aux enjeux et axes prioritaires identifiés par les financeurs. 
    </p>
    <p style="font-size:9px;margin:0px;">
        <br/>
        Merci de trouver ci-après le détail du financement accordé : 
    </p><br/>
    {prop.table.subvention}
    <p style="font-size:9px;margin:0px;">
        <br/>
        Pour votre pleine et entière information, je vous prie de trouver les élements suivants : 
            <ul>
                <li style="font-size: 9px;">concernant la subvention de la  Ville, c’est un acompte  qui vous sera versé dans un premier temps. Ce projet devra faire l’objet d’un bilan pour l’obtention  du solde.</li>
                <li style="font-size: 9px;">concernant la part des bailleurs, vous serez contacté par ceux-ci afin de signer la convention de partenariat.</li>
                <li style="font-size: 9px;">concernant la part Etat, une saisie sur le site DAUPHIN est nécessaire dés l’obtention de cette notification. <br/>
                    L’accès au portail DAUPHIN s’effectue via le lien suivant : <a href="https://usager-dauphin.cget.gouv.fr">https://usager-dauphin.cget.gouv.fr</a>
                </li>
            </ul>
        <span>Veuillez agréer, Madame la Présidente, Monsieur le Président, nos salutations distinguées.</span>
    </p>
    <!-- <p style="font-size:9px;margin:0px;">
        Veuillez agréer, Madame la Présidente, Monsieur le Président, nos salutations distinguées.
    </p> -->
    <p style="text-align:right">
        <img src="{prop.signature}" width="auto" height="120" alt="">
    </p>
    <img style="display:none" src="{prop.fakeImg}" alt="">
</div>
<p style="text-align:center;display:pdf">
    <a target="_blank" href="<?= Yii::app()->getRequest()->getBaseUrl(true)?>/co2/aap/attachedfile/answerid/{prop.id}/template/avis_favorable_QPV">Télécharger le fichier PDF</a>
</p>

