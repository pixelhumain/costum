<?php
	$filliaireCategories = CO2::getContextList("filliaireCategories"); 
	$cssAnsScriptFilesModule = array(
		'/js/default/activitypub.js'
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
	
?>

<style>
    #filterContainer .dropdown .btn-menu,
    .searchObjCSS .dropdown .btn-menu,
    #filterContainer .dropdown .dropdown-menu,
    .searchObjCSS .dropdown .dropdown-menu,
    #costum-scope-search .dropdown-result-global-search,
    .searchObjCSS #input-sec-search .input-group-addon,
    .searchObjCSS #input-sec-search .input-global-search,
    #filterContainer #input-sec-search .input-global-search,
    .searchBar-filters .search-bar,
    .searchBar-filters .main-search-bar-addon
    {
        border-color: #80c240;
    }
    .searchBar-filters .main-search-bar-addon,
    #activeFilters .filters-activate,
    .headerSearchContainer .btn-show-map{
        background-color: #80c240 !important;
    }
    .searchObjCSS #input-sec-search .input-group-addon {
        color: #80c240;
    }
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;
	var listInit=["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi"];
	
	var defaultFilters = {'$or':{}};
	if(typeof costum != "undefined" && notNull(costum)){
		// defaultFilters['$or']["parent."+costum.contextId+".type"] = costum.contextType;
	    // defaultFilters['$or']["source.keys"] = costum.slug;
	    defaultFilters["reference.costum"] = [costum.slug];
	    // defaultFilters['$or']["links.projects."+costum.contextId+".type"] = costum.contextType;
	    // defaultFilters['$or']["links.memberOf."+costum.contextId+".type"] = costum.contextType;
	}

	var paramsFilter= {
		container : "#filters-nav",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	defaults:{notSourceKey:true},
	 	loadEvent:{
	 		default:"scroll"
	 	},
	 	results : {
	 		map : {
	 			show : false
	 		},
            renderView:"directory.elementPanelHtml"
	 	},
	 	filters : {
	 		/*"top": {
	 			container : "#filters-nav",
			 	lists: {*/
			 		text : true,
			 		scope : true,
			 		themes : {
			 			lists : filliaireCategories
			 		},
			 		types : {
			 			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi"]
			 		}
			 	//}
		 	//}
	 	}
	};
	if(typeof appConfig != "undefined" && typeof appConfig.useFilters != "undefined" && !appConfig.useFilters)
		paramsFilter.filters={};
	if(typeof appConfig.loadEvent != "undefined" && notNull(appConfig.loadEvent)){
		paramsFilter.loadEvent=appConfig.loadEvent;
	}

	/* Initialise les parametres des filtres */
	mylog.log("appconfig", appConfig);
	if(typeof appConfig.searchObject != "undefined"){
		if(typeof appConfig.searchObject.indexStep != "undefined")
			paramsFilter.defaults.indexStep=appConfig.searchObject.indexStep;
		if(typeof appConfig.searchObject.sortBy != "undefined")
			paramsFilter.defaults.sortBy=appConfig.searchObject.sortBy;
		if(typeof appConfig.searchObject.filters != "undefined"){
			paramsFilter.defaults.filters=appConfig.searchObject.filters;
			if(typeof appConfig.searchObject.filters.types != "undefined"){
				paramsFilter.defaults.types=appConfig.searchObject.types;
				if(typeof paramsFilter.filters.types != "undefined")
					paramsFilter.filters.types.lists=appConfig.searchObject.types;
			}
		}
		if(typeof appConfig.searchObject.sourceKey != "undefined")
			paramsFilter.defaults.sourceKey=appConfig.searchObject.sourceKey;
		
		/*if(typeof appConfig.searchObject.notSourceKey == "undefined")
			paramsFilter.defaults.notSourceKey=appConfig.searchObject.notSourceKey;*/

	}
	if(typeof appConfig.sourceKey=="undefined" && typeof costum !="undefined"){
		paramsFilter.defaults.notSourceKey = true
	}else if(appConfig.sourceKey==true && typeof costum != "undefined"){
		paramsFilter.defaults.sourceKey = costum.contextSlug
	}else{
		paramsFilter.defaults.sourceKey = appConfig.sourceKey
	}
	if(typeof appConfig.extendFilters != "undefined"){
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	}
	if(typeof appConfig.filters != "undefined"){
		if(typeof appConfig.filters.types != "undefined"){
			paramsFilter.defaults.types=appConfig.filters.types;
			if(typeof paramsFilter.filters.types != "undefined")
				paramsFilter.filters.types.lists=appConfig.filters.types;
		}
		if(typeof appConfig.filters.type != "undefined")
			paramsFilter.defaults.type=appConfig.filters.type;
		if(typeof appConfig.filters.category != "undefined")
			paramsFilter.defaults.category=appConfig.filters.category;
		if(typeof appConfig.filters.tags != "undefined")
			paramsFilter.defaults.tags=appConfig.filters.tags;
		if(typeof appConfig.filters.forced != "undefined")
			paramsFilter.defaults.forced=appConfig.filters.forced;
	}


	if(notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.sourceKey != "undefined"){
		//paramsFilter.defaults.sourceKey=costum.slug;
	}

	if(typeof appConfig.results != "undefined")
		paramsFilter.results=appConfig.results;
	if(notNull(costum)){
		paramsFilter.defaults["filters"] = defaultFilters;
	}
	
	var filterSearch={};
		
	jQuery(document).ready(function(){
		mylog.log('filterSearch paramsFilter', paramsFilter);
		filterSearch = searchObj.init(paramsFilter);
	});
</script>