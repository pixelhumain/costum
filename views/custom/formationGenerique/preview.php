
<?php

    $cssAnsScriptFilesModule = array(
        '/js/default/preview.js',
        '/js/default/profilSocial.js',
        '/js/links.js'
    );

    // var_dump($type,$element);exit;
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

    $financers=[];
    $partners=[];
    $porteurCTE=[];
    $porteurAction=[];

    $params["nbContributors"]=0;

    $where=array("id"=>@$id, "type"=>$element["collection"], "doctype"=>"image", "contentKey"=> "slider");
    $images = Document::getListDocumentsWhere($where, "image");

    $visioDates=[];

    if(isset($element["visioDate"]["alldates"])){
        foreach($element["visioDate"]["alldates"] as $date =>$sched){ 
                
            foreach($sched as $i=>$start){
                // var_dump($date,$start);exit;   
                $visioDates[]=$date." ".$start; 
            }    
        }
    }

    $colorCode="";
    $colorClass="dark";
    $deleteLabel="";
    $editLabel="";
    if(isset($element["category"])){
        if($type==Event::COLLECTION && $element["category"]=="sessionFormation"){
            $colorCode="orange !important";
            $colorClass="orange";
            $deleteLabel="Supprimer la session";
            $editLabel="Editer la session";
        }else if($type==Project::COLLECTION && $element["category"]=="formation"){
            $colorCode="#004e87 !important";
            $colorClass="orange";
            $deleteLabel="Supprimer la formation";
            $editLabel="Editer la formation";

        }
    }

    $auth = (Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

?>

<style type="text/css">

    .preview-main-container{
        padding: 0px 40px;
    }

    .preview-main-container > div{
        margin-top:15px;
    }

    .btn-edit-preview:hover .show-hover, .btn-delete-element-preview:hover .show-hover{
        display: inline-block;
    }

    .show-hover{
        display:none;
    }
    /* .show-hover:hover{
        display:inline-block;
    } */
    
    .social-share-button img{
        margin-right: 10px;
    }

    .listsItemsPod{
        text-align:left;
        /* width: fit-content !important;
        float: left; */
        margin-left:30px;
    }

    .listsItemsPod a{
        margin-left: 10px;
    }

    .listsItemsPod h4{
        display:none;
    }

    .container-preview h3, 
    .container-preview h4, 
    .container-preview  h5{
        text-transform:none;
    }
    #modal-preview-coop h3, 
    .morelink, 
    .text-highlight, 
    .text-highlight li, 
    .text-highlight li a, 
    .pod-info-Address span, 
    #modal-interesting li{
        color:  var(--color4);
        font-size: 14px;
    }
    #modal-interesting a{
        color:  var(--color4);
        font-size: 20px;
    }
    .pod-info-Address{
        float:left;
        width:100%;
    }
    .container-preview h4, 
    .container-preview h5,
    .container-preview  .section-title,  .morecontent strong,  
    .morecontent li::marker{
        color : #1b3b53;
        font-weight:700;
        font-size:16px !important;
        text-transform: none;
    }
    .container-preview .section-title{
        color: #1b3b53;
        font-weight: 700;
        font-size: 19pt !important;
        text-transform: none;
        /*border-bottom: 5px solid #219c90;
        border-bottom-right-radius: 20px;*/
        padding-right: 20px;
        padding-bottom: 5px;
        width: max-content; 
    }
    .container-preview .section-title.no-border{
        border-bottom: none;

    }
    /* .section-title:first-letter{
        color:white;
        font-size:larger;
    } */

    .section-title:before{
        /* content: "";
        position: absolute;
        margin-top: -5px;
        z-index: -1;
        margin-left: -15px;
        width: 2em;
        height: 2em;
        background-color: var(--primary-color);
        border-radius: 50%;
        content: "";
        position: absolute;
        margin-top: -5px;
        z-index: -1;
        margin-left: -40px;
        width: 1.5em;
        height: 1.5em;
        background-color: var(--primary-color);
        border-radius: 5px; */
    }
    .section-content{
        margin-top:20px;
    }
    .section-title{
        margin-top:20px;
        color: var(--primary-color) !important;
    }
    p, .morecontent p, .morecontent ul, .morecontent li{
        color:#575756;
        font-size:16px !important;
    }
    .community-pod{
        /* display: inline-flex; */
    }
    #modalLogin, #modalRegister{
        z-index:200001;
    }
    /* .list-session li .session{
        font-size : 14px;
    } */
	@media screen and (min-width: 1360px) {
		.container-preview  .basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.container-preview  .basic-banner {
			background-size: 15% auto;
		}
	}
    .container-preview  .basic-banner {
		/* padding-top: 50px;
		padding-bottom: 100px;		 */
		background: var(--primary-color);
	}
    .container-preview .container-banner {
        display: flex;
        width: 100%;
        height: 250px;
        background: #219c90;
    }
    .container-preview .basic-info{
        display: flex;
        justify-content: start;
        align-items: end;
        padding-bottom: 40px;
    }
    .container-preview .basic-img{
        display: flex;
        justify-content: end;
        align-items: end;
        /* padding-right: 50px; */
        padding-bottom: 20px;
        padding-left: 60px; 
        text-align: right;
        padding-right: 40px;
    }
    .basic-img .box-img{
        background-color: #fff;
        border-radius: 6px;
        min-height: 160px;
        --shadow: #5ce4d6 0px 0px 0px 2px inset, #219c90 -10px -10px 3px -3px, #5ce4d6 -10px -10px, rgb(255, 255, 255) -20px -20px 0px -3px, #5ce4d6 -20px -20px, #219c90 -30px -30px 0px -3px, #5ce4d6 -30px -30px, rgb(255, 255, 255) -40px -40px 0px -3px, #5ce4d6 -40px -40px;
        /* --shadow: #5ce4d6 0px 0px 0px 2px inset, #219c90 -10px -10px 3px -3px, #5ce4d6 -10px -10px, rgb(255, 255, 255) -20px -20px 0px -3px; */
        box-shadow: var(--shadow);
        display: flex;
        padding: 10px;
        justify-content: center;
        align-items: center;
    }

    .basic-img img {
        width: auto;
        height: 150px;
        object-fit: contain;
		/* margin-left: -1em;
		width: 15em;
		height: 15em;
		border-radius: 10%;
        object-fit :cover; */
	}
    .title-banner h1{
        text-transform: none;
        font-size: 25px;
        color: white;
        border-bottom: 5px solid #fff;
        border-bottom-right-radius: 20px;
        padding-right: 20px;
        padding-bottom: 5px;
        /* text-align: right; */
        font-size: 30px;
    }
    .title-banner h1,
    .title-banner h1 a{
        text-transform : none;
        font-size : 25px;
        color : white;
    }
    @media (max-width: 768px){
        .container-preview .container-banner{
            height: 180px;
        }
        .basic-img .box-img{
            min-height: 80px;
        }
        .basic-img img {
            /* margin-left: -2em;
            width: 10em;
            height: 10em; */
            height: 80px;
        }
        .title-banner h1{
            font-size : 15px;
        }
        .container-preview  .basic-banner {
            /* padding-top: 20px;
            padding-bottom: 100px; */
        }

    }
    .left-preview-header a:hover{
        color : white
    }
    .header-address{
        font-size: 14px;
        font-weight: 900;
    }
    .section-content padding-left-10{
        align-items: baseline;
        margin-top: 30px;
        padding-left: 50px;
        padding-right: 50px;
    }
    .container-preview .content{
        font-size: 16px;
        padding-left: 15px;
    }
    .avatar-formation{
        object-fit: contain;
        width: 50px;
        height: auto;
    }

    .bg-dark{
        background-color: var(--secondary-color) !important;
    }

    .bg-primary-color{
        background-color: var(--primary-color) !important;
    }

    .text-primary-color{
        color: var(--primary-color) !important;
    }

    .text-bold{
        font-weight: bolder;
    }
    .toolsMenu{
        border-bottom: 0px;
    }
    .btn{
        font-size: medium !important
    }

    #modal-preview-coop {
        display: none; 
        position: fixed; 
        padding-top: 100px; 
        left: 0;
        top: 0 !important;
        width: 100%; 
        height: 100%; 
        overflow: hidden; 
        background-color: rgb(0,0,0); 
        background-color: rgba(0,0,0,0.8); 
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
    }

    /* Modal Content */
    .modal-content {
        margin: auto;
        width: 70%;
    }

    .container-preview{
        border-start-start-radius: 5px;
        border-end-start-radius: 5px;
        background-color:white;
        z-index: 200002 !important;
    }
    .choose-date{
        background-color: white;
        margin-bottom: 10px;
        box-shadow: 3px 1px 5px 2px #e2e2e2;
        cursor: pointer;
        width: max-content;
        padding-right: 10px;
    }
    .choose-visio-date i{
        padding: 10px;
        background: #01305e;
        color: white;
    }
    #modal-interesting .modal-content{
        width: 100%;
    }
   
</style>
<div class="modal-content">
<div class="col-xs-12 no-padding margin-bottom-10 toolsMenu">
    <div class="left-preview-header">
        <?php 
        if($type==Event::COLLECTION && isset($element["category"]) && $element["category"]=="sessionFormation"){
            if($element["type"]=="course"){ ?>
                <a href='#page.type.projects.id.<?= array_keys($element["organizer"])[0] ?>' class='btn btn-default pull-left lbh-preview-element bg-primary-color text-white margin-right-20' title="retourner à la formation"><i class='fa fa-arrow-left margin-right-5'></i>Voir la formation</a>        
            <?php } 

            if($canEdit){ ?>
                <button class="btn text-bold pull-left bg-dark text-white openSession" data-id="<?= $element['_id'] ?>"><i class="fa fa-cog"></i> Gérer cette session</button>
            <?php } ?>
            <?php 
        
            $interestedStatus='<button class="btn text-bold btn-default pull-left bg-primary-color text-black margin-left-20 interesting-session">
            <i class="fa fa-heart"></i> Je suis intéressé.e par cette session de formation</button>';
        
            if(!empty(Yii::app()->session["userId"]) && isset($element["links"]["attendees"][Yii::app()->session["userId"]]["roles"]) && in_array("Intéressé.e",$element["links"]["attendees"][Yii::app()->session["userId"]]["roles"])){
                $interestedStatus='<button class="btn text-bold btn-default pull-left bg-primary-color text-white margin-left-20 disabled">
                <i class="fa fa-heart"></i> Je suis déjà intéressé.e</button>';
            }
        //  if(!Authorisation::isElementAdmin($element["_id"], $element["collection"] ,Yii::app()->session["userId"])){
            echo $interestedStatus;
        //  } ?>
        
        <?php }
        // var_dump($type,$element);exit;
        if($type==Project::COLLECTION && isset($element["category"]) && $element["category"]=="formation"){
            if($canEdit){ ?>
                <button class="btn text-bold pull-left bg-dark text-white add-session" data-parent-email="<?= @$element['email'] ?>" data-parent-description="<?= @$element['description'] ?>" data-parent-shortdescription="<?= @$element['shortDescription'] ?>" data-parent-name="<?= $element['name'] ?>" data-parent-id="<?= $element['_id'] ?>" data-parent-type="<?= $element["collection"] ?>" data-parent-thumbimg="<?=$element['profilThumbImageUrl']?>">Ajouter une session pour cette formation <i class="fa fa-plus-square-o text-primary-color"></i></button>
            <?php } ?>
            <button class="btn text-bold pull-left margin-left-20 bg-primary-color text-white view-session" data-parent-name="<?= $element['name'] ?>" data-parent-id="<?= $element['_id'] ?>" data-parent-type="<?= $element["collection"] ?>">Voir les sessions <i class="fa fa-list"></i></button>
            <a href="#page.type.<?= $type ?>.id.<?= (String)$element["_id"]?>.view.detail" class="lbh btn pull-left bg-primary-color text-white">Aller sur la fiche formation</a>
        <?php } ?>
        <?php 
        if($type==Organization::COLLECTION || $type==Person::COLLECTION){?>
            <a href="#page.type.<?= $type ?>.id.<?= (String)$element["_id"]?>.view.detail" class="lbh btn pull-left bg-primary-color text-white">Aller sur la fiche d'identité</a>
        <?php } ?>
    </div>
    <div class="right-preview-header">
        <button class="btn text-bold btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <?php 
		if ((isset($canEdit) && $canEdit)||$auth) { 
            if(isset($element["tags"]) && in_array("Organisme de formation",$element["tags"])){ ?>
                <button class="btn text-bold btn-default pull-right margin-right-10 text-white btn-edit-preview bg-primary-color" data-type="organizations" data-id="<?php echo $element["_id"] ?>" data-subtype="organismeFormation">
                    <i class="fa fa-pencil"></i> <span class="show-hover"> <?= $editLabel ?></span>
                </button>
            <?php }else if(isset($element["tags"]) && in_array("TiersLieux",$element["tags"])){ ?>
                <button class="btn text-bold btn-default pull-right margin-right-10 text-white btn-edit-preview bg-primary-color" data-type="organizations" data-id="<?php echo $element["_id"] ?>" data-subtype="tiersLieux">
                    <i class="fa fa-pencil"></i> <span class="show-hover"> <?= $editLabel ?></span>
                </button>
            <?php }else { ?>
                <button class="btn text-bold btn-default pull-right margin-right-10 text-white btn-edit-preview bg-primary-color" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo (($type==Event::COLLECTION || $type==Project::COLLECTION)&& isset($element["category"])) ? $element["category"] : $type ?>">
                    <i class="fa fa-pencil"></i> <span class="show-hover"> <?= $editLabel ?></span>
                </button>
            <?php } ?>
            <button style="" class="btn text-bold btn-default pull-right margin-right-10 btn-delete-element-preview bg-red text-white" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo ($type==Event::COLLECTION && isset($element["category"])) ? $element["category"] : $type ?>">
				<i class="fa fa-trash"></i> <span class="show-hover"> <?= $deleteLabel ?></span>
			</button>
		<?php } else if(Authorisation::isCostumAdmin() && $type=="citoyens"){?>

            <button class="btn text-bold btn-default pull-right margin-right-10 text-white btn-edit-preview bg-primary-color" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" >
                <i class="fa fa-pencil"></i> <span class="show-hover"> <?= $editLabel ?></span>
            </button>
            <button style="" class="btn text-bold btn-default pull-right margin-right-10 btn-delete-element-preview bg-red text-white" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo ($type==Event::COLLECTION && isset($element["category"])) ? $element["category"] : $type ?>">
				<i class="fa fa-trash"></i> <span class="show-hover"> <?= $deleteLabel ?></span>
			</button>
        <?php }?>
    </div>
</div>
<div class="container-preview col-xs-12 margin-bottom-20" style="overflow-y: scroll;padding-top: 20px">
    <?php 
        if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"]))){
            // $url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
            $url= Yii::app()->theme->baseUrl . '/assets/img/background-onepage/connexion-lines.jpg';
        }else{
            $url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
        }
    ?> 

    <div class="col-md-4" style="position:sticky;top:0;z-index:1000">
        <img src="<?php echo (isset($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>" alt="Image" style="width:100%;object-fit:contain;border-radius:5px;" />
    </div>
    <div class="col-md-8">
    <!-- div class="banner-outer ">
        <div class="basic-banner">
            <div class="basic-banner-inner">
                <div class="container-banner"> 
                    <div class="basic-img">
                        <div class="box-img"> 
                            <img src="<?php echo (isset($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>" alt="">
                        </div>
                       </div>
                    <div class="basic-info">
                        <div class="title-banner">
                            <?php if($element["collection"]==Event::COLLECTION) {  
            
                                if(isset($element["links"]["organizer"])){
                                    foreach($element["links"]["organizer"] as $idorg => $valorg){
                                        if($valorg["type"]==Project::COLLECTION){
                                            $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                                            $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                                            break;
                                        }
                                    }
                                
                                } ?> 
                                <h1 class="font-weight-100">
                                    <?php 
                                        if(!empty($breadCrumbFormation)){
                                            echo $breadCrumbFormation;
                                        }
                                        echo $element["name"] 
                                    ?>   

                                </h1> 
                            <?php } else { ?>
                                <h1 class="font-weight-100"><?php echo $element["name"]??"" ?></h1>
                            <?php } ?>
                        </div>					
                    </div>
                </div>
            </div>
        </div>
    </div -->

    
    <div class="text-dark padding-left-15" style="font-size: 22pt; font-weight:bolder">
        <?php echo $element["name"]; ?>
    </div>
    <div class="col-xs-12 margin-bottom-20 ">
        <?php if($element["collection"]==Event::COLLECTION) { 
            if(isset($element["links"]["organizer"])){
                foreach($element["links"]["organizer"] as $idorg => $valorg){
                    if($valorg["type"]==Project::COLLECTION){
                        $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                        $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                        break;
                    }
                }
               
            }
            $descr= (@$element["description"]) ? $element["description"] : ""; 
            ?>
            <div class="section-content padding-left-10"> 
                <span class="text-highlight padding-right-10 padding-left-10 padding-top-5 padding-bottom-5 content bg-yellow" style="border-radius:5px"><?= (!empty($element["isPresential"]) && $element["isPresential"]=="false") ? "En Visio" : "Présentiel" ?></span>
            </div>
            <?php if(!empty($element["isPresential"]) && !empty($element["visioLink"]) && $element["isPresential"]=="false"){?>
                <div class="section-content padding-left-10" id="url">
                    <span class="section-title">Lien de la visio le formulaire d'inscription </span>
                    <span class="text-highlight content"><?=$element["visioLink"]?></span>
                </div>
            <?php } ?>
            <div class="section-content padding-left-10">
                <h4 class="section-title" style="font-weight:700;">Description</h4>
                <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
            </div>
            <!-- <div class="section-content padding-left-10" id="public">
                <?php $public=(isset($element["preferences"]["private"]) && $element["preferences"]["private"]==false) ? "Publique" : "Privée" ;?>
                <span class="section-title  no-border">Visibilté dans l'annuaire public des sessions :</span> <span class="text-highlight content"><?=$public?></span>
                
            </div> -->
            <div class="no-padding" id="dates" style="">
                <?php 
                $format = "d M Y"; //H:i
                $startDate=date_format(date_create($element["startDate"]), $format);
                $endDate=date_format(date_create($element["endDate"]), $format);
                //var_dump($element["startDate"]);exit;
                ?>
                    <div class=" section-content padding-left-10"> 
                        <span class="section-title no-border">Date de début de session :</span>
                        <span class="text-highlight startDate content"><?= date('d/m/Y', strtotime(str_replace("/","-",$element["startDate"])))?></span>
                    </div>
                    <div class="section-content padding-left-10"> 
                        <span class="no-padding section-title  no-border">Date de fin de session :</span>
                        <span class="text-highlight endDate content"><?= date('d/m/Y', strtotime(str_replace("/","-",$element["endDate"])))?></span>
                    </div>
                <?php if(isset($element["duration"])){ ?>
                    <div class="section-content padding-left-10"> 
                        <span class=" section-title  no-border">Durée de la session : </span>
                        <span class="text-highlight content"><?=$element["duration"]?> jours</span>
                    </div>
                <?php } ?>
            </div>
            <?php if(isset($element["module"])){ ?>
                <div class="section-content padding-left-10">
                    <span class=" section-title  no-border">Modules : </span>
                    <ul>
                        <?php foreach($element["module"] as $km => $vm){?>
                            <li class="text-highlight content">
                                <span><?= $vm["name"]?> ( Du <?= $vm["startDate"] ?> à  <?= $vm["endDate"] ?>)</span>                             
                            </li>
                        <?php } ?>
                    </ul>                
                </div>
            <?php } ?>
            <?php if(!empty($element["email"])){?>

                <div class="section-content padding-left-10" id="email"> 
                    <span class="no-padding section-title  no-border">Email de contact: </span>
                    <span class="text-highlight content"><?=$element["email"]?></span>
                </div>
            <?php } ?>
            <?php if(!empty($visioDates)){?>
                <div class="section-content padding-left-10" id="url" style="margin-bottom: 10px;">
                    <?php ?>
                    <p class="section-title">Date des visioconférences d'information
                    <?php 
                        echo "<ul>";                 
                            foreach($visioDates as $i=>$date){
                                echo '<span class="text-highlight content"><li>Le '.date('d/m/Y à H:i', strtotime(str_replace("/","-",$date))).'</li> </span>';
                            }
                        echo "</ul>"   
                    
                    ?>
                    </p>
                </div>
            <?php } ?>
            <?php if(!empty($element["gatheringDates"])){?>
                <div class="section-content padding-left-10" id="url" style="margin-bottom: 10px;">
                    <?php ?>
                    <p class="section-title">Les journées de formation
                        <?php 
                            echo "<ul>";                 
                                foreach($element["gatheringDates"] as $i=>$date){
                                    // var_dump(str_replace("/","-",$date),strtotime($date),strtotime(str_replace("/","-",$date)));
                                    echo '<span class="text-highlight"><li>Le '.date('d/m/Y', strtotime(str_replace("/","-",$date))).'</li> </span>';
                                }
                            echo "</ul>"   
                        
                        ?>
                    </p>
                </div>
            <?php } ?>
            <?php if(!empty($element["url"])){?>
                <div class="section-content padding-left-10" id="url">
                    <span class="section-title">Lien vers le formulaire d'inscription </span><span class="text-highlight content"><?=$element["url"]?></span>
                </div>
            <?php } ?>
            <?php if(!empty($element["links"]["attendees"])){ ?>
                <div class="section-content padding-left-10 community-pod" id="organizer-pod">
                    <p class="section-title">Les participants  </p>
                    <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les participants", "links"=>$element["links"]["attendees"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                    
                </div>
            <?php } ?>
            <?php if(!empty($element["trainer"])){?>
                <div class="section-content padding-left-10 community-pod" id="trainer-pod" >
                    <p class="section-title">Les intervenants  </p>
                    <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                    
                </div>
            <?php } ?>
            <?php if(!empty($element["partner"])){?>
                <!-- <div class="col-xs-6 community-pod" id="organizer-pod" style="margin-bottom: 10px;">
                <p class="col-xs-12 no-padding pull-left section-title">Les partenaires : </p>
                    <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                    
                </div> -->
            <?php }  
            
            ?>
            <?php if(!empty($element["address"])){?>
                <div class="col-md-10 col-xs-12 section-content padding-left-10" id="address" style="margin-bottom: 100px;">
                    <p class="section-title">Adresse  </p>
                    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                    
                </div>

            <?php } ?>    
        
        <?php } else if($element["collection"]==Project::COLLECTION) {  
            
            if(isset($element["links"]["organizer"])){
                foreach($element["links"]["organizer"] as $idorg => $valorg){
                    if($valorg["type"]==Project::COLLECTION){
                        $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                        $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                        break;
                    }
                }
               
            }
            // var_dump($formation);exit;
            
            // $formation=PHDB::findOne(Project::COLLECTION,array());
            $descr= (@$element["description"]) ? $element["description"] : ""; 
            ?>

            <?php if (isset($element["shortDescription"])){ ?>
                
                <div class="section-title padding-left-10">Description courte</div>
                <div class="content ">
                    <?= $element["shortDescription"]?>
                </div>
            <?php }?>
            <h3 class="col-xs-12 text-primary-color padding-left-10" style="font-weight:700;font-size:19pt">Contenu, objectifs pédagogiques</h3>
            <div class="content more" id="descriptionAbout" style="margin-top:none; font-size:14pt">  <?php echo $descr ?> </div>

            <?php if (isset($element["url"])){ ?>
                
                <div class="section-title padding-left-10"> URL principale </div>
                <div class="content ">
                   <u><a href="<?= $element['url']?>" target="_blank"><?= $element["url"]?></a></u>
                </div>
            <?php }?>
            <?php if (isset($element["parent"])){ ?>
                <div class="section-content padding-left-10" style="margin-bottom: 10px;"> 
                    <p class="section-title">Centres de formation  </p>
                    <?php foreach($element["parent"] as $k => $v){ ?>
                        <a href="#page.type.<?php echo $v["type"] ?>.id.<?php echo $k; ?>" target="_blank" title="<?php echo $v["name"] ?>">
                            <img class="img-round-profil" alt="image" src="<?php echo $v["profilThumbImageUrl"] ?>"><span> <?php echo $v["name"]?></span><br>
                        </a>
                    <?php } ?>
                </div>  
            <?php } ?>

            <?php if (isset($element["urlsDoc"])){ ?>
                <div class=" section-content padding-left-10" id="urlsDoc" style="margin-bottom: 10px;"> 
                    <?php 
                    $urlsDoc="<ul class ='text-highlight' >";
                    foreach($element["urlsDoc"] as $ind=>$l){
                        if($l != ""){
                            $urlsDoc.="<li><a href='".$l."' target='_blank' class='content'>".$l."</a></li>";
                        }

                    } 
                    $urlsDoc.="</ul>";
                    ?>
                    <p class="section-title">Liens vers la documentation, le programme  <span class="text-highlight content"><?= $urlsDoc ?></span></p>
                </div>
            <?php } ?>
            <?php if (isset($element["rate"])){ ?>
                <div class="section-content padding-left-10" id="rate" style="margin-bottom: 10px;">
                    
                    <p class="section-title">Tarif :</p>
                    <div class="content"><?=$element["rate"]?> <?php if ($this->costum['slug'] !='transiter') echo "euros" ?> </div>
                </div>
            <?php } ?>
            <?php if (isset($element["competence"])){ ?>
                
                <div class="section-title padding-left-10"> Prérequis nécessaires</div>
                <div class="content ">
                    <?= $element["competence"]?>
                </div>
            <?php }?>
            <?php if (isset($element["publicCible"])){ ?>
                <div class="section-title padding-left-10">  Public cible</div>
                <div class="content ">
                    <?= $element["publicCible"]?>
                </div>
            <?php }?>
            <?php if (isset($element["teachingMethods"])){ ?>
                
                <div class="section-title padding-left-10">  Méthodes pédagogiques</div>
                <div class="content ">
                    <?= $element["teachingMethods"]?>
                </div>
            <?php }?>
            <?php if (isset($element["evaluation"])){ ?>
                
                <div class="section-title padding-left-10">Evaluation</div>
                <div class="content ">
                    <?= $element["evaluation"]?>
                </div>
            <?php }?>
            <?php if (isset($element["certificationsAwards"])){ ?>
                <div class="section-title padding-left-10">Nom de la certification</div>
                <div class="content ">
                    <?= $element["certificationsAwards"]?>
                </div>
            <?php }?>
            <?php if (isset($element["levels"])){ ?>
                <div class="section-content padding-left-10" id="rate" style="margin-bottom: 10px;">
                    
                    <p class="section-title">Level :</p>
                    <div class="content">
                        <ul>
                        <?php foreach($element["levels"] as $kl=>$vl){?>
                            <li> <?= $vl ?></li>
                        <?php }?>
                    </ul>
                </div>
            <?php } ?>
            <?php if (isset($element["reportLink"])){ ?>
                <div class="section-title padding-left-10">Compte rendu</div>
                <div class="content ">
                    <?= $element["reportLink"]?>
                </div>
            <?php }?>
            <?php if (isset($element["email"])){ ?>
                <div class="section-content padding-left-10" id="email" style="margin-bottom: 10px;">
                    
                    <p class="section-title">Pour plus d'informations : <span class="text-highlight"><?=$element["email"]?></span></p>
                </div>
            <?php } ?>
            <?php if (isset($element["trainer"])){ ?>
                <!-- <div class="col-xs-6 community-pod" id="trainer-pod" style="margin-bottom: 10px;">
                <p class="col-xs-12 no-padding pull-left section-title">Les intervenants : </p>
                    <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                    
                </div> -->
            <?php } ?>
            <?php if (isset($element["partner"])){ ?>
                <div class="community-pod section-content padding-left-10" id="organizer-pod" style="margin-bottom: 10px;">
                    <p class="no-padding section-title">Les partenaires : </p>
                    <?php echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                    
                </div>
            <?php } ?>
            <?php if(!empty($element["address"])){?>
                <div class="section-content padding-left-10" id="address" style="margin-bottom: 10px;">
                    <p class="pull-left section-title">Adresse  </p>
                    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                </div>

            <?php } ?>   
            <div class="col-xs-12 list-session section-content "></div> 
        
        <?php }  ?>
    </div>
    <?php if($element["collection"]==Organization::COLLECTION){ ?>
        <div class="preview-element-info col-xs-12">
            <?php 
            if (isset($element["socialNetwork"])){?>
                <div id="socialNetwork" class="col-xs-10 section-content padding-left-10 margin-top-10 text-center">
                    <?php 
                        if (isset($element["socialNetwork"]["facebook"])){ ?>		
                        <span id="divFacebook" class="margin-right-10">
                            <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"];?>" target="_blank" id="facebookAbout" >
                                <i class="fa fa-facebook" style="color:#1877f2;"></i>
                            </a>
                        </span>
                    <?php }
                        
                    if (isset($element["socialNetwork"]["twitter"])){ ?>	
                        <span id="divTwitter">
                            <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"];?>" target="_blank" id="twitterAbout" >
                            <i class="fa fa-twitter" style="color:#1da1f2;"></i>
                            </a>
                        </span>
                    <?php } ?>
                </div>  
            <?php
            }						
            if(isset($element["description"])) { 
                $descr= (@$element["description"]) ? $element["description"] : ""; 
                ?>
                <div class="section-content padding-left-10">
                    <h2 class="section-title"> Description</h2>
                    <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
                </div>
            <?php } ?>
            <?php if(isset($element["tags"]) && in_array("Organisme de formation",$element["tags"]) && isset($element["trainingQualification"]))	{ ?>
                <div class="section-content padding-left-10 "> 
                    <h2 class="section-title"> Qualification de la formation</h2>
                    <?php $trainingQualification = explode(",", $element["trainingQualification"]); ?>
                    <ul class="text-highlight">
                        <?php foreach ($trainingQualification as $k => $v){
                            echo '<li class="content">'.$v .'</li>';
                        } ?>
                    </ul>
                </div>
            <?php } ?>	
                
            <?php
            if(isset($element["url"])){ ?>
                <div class="section-content padding-left-10">
                    <h2 class="section-title"> Url principale</h2>
                    <?php $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ; ?>
                    <a class="content" href="<?php echo $scheme.$element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo $element["url"] ?></a>
                </div>
            <?php } ?>	
            <?php if(isset($element["tags"]))	{                 
                $tabSpec=array(
                    "typePlace"=>
                        [
                            "name"=>"Typologie d'espace de travail",
                            "icon"=>"briefcase"
                        ],
                    "services"=>
                        [
                            "name"=>"Services proposés",
                            "icon"=>"tags"
                        ],
                    "manageModel"=>	
                        [
                            "name"=>"Mode de gestion",
                            "icon"=>"adjust"
                        ],
                    "state"=>	
                        [
                            "name"=>"Etat du projet",
                            "icon"=>"cubes"
                        ],
                    "spaceSize"=>	
                        ["name"=>"Taille du lieu",
                        "icon"=>"expand"
                        ],
                    "certification"=>	
                        [
                            "name"=>"Label obtenu",
                            "icon"=>"medal"
                        ],
                    "greeting"=>	
                        [
                            "name"=>"Actions d'accueil",
                            "icon"=>"info"
                        ],
                    "network" =>	
                        [
                            "name"=>"Réseau d'affiliation",
                            "icon"=>"users"
                        ]
                );
                ?>			
                <div class="section-content padding-left-10"> 
                    <?php if(in_array("TiersLieux",$element["tags"])){ ?>	
                        <h2 class="section-title">Caractéristiques du lieu</h2>
                    <?php } else if(in_array("Organisme de formation",$element["tags"])){ ?>	
                        <h2 class="section-title">Mot clef</h2>
                    <?php } ?>
                    <table class='col-xs-12'>
                        <?php	
                            foreach($tabSpec as $k => $v){ 

                                $strTags="";
                                $tagsList="";
                                $listCurrent = [];
                                if(isset( $this->costum["lists"][$k])){
                                    $listCurrent=$this->costum["lists"][$k];
                                }
                                if(!empty($element["tags"])){
                                    foreach($element["tags"] as $tag){
                                        if(in_array($tag, $listCurrent)){
                                            $strTags.=(empty($strTags)) ? $tag : "<br>".$tag; 
                                        }
                                        
                                        
                                    }

                                }
                                if(!empty($strTags)){ ?> 
                                    <tr>
                                        <td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
                                            <?php  
                                            if(isset($v["icon"])){
                                                echo "<i class='fa fa-".$v["icon"]." tableIcone'></i>";
                                            }
                                            ?>
                                        </td>	
                                        <td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
                                            <?php  
                                            if(isset($v["name"])){
                                                echo $v["name"];
                                            }
                                            
                                            ?>		
                                        </td>
                                        <td class="col-xs-7" style="padding:8px;"><?php echo $strTags; ?></td>
                                    </tr>	 
                                <?php 
                                }
                            }?>
                    </table>	                                
                    <?php  foreach($element["tags"] as $tag) {	
                        if(!in_array($tag, $listCurrent)) {
                            $tagsList.= "#".$tag." ";	
                        }
                    }?>
                    <span class="tagList content"><?php echo $tagsList ?></span>
                </div>	
            <?php } ?>	
            <div class="section-content padding-left-10 " id="list-formation"> </div> 
            <?php if(!empty($element["address"])){?>
                <div class="section-content padding-left-10" id="address">
                    <h2 class="section-title"> Adresse  </h2>
                    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                </div>
            <?php } ?>   

            <div class="section-content padding-left-10">
                <h2 class="section-title">Les membres</h2>
                <?php 
                if(isset($element["links"]) && isset($element["links"]["members"])){ 
                    echo $this->renderPartial('co2.views.pod.listElement', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"]));
                } ?>
            </div> 
                
        
        </div>
    <?php } ?>
    <?php if($element["collection"]==Person::COLLECTION){ 

        $doc = Document::getListDocumentsWhere(
            array(
                "id"=> (String)$element["_id"],
                "type"=>'citoyens',
                "contentKey"=>'pdf',
                "source.key"=>$this->costum["slug"]
            ), "file"
        );
        $docPath  = array();
        if($doc){
            foreach($doc as $ks => $vs){
                $docPath[$vs["docPath"]] = $vs["name"];
            }
        }
        ?>
        <div class="preview-element-info col-xs-12">
            <?php if (isset($element["thematic"])){ ?>
                <div class=" section-content padding-left-10" id="urlsDoc" style="margin-bottom: 10px;">                     
                    <p class="section-title">Domaines d'activité  :   <span class="text-highlight content"><?= $element["thematic"]?></span>
                   </div>
            <?php } ?>		
            <?php if(!empty($element["mobile"])){?>
                <div class="section-content padding-left-10" id="email"> 
                    <span class="no-padding section-title  no-border">Téléphone: </span>
                    <span class="text-highlight content"><?=$element["mobile"]?></span>
                </div>
            <?php } ?>
            				
            <?php  if(!empty($element["email"])){?>
                <div class="section-content padding-left-10" id="email"> 
                    <span class="no-padding section-title  no-border">Email de contact: </span>
                    <span class="text-highlight content"><?=@$element["email"]?></span>
                </div>
            <?php } ?>
            <?php if(isset($element["description"])) { 
                $descr= (@$element["description"]) ? $element["description"] : ""; 
                ?>
                <div class="section-content padding-left-10">
                    <h2 class="section-title"> Présentation</h2>
                    <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
                </div>
            <?php } ?>

            <?php if (isset($element["link"])){ ?>
                <div class=" section-content padding-left-10" id="urlsDoc" style="margin-bottom: 10px;">                     
                    <p class="section-title">Lien vers le site web :  <span class="text-highlight content"><u><a href="<?=$element["link"] ?>" target="_blank"><?=$element["link"] ?></a></u></span></p>
                </div>
            <?php } ?>

            <?php if (isset($element["parent"])){ ?>
                <div class="section-content padding-left-10" style="margin-bottom: 10px;"> 
                    <p class="section-title">Centre de formation  </p>
                    <?php foreach($element["parent"] as $k => $v){ ?>
                        <a href="#page.type.<?php echo $v["type"] ?>.id.<?php echo $k; ?>" class="hover-underline lbh" data-toggle="popover" title="<?php echo $v["name"] ?>">
                            <img width="50" height="50"  alt="image" src="<?php echo $v["profilThumbImageUrl"] ?>">
                        </a>
                    <?php } ?>
                </div>  
            <?php } ?>
            <div class=" section-content padding-left-10" id="urlsDoc" style="margin-bottom: 10px;">                     
                <?php if($docPath ){?>    
                    <p class="section-title">Pièce jointe  :   
                        <?php foreach($docPath as $docPath => $docName){
                            echo "<span class='text-highlight content'><a href='".Yii::app()->createAbsoluteUrl("/").$docPath."' target='_blank'>".$docName."</a></span><br>";
                        }?>
                    </p>
                    <?php }?>    
            </div>

            <?php if(!empty($element["address"])){?>
                <div class="section-content padding-left-10" id="address">
                    <h2 class="section-title"> Adresse  </h2>
                    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                </div>
            <?php } ?> 
        </div>
    <?php } ?>
    </div>
<!-- Modal -->
<div id="modal-delete-element-preview" class="modal fade" role="dialog" style="z-index :1000000;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-white" style="font-size:20px"><?php echo Yii::t("common","Delete")?></h4>
            </div> 
            <div class="modal-body text-dark">
                <p>

                    <?php 

                        if($id == Yii::app()->session["userId"] && $type == Person::COLLECTION){
                            echo "Si vous supprimer votre comptes, toutes vos informations personnelles et vos messages seront supprimer";
                        } else {
                            echo Yii::t('common',"Are you sure you want to delete"). " <span style='font-weight:bolder'>". $element["name"]."</span>" ;
                        }

                    ?>	
                </p>
                <!-- <br>
                    <?php 
                    // echo Yii::t('common','You can add bellow the reason why you want to delete this element :') ;
                    ?>
                <textarea id="reason" class="" rows="2" style="width: 100%" placeholder="Laisser une raison... ou pas (optionnel)"></textarea> -->
            </div>
            <div class="modal-footer">
                <!-- Utilisation du bouton confirmDeleteElement -->
                <button id="confirmDeleteElementPreview" data-id="<?php echo $element["_id"]?>" data-typeevent="<?php echo (isset($element["type"])?$element['type']:'')?>" data-category="<?php echo (isset($element["category"])?$element['category']:'')?>" data-source ="<?php echo( isset($element['source']['key']) && $element['source']['key'] == $this->costum['slug'])? 'true':'false' ?>"data-reference ="<?php echo( isset($element['reference']['costum']) && array_search('transiter', $element['reference']['costum'])!== false)? 'true':'false' ?>" data-type="<?php echo $element["collection"]?>" type="button" class="btn btn-warning"><?php echo Yii::t('common','I confim the delete !');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
            </div>
        </div>

    </div>
</div>

<div id="modal-interesting" class="modal fade" data-dismiss="modal" role="dialog" style="z-index :1000000;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content choose-date-visio">
            <div class="modal-header bg-primary-color">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="font-size:20px !important"><?php echo Yii::t("common","Inscription à la visio d'information")?></h4>
            </div>
            <div class="modal-body text-dark">
                <p>

                    Des visio-conférences sont régulièrement organisées pour vous informer en détails des modalités relatives aux contenus et au déroulé de la session.
                   <b style="color: #01305e;"> Dans la liste ci-après choisissez, la visio-conférence d'informations qui vous conviendrait.</b>
                </p>
                <?php
                    //  "visioDate" : {
                    //     "alldates" : {
                    //         "12/10/2023" : [ 
                    //             "8:00"
                    //         ],
                    //         "13/10/2023" : [ 
                    //             "11:00"
                    //         ],
                    //         "14/10/2023" : [ 
                    //             "15:00"
                    //         ]
                    //     },
                    //     "eventduration" : "1"
                    // },   
                    if(isset($element["visioDate"]["alldates"]) ){
                        echo "";
                        foreach($element["visioDate"]["alldates"] as $date =>$sched){
                            
                            foreach($sched as $i=>$start){
                                $dayDate=str_replace("/","-",$date);
                                $formatedDate=date('Y-m-d\TH:i:s\Z', strtotime($dayDate." ".$start));
                                // date_format(date_create($date." ".$start), 'd/m/Y H:i');
                                // .moment("12/10/2023 11:00",'DD/MM/YYYY hh:mm').toISOString();
                                echo "<div class='choose-date'><a href='javascript:;' class='choose-visio-date' data-date='".$formatedDate."'><i class='fa fa-calendar'></i> <b> Le ".$date." à ".$start."</b></a></div>";

                                
                            }
                            // var_dump($formatedDate);
                            
                            
                            // $ssm2 = strtotime("3:33") - $midnight;

                            //             echo
                        }
                        echo "";
                        
                        if(isset($element["visioDate"]["eventduration"])){
                            echo "<br><div><p><i class='fa fa-clock-o'></i> Durée : ".$element["visioDate"]["eventduration"]." h</p></div>";
                        }
                    }?>
                    <!-- <br>
                    <?php 
                    // echo Yii::t('common','You can add bellow the reason why you want to delete this element :') ;
                    ?>
                    <textarea id="reason" class="" rows="2" style="width: 100%" placeholder="Laisser une raison... ou pas (optionnel)"></textarea> -->
            </div>
            <!-- <div class="modal-footer">
                        Utilisation du bouton confirmDeleteElement
            <button id="confirmDeleteElementPreview" data-id="<?php echo $element["_id"]?>" data-type="<?php echo $element["collection"]?>" type="button" class="btn btn-warning"><?php echo Yii::t('common','I confim the delete !');?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
            </div> -->
        </div>

    </div>
</div>
</div>
<script type="text/javascript">
    var typePreview=<?php echo json_encode($element["collection"]); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
    var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$element["collection"]) ); ?>; 
    

    function verifyMail(contextElement){ 
        bootbox.prompt({
                title: " <span class='text-white'>Renseignez votre adresse email</span>",
                message: '<p>Pour vous inscrire à la visio d\'informations et nous permettre de vous recontacter, nous vous invitons à créer un compte.</p>',
                callback: function (result) {
                    if(result){
                        // ajaxPost(
                        //     null,
                        //     baseUrl+`/co2/link/answerwithemail/`,
                        //     {
                        //         id : params.id,
                        //         form : params.form,
                        //         email : result,
                        //         resLocation : params['redirect'] ? params['redirect'] : ""
                        //     }
                        //     , function(res){

                        //     if(res.status && notNull(res.location)){
                        //        window.location.assign(res.location);
                        //        window.location.reload();
                        //     }else{
                        //         toastr.error(res.msg);
                        //     }
                        //     },
                        //     null,
                        //     null,
                        //     {"async": false}
                        // )
                    }
                }
            });

            var emailLastVal = "";
            var lastStat = false;
            var bootboxModal = setInterval(function() {
                if($('.bootbox.modal.fade.bootbox-prompt').is(':visible')) {
                    const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
                    $('.bootbox-input.bootbox-input-text.form-control').on('keyup', delay(function(e) {
                        var self = this;
                        var mailValue=$(this).val();
                        if($(this).val().match(emailRegex) && emailLastVal != $(this).val()) {
                            emailLastVal = $(this).val();
                            lastStat = true;
                            var resVal = false;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : "",
                                    form : "",
                                    email : $(self).val()
                                },
                                function(res){
                                    if(res ) {
                                        lastStat = false;
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);

                                        // && res.duplicated default condition
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" onclick="document.getElementById('email-login').value='`+mailValue+`';">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res.tobeactivated) {
                                            lastStat = false;
                                            msgError = trad.ATemporaryAccountExists;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="document.getElementById('email2').value='`+mailValue+`';">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }else if(!res.duplicated){
                                            msgError = "Créez votre compte en cliquant sur le bouton ci-dessous";
                                            lastStat = false;
                                            var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalRegister" data-toggle="modal" data-dismiss="modal" onclick='document.getElementById("email3").value="`+mailValue+`";document.getElementById("callbackRegister").setAttribute("element",`+JSON.stringify(contextElement)+`);document.getElementById("callbackRegister").dataset.action="connect";document.getElementById("sessionRegister").checked=true;'>
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.registration}
                                            </a>`;
                                            
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $("#modal-preview-coop").css("display","none"); 
			                                $("#modal-preview-coop").html("");
                                            $('.bootbox.modal.fade.bootbox-prompt').hide();
                                        });
                                        resVal = e.which !== 13;
                                    }
                                    //  else if(res && !res.duplicated) {
                                    //     $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                    //     $('.bootbox-form-error').remove();
                                    //     resVal = true
                                    // }
                                },
                                null,
                                null,
                                {
                                    async:false
                                }
                            );
                            return resVal;
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                                return lastStat
                            } else {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                return e.which !== 13;
                            }
                        }
                    }, 350));
                    $('.bootbox-input.bootbox-input-text.form-control').on('blur', function() {
                        var self = this;
                        var mailValue=$(this).val();
                        if($(self).val() == '') {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        } else if($(self).val().match(emailRegex) && emailLastVal != $(self).val()) {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                            emailLastVal = $(self).val();
                            lastStat = true;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : "",
                                    form : "",
                                    email : $(self).val()
                                }
                                , function(res){
                                    if(res ) {
                                        // && res.duplicated default condition
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        lastStat = false;
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" onclick="document.getElementById('email-login').value='`+mailValue+`';">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res.tobeactivated) {
                                            msgError = trad.ATemporaryAccountExists;
                                            lastStat = false;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="document.getElementById('email2').value='`+mailValue+`';">
                                                    <i class="fa fa-envelope"></i>
                                                    Vous figurez parmi les contacts de la Compagnie des Tiers-Lieux. Cliquez sur le bouton pour recevoir un e-mail et ainsi créer/finaliser votre compte.
                                                </a>
                                            `;
                                        }else if(!res.duplicated){
                                            msgError = "Créez votre compte en cliquant sur le bouton ci-dessous";
                                            lastStat = false;
                                            var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalRegister" data-toggle="modal" data-dismiss="modal" onclick='document.getElementById("email3").value="`+mailValue+`";document.getElementById("callbackRegister").setAttribute("element",`+JSON.stringify(contextElement)+`);document.getElementById("callbackRegister").dataset.action="connect";document.getElementById("sessionRegister").checked=true;'>
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.registration}
                                            </a>`;
                                            
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $("#modal-preview-coop").css("display","none"); 
			                                $("#modal-preview-coop").html("");
                                            $('.bootbox.modal.fade.bootbox-prompt').hide();
                                        });
                                    }
                                    //  else if(res && !res.duplicated) {
                                    //     $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                    //     $('.bootbox-form-error').remove();
                                    // }
                                }
                            );
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                            } else
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        }
                    })
                    clearInterval(bootboxModal)
                }
            },200)
    }

    function linkInterested(data){
        var chosenDate = data.data("date") ?? "";
        // mylog.log("link interested : clicked, ",chosenDate,visioDate,extraUrlConnect);
       
        var contextElement={
            chosenDate : chosenDate,
            id : contextData._id.$id,
            interestedLink : contextData.interestedLink,
            collection : contextData.collection,
            category : contextData.category
        };
        var refLink=(typeof contextElement.interestedLink!="undefined") ? contextElement.interestedLink : "";
        var visioDate=(typeof contextElement.chosenDate!="undefined") ? contextElement.chosenDate : "";
        var extraUrlConnect="?visioDate="+visioDate;

        mylog.log("link interested : clicked, ",chosenDate,visioDate,extraUrlConnect);
        // return;
        // contextData.chosenDate=$(this).data("date");
        if(userConnected!=null && typeof userConnected._id!="undefined" && typeof userConnected._id.$id!="undefined"){
            // params2Save={
            //     value:chosenDate,
            //     path: "links.events."+contextData._id.$id+".visioDate",
            //     id : userConnected._id.$id,
            //     collection : userConnected.collection
            // }
            
            links.connectAjax(contextData.collection, contextData._id.$id, userConnected._id.$id, "citoyens", "attendees", ["Intéressé.e"], function(){urlCtrl.loadByHash(location.hash);},extraUrlConnect);
            // mylog.log("params2Save",params2Save);
            
            toastr.success("Intérêt valider")

            // dataHelper.path2Value(params2Save,function(data){

            // });
        }else{
            if(notEmpty(refLink)){
                ajaxPost(
                    null,
                    baseUrl+refLink+extraUrlConnect,
                    {},
                    function(data){},
                    null,
                    null,
                    {async:false}
                );
            }    
            var contextElementStr=JSON.stringify(contextElement);
            verifyMail(contextElementStr);

        }
    }
 

	jQuery(document).ready(function() {
        $(".listsItemsPod").removeClass("text-center");
        $(".listsItemsPod").removeClass("col-xs-12");
        var eltPreview=<?php echo json_encode($element); ?>;
        if(eltPreview.collection == "organizations"){
            getFormation(eltPreview._id.$id, eltPreview.collection, eltPreview.name);
        }
        var session = getSession(eltPreview._id.$id, eltPreview.collection, eltPreview.name) 
        if(session.count == 0){
            $(".view-session").hide()
        }
        $(".startDate").html(moment(moment(contextData.startDate), "DD/MM/YYYY").format("DD/MM/YYYY"));
        $(".endDate").html(moment(moment(contextData.endDate), "DD/MM/YYYY").format("DD/MM/YYYY"));
        inintDescs();
        $("#divMapContent").show(function () {
            afficheMap();
        });
          
		coInterface.bindTooltips();
        coInterface.bindLBHLinks();
        $(".markdown-txt").each(function(){
            descHtml = dataHelper.markdownToHtml($(this).html()); 
            var tmp = document.createElement("DIV");
            tmp.innerHTML = descHtml;
            descText = tmp.textContent || tmp.innerText || "";
            $(this).html(descText);
        });

        $(".interesting-session").off().on("click",function(){
            if(typeof contextData.visioDate!="undefined" && typeof contextData.visioDate.alldates !="undefined" && Object.keys(contextData.visioDate.alldates).length > 1){
                $("#modal-interesting").modal("show");
            }else{
                linkInterested($(this));   
                $("#modal-interesting").modal("hide");
            }
            

        });

        $(".choose-visio-date").off().on("click",function(){
            // var refLink=(typeof contextData.interestedLinked!="undefined") ? contextData.interestedLinked.split("connect")[1] : "";
            linkInterested($(this));

        })

        $(".add-session").off().on('click', function () {
            urlCtrl.closePreview();
            var parent={};
            parent[$(this).data("parent-id")] = {
                id: $(this).data("parent-id"),
                type: $(this).data("parent-type"),
                name: $(this).data("parent-name"),
                img : $(this).data("parent-thumbimg"),
            };
            email = $(this).data("parent-email");
            description = $(this).data("parent-description");
            shortDescription = $(this).data("parent-shortdescription");
            dyFObj.openForm('sessionFormation','afterLoad',{"organizer":parent,"email":email,"shortDescription":shortDescription,"description":description});
        });

        $(".view-session").off().on('click', function () {
            var parentId = $(this).data("parent-id");
            var parentType = $(this).data("parent-type");
            var parentName = $(this).data("parent-name"); 
            if(session.count == 1){ 
                urlCtrl.openPreview("#page.type.events.id."+session.id);
            }else if(session.count > 1){ 
                // urlCtrl.closePreview();  
                $("#modal-preview-coop").css("display","none"); 
                $("#modal-preview-coop").html("");
                //if($(".dashboard-account").is(":visible")) $(".dashboard-account").show();
                var url = new URL(location.href)
                if(url.searchParams.get("h") || url.searchParams.get("hp")){
                    url.searchParams.delete("h")
                    url.searchParams.delete("hp")
                    history.replaceState({}, null, url.href);
                }                       
                localStorage.setItem("organizerSession",JSON.stringify({[parentId]:parentName}));
                //urlCtrl.loadByHash("#listing-sessions?organizer="+parentId)
                urlCtrl.loadByHash("#listing-sessions")
            }
        })
        $(".openSession").off().on('click', function () {
                var formation = $(this).data("id");
                // var  = ';
                var modalDom = $('#session-modal');
                ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + formation, null, function (html) {
                    modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                        modalDom.css('padding', '');
                    });
                    modalDom.find('.modal-body').empty().html(html);
                    modalDom.modal('show');
                })
        });
        
        $(".container-preview .social-share-button").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : [{"type":"facebook"}, {"type":"twitter"} ], "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
        resizeContainer();

        $(".btn-edit-preview").off().on("click",function(){
			// alert("heure");
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
            var typeElem=$(this).data("type");
            var idElem=$(this).data("id");
            var subElem=$(this).data("subtype");
            var ctrlElem=typeObj[typeObj[typeElem].sameAs].ctrl;
            var extTypObj=(typeElem!==subElem) ? subElem : typeElem;
            mylog.log("extTypObjextTypObj",typeElem,subElem,extTypObj);
            if(typeof costum.typeObj[extTypObj] != "undefined" && typeof costum.typeObj[extTypObj].dynFormCostum != "undefined"){
                var editPrev=jQuery.extend(true, {},costum.typeObj[extTypObj].dynFormCostum);
            }
            // editPrev.onload.actions.hide.patternselect = 1;
			// editPrev.onload.actions.hide.similarLinkcustom = 1;
            // // if(costum.isCostumAdmin){
            //     editPrev.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
            //         "onText": trad.yes,
            //         "offText": trad.no,
            //         "onLabel": tradDynForm.public,
            //         "offLabel": tradDynForm.private,
            //         "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
            //         "labelInformation": tradDynForm.explainvisibleevent
            //     })
            // // }  
			mylog.log("editPrev",editPrev);     

            if(typeof editPrev != "undefined"){
                dyFObj.editElement(typeElem,idElem, subElem, editPrev);
            }else{
                dyFObj.editElement(typeElem,idElem, subElem);

            }
                        
        });
        $("#confirmDeleteElementPreview").off().on("click", function(){
            var url = baseUrl+"/"+moduleId+"/element/delete/id/"+contextData.id+"/type/"+contextData.type;
            mylog.log("deleteElement", url);
            var type = $(this).data("type");
            var typeevent = $(this).data("typeevent");
            var reference = $(this).data("reference");
            var source = $(this).data("source");
            var category = $(this).data("category");
            var param = new Object;
            if(costum.slug == "transiter"){
                if(reference){
                    url = baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/reference";
                    param = {
                        id:  $(this).data("id"),
                        type: $(this).data("type"),
                        origin: "costum",
                        sourceKey: "transiter"
                    }
                }else if((category == "formation"|| typeevent == "course") && source){
                    url = baseUrl+"/"+moduleId+"/element/delete/id/"+contextData.id+"/type/"+contextData.type;
                }else if(!reference){
                    url = baseUrl+"/costum/transiter/deletetags"
                    param = {
                        id:  $(this).data("id"),
                        type: $(this).data("type")
                    }
                }

            }
            // param.reason = $("#reason").val();
            ajaxPost(
                null,
                url,
                param,
                function(data){ 
                    if(data.result){
                        toastr.success(data.msg);
                        if(typeof data.status !="undefined"){
                            if (data.status == "deleted"){
                                // $(".btn-close-preview").trigger("click");
                                urlCtrl.closePreview();
                                urlCtrl.loadByHash(location.hash)
                                // urlCtrl.loadByHash("#search"); //envoie l'utilisateur la barre de recherche
                            }					
                            else 
                            urlCtrl.loadByHash("#page.type."+contextData.type+".id."+contextData.id); //Une autre page
                            
                        }else{
                            urlCtrl.closePreview(); 
                            urlCtrl.loadByHash(location.hash)
                        }
                    }else{
                        toastr.error(data.msg); 
                    }
                }
            );
	    });
        $(".btn-delete-element-preview").off().on("click", function(){
            $("#modal-delete-element-preview").modal("show");
        });    


        if($("#session-modal").length==0){
            var modalSessionHtml='<div class="modal fade" id="session-modal">'+
               '<div class="modal-dialog">'+
                '<div class="modal-content">'+
                   ' <div class="modal-header bg-primary-color">'+
                       ' <button type="button" class="close" data-dismiss="modal">'+
                           ' <span aria-hidden="true">&times;</span>'+
                            '<span class="sr-only">Close</span>'+
                        '</button>'+
                    '</div>'+
                    '<div class="modal-body">'+
                   ' </div>'+
                '</div>'+
               '</div>'+
            '</div>';
            $("body").append(modalSessionHtml);
        }  
	});

    function afficheMap(){
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if(notNull(contextData) == "undefined" && typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
        }else{
            var paramsMapContent = {
                container : "divMapContent",
                latLon : contextData.geo,
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };
            var interval = setInterval(function(){
                if($("#divMapContent").is(":visible")){
                    clearInterval(interval);
                    mapAbout = mapObj.init(paramsMapContent);

                    var paramsPointeur = {
                        elt : {
                            id : contextData.id,
                            collection : contextData.type,
                            geo : contextData.geo
                        },
                        center : true
                    };
                    mapAbout.addMarker(paramsPointeur);
                    mapAbout.hideLoader();

                }

            }, 100);

        }
    };
 
	function inintDescs() {
        mylog.log("inintDescs");
        if($("#descriptionAbout").length > 0){            
            var descHtml = "<i>"+trad.notSpecified+"</i>";
            if(typeof contextData.description != "undefined" && contextData.description != ""){
                descHtml = dataHelper.markdownToHtml(contextData.description) ;
            }
            $("#descriptionAbout").html(descHtml);  
            AddReadMoreOther();
        }
    }
    function AddReadMoreOther() {
        mylog.log("readmore");
        var showChar = 300;
        var ellipsestext = "...";
        var moretext = "Lire la suite";
        var lesstext = "Lire moins";
        $('.more').each(function() {
            var content = $(this).html();
            
            var textcontent = $(this).text();
            mylog.log("text description",textcontent);

            if (textcontent.length > showChar) {

                var c = textcontent.substr(0, showChar);
                //var h = content.substr(showChar-1, content.length - showChar);

                var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span style="display:none;" class="morecontent">' + content + '</span>';

                $(this).html(html);
                $(this).append('<a href="" class="morelink less">' + moretext + '</a>');
            }

        });

        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(lesstext);
                $(this).siblings('.morecontent').fadeToggle(100, function() {
                    $(this).prev().fadeToggle(100);
                });

            } else {
                $(this).addClass("less");
                $(this).html(moretext);
                $(this).parent().children('.container').fadeToggle(100, function() {
                    $(this).next().fadeToggle(100);
                });
            }
            //$(this).prev().children().fadeToggle();
            //$(this).parent().prev().prev().fadeToggle(500);
            //$(this).parent().prev().delay(600).fadeToggle(500);

            return false;
        });
    }
    function  getSession(id, type, name){
        var session = {
            count : 0,
            id : "",
            html : ""
        } 
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/element/getalllinks/type/'+type+'/id/'+id,
            {},
            function(data){
                if(data){
                    session.html += `<p class="section-title"> Sessions de formation: </p>
                    <div class="content">
                    <ul> `
                        $.each(data,function(k,e){ 
                            if(typeof e.collection!="undefined" && e.collection=="events"){  
                                session.count++; 
                                session.id = k;  
                                session.html += `<li> <a href="#page.type.events.id.${k}"class="lbh-preview-element text-highlight session content">${e.name} ( Du <span class="date">${moment(params.startDate).locale("fr").format("DD/MM/YYYY") }</span> au <span class="date">${moment(params.endDateDate).locale("fr").format("DD/MM/YYYY") }</span>)</a> </li>`;
                            }
                        })
                    session.html += `</ul> </div>`;
                    $(".list-session").html(session.html) 
                }
            },null,null,{async:false}
        ) 
        return session;
    }
    function getFormation(id, type, name){
       
		var data = { 
			searchType: ["projects"], 
			notSourceKey : true,
            indexStep : 0,
            fields : ["profilThumbImageUrl","profilImageUrl"],
            filters : {
                category : "formation"
            }
            
		};
        data.filters["parent."+id] = {'$exists' : true};
        ajaxPost(
			null,
			baseUrl + '/' + moduleId + "/search/globalautocomplete",
			data,
			function(data) {
                if(Object.keys(data.results).length > 0){
                    var str = '<h2 class="section-title">Formations</h2> <ul>';
                    $.each(data.results, function(kf, vf) {
                        var imgPath = (vf.profilThumbImageUrl)?vf.profilThumbImageUrl:(vf.profilImageUrl ? vf.profilImageUrl : '<?php Yii::app()->getModule( "co2" )->assetsUrl?>/images/thumbnail-default.jpg') ;
                        str += `<li>
                        <a href="#page.type.${vf.collection}.id.${kf}" class="hover-underline lbh-preview-element  " data-toggle="popover" title="${vf.name}">
                            <img class="avatar-formation"  alt="image" src="${imgPath}"> <span class="content">${vf.name}</span>
                        </a></li>`;
                    })
                    str += `<ul>`
                }
                $("#list-formation").html(str);
                coInterface.bindLBHLinks();
			}
		);
    }
</script> 