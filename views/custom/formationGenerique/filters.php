
<style>
	#filters-nav{ 
		position: sticky !important;
	}
	#filters-nav.menuFilters-vertical{
		margin-bottom: 2%;
	}
	.main-container{
		padding-top: 56px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS{
		padding-top: 12px !important;
	}
	.searchObjCSS .dropdown .btn-menu, .searchBar-filters .search-bar, .searchBar-filters .input-group-addon, .btn-primary-ekitia{
		padding: 11px 15px;
		border-radius :  2px !important;
		border: 2px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.searchBar-filters .input-group-addon{
		background-color: var(--primary-color) !important;
		color: white !important;
		padding: 10px 14px !important;
		height: 44px;
	}
	.searchBar-filters .input-group-addon
	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
		width: 320px !important;
	}
	.dropdown-title{
		display: none;
	}
	.thin {
		padding: 1em;
		font-size: 16px;
		text-transform: uppercase;
		width: 100%;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-primary-color-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: var(--primary-color);
		border-radius: 50%;
	}

	.tab-outer{
		justify-content: space-around !important;
	}

	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	.bootbox.modal .modal-body{
		padding: 0px;
	}

	#filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn {
		margin-left: 0px !important;
	}

	.searchObjCSS .dropdown .dropdown-menu{
		border: 2px solid var(--primary-color);
	}
	.dropdown-menu button, 
	#filters-nav .dropdown .dropdown-menu button, 
	#filterContainer .dropdown .dropdown-menu button{
		border-bottom: 2px solid var(--primary-color)!important;
	}
	.searchObjCSS .dropdown .btn-menu.active{
		background: var(--primary-color);
		color: white;
	}

	.bg-primary-color{
        background-color: var(--primary-color) !important;
    }
	.otherStr{
		margin-bottom: 10px;
	}
	.float-label{
		top: 28px !important;
		left: 15px !important;
		padding: 10px;
	}
	.item-inner .parent {
		background-color: #01305e;
		padding: 3px;
		border-radius: 0%;
		border-start-start-radius: 5px;
	}
	.parent {
		background-color: var(--primary-color);
		color: white !important;
		display: inline-block;
		border-radius: 4px;
		font-size: 16px;
	}

	.item-inner .otherStr span{
		padding-bottom : 10px;
	}
	.link-bordered{
		margin-top: 10px;
	}
	.item-text{
		overflow-wrap: break-word;
		white-space: normal;
	}
</style>

<script type="text/javascript">
    var pageApp= "<?php echo @$page?>";
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.trainingQualification != "undefined"){
		trainingQualification = {};
		Object.assign(trainingQualification, costum.lists.trainingQualification);
	}
    paramsFilter = {
	 	container : "#filters-nav",
	 	results : {
	 		scrollableDom : ".cmsbuilder-center-content",
	 		smartGrid : true,
	 		renderView : "directory.cards"
	 	},
        defaults:{
            notSourceKey : true,
			fields : ["email","mobile","link"],
            filters : {

            }
        },
	 	loadEvent : {
	 		default : "scroll"
	 	},
		header : {
			options :{
				left : {
					classes : 'col-xs-12 col-sm-4 col-md-4 no-padding',
					group : {
						count : true
					}
				},
				right : {
					classes : 'col-xs-12 col-sm-8 col-md-8 text-right no-padding',
					group : {
						map : true,
						custom : {
						}				
					}
				}
			}

		},
        filters : {
            text : true,
			// type : {
			// 	view : "dropdownList",
			// 	type : "tags",
			// 	name : "Type",
			// 	event : "tags",
			// 	list : [
			// 		"Organisme de formation",
			// 		"Tiers lieux"
			// 	]
			// }
        }
    }
	if(typeof directory.cardsAnnuaire == 'function') {
		paramsFilter.results.renderView = "directory.cardsAnnuaire"
	}
	paramsFilter['defaults']['filters']['$or'] = {}
	paramsFilter['defaults']['filters']['$or']["source.keys"] = costum.slug;
	paramsFilter['defaults']['filters']['$or']["reference.costum"] = costum.slug;                    
	if(pageApp=="annuaire-organisme-de-formation"){ 
        paramsFilter.defaults.types = [ "organizations"]; 
        paramsFilter['defaults']['filters']['tags'] = {};
        paramsFilter['defaults']['filters']['tags']['$in'] = ["Centre de formation"];
		paramsFilter['header']['options']['right']['group']['custom']['html'] = '<div class="margin-right-20" style="display:inline-flex;">'+
								'<a href="javascript:;" onclick="dyFObj.openForm(\'organismeFormation\')" class="btn pull-left text-white" style="background:var(--primary-color)"'+
									'<i class="fa fa-plus-circle"></i> Ajouter un centre de formation'+
								'</a></div>';
		paramsFilter["filters"]["trainingQualification"] = {
			view : "dropdownList",
			type : "tags",
			name : "Qualification de la formation",
			event : "tags",
			list : trainingQualification
		}
		paramsFilter["defaults"]["sortBy"] = {
			"name": 1
		}
	} 
	if(pageApp=="annuaire-tiers-lieux"){ 
        paramsFilter.defaults.types = [ "organizations"]; 
        paramsFilter['defaults']['filters']['tags'] = {}
        paramsFilter['defaults']['filters']['tags']['$in'] = ["tiersLieux","TiersLieux","Tiers Lieux"];
		paramsFilter['header']['options']['right']['group']['custom']['html'] = '<div class="margin-right-20" style="display:inline-flex;">'+
								'<a href="javascript:;" onclick="dyFObj.openForm(\'tiersLieux\')" class=" bg-primary-color btn pull-left text-black" '+
									'<i class="fa fa-plus-circle"></i> Ajouter un tiers lieux'+
								'</a></div>';
		paramsFilter["defaults"]["sortBy"] = {
			"name": 1
		}
	} 
	if(pageApp == "annuaire-globale" || pageApp == "carto-annuaire"){
        paramsFilter.defaults.types = [ "NGO","LocalBusiness","Group","GovernmentOrganization","Cooperative","citoyens"]; 
		paramsFilter["filters"]["organismeFormation"] = {
			view : "dropdownList",
			type : "tags",
			name : "Type",
			event : "tags",
			list : [
				"Centre de formation"
			]
		}
		paramsFilter["filters"]["tiersLieux"] = {
			view : "dropdownList",
			type : "tags",
			name : "Type",
			event : "tags",
			list : [
				"tiersLieux"
			]
		}
		paramsFilter["filters"]["formateur"] = {
			view : "dropdownList",
			type : "tags",
			name : "Type",
			event : "tags",
			list : [
				"Formateur"
			]
		}
		paramsFilter["defaults"]["sortBy"] = {
			"name": 1
		}

	}
	if(pageApp == "annuaire-formateur"){
        paramsFilter.defaults.types = ["citoyens"];
        paramsFilter['defaults']['filters']['tags'] = {}
        paramsFilter['defaults']['filters']['tags']['$in'] = ["Formateur"]; 
		paramsFilter['header']['options']['right']['group']['add'] = {
			label : "Ajouter un formateur",
			class : "bg-primary-color"
		}
		paramsFilter["defaults"]["sortBy"] = {
			"name": 1
		}

	}
	jQuery(document).ready(function(){
		filterSearch = searchObj.init(paramsFilter);
		$(".bg-primary-color").addClass("text-black");
		// window.addEventListener("resize", () => {
		// 	clearTimeout(window.resizeTimer);
		// 	window.resizeTimer = setTimeout(() => {
		// 		$(".item-text").css("height", "auto");
		// 		var maxHeight = Math.max.apply(null, $(".item-text").map(function() {
		// 			return $(this).outerHeight();
		// 		}).get());
		// 		$(".item-text").css("height", maxHeight);
		// 	}, 200);
		// });
	})
	// setTimeout(function() {
	// 	var maxHeightiteminner = Math.max.apply(null, $(".item-text").map(function() {
	// 		return $(this).outerHeight();
	// 	}).get());
	// 	$(".item-text").css("height", maxHeightiteminner);
	// }, 1500)

</script>