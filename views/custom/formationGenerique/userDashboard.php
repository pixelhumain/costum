<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}
?>
<style>
	#filters-nav{
		position: sticky !important;
		padding: 10px 20px 10px 20px !important;
display:none !important
	}
	.main-container{
		padding-top: 56px !important;
	}
    /*#filters-nav{
        display: none !important;
    }*/
    .banner-outer {
        background-color: #f9f9f9;
        background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
        background-position: bottom right;
        background-repeat: no-repeat;
        background-size: 25% auto;
        height: 250px;
        display:flex;
        align-items: center;
        justify-content: space-around;
    }

	.horizontalList.col-xs-12, .statusFiltersClass{
		display:inline !important;
	}
	.radio{
		display: none;
	}
	.form-horizontal label {
		text-align: left !important;
	}

	.searchObjCSS{
		padding-top: 12px !important;
	}
	.searchObjCSS .dropdown .btn-menu,  .btn-primary-ekitia{
		border-radius :  4px !important;
		border: 2px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.titleSession{
        margin-bottom : 16px;
        padding: 3em;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-content: space-around;
        align-items: center;
    }
    .item-type1 .date{
        color : #01305e
    }
    .btn{
        font-size: medium !important;
        font-weight: bolder;
    }

    .searchBar-filters{
        font-weight: bolder;
        width: 35%;
    }

    .searchBar-filters input.search-bar{
        border-radius: 40px;
        width: 100%;
        box-shadow: none;
        padding: 22px 25px;
        font-size: 16pt !important;
        background-color: #D1D9DE !important;
		border: none;
    }

    .searchBar-filters span.main-search-bar-addon{
        margin-left: -60px;
        padding:0;
        font-size: 18pt !important;
        background-color: #D1D9DE !important;
		border:none;
    }
    .searchBar-filters span.main-search-bar-addon i{
        border-radius: 37px;
        background-color: #01305e;
        padding: 6px 16px;
        color:white;
        margin-top: 4px;
    }
    .pageContent {
         background-color: white;
    }
    .link-bordered a{
        border-radius: 5px;
        background-color: var(--primary-color);
    }
    .link-bordered a.handleSession{
        display: none;
    }
</style>
<style>
	    /* timeline vertical line */

		.bodySearchContainer {
    list-style: none;
    position: relative;
    }
    .bodySearchContainer .direction-right:before {
    top: 0;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 2px;
    left:0%;
    border-left: 2px dashed var(--primary-color);
    margin-left: -1.5px;
    }

    .bodySearchContainer .direction-left:after {
    top: 0;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 2px;
    left:100%;
    border-left: 2px dashed var(--primary-color);
    margin-left: -1.5px;
    }

    .bodySearchContainer .clearFix {
    clear: both;
    height: 0;
    }
    .bodySearchContainer .timeline-badge {
    color: #fff;
    width: 66px;
    height: 64px;
    font-size: 1em;
    text-align: center;
    position: absolute;
    top: 20px;
    left:100%;
    margin-left: -30px;
    background-color: var(--primary-color);
    border-top-right-radius: 50%;
    border-top-left-radius: 5%;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
    z-index: 28;
    padding: 8px;
    }

    .bodySearchContainer .col-sm-offset-6 .timeline-badge{
        left: 0%;
    border-top-right-radius: 5%;
    border-top-left-radius: 50%;
    }



    .bodySearchContainer .timeline-badge span.timeline-balloon-date-day {
    font-size: 1.4em;
    display: block;
    }
    .bodySearchContainer .timeline-badge span.timeline-balloon-date-month {
    font-size: .7em;
    position: relative;
    top: -10px;
    }
    .bodySearchContainer .timeline-badge.timeline-filter-movement {
    background-color: #ffffff;
    font-size: 1.7em;
    height: 35px;
    margin-left: -18px;
    width: 35px;
    top: 40px;
    }
    .bodySearchContainer .timeline-badge.timeline-filter-movement a span {
    color: var(--primary-color);
    font-size: 1.3em;
    top: -1px;
    }
    .bodySearchContainer .timeline-badge.timeline-future-movement {
    background-color: #ffffff;
    height: 35px;
    width: 35px;
    font-size: 1.7em;
    top: -16px;
    margin-left: -18px;
    }
    .bodySearchContainer .timeline-badge.timeline-future-movement a span {
    color: var(--primary-color);
    font-size: .9em;
    top: 2px;
    left: 1px;
    }
    .bodySearchContainer .timeline-movement-top {
    height: 60px;
    }
    .bodySearchContainer .timeline-item {
    padding: 20px 0;
    }
    .bodySearchContainer .timeline-item img{
    border-radius: 10px;
    }
    .bodySearchContainer .timeline-item .timeline-panel {
    border: 1px solid #d4d4d4;
    border-radius: 3px;
    background-color: #FFFFFF;
    color: #666;
    padding: 10px;
    position: relative;
    /*-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);*/
    }
    .bodySearchContainer .timeline-item .timeline-panel .timeline-panel-ul {
    list-style: none;
    padding: 0;
    margin: 0;
    }
    .bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul {
    text-align: right;
    }
    .bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul li {
    color: #666;
    }
    .bodySearchContainer .timeline-item .timeline-panel.credits .timeline-panel-ul li span.importo {
    color: #468c1f;
    font-size: 1.3em;
    }
    .bodySearchContainer .timeline-item .timeline-panel.debits .timeline-panel-ul {
    text-align: left;
    }
    .bodySearchContainer .timeline-item .timeline-panel.debits .timeline-panel-ul span.importo {
    color: #e2001a;
    font-size: 1.3em;
    }
    .timeline-desc{
        text-align: justify;
        font-size: 12pt !important;
        word-wrap: break-word;
        white-space: pre-line;
    }
    .color-ekitia{
        color: var(--primary-color);
    }

	.btn{
		font-weight: bolder;
	}
</style>

<div id="db-banner" class="col-xs-12 titleSession">
    <h2 class="text-center" style="text-transform:capitalize;color:var(--primary-color);font-size:3em">Mes session de formations</h2>
</div>
<div class="col-xs-12" style="color:inherit;">
    <div id='filter-nav' class='searchObjCSS col-xs-12 col-sm-12'>
        <div>
            <a href="#sessions-publics" class="menu-sessions-admin">Espace de gestion des sessions</a>
        </div>
    </div>
    <div class='headerSearchInFormation no-padding col-xs-12 col-sm-12'></div>
    <div id="adminDirectoryFormation" class="col-xs-12"></div>
</div>
<script type="text/javascript">
    var appConfig = <?php echo json_encode(@$appConfig); ?>;

	var defaultFilters = {
        '$or':{
            
        }
    }
    
    // If user is creator
    defaultFilters['$or']["links.attendees."+userId] = {'$exists':true};
    //defaultFilters['$or']["links.attendees."+userId+".roles"] = 'Intéressé.e';
    defaultFilters['$or']["links.organizer."+userId] = {'$exists':true};

    // If user is attendees
    defaultFilters['$or']["creator"] = userId;
	
	var paramsFilter = {
		container : "#filter-nav",
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
		 	types : ["events"],
		 	filters: defaultFilters,
			indexStep: 900,
			sortBy: {"name": 1}
		},
		filters:{
			text:{
				placeholder: "Recherche de session"
			},
			status : {
				view : "dropdownList",
				type : "filters",
				name : "Filtrer par",
				event : "filters",
				field: "links.attendees."+userId+".roles",
				typeList: "object",
				trad:false,
				list : {
					"Intéressé.e":{
						value:"Intéressé.e", 
						label:"Intéressants pour moi"
					}
				}
			}
		},
	 	results : {
			renderView: "directory.cards"
		}
	}

	directory.cards = function(params){
		mylog.log("directory session", "Params", params);
		
		let populated = typeof (params.links && params.links.attendees) != "undefined";
		let isAdmin = populated?false:true;
		let intersted = populated?params.links.attendees:[];
		
		let interstedNumber = 0;
		$.each(intersted, function(i, v){
			if(v.roles && v.roles.includes("Intéressé.e")){
				interstedNumber++;
			}
		});

		let buttonSubscribe = `<a href="javascript:;" class="tooltips followBtn margin-left-5"  
					data-toggle="tooltip" data-placement="left" data-original-title="Inscription"
					data-ownerlink="follow" 
					data-id="${params.id}" 
					data-type="${params.collection}" 
					data-name="${params.name}"
					data-isFollowed="false">
					Je m'inscris
			</a>`;

		let numberTag = `${interstedNumber} interessés / ${populated?Object.keys(params.links.attendees).length:0} inscrits`;
		let str = "";
		let dateAndTags = "";
		
		if(params.collection=="news"){
			dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
		}
		if(params.collection=="events" && params.startDate){
			dateAndTags = '<span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY HH:mm") +'</span>';
		}
		
		let linkedElement = "";
		if(params.parent || params.organizer){
			let p = params.organizer || params.parent;
			linkedElement = '<div style="position:absolute;  max-width:45%;font-weight:bold; top:35px; left: 30px;">'
			$.each(p, function(oi, ov){
				linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="lbh-preview-element padding-5 margin-bottom-5" style="background-color:var(--primary-color); color:white; display:inline-block; border-radius:4px">'+
						ov.name+
					'</a><br/>';
			}),
			linkedElement+="</div>"
		}

		str += '<div class="blog-item actus-item item-type1 col-xl-3 col-md-4 col-sm-6 col-xs-12">'+
			'<div class="item-inner">'+
			'<div class="image">'+
			'<img src="'+(params.profilMediumImageUrl||"https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg")+'" class="img-responsive" alt="" decoding="async">'+
			'</div>'+
			linkedElement+
			((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
			((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+numberTag+'">'+(numberTag)+'</span>':"")+
			'<div class="item-body">'+
			'<div class="top-wrapper">'+
			'<span class="cat-date">'+dateAndTags+'</span>'+
			'<span class="category lowercase">#'+(((params.tags&&params.tags[0])?params.tags[0]:"")||"")+'</span>'+
			'</span>'+
			//'<span class="tag"></span>'+
			'</div>'+
			(params.name?`<h4 class="title">${params.name}</h4>`:"")+
			'<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
			'<div class="link-bordered">'+
			'<a href="'+params.hash+'" class="lbh-preview-element">En savoir plus</a>'+
			(isAdmin?"":buttonSubscribe)+
			'</div>'+
			'</div>'+
			'</div>'+
			'</div>';
		return str;
	}

	directory.timelineEvent = function(v){
        var hash = "";
		let populated = typeof (v.links && v.links.attendees) != "undefined";
		let isAdmin = populated?false:true;
        var resultHtml = "";
        var image = (v.profilMediumImageUrl != undefined) ?  v.profilMediumImageUrl : "https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg";
        var shortDescriptionLink = "(pas de description)";
        if(typeof v.shortDescription !="undefined"){
            if(v.shortDescription.length > 200){
                let short = v.shortDescription.substr(0, 200);
                shortDescriptionLink = short.substr(0, short.lastIndexOf(" ")) + " ...";
            }else{
                shortDescriptionLink = v.shortDescription.replace(/((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g, '<a href="$1" target="_blank" class="color-ekitia">$1</a> ');
            }
        }
        let eTypes = [];
        if(v.collection=="events" && v.type){
            if(typeof v.type == "object"){
                $.each(v.type, function(eIndex, eType){
                    if(typeof tradCategory[eType] != "undefined"){
                        eTypes.push(tradCategory[eType])
                    }else{
                        eTypes.push(eType)
                    }
                })
            }else if(typeof v.type == "string" && typeof tradCategory[v.type]!="undefined"){
                eTypes.push(tradCategory[v.type])
            }else{
                eTypes.push(v.type)
            }
        }
		let buttonSubscribe = `<a href="javascript:;" class="tooltips followBtn margin-left-5 bg-dark text-white padding-15"  
					data-toggle="tooltip" data-placement="left" data-original-title="Inscription"
					data-ownerlink="follow" 
					data-id="${params.id}" 
					data-type="${params.collection}" 
					data-name="${params.name}"
					data-isFollowed="false" style="border:none">
					Je m'inscris
			</a>`;

        resultHtml += `
            <div class="${direction} ${direction==""?"direction-left":"direction-right"} col-sm-6 timeline-item">
                <div class="timeline-badge">
                    <span class="timeline-balloon-date">${moment(v.startDate).local().locale('fr').format('DD')}</span>
                    <small class="timeline-balloon-date">${moment(v.startDate).local().locale('fr').format('MMM')}</small>
                    <span class="timeline-balloon-date-year">${moment(v.startDate).local().locale('fr').format('YYYY')}</span>
                </div>
                <div class="row no-padding">
                    <div class="${(direction=="")?"col-sm-offset-1 col-sm-10":"col-sm-offset-1 col-sm-10"}">
                        <div class="timeline-panel row">
                            <div class="timeline-panel-ul col-sm-8">
                                <h4 class="color-ekitia">${v.name}</h4>
                                <div>
                                    <span class="text-muted margin-right-10"><i class="glyphicon glyphicon-time"></i> ${moment(v.startDate).local().locale('fr').format('HH:mm')}</span>
                                    <span class="text-muted"><i class="glyphicon glyphicon-tag"></i> ${eTypes.toString()}</span>
                                </div>
                                <p class="timeline-desc markdown">${dataHelper.markdownToHtml(shortDescriptionLink)}</p>
                                <div class="link-bordered">
                                    <a href="#page.type.events.id.${v._id.$id}" class="lbh-preview-element">En savoir plus</a>
									${(isAdmin?"":buttonSubscribe)}
								</div>
                            </div>
                            <div class="col-sm-4">
                                <img class="img-responsive" src="${image}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        resultHtml+=""
        direction = (direction == "") ? "col-sm-offset-6" : "";
        return resultHtml;
    }

	jQuery(document).ready(function(){
		searchObj.results.smartCallBack = function(fObj,timeOut, last){
			mylog.log($(".smartgrid-slide-element img.isLoading").length, "length img not loaded", $(".smartgrid-slide-element.last-item-loading").length);
			if($(".smartgrid-slide-element img.isLoading").length <= 0){
				fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element"});
			}else{
				setTimeout(function(){
					fObj.results.smartCallBack(fObj, (timeout+100));
				}, (timeout+100));
			}
			
			if(Object.keys(fObj.results.data).length==0){
				$("#subTitleDash").text("Vous n'avez pas encore participé / créé une formation")
			}
		}

		filterSearch = searchObj.init(paramsFilter);
		$(".headerSearchContainer").remove();
        $(".searchBar-filters").insertAfter(".titleSession h2");
        $(".titleSession").attr("id", "filterFormation");
        if($(".searchBar-filters").length>1){
            $(".searchBar-filters")[1].remove();
        }

		$(`<a href="javascript:;" id="btn-switcher" class="pull-right btn-primary-ekitia btn-primary-outline-ekitia cosDyn-userProfil" data-toggle="dropdown" data-switch="tml" aria-expanded="false"><i class='fa fa-calendar'></i> Vue Timeline</a>`).insertBefore("#activeFilters")
        $("#btn-switcher").on("click", function(){
			if($(this).data("switch")=="tml"){
                $(this).data("switch", "card");
                $(this).html("<i class='fa fa-th-list'></i> Vue Vignette");
                direction = "";
                filterSearch.results.renderView = "directory.timelineEvent";
                filterSearch.search.init(filterSearch);
            }else{
                $(this).data("switch", "tml");
                $(this).html("<i class='fa fa-calendar'></i> Vue Timeline");
                filterSearch.results.renderView = "directory.cards";
                filterSearch.search.init(filterSearch);
            }
		})
	});

</script>