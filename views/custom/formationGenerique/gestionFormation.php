<?php 
// var_dump($page);exit;
    $title="";
    if($page=='listing-formations'){
        // $totle=
        $formSpec="formation";
        $labelFormSpec="formation";
        $title="Listing des formations";
        $colorCode="#004e87";
        $colorClass="nightblue";
        $deleteLabel="Supprimer la formation";
        $editLabel="Editer la formation";
    }else{
        $formSpec="sessionFormation";
        $labelFormSpec="session de formation";
        $title="Listing des sessions de formation";
        $colorCode="orange !important";
        $colorClass="orange";
        $deleteLabel="Supprimer la session";
        $editLabel="Editer la session";
    } 
?>
<?php 

    $cssAnsScriptFilesModule = array(
        '/js/admin/panel.js',
        '/js/admin/admin_directory.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl() );
    $adminConstruct=$this->appConfig["adminPanel"];
	if(isset($this->costum)){
		$slugContext=isset($this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"] ;
		$cssJsCostum=array();
		// if(isset($adminConstruct["js"])){
			// $jsCostum=($adminConstruct["js"]===true) ? "admin.js" : $adminConstruct["js"];
			array_push($cssJsCostum, '/js/formationGenerique/formationGenerique_admin.js');
		// }
		// if(isset($adminConstruct["css"]))
			array_push($cssJsCostum, '/css/formationGenerique/formationGenerique_index.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}
	$logo = (@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";
    $primaryColor = "#ba1c24";
        $secondColor = "#F8B030";
    
    if(isset($this->costum["css"]["color"]['color1'])){
        $primaryColor = $this->costum["css"]["color"]['color1'];
    }
    if(isset($this->costum["css"]["color"]['color2'])){
        $secondColor = $this->costum["css"]["color"]['color2'];
    }
?>
<style id="formation"> 
    /** Directory - Card style */
    .item-type1 {
        padding: 27px 15px;
        display: -webkit-box;
        display: -ms-flexbox;
        display: -moz-flex;
        display: flex;
    }

    .item-type1 .item-inner {
        width: 100%;
        -webkit-box-shadow: 0 15px 35px rgba(0, 0, 0, 0.12);
        box-shadow: 0 15px 35px rgba(0, 0, 0, 0.12);
        border-radius: 5px;
        padding-top: 5px;
    }

    .item-type1.blog-item .image {
        display: -webkit-box;
        display: -ms-flexbox;
        display: -moz-flex;
        display: flex;
        -o-justify-content: center;
        -ms-justify-content: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -ms-align-items: center;
        -o-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .item-type1.blog-item .image img {
        height: 180px !important;
        object-fit: contain;
    }

    .item-type1 .item-inner .item-body {
        padding: 20px 20px 20px;
    }

    .item-type1 .top-wrapper {
        display: -webkit-box;
        display: -ms-flexbox;
        display: -moz-flex;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -o-justify-content: space-between;
        -ms-justify-content: space-between;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .item-type1 .desc {
        margin-bottom: 8px;
    }

    .item-type1 .link-bordered a {
        padding-left: 15px;
        margin-bottom: 5px;
        padding-right: 15px;
        font-size: 1.4rem;
    }

    @media screen and (min-width: 640px) {
        .link-bordered a {
            padding: 15px 30px;
            font-size: 1.5rem;
        }
    }


    a:hover{
        text-decoration: none ;
    }

    a{
        cursor: pointer;
    }

    .bg-dark{
        background-color: #17425B !important;
    }

    .bg-primary-color{
        background-color: var(--primary-color) !important;
    }

    .text-primary-color{
        color: var(--primary-color) !important;
    }

    .text-bold{
        font-weight: bolder;
    }

    .link-bordered a {
        padding: 13px 24px;
        font-family: "Poppins", sans-serif;
        color: white;
        background-color: transparent;
        text-decoration: none;
        border: 2px solid var(--primary-color);
        -webkit-transition: all 0.25s ease-in-out;
        transition: all 0.25s ease-in-out;
        cursor: pointer;
        letter-spacing: .5px;
        line-height: 1;
        display: inline-block;
        border-radius: 0;
        font-size: 1.5rem;
    }
    span.category {
        text-transform: uppercase;
        color: var(--primary-color);
        font-weight: bold;
    }
   .parent {
        background-color:var(--primary-color); 
        color:white !important;
        display: inline-block; 
        border-radius:4px;
        font-size : 16px;
    }
    .titleSession{
        margin-bottom : 16px;
        padding: 1em;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        align-content: space-around;
        align-items: center;
    }
    .titleSession .text-<?= $colorClass ?>{
        text-transform:none;font-size:30px;
    }
    .item-type1 .date{
        color : #01305e
    }

    .btn{
        font-size: medium !important;
        font-weight: bolder;
    }

    .searchBar-filters{
        font-weight: bolder;
        width: 40%;
    }

    .searchBar-filters input.search-bar{
        border-radius: 40px;
        width: 100%;
        box-shadow: none;
        padding: 22px 25px;
        font-size: 16pt !important;
        background-color: #D1D9DE !important;
    }

    .searchBar-filters span.main-search-bar-addon{
        margin-left: -60px;
        padding:0;
        font-size: 18pt !important;
        background-color: #D1D9DE !important;
    }
    .searchBar-filters span.main-search-bar-addon i{
        border-radius: 37px;
        background-color: #01305e;
        padding: 6px 16px;
        color:white;
        margin-top: 4px;
    }
    .pageContent {
         background-color: white;
    }
    .link-bordered a{
        border-radius: 5px;
        background-color: var(--primary-color);
    }
    /* .link-bordered a.handleSession{
        display: none;
    } */

    .item-inner .parent{
        background-color:#01305e;
        padding: 3px;
        border-radius: 0%;
    }
    .item-inner .parent:first-child{
        border-start-start-radius: 5px;
    }
    .rounded-3{
        border-radius: 10px;
    }

    .float-label{
        top: 28px !important;
        left: 15px !important;
        padding: 10px;
    }

    .item-inner .otherStr span{
        padding-bottom : 10px;
    }
    .link-bordered{
        margin-top: 10px;
    }
    .item-text{
        overflow-wrap: break-word;
        white-space: normal;
    }
</style>
<div class="col-xs-12 titleSession">
    <h2 class="text-center" style="text-transform:capitalize;color:var(--primary-color);font-size:3em"><?=strtolower($title)?></h2>
</div>
<div class="col-xs-12" style="color:inherit;">
    <div id='filterFormation' class='searchObjCSS col-xs-12 col-sm-12'>
        <div>
            <a href="#sessions-publics" class="menu-sessions-admin">Espace de gestion des sessions</a>
        </div>
    </div>
    <div class='headerSearchInFormation no-padding col-xs-12 col-sm-12'></div>
    <!-- <div class='bodySearchContainer margin-top-10'>
        <div class='no-padding col-xs-12' id='dropdown_search'></div> 
        <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div> 
    </div> -->
    <div id="adminDirectoryFormation" class="col-xs-12"></div>
</div>

<script type="text/javascript">

    function handleSession(id){ 
        mylog.log("handlesession");
        var formation = $(this).data("id");
        var modalDom = $('#session-modal');
        ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + id, null, function (html) {
            modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                modalDom.css('padding', '');
            });
            modalDom.find('.modal-body').empty().html(html);
            modalDom.modal('show');
        });
        //  e.stopImmediatePropagation();
    }
    /* Déclaration couleur principal */
    var primaryColor = "#ba1c24";
    var secondColor = "#F8B030";
    if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined"&& typeof costum.css.color.color1 !="undefined"){
        primaryColor = costum.css.color["color1"]; 
    }
    if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined"&& typeof costum.css.color.color2 !="undefined"){
        secondColor = costum.css.color["color2"];
    }
    stylecolor = ":root{ --primary-color: "+primaryColor+"; --secondary-color:"+secondColor+" }"
    $("#formation").append(stylecolor);

    var currentPage="<?= $page ?>";
    var formSpec="";
    var labelFormSpec="";
    var labelFormSpecPluri="";
    var colorCode="";
    var colorClass="";
    var deleteLabel="";
    var editLabel="";
    var otherPage="";
    var typeAutocpl="";
    var fitersSessionId="";
    var filtersValue = {};
    var fitersSessionName="";
    var fitersSessionStorage = JSON.parse(localStorage.getItem("organizerSession"));
    if(localStorage.getItem("organizerSession") != null){
        $.each(fitersSessionStorage,function(key,value){
            fitersSessionId = key;
            fitersSessionName = value;
        });
    }
    if(currentPage=='listing-formations'){
        formSpec="formation";
        typeAutocpl="projects";
        labelFormSpec="formation";
        labelFormSpecPluri="formations";
        otherLabelFormSpecPluri="sessions de formation";
        colorCode="#004e87";
        colorClass="nightblue";
        otherPage="listing-sessions";
        filtersValue = {
            "category": "formation"
        }
        otherColorClass="orange";
    }else{
        formSpec="sessionFormation";
        labelFormSpec="session de formation";
        typeAutocpl="events";
        labelFormSpecPluri="sessions de formation";
        otherLabelFormSpecPluri="formations";
        colorCode="orange";
        colorClass="orange";
        otherPage="listing-formations";
        otherColorClass="nightblue";
        filtersValue = {
            "type": "course",
        }
    }   

    adminDirectory.values.organizer=function(e,id,type,aObj){
        mylog.log("values organizer",e, id, type, aObj);
        str="";
        if(typeof e.organizer!="undefined" && Object.keys(e.organizer).length>0){
            
            $.each(e.organizer,function(id,e){
                if(str!==""){
                    str+="</br>";
                }
                str+=str+="<a href='#page.type."+e.type+".id."+id+"' style='display:inline-block' class='lbh-preview-element'>"+e.name+"</a>";

            });
        }
        return str;
        
    };
    adminDirectory.values.handleSession=function(e,id,type,aObj){
        mylog.log("values handleSession",e, id, type, aObj);
        str='<button class="btn pull-left margin-left-20 bg-primary-color text-black handleSession" data-id="'+id+'"fa fa-cog"></i> Gérer cette session</button>';
        // if(typeof e.organizer!="undefined" && Object.keys(e.organizer).length>0){
            
        //     $.each(e.organizer,function(id,e){
        //         if(str!==""){
        //             str+="</br>";
        //         }
        //         str+=str+="<a href='#page.type."+e.type+".id."+id+"' style='display:inline-block' class='lbh-preview-element'>"+e.name+"</a>";

        //     });
        // }
        return str;
        
    };   

    adminDirectory.values.dates=function(e,id,type,aObj){
        mylog.log("values dates",e, id, type, aObj);
        str='Du '+moment(e.startDate.sec*1000).format("DD/MM/YYYY à HH:mm")+' au '+moment(e.endDate.sec*1000).format("DD/MM/YYYY à HH:mm");

        return str;
        
    };  

    adminDirectory.values.session=function(e,id,type,aObj){
        mylog.log("values session",e, id, type, aObj);
        var strR="";
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+'/element/getalllinks/type/'+type+'/id/'+id,
            {},
            function(data){
                var str='Aucune session associée</br>'+
                    '<a href="javascript:;" data-type="'+type+'" data-id="'+id+'" data-name="'+e.name+'"  class="add-session bg-dark btn pull-left text-black" '+
                        '<i class="fa fa-plus-circle"></i> Ajouter une nouvelle session de formation'+
                    '</a>';
                if(typeof data!="undefined"){
                    var prevCounter=0;
                    // str="";
                    $.each(data,function(k,e){
                        var toggleClass ="";
                        if(typeof e.collection!="undefined" && e.collection=="events"){
                            if(moment(e.endDate) < moment()){
                                toggleClass="hidden";
                                prevCounter++;
                            }
                            if(!str.includes("sessionLinks")){
                                str="";
                            }
                            str+="<a href='#page.type."+e.collection+".id."+k+"' style='display:grid' class='sessionLinks lbh-preview-element "+toggleClass+"'>"+e.name+"</a>";
                        }   
                    });
                    if(prevCounter>0){
                    str+="<a href='javascript:;' style='padding:3px;font-size:10px' class='btn btn-default showPreviousSession'>Voir les sessions précédentes</a>"
                    }
                }  
                strR=str;  

            },
            null,
            null,
            {async : false}
        );
        return strR;
        
    }   

    adminDirectory.bindCostum=function(aObj){
        mylog.log("adminDirectory.events.session", aObj);
        $("#"+aObj.container+" .showPreviousSession").off().on("click",function(){
            $(this).siblings(".hidden").removeClass("hidden");
            $(this).hide();
        });
        $("#"+aObj.container+" .handleSession").off().on("click",function(e){
        
                    // var  = ';
                    mylog.log("handlesession");
                    var formation = $(this).data("id");
                    var modalDom = $('#session-modal');
                    ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + formation, null, function (html) {
                        modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                            modalDom.css('padding', '');
                        });
                        // modalDom.find('.modal-header').prepend('<h4 style="display: inline-block;margin: 0;">Gestion de la session de formation</h4>');
                        modalDom.find('.modal-body').empty().html(html);
                        modalDom.modal('show');
                    });
                    e.stopImmediatePropagation();


        });
        $("#"+aObj.container+" .add-session").off().on("click",function(){
            var dataOrga={};
                dataOrga.organizer={};
                dataOrga.organizer[$(this).data("id")]={
                    type : $(this).data("type"),
                    name : $(this).data("name")
                };
            // var dataOrga=$(this).data("organizer");
            mylog.log("dataOrga",dataOrga);
            dyFObj.openForm(`sessionFormation`,`afterLoad`,dataOrga);
        });
    };


    var data={
        paramsFilter : { 
            container : "headerSearchInFormation",
			defaults : {
				types : [formSpec],
				sortBy: {"name": 1}
			} 
		},
		table : {
            name : {
                name : "Nom de la "+labelFormSpec,
                preview : true
            }
            // shortDescription : {
            //     name : "Description courte"
            // }            

        }
        // actions : {
        //     update : true,
        // 	delete : true
        // }
    };   
    if(currentPage=="listing-formations"){ 
        data.table.session ={
            name : "Sessions de formation"
        };
        data.table.shortDescription = {
                name : "Description courte"
        }; 
        
    }else{
        data.table.dates ={
            name : "Période"
        };
        data.table.organizer ={
            name : "Formation rattachée"
        };
        data.table.handleSession ={
            name : "Gérer"
        };

    }    
    var paramsFilter = {
        container : "#filterFormation",
        header : {
            dom : ".headerSearchInFormation",
            options : {
                left : {
                    classes : 'col-xs-12 col-sm-4 col-md-4 no-padding',
                    group : {
                        count : true
                    }
                },
                right : {
                    classes : 'col-xs-12 col-sm-8 col-md-8 text-right no-padding',
                    group : {  
                        // switchView : {
                        //     grid : true,
                        //     table : {
                        //         options : {
                        //             container : "adminDirectoryFormation",
                        //             directory : {},
                        // 			results : {},
                        // 			initType : [typeAutocpl],
                        // 			panelAdmin : data
        		        //         }
                        //     }
                        // },
                        custom : {
                            html :'<div class="margin-right-20" style="display:inline-flex;margin-top:8px;">'+
                            '<a href="javascript:;" onclick="dyFObj.openForm(`'+formSpec+'`)" style="background-color:#004e87;" class="bg-nightblue btn pull-left text-white padding-right-15 padding-left-15 padding-top-10 padding-bottom-10 rounded-3" '+
                                '<i class="fa fa-plus-circle"></i> Ajouter une nouvelle '+labelFormSpec+' <span class="fa fa-plus-square-o text-primary-color margin-left-10"></span>'+
                            '</a>'+
                            '<a href="#'+otherPage+'" style="margin-left:15px;" class="bg-primary-color text-white lbh btn menu-sessions-admin padding-right-15 padding-left-15 padding-top-10 padding-bottom-10 rounded-3">Listing des '+otherLabelFormSpecPluri+' <span class="fa fa-list text-dark margin-left-10"></span> </a>'+
                            '</div>'
                            // events : function(){alert("eventcustom")}
                        }    
                    }
                }
            }
        },
        defaults : {
            types : [ typeAutocpl ],
            fields : [ "name", "organizer", "parent", "collection", "email", "tags", "shortDescription", "links","codeCARIF" ],
            filters : filtersValue
        },
        filters : {
            text : true
        },
        results : {
            dom : "#adminDirectoryFormation",
            renderView: "directory.cards",
            smartGrid : true
        }
        // loadEvent : {
        // 		default : "admin",
        // 		options : {
        //             container : "adminDirectoryFormation",
        //             directory : {},
        // 			results : {},
        // 			initType : [typeAutocpl],
        // 			panelAdmin : data
        // 		}
        // }
    };

    if(typeof directory.cardsFormation == 'function') {
        paramsFilter.results.renderView = "directory.cardsFormation"
    }
    if(fitersSessionId != "" && typeAutocpl == "events"){
        paramsFilter.defaults.filters["links.organizer."+fitersSessionId]={"$exists":1};
        localStorage.removeItem("organizerSession");
    }
	
    // paramsFilter.defaults.filters["parent."+costum.contextId]={"$exists":1};
    // var paramsFilter = contactCie.getParams(key,view);			

    jQuery(document).ready(function() {
        if(fitersSessionName != "" && typeAutocpl == "events"){ 
            $(".titleSession h2").text("Listing des sessions de formation \""+fitersSessionName+"\"");
        }
        if(typeof costum[costum.slug]=="undefined"){
            costum[costum.slug]={};
        }
        if(typeof costum[costum.slug].sessionFormation=="undefined"){
            costum[costum.slug].sessionFormation = {
                afterSave : function(data){
                    dyFObj.commonAfterSave(data, function() {
                        mylog.log("aftersave date events sessionFormation", data);
                        ajaxPost(
                            null, 
                            baseUrl + '/costum/formationgenerique/aftersave/type/sessionFormation', 
                            {data : data.map}, 
                            function () {}
                        );
                        urlCtrl.loadByHash(location.hash);
                        var modalDom = $('#session-modal');
                        ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + data.id, null, function (html) {
                            modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                                modalDom.css('padding', '');
                            });
                            // modalDom.find('.modal-header').prepend('<h4 style="display: inline-block;margin: 0;">Gestion de la session de formation</h4>');
                            modalDom.find('.modal-body').empty().html(html);
                            modalDom.modal('show');
                        });
                    })
                },
                afterBuild : function(data){
                    mylog.log("afterbuild data sessionFormation",data);
                    $("#ajaxFormModal .visioDatecodate label[for=eventDurationcodate-visioDate]").text("Durée (en heure) des visios d'information relatives à cette session");
                    $("#ajaxFormModal .removeDateBlock").parent().css({"position": "absolute","left":"-10px","top": "2px"});
                }
            };
        }

        if(typeof costum[costum.slug].formation=="undefined"){
            costum[costum.slug].formation = {
                afterSave : function(data){
                    dyFObj.commonAfterSave(data, function() {
                        mylog.log("aftersave formation", data);
                        ajaxPost(
                            null, 
                            baseUrl + '/costum/formationgenerique/aftersave/type/formation', 
                            {data : data.map}, 
                            function () {}
                        );
                        urlCtrl.loadByHash(location.hash);
                    })
                }
            };
        }

        if($("#session-modal").length==0){
            var modalSessionHtml='<div class="modal fade" id="session-modal">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content col-xs-12 col-lg-8 col-md-10 no-padding">'+
                ' <div class="modal-header">'+
                    '<div style="display: inline-block;margin: 0;font-size: 22px;font-style: italic;">Gestion de la session</div>'+
                    '<a id="mini-info" href="javascript:;">   <i style="font-size:25px;" class="fa fa-info-circle"></i>'+
                    ' <button type="button" class="close" data-dismiss="modal">'+
                        ' <span aria-hidden="true">&times;</span>'+
                            '<span class="sr-only">Close</span>'+
                        '</button>'+
                    '</div>'+
                    '<div class="modal-body">'+
                ' </div>'+
                '</div>'+
            '</div>'+
            '</div>';
            $("body").append(modalSessionHtml);
        }
        
        adminDirectory.initView = function(){
            mylog.log("adminDirectory.initView");
            var aObj = this;
            var str = "";
            if(typeof aObj.panelAdmin.title != "undefined" && aObj.panelAdmin.title != null)
                str += '<div class="col-xs-12 padding-10 text-center"><h2>'+aObj.panelAdmin.title+'</h2></div>';

                str += '<div class="headerSearchContainerAdmin no-padding col-xs-12"></div>';
                str += '<div class="no-padding col-xs-12 searchObjCSS" id="filters-nav-admin"></div>';
            str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
            
            str +=	'<div class="panel-body">'+
                '<div>'+
                    '<table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin">'+
                        '<thead>'+
                            '<tr id="headerTable">'+
                            '</tr>'+
                        '</thead>'+
                        // '<tbody class="directoryLines">'+
                            
                        // '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div>'+
            '<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';

            $("#"+this.container).html(str);
        };
        adminDirectory.initViewTable = function(aObj){
            //var aObj = this;
            mylog.log("adminDirectory.initViewTable", aObj.container);
            $("#"+aObj.container+" #panelAdmin thead").nextAll().remove();
            mylog.log("adminDirectory.initViewTable !", aObj.results);
            var sumColumn="";
            var posSumColumn = 0;
            var sum=null;
            var tot="";
            var totalColumn=0;

            //vérifie si une entrée du tableau contribut l'attribut sum pour faire la somme
            $.each(aObj.panelAdmin.table, function(k, v){
                mylog.log("yiyi",Object.keys(v));
                if($.inArray("sum",Object.keys(v))>-1){
                    sumColumn=k;
                    posSumColumn=Object.keys(aObj.panelAdmin.table).indexOf(k);
                }
            });

            if(Object.keys(aObj.results).length > 0){
                $.each(aObj.results ,function(key, values){
                    mylog.log("adminDirectory.initViewTable !!! ", key, values);
                    var entry = aObj.buildDirectoryLine( values, values.collection);
                    // var selector=(entry.match(/tbody/).length>0) ? "thead tbody": "thead"; 
                    $("#"+aObj.container+" #panelAdmin").append(entry);
                    // Addition des valeurs de la colonne
                    if(sumColumn){
                        if(typeof values[sumColumn]!="Number")
                            sum = Number(values[sumColumn]);
                        totalColumn = totalColumn + sum ;
                    }	
                });	
                //Ligne supplémentaire pour faire la somme
                if(sumColumn){
                    tot += '<tr style="height: 20px;"></tr>';
                    tot += '<tr style="border-top:solid;border-bottom:solid;"><td style="border-left:none !important;border-right:none !important;font-weight:800;">TOTAL</td</tr>';


                    for(var p=0;p<posSumColumn-1;p++){
                        tot+='<td style="border-left:none !important;border-right:none !important;"></td>';
                    }
                    tot += '<td style="border-left:none !important;border-right:none !important;">'+totalColumn+'</td>';
                    $("#"+aObj.container+" #panelAdmin .directoryLines").append(tot);
                }		
            }	

            aObj.bindAdminBtnEvents(aObj);
        };
        adminDirectory.buildDirectoryLine = function( e, collection, icon ){
            mylog.log("adminDirectory.buildDirectoryLine",  e, collection, icon);
            var aObj = this;
            var strHTML="";
            if( typeof e._id == "undefined" || 
                ( (typeof e.name == "undefined" || e.name == "") && 
                (e.text == "undefined" || e.text == "") ) )
                return strHTML;
            var actions = "";
            var classes = "";
            var id = e._id.$id;
            var status=[];
            strHTML += '<tbody><tr id="'+e.collection+id+'" class="'+e.collection+' line">';
                strHTML += aObj.columTable(e, id, e.collection);
                var actionsStr = aObj.actions.init(e, id, e.collection, aObj);
                mylog.log("adminDirectory.buildDirectoryLine actionsStr",  actionsStr);
                if(actionsStr != ""){
                    aObj.actionsStr = true;
                    mylog.log("adminDirectory.buildDirectoryLine aObj.actionsStr",  aObj.actionsStr);
                    strHTML += 	'<td class="center">'+ 
                                    '<div class="btn-group">'+
                                    actionsStr +
                                    '</div>'+
                                '</td>';
                }
            strHTML += '</tr></tbody>';
            return strHTML;
        };
        // adminPanel.views.projects();
        filterFormation = searchObj.init(paramsFilter);
        $(".searchBar-filters").insertAfter(".titleSession h2");
        $(".titleSession").attr("id", "filterFormation");
        if($(".searchBar-filters").length>1){
            $(".searchBar-filters")[1].remove();
        }
        setTimeout(function() {
            var maxHeight = Math.max.apply(null, $(".item-text").map(function() {
                return $(this).outerHeight();
            }).get());
            $(".item-text").css("height", maxHeight);
        }, 1500) 
		window.addEventListener("resize", () => {
			clearTimeout(window.resizeTimer);
			window.resizeTimer = setTimeout(() => {
				$(".item-text").css("height", "auto");
				var maxHeight = Math.max.apply(null, $(".item-text").map(function() {
					return $(this).outerHeight();
				}).get());
				$(".item-text").css("height", maxHeight);
			}, 200);
		});
        // filterFormation.container="adminDirectoryFormation";
        filterFormation.admin.event=function(fObj){
            mylog.log("gestion formation searchObj.admin.event");
                if(fObj.search.obj.indexStep > 0){
                    if(fObj.results.numberOfResults%fObj.search.obj.indexStep == 0){
                        var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep);
                    }else{
                        var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep)+1;
                    }
                    $('.pageTable').pagination({
                        items: numberPage,
                        itemOnPage: fObj.search.obj.indexStep,
                        currentPage: (fObj.search.obj.nbPage+1),
                        hrefTextPrefix:"?page=",
                        cssStyle: 'light-theme',
                        prevText: '<<',
                        nextText: '>>',
                        onInit: function () {

                        },
                        onPageClick: function (page, evt) {
                            coInterface.simpleScroll(0, 10);
                            fObj.search.obj.nbPage=(page-1);
                            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
                            fObj.search.start(fObj);
                        }
                    });
                }
        };
        filterFormation.search.init(filterFormation);
        directory.cards = function(params){
            mylog.log("directory actus", "Params", params);
            var str = "";
            var dateAndTags = "";
            if(params.collection=="news"){
                dateAndTags = '<span class="date">'+(params.created?formatDate(params.created):formatDate(params.date))+(params.tags?" | ":"")+'</span>';
            }
            if(params.collection=="events"){
                dateAndTags = 'Du <span class="date">'+moment(params.startDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span> au <span class="date">'+moment(params.endDateDate).locale("fr").format("DD/MM/YYYY à HH:mm") +'</span>';
            }
            var otherStr = "";
    
            let linkedElement = "";
            if(params.parent || params.organizer){
                let p = params.organizer || params.parent;
                linkedElement = '<div style="position:absolute; max-width:45%;font-weight:bold;" class="float-label">'
                $.each(p, function(oi, ov){
                    linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent lbh-preview-element padding-5 margin-bottom-5" >'+
                            ov.name+
                        '</a><br/>';
                }),
                linkedElement+="</div>"
            }
            if(params.organizer && costum.slug == "transiter"){
                $.each(params.organizer, function(ko, vo){
                    ajaxPost(
                        '', 
                        baseUrl + "/co2/element/get/type/"+vo.type+"/id/" + ko,
                        null,
                        function (data) {
                            if(typeof data.map != 'undefined' && typeof data.map.parent != 'undefined'){
                                linkedElement = '<div style="position:absolute; max-width:45%;font-weight:bold;" class="float-label">'
                                $.each(data.map.parent, function (oi, ov) {
                                    linkedElement += '<a href="#page.type.'+ov.type+'.id.'+oi+'" class="parent lbh-preview-element padding-5 margin-bottom-5" >'+
                                        ov.name+
                                    '</a><br/>';
                                })
                                linkedElement+="</div>";  
                            }else{
                                linkedElement = "";
                            }
                        },null,"json",{async : false}
                    )
                })

            }
            if(params.collection=="citoyens"){
                otherStr = `<div class="otherStr">
                <span><i class="fa fa-envelope-o"></i>  ${params.email}</span>`;
                if(typeof params.mobile !="undefined"){
                    otherStr += `</br><span> <i class="fa fa-phone"></i>  ${params.mobile}</span>`;
                }
                if(typeof params.thematic !="undefined"){
                    otherStr += `</br><span><i class="fa fa-link"></i>  ${params.thematic}</span><br>`;
                }
                if(typeof params.link !="undefined"){
                    otherStr += `</br><span><a href="${params.link}" targrt="_blank" > <i class="fa fa-link"></i> Site web</a></span><br>`;
                }
                otherStr += "</div>";
            }
            str += '<div class="blog-item actus-item item-type1 col-xl-2 col-lg-2 col-md-3 col-sm-6 col-xs-12">'+
                '<div class="item-inner">'+
                    '<div class="image">'+
                        '<img src="'+(params.profilMediumImageUrl||"<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg")+'" class="img-responsive" alt="" decoding="async">'+
                    '</div>'+
                    linkedElement+
                    ((params.toolLevel)?'<span class="padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(params.toolLevel||"")+'">'+(params.toolLevel||"")+'</span>':"")+
                    //((params.collection=="events" && params.type)?'<span class="uppercase padding-10" style="position:absolute; background-color:#ddd; font-weight:bold; max-width:45%; top:35px; right: 30px; border-radius:4px" title="'+(tradCategory[params.type]||"")+'">'+(tradCategory[params.type]||"")+'</span>':"")+
                    '<div class="item-body">'+
                        '<div class="item-text">'+
                            (params.name?`<h4 class="title">${params.name}</h4>`:"")+
                            '<div class="top-wrapper">'+
                                '<span class="cat-date">'+dateAndTags+'</span>'+
                                // '<span class="category lowercase">#'+(((params.tags&&params.tags[0])?params.tags[0]:"")||"")+'</span>'+
                                // '</span>'+ 
                            '</div>'+otherStr+
                            '<div class="desc">'+(params.descriptionStr||params.text||"").substring(0, 40)+"..."+'</div>'+
                        '</div>'+
                        '<div class="link-bordered">'+
                            '<a href="'+params.hash+'" class="btn lbh-preview-element margin-right-20">En savoir plus</a>';
                            if(params.collection=="events"){
                                str += '<a class="btn  handleSession" onClick ="handleSession(\''+params._id.$id+'\')" data-id="'+params._id.$id+'"fa fa-cog"></i> Gérer cette session</a>';
                            }
                        str += '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';
            return str;
        }  
    });
</script>