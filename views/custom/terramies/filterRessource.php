<script type="text/javascript">
    var pageApp=<?php echo json_encode(@$page); ?>;            
	var paramsFilter= {
	 	container : "#filters-nav",
        results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true
        },
	 	filters : {
                      
            actor : {
                view : "dropdownList",
                type : "filters",
                field : "type",
                name : "Ressources professionnelles",
                event : "filters",
                keyValue : false,
                list : {
                    "mesure":"Ressources juridiques",
                    "procedure": "Protocoles et procédures",
                    "recherche":"Etudes et Recherches",
                    "atelier":"Ateliers Terramies"
                }
            },
            
            types : {
                lists : ["poi"]
            } 
        }
        
     };
  
    var filterSearch={};
    jQuery(document).ready(function() {
        mylog.log('filterSearch paramsFilter', paramsFilter);
        filterSearch = searchObj.init(paramsFilter);

        $(".headerSearchright").css("display" , "none");
        

 
    });

</script>