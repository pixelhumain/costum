<style>
    .competence-banner-content {
        background-color: #783469;
        padding: 40px;
    }
    .competence-banner-content .subtitle-header {
        font-weight: 400;
    }
    .competence-banner-content .number-header {
        margin-right: 40px;
    }
    .competence-stat .progress-circle {
        min-height: auto;
    }
    .bg-footer-competence .second-subtitle {
        font-size: 20px;
    }
    .bg-footer-competence {
        background-color: #ecedf6;
        padding-top: 20px;
        padding-bottom: 20px;
    }
    .bg-footer-competence .text-explain {
        color: #FF286B;
    }
    .bg-footer-competence .activity-stat .progress .progress-value {
        background: #ecedf6;
        color: #4623C9;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .competence-banner-content {
            padding: 15px;
        }
        .competence-banner-content .number-header {
            margin-right: 0px;
        }
    }
    @media (max-width: 767px) {
        .competence-banner-content {
            padding: 15px;
        }
        .competence-banner-content .number-header {
            text-align: center;
        }
        .competence-banner-content .number-header {
            margin-right: 0px;
        }
    }
</style>
<div class="col-xs-12 no-padding">
    <div class="col-xs-12 competence-banner-content">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
                <div class="number-header text-white">
                    EN APPUI À LA MONTÉE EN COMPÉTENCES DE TOUS
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                    <div class="title-header red-ftl">
                        60%
                    </div>
                    <div class="subtitle-header text-white">
                        DES TIERS-LIEUX FONT DE LA FORMATION
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6" style="border-left: 2px solid #ffffff;">
                    <div class="subtitle-header" >
                        <span class="red-ftl">43%</span>
                        <span class="text-white">TRAVAILLENT AVEC DES ORGANISMES DE FORMATION</span><br><br>
                    </div>
                    <div class="subtitle-header">
                        <span class="red-ftl">24%</span>
                        <span class="text-white">SONT RECONNUS ORGANISMES DE FORMATION</span><br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
        <div class="activity-stat competence-stat">
            <h4 class="blue-ftl padding-bottom-20 ">
                DIVERSITÉ DES TIERS-LIEUX SE RETROUVE DANS LES MODES DE GESTION
            </h4>
            <?php
            echo str_repeat(
                '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 progress-circle">
                        <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                            <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                            <div class="progress-value">90<span style="font-size: 15px">%</span> </div>
                        </div>
                        <div class="blue-ftl text-center">
                            DE COWORKING
                        </div>
                    </div>
                    ', 4);
            ?>
        </div>
        <hr class="hr-ftl">
    </div>

    <div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 padding-top-30">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="text-explain" style="color: #D07C40">
                AGISSANT POUR L’INCLUSION NUMÉRIQUE
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            Section pour dev-christon
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="blue-ftl second-subtitle">
                DES TIERS-LIEUX SONT DES ACTEURS DE LA MÉDIATION NUMÉRIQUE
            </div>
        </div>
    </div>


    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 progress-contain">
        <h4 class="blue-ftl text-center">
            QUELS TYPES DE SERVICES <br>EN LIEN AVEC LE NUMERIQUE PROPOSENT LES TIERS-LIEUX
        </h4>
        <div class="mp-progress">
            <div class="dp-flex">
                <div class="textbefore-progress">
                    PARTICIPATION A L'ANIMATION
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 77%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">77% Complete</span>
                        <span class="progress-type">45%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    GESTION DU LIEUX
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">45% Complete</span>
                        <span class="progress-type">45%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    PARTICIPATION A LA GOUVERNANCE
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">40% Complete</span>
                        <span class="progress-type">40%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    ACCUEIL DES AUTRES UTILISATEURS
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="52" aria-valuemin="0" aria-valuemax="100" style="width: 52%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">52% Complete</span>
                        <span class="progress-type">52%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    TUTORAT, COUP DE MAIN ETC ....
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">62% Complete</span>
                        <span class="progress-type">62%</span>
                    </div>
                </div>
            </div>
            <div class="dp-flex">
                <div class="textbefore-progress">
                    AUCUN PARTICIPATION
                </div>
                <div class="progress bg-white prgress-flex">
                    <div class="progress-bar val-progress" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                        <span class="sr-only">62% Complete</span>
                        <span class="progress-type">62%</span>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-xs-12 bg-footer-competence">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="text-explain red-ftl">
                ACTEUR DE LA TRANSITION ECOLOGIQUE
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class=" dp-flex">
                <span class="number-header blue-ftl"> 49% </span>
                <span class="second-subtitle blue-ftl">
                DES LIEUX SONT EN LIEN ETROITE AVEC LES ACTEURS DE LA TRANSITIOÀN ECOLOGIQUE
            </span>
            </div>
            <hr class="hr-ftl col-xs-12">
        </div>
        <div class="col-xs-12 no-padding">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <span class="second-subtitle blue-ftl">DES LIEUX SONT EN LIEN ETROITE AVEC LES ACTEURS DE LA TRANSITIOÀN ECOLOGIQUE<br> </span>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class=" dp-flex">
                            <span class="number-header blue-ftl"> 81% </span>
                            <span class="second-subtitle blue-ftl">
                                DE LA COLLECTE ET DU TRI DE DECHET
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class=" dp-flex">
                            <span class="number-header blue-ftl"> 51% </span>
                            <span class="second-subtitle blue-ftl">
                                DE LA SENSIBILISATION DU GRAND PUBLIC AU QUOTIDIEN ET DANS LE CADRE D'EVENNEMENTS DEDIERS
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class=" dp-flex">
                            <span class="number-header blue-ftl"> 38% </span>
                            <span class="second-subtitle blue-ftl">
                                UN ENCADREMENT DANS UNE DEMARCHE D'INCITATION AU ZERO DECHET
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class=" dp-flex">
                            <span class="number-header blue-ftl"> 33% </span>
                            <span class="second-subtitle blue-ftl">
                                DES ACTIONS DE REEMPLOIN ET DE LUTTE CONTRE L'OBSOLESCENCE PROGRAMMÉE
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <h4 class="blue-ftl padding-bottom-20 ">
                            CET ENGAGEMENT RESSENT DANS LEUR FONCTIONNEMENT QUOTIDIEN :
                        </h4>
                    </div>
                    <div class="col-xs-12 progress-contain">
                        <div class="mp-progress">
                            <div class="progress">
                                <div class="progress-bar bg-white" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                                    <span class="sr-only">45% Complete</span>
                                    <span class="progress-type">45%</span>
                                </div>
                            </div>
                            <h4 class=" blue-ftl ">
                                REALISENT DES ACHATS OU INVESTISSEMENT ECOLOGIQUE
                            </h4>
                        </div>
                        <div class="mp-progress">
                            <div class="progress">
                                <div class="progress-bar bg-white" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
                                    <span class="sr-only">45% Complete</span>
                                    <span class="progress-type">45%</span>
                                </div>
                            </div>
                            <h4 class=" blue-ftl ">
                                ONT MIS EN PLACE UN COMPOSTEUR PARTAGÉ ET DES TEMPS ÉDUCATIF RÉGULIERS POUR SON UTILISATION.
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="red-ftl">
                    <span class="number-header red-ftl"> 1/3 </span>
                    <span class="second-subtitle">SONT ENGAGÉS DANS LE LUTTE CONTRE L'OBSOLESCENCE PROGRAMMÉE</span>
                </div>
                <div class="blue-ftl">
                    <span class="second-subtitle">ET DÉVELOPPEMENT DES PROJETS AUTOUR DU REEMPLO, DU RECYCLAGE OU DE LA REPARATION D'OBJET</span>
                </div>

                <div class="stat-footer">
                    <div class="dp-flex activity-stat">
                        <div class="progress-circle">
                            <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                                <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                                <div class="progress-value bg-white blue-ftl">90<span style="font-size: 15px">%</span> </div>
                            </div>
                        </div>
                        <div class="blue-ftl" style="margin-top: auto; margin-bottom: auto; padding-left: 10px;">
                            ONT UNE PÉPINIÈRE D'ENTREPRISE
                        </div>
                    </div>
                </div>
                <div class="stat-footer">
                    <div class="dp-flex activity-stat">
                        <div class="progress-circle">
                            <div class="progress blue">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                                <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                                <div class="progress-value bg-white blue-ftl">90<span style="font-size: 15px">%</span> </div>
                            </div>
                        </div>
                        <div class="blue-ftl" style="margin-top: auto; margin-bottom: auto; padding-left: 10px;">
                            ONT UNE PÉPINIÈRE D'ENTREPRISE
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

