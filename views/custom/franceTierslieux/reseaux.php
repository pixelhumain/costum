<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
        "/js/franceTierslieux/dataviz/getElement.js",
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
    use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
?>
<style>
	.ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }
    .ans-dir .img-responsive-action { 
        height: inherit !important;
    }
</style>
    <div class="container navigator-page network-dir padding-bottom-20 padding-left-0 padding-right-0 ans-dir" style="display: none;"></div>
    <div id="custom-prev-div" style="display: block;"></div>
        <div class="container">
            <div class="row network-map-page">
                <p class="page-title">Réseaux régionaux</p>
                <button class="btn-filters-region btn btn-primary" style="margin-left: auto;display: block;" data-action="liste">Mode Liste</button>
                <!-- <hr /> -->
            </div>
        </div>
        <?= BlockCmsWidget::widget([
            "path" => "tpls.blockCms.map.shapeVisualisation",
            //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
            "notFoundViewPath" => "costum.views.tpls.blockCms.map.shapeVisualisation",
            "config" => [
                "blockCms" => array(
                    "click" => "navigators"
                )
            ]
        ]); ?>
    </div>
<script>  
let initPagination = "";
var nameBySlug = costum.store.whereList.previewOptions;

$(function(){
    localStorage.setItem("networkstate","[data-action='shape']");
    directory.PanelHtml = function(params){
        var slug = params.slug;
        var organisationArrayId = [];
        var network = "";
        var networkName = params.name;
        var networkKey = (params?.address?.level3 != undefined) ? params.address.level3 : "";
        
        var str = `
            <div class="team">
                <div class="card-team">
                    <div class="img-box">
                        <img class="img-responsive-action" alt="Responsive Team Profiles" src="${params.profilMediumImageUrl}" />
                    </div>
                    <p class="third-place-title">
                        <a href="javascript:" onclick="showAllNetwork.loadPreview('','${params.profilMediumImageUrl}','${networkName}','' ,'reseaux', '', '', '${networkKey}', function(){}, '${slug}')" class="">
                            ${networkName}
                        </a>
                    </p>
                    <p class="network-title">
                        <a href="javascript:" onclick='showAllNetwork.showModal("${networkName}", "${params.address.level3}")' style="text-decoration: none;" target="_blank">
                            Voir les tiers lieux (${typeof whereLists != "undefined" && typeof whereLists[networkName] != "undefined" ? whereLists[networkName].count : 0})
                        </a>
                    </p>`;  
        
        str += `
                    </div>
            </div>
        `;
        return str;
    }
    showAllNetwork.paramsFilters.results.renderView = "directory.PanelHtml";
    showAllNetwork.paramsFilters.defaults.textPath = "answers.franceTierslieux2022023_753_10.franceTierslieux2022023_753_10lemfa0njory1oxpszb";
    // showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/63e0a8abeac0741b506fb4f7";
    showAllNetwork.paramsFilters.defaults.types = ["organizations"];
    showAllNetwork.paramsFilters.defaults.filters = {tags: ['RéseauTiersLieux']};
    showAllNetwork.paramsFilters.defaults.fields = ["name","profilMediumImageUrl","slug"]
    searchObj.footerDom = ".reseauxFooter";
    $(".ans-dir").html(showAllNetwork.initView("","reseauxFooter"));
    let filter = searchObj.init(showAllNetwork.paramsFilters);
    showAllNetwork.initEvents();
    filter.search.init(filter);
    <?php if(isset($_GET["slug"])){ ?>
        showAllNetwork.loadPreview('',`${nameBySlug['<?= $_GET["slug"] ?>'].profilMediumImageUrl}`,`${nameBySlug['<?= $_GET["slug"] ?>'].name}`,'' ,'reseaux', '', '', `${nameBySlug['<?= $_GET["slug"] ?>'].address.level3}`)
    <?php } ?>
    $(".container-filters-menu").attr("style","height : 50px !important");
}) 

$('[data-dismiss="modal"]').click(function(){
    $(".reseauxFooter").html(initPagination);
})

$(".pageContent #regionshapeContainer").addClass("navigator-page").addClass("network-map-page");
// $(".btn-filters-region").parent().addClass("navigator-page");
</script>



