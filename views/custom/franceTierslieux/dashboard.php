<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/jquery-counterUp/waypoints.min.js",
		"/plugins/jquery-counterUp/jquery.counterup.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

    // formulaire
    $paramsData = [
        "titre" => "",
        "coform" => "",
        "path" => "",
        "type" => [
            "numberCommon/type/count"=>"Compteur", 
            "numberCommon/type/persent"=>"Pourcentage", 
            "numberCommon/type/median"=>"Médiane",
            "d3Common/type/circle"=>"Cercle D3", 
            "d3Common/type/mindmap"=>"carte mentale D3", 
            "d3Common/type/network"=>"Réseau D3", 
            "d3Common/type/relation"=>"Relation D3"],
        "width" => ["3"=>3, "4"=>4, "6"=>6, "12"=>12],
        "typeValue" => "",
        "widthValue" => ""
    ];

    if(!isset($childForm) && !isset($formInputs)){
        // Get all Forms
        $childForm = PHDB::find(Form::COLLECTION, array("parent.".$this->costum['contextId']=>['$exists'=>true],"active"=>"true"));
        //var_dump($childForm);exit;
        $formInputs = [];
        foreach ($childForm as $formKey => $formValue) {
            if(is_array($formValue["subForms"])){
               $subFormId =  array('$in' => $formValue["subForms"]);
            }

            if(is_string($formValue["subForms"]) && $formValue["subForms"] !=""){
                $subFormId = $formValue["subForms"];
            }

            if(isset($subFormId) && is_array($subFormId) && $subFormId!=""){
                $subForms = PHDB::find(Form::COLLECTION, array(
                'id' => $subFormId));
                foreach ($subForms as $subFormKey => $subFormValue) {
                    if(isset($subFormValue["inputs"])){
                        $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                    }
                }
            }
        }
    }
?>

<style type="text/css">
    option{
        font-size: 14pt;
    }
    <?php if( !isset($css["border"]) || $css["border"]==true ){ ?>
	.counterBlock{min-height:340px;}
    .wborder{border : 2px solid #ddd /*#002861*/; border-radius : 0px;}
    .title2{font-size : 24px; font-weight : bold;}
    <?php }?>
    
</style>

<div class="col-md-12 col-sm-12 col-xs-12 no-padding margin-top-20 dashboard app-<?php echo @$page ?>">
    <div class="col-xs-12 text-center" style=" color:#24284D">
    	<h1 style="margin:0px">
            <?php 
            echo $title;
            echo '&nbsp; &nbsp; <button class="btn btn-info dashboarConfigBtn addNewGraph">
                    <i class="fa fa-plus"></i>
                </button>';
            ?>
        </h1>
    </div>
    <script type="text/javascript">
    <?php  
        foreach ($blocks as $id => $d) {
            if( isset($d["graph"]) && str_contains($d["graph"]["url"], "size") ) { ?>
                var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
                mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
        <?php }
    } ?>
    </script>
   <div classs="col-xs-12" style="">
		<?php //echo $this->renderPartial("graph.views.co.menuSwitcher",array()); ?> 
		<div class="col-xs-12" id="graphSection">
   				<?php       					
   				
                $colCount = 0;
                $blocksize = 6;
                $fontSize = "24px;font-weight:bold;";
                $borderClass = "wborder";
   				 foreach ($blocks as $id => $d) 
   				 {
                    $answerPath = "";
                    $graphRootOrTitle = "";

                    if(isset($d["graph"]["data"]["path"])){
                        $answerPath = $d["graph"]["data"]["path"];
                    } 

                    if(isset($d["graph"]["data"]["title"])){
                        $graphRootOrTitle = $d["graph"]["data"]["title"];
                    }
                    if (isset($d["width"]))
                        $blocksize=$d["width"];
                    // if($id == "pieGeo")
                    //     $blocksize=12;
                    else if (isset($d["html"]))
                        $blocksize=4;
                   
                    if( isset($d["graph"]) || isset($d["html"])){

                    ?>     
                        <div class="col-md-<?php echo $blocksize?>  text-center padding-10"  >
                            <h1><span class="<?php echo (!empty($d["counter"]))? "counter" : ""?>"><?php echo $d["counter"]?></span>
                            </h1>
                            <div style='font-size:<?php echo $fontSize?>'><?php echo isset($d["title"])?$d["title"]:""; ?>
                            </div>
                            <div class="<?php echo !isset($d["graph"]["data"]["title"])?"counterBlock":"" ?>  <?php echo $borderClass; ?>" id="<?php echo $id?>" >
                            <?php 
                            if( isset($d["html"]) ) {
                                echo $d["html"];
                            }else if(isset($d["graph"])){
                                $graphType = explode("/", $d['graph']['url'])[7];
                                $blockId = (str_contains($d["graph"]["url"], "/size/"))?$d["graph"]["key"]:$id;
                                echo $this->renderPartial(
                                    explode("/", $d['graph']['url'])[5], 
                                    [
                                    "type"=>$graphType,
                                    "id"=>$blockId,
                                    "size"=>$blocksize,
                                    "path"=>$answerPath,
                                    "title"=>$graphRootOrTitle
                                    ]
                                );
                            }
                            ?>
                            </div>
                        </div>
                     <?php 
                     
                     } ?>
             <?php }?>
   		</div>
   </div>

</div>


<script type="text/javascript">
//alert("/modules/costum/views/custom/ctenat/dashboard.php")
//prepare global graph variable needed to build generic graphs

jQuery(document).ready(function() {
	setTitle("Observatoire des Tiers-lieux");
	mylog.log("render graph","/modules/costum/views/custom/franceTierslieux/dashboard.php");
	<?php  /* foreach ($blocks as $id => $d) {
		if( isset($d["graph"])) { 
            $answerPath = "";
            $graphRootOrTitle = "";

            if(isset($d["graph"]["data"]["path"])){
                $answerPath = $d["graph"]["data"]["path"];
            } 

            if(isset($d["graph"]["data"]["title"])){
                $graphRootOrTitle = $d["graph"]["data"]["title"];
            } ?>

			mylog.log('render ajaxPost url graphs','grph','#<?php echo $id?>');
            ajaxPost("#<?php echo $id; ?>", 
                baseUrl+"/graph/co/dash/g/<?= explode("/", $d['graph']['url'])[5] ?>/",
                {
                    g:"<?= explode("/", $d['graph']['url'])[5] ?>",
                    type:"<?= explode("/", $d['graph']['url'])[7] ?>",
                    id:"<?= $d['graph']['key'] ?>",
                    size:"S",
                    path:"<?=$answerPath ?>",
                    title:"<?= $graphRootOrTitle ?>"
                },
                function(data){
                    $("#<?php echo $id?>").html(data);
                },
                function(error){
                    alert(JSON.stringify(error))
                }, "html");
	<?php }
	} */?>

	$('.counter').counterUp({
	  delay: 10,
	  time: 2000
	});

	$('.counter').addClass('animated fadeInDownBig');
	$('h3').addClass('animated fadeIn');

});
</script>


<style type="text/css">
    .dashElem{
        /*height:275px;*/
        overflow:hidden;
        /*border: 1px solid #bbb;*/
    }
    .grayimg{
        opacity:0.2;
    }
    .openDashModal{
        font-size: 10px;
    }
</style>
    


<script type="text/javascript">

    jQuery(document).ready(function() {

        mylog.log("render","/modules/costum/views/custom/franceTierslieux/dashboard.php",<?php echo json_encode( $elements) ?>);
        coInterface.bindLBHLinks();
        /**
        $(".graph-config").click(function(){
            alert("It is me"+ $(this).data("path"));
        });*/

    });
</script>

<script type="text/javascript">
    let sectionDyf = {};
    let tplCtx = {};

    jQuery(document).ready(function() {
        if(costum.editMode == false){
            $(".dashboarConfigBtn").hide();
            $(".removeConfigBtn").hide();
        }else{
            $(".removeConfigBtn").show();
            $(".dashboarConfigBtn").show();
        }
        
        sectionDyf.configParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.configParams = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        "inputType" : "text",
                        "label" : "Titre du graph",
                        "rules" : {
                            "required" : true
                        },
                        "value" :  sectionDyf.configParamsData.title
                    },
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control new-graph",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {
                            <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                            ?>
                        },
                        "value": sectionDyf.configParamsData.coform
                    },
                    "path" : {
                        "inputType" : "select",
                        "class" : "new-graph",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher dans la question",
                        "options": {}
                    },
                    "url" : {
                        "inputType" : "select",
                        "label" : "Quelle type de graph",
                        "class" : "form-control new-graph",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.configParamsData.type,
                        "value":sectionDyf.configParamsData.typeValue
                    },
                    "width" : {
                        "inputType" : "select",
                        "class" : "form-control new-graph",
                        "label" : "Largeur d'espace à occuper",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.configParamsData.width,
                        "value":sectionDyf.configParamsData.widthValue
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.configParamsData.coform!=""){
                        updateInputList(sectionDyf.configParamsData.coform);
                    }
                    if($("#path.new-graph option[value='"+sectionDyf.configParamsData.path+"']").length > 0){
                        $("#path.new-graph").val(sectionDyf.configParamsData.path);
                    }
                },
                save : function (data) {
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"block<?php echo count($blocks) ?>", "data":{}};
            
                    $.each( sectionDyf.configParams.jsonSchema.properties , function(k,val) { 
                        if(k != "width"){
                            if(k == "url"){
                                tplCtx.value["graph"][k] = "/graph/co/dash/g/costum.views.custom.franceTierslieux.graph."+$("#"+k).val();
                                tplCtx.value["type"] = $("#"+k).val().split("/")[2];
                            }else{
                                tplCtx.value["graph"]["data"][k] = $("#"+k).val();
                            }
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    tplCtx.value["counter"] = "";

                    if($("#url").val().includes("numberCommon")){
                        tplCtx.value["title"] = "";
                    }else{
                        tplCtx.value["title"] = $("#title").val();
                    }

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".addNewGraph").off().on("click",function() {  
            tplCtx["id"] = "<?php echo $this->costum["contextId"] ?>";
            tplCtx["collection"] = "<?php echo $this->costum["contextType"] ?>";
            tplCtx["path"] = "costum.dashboard.config.block<?php echo count($blocks) ?>";
            dyFObj.openForm( sectionDyf.configParams, null, sectionDyf.configParamsData);
        });

        $(document).on("change", "#coform.new-graph", function(){
            updateInputList($(this).val());
        });

        let updateInputList = function(value){
            let childForm = <?php echo json_encode($formInputs) ?>;
            $("#path.new-graph").empty();

            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    if(input["type"].includes("multiCheckboxPlus")){
                        $("#path.new-graph").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes("multiRadio")){
                        $("#path.new-graph").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"] == "text"){
                        $("#path.new-graph").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }
        }
    });
    
</script>