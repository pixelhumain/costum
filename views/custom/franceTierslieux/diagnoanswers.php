<style>
    .mg-1{
        margin-left: 15px;
    }
    .mg-2{
        margin-left: 30px;
    }
    .p-null{
        border-left: 10px solid red;
        padding-left: 15px;
    }
    .p-bas{
        border-left: 10px solid orange;
        padding-left: 15px;
    }
    .p-moyenne{
        border-left: 10px solid blue;
        padding-left: 15px;
    }
    .p-haut{
        border-left: 10px solid green;
        padding-left: 15px;
    }
    .legend-item{
        display:flex;
        font-size: 14pt;
        vertical-align:center;
    }
</style>
<header class="bg-white padding-10" style="border-bottom:2px solid #ddd">
    <h1 class="text-center">Diagnostique des réponses <?= $nbTotalAnswers ?></h1>
    <div id="f" class="text-center">
        <div id="title"></div>
        <div class="row padding-left-15">
            <div class="btn-group margin-right-5" style="font-size:16pt">
                <button type="button" class="btn btn-lg btn-default rounded-0 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Etapes
                </button>
                <ul id="sections" class="dropdown-menu">
                    <?php foreach ($answersBySteps as $key => $value) { ?>
                        <li><a href="#<?= $value["id"] ?>" data-typeurl='externalLink' class='lbh-anchor-btn' ><?= $value["step"] ?></a></li>
                    <?php } ?>
                </ul>
            </div>

            <div class="btn-group margin-right-5" style="font-size:16pt">
                <button type="button" class="btn btn-lg btn-default rounded-0 dropdown-toggle padding-right-20" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Questions
                </button>
                <ul id="filterRegion" class="dropdown-menu" style="height:500px;width:600px;overflow-y:scroll">
                    <?php foreach ($answersBySteps as $key => $abs) { ?>
                        <?php foreach ($abs["answer"] as $qk => $qv) { ?>
                                <li><a href="#<?= $qk ?>" data-typeurl='externalLink' class='lbh-anchor-btn' ><?= $qv["question"] ?></a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>

            <div class="btn-group margin-right-5" style="font-size:16pt">
                <button type="button" class="btn btn-lg btn-default rounded-0 showRequired">
                    Questions Obligatoire
                </button>
            </div>
        </div>
    </div>
</header>

<main class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
            <?php foreach ($answersBySteps as $key => $abs) { ?>
                <div id="<?= $abs["id"] ?>" >
                    <h3 style="background:#ddd" class="padding-10"><?= $abs["step"] ?></h3>
                    <?php foreach ($abs["answer"] as $qk => $qv) { ?>
                        <div id="<?= $qk ?>" class="mg-1 required<?= $qv["is_required"] ?>">
                            <h5><?= $qv["question"] ?></h4>
                            <div class="mg-2 p-<?= $qv["relevance"] ?>" style="font-size:14pt">
                                <div><b>Type :</b> <?= $qv["type"] ?></div>
                                <!--div><b>Path :</b> <?= $qv["path"] ?></div-->
                                <div><b>Required :</b> <?= $qv["is_required"] ?></div>
                                <div title="Nombre de ce qui ont répondu à ce question">
                                    <b>Nombre répondant :</b> <?= $qv["nbAnswers"] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
            <div class="col-md-4">
                <div id="sideLegend" class="padding-20">
                    <h4>Légende par rapport au nombre des réponses</h4>
                    <div class="legend-item">
                        <span style="background:green;height:30px;width:30px"></span>
                        Nombre de répondants entre (entre 60% à 100%)</div>
                    <div class="legend-item">
                        <span style="background:blue;height:30px;width:30px"></span>
                        Nombre de répondants entre 20% et 60%)</div>
                    <div class="legend-item">
                        <span style="background:orange;height:30px;width:30px"></span>
                        Nombre de répondants entre 0% et 20%) </div>
                    <div class="legend-item">
                        <span style="background:red;height:30px;width:30px"></span>
                        Nombre de répondants Null</div>
                </div>
            </div>
        </div>
</main>


<script>
    jQuery(document).ready(function() {
	    var anchors = "";
    
        $(".pa-title").each(function(){
            anchors+="<li><a href='#"+$(this).attr("id")+"' data-typeurl='externalLink' class='lbh-anchor-btn' >"+$(this).text()+"</a></li>";
        })

        $("header").css({
            position: "-webkit-sticky",
            position: "sticky",
            top: "0",
            zIndex: 999
        });
    });

    $(".lbh-anchor-btn").on("click", function(e){
        e.stopPropagation();
        e.preventDefault();
        var idBtnLink = $(this).attr("href");
        $('html,body').animate({
            scrollTop: $(idBtnLink).offset().top-150},
        'slow');
    });

    $(".requiredOui").on("click", function(e){
        e.preventDefault();
    });

    $(".showRequired").on("click", function(e){
        if($(this).data("show")==true){
            $(this).data("show", false);
            $(".requiredNon").show();
            $(this).removeClass("btn-primary");
        }else{
            $(this).data("show", true);
            $(".requiredNon").hide();
            $(this).addClass("btn-primary");
        }
       
    })
</script>