<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/toolsList.css",
        "/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/dataviz/getElement.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
        "/css/franceTierslieux/answersDirectory.css",
    );
    $toolFormParam = [
      "finderPath" => "answers.lesCommunsDesTierslieux10112022_1423_0.finderlesCommunsDesTierslieux10112022_1423_0lmbmy4z4ze9v982h4fh",
    ];
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
?>
<div class="navigator-page">
  <div style="padding: 0% 10%">
      <p class="page-title">Listes des outils utilisés par les Tiers-Lieux</p>
      <!-- <hr style="border-top: 1px solid #858585;"> -->
  </div>
  <div class="wrapper">
  <!-- <ul id="list-tools"></ul> -->
  <div id="list-tools"></div>
  <div class="empty-text" hidden>
    <h3 class="text-center">
      La raison pour laquelle c'est vide, c'est qu'aucun outil n'a été enregistré à partir du formulaire d'usage des outils numériques des Tiers-Lieux. <br> <i style="font-size: 85px;" class="fa fa-info-circle margin-top-20"></i> 
    </h3>
  </div>
  </div>
</div>
<div id="custom-prev-div" style="display: block;"></div>

<script>
  $(function() {
    let listTools = {
        formId : "", 
        finderPath : "<?= $toolFormParam["finderPath"] ?>",
        toolsData : {},
        toolsObject: {},
        init : function () {
            searchObj.footerDom = ".footerSearchContainer";
            listTools.actions.getallToolsUsage();
        },
        html : {
          sectionTools : function (sectionTitle, color="") {
            return `
              <div class="section-tool">
                <p style="color:${color}" class="section-tool-title">${sectionTitle}</p>
              </div>
            `;
          },
          toolUsage : function (criteriaValues) {
            var nameKey = criteriaValues.usage.replace(/[\s\/'()]/g, '').toLowerCase();
            return `
                <li class="listing clearfix usage-list title-${nameKey}">
                    <div class="info">
                      <span class="usage-title">${criteriaValues.usage}</span>
                      <span class="usage-info">(Cas d'usage)</span>
                    </div>
                </li>
                <li>
                  <ul data-nameKey="${nameKey}" class="tool-and-desc ${nameKey}"></ul>
              </li>`;
          },
          loadingMessage : `
            <div id="tr-modal-content">
              <i class="fa fa-spin fa-spinner"></i> Loading...
            </div>
          `,
          showModal : function (allOrga=[], toolName) {
            showAllNetwork.showModal(toolName,"",allOrga);
          },
          toolList : function (toolName, counts, orgaArray, toolKey, path) {
            var percent = 0;
            var defaultImage = modules.co2.url+"/images/default_tool.png";
            var urlImageTool = (jsonHelper.pathExists("toolsImageUrl."+toolName.toLowerCase().replace(/\s/g, "")+".imageUrl")) ? ((toolsImageUrl[toolName.toLowerCase().replace(/\s/g, "")]?.imageUrl == "") ? defaultImage : toolsImageUrl[toolName.toLowerCase().replace(/\s/g, "")]?.imageUrl) : defaultImage;
            var image = (AllToolImage[toolKey] != undefined) ? AllToolImage[toolKey] : urlImageTool;
            const sumOrgas = obj => Object.values(obj).reduce((content, el) => content + el, 0);
            var totalOrgas = sumOrgas(counts);
            if (totalOrgas == orgaArray.length || totalOrgas < orgaArray.length) 
              percent = 100;
            if (totalOrgas >  orgaArray.length) 
              percent = (orgaArray.length / totalOrgas ) * 100;
            percent = percent.toFixed(0);
            return `
              <li class="col-list">
                <div class="tool-card"> 
                  <div class="tool-card-div">
                    <a class="tool-modal" data-tool-name="${toolName}" data-tool-path="${path}" href="javascript:;"><img class="${toolKey}" src="${image}" alt="" style="width: 10vh; vertical-align: inherit;">
                  </div>
                  </a> 
                    <div class="tool-desc">
                      <a href="javascript:"><b class="tool-modal tool-desc" data-tool-name="${toolName}" data-tool-path="${path}">${toolName}</b></a> <br>
                      <a href="javascript:" onclick="showAllNetwork.loadPreview('','${image}','${toolName}','', 'tools')" class="tools-preview"> Voir plus d'information </a>
                    <div style="border-image: linear-gradient(90deg, #3498db ${percent}%, transparent ${percent}%);" class="tp-percent">
                      <p class="tp-perc-text">${orgaArray.length} Tiers-Lieux (${percent} %) </p>
                    </div>
                    </div>  
                </div>
              </li> `;
          }
        },
        events : {
          clickOneList : function (toolsData={}) {
            if (toolsData != undefined && Object.keys(toolsData).length > 0) {
              $('.tool-modal').click(function () {
                var toolPath = $(this).attr("data-tool-path");
                var getOrgaArray = jsonHelper.getValueByPath(toolsData, toolPath);
                var toolName = $(this).attr("data-tool-name");
                listTools.html.showModal(getOrgaArray, toolName);
              })
            }
          }
        },
        views : {
            listAllUsages : function(usageData={}) {
                if (Object.keys(usageData).length > 0) {
                    var listHtmlContent = "";
                    var firstColor = "#0f4475";
                    var secondColor = "#0174BE";
                    var color = firstColor;
                    $.each(usageData, function (k, v) {
                        listHtmlContent += "<div class='list-content'>";
                        listHtmlContent += listTools.html.sectionTools(k, color);
                        listHtmlContent += "<div class='sublist-content' style='border-left: 4px solid "+ color +";'>";
                        $.each(v, function(criteriaKey, criteriaValues) {
                            if (criteriaValues?.usage != undefined) {
                              listTools.views.getTools(criteriaValues.trAnswers, criteriaValues);
                              listHtmlContent += listTools.html.toolUsage(criteriaValues);
                            }
                        })
                        listHtmlContent += "</div></div>";
                        color = (color == firstColor) ? secondColor : firstColor;
                    })
                    $('#list-tools').html(listHtmlContent);
                    if (Object.keys(listTools.toolsObject).length > 0) {
                      var listToolsHtml = "";
                      $.each(listTools.toolsObject, function(k, v) {
                        $.each(v, function(toolKey, toolInfo) {
                          if (toolKey != "totalCountOrga") {
                            listToolsHtml += listTools.html.toolList(toolInfo.name, v.totalCountOrga, toolInfo.orgaArray, toolInfo.key, k + "." + toolInfo.key +".orgaArray");
                          }
                        })
                        $("." + k).html(listToolsHtml);
                        listToolsHtml= "";
                      })
                    }
                    $.each($(".sublist-content ul.tool-and-desc"), function(k,v) {
                      var getAttrib = v.getAttribute("data-nameKey");
                      if (!notEmpty(v.innerHTML.trim())) {
                        $(".title-" + getAttrib).remove();
                      }
                    })
                }
            },
            getTools : function (answerData, criteriaValues) {
              let listHtml = "";
              let allTools = [];
              let toolsObject = {};
              if (Array.isArray(answerData) && answerData.length > 0) {
                answerData.map(function(content) {
                  if (Object.values(content).length > 0) {
                    $.each(Object.values(content), function (key, value) {
                      if (value.answers != undefined) {
                        let answerContent = value.answers;
                        $.each(answerContent, function (k, v) {
                          let getOrgaContent = jsonHelper.getValueByPath(value, listTools.finderPath);
                          let orgaContentKey = (typeof getOrgaContent == "object" && Object.keys(getOrgaContent)[0] != undefined) ? Object.keys(getOrgaContent)[0] : "";
                          if (k.startsWith("yesOrNo") && Object.values(v)[0].criteria != undefined && jsonHelper.getValueByPath(getOrgaContent, orgaContentKey) != undefined) {
                            var toolName = Object.values(v)[0].criteria;
                            var usage = criteriaValues.usage.replace(/[\s\/'()]/g, '').toLowerCase();
                            var toolKey = toolName.replace(/[\s\/'()]/g, '').toLowerCase();
                            if (listTools.toolsObject[usage] == undefined) 
                              listTools.toolsObject[usage] = {};
                            if (listTools.toolsObject[usage][toolKey] == undefined)
                              listTools.toolsObject[usage][toolKey] = {};
                            listTools.toolsObject[usage][toolKey].name = toolName.toUpperCase();
                            listTools.toolsObject[usage][toolKey].key = toolKey;
                            if (listTools.toolsObject[usage][toolKey].orgaArray == undefined) 
                              listTools.toolsObject[usage][toolKey].orgaArray = [];
                            var orga = jsonHelper.getValueByPath(answerContent, getNavigatorElement.suplment.tool.path.replace("answers.", ""));
                            var orgaName = Object.values(orga)[0].name;
                            listTools.toolsObject[usage][toolKey].orgaArray.push(orgaName);
                            if (listTools.toolsObject[usage][toolKey].countOrga == undefined) 
                              listTools.toolsObject[usage][toolKey].countOrga = 0;
                            listTools.toolsObject[usage][toolKey].countOrga = listTools.toolsObject[usage][toolKey].orgaArray.length
                            if (listTools.toolsObject[usage].totalCountOrga == undefined) 
                              listTools.toolsObject[usage].totalCountOrga = {};
                            listTools.toolsObject[usage].totalCountOrga[toolKey] = listTools.toolsObject[usage][toolKey].orgaArray.length
                          } 
                        })
                      }
                    })
                  }
                })          
              }
              return listHtml;
            }
          },
        actions : {
          getallToolsUsage : function () {
              var paramsTool = {
                finderPath : getNavigatorElement.suplment.tool.path,
                step : getNavigatorElement.suplment.tool.step,
                formId : getNavigatorElement.suplment.tool.id,
              };
              var toolUsage = getNavigatorElement.getToolsAnswers(paramsTool);
              if (Object.keys(toolUsage).length > 0) {
                listTools.views.listAllUsages(toolUsage);
                listTools.events.clickOneList(listTools.toolsObject);
              } else {
                $(".empty-text").fadeIn()
              }
          },
        }
    }
    listTools.init();
  })
</script>
