<div class="navigator-prev-contents" >
	<?php
		// if (isset($name)) {
		// 	$name = str_replace("#", "'", $name);
		// }

		echo $this->renderPartial(
			"costum.views.custom.franceTierslieux.previewDataViz.previewSections", 
			array(
				"name" => $name, 
				"page" => $page, 
				"sections" => $sections ?? [], 
				"pathSectionTitle" => $pathSectionTitle,
				"image" => $image,
			)
		);

		if ($page == "tiersLieux") 
			echo $this->renderPartial(
				"costum.views.custom.franceTierslieux.previewDataViz.tiersLieuxPreview", 
				array(
					"name" => $name, 
					"image" => $image, 
					"orgaId" => $orgaId, 
					"id" => $id, 
					"network" => $network,
					"networkKey" => $networkKey,
					// "survey" => $survey,
				)
			);

		if ($page == "reseaux")
			echo $this->renderPartial(
				"costum.views.custom.franceTierslieux.previewDataViz.reseauPreview", 
				array(
					"name" => $name, 
					"image" => $image,
					"page" => $page,
					// "where" => $where ?? [], 
					"sections" => $sections ?? [], 
					"count" => @$count, 
					"markerData" => $markerData,
					"networkKey" => $networkKey,
				)
			);

		if ($page == "tools")
			echo $this->renderPartial(
				"costum.views.custom.franceTierslieux.previewDataViz.toolsPreview", 
				array(
					"name" => $name,
					"image" => $image,
				)
		);
	?>
</div>
