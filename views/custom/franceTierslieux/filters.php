<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<div class="modal fase" id="downloading-processing" style="display:none">
    <div class="modal-dialog">
        <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">
                   <!-- <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span> -->
                </button>
            </div>
            <div class="modal-body">
				Téléchargement de l'étape en cours. Merci de patienter. Le temps de traitement est de l'ordre de la minute.
            </div>
        </div>
       </div>
    </div>
</div>

<div class="modal fade" id="chooseStep" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title text-white" id="myModalLabel">Choisissez l'étape à télécharger</h4>
          </div>
          <div class="modal-body">

            
              <div class="margin-top-10">
			    <label class="checkbox-inline">
					<input type="checkbox" name="idCard" id="idCard" value="idCard">
						Je souhaite télécharger les fiches d'identité des tiers-lieux, en plus de l'étape choisie
				</label><br/>
			    
			  </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            <button type="button" class="btn btn-primary chosenStep text-white" disabled>Ok</button>
          </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	// loadEvent : {
	 	// 	default : "scroll"
	 	// },
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : true 
            }
        },
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"]
	 	},
	 	filters : {
	 		scope :true,
	 		scopeList : {
				name : "Régions",
	 			params : {
	 				countryCode : ["FR","RE","MQ","GP","GF","YT"], 
	 				level : ["3"]
	 			}
	 		},
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Typologie",
	 			event : "tags",
	 			list : costum.lists.typePlace
	 		},
	 		// services:{
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Activités",
	 		// 	event : "tags",
	 		// 	list : costum.lists.services
	 		// },
	 		// greeting : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Accueil",
	 		// 	event : "tags",
	 		// 	list : costum.lists.greeting
	 		// },
	 		manageModel : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Portage",
	 			event : "tags",
	 			list : costum.lists.manageModel
	 		},
	 		// state : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Etat",
	 		// 	event : "tags",
	 		// 	list : costum.lists.state
	 		// },
			activités : {},
			équipement : {},
			lauréat :{},
	 		spaceSize : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Taille",
	 			event : "tags",
	 			list : costum.lists.spaceSize
	 		},
	 		
	 		// compagnon : {
	 		// 	view : "tags",
	 		// 	type : "tags",
	 		// 	name : "Compagnons",
	 		// 	event : "tags",
	 		// 	list : costum.lists.compagnon
	 		// },
	 		// certification : {
	 		// 	view : "dropdownList",
	 		// 	type : "tags",
	 		// 	name : "Lauréats Fabriques",
	 		// 	event : "tags",
	 		// 	list : costum.lists.certification
	 		// },
	 		// network : {
	 		// 	view : "tags",
	 		// 	type : "category",
	 		// 	name : "Réseaux",
	 		// 	event : "selectList",
	 		// 	field : "category"
	 		// }
	 	}
	};
	 
	//function lazyFilters(time){
	  //if(typeof searchObj != "undefined" )
	    //filterGroup = searchObj.init(paramsFilter);
	  //else
	    //setTimeout(function(){
	      //lazyFilters(time+200)
	    //}, time);
	//}
var filterSearch={};
var paramsExport={};
var mappingKeyName={
  "franceTierslieux1522023_1341_0": "AUTORISATION D’UTILISATION DES DONNÉES",
  "franceTierslieux1522023_1352_1": "FICHE D'IDENTITÉ",
  "franceTierslieux1522023_146_2": "INFORMATIONS GÉNÉRALES ",
  "franceTierslieux1522023_1549_3": "FONCIER",
  "franceTierslieux1822023_1721_4": "VOS ACTIVITÉS",
  "franceTierslieux1922023_1231_5": "PUBLICS",
  "franceTierslieux1922023_1231_6": "RESSOURCES HUMAINES",
  "franceTierslieux1922023_1417_7": "GOUVERNANCE",
  "franceTierslieux1922023_1523_8": "PARTENARIATS",
  "franceTierslieux2022023_732_9": "MODÈLE ÉCONOMIQUE",
  "franceTierslieux2022023_753_10": "PERSPECTIVES FUTURES"
};

function is_empty(__input) {
            return typeof __input==='undefined' || typeof __input==='object' && (__input===null || __input instanceof Array && __input.length===0) || typeof __input==='string' && __input==='';
        }

function html_special_chars(text) {
				return text
					.replace(/&/g, "&amp;")
					.replace(/</g, "&lt;")
					.replace(/>/g, "&gt;")
					.replace(/"/g, "&quot;")
					.replace(/'/g, "&#039;");
			}
			function exportCsvRecensement (post){
				// mylog.log("parsing post",JSON.parse(post));
				// return;
				const url = `${baseUrl}/survey/exportation/${post.split_steps ? 'multiple-file-csv-coform' : 'csv-coform'}`;
				ajaxPost(null, url, post, function (__response) {
					$('#csv-export span').removeClass();
					// if(typeof __response!="undefined" && typeof __response.metadata!="undefined" && typeof __response.metadata.name!="undefined"){
					// 	__response.metadata.name="";
					// }
					mylog.log("retour export",__response);
					if($('#chooseStep #idCard').prop("checked") == false){
						if(typeof __response!="undefined" && notNull(__response.orgadynform)){
							delete __response.orgadynform
						}
						// configExport.is_exporting_dynform=false;
					}
					$('#chooseStep #idCard').prop("checked",false);
					


					
					if(typeof __response!="undefined" && typeof __response.metadata!="undefined"){
						if(typeof __response.metadata.bodies!="undefined"){
    						var lengthFields=__response.metadata.bodies.length-1;
    						mylog.log("last field",lengthFields,__response.metadata.bodies[lengthFields]);
    						if(__response.metadata.bodies[lengthFields]["Alias"]=="RES_REGION"){
    						    // alert(__response.metadata.bodies[lengthFields]["Types"]);
    							__response.metadata.bodies[lengthFields]["Types"]="chaîne de caractères (str) à choix multiples (Réseau Bretagne Tiers-Lieux (Bretagne);Réseau La Compagnie des Tiers-Lieux (Hauts-de-France);Réseau La Rosée (Occitanie);Réseau La Réunion des Tiers-Lieux (La Réunion);Réseau Relief (Auvergne Rhône-Alpes);Réseau TILINO (Normandie);Réseau Tiers-Lieu Grand Est;Réseau Tiers-Lieux Bourgogne-Franche-Comté (BFC);Réseau Île-de-France Tiers-Lieux (Consortium : A+ c'est mieux, Actifs, le Collectif des tiers-lieux, Makers IDF);Aucun)";
						    }
					    }

						
					}

					if (post.split_steps) {
						let time_manager = 0;
						var nbExportedSteps=0;
						var nameExportedSteps="";
						$.each(__response, (__, __csv) => {
							const {heads, bodies, name} = __csv;
							var newName=name;
							if (!is_empty(heads) && !is_empty(bodies)) {
								// mylog.log("name attribution", !is_empty(heads), !is_empty(bodies),__, nbExportedSteps, notEmpty(nameExportedSteps), __response.metadata.name)
						        if(__=="metadata" && nbExportedSteps==1 && notEmpty(nameExportedSteps) && typeof __response!="undefined" && typeof __response.metadata!="undefined" && typeof __response.metadata.name!="undefined" ){
							        mylog.log("split",nbExportedSteps,nameExportedSteps,__);
									newName=__response.metadata.name+"_"+nameExportedSteps;
									// alert(newName);
						        }
								// setTimeout(() => {
									export_file(heads, bodies, newName);
								// }, time_manager * 500);
								// time_manager += 1;
								nbExportedSteps++;
								nameExportedSteps=newName;
							}
						});
						
					} else {
						// alert("else");
						const {heads, bodies} = __response;
						export_file(heads, bodies);
					}
					$("#downloading-processing").modal("hide");
				},
				function(error){
					toastr.warning("Un problème a été rencontré. Veuillez vous rapprocher de l'administrateur");
					$("#downloading-processing").modal("hide");
				}
				);   
			}

		    function export_file(__headers, __body, __specification = '') {
				if (!is_empty(json2csv) && !is_empty(json2csv.Parser)) {
                const parser = json2csv.Parser;
                const fields = __headers;
                const exportation_preparation = new parser({fields, delimiter : (notNull(document.getElementById('csv_delimiter'))) ? document.getElementById('csv_delimiter').value : ';'});
                const csv = exportation_preparation.parse(__body);
                let filename = (notNull(document.getElementById('csv_exportation_name'))) ? document.getElementById('csv_exportation_name').value : "BDFTL_2023";
                if (!is_empty(filename)) {
                    if (!is_empty(__specification)) filename += '_' + __specification;
                    const splited_filename = filename.split('.');
                    // const file_signature = moment().format('YYYYMMDD-HHmm');
					const file_signature = '';
                    filename = filename.length>200 ? filename.substring(0, 200) + '...' : filename;
                    filename = splited_filename.length>1 && splited_filename[splited_filename.length - 1].toLowerCase()==='csv' ? `${splited_filename.slice(-1).join('')}_${file_signature}.csv` : `${filename}${file_signature}.csv`;
					mylog.log("filename",splited_filename,filename);
					filename=slugify(filename).replaceAll(/-/g,"_");

                    //var universalBOM = "\uFEFF";
                    const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
                        type : 'text/csv;charset=utf-8;'
                    });
                    if (!is_empty(navigator) && !is_empty(navigator.msSaveBlob)) { // IE 10+
                        navigator.msSaveBlob(blob, filename);
                    } else {
                        const link = document.createElement('a');
                        if (link.download!==undefined) { // feature detection
                            // Browsers that support HTML5 download attribute
                            const url = URL.createObjectURL(blob);
                            link.href = url;
                            link.download = filename;
                            link.style.display = 'hidden';
                            document.body.appendChild(link);
                            link.dispatchEvent(
                                new MouseEvent('click', {
                                    bubbles : true,
                                    cancelable : true,
                                    view : window
                                })
                            );
                            document.body.removeChild(link);
                            URL.revokeObjectURL(url);
                        }
                    }
                } else {
                    toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
                }
            }
        }

		function multiWindowOpen(url1,url2){
			window.open(url1,"_blank");
			window.open(url2,"_blank");

		}
		function prepareRecensement2023(data){
			    

			if(costum.isCostumAdmin || location.hash=="#exportAccess"){
				var sort_by_name = function(a, b) {
                    // mylog.log("vio",a.childNodes[0].innerHTML.toLowerCase());
                    return a.childNodes[0].innerHTML.toLowerCase().localeCompare(b.childNodes[0].innerHTML.toLowerCase());
                }

			    mylog.log('zone download public filter', data);
			    // fObj.filters.lists.scopeList[k] = data ;
			    var strDL = "";
		        strDL += '<div class="btn-group margin-top-5">'+  
				'<button type="button" style="background: #FF286B;border: none;font-weight: bolder;" class="btn btn-default dropdown-toggle text-white margin-left-5" type="button" id="dropdownDLFicheIdentiteCarto" data-toggle="dropdown" >'+
								'<i class="fa fa-download text-white"></i> Télécharger BDD - Cartographie dynamique'+
							'</button>'+
							'<ul style="height:50vh;overflow-y: scroll;" class="tl-carto dropdown-menu dropdown-menu-left">';
							    // strDL += '<li class="text-left"><a class="pull-left" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux" target="_blank">France entière</a></li>';
								// strDL += '<li class="text-left"><a class="download-orga-profile pull-left" href="javascript:;">France entière</a></li>';
								$.each(data, function(kV, vV){
									strDL += '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([baseUrl+`/upload/communecter/organizations/5ec3b895690864db108b46b8/file/BDDCartographieInteractiveMetadonnees.csv`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/`+kV+`/extraFormat/csv/fileName/BDD_Cartographie_Interactive_Fiche_d_identite_`+slugify(vV.name).replaceAll(/-/g,"_")])+' href="javascript:;" >'+vV.name+'</a></li>';
									// strDL += '<li class="text-left"><a class="download-orga-profile pull-left" data-level3="'+kV+'" href="javascript:;">'+vV.name+'</a></li>';
								});
				strDL +=  '</ul></div>';
				$("#menuTopCenter .cosDyn-app").html(strDL);
				

                var list = $(".tl-carto").find("li").get();
                list.sort(sort_by_name);
                for (var i = 0; i < list.length; i++) {
                    list[i].parentNode.appendChild(list[i]);
					mylog.log("noeud parent",list[i].parentNode);
                }
				// prepend France entière
				var franceHtml = '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([baseUrl+`/upload/communecter/organizations/5ec3b895690864db108b46b8/file/BDDCartographieInteractiveMetadonnees.csv`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/BDD_Cartographie_Interactive_Fiche_d_identite`])+' href="javascript:;" >France Entière</a></li>';
                $("#dropdownDLFicheIdentiteCarto + .tl-carto").prepend(franceHtml);

                // bouton suivant plus affiché
				var strDL =  '<div class="btn-group margin-top-5">'+
				            '<button type="button" style="background: #FF286B;border: none;font-weight: bolder;" class="btn btn-default dropdown-toggle text-white margin-left-5" type="button" id="dropdownDLFicheIdentiteRecensement" data-toggle="dropdown" >'+
								'<i class="fa fa-download text-white"></i> Télécharger Fiches identité des Tiers-Lieux (recensés en 2023)'+
							'</button>'+
							'<ul style="height:50vh;overflow-y: scroll;" class="tl-recensement dropdown-menu dropdown-menu-left">';
							    // strDL += '<li class="text-left"><a class="pull-left" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux" target="_blank">France entière</a></li>';
								// strDL += '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([`https://conextcloud.communecter.org/s/3mNHzQCR3GdZawx/download`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux`])+' href="javascript:;" >France Entière</a></li>';
								strDL += '<li class="text-left"><a class="download-orga-profile pull-left" href="javascript:;">France entière</a></li>';
								$.each(data, function(kV, vV){
									// strDL += '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([`https://conextcloud.communecter.org/s/3mNHzQCR3GdZawx/download`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/`+kV+`/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux`])+' href="javascript:;" >'+vV.name+'</a></li>';
									strDL += '<li class="text-left"><a class="download-orga-profile pull-left" data-level3="'+kV+'" href="javascript:;">'+vV.name+'</a></li>';
								});
				strDL +=  '</ul></div>';
				// $("#menuTopCenter .cosDyn-app").append(strDL);
				

                var list = $(".tl-recensement").find("li").get();
                list.sort(sort_by_name);
                for (var i = 0; i < list.length; i++) {
                    list[i].parentNode.appendChild(list[i]);
					mylog.log("noeud parent",list[i].parentNode);
                } 


				// var strDLR = ``;
				// 		// strDL +=  '<a class="pull-left btn-menu-connect" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/'+costum.address.level3+'/extraFormat/csv" target="_blank">Téléchargement></a>';
				// 		strDLR +=  `<a id="download-recensement" style="background: #FF286B;border: none;font-weight: bolder;" class="text-white btn btn-default pull-left" href="javascript:;"><i class="fa fa-download text-white"></i> Base de données des tiers-lieux 2023</a>`;
                //         // onclick="exportCsvRecensement(`+JSON.stringify(JSON.stringify(post))+`)"
				// 		$("#menuTopCenter .cosDyn-app").append(strDLR);

				var strDL =  '<div class="btn-group margin-top-5">'+
				            '<button type="button" style="background: #FF286B;border: none;font-weight: bolder;" class="btn btn-default dropdown-toggle text-white margin-left-5" type="button" id="dropdownDLFicheIdentiteRecensement" data-toggle="dropdown" >'+
								'<i class="fa fa-download text-white"></i> Télécharger BDD - Recensement tiers-lieux 2023'+
							'</button>'+
							'<ul style="height:50vh;overflow-y: scroll;" class="recensement-global dropdown-menu dropdown-menu-left">';
							    // strDL += '<li class="text-left"><a class="pull-left" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux" target="_blank">France entière</a></li>';
								// strDL += '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([`https://conextcloud.communecter.org/s/3mNHzQCR3GdZawx/download`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/0/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux`])+' href="javascript:;" >France Entière</a></li>';

								$.each(data, function(kV, vV){
									// strDL += '<li class="text-left"><a class="pull-left open-links-identity" data-links='+JSON.stringify([`https://conextcloud.communecter.org/s/3mNHzQCR3GdZawx/download`,baseUrl+`/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/`+kV+`/extraFormat/csv/fileName/Fiches_identité_de_Tiers_Lieux`])+' href="javascript:;" >'+vV.name+'</a></li>';
									strDL += '<li class="text-left"><a class="download-recensement pull-left" data-level3="'+kV+'" href="javascript:;">'+vV.name+'</a></li>';
								});
				strDL +=  '</ul></div>';
				$("#menuTopCenter .cosDyn-app").prepend(strDL);

                var list = $(".recensement-global").find("li").get();
                list.sort(sort_by_name);
                for (var i = 0; i < list.length; i++) {
                    list[i].parentNode.appendChild(list[i]);
					mylog.log("noeud parent",list[i].parentNode);
                } 

				var strFranceEntiere = '<li class="text-left"><a class="download-recensement pull-left" href="javascript:;">France entière</a></li>';
                $("#dropdownDLFicheIdentiteRecensement + .recensement-global").prepend(strFranceEntiere);

				coInterface.setTopPosition();

			    ajaxPost(null, baseUrl+'/'+moduleId+'/default/render?url=survey.views.custom.exportation-csv.coform',
					{form : "63e0a8abeac0741b506fb4f7"},
					function(data){
                        $("#page-top").append(data);
						var post={
							excluded_inputs : [
								"tpls.forms.titleSeparator",
								"tpls.formssectionTitle",
								"tpls.forms.sectionDescription",
								"tpls.forms.cplx.list",
								"tpls.forms.costum.cressReunion.element" ,
								"tpls.forms.cplx.validateStep",
								"tpls.forms.costum.franceTierslieux.validateStepFtl",
								"tpls.forms.costum.franceTierslieux.validateStep",
								"tpls.forms.sectionTitle"
							], 
							is_exporting_following_answer: false,
							following_answer_id: "followingAnswerId",
							is_exporting_dynform: true,
							export_dynform_after: "franceTierslieux1522023_1341_0lf2j30fgrikequtany",
							is_exporting_answer_id: true,
							completed_answers_only: false,
							is_exporting_user_email: true,
							is_custom_checkbox_aside: true,
							split_steps: true,
							settings : {
								address: "multicolumn",
								checkbox: "multicolumn",
								finder: "complete",
								finder_column: "multicolumn"
							},
							query :{
								form : "63e0a8abeac0741b506fb4f7",
								answers : {
									'$exists' : true
								}   
							},
							steps:{},
							champs:[],
							// region_filters:[costum.address.level3Name],
						};
							$('.csv-field-export').each(function () {
								const champ = this;
								mylog.log("champ this",champ);
								if (champ.checked) {
									post.steps[champ.dataset.step] = post.steps[champ.dataset.step] ? post.steps[champ.dataset.step] : [];
									post.steps[champ.dataset.step].push({
										value : champ.value,
										label : champ.dataset.label,
										type : champ.dataset.type,
									});
				
									// lately to delete
									post.champs.push({
										step : champ.dataset.step,
										label : champ.dataset.label,
										type : champ.dataset.type,
										value : champ.value
									});
								}
							});
							mylog.log("config export",post);
							// delete l'étape autorisation
							delete post.steps["franceTierslieux1522023_1341_0"];
							delete post.steps["franceTierslieux1522023_1352_1"];
							$('#csv_exportation_name').val("BDFTL_2023");

							var radioOpt='<div>';

							$.each(post.steps,function(key,val){
								radioOpt+='<label class="radio-inline">'+
								'<input type="radio" name="stepChoice" id="'+key+'" value="'+key+'">'+
								mappingKeyName[key]+
								'</label><br/>';

							});
							radioOpt+='</div>';

							$("#chooseStep .modal-body").prepend(radioOpt);

							paramsExport=$.extend({},post);


			// 				<label class="radio-inline">
            //   <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 1
            // </label>

					    // mylog.log("data params post strDLR",post);
						// var strDLR = ``;
						// // strDL +=  '<a class="pull-left btn-menu-connect" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/'+costum.address.level3+'/extraFormat/csv" target="_blank">Téléchargement></a>';
						// strDLR +=  `<a id="download-recensement" style="background: #FF286B;border: none;font-weight: bolder;" class="text-white btn btn-default pull-left" href="javascript:;"><i class="fa fa-download text-white"></i> Base de données des tiers-lieux 2023</a>`;
                        // // onclick="exportCsvRecensement(`+JSON.stringify(JSON.stringify(post))+`)"
						// $("#menuTopCenter .cosDyn-app").append(strDLR);

						$(".download-recensement").off().on("click",function(){
							$("#chooseStep").modal("show");
							var chosenRegion=null;
							chosenRegion=$(this).data("level3");
							// alert(chosenRegion);
							// alert(notEmpty(chosenRegion))


							if(notEmpty(chosenRegion)){
								paramsExport.region_filters=[chosenRegion];
							}else{
								delete paramsExport.region_filters;
							}
							
						

							$('#chooseStep input[type=radio]').off().on("click",function(){
								var configExport=jQuery.extend(true,{},paramsExport);
								$('#chooseStep .chosenStep').removeAttr("disabled");
    								var onlyStep=$('#chooseStep input[type=radio]:checked').val();
    							
    							
    							$.each(configExport.steps,function(key,val){
    								if(key!==onlyStep){
    									delete configExport.steps[key];
    								}
    
    							});
    							mylog.log("params to send",configExport,post);
								$('#chooseStep .chosenStep').off().on("click",function(){
									
									// alert(onlyStep);
									$("#chooseStep").modal("hide");
									if(notEmpty(chosenRegion)){
        								configExport.region_filters=[chosenRegion];
        							}else{
        								delete configExport.region_filters;
        							}

									
									mylog.log("configExport",configExport);
									exportCsvRecensement(configExport);
									$("#downloading-processing").modal("show");
									$('#chooseStep input[type=radio]').prop('checked', false);
									$('#chooseStep .chosenStep').attr("disabled",true);
        						});
    							
    						});

							
							
							// exportCsvRecensement(post);
						
							// $("#csv-exportation").modal("show");
						});
                    },
                "html");
		    }
		}
	jQuery(document).ready(function() {
		$("#page-top").append("<div id='coform-params'></div>");
		filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();
	//	lazyFilters(0);

	var myInterval=setInterval(function(){ 
		mylog.log("setinterval repeat");
	    if(Object.keys(filterSearch.filters.lists.scopeList.scopeList).length>0 && Object.keys(filterSearch.results.count).length>0){
			mylog.log("setinterval last");
			prepareRecensement2023(filterSearch.filters.lists.scopeList.scopeList);
			clearInterval(myInterval);
			$(".open-links-identity").off().on("click",function(){
				mylog.log("linkssss",$(this).data("links"));
				var links=$(this).data("links");
				bootbox.confirm({
                    message: '<span style="font-size:1.5em";>Confirmez-vous vouloir télécharger la base de données de la cartographie des tiers-lieux au format (.csv) ? </span></br></br><span style="font-style:italic;"><span style="font-weight:bold;">NB </span>: Après avoir cliqué sur "Oui", vérifiez que vous avez bien téléchargé deux fichiers (le fichier listant les fiches d\'identité + le fichier des métadonnées détaillant le nom des colonnes). </br>Si ce n\'est pas le cas, il se peut que votre navigateur internet ait bloqué le téléchargement.</span>',
                    buttons: {
                        confirm: {
                        label: 'Oui',
                        className: 'btn'
                        },
                        cancel: {
                            label: 'Non',
                            className: 'btn-danger text-white'
                        }
                    },
                    callback: function (result) {
                        if(result){
                            for (let i = 0; i < links.length; i++) {
                                mylog.log("link a voir",links[i]);
                                window.open(links[i], "_blank");
                            }
            			}
                    }
                });
			});

			$(".download-orga-profile").off().on("click",function(){
				var settings={
							// excluded_inputs : [
							// 	"tpls.forms.titleSeparator",
							// 	"tpls.formssectionTitle",
							// 	"tpls.forms.sectionDescription",
							// 	"tpls.forms.cplx.list",
							// 	"tpls.forms.costum.cressReunion.element" ,
							// 	"tpls.forms.cplx.validateStep",
							// 	"tpls.forms.costum.franceTierslieux.validateStepFtl",
							// // 	"tpls.forms.costum.franceTierslieux.validateStep",
							// // 	"tpls.forms.sectionTitle"
							// // ], 
							is_exporting_following_answer: false,
							// following_answer_id: "followingAnswerId",
							is_exporting_dynform: true,
							// export_dynform_after: "franceTierslieux1522023_1352_1le5ofi0x733rc1gmbk4",
							is_exporting_answer_id: false,
							completed_answers_only: true,
							is_exporting_user_email: false,
							is_custom_checkbox_aside: true,
							split_steps: true,
							settings : {
								address: "multicolumn",
								checkbox: "multicolumn",
								finder: "complete",
								finder_column: "multicolumn"
							},
							query :{
								form : "63e0a8abeac0741b506fb4f7",
								answers : {
									'$exists' : true
								}   
							},
							steps:{},
							champs:[],
							// region_filters:[costum.address.level3Name],
						};
					if($(this).data("level3")!="undefined")	{
						settings.region_filters=[$(this).data("level3")];
					}
					exportCsvRecensement(settings);

			});
	    }}
	, 1000);

// S'arrête après 5 secondes
// setTimeout(() => { clearInterval(timerId); alert('stop'); }, 5000);

	// var paramsDL={
	// 		countryCode : ["FR","RE","MQ","GP","GF","YT"], 
	//  		level : ["3"]
	// 	};
		// ajaxPost(null,
		// 	baseUrl + '/co2/search/getzone/',
		// 	paramsDL, 
		
		
		
		
		
	});

</script>



