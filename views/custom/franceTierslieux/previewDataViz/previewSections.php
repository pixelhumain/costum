<?php
	$countMenu = 0;
?>
<style>
    .vertical-menu {
		padding-right: 0px !important;
	}
	.vertical-menu ul li {
		margin-top: 15px;
	}
	.vertical-menu ul li a{
		text-decoration: none;
	}
	/* .listLeftBtn .active a {
		border-left: solid 4px ;
    	padding-left: 12px ;
		color: black !important;
	} */
	.listLeftBtn .active a {
		/* border-left: solid 4px ; */
    	padding-left: 12px ;
		/* color: #fff !important;		 */
		color: #8DBC21 !important;
	}

	.all-economic-model-btn {
		display: flex;
    	justify-content: center;
	}

	.all-economic-model-btn button{
		margin: 2px;
	}

	.tab-pane {
		margin-bottom: 100px;
	}

	.listLeftBtn a[aria-expanded="false"] {
		text-decoration: none;
		/* color: #737f8b; */
		color: #244A58;
	}
	.all-content-prev .tab-content {
		overflow-y: scroll; 
		height: 78vh;
	}

	.parent-tab {
		cursor: pointer;
	}

	/* dark */

	/* .content-img-profil-preview {
		margin-top: 61.5px;
	}
	.img-responsive {
		margin: auto;
		box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);
	} */
	.listLeftBtn {
		list-style: none; 
		padding-left: 0px;
		height: 85vh;
		overflow: auto;
		overflow-x: auto;
	}
	/* .toolsMenu {
		background: #e3e3e3;
	} */

	/* dark */
	.toolsMenu {
		/* background: #1C0C5B; */
		/* background: #4A6FA5; */
		background: white;
		color: black;
		/* color: white; */
	}

	.enlarge {
		top:0px !important; 
		left:0px !important
	}

	/* dark */
	.vertical-menu {
		/* background: #270082; */
		/* background: #394648; */
		/* color: white !important; */
		background: #EDF0F2;
	}

	.goToItemPage {
		/* background: #4623C9 !important; */
		background: #8AC4C5;
	}

	.tool-image {
		float: left;
		margin-right: 10px !important;
    	width: 10%;
	}
	.text-network {
		line-height: inherit;
		color : #3069a3;
	}
	.section-pie-name {
		color : black;
	}
	.navigator-prev-contents .vertical-menu , .navigator-prev-contents .listLeftBtn{
		height: 65vh !important; 
		/* margin: 1%; */
	}

	.all-content-prev {
		box-shadow: -10px -5px 34px 0px #0000001C;
    	border-radius: 14px;
	}
	/* .tab-pane {
		margin-bottom: 200px !important;
	} */

	<?php if($page == "tiersLieux"){ ?>
	.btn-hexa-container{
		background: white;
		border: 1px solid;
		padding: 0;
		text-align: left;
	}
	.btn-hexa-container.btn-primary{
		color: white;
		background-color: #2C3E50;
		border-color: #2C3E50;
		font-weight: 700;
	}

	.btn-hexa-container button{
		background: none;
		border: none;
		padding: 6px 6px 6px 12px;
	}
	.tab-pane #hexagone{
		position: relative;
	}
	.filters-container-circle #hexagone-filter-container{
		display: flex;
		flex-direction: column;
		width: 300px;
		background-color: white;
	}
	.hexagone-child-container.in{
		display: flex;
		flex-direction: column;
		text-align: left;
	}
	.hexagone-child-container{
		padding: 0px 15px;
	}

	.hexagone-child-container .btn-hexagone-zoomer{
		text-align: left;
	}

	.btn-hexa-container .btn-form-modal{
		color: #8DBC21;
	}
	#hexagone {
		background: white;
		border-radius: 20px;
		padding: 20px;
		overflow-y: auto;
		height: 75vh;
	}
	 <?php } ?>
</style>

<!-- <div class="col-xs-12 padding-10 preview-answers toolsMenu">
	<div class="col-md-6 title-tiers">
		<img class="img-responsive shadow2 tool-image" src="<?= $image ?>">
		<h4 class="tr-toolbar-title"><?= str_replace("#", "'", $name) ?></h4>
	</div>
	<button class="btn btn-default close-preview-answers pull-right btn-close-preview">
		<i class="fa fa-times"></i>
	</button>
	<button class="btn btn-default full-width margin-right-10 pull-right">
		<i class="fa fa-window-maximize"></i>
	</button>
	<?php 
		if ($page == "tiersLieux") 
			echo '<a href="javascript:" class="goToItemPage lbh btn btn-primary pull-right margin-right-10">Aller sur la page</a>';
	?>
</div> -->
<div class="form-answer-content" ></div>
<?php 
	if($page == "tiersLieux"){ ?>
	<div id="hexagone" class="col-xs-12" hidden>
		<div class="filters-container-circle hidden-sm hidden-xs" style="position: absolute; right: 0; overflow: auto">
			<div id="hexagone-filter-container" class="margin-top-10 text-center"></div>
		</div>
	</div>
<?php	}
?>
<div class="col-xs-12 map-content-prev" hidden>
	<div id="map-preview-filter"></div>
	<div id="map-preview"></div>
</div>
<div class="col-xs-12 vertical-menu all-content-prev">
	<div class="col-xs-3" style="padding: 0;">
		<ul data-ref="" class="listLeftBtn">
			<?php foreach ($sections as $key => $value) {
				$aria = ($countMenu == 0) ? "true" : "false";
				if (gettype($value) != "array") {
					$hide = ($key == "allForm" || $key == "hexagone") ? "hidden" : "";
					echo '<li '.$hide.'><a data-toggle="tab" id="section-'.$key.'" href="#menu'.$countMenu.'" class="section-button" aria-expanded="'.$aria.'">'.$value.'</a><li>';
				}
				else {
					$stepPattern = '/^(?:.*?)(franceTierslieux[\d_]+)(?:.*)$/';
					preg_match($stepPattern, $key, $matches);
					$path = (isset($matches) && isset($matches[0]) && isset($matches[1])) ? "answers.".$matches[1].".".$matches[0] : "";
					$label = $pathSectionTitle[$path] ?? $key;
					$parenSectionK = (gettype(strpos($label, "#")) == "integer" && strpos($label, "#") == 0) ? "onclick='loadNetworkPie.loadParentSection(\"".$key."\", \"". str_replace("#", "", $label) ."\")'" : "";
					$mapClass = (gettype(strpos($label, "#")) == "integer" && strpos($label, "#") == 0) ? "map-section" : "";
					echo '<li><a data-count="'.$countMenu.'" data-sub="sub-'.$countMenu.'" style="cursor: pointer;" '. $parenSectionK .' class="margin-bottom-15 parent-tab '. $mapClass .'" data-name="'.$label.'" aria-expanded="false">'. str_replace("#", "", $label).'</a>';
					echo '<ul style="list-style: none;" data-ref="sub-'.$countMenu.'" class="sub-menu-section sub-'.$countMenu.' '.str_replace(["'", " "], "_", $label).'" hidden>';
						foreach ($value as $subKey => $subValue) {
							if ($path != "") {
								if (isset($subValue) && $subValue > 0) {
									echo '<li><a data-toggle="tab" class="filter-map-section"  data-title="'. $label .'" data-key="'.$subKey.'" data-path="'.$path.'" href="#menu'.$countMenu.'" aria-expanded="false">'.$subKey.' ('.$subValue.') </a><li>';
								}			
							} else {
								echo '<li><a data-toggle="tab" href="#menu'.$countMenu.'" aria-expanded="false">'.$subValue.'</a><li>';
							}
							$countMenu ++;
						}
					echo '</ul>';
					echo '<li>';
				}
				$countMenu ++;
			}?>
		</ul>
	</div>

  <div class="col-xs-9 tab-content container" style="padding-left: 30px; padding-right: 0px !important;">
		<?php 
			$countMenu = 0;
			foreach ($sections as $key => $value) {
				$activeContent = ($countMenu == 0) ? "active in" : "";
				if (gettype($value) != "array") {
					echo '<div id="menu'.$countMenu.'" class="tab-pane fade '.$activeContent.'">';
					echo '<div id="'.$key.'" class="col-xs-10  no-padding margin-bottom-20 margin-top-20"></div>';
					echo '</div>';
				} else {
					foreach ($value as $subKey => $subValue) { 
						echo '<div id="menu'.$countMenu.'" class="tab-pane fade '.$activeContent.'">';
						echo '<div id="'.$subKey.'" class="col-xs-10  no-padding margin-bottom-20 margin-top-20"></div>';
						echo '</div>';
						$countMenu ++;
					}
				}
				
				$countMenu ++;
		}?>
  </div>
</div>

<script>
	var loadNetworkPie = {
		sectionsData : <?= json_encode($sections ?? []) ?>,
		loadParentSection : function (sectionKey, name) {
			$(".canvasContent").hide();
			$("#activityLinkMap").hide();
			$(".tab-pane.fade.active.in").removeClass("active").removeClass("in");
			$("#pieContent").addClass("active").addClass("in");
			$("#titleContent").html(`<div><h4 class="text-center text-network"> Voici un diagramme circulaire représentant les différents composants de la section : <b class="section-pie-name"> ${name} </b></h4></div>`);
			this.loadPieChart("#sectionPie", this.sectionsData[sectionKey]);
		},
		loadPieChart : function (selector, chartData={}) {
			var labels = Object.keys(chartData);
			var data = Object.values(chartData);
			var chartType = "pie"
			var chartConfig = {
				type: chartType,
				data: {
					labels: labels,
					datasets: [{
						data: data,
						// backgroundColor: backgroundColors
					}]
				},
				options: {
					responsive: true
				}
			}
			var canvas = document.getElementById("mapSectionPie");
			var chart = Chart.getChart(canvas);
			if (chart) {
				chart.destroy();
			}
			var chartJs = new Chart(ctx, chartConfig);	
		},
	}
	$("a.parent-tab").click(function(){
		let ulClass = $(this).data("sub");
		let count = $(this).data("count");
		let dataName = ($(this).data("name") != undefined) ? $(this).data("name") : "";
		let delay = 150;
		let hideStatus = $("." + ulClass).is(':hidden');
		$(".parent-tab").attr("style", "color: #244A58")
		$(".sub-menu-section").hide();
		$(this).attr("style", "color: black")
		if (hideStatus) {
			$("." + ulClass).show();
		} else {
			$("." + ulClass).hide();
		}
		$(".listLeftBtn  .active a").attr("aria-expanded", "false");
		$(".listLeftBtn  .active ").attr("class", "");
		if (!dataName.includes("#")) {
			setTimeout(function() {
				$("[href='#menu"+count+"'], .filter-map-section [href='#menu"+count+"']").trigger('click');
			}, delay);
		}  
	})

	if($("#modal-preview-coop").attr("class").includes("enlarge")){
		$(".preview-answers").find(".full-width")
			.html('<i class="fa fa-window-restore"></i>')
			.addClass("initial-width")
			.removeClass("full-width");
	}	

	$(".preview-answers").on('click','.full-width', function() { 
		$("#modal-preview-coop").addClass("enlarge")
		$(this)
			.html('<i class="fa fa-window-restore"></i>')
			.addClass("initial-width")
			.removeClass("full-width");
	})

	$(".preview-answers").on('click', ".initial-width", function() { 
		$("#modal-preview-coop").removeClass("enlarge")
		$(this)
			.addClass("full-width")
			.html('<i class="fa fa-window-maximize"></i>')
			.removeClass("initial-width")
	})

	$("#info").parent().attr("style", "justify-content: space-between !important;");

</script>