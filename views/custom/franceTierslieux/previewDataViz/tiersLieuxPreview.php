<?php 
    $cssAnsScriptFilesTheme = array(
		'/plugins/jQCloud/dist/jqcloud.min.js',
		'/plugins/jQCloud/dist/jqcloud.min.css',
		"/plugins/d3/d3.v3.min.js",
		"/plugins/Chart.js/Chart.v4.js",
		"/plugins/rater/rater-js.js"
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
	$cssAnsScriptFiles = array(
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
		"/js/franceTierslieux/dataviz/radarChart.js",
		"/js/franceTierslieux/dataviz/hexagonTools.js",
		"/css/franceTierslieux/navigatorThirdPlace/thirdPlacePreview.css",
		"/css/franceTierslieux/radarChart.css",
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
	$graphAssets = [
		'/js/hexagon.js'
	];
	HtmlHelper::registerCssAndScriptsFiles(
		$graphAssets,
		Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
	);
	$isThisUserAdmin = (Authorisation::isElementAdmin($orgaId, Organization::COLLECTION, $_SESSION["userId"], false, false) || Authorisation::isUserSuperAdmin($_SESSION["userId"]));
?>
<style>
	.d-none {
		display: none !important;
	}
</style>
<!-- <style>
	.btn-hexa-container{
		background: white;
    	border: 1px solid;
		padding: 0;
		text-align: left;
	}
	.btn-hexa-container.btn-primary{
		color: white;
		background-color: #2C3E50;
		border-color: #2C3E50;
		font-weight: 700;
	}

	.btn-hexa-container button{
		background: none;
    	border: none;
		padding: 6px 6px 6px 12px;
	}
	.tab-pane #hexagone{
		position: relative;
	}
	.filters-container-circle #hexagone-filter-container{
		display: flex;
		flex-direction: column;
		width: 300px;
		background-color: white;
	}
	.hexagone-child-container.in{
		display: flex;
		flex-direction: column;
		text-align: left;
	}
	.hexagone-child-container{
		padding: 0px 15px;
	}

	.hexagone-child-container .btn-hexagone-zoomer{
		text-align: left;
	}

	.btn-hexa-container .btn-form-modal{
		color: #8DBC21;
	}
</style> -->

<script>
	var previewFormRefresh, callbackCreate, criteriaDescriptions;
	$(function() {
		criteriaDescriptions = getNavigatorElement.getCriteria();
		let preview = {
			network : "<?= $network ?>",
			organizationId : "<?= $orgaId ?>",
			id : "<?= $id ?>",
			name : "<?= $name ?>",
			getOrganizations : {},
			getSraffRatting : {},
			getElevel3AddressAnswersquipmentRating : {},
			allCriteria : {},
			networkKey : "<?= $networkKey ?>",
			getAnswers : {...JSON.parse(localStorage.getItem("surveyAnswer")), answerId : "<?= $id ?>"},
			isAuthorized : <?php if ($isThisUserAdmin == true) echo 0; else echo 1 ?>,
			activityClass : "activityBtn",
			satisfactionTrClass : "satisfactionTrBtn",
			listToolsClass : "listToolsBtn",
			allSuplmentData : {},
			toolDetail : "",
			getAllAnswers : function (orgaId = "") {
				var data = showAllNetwork.getAllAnswers(orgaId);
				if (typeof data == "object" && Object.keys(data).length > 0) {
		 			preview.allSuplmentData = (data?.results != undefined) ? data.results : {};
		 			preview.getOrganizations = (data?.orgaData != undefined) ? data.orgaData : {};
				}
			},
			init : function () {
				preview.actions.getSurveyData();
				preview.actions.load();
				preview.views.load();
				preview.events.load();
			},
			events : {
				load : function () {
					preview.events.sectionButtons();
					preview.events.showForms();
					preview.events.showHexagone();
				},
				showForms : function () {
					const checkFromsBtn = () => {
						if ($(".btn-list-forms").length > 0) {
							clearInterval(checkMapInterval);
							if (preview.isAuthorized == 1) {
								$(".btn-list-forms").remove();
							} else {
								$(".btn-list-forms").click(function() {
									$("#section-allForm").click();
								})
							}
						}
					}
					const checkMapInterval = setInterval(checkFromsBtn, 20);
				},
				showHexagone : function () {
					const checkFromsBtn = () => {
						if ($(".btn-hexagone").length > 0) {
							clearInterval(checkHexagoneBtnInterval);
							if (preview.isAuthorized == 1) {
								$(".btn-hexagone").remove();
							} else {
								$(".btn-hexagone").click(function() {
									var frequentationBtnAndSup = preview.actions.generateButton(getNavigatorElement.suplment.frequentation.id, getNavigatorElement.suplment.frequentation.path, "frequentationBtn","LA FRÉQUENTATION");
									var equipmentSuplement = preview.actions.generateButton(getNavigatorElement.suplment.economicModel.id, getNavigatorElement.suplment.economicModel.path, "equipmentBtn","LES ÉQUIPEMENTS");
									var activityFormButton = preview.actions.generateButton(getNavigatorElement.suplment.avtivity.id, getNavigatorElement.suplment.avtivity.path, preview.activityClass, "MODÈLE ÉCONOMIQUE");
									var satisfactionSupl = preview.actions.generateButton(getNavigatorElement.suplment.satisfaction.id,getNavigatorElement.suplment.satisfaction.path,preview.satisfactionTrClass,"SATISFACTION", userId);
									// var allSatisfactionSupl = preview.actions.generateButton(getNavigatorElement.suplment.satisfaction.id,getNavigatorElement.suplment.satisfaction.path,preview.satisfactionTrClass);
									var listToolsForm = preview.actions.generateButton(getNavigatorElement.suplment.tool.id, getNavigatorElement.suplment.tool.path, preview.listToolsClass, "LISTE DES OUTILS");
									var surveyFormButton = preview.actions.generateButton(getNavigatorElement.surveyData.formId, getNavigatorElement.surveyData.path, "recencementClass", "RECENSEMENT");
									var networkSuplement = preview.actions.generateButton(getNavigatorElement.suplment.networkSupl.id, getNavigatorElement.suplment.networkSupl.path, "networkSuplClass", "RESEAUX");
									let name = $(this).data("name"), page = $(this).data("page");
									if ($(".btn-list-forms").length > 0) 
										$(".btn-list-forms").hide();
									if ($(".btn-hexagone").length > 0) 
										$(".btn-hexagone").hide();
									$("#hexagone").show();
									$(".vertical-menu.all-content-prev").hide();
									$(".btn-prev-map").attr("onclick", "showAllNetwork.openInitialPrevGraph('"+name+"', '"+page+"')");
									$(".btn-prev-map").html(`<i class="fa fa-arrow-circle-left" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Revenir aux sections`);
									frequentationBtnAndSup = frequentationBtnAndSup.refresh();
									equipmentSuplement = equipmentSuplement.refresh();
									activityFormButton = activityFormButton.refresh();
									satisfactionSupl = satisfactionSupl.refresh();
									listToolsForm = listToolsForm.refresh();
									surveyFormButton = surveyFormButton.refresh();
									networkSuplement = networkSuplement.refresh();
									preview.views.answerInformation.showHexagone({
										"listTools": listToolsForm,
										"activity": activityFormButton,
										"survey" : surveyFormButton,
										"frequentation": frequentationBtnAndSup,
										"equipement": equipmentSuplement,
										"network": networkSuplement,
										"satisfaction": satisfactionSupl
									});
								})
							}
						}
					}
					const checkHexagoneBtnInterval = setInterval(checkFromsBtn, 20);
				},
				sectionButtons : function() {
					$('#resumeBarContent, .resumeBtnClass').hide();
					$('.sub-menu-section li a').on('click', function () {
						$('.map-preview-tiers').hide();
						$('#resumeBarContent, .resumeBtnClass').hide();
					}) 
					// enlever le section reseau lorsqu'il n'y a pas de reseau
					if (preview.network == undefined || preview.network == "") {
						$("#section-reseaux").parent().remove();
					}
					$('.listLeftBtn > li >  .section-button').on('click',function() { 
						$(".parent-tab").attr("style", "color: #244A58 !important")
						$('#resumeBarContent, .resumeBtnClass').hide();
						$(".sub-menu-section").hide();
						$('.map-preview-tiers').hide();
					})

					$('.listLeftBtn > li >  #section-reseaux').on('click',function() {
						$("#mapContainerTiers").show();
						$(".parent-tab").css("color", "#244A58")
						coInterface.showLoader("#mapContainerTiers")
						$('#resumeBarContent, .resumeBtnClass').hide();
						$(".sub-menu-section").hide();
						$('.map-preview-tiers').hide();
						if ($(this).attr("id") == "section-reseaux") { 		
							$('.map-preview-tiers').fadeIn();
							// $('.map-preview-tiers').fadeIn();
							$("#reseaux").html(`<h3>Section qui represente les liens de ce tiers-lieux par rapport aux autres</h3><div class="network-button"><div>`)	
							$("#reseaux").parent().css("margin-bottom", "0px")
							var networkSuplement = preview.actions.generateButton(getNavigatorElement.suplment.networkSupl.id, getNavigatorElement.suplment.networkSupl.path, "networkSuplClass", "RESEAUX");
							networkSuplement = networkSuplement.refresh();
							var networkFilters = {'$or' : {'$and': [{"name" : {'$in': whereLists[preview.network].arrayName}},{"address.level3": whereLists[preview.network].level3Key}]}}
							var networkFormAnswers = {};
							var thematicAnswers = {};
							var level3AddressAnswers = {};
							var thematicFilter = {'$or': []};
							var thematicDropdown = {};
							var thematicArrayOrg = {};
							var level3Dropdown = {};
							var thematicOrga = [];
							var networkByLev3Orga = [];
							if (jsonHelper.getValueByPath(networkSuplement, "answerId") != undefined && jsonHelper.getValueByPath(networkSuplement, "getSuplForm."+ networkSuplement.answerId + ".answers") != undefined) {
								networkFormAnswers = jsonHelper.getValueByPath(networkSuplement, "getSuplForm."+ networkSuplement.answerId + ".answers");
								// reseau thematique 
								if (jsonHelper.getValueByPath(networkFormAnswers, getNavigatorElement.suplment.networkSupl.thematicPath) != undefined) {
									thematicAnswers = jsonHelper.getValueByPath(networkFormAnswers, getNavigatorElement.suplment.networkSupl.thematicPath);
									if (thematicAnswers != undefined && Array.isArray(thematicAnswers) && thematicAnswers.length > 0) {
										for (var index = 0; index < thematicAnswers.length; index++) {
											var answerKey = Object.keys(thematicAnswers[index])[0];
											thematicArrayOrg[answerKey] = [];
											thematicDropdown[answerKey] = {};
											thematicFilter.$or.push({["answers." + getNavigatorElement.suplment.networkSupl.thematicPath + "." + answerKey] : {'$exists': true}});
										}
										var getAllNetworkResults = showAllNetwork.getAnswers ('/form/' + getNavigatorElement.suplment.networkSupl.id , {where: thematicFilter, show: [getNavigatorElement.suplment.networkSupl.path, "answers." + getNavigatorElement.suplment.networkSupl.thematicPath]});
										var getOrgaThematic = {};
										if (typeof getAllNetworkResults.result == "boolean" && getAllNetworkResults.result == true && typeof getAllNetworkResults.allAnswers == "object" && Object.keys(getAllNetworkResults.allAnswers).length > 0) {
											$.each(getAllNetworkResults.allAnswers, function(key, value){
												if (jsonHelper.getValueByPath(value, getNavigatorElement.suplment.networkSupl.path) != undefined) {
													var finderOrga = jsonHelper.getValueByPath(value, getNavigatorElement.suplment.networkSupl.path);
													if (typeof finderOrga == "object" && Object.keys(finderOrga).length > 0) {
														var orgaKey = Object.keys(finderOrga)[0];
														thematicOrga.push(finderOrga[orgaKey].name)
														if (jsonHelper.getValueByPath(value, "answers." + getNavigatorElement.suplment.networkSupl.thematicPath) != undefined && Array.isArray(jsonHelper.getValueByPath(value, "answers." + getNavigatorElement.suplment.networkSupl.thematicPath))) {
															var filterKeys = jsonHelper.getValueByPath(value, "answers." + getNavigatorElement.suplment.networkSupl.thematicPath);
															for (let index = 0; index < filterKeys.length; index++) {
																if (typeof filterKeys[index] == "object" && Object.keys(filterKeys[index]).length > 0) {
																	var filterElementkey = Object.keys(filterKeys[index])[0];
																	if (Array.isArray(thematicArrayOrg[filterElementkey])) {
																		thematicArrayOrg[filterElementkey].push(finderOrga[orgaKey].name);
																		jsonHelper.setValueByPath(thematicDropdown, filterElementkey + ".label", filterElementkey);
																		jsonHelper.setValueByPath(thematicDropdown, filterElementkey + ".value", thematicArrayOrg[filterElementkey]);
																	}	
																}
															}
														}
													}
												}
											})
										}
									}
								}
								if (jsonHelper.getValueByPath(networkFormAnswers, getNavigatorElement.suplment.networkSupl.leve3Address) != undefined ) {
									var level3AddressAnswers = jsonHelper.getValueByPath(networkFormAnswers, getNavigatorElement.suplment.networkSupl.leve3Address);
									var networkFilter = jsonHelper.getValueByPath(whereLists, level3AddressAnswers);
									if (jsonHelper.getValueByPath(networkFilter, "arrayName") != undefined && jsonHelper.getValueByPath(networkFilter, "level3Key")) 
										networkFilters = {'$or' : {'$and': [{"name" : {'$in': jsonHelper.getValueByPath(networkFilter, "arrayName")}},{"address.level3": jsonHelper.getValueByPath(networkFilter, "level3Key")}]}}
								}
								if (thematicOrga.length > 0) {
									networkFilters["$or"]["name"] = {'$in': []};
									networkFilters.$or.name.$in.push(...thematicOrga);
								}
							}
							$('div.network-button').html(networkSuplement["html"]);
							$("#reseaux").attr("class","");		

							const checkFilterActive = () => {
								if ($(".map-preview-tiers #filterContainer #activeFilters").length > 0) {
									clearInterval(checkMapInterval);
									$(".map-preview-tiers #filterContainer #activeFilters .activeFilter-label").remove()
									$(".map-preview-tiers #filterContainer #activeFilters").attr('style', 'display:block;margin-top: 5px;padding: 10px !important;');
								}
							}
							const checkMapInterval = setInterval(checkFilterActive, 100);

							preview.actions.filterMap("#filterContainer", networkFilters, level3Dropdown, thematicDropdown);
						}

					})
					$(preview.actions.clickSectionHref("#resume")).on("click", function() { 
						$('#resumeBarContent, .resumeBtnClass').fadeIn();
					})
				},
				clickModalEvent : function (selector) {
					$(selector).off().on('click', function (e) {
						var containerParentId = $(this).parent().parent().attr('id');
						var answerId = (($(this).data("id") == null) ? "newanswer" : $(this).data("id"));
						var formId = $(this).data("elformid");
						var step = $(this).data("step");
						var input = $(this).data("input");
						var path = $(this).data("path");
						$(".map-preview-tiers").css('display', 'none');
						containerParentId = (containerParentId == undefined) ? $(this).parent().parent().parent().parent().attr('id') : containerParentId;
						preview.actions.answerModal(answerId, formId, step, path, containerParentId = (containerParentId == undefined) ? "section-info" : containerParentId, input);
						var timeout = 100;
						if ($("#resumeBarContent").length > 0) 
							$("#resumeBarContent").hide();
						setTimeout(() => {
							if ($("#customHeader").length > 0) {
								$("#customHeader").parent().addClass("col-xs-12").removeClass("col-xs-10")
							}
						}, timeout);
					});
				},
				clickRating : function (callBack) {
					$(	preview.actions.clickSectionHref("#equipmentSatisfaction") + "," + 
						preview.actions.clickSectionHref("#ownSatisfaction") + "," +
						preview.actions.clickSectionHref("#allEquipmentSatisfaction") + "," + 
						preview.actions.clickSectionHref("#allOwnSatisfaction") 
					).click(function(){
						let getSraffRattingPth = "answers.criteriaslesCommunsDesTierslieux472023_154_0ljo82pxu1xcmrfcj4zw";
						let getEquipmentRatingPth = "answers.criteriaslesCommunsDesTierslieux472023_154_0ljo76y798n0d7ylz3i6";
						preview.getSraffRatting = preview.actions.getAnswerForm(getSraffRattingPth, null, getNavigatorElement.suplment.satisfaction.id, null, getSraffRattingPth);
						preview.getEquipmentRating = preview.actions.getAnswerForm(getEquipmentRatingPth, null, getNavigatorElement.suplment.satisfaction.id, null, getEquipmentRatingPth);
						allCriteria = {
							...preview.actions.combineAllCriteria(preview.getSraffRatting?.allAnswers, getSraffRattingPth),
							...preview.actions.combineAllCriteria(preview.getEquipmentRating?.allAnswers, getEquipmentRatingPth)
						}
						callBack();
					})
				},
				clickRadoBtnRender : function (radioId="", pathToArray="", callBack = () => {}, inverse=false) {
					if (radioId != "" && pathToArray != "") {
						$(preview.actions.clickSectionHref(radioId)).click(function () {
							let satisfactForm = getNavigatorElement.satisfactionForm;
							let result = (satisfactForm != undefined) ? jsonHelper.getValueByPath(satisfactForm, "params." + pathToArray + ".list") : [];
							let colors = (result != undefined && Array.isArray(result)) ? preview.actions.generateColorRange(result.length, inverse) : [];							
							callBack(result, colors);
						})	
					}
				}
			},
			views : {
				load: function() {
					preview.views.answerInformation.loadAnswerInformation();
					preview.views.showMapNetwork();
				},
				getDetailTable : function(data){
					var smiley = {
						love : {dec : "&#128525" ,label : "Love", noteWithCoeff:0, ansCount:0},
						happySmile : {dec : "&#128512" ,label : "Smile", noteWithCoeff:0, ansCount:0},
						neutral : {dec : "&#128528" ,label : "Neutre", noteWithCoeff:0, ansCount:0},
						sad : {dec : "&#128542" ,label : trad.sad, noteWithCoeff:0, ansCount:0},
						cry : {dec : "&#128557" ,label : "Pleure", noteWithCoeff:0, ansCount:0}
					};
					var html = `
						<table class="table" border="0">
							<tbody>
							`;
							$.each(data,function(k,v){
								var url = "";
								if(exists(data.orgaId)){
									url = `${baseUrl}#page.type.organizations.id.${v.orgaId}`;
								}
								html += `
									<tr id='row-${k}' >
										<td>
											<a href='${url}' target="_blank" class="">
												<img src="${v.profilThumbImageUrl}" alt=""  width="45" height="45" style="border-radius: 100%;object-fit:cover"/>
											</a>
										</td>
										<td>
											<a href='${url}' target="_blank" class="">
												<span>${v?.name}</span>
											</a>
										</td>
										<td>
											<span>${v?.criteria}</span>
										</td>`;
										
								html += `<td>
											<span style="font-size:23px">`;
											if(typeof v.happiness != "undefined"){
								html += `       ${smiley[v.happiness]["dec"]}`;
											}
								html +=     `</span>
										</td>`;

										if(typeof v.note != "undefined"){
								html += `<td>
											<div class="note-value-detail" data-rating='${v.note}' data-key='${k}'</div>
										</td>`;
										}
							html += `</tr>`;
							})
							html += `</tbody>                
						</table>`;
					return html;
				},
				modalButtonHtml : function (modalClass="", answerId="", formId="", step="empty", btnName="Formulaire", color="", path="", isPublic=false, input="empty") {
					console.log("Path modal button", path);
					if (preview.isAuthorized == 0 || isPublic == true) {
						return `
							<div class="btn-to-form">
								<button 
									class="btn btn-form-modal btn-small ${(color != "") ? "bg-" + color : "bg-blue"} ${modalClass}" style="cursor: pointer; color: white; " 
									data-id="${answerId}" 
									data-step="${step}"
									data-elformid="${formId}"
									data-path="${path}"
									data-input="${input}">
									<h5> <i class="fa fa-pencil"> </i> ${btnName} </h5>
								</button>		
							</div>				
						`;
					} else {
						return ``;
					}
				},
				modalSmallButtonHtml : function (modalClass="", answerId="", formId="", step="empty", color="", path="", isPublic=false, input="empty") {
					console.log("Path modal small button", path);
					if (preview.isAuthorized == 0 || isPublic == true) {
						return `<button 
									class="btn btn-form-modal btn-small ${(color != "") ? "bg-" + color : ""} ${modalClass}" style="cursor: pointer;" 
									data-id="${answerId}" 
									data-step="${step}"
									data-elformid="${formId}"
									data-path="${path}"
									data-input="${input}">
									<h5> <i class="fa fa-pencil"> </i></h5>
								</button>`;
					} else {
						return ``;
					}
				},
				showMapNetwork : function () {
					// <div id="filterContainer"></div>
					let networkHtml = `
						<div class="map-preview-tiers">	
							<div id="filterContainer"></div>
							<div id="mapContainerTiers" class="show-map" hidden></div>
						</div>
					`;
					$(".tab-content").append(networkHtml); // reseauMap
				},
				showGauge : function (width="600", height="400", percentage=0) {
					let minValue = 135;
					let maxValue = 315;
					let percentageDegree = (percentage / 100) * (maxValue - minValue) + minValue;
					let gaugeHtml = `
						<div class="gauge-content">
							<svg width="${width}" height="${height}" viewBox="-30 0 400 200">
								<path  stroke="#E81123" stroke-width="20" fill-opacity="0" d="M 10, 170.00000000000003 A 160,160 0 0, 1 52.98329862125358, 60.88037940664711"></path>
								<path fill="#FFF" stroke="#FF8C00" stroke-width="20" fill-opacity="0" d="M 60.880262390000226, 52.98340774093273 A 160,160 0 0, 1 164.41592062507033, 10.0974732609441"></path>
								<path fill="#FFF" stroke="#FCD116" stroke-width="20" fill-opacity="0" d="M 175.5839194724002, 10.097467676944689 A 160,160 0 0, 1 279.1196205933529, 52.983298621253624"></path>
								<path fill="#FFF" stroke="#3BA316" stroke-width="20" fill-opacity="0" d="M 287.0165922590673, 60.88026239000028 A 160,160 0 0, 1 329.99999999991996, 169.99983999999995"></path>
								<path fill="#7f8c8d" d="M170 170l10.606601717798211 0 l95.4594154601839 106.06601717798212 l-106.06601717798212 -95.4594154601839l0 -10.606601717798211z" transform="rotate(${percentageDegree}, 170, 170)"></path>
							</svg>
						</div>
					`;
					return gaugeHtml;
				},
				emptyContent : function (text="") {
					var adminText = `
						Vous n'avez pas rempli le formulaire complémentaire pour visualiser ces données. <br>
						En remplissant le formulaire, vous nous permettez de disposer d'une vision plus complète de la situation, ce qui nous aide à prendre des décisions éclairées pour l'amélioration de ces espaces. Votre participation contribue également à créer une communauté solidaire et à favoriser l'échange de connaissances entre les utilisateurs. <br>
						Vous pouvez y accéder en cliquant sur le bouton <b> FORMULAIRE </b>
					`;
					var simpleUser = `
						Cette section est vide! <br> Nous vous encourageons à prendre contact avec l'administrateur de ce tiers-lieux.
					`;
					return (text == "" || text == undefined) ? `
						<p>

							${(preview.isAuthorized == 0) ? adminText : simpleUser} 
						</p>
						<div class="form-btn-content"></div>
					` : `<p> ${text} </p>`;
				},
				emptyCtnWithIdndData : function (dataPrev, contentId="") {
					if (dataPrev == undefined || typeof dataPrev != "object"  || contentId != "") {
						$(contentId). append(`
							<div class="margin-top-20 emptyCtnWithIdndData"> ${preview.views.emptyContent()} </div>
						`)
					}
				},
				initialInformation : {
					loadInitalInformation : function () {
						preview.views.initialInformation.showAddress();
						preview.views.initialInformation.showSiteWeb();
						preview.views.initialInformation.showShortDescription();
						preview.views.initialInformation.showSocialLink();
						preview.views.initialInformation.showNetwork();
					},
					showAddress : function () {
						let address = (preview.getOrganizations?.address?.streetAddress != undefined) ? preview.getOrganizations?.address?.streetAddress : "";
						let postalCode = (preview.getOrganizations?.address?.postalCode != undefined) ? ", "+preview.getOrganizations?.address?.postalCode : "";
						let level4Name = (preview.getOrganizations?.address?.level4Name != undefined) ? ", "+preview.getOrganizations?.address?.level4Name : "";
						let level3Name = (preview.getOrganizations?.address?.level3Name != undefined) ? ", "+preview.getOrganizations?.address?.level3Name : "";
						let level2Name = (preview.getOrganizations?.address?.level2Name != undefined) ? ", "+preview.getOrganizations?.address?.level2Name : "";
						let level1Name = (preview.getOrganizations?.address?.level1Name != undefined) ? ", "+preview.getOrganizations?.address?.level1Name : "";
						address = address + postalCode + level3Name + level2Name + level1Name;
						let showAddressHtml = `
							<p>
								<i class="fa fa-map-marker fa-fw" aria-hidden="true" style="color: #8DBC21"></i>
								${address}
							</p>
						`;
						$(".elementAddress").html(showAddressHtml);
					}, 
					showSiteWeb : function () {
						let elementWebSite = (preview.getOrganizations?.url != undefined) ? preview.getOrganizations?.url : "";
						let urlHtml = `
							<a href="${elementWebSite}"><p><i class="fa fa-globe fa-fw" aria-hidden="true" style="color: #8DBC21"></i> Consulter le site</p></a>
						`;
						if (elementWebSite != "") {
							$(".elementUrl").html(urlHtml);
						}
					},
					showShortDescription : function () {
						let descriptionValue = (preview.getOrganizations?.shortDescription != undefined) ? preview.getOrganizations.shortDescription : "";
						let descHtml = `
							${(descriptionValue != "") ? `							
								<div class="description-content">
									<p class="sub-title-info"> Description courte : </p>
									<p>${descriptionValue}</p>							
								</div>` 
							: ""}
						`;
						$(".shortDescriptionHeader").html(descHtml);
						
					},
					showBtnForms : function (id,frequentationBtnAndSup, equipmentSuplement, activityFormButton, satisfactionSupl, tools, surveyFormButton, networkSuplement, mobilizeSuplement) {
						// var btnFormsHtml = `
						// 	<div class="form-sup-content margin-top-20">
						// 		<h4 class=""> Fomulaires supplémentaire: </h4>
						// 		<p> Veuiller completer le(s) formulaire(s) </p>
						// 		<div class="buttons-content">

						// 			<table class="table" border="0">
						// 				<tbody>
						// 					<tr>
						// 						${preview.views.initialInformation.infobtnList(frequentationBtnAndSup)}
						// 						${preview.views.initialInformation.infobtnList(equipmentSuplement)}
						// 					</tr>
						// 					<tr>
						// 						${preview.views.initialInformation.infobtnList(activityFormButton)}
						// 						${preview.views.initialInformation.infobtnList(satisfactionSupl)}
						// 					</tr>
						// 					<tr>
						// 						${preview.views.initialInformation.infobtnList(tools)}
						// 						${preview.views.initialInformation.infobtnList(surveyFormButton)}
						// 					</tr>
						// 				</tbody>                
						// 			</table>
									

						// 		</div>							
						// 	</div>
						// `;
						var btnFormsHtml = `
							<div class="row all-btn-form">
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> FRÉQUENTATION DES TIERS-LIEUX </h5>
									<p><b>(${frequentationBtnAndSup.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${frequentationBtnAndSup.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(frequentationBtnAndSup.html)} </div>
								</div>
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> DIAGNOSTIC DES USAGES DES ÉQUIPEMENTS PROPOSÉS EN TIERS-LIEUX </h5>
									<p><b>(${equipmentSuplement.percent.toFixed(0)}%)</b></p>
									<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
										<div class="progress-inner">
											<div class="progress" style="background: #EDF0F2; height: 7px;">
												<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${equipmentSuplement.percent.toFixed(0)}%;"></div>
											</div>
										</div>
									</div>
									<div class="btn-content"> ${(equipmentSuplement.html)} </div>
								</div>
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> MODÈLE ÉCONOMIQUE DES TIERS-LIEUX </h5>
									<p><b>(${activityFormButton.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${activityFormButton.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(activityFormButton.html)} </div>
								</div>
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> SATISFACTION DU TIERS-LIEU </h5>
									<p><b>(${satisfactionSupl.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${satisfactionSupl.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(satisfactionSupl.html)} </div>
								</div>
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> USAGES DES OUTILS NUMÉRIQUES </h5>
									<p><b>(${tools.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${tools.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(tools.html)} </div>
								</div>
								<!--div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> Grand recensement 2023 </h5>
									<p><b>(${surveyFormButton.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${surveyFormButton.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(surveyFormButton.html)} </div>
								</div-->
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> Réseau régionnaux & thématique </h5>
									<p><b>(${networkSuplement.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${networkSuplement.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(networkSuplement.html)} </div>
								</div>
								<div class="btn-content text-center">
									<h5 style="margin-top: 24px;"> Tiers Lieux de France mobilisons nous !</h5>
									<p><b>(${mobilizeSuplement.percent.toFixed(0)}%)</b></p>
										<div class="progress-bar-outer" style="width: 60%;margin: 0px auto;">
                                                <div class="progress-inner">
                                                    <div class="progress" style="background: #EDF0F2; height: 7px;">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ${mobilizeSuplement.percent.toFixed(0)}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
									<div class="btn-content"> ${(mobilizeSuplement.html)} </div>
								</div>
								
							</div>
						`;
						$(id).append(btnFormsHtml);
						$(".all-btn-form .btn-form-modal").html('<i class="fa fa-pencil"></i>').attr("style", "    border-radius: 50%;");
					},
					testBtnStatus : function (btnHtml) {
						let defaultDescNdCheck = {
							check : "",
							description : "Formulaire incomplet"
						};
						let btnHtmlClass = $(btnHtml).attr("class");
						if ($(btnHtml).length != 0 && btnHtmlClass.includes("bg-green")) {
							defaultDescNdCheck.check = "checked";
							defaultDescNdCheck.description = "Formulaire complet";
						} 
						return defaultDescNdCheck;
					},
					infobtnList : function (btnHtml) {
						let checkboxStatusNdDesc = preview.views.initialInformation.testBtnStatus(btnHtml)
						return (btnHtml != "") ? `
							<td class="icon-td-table">
								<div class="form-check">
									<input class="form-check-input form-checkbox" type="checkbox" value="" id="flexCheckIndeterminateDisabled" ${checkboxStatusNdDesc.check} disabled>
									<label class="form-check-label" for="flexCheckIndeterminateDisabled">
										${checkboxStatusNdDesc.description}
									</label>
								</div>
							</td>
							<td class="button-td-content">
								${btnHtml}
							</td>
						` : "";
					},
					showSocialLink : function () {
						if (typeof jsonHelper.getValueByPath(preview.getOrganizations, "socialNetwork") == "object" && Object.keys(preview.getOrganizations).length > 0) {
							//socialBar
							let socialNetworkHtml = "";
							
							$.each(preview.getOrganizations.socialNetwork, function (index, socialValue) {
								if (typeof index == "string") {
									if (index.includes("linkedin")) {
										// socialNetworkHtml += '<img class="co3BtnShare" width="40" src="' + parentModuleUrl + '/images/social/linkedin-icon.png" onclick="window.open(`'+ socialValue.url +'`,`_blank`)">';
										socialNetworkHtml += `<span id="divLinkedin" class="margin-right-5">
											<a class="contentInformation text-center btn-network tooltips" href="${socialValue}" target="_blank" id="linkedinAbout">
												<i class="fa fa-linkedin" style="color:#1877f2;"></i>
											</a>
										</span>`;
									}
									if (index.includes("instagram")) {
										socialNetworkHtml += `<span id="divInstagram" class="margin-right-5">
											<a class="contentInformation text-center btn-network tooltips" href="${socialValue}" target="_blank" id="instagramAbout">
												<i class="fa fa-instagram" style="color:#c17235;"></i>
											</a>
										</span>`;
									}
									if (index.includes("facebook")) {
										socialNetworkHtml += `<span id="divFacebook" class="margin-right-5">
											<a class="contentInformation text-center btn-network tooltips" href="${socialValue}" target="_blank" id="twitterAbout">
												<i class="fa fa-facebook" style="color:#1877f2;"></i>
											</a>
										</span>`;
									}
									if (index.includes("twitter")) {
										socialNetworkHtml += `<span id="divTwitter" class="margin-right-5">
											<a class="contentInformation text-center btn-network tooltips" href="${socialValue}" target="_blank" id="facebookAbout">
												<i class="fa fa-facebook" style="color:#1da1f2;"></i>
											</a>
										</span>`;
									}
								}
							})
							$(".socialBar").html(socialNetworkHtml);
						} else {
							$(".social-share-button-preview").remove();
						}
					},
					showNetwork : function () { 
						let networkHtml = `
							<p class="sub-title-info"> Réseaux : </p>
							<p>${preview.network}</p>
						`
						$(".showTheNetwork").html(networkHtml);
					}
				},
				answerInformation : {
					loadAnswerInformation : function () {
						// (suplForm="", btnPath="", btnClass="", formName="")
						preview.getAllAnswers("." + preview.organizationId);
						var frequentationBtnAndSup = preview.actions.generateButton(getNavigatorElement.suplment.frequentation.id, getNavigatorElement.suplment.frequentation.path, "frequentationBtn","LA FRÉQUENTATION");
						var equipmentSuplement = preview.actions.generateButton(getNavigatorElement.suplment.economicModel.id, getNavigatorElement.suplment.economicModel.path, "equipmentBtn","LES ÉQUIPEMENTS");
						var activityFormButton = preview.actions.generateButton(getNavigatorElement.suplment.avtivity.id, getNavigatorElement.suplment.avtivity.path, preview.activityClass, "MODÈLE ÉCONOMIQUE");
						var satisfactionSupl = preview.actions.generateButton(getNavigatorElement.suplment.satisfaction.id,getNavigatorElement.suplment.satisfaction.path,preview.satisfactionTrClass,"SATISFACTION", userId);
						// var allSatisfactionSupl = preview.actions.generateButton(getNavigatorElement.suplment.satisfaction.id,getNavigatorElement.suplment.satisfaction.path,preview.satisfactionTrClass);
						var listToolsForm = preview.actions.generateButton(getNavigatorElement.suplment.tool.id, getNavigatorElement.suplment.tool.path, preview.listToolsClass, "LISTE DES OUTILS");
						var surveyFormButton = preview.actions.generateButton(getNavigatorElement.surveyData.formId, getNavigatorElement.surveyData.path, "recencementClass", "RECENSEMENT");
						var networkSuplement = preview.actions.generateButton(getNavigatorElement.suplment.networkSupl.id, getNavigatorElement.suplment.networkSupl.path, "networkSuplClass", "RESEAUX");
						var mobilizeBtnAndSup = preview.actions.generateButton(getNavigatorElement.suplment.mobilize.id, getNavigatorElement.suplment.mobilize.path, "mobilizeBtn","TIERS-LIEUX MOBILISÉS");
						preview.views.answerInformation.showInfo();
						$(preview.actions.clickSectionHref(getNavigatorElement.showAllForms.id)).on("click", function() { 
							preview.getAllAnswers("." + preview.organizationId);
							frequentationBtnAndSup = frequentationBtnAndSup.refresh();
							mobilizeBtnAndSup = mobilizeBtnAndSup.refresh();
							equipmentSuplement = equipmentSuplement.refresh();
							activityFormButton = activityFormButton.refresh();
							satisfactionSupl = satisfactionSupl.refresh();
							listToolsForm = listToolsForm.refresh();
							surveyFormButton = surveyFormButton.refresh();
							networkSuplement = networkSuplement.refresh();
							preview.views.answerInformation.showForms(frequentationBtnAndSup, equipmentSuplement, activityFormButton, satisfactionSupl, listToolsForm, surveyFormButton, networkSuplement, mobilizeBtnAndSup);
						})

						// $(preview.actions.clickSectionHref(getNavigatorElement.showInfo.id)).on("click", function() { 
						// 	frequentationBtnAndSup = frequentationBtnAndSup.refresh();
						// 	equipmentSuplement = equipmentSuplement.refresh();
						// 	activityFormButton = activityFormButton.refresh();
						// 	satisfactionSupl = satisfactionSupl.refresh();
						// 	listToolsForm = listToolsForm.refresh();
						// 	surveyFormButton = surveyFormButton.refresh();
						// 	// preview.views.answerInformation.showInfo(frequentationBtnAndSup["htmlWhithName"], equipmentSuplement["htmlWhithName"], activityFormButton["htmlWhithName"], satisfactionSupl["htmlPublicWithName"], listToolsForm["htmlWhithName"], surveyFormButton["htmlWhithName"]);
						// 	// preview.views.answerInformation.showInfo();
						// })

						$(preview.actions.clickSectionHref(getNavigatorElement.showActivity.id)).on("click", function() { 
							activityFormButton = activityFormButton.refresh()
							coInterface.showLoader(getNavigatorElement.showActivity.id);
							activityFormButton["html"] = preview.actions.setStepAndInput(activityFormButton["html"], getNavigatorElement.showActivity.step , getNavigatorElement.showActivity.input);
							preview.views.answerInformation.showActivity(activityFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showPrincipleAndValue.id)).on("click", function() {
							surveyFormButton = surveyFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showPrincipleAndValue.id);
							surveyFormButton["html"] = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showPrincipleAndValue.step , getNavigatorElement.showPrincipleAndValue.input);
							preview.views.answerInformation.showPrincipleAndValue(surveyFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showEquipment.id)).on("click", function() {
							equipmentSuplement = equipmentSuplement.refresh();
							coInterface.showLoader(getNavigatorElement.showEquipment.id);
							equipmentSuplement["html"] = preview.actions.setStepAndInput(equipmentSuplement["html"], getNavigatorElement.showEquipment.step , getNavigatorElement.showEquipment.input);
							preview.views.answerInformation.showEquipment(equipmentSuplement);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showDomain.id)).on("click", function() {
							surveyFormButton = surveyFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showDomain.id);
							surveyFormButton["html"] = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showDomain.step , getNavigatorElement.showDomain.input);
							preview.views.answerInformation.showDomain(surveyFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showFrequentation.id)).on("click", function() {
							frequentationBtnAndSup = frequentationBtnAndSup.refresh();
							coInterface.showLoader(getNavigatorElement.showFrequentation.id);
							frequentationBtnAndSup["html"] = preview.actions.setStepAndInput(frequentationBtnAndSup["html"], getNavigatorElement.showFrequentation.step , getNavigatorElement.showFrequentation.input);
							preview.views.answerInformation.showFrequentation(frequentationBtnAndSup);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showAgeFrequentation.id)).on("click", function() {
							frequentationBtnAndSup = frequentationBtnAndSup.refresh();
							coInterface.showLoader(getNavigatorElement.showAgeFrequentation.id);
							frequentationBtnAndSup["html"] = preview.actions.setStepAndInput(frequentationBtnAndSup["html"], getNavigatorElement.showAgeFrequentation.step , getNavigatorElement.showAgeFrequentation.input);
							preview.views.answerInformation.showAgeFrequentation(frequentationBtnAndSup);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showEmployeesContract.id)).on("click", function() {
							surveyFormButton = surveyFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showEmployeesContract.id);
							surveyFormButton["html"] = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showEmployeesContract.step , getNavigatorElement.showEmployeesContract.input);
							preview.views.answerInformation.showEmployeesContract(surveyFormButton);
						})
						
						$(preview.actions.clickSectionHref(getNavigatorElement.showGouvernance.id)).on("click", function() {
							surveyFormButton = surveyFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showGouvernance.id);
							surveyFormButton["html"] = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showGouvernance.step , getNavigatorElement.showGouvernance.input);
							preview.views.answerInformation.showGouvernance(surveyFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.listTools.id)).on("click", function() {
							listToolsForm = listToolsForm.refresh();
							coInterface.showLoader(getNavigatorElement.listTools.id);
							// listToolsForm["html"] = preview.actions.setStepAndInput(listToolsForm["html"], getNavigatorElement.listTools.step);
							preview.views.answerInformation.listTools(listToolsForm);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showHealth.id)).on("click", function() {
							frequentationBtnAndSup = frequentationBtnAndSup.refresh();
							activityFormButton = activityFormButton.refresh();
							surveyFormButton = surveyFormButton.refresh();
							equipmentSuplement = equipmentSuplement.refresh();
							coInterface.showLoader(getNavigatorElement.showHealth.id);
							// frequentationBtnAndSup["html"] = preview.actions.setStepAndInput(frequentationBtnAndSup["html"], getNavigatorElement.showFrequentation.step);
							// activityFormButton["html"] = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showActivity.step);
							// equipmentSuplement["html"] = preview.actions.setStepAndInput(equipmentSuplement["html"], getNavigatorElement.showHealth.step, getNavigatorElement.showHealth.input);
							preview.views.answerInformation.showHealth(frequentationBtnAndSup, equipmentSuplement, activityFormButton, surveyFormButton); 
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showCreateValue.id)).on("click", function() {
							activityFormButton = activityFormButton.refresh();
							equipmentSuplement = equipmentSuplement.refresh();
							coInterface.showLoader(getNavigatorElement.showCreateValue.id);
							preview.views.answerInformation.showCreateValue(activityFormButton, equipmentSuplement);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showDistributionValue.id)).on("click", function() {
							activityFormButton = activityFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showDistributionValue.id);
							activityFormButton["html"] = preview.actions.setStepAndInput(activityFormButton["html"], getNavigatorElement.showDistributionValue.step , getNavigatorElement.showDistributionValue.input);
							preview.views.answerInformation.showDistributionValue(activityFormButton, surveyFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showCaptureValue.id)).on("click", function() {
							activityFormButton = activityFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showCaptureValue.id);
							activityFormButton["html"] = preview.actions.setStepAndInput(activityFormButton["html"], getNavigatorElement.showCaptureValue.step , getNavigatorElement.showCaptureValue.input);
							preview.views.answerInformation.showCaptureValue(activityFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showResume.id)).on("click", function() {
							activityFormButton = activityFormButton.refresh();
							coInterface.showLoader(getNavigatorElement.showResume.id);
							// activityFormButton["html"] = preview.actions.setStepAndInput(activityFormButton["html"], getNavigatorElement.showCaptureValue.step , getNavigatorElement.showCaptureValue.input);
							preview.views.answerInformation.showResume(activityFormButton);
						})

						$(preview.actions.clickSectionHref(getNavigatorElement.showForm.id)).on("click", function() {
							satisfactionSupl = satisfactionSupl.refresh();
							coInterface.showLoader(getNavigatorElement.showForm.id);
							// activityFormButton["html"] = preview.actions.setStepAndInput(activityFormButton["html"], getNavigatorElement.showCaptureValue.step , getNavigatorElement.showCaptureValue.input);
							preview.views.answerInformation.showForm(satisfactionSupl);
						})

						$('[data-name="Retour utilisateurs"]').on("click", function() {
						// 	activityFormButton = activityFormButton.refresh();
						// 	coInterface.showLoader(getNavigatorElement.showForm.id);
							var allSatisfaction = showAllNetwork.getAnswers("/form/" + getNavigatorElement.suplment.satisfaction.id);
							preview.views.answerInformation.showFeedBack(allSatisfaction?.allAnswers);
						})

						$("#section-similarTr").on("click", function() { 
							preview.views.answerInformation.showSimilarTr(preview.getAnswers);	
						})
						
						$(preview.actions.clickSectionHref("#presentation")).on("click", function() {
							satisfactionSupl = satisfactionSupl.refresh();
							activityFormButton = activityFormButton.refresh();
							equipmentSuplement = equipmentSuplement.refresh();
							preview.views.answerInformation.initialiseEconomicModel(activityFormButton, equipmentSuplement, satisfactionSupl);
						})
						/* $(".btn-hexagone").on("click", function() {
							let name = $(this).data("name"), page = $(this).data("page");
							if ($(".btn-list-forms").length > 0) 
								$(".btn-list-forms").hide();
							if ($(".btn-hexagone").length > 0) 
								$(".btn-hexagone").hide();
							$("#hexagone").show();
							$(".vertical-menu.all-content-prev").hide();
							$(".btn-prev-map").attr("onclick", "showAllNetwork.openInitialPrevGraph('"+name+"', '"+page+"')");
							$(".btn-prev-map").html(`<i class="fa fa-arrow-circle-left" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Revenir aux sections`);
							frequentationBtnAndSup = frequentationBtnAndSup.refresh();
							equipmentSuplement = equipmentSuplement.refresh();
							activityFormButton = activityFormButton.refresh();
							satisfactionSupl = satisfactionSupl.refresh();
							listToolsForm = listToolsForm.refresh();
							surveyFormButton = surveyFormButton.refresh();
							networkSuplement = networkSuplement.refresh();
							preview.views.answerInformation.showHexagone({
								"listTools": listToolsForm,
								"activity": activityFormButton,
								"survey" : surveyFormButton,
								"frequentation": frequentationBtnAndSup,
								"equipement": equipmentSuplement,
								"network": networkSuplement,
								"satisfaction": satisfactionSupl
							});
						}) */
						//event
						// preview.events.loadAllAnswers(equipmentSuplement, frequentationBtnAndSup, activityFormButton, allSatisfactionSupl);

					},
					initialiseEconomicModel : function (activityFormButton, equipmentSuplement, satisfactionSupl) {
						// presentation
						let initEcHtml = `
							<p class="text-center margin-top-20" style="font-size: 22px;"> Bienvenue dans la section <b>"Modèle économique"</b> de
							notre Observatoire des Tiers Lieux!
							Explorez les aspects clés du modèle économique de
							nos espaces collaboratifs à travers un visuel en <b>Rose
							des vents</b>. Découvrez cette représentation visuelle
							nouvelle et originale. Bonne exploration !</p>	
							<div class="all-economic-model-btn">
								${activityFormButton["htmlWhithName"]}
								${equipmentSuplement["htmlWhithName"]}
								${satisfactionSupl["htmlWhithName"]}
							<div>
						`;
						preview.actions.listCheckbox({}, "Présentation", "#presentation", false, "");
						$("#presentation").append(initEcHtml);
					},
					showInfo : function () {
						let infoHtml = `
							<div class="content-img-profil-preview">
								<img class="thumbnail" src="<?= $image ?>" style="width: 15%;">
								<p class="title-info"><?= str_replace("#", "'", $name) ?></p>
							</div>
							<div class="preview-element-info">
								<div class="col-xs-12 elementAddress" ></div>
								<div class="col-xs-12 elementUrl" ></div>
								<div class="header-tags col-xs-12 text-center blockFontPreview "></div>
							</div>
							<div class="col-xs-10 margin-top-10 showTheNetwork"></div>
							<div class="col-xs-10  shortDescriptionHeader"></div>
							<div class="social-share-button-preview col-xs-12 text-center">
								<div class="socialBar"></div>
							</div>
						`;
						
						$("#info").html(infoHtml);
						preview.views.initialInformation.loadInitalInformation();
					},
					showForms : function (frequentationBtnAndSup, equipmentSuplement, activityFormButton, satisfactionSupl, tools, surveyFormButton, networkSuplement, mobilizeSuplement) {
						preview.actions.listCheckbox({}, "Formulaires", getNavigatorElement.showAllForms.id, false, "Representation des taux de remplissage des formulaires.");
						preview.views.initialInformation.showBtnForms(getNavigatorElement.showAllForms.id, frequentationBtnAndSup, equipmentSuplement, activityFormButton, satisfactionSupl, tools, surveyFormButton, networkSuplement, mobilizeSuplement);
					},
					showHexagone: function({listTools, frequentation, equipement, activity, satisfaction, survey, network } = {}){
						function initHexagone(){
							preview.actions.listCheckbox({}, "Hexagone graph", getNavigatorElement.showHexagone.id, false, "Representation des réponses des formulaires en hexagone.", `<button class="btn" style="margin-top: 20px;margin-left: 20px;box-shadow: 0px 4px 18px 0px #0000001C;padding: 13px;font-weight: bold;border-radius: 15px;" id="configureBtn">Configurer les icones</button>`);
							var hexagonHeader = `
							<div class="filters-container-circle hidden-sm hidden-xs" style="position: absolute; right: 0; overflow: auto">
								<div id="hexagone-filter-container" class="margin-top-10 text-center"></div>
							</div>`;
							$(getNavigatorElement.showHexagone.id).append(hexagonHeader);
							$(getNavigatorElement.showHexagone.id).append('<canvas id="hexCanvasTL" class="hexContainerCanvas" height="550"></canvas>');
							let hexagonGrid = new HexagonGrid("hexCanvasTL", 50, window.innerWidth - (window.innerWidth * 9 *2 / 100) - 320);
							let dataKeys = {
								"Activités proposées" : {
									data: hexagonTools.activityData(activity, preview.actions.setStepAndInput(activity["smallHtml"], getNavigatorElement.showActivity.step , getNavigatorElement.showActivity.input), getNavigatorElement.showActivity.answerPath),
									btn: preview.actions.setStepAndInput(activity["smallHtml"], getNavigatorElement.showActivity.step , getNavigatorElement.showActivity.input),
								},
								"Valeurs et principes" : {
									data :hexagonTools.principleData(survey,preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showPrincipleAndValue.step , getNavigatorElement.showPrincipleAndValue.input), getNavigatorElement.showPrincipleAndValue.answerPath),
									btn: preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showPrincipleAndValue.step , getNavigatorElement.showPrincipleAndValue.input),
								},
								"Types d'équipements" : {
									data: hexagonTools.equipementData(equipement, preview.actions.setStepAndInput(equipement["smallHtml"], getNavigatorElement.showEquipment.step , getNavigatorElement.showEquipment.input), getNavigatorElement.showEquipment.answerPath),
									btn: preview.actions.setStepAndInput(equipement["smallHtml"], getNavigatorElement.showEquipment.step , getNavigatorElement.showEquipment.input),
								},
								"Domaines" : {
									data: hexagonTools.domainData(survey, preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showDomain.step , getNavigatorElement.showDomain.input), getNavigatorElement.showDomain.answerPath),
									btn: preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showDomain.step , getNavigatorElement.showDomain.input),
								},
								"Fréquentation" : {
									data: hexagonTools.frequentationData(frequentation, preview.actions.setStepAndInput(frequentation["smallHtml"], getNavigatorElement.showFrequentation.step , getNavigatorElement.showFrequentation.input), getNavigatorElement.showFrequentation.answerPath),
									btn: preview.actions.setStepAndInput(frequentation["smallHtml"], getNavigatorElement.showFrequentation.step , getNavigatorElement.showFrequentation.input),
								},
								"Fréquentation par âge" : {
									data: hexagonTools.ageFrequentationData(frequentation, preview.actions.setStepAndInput(frequentation["smallHtml"], getNavigatorElement.showAgeFrequentation.step , getNavigatorElement.showAgeFrequentation.input), getNavigatorElement.showAgeFrequentation.answerPath),
									btn: preview.actions.setStepAndInput(frequentation["smallHtml"], getNavigatorElement.showAgeFrequentation.step , getNavigatorElement.showAgeFrequentation.input),
								},
								"Ressource humaines" : {
									data: hexagonTools.contratData(survey, preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showEmployeesContract.step , getNavigatorElement.showEmployeesContract.input), getNavigatorElement.showEmployeesContract.answerPath),
									btn: preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showEmployeesContract.step , getNavigatorElement.showEmployeesContract.input),
								},
								"Gouvernance" : {
									data: hexagonTools.gouvernanceData(survey, preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showGouvernance.step , getNavigatorElement.showGouvernance.input), getNavigatorElement.showGouvernance.answerPath),
									btn: preview.actions.setStepAndInput(survey["smallHtml"], getNavigatorElement.showGouvernance.step , getNavigatorElement.showGouvernance.input),
								},
								"Outils numériques" : {
									data: hexagonTools.toolsData(listTools, listTools["smallHtml"], "list-tools"),
									btn: listTools["smallHtml"],
								},
							}
							Object.keys(dataKeys).forEach(element => {
								let btn = `<div class="btn margin-5 btn-hexa-container">
									<button class="btn-expand" data-toggle="collapse" href="#${hexagonGrid.slugify(element)}">
										<i class="fa fa-chevron-down"></i>
									</button>
									<button class="btn-hexagone-zoomer" data-type="group">
										${element}
										<span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${element}" data-countkey="${element}" data-countlock="false">${dataKeys[element].data.length}</span>
									</button>
									${dataKeys[element].btn}
								</div>
								<div class="collapse hexagone-child-container" id="${hexagonGrid.slugify(element)}">
								`;
								dataKeys[element].data.forEach((child) => {
									btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonGrid.slugify(child.text)}'>
										${child.text}
									</button>`;
								})
								btn += `</div>`
								$(getNavigatorElement.showHexagone.id + " #hexagone-filter-container").append(btn);
							});
							let center = hexagonGrid.getCenterTile();
							hexagonGrid.drawHexAtColRow(center.column , center.row, "transparent", "","<?= $image ?>");
							let data = [];
							data.push({
								"group": "Activités proposées",
								"childs": dataKeys["Activités proposées"].data
							})
							data.push({
								"group": "Valeurs et principes",
								"childs": dataKeys["Valeurs et principes"].data
							})
							data.push({
								"group": "Types d'équipements",
								"childs": dataKeys["Types d'équipements"].data
							})
							data.push({
								"group": "Domaines",
								"childs": dataKeys["Domaines"].data
							})
							data.push({
								"group": "Fréquentation",
								"childs": dataKeys["Fréquentation"].data
							})
							data.push({
								"group": "Fréquentation par âge",
								"childs": dataKeys["Fréquentation par âge"].data
							})
							data.push({
								"group": "Ressource humaines",
								"childs": dataKeys["Ressource humaines"].data
							})
							data.push({
								"group": "Gouvernance",
								"childs": dataKeys["Gouvernance"].data
							})
							data.push({
								"group": "Outils numériques",
								"childs": dataKeys["Outils numériques"].data
							})
							hexagonGrid.placeGroupsWithOffsets(data, center, true, true, 15);
							hexagonGrid.fitBounds();
							$(".btn-hexagone-zoomer").off().on('click', function(){
								if($(this).data("type") == "group"){
									if($(this).parents(".btn-hexa-container").hasClass("btn-primary")){
										$(".btn-hexa-container").removeClass("btn-primary");
										hexagonGrid.zoomTo = [];
										hexagonGrid.fitBounds();
									}else{
										let group = $(this).find("span").data("countvalue");
										$(".btn-hexa-container").removeClass("btn-primary");
										$(".btn-hexagone-zoomer").removeClass("btn-primary");
										$(this).parents(".btn-hexa-container").addClass("btn-primary");
										hexagonGrid.zoomToGroup(group);
									}
								}else{
									if($(this).hasClass("btn-primary")){
										$(".btn-hexagone-zoomer").removeClass("btn-primary");
										hexagonGrid.zoomTo = [];
										hexagonGrid.fitBounds();
									}else{
										let texte = $(this).data("value");
										$(".btn-hexa-container").removeClass("btn-primary");
										$(".btn-hexagone-zoomer").removeClass("btn-primary");
										$(this).addClass("btn-primary");
										let element = Object.values(hexagonGrid.drawedHex).find(hex => {
											return hexagonGrid.slugify(hex.text) == texte;
										});
										if(typeof element != "undefined"){
											hexagonGrid.zoomToElement(element.column, element.row);
										}
									}
								}
							})
							HexagonGrid.prototype.clickEvent = function(e) {
								let hexa = this;
								var mouseX = e.pageX;
								var mouseY = e.pageY;

								var localX = mouseX - this.canvasOriginX;
								var localY = mouseY - this.canvasOriginY;
								var tile = this.getSelectedTile(localX, localY);
								if(this.hasHexagon(tile.column, tile.row)){
									let group = this.drawedHex[`${tile.column},${tile.row}`];
									let data = this.data[group.group][`${group.column},${group.row}`];
									if(typeof criteriaDescriptions != "undefined" && typeof criteriaDescriptions[data.text] != "undefined"){
										showAllNetwork.openPreviewAnswers(baseUrl + "/co2/cms/navigatorai", {"criteria": data.text, "messages" : `Donne moi la réponse de cette question en HTML et CSS imbriqué dans l'HTML. "Donne moi les informations nécessaires qu'on doit connaitre sur ${data.text} avec des longues descriptions"`, "formId": data.formId,"path": data.path, "options" : {}});
									}else{
										bootbox.prompt({
											"title" : "Il n'y a pas encore des informations pour "+data.text,
											"message": "<p>Cette section n'a pas encore des informations enregistrés dans notre base de données. Vous pouvez écrire une descritpion manuellement, ou questionner l'Open AI pour la generer automatiquement.</p>",
											buttons: {
												cancel: {
													label: '<i class="fa fa-times"></i> '+trad.cancel
												},
												confirm: {
													label: '<i class="fa fa-check"></i> <?php echo Yii::t('common', 'Create') ?>'
												}
											},
											inputType: 'select',
											inputOptions: [
												{
													text: 'Selectionner une option',
													value: ''
												},
												{
													text: 'Générer avec OpenAI',
													value: 'openai'
												},
												{
													text: 'Créer manuellement',
													value: 'hexagone-editor'
												}
											],
											callback : function(result){
												if(result){
													if(result == "openai"){
														showAllNetwork.openPreviewAnswers(baseUrl + "/co2/cms/navigatorai", {"criteria": data.text, "messages" : `Donne moi la réponse de cette question en HTML et CSS imbriqué dans l'HTML. "Donne moi les informations nécessaires qu'on doit connaitre sur ${data.text} avec des longues descriptions"`, "formId": data.formId,"path": data.path, "options" : {}}, true);
													}else if(result == "hexagone-editor"){
														urlCtrl.openPreview("/view/url/costum.views.custom.graph.hexagoneDetailEditor", {"preview": true, "criteria": data.text, "type": "navigatorHexagone", "callback": "callbackCreate", "paramsCallback": data});
													}
												}
											}
										})
									}
								}
							}
							$("#configureBtn").off("click").on("click", function(){
								initIcons(dataKeys);
							})
						}

						function initIcons(datas){
							preview.actions.listCheckbox({}, "Configurer les icones", getNavigatorElement.showHexagone.id, false, "", `<button class="btn" style="margin-top: 20px;margin-left: 20px;box-shadow: 0px 4px 18px 0px #0000001C;padding: 13px;font-weight: bold;border-radius: 15px;" id="show-hexagone"><i class="fa fa-arrow-circle-left" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Retourner vers l'hexagone</button>`);
							let html = `
								<div class="table" style="margin-top: 20px">
									<table style="table-layout: fixed;word-wrap: break-word;" class="table table-striped table-bordered table-hover" id="icon-config">
										<thead>
											<tr id="headerTable">
												<th class="text-center ">Element</th>
												<th class="text-center ">Icone</th>
												<th class="col-xs-2 text-center">Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							`;
							$(getNavigatorElement.showHexagone.id).append(html);
							$.each(datas, function(key, value){
								value.data.forEach(function(element){
									let tr = `<tr>
										<td>${element.text}</td>
										<td><img src="${baseUrl}/${element.image}" alt="${element.text} image" class="linked-image" data-text="${element.text}" style="width: 70px"></td>
										<td><button class="btn btn-sm btn-update-icons" data-icons="${element.text}" data-image="${element.image}" style="box-shadow: 0px 4px 18px 0px #0000001C;padding: 13px;font-weight: bold;border-radius: 15px;"><i class="fa fa-pencil"></i></button></td>
									</tr>`;
									$(getNavigatorElement.showHexagone.id + " #icon-config tbody").append(tr);
								})
							})
							$("#show-hexagone").off("click").on("click", function(){
								listTools = listTools.refresh();
								frequentation = frequentation.refresh();
								equipement = equipement.refresh();
								activity = activity.refresh();
								satisfaction = satisfaction.refresh();
								survey = survey.refresh();
								network = network.refresh();
								initHexagone();
							})
							$(".btn-update-icons").off("click").on("click", function(){
								let icons = $(this).data("icons");
								let imageData = $(this).data("image");
								var ctDialog = bootbox.dialog({
									message: '<div id="image-inputs" class="row"></div>', 
									title: "Icons pour "+icons,
									loseButton: false 
								});
								var myInputs = {
									container: "#image-inputs",
									inputs: [
										{
											type: "inputFileImage",
											options: {
												name: "image",
												label: "Image du fond",
												collection: "documents",
												class: "imageUploader col-sm-12",
												endPoint: "/subKey/hexagon",
												domElement: "image",
												filetype: ["jpeg", "jpg", "gif", "png"],
											}
										}
									],
									onchange: function(name, value, payload){
										if(name == "image"){
											imageData = value;
											if(value.includes("images/thumbnail-default.jpg")){
												imageData = null;
											}
										}
									}
								}
								if(imageData != "" && imageData != null){
									myInputs.inputs[0].options.defaultValue = imageData;
								}

								new CoInput(myInputs);
								ctDialog.find('.bootbox-body').append('<button class="btn" style="display:block; margin-left: auto; box-shadow: 0px 4px 18px 0px #0000001C;padding: 13px;font-weight: bold;border-radius: 15px;" id="save-image"><i class="fa fa-save"></i> Sauvegarder</button>');
								$("#save-image").off("click").on("click", function(){
									if(imageData != "" && imageData != null){
										ajaxPost(
											null,
											baseUrl+"/costum/francetierslieux/icons",
											{
												label: icons,
												icon: imageData,
												type: "insert"
											},
											function(data){
												if(data.results){
													linkedImage = data.data;
													$(".linked-image[data-text='"+icons+"']").attr("src", baseUrl+"/"+imageData);
													ctDialog.modal("hide");
												}
											}
										)
									}
								})
							})
						}
						initHexagone();
					},
					showActivity : function (activityFormButton) {
						// step franceTierslieux1822023_1721_4
						// input multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9qwknlysdiuab
						// #activity
						preview.actions.listCheckbox({}, "Activités proposées", "#activity", false, "Cette section montre les activité du Tiers-Lieux.", activityFormButton["html"]);	
						let initChartHtml = "";
						let activityChartData = jsonHelper.getValueByPath(activityFormButton["getSuplForm"], activityFormButton["answerId"] + getNavigatorElement.showActivity.activityAnswerPath);
						if (activityChartData == undefined || typeof activityChartData != "object") {
							initChartHtml += preview.views.emptyContent();
						}
						initChartHtml += `<div class="pieContent"><canvas id="activityPie"></canvas></div>`;
						$("#activity").append(initChartHtml);
						if (activityFormButton["canShowContent"]) {
							let chartData = {
								...preview.actions.getKeyAndTextSup(activityChartData),
							};
							let ctx = document.getElementById("activityPie").getContext("2d");
							preview.actions.section.loadActivityChart(chartData, "pie", ctx);
						}
					},
					showPrincipleAndValue : function (surveyFormButton) {
						// step franceTierslieux1522023_146_2
						// input multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op
						// #principleAndValue
						let principleAndValue = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showPrincipleAndValue.step + ".multiCheckboxPlus" + getNavigatorElement.showPrincipleAndValue.input);
						let dataPrinciple = [];
						preview.actions.listCheckbox({}, "Valeurs et principes", getNavigatorElement.showPrincipleAndValue.id,false, "Cette section montre les valeurs et principes du Tiers-Lieux", surveyFormButton["html"]);
						let initWordCloudHtml = `
							${(principleAndValue == undefined || typeof principleAndValue != "object") ? preview.views.emptyContent() : ""}
							<div id="wordsContent">	
								<div id="words" style="width: 600px; height: 400px;"></div> 
							</div> 
						`;
						$("#principleAndValue").append(initWordCloudHtml);
						preview.events.clickModalEvent(".principleValue");
						if (principleAndValue != undefined) {
							$.each(principleAndValue, function (dataKey, dataValue) {
								let name = Object.values(dataValue)[0].value;
								let importance = Object.values(dataValue)[0].rank;
								dataPrinciple.push({
									text: name,
									weight: importance
								})
							})
						}
						preview.actions.section.loadPrincipleWordC(dataPrinciple)
					},
					showEquipment : function (equipementSuplement) {
						// step franceTierslieux1822023_1721_4
						// input multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4
						// #equipment
						// assetPath	
						let equipementPath = ".answers." + getNavigatorElement.showEquipment.step + ".multiCheckboxPlus" + getNavigatorElement.showEquipment.input;
						let equipementSuplAnswer = jsonHelper.getValueByPath(equipementSuplement["getSuplForm"], equipementSuplement["answerId"] + equipementPath);
						preview.actions.listCheckbox({}, "Types équipements", getNavigatorElement.showEquipment.id, false, "", equipementSuplement["html"]);
						let initImagesHtml = `
							<div class="imagesEquipment">
								${preview.actions.section.listEquipmentImage(equipementSuplAnswer, true)}
							</div>
						`;
						$(getNavigatorElement.showEquipment.id).append(initImagesHtml);
						if (equipementSuplAnswer == undefined || typeof equipementSuplAnswer != "object") 
							$(".imagesEquipment").prepend(preview.views.emptyContent());
						// preview.events.clickModalEvent(".showModalEquipment");
					},
					showDomain : function (surveyFormButton) {
						// step franceTierslieux1522023_146_2
						// input multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q
						// #domain
						let domain = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showDomain.step + ".multiCheckboxPlus" + getNavigatorElement.showDomain.input);
						preview.actions.listCheckbox({}, "Domaines", getNavigatorElement.showDomain.id, false,"", surveyFormButton["html"]);
						let pictorialCharthtml = `
							<div class="domain-pictorial">
								${preview.actions.listPictorialChart(domain, ".domain-pictorial-chart")}
							</div>
						`;
						$(getNavigatorElement.showDomain.id).append(pictorialCharthtml);
						if (domain == undefined || typeof domain != "object") {
							$(".domain-pictorial").prepend(preview.views.emptyContent())
						}
						preview.events.clickModalEvent(".domainForm");
					},
					showFrequentation : function (frequentationForm) {
						// step franceTierslieux1922023_1231_5
						// input franceTierslieux1922023_1231_5lebb61gowlj39f2a2e
						// #frequentation
						// array
						let frequentationSuplAnswer = {};
						let frequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhrhuhp0xv1qqamf0i";
						preview.actions.listCheckbox({}, "Fréquentation", "#frequentation", true, "", frequentationForm["html"]);
						let percentageInitHtml = "";
						frequentationSuplAnswer = jsonHelper.getValueByPath(frequentationForm["getSuplForm"], frequentationForm["answerId"] + frequentationPath);
						if (frequentationSuplAnswer == undefined || typeof frequentationSuplAnswer != "object") {
							percentageInitHtml += preview.views.emptyContent();
						}
						if (frequentationForm["canShowContent"]) {
							percentageInitHtml += `
								<div class="image-container">
									${preview.actions.section.listFrequentation(frequentationSuplAnswer)}
								</div>
							`;	
						}
						$("#frequentation").append(percentageInitHtml);
					},
					showAgeFrequentation : function (ageFrequentationForm) {
						// step franceTierslieux1922023_1231_5
						// input franceTierslieux1922023_1231_5lebb61gpa1kh26lte5v
						// #ageFrequentation
						// 64649a64da37b44c5004e6e1 multiResultContainermultiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq
						let ageFrequentationSuplAnswer = {};
						let ageFrequentationPath = ".answers.lesCommunsDesTierslieux1752023_1312_0.multiCheckboxPluslesCommunsDesTierslieux1752023_1312_0lhri00zcnzrmyqw7eq";
						preview.actions.listCheckbox({}, "Fréquentation par âge", "#ageFrequentation", true, "", ageFrequentationForm["html"]);
						let percentageInitHtml = "";					
						ageFrequentationSuplAnswer = jsonHelper.getValueByPath(ageFrequentationForm["getSuplForm"], ageFrequentationForm["answerId"] + ageFrequentationPath);
						if (ageFrequentationSuplAnswer == undefined || typeof ageFrequentationSuplAnswer != "object") {
							percentageInitHtml += preview.views.emptyContent();
						}
						if (ageFrequentationForm["canShowContent"]) { 
							percentageInitHtml += `			
								<div class="image-container">
									${preview.actions.section.listFrequentation(ageFrequentationSuplAnswer, "age")}
								</div>
							`;		
						}
						$("#ageFrequentation").append(percentageInitHtml);

					},
					showEmployeesContract : function (surveyFormButton) {
						// step franceTierslieux1922023_1231_6
						// input multiCheckboxPlusfranceTierslieux1922023_1231_6lebdout9qitprmvmmwt
						// #employeeContract
						let employeeContract = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showEmployeesContract.step + ".multiCheckboxPlus" + getNavigatorElement.showEmployeesContract.input);
						preview.actions.listCheckbox({}, "Contrat des salariés", getNavigatorElement.showEmployeesContract.id, false, "", surveyFormButton["html"]);
						let percentageInitHtml = `
							${(employeeContract == undefined || typeof employeeContract != "object") ? preview.views.emptyContent() : ""}
							<div class="image-container">
								${preview.actions.section.listContract(employeeContract)}
							</div>
						`;
						$(getNavigatorElement.showEmployeesContract.id).append(percentageInitHtml);
						preview.events.clickModalEvent(".contractEmployee");
					},
					showGouvernance : function (surveyFormButton) {
						// step franceTierslieux1922023_1417_7
						// input multiCheckboxPlusfranceTierslieux1922023_1417_7lebeyrj9evio2mudes
						// #gouvernance		
						let gouvernance = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showGouvernance.step + ".multiCheckboxPlus" + getNavigatorElement.showGouvernance.input);
						preview.actions.listCheckbox({}, "Gouvernance", getNavigatorElement.showGouvernance.id, false, "", surveyFormButton["html"]);
						let gouvCharthtml = `
							${(gouvernance == undefined || typeof gouvernance != "object") ? preview.views.emptyContent() : ""}
							<div>
								${preview.actions.listPictorialChart(gouvernance, ".domain-pictorial-chart", "textsup")}
							</div>
						`;
						$(getNavigatorElement.showGouvernance.id).append(gouvCharthtml);
						preview.events.clickModalEvent(".gouvernanceBtn");
					},
					listTools: function (listToolsForm) {
						//finderlesCommunsDesTierslieux10112022_1423_0ljb8xhl6zywb6rehocs
						//lesCommunsDesTierslieux10112022_1423_0
						// #numericUsage
						let listToolsHtml = ``;
						let listToolSuplAnswer = {};
						preview.actions.listCheckbox({}, "Liste des outils", getNavigatorElement.listTools.id, false, "", listToolsForm["html"]);
						listToolSuplAnswer = jsonHelper.getValueByPath(listToolsForm["getSuplForm"], listToolsForm["answerId"] + ".answers");
						if (listToolSuplAnswer?.lesCommunsDesTierslieux10112022_1423_0 != undefined)
							delete listToolSuplAnswer.lesCommunsDesTierslieux10112022_1423_0;
						if  (listToolSuplAnswer == undefined || listToolSuplAnswer == null || typeof listToolSuplAnswer != "object" || Object.keys(listToolSuplAnswer).length == 0) {
							listToolsHtml += preview.views.emptyContent();					
						} else {
							listToolsHtml += `
								<div>
									<ul class="list-tootls-with-title">
										${preview.actions.extractUniqueCriteria(listToolSuplAnswer)}
									</ul>							
								</div>
							`;
						}
						$(getNavigatorElement.listTools.id).append(listToolsHtml);
					},
					showCreateValue: function (activityAndProduction, equipmentSuplmement) {
						// #createValue
						preview.actions.listCheckbox({}, "Creation de valeur", getNavigatorElement.showCreateValue.id);
						let equipementCreateValue = equipmentSuplmement;
						let economicModelAnswer = jsonHelper.getValueByPath(equipementCreateValue["getSuplForm"], equipementCreateValue["answerId"] + ".answers.lesCommunsDesTierslieux1052023_949_0.multiCheckboxPluslesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw");
						let productionPercentage = jsonHelper.getValueByPath(activityAndProduction["getSuplForm"], activityAndProduction["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq");
						let ActivityPercentage = jsonHelper.getValueByPath(activityAndProduction["getSuplForm"], activityAndProduction["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3onic3xd6dvbjzxu");
						let equipmentNumber = 0;
						let chartData = {...preview.actions.getKeyAndTextSup(economicModelAnswer)};
						let chartData1 ={...preview.actions.getKeyAndTextSup(ActivityPercentage)}
						let chartData2 ={...preview.actions.getKeyAndTextSup(productionPercentage)}
						let createValueHtml = `
							<h4>Nombre équipement</h4>
							${(economicModelAnswer == undefined || typeof economicModelAnswer != "object") ? preview.views.emptyContent() : ""}
							${preview.actions.setStepAndInput(equipementCreateValue["html"], getNavigatorElement.showCreateValue.equipmentSuplement.step, getNavigatorElement.showCreateValue.equipmentSuplement.input)}
							<div id="showRadarCreateValue" class='canvas-chart'></div>
							<h4>Pourcentage production</h4>
							${(ActivityPercentage == undefined || typeof ActivityPercentage != "object") ? preview.views.emptyContent() : ""}
							${preview.actions.setStepAndInput(activityAndProduction["html"], getNavigatorElement.showCreateValue.production.step, getNavigatorElement.showCreateValue.production.input)}
							<div id="showRadarCreateValue1" class='canvas-chart'></div>
							<h4 margin-top-20" >Pourcentage acitité</h4>
							${(productionPercentage == undefined || typeof productionPercentage != "object") ? preview.views.emptyContent() : ""}
							${preview.actions.setStepAndInput(activityAndProduction["html"], getNavigatorElement.showCreateValue.activity.step, getNavigatorElement.showCreateValue.activity.input)}
							<div id="showRadarCreateValue2" class='canvas-chart'></div>
						`;
						$(getNavigatorElement.showCreateValue.id).append(createValueHtml);
						chartData = preview.actions.transformObjectToArray(chartData);
						chartData1 = preview.actions.transformObjectToArray(chartData1);
						chartData2 = preview.actions.transformObjectToArray(chartData2);
						preview.actions.showRadar('div #showRadarCreateValue', chartData, equipmentSuplmement);
						preview.actions.showRadar('div #showRadarCreateValue1', chartData1);
						preview.actions.showRadar('div #showRadarCreateValue2', chartData2);
	
					},
					showDistributionValue: function (amount, surveyFormButton) {
						// "#distValue"
						let employeeContract = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showEmployeesContract.step + ".multiCheckboxPlus" + getNavigatorElement.showEmployeesContract.input);
						preview.actions.listCheckbox({}, "Distribution de valeur", getNavigatorElement.showDistributionValue.id, false, "", amount["html"]);
						let montant = jsonHelper.getValueByPath(amount["getSuplForm"], amount["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl16ek1pd92s71wm8");
						let createValueHtml = `<div id="showRadarDistributionVal" class=' radar-test canvas-chart'></div>`;
						let allResultAnswers = [];
						$(getNavigatorElement.showDistributionValue.id).append(createValueHtml);
						if (montant != undefined && typeof montant == "object") {
							allResultAnswers = [...allResultAnswers, ...montant];
							let chartData = {};
							if (employeeContract != undefined)
								allResultAnswers = [...allResultAnswers, ...employeeContract]
							chartData = {...preview.actions.getKeyAndTextSup(allResultAnswers)}
							chartData = preview.actions.transformObjectToArray(chartData);
							preview.actions.showRadar('div.radar-test', chartData);
						} else {
							$("#showRadarDistributionVal").prepend(preview.views.emptyContent());
						}
					},
					showCaptureValue: function (valueCapture) {
						// #captValue
						preview.actions.listCheckbox({}, "Capture de valeur", getNavigatorElement.showCaptureValue.id, false, "", valueCapture["html"]);
						let capture = jsonHelper.getValueByPath(valueCapture["getSuplForm"], valueCapture["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr");
						let createValueHtml = `	
							${(capture == undefined || typeof capture != "object") ? preview.views.emptyContent() : ""}
							<div id="showRadarCaptureValue"></div>
						`;
						$(getNavigatorElement.showCaptureValue.id).append(createValueHtml);
						let chartData = { ...preview.actions.getKeyAndTextSup(capture)}
						chartData = preview.actions.transformObjectToArray(chartData);
						preview.actions.showRadar('div #showRadarCaptureValue', chartData);
					},	
					showHealth : function (personFrequentation, equipementSatisfaction, activitySuplForm, surveyFormButton) {
						// "#tiersHealth"
						let memberShip = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showHealth.memberShip.step + "." + getNavigatorElement.showHealth.memberShip.input);
						let generatedProductAns = jsonHelper.getValueByPath(surveyFormButton["getSuplForm"], surveyFormButton["answerId"] + ".answers." + getNavigatorElement.showHealth.generatedProductAns.step + "." + getNavigatorElement.showHealth.generatedProductAns.input);
						let memberShipBtn = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showHealth.memberShip.step, getNavigatorElement.showHealth.memberShip.input);
						let generatedProductBtn = preview.actions.setStepAndInput(surveyFormButton["html"], getNavigatorElement.showHealth.generatedProductAns.step, getNavigatorElement.showHealth.generatedProductAns.input);
						preview.actions.listCheckbox({}, "Sante du tiers lieux", getNavigatorElement.showHealth.id);
						let personPercentage = preview.actions.personFrequentationPerc(
							personFrequentation["getSuplForm"], 
							"answers.evaluationlesCommunsDesTierslieux1752023_1312_0lhrhon89o6qyj512jp9",
							"answers.lesCommunsDesTierslieux1752023_1312_0.lesCommunsDesTierslieux1752023_1312_0liycoyb6rhah0gc8bt"
						);

						let satisfactionPercentage = preview.actions.equipmentSatisfaction(
							equipementSatisfaction["getSuplForm"], 
							"answers.yesOrNolesCommunsDesTierslieux1052023_949_0laazfcfrg2ze8nirm0f"
						);

						let  totalCurrGenProdArr = [
							"Vente de produit manufacturés", 
							"Ateliers", 
							"Conseil, accompagnement, ingénierie de projet",
							"Formation professionnelle",
							"Location d’espaces",
							"Location de machines",
							"Offre d’hébergement"
						]
						
						let generateProductPrcnt =  preview.actions.generateProduct(
							activitySuplForm["getSuplForm"], 
							"answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr", 
							totalCurrGenProdArr,
							(generatedProductAns != undefined && !isNaN(generatedProductAns)) ? Number(generatedProductAns) : 0
						);
						let renderHtml = `
							<div class="row">
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Frequentation personnes</h5> 
									</div>
									${preview.views.showGauge("400","200", personPercentage)}
									<div class="health-btn">
										${personFrequentation['html']}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Satisifaction des équipements</h5> 
									</div>
									${preview.views.showGauge("400","200", satisfactionPercentage)}
									<div class="health-btn">
										${equipementSatisfaction['html']}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Adhérents</h5> 
									</div>
									${preview.views.showGauge("400","200", preview.actions.membershipPercentage(memberShip, 500))}
									<div class="health-btn">
										${memberShipBtn}
									</div>
								</div>
								<div class="col-xs-6">
									<div class="health">
										<h5 class = "text-center margin-bottom-20" style="margin-top : 50px">Produits générés</h5> 
									</div>
									${preview.views.showGauge("400","200", generateProductPrcnt)}
									<div class="health-btn">
										${generatedProductBtn}
									</div>
								</div>
							</div>
						`;
						$(getNavigatorElement.showHealth.id).append(renderHtml);
						// preview.events.clickModalEvent(".membershipBtn");
						// preview.events.clickModalEvent(".generatedProduct");
					},
					showResume : function (resumeShowForm) {
						// #resume
						resumeShowForm = resumeShowForm.refresh("Dépenses");
						let negativeBtn = preview.actions.setStepAndInput(resumeShowForm["htmlWhithName"], getNavigatorElement.showResume.negativeValue.step , getNavigatorElement.showResume.negativeValue.input);
						resumeShowForm = resumeShowForm.refresh("Revenus");
						let positiveBtn = preview.actions.setStepAndInput(resumeShowForm["htmlWhithName"], getNavigatorElement.showResume.positiveValue.step , getNavigatorElement.showResume.positiveValue.input);
						let negativeValue = jsonHelper.getValueByPath(resumeShowForm["getSuplForm"], resumeShowForm["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv");
						let positiveValue = jsonHelper.getValueByPath(resumeShowForm["getSuplForm"], resumeShowForm["answerId"] + ".answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu51awl25edd4zlmsp");
						let chartBarData = {
							positive: positiveValue,
							negative: negativeValue
						}
						let initNegPosbar = `
							<div id="resumeBarContent">
								<canvas id="resumeBar"></canvas>
							</div>						
						`;
						$(getNavigatorElement.showResume.id).removeClass("col-xs-10").addClass("col-xs-12");
						preview.actions.listCheckbox({}, "Resume", getNavigatorElement.showResume.id, false, "", "<div class='resune-class'>"+ negativeBtn + positiveBtn +"</div>");
						$('.tab-content').append(initNegPosbar)
						mylog.log(notEmpty(negativeValue));
						if (notEmpty(negativeValue) === false || typeof negativeValue == "undefined" || isNaN(negativeValue) || notEmpty(positiveValue) === false || typeof positiveValue == "undefined" || isNaN(positiveValue)) {
							$("#resumeBarContent").html(preview.views.emptyContent());
						} else {
							$("#resumeBarContent").html('<canvas id="resumeBar"></canvas>');
							var canvas = document.getElementById("resumeBar");
							var chart = Chart.getChart(canvas);
							if (chart) {
								chart.destroy();
							}
							ctx = document.getElementById("resumeBar").getContext("2d");
							preview.actions.section.loadActivityChart(chartBarData, "bar", ctx);
						}

					},
					groupUserFeedback : function (data={}, notOnlyUser=false) {
						preview.views.answerInformation.showVisitFrequency(data?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo7l4388kmi8ldtyem, notOnlyUser);
						preview.views.answerInformation.showReasonOfVisit(data?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo7ur9jntbfdqk3yfd, notOnlyUser);
						preview.views.answerInformation.showEquipmentSatisfaction(data?.yesOrNolesCommunsDesTierslieux472023_154_0ljo76y798n0d7ylz3i6, notOnlyUser);
						preview.views.answerInformation.showAualityOfEvent(data?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo8gas97jeo5qspxz9, notOnlyUser);
						preview.views.answerInformation.showownSatisfaction(data?.yesOrNolesCommunsDesTierslieux472023_154_0ljo82pxu1xcmrfcj4zw, notOnlyUser);
						preview.views.answerInformation.showValuePrice(data?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo8jeyn5mw8iujh658, notOnlyUser);
						preview.views.answerInformation.showFavoriteAspect(data?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo8pn32tg65lmzv99c, notOnlyUser);
					},
					showForm : function (satisfactionSupl) {
						let getSatisfactionAns = jsonHelper.getValueByPath(satisfactionSupl["getSuplForm"], satisfactionSupl["answerId"] + ".answers");
						let verifyFormFill = $(satisfactionSupl["htmlPublicWithName"]).attr('class').includes('bg-green');
						let feedBackText = (verifyFormFill == false) ? `
							<p class= "text-center satisfaction-text">
								Vous avez visité ce tiers-lieu ? <br>
								<b style="color:red;"> Votre retour nous intéresse </b> <br>
								Partagez-nous votre expérience en remplissant
								notre formulaire de satisfaction du tiers lieu.
								Nous apprécions vos commentaires et
								suggestions pour améliorer ces espaces et
								rendre votre expérience encore meilleure. Merci
								d'avance pour votre participation !	
							</p>							
						` : 
						`
							<p class= "text-center satisfaction-text filled-form">
								Merci pour votre retour !
							</p>
						`;
						preview.actions.listCheckbox({}, "Votre retour", "#form", false, "", satisfactionSupl["htmlPublicWithName"]);
						$('#form').append(`
							<div class="content-center" >
									${feedBackText}						
							</div>
						`)
						if (satisfactionSupl?.getSuplForm != undefined) 
							preview.views.answerInformation.groupUserFeedback(Object.values(satisfactionSupl.getSuplForm)[0]?.answers);
					},
					showFeedBack : function (allSatisfactionSupl={}) {
						let allAnswers = preview.actions.groupAllAnswers(allSatisfactionSupl)?.answers;
						preview.views.answerInformation.groupUserFeedback(allAnswers, true);
						preview.views.answerInformation.showAmeliorateDomain(allAnswers?.lesCommunsDesTierslieux472023_154_0?.lesCommunsDesTierslieux472023_154_0ljo8lozm9x4daghhsx6, true);
					},
					showSimilarTr : function (params) {
						preview.actions.listCheckbox({}, "Tiers-lieux similaires", "#similarTr", false);
						var text = `Vous n'avez pas rempli les formulaires pour visualiser les données dans cette section.`;
						let agregatesPaths = getNavigatorElement.aggregateSimilar;
						let aggregateParams = {
							allPaths : {}
						};
						let trList = {};
						let cardList = "<div class='ans-dir'> <div class='cardContainer'>";
						if (agregatesPaths?.fields != undefined && params?.answers != undefined) {
							$.each(agregatesPaths.fields, function(keyPath, valuePath) {
								let answerPath = valuePath.replace("answers.", "");
								let answerValue = jsonHelper.getValueByPath(params?.answers, answerPath);
								aggregateParams.allPaths[valuePath] = answerValue;
							})
							
							ajaxPost(
								null,
								baseUrl + '/costum/francetierslieux/getaggregatetr',
								aggregateParams,
								function (data) {   
									trList = data;
								},
								null,
								null,
								{ async: false }     
							)  
							var listAllResults = ""
							if (trList?.profileResults != undefined && Object.keys(trList?.profileResults).length > 0) {
								$.each(trList.profileResults, function (key, value) {
									if (value?.links?.organizations != undefined) {
										if (key != 0)
											listAllResults += showAllNetwork.listOrgaInCard(value);
									}
								})
								if (listAllResults == "") {
									$("#similarTr").append(preview.views.emptyContent(text));
								} else {
									cardList += listAllResults;
								}
							} else {
								$("#similarTr").append(preview.views.emptyContent(text));
							}
							
							cardList += "</div></div>";
							$("#similarTr").append(cardList);

							
						} else {
							$("#similarTr").append(preview.views.emptyContent(text));
						}
					},
					showVisitFrequency : function (satisfactionSupl, group=false) {
						// satisfactionSupl = (group) ? preview.actions.findMostFrequentString(satisfactionSupl) : satisfactionSupl;
						let visitFrequencyContentId = (group) ? "#allVisitFrequency" : "#visitFrequency";
						preview.actions.listCheckbox({}, (group) ? "Fréquence de visite" : "Votre Fréquence de visite", visitFrequencyContentId, false);
						if (satisfactionSupl != undefined) {
							if (group && Array.isArray(satisfactionSupl)) {
								let initChartHtml = `<div class="frequencyPieDiv"><canvas id="frequencyPie"></canvas></div>`;
								$(visitFrequencyContentId).append(initChartHtml);
								let chartData = satisfactionSupl.reduce((acc, item) => {
									acc[item] = (acc[item] || 0) + 1;
									return acc;
								}, {});
								let ctx = document.getElementById("frequencyPie").getContext("2d");
								preview.actions.section.loadActivityChart(chartData, "pie", ctx);
							} else {
								let meterId = "visitFrequencyMeter";
								let sectionPath = "radioNewlesCommunsDesTierslieux472023_154_0ljo7l4388kmi8ldtyem";
								preview.actions.section.showBarMeter (visitFrequencyContentId, meterId, sectionPath, satisfactionSupl);
							}
						} else {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, visitFrequencyContentId);
						}
					},
					showReasonOfVisit : function (satisfactionSupl, group=false) {
						let reasOfVisit = (group) ? "#allReasonOfVisit" :"#reasonOfVisit";
						preview.actions.listCheckbox({}, (group) ? "Motifs de visite" : "Vos Motifs de visite", reasOfVisit , false);
						if (satisfactionSupl != undefined) {
							if (group) {
								let satisfactionData = satisfactionSupl.reduce((acc, item) => {
									acc[item] = (acc[item] || 0) + 1;
									return acc;
								}, {});
								let gouvCharthtml = `
									<div>
										${preview.actions.listPictorialChart(satisfactionData, ".reasonofVisit-pictorial-chart", "default")}
									</div>
								`;
								$(reasOfVisit).append(gouvCharthtml);								
							} else {
								$(reasOfVisit).append(`
									<div class="row">
										${preview.actions.section.listFeedBackImage(satisfactionSupl)}  
									</div>					
								`);
							}
						} else {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, reasOfVisit);
						}
					},
					showEquipmentSatisfaction : function (satisfactionSupl, group=false) {
						let equipmentSatisfaction = (group) ? "#allEquipmentSatisfaction" :"#equipmentSatisfaction";
						preview.actions.listCheckbox({}, (group) ? "Satisfaction équipement" : "Votre Satisfaction équipement", equipmentSatisfaction , false);
						//allCriteria
						if (satisfactionSupl == undefined) {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, equipmentSatisfaction);
						} else {
							preview.events.clickRating(function() {
								$(equipmentSatisfaction).html("");
								preview.actions.listCheckbox({}, (group) ? "Satisfaction équipement" : "Votre Satisfaction équipement", equipmentSatisfaction , false);
								$(equipmentSatisfaction).append(`
									<div class="row pictorial-list">
										${preview.actions.section.listFeedBackImage(satisfactionSupl, true)}  
									</div>					
								`);
							});
						}
					},
					showAualityOfEvent : function (satisfactionSupl, group=false) {
						let eventId = (group) ? "#allQualityOfEvent" :"#qualityOfEvent";
						preview.actions.listCheckbox({}, "Qualité des événements", eventId , false);
						if (satisfactionSupl != undefined) {
							let targetString = (Array.isArray(satisfactionSupl)) ? preview.actions.findMostFrequentString(satisfactionSupl) : satisfactionSupl;
							let qualityEventMeter = (group) ? "allQualityEventMeter" : "qualityEventMeter";
							let sectionPath = "radioNewlesCommunsDesTierslieux472023_154_0ljo8gas97jeo5qspxz9";
							preview.actions.section.showBarMeter (eventId, qualityEventMeter, sectionPath, targetString);							
						} else {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, eventId);
						}
					},
					showownSatisfaction : function (satisfactionSupl, group=false) {
						let ownSatisfaction = (group) ? "#allOwnSatisfaction" :"#ownSatisfaction";
						preview.actions.listCheckbox({}, "Satisfaction du personnel", ownSatisfaction , false);
						if (satisfactionSupl == undefined) {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, ownSatisfaction);							
						} else {
							preview.events.clickRating(function() {
								$(ownSatisfaction).html("");
								preview.actions.listCheckbox({}, "Satisfaction du personnel", ownSatisfaction , false);
								$(ownSatisfaction).append(`
									<div class="row pictorial-list">
										${preview.actions.section.listFeedBackImage(satisfactionSupl, true)}  
									</div>					
								`);
							});	
						}					
					},
					showValuePrice : function (satisfactionSupl, group=false) {
						let valuePriceId = (group) ? "#allValuePrice" :"#valuePrice";
						preview.actions.listCheckbox({},"Valeur / prix" ,valuePriceId , false);
						if (satisfactionSupl != undefined) {
							let targetString = (Array.isArray(satisfactionSupl)) ? preview.actions.findMostFrequentString(satisfactionSupl) : satisfactionSupl;
							let valuePriceMeter = (group) ? "allValuePriceMeter" : "valuePriceMeter";
							let sectionPath = "radioNewlesCommunsDesTierslieux472023_154_0ljo8jeyn5mw8iujh658";
							preview.actions.section.showBarMeter (valuePriceId, valuePriceMeter, sectionPath, targetString, true);
						} else {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, valuePriceId);
						}
					},
					showFavoriteAspect : function (satisfactionSupl, group=false) {
						let favAspectId = (group) ? "#allFavoriteAspect" :"#favoriteAspect";
						preview.actions.listCheckbox({}, (group) ? "Aspects appréciés" : "Mes aspects appréciés", favAspectId , false);
						if (satisfactionSupl != undefined && Array.isArray(satisfactionSupl)) {
							if (group) {
								let initChartHtml = `<div class="pieContent"><canvas id="aspectPie"></canvas></div>`;
								$(favAspectId).append(initChartHtml);
								let chartData = satisfactionSupl.reduce((acc, item) => {
									acc[item] = (acc[item] || 0) + 1;
									return acc;
								}, {});
								let ctx = document.getElementById("aspectPie").getContext("2d");
								preview.actions.section.loadActivityChart(chartData, "pie", ctx);
							} else {
								$(favAspectId).append(`
									<div class="row">
										${preview.actions.section.listFeedBackImage(satisfactionSupl)}  
									</div>					
								`);	
							}
						} else {
							preview.views.emptyCtnWithIdndData(satisfactionSupl, favAspectId);
						}
					},
					showAmeliorateDomain : function (allSatisfactionSupl, group=false) {
						let ameliorateDomainId = "#allAmeliorateDomain";
						preview.actions.listCheckbox({}, "Domaines à améliorer",ameliorateDomainId , false);
						if (allSatisfactionSupl != undefined && Array.isArray(allSatisfactionSupl)) {
							let initChartHtml = `<canvas id="ameliorationBar"></canvas>`;
							$(ameliorateDomainId).append(initChartHtml);
							let chartData = allSatisfactionSupl.reduce((acc, item) => {
								acc[item] = (acc[item] || 0) + 1;
								return acc;
							}, {});
							let ctx = document.getElementById("ameliorationBar").getContext("2d");
							preview.actions.section.loadActivityChart(chartData, "bar", ctx, true);	
						} else {
							preview.views.emptyCtnWithIdndData(allSatisfactionSupl, ameliorateDomainId);
						}
					}
				}
			},
			actions : {
				load : function () {
					preview.actions.goToItemPage();
					preview.actions.clickInitial();
				},
				section : {
					loadActivityChart : function (chartData={}, chartType="", ctx, isDefault=false) {
						var labels = Object.keys(chartData);
						var data = Object.values(chartData);
						var backgroundColors = preview.actions.getRandomActivityColors(labels.length);
						var chartConfig = {}
						if (chartType == "pie") {
							chartConfig = {
								type: chartType,
								data: {
									labels: labels,
									datasets: [{
										data: data,
										backgroundColor: backgroundColors
									}]
								},
								options: {
									responsive: true
								}
							}
						}
						if (chartType == "bar") {
							if (isDefault == false) { 	
								chartConfig =  {
									type: chartType,
									data: {
										labels: ['Data'],
										datasets: [
											{
												label: 'Revenus',
												backgroundColor: 'rgb(0, 131, 126)',
												data: [chartData.positive],
												barThickness: 40
											},
											{
												label: 'Dépenses',
												backgroundColor: 'rgb(116, 31, 24)',
												data: [-chartData.negative],
												barThickness: 40
											}
										]
									},
									options: {
										responsive: true,
										scales: {
											x: {
												stacked: true
											},
											y: {
												stacked: true,
												ticks: {
													beginAtZero: true
												},
												gridLines: {
													display: false, 
												}
											}
										}
									}
								}
							} else if (isDefault == true) { 	
								let dataValue = [];
								chartConfig =  {
									type: chartType,
									data: {
										labels: [],
										datasets: []
									},
									options: {
										responsive: true,
									}
								}
								if (chartData != undefined && Object.keys(chartData).length > 0) {
									$.each(chartData, function (k, v) {
										dataValue.push(v);
										let datasetValue = [{
											label: 'DOMAINES À AMÉLIORER',
											backgroundColor: 'rgb(0, 131, 126)',
											data: dataValue,
											barThickness: 40
										}];
										chartConfig.data.labels.push(k);
										chartConfig.data.datasets = datasetValue;
									})
								}
							}
						}
						let chartJs = new Chart(ctx, chartConfig);	
					},
					loadPrincipleWordC : function (wordCdata) {
						$("#words").jQCloud(wordCdata, {
							delay: 100,
							fontSize: {
								from: 0.1,
								to: 0.04
							}
						});
					},
					domainPictorialChart : function (data, section="" ,rank=0) {
						if (typeof data.svgDivClass == "undefined") {
							data.svgDivClass = "";
						}
						if (data.percentage > 100) {
							data.percentage = 100;
						}
						if (data.percentage < 0) {
							data.percentage = 0;
						}
						let randomRef = Math.random();
						let realPercent = (100 - data.percentage);
						let path = (typeof ftIconSvgPath[data.label] != "undefined") ? ftIconSvgPath[data.label] : ftIconSvgPath["bureauIcon"];
						let svgPictureHtml = `
							<div ${(data.svgDivClass == "") ? "" : "class='"+data.svgDivClass+"'"}> 
								<svg viewBox="0 0 220 200"  style="margin: ${(jsonHelper.pathExists("svgHeigth." + data.label + ".margin")) ? svgHeigth[data.label].margin : data.marginX}px ${data.marginY}px">
								<defs>
								<linearGradient height = "200" id="progress${randomRef}" x1="0" y1="1" x2="0" y2="0">
									<stop id="color1" offset="${data.percentage / 100}" stop-color="grey"/>
									<stop id="color2" offset="${data.percentage / 100}" stop-color="lightblue"/>
								</linearGradient>
								</defs>
									${data.percentage}%
								</text>
								<path fill="url(#progress${randomRef})" style="opacity:0.973" d="${path}"/>
								</svg>		
								<p class="text-center">${data.label} <b> (${data.percentage}%) </b> </p>
							</div>		
						`;
						return svgPictureHtml;
					},
					equipmentImage : function (label, image, Eqclass="", withRating=false, noteValue="0", cursorPointer=false, resultNumber = 0) {
						let percentageNote = (withRating) ? (isNaN(noteValue)) ? 0 : (Number(noteValue) / 5) * 100 : 0;
						let ratingHtmlContent = `
							<div class="star-rating" style="width: 100px; height: 20px; background-size: 20px;">
							<div class="star-value" style="background-size: 20px; width: ${percentageNote}%;"></div> 
							</div> 
						`;
						let equipmentHtml = `
							<div ${(Eqclass == "") ? "class='pictorial-width text-center'": Eqclass}>
								<img data-number="${resultNumber}" src="${image}" alt="" style="width: 60%;">
								<p class="margin-top-10">${label}</p>
								${(withRating) ? ratingHtmlContent : ""}
								${(cursorPointer) ? '<p style="font-size: 15px;"><b>NOMBRE : '+ resultNumber +'</b></p>' : ""}
							</div>
						`;
						return equipmentHtml;
					},
					getEquipmentNumber : function (data={}) {
						let equipmentImage = "";
						let resultHtml = "";
						let groupImage = [];
						let objectWithNumber = {};
						if (Object.keys(data).length > 0) {
							$.each(data, function (equipmentKey, equipmentValue) {
								let equipmentValueName = Object.keys(equipmentValue)[0];
								let imagePath = franceTiersLieuxImages[equipmentValueName];
								let imageKey = imagePath.split("/")[6].split(".")[0];
								if (!imagePath.includes(equipmentValueName) && imageKey != undefined /* && !groupImage.includes(imageKey) */ && Object.values(equipmentValue)[0].textsup != undefined) {
									if (objectWithNumber[imageKey] == undefined) {
										objectWithNumber[imageKey] = 0;
									}
									objectWithNumber[imageKey] = objectWithNumber[imageKey] + Number(Object.values(equipmentValue)[0].textsup);
									groupImage.push(imageKey); 
								}
							})
						}
						return objectWithNumber;
					},
					listEquipmentImage : function (data={}, pointer=false) {
						let equipmentImage = "";
						let resultHtml = "";
						let groupImage = [];
						let dataWithNumber = preview.actions.section.getEquipmentNumber(data);
						if (Object.keys(dataWithNumber).length > 0) {
							$.each(dataWithNumber, function (equipmentKey, equipmentValue) { 
								let imagePath = franceTiersLieuxImages[equipmentKey];
								equipmentImage += preview.actions.section.equipmentImage(equipmentKey, imagePath, "", null, null, true, equipmentValue);
							})
						}
						resultHtml += `
							<div class="row">
								${equipmentImage}
							</div>
						`;
						
						return resultHtml;
					},
					listFrequentation : function (data={}) {
						data = preview.actions.getKeyAndTextSup(data)
						let listHtml = "";
						if (Object.keys(data).length > 0) {
							$.each(data, function(datakey, dataValue) {
								listHtml+= preview.actions.genRepeatedImages(franceTiersLieuxImages[datakey], datakey, dataValue, dataValue + "%");
							})
						}	
						return listHtml;
					},
					listContract : function (data={}) {
						let totalTextSup = preview.actions.totalNumberContract(data);
						let listContractHtml = "";
						if (Object.keys(data).length > 0) {
							$.each(data, function(datakey, dataValue) {
								let percent = (isNaN(Object.values(dataValue)[0].textsup)) ? 0 : (Number(Object.values(dataValue)[0].textsup) / totalTextSup) * 100;
								listContractHtml += preview.actions.genRepeatedImages(franceTiersLieuxImages[Object.values(dataValue)[0].value], Object.values(dataValue)[0].value, percent, Object.values(dataValue)[0].textsup);
							})
						}

						return listContractHtml;
					},
					listFeedBackImage : function (data, withRating=false) {
						let imageList = "";
						if (data != undefined && (typeof data === 'object' || Array.isArray(data))) {
							$.each(data, function (k, v) {
								if (typeof v == "object" && v?.note != undefined) {
									let criteriaName = allCriteria[k]?.usage;
									let noteValue = v.note;
									imageList += preview.actions.section.equipmentImage((criteriaName == undefined) ? "Pas de titre" : criteriaName, franceTiersLieuxImages[criteriaName], "", withRating, noteValue);
								} else {
									imageList += preview.actions.section.equipmentImage(v, franceTiersLieuxImages[v], "", withRating);
								}
							})
						}
						criteriaName = {};
						return imageList;
					},
					// Meter bar 
					createVerticalBarMeters : function (legends, containerIdrClass, colors, activeSection) {
						let container = $(containerIdrClass);
						$.each(legends, function(index, legend) {
							let meterBar = $('<div>').addClass('vertical-bar-meter');
							meterBar.css('background-color', preview.actions.section.getColorByIndex(index, colors));
							meterBar.css('border-radius', preview.actions.section.getBorderRadiusByIndex(index, legends));
							let legendElement = $('<div>').addClass('verticalLegend').text(legend);
							let marker = $('<div>').addClass('marker').attr("data-name", legend.replace(/[' ]/g, '')).attr('hidden', true);
							marker.html('<i style="font-size: 27px;" class="fa fa-arrow-right"></i>');
							meterBar.append(legendElement);
							meterBar.append(marker);
							container.append(meterBar);
						
						});
						// active bar 
						$(".marker[data-name='" + activeSection.replace(/[' ]/g, '') + "']").removeAttr('hidden');
					},	
					getColorByIndex : function (index, colors) {
						// const colors = ['#FF6384', '#36A2EB', '#FFCE56', '#4BC0C0'];
						return colors[index % colors.length];
					},
					getBorderRadiusByIndex : function (index, legends) {
						let firstIndex = 0;
						let lastIndex = legends.length - 1;
						let borderRadius = (index === firstIndex) ? '10px 10px 0 0' : (index === lastIndex) ? '0 0 10px 10px' : '0';
						return borderRadius;
					},
					showBarMeter : function (sectionId, meterContainerId, sectionPath, satisfactionSupl, inverse=false) {
						let meterBarContent = `
							<div class="vertical-bar" id="${meterContainerId}"></div>
						`;
						meterContainerId = "#" + meterContainerId;
						$(sectionId).append(meterBarContent)
						preview.events.clickRadoBtnRender(
							sectionId, 
							sectionPath,
							function (array=[], colors=[]) {
								if (array != undefined && colors != undefined && Array.isArray(array) && Array.isArray(colors)) {
									$(meterContainerId).html("");
									preview.actions.section.createVerticalBarMeters(array, meterContainerId, colors, satisfactionSupl);
								}
							},
							inverse
						)
					}																							
				},
				setStepAndInput : function (btn, stepValue="", inputValue="") {
					if (stepValue != undefined && stepValue != "")
						btn = btn.replace('data-step=""', 'data-step="empty"');
						btn = btn.replace('data-step="empty"', "data-step='" + stepValue + "'");
					if (inputValue != undefined && inputValue != "") 
						btn = btn.replace('data-input="empty"', "data-input='" + inputValue + "'");
					return btn;
				},
				getSurveyData : function (surveyData = {}, reload=false) {
					surveyData = (surveyData != undefined && Object.keys(surveyData).length > 0) ? surveyData : JSON.parse(localStorage.getItem("surveyAnswer"));
					let surveySubForm = typeof getNavigatorElement != "undefined" && typeof getNavigatorElement.subForms != "undefined" && typeof getNavigatorElement.subForms.subformArray != "undefined" ? getNavigatorElement.subForms.subformArray : [];
					let inputs = allSurveyInput;
					let surveyAnswerId = preview.id;
					let surveyForm = getNavigatorElement.surveyData.formId;
					let surveyObject = {}
					jsonHelper.setValueByPath(surveyObject, "allAnswers." + surveyAnswerId + ((reload) ? ".answers" : ""), surveyData);
					jsonHelper.setValueByPath(surveyObject, "allAnswers." + surveyAnswerId + "._id.$id", surveyAnswerId);
					jsonHelper.setValueByPath(surveyObject, "form._id.$id", surveyForm);
					jsonHelper.setValueByPath(surveyObject, "form.subForms", surveySubForm);
					jsonHelper.setValueByPath(surveyObject, "inputs", [inputs]);
					jsonHelper.setValueByPath(preview.allSuplmentData, surveyForm, surveyObject);
				},
				getOnlyInputAnswer: function (answerContent={}) {
					let getAllInputs = {}
					if (answerContent != undefined && Object.keys(answerContent).length > 0) {
						$.each(answerContent, function(k, v) {
							getAllInputs = {...getAllInputs, ...v};
						})
					}
					return getAllInputs;
				},
				goToItemPage : function () {
					$(".goToItemPage").attr("href","#@"+preview.getOrganizations.slug);
				},
				listCheckbox : function (input={}, title="", dom="", isArray=false, descriptionText="", button="") {
					let listInputHtml = `
						<div class="title-btn" >
							<h3>${title}</h3>
							${button}
						</div>
						<p>${descriptionText}</p>
					`;
					if (Object.keys(input).length != 0) {
						listInputHtml += "<ul>"
						if (input != undefined) {
							$.each(input, function(key, value) {
								if (isArray) {
									listInputHtml += "<li>" + value + "</li>";
								} else {
									listInputHtml += "<li>" + Object.keys(value) + "</li>";
								}
							})
						} else {
							listInputHtml += "<li> Pas de résultat </li>";
						}
						listInputHtml += "</ul>"
					}
					$(dom).html(listInputHtml);
				},
				clickInitial : function () {
					$("[href='#menu0']").trigger("click");
				},
				getRandomActivityColors : function (count) {
					let colors = [];
					for (var i = 0; i < count; i++) {
						var color = '#' + Math.floor(Math.random() * 16777215).toString(16);
						colors.push(color);
					}
					return colors;
				},
				generateFilterObject : function (inputData={}, path="") {
					let objectValues = {};
					let objectResult = {};
					let filterChoice = "";
					if (Object.keys(inputData).length > 0) {
						objectValues = Object.values(inputData);
							$.each(objectValues, function (k, v) {
								filterChoice = Object.keys(v)[0];
									objectResult [filterChoice] = {
										label : filterChoice,
										field : path + "." + filterChoice + ".value"
									}
							})
					}
					return objectResult;
				},
				listPictorialChart : function (data={}, selector, type) {
					let pictorialData = {};
					let totalTextSup = (type == "textsup") ? preview.actions.totalTextSup(data) : 0;
					let percent = 0;
					pictorialData["width"] = 200;
					pictorialData["height"] = 200;
					pictorialData["marginY"] = 20;
					pictorialData["marginX"] = 10;
					pictorialData["svgDivClass"] = "pictorial-width";

					// 
					let list = "";
					if (Object.keys(data).length > 0) {
						let domainResultLength = Object.keys(data).length;
						let defaultPercentageTotal = Object.values(data)?.reduce((acc, value) => acc + value, 0);
						let countRank = 0;
						$.each(data, function(datakey, dataValue) {
							pictorialData["imageUrl"] = assetPath + "/images/franceTierslieux/icon_bureau.png";
							if (type == "textsup") {
								pictorialData["percentage"] = Number(Math.round((Number(Object.values(dataValue)[0].textsup) / totalTextSup) * 100));
								pictorialData["label"] = Object.keys(dataValue)[0];
								list += preview.actions.section.domainPictorialChart(pictorialData, type);
							} else if (type == "default") {
								pictorialData["percentage"] = (defaultPercentageTotal == undefined && isNaN(dataValue) && Number(dataValue) == 0) ? 0 : Math.round((Number(dataValue) / defaultPercentageTotal) * 100);
								pictorialData["label"] = datakey;
								list += preview.actions.section.domainPictorialChart(pictorialData, type, countRank);
							} else {
								pictorialData["percentage"] = (Number(Object.values(dataValue)[0].rank) / Number(domainResultLength)) * 100;
								pictorialData["percentage"] = 100 - Math.round(pictorialData["percentage"]) + 10;
								pictorialData["percentage"] = (pictorialData["percentage"] >= 100) ? 97 : (pictorialData["percentage"] <= 0) ? 7 : pictorialData["percentage"];
								pictorialData["label"] = Object.keys(dataValue)[0];
								list += preview.actions.section.domainPictorialChart(pictorialData, type, Object.values(dataValue)[0].rank);
							}
							countRank++;
						})
					}

					let listHtml = `
						 <div class="row pictorial-list">
						 	${list}
						 </div>
					`;
					return listHtml;
				},
				genRepeatedImages : function (imageUrl, label, percent, textSup=0, isNumberUser=false) {
					var html = '<div class="image-container" style="display: flex; align-items: center; justify-content: space-between; margin-bottom: 30px">';
					html += '<div class=col-md-3>';
					html += `<span class="label margin-right-10" style="color: black; font-size: 15px;white-space: break-spaces;line-height: 1.5;"> ${label} ${(textSup != 0) ? " (<b>" + textSup + "</b>)" : ""} </span>`;
					html += '</div>';
					html += '<div class="image-repeat col-md-9">';
					var repetitions = (isNumberUser) ? textSup : Math.ceil((percent * 2) / 10);
					
					for (var i = 0; i < repetitions; i++) {
						html += '<img src="' + imageUrl + '" style="width: 6vh;">';
					}
					html += '</div>';
					html += '</div>';
					// if (textSup == 0 && percent == 0) 
					// 	return "";

					return html;
				},
				totalTextSup : function (data={}) {
					let totalResult = 0;
					if (Object.keys(data).length > 0) {
						$.each(data, function(datakey, dataValue) {
							totalResult = totalResult + Number(Object.values(dataValue)[0].textsup);
						})
					}
					return totalResult
				},
				getKeyAndTextSup : function (data={}, title="") {
					let finalObject = {};
					title = title.toUpperCase();
					let legendTitle = "";
					let numberValue = 0;
					if (Object.keys(data).length > 0) { 
						$.each(data, function(datakey, dataValue) {
							legendTitle = Object.values(dataValue)[0].value;
							if (!isNaN(Object.values(dataValue)[0].textsup)) {
								numberValue = Number(Object.values(dataValue)[0].textsup);
								finalObject[legendTitle] = numberValue;
							}
						})
					}
					return finalObject;
				},
				filterMap : function (container="", networkFilters={}, level3Dropdown={}, thematicDropdown={}) {
					var paramsFIlter = {
						urlData: baseUrl + "/co2/search/globalautocomplete",
						container: container,
						defaults: {
							indexStep : 800,
							notSourceKey: true,
							types : ["organizations"],
							filters: networkFilters
						},
						filters : {
							text: true,
						}
					}
					if (typeof level3Dropdown == "object" && Object.keys(level3Dropdown).length > 0) {
						paramsFIlter.filters["level3Dropdown"] = {
							view : "dropdownList",
							type : "valueArray",
							field : "name",
							name : "Réseaux régionnaux",
							event : "inArray",
							typeList: "object",
							list : level3Dropdown
						}
					}

					if (typeof thematicDropdown == "object" && Object.keys(thematicDropdown).length > 0) {
						paramsFIlter.filters["thematicDropdown"] = {
							view : "dropdownList",
							type : "valueArray",
							name : "Réseaux thématiques",
							field : "name",
							event : "inArray",
							typeList: "object",
							list : thematicDropdown
						}
					}

					let filterNetwork = searchObj.init(paramsFIlter);
						filterNetwork.results.render = function(fObj, results, data) {
							preview.actions.showMap("#mapContainerTiers", results)
						};
					filterNetwork.search.init(filterNetwork);
				},
				showMap : function (container, data={}) {
                    let customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                    let appMap = new CoMap({
                        zoom : 5,
                        container : container,
                        activePopUp : true,
                        mapOpt:{
                            zoomControl:true,
							doubleClick : true,
                        },
                        activePreview : false,
                        mapCustom:customMap,
                    })        
					appMap.showLoader();
                    appMap.addElts(data);
                }, 
				showRadar : function (container='', dataContent=[], allDataSUpplement={}) {
					radarChart.defaultConfig.color = function() {};
					radarChart.defaultConfig.radius = 3;
					radarChart.defaultConfig.w = 600;
					radarChart.defaultConfig.h = 600;

					var data = [
						{
						className: 'default', 
						axes: dataContent
						}
					];

					var chart = radarChart.chart();
					var cfg = chart.config(); 
					var svg = d3.select(container).append('svg')
						.attr('width', "900")
						.attr('height', cfg.h + cfg.h / 4);
					svg.append('g').classed('single', 1).attr("transform", "translate(170, 90)").datum(data).call(chart);
				},
				transformObjectToArray : function (obj) {
					return Object.entries(obj).map(([axis, value]) => ({ axis, value }));
				},
				clickSectionHref: function(id) {
					let sectionId = '#' + $(id).parent().attr("id");
					return "[href='"+sectionId+"']";
				},
				answerModal : function (answerId="", formId="", step="", path="", containerParentId="", input="") {
					let contentId = $("div.tab-pane.active.in").children().attr("id");
					// answerId = (answerId == "" || answerId == null || answerId == undefined || answerId == "newanswer") ?  : answerId;
					if(typeof containerParentId != "undefined" && containerParentId != "" && containerParentId.indexOf("hexagone") > -1){
						contentId = "hexagone";
					}
					if ( (answerId == "" || answerId == null || answerId == undefined || answerId == "newanswer") 
							&& (preview.isAuthorized == 0 || formId == getNavigatorElement.suplment.satisfaction.id)) { 
						answerId = showAllNetwork.newAnswers(formId,function(data, dataId){
							if (path != undefined || path != null || path != "") {
								let dataToSend = {
									id: data['_id']['$id'],
									collection: "answers",
									path: path ,
									value : {
										id: preview.organizationId,
										name : "<?= $name ?>".replace("#","'"),
										type : "organizations"
									} 
								}
								if (preview?.getOrganizations?.profilImageUrl != undefined) {
									dataToSend.value.img = preview.getOrganizations.profilImageUrl;
								}
								dataHelper.path2Value(dataToSend, function (params) {});
								// if (formId == getNavigatorElement.surveyData.formId) {
								let dataToSendLinks = {
									id: data['_id']['$id'],
									collection: "answers",
									path: "links.organizations.<?= $orgaId ?>",
									value : {
										name : "<?= $name ?>".replace("#","'"),
										type : "organizations"
									} 
								}
								dataHelper.path2Value(dataToSendLinks, function (params) {});
								// }
							}
							preview.allSuplmentData[formId].allAnswers = {[dataId] : data}
						}, (preview.isAuthorized == 0 || formId == getNavigatorElement.suplment.satisfaction.id))
					}
					if (notEmpty(answerId) && (preview.isAuthorized == 0 || formId == getNavigatorElement.suplment.satisfaction.id) && containerParentId != "" && containerParentId != undefined) { 
						$(".form-answer-content").show();
						coInterface.showLoader("#" + contentId);
						let ajaxBaseUrl = baseUrl + '/survey/answer/index/id/' + answerId + '/form/' + formId + '/mode/w';
						if (step != undefined && step != "" && step != "empty") {
							ajaxBaseUrl = baseUrl +  "/survey/answer/answer/id/" + answerId + "/mode/w/form/" + formId + "/step/" + step;
							if ((input != undefined && input != "" && input != "empty")) 
								ajaxBaseUrl = baseUrl +  "/survey/answer/answer/id/" + answerId + "/mode/w/form/" + formId + "/step/" + step + "/input/" + input;
						}
	
						ajaxPost(null, ajaxBaseUrl,
						{ url : window.location.href },
						function(data){
							let returnBtn = `
								<div class="form-btn">
									<div class="form-return-content">
										<button style="color:white;" class="bg-main2 btn btn-return-form btn-default margin-top-20 margin-bottom-20" >
											<i class="fa fa-arrow-circle-left"></i>
										</button>
									</div>
								</div>
							`;
							$("#" + contentId).html(data).prepend(returnBtn);
							if ($("#modeSwitch").length > 0)
								$("#modeSwitch").hide();
							if ($("#customHeader").length > 0)
                    			$("#customHeader").css("margin-top","30px");
							$("#" + contentId).addClass("col-xs-12").removeClass("col-xs-10");
							if ($('.coFormbody').length > 0)
								$('.coFormbody').attr("style", "height: 58vh;");
							$(".btn-return-form").click(function() {
								coInterface.showLoader("#" + contentId);
								preview.getAllAnswers("." + preview.organizationId);
								// preview.actions.getSurveyData(showAllNetwork.getElement('answers', answerId)?.answers, true);
								if(contentId != "hexagone"){
									$("#" + contentId).addClass("col-xs-10").removeClass("col-xs-12");
									$(preview.actions.clickSectionHref("#" + contentId)).trigger('click');
								}else{
									$(".btn-hexagone").trigger("click");
								}
								// setTimeout(() => {$(preview.actions.clickSectionHref("#" + contentId)).dblclick();}, 200);
							})
							if (formId == getNavigatorElement.suplment.tool.id)
								data = preview.actions.listToolByThirdPlaces(data, contentId, returnBtn);
						},"html");
					 } else { 
						toastr.error("Vous n'êtes pas administrateur du Tiers-Lieux")
					}
				},
				listToolByThirdPlaces : function (data="") {
					if (typeof data == "string" && notEmpty(data)) {
						var paramsTool = {}
						var criteriaObject = {};
						paramsTool.finderPath = getNavigatorElement.suplment.tool.path;
						paramsTool.step = getNavigatorElement.suplment.tool.step;
						paramsTool.formId = getNavigatorElement.suplment.tool.id;
						var toolUsage = getNavigatorElement.getToolsAnswers(paramsTool);
						criteriaObject = preview.actions.genNewCriteriaObject(toolUsage);
						var criteriaCounter = document.querySelectorAll('[id^="count-"]');
						criteriaCounter.forEach(function(element) {
							const elementId = element.id;
							const criteriaKey = element.id.replace("count-","").replace("new-","");
							const criteriaKBtn = "." + element.classList[2];
							const input = document.getElementById("crit-" + criteriaKey);
							var answers = (typeof jsonHelper.getValueByPath(criteriaObject, criteriaKey + ".answers") == "object") ? criteriaObject[criteriaKey].answers : {};	
							var critCount = (criteriaObject[criteriaKey] != undefined) ? criteriaObject[criteriaKey].count : 0;
							$(criteriaKBtn).off("click");
							element.classList.remove(element.classList[2]);
							element.textContent = critCount;
							element.onclick = function(){preview.actions.getDetail(answers, criteriaKey)};
							$("#count-" + criteriaKey).off().one("DOMSubtreeModified", function() {
                                preview.actions.getCountModified(data, $(this));
                            });
						});
					}
					return data;
				},
                getCountModified(data, element){
                    if (typeof data == "string" && notEmpty(data)) {
                        var paramsTool = {}
                        var criteriaObject = {};
                        paramsTool.finderPath = getNavigatorElement.suplment.tool.path;
                        paramsTool.step = getNavigatorElement.suplment.tool.step;
                        paramsTool.formId = getNavigatorElement.suplment.tool.id;
                        var toolUsage = getNavigatorElement.getToolsAnswers(paramsTool);
                        criteriaObject = preview.actions.genNewCriteriaObject(toolUsage);
                        const criteriaKey = element.data("key");
                        var answers = (typeof jsonHelper.getValueByPath(criteriaObject, criteriaKey + ".answers") == "object") ? criteriaObject[criteriaKey].answers : {};
                        var critCount = (criteriaObject[criteriaKey] != undefined) ? criteriaObject[criteriaKey].count : 0;
                        setTimeout(() => {
                            element.text(critCount);
                        }, 10)
                        element[0].onclick = function(){preview.actions.getDetail(answers, criteriaKey)};
                    }
                },
				getDetail : function (answers, criteriaKey) {
                    var ctDialog = bootbox.dialog({
                        message: '<div id="table-detail-' + criteriaKey + '"><i class="fa fa-spin fa-spinner"></i> Loading...</div>', 
                        loseButton: false 
                    })
					if(typeof answers != "undefined"){
						var html = preview.views.getDetailTable(answers);
						if(html != ""){
							$('#table-detail-' + criteriaKey + '').html(html);
						}
						else
							ctDialog.modal('hide');
					}
				},
				dataDetailFormat: function(answers={}, criteriaKey="") {
					var results = {};
					if (Object.keys(answers).length > 0) {
						for(const[answerId, answerContent] of Object.entries(answers)) { 
							const orga = jsonHelper.getValueByPath(answerContent, getNavigatorElement.suplment.tool.path);
							const orgaId = Object.keys(orga)[0];
							const criteriaInput = Object.keys(answerContent.answers).filter((key) => key.startsWith("yesOrNo")).toString();
							var ansObj = {};
							ansObj.orgaId = orgaId;
							ansObj.name = orga[orgaId].name;
							ansObj.profilThumbImageUrl = (orga[orgaId].img != undefined) ? orga[orgaId].img : defaultImage;
							if (jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".criteria") != undefined)
								ansObj.criteria = jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".criteria");
							if (jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".happiness") != undefined)
								ansObj.happiness = jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".happiness");
							if (jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".note") != undefined)
								ansObj.note = jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".note");
							if (jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".user") != undefined)
								ansObj.user = jsonHelper.getValueByPath(answerContent, "answers." + criteriaInput + "." + criteriaKey + ".user");
							if (ansObj.user != undefined) 
								results[answerId] = ansObj;
						}
						return results;
					}
				},
				genNewCriteriaObject : function(toolUsage={}) {
					var criteriaObject = {};
					if (typeof toolUsage == "object" && Object.keys(toolUsage).length > 0) {
						for(const usage of Object.values(toolUsage)) { 
							for(const[criteria, criteriaContent] of Object.entries(usage)) { 
								if (criteria != "count") {
									if (criteriaObject[criteria] == undefined) {
										criteriaObject[criteria] = {};
										criteriaObject[criteria].count = 0;
										criteriaObject[criteria].answers = [];
									}
									criteriaObject[criteria].answers = (criteriaContent.trAnswers != undefined && criteriaContent.trAnswers[0] != undefined) ? criteriaContent.trAnswers[0] : {};
									criteriaObject[criteria].answers = preview.actions.dataDetailFormat(criteriaObject[criteria].answers, criteria)
									criteriaObject[criteria].count = (typeof criteriaObject[criteria].answers == "object" &&
																	  Object.keys(criteriaObject[criteria].answers).length > 0) ? 
																	  Object.keys(criteriaObject[criteria].answers).length : 0;
								}
							}
						}
					}
					return criteriaObject;
				},
				getSuplementAnswers : function (formId="", params={}) {
					let results = {}
					if (formId != "") {
						ajaxPost(
							null,
							baseUrl + '/costum/francetierslieux/getallanswers/type/answers/form/' + formId + '/getForm/true',
							params,
							function (data) {   
								if (data.result) { 
									results = data;
								} 
							},
							null,
							"json",
							{ async: false }     
						);
					}
					return results;
				},
				getAnswerForm : function (path="", orgaId="", formId="", userId="", showAnswerPath = "answers") {
					let paramAnswers = {
						show: [
							showAnswerPath,
							"links.answered",
							"user"              
						],
						where : {
							[path] : 
								{
									"$exists" : true
								}
						}
					}
					if (userId != undefined && userId != "") {
						paramAnswers.where["user"] = userId;
					}

					let answerContent = preview.actions.getSuplementAnswers(formId, paramAnswers);
					return answerContent;
				},
				newAnswers : function (formId="", answerPath="") {
					var dataId = showAllNetwork.getExistingAnsId(formId, answerPath, preview.organizationId);
					return dataId;				
				},
				generateButton : function (suplForm="", btnPath="", btnClass="", formName="", userId, loadClickEvent=false, clickEventClass="") {
						var button = {};
						var getSuplForm = (preview.allSuplmentData[suplForm] != undefined) ? preview.allSuplmentData[suplForm] : {};
						var path = btnPath + "." + preview.organizationId;
						var resultAnswerId = (!Array.isArray(getSuplForm.allAnswers) && Object.keys(getSuplForm.allAnswers)[0] != undefined && !Object.keys(getSuplForm.allAnswers)[0].includes("null")) ? Object.keys(getSuplForm.allAnswers)[0] : preview.actions.newAnswers(suplForm, path); 	
						var initSuplFormData = getSuplForm;	
						getSuplForm = preview.allSuplmentData[suplForm];
						getSuplForm = getSuplForm.allAnswers;
						if (!notEmpty(resultAnswerId) || Object.keys(getSuplForm)[0] == undefined || !notNull(Object.keys(getSuplForm)[0]) || jsonHelper.getValueByPath(getSuplForm, resultAnswerId + ".answers") == undefined) {
							resultAnswerId = preview.actions.newAnswers(suplForm, path);
						}
						var btnColor = showAllNetwork.setButtonColorOrPerc(jsonHelper.getValueByPath(getSuplForm, resultAnswerId + ".answers"), initSuplFormData.inputs, initSuplFormData.form.subForms);
						button.percent = showAllNetwork.setButtonColorOrPerc(jsonHelper.getValueByPath(getSuplForm, resultAnswerId + ".answers"), initSuplFormData.inputs, initSuplFormData.form.subForms, true);
						button.html = preview.views.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", "Formulaire", "", path);
						button.smallHtml = preview.views.modalSmallButtonHtml(btnClass, resultAnswerId, suplForm, "",  "", path);
						button.htmlWhithName = preview.views.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor, path);
						button.htmlPublicWithName = preview.views.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor, path, true);
						button.getSuplForm = getSuplForm;
						button.answerId = resultAnswerId;
						button.canShowContent = true;
						if (btnClass != "")
							setTimeout(() => {preview.events.clickModalEvent("." + btnClass);}, 100);

						button["refresh"] = function (btnName="") {
							return preview.actions.generateButton(suplForm, btnPath, btnClass, (btnName != "") ? btnName : formName, userId, loadClickEvent, clickEventClass)
						}
						return button;
				},
				extractUniqueCriteria : function (contentAnswer = {}) {
					let criteriaArray = "";
					let criteriaList = "";
					var criteriaArrays = [];
					if (Object.keys(contentAnswer).length > 0 && getNavigatorElement.getToolsTilte != undefined) {
						for (var key in contentAnswer) {
							if (contentAnswer.hasOwnProperty(key)) {
								let paramsKey = key.replace("yesOrNo","");
								if (jsonHelper.getValueByPath(getNavigatorElement, "getToolsTilte." + paramsKey + ".label") != undefined) {
									let title = jsonHelper.getValueByPath(getNavigatorElement, "getToolsTilte." + paramsKey + ".label");
									var criteriaObj = contentAnswer[key];
									// criteriaArray += '<li data-title="'+title+'"> <div class="title-nd-contents"> <div class="tools-title"><p class="title-section">' + title + '</p></div><div class="tools-content"><ul class="listTools">';
									var toolsArray = []
									for (var criteriaKey in criteriaObj) {
										if (!criteriaArrays.includes(criteriaObj[criteriaKey].criteria)) {
											criteriaArrays.push(criteriaObj[criteriaKey].criteria);
											if (criteriaObj.hasOwnProperty(criteriaKey)) {
												var criteria = criteriaObj[criteriaKey].criteria;
												if (jsonHelper.getValueByPath(criteriaObj, criteriaKey + ".user") != undefined && criteria != "" && criteria != undefined && criteria != null) {
													toolsArray.push(criteria);
													var toolNameKey = criteria.replace(/[\s\/']/g, '').toLowerCase();
													var defaultImage = parentModuleUrl+"/images/default_tool.png";
													var urlImageTool = (jsonHelper.pathExists("toolsImageUrl."+criteria.toLowerCase().replace(/\s/g, "")+".imageUrl")) ? (toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl == "") ? defaultImage : toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.imageUrl : defaultImage;
													urlImageTool = (jsonHelper.pathExists("AllToolImage." + toolNameKey)) ? AllToolImage[toolNameKey] : urlImageTool;
													urlImageTool = (jsonHelper.pathExists("linkedImage." + criteria)) ? linkedImage[criteria] : urlImageTool;
													var hrefImageTool = (jsonHelper.pathExists("toolsImageUrl."+criteria.toLowerCase().replace(/\s/g, "")+".url")) ? toolsImageUrl[criteria.toLowerCase().replace(/\s/g, "")]?.url : "";
													criteriaList += '<li class="margin-rigth-5"><a href="javascript:;"><img src="'+urlImageTool+'" alt="" style="width: 8vh;"><p>'+criteria+'</p></a></li>'
												}
											}
										}
									}
									if (toolsArray.length > 0) {
										criteriaArray += '<li data-title="'+title+'"> <div class="title-nd-contents"> <div class="tools-title"><p class="title-section">' + title + '</p></div><div class="tools-content"><ul class="listTools">';
											criteriaArray += criteriaList;
										criteriaArray += '</ul></div></li>';
									}
									criteriaList = "";
									toolsArray = [];
								}
							}
						}
					}
					return criteriaArray;
				},
				personFrequentationPerc : function (data={}, path="", day="") {
					let result = 0;
					if (data != null && data != undefined && Object.keys(data).length > 0 && path != "" && day != "") {
						let evaluationContent = jsonHelper.getValueByPath(Object.values(data)[0], path);
						let days = jsonHelper.getValueByPath(Object.values(data)[0], day)
						let count = 0;
						if (evaluationContent != undefined && days != undefined && days.length > 0) {
							let totalScore = days.length * 4;
							$.each(days, function(k, v) {
								if (evaluationContent[v] != undefined) {
									count = count + Number(Object.keys(evaluationContent[v])[0]);
								}
							})
							result = (count / totalScore) * 100
						}
					}
					return result;
				},
				equipmentSatisfaction : function (data = {}, path="") {
					let result = 0;
					if (Object.keys(data).length > 0 && path != "") {
						let answerContent = jsonHelper.getValueByPath(Object.values(data)[0], path);
						let allRate = 0;
						if (answerContent != undefined) {
							let ratingLength = Object.keys(answerContent).length * 5;
							$.each(answerContent, function (k, v) {
								if (v.note != undefined) {
									allRate = allRate + Number(v.note);
								}
							})
							result = (allRate / ratingLength) * 100
						}
					}
					return result;
				},
				membershipPercentage : function (number=0, numberMax=0) {
					let result = 0;
					if (!isNaN(number)) {
						if (Number(number) != undefined && Number(number) > 0 && Number(number) > Number(numberMax)) {
							return 100;
						} else {
							result = (Number(number) / 500) * 100
						}
					}
					return result;
				},
				generateProduct : function (data={}, path="", arrayctReference=[], numberRef=0) {
					let generateProductRes = 0;
					let totalCurrGenProd = 0;
					if (arrayctReference.length > 0  && Object.keys(data).length > 0 && path != "") {
						let generateProductContent = jsonHelper.getValueByPath(Object.values(data)[0], path)
						if (generateProductContent != undefined) {
							$.each(generateProductContent, function(k,v) {
								if (arrayctReference.includes(Object.keys(v)[0]) && Object.values(v)[0].textsup != undefined && !isNaN(Object.values(v)[0].textsup)) {
									totalCurrGenProd = totalCurrGenProd + Number(Object.values(v)[0].textsup);
									
								}
							})
						}
						generateProductRes = (totalCurrGenProd > numberRef) ? 100 : (totalCurrGenProd == 0 && numberRef == 0) ? 0 : (totalCurrGenProd / numberRef) * 100;
						generateProductRes = (generateProductRes < 0) ? 0 : (generateProductRes > 100) ? 100 : generateProductRes;
					}
					return generateProductRes;
				},
				totalNumberContract : function (data={}) {
					let totalCount = 0
					if (Object.keys(data).length > 0) {
						$.each(data, function (k, v) {
							if (typeof v && Object.values(v) != undefined && Object.values(v).length > 0 && Object.values(v)[0] != undefined) {
								if (!isNaN(Object.values(v)[0].textsup)) {
									totalCount = totalCount + Number(Object.values(v)[0].textsup);
								}
							}
						})
					}
					return totalCount;
				},
				getUserComplement : function (suplementData={}) {
					let targetSuplement = {}
					if (Object.keys(suplementData).length > 0 && suplementData != undefined) {
						$.each(suplementData, function(key, value) {
							let user = value?.user
							if (user != undefined && user != "" && user == userId && suplementData[key] != undefined) {
								targetSuplement = suplementData[key];
							}
						})
					}
					return targetSuplement;
				},
				groupAllAnswers : function (dataAnswers={}) {
					let resultContent = {};
					if (dataAnswers != undefined && Object.values(dataAnswers).length > 0) {
						Object.values(dataAnswers).map(function (content) {
							let answers = content?.answers;
							if (answers != undefined && Object.values(answers).length > 0) {
								$.each(answers, function(ansKey, ansValue) { 
									$.each(ansValue, function (key, value) {
										if (!key.includes("finder")) {
											if (typeof value == "string") {
												if (jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key) == undefined) {
													jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key, []);
												}
												resultContent.answers[ansKey][key].push(value);
											}
											if (value?.length != undefined && Array.isArray(value)) {
												if (jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key) == undefined) {
													jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key, []);
												}
												// jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key, [...new Set([...jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key), ...value])])
												jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key, [...jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key), ...value])
											}
											if (value?.note != undefined && !isNaN(value.note)) {
												if (jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key) == undefined) {
													jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key + ".noteTolal", 0);
													jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key + ".countNote", 0);
												}
												jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key + ".noteTolal", Number(jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key + ".noteTolal")) + Number(value.note));
												jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key + ".countNote", Number(jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key + ".countNote")) + 1);
												jsonHelper.setValueByPath(resultContent, "answers." + ansKey + "." + key + ".note", Number(jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key + ".noteTolal")) / Number(jsonHelper.getValueByPath(resultContent, "answers." + ansKey + "." + key + ".countNote")));
											}
										}
									})
								})
							}
						})
					}
					return resultContent;
				},
				findMostFrequentString : function (arrayString=[]) {
					const occurrenceCounts = {};
					if (arrayString?.length != undefined && arrayString.length > 0) {
						arrayString.forEach((str) => {
							if (occurrenceCounts[str]) {
								occurrenceCounts[str]++;
							} else {
								occurrenceCounts[str] = 1;
							}
						});

						let averageOccurrences = 0;
						for (const str in occurrenceCounts) {
							averageOccurrences += occurrenceCounts[str];
						}
						averageOccurrences /= Object.keys(occurrenceCounts).length;

						let mostFrequentString;
						let highestOccurrences = 0;
						for (const str in occurrenceCounts) {
							if (occurrenceCounts[str] > highestOccurrences) {
								mostFrequentString = str;
								highestOccurrences = occurrenceCounts[str];
							}
						}
						return mostFrequentString;
					} 
					// return "";
				},
				combineAllCriteria : function (allResults={}, path="") {
					let allCriteria = {};
					if(allResults != undefined && Object.keys(allResults).length > 0 && path != "") {
						$.each(allResults, function(k,v) {
							if (jsonHelper.getValueByPath(v, path) != undefined) {
								allCriteria = {...allCriteria, ...jsonHelper.getValueByPath(v, path)}
							}
						}) 
					}
					
					return allCriteria;
				},
				generateColorRange : function (size, invers=false) {
					const colors = [];
					const startColor = (invers) ? [255, 0, 0] : [0, 255, 0]; 
					const endColor = (invers) ? [0, 255, 0] : [255, 0, 0]; 
					const stepR = (endColor[0] - startColor[0]) / (size - 1);
					const stepG = (endColor[1] - startColor[1]) / (size - 1);
					const stepB = (endColor[2] - startColor[2]) / (size - 1);
					for (let i = 0; i < size; i++) {
						const r = Math.round(startColor[0] + stepR * i);
						const g = Math.round(startColor[1] + stepG * i);
						const b = Math.round(startColor[2] + stepB * i);
						const color = `rgb(${r}, ${g}, ${b})`;
						colors.push(color);
					}
					return colors;
				}
			}
		}
		preview.init();

		previewFormRefresh = function () {
			delete costum.store.verifyPerc[preview.organizationId]
			preview.getAllAnswers("." + preview.organizationId);
		}
		callbackCreate = function(params){
			$("#modal-preview-coop").hide();
			showAllNetwork.openPreviewAnswers(baseUrl + "/co2/cms/navigatorai", {"criteria": params.text, "messages" : `Donne moi la réponse de cette question en HTML et CSS imbriqué dans l'HTML. "Donne moi les informations nécessaires qu'on doit connaitre sur ${params.text} avec des longues descriptions"`, "formId": params.formId,"path": params.path, "options" : {}});
			criteriaDescriptions = getNavigatorElement.getCriteria();
		}
	})
	
</script>