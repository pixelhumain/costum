<?php 
    $cssAnsScriptFiles = array(
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
        "/js/franceTierslieux/dataviz/radarChart.js",
		"/css/franceTierslieux/radarChart.css",
    );
    $cssAnsScriptFilesTheme = array(
		'/plugins/jQCloud/dist/jqcloud.min.js',
		'/plugins/jQCloud/dist/jqcloud.min.css',
		"/plugins/Chart.js/Chart.v4.js",
        "/plugins/d3/d3.v3.min.js"
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
    $countTiers = isset($count) ? $count : 0;
    $allTiersLieux = PHDB::count(Organization::COLLECTION, array ('$or' => array(
        array("reference.costum"=>"franceTierslieux"),
        array("source.keys"=>"franceTierslieux")
    )));
    $tiersLieuxPercent = ($countTiers * 100) / $allTiersLieux;
?>
<style>
    .show-map {
        height: 50vh;
    }
    .all-content-prev .tab-content {
        overflow-y: inherit;
        padding-left: 0px !important;
        padding-right: 0px !important;
        background: #f9f7f7;
        /* height: 78vh; */
        height: 100%;
    }
    .container-filters-menu {
        background-color: inherit;
    }
    #filterContainer .dropdown .btn-menu {
        border-radius: inherit;
    }

    .image-network-content {
        display: flex;
        justify-content: center;
        margin-left: 50px;
        margin-bottom: 10%;
    }

    .stat-content,.pieContent, .word-cloud-content, #pieContent {
        display: flex;
        justify-content: center;
    }

    .pieContent { 
        width: 60vh !important;
        margin: 5px auto;
    }

    .tab-pane {
        margin-bottom : inherit;
    }

    #distValue {
        height: 67vh;
        overflow-y: auto;
        width: auto;
    }

    /* #wordCloud {
        margin-left: 15vh;
        margin-top: 50px;
    } */

    .stat-network {
        overflow-y: auto;
        /* height: 90vh; */
        height: 60vh;
    } 

    .ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }

</style>

<script>
    $(function() {
        let name = "<?= str_replace("#", "", $name) ?>";
        let where = JSON.parse(localStorage.getItem("param-where"));
        let markersAndSuplForms = showAllNetwork.getAnswers ('/form/' + getNavigatorElement.surveyData.formId + '/getMarkers/true/network/true', showAllNetwork.markerParams(name, where));
        let orgaObject = markersAndSuplForms['markers'];
        let economicModelObject = {
            distributionValue : {
                id: "distValue",
                path : { 
                    Nombre_d__équipement : "answers.lesCommunsDesTierslieux1052023_949_0.multiCheckboxPluslesCommunsDesTierslieux1052023_949_0lhi0h2l1gjvac3zpbcw",
                    Pourcentage_de_production : "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3onic3xd6dvbjzxu",
                    Pourcentage_activité : "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0liu3d5bbs2lwwdpiosq"
                },
                title : "Distribution de valeur"
            },
            valueCreation : {
                id: "createValue",
                path : { 
                   valueDistribution: "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl16ek1pd92s71wm8"
                },
                title : "Création de valeur"
            },
            captureValue : {
                id: "captValue",
                path : {
                    valuecapturePath: "answers.lesCommunsDesTierslieux1262023_1215_0.multiCheckboxPluslesCommunsDesTierslieux1262023_1215_0lisl7yq4i5vugcb4ywr"
                },
                title : "Capture de valeur"
            },
            resume : {
                id: "resume",
                path : {
                    benef : "answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu50gd93x5x1uva3xv",
                    amount : "answers.lesCommunsDesTierslieux1262023_1215_0.lesCommunsDesTierslieux1262023_1215_0liu51awl25edd4zlmsp"
                },
                title : "Résumé"
            }

        }
        let preview = {
            init : function () {
                preview.views.load();
                preview.actions.load();
                preview.events.load();
            },
            events : {
                load : function () {
                    $(".filter-map-section").click(function() {
                        $("#activityLinkMap").show();
                        var sectionPath = $(this).data("path");
                        var sectionName = $(this).data("key");
                        var menuReference = $(this).attr("href");
                        var title = $(this).data("title").replace("#","");
                        var filterValue = {};
                        var renderHtml = `
                        <div class="margin-left-20 margin-top-20">
                            <h4>Voici la cartographie qui affiche les tiers-lieux du ${name} :</h4><br>
                            <h5 style="text-align: center; padding-bottom: 15px; margin-bottom: 10px;">${title} : ${sectionName}</h5>
                        </div>
                        `;
                        $(".all-content-prev .tab-content " + menuReference).html(renderHtml);
                        preview.actions.filterMap({}, "", {[sectionPath] : {"$exists": sectionName}}, sectionName);
                        preview.events.markerCardEvent("#" + title, sectionName);
                    })

                    $("#section-reseaux").click(function () {
                        $("#activityLinkMap, .sub-menu-section").hide();
                        $("#barChartresume").hide();
                        $(".canvasContent").hide();
                    })

                    $(".sub-menu-section li a").click(function () {
                        $(".canvasContent").hide();
                    })

                    $(".Modèle_économique li a").click(function () {
                        $("#activityLinkMap").hide();
                    })

                    $("a[href='#"+$("#resume").parent().attr("id")+"']").click(function() {
                        $(".canvasContent").fadeIn();
                    })

                    $(".all-content-prev .tab-content").append(`<div id="pieContent" class="tab-pane fade "><div id="sectionPie" class="col-xs-10  no-padding margin-bottom-20 margin-top-20"><div id="titleContent"></div><div class="pieContent"><canvas id="mapSectionPie"><canvas></div></div></div>`);
                    ctx = document.getElementById("mapSectionPie").getContext("2d");

                    $("#distValue, #captValue, #createValue").removeClass("col-xs-10");

                    // $("#pieContent").hide();
                    $(".canvasContent").hide();
                },
                markerCardEvent : function (dataName, dataKey) {
                    const checkForClass = () => {
                        if ($('.lbh-preview-element').length > 0) {
                            clearInterval(checkInterval); // Arrête la vérification
                            $(".lbh-preview-element").on("click",function(e) {
                                var timeout = 200;
                                setTimeout(() => {
                                    if ($(".return-to-map").length == 0) {
                                        $(".toolsMenu").append(`<button class="btn btn-default return-to-map"> Retour sur la carte </button>`);
                                        $(`.return-to-map`).click(function(e) {
                                            e.stopPropagation();
                                            urlCtrl.closePreview();
                                            showAllNetwork.loadPreview('',"<?= $image ?>" ,name,'' ,'reseaux', '', '', "<?= $networkKey ?>");
                                            const checkForMapClass = () => {
                                                if ($('[data-name="'+ dataName +'"]').length > 0) {
                                                    clearInterval(checkMapInterval);
                                                    $('[data-name="'+ dataName +'"]').click()
                                                    $('[data-key="'+ dataKey +'"]').click()
                                                }
                                            }
                                            const checkMapInterval = setInterval(checkForMapClass, 100);
                                        })
                                    }
                                }, timeout);
                            })
                        }
                    }
                    const checkInterval = setInterval(checkForClass, 500);
                }

            },
            views : {
                load : function () {
                    preview.views.sections.load();
                    preview.views.map();
                },
                emptyContent : function () {
					return `
                        <div class="col-md-offset-1">
                            <p>
                                Les tiers lieux n'ont pas rempli les formulaires supplementaires pour visualiser ces données.
                                En remplissant le formulaire, vous nous permettez de disposer d'une vision plus complète de la situation, ce qui nous aide à prendre des décisions éclairées pour l'amélioration de ces espaces. Votre participation contribue également à créer une communauté solidaire et à favoriser l'échange de connaissances entre les utilisateurs. <br>
                            </p>                        
                        </div>
					`;
				},
                map : function () {
                    let initMap = `
                        <div id="activityLinkMap" style="height: 80% !important;" class="show-map"></div>
                    `;
                    $(".all-content-prev .tab-content").append(initMap);
                },
                sections : {
                    load : function () {
                        preview.views.sections.showNetworkDescription();
                        
                    },
                    showNetworkDescription : function () {              
                        let paramsToGetAns = {
                            showInOrga : {
                                "address.level4" : 1,
                                "_id" : 0
                            }            
                            ,
                            show: [
                                "links.organizations", 
                                "_id",
                                "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q",
                                "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op",
                            ],    
                            where : where
                        };
                        let descriptionHtml = `
                            <div class="margin-bottom-20 text-center">
                                <h3 class="margin-bottom-20">DESCRIPTION DU RESEAU</h3>
                            </div>
                            <hr class="margin-bottom-20" style=" width: 50%; border-top: 1px solid #000;">
                            <div class="col-md-12 stat-network"></div>
                        `;
                        $(".tab-pane #reseaux").removeClass("col-xs-10");
                        $(".tab-pane #reseaux").html(descriptionHtml);
                        let showTheseReferences = `
                                <div class="col-md-6 text-center tiers-number"></div>
                                <div class="col-md-6 text-center percentage"></div>
                                <div class="col-md-6 text-center communes" ></div>
                                <div class="col-md-6 text-center activityDomain"></div>
                                <div class="col-md-12 word-cloud-content">
                                    <div id="wordCloud" class="col-md-12 text-center" style="width: 600px; height: 400px;"></div>
                                </div>
                        `;
                        // TIERS-LIEUX NUMBER
                        $(".stat-network").append(showTheseReferences);
                        $(".tiers-number").append(preview.actions.generateCounterText(<?= $countTiers ?>, "Tiers-Lieux", "red", "35px"));
                        $(".percentage").append(preview.actions.generateCounterText(<?= $tiersLieuxPercent ?>, "De tous les Tiers-Lieux", "skyblue", "35px", true));
                        // COMMUNES
                        let allAnswersOrg = showAllNetwork.getAnswers("/form/63e0a8abeac0741b506fb4f7/showOrga/true", paramsToGetAns).allAnswers;
                        $(".communes").append(preview.actions.generateCounterText(preview.actions.getLevel4Number(allAnswersOrg), "Communes impliquées", "orange", "35px"));
                        // ACTIVITY DOMAIN      
                        $(".activityDomain").append(preview.actions.generateCounterText(preview.actions.countActivityDomain(allAnswersOrg, "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2y8qd7qkuri9q", "domain"), "Domaine d'activité", "purple", "35px"));
                        // WORD TEXT 
                        let data = preview.actions.countActivityDomain(allAnswersOrg, "answers.franceTierslieux1522023_146_2.multiCheckboxPlusfranceTierslieux1522023_146_2le5osg2xb6hjxhv95op", "value")
                        preview.actions.wordCloud(data)
                        preview.views.sections.economicModel(allAnswersOrg);
                    },
                    economicModel : function () {
                        let paramsToGetAns = {
                            show: [
                                "links", 
                                "_id",
                                economicModelObject.distributionValue.path["Nombre_d__équipement"],
                                economicModelObject.distributionValue.path["Pourcentage_de_production"],
                                economicModelObject.distributionValue.path["Pourcentage_activité"],
                                economicModelObject.valueCreation.path["valueDistribution"],
                                economicModelObject.captureValue.path["valuecapturePath"],
                                economicModelObject.resume.path["benef"],
                                economicModelObject.resume.path["amount"]
                            ],    
                        };
                        let allResults = markersAndSuplForms['suplAnswers']
                        $.each(economicModelObject, function(k,v){                 
                            
                            let initHtml = `
                                <div class="margin-left-20">
                                    <h3>${v.title}</h3>
                                </div>                   
                            `;   

                            $.each(v.path, function(key, value){
                                if (k == "resume") {
                                    
                                } else {
                                    if (k == "distributionValue") {
                                        let accent = key.replace("__","'");
                                        let word = accent.replace("_"," ");
                                        initHtml += `
                                            <div class="${key}">        
                                                <h4 style="margin-left: 82px;">${word}</h4>
                                            </div>  
                                        `;
                                    } else {
                                        initHtml += `
                                            <div class="${key}"> </div>  
                                        `;
                                    }
                                }
                            })
                            $("#" + v.id).html(initHtml);
                            let chartBarData = {}
                            $.each(v.path, function(key, value){ 
                                if (k == "resume") {
                                    if (key == "benef") {
                                        chartBarData["positive"] = Number(preview.actions.totalBenefOrAmount(allResults, value));
                                    } else {
                                        chartBarData["negative"] = - Number(preview.actions.totalBenefOrAmount(allResults, value));
                                    }
                                       
                                } else {
                                    if (preview.actions.transformObject(preview.actions.onlyAnswer(allResults, value)).length > 0) {
                                        preview.actions.showRadar('.' + key, preview.actions.transformObject(preview.actions.onlyAnswer(allResults, value))); 
                                    } else {
                                        $('.' + key).append(preview.views.emptyContent());
                                    }
                                     
                                }
                            })

                            if (k == "resume") {
                                let initrendHtml = `
                                    <div class="canvasContent">
                                        <canvas id="BarChartNetworkResume"><canvas>
                                    </div>
                                `;
                                $(".all-content-prev .tab-content").append(initrendHtml);
                                ctx = document.getElementById("BarChartNetworkResume").getContext("2d");
                                preview.actions.loadActivityChart(chartBarData, "bar", ctx);
                                if (chartBarData.positive == 0 && chartBarData.negative == -0) {
                                    $(".canvasContent").append(preview.views.emptyContent());
                                    $("canvas#BarChartNetworkResume").hide();
                                    $("#resume").removeClass("col-xs-10");
                                }
                            }

                        })
                    }
                },
                progressChart : function (ctx, percentage) {
                    let chart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                        labels: ['Progress', 'Remaining'],
                        datasets: [{
                            data: [percentage, 100 - percentage],
                            backgroundColor: ['#5283ff','#fff']
                        }]
                        },
                        plugins: [{
                        beforeDraw: (chart) => {
                            var width = chart.width,
                                height = chart.height,
                                ctx = chart.ctx;
                            
                            ctx.restore();
                            var fontSize = (height / 150).toFixed(2);
                            ctx.font = fontSize + "em sans-serif";
                            ctx.fillStyle = "#9b9b9b";
                            ctx.textBaseline = "middle";
                            var text = chart.data.datasets[0].data[0] + "%",
                                textX = Math.round((width - ctx.measureText(text).width) / 2),
                                textY = height / 2;
                            ctx.fillText(text, textX, textY);
                            ctx.save();
                        }
                        }],
                        options: {
                                maintainAspectRatio: false,
                                cutout: '85%',
                                rotation: Math.PI / 2,
                                plugins: {
                                legend: {
                                    display: false
                            },
                            tooltip: {
                                filter: (tooltipItem) => tooltipItem.dataIndex === 0
                            }
                            }
                        }
                    });
                }
            },
            actions : {
                load : function () {
                    preview.actions.clickInitial();
                    preview.actions.filterMap();
                },
                clickInitial : function () {
					$("[href='#menu0']").trigger("click");
				},
                loadActivityChart : function (chartData={}, chartType="", ctx) {
                    var labels = Object.keys(chartData);
                    var data = Object.values(chartData);
                    var backgroundColors = preview.actions.getRandomActivityColors(labels.length);
                    var chartConfig = {}

                    if (chartType == "pie") {
                        chartConfig = {
                            type: chartType,
                            data: {
                                labels: labels,
                                datasets: [{
                                    data: data,
                                    backgroundColor: backgroundColors
                                }]
                            },
                            options: {
                                responsive: true
                            }
                        }
                    }

                    if (chartType == "bar") { 	
                        chartConfig =  {
                            type: chartType,
                            data: {
                                labels: ['Data'],
                                datasets: [
                                    {
                                        label: 'Positive Gain',
                                        backgroundColor: 'rgb(0, 131, 126)',
                                        data: [chartData.positive],
                                        barThickness: 40
                                    },
                                    {
                                        label: 'Negative Loss',
                                        backgroundColor: 'rgb(116, 31, 24)',
                                        data: [-chartData.negative],
                                        barThickness: 40
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                scales: {
                                    x: {
                                        stacked: true
                                    },
                                    y: {
                                        stacked: true,
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                    let chartJs = new Chart(ctx, chartConfig);	
                },
                wordCloud : function (wordCdata) {
                    $('#wordCloud').jQCloud(wordCdata, {
                        delay: 100,
                        fontSize: {
                            from: 0.1,
                            to: 0.04
                        }
                    });
                },
                showMap : function (container, data={}) {
                    mylog.log("MarkerData", data);
                    let customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                    customMap.getPopup = function (data) {
                        var id = data._id ? data._id.$id:data.id;
                        var imagePath = (data?.profilMediumImageUrl != undefined) ? data.profilMediumImageUrl : defaultImage;
                        var imgProfil = mapCO.getOptions().mapCustom.getThumbProfil(data);
        
                        var eltName = data.title ? data.title:data.name;
                        var popup = "";
                        popup += "<div class='padding-5' id='popup" + id + "'>";
                        popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                        popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";
        
                        if(data.tags && data.tags.length > 0){
                            popup += "<div style='margin-top : 5px;'>";
                            var totalTags = 0;
                            $.each(data.tags, function(index, value){
                                
                                if (totalTags < 2 && value!=="Compagnon France Tiers-Lieux") {
                                    popup += "<div class='popup-tags'>#" + value + " </div>";
                                    totalTags++;
                                }
                            })
                            popup += "</div>";
                        }
                        if(data.address){
                            var addressStr="";
                            if(data.address.streetAddress)
                                addressStr += data.address.streetAddress;
                            if(data.address.postalCode)
                                addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                            if(data.address.addressLocality)
                                addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                            popup += "<div class='popup-address text-dark'>";
                            popup +=    "<i class='fa fa-map-marker'></i> "+addressStr;
                            popup += "</div>";
                        }
                        if(data.shortDescription && data.shortDescription != ""){
                            popup += "<div class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Description</div>";
                            popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
                            popup += "</div>";
                        }
                        if((data.url && typeof data.url == "string") || data.email || data.telephone){
                            popup += "<div id='pop-contacts' class='popup-section'>";
                            popup += "<div class='popup-subtitle'>Contacts</div>";
                            
                            if(data.url && typeof data.url === "string"){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                                popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                                popup += "</div>";
                            }
        
                            if(data.email){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                                popup += "</div>";
                            }
        
                            if(data.telephone){
                                popup += "<div class='popup-info-profil'>";
                                popup += "<i class='fa fa-phone fa_phone'></i> ";
                                var tel = ["fixe", "mobile"];
                                var iT = 0;
                                $.each(tel, function(keyT, valT){
                                    if(data.telephone[valT]){
                                        $.each(data.telephone[valT], function(keyN, valN){
                                            if(iT > 0)
                                                popup += ", ";
                                            popup += valN;
                                            iT++; 
                                        })
                                    }
                                })
                                popup += "</div>";
                            }
        
                            popup += "</div>";
                            popup += "</div>";
                        }
                        var url = '#page.type.' + data.collection + '.id.' + id;
                        popup += "<div class='popup-section'>";
                        // Mahefa
                        if(paramsMapCO.activePreview)
                            popup += `<a onclick='showAllNetwork.loadPreview("","${imagePath}","${data.name.replace("'", "#")}","${id}","tiersLieux", "${name}" , {}, "<?= $networkKey ?>", function(){},"${data.slug}")' class='item_map_list popup-marker' id='popup${id}'>`;
                        else
                            popup += "<a href='" + url + "' target='_blank' class='lbh item_map_list popup-marker' id='popup" + id + "'>";
                        popup += '<div class="btn btn-sm btn-more col-md-12">';
                        popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                        popup += '</div></a>';
                        popup += '</div>';
                        popup += '</div>';
        
                        return popup;                        
                    };
                    let appMap = new CoMap({
                        zoom : 5,
                        container : container,
                        activePopUp : true,
                        mapOpt:{
                            zoomControl: true,
                            doubleClick : true
                        },
                        activePreview : true,
                        mapCustom:customMap,
                        elts : {}
                    })        
                    appMap.addElts(data);
                },
                filterMap : function(dataFilter={}, containerId="", pathFilter={}, activity = "") {
                    var path = (Object.keys(pathFilter)[0] != undefined) ? Object.keys(pathFilter)[0] : "";
                    if (typeof path == "string" && notEmpty(path) && path.length > 0) {
                        var paramsFilter= {
                            interface : {
                                events : {
                                    page : true,
                                    scroll : true,
                                }
                            },
                            container : containerId,
                            urlData : baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/" + getNavigatorElement.surveyData.formId + "/answerPath/" + path,
                            defaults: {
                                notSourceKey : true,
                                types:["answers"],
                                indexStep : 9000,
                                forced : {
                                        filters : { }
                                    }
                            },
                        };
                        paramsFilter["filters"] = dataFilter;
                        paramsFilter.defaults.forced["filters"] = (Object.keys(pathFilter).length > 0) ? pathFilter : {} ;

                        var filterSearch={};
                        
                        filterSearch = searchObj.init(paramsFilter);
                        filterSearch.results.render=function(fObj, results, data){
                            let orgaId = "";
                            let type = "";
                            let answerOrga = {};
                            let markerData = [];
                            $.each(results, function (k, v) {
                                markerData.push(orgaObject[k])
                            })
                            markerData = Object.assign({}, ...markerData);
                            if(activity.length > 0){
                                if(activity == "Bureau / Coworking"){
                                    if(typeof showAllNetwork.organizationsData != "undefined"){
                                        $.each(showAllNetwork.organizationsData, function(k,v){
                                            if(typeof v.tags != "undefined" && (v.tags.includes("Bureau / Coworking") || v.tags.includes("Bureaux partagés / Coworking"))){
                                                if(typeof markerData[k] == "undefined"){
                                                    markerData[k] = v;
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            if (!(Object.keys(markerData).length > 0)) {
                                let rectifiedFilter = paramsFilter.defaults.forced["filters"];
                                for (var key in rectifiedFilter) {
                                    if (rectifiedFilter.hasOwnProperty(key)) {
                                        if (rectifiedFilter[key].hasOwnProperty("$exists")) {
                                            rectifiedFilter[key] = rectifiedFilter[key]["$exists"];
                                            delete rectifiedFilter[key]["$exists"];
                                        }
                                    }
                                }
                                preview.actions.filterMap({}, "", rectifiedFilter);   
                            }
                            preview.actions.showMap('#activityLinkMap', markerData)
                        };
                        
                        filterSearch.container = "#activityLinkMap" 
                        filterSearch.search.init(filterSearch);
                    }
                },
                generateCounterText : function (number=0, text="", color="", size=0, isPercent=false) {
                
                    return `
                        <h4 style="margin: 0px !important; color:${color}; font-size:${size};" >${(isPercent == true) ? Math.ceil(number) + " % " : number}</h4>
                        <span style="color:${color}; font-size:${size};"> ${text}  </span>
                    `;
                    ;
                }, 
                getLevel4Number : function (dataOrga={}) {
                    if (Object.keys(dataOrga).length > 0) {
                        let allLev4 = {};
                        $.each(dataOrga, function(key,value) {
                            let levelValue = value?.address?.level4;
                            if (levelValue != undefined) {
                                allLev4[levelValue] = "level4";
                            }          
                        })
                       
                        return (Object.keys(allLev4).length > 0) ? Object.keys(allLev4).length + 1 : 0;
                    }
                },
                transformObjectToArray : function (obj) {
                    let entries = Object.entries(obj);
                    let sortedEntries = entries.sort((a, b) => b[1] - a[1]);
                    let result = sortedEntries.map(([text, weight]) => ({ text, weight }));
                    return result;
                },
                transformObject : function (obj) {
					return Object.entries(obj).map(([axis, value]) => ({ axis, value }));
				},
                countActivityDomain : function (activityDomainData = {}, answerPath=undefined, typeAnswer) {
                    let result = {};
                    if (Object.keys(activityDomainData).length > 0) {
                        result = 0;
                        let dataWord = {}
                        $.each(activityDomainData, function(keyAct, valueAct) {
                            if (answerPath != undefined) {
                                let answer = jsonHelper.getValueByPath(valueAct, answerPath) ?? {};
                                let allDomain = {};
                                if (typeAnswer == "domain") {
                                    if (Object.keys(answer).length > 0) {
                                        $.each(answer, function(k, v) {
                                            allDomain[Object.keys(v)[0]] = "domain";
                                        })
                                        result = Object.keys(allDomain).length; 
                                    }
                                }  
                                if (typeAnswer == "value") {      
                                    if (Object.keys(answer).length > 0) {
                                        $.each(answer, function(k, v) {
                                            if (typeof dataWord[Object.keys(v)[0]] == "undefined") {
                                                dataWord[Object.keys(v)[0]] = 0;
                                            }
                                            dataWord[Object.keys(v)[0]] = dataWord[Object.keys(v)[0]] + 1;
                                        })
                                    }
                                }     
                            }
                        }) 
                        if (Object.keys(dataWord).length > 0) {
                            return preview.actions.transformObjectToArray(dataWord);
                        }
                    }
                    return result ;
                },
                showRadar : function (container='', dataContent=[]) {
					radarChart.defaultConfig.color = function() {};
					radarChart.defaultConfig.radius = 3;
					radarChart.defaultConfig.w = 600;
					radarChart.defaultConfig.h = 600;

					var data = [
						{
						className: 'default', 
						axes: dataContent
						}
					];

					var chart = radarChart.chart();
					var cfg = chart.config(); 
					var svg = d3.select(container).append('svg')
						.attr('width', "900")
						.attr('height', cfg.h + cfg.h / 4);
					svg.append('g').classed('single', 1).attr("transform", "translate(170, 90)").datum(data).call(chart);
				},
                onlyAnswer : function (data={}, path="") {
                    let result = {}
                    if( Object.keys(data).length > 0 && path != "" ) {   
                        
                        $.each(data, function (k, v) {
                            if (v.answers != undefined && Object.keys(v.answers).length > 0) {
                                let answerContent = jsonHelper.getValueByPath(v, path);
                                if (answerContent != undefined) {
                                    $.each(answerContent, function(ansKey, ansValue) {
                                       let valueKey =  Object.keys(ansValue)[0];
                                       if (typeof result [Object.keys(ansValue)[0]] == "undefined") {
                                            result [Object.keys(ansValue)[0]] = 0;
                                       }
                                       result [Object.keys(ansValue)[0]] = Number(result [Object.keys(ansValue)[0]]) + ((ansValue[valueKey].textsup != undefined) ? Number(ansValue[valueKey].textsup) : 0);
                                    })
                                }
                            }
                        })
                        return result;
                    }
                    return result;
                },
                getRandomActivityColors : function (count) {
					let colors = [];
					for (var i = 0; i < count; i++) {
						var color = '#' + Math.floor(Math.random() * 16777215).toString(16);
						colors.push(color);
					}
					return colors;
				},
                totalBenefOrAmount : function (data={}, path, benefOrAmountValue) {
                    let total = 0;
                    if (Object.keys(data).length > 0) {
                        $.each(data, function (index,value) { 
                            if (jsonHelper.getValueByPath(value, path) != undefined && !isNaN(jsonHelper.getValueByPath(value, path))) {
                                total = total + Number(jsonHelper.getValueByPath(value, path));
                            }
                        })
                    }
                    return total;
                }
            }
        }     
        preview.init();

        $('.listLeftBtn li').on('click',function() {
            $('.filters-activate').trigger('click');
            coInterface.showLoader("#activityLinkMap");
        })

        $('.listLeftBtn .active a').on('click',function() { 
            $(".parent-tab").css("color", "#244A58");
        })

    })
</script>