<style>
    .answers-prev-content .btn-prev-header .page-title {
        font-size: 20px;
    }
    div#map-answers {
        height: 65vh !important;
    }
</style>
<div class="btn-prev-header">
    <p class="page-title"><?= @$criteria ?> </p>
    <div class="btn-content">
        <button class="btn btn-all-tl <?= @$path != "list-tools" ? "" : "hidden" ?>" onclick='showAllNetwork.openAnswersMap(`<?= @$criteria ?>`, `<?= @$formId ?>`, `<?= @json_encode($path) ?>`)'>
            <i class="fa fa-eye" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Afficher les tiers lieux
        </button>
        <button class="btn btn-close-page">
            <i class="fa fa-times-circle" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Fermer
        </button>
    </div>
</div>

<div class="col-xs-12 answers-map-preview" style="display: none;">
	<div id="map-answers-filter"></div>
	<div id="map-answers"></div>
</div>
<div class="col-xs-12 answers-content-preview">
    <?= @$criteriaData["description"] ?>
</div>