<?php 
    $toolFormParam = [
        "formId" => "650d40a88b4bfa3a7e27f2e8",
        "inputId" => "650d4183cb82171c2116f016",
        "nameInput" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho1prnldcj8eml",
        "nameStep" => "lesCommunsDesTierslieux2292023_1125_0",
        "toolBtnPath" => "answers.lesCommunsDesTierslieux2292023_1125_0.lesCommunsDesTierslieux2292023_1125_0lmua2ho1prnldcj8eml",
        "toolBtnClass" => "tool-form-btn",
        "toolBtnName" => "INFORMATION DE L'OUTIL",

        "toolUrl" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho2ua9lls3phwk",
        "commonQualify" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho238a9q6jc5gh",
        "thematicVerb" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho2s6o8j4fyy3l",
        "thematicGenre" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho2y0qxvrkctcn",
        "firstNameNdName" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho2dcmozmqrvv",
        "phone" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho2iv6dav2vr4",
        "mail" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho3u2yqsq32ru",
        "comment" => "lesCommunsDesTierslieux2292023_1125_0lmua2ho3ks8z34wvwec",
    ];
    $isAuthorized = Authorisation::isElementAdmin($this->costum["contextId"],$this->costum["contextType"],Yii::app()->session["userId"]);
?>
<style>
    .tab-content {
        background: #fff;
        height: 100%;
    }
    .btn-return-form-tools {
        background: #8AC4C5 !important;
    }
    .btn-form {
        display: flex;
        justify-content: center;
    }
    .empty-text-info {
        margin-top: 1em;
        /* font-size: x-large; */
    }
    .link-tool {
        font-size: 17px;
    }
    .content-img-profil-preview {
        display: flex;
        justify-content: space-around;
    }
    .content-img-profil-preview  img{
        width: 15% !important;
    }
    
</style>

<script>
    $(function() {
        let toolsPreview = {
            imagePath : "params.imagesPath",
            answerAndForm : {},
            answerId : "",
            init: function() {
                showAllNetwork.isAuthorized = ("<?= $isAuthorized ?>" == "") ? 0 : 1;
                showAllNetwork.answerNameToComplete = "<?= $name ?? "" ?>";
                $("#section-info").trigger('click');
                this.actions.getFormAndAnswer();
                this.views.loadInformation();
                this.loadEvent();
            },
            loadEvent : function () {
                const checkFromsBtn = () => {
						if ($(".btn-return-form-tools").length > 0) {
							clearInterval(checkInterval);
                            var contentId = $("div.tab-pane.active.in").children().attr("id");
                            $(".btn-return-form-tools").off().on("click", function() {
                                var nameKey = "<?= $name ?>".replace(/[\s\/']/g, '').toLowerCase();
                                var imageUrl = (typeof toolsPreview.actions.getNewImage() == "string" && notEmpty(toolsPreview.actions.getNewImage())) ? toolsPreview.actions.getNewImage() : parentModuleUrl + '/images/default_tool.png';
                                $("img." + nameKey).attr("src", imageUrl);
                                $('[data-tool-name="<?= $name ?>"] img').attr("src", imageUrl);
                                $(showAllNetwork.clickSectionHref("#" + contentId)).attr("data-image-url", imageUrl);
                                // toolsPreview.init(showAllNetwork.answerNameToComplete);
                                $(showAllNetwork.clickSectionHref("#" + contentId)).trigger('click');
                                var dataToSendLinks = {
                                    id: getNavigatorElement.suplment.tool.id,
                                    collection: "forms",
                                    path: toolsPreview.imagePath + "." + nameKey,
                                    value : imageUrl
                                }
                                dataHelper.path2Value(dataToSendLinks, function (params) {});
                                if (typeof AllToolImage != undefined) 
                                    AllToolImage = {};
                                jsonHelper.setValueByPath(AllToolImage, nameKey, imageUrl);
                            })
						}
					}
				const checkInterval = setInterval(checkFromsBtn, 100);
            },
            views : {
                loadInformation : function () {
                    var toolInfoNdBtn = toolsPreview.actions.generateButton("<?= $toolFormParam["formId"] ?>", "<?= $toolFormParam["toolBtnPath"] ?>", "<?= $toolFormParam["toolBtnClass"] ?>", "<?= $toolFormParam["toolBtnName"] ?>");
                    toolsPreview.views.showInfo(toolInfoNdBtn);
                    $("#section-info").click(function() {
                        toolInfoNdBtn = toolInfoNdBtn.refresh();
                        toolsPreview.views.showInfo(toolInfoNdBtn);
                        if ($(this).attr("data-image-url") != undefined && typeof $(this).attr("data-image-url") == "string" && notEmpty($(this).attr("data-image-url"))) {
                            var imagePath = $(this).attr("data-image-url");
                            $("div.content-img-profil-preview img").attr("src", imagePath);
                        }
                    })
                },
                showInfo : function (infoContent) {
                    let getAnswers = {};
                    let answersContent = {};
                    if (toolsPreview?.answerId != undefined) 
                        getAnswers = jsonHelper.getValueByPath(infoContent, "getSuplForm." + toolsPreview.answerId);
                    if (getAnswers?.answers?.<?= $toolFormParam["nameStep"] ?> != undefined)
                        answersContent = getAnswers?.answers.<?= $toolFormParam["nameStep"] ?>;
                        var allImage = getNavigatorElement.getToolFormImages();
                        var image = (allImage["<?= $name ?>".replace(/[\s\/']/g, '').toLowerCase()] != undefined) ? allImage["<?= $name ?>".replace(/[\s\/']/g, '').toLowerCase()] : parentModuleUrl + '/images/default_tool.png';
                    let infoHtml = `
                        <div class="content-img-profil-preview">
                            <img class="img-responsive shadow2 thumbnail" src="${image}">
                        </div>
                        <div class="preview-element-info col-xs-12">
                            <h3 class="text-center"><?= $name ?></h3>
                            <div class="col-xs-12 btn-form" >${infoContent["html"]}</div>
                            <div class="col-xs-12 text-center emptyContent"></div>
                            <div class="col-xs-12 margin-top-20 text-center elementAddress"></div>
                            <div class="col-xs-12 text-center elementUrl" ></div>
                            <div class="col-xs-12 text-center margin-top-20 commonQualify"></div>
                            <div class="col-xs-12 text-center margin-top-20 thematicVerb"></div>
                            <div class="col-xs-12 text-center margin-top-20 phone"></div>
                            <div class="col-xs-12 text-center margin-top-20 mail"></div>
                            <div class="header-tags col-xs-12 text-center blockFontPreview margin-top-20"></div>
                        </div>
                        <div class="social-share-button-preview col-xs-12 text-center margin-top-20 margin-bottom-20">
                            <div class="socialBar"></div>
                        </div>
                        <div class="col-xs-10 col-xs-offset-1 margin-top-20 showTheNetwork"></div>
                        <div class="col-xs-10 col-xs-offset-1 margin-top-20 shortDescriptionHeader"></div>
                    `;
                    $("#info").html(infoHtml)
                              .removeClass("col-xs-10")
                              .find(".img-responsive")
                              .css("width","-webkit-fill-available")
                    if (toolsPreview.answerId == "" || answersContent == undefined || Object.keys(answersContent).length == 1) 
                        $(".emptyContent").html(`
                            <p class="empty-text-info">Il n'y a pas d'information sur cet outil. Veuillez contacter l'administrateur du site.</p>
                        `)

                    if (answersContent != undefined && Object.keys(answersContent).length > 0) {
                        mylog.log("test test", answersContent);
                        mylog.log("test test", "<?= $toolFormParam["toolUrl"] ?>");
                        if (answersContent.<?= $toolFormParam["toolUrl"] ?> != undefined)
                            $(".elementUrl").html(`<a class="link-tool" href="${answersContent.<?= $toolFormParam["toolUrl"] ?>}"><i class="fa fa-link"></i> Lien du site de l'outil</a>`);
                        if (answersContent.<?= $toolFormParam["commonQualify"] ?> != undefined)
                            $(".commonQualify").html(`<p>Personne qualifieé de ce commun : <b>${answersContent.<?= $toolFormParam["commonQualify"] ?>} ${(answersContent.<?= $toolFormParam["firstNameNdName"] ?> != undefined) ? "( " + answersContent.<?= $toolFormParam["firstNameNdName"] ?> + " )" : ""}</b></p>`);
                        if (answersContent.<?= $toolFormParam["thematicVerb"] ?> != undefined)
                            $(".thematicVerb").html(`<p>Thématique (verbe) de ce commun : <b>${answersContent.<?= $toolFormParam["thematicVerb"] ?>}</b></p>`);
                        if (answersContent.<?= $toolFormParam["thematicGenre"] ?> != undefined)
                            $(".thematicGenre").html(`<p>Thématique (genre) de ce commun : <b>${answersContent.<?= $toolFormParam["thematicGenre"] ?>}</b></p>`);
                        if (answersContent.<?= $toolFormParam["phone"] ?> != undefined)
                            $(".phone").html(`<p>Télephone : <b>${answersContent.<?= $toolFormParam["phone"] ?>}</b></p>`);
                        if (answersContent.<?= $toolFormParam["mail"] ?> != undefined)
                            $(".mail").html(`<p>Email : <b>${answersContent.<?= $toolFormParam["mail"] ?>}</b></p>`);                            
                    }
                }
            },
            actions : {
                getNewImage: function () {
                    var imageUrl = "";
                    var imageContent = {};
                    var number = 0;
                    if ($("li.qq-upload-success").length == 1) {
                        imageContent = $("li.qq-upload-success");
                    } else if ($("li.qq-upload-success").length > 0) {
                        for (let index = 0; index < $("li.qq-upload-success").length; index++) {
                            if (!isNaN($($("li.qq-upload-success")[index]).attr("qq-file-id"))) {
                                var currentNumber = Number($($("li.qq-upload-success")[index]).attr("qq-file-id"))
                                if (number == 0) { 
                                    number = currentNumber;
                                    imageContent = $($("li.qq-upload-success")[index]);
                                } else if (currentNumber <= number) {
                                    number = currentNumber;
                                    imageContent = $($("li.qq-upload-success")[index]);
                                }
                            }
                        }
                    } 
                    if (typeof imageContent == "object" && Object.keys(imageContent).length > 0) {
                        imageUrl = imageContent.find("a.qq-upload-link").attr("href");
                    }
                    return imageUrl;
                },
                getFormAndAnswer : function () {
                    var params = {};
                    params.where = {"answers.<?= $toolFormParam["nameStep"] ?>.<?= $toolFormParam["nameInput"] ?>" : "<?= $name ?? "" ?>"}
                    var getInput = showAllNetwork.getElement("forms", "<?= $toolFormParam["inputId"] ?>");
                    var getAnswer = showAllNetwork.getAnswers("/form/" + "<?= $toolFormParam["formId"] ?>", params).allAnswers;
                    toolsPreview.answerId = (Object.keys(getAnswer)[0] != undefined) ? Object.keys(getAnswer)[0] : "";
                    jsonHelper.setValueByPath(toolsPreview.answerAndForm, "<?= $toolFormParam["formId"] ?>.allAnswers", getAnswer);
                    jsonHelper.setValueByPath(toolsPreview.answerAndForm, "<?= $toolFormParam["formId"] ?>.form", {subForms: ['<?= $toolFormParam["nameStep"] ?>'], _id:{$id:'<?= $toolFormParam["formId"] ?>'}});
                    jsonHelper.setValueByPath(toolsPreview.answerAndForm, "<?= $toolFormParam["formId"] ?>.inputs", getInput.inputs);
                },
                generateButton : function (suplForm="", btnPath="", btnClass="", formName="", userId, loadClickEvent=false, clickEventClass="") {
                    var button = {};
                    // var getSuplForm = preview.actions.getAnswerForm (btnPath + "." + preview.organizationId, preview.organizationId, suplForm, userId);
                    var getSuplForm = (toolsPreview.answerAndForm[suplForm] != undefined) ? toolsPreview.answerAndForm[suplForm] : {};
                    var initSuplFormData = getSuplForm;		
                    getSuplForm = getSuplForm.allAnswers;
                    if (Object.keys(getSuplForm).length > 0) {
                        var resultAnswerId = Object.keys(getSuplForm)[0];
                        var btnColor = showAllNetwork.setButtonColor(getSuplForm[resultAnswerId].answers,initSuplFormData.inputs, formName, initSuplFormData.form.subForms);
                        button.html = showAllNetwork.modalButtonHtml(btnClass, resultAnswerId, suplForm);
                        button.htmlWhithName = showAllNetwork.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor);
                        button.htmlPublicWithName = showAllNetwork.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor, "", true);
                        button.getSuplForm = getSuplForm;
                        button.answerId = resultAnswerId;
                        button.canShowContent = true;
                    } else {
                        var newResultAnswerId = showAllNetwork.newAnswers(suplForm, function(result) {
                            var dataToSend = {
                                id: result['_id']['$id'],
                                collection: "answers",
                                path: btnPath,
                                value : showAllNetwork.answerNameToComplete,
                            }
                            dataId = data['_id']['$id'];
                            dataHelper.path2Value(dataToSend, function (params) {});
                        }, (showAllNetwork.isAuthorized == 1)); 
                        button.html = showAllNetwork.modalButtonHtml(btnClass, newResultAnswerId, suplForm);
                        button.htmlWhithName = showAllNetwork.modalButtonHtml(btnClass, newResultAnswerId, suplForm, "", formName);
                        button.htmlPublicWithName = showAllNetwork.modalButtonHtml(btnClass, newResultAnswerId, suplForm, "", formName, "", "", true);
                        button.canShowContent = false;									
                    }
                    if (btnClass != "")
                        setTimeout(() => {showAllNetwork.clickModalEvent("." + btnClass, function () { toolsPreview.loadEvent(); }, "tools")}, 100);

                    button.refresh = function (btnName="") {
                        return toolsPreview.actions.generateButton(suplForm, btnPath, btnClass, (btnName != "") ? btnName : formName, userId, loadClickEvent, clickEventClass)
                    }
                    return button;
                },
            },

        }
        toolsPreview.init();
    });
</script>