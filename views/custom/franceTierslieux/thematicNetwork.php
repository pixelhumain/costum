<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
        "/js/franceTierslieux/dataviz/getElement.js",
    );
	$getOptions = PHDB::find( Organization::COLLECTION,["tags" => "RéseauTiersLieux" ], ['name',"profilMediumImageUrl", 'address', "slug"]) ?? [];
	foreach ($getOptions as $key => $value) {
        $inScope = PHDB::find(Organization::COLLECTION , array(
        "address.level3" => $value['address']['level3'],
        '$or' => array(
            array("reference.costum"=>"franceTierslieux"),
            array("source.keys"=>"franceTierslieux")
        ) ), ["name", "collection", "profilImageUrl"] );
        $orga = array();
        $arrayName = array();
        $networkImage = "";
        foreach ($inScope as $key => $item) {
            unset($item["_id"]);
            array_push($communityIds, $key);
            $item["type"] = $item["collection"];
            unset($item["collection"]);
            array_push($orga, [$key => $item]);
            array_push($arrayName, $item["name"]);
            $networkImage = (isset($item["profilImageUrl"])) ? $item["profilImageUrl"] : "";
        }
        $where[$value['name']] = array("where" => array("links.organizations" => array('$in'=>$orga)), "count" => count($orga), "level3Key" => $value['address']['level3'], "arrayName" => $arrayName, "imageUrl" => $networkImage);
    }
    $getOptions = array_map(function($item) {
		return array($item["slug"] => array("name" => $item["name"], "profilMediumImageUrl" => $item["profilMediumImageUrl"], "address" => $item["address"]));
	}, array_values((array) $getOptions));
    $getOptions = array_reduce($getOptions, 'array_merge', array());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
?>
<style>
	.ans-dir .bodySearchContainer, .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }
    .ans-dir .img-responsive-action { 
        height: inherit !important;
    }
</style>
    <div class="container network-dir navigator-page padding-bottom-20 padding-left-0 padding-right-0 ans-dir" style="display: block;"></div>
    <div id="custom-prev-div" style="display: block;"></div>
    <!-- <div class="container padding-bottom-20 padding-left-0 padding-right-0 shapeContainer">
        <div class="row">
            <h2 class="text-left">Les réseaux</h2>
            <button class="btn-filters-region btn btn-primary" style="margin-left: auto;display: block;" data-action="liste">Mode Liste</button>
            <hr />
        </div>
    </div> -->
<script>  
let initPagination = "";
var whereLists = <?= json_encode($where) ?>; 
var nameBySlug = <?= json_encode($getOptions) ?>;

$(function(){
    directory.PanelHtml = function(params){
        params.image = (params.image == "") ? defaultImage : params.image;
        var str = `
            <div class="team">
                <div class="card-team">
                    <div class="img-box">
                        <img class="img-responsive-action" alt="Responsive Team Profiles" src="${params.image}" />
                    </div>
                    <p class="network-grand-title">
                        <a href="javascript:" onclick='showAllNetwork.showModal("${params.name}", "", "${params.orgaNameArray}", ".reseauxFooter")' class="">
                            ${params.name}
                        </a>
                    </p>
                  `;  
        
        str += `
                    </div>
            </div>
        `;
        return str;
    }
    showAllNetwork.paramsFilters.results.renderView = "directory.PanelHtml";
    showAllNetwork.paramsFilters.defaults.textPath = "answers." + getNavigatorElement.suplment.networkSupl.thematicPath;
    showAllNetwork.paramsFilters['urlData'] = baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk";
    showAllNetwork.paramsFilters.defaults.types = ["answers"];
    // showAllNetwork.paramsFilters.defaults.filters = {tags: ['RéseauTiersLieux']};
    showAllNetwork.paramsFilters.defaults.filters = {["answers." + getNavigatorElement.suplment.networkSupl.thematicPath] : {'$exists': true}};
    // showAllNetwork.paramsFilters.defaults.fields = ["name","profilMediumImageUrl","slug"]
    showAllNetwork.paramsFilters.defaults.fields = ["answers." + getNavigatorElement.suplment.networkSupl.thematicPath, getNavigatorElement.suplment.networkSupl.path];
    showAllNetwork.paramsFilters.defaults.params = {thematicPath: getNavigatorElement.suplment.networkSupl.thematicPath, finderPath: getNavigatorElement.suplment.networkSupl.path};
    searchObj.footerDom = ".reseauxFooter";
    $(".ans-dir").html(showAllNetwork.initView("Réseaux thématiques","reseauxFooter"));
    $(".btn-filters-region").remove();
    let filter = searchObj.init(showAllNetwork.paramsFilters);
    // showAllNetwork.initEvents();
    filter.search.init(filter);
    <?php if(isset($_GET["slug"])){ ?>
        showAllNetwork.loadPreview('',`${nameBySlug['<?= $_GET["slug"] ?>'].profilMediumImageUrl}`,`${nameBySlug['<?= $_GET["slug"] ?>'].name}`,'' ,'reseaux', '', '', `${nameBySlug['<?= $_GET["slug"] ?>'].address.level3}`)
    <?php } ?>
    $(".container-filters-menu").attr("style","height : 50px !important");
}) 

$('[data-dismiss="modal"]').click(function(){
    $(".reseauxFooter").html(initPagination);
})
</script>



