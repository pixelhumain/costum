<div class="navigator-prev-contents">
    <style>
        .vertical-menu {
            padding-right: 0px !important;
        }

        .vertical-menu ul li {
            margin-top: 15px;
        }

        .vertical-menu ul li a {
            text-decoration: none;
        }

        /* .listLeftBtn .active a {
            border-left: solid 4px ;
            padding-left: 12px ;
            color: black !important;
        } */
        .listLeftBtn .active a {
            /* border-left: solid 4px ; */
            padding-left: 12px;
            /* color: #fff !important;		 */
            color: #8DBC21 !important;
        }

        .all-economic-model-btn {
            display: flex;
            justify-content: center;
        }

        .all-economic-model-btn button {
            margin: 2px;
        }

        .tab-pane {
            margin-bottom: 100px;
        }

        .listLeftBtn a[aria-expanded="false"] {
            text-decoration: none;
            /* color: #737f8b; */
            color: #244A58;
        }

        .tab-content {
            overflow-y: auto;
            height: 70vh;
        }

        .parent-tab {
            cursor: pointer;
        }

        /* .content-img-profil-preview {
            margin-top: 61.5px;
        }
        .img-responsive {
            margin: auto;
            box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);
        } */
        .listLeftBtn {
            list-style: none;
            padding-left: 0px;
            height: 85vh;
            overflow: auto;
            overflow-x: auto;
        }

        /* .toolsMenu {
            background: #e3e3e3;
        } */

        /* dark */
        .toolsMenu {
            /* background: #1C0C5B; */
            /* background: #4A6FA5; */
            background: white;
            color: black;
            /* color: white; */
        }

        .enlarge {
            top: 0px !important;
            left: 0px !important
        }

        /* dark */
        .vertical-menu {
            /* background: #270082; */
            /* background: #394648; */
            /* color: white !important; */
            background: #EDF0F2;
        }

        .goToItemPage {
            /* background: #4623C9 !important; */
            background: #8AC4C5;
        }

        .tool-image {
            float: left;
            margin-right: 10px !important;
            width: 10%;
        }

        .text-network {
            line-height: inherit;
            color: #3069a3;
        }

        .section-pie-name {
            color: black;
        }

        .navigator-prev-contents .vertical-menu,
        .navigator-prev-contents .listLeftBtn {
            height: 65vh !important;
            /* margin: 1%; */
        }

        .all-content-prev {
            box-shadow: -10px -5px 34px 0px #0000001C;
            border-radius: 14px;
        }
        .all-btn-form {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }
        .all-btn-form .btn-content{
            width: 32%;
            margin: 11px auto;
            background-color: aliceblue;
            border-radius: 11px;
        }

        .info-content{
            text-align: justify;
        }

        /* .tab-pane {
            margin-bottom: 200px !important;
        } */
    </style>
    <div class="form-answer-content" ></div>
    <div class="col-xs-12 vertical-menu all-content-prev">
        <div class="col-xs-12 tab-content container" style="padding-left: 30px; padding-right: 30px; padding-top: 20px; text-align: justify;">
            <div class="text-info">
                <p>Tiers-lieux de France, c’est le moment de montrer notre rôle politique dans le quotidien de nos territoires. C’est le moment de se mobiliser pour accueillir les débats et les revendications politiques et sociales. Soyons les tiers-lieux que nous avons toujours voulu être.</p>
                <h4>Comme <strong>Antoine Burret</strong> a dit</h4>
                <ul class="citation">
                    <li>
                        Dans la culture française, un tiers-lieu ne peut pas honnêtement se dire tiers-lieu sans avoir une dimension de contestation et de débats politiques.
                    </li>
                    <li>
                        Les tiers-lieux sont dans une proximité géographique et émotionnelle qui les conduisent de fait à recevoir tous les maux de la société. S'il n'y a nulle part d'autre ou aller, c'est dans le tiers-lieu le plus proche que cela se passe.
                    </li>
                    <li>
                        Les tiers-lieux représentent non seulement un foyer de résistance politique, mais aussi une capacité à résister aux crises.
                    </li>
                    <li>
                        Dans quelle mesure le créateur d'un tiers-lieu commet un acte politique ou de soutien communautaire ?
                    </li>
                    <li>
                        Les tavernes, les pubs ou les cafés historiques sont devenus des lieux de résistance politique et de résistance aux crises.
                    </li>
                    <li>
                        Les dirigeants des régimes totalitaires savent parfaitement le potentiel politique des tiers-lieux et c'est pourquoi ils les découragent activement.
                    </li>
                </ul>
                <h4>Pour mémoire quote un peu en rapport avec le moment :</h4>
                <ul class="info-content">
                    <li>Pourtant, certaines communautés continuent de se reconnaître dans ce mot. Ce qu’elles expriment avec lui, c’est une certaine idée de ce que devrait être un lieu public de proximité géré par une communauté. Un lieu de rencontre avec l’autre, l’ennemi, le contradicteur. Un lieu dans lequel les activités ne sont qu’un prétexte pour réunir la diversité d’un milieu. Un lieu qui nous attache, où l’on peut rire, être soi-même, s’engueuler, accueillir, expérimenter, créer, travailler si nécessaire, mais toujours avec le sentiment d’être reconnu. Un lieu qui hybride les postures, qui fait cohabiter les différences, mais qui n’installe pas de hiérarchie. Qui appartient aux habitants d’un quartier, et qui donne le sentiment d’être d’une ville, d’un pays, où l’on peut exprimer librement ses opinions, participer même de manière informelle à la vie publique et agir sur des domaines critiques. Où l’on peut manifester son mécontentement et en discuter avec ses opposants. Des tiers-lieux qui ne veulent pas aspirer les financements publics qui jusqu’alors étaient dévolus aux personnes les plus démunies. Des tiers-lieux qui ne veulent pas tout faire et tout être à la fois, au gré des agendas politiques. Des tiers-lieux qui ne veulent pas amortir la destruction programmée des services publics et qui se battent contre ceux qui disent vouloir « montrer à la puissance publique qu’on peut faire mieux avec moins ». Des tiers-lieux où il est question de poésie et pas de rentabilité, de l’avenir et pas du prochain mandat, qui veulent éviter une guerre civile et ne pas en être l’instrument.</li>
                </ul>
            </div>
            <div id="all-form">
                <h4>Votre tier lieu n'est pas encore mobilisé ? Cliquer sur l'icon <i class="fa fa-pencil"></i> pour le déclarer</h4>
                <div class="row all-btn-form">
                    <div class="btn-content text-center">
                        <h5 style="margin-top: 24px;"> Tiers Lieux de France mobilisons nous !</h5>
                        <div class="btn-content">
                            <div class="btn-to-form">
                                <button 
                                    class="btn btn-form-modal btn-small bg-blue mobilizeBtn" style="cursor: pointer; color: white; border-radius: 50%;" 
                                    data-elformid="666c245dd4968131956aa91e"
                                    data-path="answers.lesCommunsDesTierslieux1462024_1313_0.finderlesCommunsDesTierslieux1462024_1313_0lxeldfuzo8xtrxheuwo.641e0b86136056552d3b689e"
                                    data-input="empty">
                                    <i class="fa fa-pencil"></i>
                                </button>		
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".mobilizeBtn").off().on('click', function (e) {
        var containerParentId = $(this).parent().parent().attr('id');
        var formId = $(this).data("elformid");
        var step = $(this).data("step");
        var input = $(this).data("input");
        var path = $(this).data("path");
        showAllNetwork.newAnswers(formId, function(data, dataId){
            urlCtrl.loadByHash(`#answer.index.id.${dataId}.mode.w`);
        }, true);
    });
</script>