<div class="mp-progress">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
            <span class="sr-only">45% Complete</span>
            <span class="progress-type">45%</span>
        </div>
    </div>
    <div class="text-progress">
        DES TIERS-LIEUX DANS LES 22 MÉTROPOLES
    </div>
</div>