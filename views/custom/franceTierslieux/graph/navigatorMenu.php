<style type="text/css">

</style>

<?php 
    $cssAnsScriptFilesModule = array(
	    '/js/default/preview.js'
    );
    
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<div id="preview-elt-navigatormenu" class="col-xs-12 no-padding" style="background:#4623c9">
	<div class="col-xs-12 padding-10">
		<button class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i> FERMER
		</button>
	</div>
	<div class="container-preview padding-right-20" style="overflow-y: auto; padding: 0px 4.2em 0 4.2em">
		<h1 class="margin-bottom-20" style="color:#ff286b">LE PANORAMA 2023</h1>
        <div id="containerpreview" class="row"></div>
        <h1 class="margin-top-50" style="color:#ff286b">L'OBSERVATOIRE</h1>
	</div>
</div>

<script type="text/javascript">
    $('.sectionAnchor').each(function(index, el){
        console.log("preview element b", $(el.querySelector(".sectionAnchorTitle > b")).length);
        $("#containerpreview").append(`<div class="col-md-6">
            <a href="#${$(el).parent().parent(".cmsbuilder-block[data-blocktype='section']").attr("id")}" class="text-white lbh-anchor-btn" style="font-weight:bold; font-size: 16pt; text-decoration:none">
                ${$(el).find("b").eq(1).text()}
            </a>
        </div>`);
    })

    $(document).off("click",".lbh-anchor-btn").on("click",".lbh-anchor-btn", function(e){
        e.stopPropagation();
        e.preventDefault();
        var idBtnLink = $(this).attr("href");
        $('html,body').animate({
            scrollTop: $(idBtnLink).offset().top-60},
        'slow');
    });
</script>