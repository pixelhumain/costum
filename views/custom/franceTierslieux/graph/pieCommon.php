<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>


<div id="canvas-holder" style="margin:0px auto;width:65%">
		<canvas id="<?php echo $id?>-canva"></canvas>
</div>

<script>
jQuery(document).ready(function() {
	mylog.log("render","/modules/costum/views/custom/franceTierslieux/graph/pieModel.php");
	var config = {
		type: 'pie',
		data: <?php echo $id ?>Data
	};
var canvas = document.getElementById('<?php echo $id?>-canva');
if (canvas.getContext) {
	var ctx = canvas.getContext('2d');

	window.myPie<?php $id ?> = new Chart(ctx, config);
}
else{
	alert("canva n'est pas supporté !");
}	

});

</script>