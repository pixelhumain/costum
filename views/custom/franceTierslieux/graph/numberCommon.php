<?php
    if($this->costum["contextType"] && $this->costum["contextId"]){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    }
?>

<style type="text/css">
    .effectif{
      padding: 1em 0.2em;
      /*background: #ddd;*/
    }

    .dash-icon{
        font-size: 2em;
        border-radius: 50%;
        padding: 0.8em;
        margin-top: 0.3em;
        margin-bottom: auto;
        background: #eee;
    }
    .text-theme{
        color: #4E54C9 <?php //echo (isset($this->costum["css"]["color"]["blue"])?$this->costum["css"]["color"]["blue"]:"#4E54C9") ?>;
    }
</style>
<?php

// formulaire
    $paramsData = [
        "title" => "",
        "coform" => "",
        "path" => "",
        "type" => ["count"=>"Compteur", "median"=>"Médiane", "average"=>"Moyenne", "persent"=>"Pourcentage"],
        "width" => ["2"=>2, "3"=>3, "4"=>4, "6"=>6, "8"=>8, "9"=>9, "10"=>10, "12"=>12],
        "typeValue" => "",
        "widthValue" => ""
    ];
    if(!isset($childForm) && !isset($formInputs)){
        // Get all Forms
        $childForm = PHDB::find(Form::COLLECTION, array("parent.".$this->costum['contextId']=>['$exists'=>true],"active"=>"true"));
        $formInputs = [];
        foreach ($childForm as $formKey => $formValue) {
            if(is_array($formValue["subForms"])){
               $subFormId =  array('$in' => $formValue["subForms"]);
            }

            if(is_string($formValue["subForms"]) && $formValue["subForms"] !=""){
                $subFormId =  $formValue["subForms"];
            }

            if(isset($subFormId) && is_array($subFormId) && $subFormId!=""){
                $subForms = PHDB::find(Form::COLLECTION, array(
                'id' => $subFormId));
                foreach ($subForms as $subFormKey => $subFormValue) {
                    if(isset($subFormValue["inputs"])){
                        $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                    }
                }
            }
        }
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["coform"])){
        $formId = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["coform"];
        $paramsData["coform"] = $formId;
    }

    if(isset($el["costum"]["dashboard"]["config"][$id])){
        $paramsData["path"] = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["path"];
        $paramsData["widthValue"] = $el["costum"]["dashboard"]["config"][$id]["width"];
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["type"])){
        $paramsData["typeValue"] = $el["costum"]["dashboard"]["config"][$id]["type"];
    }

    if(isset($el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["title"])){
        $paramsData["title"] = $el["costum"]["dashboard"]["config"][$id]["graph"]["data"]["title"];
    }

    // Default Form
    if(isset($formId)){
        $formTL = PHDB::findOneById(Form::COLLECTION, $formId);
    }else{
        $formTL = PHDB::findOne(Form::COLLECTION, array("parent.".$this->costum["contextId"] => array('$exists' => true)));
    }

    $formConfig = [];

    if(isset($path) && $path!="" && isset($formTL) && isset($formTL["params"][explode(".", $path)[1]])){
        $formConfig = $formTL["params"][explode(".", $path)[1]];
    }

$answers = [];

$answers = PHDB::find(Answer::COLLECTION, array('form' => (string)$formTL['_id'], "draft" => array('$exists' => false), "answers" => array('$exists' => true)));
$answerPath = array();

if(isset($path) && $path!=""){
    $answerPath = explode(".", $path);
}

$numberArray = array();
$firstValue = null;
foreach ($answers as $key => $value) {
    if(count($answerPath)==2 && isset($value["answers"][$answerPath[0]][$answerPath[1]])){
        $answer = $value["answers"][$answerPath[0]][$answerPath[1]];
    }else{
        $answer = null;
    }

    if(is_array($answer)){
        if(isset($answer["value"]) && $answer["value"]=="Oui"){
            array_push($numberArray, $answer);
        }else if(!isset($answer["value"])){
            var_dump($answer);
            foreach ($answer as $key => $value) {
                if($firstValue==null){
                    $firstValue = array_key_first($value);
                }

                if($firstValue==array_key_first($value)){
                    array_push($numberArray, $answer);
                }
            }
        }
    }else{
        if(!is_null($answer)){
            array_push($numberArray, $answer);
        }
    }
}

$firstValue = null;

$number = 0;
//echo $this->costum["contextId"];
//echo $formTL['_id']->{'$id'};
if(isset($type)){
    if($type=="median"){
        sort($numberArray);
        $count = sizeof($numberArray);
        $index = floor($count/2);
        if (!$count) {
            //echo "no values";
        } elseif ($count & 1) {
            $number = $numberArray[$index];
        } else {
            $number = ($numberArray[$index-1] + $numberArray[$index]) / 2;
        }

        $title = isset($title)?$title." (Médiane)":"";
    }else if($type=="average"){
        sort($numberArray);
        $count = sizeof($numberArray);
        $numberSum = 0;

        for ($k=0; $k < $count ; $k++) {
            $numberSum += $numberArray[$k];
        }
        $number = round(($numberSum/$count),2);
        $title = isset($title)?$title." (Moyenne)":"";
    }
    else if($type=="persent"){
        $percentage = (100*count($numberArray))/count($answers);
        $percentage = round($percentage, 2);
        if(is_nan($percentage)){
            $percentage = 0;
        }
        $number = $percentage."%";
    }else if($type=="count"){
        $number = count($numberArray);
    }
}

?>
<div class="effectif">
    <div class="row ">
        <div class="col-md-4">
            <i class="fa fa-paperclip text-theme  dash-icon"></i>
        </div>
        <div class="col-md-8">
            <h6>
                <?php echo $title; ?>
            </h6>
            <h1 class="text-info">
                <?php echo $number ?>

                <?php if(Authorisation::isInterfaceAdmin()){
                    echo '<span class="pull-right" style="position:absolute; bottom:-14px; right: 10px"><button class="btn btn-sm btn-info dashboarConfigBtn edit'.$id.'Params" data-id="'.$id.'">
                            <i class="fa fa-cogs"></i>
                        </button> ';

                    echo ' <button class="btn btn-sm btn-danger removeConfigBtn remove'.$id.'Params" data-id="'.$id.'">
                            <i class="fa fa-trash"></i>
                        </button></span>';
                } ?>
            </h1>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        contextData = <?php echo json_encode($el); ?>;
        var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
        var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
        var contextName = <?php echo json_encode($el["name"]); ?>;
        contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
    });
</script>

<script type="text/javascript">
    let sectionDyf = {};
    let tplCtx = {};

    jQuery(document).ready(function() {
        if(localStorage.getItem("previewMode") && localStorage.getItem("previewMode")=="v"){

            $(".tlDashboarConfig").hide();
        }else{
            $(".tlDashboarConfig").show();
        }

        sectionDyf.<?php echo $id ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $id ?>Params = {
            "jsonSchema" : {
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        "inputType" : "text",
                        "label" : "Libellé",
                        "value" :  sectionDyf.<?php echo $id ?>ParamsData.title
                    },
                    "url" : {
                        "inputType" : "select",
                        "label" : "Quelle type de graph",
                        "class" : "form-control <?php echo $id ?>",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $id ?>ParamsData.type,
                        "value":sectionDyf.<?php echo $id ?>ParamsData.typeValue
                    },
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $id ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {
                            <?php
                            foreach($childForm as $key => $value) {
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }
                            ?>
                        },
                        "value": sectionDyf.<?php echo $id ?>ParamsData.coform
                    },
                    "path" : {
                        "inputType" : "select",
                        "class" : "<?php echo $id ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {}
                    },
                    "width" : {
                        "inputType" : "select",
                        "class" : "form-control <?php echo $id ?>",
                        "label" : "Largeur d'espace à occuper",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $id ?>ParamsData.width,
                        "value":sectionDyf.<?php echo $id ?>ParamsData.widthValue
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $id ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $id ?>ParamsData.coform);
                    }
                    if($("#path.<?php echo $id ?> option[value='"+sectionDyf.<?php echo $id ?>ParamsData.path+"']").length > 0){
                        $("#path.new-graph").val(sectionDyf.<?php echo $id ?>ParamsData.path);
                    }
                },
                save : function (data) {
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $id ?>", "data":{}};

                    $.each( sectionDyf.<?php echo $id ?>Params.jsonSchema.properties , function(k,val) {
                        if(k != "width"){
                            if(k == "url"){
                                tplCtx.value["graph"][k] = "/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.numberCommon/type/"+$("#"+k).val();
                                tplCtx.value["type"] = $("#"+k).val();
                            }else{
                                tplCtx.value["graph"]["data"][k] = $("#"+k).val();
                            }
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    tplCtx.value["counter"] = "";
                    tplCtx.value["title"] = "";

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $id ?>Params").off().on("click",function() {
            tplCtx["id"] = contextData.id;
            tplCtx["collection"] = contextData.collection;
            tplCtx["path"] = "costum.dashboard.config."+$(this).data("id");
            dyFObj.openForm( sectionDyf.<?php echo $id ?>Params,null, sectionDyf.<?php echo $id ?>ParamsData);
        });

                $(".remove<?php echo $id ?>Params").off().on("click",function() {
            tplCtx["id"] = contextData.id;
            tplCtx["collection"] = contextData.collection;
            tplCtx["path"] = "costum.dashboard.config."+$(this).data("id");
            tplCtx["value"] = {};

            bootbox.dialog({message:`<div class="alert-white text-center"><br>
                <strong>Vous voulez vraiement supprimer cette section de graph</strong>
                <br><br>
                <button id="deleteGraphBtn" class="btn btn-danger bootbox-close-button">JE CONFIRME</button>
                <button type="button" class="btn btn-default bootbox-close-button" aria-hidden="true">NON, ANNULER</button></div>`});

                $("#deleteGraphBtn").on("click", function(){
                    dataHelper.path2Value( tplCtx, function(params) {
                        toastr.info("La suppression de graph <?php $paramsData['title'] ?>");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                    });
                });
        });

        $(document).on("change", "#coform.<?php echo $id ?>", function(){
            updateInputList($(this).val());
        });

        let updateInputList = function(value){
            let childForm = <?php echo json_encode($formInputs) ?>;
            $("#path.<?php echo $id ?>").empty();

            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    if(input["type"] == "text"){
                        $("#path.<?php echo $id ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes("multiRadio")){
                        $("#path.<?php echo $id ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }

                    if(input["type"].includes("multiCheckboxPlus")){
                        $("#path.<?php echo $id ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }
        }
    });

</script>
