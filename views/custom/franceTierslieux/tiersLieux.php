<?php 
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/js/franceTierslieux/answerDirectory.js",
		"/js/franceTierslieux/dataviz/getElement.js",
    );
	$formId = "63e0a8abeac0741b506fb4f7";
    $answerStep = "franceTierslieux2022023_753_10";
    $answerInput = "franceTierslieux2022023_753_10lemfa0njory1oxpszb";
	// $formInputs = FranceTierslieux::getInputsOrElement(Form::COLLECTION,$formId);
	$formInputs = [];
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( "costum" )->getAssetsUrl());
?>

<!-- <style>
	.ans-dir .bodySearchContainer,.ans-dir .footerSearchContainer .body-search-modal{
        height: auto !important;
        position: relative;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 100%;
        justify-content: space-between;
        align-items: stretch;
    }
    .ans-dir .divEndOfresults{
        display: none;
    }

	@media screen and (min-width: 765px) {
	/* Vos règles de style ici */
		#filterContainercacs {
			border: solid 1px !important;
			border-radius: 15px;
			padding: 15px;	
			margin-bottom: 15px;	
		}
	}

	@media screen and (min-width: 1287px) {
	/* Vos règles de style ici */
		#filterContainercacs {
			border: solid 1px !important;
			border-radius: 15px;
			padding: 15px;	
			/* margin-bottom: 15px;	 */
			width: 19%;
			margin: 0px;
			right: 72%;
			position: absolute;
		}

		.headerSearchIncommunity, .bodySearchContainer, .footerSearchContainer {
			width: 75% !important;
    		left: 25% !important;		
		}

		#filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn {
    		padding: 10px 22px !important; 
		}

		.panel-heading.cursor-pointer.accordion-bg-hover.tr-all {
			margin-top: 15px;
		}
	}

	.searchBar-filters {
		margin: 5px 0px;
	}

	.openFilterParams {
		top: -10px;
		background-color: white;
		position: relative;
		padding: 0px 10px;		
	}

	.panel-heading {
	    background-color: aliceblue;
    	margin: 5px 0px;	
	}

	.btn-filters-select {
		border: none;
		margin: 1px 0px;
	}

	.filters-activate {
		margin: 15px 0px;
	}


</style> -->

<style>
	@media screen and (max-width: 767px) {
		.ans-dir #filterContainerInside {
			min-height: inherit !important;
		}
	}

	.marker-mobilized {
		width: 100%;
		height: 100%;
		border-radius: 50% 50% 50% 0;
		background: #00a8e0;
		transform: rotate(-45deg);
	}
	/* .marker-mobilized::after {
		content: '';
		width: 35px;
		height: 35px;
		margin: 2px 1px 1px 3px;
		background: #fff;
		position: absolute;
		border-radius: 50%;
	} */

	.mobilized-div-marker img {
		position: absolute;
		width: 35px;
		height: 35px;
		top: 0px;
		left: 0px;
		text-align: center;
		border-radius: 50%;
		object-fit: contain;
	}

    .answers-prev-content{
        height: 75vh;
		overflow: auto;
    }
</style>

<div class="tiers-lieux-page navigator-page">
	<div class="padding-bottom-20 padding-left-0 padding-right-0 ans-dir" style="padding: 0% 10%"></div>
</div>
<div id="custom-prev-div" style="display: block;"></div>
<div id="custom-prev-answers" style="display: none;">
	<div class="answers-prev-content"></div>
</div>

<script>   
var filtersTL;
paramsMap = $.extend(true, {}, paramsMapCO, {
    mapCustom: {
        icon: {
            getIcon: function (params) {
                var elt = params.elt;
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof elt.profilMediumImageUrl !== "undefined" && elt.profilMediumImageUrl != "")
                    imgProfilPath = baseUrl + elt.profilMediumImageUrl;
                var myIcon = L.divIcon({
                    className: "mobilized-div-marker",
                    iconAnchor: [17.5, 35],
                    iconSize: [35, 35],
                    labelAnchor: [-35, 0],
                    popupAnchor: [0, -35],
                    html: `<div class='marker-mobilized'></div><img src="${modules.costum.url}/images/franceTierslieux/answersImages/mobilized.png">`
                });
                return myIcon;
            }
        },
    }
});
var mapCOTLMobilized = new CoMap(paramsMap);
$(function(){
	var thirdPlace = {
		init: function () {
			this.actions.init();
		},
		actions : {
			init : function () {

			},
			getThepaticWthOrg : function () {
				var results = {};
				var params = {
					searchType : ["answers"],
					params : {thematicPath: getNavigatorElement.suplment.networkSupl.thematicPath, finderPath: getNavigatorElement.suplment.networkSupl.path},
					fields : ["answers." + getNavigatorElement.suplment.networkSupl.thematicPath, getNavigatorElement.suplment.networkSupl.path],
					filters : {["answers." + getNavigatorElement.suplment.networkSupl.thematicPath] : {'$exists': true}},
					indexMin : 0,
					initType : '',
					count : false,
					locality : "",
  					fediverse : false,
					sortBy : {updated : -1},
					indexStep : 10000,
  					notSourceKey : true,
				}
				ajaxPost(
					null,
					baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk",
					params,
					function (data) {
						if (data.count.answers > 0) {
							results = data.results;
						}
					},
					null,
					"json",
					{ async: false }
				);
				return results;
			}
		},
		getTLMobilized: function(){
			var results = {};
			var params = {
				searchType : ["answers"],
				params : {thematicPath: getNavigatorElement.suplment.mobilize.thematicPath, finderPath: getNavigatorElement.suplment.mobilize.path},
				fields : ["answers." + getNavigatorElement.suplment.mobilize.thematicPath, getNavigatorElement.suplment.mobilize.path],
				filters : {["answers." + getNavigatorElement.suplment.mobilize.thematicPath] : {'$exists': true}},
				indexMin : 0,
				initType : '',
				count : false,
				locality : "",
				fediverse : false,
				sortBy : {updated : -1},
				indexStep : 10000,
				notSourceKey : true,
			}
			ajaxPost(
				null,
				baseUrl + "/costum/francetierslieux/autoglobalthematicntwrk",
				params,
				function (data) {
					if (data.count.answers > 0) {
						results = data.results;
					}
				},
				null,
				"json",
				{ async: false }
			);
			return results;
		}
	}
	thirdPlace.init()
    // const arrayToObject = (arr) => {
    //     return arr.reduce(function(obj, key) {
    //         obj[key] = key;
    //         return obj;
    //     }, {});
    // };
	// ajaxPost(
	// 	null,
	// 	baseUrl+"/"+moduleId+"/element/get/type/forms/id/63e0a8abeac0741b506fb4f7",
	// 	{},
	// 	function (data) {   
	// 		if (data.result) {
	// 			aapObject["formConfig"] = data.map;
	// 			aapObject["formParent"] = data.map;
	// 		} 
	// 	},
	// 	null,
	// 	"json",
	// 	{ async: false }     
	// );
	// aapObject["formInputs"] = <?= json_encode($formInputs) ?>;
	var filterBuilded = {};

	// let filterBuilded = {
	// };
	// if(aapObject.formParent.params) {
	// 	if(aapObject.formParent.params.filterParams) {
	// 		var filterParamsIter = 0;
	// 		for(const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
	// 			if(key.toLocaleLowerCase().includes('text')) {
	// 				filterBuilded[key] = {
	// 					view : 'text',
	// 					field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
	// 					event : "text",
	// 					placeholder : trad.search + ` ${trad.by} `+ value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)]
	// 				}
	// 			}
	// 			if(key.toLocaleLowerCase().includes('select')) {
	// 				let listValue = [];
	// 				if(aapObject.formParent.params[value["paramsKeyFor"+ key.charAt(0).toUpperCase() + key.substring(1)]]) {
	// 					const keyUtil = key.charAt(0).toUpperCase() + key.substring(1);
	// 					if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["list"] || aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["options"]) 
	// 						listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]])[0];
	// 					if(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"]) {
	// 						listValue = Object.values(aapObject.formParent.params[value["paramsKeyFor"+ keyUtil]]["global"])[0];
	// 					}
	// 				}
	// 				const inputKeys = value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)].split(".");
	// 				let inputType;
	// 				if(aapObject.formInputs[inputKeys[1]]) {
	// 					const inputData = Object.values(aapObject.formInputs[inputKeys[1]])[0]["inputs"];
	// 					if(inputData[inputKeys[2]])
	// 						inputType = inputData[inputKeys[2]].type
	// 				}

	// 				filterBuilded[key] = {
	// 					view : 'accordionList',
	// 					type : "filters",
	// 					field : value["inputFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
	// 					event : "filters",
	// 					name : value["labelFor"+ key.charAt(0).toUpperCase() + key.substring(1)],
	// 					keyValue : inputType == "tpls.forms.select" ? false : true,
	// 					list : listValue
	// 				}
	// 				// if(inputType == "tpls.forms.cplx.checkboxNew") filterBuilded[key]["multiple"] = true;
	// 				mylog.log("input type", inputType)
	// 				mylog.log("builded filter", filterBuilded)
	// 			}
	// 			if(filterParamsIter == 0) {
	// 				if(Object.keys(filterBuilded)[0].includes("text")){
	// 					delete filterBuilded.usersFilter
	// 					filterBuilded.usersFilter = {}
	// 				} else {
	// 					filterBuilded = { usersFilter: {}, ...filterBuilded}
	// 				}
	// 			}
	// 			filterParamsIter++
	// 		}
	// 	} else {
	// 		filterBuilded.usersFilter = {}
	// 	}
	// }
	// filterBuilded.sortBy = {
	// 	view : "accordionList",
	// 	type : "sortBy",
	// 	name : trad.sortBy,
	// 	event : "sortBy",
	// 	list : {
	// 		"Date de création croissant" : {
	// 			"created" : 1,
	// 		},
	// 		"Date de création décroissant" : {
	// 			"created" : -1,
	// 		},
	// 		"Date de mise à jour croissant" : {
	// 			"updated" : 1,
	// 		},
	// 		"Date de mise à jour décroissant" : {
	// 			"updated" : -1,
	// 		},					
	// 	}
	// }
	var thematicNetwork = thirdPlace.actions.getThepaticWthOrg();
	filterBuilded['text'] = true;
	filterBuilded['networkFilter'] = {
        name : "filtre par réseau",
        view : "dropdownList",
        type : "filters",
        field : "address.level3",
        event : "filters",
		//typeList : "object",
        keyValue : false,
		event : "filters",
        list : costum.store.whereList.simpleOptions,
    }

	if (typeof thematicNetwork == "object" && Object.keys(thematicNetwork).length > 0) {
		var listThematicNetwork = {}
		$.each(thematicNetwork, function(k, v) {
			listThematicNetwork[k] = {
				label : k,
				value : v.orgaNameArray
			}
		})
		filterBuilded['thematicNetwork'] = {
			name : "filtre par réseaux thematique",
			view : "dropdownList",
			type : "valueArray",
			field : "_id",
			event : "inArray",
			typeList : "object",
			list : listThematicNetwork,
		}
	}
	var mobilizeTLs = thirdPlace.getTLMobilized();
	if(typeof mobilizeTLs == "object" && Object.keys(mobilizeTLs).length > 0){
		var listmobilizeTLs = {}
		$.each(mobilizeTLs, function(k, v) {
			if(k == "Oui"){
				listmobilizeTLs["Tiers Lieux Mobilisés 2024"] = {
					label : "Tiers Lieux Mobilisés 2024",
					value : v.orgaNameArray
				}
			}
		})
		filterBuilded['mobilizeTLs'] = {
			name : "Tiers Lieux Mobilisés 2024",
			view : "dropdownList",
			type : "valueArray",
			field : "_id",
			event : "inArray",
			typeList : "object",
			list : listmobilizeTLs,
		}
		
	}

	showAllNetwork.paramsFilters.filters = filterBuilded;
	showAllNetwork.paramsFilters.header.options["right"] = {
		classes : 'col-xs-4 text-right no-padding',
		group : {
			map : true
		}
	}
    // showAllNetwork.paramsFilters.defaults.textPath = ["name"];
    showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/organizations/form/63e0a8abeac0741b506fb4f7";
	showAllNetwork.paramsFilters.defaults.types = ["organizations"];
    showAllNetwork.paramsFilters.defaults.filters = {'$or': {"source.keys": "franceTierslieux", "source.key": "tierslieuxorg", "reference.costum" : "franceTierslieux" }};
	showAllNetwork.paramsFilters.results.renderView = "directory.modalPanel";
	searchObj.footerDom = ".tiers-lieux-page .footerSearchContainer";
	showAllNetwork.paramsFilters.results.callBack = function(fObj){
		if(typeof fObj.search.obj.valueArray != "undefined" && fObj.search.obj.valueArray.indexOf("Tiers Lieux Mobilisés 2024") > -1){
			$(".btn-show-map").show();
		}else{
			$(".btn-show-map").hide();
		}
	}
	showAllNetwork.paramsFilters.mapCo = mapCOTLMobilized;
    filtersTL = showAllNetwork.showElements();
	// filtersTL.pagination.start = function(fObj){
	// 	if(fObj.results.map.active == false){
	// 		if (typeof coInterface.showCostumLoader != "undefined") {
	// 			coInterface.showCostumLoader(fObj.results.dom, trad.currentlyresearching)
	// 		} else {
	// 			coInterface.showLoader(fObj.results.dom, trad.currentlyresearching);
	// 		}
	// 	}
	// }
	filtersTL.search.manageHistory = function(fObj, setExtraP=true, setSearchP=true){	
		onchangeClick=false;
		hashT=location.hash.split("?");
		setUrl=hashT[0].substring(0);
		extraParamsArray=["preview"];
		extraUrl="";
		if(setExtraP && notEmpty(hashT[1])){
			checkTableGet=hashT[1].split("&");
			$.each(checkTableGet, function(e, v){
				nameLabel=v.split("=")[0];
				if($.inArray(nameLabel,extraParamsArray)>=0)
					extraUrl+=(notEmpty(extraUrl)) ? "&"+v : v;
			});
			setUrl+=(notEmpty(extraUrl)) ? "?"+extraUrl : "";
		}
		if(setSearchP){
			searchParamsUrl=fObj.search.getUrlSearchParams(fObj);
			if(notEmpty(searchParamsUrl)){
				setUrl+= (notEmpty(extraUrl)) ? "&" : "?" ;
				setUrl+= searchParamsUrl;
			}
		}
		if(fObj.results.map.active){
			setUrl += setUrl.indexOf("?") > -1 ? "&" : "?";
			setUrl += "map=true";
		}else{
			if(setUrl.indexOf("map=true") > -1){
				setUrl = setUrl.replace("&map=true", "").replace("?map=true", "");
			}
		}
		if(historyReplace)
			history.replaceState({}, null, setUrl);
		else if(location.hash != setUrl)
			location.hash=setUrl;
		historyReplace=false;
	}
	filtersTL.helpers.toggleMapVisibility = function(fObj){
		coInterface.initHtmlPosition();
		if(fObj.results.map.active){
			fObj.results.map.active=false;
			fObj.search.currentlyLoading = false;
			fObj.search.countResults = false;
			// fObj.search.loadEvent.active = "scroll";
			fObj.search.loadEvent.bind = false;
			fObj.search.loadEvent.default = "pagination"

			$(fObj.mapObj.parentContainer).fadeOut();
			//$(mapCO.hideContainer).show();
			fObj.mapObj.map.invalidateSize();
			if(!fObj.results.map.sameAsDirectory && fObj.results.map.changedfilters){
				fObj.results.map.changedfilters=false;
				fObj.search.init(fObj);
			}else{
				fObj.search.manageHistory(fObj);
			}
		}else{
			if(fObj.results.numberOfResults > 6000){
				fObj.results.showLimitMessage(fObj);
			}else{
				fObj.results.map.active=true;
				$(fObj.mapObj.parentContainer).fadeIn();
				//$(mapCO.hideContainer).hide();

				/*
					keep current zoom state when toogle map visibility
					that's why I commented the following code
				*/
				/* var mapCO_options = mapCO.getOptions();
				fObj.mapObj.map.panTo(mapCO_options.mapOpt.center);
				fObj.mapObj.map.setZoom(mapCO_options.mapOpt.zoom);
				fObj.mapObj.map.invalidateSize(); */
				if(!fObj.results.map.sameAsDirectory && fObj.results.map.changedfilters){
					fObj.results.map.changedfilters=false;
					fObj.search.init(fObj);
				}else{
					fObj.search.manageHistory(fObj);
				}
			}
		}
	}
	// $('.tiers-lieux-page .tiersLieuxContent #filterContainerInside').addClass('tiers-lieux-filter');
	// $('.tiers-lieux-page .tiers-lieux-filter').prepend(`
	// 	<div class="another-filters" style="display: flex; justify-content: center !important;">
	// 		<a href="javascript:;" class="openFilterParams" >
	// 			<i class="fa fa-gear fa-1x"></i> Paramètres du filtre
	// 		</a>
	// 	</div>
	// `)

	// $(".tiers-lieux-page .openFilterParams").off().on("click", function () {
	// 	var inputsText = {};
	// 	var inputsSelect = {};
	// 	let dataInputs = {};
	// 	let dataParamsInputs = {};
	// 	const inputSelectType = ["tpls.forms.cplx.radioNew", "tpls.forms.cplx.checkboxNew", "tpls.forms.select"];
	// 	const paramsKeys = aapObject.formParent.params ? Object.keys(aapObject.formParent.params) : [];
	// 	if (aapObject.formInputs) {
	// 		for (const [index, elem] of Object.entries(aapObject.formInputs)) {
	// 			const key = Object.keys(elem)[0];
	// 			if (elem[key].inputs) {
	// 				for (const [k, v] of Object.entries(elem[key].inputs)) {
	// 					dataInputs[k] = index;
	// 					if (v.type) {
	// 						if (v.type == "text") {
	// 							inputsText[k] = v.label;
	// 						}
	// 						if (inputSelectType.indexOf(v.type) > -1) {
	// 							inputsSelect[k] = v.label;
	// 							const indexFinded = paramsKeys.findIndex(e => e.includes(k));
	// 							const inputParamsKey = indexFinded > -1 ? paramsKeys[indexFinded] : "";
	// 							inputParamsKey != "" ? dataParamsInputs[k] = inputParamsKey : "";
	// 						}
	// 					} else {
	// 						inputsText[k] = v.label;
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	sectionDyfFilter = {
	// 		"jsonSchema": {
	// 			"title": trad.configureFilter,
	// 			"icon": "cog",
	// 			"text": "Sélectionnez la réponse d'un champ text pour filtrer par titre",
	// 			"properties": {
	// 				labelForText: {
	// 					inputType: "text",
	// 					label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
	// 					placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
	// 					rules: {
	// 						required: true
	// 					},
	// 					value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
	// 						aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.labelForText : null
	// 					) : null) : null,
	// 				},
	// 				inputForText: {
	// 					inputType: "select",
	// 					label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
	// 					placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
	// 					noOrder: true,
	// 					options: inputsText,
	// 					rules: {
	// 						required: true
	// 					},
	// 					value: aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (
	// 						aapObject.formParent.params.filterParams.text ? aapObject.formParent.params.filterParams.text.inputForText.split('.')[2] : null
	// 					) : null) : null
	// 				}
	// 			},
	// 			onLoads: {},
	// 			save: function () {
	// 				let dataToSend = {
	// 					id: aapObject.formParent["_id"]["$id"],
	// 					path: "params.filterParams",
	// 					collection: 'forms',
	// 					value: {
	// 					},
	// 					costumEditMode : true,
	// 				}
	// 				$.each(sectionDyfFilter.jsonSchema.properties, function (k, val) {
	// 					if ($("#" + k).val()) {
	// 						const keyForGroup = k.substring(k.indexOf('For') + 3).toLocaleLowerCase();
	// 						if (!dataToSend.value[keyForGroup]) dataToSend.value[keyForGroup] = {};
	// 						switch (k) {
	// 							case "inputFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1): {
	// 								if (dataInputs[$("#" + k).val()])
	// 									dataToSend.value[keyForGroup][k] = 'answers.' + dataInputs[$("#" + k).val()] + '.' + $("#" + k).val();
	// 								if (dataParamsInputs[$("#" + k).val()])
	// 									dataToSend.value[keyForGroup]["paramsKeyFor" + keyForGroup.charAt(0).toUpperCase() + keyForGroup.substring(1)] = dataParamsInputs[$("#" + k).val()]
	// 							} break;

	// 							default: dataToSend.value[keyForGroup][k] = $("#" + k).val();
	// 								break;
	// 						}
	// 					}
	// 				});

	// 				mylog.log("save tplCtx", dataToSend);
	// 				if (typeof dataToSend.value == "undefined") {
	// 					toastr.error('value cannot be empty!');
	// 				} else {
	// 					dataHelper.path2Value(dataToSend, function (params) {
	// 						urlCtrl.loadByHash(location.hash)
	// 					});
	// 				}

	// 			}
	// 		}
	// 	};
	// 	sectionDyfFilter.jsonSchema.properties = aapObject.formParent.params ? (aapObject.formParent.params.filterParams ? (Object.keys(aapObject.formParent.params.filterParams).length > 0 ? {} : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties) : sectionDyfFilter.jsonSchema.properties;
	// 	if (aapObject.formParent.params) {
	// 		if (aapObject.formParent.params.filterParams) {
	// 			for (const [key, value] of Object.entries(aapObject.formParent.params.filterParams)) {
	// 				for (const [k, v] of Object.entries(value)) {
	// 					let label = trad.textFilter;
	// 					!key.includes("text") ? label = trad.selectFilter : "";
	// 					if (k.includes("label")) {
	// 						sectionDyfFilter.jsonSchema.properties[k] = {
	// 							inputType: "text",
	// 							label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + label,
	// 							placeholder: trad.EnterText + " " + trad.for + " " + label,
	// 							value: v
	// 						}
	// 					} else if (k.includes("input")) {
	// 						sectionDyfFilter.jsonSchema.properties[k] = {
	// 							inputType: "select",
	// 							label: trad.SelectField + " " + trad.for + " " + " " + label,
	// 							placeholder: trad.SelectField + " " + trad.for + " " + label,
	// 							options: key.includes("text") ? inputsText : inputsSelect,
	// 							value: v.split(".")[2]
	// 						}
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// 	sectionDyfFilter.jsonSchema.onLoads.onload = function () {
	// 		let objectForArrayGroup = {};
	// 		for (const [key, value] of Object.entries(sectionDyfFilter.jsonSchema.properties)) {
	// 			if (key.includes('label') || key.includes('input')) {
	// 				const keyForGroup = key.substring(key.indexOf('For') + 3);
	// 				if (!objectForArrayGroup[keyForGroup]) {
	// 					objectForArrayGroup[keyForGroup] = {
	// 						groupType: keyForGroup.includes("ext") ? trad.text.charAt(0).toUpperCase() + trad.text.slice(1) : trad.selection.charAt(0).toUpperCase() + trad.selection.slice(1),
	// 						inputToGroup: []
	// 					};
	// 				}
	// 				objectForArrayGroup[keyForGroup].inputToGroup.push(key + value.inputType);
	// 			}
	// 		}
	// 		for (const [key, value] of Object.entries(objectForArrayGroup)) {
	// 			alignInput2(value.inputToGroup, key, 6, 12, null, null, value.groupType, "#3f4e58", "");
	// 			$(".fieldset" + key).append(`<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-propertiesToRemove="labelFor${key},inputFor${key}" data-inputSelector=".fieldset${key}"><i class="fa fa-times"></i></button>`);
	// 		}

	// 		$(`
	// 			<div class="form-group mb-0 d-flex justify-content-center flex-xs-column filter-actions-group">
	// 				<button type="button" class="btn btn-xs btn-primary addTextFilter">${trad.Add + " " + trad.textFilter}</button>
	// 				<button type="button" class="btn btn-xs btn-info addSelectFilter">${trad.Add + " " + trad.selectFilter}</button>
	// 			</div>
	// 		`).insertBefore(".form-actions");
	// 		let inputsTextOptions = "";
	// 		for (const [key, value] of Object.entries(inputsText)) {
	// 			inputsTextOptions += `<option value="${key}" data-value="">${value}</option>`;
	// 		}
	// 		let inputsSelectOptions = "";
	// 		for (const [key, value] of Object.entries(inputsSelect)) {
	// 			inputsSelectOptions += `<option value="${key}" data-value="">${value}</option>`;
	// 		}
	// 		$(".addTextFilter").off().on("click", function () {
	// 			const fieldSet = "Text" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
	// 			let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
	// 			let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
	// 			$(`
	// 				<div class="col-xs-12 fieldset fieldset${fieldSet}">
	// 					<div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
	// 						<label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
	// 							<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter}
	// 						</label>
	// 						<input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.textFilter}">
	// 					</div>
						
	// 					<div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
	// 						<label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
	// 							<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.textFilter}
	// 						</label>
	// 						<select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.textFilter}">
	// 							<option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.textFilter}</option>
	// 							${inputsTextOptions}
	// 						</select>
	// 					</div>
	// 					<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
	// 						<i class="fa fa-times"></i>
	// 					</button>
	// 				</div>
	// 			`).insertAfter(lastField).hide().fadeIn(300);
	// 			styleWrapped = `
	// 				.modal-content .fieldset${fieldSet}:before {
	// 					content: "${trad.text.charAt(0).toUpperCase() + trad.text.slice(1)}";
	// 					color: #3f4e58;
	// 					position: absolute;
	// 					top: -15px;left: 14px;
	// 					background: white;
	// 					font-size: medium;
	// 					font-weight: bold;
	// 					padding: 0 15px;
	// 				}
	// 			`;
	// 			$("head style").last().append(styleWrapped);
	// 			sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
	// 				inputType: "text",
	// 				label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.textFilter,
	// 				placeholder: trad.EnterText + " " + trad.for + " " + trad.textFilter,
	// 				value: ""
	// 			};
	// 			sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
	// 				inputType: "select",
	// 				label: trad.SelectField + " " + trad.for + " " + trad.textFilter,
	// 				placeholder: trad.SelectField + " " + trad.for + " " + trad.textFilter,
	// 				options: inputsText,
	// 				value: ""
	// 			}
	// 		});
	// 		$(".addSelectFilter").off().on("click", function () {
	// 			const fieldSet = "Select" + Object.keys(sectionDyfFilter.jsonSchema.properties).length + Math.random().toString(36).substring(4);
	// 			let fieldSets = $(this).parent().parent().find(".fieldset").toArray();
	// 			let lastField = fieldSets.length > 0 ? fieldSets[fieldSets.length - 1] : "#ajaxFormModal .errorHandler";
	// 			$(`
	// 				<div class="col-xs-12 fieldset fieldset${fieldSet}">
	// 					<div class="labelFor${fieldSet}text col-xs-12 col-md-6 col-md-offset-null">
	// 						<label class="col-xs-12 text-left control-label no-padding" for="labelFor${fieldSet}">
	// 							<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter}
	// 						</label>
	// 						<input type="text" class="form-control " name="labelFor${fieldSet}" id="labelFor${fieldSet}" placeholder="${trad.EnterText + " " + trad.for + " " + trad.selectFilter}">
	// 					</div>
						
	// 					<div class="inputFor${fieldSet}select col-xs-12 col-md-6 col-md-offset-null">
	// 						<label class="col-xs-12 text-left control-label no-padding" for="inputFor${fieldSet}">
	// 							<i class="fa fa-chevron-down" style="display: none;"></i> ${trad.SelectField + " " + trad.for + " " + trad.selectFilter}
	// 						</label>
	// 						<select class="form-control   " placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}" name="inputFor${fieldSet}" id="inputFor${fieldSet}" style="width: 100%;height:auto;" data-placeholder="${trad.SelectField + " " + trad.for + " " + trad.selectFilter}">
	// 							<option class="text-red" style="font-weight:bold" disabled="" selected="">${trad.SelectField + " " + trad.for + " " + trad.selectFilter}</option>
	// 							${inputsSelectOptions}
	// 						</select>
	// 					</div>
	// 					<button type="button" class="btn btn-danger removeFilterOption btn-xs align-top-right" data-inputselector=".fieldset${fieldSet}">
	// 						<i class="fa fa-times"></i>
	// 					</button>
	// 				</div>
	// 			`).insertAfter(lastField).hide().fadeIn(300);
	// 			styleWrapped = `
	// 				.modal-content .fieldset${fieldSet}:before {
	// 					content: "${trad.selection}";
	// 					color: #3f4e58;
	// 					position: absolute;
	// 					top: -15px;left: 14px;
	// 					background: white;
	// 					font-size: medium;
	// 					font-weight: bold;
	// 					padding: 0 15px;
	// 				}
	// 			`;
	// 			$("head style").last().append(styleWrapped);
	// 			sectionDyfFilter.jsonSchema.properties["labelFor" + fieldSet] = {
	// 				inputType: "text",
	// 				label: trad.text.charAt(0).toUpperCase() + trad.text.slice(1) + " " + trad.for + " " + trad.selectFilter,
	// 				placeholder: trad.EnterText + " " + trad.for + " " + trad.selectFilter,
	// 				value: ""
	// 			};
	// 			sectionDyfFilter.jsonSchema.properties["inputFor" + fieldSet] = {
	// 				inputType: "select",
	// 				label: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
	// 				placeholder: trad.SelectField + " " + trad.for + " " + trad.selectFilter,
	// 				options: inputsSelect,
	// 				value: ""
	// 			}
	// 		});
	// 		$(document).on("click", ".removeFilterOption", function (e) {
	// 			e.stopImmediatePropagation();
	// 			const propertiesToRemove = $(this).attr("data-propertiesToRemove") ? $(this).attr("data-propertiesToRemove").split(",") : [];
	// 			propertiesToRemove.forEach(e => {
	// 				if (sectionDyfFilter.jsonSchema.properties[e]) delete sectionDyfFilter.jsonSchema.properties[e]
	// 			})
	// 			$($(this).attr("data-inputSelector")).fadeOut(300, function () {
	// 				$(this).remove()
	// 			});
	// 		});
	// 	}
	// 	dyFObj.openForm(sectionDyfFilter);
    // });
}) 				
// $(".page-link.next, .current.next").html('<i class="fa fa-chevron-right"></i>');
// 	$(".page-link.prev, .current.prev").html('<i class="fa fa-chevron-left"></i>');
</script>