<?php

if(isset(Yii::app()->session["userId"])){
    $cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/css/franceTierslieux/navigatorThirdPlace/myThirdPlace.css",
        "/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/dataviz/getElement.js",
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( "costum" )->getAssetsUrl());
    $getAdminOrga = PHDB::find(Organization::COLLECTION, [
        "links.members.". Yii::app()->session["userId"] .".isAdmin" => ['$eq' => true],
        "links.members.". Yii::app()->session["userId"] .".isAdminPending" => ['$ne' => true],
        "links.members.". Yii::app()->session["userId"] .".toBeValidated" => ['$ne' => true],
        '$or' => array(
            array("reference.costum"=>"franceTierslieux"),
            array("source.keys"=>"franceTierslieux")
        )
    ],["_id" => 0, "slug" => 1]);
    $getAdminOrga = array_map(function($item) {
		return $item["slug"];
	}, array_values((array) $getAdminOrga));
?>

<div class="my-tr-content navigator-page">
    <div style="padding: 0% 10%">
        <p class="page-title"> Mes Tiers-Lieux </p>
        <!-- <hr style="border-top: 1px solid #858585;"> -->
    </div>

    <div class="my-tiers-lieux-page">
        <div class="my-orga-btn-content margin-bottom-20" >
            <div class="ui-group">
                <div class="button-group js-radio-button-group" data-filter-group="maincat">
                <h4 style="margin-right: 15px;">Voir : </h4>
                <button class="button is-checked my-tr"><p>Mes Tiers-Lieux</p></button>
                    <button class="button my-news"><p>News</p></button>
                    <button class="button my-agenda "><p>Agenda</p></button>
                    <button class="button my-project "><p>Projet</p></button>
                    <button class="button my-similar-tr "><p>Tiers-lieux similaires</p></button>
                </div>
            </div>
        </div>
        <p class="description-list-orga"></p>

        <div class="agenda-content" > 
            <!-- 64aae87a7daffb95e80587a7 -->
            <?php
                // echo $this->renderPartial('costum.views.tpls.blockCms.events.generic_agenda', ['readonly_content' => true, 'show_filter' => false,'content_type' => 'events', 'default_filter_keys' => ['64aae87a7daffb95e80587a7']]);
            ?>
        </div>

        <div class="padding-bottom-20 padding-left-0 padding-right-0 ans-dir" style="padding: 0% 10%"></div>

        <div>
            <?php 
            
            // echo $this->renderPartial('news.views.co.index', 
            // array(  "news"=>array("64ab911e50bb3f1b4f080e2d" => PHDB::findOneById("news", "64ab911e50bb3f1b4f080e2d") ), 
            //         "nbCol"=> 1,
            //         "endStream"=>true,
            //         "inline"=>true) 
            // );  
            
            ?>       
        </div>
    </div>
</div>
<div id="custom-prev-div" style="display: none;"></div>
<p class="text-red not-connected text-center margin-top-20" style="font-size: 23px;"> Vous n'êtes actuellement pas connecté,<br> veuillez vous connecter pour accéder à cette page</p>


<script>   
var nbCol = 1;
$(function(){
    var myOrga = {
        init : function () {
            myOrga.events.init();
            myOrga.actions.init();
        },
        myOrgaFilter: {
            '$or': {
                "slug": {'$in': <?= json_encode($getAdminOrga) ?>} ,
                ["vote." + userId] : { "$exists": true }
            }
        },
        events : {
            init : function () {
                // if (AllTiersLiked != undefined && Object.keys(AllTiersLiked).length > 0) 
                searchObj.footerDom = ".footerSearchContainer";    
                myOrga.events.generalEvents();
                // else 
                //     $(".my-tiers-lieux-page").html("<div class='empty-text'><h2>C'est vide car vous n'avez pas aimé un ou plusieurs tiers-lieux.</h2></div>");
            },
            generalEvents : function () {
                if (isUserConnected == "logged") {
                    let elementData = {};
                    let resultat = {};
                    let sectionDescription = {
                        descriptionMyTr : 'Listes des tiers-lieux que vous avez aimé',
                        descriptionMyNews : 'Les nouvelles de vos tiers-lieux favoris',
                        descriptionMyAgenda : 'Les agendas de vos tiers-lieux favoris',
                        descriptionMyProject : 'Listes des projets de vos tiers-lieux favoris',
                        descriptionSimilaryTr : 'Les tiers-lieux similaires à vos tiers-lieux favoris'
                    }
                    showAllNetwork.paramsFilters.defaults.filters = myOrga.myOrgaFilter;
                    showAllNetwork.paramsFilters.defaults.types = ['organizations'];
                    showAllNetwork.paramsFilters.urlData = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/" + getNavigatorElement.surveyData.formId;
                    showAllNetwork.paramsFilters.results.renderView = "directory.modalPanel";
                    showAllNetwork.paramsFilters.defaults.fields = [];
                    showAllNetwork.showElements(
                        "",
                        "",
                        true,
                        true,
                        function(data) {
                            if (Array.isArray(data) && data.length == 0)
                                $(".my-tiers-lieux-page").html("<div class='empty-text'><h3 class='text-center'>C'est vide car vous n'avez pas aimé un ou plusieurs tiers-lieux. <br> <i style='font-size: 85px;' class='fa fa-info-circle margin-top-20' ></i> </h3></div>");
                        }
                    );
                    showAllNetwork.showElements("","",true);
                    $('.not-connected').remove();
                    $(".ans-dir").addClass("default-dir")
                    $(".description-list-orga").text(sectionDescription.descriptionMyTr)

                    $('.my-tr').click(function() {
                        $(".description-list-orga").text(sectionDescription.descriptionMyTr);
                        $(".agenda-content").hide();
                        if ($(".default-dir").length == 0 && $(".default-dir").length < 2) {
                            $(".ans-dir").addClass("default-dir")
                        }
                        myOrga.sections.listLikes();
                    })

                    $(".my-agenda").click(function() { 
                        $(".description-list-orga").text(sectionDescription.descriptionMyAgenda);
                        $(".ans-dir").removeClass("default-dir")
                        $(".agenda-content").show();
                        $(".agenda-content .fc-today-button").click();
                        $(".ans-dir").html("");
                        myOrga.sections.agenda();
                    })

                    $('.my-news').click(function() {
                        $(".description-list-orga").text(sectionDescription.descriptionMyNews);
                        coInterface.showLoader(".ans-dir");
                        $(".ans-dir").removeClass("default-dir")
                        $(".agenda-content").hide();
                        myOrga.sections.news();
                    })

                    $('.my-project').click(function() {
                        $(".description-list-orga").text(sectionDescription.descriptionMyProject)
                        $(".ans-dir").html("");
                        $(".ans-dir").removeClass("default-dir")
                        $(".agenda-content").hide();
                        myOrga.sections.projects();
                    })

                    $('.my-similar-tr').click(function() {
                        $(".description-list-orga").text(sectionDescription.descriptionSimilaryTr)
                        $(".ans-dir").html("");
                        $(".ans-dir").removeClass("default-dir")
                        $(".agenda-content").hide();
                        myOrga.sections.mySimilarTr();
                    })
                }  else {
                    $(".ui-group").hide();
                } 
                $(".agenda-content").hide();             
            }
        },
        actions : {
            init: function () {
                
            },
            getLikedNews: function (AllTiersLiked) {
                var htmlResults = "";
                ajaxPost(
                    null,
                    baseUrl + '/costum/francetierslieux/getlikednewsaction/type/organizations',
                    {elements: AllTiersLiked},
                    function (data) {   
                        htmlResults = (notEmpty(data)) ? `
                            <div class="col-xs-12 no-padding container-live inline nb-col-1">
                                <ul class="timeline inline-block" id="news-list">${data}</ul>
                            </div>
                        ` : "";
                    },
                    null,
                    null,
                    { async: false }     
                )  
                return htmlResults;
            }
        },
        sections : {
            init : function () {

            },
            listLikes : function () {
                showAllNetwork.paramsFilters.defaults.filters = myOrga.myOrgaFilter;
                showAllNetwork.paramsFilters.defaults.types = ['organizations'];
                showAllNetwork.paramsFilters.urlData = baseUrl+"/costum/francetierslieux/answerdirectory/source/"+costum.slug+"/form/" + getNavigatorElement.surveyData.formId;
                showAllNetwork.paramsFilters.results.renderView = "directory.modalPanel";
                showAllNetwork.paramsFilters.defaults.fields = [];
                if (isUserConnected == "logged") {
                    $('.not-connected').remove();
                    showAllNetwork.showElements("","",true)
                }
            },
            agenda : function () {
                 //elementPanelHtml
                showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory";
                showAllNetwork.paramsFilters.defaults.types = ['events'];
                showAllNetwork.paramsFilters.defaults.params = Object.keys(AllTiersLiked);
                showAllNetwork.paramsFilters.defaults.filters = {}; //elementPanelHtml
                showAllNetwork.paramsFilters.defaults.filters["$or"] = {};
                showAllNetwork.paramsFilters.results.renderView = "directory.eventPanelHtmlFullWidth";
                showAllNetwork.paramsFilters.defaults.fields = ["collection", "name", "_id", "links.attendees", "links.organizer"];
                if (Object.keys(AllTiersLiked).length) {
                    $.each(AllTiersLiked, function (e, v) {
                        let orgaId = v?._id?.$id;
                        if (orgaId != undefined) {
                            showAllNetwork.paramsFilters.defaults.filters["$or"]["links.attendees." + orgaId] = {"$exists": true}
                            showAllNetwork.paramsFilters.defaults.filters["$or"]["links.organizer." + orgaId] = {"$exists": true}
                        }
                    })
                }
                

                showAllNetwork.showElements("","",true)            
                var timeout = 1000;
                setTimeout(() => {
                    $('.searchEntityContainer.events').parent().attr('style', 'position: relative;');
                }, timeout);
            },
            news : function () {
                var orgasArrays = Object.keys(AllTiersLiked);
                var viewNews = myOrga.actions.getLikedNews(orgasArrays);
                $(".ans-dir").html(viewNews);
                if (!notEmpty(viewNews)) {
                    let urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/isLive/false/formCreate/false/nbCol/1";
                    ajaxPost(".ans-dir",baseUrl+"/"+urlNews,{}, function(news){ }, function(error){}, "html");
                }
            }, 
            projects: function () {
                showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory";
                showAllNetwork.paramsFilters.defaults.types = ['projects'];
                showAllNetwork.paramsFilters.defaults.filters = {}; //elementPanelHtml
                showAllNetwork.paramsFilters.defaults.filters["$or"] = {};
                showAllNetwork.paramsFilters.results.renderView = "directory.rotatingCard";
                showAllNetwork.paramsFilters.defaults.params = Object.keys(AllTiersLiked);
                showAllNetwork.paramsFilters.defaults.fields = ["collection", "name", "_id", "links.contributors"];
                // showAllNetwork.showElements("","",true)
                if (Object.keys(AllTiersLiked).length) {
                    $.each(AllTiersLiked, function (e, v) {
                        let orgaId = v?._id?.$id;
                        if (orgaId != undefined) {
                            showAllNetwork.paramsFilters.defaults.filters["$or"]["links.contributors." + orgaId] = {"$exists": true}
                        }
                    })
                }
                showAllNetwork.showElements("","",true)
            },
            mySimilarTr: function () {
                showAllNetwork.paramsFilters.defaults.filters = myOrga.myOrgaFilter;
                showAllNetwork.paramsFilters.defaults.types = ['organizations']
                showAllNetwork.paramsFilters.urlData = baseUrl+"/costum/francetierslieux/answerdirectory/source/organizations/form/63e0a8abeac0741b506fb4f7";
                showAllNetwork.paramsFilters.results.renderView = "directory.modalPanel";
                showAllNetwork.paramsFilters.defaults.fields = [];
                if (isUserConnected == "logged") {
                    $('.not-connected').remove();
                    let showElements = showAllNetwork.showElements("","",true);
                    // mylog.log("listing", showAllNetwork.showElements("","",true)); 
                    showElements.results.render = function (fObj, results, data) {
                        let aggregateParams = {
                            allPaths : {}
                        };
                        let agregatesPaths = showAllNetwork.getElement("mappings", "64f987cf01e4500d4019a9f0");
                        $.each(results, function (key, values) {
                            // mylog.log("aggregate params", values);
                            let answers = values?.answers?.answers;
                            if (agregatesPaths?.fields != undefined && answers != undefined) {
                                $.each(agregatesPaths.fields, function(keyPath, valuePath) {
                                    let answerPath = valuePath.replace("answers.", "");
                                    let answerValue = jsonHelper.getValueByPath(answers, answerPath);
                                    // fusion all answerValue wih same keyPath
                                    aggregateParams.limit = 30;
                                    if (aggregateParams.allPaths[keyPath] == undefined) 
                                        aggregateParams.allPaths[keyPath] = [];
                                    
                                    if (answerValue != undefined) 
                                        aggregateParams.allPaths[valuePath] = [... new Set([...aggregateParams.allPaths[keyPath], ...answerValue])];
                                })
                            }
                        });
                        $(".ans-dir").html("");
                        let trList = {};
                        ajaxPost(
                            null,
                            baseUrl + '/costum/francetierslieux/getaggregatetr',
                            aggregateParams,
                            function (data) {   
                                trList = data;
                            },
                            null,
                            null,
                            { async: false }     
                        )   
                        // create params
                        var orgaNames = [];
                        if (jsonHelper.getValueByPath(trList, "profileResults") != undefined) {
                            $.each(trList.profileResults, function(key, value) {
                                if (jsonHelper.getValueByPath(value, "links.organizations") != undefined && Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name != undefined) {
                                    orgaNames.push(Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name); 
                                }
                            })
                            showAllNetwork.paramsFilters.defaults.filters = {name: {'$in': orgaNames}};
                            showAllNetwork.showElements("","",true)
                            $(".ans-dir").addClass("default-dir");
                        }
                        
                        // let cardList = "<div class='ans-dir'> <div class='cardContainer'>";
                        // $.each(trList.profileResults, function (key, value) {
                        //     if (key != 0)
                        //         cardList += showAllNetwork.listOrgaInCard(value);
                        // })
                        // cardList += "</div></div>";
                        // $(".ans-dir").html(cardList);
                    }
                }
            }
        }
    }
    myOrga.init();
    
    $('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});


    // $('.my-tr').trigger('click');
}) 				
$(".myTiersLieuxContent").find(".like-project-container").remove();

</script>

<?php
    }else{   
?>
    <script>
        Login.openLogin();
    </script>
<?php
    }
?>