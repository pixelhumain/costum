<?php function indent($key, $data){
    $ckey = (is_int($key))?"":$key." : ";
    if(is_string($data)){
        echo "<div style='margin-left: 30px;'>$ckey $data</div>";
    }else if(is_int($data)){
        echo "<div style='margin-left: 30px;'><b>$ckey</b> $data</div>";
    }else if(is_array($data)){
        echo "<div style='margin-left: 30px; border-left: 2px dashed #ddd; border-radius: 10px; padding-left:5px'>";
        echo "<b>$key : </b>"; 
        foreach ($data as $k => $v) {
            indent($k, $v);
        }
        echo "</div>";
    }
} ?>
<div class="container" style="font-size:14pt">
    <h2 class="text-center">Données stats Récensement</h2>
    <div class="text-center">
        <?php 
            if(isset($byRegions)){
                foreach ($byRegions as $key => $value) {
                    echo '<a href="#'.str_ireplace([" ", "'"], "", $key).'" 
                            class="btn btn-default btn-region margin-5 padding-5">'.$key.'</a>';
                }
            }
        ?>
    </div>
    <div class="row" style="display:grid;justify-items: center;">
        <div class="col-md-7 margin-10" style="background-color:#eee">
            <h3>Par Etapes au <?php echo date("d/m"); ?> : </h3>
            <div style="color: #555; background:#eee">
                <?php       
                    if(isset($bySteps)){
                        echo "<div style='margin-left: 30px;'>";
                        foreach ($bySteps as $key => $value) {
                            echo "<div>$key : $value </div>";
                        }
                        echo "</div>";
                    }
                ?>
            </div>
        </div>
        <div class="col-md-7 margin-10" style="background-color:#eee">
            <h3> Par régions au <?php echo date("d/m"); ?> : </h3>
            <?php       
                if(isset($byRegions)){
                    foreach ($byRegions as $key => $value) {
                        echo '<div id="'.str_ireplace([" ", "'"], "", $key).'" class="section-region margin-5 padding-5" style="color: #555; font-size-14pt">';
                        indent($key, $value);
                        echo '</div>';
                    }
                }
            ?>
        </div>
    </div>
    <a href="#" class="btn btn-primary padding-10" style="position:fixed; bottom: 10%; right: 10%; font-size:16pt; border-radius: 40px"><i class="fa fa-arrow-up"></i></a>
</div>

<script>
    $(document).ready(function(){
        $("a.btn-region").on("click", function(){
            $(".section-region").removeClass("bg-info");
            $("div"+$(this).attr("href")).addClass("bg-info");
        });
    })
</script>