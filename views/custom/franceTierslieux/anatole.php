<style>
    .bg-contain {
        background-color: #4ecdc4;
        background-image: url(https://cvip.sphinxonline.net/surveyserver/Content/img/theme/background_8.png);
        background-position: center;
        background-repeat: repeat-x;
        background-attachment: fixed;
        min-height: 100vh;
    }
    .contain-offset {
        border: 1px solid #CCCCCC;
        padding: 0;
        min-height: 100px;
        margin-top: 10px;
        padding-bottom: 5px;
        background-color: #fff;
        -webkit-border-radius: 0;
        -moz-border-radius: 0;
        border-radius: 0;
        -webkit-box-shadow: 5px 5px 10px #000;
        -moz-box-shadow: 5px 5px 10px #000;
        box-shadow: 5px 5px 10px #000;
    }
    .banner-section {
        padding: 20px;
    }
    .body-section, .footer-section {
        padding: 30px 15px;
    }
    .text-body {
        font-size: 20px;
    }
    .header-title {
        text-transform: none;
        font-size: 48px;
    }
    .brands {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    }

    .brands__item {
        flex: 0 0 50%;
        list-style: none;
        text-align: center;
    }
    .brands__item img {
        width: 130px;
        height: 75px;
        object-fit: contain;
        mix-blend-mode: multiply;
    }

    @media (min-width: 700px) {
        .brands__item {
            flex: 0 0 33.33%;
        }
    }

    @media (min-width: 1100px) {
        .brands__item {
            flex: 0 0 25%;
        }
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .header-title {
            font-size: 26px;
        }
    }
    @media (max-width: 767px) {
        .header-title {
            font-size: 22px;
        }
        .text-body {
            font-size: 14px;
        }
        .body-section, .footer-section {
            padding: 10px 5px;
        }
    }

</style>

<div class="col-xs-12 bg-contain">
    <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 contain-offset no-padding">
        <div class="banner-section">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <h1 class="header-title">La coopérative tiers-lieux</h1>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                    <img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/baner.png" alt="Book Icon">
                </div>
            </div>
        </div>
        <hr>

        <div class="body-section">
            <div class="text-body">
                Vous êtes salarié, bénévole, prestataire sur une fonction de gestion de tiers-lieu, ce rapide sondage est fait pour vous !<br><br>


                🧾  Cette enquête vise à établir un état des lieux de l’emploi et des contributions en tiers-lieu en 2022 pour donner à voir l’évolution du secteur, adapter les modalités d’accompagnement des structures employeuses et les besoins en formation des salariés / bénévoles<br><br>


                📨  La Coopérative Tiers-Lieux, La Compagnie des Tiers-Lieux, Cap Tiers-Lieux Pays de la Loire, le consortium Île de France Tiers-Lieux, Illusion et Macadam, Bretagne Tiers-Lieux et le Réseau Sud Tiers-Lieux, auront accès aux réponses de ce questionnaire. Les données que vous nous transmettez seront traitées de manière anonyme.<br><br>


                ⌛️ Nous vous remercions de prendre le temps de répondre  à ce sondage. Attention, prévoyez bien 15 minutes pour le remplir et ne laisser pas la page inactive plus de 5 minutes sans quoi vos réponses s'auto-détruiront instantanément.<br><br>

            </div>
        </div>
        <hr>

        <div class="footer-section">
            <ul class="brands">
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/logo-02.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/bannerFTL.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/Modèle de gestion.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/baner.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/cartographie.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/Services.png" alt="" />
                </li>
                <li class="brands__item">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/Réseau associé.png" alt="" />
                </li>
            </ul>
        </div>
        <hr>

    </div>
</div>