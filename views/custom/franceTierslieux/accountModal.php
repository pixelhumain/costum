<style type="text/css">

.answerLi{
		box-shadow: 1px 2px 3px -1px rgba(0,0,0,0.5);
	}
	.answerLi:hover{
		background-color: #254c960a;
    	box-shadow: 1px 2px 4px 0px rgba(0,0,0,0.5);
	}
	.answerLi .btn-open-answer{
		color: white;
	    font-size: 14px;
	    font-weight: 800;
	    padding: 3px 10px;
	    background-color: #254c97;
	}
	.answerLi .titleAnsw{
		color: #2c4b89;
	    font-size: 22px;
	    text-transform: inherit;
	}
	.answerLi b{
	    color: #183875;

	}
	.answerLi .infoAnsw{
		font-size: 14px;
	}
	.answerLi .container-infos{
		padding:0px;
    	padding-bottom: 5px !important;
    }


    .tl-list{
    	padding:5px;
    	display: flex;
    }

	.notifications-dash .notifLi{
		background-color: rgba(208, 249, 208, 1);
	}
	.notifications-dash .notifLi.read{
		background-color: white !important;
	}
	.notifications-dash ul.notifListElement {
	    padding: 0px !important;
	    max-height: 350px;
	    overflow-y: scroll;
	    border-radius: 5px;
	}
	.notifications-dash a.notif{
		font-size : 11px;
		font-weight: bold;
	}
	.notifications-dash a.notif .content-icon{
		position: absolute;
	}
	.notifications-dash a.notif .content-icon span.label{
		background-color: #56c557b5 !important;
	}
	.notifications-dash a.notif .notif-text-content{
		width: 91.66666667% !important; 
		float: right;
	}
	.notifications-dash .pageslide-title{
		width: 100%;
    	padding-bottom: 5px;
    	text-transform: uppercase !important;
    	font-size: 18px !important;
	}
	.notifications-dash .pageslide-title i.fa-angle-down{
		display:none;
	}
	.notifications-dash .btn-notification-action{
		display: none;
	}

	@media (max-width: 792px){
		.menu-dashboard{
			display:none;
			width: 90%;
		}
	}
	.dashboard-modal-content h1, .dashboard-modal-content h2, .dashboard-modal-content h3{
		color: white;
	}
	.dashboard-modal-content .dash-answers{
		display: none;
	}
	.dashboard-modal-content .answerLi{    
	    color: white;
    	text-transform: initial;
    	box-shadow: 1px 2px 3px -1px #f4f4f44f;
    	font-weight: initial;
	}
	.dashboard-modal-content .answerLi .titleAnsw {
	    color: #67b04c;
	    font-size: 20px;
	}
	.dashboard-modal-content .answerLi b {
    	color: #a2ea87;
	}
	.dashboard-modal-content .answerLi:hover {
    	background-color: #3d4f3c;
	}
</style>
 <div class="col-md-3 col-sm-4 margin-bottom-20 menu-dashboard">
	<?php $imgPath=(isset(Yii::app()->session["user"]["profilMediumImageUrl"]) && !empty(Yii::app()->session["user"]["profilMediumImageUrl"])) ? Yii::app()->createUrl(Yii::app()->session["user"]["profilMediumImageUrl"]): $this->module->getParentAssetsUrl()."/images/thumbnail-default.jpg"; ?>  
	<img class="img-responsive profil-img" src="<?php echo $imgPath ?>">
	<?php if (Authorisation::isInterfaceAdmin()){ ?>
		<a href="#admin" class="lbh btn-dash-link col-xs-12">Administration</a>
	<?php	} ?>
	<!-- <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh btn-dash-link col-xs-12">Mon profil</a>-->
	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>.view.settings" class="lbh btn-dash-link col-xs-12">Mes paramètres</a> 
	<!-- <a href="javascript:;" onclick="rcObj.loadChat("","citoyens", true, true)" class="btn-dash-link col-xs-12"> Ma messagerie</a> -->
	<!-- <div class="col-xs-12 no-padding notifications-dash"></div> -->
	<a href="javascript:;" class="btn-dash-link logoutBtn col-xs-12 bg-red" style="border-bottom: inherit;
    border-radius: 5px;"><i class="fa fa-sign-out"></i> Déconnexion</a>
</div>
<?php 
$tplSlug=isset($this->costum["slug"]) ? $this->costum["slug"] : $this->costum["assetsSlug"];
//var_dump("<pre>",$this->costum,"</pre>");
?>
<div class="col-md-9 col-sm-8 col-xs-12 dashboard-modal-content">
	<div class="col-xs-12 no-padding"><h2><?php echo Yii::app()->session["user"]["name"] ?></h2></div>
	<?php 
	$holdingTL=PHDB::find(Organization::COLLECTION,array("tags"=>"TiersLieux","links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
	//var_dump($holdingTL);
	?>
	<!-- <div class="col-xs-12">
	   <h3>Les tiers-lieux que je gère</h3>
	   <div class="col-xs-12" style="max-height: 200px;  overflow-y: auto;">
	   	<table>
	   		<thead><tr><th>Tiers-lieu</th><th>Réponses associées</th></tr></thead>

	   	<tbody> -->
<?php 
	 foreach($holdingTL as $id => $tl){
        $ansTL=PHDB::find(Form::ANSWER_COLLECTION,array("links.organizations.".(string)$tl["_id"] =>array('$exists'=>1)));
		// $typeElt=$tl["type"];
		$img= (isset($tl["profilThumbImageUrl"])) ? $tl["profilThumbImageUrl"] : "";
	?>	
       <!--  <tr>
        <td>	
		<div class="tl-list lbh-preview-element" href="#page.type.organizations.id.<?= (string)$tl["_id"] ?>">
		    <img src="<?= $img ?>" class="thumb-send-to pull-left img-circle" height="40" width="40">
			<span class="info-contact pull-left margin-left-20">
			    <span class="name-element text-dark text-bold" data-id="<?= (string)$tl["_id"] ?>">
			       <?= $tl["name"] ?>	
			    </span>
			<br/>
			    <span class="type-element text-light pull-left">
					<?php 
					// echo Yii::t("common",$typeElt) 
					?></span>
			</span>

		</div>
		</td>
		<td> -->
			<?php 	
			 foreach($ansTL as $ansId =>$ans){
			 	$form=PHDB::findOne(Form::COLLECTION,array("_id"=> New MongoId($ans["form"])),array("name"));
            ?>
                <!-- <a class='col-xs-12' href="#answer.index.id.<?= $ansId ?>"><?= $form["name"] ?></a> -->

			<?php
			 }
			?>

			
		<!-- </td>
	</tr> -->
	<?php 	
	 }
	?>
	    <!-- </tbody>
	    </table>
	    </div>
    </div> -->

    <?php
	$formRec=Form::getBySourceAndId($tplSlug);
	// var_dump($formRec);exit;
	$answs=Answer::getListBy(null, $this->costum["slug"],Yii::app()->session["userId"]);
	?>	
	<div class="col-xs-12 no-padding margin-bottom-20" style="border-bottom: 1px solid white;"><h3>Mes réponses (<?php echo count($answs) ?>)</h3></div>
	<?php  
		if(empty($answs)){
			echo "<span class='col-xs-12 text-white no-padding'>Vous n'avez pas répondu au recensement 2023.</span>".
			"<a href='#answer.index.id.new.".(string)$formRec["form"]["_id"].".mode.w.standalone.true' class='btn btn-success lbh margin-top-10'><i class='fa fa-plus'></i> Je réponds au recensement des tiers-lieux</a>";
		}else{
		?>

			<div class="col-xs-12">
		<?php		
			foreach($answs as $ansId => $ans){
				$form=PHDB::findOne(Form::COLLECTION,array("_id"=>New MongoId($ans["form"])));
				if(isset($form)){
			?>	
				<div class="answerLi col-xs-12 no-padding margin-bottom-10" <?php echo "" ?> data-id="<?php echo $ansId ?>">
				<!-- <div class="col-xs-4 padding-top-10">
					<img src="<?php echo "" ?>" class="img-responsive margin-auto">
				</div> -->
				<div class="col-xs-12 container-infos">
					<div class="col-xs-12 no-padding">
						<h3 class="margin-top-5 titleAnsw"> <?php echo $form["name"] ?></h3> 
					</div>
					<span class="info-answ"><?php echo (isset($ans["links"]["organizations"]) && !empty($ans["links"]["organizations"])) ? "Réponse liée à <a href='#page.type.organizations.id.".array_keys($ans["links"]["organizations"])[0]."' class='lbh-preview-element text-red'>".$ans["links"]["organizations"][array_keys($ans["links"]["organizations"])[0]]["name"]."</a>" : "<span style='font-style: italic;'>Aucun tiers-lieu lié à la réponse</span>" ?></span><br/>
					<span class="info-answ bold"> <i class="fa fa-calendar"></i> Créée le : <?php echo date("d.m.y",$ans["created"]) ?></span>
					<?php if ( isset($ans["updated"] )) {?>
					<br/><span class="info-answ bold"> <i class="fa  fa-edit"></i> Dernière modification le : <?php echo date("d.m.y",$ans["updated"]); ?></span>
					<?php } ?>

					<!-- <span class="info-answ col-xs-12 no-padding"> <b>Déposé par :</b> <?php echo "nameprop"; ?></span> -->

					<!-- <span class="info-answ col-xs-12 no-padding"> <b>Description :</b> <?php echo "desnamepropcirption"; ?></span> -->
					<br/>	
					<span class="info-answ"> <i class="fa fa-step"></i>Etape en cours : <?php echo isset($ans["step"]) ? (array_search($ans["step"],$form["subForms"]) + 1)." / ".count($form["subForms"]) : "1 / ".count($form["subForms"]) ?></span>
					
					<br/>
					
					<!-- <span class="label label-<?php echo "" ?>"> <i class="fa fa-pencil-square-o"></i> <?php echo "" ?> </span>

					<span class="label label-<?php echo "" ?> margin-left-5"> <i class="fa fa-black-tie"></i> <?php "statut" ?> </span> -->
					
					<br/>
					<?php if (true) {?>
						<?php if ( true) {?>
						<!-- <a href="javascript:;" data-id='<?php echo $ans["_id"] ?>' class='answerTasksBtn btn btn-xs btn-default '> <i class="fa  fa-cogs "></i>Tasks <span class="margin-5  label label-primary"> <i class="fa fa-square-o"></i> <?php echo "todo" ?> </span> <span class="margin-5  label label-success"> <i class="fa   fa-check-square-o"></i> <?php echo "done" ?> </span></a> -->
						<?php } ?>
					<?php } ?>
					<div class="col-xs-12 no-padding">
						<!-- <a href="#page.type.answers.id.<?php echo $ansId ?>" class="lbh-preview-element margin-top-5 btn btn-open-answer">
							<i class="fa fa-sign-in"></i> Prévisualiser
						</a> -->
						<a href="#answer.index.id.<?php echo $ansId ?>.mode.w.standalone.true" class="lbh margin-top-5 btn btn-open-answer">
							<i class="fa fa-sign-in"></i> Ouvrir
						</a>
						<?php 
						if (isset($ans["user"]) && $ans["user"]==Yii::app()->session["userId"] ){
						?>
							<button class="btn btn-default margin-left-10 bg-red text-white deleteAnswer" data-ansid="<?php echo $ansId ?>" data-id="<?php echo "" ?>" data-type="<?php echo Answer::COLLECTION ?>" data-form="<?php echo (string) $form['_id'] ?>" style="border:none;">
								<i class=" fa fa-trash"></i>
							</button>
						<?php 
						}
						?>	
					</div>
				</div>
		</div>
		<?php 
		       }
			}
			?>

			<div class="col-xs-12">
		<?php	
	       // echo $this->renderPartial("costum.views.custom.deal.newAnswers",$params);
		 }
	 ?> 
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
	coInterface.bindLBHLinks();
	mylog.log("pageProfil.views.notifications");
	$("#showAnswerBtndashboard").trigger("click");
	var url = "element/notifications/type/citoyens/id/"+userId;
	$('.deleteAnswer').on('click', function() {
		var itemId = $(this).data("ansid");
		const formIdFtl = $(this).attr("data-form");
		
		id = $(this).data("ansid");
		bootbox.dialog({
			title: trad.confirmdelete,
			message: "<span class='text-red bold'><i class='fa fa-warning'></i> "+trad.actionirreversible+"</span>",
			buttons: [
			{
				label: "Ok",
				className: "btn btn-primary pull-left",
				callback: function() {
				getAjax("", baseUrl+"/survey/co/delete/id/"+itemId , function(){
					//urlCtrl.loadByHash(location.hash);
					$('.answerLi[data-id="'+itemId+'"]').remove();
					$(".close-modal").trigger("click");
					history.pushState(null, "New Title","#answer.index.id.new.form."+formIdFtl+'.standalone.true');
					urlCtrl.loadByHash(location.hash);
					// ajaxPost('#modal-dashboard-account', baseUrl+'/'+moduleId+'/default/render?url=co2.views.person.dashboard',
					// {view : "costum.views.custom.franceTierslieux.accountModal"},
					// function(){});
				},"html");
				}
			},
			{
				label: trad.cancel,
				className: "btn btn-default pull-left",
				callback: function() {}
			}
			]
		});
	});
	// ajaxPost('.notifications-dash', baseUrl+'/'+moduleId+'/'+url, 
	// 	null,
	// 	function(){});
});
</script>