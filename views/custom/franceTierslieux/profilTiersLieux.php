<?php 
	$cssAnsScriptFiles = array(
        "/css/franceTierslieux/answersDirectory.css",
        "/css/franceTierslieux/navigatorThirdPlace/mythirdPlaceProfile.css",
		"/js/franceTierslieux/answerDirectory.js",
        "/js/franceTierslieux/dataviz/getElement.js",
        "/js/franceTierslieux/previewIconList/previewIconSvg.js",
    );
    $cssAnsScriptFilesTheme = array(
		'/plugins/Chart.js/Chart.v4.js',
        "/plugins/d3/d3.v3.min.js"
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFiles, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
    $getUrl = $_REQUEST["clienturi"] ?? "";
?>

<div id="my-tr-content" class="navigator-page">
    <div class="page-title" style="padding: 0% 10%">
        <p class="page-title-profile">Mon Profil Tiers-lieux</p>
        <!-- <hr style="border-top: 1px solid #858585;"> -->
    </div>

    <div class="form-fill-text text-center">
        <h4> Vous n'avez pas encore rempli tous les questions du formulaire de votre profil tiers-lieux </h4>
    </div>
    <div class="padding-bottom-20 padding-left-0 padding-right-0 profile-section-content" style="padding: 0% 10%"></div>
</div>
<div id="custom-prev-div" style="display: block;"></div>
<style>
    span.infoDate p{
        width: 2em;
        height: 2em;
        background: #244A58;
        text-align: center;
        padding: 4px 0px;
        border-radius: 50%;
    }

    span.infoDate p{
        color : white;
    }
</style>
<script>
    let formId = getNavigatorElement.suplment.profile.id;
    $(function() { 
        var profilTr = {
            thisPageUrl : "<?= $getUrl ?>",
            formAndGenButton : {},
            init : function () {
                profilTr.formAndGenButton = profilTr.actions.getFormAndGenButton()
                let allSuplFormAnswers = profilTr.formAndGenButton;
                let trResultsList = profilTr.actions.getTrList(allSuplFormAnswers);
                profilTr.views.init(true, trResultsList);
                profilTr.actions.init();
                profilTr.event.load(trResultsList);
                searchObj.footerDom = ".footerSearchContainer";
            },
            event : {
                load : function (trResultsList) {
                    // load when classment btn clicked
                    let descriptionClassment = "Classement des tiers-lieux ayant un score de similarité par rapport aux réponses des questions de votre formulaire.";
                    $(".description-heading").text(descriptionClassment);
                    profilTr.event.clickASectionEvent(".profile-classement", profilTr.views.classmentSectionView(trResultsList?.profileResults), trResultsList, descriptionClassment, false);
                    // load when profile map is clicked profile-map
                    let descriptionPrMap = "Carte d'emplacement des tiers-lieux ayant un score de similarité par rapport à votre formulaire.";
                    profilTr.event.clickASectionEvent(".profile-map", profilTr.views.profileMapView(trResultsList?.profileResults), trResultsList, descriptionPrMap, false);
                    // default click
                    $('.profile-classement').trigger("click");
                },
                loadDefaultSection : function (sectionHtml="") {
                    $('.profile-section-content').append(sectionHtml);
                },
                clickASectionEvent : function (selector, sectionHtml, trResultsList={}, description="",defaultLoad=true) {
                    $(selector).click(function () {
                        $(".description-heading").text(description);
                        if (defaultLoad) 
                            $('.profile-section-content').html(sectionHtml);
                        $('.is-checked').removeClass('is-checked');
                        $(this).addClass('is-checked');
                        profilTr.views.init(false, trResultsList, description);
                    })
                }
            },
            views : {
                init : function (loadDefault, trResultsList, description) {
                    profilTr.views.showMyProfileFormBtn(profilTr.formAndGenButton, description);
                    // profilTr.actions.showMap("#mapContainerProfileTr", trResultsList?.markerData);
                },
                classmentSectionView : function (profileTrList={}) {
                    $(".profile-classement, .profile-classement.is-checked").click(function() {
                        if (typeof profileTrList == "object" && Object.keys(profileTrList).length > 0) {
                            countRanking = 1;
                            var getAllOrg = [];
                            $.each(profileTrList, function(key, value) {
                                if (jsonHelper.getValueByPath(value, "links.organizations") != undefined && Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name != undefined) {
                                    getAllOrg.push(Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name);  //mylog.log("teeesssst", );
                                }
                            })
                            $('.profile-section-content').html(` <div class="padding-bottom-20 padding-left-0 padding-right-0 ans-dir default-dir" style="padding: 0% 10%"></div>`);
                            showAllNetwork.paramsFilters['urlData'] = baseUrl+"/costum/francetierslieux/answerdirectory/source/organizations/form/63e0a8abeac0741b506fb4f7";
                            showAllNetwork.paramsFilters.defaults.types = ['organizations'];
                            showAllNetwork.paramsFilters.defaults.filters = {name : {'$in' : getAllOrg}}; //elementPanelHtml
                            showAllNetwork.paramsFilters.filters = {text: true}
                            showAllNetwork.paramsFilters.results.renderView = "directory.listProfile";
                            showAllNetwork.showElements("","",true)
                            $('.ans-dir .row').remove();
                            const checkForMapClass = () => {
                                if ($('.endOfresults').length > 0 && $('.pageTable').length > 0) {
                                    clearInterval(checkInterval);
                                    $('.endOfresults').hide();
                                    $('#filterContainerInside').remove();
                                    // $('.pageTable').hide();
                                }
                            }
                            const checkInterval = setInterval(checkForMapClass, 100);
                        }
                    })
                },
                profileMapView : function (profileTrList={}) {
                    $(".profile-map").click(function() {
                        $('.profile-section-content').html(`<div id="filterContainer"></div><div id="mapContainerProfileTr" class="show-map"></div>`);
                        var getAllOrg = [];
                        $.each(profileTrList, function(key, value) {
                            if (jsonHelper.getValueByPath(value, "links.organizations") != undefined && Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name != undefined) {
                                getAllOrg.push(Object.values(jsonHelper.getValueByPath(value, "links.organizations"))[0].name);  //mylog.log("teeesssst", );
                            }
                        })
                        var paramsFIlter = {
                            urlData: baseUrl + "/co2/search/globalautocomplete",
                            container: "#filterContainer",
                            defaults: {
                                notSourceKey: true,
                                types : ["organizations"],
                                filters: {name: {'$in' : getAllOrg}}
                            },
                            filters : {
                                text: true,
                            }
                        }
                        let filterNetwork = searchObj.init(paramsFIlter);
                        filterNetwork.results.render = function(fObj, results, data) {
                            profilTr.actions.showMap("#mapContainerProfileTr", results);
                        };
                        filterNetwork.search.init(filterNetwork);
                    })
                },
                showMyProfileFormBtn : function (button, description) {
                    let specificBtn = $(button["htmlWhithName"]).html("<i class='fa fa-pencil'></i> Formulaire");
                    if (button != undefined && button["htmlWhithName"] != undefined && !$(button["htmlWhithName"]).attr("class").includes('bg-green')) {
                        $(".form-fill-text").append(button["htmlWhithName"]);                      
                    } else if ($(".my-orga-btn-content").length == 0) {
                        let myProfileTrSections = `
                            <div class="my-orga-btn-content margin-bottom-20" >
                                <div class="ui-group">
                                    <div class="button-group js-radio-button-group" data-filter-group="maincat">
                                        <button class="button is-checked profile-classement "><p>Classement</p></button>
                                        <button class="button profile-map"><p>Emplacement des Tiers-lieux</p></button>
                                    </div>
                                </div>
                                <p class="description-heading"></p>
                            </div>                                                                  
                        `;
                        $(".form-fill-text h4").text("");
                        $(".description-heading").text(description);
                        $(".page-title").append(specificBtn);
                        $(".form-fill-text").append(myProfileTrSections);
                    }
                }
            },
            actions : {
                init : function () {
                },
                showMap : function (container, data={}) {
                    if ($(container).length > 0) {
                        let customMap = (typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "maptiler"};
                        let appMap = new CoMap({
                            zoom : 5,
                            container : container,
                            activePopUp : true,
                            mapOpt:{
                                zoomControl : true,
                                doubleClick : true,
                            },
                            activePreview : false,
                            mapCustom:customMap,
                        })        
                        appMap.showLoader();
                        appMap.addElts(data);
                    }
                },
                loadClassmentChart : function (chartData={}, ctx) {
                    let chartType = "pie";
                    let labels = Object.keys(chartData);
                    let data = Object.values(chartData);
                    let chartConfig = {}
                    let dataValue = [];
                    chartConfig =  {
                        type: chartType,
                        data: {
                            labels: [],
                            datasets: []
                        },
                        options: {
                            responsive: true,
                        }
                    }
                    if (chartData != undefined && Object.keys(chartData).length > 0) {
                        $.each(chartData, function (k, v) {
                            dataValue.push(v);
                            let datasetValue = [{
                                label: 'Tiers-Lieux',
                                data: dataValue,
                                barThickness: 40
                            }];
                            chartConfig.data.labels.push(k);
                            chartConfig.data.datasets = datasetValue;
                        })
                    }
                    let chartJs = new Chart(ctx, chartConfig);	
                },                
                getFormAndGenButton : function () {
                    let myProfileTrFormId = formId;
                    let myProfileTrClass = "btn-profile-tiers-lieux";
                    let myProfileTrBtnName = "Formulaire mon profil Tiers-lieux";
                    return profilTr.actions.generateButtonOrGetsupl(
                        myProfileTrFormId,
                        myProfileTrClass,
                        myProfileTrBtnName,
                        userId
                    );
                },
                generateButtonOrGetsupl : function (suplForm="", btnClass="", formName="", userId) {
                    let button = {};
                    let getSuplForm = profilTr.actions.getAnswerForm (
                        suplForm,
                        userId
                    );
                    let initSuplFormData = getSuplForm;		
                    getSuplForm = getSuplForm?.allAnswers;
                    if (getSuplForm == undefined || getSuplForm.length == 0) {
                        let newResultAnswerId = profilTr.actions.newAnswers(
                            suplForm
                        ); 
                        
                        button["html"] = profilTr.actions.modalButtonHtml(btnClass, newResultAnswerId, suplForm);
                        button["htmlWhithName"] = profilTr.actions.modalButtonHtml(btnClass, newResultAnswerId, suplForm, "", formName, "red");
                        button["canShowContent"] = false;	            
                    } else {
                        let resultAnswerId = Object.keys(getSuplForm)[0];
                        let btnColor = (getSuplForm[resultAnswerId]?.answers == undefined) ? "red" : profilTr.actions.setButtonColor(getSuplForm[resultAnswerId].answers, initSuplFormData.inputs, initSuplFormData.form.subForms);
                        button["html"] = profilTr.actions.modalButtonHtml(btnClass, resultAnswerId, suplForm);
                        button["htmlWhithName"] = profilTr.actions.modalButtonHtml(btnClass, resultAnswerId, suplForm, "", formName, btnColor);
                        button["getSuplForm"] = getSuplForm;
                        button["answerId"] = resultAnswerId;
                        button["canShowContent"] = true;  
                    }
                    if (btnClass != "")
                        setTimeout(() => {
                            profilTr.actions.clickModalEvent("." + btnClass);
                            profilTr.actions.clickModalEvent("." + btnClass + "rlt");
                        }, 100);
                    return button;
                },
                getAnswerForm : function (formId="", userId="", showAnswerPath = "answers") {
                    let paramAnswers = {
                        show: [
                            showAnswerPath,
                            "links.answered",
                            "user"              
                        ],
                        where : {
                            form : formId
                        }
                    }
                    if (userId != undefined && userId != "") {
                        paramAnswers.where["user"] = userId;
                    }
                    let answerContent = profilTr.actions.getSuplementAnswers(formId, paramAnswers);
                    return answerContent;
                },
                getSuplementAnswers : function (formId="", params={}) {
                    let results = {}
                    if (formId != "") {
                        ajaxPost(
                            null,
                            baseUrl + '/costum/francetierslieux/getallanswers/type/answers/form/' + formId + '/getForm/true',
                            params,
                            function (data) {   
                                if (data.result) { 
                                    results = data;
                                } 
                            },
                            null,
                            "json",
                            { async: false }     
                        );
                    }
                    return results;
                },
                setButtonColor : function (answers={}, subFormsArrays=[], subFrmArray=[]) {
                    let countFormParams = 0;
                    let countAnswers = 0;
                    if (Object.keys(answers).length > 0) {
                        delete answers[getNavigatorElement.suplment.profile.exludeStep][getNavigatorElement.suplment.profile.exludeInput]
                        $.each(answers, function(k, v) {
                            if (subFrmArray.includes(k)) {
                                countAnswers = countAnswers + Object.keys(v).length;
                            } else {
                                if (k != "") {
                                    countAnswers = countAnswers + 1;
                                }
                            }
                        })
                    }
                    if (subFormsArrays.length > 0 && subFormsArrays[0] != undefined) {
                        delete subFormsArrays[0][getNavigatorElement.suplment.profile.exludeInput.replace("finder", "")];
                        countFormParams = Object.keys(subFormsArrays[0]).length;
                    }
                    return (countAnswers == countFormParams) ? "green" : "red";
                },
                modalButtonHtml : function (modalClass="", answerId="", formId="", step="", btnName="Formulaire", color="", relate=false, path="") {
                    let getFinderId = ((path != "" || path != undefined) && path.split(".")[2] != undefined) ? path.split(".")[2].replace("finder", "") : "";
                    return `
                        <button 
                            class="btn btn-small ${(color != "") ? "bg-" + color : "bg-blue"} ${modalClass}" style="cursor: pointer; color: white; " 
                            data-id="${answerId}" 
                            data-step="${step}"
                            data-elformid="${formId}"
                            data-relate="${(relate) ? 'true' : 'false'}"
                            data-key="${(getFinderId != '' || getFinderId != undefined) ? getFinderId : ''}"> 
                            <h5> <i class="fa fa-pencil"> </i> ${btnName} </h5>
                        </button>						
                    `;
                },
                newAnswers : function (formId="", answerPath="") {
                    let dataId = "";
                        ajaxPost(
                            null,
                            baseUrl + `/survey/answer/newanswer/form/${formId}`,
                            {
                                "action": "new"
                            },
                            function (data) {
                                if (data['_id']) {
                                    dataId = data['_id']['$id'];
                                }
                            },
                            null,
                            "json",
                            { async: false }  
                        )	

                    return dataId;				
                },
                clickModalEvent : function (selector) {
                    $(selector).off().on('click', function (e) {
                        let containerParentId = $(this).parent().parent().attr('id');
                        containerParentId = (containerParentId == undefined) ? $(this).parent().parent().parent().parent().attr('id') : containerParentId;
                        profilTr.actions.answerModal($(this).data("id"), $(this).data("elformid"),$(this).data("step"), containerParentId);
                    });
                },
                answerModal : function (answerId="", formId="", step="", containerParentId="") {
                    localStorage.setItem("menuHref", $(".vertical-menu").find(".active a").attr("href"));
                    localStorage.setItem("parentMenu", $(".vertical-menu").find(".active").parent().data("ref"));
                    if (containerParentId != "" && containerParentId != undefined) { 
                        $(".form-answer-content").show();
                        coInterface.showLoader("#" + containerParentId);
                        ajaxPost(null, baseUrl+'/survey/answer/index/id/'+answerId+'/form/'+formId+'/mode/w',
                        { url : window.location.href },
                        function(data){
                            $("#" + containerParentId).html(data);
                            setTimeout(() => {	
                                var getUrlTarget = profilTr.thisPageUrl.split("#")[1];
                                $("#wizardcontainer").prepend(`
                                    <div class="form-btn">
                                        <button style="color:white;position: fixed;right: 85%;" class="bg-main2 btn btn-return-form btn-default margin-top-20 margin-bottom-20" >
                                        <h6><i class="fa fa-arrow-circle-left"></i>  Retour</h6>
                                        </button>
                                    </div>
                                `)
                                
                                $(".btn-return-form").click(function() {
                                    urlCtrl.loadByHash("#" + getUrlTarget);
                                })
                                $(".step-vld > button").click(function() { 
                                    urlCtrl.loadByHash("#" + getUrlTarget);
                                })
                            }, 200);
                            if (step != "") {
                                showStepForm('#'+step);
                            }	
                        },"html");
                        } else { 
                        toastr.error("Erreur")
                    }
                }, 
                getTrList : function (allAns) {
                    let gatewayPaths = showAllNetwork.getElement("mappings", "64f9721b01e4500d40198831")?.fields;
                    let trList = [];
                    let allTrProfileAnswers =  (allAns["getSuplForm"] != undefined) ? Object.values(allAns["getSuplForm"])[0]?.answers?.lesCommunsDesTierslieux1662023_112_0 : {}
                    let aggregateParams = {
                        allPaths : {}
                    };
                    if (allTrProfileAnswers != undefined && gatewayPaths != undefined) {
                        $.each(allTrProfileAnswers ,function (k, v) {
                            if (gatewayPaths[k] != undefined) {
                                aggregateParams.allPaths[gatewayPaths[k]] = v
                            }
                        })
                    }
                    aggregateParams.limit = 20;
                    ajaxPost(
                        null,
                        baseUrl + '/costum/francetierslieux/getaggregatetr',
                        aggregateParams,
                        function (data) {   
                            trList = data;
                        },
                        null,
                        null,
                        { async: false }     
                    )  
                    return trList;
                },  
            }
        }
        profilTr.init();
    })

</script>
