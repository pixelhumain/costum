<div class="textHVA col-xs-10 col-xs-offset-1 padding-10">
	En vous connectant sur ce site, vous acceptez sans réserves les présentes modalités. Aussi, conformément de l’Article n°6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance dans l’économie numérique, les responsables du présent site internet <a href="https://www.portailhva.org/" >https://www.portailhva.org/</a> sont : <br/><br/>

	<span class="text-center col-xs-12"><b>Editeur du site :</b></span><br/><br/>
	Association RIHVA "Réseau des Initiatives de la Haute Vallée de l'Aude"<br/>
	chez Georges BENNAVAIL, 1 chemin de Camières, 11260 ROUVENAC, reseauhva@gmail.com <br/>
	n° RNA : W112002719, identifiant SIREN : 84246473700019, code APE : 9499Z, <br/>
	Responsable éditorial : <br/>
	Georges Bennavail tel: 04.68.74.39.73, georges.bennavail@orange.fr<br/>
	Cette association qui a pour objet de <b>faciliter la mise en réseau des acteurs porteurs d'initiatives en haute vallée de l'Aude</b> a inscrit son projet dans une dimension d’intérêt général en s’ouvrant à tous les publics, notamment les plus fragiles en garantissant en toutes circonstances un fonctionnement démocratique, transparent et en assurant le caractère désintéressé de sa gestion.<br/>
	Les modérateurs du site sont les administrateurs de l'association désignés pour cela.<br/>
	Les renseignements nominatifs recueillis lors de votre adhésion font l'objet d'un traitement informatisé et sont destinés au secrétariat de l'association. Conformément aux dispositions de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous bénéficiez d'un droit d'accés aux informations qui vous concernent et de rectification de celles-ci. Pour exercer ce droit et obtenir la communication de ces informations, veuillez vous adresser au secrétariat de l'association.<br/><br/>

	<span class="text-center col-xs-12"><b>Développeur du site :</b></span><br/><br/>
	
	Association loi 1901 Open Atlas. <br/>
	Numéro de SIRET : 513 381 830 00027. <br/>
	56 RUE ANDY, 97422, La Réunion. <br/>
	<!-- Téléphone : <br/> -->
	Email : contact@communecter.org <br/>
	Site Web : <a href="https://www.open-atlas.org/" >https://www.open-atlas.org/</a> <br/>

	<span class="text-center col-xs-12"><b>Hébergeur du site :</b></span><br/><br/>

	Hébergeur : OVH.net, <a href="https://www.ovh.com/" >https://www.ovh.com/</a><br/>
	Nom de domaine : Gandi.net, <a href="https://help.gandi.net/fr/contact/" >https://help.gandi.net/fr/contact/</a>  <br/><br/>

	<span class="text-center col-xs-12"><b>Conditions d'utilisation :</b></span><br/><br/>

	Seuls les acteurs-contributeurs dont les structures/organisations ont été validé, sont autorisés à alimenter leurs pages allouées sur le site. <br/>
	Le site ne pourra, <b>en aucun cas, être tenu responsable</b> du contenu des documents (textes, photos et autres) fournis par les acteurs-contributeurs. <br/>
	<b>Les modérateurs du site se réservent le droit de supprimer</b>, sans préavis ni indemnité ni droit à remboursement, toute information qui ne serait pas conforme à la charte du site et/ou qui serait susceptible de porter atteinte à un tiers. <br/>
	<b>Il est permis à tout utilisateur </b>(acteur-contributeur ou visiteur) de nous signaler un contenu qui lui semble abusif et/ou contraire à la charte en cliquant sur l'onglet <b>"nous contacter"</b>.<br/>
</div>