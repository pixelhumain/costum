<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}

	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}
</style>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	defaults: {
	 		types : ["organizations"],
	 		sort : { name : 1 },
            indexStep : 0
	 	},
	 	results : {
	 		renderView : "directory.lightPanelHtml"
	 	},
	 	header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						add : true,
						map : true
					}
				}
			}
	 	}
	};

	var filterSearch={};

	jQuery(document).ready(function() {
		mylog.log("HVA filters/search.php");
		filterSearch = searchObj.init(paramsFilter);
		searchInterface.init();
	});
</script>