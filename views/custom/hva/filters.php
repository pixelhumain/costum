<style type="text/css">
	#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    /*padding-top: 30px !important;*/
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}

	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}
</style>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	calendar : true,
	 	loadEvent: {
	 		default : "agenda"
	 	},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		types : {
	 			lists : ["events"]
	 		}
	 	}
	};
	var filterSearch={};
	searchObj.agenda.options.heightScroll = 900;
	searchObj.agenda.options.noResult = true;
	searchObj.agenda.options.nbLaunchSesearch = 2;
	searchObj.agenda.getDateHtml = function(fObj){
	  //mylog.log("searchObj.agenda.getDateHtml");
	  var startMoment = fObj.agenda.getStartMoment(fObj);
	  return "<div class='col-xs-12 margin-top-5' style='background-color : #3b9ca263; '><center>"+
	          "<h5>"+moment(startMoment).locale("fr").format('dddd DD MMMM')+"</h5></center></div>";
	}

	jQuery(document).ready(function() {
		mylog.log("HVA filters.php");
		filterSearch = searchObj.init(paramsFilter);
	});
</script>