<?php 
    $cssAnsScriptFilesModule = array(
	    '/js/cocityPrez/cocityPrez_index.js'
	);
  	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
?>
<style>
    .container-analyse .CustomCard {
        margin: 10px 0 20px 0;
        background-color: rgba(214, 224, 226, 0.2);
        border-top-width: 0;
        border-bottom-width: 2px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 15px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.2);
    }

    .container-analyse .CustomCard.hoverCustomCard {
        position: relative;
        padding-top: 0;
        overflow: hidden;
        text-align: center;
    }

    .container-analyse .CustomCard.hoverCustomCard:hover {
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.4), 0px 7px 13px -3px rgba(0, 0, 0, 0.3),
        0px -3px 0px rgba(0, 0, 0, 0.2) inset;
    }

    .container-analyse .CustomCard.hoverCustomCard .CustomCardheader {
        background-size: cover;
        height: 85px;
        padding-top: 5px;
    }

    .container-analyse .CustomCard.hoverCustomCard .avatar {
        position: relative;
        top: -35px;
        margin-bottom: -35px;
    }

    .container-analyse .CustomCard.hoverCustomCard .avatar span {
        width: 60px;
        height: 60px;
        font-size: 36px;
        /* padding: 6px; */
        color: white;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        border: 5px solid rgba(255,255,255,0.5);
        background-color: #2C3E50;
        display: inline-block;
    }

    .container-analyse .CustomCard.hoverCustomCard .info {
        padding: 4px 8px 10px;
        border-bottom: 1px solid #2C3E50;
    }

    .container-analyse .CustomCard.hoverCustomCard .info .desc {
        overflow: hidden;
        font-size: 20px;
        line-height: 20px;
        color: #2C3E50;
        text-overflow: ellipsis;
        font-weight: bold;
    }

    .container-analyse .CustomCard.hoverCustomCard .bottom {
        padding: 10px 5px;
        margin-bottom: -6px;
        text-align: center;
            min-height: 60px;
    }
    .container-analyse .justify-content-center {
        -ms-flex-pack: center !important;
        justify-content: center !important;
    }
    .container-analyse .flex-wrap {
        -ms-flex-wrap: wrap !important;
        flex-wrap: wrap !important;
    }
    .container-analyse .d-flex {
        display: -ms-flexbox !important;
        display: flex !important;
    }
    .container-analyse .pl-3, .px-3 {
        padding-left: 1rem !important;
    }
    .container-analyse .pr-3, .px-3 {
        padding-right: 1rem !important;
    }
    .container-analyse .pb-2, .py-2 {
        padding-bottom: .5rem !important;
    }
    .container-analyse .pt-2, .py-2 {
        padding-top: .5rem !important;
    }
    .container-analyse .text-muted {
        color: #6c757d !important;
    }
    .container-analyse .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }
    .container-analyse .small, small {
        font-size: 13px;
        font-weight: 400;
    }
    .container-analyse .font-weight-bold {
        font-weight: 700 !important;
    }
    .mb-0, .my-0 {
        margin-bottom: 0 !important;
    }
    .container-analyse .card-analyse hr {
        margin-top: 10px;
        margin-bottom: 10px;
        border: 0;
        border-top: 1px solid #2C3E50;
    }

    .container-analyse .CustomCard:hover .avatar i {
        transform: translateY(-5px);
        animation: bounce 1s infinite ease-in-out;
    }
    .title-zone {
        text-transform: none;
        text-align: center;
    }
    .search-sf h3 {
        text-transform: none;
    }

</style>

<div class="container-analyse col-xs-12 col-md-12">
    <div class="col-sm-12 col-xs-12 padding-top-25">
        <h4 class="pull-left">
            <i class="fa fa-angle-down"></i> <i class="fa fa-clone"></i> Analyse cocity
        </h4>
        <a href="#admin" class="lbh btn btn-default pull-right margin-left-5" data-view="index" id="btn-backToHome" style="display: none;">
            <b>Retourner vers l'accueil <i class="fa fa-arrow-right"></i></b>
        </a>
        <br><hr><br>
    </div>
    <div class="col-xs-12">
        <h4 class="title-zone">Liste des Régions</h4>
        <div class="row padding-bottom-20 padding-top-20">
            <div class="col-xs-12 col-md-4 col-lg-3">
                <btn class="btn btn-primary btn-region hide" data-key="3"><i class="fa fa-reply"></i> Région</btn>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3 pull-right">
                <form action="#" method="get">
                    <div class="input-group">
                        <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
                        <input class="form-control" id="system-search" name="q" placeholder="mot clé" required>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
	<div class="row card-body"></div>
</div>

<script type="text/javascript">
    
    var ObjElmt = {
        getElement : function(level,departement=null,epci=null,cities=null,country=null) {
            var params = {};
            params.level = level;
            if (departement != null)
                params.departement = departement;
            if (epci != null) 
                params.epci = epci;
            if (cities != null) 
                params.cities = cities;
            if (country != null) 
                params.country = country;
            ajaxPost(null,
            baseUrl + "/costum/cocity/getnbrelmbycity", 
                    params, 
                    function(data){
                        mylog.log("datadata",data);
                        var str = "";
                        if(data && typeof data == "object" ) {
                            $.each(data, function(key, value) {
                                str+=`<div class="col-md-4 col-sm-6 col-xs-12 card-analyse">
                                        <div class="CustomCard hoverCustomCard">
                                            <div class="CustomCardheader text-white btn-primary">
                                                <h5 class="col pt-2"><strong>${value.name}</strong></h5>
                                            </div>
                                            <div class="avatar">
                                                <span><i class="fa fa-map-marker"></i></span>
                                            </div>
                                            <div class="info">
                                                <div class="d-flex justify-content-center flex-wrap">
                                                    <div class="text-center px-3 py-2">
                                                        <p class="small text-muted mb-0">Organisation</p>
                                                        <p class="font-weight-bold mb-0">${value.nbrorganizations}</p>
                                                    </div>
                                                    <div class="text-center px-3 py-2">
                                                        <p class="small text-muted mb-0">Projet</p>
                                                        <p class="font-weight-bold mb-0">${value.nbrprojects}</p>
                                                    </div>
                                                    <div class="text-center px-3 py-2">
                                                        <p class="small text-muted mb-0">Evenement</p>
                                                        <p class="font-weight-bold mb-0">${value.nbrevents}</p>
                                                    </div>
                                                    <div class="text-center px-3 py-2">
                                                        <p class="small text-muted mb-0">Citoyen</p>
                                                        <p class="font-weight-bold mb-0">${value.nbrcitoyens}</p>
                                                    </div>
                                                    <div class="text-center px-3 py-2">
                                                        <p class="small text-muted mb-0">Poi</p>
                                                        <p class="font-weight-bold mb-0">${value.nbrpoi}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="text-center desc"> Nombre total : ${value.TotalElement}</div>
                                            </div>
                                            <div class="bottom mx-auto">
                                                <a class="btn btn-primary btn-departement ${value.level == '3' ? '' : 'hide'}" data-key="${key}" data-name="${value.name}">
                                                    <i class="fa fa-map-marker text-white pointer"></i> ${(value.countryCode == "MG") ? "District" : "Departement"} 
                                                </a>
                                                <a class="btn btn-primary btn-epci ${value.level == '4' ? '' : 'hide'}" data-key="${key}" data-name="${value.name}" data-country="${value.countryCode}">
                                                    <i class="fa fa-map-marker text-white pointer"></i> ${(value.countryCode == "MG") ? "Commune" : "EPCI"} 
                                                </a>
                                                <a class="btn btn-primary btn-cities ${value.level == '5' && value.countryCode != "MG" ? '' : 'hide'}" data-key="${key}" data-name="${value.name}">
                                                    <i class="fa fa-map-marker text-white pointer"></i> Ville
                                                </a>
                                                <a class="btn btn-primary hide">
                                                    <i class="fa fa-map-marker text-white pointer"></i> Generer un cocity
                                                </a>
                                                
                                            </div>
                                        </div>
                                    </div>`;
                            });
                        } else {
                            str += "<h2>Aucun Element Trouvé</h2>";
                        }
                        $(".container-analyse .card-body").html(str);
                        ObjElmt.eventElmt();
                    }
                );
        },
        eventElmt : function() {
            $(".btn-departement").off().on("click",function(){
                coInterface.showLoader(".container-analyse .card-body")
                ObjElmt.getElement("4",$(this).data('key'));
                $(".btn-region").removeClass("hide")
                $(".title-zone").html("Liste des departements dans la région "+$(this).data('name'));
                _typeCreate = "region";
            });
            $(".btn-epci").off().on("click",function(){
                coInterface.showLoader(".container-analyse .card-body")
                ObjElmt.getElement("5",null,$(this).data('key'),null,$(this).data('country'));
                $(".title-zone").html(($(this).data('country') == "MG" ? "Liste des Commune dans la district " : "Liste des epci dans la departement de ") +$(this).data('name'));
                _typeCreate = "departement";
            });
            $(".btn-cities").off().on("click",function(){
                coInterface.showLoader(".container-analyse .card-body")
                ObjElmt.getElement(null,null,null,$(this).data('key'));
                $(".title-zone").html("Liste des villes dans l'epci "+$(this).data('name'));
                _typeCreate = "epci";
            });
        }

    }

    $(document).ready(function() {
        coInterface.showLoader(".container-analyse .card-body")
        var activeSystemClass = $('.list-group-item.active');

        $('#system-search').keyup( function() {
        var that = this;
            var tableBody = $('.container-analyse .card-body');
            var tableRowsClass = $('.container-analyse .card-body .card-analyse');
            $('.search-sf').remove();
            tableRowsClass.each( function(i, val) {
                var rowText = $(val).text().toLowerCase();
                var inputText = $(that).val().toLowerCase();

                if( rowText.indexOf( inputText ) == -1 ){
                    tableRowsClass.eq(i).hide();
                }else{
                    $('.search-sf').remove();
                    tableRowsClass.eq(i).show();
                }
            });   
            if(tableRowsClass.children(':visible').length == 0){
                tableBody.append('<div class="search-sf col-xs-12 text-center"><h3>Aucune résultat Trouvé</h3></div>');
            }
        });
        ObjElmt.getElement("3");
        var _typeCreate = "country";
        $(".btn-region").click(function() {
            coInterface.showLoader(".container-analyse .card-body")
            let valeur = $(this).data("key");
            ObjElmt.getElement(valeur);
            $(".btn-region").addClass("hide")
            $(".title-zone").html("Liste des Régions");
        });
        
});
</script>