<?php 
    $allDoc = Document::getListDocumentsWhere(
        array(
        "id"=> (String)$element["_id"],
        "type"=>$element["collection"]
        ), "file"
    );
    $documentsLogo = "";
    $documentsBrochure = [];
    $documentsBusinessCard = [];
    foreach($allDoc as $kDoc => $vDoc){
        if(isset($vDoc["subKey"])) {
            if($vDoc["subKey"] == "documentsLogo"){
                $documentsLogo = $vDoc["docPath"];
            }
            if($vDoc["subKey"] == "documentsBrochure"){
                if(strpos($vDoc["docPath"],".php")){
                    $documentsBrochure["pdf"] = [];
                    $documentsBrochure["pdf"][] = $vDoc["docPath"];
                }else{
                    $documentsBrochure["img"] = [];
                    $documentsBrochure["img"][] = $vDoc["docPath"];
                }
            }
            if($vDoc["subKey"] == "documentsBusinessCard"){
                if(strpos($vDoc["docPath"],".pdf")){
                    $documentsBusinessCard["pdf"] = [];
                    $documentsBusinessCard["pdf"][] = [
                        "name" => $vDoc["name"],
                        "docPath" => $vDoc["docPath"]
                    ];
                }else{
                    $documentsBusinessCard["img"] = [];
                    $documentsBusinessCard["img"][] = $vDoc["docPath"];
                }
            }
        }
    }
?>

<style type="text/css">
    .ficheMetier body {
        background-color: #f5f7fb;
    }
    .ficheMetier .title-subtitle {
        text-align: center;
    }
    .ficheMetier div[class^="illustration-border"] {
        position: absolute;
        background-color: #c5d117;
    }
    .ficheMetier .card-title {
        margin-top: 5px;
        margin-bottom: 5px;
        font-size : 20px;
        font-weight: 700;
        text-decoration: underline;
    }
    .ficheMetier .card-subtitle {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 25px;
        text-transform : uppercase;
        font-weight: 700;
    }
    .ficheMetier .illustration-border-left-1 {
        width: 4px;
        height: 60px;
        top: 0;
        left: 0;
    }
    .ficheMetier .illustration-border-left-2 {
        width: 10%;
        height: 4px;
        top: 0;
        left: 0;
    }
    .ficheMetier .illustration-border-right-1 {
        width: 4px;
        height: 60px;
        bottom: 0;
        right: 0;
    }
    .ficheMetier .illustration-border-right-2 {
        width: 10%;
        height: 4px;
        bottom: 0;
        right: 0;
    }
    .ficheMetier .fiche-img {
        width: 100%;    
        height: 300px;
        object-fit: contain;
        object-position: center;
    }
    .ficheMetier .doc-img {
        width: auto;    
        height: 300px;
        object-fit: contain;
        object-position: center;
    }
    .ficheMetier .well-fiche.text-blue-fiche p {
        font-size: 20px !important;
    }
    .ficheMetier .fiche-parag {
        color: #093081;
        font-size: 25px;
        margin: 15px 0px;
        text-align: center;
    }
    .ficheMetier .bg-blue-fiche {
        background-color: #90b2f8;
    }
    .ficheMetier .bg-white {
        background-color: #ffffff;
    }
    .ficheMetier .text-blue-fiche {
        color: #093081;
    }
    .ficheMetier .well-fiche {
        margin: 15px 0px;
        font-size: 20px;
        border-radius: 15px;
        word-break: break-word;
    }
    .ficheMetier .label-fiche {
        border-radius: 0px; 
        padding: 5px 60px;
        position: relative;
        min-width: 45%;
        text-align: center;
        display: inline-block;
        margin-bottom: 25px;
        font-size: 20px;
        margin-top:5px;
    }
    .ficheMetier .well-fiche .read-more {
        display: block;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 50%;
        background: #fff;
        color: #90b2f8;
        border: 1px solid #e7e7e7;
        font-size: 18px;
        margin: 0 auto;
        position: absolute;
        top: -9px;
        left: 0;
        right: 0;
        transition: all 0.3s ease-out 0s;
        text-align: center;
        font-weight: 700;
        text-decoration: none;
    }
    .ficheMetier .fiche-logo .logo {
        height: 100px;
        margin: auto;
        margin-bottom: 30px;
    }
    .ficheMetier .fiche-logo {
        text-align: center;
    }
    .ficheMetier .fiche-logo .description {
        font-weight: 700;
        margin-top: 20px;
    }

    @media (max-width:767px)  {
        .ficheMetier .card-title {
            font-size: 16px;
        }
        .ficheMetier .card-subtitle {
            font-size: 20px;
        }
        .ficheMetier .fiche-parag {
            font-size: 20px;
        }
        .ficheMetier .well-fiche {
            font-size: 15px;
        }
        .ficheMetier .label-fiche {
            padding: 5px 10px;
            margin-bottom: 20px;
            font-size: 14px;
        }
        .ficheMetier .well-fiche .read-more {
            top: -23px;
        }
        .ficheMetier .fiche-themetique {
            margin-top: 30px;
        }
        .ficheMetier .fiche-logo .logo {
            height: 70px;
        }
    }
    .ficheMetier .btnClass .btnstyle {
        margin-bottom : 2px;
        border: 2px solid #90b2f8;
        font-size : 16px;
    }
    .ficheMetier .btnClass .green {
        margin-bottom : 2px;
        border: 2px solid #34a853;
        font-size : 16px;
    }
    .ficheMetier .btn-delete {
        border: 2px solid red;
        margin-bottom : 2px;
        color: red;
        font-size : 16px;
    }
    .ficheMetier .btnClass{
        margin-top:10px;
    }
    .ficheMetier.tag-fiche-metier{
        font-size: 15px;
        
    }
    .link-fiche{
        text-decoration: underline;
        color: #093081;
    }
</style>
<div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 ficheMetier ">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="title-subtitle col-xs-12">
                 <div class="card-title text-blue-fiche">
                    <?php echo @$element["userFirstName"] ." ". @$element["userSurname"] ." ". $element["name"];  ?>
                </div> 
            </div>
            <div class="fiche-parag text-blue-fiche col-xs-12">
                <div class="list-tag">
                    <?php if(isset($element["tags"])){
                        $tagss = array_unique($element["tags"]);
                        foreach($tagss as $kTags => $vTags){?>
                            <a href = "#search?tags=<?=$vTags?>" class="lbh" >
                                <span class="btn-tag  bg-blue-fiche text-blue-fiche badge tag-fiche-metier btn-tag tag">
                                    #<?=$vTags?>
                                </span>
                            </a>
                        <?php }
                    } ?>
                </div>
                <div class="col-xs-12 btnClass">
                 
                    <!-- btn édition -->
                    <?php if(Authorisation::canEditItem(Yii::app()->session["userId"], $element["collection"],(String)$element["_id"])){?>
                        <a href="javascript:;" class="btn-edit-element pull-left btn margin-right-10 letter-blue btnstyle" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-pencil"></i> Modifier les informations</a>
                    <?php }?>
                    <!-- btn supprimer -->
                     <?php if(Authorisation::isInterfaceAdmin()) {?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="delete" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php } ?>
                     <?php if(Authorisation::isInterfaceAdmin()) {?>
                        <a href="javascript:;" class="addInCat pull-left btn margin-right-10 letter-blue btnstyle" data-username="<?php echo isset($creator)?$creator:"" ?>" data-type="organizations"><i class="fa fa-plus"></i> Ajouter le créateur de la fiche dans le channel </a>
                    <?php } ?>
                </div>	
            </div>
            <div class="well well-fiche col-xs-12 text-blue-fiche bg-blue-fiche" >
                <!-- <b>Qui suis-je ?</b> <br> -->
                <?php if (isset($element["userSurname"])){?>
                    <b>Nom : </b> 
                        <?php   echo $element["userSurname"]; ?><br>
                <?php }?>	
                <?php if (isset($element["userFirstName"])){?>
                    <b> Prénom : </b> 
                        <?php   echo $element["userFirstName"]; ?><br>
                <?php }?>	
                <?php if (isset($element["userDate"])){?>
                    <b> Date à laquelle j’ai été formée par la Plume Sociale : </b> <br>
                        <?php   echo $element["userDate"]; ?><br>
                <?php }?>	
                <?php if (isset($element["userSelfEmployed"])){?>
                    <b> Suis-je en libéral : </b> 
                        <?php  
                            if($element["userSelfEmployed"] == false || $element["userSelfEmployed"] == "false")    
                                echo " en cours de formation";
                            else
                                echo "en libéral";
                        ?><br>
                <?php }?>	
                <?php if (isset($element["telephone"]["mobile"])){ ?>
                <b>Téléphone : </b> <?php 
                    $replaced = str_replace(' ', '', $element["telephone"]["mobile"][0]);
                    if (strpos($element["telephone"]["mobile"][0],"+") !== false){
                        $result = sprintf("%s %s %s %s %s %s",
                        substr($replaced, 0, 4),
                        substr($replaced, 4, 1),
                        substr($replaced, 5, 2),
                        substr($replaced, 7, 2),
                        substr($replaced, 9, 2),
                        substr($replaced, 11));

                    } else{
                        $result = sprintf("%s %s %s %s %s %s",
                        substr($replaced, 0, 2),
                        substr($replaced, 2, 2),
                        substr($replaced, 4, 2),
                        substr($replaced, 6, 2),
                        substr($replaced, 8, 2),
                        substr($replaced, 10));
                    }
                    echo $result; ?>
                <br>                        
            <?php } ?>
                
            <?php if (isset($element["email"])) {?>
                <b>Mail : </b><?php 
                echo $element["email"]; ?>
                <br>
            <?php } ?>
               
            </div>
        </div>
        <?php 
        if(isset($element["profilImageUrl"])) 
            $src = $element["profilImageUrl"];
        else 
            $src =  Yii::app()->getModule("co2")->assetsUrl."/images/logos/default_img.png";
        ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <img  class="img-responsive fiche-img" src = "<?= $src?>">
            <?php if(isset($element["url"]) && isset($element["socialNetwork"]["linkedin"]) && isset($element["socialNetwork"]["facebook"]) && isset($element["socialNetwork"]["instagram"]) && isset($element["socialNetwork"]["otherSociaNetworks"])) {?>
                <div class="well well-fiche text-blue-fiche bg-white" >            
                    <?php if (isset($element["url"])){?>					
                        <b>Mon site web : </b>  <a class="link-fiche" href="<?php echo $element['url']?>" target="_blank"> 
                            <?php echo $element["url"]; ?>                       
                        </a> 
                        <br>
                    <?php } ?>
                    <?php if (isset($element["socialNetwork"]["linkedin"])){?>					
                        <b>Page linkedIn : </b>  <a class="link-fiche" href="<?php echo $element["socialNetwork"]["linkedin"]?>" target="_blank"> 
                            <?php echo $element["socialNetwork"]["linkedin"]; ?>                       
                        </a> 
                        <br>
                    <?php } ?>
                    <?php if (isset($element["socialNetwork"]["facebook"])){?>					
                        <b>Page facebook : </b>  <a class="link-fiche" href="<?php echo $element["socialNetwork"]["facebook"]?>" target="_blank"> 
                            <?php echo $element["socialNetwork"]["facebook"]; ?>                       
                        </a> 
                        <br>
                    <?php } ?>
                    <?php if (isset($element["socialNetwork"]["instagram"])){?>					
                        <b>Page instagram : </b>  <a class="link-fiche" href="<?php echo $element["socialNetwork"]["instagram"]?>" target="_blank"> 
                            <?php echo $element["socialNetwork"]["instagram"]; ?>                       
                        </a> 
                        <br>
                    <?php } ?>
                    <?php if (isset($element["socialNetwork"]["otherSociaNetworks"])){?>					
                        <b>Autre réseau social : </b>  
                            <?php echo $element["socialNetwork"]["otherSociaNetworks"]; ?>                       
                        </a> 
                        <br>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="well well-fiche text-blue-fiche bg-white" >
                <div class="text-center">
                    <span class="label label-fiche bg-blue-fiche">Mes qualifications</span>
                </div> 
                <?php if (isset($element["qualificationsStateDiplomas"])){?>					
                    <b>Diplôme(s) d’Etat(s) obtenu(s) : </b>  <br>
                    <?php foreach(explode("," , $element["qualificationsStateDiplomas"]) as $v){  
                        echo " - ". $v ."<br>";
                        
                    } ?>   <br>
                <?php } ?>
                <?php if (isset($element["qualificationsOtherCompetences"])){?>					
                    <b>Autres compétences  : </b>  <br>
                    <?php foreach(explode("," , $element["qualificationsOtherCompetences"]) as $v){  
                        echo " - ". $v ."<br>";
                        
                    } ?>   <br> 
                <?php } ?>
                <?php if (isset($element["qualificationsOtherQualifications"])){?>					
                    <b>Autres qualifications  : </b>  <br>
                    <span class="markdown"><?php echo $element["qualificationsOtherQualifications"]; ?></span> 
                <?php } ?>
                
            </div>
        </div>
        <div class="col-xs-6">
            <div class="well well-fiche text-blue-fiche bg-white" >
                <div class="text-center">
                    <span class="label label-fiche bg-blue-fiche">Mon entreprise </span>
                </div> 
                
                <?php if (isset($element["name"])) {?>
                    <b>Nom de mon entreprise : </b>
                    <?php echo $element["name"]; ?>
                    <br>
                <?php } ?>
                <?php if (isset($element["companyInterventionMethods"])) {?>
                    <b>Modalités d’interventions : </b>
                    <?php
                        if(is_array($element["companyInterventionMethods"])){
                            foreach($element["companyInterventionMethods"] as $kcim => $vcim){
                                echo "-". $vcim."</br>";
                            }
                        }else {
                            echo $element["companyInterventionMethods"];
                        } ?>
                    <br>
                <?php } ?>
                <?php if (isset($element["address"])){?>
                    <b>Siège de mon entreprise : </b> 
                        <?php if (isset($element["address"]["streetAddress"] ))	
                            echo $element["address"]["streetAddress"];
                        echo " ".$element["address"]["postalCode"]." ".$element["address"]["addressLocality"];	
                        ?><br>
                <?php }?>	
                <?php if (isset($element["companyLocationsFaceTofacesessions"])){?>					
                    <b>Lieu d’interventions en présentiel  : </b>  
                    <span class="markdown"><?php echo $element["companyLocationsFaceTofacesessions"]; ?></span> <br>
                <?php } ?>
                <?php if (isset($element["companyLocationsRemoteInterventions"])){?>					
                    <b>Lieu d’interventions en distanciel  : </b>  
                    <span class="markdown"><?php echo $element["companyLocationsRemoteInterventions"]; ?></span> <br>
                <?php } ?>
                <?php if (isset($element["companyPublicCible"])){?>					
                    <b>Public(s) cible(s)</b>  <br>
                    <?php foreach(explode("," , $element["companyPublicCible"]) as $v){  
                        echo " - ". $v ."<br>";                        
                    } ?>   <br>
                <?php } ?>
                
            </div>
        </div>
    </div>    
    <?php if (isset($element["companyDescription"])){?>		
        <div class="row">
            <div class="col-xs-12">
                <div class="well well-fiche text-blue-fiche bg-white" >
                    <div class="text-center">
                        <span class="label label-fiche bg-blue-fiche">Présentation de mon entreprise  : </span>
                    </div>
                    <span class="markdown"><?php echo $element["companyDescription"]; ?></span> 
                    <br>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="well well-fiche text-blue-fiche bg-white" >
                <div class="text-center">
                    <span class="label label-fiche bg-blue-fiche">Mes images et documents</span>
                </div>
                    <?php if(!empty( $documentsLogo)){?>
                    <div>Logo de mon entreprise</div>
                    <img  class="img-responsive doc-img" src = "<?= $documentsLogo?>">                    
                    <br>
                <?php } ?>
                <?php if(!empty($documentsBrochure)){?>
                    <div>Ma carte de visite</div>
                    <?php if(!empty($documentsBrochure["pdf"])){
                        foreach($documentsBrochure["pdf"] as $cpdf){?>
                            - <a class="link-fiche" href="<?php echo $cpdf; ?>" target="_blank"> <?php echo $cpdf; ?> </a> <br>                                
                        <?php }
                    }else if(!empty($documentsBrochure["img"])){ 
                        foreach($documentsBrochure["img"] as $cimg){?>
                            <img  class="img-responsive doc-img" src = "<?= $cimg?>">                                
                        <?php } ?>                        
                    <?php } ?>
                    <br>
                <?php } ?>
                <?php if(!empty($documentsBusinessCard)){ ?>
                    <div>Ma plaquette</div>                    
                    <?php if(!empty($documentsBusinessCard["pdf"])){
                        foreach($documentsBusinessCard["pdf"] as $cpdf){?>
                            - <a class="link-fiche" href="<?php echo $cpdf["docPath"]; ?>" target="_blank" download> <?php echo $cpdf["name"]; ?> </a> <br>                                
                        <?php }
                    }
                    if(!empty($documentsBusinessCard["img"])){ 
                        foreach($documentsBusinessCard["img"] as $cimg){?>
                            <img  class="img-responsive doc-img" src = "<?= $cimg?>">                                
                        <?php } ?>                        
                    <?php } ?>
                    <br>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function() {
        setTitle("", "", costum.metaTitle);
        directory.bindBtnElement();
        $(".ficheMetier .addInCat").off().on("click",function () {
            var username = $(this).data("username");
            var chat = costum.tools.chat.int;
            let hashRef = (chat["url"] != "undefined")?chat["url"].split("/"):[];
            let room = "";
            if(hashRef.length>0){
                room = hashRef[hashRef.length-1];
            }
            ajaxPost(
                null, 
                baseUrl + '/costum/cercletravailleurssociaux/addtochat',
                {
                    "room" : room,
                    "username" : username,
                    "contextType": costum.contextType,
                }, 
                function(res){
                    toastr.success("Utilisateur ajouté dans le channel");
                },
                function(error){
                    mylog.log("Error", error);
                }
            )
        })
        $(".ficheMetier .delete").off().on("click",function () {
            $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
            var btnClick = $(this);
            var id = $(this).data("id");
            var type = "organizations";
            var params = [];
            var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
            bootbox.confirm("voulez vous vraiment supprimer cet acteur !!",
                function(result)
                {
                    if (!result) {
                        btnClick.empty().html('<i class="fa fa-trash"></i>  Supprimer');
                        return;
                    } else {
                        ajaxPost(
                            null,
                            urlToSend,
                            params,
                            function(data){
                                if ( data && data.result ) {
                                    toastr.success("acteur effacé");
                                    $("#"+type+id).remove();
                                    urlCtrl.loadByHash("#annuaire");
                                } else {
                                    toastr.error("something went wrong!! please try again.");
                                }
                            }
                        );
                    }
                }
            );
        });

    })
</script>