<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>

<style>

	#filterContainer #input-sec-search .input-global-search, .searchBar-filters .search-bar, #filterContainer #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
		margin-top:0px !important;	
	}

	.searchObjCSS .searchBar-filters {
		margin-top: 5px;
	}
	@media (min-width: 1338px){
		.searchObjCSS .searchBar-filters{
			width: 37%!important;
		}
		.searchBar-filters .main-search-bar-addon{
			width: 7% !important;
		}

	}
	@media screen and (min-width: 1200px) and (max-width: 1337px) { 
		.searchObjCSS .searchBar-filters{
			width: 44%!important;
		}
		.searchBar-filters .main-search-bar-addon{
			width: 7% !important;
		}

	}
	.commonClassDesc {
		word-wrap: break-word;
		display: -webkit-box !important;
		-webkit-line-clamp: 3;
		-webkit-box-orient: vertical;
		overflow: hidden;
	}
	.searchObjCSS .dropdown .btn-menu,
	.searchObjCSS .dropdown .dropdown-menu .list-filters{
		border: 1px solid #0e568d;
    	color: #0e568d;
	}
	.searchBar-filters .search-bar{
		border-color : #0e568d;
	}
	.searchBar-filters .main-search-bar-addon{
		background-color: #0e568d;
	}
</style>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var thematic = null;
	var typeFinancing = null;
	var greatFamily = null;
	var statu = null;
	/*
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.secteur != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.secteur);
	}
	*/
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.themes != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.themes);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.accompanimentTypeFilter != "undefined"){
		greatFamily = {};
		Object.assign(greatFamily, costum.lists.accompanimentTypeFilter);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.status != "undefined"){
		statu  = {};
		Object.assign(statu , costum.lists.status);
	}
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.typeFinancing != "undefined"){
		typeFinancing  = {};
		
		Object.assign(typeFinancing , costum.lists.typeFinancing);
	}

	var defaultScopeList = ["RE"];

	var defaultFilters = {'$or':{}};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["source.keys"] = costum.slug;
    // defaultFilters['$or']["reference.costum"] = costum.slug;
    // defaultFilters['$or']["links.projects."+costum.contextId] = {'$exists':true};
    // defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
	let paramString = pageApp.split('?')[1];
	let queryString = new URLSearchParams(paramString);

	for (let pair of queryString.entries()) {
		if(pair[0] != "tags"){
			if(pair[0] != "text"){
				defaultFilters[pair[0]]=pair[1].replace("%", "");
			}
		}else{ // remove this if & are removed from tags
			searchObj.search.obj.tags = [];
		}
	}


	var paramsFilter= {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : ["organizations"],
		 	fields: ["name","userFirstName", "category", "type", "userSurname", "companyDescription"],
		 	filters: defaultFilters,
			sortBy: {"created": 1},
			indexStep:0
		},
        header : {
            options : {
                left : {
                    classes : 'col-xs-8 elipsis no-padding',
                    group:{
                        count : true
                    }
                },
                right : {
                    classes : 'col-xs-4 text-right no-padding',
                    group : { 
                        map : true,
                        add : {
							label : "Créer ma fiche"
						}
                    }
                }
            }
        },
		filters : {	
			stateDiplomas : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "Diplôme",
	 			keyValue: true,
				event : "tags",
		 		list : costum.lists.stateDiplomas
		 	},
			departement:{
				view : "scopeList",
				name : "Département",
				params : {
					countryCode : ["FR","RE","MQ","GP","GF","YT"],
					level : ["4"],
					sortBy : "name"
				}
			},
			publicCible : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "Publics cibles",
	 			keyValue: true,
				event : "tags",
		 		list : costum.lists.publicCible
		 	},
			liberal : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "En activité",
	 			keyValue: true,
				event : "tags",
		 		list : ["en libéral","en cours de formation"]
		 	},
			interventionMethods : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "Modalités d’interventions",
	 			keyValue: true,
				event : "tags",
		 		list : costum.lists.interventionMethods
		 	},
			text : {
				searchBy:["name","userSurname","userFirstName"], 
				placeholder : "Quelle collègue recherchez-vous ?"
			} 	
	 		
	 	},
	 	results : {
			renderView: "cercleTRavailleurFilter.elementPanelHtmlFullWidth",
		 	smartGrid : false
		}
	}
	var cercleTRavailleurFilter = {
		elementPanelHtmlFullWidth : function (params) { 

		var str = '';
 		var userFirstName = (typeof params.userFirstName != "undefined")?params.userFirstName:" "
 		var userSurname = (typeof params.userSurname != "undefined")?params.userSurname:" "
 		var companyDescription = (typeof params.companyDescription != "undefined")?params.companyDescription:" "
		str += '<div class="container-lib-item">' +
			'<div id="entity_' + params.collection + '_' + params.id + '" class="col-md-12 col-xs-12 col-sm-12 no-padding lib-item searchEntityContainer ' + params.containerClass + '" data-category="view">' +
			'<div class="lib-panel">' +
			'<div class="row box-shadow">' +
			'<div class="col-xs-12 col-sm-3 col-md-2 no-padding">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' +
			params.imageProfilHtml +
			'</a>' +
			'</div>' +
			'<div class="col-xs-12 col-sm-9 col-md-10">' +
			'<div class="lib-row lib-header">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' + userFirstName + " "+  userSurname + " " + params.name  + '</a>' +
			'<div class="lib-header-seperator"></div>' + 
			'</div>';
			str += directory.descriptionConfigAppend (companyDescription);
		if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';
		if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality">' +
				'<span> ' + params.localityHtml + '</span>' +
				'</div>';
		}
		str += '</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		return str;
	}
	} 
	if(pageApp == "projects"){
		delete paramsFilter.filters["type"];
	}

	if(thematic==null || pageApp=="projects"){
		delete paramsFilter.filters["secteur"];
	}

	if(pageApp=="projects"){
		delete paramsFilter.filters["families"];
	}
	 
	jQuery(document).ready(function() { 

		filterSearch = searchObj.init(paramsFilter);
		$(".count-badge-filter").remove();
		
		$(".filters-activate[data-key='startup'], .filters-activate[data-type='objectiveOdd']").on("click", function(){
			urlCtrl.loadByHash(location.hash);
		})
		
	});

</script>