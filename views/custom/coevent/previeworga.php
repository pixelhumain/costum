<?php
    $organizer = PHDB::findOne($params['type'], array('_id' => new MongoId($params["id"])));
    // var_dump($organizer);
?>


<style>

    #modal-preview-coop:has(.co-container-drawer) {
        background: #1c1c1e;
        overflow-y: auto;
        border-left: 1px solid #444 !important;
    }
    .co-row {
        display: flex;
        flex-direction: column;
    }

    .co-d-center {
        display: flex;
        justify-content: center;
    }

    .co-d-between {
        display: flex;
        justify-content: space-between;
    }

    .co-d-end {
        display: flex;
        justify-content: end;
    }

    .co-img-large {
        width: 250px;
        height: 200px;
        border-radius: 10px
    }

    .co-p-4 {
        padding: 20px
    }

    .co-text-white {
        color: white;
    }

    .co-text-gray {
        color: #938f8f;
    }

    .date-card {
        width: 50px;
        height: 50px;
        background-color: #333;
        color: white;
        border-radius: 10px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        font-family: Arial, sans-serif;
    }

    .month {
        font-size: 12px;
        font-weight: bold;
        margin-top: 5px
    }

    .day {
        font-size: 20px;
        font-weight: bold;
    }

    .co-title-evt {
        font-size: 15px;
        font-weight: bold;
    }



    .co-card {
        background-color: #333;
        color: white;
        border-radius: 10px;
        padding: 5px;
        font-family: Arial, sans-serif;
        box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.3);
    }

    .co-card-title {
        font-size: 14px;
        font-weight: bold;
        opacity: 0.7;
        margin-bottom: 10px;
        padding: 6px 2px;
    }

    .co-card-message {
        font-size: 16px;
        margin-bottom: 20px;
        line-height: 1.4;
    }

    .co-user-info {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
    }

    .co-avatar {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-color: #f28b82;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        margin-right: 10px;
    }

    .co-user-details {
        font-size: 16px;
    }

    .co-username {
        font-weight: bold;
    }

    .co-email {
        font-size: 14px;
        color: #aaa;
    }

    .co-register-button {
        width: 100%;
        padding: 10px 0;
        background-color: white;
        color: #333;
        font-size: 16px;
        font-weight: bold;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .co-card-body {
        padding: 10px;
        background: #1c1c1e;
    }

    .co-register-button:hover {
        background-color: #ddd;
    }

    .co-d-flex{
        display: flex;
    }

    .co-list-participate{
        border: 2px solid #333;
        padding: 10px;
        border-radius: 5px;
    }

    .co-img-user-list {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        display: inline-block;
    }

    .disabled-co{
        cursor: not-allowed !important;
    }

    @media only screen and (max-width: 600px) {
        .co-d-flex{
            display: block;
        }    
    }


    .co-tags {
        list-style: none;
        margin: 0;
        overflow: hidden; 
        padding: 0;
    }

    .co-tags li {
        float: left; 
    }

    .co-tag {
        background: #938f8f;
        border-radius: 3px 0 0 3px;
        color: #fff;
        display: inline-block;
        height: 26px;
        line-height: 26px;
        padding: 0 20px 0 23px;
        position: relative;
        margin: 0 10px 10px 0;
        text-decoration: none;
        font-size: 15px;
        -webkit-transition: color 0.2s;
    }

    .co-tag::before {
        background: #fff;
        border-radius: 10px;
        box-shadow: inset 0 1px rgba(0, 0, 0, 0.25);
        content: '';
        height: 6px;
        left: 10px;
        position: absolute;
        width: 6px;
        top: 10px;
    }

    .co-tag::after {
        background: #1c1c1e;
        border-bottom: 13px solid transparent;
        border-left: 10px solid #938f8f;
        border-top: 13px solid transparent;
        content: '';
        position: absolute;
        right: 0;
        top: 0;
    }

    .co-tag:hover {
        background-color: #c7d301;
        color: white;
        text-decoration: none;
    }

    .co-tag:hover::after {
        border-left-color: #c7d301; 
    }


    .co-btngroup {
        line-height: 1;
        white-space: nowrap;
        display: inline;
        position: absolute;
        right: 10px;
        top: 10px;
    }
    
    .co-btn-close {
        padding: 10px;
        border: 1px solid #444;
        background: transparent;
        color: white;
    }
    
    .co-btn {
        padding: 10px;
        border: 1px solid #444;
        background: transparent;
        color: white;
    }

    .co-btn:first-child {
        border-left-width: 2px;
        border-radius: 2em 0 0 2em;
    }

    .co-btn-with-line {
        border: none; 
        padding-left: 0; 
        background: transparent;
        text-decoration-line: underline;
    }
    
    .co-btn-with-line:hover {
        color: #c7d301;
    }

    .co-drop-cap::first-letter {
        font-size: 5em;
        font-weight: bold; 
        float: left; 
        margin-right: 10px;
        line-height: 0.8;
        font-family: serif;
        color: #c7d301;
    }


    .co-twelve h5 {
        font-size: 28px;
        font-weight: 500;
        letter-spacing: 0;
        padding-bottom: 10px;
        position: relative;
    }
    .co-twelve h5:before {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        height: 5px;
        width: 55px;
        background-color: #c7d301;
    }
    .co-twelve h5:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 2px;
        height: 1px;
        width: 95%;
        max-width: 255px;
        background-color: #c7d301;
    }

    .co-to-close {
        background: #444;
    }
    
    .co-to-close:hover {
        background: #878585;
    }

    .co-btn-navigate:hover {
        background: #5d5959;
        color: white;
        text-decoration: none;
    }

</style>

<div class="co-container-drawer">
    <div class="co-row">
        <div class="col-lg-12 co-d-end" style="padding: 10px 0;">
            <div>
                <?php if ( !isset($params["showBackevent"]) || !$params["showBackevent"] ) : ?>
                    <button class="co-btn" onclick="retourViewEvent()"><i class="fa fa-eye" aria-hidden="true"></i> <?= Yii::t("cms","See the event") ?></button>
                <?php endif; ?>
                <a href="#page.type.<?= (String)$organizer["collection"]?>.id.<?= (String)$organizer["_id"]?>" class="co-btn co-btn-navigate lbh" data-slug="<?= $organizer['slug'] ?>"><i class="fa fa-link" aria-hidden="true"></i> <?= Yii::t("cms","Go to page") ?></a>
                <button class="co-btn co-to-close" ><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="col-lg-12 co-d-center">
            <?php if (isset($organizer['profilImageUrl'])): ?>
                <img src="<?= $organizer['profilImageUrl'] ? $organizer['profilImageUrl']  : Yii::app()->getModule( "co2" )->assetsUrl+"/images/thumbnail-default.jpg" ?>" alt="" srcset="" class="co-img-large costum-img-preview-detail" style="width: 250px; height: 200px; object-fit: cover;">
            <?php endif; ?>
        </div>
        <div class="col-lg-12 co-p-4">
            
            <?php if (isset($organizer['name'])): ?>
                <div class="co-twelve">
                    <h5 class="co-text-white co-title-evt"><?= $organizer['name'] ?></h5>
                </div>
            <?php endif; ?> 

            <?php if (isset($organizer['shortDescription'])): ?>
                <p class="co-text-gray" style="font-size: 14px; margin: 20px 5px;text-align: justify;"><?= $organizer['shortDescription'] ?></p>
            <?php endif; ?> 

            <?php if (isset($organizer['type'])): ?>
                <div class="col-lg-12" style="margin-top: 10px;">
                    <div style="display: flex; align-items: center;">
                        <div class="date-card">
                            <i class="fa fa-user co-text-gray" aria-hidden="true" style="font-size: 24px;"></i>
                        </div>
                        <div class="co-text-white" style="margin-left: 10px;">
                            <h5 style="text-transform: capitalize;"><?= $organizer['type'] ?></h5>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($organizer['address']['streetAddress'])): ?>
                <div class="col-lg-12" style="margin-top: 10px;">
                    <div style="display: flex; align-items: center;">
                        <div class="date-card">
                            <i class="fa fa-map-marker co-text-gray" aria-hidden="true" style="font-size: 24px;"></i>
                        </div>
                        <div class="co-text-white" style="margin-left: 10px;">
                            <h5 style="text-transform: capitalize;"><?= $organizer['address']['streetAddress'] ?> , <?= $organizer['address']['postalCode'] ?>  , <?= $organizer['address']['addressLocality'] ?> , <?= $organizer['address']['level1Name'] ?></h5>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
                    
            <?php if (isset($organizer['tags'])): ?>
                <div class="col-lg-12" style="margin-top: 10px;">
                    <div style="display: flex; align-items: center;">
                        <div class="date-card">
                            <i class="fa fa-hashtag co-text-gray" aria-hidden="true" style="font-size: 24px;"></i>
                        </div>
                        <div class="co-text-white" style="margin-left: 10px;">
                            <h5 style="text-transform: capitalize;">Les TAGS</h5>
                        </div>
                    </div>
                </div>
                <ul class="co-tags" style="padding: 0 40px;">
                    <br>
                    <?php foreach ($organizer['tags'] as $tag): ?>
                        <li><a href="#" class="co-tag"><?= htmlspecialchars($tag); ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>


<script>
    
    $(document).ready(function() {
        $(".co-to-close").on("click", function(){
            var idParent = $('#modal-preview-coop');
                
            if (idParent.css('display') === 'none') {
                idParent.css('display', 'block');
            } else {
                idParent.css('display', 'none');
            }
        });
    });
    
    <?php if ( !isset($params["showBackevent"]) || !$params["showBackevent"] ) : ?> 
        function retourViewEvent(){
            urlCtrl.openPreview("/view/url/costum.views.custom.coevent.preview",
                {
                    "params" : 
                        {
                            "expectedAttendeesOne": "<?= $params['expectedAttendeesOne'] ?? "" ?>",
                            "parentId" : <?= $params['parentId'] ?? "" ?> ,
                            "event": <?= json_encode($params['event'] ?? []); ?>,
                            "actors": <?= json_encode($params['actors'] ?? []); ?>,
                            "isFollow": "<?= $params['isFollow'] ?? false ?>",
                            "parentStart" : parent_start.format('YYYY-MM-DD HH:mm'), 
                            "parentEnd" : parent_end.format('YYYY-MM-DD HH:mm')
                        }
                }
            );
        }
    <?php endif; ?>

</script>