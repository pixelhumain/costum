<?php
    $params['isFollow'] = filter_var($params['isFollow'] ?? false, FILTER_VALIDATE_BOOLEAN) ;

    $auth = false  ;
    if ( Authorisation::isCostumAdmin() || Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])) || Role::isUserSuperAdminCms(Role::getRolesUserId(Yii::app()->session["userId"])) ){
        $auth = true;
    }


	$imgEventDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/event.png";

    // echo "<pre>";
    // var_dump($params['event']);die();
?>

<style>

    .d-none-co {
        display: none !important;
    }

    #modal-preview-coop:has(.co-container-drawer) {
        background: #1c1c1e;
        overflow-y: auto;
        border-left: 1px solid #444 !important;
    }
    .co-row {
        display: flex;
        flex-direction: column;
    }

    .co-d-center {
        display: flex;
        justify-content: center;
    }

    .co-d-between {
        display: flex;
        justify-content: space-between;
    }

    .co-d-end {
        display: flex;
        justify-content: end;
    }

    .co-img-large {
        width: 250px;
        height: 200px;
        border-radius: 10px
    }

    .co-p-4 {
        padding: 20px
    }

    .co-text-white {
        color: white;
    }

    .co-text-gray {
        color: #938f8f;
    }

    .date-card {
        width: 50px;
        height: 50px;
        background-color: #333;
        color: white;
        border-radius: 10px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        font-family: Arial, sans-serif;
    }

    .month {
        font-size: 12px;
        font-weight: bold;
        margin-top: 5px
    }

    .day {
        font-size: 20px;
        font-weight: bold;
    }

    .co-title-evt {
        font-size: 15px;
        font-weight: bold;
    }


    .co-card {
        background-color: #333;
        width: 100%;
        color: white;
        border-radius: 10px;
        padding: 5px;
        font-family: Arial, sans-serif;
        box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.3);
    }

    .co-card-title {
        font-size: 14px;
        font-weight: bold;
        opacity: 0.7;
        margin-bottom: 10px;
        padding: 6px 2px;
    }

    .co-card-message {
        font-size: 16px;
        margin-bottom: 20px;
        line-height: 1.4;
    }

    .co-user-info {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
    }

    .co-avatar {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-color: #f28b82;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 20px;
        margin-right: 10px;
    }

    .co-user-details {
        font-size: 16px;
    }

    .co-username {
        font-weight: bold;
    }

    .co-email {
        font-size: 14px;
        color: #aaa;
    }

    .co-register-button {
        width: 100%;
        padding: 10px 0;
        background-color: white;
        color: #333;
        font-size: 16px;
        font-weight: bold;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .co-card-body {
        padding: 10px;
        background: #1c1c1e;
    }

    .co-register-button:hover {
        background-color: #ddd;
    }

    .co-d-flex{
        display: flex;
    }

    .co-list-participate{
        border: 2px solid #333;
        padding: 10px;
        border-radius: 5px;
    }

    .co-img-user-list {
        width: 100%;
        height: 100%;
        border-radius: 50%;
        display: inline-block;
    }

    .disabled-co{
        cursor: not-allowed !important;
    }

    @media only screen and (max-width: 600px) {
        .co-d-flex{
            display: block;
        }    
    }

    .co-btngroup {
        line-height: 1;
        white-space: nowrap;
        display: inline;
        position: absolute;
        right: 10px;
        top: 10px;
    }
    
    .co-btn-close {
        padding: 10px;
        border: 1px solid #444;
        background: transparent;
        color: white;
    }
    
    .co-btn {
        padding: 10px;
        border: 1px solid #444;
        background: transparent;
        color: white;
    }

    .co-btn:first-child {
        border-left-width: 2px;
        border-radius: 2em 0 0 2em;
    }

    .co-btn.co-to-close {
        border-radius: 0 !important;
    }

    .co-btn-with-line {
        border: none; 
        padding-left: 0; 
        background: transparent;
        text-decoration-line: underline;
    }
    
    .co-btn-with-line:hover {
        color: #c7d301;
    }


    .co-twelve h5 {
        font-size: 28px;
        font-weight: 500;
        letter-spacing: 0;
        padding-bottom: 10px;
        position: relative;
    }
    .co-twelve h5:before {
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        height: 5px;
        width: 55px;
        background-color: #c7d301;
    }
    .co-twelve h5:after {
        content: "";
        position: absolute;
        left: 0;
        bottom: 2px;
        height: 1px;
        width: 95%;
        max-width: 255px;
        background-color: #c7d301;
    }
    
    .co-to-close {
        background: #444;
    }
    .co-to-close:hover {
        background: #878585;
    }
    
    .co-to-delete {
        background: red;
    }
    .co-to-delete:hover {
        background: #9b3d3d;
    }

    .edit-event-button {
		background-color: #3949AB;
		color: #ffffff;
		border: none;
		border-radius: 5px;
		padding: 10px;
		cursor: pointer;
		align-self: flex-start;
	}

    .btn-unfollow-me {
        position: absolute;
        right: 0px;
        top: 10px;
        border: none;
        color: red;
        background: transparent;
        font-size: 25px;
    }

</style>

<div class="co-container-drawer">
    <div class="co-row">
        <div class="col-lg-12 co-d-end" style="padding: 10px 0;">
            <div>
                <?php if (( !isset($params["showEdit"]) || !$params["showEdit"]) && $auth): ?>
                    <button class="co-btn co-to-edit" onclick="eventEdit(this)">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </button>
                    <button class="co-btn co-to-delete" onclick="eventDelete(this)" data-id="<?= $params['event']['id'] ?>">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                <?php endif; ?>
                <button class="co-btn co-to-close" onclick="toClosePreview()"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
        </div>
        co-btn
        <div class="col-lg-12 co-d-center">
            <?php if (isset($params['event'])): ?>
                <img src="<?= $params['event']['profile'] ?? $params['event']['profilImageUrl'] ?? "" ?>"  
                    onerror="this.src='<?= $imgEventDefault ?>';" 
                    alt="" srcset="" 
                    class="co-img-large costum-img-preview-detail costum-img-preview" 
                    style="width: 250px; height: 200px; object-fit: cover;">

            <?php endif; ?>
        </div>
        <div class="col-lg-12 co-p-4">
            <?php if (isset($params['event'])): ?>
                <div class="co-twelve"  style="margin-bottom: 20px;">
                    <h5 class="co-text-white co-title-evt"><?= $params['event']['name'] ?></h5>
                </div>
            <?php endif; ?>
            <?php if (!empty($params['actors']) && is_array($params['actors'])): ?>
                <span class="co-text-gray" style="font-size: 14px;">
                    <i class="fa fa-user" aria-hidden="true"></i> <?= Yii::t("cms","Organized by") ?>
                    <?php foreach ($params['actors'] as $actor): ?>
                        <button onclick="open_preview_organizer(this)" 
                                data-id="<?= htmlspecialchars($actor['id'] ?? "" )  ?>" 
                                data-type="<?= htmlspecialchars($actor['type'] ?? "" )  ?>" 
                                class="co-btn-with-line">
                            <img src="<?= ($actor['image'] ?? "") ? $actor['image'] : Yii::app()->getModule("co2")->assetsUrl . "/images/thumbnail-default.jpg" ?>" 
                                alt="" style="width: 20px; height: 20px; border-radius: 50%;">
                            <?= $actor['name'] ?? "" ?>
                        </button>
                        <?= next($params['actors']) ? ', ' : '' ?>
                    <?php endforeach; ?>
                </span>
            <?php endif; ?>


            <?php if (isset($params['event']['short_description'])): ?>
                <p class="co-text-gray" style="font-size: 14px; margin: 5px;"><?= $params['event']['short_description'] ?></p>
            <?php endif; ?>

            <?php if (isset($params['event'])): ?> 
                <div class="co-row" style="margin: 20px 0;">
                <?php
                    setlocale(LC_TIME, 'fr_FR.utf8', 'fra');

                    // Récupération des dates
                    $startDateStr = $params['event']['startDate'] ?? $params['event']['start_date'] ?? "";
                    $endDateStr = $params['event']['endDate'] ?? $params['event']['end_date'] ?? "";

                    $dateAujourdhui = new DateTime();
                    $dateAujourdhui->setTime(0, 0, 0); // Normalisation de la date d'aujourd'hui

                    $startDate = null;
                    $endDate = null;

                    // Conversion de la date de début
                    if (!empty($startDateStr)) {
                        if (strpos($startDateStr, 'T') !== false) {
                            $startDate = new DateTime($startDateStr); // Format ISO 8601
                        } else {
                            $startDate = DateTime::createFromFormat('d/m/Y H:i', $startDateStr); // Format DD/MM/YYYY HH:MM
                        }
                    }

                    // Conversion de la date de fin
                    if (!empty($endDateStr)) {
                        if (strpos($endDateStr, 'T') !== false) {
                            $endDate = new DateTime($endDateStr); // Format ISO 8601
                        } else {
                            $endDate = DateTime::createFromFormat('d/m/Y H:i', $endDateStr); // Format DD/MM/YYYY HH:MM
                        }
                    }

                    // Vérification si l'événement est actif
                    $isEventActive = $endDate && $endDate >= $dateAujourdhui;

                    // Formatage des dates pour l'affichage
                    $formattedDate = $startDate ? strftime('%A %e %B %Y', $startDate->getTimestamp()) : "";
                    $formattedDateFin = $endDate ? strftime('%A %e %B %Y', $endDate->getTimestamp()) : "";
                    $heureDebut = $startDate ? $startDate->format('H:i') : "";
                    $heureFin = $endDate ? $endDate->format('H:i') : "";

                    // Gestion du bouton d'inscription
                    if (isset($params['event']['id'])) {
                        $isFollowed = filter_var($params['isFollow'], FILTER_VALIDATE_BOOLEAN);
                        $buttonClass = 'co-register-button';
                        $valuetext = $isFollowed 
                            ? Yii::t("cms", "Thank you for registering! We're delighted to count you among the event's participants.")
                            : Yii::t("cms", "Welcome to the event! To join the event, please register below.");
                    }
                ?>
                    <div class="col-lg-12">
                        <div style="display: flex; align-items: center;">
                            <div class="date-card">
                                <div class="month co-text-white" style="text-transform: uppercase;"> <?= isset($startDate) && $startDate instanceof DateTime ? $startDate->format('M') : "" ?> </div>
                                <div class="day co-text-gray"> <?= isset($startDate) && $startDate instanceof DateTime ? $startDate->format('d') : "" ?> </div>
                            </div>
                            <div class="co-text-white" style="margin-left: 10px;">
                                <h5 style="text-transform: capitalize;"><?= @$formattedDate  ?> - <?= @$formattedDateFin  ?></h5>
                                <small class="co-text-gray"><?= @$heureDebut; ?> - <?= @$heureFin; ?> </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div style="display: flex; align-items: center;">
                            <div class="date-card">
                                <i class="fa fa-map-marker co-text-gray" aria-hidden="true" style="font-size: 24px;"></i>
                            </div>
                            <div class="co-text-white" style="margin-left: 10px;">
                                <h5 style="text-transform: capitalize;"><?= @$params['event']['address']['streetAddress'] ?> <?= @$params['event']['address']['level3Name'] ?></h5>
                                <small class="co-text-gray"><?= @$params['event']['address']['addressLocality'] ?>, <?= @$params['event']['address']['level1Name'] ?> </small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 20px;">
                        <div class="row co-d-flex">
                            <?php if (isset($params['isFollow']) && $params['isFollow'] == false): ?>
                                <div class="col-lg-12 add-hidden <?php echo ($params['isFollow'] === true) ? 'd-none-co' : ''; ?>">
                                    <div class="co-card">
                                        <div class="co-card-title"><?php echo Yii::t("common", "Registration") ?></div>
                                        <div class="co-card-body">
                                            <div class="co-card-message">
                                                <?php
                                                    $dateFinStr = $params['event']['endDate'] ?? $params['event']['end_date'] ?? "";
                                                    $dateAujourdhui = new DateTime();
                                                    $dateAujourdhui->setTime(0, 0, 0);
                                                    $dateFin = null;

                                                    if (!empty($dateFinStr)) {
                                                        if (strpos($dateFinStr, 'T') !== false) {
                                                            $dateFin = new DateTime($dateFinStr);
                                                        } else {
                                                            $dateFin = DateTime::createFromFormat('d/m/Y H:i', $dateFinStr);
                                                        }
                                                    }
                                                if ($dateFin && $dateFin >= $dateAujourdhui): ?>
                                                    <?= $valuetext ?>
                                                <?php else: ?>
                                                    <?= Yii::t("cms", "The event has already ended.") ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="co-user-info">
                                                <div class="co-avatar" style="background: transparent !important;"></div>
                                                <div class="co-user-details">
                                                    <div class="co-username" style="color: transparent;"></div>
                                                    <div class="co-email" style="color: transparent;"></div>
                                                </div>
                                            </div>
                                            <?php
                                                $dateFinStr = $params['event']['endDate'] ?? $params['event']['end_date'] ?? "";
                                                $dateAujourdhui = new DateTime();
                                                $dateAujourdhui->setTime(0, 0, 0);
                                                $dateFin = null;

                                                if (!empty($dateFinStr)) {
                                                    if (strpos($dateFinStr, 'T') !== false) {
                                                        $dateFin = new DateTime($dateFinStr);
                                                    } else {
                                                        $dateFin = DateTime::createFromFormat('d/m/Y H:i', $dateFinStr);
                                                    }
                                                }

                                                if ($dateFin && $dateFin >= $dateAujourdhui): ?>
                                                    <button class="<?= $buttonClass ?>" onclick="eventParticipate(this)" 
                                                        data-program="<?= @$params['event']['id'] ?>"
                                                        data-expected-attendees="<?= $params["expectedAttendeesOne"] ?? "" ?>"
                                                        data-toggle="tooltip" data-placement="left" data-original-title="<?= $tip ?? '' ?>" 
                                                        data-ownerlink="<?= $params['isFollow'] ? "unfollow" : "follow" ?>" 
                                                        data-id="<?= @$params['event']['id'] ?>" 
                                                        data-type="events" data-name="<?= @$params['event']['name'] ?>" 
                                                        data-isFollowed="<?= $params['isFollow'] ?>">
                                                        <?= $params['isFollow'] ? Yii::t("cms","Already registered") : Yii::t("cms","One-click registration") ?>
                                                    </button>
                                                <?php endif; ?>

                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-lg-12">
                                <div class="co-card">
                                    <div class="co-card-title"><?php echo Yii::t("common", "Attendees") ?></div>

                                    <div class="co-card-body co-scroll list-attendees" id="list-attendees" style="height:190px;overflow-y: auto;">
                                        <?php if(isset($params["event"]["links"]["attendees"])){
                                            foreach($params["event"]["links"]["attendees"] as $k => $va){
                                                ?>
                                                    <div  id="my-list-attendees" class="co-user-info co-list-participate <?= $k?>" style="position: relative;"></div>
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="modal fade modal-boostrap-create-event" id="openModalEditEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;overflow-y: auto;">
	<div class="modal-dialog modal-personnalise modal-dialog-centered co-resposnive" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?= Yii::t('cms', 'Save') ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body col-xs-12">
				<div class="row">
					<div class="event-container  col-xs-12 col-md-4 col-lg-4">
						<div class="event-image">
							<div id="editevent-image-input"></div>
						</div>
					</div>
					<div class="col-xs-12 col-md-8 col-lg-8 event-details"></div>
				</div>
			</div>
			<div class="modal-footer col-xs-12">
				<button class="edit-event-button" data-id="<?= $params['event']['id'] ?>" ><?= Yii::t('cms', 'Save') ?></button>
			</div>
		</div>
	</div>
</div>
<script>
    var attendees = <?= json_encode($params["event"]["links"]["attendees"] ?? [])?>;
    var event_param = <?= json_encode($params["event"] ?? [])?>;
    var actor_param = <?= json_encode($params["actors"] ?? [])?>;
    var expectedAttendeesOne_param = <?= $params["expectedAttendeesOne"] ?? "" ?>;
    var isFollow_param = JSON.stringify(<?= $params['isFollow'] ?>);
    var parentId_param = JSON.stringify("<?= $params['parentId'] ?>");
    var parentStart = <?= json_encode($params["parentStart"] ?? "")?>;
    var parentEnd = <?= json_encode($params["parentStart"] ?? "")?>;
    
    if (Object.keys(attendees).length > 0) {
        $.each(attendees,function(k,v){
            if ( typeof v === 'object') v = v.type;
            ajaxPost(
                null,
                baseUrl+"/"+moduleId+"/element/get/type/"+v+"/id/"+k,
                {},
                function(data) {
                    var dt = data.map
                    var dt_img = dt.profilImageUrl
                    var idUser = "<?= Yii::app()->session["userId"] ?>"
                    var defaultImageUrl = "<?= Yii::app()->getModule('co2')->assetsUrl . '/images/thumbnail-default.jpg' ?>";
                    var str = `
                            <div class="co-avatar">
                                <img src="${dt_img ? dt_img : defaultImageUrl}"
                                    alt="img-pdp" class="co-img-user-list" />
                            </div>
                            <div class="${data.map._id.$id}"></div>
                            <div class="co-user-details">
                                <div class="co-username">${data.map.name}</div>
                                <div class="co-email">${data.map.email ? data.map.email : '' }</div>
                                ${
                                    dt._id.$id === idUser
                                        ? `<button class="btn-unfollow-me"
                                                onclick="eventParticipate(this)" 
                                                data-program="<?= htmlspecialchars($params['event']['id'] ?? '', ENT_QUOTES, 'UTF-8'); ?>"
                                                data-expected-attendees="<?= htmlspecialchars($params['expectedAttendeesOne'] ?? '', ENT_QUOTES, 'UTF-8'); ?>"
                                                data-toggle="tooltip" 
                                                data-placement="left" 
                                                data-original-title="<?= htmlspecialchars($tip ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-attendees="<?= htmlspecialchars($attendees ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-ownerlink="unfollow"
                                                data-id="<?= htmlspecialchars($params['event']['id'] ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-type="events" 
                                                data-name="<?= htmlspecialchars($params['event']['name'] ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-isFollowed="<?= $params['isFollow']; ?>">
                                                <i class="fa fa-close"></i>
                                            </button>`
                                        : ''
                                }
                            </div>
                    `
                    $("#my-list-attendees."+k).html(str)
                }, null, null, {
                    async: false
            });

        })
    }

    $(document).ready(function(){
        let images = [];
        let currentIndex = 0;
        
        $(".costum-img-preview").each(function () {
            mylog.log('tonga ato', $(this).attr("src"));
            images.push($(this).attr("src"));
        });
        mylog.log('image initial', $(".costum-img-preview"))
    
        mylog.log('imagessss', images)
    
        $(".costum-img-preview").click(function () {
            currentIndex = images.indexOf($(this).attr("src"));
            showImage(currentIndex);
            $('#costum-image-viewer').show();
        });
    
        $("#costum-image-viewer .co-close").click(function () {
            $('#costum-image-viewer').hide();
        });
    
        // Afficher l'image à un index donné
        function showImage(index) {
            if (index >= 0 && index < images.length) {
                $("#id-full-image").attr("src", images[index]);
            }
        }
    

        $("#costum-image-viewer .co-prev").click(function () {
            navigateLeft();
        });

        $("#costum-image-viewer .co-next").click(function () {
            navigateRight();
        });

        $(document).keydown(function (e) {
            if ($('#costum-image-viewer').is(":visible")) {
                switch (e.key) {
                    case "ArrowLeft":
                        navigateLeft();
                        break;
                    case "ArrowRight":
                        navigateRight();
                        break;
                    case "Escape":
                        $('#costum-image-viewer').hide();
                        break;
                }
            }
        });

        function navigateLeft() {
            currentIndex = (currentIndex > 0) ? currentIndex - 1 : images.length - 1;
            showImage(currentIndex);
        }

        function navigateRight() {
            currentIndex = (currentIndex < images.length - 1) ? currentIndex + 1 : 0;
            showImage(currentIndex);
        }
    });


    function open_preview_organizer(event){
        var idOrganizer = $(event).data('id');
        var typeOrganizer = $(event).data('type');
        urlCtrl.openPreview("/view/url/costum.views.custom.coevent.previeworga", 
            {"params" : {
                    "id" : idOrganizer,
                    "type": typeOrganizer,
                    "expectedAttendeesOne": expectedAttendeesOne_param,
                    "parentId": parentId_param,
                    "event": event_param,
                    "actors": actor_param,
                    "isFollow": isFollow_param,
                    "parentStart" : parent_start.format('YYYY-MM-DD HH:mm'), 
                    "parentEnd" : parent_end.format('YYYY-MM-DD HH:mm')
                }
            });
    }

    function eventDelete(dom){
        var that = dom
        var self = $(that)
        var  idEvent = self.data('id');
        $.confirm({
            title : trad.delete,
            content : 'Procéder à la suppression de l\'événement',
            buttons : {
                confirm : {
                    text : 'Confirmer',
                    action() {
                        var url = baseUrl + '/' + moduleId + '/element/delete/id/' + self.data('id') + '/type/events';
                        var request_params = {reasons : 'none'};
                        ajaxPost(null, url, request_params, function (__data) {
                            if (__data.result) {
                                toastr.success(__data.msg);
                                refreshBlock("<?= $params['parentId'] ?>", ".cmsbuilder-block[data-id='<?= $params['parentId'] ?>']");
                                $("#modal-preview-coop").hide()
                            } else {
                                toastr.error(__data.msg);
                            }
                        });
                    }
                },
                cancel : {
                    text : 'Annuler'
                }
            }
        })        
    }

    function eventEdit(dom){
        var defaultData = <?= json_encode($params["event"]) ?>;
        var parentStart = <?= json_encode($params["parentStart"]) ?>;
        var parentEnd =  <?= json_encode($params["parentEnd"]) ?>;
        var startDate = moment(defaultData["start_date"] ?? "");
        var defaultStartDate = startDate.format("DD/MM/YYYY HH:mm");
        var endDate = moment(defaultData["end_date"] ?? "");
        var defaultEndDate = endDate.format("DD/MM/YYYY HH:mm");

        var allAdresses = defaultData["addresses"] ?? [];
        var address = defaultData["address"] ?? {};
        var centralAdresse = {
            "center" : true,
            "address" : address
        }

        if ( typeof defaultData.geo !== "undefined") centralAdresse.geo = defaultData.geo ;
        if ( typeof defaultData.geoPosition !== "undefined") centralAdresse.geoPosition = defaultData.geoPosition;
        allAdresses.unshift(centralAdresse);
        var newData = {
            id : defaultData.id,
            collection: "events",
            name : defaultData.name,
            startDate : defaultData["start_date"],
            endDate : defaultData["end_date"],
            type : defaultData.type,
            timeZone : moment.tz.guess()
        };

        uploadObj.set("events",defaultData.id);
        uploadObj.initUploader();

        new CoInput({
						container: "#editevent-image-input",
						inputs: [
							{
								type: "inputFileImage",
								options: {
									name: "image",
									label: "Image",
									collection: "documents",
									defaultValue: "",
									canSelectMultipleImage : false,
									payload: {
										path: "logo",
									},
									icon: "chevron-down",
									class: "logoUploader inputContainerCoEvent",
									contentKey: "profil",
									uploadOnly: true,
									uploaderInitialized : true,
									endPoint: "",
									domElement: "profil",
									defaultValue: defaultData["profile"] ?? ""
								},
							},
							{
								type : "dateTimePicker",
								options: {
									name : "startDate",
									label: tradCms.startDate +" *",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent newStartDate",
                                    defaultValue: defaultStartDate ?? ""
								}
							},
							{
								type : "dateTimePicker",
								options: {
									name : "endDate",
									label: tradCms.endDate +" *",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent newEndDate",
                                    defaultValue: defaultEndDate ?? ""
								}
							},
							{
								type : "tags",
								options : {
									name : "tags",
									label : tradDynForm.keyWords,
									options : [""],
									defaultValue : "",
									class: "inputContainerCoEvent",
									icon : "chevron-down",
                                    defaultValue: defaultData["tags"] ?? []
								}
							},
							{
								type : "inputSimple",
								options: {
									name : "email",
									label : "E-mail",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent",
                                    defaultValue: defaultData["email"] ?? ""
								}
							},
							{
								type : "inputSimple",
								options: {
									name : "url",
									label : "URL",
									defaultValue : "",
									icon : "chevron-down",
									class: "inputContainerCoEvent",
                                    defaultValue: defaultData["url"] ?? ""
								}
							},
							
						],
						onchange: function(name, value, payload) {				
							// Handle changes here
							if ( name === "image") {
								jsonHelper.setValueByPath(newData, "profilImageUrl", value);
							} else if ( name === "startDate"){
								var value = moment(value, "DD/MM/YYYY HH:mm");
								var dateDebutParent = moment(parentStart);
								var dateDebutConvertie = value.tz(newData.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
								if (moment(dateDebutConvertie).isSameOrBefore(dateDebutParent)){
									$(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i>${tradCms.mustBeAfter+" "+dateDebutParent.format("DD/MM/YYYY HH:mm")}.</span>`);
							    } else {
									$(".warningStartDate").remove();
									jsonHelper.setValueByPath(newData, name,dateDebutConvertie);
								}
							} else if ( name === "endDate"){
								var value = moment(value, "DD/MM/YYYY HH:mm");
								var dateFinParent = moment(parentEnd);
								var dateEndConvertie = value.tz(newData.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
								if ( moment(dateEndConvertie).isSameOrBefore(newData.startDate) || moment(dateEndConvertie).isAfter(dateFinParent)){
									$(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i>${ tradCms.mustBeBetween+" "+moment(newEvent.startDate).format("DD/MM/YYYY HH:mm")+" "+tradCms.and+" "+ dateFinParent.format("DD/MM/YYYY HH:mm")}.</span>`);
								} else {
									$(".warningEndDate").remove();
									jsonHelper.setValueByPath(newData, name , dateEndConvertie );
								}
							} else if ( name === "tags"){
								jsonHelper.setValueByPath(newData, name,value.split(','));
							} else {
								jsonHelper.setValueByPath(newData, name,value);
							}
						},
						onblur : function(name , value, payload){
							
						}
				});

        new CoInput({
                container: ".event-details",
                inputs: [
                    {
                        type : "inputSimple",
                        options: {
                            name : "name",
                            label : tradCms.eventName +" *",
                            defaultValue : "",
                            icon : "chevron-down",
                            class: "inputContainerCoEvent",
                            defaultValue : defaultData["name"] ?? ""
                        }
                    },
                    {
								type : "select",
								options: {
									name : "type",
									label : tradDynForm.eventTypes +" *",
									options :  Object.keys(eventTypes).map(function(value) {
										return {
											label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
											value : value
										}
									}),
                                    icon : "chevron-down",
									class: "inputContainerCoEvent",
                                    defaultValue: defaultData["type"] ?? ""
								}
                    },
                    {
                        type : "finders",
                        options: {
                            name : "organizer",
                            label: tradDynForm.whoorganizedevent,
                            initType: ["projects", "organizations"],
                            multiple: true,
                            initMe: false,
                            icon : "chevron-down",
                            openSearch: true,
                            initBySearch: true,
                            class: "inputContainerCoEvent",
                            defaultValue: defaultData["organizer"] ?? []
                        }
                    },
                    {
                        type : "formLocality",
                        options: {
                            name : "formLocality",
                            label: tradCms.place,
                            icon : "chevron-down",
                            class: "inputContainerCoEvent",
                            defaultValue: allAdresses,
                        }
                    },
                    {
                        type : "textarea",
                        options: {
                            name : "shortDescription",
                            label : tradCms.shortDescription,
                            defaultValue :"",
                            icon : "chevron-down",
                            class: "inputContainerCoEvent",
                            defaultValue : defaultData["short_description"] ?? ""
                        },
                    },
                ],
                onblur: function(name, value, payload) {
                    if ( name === "formLocality"){
                            var address = [];
                            var geo = [];
                            var geoPosition = [];
                            var addresses = [];
                            value.forEach((location, index) => {
                                if (location.center) {
                                    address = location.address;
                                    geo = location.geo;
                                    geoPosition = location.geoPosition;
                                } else {
                                    addresses.push(location);
                                }
                            });
                            jsonHelper.setValueByPath(newData, "address", address);
                            jsonHelper.setValueByPath(newData, "geo", geo);
                            jsonHelper.setValueByPath(newData, "geoPosition", geoPosition);
                            jsonHelper.setValueByPath(newData, "addresses", addresses);
                    } else {
                        jsonHelper.setValueByPath(newData, name, value);
                    } 
                }
        });
                
        $("#openModalEditEvent").modal("show");

        $(".edit-event-button").off("click").on("click", function(){
            ajaxPost(
					null,
					baseUrl+"/co2/element/save",
					newData, 
					function (response) {  
						if (response.result){
                            $("#openModalCreateEvent").modal("hide");
                            refreshBlock("<?= $params['parentId'] ?>", ".cmsbuilder-block[data-id='<?= $params['parentId'] ?>']");
                            toastr.success(tradCms.editionSucces);
						}                  
					}
				);
        });
    }

    function eventParticipate(dom){
        var toHide = $(".add-hidden");
        toHide.toggleClass('d-none-co');
        var that = dom;
        thisElement = $(that);
        var isExpAttFull = (thisElement.data("expected-attendees") > 0 && Object.keys(thisElement.data("attendees")).length == eventData.expectedAttendees);
        var action = thisElement.data("ownerlink")
        if (!isExpAttFull) {
            if (userId) {
                subscribeToEventOnPreview(thisElement,userId, function() {
                    updateAttendeesList(userId,action);
                });
            } else {
                $("#modalLogin").on('show.bs.modal', function(e) {
                    if ($("#infoBL").length == 0) {
                        $("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                            Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                            <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                        </div>`);
                    }
                });
                $("#modalLogin").on('hide.bs.modal', function(e) {
                    $("#infoBL").remove();
                });
                toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
                Login.openLogin();
            }
        } else {
            if (userId)
            subscribeToEventOnPreview(thisElement);
        }

        // if($(this).hasClass("changed-co")){
        // refreshBlock("<?= $params['parentId'] ?>", ".cmsbuilder-block[data-id='<?= $params['parentId'] ?>']");
        // }
    }
        
    function updateAttendeesList(userId =  null,action="follow") {
        if(action == "follow"){
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/element/get/type/citoyens/id/" +userId,
                {},
                function(data) {
                    var dt = data.map;
                    var dt_img = dt.profilImageUrl;
                    var defaultImageUrl = "<?= Yii::app()->getModule('co2')->assetsUrl . '/images/thumbnail-default.jpg' ?>";
                    
                    var str = `<div id="my-list-attendees" class="co-user-info co-list-participate ${userId}" style="position: relative">
                            <div class="co-avatar">
                                <img src="${dt_img ? dt_img : defaultImageUrl}"
                                    alt="img-pdp" class="co-img-user-list" />
                            </div>
                            <div class="${data.map._id.$id}"></div>
                            <div class="co-user-details">
                                <div class="co-username">${data.map.name}</div>
                                <div class="co-email">${data.map.email ? data.map.email : '' }</div>
                                ${
                                    dt._id.$id === userId
                                        ? `<button class="btn-unfollow-me"
                                                onclick="eventParticipate(this)" 
                                                data-program="<?= htmlspecialchars($params['event']['id'] ?? '', ENT_QUOTES, 'UTF-8'); ?>"
                                                data-expected-attendees="<?= htmlspecialchars($params['expectedAttendeesOne'] ?? '', ENT_QUOTES, 'UTF-8'); ?>"
                                                data-toggle="tooltip" 
                                                data-placement="left" 
                                                data-original-title="<?= htmlspecialchars($tip ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-attendees="<?= htmlspecialchars($attendees ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-ownerlink="unfollow" 
                                                data-id="<?= htmlspecialchars($params['event']['id'] ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-type="events" 
                                                data-name="<?= htmlspecialchars($params['event']['name'] ?? '', ENT_QUOTES, 'UTF-8'); ?>" 
                                                data-isFollowed="<?= $params['isFollow']; ?>">
                                                <i class="fa fa-close"></i>
                                            </button>`
                                        : ''
                                }
                            </div>
                            </div>
                    `;
                    $("#list-attendees" ).append(str);
                    $(".co-btn.co-to-close").addClass("changed-co");
                }, 
                null, 
                null, 
                { async: false }
            );
        }else{
            $("#list-attendees ."+userId ).remove();
        }
    }

    function subscribeToEventOnPreview(eventSource, theUserId = null, on_success) {
		var labelLink = "";
		var parentId = eventSource.data("id");
		var parentType = eventSource.data("type");
		var childId = (theUserId) ? theUserId : userId;
		var childType = "citoyens";
		var name = eventSource.data("name");
		var id = eventSource.data("id");
		//traduction du type pour le floopDrawer
		eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
		var connectType = (parentType == "events") ? "connect" : "follow";

		if (eventSource.data("ownerlink") == "follow") {
			callback = function() {
				labelLink = (parentType == "events") ? "DÉJÀ INSCRIT(E)" : trad.alreadyFollow;
				if (eventSource.hasClass("btn-add-to-directory")) {
					labelLink = "";
				}
				$(`[data-id=${id}]`).each(function() {
                    <?php if ( !isset($params["showEdit"]) || !$params["showEdit"]) : ?>
                        $(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
                        $(this).data("ownerlink", "unfollow");
                        $(this).data("original-title", labelLink);
                    <?php endif; ?>
					var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
					intCounter = parseInt(strCounter);
					newCounter = intCounter + 1;
					$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
				});
				if (typeof on_success === 'function')
					on_success();
			}
			request_join_eventPreview({
				user: childId,
				event: id
			}).then();
			// return ;
			if (parentType == "events")
				links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
			else
				links.follow(parentType, parentId, childId, childType, callback);
		} else {
			//eventSource.data("ownerlink")=="unfollow"
			connectType = (parentType == "events") ? "attendees" : "followers";
			callback = function() {
				labelLink = (parentType == "events") ? "S'inscrire" : "Déjà inscrite";
				if (eventSource.hasClass("btn-add-to-directory")) {
					labelLink = "";
				}
				$(`[data-id=${id}]`).each(function() {
                    <?php if ( !isset($params["showEdit"]) || !$params["showEdit"]) : ?>
                        $(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
                        $(this).data("ownerlink", "follow");
                        $(this).data("original-title", labelLink);
                        $(this).removeClass("text-white");
                    <?php endif; ?>
					var strCounter = $(this).siblings(".rolesLabel.attendeesCounter").find("span").html();
					intCounter = parseInt(strCounter);
					newCounter = intCounter - 1;
					$(this).siblings(".rolesLabel.attendeesCounter").find("span").html(newCounter.toString());
				});
				if (typeof on_success === 'function'){
					on_success();}
			};
			links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
		}
	}

    function request_join_eventPreview(args) {
		var url = baseUrl + '/costum/coevent/join_event';
		return new Promise(function(resolve, reject) {
			ajaxPost(null, url, args, resolve, reject);
		});
	}

    function toClosePreview(){

        if($(this).hasClass("changed-co")){
            refreshBlock("<?= $params['parentId'] ?>", ".cmsbuilder-block[data-id='<?= $params['parentId'] ?>']");
        }

        var idParent = $('#modal-preview-coop');
            
        if (idParent.css('display') === 'none') {
            idParent.css('display', 'block');
        } else {
            idParent.css('display', 'none');
        }
    }

</script>