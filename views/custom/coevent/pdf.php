<?php
$structField = "structags";
$cssAnsScriptFilesModule = array(
	'/plugins/font-awesome/css/font-awesome.min.css',
	'/plugins/font-awesome-custom/css/font-awesome.css'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl); ?>
<style>
	.label-fiche-title{
        color :  #275283;
        font-weight : bold;

    }    
	.text-blue-fiche{
        color: #000c66;
        font-weight : 700;
    }
	hr.ffff{
		border-top: 1px dotted red;
	}
</style>
<div style="background-color:#f8f8f8;box-shadow:4px 3px 4px 1px #c33c66;">
	<?php if (!empty($context['banner'])) : ?>
		<img src="<?= $context['banner'] ?>" alt="banniere" style="width:800px; height: 200px; object-fit:contain">
	<?php
	endif;
	?>
	<table   cellspacing="2" cellpadding="2" >
		<tr>
			<td colspan="3" >
				<p><a href="<?= $context["url"] ?>" class="label label-fiche-title" style="font-size:30px; text-decoration: none;" ><?= $context["name"] ?></a></p> 
			</td>
			<td colspan="1" style="text-align:right;float:right">
				<?php if (!empty($context['logo'])) : ?>
					<img src="<?= $context['logo'] ?>" alt="logo" style="height: 80px; width: auto;object-fit: contain;object-position: left;">
				<?php endif ?>
			</td>
		</tr>
	</table>
	<?php if(isset($context["shortDescription"]) && $context["shortDescription"]!= ""){?>
		<p class ="text-description" style="text-align:justify;  font-size:14px"><?= $context["shortDescription"] ?></p>
	<?php } ?>
	<?php if(isset($context["description"])  && $context["description"]!= ""){?>
		<p class ="text-description" style="text-align:justify;  font-size:14px"><?= $context["description"] ?></p>
	<?php } ?>
	<p class="label label-fiche-title " style="text-align:center; font-size:25px;text-decoration: underline;" >Le programme</p>
	
	<?php
	$count = 0;
	foreach ($subevents as $subevent)
	{ 
		$count++; 
		if($count != 1 && $count <= count($subevents) && $subevent["background"] == ''){?>  
			<img style="width:800px; height: 30px; object-fit:contain" src="<?= Yii::app()->getModule("costum")->assetsUrl."/images/coevent/trait_blue.png" ?>">
		<?php } 
		if (!empty($subevent["background"])): ?>
			<img style="width:800px; height: 200px; object-fit:contain" src="<?= $subevent["background"] ?>">
		<?php endif; ?>
		<table   cellspacing="2" cellpadding="2" >
			<tr>
				<?php 				 
				if(isset($subevent['logo']) && $subevent['logo'] != '') 
					$src = $subevent['logo'];
				else 
					$src =  Yii::app()->getModule("costum")->assetsUrl."/images/coevent/logo_event.png";
				?>
				<td colspan="1" >						
					<img src="<?= $src ?>" style="height: 80px; width: auto;object-fit: contain;object-position: center;text-align:center;float:center" /> 
				</td>
				<td colspan="3" style="text-align:left;float:left">
					<p class ="label label-fiche-title" style="text-align:justify;  font-size:20px; font-weight:bold;"><?= $subevent["name"] ?> </p>
					<?php
					$start_date = [
						'datetime' => $subevent['startDate'],
						'coded_date' => $subevent['startDate']->format('Y-m-d'),
						'formated_date' => $subevent['startDate']->format('d') . ' ' . Yii::t('translate', $subevent['startDate']->format('F')) . ' ' . $subevent['startDate']->format('Y'),
						'formated_hour' => $subevent['startDate']->format('H:i')
					];
					$end_date = [
						'datetime' => $subevent['endDate'],
						'coded_date' => $subevent['endDate']->format('Y-m-d'),
						'formated_date' => $subevent['endDate']->format('d') . ' ' . Yii::t('translate', $subevent['endDate']->format('F')) . ' ' . $subevent['endDate']->format('Y'),
						'formated_hour' => $subevent['endDate']->format('H:i')
					];
					if ($start_date['coded_date'] === $end_date['coded_date']) :
					?>
						<p class ="text-description" style="text-align:justify;  font-size:13px"> Le <span style="font-weight: bold;"> <?= $start_date['formated_date'] ?>  </span> de <span style="font-weight: bold;"><?= $start_date['formated_hour'] ?></span> à <span style="font-weight: bold;"><?= $end_date['formated_hour'] ?></span></p>
					<?php else : ?>
						<p class ="text-description" style="text-align:justify;  font-size:14px">Le <?= $start_date['formated_date'] ?> à <span style="font-family: sans-serif;"><?= $start_date['formated_hour'] ?></span> jusqu'au <?= $end_date['formated_date'] ?> à <span style="font-family: sans-serif;"><?= $end_date['formated_hour'] ?></span></p>
					<?php
					endif; ?>
				</td>
			</tr>
		</table>
		<?php if (!empty($subevent["address"])): ?>
			<img src="<?= $subevent["marker"] ?>" alt="address" style="width: 20px; height: 20px;"> <?= $subevent["address"] ?>
		<?php
		endif;
		if (!empty($subevent["shortDescription"])): ?>
			 <p class ="text-description" style="text-align:justify;  font-size:14px">  <?= $subevent["shortDescription"] ?></p>
		<?php endif;
		
		if (!empty($subevent["fullDescription"])):?>
			<p class ="text-description" style="text-align:justify;  font-size:14px">  <?= $subevent["fullDescription"] ?></p>
		<?php endif; ?> 
	<?php } ?>
</div>