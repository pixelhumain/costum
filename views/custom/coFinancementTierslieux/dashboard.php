

<?php
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/d3.min.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/sankeyaap.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/color.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/interpolate.js'], Yii::app()->request->baseUrl);
    HtmlHelper::registerCssAndScriptsFiles(['/plugins/d3/d3-v3/chromatic.js'], Yii::app()->request->baseUrl);

        $checkedId = $this->costum["contextId"] ?? "";
        $where = array("costumId" => $checkedId,"behalf"=>"true");

        $donation=PHDB::find(Crowdfunding::COLLECTION,$where);

        $nodes=[];
        $links=[];

        foreach($donation as $key=>$value){
            $source = $value["orgaName"] ?? $value["invoiceName"] ?? "Pas de financeur trouvé";
            $target = !empty($value["receiver"][key($value["receiver"])]["name"]) ? $value["receiver"][key($value["receiver"])]["name"] : "Pas de commun fléché";
            $value = !empty($value["amount"]) ? $value["amount"] : 1;

            $links[]=array("source"=>" ".$source,"target"=>$target,"value"=>$value);
            if (!in_array(["name" => " ".$source, "level" => 0],$nodes)) {
                $nodes[] = array("name"=>" ".$source ,"level" => 0);
            };
            if (!in_array(["name" => $target , "level" => 1],$nodes)) {
                $nodes[] = array("name"=>$target , "level" => 1);
            };
        }

    ?>

    <style>
        #my_dataviz svg{
          overflow:overlay;
        }
    </style>
    <div class="col-xs-12 padding-30">
        <h4 style="color:#05323e;">Flux financiers représentant les dons des structures envers les communs faisant l'objet de la campagne <a href="#adopteUnCommun" class="lbh" style="color:#8ac4c5;">Adopte Un Commun</a></h4>
        <div id="my_dataviz"></div>
    </div>    
        

    <script>
        let nodes = <?php echo (!empty($nodes) ? json_encode($nodes) : "{}" ); ?>;
        let links = <?php echo (!empty($links) ? json_encode($links) : "{}" ); ?>;
        let barColor = ["#002060ff", "#164490ff", "#4d75bcff", "#98b3e6ff", "#d5e2feff", "#008cb0ff"];

        jQuery(document).ready(function() {
            ajaxPost("#my_dataviz", baseUrl+'/graph/co/chart/',
                {
                    id : "propositionsankey",
                    data : {
                        "links": JSON.stringify(links),
                        "nodes": JSON.stringify(nodes)
                    },
                    labels : [ "Structures donatrices" ,  "Communs financés"],
                    g : 'graph.views.co.ocecoform.sankey',
                    colors : barColor,
                }
                , function(){} ,"html");
        });

        /*var nodes =  <?php echo json_encode((isset($nodes)) ? $nodes : [] ) ?>;
        var links = <?php echo json_encode((isset($links)) ? $links : [] )?>;

        var data={
            nodes : nodes,
            links : links
        };

        mylog.log("nodes",nodes);
        mylog.log("links",links);

       var margin = {top: 10, right: 10, bottom: 10, left: 10},
           width = 450 - margin.left - margin.right,
           height = 480 - margin.top - margin.bottom;    

        var svg = d3.select("#my_dataviz").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        //Color scale used
        var color = d3.scaleOrdinal().range(["#002060ff", "#164490ff", "#4d75bcff", "#98b3e6ff", "#d5e2feff", "#008cb0ff"]);


        // Set the sankey diagram properties
        var sankey = d3.sankey()
          .nodeWidth(175)
          .nodePadding(10)
          .size([width, height]);    


        // Constructs a new Sankey
        sankey
          .nodes(data.nodes)
          .links(data.links)
          .layout(0);  

        mylog.log("sankey.link",sankey.link());  

        // add in the links
        var link = svg.append("g")
          .selectAll(".link")
          .data(links)
          .enter()
          .append("path")
          .attr("class", "link")
          .attr("d", sankey.link())
          .style("stroke-width", function(d) {
            mylog.log("d link",d);
            return Math.max(1, d.dy);
          })
          .sort(function(a, b) {
            return b.dy - a.dy;
          })


        // add in the nodes
        var node = svg.append("g")
          .selectAll(".node")
          .data(nodes)
          .enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) {
            mylog.log("d node",d);
            return "translate(" + d.x +"," + d.y + ")";
          })*/

        </script>
 

