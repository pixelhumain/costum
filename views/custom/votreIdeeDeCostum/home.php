<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js',
      '/js/docs/docs.js',
      'plugins/showdown/showdown.min.js',         
    );

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl());
    $poiList = array();
    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                               "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                               "type"=>"cms") );
    } 
    $tag = array();
    ?>
<style>
.center {
    margin-left: 10%;
}
.info-cms{
    margin-top: 2rem;
    box-shadow: 0px 0px 10px 5px #f2f2f2;
    padding : 3%; 
}
@media (max-width:768px){
    .center{
        margin-left:0%;
    }
}
</style>
<?php
$params = [  "tpl" => "votreIdeeDeCostum","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
?>
<div class="container center">
<?php // var_dump($el); ?>
    <!-- INITIALISATION DU HEADER -->
    <div class="header" style="padding-top : 2rem;">
        <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/votreIdeeDeCostum/logo.png" class="img-responsive">
    </div>

    <!-- INITIALISATION DU CMS (BON POUR LA TEMPS ON VA FAIRE L'ASPECT GRAPHISMES) -->
    
    <div class="info-cms col-md-12">
    <div id="container-image"></div>

    <h2> Information sur <?= $el["name"] ?> </h2> <hr>
    <div id="container-projet">
    Description court : <br />
    <p><?= @$el["shortDescription"] ?></p>
    Description : 
    <p><?= @$el["description"]; ?></p>
    </div>
    <br />
    <a class="btn btn-default" href="#@<?= $this->costum["contextSlug"] ?>" data-hash="#page.type.<?= $this->costum["contextType"] ?>.id.<?= $this->costum["contextId"] ?>" target="_blank"><i class="fa fa-id-card-o" aria-hidden="true"></i>
 Voir votre page d'élément </a>
    <hr>
    <h2> Information concernant le costum </h2>
        <?php 
        // var_dump($id);
                    // var_dump($poiList);
                   $structField = "structags";
                //    var_dump(Poi::getPoiByStruct($poiList,"description1",$structField));
                    $p =  Poi::getPoiByStruct($poiList,"description1",$structField);
                //    var_dump($p);
                    if( count($p)!=0 ){
                       
                    ?>
                    <!-- <input type="hidden" value="<?php ?>" -->
                    <div id="container-docs" ></div>
                    <?php 
                        $edit ="update";
                    }
                    else 
                        $edit ="create";
                    
                    // echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 

                   ?>

                   		
<?php
if(Authorisation::canEdit( Yii::app()->session["userId"] , $this->costum["contextId"], $this->costum["contextType"] ))
{ ?>
	<br>
	<?php if( $edit == "create" ){ ?>
	    <a href="javascript:;"  class="createBlockBtn btn btn-primary" data-tag="description1"><i class="fa fa-pencil" aria-hidden="true"></i>
Créer la fiche projet COstum</a>

	<?php } else if ( $edit == "update") { ?>
	    
	    <a href="javascript:;"  data-id="<?php echo (string)  @$p[0]["_id"]; ?>" data-type="poi" onclick=""  class="editThisBtn btn btn-primary">Modifier mes informations</a> 

	    <a class="deleteThisBtn btn btn-danger " data-id="<?php echo (string) @$p[0]["_id"]; ?>" data-type="poi" href="javascript:;"><i class="fa fa-trash"></i></a> 
    <?php } 
}
?>
 <a href='<?= Yii::app()->createUrl("/costum/co/index/slug/".@$_GET["slug"]."/test/costumBuilder"); ?>' class="btn btn-warning" id="costumBuilder" target="_blank"><b> <i class="fa fa-cog" aria-hidden="true"></i>
Tester des costums existants </b></a>

    </div>
</div>


<div class="container center text-center" style="margin-top: 2%; margin-bottom: 2%;">
            <a class="btn btn-success" href="https://chat.communecter.org/channel/custom" target="_blank" style="font-size: 3rem;"><i class="fa fa-comments"></i> Contactez-nous</a>
</div>


<script>

var dynFormCostumIdeaCMS = {
		"beforeBuild":{
			    "removeProp" : {
			    	"image": 1,
                },
		        "properties" : {
		            "structags" : {
		                "inputType" : "hidden",
                        "value" : ["description1"]
		            },
		            "documentation" : {
                        "inputType" : "uploader",
                        "label" : "Document associé (5Mb max)",
                        "showUploadBtn" : false,
                        "docType" : "file",
                        "itemLimit" : 15,
                        "contentKey" : "file",
                        "order" : 8,
                        "domElement" : "documentationFile",
                        "placeholder" : "Le pdf",
                        "afterUploadComplete" : null,
                        "template" : "qq-template-manual-trigger",
                        "filetypes" : [
                        	"pdf","xls","xlsx","doc","docx","ppt","pptx","odt","ods","odp", "csv","png","jpg","jpeg","gif"
                        	]
                    },
                    "parent" : {
                        "inputType" : "finder",
                        "label" : "tradDynForm.whoisauthor",
                        "multiple" : true,
                        "rules" : { "required" : true, "lengthMin":[1, "parent"]}, 
                        "initType": [ "projects"],
                        "openSearch" :true
	            }
		        }
		    },
	    "onload" : {
	        "actions" : {
	            "setTitle" : "FICHE COSTUM",
	            "html" : {
	                "nametext>label" : "Nom de votre costum",
                    "infocustom" : "Détaillez-nous le costum de vos rêves",
                    "descriptiontextarea>label" : "Descrivez votre costum"
	            },
	            "presetValue" : {
	                "type" : "cms"
	            },
	            "hide" : {
                    "formLocalityformLocality" : 1,
                    "parentfinder" : 1,
	                "locationlocation" : 1,
	                "breadcrumbcustom" : 1,
	                "urlsarray" : 1,
	                "imageuploader":1
	            }
	        }
	    }
    };
    
jQuery(document).ready(function() {

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumIdeaCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,null,null,dynFormCostumIdeaCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });

    openCms("<?php echo (string) @$p[0]["_id"]; ?>"); 

    // if(notEmpty) document.getElementById("costumBuilder").href= window.location.href.split("#")[0] + "/test/costumBuilder";
    // listCostum();
});

function openCms(poiId){
	
	var url = baseUrl+'/co2/element/get/type/poi/id/'+poiId+"/edit/true";
	// hashdocs="#docs.poi."+poiId;
	// currentDocPos=pos;
	//alert(".link-docs-menu-poi"+currentDocPos);
	//alert(hashdocs);
    //location.hash = hashdocs;
    descHtml = "";
	ajaxPost(null ,url, null,function(data){
        mylog.log("documentation","renderDocHTML",data);
        if(data.result){
            descHtml += "<h4>Nom du costum : </h4>" + data.map.name;
            descHtml += "<h4>Description du costum :</h4>" + data.map.description;
            if(data.map.documents){
                mylog.log("building","poi doc associated documents",data.map.documents);
                descHtml += "<br/><h4>Documents</h4>";
                $.each(data.map.documents, function(k,d) { 
                    if(d.doctype=="file"){
                    var dicon = "fa-file";
                    var fileType = d.name.split(".")[1];
                    mylog.log("documents",d);
                    if( fileType == "png" || fileType == "jpg" || fileType == "jpeg" || fileType == "gif" )
                    dicon = "fa-file-image-o";
                    else if( fileType == "pdf" )
                    dicon = "fa-file-pdf-o";
                    else if( fileType == "xls" || fileType == "xlsx" || fileType == "csv" )
                    dicon = "fa-file-excel-o";
                    else if( fileType == "doc" || fileType == "docx" || fileType == "odt" || fileType == "ods" || fileType == "odp" )
                    dicon = "fa-file-text-o";
                    else if( fileType == "ppt" || fileType == "pptx" )
                    dicon = "fa-file-text-o";
                    else 
                    dicon = "fa-file";
                    descHtml += "<a href='"+d.path+"' target='_blanck'><i class='text-red fa "+dicon+"'></i> "+d.name+"</a><br/>" 
                    }
                });
            }
            
            if(notEmpty(data.map.tags)){
                mylog.log("poi tags associated", data.tags);
                descHtml += "<h4> Tags </h4><div class='text-red'>";
                
                $.each(data.map.tags, function(k,v){
                    descHtml += "#"+v+ " ";
                });
                descHtml += "</div>";
            }
            // descHtml = documentation.renderDocHTML(data);
			// $('#container-docs').html(descHtml);
            $('#container-docs').html(descHtml);
        }
    });

    
}
</script>
