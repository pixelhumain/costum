<?php  
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl); 
    $poiList = array();
    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    
        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                               "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                               "type"=>"cms") );
    } 
?>

<style>
section {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}
.header{
    background: #007a5e;
    background-image : url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireCameroun/header.jpg);
    width: 101%;
    height: auto;
    background-size: cover;
    padding-bottom: 50%;
}
.info-card{
    padding: 5%;
    color: white;
}
.btn-perso {
    background-color: transparent;
    border: none;
}
.logo{
    width: 35%;
    padding-top: 2%;
}
.hexagon1{
    margin: 0px 0 0 -51% !important;
}
@media (max-width:768px){
    .header{
    padding-bottom: 10%;
    }
}

</style>
<div class="header">
    <div class="col-md-12">
    <center>
    <div class="logo">
        <?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 24.0.3, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 483 550" style="enable-background:new 0 0 483 550;" xml:space="preserve">
<style type="text/css">
	.st0{opacity:0.85;fill:#F5F5F5;}
	.st1{fill:#020202;}
	.st2{fill:none;stroke:#020202;stroke-width:1.0337;stroke-miterlimit:10;}
	.st3{fill:none;}
	.st4{font-family:'SharpSansNo1-Medium';}
	.st5{font-size:30.9044px;}
	.st6{letter-spacing:5;}
	.st7{font-family:'SharpSansNo1-Bold';}
	.st8{clip-path:url(#SVGID_2_);fill:#CE1126;}
	.st9{clip-path:url(#SVGID_2_);fill:#FCD116;}
</style>
<polygon class="st0" points="463.45,405.64 463.45,148.06 240.38,19.27 17.31,148.06 17.31,405.64 240.38,534.43 "/>
<g>
	<ellipse class="st1" cx="206.14" cy="132.43" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="258.67" cy="129.77" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="303.74" cy="147.9" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="306.32" cy="204.16" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="144.13" cy="166.73" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="113.79" cy="216.92" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="106.9" cy="264.85" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="115.46" cy="304.8" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="192.18" cy="250.77" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="199.94" cy="283.83" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="165.43" cy="302.13" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="152.69" cy="264.85" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="161.15" cy="218.09" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="195.66" cy="170.98" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="208.5" cy="223.53" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="236.32" cy="182.8" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="236.32" cy="212.67" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="287.79" cy="170.98" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="265.32" cy="222.34" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="281.59" cy="248.28" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="317.55" cy="295.96" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="277.31" cy="279.58" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="363.7" cy="287.45" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="365.57" cy="236.75" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="346.04" cy="185.24" rx="4.28" ry="4.25"/>
	<ellipse class="st1" cx="337.48" cy="255.02" rx="4.28" ry="4.25"/>
	<polygon class="st2" points="106.9,264.85 152.69,264.85 112.55,216.92 161.15,216.92 192.18,250.2 152.69,264.85 199.94,283.83 
		165.43,302.13 115.76,304.8 	"/>
	<polyline class="st2" points="106.9,264.85 112.55,216.92 144.82,166.73 161.15,216.92 195.66,170.98 208.5,222.34 192.18,250.2 
		199.94,283.83 	"/>
	<polyline class="st2" points="195.66,170.98 206.14,131.96 236.32,182.8 258.67,129.9 287.79,170.98 236.32,212.67 	"/>
	<polyline class="st2" points="317.55,295.96 277.31,279.58 282.31,248.28 265.68,222.34 236.32,212.67 208.5,222.34 236.32,182.8 
		195.66,170.98 144.82,166.73 206.14,131.96 258.67,129.9 303.74,146.25 346.04,185.24 366.13,236.75 363.7,287.45 317.55,295.96 
		337.48,255.02 346.04,185.24 306.32,204.16 303.74,146.25 287.79,170.98 236.32,182.8 	"/>
	<polyline class="st2" points="366.13,236.75 337.48,255.02 363.7,287.45 	"/>
	<line class="st2" x1="236.32" y1="182.8" x2="236.32" y2="212.67"/>
	<polygon class="st2" points="306.32,204.16 282.31,248.28 337.48,255.02 306.32,204.16 265.68,222.34 287.79,170.98 	"/>
	<polyline class="st2" points="115.76,304.8 152.69,264.85 165.43,302.13 	"/>
	<polyline class="st2" points="208.5,222.34 161.15,216.92 152.69,264.85 	"/>
	<line class="st2" x1="277.31" y1="279.58" x2="337.48" y2="255.02"/>
</g>
<rect x="80.99" y="318.79" class="st3" width="330.58" height="31.39"/>
<text transform="matrix(1 0 0 1 80.9925 341.3473)"><tspan x="0" y="0" class="st4 st5 st6">SMARTERRITOIRE</tspan></text>
<g>
	<defs>
		<path id="SVGID_1_" d="M236.34,132.43c-72.11,0-130.55,58.44-130.55,130.55c0.45,20.57,5.72,40.29,13.95,58.72
			c33.67,75.64,116.61,129.28,116.61,129.28s82.69-53.49,116.43-128.95c8.3-18.5,13.65-38.34,14.1-59.04
			C366.86,190.87,308.42,132.43,236.34,132.43z M236.34,309.05c-25.45,0-46.07-20.65-46.07-46.07c0-25.45,20.62-46.07,46.07-46.07
			s46.07,20.62,46.07,46.07C282.4,288.41,261.78,309.05,236.34,309.05z"/>
	</defs>
	<clipPath id="SVGID_2_">
		<use xlink:href="#SVGID_1_"  style="overflow:visible;"/>
	</clipPath>
	<path class="st8" d="M264.78,445.52c-0.84,0.28-1.83,0.56-2.67,0.84c-15.48,3.52-31.23,5.06-47.13,4.5l0,0
		c-95.11-3.66-116.77-93.42-116.77-93.42c75.27-8.02,116.63,6.75,139.28,26.17C255.5,399.09,265.63,421.88,264.78,445.52z"/>
	<path class="st9" d="M344.32,353.84c0,0-16.33,60.92-70.64,84.13c0.28-3.24-1.81-37-29.11-60.5
		C261.73,361.86,291.7,350.46,344.32,353.84z"/>
</g>
</svg>

        </div>
    </center>
    </div>
</div>

<!-- SEARCHBARRE -->
<div class="hidden-xs searchbar col-md-12">
<!-- DEBUT SVG -->
<div class=" text-center">
    <?xml version="1.0" encoding="utf-8"?>
    <!-- Generator: Adobe Illustrator 22.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
    <svg class="search" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 80 1006 95" style="enable-background:new 0 0 1006 217; margin-top: -2%; width: 50%;" xml:space="preserve">
    <style type="text/css">
        .st0{fill:#FFFFFF;}
        .st1{fill:#007a5e;}
    </style>
    <g>
        
            <!-- <image style="overflow:visible;opacity:0.29;" width="956" height="124" xlink:href="BD554E7F0366A2C6.png"  transform="matrix(1 0 0 1 20.4646 63.5639)">
        </image> -->
        <g style="filter: drop-shadow(0 0px 10px rgba(0, 0, 0, 0.5));">
            <polygon class="st0" points="943.5,117.4 922.2,80.5 908.1,80.5 879.6,80.4 77,80.4 77,80.5 58.9,80.5 37.5,117.4 58.9,154.4 
                77,154.4 77,154.5 737.5,154.5 879.6,154.4 922.2,154.4       "/>
        </g>
    </g>
    <g>
    <a data-type="filters" href="javascript:;" id="second-search-bar-addon-smarterre">
            <polygon class="st1" points="921.7,80.2 878.5,80.2 857.8,116.7 879.4,154 922.6,154 944.2,116.7  "/>
            <path class="st0" d="M919,116.9l-2.3-2.3l0,0l-6.3-6.3l-2.3,2.3l4.5,4.4h-28v3.2h28.5l-5,5l2.3,2.3l6.3-6.3l0,0L919,116.9
            L919,116.9L919,116.9z M914.2,117.2v-0.5l0.3,0.3L914.2,117.2z"/>
    </a>
    </g>
    <g>
        <text x="125" y="130"></text>
    </g>
    <path class="st1" d="M110.7,138.8c-0.1-0.1-0.1-0.2-0.2-0.2c-3.3-3.3-6.6-6.6-9.9-9.9c-0.1-0.1-0.1-0.1-0.2-0.2
        c-4,2.8-8.3,3.7-13.1,2.5c-3.8-0.9-6.8-3.1-9.1-6.3c-4.4-6.3-3.4-15.1,2.4-20.4c5.7-5.2,14.6-5.4,20.5-0.5
        c6.1,5.1,7.7,14.3,2.7,21.3c0.1,0.1,0.1,0.1,0.2,0.2c3.3,3.3,6.5,6.5,9.8,9.8c0.1,0.1,0.2,0.1,0.3,0.2c0,0,0,0.1,0,0.1
        C113,136.5,111.9,137.7,110.7,138.8C110.7,138.8,110.7,138.8,110.7,138.8z M80.4,115.9c0,6,4.9,10.9,10.8,10.9
        c6,0,10.8-4.9,10.8-10.8c0-6-4.8-10.9-10.8-10.9C85.3,105,80.4,109.9,80.4,115.9z"/>
    </svg>
</div>

<div class="hidden-xs" style="margin-top: -4.5%; position: absolute; width: 450px; border: none; margin-left: 31%;">
    <input type="text" class="main-search-bar barS" id="second-search-bar" placeholder="Rechercher un territoire" style="border: none !important; box-shadow: 0px 0px 0px 0px !important;
border-radius: 0px 0px 0px 0px !important;">

    <div id="dropdown" class="dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding" style="top: 16px;max-height: 100%;background-color:white;width: 110%;margin-left: 0%;z-index: 1000;">
    </div>
</div>
<!-- FIN SVG -->
</div>

<div class="agenda-carousel row">
    <div style="margin-top: 6vw" class="col-xs-10 col-sm-12 col-md-12 no-padding carousel-border" >
        <div id="docCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner itemctr">
            </div>
            <div id="arrow-caroussel">
                <a class="left carousel-control" href="#docCarousel" data-slide="prev">
                    <img style="width:11vw;margin-top: 16vw;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche_slide_gauche.svg" class="img-responsive">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#docCarousel" data-slide="next">
                     <img style="width:11vw;margin-top: 15vw;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche_slide_droite.svg" class="img-responsive">
                    <span class="sr-only">Next</span>
                </a>
            </div>   
        </div>
    </div> 
</div>

<div class="container">
    <div style="margin-top: 3vw;border-radius: 10px 10px 10px 10px;background-color:#ce1127;" class="col-md-12">
        <div>
                <!-- Début news svg--> 
                <center>
                <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.09 11.71" style="width: 50%; padding: 2%;"><defs>
                        <style>.cls-1{fill:#fff;}.cls-2{font-size:5.11px;font-family:SharpSansNo1-Medium, Sharp Sans No1;font-weight:500;letter-spacing:-0.01em;}.cls-3{letter-spacing:-0.03em;}.cls-4{letter-spacing:-0.01em;}.cls-5{letter-spacing:0em;}.cls-6{letter-spacing:-0.04em;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.02em;}</style>
                    </defs>
                    <title>
                        je souhaite</title>
                    <polygon class="cls-1" points="88.09 5.86 84.72 11.69 3.37 11.69 0 5.86 3.37 0.02 84.72 0.02 88.09 5.86"/>
                    <text  y="8" x="26" style="font-size: 0.65rem;fill: #ce1127;">
                        ACTUALITÉS
                    </text>
                    </svg>
            </center>
        <!-- Fin news svg --> 
        </div>
    </div>
    <!-- NEWS -->
    <div id="newsstream" class="col-md-12">
    </div>

    <div style="background-color: #ce1127;border-radius: 10px 10px 10px 10px;" class="text-center container col-lg-12 col-sm-12 col-xs-10 col-md-12">
        <a style="color: white;" href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg" class="img-responsive" style="width: 15%;margin-top: -3vw;"><br>
            <p style="margin-top: -4vw;">Voir plus d'actualités<p> 
        </a>
    </div>
</div>

<div class="container">
    <div style="margin-top: 3vw;" class="col-md-12">
                    <!-- Début news svg--> 
                    <center>
                <svg id="Calque_1" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88.09 11.71" style="width: 50%; padding: 2%;"><defs>
                        <style>.cl-1{fill:#007a5e;}.cls-2{font-size:5.11px;font-family:SharpSansNo1-Medium, Sharp Sans No1;font-weight:500;letter-spacing:-0.01em;}.cls-3{letter-spacing:-0.03em;}.cls-4{letter-spacing:-0.01em;}.cls-5{letter-spacing:0em;}.cls-6{letter-spacing:-0.04em;}.cls-7{letter-spacing:-0.01em;}.cls-8{letter-spacing:-0.02em;}</style>
                    </defs>
                    <title>
                        je souhaite</title>
                    <polygon class="cl-1" points="88.09 5.86 84.72 11.69 3.37 11.69 0 5.86 3.37 0.02 84.72 0.02 88.09 5.86"/>
                    <text  y="8" x="31" style="font-size: 0.65rem;fill: white;">
                        AGENDA
                    </text>
                    </svg>
            </center>
        <!-- Fin news svg --> 
    </div>
        <p class="text-center">Évènement à venir</p>
            <div style="margin-top: 4vw;" id="containEvent">  
            </div>
    

    <div style="margin-top:2vw;background-color: green;border-radius: 10px 10px 10px 10px;" class="text-center container col-lg-12 col-sm-12 col-xs-10 col-md-12">
        <a style="color: white;" href="javascript:;" data-hash="#agenda" class="lbh-menu-app" style="text-decoration : none; font-size:2rem; color:white;">
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterritoireautre/fleche.svg" class="img-responsive plus-m" style="width: 12vw;margin-top: -3vw"><br>
            <p style="margin-top: -4vw;">Voir plus d'évènement<p> 
        </a>
    </div>
</div>

    <div style="margin-top: 2vw;">
        <img style="width: 100%;position: absolute;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/smarterritoireautre/phototerritoire.jpg">
        <?php echo $this->renderPartial("costum.assets.images.smarterritoireautre.bloc_smarterre"); ?> 
    </div>   
<script>
jQuery(document).ready(function() {
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : '<?php echo htmlentities($el['name']) ?>',
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    logo = costum.logo;

    $("#hexa-haut").attr("src",logo);

        globalSearchFilieres();
        afficheEvent();

    urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
});

 function globalSearchFilieres(){
    console.log("----------------- Affichage community");
    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType
    };
     ajaxPost(
         null,
         baseUrl + "/costum/smarterritoireautre/getcommunityaction",
         params,
         function(data){
             console.log("success : ",data);
             var str = "";
             var ctr = "";
             var itr = "";
             var assetsUrl = "<?php echo Yii::app()->getModule('costum')->assetsUrl;?>";
             // Icones en svg
             var acteurs = assetsUrl+"/images/smarterritoireautre/acteur_icone.svg";
             var agenda = assetsUrl+"/images/smarterritoireautre/actu_icone.svg";
             var actus = assetsUrl+"/images/smarterritoireautre/agenda_icone.svg";
             var projet = assetsUrl+"/images/smarterritoireautre/projet_icone.svg";

             var fleches = assetsUrl+"/images/smarterritoireautre/fleche.svg";

             //Pour charger l'image
             var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
             var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

             var i = 1;

             if(data.result == true){
                 // console.log("data.element",data.elt);
                 $(data.elt).each(function(key,value){
                     console.log(value);
                     var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;

                     var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                     var img = (value.img != "none") ? baseUrl + value.img : url;
                     var imgBanner = (value.imgBanner != "none") ? baseUrl + value.imgBanner : url;
                     var filiereGenerique = baseUrl+"/costum/co/index/slug/"+value.slug ;


                     str += "<div class='col-md-4 card-mobile'>";
                     str += "<div class='card'>";
                     str += "<img src='"+imgMedium+"' class='img-responsive img-event'>";
                     str += "<div class='col-xs-12 col-sm-12 col-md-12 card-c'>";
                     str += "</button></div></div></div></div>";

                     if(i == 1) ctr += "<div class='item active'>";
                     else ctr += "<div class='item'>";

                     ctr += "<div style='position:absolute;text-align:center;' class='resume col-xs-11 col-sm-11 col-md-12'>";
                     ctr += '<svg class="hexaName" version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 227.1 46" style="enable-background:new 0 0 227.1 46;" xml:space="preserve">';
                     ctr += '<style type="text/css">.st0{fill:#FFFFFF;}.st1{fill:#007a5e;}</style>';
                     ctr += '<g><g><image style="overflow:visible;opacity:0.29;" width="237" height="44"  transform="matrix(1 0 0 1 -2.0847 2.548)"></image><g><polygon class="st0" points="222.6,21.6 215.1,8.6 210.1,8.6 200.1,8.6 17.8,8.6 17.8,8.6 11.4,8.6 3.9,21.6 11.4,34.6 17.8,34.6 17.8,34.6 150.1,34.6 200.1,34.6 215.1,34.6"/></g></g><g><g><path class="st1" d="M37.5,16.2v-2.5H20v13.7c0,0,0,2.5,2.5,2.5h15.6c0,0,1.9,0,1.9-2.5V16.2H37.5z M22.5,28.7c-1.2,0-1.2-1.2-1.2-1.2V14.9h15v12.5c0,0.6,0.2,1,0.4,1.2H22.5z"/><rect x="22.5" y="17.4" class="st1" width="12.5" height="1.2"/><rect x="29.4" y="24.9" class="st1" width="4.4" height="1.2"/><rect x="29.4" y="22.4" class="st1" width="5.6" height="1.2"/><rect x="29.4" y="19.9" class="st1" width="5.6" height="1.2"/><rect x="22.5" y="19.9" class="st1" width="5.6" height="6.2"/><text class="nameFil" x="48px" y="26px"><a target"_blank" href="'+filiereGenerique +'">'+value.name+'</text></a></g></g></g>';
                     ctr += '</svg>';
                     ctr += "<ul class='dataSlide' style=''>";
                     ctr += "<a href='#'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+acteurs+"><br>"+value.countActeurs+"<br>Acteur(s)"+"<img src='"+fleches+"'></li></a>";
                     ctr += "<a href='#live' target='_blank'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+actus+"><br>"+value.countActus+"<br> Actu(s)"+"<img src='"+fleches+"'></li></a>";
                     ctr += "<a href='#agenda' target='_blank'><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+agenda+"><br>"+value.countEvent+"<br> évènement(s)"+"<img src='"+fleches+"'></li></a>";
                     ctr += "<a><li style='font-size: 26px;color:white;font-weight:bold;' class='inline-block count'><img class='imgSlide' src="+projet+"><br>"+value.countProjet+"<br> Projet(s)"+"<img src='"+fleches+"'></li></a>";
                     ctr += "</ul>";
                     ctr += "</div>";

                     ctr += "<img style='width:100%;height:43vw;' src='"+imgBanner+"' class='ctr-img img-responsive'>";
                     ctr += "<div style='border-style: solid;height: 1vw;border-color:transparent;background-color:#00B2BA;'>";
                     ctr += "</div>";
                     ctr += "</div>";

                     i++;
                 });
                 $(".itemctr").html(ctr);
             }
             else{
                 str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucune filière n'éxiste</b></div>";
                 $(".agenda-carousel").hide();
             }

             // if(i != 2) $("#arrow-caroussel").show();
             // else $("#arrow-caroussel").hide();
             // console.log("i truc -------------", i );
             // $("#event-affiche").html(str);
             // console.log(str);
         },
         function(e){
             console.log("error : ",e);
         },
         "json",
         {async : false}
     );

}


function afficheEvent(){
    console.log("----------------- Affichage évènement");

    var params = {
        contextId : costum.contextId,
        contextType : costum.contextType,
        source : costum.contextSlug
    };
    ajaxPost(
        null,
        url : baseUrl + "/costum/smarterritoireautre/geteventaction",
        params,
        function(data){
            console.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + costum.htmlConstruct.directory.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";

            var fleches = ph + "/images/smarterritoireautre/fleche-plus-noire.svg";

            var i = 1;

            if(data.result == true){

                $(data.element).each(function(key,value){
                    console.log("data.element",data.element);

                    var startDate = (typeof value.startDate != "undefined") ? "Du "+dateToStr(value.startDate, "fr", false, false) : null;

                    /**
                     Phase de dev
                     **/
                    var imgMedium = (value.imgMedium != "none") ? "/ph/"+value.imgMedium : url;
                    var img = (value.img != "none") ? "/ph/" + value.img : url;
                    /**
                     Phase de prod
                     **/
                    // var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;
                    // var img = (value.img != "none") ? value.img : url;

                    str += '<div class="card">';
                    str += "<button class='col-md-4 btn-perso lbh-preview-element' href='#@"+value.slug+"' data-hash='#page.type.events.id."+value.id+"' style='text-decoration : none; color:white;'>";
                    str += '<div id="event-affiche" class="card-color">';
                    str += '<div id="affichedate" class="info-card text-center">';
                    str += '<div id="afficheImg" class="img-hexa">';
                    str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><div class="hexagon-in2" style="background-image: url('+imgMedium+');"><div style=";display:none;color:black;position: absolute;margin-left: -113px;margin-top: 6vw;" class="text-in1">'+value.name+'<br><img width="5vw;" src="'+fleches+'"></div></div></div></div>';
                    str += '</div>';
                    str += ''+startDate+'</br>'+value.type;
                    str += '</div>';
                    str += '</div>';
                    str += '</button>'
                    str += '</div>';

                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containEvent").html(str);
            console.log(str);
        },
        function(data){
            console.log("error : ",data);
        },
        "json",
        {async : false}
    );
}
</script>