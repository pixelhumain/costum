<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var mapShown= (pageApp=="search") ? true : false;

	var typePlaceNoOther=costum.lists.typePlace;
	// typePlaceNoOther.pop();
	var manageModelNoOther=costum.lists.manageModel;
	// manageModelNoOther.pop();
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$all'
	 		}
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : mapShown
            }
        },
        header:{
	 		options:{
	 			left:{
	 				classes :"col-xs-6 no-padding",
	 				group:{
						count : false
					}
	 			},
	 			right:{
	 				classes :"col-xs-5 no-padding",
	 				group:{
						map : true
					}
	 			}
	 		}
	 	},	
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"]
	 	},
	 	filters : {
			text : {
                placeholder:"Nom du réseau recherché"
			}
	 	}
	};

    var filterSearch={};
	jQuery(document).ready(function() {
		  filterSearch = searchObj.init(paramsFilter);
	});

</script>



