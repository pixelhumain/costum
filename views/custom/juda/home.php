<?php
    if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
        $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

        $poiList = PHDB::find(Poi::COLLECTION, 
                        array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                               "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                               "type"=>"cms") );
    }
    $params = [  "tpl" => "votreIdeeDeCostum","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el ];
?>
<style>
    a:active,
    a:hover {
        outline-width: 0;
    }
    hr {
        border: none;
        height: 20px;
        border-bottom: 1px solid #1c140ea1;
        box-shadow: 0px 8px 16px -7px #333;
        margin: -20px auto 20px;
    }
    .t-capital {
        text-transform: capitalize !important;
    }
    .d-flex {
        display: flex;
    }
    .justify-content-between {
        justify-content: space-between;
    }
    .mb-1 {
        margin-bottom: 12px;
    }
    .mt-2 {
        margin-top: 2em;
    }
    .ml-2 {
        margin-left: 2em;
    }
    .mb-2 {
        margin-bottom: 2em;
    }
    .mt-3 {
        margin-top: 3em;
    }
    .pl-2 {
        padding-left: 4px;
    }
    .centered {
        text-align: center
    }
    .side-menu {
        position: fixed;
        left: 3em;
        top: 5em;
        padding: 1em;
        line-height: 2; 
    }
    .side-menu ul {
        list-style: none;
        padding: 0;
        margin: 0; 
    }
    .side-menu ul ul {
        padding-left: 2em; 
    }
    .side-menu li a {
        display: inline-block;
        color: #aaa;
        text-decoration: none;
        transition: all 0.3s cubic-bezier(0.23, 1, 0.32, 1); 
    }
    .side-menu li.visible > a {
        color: #111;
        transform: translate(5px); 
    }

    .side-marker {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: -1; 
    }
    .side-marker path {
        transition: all 0.3s ease; 
    }
    .contents {
        padding: 1em;
        font-size: 1.2em;
        font-family: 'Frank Ruhl Libre', sans-serif; 
    }
    .title {
        font-weight: 600;
    }
    .title-blue {
        color: #055a7a;
        font-weight: 600;
    }
    .bg-my-blue {
        background-color: #009bab;
    }
    .blue {
        color: #055a7a;
    }
    
</style>

<div class="col-lg-12 col-sm-12 row">
    <div class="col-lg-3 col-sm-3">
        <nav class="side-menu">
            <p class="centered">
                <a>
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/juda/juda.jpg" class="img-circle" width="110" height="115">
                </a>
            </p>
            <h5 class="centered h5-title">Schumann <?= $el["name"] ?></h5>
            <ul>
                <li><a href="#intro">À propos</a></li>
                <li>
                    <a href="#info">Informations</a>
                </li>
                <li>
                    <a href="#projet">Projets</a>
                    <ul>
                        <li><a href="#projet-m1">Projet d'etude en M1</a></li>
                        <li><a href="#projet-l3">Projet d'etude en L3</a></li>
                        <li><a href="#projet-l2">Projet d'etude en L2</a></li>
                        <li><a href="#projet-l1">Projet d'etude en L1</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#stage">Stage</a>
                </li>
                <li>
                    <a href="#expe">Experience</a>
                </li>
                <li>
                    <a href="#etude">Etude</a>
                </li>
            </ul>
            <svg class="side-marker" width="200" height="200" xmlns="http://www.w3.org/2000/svg">
                <path stroke="#444" stroke-width="3" fill="transparent" stroke-dasharray="0, 0, 0, 1000" stroke-linecap="round" stroke-linejoin="round" transform="translate(-0.5, -0.5)" />
            </svg>
        </nav>
    </div>
    <div class="col-lg-9 col-sm-9">
        <aside class="contents">
            <section id="intro">
                <h3>À propos</h3>
                <hr>
                <p>
                    Etudiant à l'Ecole de Management et d'Innovation Technologique. Actuelement M2 en Modelisation et Ingenierie Informatique (M2I).
                </p>
                <p>
                    MicroTasker à Sayna
                </p>
            </section>
            <section>
                <h3 id="info">Information</h3>
                <hr>
                <div class="ml-2 d-flex">
                    <div>
                        <p><i class="fa fa-info"></i></p>
                        <p><i class="fa fa-calendar"></i></p>
                        <p><i class="fa fa-flag"></i></p>
                        <p><i class="fa fa-map-marker"></i></p>
                        <p><i class="fa fa-at"></i></p>
                        <p><i class="fa fa-address-book"></i></p>
                    </div>
                    <div style="padding-left: 10px">
                        <p>Schumann Juda Hasinjanahary</p>
                        <p>20 Août 1998 à Andranovorivato</p>
                        <p>Malagasy</p>
                        <p>Lot 120/k10/3604 Mitsinjosoa II Antsororokavo Fianarantsoa 301</p>
                        <p>judaaina@gmail.com</p>
                        <p>+261 34 40 895 16</p>
                    </div>
                </div>
            </section>
            <section>
                <h3 id="projet">Projets</h3>
                <hr>
                <div id="projet-m1">
                    <h4>Projets d'etude en premiere année de Master</h4>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et réalisation d´un site web de gestion de vente de télévision</p>
                            <p class="title-blue">mars 2021</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Spring <br>
                                - Struts <br>
                                - JS <br>
                                - Bootstrap <br>
                                - Oracle <br>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Réalisation d'une application de manipulation et modification d'image</p>
                            <p class="title-blue">fevrier 2021</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Angular <br>
                                - Electron <br>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="projet-l3" class="mt-3">
                    <h4>Projets d'etude en troisieme annee de licence</h4>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application de messagerie</p>
                            <p class="title-blue">octobre 2019</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - JavaFx <br>
                                - Postgresql <br>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application web de gestion de reabonnement</p>
                            <p class="title-blue">octobre 2019</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - React <br>
                                - Codeigniter <br>
                                - Mysql <br>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application web de gestion de vente de television</p>
                            <p class="title-blue">septembre 2019</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Jsp <br>
                                - Struts <br>
                                - Jquery <br>
                                - Ajax <br>
                                - Postgresql
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application web de vente de television</p>
                            <p class="title-blue">aout 2019</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Struts <br>
                                - Jquery <br>
                                - Ajax <br>
                                - Postgresql
                            </span>
                        </div>
                    </div>
                </div>
                <div id="projet-l2" class="mt-3">
                    <h4>Projets d'etude en deuxieme annee de licence</h4>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application de gestion de vente de television</p>
                            <p class="title-blue">octobre 2018</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Java <br>
                                - Mysql <br>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application de gestion de reparation de velo</p>
                            <p class="title-blue">octobre 2018</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - C# <br>
                                - Mysql <br>
                            </span>
                        </div>
                    </div>
                    <div class="mb-2">
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception et realisation d'une application de gestion de reparation de velo</p>
                            <p class="title-blue">octobre 2018</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - PHP <br>
                                - Mysql <br>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="projet-l1" class="mt-3">
                    <h4>Projets d'etude en premiere annee de licence</h4>
                    <div class="">
                        <span class="d-flex justify-content-between">
                            <p class="title">Creation d'une application de gestion de reparation de velo</p>
                            <p class="title-blue">novembre 2017</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - MsAccess <br>
                            </span>
                        </div>
                    </div>
                </div>
            </section>
            <section id="stage">
                <h3>Stage</h3>
                <hr>
                <div>
                    <h4>Mini memoire en premiere annee de master</h4>
                    <div>
                        <span class="d-flex justify-content-between">
                            <p class="title">Refonte du Systeme Informatise pour la Gestion de Secours au deces (SIGSD)</p>
                            <p class="title-blue">mai 2021 - jul. 2021</p>
                        </span>
                        <p class="blue ml-2">EMIT, Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - Angular <br>
                                - Codeigniter <br>
                                - OzekiSMS <br>
                                - Oracle
                            </span>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <span class="d-flex justify-content-between">
                        <h4>Stage de quatre mois</h4>
                        <p class="title-blue">decembre 2019 - mars 2020</p>
                    </span>
                    <div>
                        <span class="d-flex justify-content-between">
                            <p class="title">Conception d'un Systeme Informatise pour la Gestion de Secours au Deces (SIGSD), back office - cas du Service Regional de Solde et de Pension Haute Matsiatra</p>
                        </span>
                        <p class="blue ml-2">Information Technology & Developement Consulting Madagascar (ITDC), Fianarantsoa</p>
                        <div class="ml-2 d-flex">
                            <span>Outils: </span>
                            <span class="pl-2">
                                - AngularJS <br>
                                - Codeigniter <br>
                                - OzekiSMS <br>
                                - Oracle
                            </span>
                        </div>
                    </div>
                </div>
                <div class="mt-3">
                    <h4>Stage de trois mois</h4>
                    <span class="d-flex justify-content-between">
                        <p class="title">Conception et realisation d'une application de gestion de reabonnement canal+</p>
                        <p class="title-blue">oct. 2018 - dec. 2018</p>
                    </span>
                    <p class="blue ml-2">Canal+, Ambalavao</p>
                    <div class="ml-2 d-flex">
                        <span>Outils: </span>
                        <span class="pl-2">
                            - Java <br>
                            - Mysql <br>
                        </span>
                    </div>
                    <p></p>
                </div>
            </section>
            <section id="expe">
                <h3>Experience</h3>
                <hr>
                <div style="min-height: 370px">
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span class="d-flex justify-content-between">
                            <span>Javascript</span>
                            <!-- <span><i class="fa fa-code"></i></span> -->
                        </span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Node</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Angular</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>React</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 42%;" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Java</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 58%;" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Struts</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 55%;" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Spring</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 53%;" aria-valuenow="53" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>PHP</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 62%;" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Codeigniter</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 62%;" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Laravel</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 42%;" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Symfony</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Flutter</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 35%;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Python</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-10 mb-1">
                        <span>Django</span>
                        <div class="progress" style="height: 12px">
                            <div class="progress-bar bg-my-blue" role="progressbar" style="width: 43%;" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="etude">
                <h3>Etude</h3>
                <hr>
                <div class="mb-2">
                    <span class="d-flex justify-content-between">
                        <h4 class="t-capital">Deuxieme annee en Master</h4>
                        <p class="title-blue">novembre 2021 - Aujourd'hui</p>
                    </span>
                    <p class="blue ml-2">EMIT, Fianarantsoa</p>
                    <div class="d-flex ml-2">
                        <span>Mention: </span>
                        <span class="pl-2">- Informatique</span>
                    </div>
                    <div class="d-flex ml-2">
                        <span>Parcours: </span>
                        <span class="pl-2">- M2I</span>
                    </div>
                </div>
                <div class="mb-2">
                    <span class="d-flex justify-content-between">
                        <h4 class="t-capital">Premiere annee en Master</h4>
                        <p class="title-blue">novembre 2020 - aout 2021</p>
                    </span>
                    <p class="blue ml-2">EMIT, Fianarantsoa</p>
                    <div class="d-flex ml-2">
                        <span>Mention: </span>
                        <span class="pl-2">- Informatique</span>
                    </div>
                    <div class="d-flex ml-2">
                        <span>Parcours: </span>
                        <span class="pl-2">- M2I</span>
                    </div>
                </div>
                <div class="mb-2">
                    <span class="d-flex justify-content-between">
                        <h4 class="t-capital">Troisieme annee en Licence</h4>
                        <p class="title-blue">2018 - novembre 2019</p>
                    </span>
                    <p class="blue ml-2">EMIT, Fianarantsoa</p>
                    <div class="d-flex ml-2">
                        <span>Mention: </span>
                        <span class="pl-2">- Informatique</span>
                    </div>
                    <div class="d-flex ml-2">
                        <span>Parcours: </span>
                        <span class="pl-2">- Developpement d'application Internet et Intranet</span>
                    </div>
                </div>
                <div class="mb-2">
                    <span class="d-flex justify-content-between">
                        <h4 class="t-capital">Deuxieme annee en Licence</h4>
                        <p class="title-blue">2017 - 2018</p>
                    </span>
                    <p class="blue ml-2">EMIT, Fianarantsoa</p>
                    <div class="d-flex ml-2">
                        <span>Mention: </span>
                        <span class="pl-2">- Informatique</span>
                    </div>
                    <div class="d-flex ml-2">
                        <span>Parcours: </span>
                        <span class="pl-2">- Developpement d'application Internet et Intranet</span>
                    </div>
                </div>
                <div class="mb-2">
                    <span class="d-flex justify-content-between">
                        <h4 class="t-capital">Premiere annee en Licence</h4>
                        <p class="title-blue">2016 - 2017</p>
                    </span>
                    <p class="blue ml-2">EMIT, Fianarantsoa</p>
                    <div class="d-flex ml-2">
                        <span>Mention: </span>
                        <span class="pl-2">- Informatique</span>
                    </div>
                    <div class="d-flex ml-2">
                        <span>Parcours: </span>
                        <span class="pl-2">- Developpement d'application Internet et Intranet</span>
                    </div>
                </div>
            </section>
        </aside>
    </div>
</div>


<script>
    $(document).ready(function() {
        var toc = document.querySelector( '.side-menu' );
        var tocPath = document.querySelector( '.side-marker path' );
        var tocItems;

        // Factor of screen size that the element must cross
        // before it's considered visible
        var TOP_MARGIN = 0.1,
            BOTTOM_MARGIN = 0.2;

        var pathLength;

        var lastPathStart,
            lastPathEnd;

        window.addEventListener( 'resize', drawPath, false );
        window.addEventListener( 'scroll', sync, false );

        drawPath();

        function drawPath() {

            tocItems = [].slice.call( toc.querySelectorAll( 'li' ) );

            // Cache element references and measurements
            tocItems = tocItems.map( function( item ) {
                var anchor = item.querySelector( 'a' );
                var target = document.getElementById( anchor.getAttribute( 'href' ).slice( 1 ) );

                return {
                    listItem: item,
                    anchor: anchor,
                    target: target
                };
            } );

            // Remove missing targets
            tocItems = tocItems.filter( function( item ) {
                return !!item.target;
            } );

            var path = [];
            var pathIndent;

            tocItems.forEach( function( item, i ) {

                var x = item.anchor.offsetLeft - 5,
                    y = item.anchor.offsetTop,
                    height = item.anchor.offsetHeight;

                if( i === 0 ) {
                    path.push( 'M', x, y, 'L', x, y + height );
                    item.pathStart = 0;
                }
                else {
                    // Draw an additional line when there's a change in
                    // indent levels
                    if( pathIndent !== x ) path.push( 'L', pathIndent, y );

                    path.push( 'L', x, y );

                    // Set the current path so that we can measure it
                    tocPath.setAttribute( 'd', path.join( ' ' ) );
                    item.pathStart = tocPath.getTotalLength() || 0;

                    path.push( 'L', x, y + height );
                }

                pathIndent = x;

                tocPath.setAttribute( 'd', path.join( ' ' ) );
                item.pathEnd = tocPath.getTotalLength();

            } );

            pathLength = tocPath.getTotalLength();

            sync();

        }

        function sync() {

            var windowHeight = window.innerHeight;

            var pathStart = pathLength,
                pathEnd = 0;

            var visibleItems = 0;

            tocItems.forEach( function( item ) {

                var targetBounds = item.target.getBoundingClientRect();

                if( targetBounds.bottom > windowHeight * TOP_MARGIN && targetBounds.top < windowHeight * ( 1 - BOTTOM_MARGIN ) ) {
                    pathStart = Math.min( item.pathStart, pathStart );
                    pathEnd = Math.max( item.pathEnd, pathEnd );

                    visibleItems += 1;

                    item.listItem.classList.add( 'visible' );
                }
                else {
                    item.listItem.classList.remove( 'visible' );
                }

            } );

            // Specify the visible path or hide the path altogether
            // if there are no visible items
            if( visibleItems > 0 && pathStart < pathEnd ) {
                if( pathStart !== lastPathStart || pathEnd !== lastPathEnd ) {
                    tocPath.setAttribute( 'stroke-dashoffset', '1' );
                    tocPath.setAttribute( 'stroke-dasharray', '1, '+ pathStart +', '+ ( pathEnd - pathStart ) +', ' + pathLength );
                    tocPath.setAttribute( 'opacity', 1 );
                }
            }
            else {
                tocPath.setAttribute( 'opacity', 0 );
            }

            lastPathStart = pathStart;
            lastPathEnd = pathEnd;

        }
    })
</script>