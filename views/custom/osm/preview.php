
<style type="text/css">
    .blockFontPreview {
        font-size: 14px;
        color: #777;
    }

    #container-list-links-element .panel-heading h4 {
        margin: 0px;
        font-size: 18px !important;
        font-weight: 200;
        padding: 10px;
        border-radius: 0px;
    }

    #container-list-links-element ul {
        list-style: none !important;
        padding-left: 0px;
    }

    #container-list-links-element .btn-scroll-type {
        border: none!important;
        width: 100%;
        padding: 0px 3px;
        text-align: left;
        margin-top: 0px;
        background-color: transparent;
        float: left;
    }

    #container-list-links-element .thumb-send-to {
        width: 15%;
        height: auto;
    }

    #container-list-links-element .thumb-send-to {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        margin: 5px;
        padding: 3px;
        margin-top: 7px;
    }

    #container-list-links-element .info-contact {
        max-width: 78%;
        font-size: 14px;
        display: inline-block;
        vertical-align: middle;
    }

    #container-list-links-element .btn-select-contact {
        width: 80% !important;
        border-radius: 0px;
    }
    .elipsis {
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        max-width: 98%;
        display: inline-block;
    }

    #container-list-links-element .btn-show-all {
        float: right;
        margin-right: 8px;
        padding: 0px 3px;
        color: #4385f4;
        background: white;
        border: 1px solid;
        border-radius: 2px;
    }
</style>

<div class="col-xs-12 no-padding">
    <div class="col-xs-12 padding-10 toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <a href="javascript:;" class="btn btn-primary pull-right margin-right-10 btn-creat-new-element-osm"><?= (isset($isCoData) && $isCoData) ? "Aller sur la page" : "Creer une organisation" ?></a>
    </div>
    <div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll; height: 529px;">
        <div class="col-xs-12 no-padding" id="col-banner" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
            <div id="contentBanner" class="col-xs-12 no-padding">
                <img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" alt="Banner" src="/themes/CO2/assets/img/background-onepage/connexion-lines.jpg">
            </div>
        </div>
        <div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" style="margin-top: -49px;">
            <?php $srcImage = (isset($profilImageUrl)) ? $profilImageUrl : "/images/thumb/default_organizations.png";  ?>
            <img class="img-responsive shadow2 thumbnail" id="img-preview-osm" style="margin: auto;box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?= Yii::app()->getModule('co2')->assetsUrl . "/" . $srcImage ?>">
        </div>
        <div class="preview-element-info col-xs-12">
            <h3 class="text-center"><?= (isset($name)) ? $name : ""; ?></h3>
            <?php if(isset($shortDescription) && $shortDescription != "") { ?>
                <div class="col-xs-10 col-xs-offset-1 margin-top-20">
                    <span class="col-xs-12 text-center" id="shortDescriptionHeader-preview-osm"><?= $shortDescription ?></span>
                </div>
            <?php } 
            if((isset($isCoData) && $isCoData) ) { ?>
                <span class="col-xs-12 text-center blockFontPreview margin-top-20" id="type-preview-osm">
                    
                </span>
            <?php } ?>
            
            <?php 
            if(isset($address) && $address != null && isset($address["streetAddress"]) && isset($address["postalCode"]) && isset($address["addressLocality"])) { ?>
                <div class="header-address col-xs-12 text-center blockFontPreview margin-top-20" id="address-preview-osm">
                    <i class="fa fa-map-marker"></i><?= $address["streetAddress"] ?>,  <?= $address["postalCode"] ?>, <?= $address["addressLocality"] ?>
                </div>
            <?php }
                ?>
            <?php if (isset($tags)) { ?>
                <div class="header-tags col-xs-12 text-center blockFontPreview margin-top-20" id="tags-preview-osm">
                
                    <?php foreach ($tags as $tag) { ?>
                        <a href="javascript:;" class="letter-red" style="vertical-align: top;">#<?= $tag ?></a>
                <?php } ?>
                
            </div>
            <?php } ?>

            <?php if (isset($isCoData) && $isCoData) {
                ?>
                <div id="container-list-links-element">
                    <div class="floopScroll co-scroll col-xs-12 no-padding" id="link-container">
                        
                    </div>

                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        var isExist = '<?=  (isset($isCoData) && $isCoData) ? "true" : "false" ?>' ;
        isExist = (isExist == "true") ? true : false;

        function getMyTplCtx(id) {
            return {
                "id": id,
                "collection": "organizations",
                "path": "allToRoot",
                "value": {
                    "costum": {
                        "slug": "costumize",
                        "htmlConstruct": {
                            "header": {
                                "menuTop": {
                                    "left": {
                                        "buttonList": {
                                            "logo": {
                                                "height": "70"
                                            },
                                            "xsMenu": {
                                                "buttonList": {
                                                    "app": {
                                                        "label": true,
                                                        "icon": false,
                                                        "spanTooltip": false,
                                                        "labelClass": "padding-left-10",
                                                        "buttonList": {
                                                            "#menu-1": true,
                                                            "#menu-2": true
                                                        }
                                                    }
                                                }
                                            },
                                            "app": {
                                                "label": true,
                                                "icon": true,
                                                "spanTooltip": false,
                                                "class": "hidden-xs line-height-3 padding-left-10",
                                                "labelClass": "padding-left-5",
                                                "buttonList": {
                                                    "#menu-1": true,
                                                    "#menu-2": true
                                                }
                                            }
                                        }
                                    },
                                    "right": {
                                        "buttonList": {
                                            "userProfil": {
                                                "name": true,
                                                "img": true
                                            },
                                            "dropdown": {
                                                "buttonList": {
                                                    "admin": true,
                                                    "login": true,
                                                    "logout": true
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            "element": {
                                "tplCss": [
                                    "v1.0_pageProfil.css"
                                ],
                                "banner": "co2.views.element.bannerHorizontal",
                                "containerClass": {
                                    "centralSection": "col-xs-12 col-lg-10 col-lg-offset-1"
                                },
                                "menuTop": {
                                    "class": "col-xs-12",
                                    "left": {
                                        "class": "col-lg-3 col-md-3 col-sm-4 hidden-xs no-padding",
                                        "buttonList": {
                                            "imgUploader": true
                                        }
                                    },
                                    "right": {
                                        "class": "col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center",
                                        "buttonList": {
                                            "xsMenu": {
                                                "class": "visible-xs",
                                                "buttonList": {
                                                    "detail": true,
                                                    "community": true,
                                                    "gallery": true,
                                                    "events": true,
                                                    "classifieds": true,
                                                    "newspaper": true
                                                }
                                            },
                                            "detail": true,
                                            "community": true,
                                            "gallery": true,
                                            "events": true,
                                            "classifieds": true,
                                            "newspaper": true,
                                            "form": true
                                        }
                                    }
                                }
                            },
                            "adminPanel": {
                                "js": true,
                                "add": true,
                                "menu": {
                                    "directory": true,
                                    "reference": true,
                                    "community": true,
                                    "converter": true,
                                    "import": true,
                                    "mails": true,
                                    "log": true,
                                    "moderation": true,
                                    "statistic": true,
                                    "mailerror": true,
                                    "notsendmail": true
                                }
                            }
                        },
                        "typeObj": {
                            "organizations": {
                                "add": true
                            },
                            "event": {
                                "add": true
                            },
                            "projects": {
                                "add": true
                            },
                            "poi": {
                                "add": true
                            }
                        },
                        "app": {
                            "#menu-1": {
                                "name": {
                                    "fr" : "Menu 1"
                                },
                                "staticPage": true,
                                "urlExtra": "/page/menu1",
                                "metaDescription": "",
                                "restricted": {
                                    "draft": true,
                                    "admins": true,
                                    "members": false
                                },
                                "selectApp": false,
                                "appTypeName": "",
                                "hash": "#app.view",
                                "lbhAnchor": false
                            },
                            "#menu-2": {
                                "name": {
                                    "fr" : "Menu 2"
                                },
                                "urlExtra": "/page/menu2",
                                "staticPage": true,
                                "metaDescription": "",
                                "restricted": {
                                    "draft": true,
                                    "admins": true,
                                    "members": false
                                },
                                "selectApp": false,
                                "appTypeName": "",
                                "hash": "#app.view",
                                "lbhAnchor": false
                            }
                        }
                    }
                },
                "format": true
            };
        }

        function ifExistsElement(ifExists, ifNotExists) {
            params = {
                collection: "all",
                name: "<?= (isset($name)) ? $name : ""; ?>",
            };
            if(<?= isset($geo) ?>){
                params["lat"] = "<?= (isset($geo["latitude"])) ? $geo["latitude"] : ""; ?>";
                params["long"] = "<?= (isset($geo["longitude"])) ? $geo["longitude"] : ""; ?>";
            }
            ajaxPost(
                null,
                baseUrl + "/co2/element/existselement",
                params,
                function(data) {
                    if (data != null) {
                        if (typeof data.result != "undefined" && data.result) {
                            isExist = true;
                            ifExists(data);
                        }
                            
                        else
                            ifNotExists(data);
                    }
                }, {
                    async: false
                }
            );

        }

        function createCostum(id) {
            dataHelper.path2Value(getMyTplCtx(id), function(response) {
                if (typeof response != "undefined" && response != null && typeof response.id != "undefined" && typeof response.elt != "undefined" && response.elt != null && typeof response.elt.slug != "undefined") {
                    useTemplate(response.id, response.elt);

                } else {

                }
            })
        }

        function useTemplate(id, element, keyApp = "#welcome") {
            const idTemplate = "62bc5051116f6c58f1667d73"
            if (idTemplate != "") {
                var params = {
                    "id": idTemplate,
                    "parentId": element._id.$id,
                    "page": keyApp,
                    "parentSlug": element.slug,
                    "parentType": element.collection
                };
                ajaxPost(
                    null,
                    baseUrl + "/co2/template/use",
                    params,
                    function(data) {
                        if (userConnected != null)
                            redirectToCostum(element, true);
                        else
                            redirectToCostum(element);
                    }, {
                        async: false
                    }
                );
            }
        }

        function showDynform(name, tag, geo,geoPosition, address = {}) {
            let addresses = [{
                address: <?= (isset($address)) ? json_encode($address) : json_encode(array()); ?>
            }];
            if(Object.keys(address).length > 0){
                addresses = [{
                    address: address,
                    geo: geo,
                    geoPosition, geoPosition
                }];
            }
            mylog.log("Addresses ", address)
            $(".btn-close-preview").trigger("click");
            dyfOrga = {
                "beforeBuild": {
                    "properties": {
                        "name": {
                            "label": "Nom de votre organisation",
                            "inputType": "text",
                            "placeholder": "Nom de votre organisation",
                            "value": name,
                            "order": 1
                        },
                    }
                },
                "onload": {
                    "actions": {
                        "setTitle": "Creer votre organisation",
                        "src": {
                            "infocustom": "<br/>Remplir le champ"
                        }
                    }
                }
            };
            dyfOrga.afterSave = function(orga) {
                dyFObj.commonAfterSave(orga, function() {});
                if (orga != null && typeof orga.id != "undefined")
                    createCostum(orga.id);
            }
            dyFObj.openForm('organization', null, Object.assign({
                type: "NGO",
                role: "admin",
                tags: tag,
                shortDescription: "<?= (isset($shortDescription)) ? $shortDescription : '' ?>",
                addresses: addresses
            }, {}), null, dyfOrga);
        }

        function redirectToCostum(element, editTrue = false) {
            if (element != null && typeof element.slug != "undefined") {
                const slug = element.slug;
                if (typeof element.costum != "undefined") {
                    if (editTrue)
                        window.open(baseUrl + '/costum/co/index/slug/' + slug + '/edit/true', "_blank") || window.location.replace("<?= Yii::app()->baseUrl ?>" + '/costum/co/index/slug/' + slug + '/edit/true');
                    else
                        window.open(baseUrl + '/costum/co/index/slug/' + slug, "_blank") || window.location.replace("<?= Yii::app()->baseUrl ?>" + '/costum/co/index/slug/' + slug);
                } else if (typeof element._id != "undefined" && element._id != null && typeof element._id.$id != "undefined")
                    createCostum(element._id.$id);

            }
        }

        if(isExist) {
            const collection = "<?= (isset($collection)) ? $collection : '' ?>"
            const type = "<?= (isset($type)) ? $type : '' ?>";
            const profilImageUrl = "<?= (isset($profilImageUrl)) ? $profilImageUrl : '' ?>";

            if(profilImageUrl != "")
                $("#img-preview-osm").attr("src", profilImageUrl);

            let color = "";
            let typeToShow = "";

            if(collection == "projects") {
                color = "purple";
                typeToShow = 'PROJET';
                $("#type-preview-osm").html(`<span  class ="text-` + color + ` uppercase">` + typeToShow + ` </span>`);
            }
            else {
                switch (type) {
                    case 'Group':
                        color = "turq";
                        typeToShow = trad.Group;
                        break;
                    case 'NGO':
                        color = "green";
                        typeToShow = trad.NGO;
                        break;
                    case 'LocalBusiness':
                        color = "azure";
                        typeToShow = trad.LocalBusiness;
                        break;
                    case 'GovernmentOrganization':
                        color = "red";
                        typeToShow = trad.GovernmentOrganization;
                        break;
                    case 'Cooperative':
                        color = "nightblue";
                        typeToShow = trad.Cooperative;
                        break;
                    default:
                        break;
                };
                $("#type-preview-osm").html(`<span  class ="text-` + color + ` uppercase"> organisation </span><i class="fa fa-x fa-angle-right margin-left-10"></i> <span class="margin-left-10">` + typeToShow + `</span>`);
            }
            ifExistsElement(
                function(data) {
                    if(data != null && typeof data.elt != "undefined" && data.elt != null && typeof data.elt.links != "undefined")
                        refreshLink(data.elt.links);
                },
                function() {
                }
            );
        }

        function refreshLink(links) {
            projetInLinks = {};
            eventInLinks = {};
            membreInLinks = {};
            comminityInLinks = {};
            var str = "";
            $.each(links , function(id, link) {

                if(link != null && typeof link["collection"] != "undefined") {
                    if(link["collection"] == "citoyens" || link["collection"] == "people")
                        membreInLinks[id] = link;
                    if(link["collection"] == "organizations")
                        comminityInLinks[id] = link;
                    if(link["collection"] == "projects")
                        projetInLinks[id] = link;
                    if(link["collection"] == "events")
                        eventInLinks[id] = link;
                }

                else if(link != null && typeof link["typeSig"] != "undefined") {
                    if(link["typeSig"] == "citoyens" || link["typeSig"] == "people")
                        membreInLinks[id] = link;
                    if(link["typeSig"] == "organizations")
                        comminityInLinks[id] = link;
                    if(link["typeSig"] == "projects")
                        projetInLinks[id] = link;
                    if(link["typeSig"] == "events")
                        eventInLinks[id] = link;
                } 
                else if(link != null && typeof link["type"] != "undefined") {
                    if(link["type"] == "citoyens" || link["type"] == "people")
                        membreInLinks[id] = link;
                    if(link["type"] == "organizations")
                        comminityInLinks[id] = link;
                    if(link["type"] == "projects")
                        projetInLinks[id] = link;
                    if(link["type"] == "events")
                        eventInLinks[id] = link;
                }  
                

            });


            if(Object.keys(eventInLinks).length > 0) {
                i = 1;
                str += `
                    <div class="panel panel-default" id="scroll-type-events">
                        <div class="panel-heading" style="background-color:#ffffff;">
                            <h4 class="text-green"><i class="fa fa-group"></i> <span class=""> Evenement</span><span class="badge text-white bg-green">` + Object.keys(eventInLinks).length + `</span></h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div class="list-group no-padding">
                                <ul id="floopType-events" class="floop-type-list">`;
                                i = 1;
                                $.each(eventInLinks , function(id, event){
                                    
                                    if(i <= 10) {
                                        let image = (typeof event.profilImageUrl) ? event.profilImageUrl : modules.co2.url +"/images/thumb/default_organizations.png";
                                        let name = (typeof event.name != 'undefined') ? event.name : "";
                                        let date = (typeof event.startDate != 'undefined' && typeof event.endDate != 'undefined') ? event.startDate + " à " + event.endDate : "";
                                        str +='<li class=" floopEntries col-xs-12 no-padding" id="floopItem-events-'+id+'" idcontact="'+id+'">'+
                                                '<div class="btn-chk-contact btn-scroll-type"><img src="'+ image +'" data-src="'+ image +'" class="thumb-send-to bg-green" height="35" width="35"><span class="info-contact btn-select-contact elipsis"><a target="_blank" href="' + baseUrl + "#@" + event['slug'] +'" class="contact'+id+'"><span class="name-contact text-dark text-bold" idcontact="'+id+'">'+ name +'</span></a><span class="floop-statu col-xs-12 no-padding">'+ date +'</span></span></div>'+
                                           '</li>';
                                    }
                                    i++;
                                });

                                if(i > 10) {
                                    str += `
                                        <a target="_blank" href="` + baseUrl + "#@" +`<?= (isset($slug)) ? $slug : '' ?>" class="btn-show-all btn-show-more-events" data-type="events"><i class="fa fa-angle-down"></i> Voir tout</a>
                                    `; 
                                }
                                
                                            
                                    
                str += `
                                </ul>
                            </div>
                        </div>
                    </div>   
                `;
            }   
            
            if(Object.keys(projetInLinks).length > 0) {
                i = 1;
                str += `
                    <div class="panel panel-default" id="scroll-type-project">
                        <div class="panel-heading" style="background-color:#ffffff;">
                            <h4 class="text-purple"><i class="fa fa-lightbulb-o"></i> <span class=""> Projets</span><span class="badge text-white bg-purple">` + Object.keys(projetInLinks).length + `</span></h4>
                        </div>
                        <div class="panel-body no-padding">
                                <div class="list-group no-padding">
                                    <ul id="floopType-projects" class="floop-type-list">`;
                                i = 1;
                                $.each(projetInLinks , function(id, projet){
                                    
                                    if(i <= 10) {
                                        let image = (typeof projet.profilImageUrl) ? projet.profilImageUrl : modules.co2.url +"/images/thumb/default_organizations.png";
                                        let name = (typeof projet.name != 'undefined') ? projet.name : "";
                                    
                                        str += `
                                            <li class=" floopEntries col-xs-12 no-padding" id="floopItem-project-`+id+`" idcontact="`+id+`">
                                                <div class="btn-chk-contact btn-scroll-type"><img src="`+ image +`" data-src="`+ image +`" class="thumb-send-to bg-purple" height="35" width="35"><span class="info-contact btn-select-contact elipsis"><a target="_blank" href="` + baseUrl + "#@" + projet['slug'] +`" class="contact`+id+`"><span class="name-contact text-dark text-bold" idcontact="`+id+`">`+ name+`</span></a><span class="floop-statu col-xs-12 no-padding"></span></span></div>                                                  
                                            </li>
                                        `;
                                        i++;
                                    }
                                });

                                if(i > 10) {
                                    str += `
                                        <a target="_blank" href="` + baseUrl + "#@" +`<?= (isset($slug)) ? $slug : '' ?>" class="btn-show-all btn-show-more-projects" data-type="projects"><i class="fa fa-angle-down"></i> Voir tout</a>
                                    `; 
                                }
                                
                                            
                                    
                str += `
                                </ul>
                            </div>
                        </div>
                    </div>   
                `;
            }

            if(Object.keys(membreInLinks).length > 0) {
                i = 1;
                str += `
                    <div class="panel panel-default" id="scroll-type-citoyens">
                        <div class="panel-heading" style="background-color:#ffffff;">
                            <h4 class="text-purple"><i class="fa fa-lightbulb-o"></i> <span class=""> Membres</span><span class="badge text-white bg-purple">` + Object.keys(membreInLinks).length + `</span></h4>
                        </div>
                        <div class="panel-body no-padding">
                                <div class="list-group no-padding">
                                    <ul id="floopType-citoyens" class="floop-type-list">`;
                                i = 1;
                                $.each(membreInLinks , function(id, citoyens){
                                    
                                    if(i <= 10) {
                                        let image = (typeof citoyens.profilImageUrl) ? citoyens.profilImageUrl : modules.co2.url +"/images/thumb/default_organizations.png";
                                        let name = (typeof citoyens.name != 'undefined') ? citoyens.name : "";
                                        let email = (typeof citoyens.email != 'undefined') ? citoyens.email : "";
                                        str += `
                                            <li class=" floopEntries col-xs-12 no-padding" id="floopItem-citoyens-`+id+`" idcontact="`+id+`">
                                                <div class="btn-chk-contact btn-scroll-type"><img src="`+ image +`" data-src="`+ image +`" class="thumb-send-to bg-purple" height="35" width="35"><span class="info-contact btn-select-contact elipsis"><a target="_blank" href="` + baseUrl + "#@" + citoyens['slug'] +`" class="contact`+id+`"><span class="name-contact text-dark text-bold" idcontact="`+id+`">`+ name +`</span></a><span class="floop-statu col-xs-12 no-padding">`+ email+`</span></span></div>                                        
                                            </li>
                                        `;
                                        i++;
                                    }
                                });

                                if(i > 10) {
                                    str += `
                                        <a target="_blank" href="` + baseUrl + "#@" +`<?= (isset($slug)) ? $slug : '' ?>" class="btn-show-all btn-show-more-citoyens" data-type="citoyens"><i class="fa fa-angle-down"></i> Voir tout</a>
                                    `; 
                                }
                                
                                            
                                    
                str += `
                                </ul>
                            </div>
                        </div>
                    </div>   
                `;
            }

            if(Object.keys(comminityInLinks).length > 0) {
                i = 1;
                str += `
                    <div class="panel panel-default" id="scroll-type-organisation">
                        <div class="panel-heading" style="background-color:#ffffff;">
                            <h4 class="text-green"><i class="fa fa-group"></i> <span class=""> Communauté</span><span class="badge text-white bg-green">` + Object.keys(comminityInLinks).length + `</span></h4>
                        </div>
                        <div class="panel-body no-padding">
                            <div class="list-group no-padding">
                                <ul id="floopType-organisation" class="floop-type-list">`;
                                i = 1;
                                $.each(comminityInLinks , function(id, organisation){
                                    if(i <= 10) {
                                        let image = (typeof organisation.profilImageUrl) ? organisation.profilImageUrl : modules.co2.url +"/images/thumb/default_organizations.png";
                                        let name = (typeof organisation.name != 'undefined') ? organisation.name : "";
                                        str += `
                                            <li class=" floopEntries col-xs-12 no-padding" id="floopItem-organisation-`+id+`" idcontact="`+id+`">
                                                <div class="btn-chk-contact btn-scroll-type"><img src="`+ image +`" data-src="`+ image +`" class="thumb-send-to bg-green" height="35" width="35"><span class="info-contact btn-select-contact elipsis"><a target="_blank" href="` + baseUrl + "#@" + organisation['slug'] +`" class="contact`+id+`"><span class="name-contact text-dark text-bold" idcontact="`+id+`">`+ name+`</span></a><span class="floop-statu col-xs-12 no-padding"></span></span></div>                                                  
                                            </li>
                                        `;
                                        i++;
                                    }
                                });

                                if(i > 10) {
                                    str += `
                                        <a target="_blank" href="` + baseUrl + "#@" +`<?= (isset($slug)) ? $slug : '' ?>" class="btn-show-all btn-show-more-organisation" data-type="organisation"><i class="fa fa-angle-down"></i> Voir tout</a>
                                    `; 
                                }
                                
                                            
                                    
                str += `
                                </ul>
                            </div>
                        </div>
                    </div>   
                `;
            } 
            $("#link-container").html(str);

        } 
       
        $(".btn-creat-new-element-osm").off().on("click", function() {
            ifExistsElement(
                function(data) {
                    if (typeof data.elt != "undefined" && data.elt != null && typeof data.elt.slug != "undefined")
                        redirectToCostum(data.elt);
                },
                function(data) {
                    const name = "<?= (isset($name)) ? $name : ""; ?>";
                    const tag = <?= (isset($tags)) ? json_encode($tags) : json_encode([]); ?>;
                    var geo = <?= (isset($geo)) ? json_encode($geo) : json_encode(["latitude" => "", "longitude" => ""]); ?>;
                    var geoPosition = <?= (isset($geoPosition)) ? json_encode($geoPosition) : null; ?>;
                    mylog.log("Data from response",name, tag, geo);
                    showDynform(name, tag, (typeof data.geo != "undefined" && typeof data.geo.geo != "undefined" ? data.geo.geo : geo), (typeof data.geo != "undefined" && typeof data.geo.geoPosition != "undefined" ? data.geo.geoPosition : geoPosition), (typeof data.geo != "undefined" && typeof data.geo.address != "undefined" ? data.geo.address : {}));
                }
            );
        });

    });
</script>