<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$cssAnsScriptFilesModule = array(
    '/js/osm/osmType.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule("co2")->assetsUrl);
?>

<style>
    .content[_ngcontent-ng-c3777985993] {
        overflow: auto scroll;
        background-color: #fff;
        flex-grow: 1;
    }

    #type-element-to-contribution {
        font-size: 1.2em;
        font-weight: 700;
        margin-left: 0;
        margin-right: 0;
        margin-top: 2px;
        margin-bottom: 2px;
    }

    .description-type {
        font-size: .8em;
        opacity: .8;
        margin-left: 0;
        margin-right: 0;
        margin-top: 0;
        margin-bottom: 2px;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.5;
    }

    .contribution-input {
        border: 0;
        background-image: -o-linear-gradient(#009688, #009688), -o-linear-gradient(#D2D2D2, #D2D2D2);
        background-image: linear-gradient(#009688, #009688), linear-gradient(#D2D2D2, #D2D2D2);
        background-size: 0 2px, 100% 1px;
        background-repeat: no-repeat;
        background-position: center bottom, center calc(100% - 1px);
        background-color: rgba(0, 0, 0, 0);
        -o-transition: background 0s ease-out;
        transition: background 0s ease-out;
        float: none;
        box-shadow: none;
        height: auto !important;
        color: #000;
    }

    .contribution-input:focus {
        border: 0;
        background-image: -o-linear-gradient(#009688, #009688), -o-linear-gradient(#D2D2D2, #D2D2D2);
        background-image: linear-gradient(#009688, #009688), linear-gradient(#D2D2D2, #D2D2D2);
        background-size: 0 2px, 100% 1px;
        background-repeat: no-repeat;
        background-position: center bottom, center calc(100% - 1px);
        background-color: rgba(0, 0, 0, 0);
        -o-transition: background 0s ease-out;
        transition: background 0s ease-out;
        float: none;
        box-shadow: none;
        height: auto !important;
        border-bottom: 2px solid #00a041;
    }

    .select-new-input-contribution {
        margin-top: 20px;
        cursor: pointer;
    }

    .select-new-input-contribution:hover .thumbnail, .select-new-type-contribution:hover .thumbnail {
        border: 1px solid green;
    }

    .container-type-contribution, .select-new-type-contribution {
        cursor: pointer;
    }

    
</style>


<style type="text/css">
    .blockFontPreview {
        font-size: 14px;
        color: #777;
    }

    .editeur-osm {
        max-width: 500px;
    }


    .editeur-osm h3 {
        font-size: 16px;
        line-height: 1.25;
        font-weight: bold;
        margin-bottom: 10px;
    }

    .editeur-osm .preset-list-item {
        margin-bottom: 10px;
        position: static;
    }

    .editeur-osm .preset-list-button-wrap {
        min-height: 62px;
        max-width: 500px;
        display: flex;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    .editeur-osm div {
        touch-action: pan-x pan-y;
    }

    .editeur-osm .preset-list-button {
        width: 100%;
        height: 100%;
        position: relative;
        display: flex;
        align-items: center;
    }

    .editeur-osm button {
        text-align: center;
        border: 0;
        background: #fff;
        color: #333;
        font-size: 12px;
        display: inline-block;
        border-radius: 4px;
    }

    .editeur-osm .popover.arrowed.bottom {
        margin-top: 10px;
        left: 31px;
        top: 75px;
        text-align: center;
    }

    .editeur-osm .preset-icon-container {
        position: relative;
        width: 60px;
        height: 60px;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
        flex: 0 0 auto;
    }

    .editeur-osm div {
        touch-action: pan-x pan-y;
    }

    .editeur-osm .preset-list-button .label {
        display: flex;
        flex-flow: row wrap;
        align-items: center;
        background: #f6f6f6;
        text-align: left;
        padding: 5px 10px;
        border-left: 1px solid rgba(0, 0, 0, .1);
        flex: 1 1 100%;
        align-self: stretch;
    }

    .editeur-osm .preset-icon-container svg,
    .editeur-osm .preset-icon-container svg>* {
        cursor: inherit !important;
    }

    .editeur-osm .preset-icon-fill {
        margin: auto;
        position: absolute;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
    }

    .editeur-osm .preset-icon {
        width: 100%;
        height: 100%;
        position: absolute;
        z-index: 1;
    }

    .editeur-osm .preset-list-button .label-inner {
        width: 100%;
        line-height: 1.35em;
    }

    .editeur-osm .preset-list-button .label-inner .namepart:nth-child(1) {
        font-weight: bold;
    }

    .editeur-osm .preset-icon.framed .icon {
        transform: scale(0.4);
    }

    .editeur-osm .preset-icon .icon {
        position: absolute;
        margin: auto;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        transform: scale(0.48);
    }

    .editeur-osm .section .grouped-items-area {
        padding: 10px;
        margin: 0 -10px 10px -10px;
        border-radius: 8px;
        background: #ececec;
    }

    .editeur-osm div,
    .editeur-osm textarea,
    .editeur-osm label,
    .editeur-osm input,
    .editeur-osm form,
    .editeur-osm span,
    .editeur-osm ul,
    .editeur-osm li,
    .editeur-osm ol,
    .editeur-osm a,
    .editeur-osm button,
    .editeur-osm h1,
    .editeur-osm h2,
    .editeur-osm h3,
    .editeur-osm h4,
    .editeur-osm h5,
    .editeur-osm p,
    .editeur-osm img {
        box-sizing: border-box;
    }

    .editeur-osm div {
        touch-action: pan-x pan-y;
    }

    .editeur-osm .form-field {
        display: flex;
        flex-flow: row wrap;
        margin-bottom: 10px;
        width: 100%;
        transition: margin-bottom 200ms;
    }

    .editeur-osm .field-label {
        display: flex;
        flex-flow: row nowrap;
        flex: 1 1 100%;
        position: relative;
        font-weight: bold;
        color: #333;
        background: #f6f6f6;
        border: 1px solid #ccc;
        border-radius: 4px 4px 0 0;
        overflow: hidden;
        max-height: 50px;
        margin-bottom: 0px;
    }

    .editeur-osm .form-field-input-wrap {
        position: relative;
        display: flex;
        flex-flow: row nowrap;
        width: 100%;
        flex: 1 1 auto;
        border-top: 0;
        border-radius: 0 0 4px 4px;
    }

    .editeur-osm .localized-multilingual {
        padding: 0 10px;
        flex-basis: 100%;
    }

    .editeur-osm .tag-reference-body {
        flex: 1 1 auto;
        width: 100%;
        overflow: hidden;
        display: none;
        padding-top: 10px;
        max-height: 0px;
        opacity: 0;
    }

    .editeur-osm .field-label .label-text {
        overflow: hidden;
        text-overflow: ellipsis;
        flex: 1 1 auto;
        padding: 5px 0 4px 10px;
    }

    .editeur-osm .field-label button:not(:hover):not(:active):not(:focus) {
        background: none;
    }

    .editeur-osm .field-label button {
        flex: 0 0 auto;
        border-left: 1px solid #ccc;
        width: 32px;
        border-radius: 0;
    }

    .editeur-osm .field-label .label-text span {
        white-space: nowrap;
    }

    .editeur-osm .field-label .icon {
        opacity: .5;
    }

    .editeur-osm .form-field-name {
        background: #f6f6f6;
        border: 1px solid #ccc;
        border-radius: 4px 4px 0 0;
    }

    .editeur-osm .form-field-input-wrap,
    .editeur-osm .localized-main {
        margin-top: 0px;
        padding-top: 0px;
    }

    .editeur-osm input.localized-main {
        width: 100%;
        height: 50px;
    }

    .editeur-osm textarea.localized-main {
        width: 100%;
        height: 150px;
    }
</style>
<div class="col-xs-12 no-padding">
    <div class="col-xs-12 padding-10 toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
        <a href="javascript:;" class="btn btn-primary pull-right margin-right-10 btn-contribution-element-osm">Envoyer les modifications</a>
    </div>
    <div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll;">
        <div id="container-config-account" <?php if(isset($_SESSION["osm-account"])) echo "style='display:none;'"; ?>>
            <div class="search-header">
                <div class="col-sm-12">
                    <h3 class="text-center">Veuillez entre votre compte Contributeur OSM </h3>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="user-account">
                                    Email Ou Nom d'utilisateur
                                </label>

                                <input type="text" id="user-account" name="email" class="form-control  contribution-input" placeholder="Votre Email ou Votre Nom d'utilisateur sur OSM" value="">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="password-account">
                                    Mot de Passe
                                </label>
                                <input type="password" id="password-account" name="password" class="form-control  contribution-input" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <button class="btn btn-block btn-success" type="boutton" id="btn-use-myaccount">Se Connecter</button>
                <button class="btn btn-block btn-info" type="boutton" id="btn-use-other-account">Utiliser le compte par défaut</button>
            </div>
        </div>

        <div id="container-all-update"  style="display:none;">
            <div class="search-header" id="container-input-contribution">
                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer container-type-contribution" id="container-type-contribution">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="min-height:70px;background-color: rgb(239, 146, 40);margin-left:9px;border-radius:5% 0% 0% 5%;display: flex; justify-content: center; align-items: center;">
                                    <i id="icon-type-element-to-contribution" style="color:#ffffff;height:100%;width:100%;font-size:30px;" class=""></i>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="margin-top:9px;">
                                    <h5 id="type-element-to-contribution"></h5>
                                    <p id="description-type-element-to-contribution" class="description-type"></p>
                                    <input type="hidden" id="type-val-contribution" name="type" class="form-control  contribution-input" value="">
                                    <input type="hidden" id="type-key-contribution" name="key" class="form-control  contribution-input" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="name-contribution">
                                    Nom
                                </label>

                                <input type="text" id="name-contribution" name="name" class="form-control  contribution-input" placeholder="Nom courant (si existant)" value="">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="shortDescription-contribution">
                                    Description
                                </label>

                                <textarea id="shortDescription-contribution" name="description" class="form-control  contribution-input" placeholder="Description" rows="4"></textarea>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="fauteuil-roulant-contribution">
                                    Accès en fauteuil roulant
                                </label>

                                <select id="fauteuil-roulant-contribution" name="website" class="form-control  contribution-input">
                                    <option selected="selected"></option>
                                    <option value="yes">Oui</option>
                                    <option value="limited">Limité</option>
                                    <option value="no">Non</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="website-contribution">
                                    Site Web
                                </label>

                                <input type="text" id="website-contribution" name="website" class="form-control  contribution-input" placeholder="exemple : https://www.communecter.org">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="tel-contribution">
                                    Téléphone
                                </label>

                                <input type="text" id="contact_phone-contribution" name="tel" class="form-control  contribution-input" placeholder="Numéro Téléphone">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="wikidataId-contribution">
                                    WikiData ID
                                </label>

                                <input type="text" id="wikidataId-contribution" name="website" class="form-control  contribution-input" placeholder="Q000000">

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                        <div class="caption card-footer">
                            <div class="form-group input-group-sm   ">
                                <label class="switch-text-label" for="commentaire-contribution">
                                    Commentaire
                                </label>

                                <textarea id="commentaire-contribution" name="description" class="form-control  contribution-input" placeholder="Commentaire" rows="4"></textarea>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <button class="btn btn-block btn-success" type="boutton" id="btn-to-show-selectNewInputContributionModal"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div id="container-all-add-input" style="display:none;">
            <div id="selectNewInputContributionModal">

            </div>
            <div class="col-sm-12">
                <button class="btn btn-block btn-danger" type="boutton" id="btn-to-annuler-selectNewInputContributionModal">Annuler</button>
            </div>
        </div>

        <div id="container-all-type-select"   <?php if(!isset($_SESSION["osm-account"])) echo "style='display:none;'"; ?>>
            <div class="form-group col-sm-12" style="position: fixed;width: 59%;background-color: white;z-index: 999;">
                <input type="text" class="form-control" id="rechercheType" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.3);height:45px;" placeholder="Recherche Type">
            </div>
            <div id="selectType" style="padding-top: 75px;">

            </div>
            <div class="col-sm-12">
                <button class="btn btn-block btn-danger" type="boutton" id="btn-to-annuler-select-type">Annuler</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {
        var osmType = "";
        var newInput = [];
        var allInputForSearche = [];
        const lat = <?= $lat ?>;
        const lon = <?= $lon ?>;

        var allTypeDataOsm = getAllType();
        showAllType();

        $(".btn-contribution-element-osm").off().on("click", function(e) {
            e.preventDefault();
            contribution();
        });

        $("#btn-to-show-selectNewInputContributionModal").off().on("click", function() {
            if($("#").val() != "") {
                $("#selectNewInputContributionModal").html("");
                refreshInputForType();
                $("#container-all-update").hide(500);
                $("#container-all-add-input").show(500);
            }
            else
                toastr.error("Vous devez d'abord selectionner le type de l'element!");
        });

        $("#container-type-contribution").off().on("click", function() {
            showAllType();
            $("#container-all-update").hide(500);
            $("#container-all-type-select").show(500);
        });

        $("#btn-to-annuler-selectNewInputContributionModal").off().on("click", function() {
            $("#container-all-add-input").hide(500);
            $("#container-all-update").show(500);
        });

        $("#btn-to-annuler-select-type").off().on("click", function() {
            $("#container-all-type-select").hide(500);
            $("#container-all-update").show(500);
        });

        $("#btn-use-myaccount, #btn-use-other-account").off().on("click", function() {
            setUserAccount();
        });

        $("#container-all-type-select #rechercheType").off().on("input", function() {
            showAllType();
        });

        function setUserAccount() {
            const params = {
                setUser : true,
                user : $("#user-account").val(),
                password : $("#password-account").val(),
            }
            ajaxPost(
                null,
                baseUrl + '/api/convert/osm', 
                params,
                function(data) {
                    $("#container-config-account").hide(500);
                    $("#container-all-type-select").show(500);
                }, {
                    async: false
                }
            );
        }

       
        function contribution() {
            var updateParams = [{
                    value: $("#type-val-contribution").val(),
                    key: $("#type-key-contribution").val(),
                },
                {
                    value: $("#name-contribution").val(),
                    key: "name"
                },
                {
                    value: $("#shortDescription-contribution").val(),
                    key: "description"
                },
                {
                    value: $("#fauteuil-roulant-contribution").val(),
                    key: "wheelchair",
                },
                {
                    value: $("#website-contribution").val(),
                    key: "contact:website"
                },
                {
                    value: $("#wikidataId-contribution").val(),
                    key: "wikidata"
                },

                {
                    value: $("#contact_phone-contribution").val(),
                    key: "contact:phone"
                },
            ];

            $.each(newInput, function(key, input) {
                if ($("#" + input + "-contribution") != null && typeof $("#" + input + "-contribution").data("tag") != "undefined") {
                    updateParams.push({
                        value: $("#" + input + "-contribution").val(),
                        key: input.replace("_", ":"),
                    });
                }
            });
            
            ajaxPost(
                null,
                baseUrl + '/api/convert/osm', {
                    contribution: {
                        lat : lat,
                        lon : lon,
                        create: updateParams,
                    },
                    comment : $("#commentaire-contribution").val(),
                    contextId : costum.contextId,
                    contextType : costum.contextType,
                },
                function(data) {
                    mylog.log("eeeeeeeeeeeeeeeeeeeeeeee", data);
                    if(data != null && typeof data.res != "undefined" && data.res) {
                        var urlResult = "https://www.openstreetmap.org/changeset/" + data.changesetId;
                        if(typeof data.elementId != "undefined")
                            urlResult = "https://www.openstreetmap.org/node/" + data.elementId;




                        var link = '. <a target="_blank" href="'+urlResult+'">'+tradCms.editionSucces +' Vous pouver voir le resultat ici.</a>'
                        toastr.success(link);

                        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']"); 
                    }
                    else 
                        toastr.error(tradCms.somethingWrong)
                    
                }, {
                    async: false
                }
            );
        }

        function addNewElementInMap(element , map) {
            var markerAdd = {};
            $.each(element, function(key, val) {
                markerAdd["elt"] = val;
            });
            
            map.addMarker(markerAdd);
            /* let bounds =  map.getMap().getBounds();
            let center = bounds.getCenter();

            let customIcon = L.icon({
                iconSize: [60, 60], 
                iconAnchor: [20, 40]
            });
            
            myMarkerAdd = L.marker(center, { icon : customIcon, draggable: true }).addTo(map.getMap());

            if(buttonAddInContribution != null) {
                map.getMap().removeControl(buttonAddInContribution);
                buttonAddInContribution = null;
            } */
        }

        function getNameType(tag, types, res) {
            if (types != null) {
                $.each(types, function(key, type) {

                    if (type != null && typeof type.name != "undefined" && typeof type.element != "undefined") {

                        res = getNameType(tag, type.element, res);

                    } else if (type != null && typeof type.name != "undefined" && typeof type.type != "undefined" && typeof type.tag != "undefined") {

                        $.each(tag, function(kk, vv) {
                            if (type.type == vv && vv != "yes")
                                res = {
                                    name: type.name,
                                    type: type.type,
                                    key: type.tag
                                };
                        });


                    }
                });
            }
            return res;
        }

        function getType(tag) {
            var res = {
                name: "",
                type: "",
                key: ""
            };
            if (tag.length > 0) {
                res = getNameType(tag, osmAllTYpe, res);
            }
            return res;
        }

        function getNameAllType(types, res) {
            if (types != null) {
                $.each(types, function(key, val) {
                    if (val != null && typeof val.name != "undefined" && typeof val.element != "undefined") {
                        res = getNameAllType(val.element, res);
                    } else if (val != null && typeof val.name != "undefined" && typeof val.type != "undefined" && typeof val.tag != "undefined") {
                        res.push(val);
                    }
                });
            }
            return res;
        }

        function getAllType() {
            var res = [];
            res = getNameAllType(osmAllTYpe, res);
            return res;
        }

        function showAllType() {
            $("#selectType").html("");
            var str = "";
            var filtre = $("#container-all-type-select #rechercheType").val();
            $.each(allTypeDataOsm, function(key, val){
                if(val != null) {
                    let icon = (typeof val.icon != "undefined") ? val.icon : "fa fa-map-marker";
                    let title = (typeof val.name != "undefined") ? val.name : "";
                    let descripionType = (typeof val.tag != "undefined" && val.tag != "" && typeof val.type != "undefined" && val.type != "") ? val.tag +"="+ val.type : "";
                    let tagT = (typeof val.tag != "undefined") ? val.tag : "" ;
                    let typeT = (typeof val.type != "undefined") ? val.type : "" ;
                    if(filtre == "" || ( filtre != "" && (title.indexOf(filtre) !== -1 || tagT.indexOf(filtre) !== -1 || typeT.indexOf(filtre) !== -1))) {
                        str = "";
                        str += '<div class="col-sm-12 select-new-type-contribution"><div class="thumbnail " style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">';
                        str += '<div class="caption card-footer ">';
                        str += '<div class="row">'+'<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="min-height:70px;background-color: rgb(239, 146, 40);margin-left:9px;border-radius:5% 0% 0% 5%;display: flex; justify-content: center; align-items: center;">';
                        str += '<i style="color:#ffffff;height:100%;width:100%;font-size:30px;" class="'+icon+'"></i>';
                        str += '</div><div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" style="margin-top:9px;">';
                        str += '<h5 class="name-new-type">'+ title +'</h5>';
                        str += '<p class="description-type description-new-type">' + descripionType + '</p>';
                        
                        

                        str += '<input type="hidden"  name="type" class="form-control select-new-type-tag" value="'+ tagT +'">';
                        str += '<input type="hidden"  name="key" class="form-control  select-new-type-type" value="'+ typeT +'">'
                        str += '<input type="hidden"  name="icon" class="form-control  select-new-type-icon" value="'+ icon +'">'
                        str += '</div></div></div></div></div>';
                        
                        $("#selectType").append(str);
                    }
                }
            });

            $(".select-new-type-contribution").off().on("click", function(){
                $("#type-element-to-contribution").html($(this).find(".name-new-type").html());

                $("#icon-type-element-to-contribution").removeClass();
                $("#icon-type-element-to-contribution").addClass($(this).find(".select-new-type-icon").val());
                $("#description-type-element-to-contribution").html($(this).find(".description-new-type").html());
                $("#type-val-contribution").val($(this).find(".select-new-type-type").val());
                $("#type-key-contribution").val($(this).find(".select-new-type-tag").val());

                $("#container-all-type-select").hide(500);
                $("#container-all-update").show(500);
            });
        }

        function refreshInputForType() {
            var input = [];
            const generalInput = [

            ];
            var type = $("#type-val-contribution").val();
            if (type != "") {
                input = getInputForType(type, osmAllTYpe, input);
                setInputForType(input, type);
                setInputForType(generalInputOsmContribution, type);

                $(".select-new-input-contribution").off().on("click", function() {
                    var inputTmp = [];

                    inputTmp = getInputInGeneralInputByType($(this).data("tag").replace("_", ":"), inputTmp);


                    if (inputTmp.length == 0)
                        inputTmp = getInputForType($(this).data("type"), osmAllTYpe, inputTmp);


                    for (i = 0; i < inputTmp.length; i++) {
                        if (inputTmp[i] != null && typeof inputTmp[i].name != "undefined" && typeof inputTmp[i].tag != "undefined" && typeof inputTmp[i].type != "undefined" && inputTmp[i].tag == $(this).data("tag")) {
                            strTmp = creatInputForType(inputTmp[i]);
                            $("#container-input-contribution").append(strTmp);
                            newInput.push(inputTmp[i].tag.replace(":", "_"));
                            $("#container-all-add-input").hide(500);
                            $("#container-all-update").show(500);
                        }
                    }

                });
            }
        }

        function setInputForType(inputs, type) {
            if (typeof inputs != "undefined" && inputs != null && inputs.length > 0) {
                for (i = 0; i < inputs.length; i++) {
                    if (inputs[i] != null && typeof inputs[i].name != "undefined" && typeof inputs[i].tag != "undefined" && typeof inputs[i].type != "undefined") {
                        var alreadyUse = false;
                        
                        if(newInput.indexOf(inputs[i].tag.replace(":", "_")) === -1) {
                            str = `
                                <div data-type="` + type + `"  data-tag="` + inputs[i].tag + `" class="select-new-input-contribution col-sm-12" id="select-new-input-contribution-` + inputs[i].tag.replace(":", "_") + "-" + i + `">
                                    <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                                        <div class="caption card-footer">
                                            <div class="form-group input-group-sm   ">
                                                <h5 id="type-element-to-contribution" class="switch-text-label">` + inputs[i].name + `</h5>
                                                <p   class="description-type">` + inputs[i].tag + "=*" + `</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `;
                            $("#selectNewInputContributionModal").append(str);
                            allInputForSearche.push({
                                id: "#select-new-input-contribution-" + inputs[i].tag.replace(":", "_") + "-" + i,
                                name: inputs[i].name,
                                tag: inputs[i].tag,
                            });
                        }
                        
                    }
                }
            }
        }

        function creatInputForType(input) {
            var str = "";
            if (input != null && typeof input.name != "undefined" && typeof input.tag != "undefined" && typeof input.type != "undefined") {
                str += `
                    <div class="col-sm-12">
                        <div class="thumbnail" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);">
                            <div class="caption card-footer">
                                <div class="form-group input-group-sm   ">
                                    <label class="switch-text-label" for="` + input.tag.replace(":", "_") + `-contribution">
                                        ` + input.name + `
                                    </label>`;

                if (input.type == "text")
                    str += `<input data-tag="` + input.tag.replace(":", "_") + `" type="text" id="` + input.tag.replace(":", "_") + `-contribution" name="` + input.tag.replace(":", "_") + `" class="form-control  contribution-input" placeholder="` + input.name + `">`
                else if (input.type == "select") {
                    str += `<select data-tag="` + input.tag.replace(":", "_") + `" id="` + input.tag.replace(":", "_") + `-contribution" name="` + input.tag.replace(":", "_") + `" class="form-control  contribution-input">
                            <option value=""></option>`;
                    if (typeof input.option != "undefined" && input.option != null) {
                        for (let i = 0; i < input.option.length; i++) {
                            if (typeof input.option[i] != "undefined" && input.option[i] != null && typeof input.option[i].label != "undefined" && typeof input.option[i].value != "undefined") {
                                str += '<option value="' + input.option[i].value + '">' + input.option[i].label + '</option>';
                            }

                        }
                    }
                    str += `</select>`;
                }

                str += `</div></div></div></div>`;
            }
            return str;
        }

        function getInputForType(tag, types, res) {
            if (types != null) {
                $.each(types, function(key, type) {

                    if (type != null && typeof type.name != "undefined" && typeof type.element != "undefined") {

                        res = getInputForType(tag, type.element, res);

                    } else if (type != null && typeof type.name != "undefined" && typeof type.type != "undefined" && typeof type.tag != "undefined") {
                        if (type.type == tag) {

                            if (typeof type.input != undefined)
                                res = type.input;
                                
                        }

                    }
                });
            }
            return res;
        }

        function getInputInGeneralInputByType(tag, res) {
            res = [];
            $.each(generalInputOsmContribution, function(key, val) {
                if (val != null && typeof val.type != "undefined" && val.tag == tag)
                    res = val;
            });
            if (res.length == 0)
                return res;
            return [res];
        }
    });
</script>