
<?php 

    $cssAnsScriptFilesTheme = array(
        "/plugins/jquery-counterUp/waypoints.min.js",
        "/plugins/jquery-counterUp/jquery.counterup.min.js",
    ); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>


<style>
.dash-content {
  padding: 60px;
}

h4 {
  font-family: "Arial";
  font-weight: 500;
  font-size: 25px;
  color: #aeaeae;
  text-transform: uppercase;
  margin: 0;
}

hr {
  margin-bottom: 16px;
  border: 1px solid #eaeaea;
}

.border-card {
  background: #fff;
  margin-bottom: 5px;
  /*display: flex;*/
  align-items: center;
  font-family: "Arial";
  font-size: 14px;
  padding: 5px 16px;
  cursor: pointer;
  border-radius: 4px;
  border: 1px solid #eaeaea;
  box-shadow: 0px 2px 1px 0px rgba(0, 0, 0, 0.1);
  transition: all 0.25s ease;
}
.border-card:hover {
  -webkit-transform: translateY(-1px);
          transform: translateY(-1px);
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.1), 0 5px 10px 0 rgba(0, 0, 0, 0.1);
}
.border-card.over {
  background: rgba(70, 222, 151, 0.15);
  padding-top: 24px;
  padding-bottom: 24px;
  border: 2px solid #47DE97;
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0), 0 5px 10px 0 rgba(0, 0, 0, 0);
}
.border-card.over .card-type-icon {
  color: #47DE97 !important;
}
.border-card.over p {
  color: #47DE97 !important;
}

.content-wrapper {
  display: flex;
  justify-content: flex-start;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  transition: all 0.25s ease;
}

.min-gap {
  flex: 0 0 40px;
}

.card-type-icon {
  flex-grow: 0;
  flex-shrink: 0;
  margin-right: 16px;
  font-weight: 400;
  color: #323232;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  text-align: center;
  line-height: 40px;
  font-size: 14px;
  transition: all 0.25s ease;
}
.card-type-icon.with-border {
  color: #aeaeae;
  border: 1px solid #eaeaea;
}
.card-type-icon i {
  line-height: 40px;
}

.label-group {
  white-space: nowrap;
  overflow: hidden;
}
.label-group.fixed {
  flex-shrink: 0;
}
.label-group p {
  margin: 0px;
  line-height: 21px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.label-group p.title {
  color: #323232;
  font-weight: 500;
  width: 250px;
}
.label-group p.title.cta {
  text-transform: uppercase;
}
.label-group p.caption {
  font-weight: 400;
  /*color: #aeaeae;*/
  color: orange;
  width: 250px;
}

.end-icon {
  margin-left: 16px;
}

fieldset {
  position: relative;
  border: 0;
  padding: 0;
}

.control {
  display: inline-flex;
  position: relative;
  margin: 5px;
  cursor: pointer;
  border-radius: 99em;
}

.control input {
  position: absolute;
  opacity: 0;
  z-index: -1;
}

.control svg {
  margin-right: 6px;
  border-radius: 50%;
  box-shadow: 3px 3px 16px rgba(0, 0, 0, .2)
}

.control__content {
  display: inline-flex;
  align-items: center;
  padding: 6px 12px;
  font-size: 20px;
  line-height: 32px;
  color: rgba(0, 0, 0, .55);
  background-color: rgba(0, 0, 0, .05);
  border-radius: 99em;
}

.control:hover .control__content {
  background-color: rgba(0, 0, 0, .1);
}

.control input:focus ~ .control__content {
  box-shadow: 0 0 0 .25rem rgba(12, 242, 143, .2);
  background-color: rgba(0, 0, 0, .1);
}

.control input:checked ~ .control__content {
  background-color: rgba(12, 242, 143, .2);
}

.banner-img {
    display: block;
    margin-left: auto;
    margin-right: auto;
}

@media only screen and (min-width: 1000px) {
  .banner-img {
    height: 200px;
  }
}

@media only screen and (max-width: 1200px){
  .banner-img {
    height: 100px;
  }
}

.description {
  padding: 60px;
  padding-bottom: 0px !important;
}

.description-block {
    font-size: 20px;
    border-left: 4px solid <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    padding-left: 15px;
    font-weight: bold;
}

.btn-soutien {
    display: block;
    margin-left: auto;
    margin-right: auto;
    background-color:  <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;
    border-radius: 5px;
    color: #fff;
    width: 300px;
    font-size: 20px;
}

    .bgFormColor{
        background-color: <?php echo Yii::app()->session["costum"]["colors"]["dark"]; ?>;
    }
    .bgFormColorTxt{
      color : <?php echo Yii::app()->session["costum"]["colors"]["grey"]; ?>;
    }
</style>

<div class="bgFormColor" style=" z-index: 5; ">
  <img  class="banner-img"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/lartevolution.jpg" >
</div>

<div class="description bgFormColor bgFormColorTxt"  style="padding: 60px">
  <h1 style="color:white;" class="text-center">Fêtes de l'art évolution</h1>
    <p style="padding-top: 20px;"">
      Tous les artistes et les citoyens mobilisés et en action pour une fete Libre, une réappropriation de l’espace commun culturel.
    </p>
    <p class="description-block"> un temps de partage créatif.</p>
    <p>Un appel à tous les artistes à venir faire leur art avec du sens et aux citoyens de partager un petit bout d’art, toujours avec un message du monde dont on rêve.</p>
    <p class="description-block">Un moment de partage populaire.</p>
    <p class="description-block">La culture est libre et le restera</p>
    <p class="description-block">Nous sommes la culture.</p>

    <p>un déploiement spontané, éphémère et réccurent. Jamais organiser en rassemblement.</p>

    <p class="description-block">c’est l’art qui rassemblera</p>

    <p style="margin-bottom: 30px">Tout les types d’arts, et sutout l’art citoyen ouvert à tous.</p>

<?php 
  $allreadyR = true;
  // var_dump();
 

  if( Person::logguedAndValid() ){ 
    foreach ($allAnswers as $key => $value) 
    {
        if (Yii::app()->session["userId"] == $value["user"] && isset($value["answers"]) ) 
          $allreadyR = false;
    }
    ?>
        <a class="btn btn-soutien lbh" href="#" > Je soutiens et je participe </a>
<?php } else { ?>
    <button  data-toggle="modal" data-target="#modalLogin" class="btn btn-soutien" >Je soutiens et je participe</button>
<?php } ?>
</div>

<div class="bgFormColor text-center">
  <img style="width:80%;margin:20px auto; "  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/map-10.png" >
</div>

<div class="dash-content bgFormColor">
<div>
<h4 style="color: <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>">Les Artistes en actions</h4>
</div>
<hr>

<div class="border-card bgFormColor">
<div class="content-wrapper">
        <div class="label-group fixed">
            <p class="caption">Nom</p>
        </div>
        
        <div class="min-gap"></div>
        
       <!--  <div class="label-group fixed">
            <p class="title"><?php if(isset($value["answers"]["communityForm1"]["communityForm12"])){ echo $value["answers"]["communityForm1"]["communityForm12"] ; } ?></p>
             <p class="caption">Email</p> 
        </div> -->

        <div class="min-gap"></div>

        <div class="label-group fixed">
            <p class="caption">Adresse</p>
        </div>

        <div class="min-gap"></div>

        <div class="label-group">
            <p class="caption">Spécialité</p>
        </div>

    </div>
  </div>

<?php foreach ($allAnswers as $key => $value) {
        if(isset($value["answers"])){
?>

<div class="border-card bgFormColor">
    <!-- <div class="card-type-icon with-border">1</div> -->
    
    <div class="content-wrapper">
        <div class="label-group fixed ">
            <p class="title text-white"><?php if(isset($value["answers"]["communityForm1"]["communityForm11"])){ echo $value["answers"]["communityForm1"]["communityForm11"] ; } ?></p>
            <!-- <p class="caption">Nom</p> -->
        </div>
        
       <div class="min-gap"></div>

        <div class="label-group fixed ">
            <p class="title text-white"><?php if(isset($value["answers"]["communityForm1"]["communityForm13"])){ echo $value["answers"]["communityForm1"]["communityForm13"] ; } ?></p>
            <!-- <p class="caption">Adresse</p> -->
        </div>

        <div class="min-gap"></div>

        <div class="label-group">
            <p class="title text-white"><?php if(isset($value["answers"]["communityForm1"]["communityForm15"])){
                if(is_array($value["answers"]["communityForm1"]["communityForm15"])) {
                    foreach ($value["answers"]["communityForm1"]["communityForm15"] as $k => $v) {
                      echo $v["liste_row"]; echo " | ";
                    }
                }
              } ?></p>
        </div>

    </div>

</div>


<?php }} ?>



<div class="description bgFormColor bgFormColorTxt"  style="padding: 60px">
  <h1 style="color:white;" class="text-center">Fêtes votre part (évolution)</h1>
    <p style="padding-top: 20px;"">
      Tous les artistes et les citoyens mobilisés, signifie 
    </p>
    <p class="description-block"> l’ubiquité , mouvement se passe partout au meme moment</p>
    
    <p>Chacun porte le message qui lui correspond le plus</p>
    <p class="description-block"> Porter un Message Libre</p>
    <p>Pas de marques , que du soutien ( Je suis …, , Je suis charlie, Je suis Floyd, Je suis Adama, je suis la Foret, je suis L’océan, Je suis 100 NOMS... )</p>
    <p>Utiliser toute les images que vous voulez, créez et partagez les votre. </p>
    <a class="btn btn-soutien lbh" href="#" >Inscrit ton initiative</a></p>
</div>


<div class="bgFormColor text-center">
  <img style="width:80%;margin:20px auto; "  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/lune-lune-lune.jpg" >
</div>


<div class="description bgFormColorReverse bgFormColorTxt"  style="border:2px solid <?php echo Yii::app()->session["costum"]["colors"]["pink"]; ?>;padding: 30px 60px; margin: 30px 0;">
  <h1 style="color:white;" class="text-center">Slogans</h1>
    
      <p class="description-block"> Le coup de clairon
      <p class="description-block"> Faites l’art évolution
      <p class="description-block"> L’Art envahit les rues, et Nos rêves sont trop grands pour vos cases
      <p class="description-block"> célébrer les changements de saison
      <p class="description-block"> La liberté de penser, de créer
      <p class="description-block"> Désobeissance artistique
      <p class="description-block"> Nos rêves sont trop grands pour vos cases
      <p class="description-block"> La terre native sur une terre nette
      <p class="description-block"> Pleine lune pour une pleine conscience culturelle
      <p class="description-block"> N’importe qui n’importe où , peut faire sa part de culture citoyenne
    
</div>

<!-- 
<div class="bgFormColor text-center">
  <img style="width:80%;margin:20px auto; "  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/artevolution-11.png" >
</div>
<img style="width:100%;transform: rotate(180deg);"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/community/murmurations-sunset-bg2.jpg" > -->