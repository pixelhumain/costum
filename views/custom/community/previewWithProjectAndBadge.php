<?php 
$isEmpty = true;
$isBadgeShowed = false;
$isBadgeConfigurable = false;
$labelsJson = json_encode($params['actionsLast12Month']['labels'] ?? []);
$dataJson = json_encode($params['actionsLast12Month']['data'] ?? []);
$gitlab = (!empty($params["socialNetwork"]["gitlab"])? $params["socialNetwork"]["gitlab"]:"javascript:;") ;
$telegram =  (!empty($params["socialNetwork"]["telegram"])? $params["socialNetwork"]["telegram"]:"javascript:;") ;
$diaspora =  (!empty($params["socialNetwork"]["diaspora"])? $params["socialNetwork"]["diaspora"]:"javascript:;") ;
$mastodon =  (!empty($params["socialNetwork"]["mastodon"])? $params["socialNetwork"]["mastodon"]:"javascript:;") ;
$facebook = (!empty($params["socialNetwork"]["facebook"])? $params["socialNetwork"]["facebook"]:"javascript:;") ;
$twitter =  (!empty($params["socialNetwork"]["twitter"])? $params["socialNetwork"]["twitter"]:"javascript:;") ;
$github =  (!empty($params["socialNetwork"]["github"])? $params["socialNetwork"]["github"]:"javascript:;") ;
$instagram =  (!empty($params["socialNetwork"]["instagram"])? $params["socialNetwork"]["instagram"]:"javascript:;") ;
$signal =  (!empty($params["socialNetwork"]["signal"])? $params["socialNetwork"]["signal"]:"javascript:;") ;
$linkedin =  (!empty($params["socialNetwork"]["linkedin"])? $params["socialNetwork"]["linkedin"]:"javascript:;") ;
$skype =  (!empty($params["socialNetwork"]["skype"])? $params["socialNetwork"]["skype"]:"javascript:;") ;
$credits = $params["userCredits"] ?? 0;
$actionsCreatedtodo = $params["actionsCreated"]["todo"] ?? 0;
$actionsCreateddone = $params["actionsCreated"]["done"] ?? 0;
$actionsParicipatedtodo = $params["actionsParticipated"]["todo"] ?? 0;
$actionsParicipateddone = $params["actionsParticipated"]["done"] ?? 0;
$participationsDone = $params["countAll"]["done"] ?? 0;
$participationsTodo = $params["countAll"]["todo"] ?? 0;
$allParticipations = $participationsDone + $participationsTodo;
$cssAnsScriptFilesTheme = array(
    "/plugins/Chart-2.8.0/Chart.min.js",
    "/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
    // "/plugins/material/material-dashboard.min.css",
); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

function sortByOrder($a, $b)
{
    return (isset($a["order"]) ? $a["order"] : 99999) < (isset($b["order"]) ? $b["order"] : 99999) ? -1 : 1;
}

function is_associative_array($array) {
    if (array_keys($array) !== range(0, count($array) - 1)) {
        return true;  // Associative array (keys are not numeric or not in sequence)
    }
    return false;  // Indexed array (numeric and sequential keys)
}

?>
<style>
    h4 {
        text-align: center;
        text-transform: none;
        color: #333;
    }

    #modal-preview-coop {
        padding: 2%;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0,0,0,0.1);
        left: unset;
        right: unset;
        background-color: transparent;
        width: 100%;
        display: flex !important;
        justify-content:center;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.805);
    }

    .resourcePreview {
        padding: 20px;
        padding: 20px;
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        border-radius: 10px;
    }

    .resourcePreview .btn-tag-panel span {
        font-size: 13px;
        color: #ffefef;
        background: #00b5c0;
    }

    .resourcePreview hr {
        height: 2px;
        background: #000;
        margin: 10px 35%;
    }

    .image-placeholder {
        height: 15vh;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: #e9e9e9;
        border: 2px dashed #ccc;
        margin-bottom: 20px;
    }

    .image-placeholder img {
        max-height: 100%;
        max-width: 100%;
    }

    .btn-tag-preview {
        font-weight: 300;
        padding: 5px 10px;
        background: #f3f3f3;
        color: #333;
        border: 1px solid #ccc;
        margin-top: 5px;
        border-radius: 5px;
        display: inline-block;
    }

    .tags-list {
        padding: 0;
        list-style-type: none;
    }

    .tags-list a {
        text-decoration: none;
    }

    .address {
        margin-top: 20px;
        font-size: 16px;
        color: #0c2035;
        text-align: center;
    }

    .address i {
        margin-right: 5px;
        font-weight: bold;
    }

    #close-graph-modal {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
        margin-top: -25px;
        margin-right: -15px;
    }

    .div-img img {
        max-width: 60%;
        border-radius: 10px;
    }

    .div-img {
        margin-bottom: 15px;
        padding-top: 5px;
        padding-bottom: 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .resourcePreview h4 {
        position: relative;
        padding: 10px 20px;
        /* margin: 1px 0px;
        margin-left: -15px;
        margin-right: -15px; */
        line-height: 20px;
        font-weight: bold;
        color: #fff;
        background: #2BB0C6;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        zoom: 1;
        text-transform: none;
        width: -webkit-fill-available;
    }

    .resourcePreview h4::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 0;
        border-width: 0 14px 12px 0;
        border-style: solid;
        border-color: transparent #115052;
    }

    .resourcePreview h4::after {
        right: 0 !important;
        border-width: 0 0 12px 14px !important;
        left: auto;
        content: "";
        position: absolute;
        top: 100%;
        border-style: solid;
        border-color: transparent #115052;
    }

    .resourcePreview .address h5 {
        text-transform: none;
        font-size: 17px;
    }

    .resourcePreview .address h5 span {
        text-transform: none;
        font-size: 17px;
    }

    .resourcePreview .typeElement {
        margin-bottom: 10px;
    }

    .previews-elements hr.hr10 {
        margin-bottom: 10px;
        margin-top: 10px;
        border: 0;
        border-top: 1px solid #eee;
    }

    .previews-elements .btn-link {
        color: #fff;
        background-color: #9fbd38;
        border: 1px solid #fff;
        border-radius: 4px;
        margin: 4px;
        min-width: 105px;
    }

    .resourcePreview::-webkit-scrollbar {
        width: 5px; 
    }

    .resourcePreview::-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1);
    }

    .resourcePreview::-webkit-scrollbar-thumb {
        background-color: rgba(255, 255, 255, 0.5); 
        border-radius: 10px; 
    }

    .resourcePreview::-webkit-scrollbar-thumb:hover {
        background-color: rgba(255, 255, 255, 0.8); 
    }

    .containerPreviewWithProjectAndBadge {
        background-color: white;
        color: black;
        padding: 30px;
        box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.3);
        left: 50%;
        border-radius: 15px;
        min-width: 40vw;
    }
    
    #badgeContainer {
        border-bottom: 2px solid black;
    }

    .profile-cardPreviewWithProjectAndBadge {
        text-align: center;
    }

    .headerPreviewWithProjectAndBadge img {
        width: 70px;
        height: 70px;
        object-fit: cover;
        margin-bottom: 15px;
    }

    #projectCard img {
        width: 70px;
        height: 70px;
        object-fit: cover;
        margin-bottom: 15px;
    }

    .headerPreviewWithProjectAndBadge h1 {
        font-size: 24px;
        font-weight: bold;
        margin-bottom: 10px;
        font-family: serif !important;
    }

    .headerPreviewWithProjectAndBadge .profile-img{
        width: 150px;
        height: 150px;
        border-radius: 50%;
    }

    .headerPreviewWithProjectAndBadge p {
        margin: 5px 0;
        font-size: 14px;
    }

    .sectionPreviewWithProjectAndBadge {
        margin: 20px 0;
    }

    .sectionPreviewWithProjectAndBadge h2 {
        font-size: 18px;
        margin-bottom: 10px;
        font-weight: bold;
        letter-spacing: 1px;
    }

    .sectionPreviewWithProjectAndBadge p {
        font-size: 14px;
        line-height: 1.5;
    }


    .previewProjectimage-grid {
        display: flex;
        justify-content: center;
        align-items: center; 
        flex-wrap: wrap;       
        background-color: white;
        margin: 5px;
        padding: 15px;
    }

    .previewBadgeimage-grid {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(70px, 1fr));
        justify-content: center;
        align-items: center; 
        flex-wrap: wrap;       
        background-color: white;
        margin: 5px;
        padding: 15px;
    }

    .previewProjectAndbadgeimage-item {
        flex: 1 1 70px;
        width: 100%;
        display: block;
        margin: 5px;
        display: flex;
        justify-content: center;
    }

    .tags {
        padding: 5px;
    }

    .tags h1 {
        position:relative; font-size:20px; font-weight:700;  letter-spacing:0px; text-transform:uppercase; width:max-content; text-align:center; margin:auto; white-space:nowrap; border:2px solid #222;padding:5px 11px 3px 11px;
    }
    .tags h1:before, .tags h1:after {
        background-color: #c50000;
        position:absolute; 
        content: '';
        height: 7px;

        width: 7px; border-radius:50%;
        bottom: 12px;
    }
    .tags h1:before {
        left:-20px;
    }
    .tags h1:after {
        right:-20px;
    }

    #projectCard{
        box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.3);
        padding: 10px;
    }

    .main-body {
        padding: 15px;
        background-color: white;
        border-radius: 4px;
    }
    .card {
        /* box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06); */
        box-shadow: 1px 0 3px 2px rgb(0 0 0 / 20%)
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0,0,0,.125);
        border-radius: .25rem;
    }

    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }

    .gutters-sm {
        margin-right: -8px;
        margin-left: -8px;
    }

    .gutters-sm>.col, .gutters-sm>[class*=col-] {
        padding-right: 8px;
        padding-left: 8px;
    }
    .mb-3, .my-3 {
        margin-bottom: 1rem!important;
        border-radius: 4px ;
    }

    .mb-3 ul{
        border-radius: 4px !important ;
    }

    .bg-gray-300 {
        background-color: #e2e8f0;
    }
    .h-100 {
        height: 100%!important;
        border-radius: 4px;
    }
    .shadow-none {
        box-shadow: none!important;
    }

    .list-social {
        display : flex;
        flex-direction: column ;

    }

    .btn-network {
        display: inline-block;
        width: 50px;
        height: 50px;
        background: #fff/*#f1f1f1*/;
        margin: 10px;
        border-radius: 30%;
        box-shadow: 0 5px 15px -5px #2C3E50;
        color: #1da1f2;
        overflow: hidden;
        position: relative;
        border: 1px solid #1d3b58;
    }

    a.btn-network:hover {
        transform: scale(1.3);
        color: #f1f1f1;
    }

    .btn-network i {
        line-height: 50px;
        font-size: 25px;
        transition: 0.2s linear;
    }
    .btn-network img {
        margin: 12px;
        width: 25px;
        transition: 0.2s linear;
    }

    .btn-network:hover i, .btn-network:hover img {
        transform: scale(1.3);
        color: #f1f1f1;
    }

    .fa-facebook {
        color: #3c5a99;
    }
    .fa-twitter {
        color: #1da1f2;20px
    }
    .fa-telegram{
        color: #55acee;
    }
    .fa-instagram {
        color: #c94d82;
    }
    .fa-github {
        color: #444;
    }
    .fa-linkedin {
        color: #1da1f2;
    }

    .btn-network::before {
        content: "";
        position: absolute;
        width: 120%;
        height: 120%;
        background: #1d3b58;
        transform: rotate(45deg);
        left: -110%;
        top: 90%;
    }

    .btn-network:hover::before {
        animation: aaa 0.7s 1;
        top: -10%;
        left: -10%;
    }

    .middle {
        text-align: center;
    }

    /*widget css*/
    .card-bodyWidget{
        display: inline-block;
        font-family: "Roboto", sans-serif;
        margin: 10px;
        padding-left: 5px;
        height: 90px;
        color: #fff;
        border-radius: 5px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
    }
    .float-left{
        float: left;
    }
    .float-right{
        float: right;
    }
    .card-bodyWidget h3{
        margin-top: 15px;
        margin-bottom: 5px;
        color: black;
    }
    .count, .count{
        font-size: 30px;
        font-weight: 500;
    }
    .card-bodyWidget p{
        font-size: 16px;
        margin-top: 0;
        color: black;
    }

    .card-bodyWidget svg{
        width: 70px;
        opacity: 0.6;
        margin-top: 10px;
        margin-right: 5px;
        color: black;
        margin-top: 15px;
    }

    .color1{
        background: #e54e5e;
    }
    .color2{
        background: #0091c6;
    }
    .color3{
        background: #68ca91;
    }

    .color4{
        background: #f61201
    }

    @media screen and (max-width: 768px) {
        #close-graph-modal{
            margin-top: -17px;
            margin-right: 25px;
        }
    }

</style>
<div class="previews-elements">
    <div>
        <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <div id="resourcePreview" class="resourcePreview">
        <div class="container">
            <div class="main-body">
                <div class="row gutters-sm">
                    <div class="col-md-4">

                        <div class="card mb-3">
                            <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center headerPreviewWithProjectAndBadge">
                                <img src="<?= $params["profileImg"] ?>" alt="Admin" class="profile-img" width="150">
                                <div class="mt-3">
                                    <h4><?= $params["name"] ?></h4>
                                </div>
                            </div>
                            </div>
                        </div>

                        <?php if (!empty($params["socialNetwork"])): ?>
                            <div class="card mb-3">
                                <div class="middle">
                                    <span id="divDiaspora">
                                        <?php if ($diaspora != "javascript:;"){ ?>
                                            <a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Diaspora">
                                                <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/diaspora_icon.png">
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divMastodon">
                                        <?php if ($mastodon != "javascript:;"){ ?>
                                            <a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout"  class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Mastodon">
                                                <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/mastodon.png">
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divFacebook">
                                        <?php if ($facebook != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" data-toggle="tooltip" data-placement="top" title="Facebook">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divTwitter">
                                        <?php if ($twitter != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" data-toggle="tooltip" data-placement="top" title="Twitter">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divSkype">
                                        <?php if ($skype != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $skype ; ?>" target="_blank" id="skypeAbout" data-toggle="tooltip" data-placement="top" title="Skype">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divInstagram">
                                        <?php if ($instagram != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" data-toggle="tooltip" data-placement="top" title="Instagram">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divSignal">
                                        <?php if ($signal != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $signal ; ?>" target="_blank" id="signalAbout" data-toggle="tooltip" data-placement="top" title="Signal">
                                                <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/signal_icon.png" >
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divLinkedin">
                                        <?php if ($linkedin != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $linkedin ; ?>" target="_blank" id="linkedinAbout" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                                            <i class="fa fa-linkedin"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divGithub">
                                        <?php if ($github != "javascript:;"){ ?>
                                            <a class="contentInformation btn-network tooltips" href="<?php echo $github ; ?>" target="_blank" id="githubAbout" data-toggle="tooltip" data-placement="top" title="Github">
                                                <i class="fa fa-github"></i>
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divGitlab">
                                        <?php if ($gitlab != "javascript:;"){ ?>
                                            <a href="<?php echo $gitlab ; ?>" class="contentInformation btn-network tooltips"  target="_blank" id="gitlabAbout" data-toggle="tooltip" data-placement="top" title="Gitlab">
                                                <img src="<?php echo Yii::app()->getModule("co2")->assetsUrl; ?>/images/gitlab_icon.png" >
                                            </a>
                                        <?php } ?>
                                    </span>

                                    <span id="divTelegram">
                                        <?php if ($telegram != "javascript:;"){ ?>
                                            <a href="<?php echo $telegram ; ?>" target="_blank" id="telegramAbout" class="contentInformation btn-network tooltips" data-toggle="tooltip" data-placement="top" title="Telegram">
                                                <i class="fa fa-telegram"></i>
                                            </a>
                                        <?php } ?>
                                    </span>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($params["badgesDetails"])): 
                                $badges = $params["badgesDetails"];
                                uasort($badges, "sortByOrder");
                                $badgeAssigned = null;
                                ?>
                            <div class="card mb-3">
                                        <div class="tags">
                                            <h1>Badges</h1>
                                        </div>
                                        <div class="previewBadgeimage-grid" id="badgeContainer">
                                    <?php foreach ($badges as $key => $value): ?>
                                            <div class="previewProjectAndbadgeimage-item" data-name="<?= $value["name"] ?>">
                                                <img class="img-responsive badge-image" data-toggle="tooltip" title="<?= $value["name"] ?>" src="<?= $value["image"] ?>" />
                                            </div>
                                    <?php endforeach; ?>
                                        </div>
                            </div>

                        <?php endif; ?>

                        <div class="card mb-3">
                            <div class="card-bodyWidget color1">
                                <div class="float-left">
                                    <h3>
                                        <span class="count"> <?= $credits ?> </span>
                                    </h3>
                                    <p> Credits </p>
                                </div>
                                <div class="float-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M0 432c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V256H0v176zm192-68c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-40zm-128 0c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H76c-6.6 0-12-5.4-12-12v-40zM576 80v48H0V80c0-26.5 21.5-48 48-48h480c26.5 0 48 21.5 48 48z"/></svg>
                                </div>

                            </div>
                            
                            <div class="card-bodyWidget color2">
                                <div class="float-left">
                                    <h3>
                                        <span class="count"> <?= $allParticipations ?> </span>
                                    </h3>
                                    <p><?php echo Yii::t("cms", "Shareholdings")?></p>
                                </div>
                                <div class="float-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M488 192H336v56c0 39.7-32.3 72-72 72s-72-32.3-72-72V126.4l-64.9 39C107.8 176.9 96 197.8 96 220.2v47.3l-80 46.2C.7 322.5-4.6 342.1 4.3 357.4l80 138.6c8.8 15.3 28.4 20.5 43.7 11.7L231.4 448H368c35.3 0 64-28.7 64-64h16c17.7 0 32-14.3 32-32v-64h8c13.3 0 24-10.7 24-24v-48c0-13.3-10.7-24-24-24zm147.7-37.4L555.7 16C546.9 .7 527.3-4.5 512 4.3L408.6 64H306.4c-12 0-23.7 3.4-33.9 9.7L239 94.6c-9.4 5.8-15 16.1-15 27.1V248c0 22.1 17.9 40 40 40s40-17.9 40-40v-88h184c30.9 0 56 25.1 56 56v28.5l80-46.2c15.3-8.9 20.5-28.4 11.7-43.7z"/></svg>
                                </div>

                            </div>
                            <div class="card-bodyWidget color3">
                                <div class="float-left">
                                    <h3>
                                        <span class="count"> <?= $participationsDone ?> </span>
                                    </h3>
                                    <p><?php echo Yii::t("cms", "Complete actions")?></p>
                                </div>
                                <div class="float-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zM227.3 387.3l184-184c6.2-6.2 6.2-16.4 0-22.6l-22.6-22.6c-6.2-6.2-16.4-6.2-22.6 0L216 308.1l-70.1-70.1c-6.2-6.2-16.4-6.2-22.6 0l-22.6 22.6c-6.2 6.2-6.2 16.4 0 22.6l104 104c6.2 6.2 16.4 6.2 22.6 0z"/></svg>
                                </div>

                            </div>
                            <div class="card-bodyWidget color4">
                                <div class="float-left">
                                    <h3>
                                        <span class="count"> <?= $participationsTodo ?> </span>
                                    </h3>
                                    <p><?php echo Yii::t("cms", "Actions to do")?></p>
                                </div>
                                <div class="float-right">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M464 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-6 400H54a6 6 0 0 1 -6-6V86a6 6 0 0 1 6-6h404a6 6 0 0 1 6 6v340a6 6 0 0 1 -6 6zm-42-92v24c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-24c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12zm0-96v24c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-24c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12zm0-96v24c0 6.6-5.4 12-12 12H204c-6.6 0-12-5.4-12-12v-24c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12zm-252 12c0 19.9-16.1 36-36 36s-36-16.1-36-36 16.1-36 36-36 36 16.1 36 36zm0 96c0 19.9-16.1 36-36 36s-36-16.1-36-36 16.1-36 36-36 36 16.1 36 36zm0 96c0 19.9-16.1 36-36 36s-36-16.1-36-36 16.1-36 36-36 36 16.1 36 36z"/></svg>
                                </div>

                            </div>
                        </div>  
                        
                    </div>

                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <?php if (!empty($params["projects"])): 
                                    $project = $params["projects"];
                                ?>
                                        <div class="tags">
                                            <h1><?php echo Yii::t("cms", "Projects contributions")?></h1>
                                        </div>
                                        <div class="previewProjectimage-grid" id="projectContainer">
                                    <?php  foreach ($project as $key => $value): ?>
                                        <div class="previewProjectAndbadgeimage-item" data-name="<?= $value["name"] ?>">
                                            <div style="text-align: center;">
                                                <div id="projectCard" style="display: inline-block; text-align: center;">
                                                    <span><?= $value["name"] ?></span>
                                                    <img class="img-responsive project-image" 
                                                        data-toggle="tooltip" title="<?= $value["name"] ?>" 
                                                        src="<?= $value["profilImageUrl"] ?>" 
                                                        style="display: block; margin: 0 auto;" />
                                                    <span><?= $value["activityCount"] ?> Actions</span>
                                                </div>  
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                        </div>
                                <?php endif; ?>
                            </div>
                        </div>
                                        
                        <div class="row gutters-sm">

                            <div class="col-sm-6 mb-3">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="d-flex align-items-center mb-3"> <?php echo Yii::t("cms", "Actions created")?> </h6>
                                            <canvas class="" id="pieChartCreated<?php echo $params["userId"]?>"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 mb-3">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="d-flex align-items-center mb-3"> Participations </h6>
                                            <canvas class="" id="pieChartParticipated<?php echo $params["userId"]?>"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <div class="card-body">
                                    <h6 class="d-flex align-items-center mb-3"> <?php echo Yii::t("cms", "Last 12 months' activity")?> </h6>
                                        <canvas class="" id="lineChart12Month<?php echo $params["userId"]?>"></canvas>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
    </div>
</div>

<script>

    $('.count').each(function(){
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration:2000,
                easing:'swing',
                step: function(now){
                    $(this).text(Math.ceil(now));
                }
            });
    });

    var createdChartData = [{
        data: [<?= $actionsCreatedtodo ?>,<?= $actionsCreateddone ?>,],
        weight: 1,
        datalabels : {
            anchor: 'end',
            align: 'end',
            color:['Red','Yellow'],
            font : {
                size : 13,
                weight: 'bold'
            }
        },
        backgroundColor: [
            '#e54e5e',
            '#68ca91',
        ],

    }];

    var participatedChartData = [{
        data: [<?= $actionsParicipatedtodo ?>,<?= $actionsParicipateddone ?>,],
        weight: 1,
        datalabels : {
            anchor: 'end',
            align: 'end',
            color:['Red','Yellow'],
            font : {
                size : 13,
                weight: 'bold'
            }
        },
        backgroundColor: [
            '#e54e5e',
            '#68ca91',
        ],

    }];

    var chart<?php echo $params["userId"] ?> = document.getElementById('pieChartCreated<?php echo $params["userId"]?>').getContext('2d');
    window.myChart<?php echo $params["userId"]?> = new Chart(chart<?php echo $params["userId"]?>, {
        type: 'pie',
        data: {
            labels: [
                "<?php echo Yii::t("cms", "To do")?>",
                "<?php echo Yii::t("cms", "Done")?>",
            ],
            datasets: createdChartData
        },
        options: {
            scales: {
            y: { 
                beginAtZero: true
            }
            }
        }

    });

    var chart<?php echo $params["userId"] ?> = document.getElementById('pieChartParticipated<?php echo $params["userId"]?>').getContext('2d');
    window.myChart<?php echo $params["userId"]?> = new Chart(chart<?php echo $params["userId"]?>, {
        type: 'pie',
        data: {
            labels: [
                "<?php echo Yii::t("cms", "To do")?>",
                "<?php echo Yii::t("cms", "Done")?>",
            ],
            datasets: participatedChartData
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }

    });

    const labels = <?php echo $labelsJson; ?>;
    const data = {
        labels: labels,
        datasets: [{
            label: "Actions",
            data: <?php echo $dataJson; ?>,
            fill: false,
            borderColor: '#e54e5e',
            tension: 0.1
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    };

    var myChart = new Chart(
        document.getElementById('lineChart12Month<?php echo $params["userId"]?>'),
        config
    );


</script>