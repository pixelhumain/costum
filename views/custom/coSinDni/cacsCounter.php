
<style>
.counter-content-<?= $kunik ?>{
    position: relative;
    width: 100%;
    min-height: 200px;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: rgba(0, 0, 0, 0.05) 0px 0px 0px 1px;
}
</style>
<div class="row">
    <div class="col-xs-12 no-padding">
        <div class="col-xs-6">
            <div class="counter-content-<?= $kunik ?> cac-counter-content-<?= $kunik ?>">
                
            </div>
        </div>
        <div class="col-xs-6">
            <div class="counter-content-<?= $kunik ?> action-counter-content-<?= $kunik ?>">
                
            </div>
        </div>
    </div> 
</div>
<script>
    $(function(){
        const cacsObj = {
            init : function(cobj){
                cobj.countCacs(cobj);
                cobj.countActions(cobj);
            },
            data : {},
            countCacs : function(cobj){
                ajaxPost(null, baseUrl + "/co2/aap/cacs/action/listcacs/slug/"+costum.slug,
                    {
                        indexStep: 0,
                        notSourceKey : true,
                    },
                    function (data) {
                        if(data.result){
                            cacsObj.data = data.results;
                            mylog.log(cacsObj.data,"cacsObj.data");
                            $('.cac-counter-content-<?= $kunik ?>').html(`<h1>${Object.keys(cacsObj.data).length} CACS</h1>`)
                        }
                    }, null,null,{async:false}
                );
            },
            countActions : function(cobj){
                params = {      
                    searchType : ["answers"],
                    fields : ["urls","address","geo","geoPosition"],
                    indexStep :0,
                    indexMin :0,
                    notSourceKey : true,          
                };
                ajaxPost(
                    null,
                    baseUrl + "/co2/aap/cacs/action/globalautocompleteaction/slug/"+costum.slug,
                    params,
                    function(data){
                        mylog.log(data,"dataargentina");
                        $('.action-counter-content-<?= $kunik ?>').html(`<h1>${Object.keys(data.results).length} Actions</h1>`);
                    }
                )
            }
        }
        cacsObj.init(cacsObj);
    })
</script>