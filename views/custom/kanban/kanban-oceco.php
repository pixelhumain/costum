<style type="text/css">
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-kanban-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>

<?php 
    use yii\base\Widget;
    use modules\costum\components\blockCms\KanbanWidget;

    $cssAnsScriptFilesModule = array(
	    '/js/default/preview.js'
    );

    $kanbanAssets = [
        '/plugins/kanban/kanban.js',  '/plugins/kanban/kanban.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $kanbanAssets,
        Yii::app()->request->baseUrl
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<div id="preview-kanban" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10 kanban-header">
        <span class="pull-left kanban-title">
            <h4 class="titre-context"></h4>
        </span>
		<button id="close-kanban-modal" class="btn btn-default pull-right btn-close-modal btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
	</div>
	<div class="container-preview padding-10" style="overflow-y: auto;">
        <?php     
            $params = array(
                'kunik' => "kanban",
                'el' => [],
                'blockKey' => "kanban",
                "blockCms" => array(
                    "test"  => "",
                )
            );

            echo $this->renderPartial('costum.views.tpls.blockCms.kanban.oceco', $params);
        ?>
	</div>
</div>
<script>
    $(function() {
        let titreContext = "Les actions";
        if(contextData!=null && typeof contextData.name != 'undefined') {
            titreContext = 'Les actions dans : '+ contextData.name
        }else if(typeof costum != "undefined" && typeof costum.title != "undefined"){
            titreContext = 'Les actions dans : '+ costum.title
        }

        $('.titre-context').text(titreContext)
    })
</script>