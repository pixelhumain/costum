<style type="text/css">
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-kanban-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>

<?php 
    use yii\base\Widget;
    use modules\costum\components\blockCms\KanbanWidget;

    $cssAnsScriptFilesModule = array(
	    '/js/default/preview.js'
    );
    
    $roles = [];
    if(isset($context["type"]) && isset($context["_id"])){
        $roles = PHDB::findOne($context["type"], ['_id' => new MongoId($context["_id"]['$id'])],["roles"]);
        $roles = $roles["roles"] ?? []; 
    }

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<div id="preview-kanban" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10 kanban-header">
        <span class="pull-left kanban-title">
            <h4 class="titre-role">Gestion des rôles</h4>
        </span>
		<button id="close-kanban-modal" class="btn btn-default pull-right btn-close-modal btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
	</div>
	<div class="container-preview padding-10" style="overflow-y: auto;">
        <?php     
            $params = array(
                'kunik' => $context["_id"]['$id'],
                'el' => [],
                'blockKey' => "kanban",
                "blockCms" => array(
                    "test"  => "",
                ),
                "roles" => $roles
            );

            echo $this->renderPartial('costum.views.tpls.blockCms.kanban.kanban', $params);
        ?>
	</div>
</div>
<script>
    $(function() {
        let titreRole = "Gestion des rôles";
        if(contextData!=null && typeof contextData.name != 'undefined') {
            titreRole = 'Gestion des rôles de : '+ contextData.name
        }else if(typeof costum != "undefined" && typeof costum.title != "undefined"){
            titreRole = 'Gestion des rôles de : '+ costum.title
        }

        $('.titre-role').text(titreRole)
    })
</script>