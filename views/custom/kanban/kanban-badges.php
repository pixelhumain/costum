<style type="text/css">
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-badges-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
</style>

<?php 
    use yii\base\Widget;
    use modules\costum\components\blockCms\BadgesWidget;

    $cssAnsScriptFilesModule = array(
	    '/js/default/preview.js'
    );

    $badgesAssets = [
        '/plugins/kanban/kanban.js',  '/plugins/kanban/kanban.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $badgesAssets,
        Yii::app()->request->baseUrl
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<div id="preview-badges" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10">
        <span class="pull-left">
            <h4 class="titre-badge">Gestion des badges</h4>
        </span>
		<button id="close-badges-modal" class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button>
	</div>
	<div class="container-preview padding-10" style="overflow-y: auto;">
        <?php     
            $params = array(
                'kunik' => "badges",
                'el' => [],
                'blockKey' => "badges",
                "blockCms" => array(
                    "test"  => "",
                )
            );

            echo $this->renderPartial('costum.views.tpls.blockCms.kanban.badges', $params);
        ?>
	</div>
</div>
<script>
    $(function() { 
        if(typeof contextData != 'undefined' && typeof contextData.name != 'undefined') {
            let titreBadge = 'Gestion des badges de : '+contextData.name
            $('.titre-badge').text(titreBadge)
        }
    })
</script>