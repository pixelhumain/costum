<style type="text/css">
	.searchBar-filters{
		width: 60%;
		margin: auto !important;
		float: inherit !important;
	}
	.searchBar-filters .search-bar{
		width: 100%;
    	border: 1px solid #5b2648;
    }
    .searchBar-filters .main-search-bar-addon{
    	background-color: #5b2648 !important;
	    border: 1px solid #5a2647;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
    }
    .container-filters-menu{
    	text-align: center;
    }
</style>
<div class="col-xs-12 padding-20" id="searchInMesures"></div>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var paramsFilter= {
	 	container : "#searchInMesures",
	 	interface : {
	 		events : {
	 			scroll : true,
	 			scrollOne : true
	 		}
	 	},
	 	defaults:{
	 		types : ["poi"],
	 		indexStep : "0",
	 		type:"measure",
	 		forced:{
	 			type:"measure"
	 		},
	 		sortBy : [ 
                "name"
            ]
	 	},
	 	filters : {
	 		text : {
	 			placeholder : "Rechercher parmi les mesures"
	 		},
	 		tags : {
	 			view : "horizontalList",
	 			event : "filters",
	 			classList : "bg-purple btn-key-cloud-unik",
	 			dom : ".tags-cloud",
	 			multiple:true,
	 			type : "tags",
	 			active : true,
	 			list : [
	                "Climat et biodiversité",
	                "Justice sociale et solidarité",
	                "Démocratie et citoyenneté",
	                "Économie",
	                "Commune",
	                "Intercommunalité"
            	]
	 		}
	 	}
	};
	var filterSearch={};
	jQuery(document).ready(function() {
		mylog.log('filterSearch paramsFilter', paramsFilter);
		$(".tags-cloud").empty();
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.search.callBack = function(fObj, results){
			//mylog.log("startSearch directory.js", typeof callBack, callBack, loadingData);
			fObj.search.currentlyLoading = false;
			fObj.search.obj.count=false;
			fObj.search.obj.countType=[];
			fObj.filters.actions.text.spin(fObj, false);
			//if(typeof fObj[fObj.search.loadEvent.active].callBack != "undefined") fObj[fObj.search.loadEvent.active].callBack(fObj,results);
			if(results.length < fObj.search.obj.indexStep){
				fObj[fObj.search.loadEvent.active].stopPropagation=true;
				$(fObj.results.dom+" .processingLoader").remove();
			}
			inc=0;
			$(".bodySearchContainer.mesures .searchEntityContainer").each(function(){
				title=$(this).find(".entityName").text();
			//	alert(title);
				if(title.indexOf("# ") >= 0)
					$(this).addClass("principes");
			});
			countPR=$(".bodySearchContainer.mesures .searchEntityContainer.principes").length;
			inc=1;
			$(".bodySearchContainer.mesures .searchEntityContainer.principes").each(function(){
				if(countPR==inc)
					$(this).addClass("margin-bottom-20");
				inc++;
			});
			if($("#activeFilters").is(":visible")){
				$("#activeFilters").remove();
			}
			/*if($(".app-mesures").length > 0 && $(".app-mesures #main-search-bar").length <= 0){
				inpSearch='<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center margin-top-20"><input type="text" class="form-control pull-left text-center main-search-bar" id="main-search-bar" placeholder="Rechercher parmi les mesures">'+
		    		'<span class="text-white input-group-addon pull-left main-search-bar-addon" id="main-search-bar-addon">'+
		        		'<i class="fa fa-arrow-circle-right"></i></div>'+
		    		'</span>';
				$(".app-mesures").prepend(inpSearch);
				//searchInterface.setSearchbar();
			}
			if($(".bodySearchContainer.mesures .searchEntityContainer").length <= 0){
				$("#dropdown_search").append("<span class='text-purple col-xs-12 text-center margin-top-50' style='text-transform:uppercase;font-size:20px;'>Aucun résultat</span>");
		    } */ 	
		};
		/*filterSearch.results.end= function(fObj){
			return "";
			//return "<span class='text-purple col-xs-12 text-center margin-top-50' style='text-transform:uppercase;font-size:20px;'>Aucun résultat</span>";
		}*/
	});
</script>