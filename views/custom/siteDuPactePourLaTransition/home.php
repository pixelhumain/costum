<?php 
$cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',              
    );
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
/*$cssAnsScriptFilesModule = array(
    '/js/classes/CO3_Element.js',
    '/js/classes/CO3_Poi.js',
    '/js/classes/CO3_Article.js',
    '/js/classes/CO3_Event.js',
    '/js/classes/CO3_Obj.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());*/

$articles_une=Poi::getPoiByWhereSortAndLimit(array("rank"=>"true", "source.key"=>"siteDuPactePourLaTransition"),array("updated"=>-1), 4, 0);
?>
<style type="text/css">
	#bg-homepage {
		width: 100%;
	}
	.start-img{
		filter: brightness(0.4);
		-webkit-filter: brightness(0.4);
	}
	.intituteHomePunch{
		color: white;
	    text-align: right;
	    position: absolute;
	    font-weight: 900;
	    font-size: 40px;
	    left: 0px;
	}
	.puceAlignHome .puceInc{
		padding: 10px 20px;
		background-color: #5b2649;
		color: white;
		text-align: center;
	}
	.puceAlignHome .puceTextExplain{
		padding-right: 0px;
	}
	.btn-redirect-home {
        font-size: 22px;
	    border-radius: 6px;
	    color: white !important;
	    padding: 8px 10px;
	    background-color: #5b2649 !important;
	}
	@media (min-width: 992px) {
		#actus .btn-redirect-home {
	        float: right;
	    	margin-right: 50px;
		}
	}
	@media (max-width: 460px){
		span.intituteHomePunch{
			/*text-align: center;*/
			font-size: 18px;
		}
	}
	@media (max-width: 768px){
		.intituteHomePunch{
			/*text-align: center;*/
			font-size: 30px;
		}
	}
	@media (min-width: 1000px){
		.intituteHomePunch{
			font-size: 50px;
		}
	}
	@media (min-width: 1250px){
		.intituteHomePunch{
			font-size: 60px;
		}
	}
	.BtnFiltersCollectif {
	    position: absolute;
	    z-index: 10;
	    top: 10px;
	    right: 10px;
	    max-width: 25%;
	    font-size: 18px;
	}
	.showBtn {
		color: #5b2649!important;
    	background-color: #ffa200
    	border-color: #fbae55;/*#428bca*/
	}
	#colActif{
		background-color: #c6793d;
	}
	#colSoutien{
		background-color: #ffa200;
	}
	#elus{
		background-color: #428bca;
	}
	.list-group .list-group-item {
	    padding: 5px 10px;
	}
	.filtreMap {
		color: #5b2649!important;
		/*padding-left: 35px;*/
	}
	.filtreMap:hover, .filtreMap:active, .filtreMap:focus{
		background-color: #5b2649!important;
    	color: #fff!important;
	}
	#elus:hover i, #elus:active i, #elus:focus i{
		color: #428bca!important;
		transform: rotate(360deg);
	}
	#colSoutien:hover i, #colSoutien:active i, #colSoutien:focus i{
		color: #ffa200!important;
		transform: rotate(360deg);
	}
	#colActif:hover i, #colActif:active i, #colActif:focus i{
		color: #c6793d!important;
		transform: rotate(360deg);
	}
	.section-newActu{
		padding: 40px 20px 0px 20px;
	}
	.tooltip.top .tooltip-inner {
		background-color:#000;
		padding:5px 20px;
		/*opacity:0.9;*/
		border-radius:0;
		border-top:1px solid red;
	}
	.tooltip.top .tooltip-arrow {
		border-top-color:#000;
		/*opacity:0.9;*/
	}
	.icon-info {
		color: #fff;
	    font-size: 24px;
	    transition: all .3s ease-out;
	    /*position: absolute;
	    left: 5px;*/
	}
	@media (max-width: 767px) {
		.BtnFiltersCollectif {    
			max-width: 40%;
		    font-size: 14px;
		}
		.icon-info {
    		font-size: 18px;
		}
		.filtreMap {
			padding-right: 20px;
		}	
	}
</style>
<div class="col-xs-12 no-padding">
	<div id="start" class="section-home section-home-video">
		<div class="col-xs-12 content-video-home no-padding">
			<div class="col-xs-12 no-padding container-video">
				<!--<a href="javascript:;" id="launchVideo">
				<i class="fa fa-play-circle"></i>
				</a>-->
				<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/couverture_pacte.png'>
			<!--
				<span class="intituteHomePunch col-xs-12 col-sm-12 col-md-10">
					Adoptons <u>32 mesures</u> concrètes pour<br/> des communes plus écologiques et<br/> plus justes.
				</span>
			-->
			</div>
		</div>
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<!--<div class="col-md-12 col-sm-12 col-xs-12 padding-20 section-home" style="padding-left:100px; margin-top:0px;background-color: #f6f6f6;">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top: -19px;margin-bottom: -18px;background-color: #fff;font-size: 14px;z-index: 5;">
        <h3 class="col-xs-12 text-center">
                <small style="text-align: left">
  color: #2b2b2b;
  text-shadow: 1px 1px 0px rgba(0,0,0,0.1);">
				<span class="main-title" style="color:#5b2649;   font-size: 45px;">Construire ensemble<br/>les communes de demain</span>
				<br/>
				<hr class="col-xs-4 col-xs-offset-4" style="border-top: 3px solid #fda521;margin-top: 35px;margin-bottom: 40px;">
				<br/>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-users" style="color:#5b2649;"></i><br/><br/>
					Les habitant.es <br/> définissent les priorités<br/> pour leurs communes
				</div>
				<div  class="col-sm-2 col-xs-4 col-xs-offset-4 col-sm-offset-0 margin-top-10 margin-bottom-10">
					<img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/logo-min.png'>
				</div>
				<div  class="col-sm-5 col-xs-12 text-center" style="font-size: 1.4em;padding-right:30px;">
					<i class="fa fa-2x fa-university" style="color:#5b2649;"></i><br/><br/>
					Les candidat.es <br/>s'engagent à les mettre en oeuvre<br/> une fois élu.es
				</div>
				</bloquote>
				</h3>
           </div>
    </div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<div id="search" class="section-home col-xs-12 bg-purple padding-20" style="margin-top:0px;color:white; padding-bottom: 40px;">
		<!-- <div class="col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 header-section">
			<h3 class="text-center">S'engager dans votre commune en trouvant</h3>
		</div> -->
		<div class="col-xs-12">
			<h3 class="text-center"><i class="fa fa-search"></i> Que se passe-t-il dans <span class="text-orange">ma commune</span> ?</h3>
             <div class="col-xs-12 text-center content-input-scope-pacte"></div>
        </div>
       
        
		<!-- <div class="col-xs-12">
			<h3 class="text-center"><span class="text-orange">Mais</span> aussi...</h3>
			<a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-2 col-xs-12">
					
				Les<br/>
              	<span class="text-orange number-btn">32</span><br>
				Mesures
			</a>
			<a href="javascript:;" data-hash="#toolkit" class="col-md-4 col-sm-6 col-xs-12 lbh-menu-app btn btn-redirect-home">
				Et ...<br>
				<span class="text-orange">Une multitude</span><br>
				des ressources
			</a>
	
		</div> -->
	</div>
	<div  id="mapPacte" class="section-home col-xs-12 no-padding no-margin" style="height: 450px"></div>
   
	<!--<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<!-- ACTUALITÉ-->
	<!--<div id="newActu" class=" col-xs-12 section-newActu"></div> -->
		
	<div id="actus" class="section-home col-xs-12 no-margin">
		<div class="col-xs-12 header-section text-center">
			<!--<h3 class="title-section col-sm-8 col-xs-12">
				<i class="fa fa-rss"></i> L'actualité du Pacte
			</h3>-->
			<a href="javascript:;" data-hash="#actualites" class="lbh-menu-app btn btn-redirect-home btn-small-orange">Toute l'actu<span class="hidden-sm hidden-xs">alité</span></a>
			<!--<hr/>-->
		</div>
		<div class="col-xs-12" id="actus-pacte">
		</div>
	</div>

	<div class="col-xs-12">
		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3 margin-top-10">
			<a href="https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn btn-redirect-home col-xs-12 pull-right text-center"
			style="font-size: 22px !important;">Rester informé⋅e</a>
		</div>
		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-lg-3 margin-top-10">
		<a href="https://www.helloasso.com/associations/collectif-pour-une-transition-citoyenne/formulaires/3" target="_blank" class="btn btn-redirect-home col-xs-12 pull-left text-center"
			style="font-size: 22px !important;">Faire un don</a>
		</div>
	</div>

	<div id="partenaire" class="col-xs-12 support-section section-home no-margin">
	 	<!--<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 no-padding" style="font-size: 20px;">
        	<div class="puceAlignHome col-xs-12">
        		<span class="puceInc col-xs-1">1</span><span class="puceTextExplain col-xs-11">Le Pacte pour la Transition propose 32 mesures concrètes pour une transition locale</span>
        	</div>
        	<div class="puceAlignHome col-xs-12 margin-top-10">
        		<span class="puceInc col-xs-1">2</span><span class="puceTextExplain col-xs-11"> Les habitant·es définissent les mesures prioritaires pour leurs communes</span>
        	</div>
        	<div class="puceAlignHome col-xs-12 margin-top-10">
        		<span class="puceInc col-xs-1">3</span><span class="puceTextExplain col-xs-11"> Les candidat·es aux Municipales de 2020 s’engagent à les mettre en œuvre une fois élu·es</span>
        	</div>
        </div>
        <div class="col-xs-12">
        	<center><h4><a href="javascript:;" data-hash="#mesures" class="lbh-menu-app btn btn-redirect-home col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3 col-xs-12">Voir les mesures</a></h4></center>
        </div>
        <div class="col-xs-12 header-section" style="margin-bottom: 0px;">
        	<h3 class="title-section col-xs-12 text-center" style="margin-top: 35px;">50+ organisations</h3>
        </div>-->
        <div class="col-xs-12 no-padding">
          <img class="img-responsive hidden-xs" style="margin: auto;width: 100%" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte.png'>
          <img class="img-responsive visible-xs" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/partenaires_pacte_xs.png'>

          
        </div>
        <!--<div class="col-xs-12 header-section" style="margin-bottom: 0px;">
        	<h3 class="col-xs-12 text-center" style="margin-top: 0px;">et des milliers de citoyen·nes
			</h3>
        </div>-->
	</div>

	<!--<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>
	<div id="search" class="section-home col-xs-12 no-padding" style="margin-top: 0px;">
		<img class="img-responsive" style="margin: auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/bande_site.png'>
	</div>
	<div class="col-xs-12 section-separtor no-padding">
		<div class="col-xs-4 bg-orange"></div>
		<div class="col-xs-4 bg-blue"></div>
		<div class="col-xs-4 bg-orange"></div>
	</div>-->
	<!--<div id="useit" class="section-home col-md-10 col-md-offset-1">
		<div class="col-xs-12 header-section">
			<h3 id="porterPacte" class="title-section col-sm-8">Vous voulez participer hors de votre commune ?</h3>
			<hr/>
		</div>
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 padding-20 text-explain" >
			<!-- <a href="https://framaforms.org/devenir-benevole-pour-le-pacte-pour-la-tran
			sition-1551263682" target="_blank" class="btn-main-menu col-xs-12 col-sm-6 btn-redirect-home btn-useit"  >
				<div class="text-center">
				  <div class="col-md-12 no-padding text-center">
				      <h4 class="no-margin uppercase">
				        <i class="fa fa-group"></i><br/>
				        <?php //echo Yii::t("home","Devenir<br/>bénévole") ?>
				      </h4>
				  </div>
				</div>
			</a> -->

			<!--<a href=" https://my.sendinblue.com/users/subscribe/js_id/3t7aq/id/1" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 pull-right btn-redirect-home btn-useit"  >

			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center ">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-rss"></i><br/>
			            <?php echo Yii::t("home","Vous abonner<br/> à la Newsletter") ?>
			          </h4>
			      </div>
			  </div>
			</a>
			<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu col-xs-12 col-sm-5  margin-top-20 btn-redirect-home btn-useit">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-envelope"></i><br/>
			            <?php echo Yii::t("home","Nous<br/> écrire") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->
			<!--<a href="https://www.helloasso.com/associations/collectif-pour-une-transitio
			n-citoyenne/formulaires/2" target="_blank" class="btn-main-menu col-xs-12 col-sm-4 margin-top-20 btn-redirect-home btn-useit pull-right">
			  <div class="text-center">
			      <div class="col-md-12 no-padding text-center">
			          <h4 class="no-margin uppercase">
			            <i class="fa fa-heart"></i><br/>
			            <?php echo Yii::t("home","Faire un don") ?>
			          </h4>
			      </div>
			  </div>
			</a>-->

      <!--</div>-->
	   <!--  <div class="text-center col-xs-12 col-md-8 col-md-offset-2 text-center text-orange margin-top-20"><b><span style="font-size:30px;">...Et en parler tout autour de vous !!</span></b>
	    </div> -->
    <!--</div>-->
   
</div>


<div class="portfolio-modal modal fade text-center py-5"  id="popupCollectif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
        	<div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body" style="font-size: 22px">
                <div class="text-center contain-logo">
                    <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/PACTE-TRANSITION-LOGOTYPEcouleurs.png" alt="image"/>
                </div>
                <div id="text-head-collectif"></div>
                <div id="linkForm" class="padding-link-buttom">

				</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	var imgH=0;
	var articles_une = "";
	if (typeof mapPacteHome == "undefined") 
		var mapPacteHome = {};
	var dataSearchPacte = {} ;
	var resultMapGroup={};
	var mapLoadPacte=false;
	function specImage($this){
		imgH=$($this).height();
		$("#launchVideo").css({"line-height": imgH+"px"});
		$(".container-video").css({"max-height": imgH+"px"});
		$("#launchVideo").click(function(){
			str='<div style="padding:0 0 0 0;position:relative;height:'+imgH+'px;">'+
					'<iframe src="https://player.vimeo.com/video/106193787?title=0&byline=0&portrait=0&autoplay=1" '+' style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" '+  'webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'+
				'</div>';
			$(".container-video").addClass("col-md-10 col-md-offset-1").html(str);	
		});
	}
	function initTextInBand(time){
		punchH=$(".intituteHomePunch").height();
		imgH=$(".container-video").height();
		posTopPunch=(imgH-punchH)/2;
		$(".intituteHomePunch").css({"top":posTopPunch+"px"});
		time+=500;
		if(time < 100000)
			setTimeout(function(){initTextInBand(time)}, time);
	}
	function initWelcomePacte(){
		initTextInBand(0);
		articles_une = new CO3_Article(null,<?php echo json_encode($articles_une); ?>);
		pacte.initScopeObj();
		$("#actus-pacte").html(articles_une.SetColNum(2).SetDisplaySocial(false).RenderHtml());
		coInterface.bindLBHLinks();	
	}
	function initPacteMapView(){
		if(typeof mapPacteHome != "undefined" && typeof mapPacteHome.clearMap != "undefined") { 
			mapPacteHome.clearMap();
		}
		 // allMaps.clear();
		mapLoadPacte=true;
	  pacte.initScopeObj();
	  var paramsMapPacte = {
	    zoom : 5,
	    container : "mapPacte",
	    activePopUp : false,
	    tile : "maptiler",
	    center : ["47.482649", "2.431357"]
	    // mapOpt:{
	    //   latLon : ["47.482649", "2.431357"]
	    // }
	  };
	  var mapPacteHome = mapObj.init(paramsMapPacte);
	  //timeoutLoadingPacte(0);
	  dataSearchPacte=searchInterface.constructObjectAndUrl();
	  dataSearchPacte.searchType = ["organizations"];
	  dataSearchPacte.indexStep=0;
	  pacte.mapDefault();
	  //dataSearchPacte.searchType = ["NGO","LocalBusiness","Group","GovernmentOrganization"];
	  dataSearchPacte.private = true;
	  //dataSearchPacte.sourceKey = costum.slug;
	  //var dataResult;
	  ajaxPost(
	  	null,
	  	baseUrl+"/costum/pacte/mapsearch",
	  	dataSearchPacte,
	  	function(data){ 
	      mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
	      if(!data){ 
	        toastr.error(data.content); 
	      } 
	      else{
	      	resultMapGroup=data.result;
	        mapPacteHome.addElts(data.result, true);
	        mylog.log("data.result",data.result);
	        var dataResult  =  data.result;

		        //Show btn filtre
		    $('#mapPacte').append( '<div class="pull-right BtnFiltersCollectif"></div>');
            $('.BtnFiltersCollectif').html( 
			  '<div id="mainMenuFilter ">'+
			    '<div class="list-group panel">  '+
			        '<a href="javascript:;" data-category="soutien"class="filtreMap list-group-item text-center" id="colSoutien"><span class="tooltips" data-toggle="tooltip" data-placement="top" title="Une ou plusieurs personnes soutiennent le Pacte dans cette commune"><i class="icon-info fa fa-info-circle"></i></span> Soutiens du Pacte</a>'+
			        '<a href="javascript:;" data-category="actif" class="filtreMap list-group-item text-center" id="colActif"><span class="tooltips" data-toggle="tooltip" data-placement="top" title="Plusieurs personnes se sont réunies pour former un collectif citoyen et s’investissent autour du Pacte dans cette commune"><i class="icon-info fa fa-info-circle"></i></span> Collectifs actifs</a>'+
			        '<a href="javascript:;" data-category="actifSigned" data-category3="undefined" class="filtreMap list-group-item text-center" id="elus"><span class="tooltips"  data-toggle="tooltip" data-placement="top" title=" Le collectif citoyen actif sur cette commune a obtenu un engagement des élu·es majoritaires pour au moins une des communes sur lesquelles il est actif"><i class="icon-info fa fa-info-circle"></i></span> Collectifs engagés</a>'+
			    '</div>'+
			  '</div>'
			);

            $('.filtreMap').off().on("click", function(){
           		mapPacteHome.clearMap();     
           		mapPacteHome.addElts(filterCategory(dataResult,$( this ).data( "category" ) ));
           	});
	      }
	    }, 
	  	function(data){
	  		$("#dropdown_search").html(data.responseText);  
		    mylog.log(">>> error autocomplete search"); 
		    mylog.dir(data);   
		    //signal que le chargement est terminé
		    loadingData = false;
		}, "json",
		{"headers": {"Accept-Encoding": "None"}});
	 
}
	//new function
	function filterCategory(result,category){
		var resultR=[];
		$.each( result, function( key, value ) {
			if (category == "actifSigned" && value["category"] == "actifSigned") {
				resultR.push(value);
			}else if (category == "actif" && $.inArray(value["category"], ["actif", "actifSigned"]) >= 0) {
				resultR.push(value);
			}
			else if(category=="soutien"){
				resultR.push(value);
			}
			
		});
		return resultR;
	}

	function lazyWelcome(time){
	  if(typeof CO3_Article != "undefined" && typeof pacte != "undefined"){
	  	if(!mapLoadPacte)
	  		initPacteMapView();
	    initWelcomePacte();

	  }else
	    setTimeout(function(){
	      lazyWelcome(time+200)
	    }, time);
	}
	jQuery(document).ready(function() {
		setTitle("Pacte pour la Transition");
		lazyWelcome(0);
		$('.form-register .emailRegister label').html('<i class="fa fa-envelope"></i>&nbsp;E-mail');
		/***************set default image****************************/
		$('#actus-pacte a.container-img-profil').each(function(){
			if($(this).text() == "undefined" || $(this).text() == " "){
				$(this).css({"height": "360px"});
				$(this).html('<i class=\'fa fa-picture-o fa-5x\' style=\'position:absolute;top:50%;left:50%;transform:translate(-50%,-92%);margin:0px !important;font-size:133px !important\'></i>');
			}
				
				
		});

		var inputScope = $('input[placeholder="Code postal ..."]');
		var placeholderValue = inputScope.attr('placeholder'); 
		inputScope.attr('placeholder','Code postal ou nom de la commune');

		
		var inputScope = $('input[placeholder="Code postal ..."]');
		var placeholderValue = inputScope.attr('placeholder'); 
		inputScope.attr('placeholder','Code postal ou nom de la commune');

		var $imgs = $('a.container-img-profil .img-responsive')
		$imgs.each(function(){
			var $img = $(this)
			$img.attr("src", $img.attr('src').replace("/medium", ""));
		});

		setTimeout(function(){ 
			$('[data-toggle="tooltip"]').tooltip(); 
		}, 1000);
		

	});



</script>
