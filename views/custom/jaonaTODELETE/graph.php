<!DOCTYPE html>
<html lang="en">

<?php
if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"]);
}
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CV</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/css/jaona/timeline.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/css/jaona/cv.css">
</head>

<body>
    <div id="graph-container">
        <svg id="graph">
        </svg>
        <button id="btn-retour" style="display:none;" class="btn">Retour</button>
    </div>
    <script>
        var dataTimeline = [];
        var rootMindMap = {
            id: "root",
            name: "Mes projets"
        };
        var rootDendo = {
            id: "root",
            name: "Competences"
        }
        var dataDendo = {
            ...rootDendo
        }
        var dataMindMap = {
            ...rootMindMap
        };
        const dateOptions = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };

        function refreshDataTimeline(width, height) {
            dataTimeline = [];
            getAjax('', baseUrl + '/' + moduleId + '/element/getdatadetail/type/' + costum.contextType + '/id/' + costum.contextId + '/dataName/events/sort/0', (data) => {
                for (const parcours of Object.values(data)) {
                    var d = {}
                    d["id"] = parcours.id;
                    d["title"] = parcours.name;
                    d["description"] = parcours.shortDescription ?? "";
                    d["debut"] = new Date(parcours.startDate).toLocaleDateString("fr-FR", dateOptions)
                    d["fin"] = new Date(parcours.endDate).toLocaleDateString("fr-FR", dateOptions)
                    dataTimeline.push(d);
                }
                timeline(width, height, dataTimeline, null, costum.isCostumAdmin);
                menuData["menu-parcours"].count = dataTimeline.length
                updateCount(menuData)
            });
        }

        function refreshDataMindMap(width, height) {
            dataMindMap = {
                ...rootMindMap
            };
            getAjax('', baseUrl + '/' + moduleId + '/element/getdatadetail/type/' + costum.contextType + '/id/' + costum.contextId + '/dataName/projects', (data) => {
                const children = [];
                for (const [id, projet] of Object.entries(data)) {
                    var d = {
                        id
                    }
                    d["name"] = projet.name;
                    d["children"] = []
                    for (const [id, parent] of Object.entries(projet.parent)) {
                        d["children"].push({
                            id,
                            name: parent.name
                        })
                    }
                    children.push(d);
                }
                dataMindMap.children = children;
                mindmap.updateData(dataMindMap);
                menuData["menu-projet"].count = children.length
                updateCount(menuData)
            });
        }

        function refreshDendo(width, height) {
            dataDendo = {
                ...rootDendo
            };
            getAjax('', baseUrl + '/' + moduleId + '/element/getdatadetail/type/' + costum.contextType + '/id/' + costum.contextId + '/dataName/classifieds', (data) => {
                const sections = {};
                for (const [id, comp] of Object.entries(data)) {
                    var d = {
                        id
                    }
                    if (!(comp.section in sections)) {
                        sections[comp.section] = []
                    }
                    d["section"] = comp.section
                    d["name"] = comp.name;
                    d["value"] = +comp.category
                    d["children"] = []
                    sections[comp.section].push(d);
                }
                const children = [];
                let count = 0
                for (const [section, datas] of Object.entries(sections)) {
                    count += datas.length;
                    const child = {
                        id: section,
                        name: section,
                        children: datas
                    }
                    children.push(child);
                }
                dataDendo.children = children;
                console.log(dataDendo)
                dendo(width, height, dataDendo, costum.isCostumAdmin);
                menuData["menu-competence"].count = count
                updateCount(menuData)
            });
        }

        function refreshContact(width, height) {
            var contactData = [];
            getAjax('', baseUrl + '/' + moduleId + '/element/getdatadetail/type/' + costum.contextType + '/id/' + costum.contextId + '/dataName/poi', (data) => {
                for (const [id, comp] of Object.entries(data)) {
                    var d = {
                        id
                    }
                    d["type"] = comp.type
                    d["name"] = comp.name;
                    d["icon"] = mapIcon[comp.type]
                    contactData.push(d);
                }
                contact(width, height, contactData, costum.isCostumAdmin);
                menuData["menu-contact"].count = contactData.length
                updateCount(menuData)
            });
        }
        jQuery(document).ready(function() {
            contextData = <?php echo json_encode($el); ?>;
            var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
            var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
            var contextName = <?php echo json_encode($el["name"]); ?>;
            contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
        });
    </script>
    <script>
        var me = "<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/jaona/me2.jpg";
    </script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/d3.v6.min.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/menu-home.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/dendo-cv.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/timeline.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/mindmap.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/contact.js"></script>
    <script src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/js/jaona/graph/crud.js"></script>
    <script>
        var width = 800,
            height = 800;
        mindmap.mindmap(width, height, dataMindMap, null, costum.isCostumAdmin);
        refreshDataTimeline(width, height);
        refreshDataMindMap();
        refreshDendo(width, height);
        refreshContact(width, height);
    </script>
    <script>
        $(document).ready(() => {
            $("#bg-homepage").attr("class", "");
            const {
                width,
                height
            } = d3.select("g#home-group").select("g").node().getBBox();
            const {
                innerWidth,
                innerHeight
            } = window;
            const num = Math.max(width, height);
            const den = Math.max(innerHeight, innerWidth)
            console.log(num, den)
            const k = Math.max(width, height) / Math.max(innerHeight, innerWidth)
            console.log(k)
            d3.select("g#home-group").select("g").style("transform", `scale(${k})`)

            // d3.select("svg#graph")
            //     .call(homeZoom)
            //     .call(
            //         homeZoom.transform,
            //         d3.zoomIdentity.translate(0, 0).scale(k)
            //     );
        })
        window.onpopstate = function(event) {
            console.log(event.state)
            if (event.state) {
                if (event.state.href != "#") {
                    zoomTo(event.state, 750, false);
                } else {
                    reset();
                }
            } else {
                history.back();
            }
        }
    </script>
    <script>

    </script>

</body>

</html>