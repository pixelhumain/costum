<?php
//$cssAnsScriptFilesModule = array(
//	'/css/ctenat/filters.css',
//);
//HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>

<div class="projectsProgress col-xs-12 col-md-10 col-md-offset-1 margin-bottom-20">
	<div class="col-xs-10 col-xs-offset-1">
		<!-- <img class="img-responsive col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" src="<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/ctenat/light-bulb.png"/>
		<span class="col-sm-6 col-sm-offset-3 margin-top-10" style="font-size: 20px;text-align: center;text-transform: uppercase;">Contenu à venir rapidement</span> -->
	</div> 
</div>
<!-- <div id="filterContainer" class="col-xs-12 col-md-10 col-md-offset-1">
</div> -->

<?php
    $typeObj = $this->costum["typeObj"];

    $typeActeurs = $typeObj["organization"]["dynFormCostum"]["beforeBuild"]["properties"]["category"]["options"];
?>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    let typeActeurs = <?php echo json_encode($typeActeurs); ?>;

	// if (pageApp == "filiere") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["organizations"]
		 	},
		 	filters : {
		 		category : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "category",
		 			name : "Acteurs",
		 			event : "filters",
		 			list : typeActeurs
		 		},
		 		domainAction : {
		 			view : "dropdownList",
		 			type : "tags",
		 			name : "Rechercher par :",
		 			event : "tags",
		 			list : costum.lists.domainAction
		 		}
		 	},
		 	results : {
				renderView: "directory.classifiedPanelHtml",
			 	smartGrid : true
			}
		};
	// }

    // if(pageApp == "Producteur" || pageApp == "Formateur" || pageApp == "Strategie"){
    //     paramsFilter.defaults = {
		  //   fields : { "category" : pageApp }
	   //  };
    // }

jQuery(document).ready(function() {
	// if (pageApp == "search") {
	// 	$(".btn-hide-map").css("display","none");
	// }
	filterSearch = searchObj.init(paramsFilter);
});
</script>