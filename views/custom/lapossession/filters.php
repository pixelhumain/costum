<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var eventList=<?php echo json_encode(Event::$types); ?>;

	var paramsFilter = {
		container : "#filters-nav",
	 	header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true,
						types : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						map : true
					}
				}
			}
	 	}
	};

	if (pageApp == "agenda") {
	 	paramsFilter.urlData = baseUrl+"/co2/search/agenda";
	 	paramsFilter.calendar = true;
	 	paramsFilter.loadEvent = {
	 		default : "agenda"
	 	};

	 	paramsFilter.defaults = {
 			types : ["events"]
 		};

 		paramsFilter.results = {
 			renderView : "directory.eventPanelHtml"
 		};

	 	paramsFilter.filters = {
	 		text : true,
	 		scope : true,
	 		type : {
	 			view : "dropdownList",
	 			event : "selectList",
	 			type : "type",
	 			list : eventList
	 		}
	 	};
	}

	if (pageApp == "annonces") {
		paramsFilter.inteface = {
			events : {
				scroll : true,
				scrollOne : true
			}
		};

		paramsFilter.results = {
			renderView: "directory.classifiedPanelHtml",
		 	smartGrid : true
		};

		paramsFilter.defaults = {
			types : ["classifieds"],
			fields : ["subtype"]
		}
	}

	if (pageApp == "annuaire"){

	 	paramsFilter.defaults = {
	 		types : ["organizations"],
	 		indexStep : 0
	 	};

	 	paramsFilter.inteface = {
			events : {
				scroll : true,
				scrollOne : true
			}
		};

		paramsFilter.results = {
			renderView: "directory.elementPanelHtml",
		 	smartGrid : true
		};

	 	paramsFilter.filters = {
 			category : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "category",
	 			name : "Catégorie d'action",
	 			event : "filters",
	 			list : {
	 				"citoyennete" : "citoyennete",
                    "energie" : "energie",
                    "construction" : "construction",
                    "communs" : "communs",
                    "dechets" : "dechets",
                    "education" : "education",
                    "economie" : "economie",
                    "alimentation/sante" : "alimentation/sante"
	    		}
 			}
 		};
	}

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});
</script>
