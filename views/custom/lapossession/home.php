<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );

	$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);

	$cssAnsScriptFilesModuleMap = array( 
	// 	'/leaflet/leaflet.css',
	// 	'/leaflet/leaflet.js',
	// 	'/css/map.css',
	// 	'/markercluster/MarkerCluster.css',
	// 	'/markercluster/MarkerCluster.Default.css',
	// 	'/markercluster/leaflet.markercluster.js',
	// 	'/js/map.js',
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
     
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>


	<div class="col-lg-12 col-md-12 no-padding" id="bandeau">

		<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauHome.jpg">
		
		<div id="pictos">
			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauPictos.svg">
		</div>

		<div id="bandeauOvale">
			<img class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauOvale.svg">
		</div> 

		<img class="img-responsive" id="pictoBandeau" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/PictoBandeau.svg">
			<p class="" id="TextBandeau">
				ACTEURS ET POINTS D'INTÉRÊT
			</p>
	</div>

	<div class="hidden-xs col-md-4 col-lg-4 d-inline-block" id="searchBar">
		<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Je recherche (mot clé, une association, un point d'interet...)">
			<a data-type="filters" href="javascript:;">
				<span id="second-search-bar-addon-possession" class="text-white input-group-addon pull-left main-search-bar-addon-possession">
					<i class="fa fa-search"></i>
				</span>
			</a>
	</div> 

	<div id="dropdown" class="hidden-xs dropdown-result-global-search col-sm-4 col-md-4 col-lg-4 no-padding">
	</div>

	<!--Ajout de la map -->
	<!-- <div class="mapContain"> -->
		<div style="margin-top: -1vw;" class="col-xs-12 mapBackground no-padding" id="mapPossession">
		</div>
	<!-- </div> -->

	<div class="container">
		<div class="col-xs-12 col-lg-12">
			<img class="img-responsive no-padding" id="bandeGrise" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Bande-Grise.svg">
			<img class="img-responsive no-padding col-xs-2" id="HexagoneVert" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexagoneVert.svg">

			<div class="centered" id="TextPicto">
				<span style="margin-left: 19%;">FILTRE</span> 
				<br> 
				<span style="font-weight: bold">THÉMATIQUE</span>
			</div>
		</div>
	</div>
	
<div id="bonhomme">
	<?php echo $this->renderPartial("costum.views.custom.lapossession.bonhomme"); ?>
</div>

<div class="ContainSection col-xs-12 no-padding">
	<div class="col-xs-6 no-padding" id="section1">
			<img class="img-responsive" id="citoyen" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Photo-Citoyen.jpg">
			<div>
				<img class="img-responsive no-padding col-xs-1 col-lg-1" id="iconesCitoy" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Hexacitoyen.svg">
				<a href="javascript:;" data-hash="#citoyens" class="lbh-menu-app " style="text-decoration : none;">
					<h5 class="titleCitoy">
						CITOYEN
					</h5>
				</a>
				<div class="col-xs-12 no-padding text-center" id="textCitoy">
					<p style="margin-top: 6%;font-size: 1.5vw;">
						JE TROUVE DES ASSOCIATIONS<br>
						JE CONSULTE LES POINTS D'INTÉRÊTS<br>
						JE GÈRE MON RÉSEAU<br>
						JE SUIS L'ACTUALITÉ DES ASSOCIATIONS
					</p>
				</div>
			</div>
	</div>

	<div class="col-xs-6 no-padding" id="section2">
			<img class="img-responsive" id="asso" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/Photo-Asso.jpg">
		<div style="margin-top: 1%;">
			<a href="javascript:;" data-hash="#search" class="lbh-menu-app " style="text-decoration : none;">
				<img class="img-responsive no-padding col-lg-1 col-xs-1" id="iconeAsso" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexaAssociation.svg">
					<h5 class="titleAsso">
						ASSOCIATION
					</h5>
			</a>
			<div class="col-xs-12 no-padding text-center" id="textAsso">
				<p style="margin-top: 6%;font-size: 1.5vw;">
					JE CRÉE OU TROUVE MON ASSOCIATION<br>
					JE FÉDÈRE MON RÉSEAU<br>
					JE GÈRE MES PROJETS<br>
					JE CRÉE MES ACTUS & ÉVÈNEMENTS
				</p>
			</div>
		</div>
	</div>
</div>

	
	<div style="margin-top: 2%;margin-left: 9%;" class="col-xs-10 actualité">
		<p id="TitreActua">ACTUALITÉS</p>
		<img class="img-responsive no-padding col-lg-1 col-xs-1" id="HexaActu" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/HexaActu.svg">
		<img class="img-responsive no-padding col-xs-10" id="BandeauVert" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/BandeauVert.svg">
	</div>

	<div id="newsstream">
        <div class="" style="background-color: white;">
        </div>
    </div>

    <div class="text-center">
	    <a href="javascript:;" data-hash="#live" class="lbh-menu-app" style="text-decoration : none; font-size:2rem;">
	    	<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coeurNumerique/svg/plus.svg" class="img-responsive plus-m" style="margin: 1%;width: 3%;"><br>
	    	Voir plus d'actualités
	    </a>
    </div>

<script type="text/javascript">
jQuery(document).ready(function(){
	setTitle("La Possession");

	$("#navbar").removeClass("navbar-collapse pull-right nav-right margin-right-15").addClass("navbar-collapse pull-right navbar-right");

	urlNews = "/news/co/index/collection/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false/source/"+costum.slug;

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html"); 
	
	var mapPossessionHome = {};
	var paramsMapPossession  = {};


	setTitle("La Possession");
	getMap();

});

function getMap(){

	var params = {
		contextType : costum.contextType,
		contextSlug : costum.contextSlug
	};

	paramsMapPossession = {
        container : "mapPossession",
        activePopUp : true,
        tile : "maptiler",
        menuRight : true
    };

	mapPossessionHome = mapObj.init(paramsMapPossession);

    ajaxPost(
        null,
        baseUrl + "/costum/lapossession/getorganization",
        params,
        function(data){
            mylog.log("success : ",data);

            $.each(data.elt , function(e,v){
                mylog.log("v.imgMedium",v.profilThumbImageUrl,v.imgMedium);
                if (!v.profilThumbImageUrl && v.imgMedium) {
                    v.profilThumbImageUrl = v.imgMedium;
                }
            });

            mylog.log("before load map",data.elt);

            mapPossessionHome.addElts(data.elt);
        },
        function(e){
            mylog.log("error : ",e);
        },
        "json",
        {async : false}
    );

	mapPossessionHome.map.panTo([-20.9329664,55.33543500]);
}


$("#second-search-bar").off().on("keyup",function(e){ 
						$("#input-search-map").val($("#second-search-bar").val());
						$("#second-search-xs-bar").val($("#second-search-bar").val());
						if(e.keyCode == 13){
								searchObject.text=$(this).val();
								searchObject.searchType = ["organizations"];
								searchObject.sourceKey = "lapossession";
								myScopes.type="open";
								myScopes.open={};
							 startGlobalSearch(0, indexStepGS);
								$("#dropdown").css('display','block');
						 }  
});

$("#second-search-xs-bar").off().on("keyup",function(e){ 
						$("#input-search-map").val($("#second-search-xs-bar").val());
						$("#second-search-bar").val($("#second-search-xs-bar").val());
						if(e.keyCode == 13){
								searchObject.text=$(this).val();
								searchObject.searchType = ["organizations"];
								searchObject.sourceKey = "lapossession";
								myScopes.type="open";
								myScopes.open={};
								startGlobalSearch(0, indexStepGS);
								$("#dropdown").css('display','block');            
						}
});

$("#second-search-bar-addon-possession, #second-search-xs-bar-addon").off().on("click", function(){
						$("#input-search-map").val($("#second-search-bar").val());
						searchObject.text=$("#second-search-bar").val();
						searchObject.searchType = ["organizations"];
						searchObject.sourceKey = "lapossession";
						myScopes.type="open";
						myScopes.open={};
						startGlobalSearch(0, indexStepGS);
						$("#dropdown").css('display','block');
});
</script>