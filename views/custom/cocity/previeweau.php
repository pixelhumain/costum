<?php ?>
<style>
    #close-modal-previews-eau {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 12pt;
        margin-top: -30px;
        margin-right: -30px;
    }

    #modal-preview-coop {
        left: 0% !important;
        padding: 2%;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    }

    .btn-show-map,
    .btn-show-tableau,
    .btn-show-graph {
        margin: 10px 3px;
        font-size: 14px;
        width: 40%;
        border-radius: 4px;
        height: 34px;
        background-color: #00b8d7;
        color: white;
        border: 2px solid transparent;
        float: right;
        transition: transform 0.2s, border-color 0.2s, box-shadow 0.2s;
    }

    .btn-show-map:active,
    .btn-show-tableau:active,
    .btn-show-graph:active {
        transform: scale(0.95);
        border-color: #007a91;
        box-shadow: 0 2px 8px rgba(0, 0, 0, 0.2);
    }

    .menu-container {
        background-color: #00b8d7;
        text-align: center;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
    }

    .submenu {
        list-style: none;
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: center;
    }

    .menu-item {
        cursor: pointer;
        color: white;
        font-size: 16px;
        font-weight: bold;
        padding: 12px 30px;
        transition: background 0.3s, color 0.3s;
        border-bottom: 3px solid transparent;
    }

    .menu-item:hover {
        background-color: #047681;
    }

    .menu-item.active {
        background-color: #047681;
        border-bottom: 3px solid #ffcc00;
        color: #ffcc00;
    }

    .content-container {
        margin: 5px auto;
        width: 100%;
        background-color: #fff;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
        overflow-y: auto;
        max-height: 70vh;
    }

    .content-container::-webkit-scrollbar,
    .col-md-6.note-one-data::-webkit-scrollbar {
        width: 8px;
    }

    .content-container::-webkit-scrollbar-thumb,
    .col-md-6.note-one-data::-webkit-scrollbar-thumb {
        background: #ccc;
        border-radius: 5px;
    }

    .content-container::-webkit-scrollbar-thumb:hover,
    .col-md-6.note-one-data::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .content-container::-webkit-scrollbar-track,
    .col-md-6.note-one-data::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }

    .content-container h3 {
        text-transform: none;
    }

    .content {
        display: none;
        padding: 5px;
    }

    .content.active {
        display: block;
    }

    #table-achats-substances,
    #table-achats-produits,
    #table-ventes-substances,
    #table-ventes-produits {
        max-height: 40vh;
        overflow-y: auto;
        overflow-x: auto;
        border: 1px solid #ddd;
        margin-bottom: 20px;
        display: block;
    }

    #table-potable {
        height: 79vh;
        overflow-y: auto;
        overflow-x: auto;
        border: 1px solid #ddd;
        margin-bottom: 20px;
        display: block;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        table-layout: auto;
    }

    th,
    td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
        white-space: normal;
        word-wrap: break-word;
    }

    th {
        background-color: #f4f4f4;
        font-weight: bold;
        min-width: 150px;
        overflow: visible;
        text-overflow: ellipsis;
        position: sticky;
        top: 0;
        z-index: 1;
        word-wrap: break-word;
        text-align: center;
    }

    .chartContainer {
        width: 95%;
        margin: 10px auto;
    }

    .all-previws-eau .map-container,
    .all-previws-eau .note-one-data {
        height: 77vh;
    }

    @media (max-width: 768px) {

        .all-previws-eau .map-container,
        .all-previws-eau .note-one-data {
            height: auto;
        }
    }

    .col-md-6.note-one-data {
        display: flex;
        flex-wrap: wrap;
        gap: 1rem;
        padding: 1rem;
        background-color: #f9f9f9;
        border-radius: 8px;
        overflow: auto;
    }

    @media (max-width: 768px) {
        .col-md-6.note-one-data {
            padding: 0.5rem;
            gap: 0.5rem;
        }
    }

    .data-card {
        flex: 1 1 calc(50% - 1rem);
        background: #fff;
        border: 1px solid #ddd;
        border-radius: 8px;
        padding: 1rem;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        transition: transform 0.2s ease, box-shadow 0.2s ease;
    }

    .data-card h2 {
        margin: 0 0 0.5rem;
        font-size: 1.4rem;
        color: #333;
    }

    .data-card p {
        margin: 0.25rem 0;
        font-size: 1.1rem;
        color: #555;
    }

    .data-card a {
        color: #007bff;
        text-decoration: none;
    }

    .data-card a:hover {
        text-decoration: underline;
    }

    .data-card:hover {
        transform: translateY(-5px);
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.15);
    }

    @media (max-width: 768px) {
        .data-card {
            flex: 1 1 calc(100% - 0.5rem);
            padding: 0.8rem;
        }

        .data-card h2 {
            font-size: 1.2rem;
        }

        .data-card p {
            font-size: 1rem;
        }
    }

    .col-md-6.note-one-data h2 {
        font-size: 1.8rem;
        margin-bottom: 1rem;
        text-align: center;
        color: #333;
    }

    @media (max-width: 768px) {
        .col-md-6.note-one-data h2 {
            font-size: 1.5rem;
        }
    }

    @media (max-width: 768px) {
        #main-map {
            height: 300px;
        }
    }

    #title-chart-prelevement {
        width: 100%;
        font-size: 1.8rem;
        text-align: center;
    }

    select {
        margin-bottom: 20px;
        padding: 8px;
    }

    .loader-loader-previews {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;

    }

    .loader-loader-previews .content-loader-previews {
        width: auto;
        height: auto;
    }

    .loader-loader-previews .content-loader-previews .processingLoader {
        width: auto !important;
        height: auto !important;
    }

    .backdrop-loader-previews {
        position: fixed;
        opacity: .8;
        background-color: #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100vh;
        z-index: 999999999 !important;
    }

    .popup_map_previews h3 {
        text-transform: none;
        font-size: 16px;
        text-align: center;
    }

    .popup_map_previews p {
        font-size: 14px;
    }

    .observations_cours_eau .tooltip {
        position: absolute;
        background-color: white;
        color: black;
        padding: 5px;
        border-radius: 5px;
        display: none;
        pointer-events: none;
        opacity: 1 !important;
        font-weight: bold;
        padding: 15px;
    }
    .container_eau {
        position: relative;
        width: 100%;
        height: 80vh;
        overflow: hidden;
    }
    .container_eau .map-container-eau {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #f0f0f0;
        z-index: 1;
    }
    #legende-container {
        position: absolute;
        top: 10px;
        right: 10px;
        background-color: rgba(255, 255, 255, 0.9); 
        padding: 10px;
        border-radius: 5px;
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        z-index: 2;
    }
   
</style>
<div class="all-previws-eau">
    <div class="loader-loader-previews hidden" id="loader-loader-previews" role="dialog">
        <div class="content-loader-previews" id="content-loader-previews"></div>
    </div>
    <div class="backdrop-loader-previews hidden" id="backdrop-loader-previews"></div>
    <button id="close-modal-previews-eau" class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <div class="row">
        <div class="col-md-10">
            <h3 class="section-title previews-title"></h3>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn-show-map hidden-xs list-affiche" id="showMap">
                <i class="fa fa-map-marker"></i> &nbsp;&nbsp;&nbsp; Carte
            </button>
            <button type="button" class="btn-show-tableau hidden-xs map-affiche hidden" id="showTableau">
                <i class="fa fa-calendar"></i> &nbsp;&nbsp;&nbsp; Tableau
            </button>
            <button type="button" class="btn-show-graph hidden-xs graph-affiche" id="showGraphe">
                <i class="fa fa-bar-chart"></i> &nbsp;&nbsp;&nbsp; Analyse
            </button>
        </div>
    </div>
    <div class="menu-container div-container-preview vente_achat_phyto hidden">
        <ul class="submenu">
            <li class="menu-item active" onclick="showContent('achats', this)">Achats</li>
            <li class="menu-item" onclick="showContent('vente', this)">Vente</li>
        </ul>
    </div>
    <div class="content-container div-container-preview vente_achat_phyto hidden">
        <div id="achats" class="content active">
            <h3>Les informations concernant les achats des substances.</h3>
            <div id="table-achats-substances" class="tabl">

            </div>
            <div class="chartContainer annalyse hidden">
                <canvas id="achatsubstanceChart"></canvas>
            </div>

            <h3>Les informations concernant les achats des produits.</h3>
            <div id="table-achats-produits" class="tabl">

            </div>
            <div class="chartContainer annalyse hidden">
                <canvas id="achatproduitChart"></canvas>
            </div>

        </div>
        <div id="vente" class="content">
            <h3>Les informations concernant les ventes des substances.</h3>
            <div id="table-ventes-substances" class="tabl">

            </div>
            <div class="chartContainer annalyse hidden">
                <canvas id="ventesubstanceChart"></canvas>
            </div>

            <h3>Les informations concernant les ventes des produits.</h3>
            <div id="table-ventes-produits" class="tabl">

            </div>
            <div class="chartContainer annalyse hidden">
                <canvas id="venteproduitChart"></canvas>
            </div>
        </div>
    </div>
    <div class="row div-container-preview prelevements hidden">
        <div class="col-md-6 map-container" id="main-map">

        </div>
        <div class="col-md-6 note-one-data tabl">

        </div>
        <div class="col-md-6 chartPrelevement annalyse hidden">
            <h3 id="title-chart-prelevement"></h3>
            <canvas id="prevelement_chroniques"></canvas>
        </div>
    </div>
    <div class="div-container-preview potable hidden">
        <div class="row">
            <label for="communeFilter">Filtrer par commune:</label>
            <select id="communeFilter">
                <option value="">Tous</option>
            </select>
        </div>
        <div id="table-potable" class="tabl">

        </div>
    </div>
    <div class="div-container-preview observations_cours_eau hidden">
        <div class="container_eau">
            
            <div class="map-container-eau" id="cours_eau_map">

            </div>
        
            <div id="legende-container">

            </div>
        </div>
    </div>
    <div id="tooltip" class="tooltip"></div>
</div>
       
<script>
    var labels = {
        "amm": "Numéro d'autorisation de mise sur le marché",
        "annee": "Année",
        "achat_etranger": "Achat à l'étranger",
        "classification": "Classification",
        "classification_mention": "Mention de classification",
        "code_cas": "Code CAS",
        "code_substance": "Code de la substance",
        "code_territoire": "Code du territoire",
        "fonction": "Fonction du produit",
        "libelle_substance": "Libellé de la substance",
        "libelle_territoire": "Libellé du territoire",
        "quantite": "Quantité",
        "type_territoire": "Type de territoire",
        "uri_substance": "URI de la substance",
        "uri_territoire": "URI du territoire",
        "eaj": "Emploi autorisé en jardin amateur",
        "unite": "Unité de mesure",
        "code_departement": "Code du département",
        "nom_departement": "Nom du département",
        "code_prelevement": "Code du prélèvement",
        "code_parametre": "Code du paramètre",
        "code_parametre_se": "Code du paramètre SE",
        "code_parametre_cas": "Code du paramètre CAS",
        "libelle_parametre": "Libellé du paramètre",
        "libelle_parametre_maj": "Libellé du paramètre (maj)",
        "libelle_parametre_web": "Libellé du paramètre pour le web",
        "code_type_parametre": "Code du type de paramètre",
        "code_lieu_analyse": "Code du lieu d'analyse",
        "resultat_alphanumerique": "Résultat alphanumérique",
        "resultat_numerique": "Résultat numérique",
        "libelle_unite": "Libellé de l'unité",
        "code_unite": "Code de l'unité",
        "limite_qualite_parametre": "Limite de qualité du paramètre",
        "reference_qualite_parametre": "Référence de qualité du paramètre",
        "code_commune": "Code de la commune",
        "nom_commune": "Nom de la commune",
        "nom_uge": "Nom de l'UGE",
        "nom_distributeur": "Nom du distributeur",
        "nom_moa": "Nom du MOA",
        "date_prelevement": "Date de prélèvement",
        "conclusion_conformite_prelevement": "Conclusion de conformité du prélèvement",
        "conformite_limites_bact_prelevement": "Conformité aux limites bactériologiques",
        "conformite_limites_pc_prelevement": "Conformité aux limites physico-chimiques",
        "conformite_references_bact_prelevement": "Conformité aux références bactériologiques",
        "conformite_references_pc_prelevement": "Conformité aux références physico-chimiques",
        "reference_analyse": "Référence de l'analyse",
        "code_installation_amont": "Code de l'installation en amont",
        "nom_installation_amont": "Nom de l'installation en amont",
        "reseaux": "Réseaux"
    };
    var urls = {};
    var links = <?= json_encode($links) ?>;
    var title = <?= json_encode($title) ?>;
    var address = <?= json_encode($region) ?>;
    var titre = title;
    if (title == "Ecoulement des cours de eau") titre = "Ecoulement des cours d'eau";
    if (title == "Qualité de eau potable") titre = "Qualité d'eau potable";
    titre != "" ? $(".previews-title").html(titre) : "";



    async function fetchAllData(links) {
        $('#loader-loader-previews').removeClass('hidden');
        $('#backdrop-loader-previews').removeClass('hidden');
        coInterface.showLoader('#content-loader-previews');
        try {
            var requests = Object.entries(urls).map(([key, url]) =>
                fetch(url)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`Erreur ${response.status}: ${response.statusText}`);
                    }
                    return response.json();
                })
                .then(data => ({
                    key,
                    data
                }))
            );

            var results = await Promise.all(requests);

            results.forEach(({
                key,
                data
            }) => {
                if (links == "vente_achat_phyto") {
                    afficheData(key, data);
                };

                if (links == "prelevements") {
                    showDataToMap(key, data);
                }

                if (links == "resultats_dis") {
                    setDataPotable(key, data);
                }

                if (links == "observations") {
                    showDataCoursEau(key, data);
                }
            });

        } catch (error) {
            toastr.error("Erreur lors de la récupération des données.");
        }
    }

    function afficheData(key, data) {
        let targetElementId = "";

        switch (key) {
            case "achats_substances":
                targetElementId = "table-achats-substances";
                chartSubstance(data.data, "achatsubstanceChart");
                break;
            case "achats_produits":
                targetElementId = "table-achats-produits";
                chartProduit(data.data, "achatproduitChart");
                break;
            case "ventes_substances":
                targetElementId = "table-ventes-substances";
                chartSubstance(data.data, "ventesubstanceChart");
                break;
            case "ventes_produits":
                targetElementId = "table-ventes-produits";
                chartProduit(data.data, "venteproduitChart");
                break;
        }

        var container = document.getElementById(targetElementId);
        container.innerHTML = ""; // Nettoyer le contenu précédent

        if (data && data.data && data.data.length > 0) {
            var table = document.createElement("table");
            table.border = "1";
            table.style.width = "100%";
            table.style.borderCollapse = "collapse";

            var headerRow = document.createElement("tr");
            Object.keys(data.data[0]).forEach(key => {
                var th = document.createElement("th");
                th.textContent = labels[key] || key; 
                th.style.padding = "8px";
                th.style.backgroundColor = "#f2f2f2";
                th.style.border = "1px solid #ddd";
                headerRow.appendChild(th);
            });
            table.appendChild(headerRow);

            data.data.forEach(item => {
                var row = document.createElement("tr");
                Object.entries(item).forEach(([key, value]) => {
                    var td = document.createElement("td");
                    td.textContent = value || ""; 
                    td.style.padding = "8px";
                    td.style.border = "1px solid #ddd";
                    row.appendChild(td);
                });
                table.appendChild(row);
            });

            container.appendChild(table);
        } else {
            container.textContent = "Aucune donnée disponible.";
        }

        $('#loader-loader-previews').addClass('hidden');
        $('#backdrop-loader-previews').addClass('hidden');
    }

    function showContent(sectionId, clickedItem) {
        var contents = document.querySelectorAll('.content');
        contents.forEach(content => content.classList.remove('active'));

        document.getElementById(sectionId).classList.add('active');

        var menuItems = document.querySelectorAll('.menu-item');
        menuItems.forEach(item => item.classList.remove('active'));

        clickedItem.classList.add('active');
    }

    var generateYearColors = (years) => {
        var colorMap = {};
        years.forEach((year, index) => {
            var r = Math.floor(Math.random() * 155 + 100);
            var g = Math.floor(Math.random() * 155 + 100);
            var b = Math.floor(Math.random() * 155 + 100);
            colorMap[year] = `rgba(${r}, ${g}, ${b}, 0.6)`;
        });
        return colorMap;
    };

    var chartSubstance = (rawData, idCanvas) => {
        var ctx = document.getElementById(idCanvas).getContext('2d');
        var years = [...new Set(rawData.map(item => item.annee))];

        var yearColorMap = generateYearColors(years);

        var substances = [...new Set(rawData.map(item => item.libelle_substance))];

        var datasets = years.map((year) => {
            var yearData = substances.map(substance => {
                var substanceData = rawData.find(item =>
                    item.libelle_substance === substance && item.annee === year
                );
                return substanceData ? substanceData.quantite : 0;
            });

            return {
                label: `Quantité ${year}`,
                data: yearData,
                backgroundColor: substances.map(() => yearColorMap[year]),
                borderColor: substances.map(() => yearColorMap[year]),
                borderWidth: 1
            };
        });

        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: 'top',
                    },
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                return `${context.dataset.label}: ${context.raw.toFixed(2)} unités`;
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Substances'
                        }
                    },
                    y: {
                        beginAtZero: true,
                        title: {
                            display: true,
                            text: 'Quantité'
                        }
                    }
                }
            }
        });
        chart.data.labels = substances;
        chart.data.datasets = datasets;
        chart.update();
    };

    var chartProduit = (data, idCanvas) => {
        var years = [...new Set(data.map(item => item.annee))];

        var yearColors = generateYearColors(years);

        var groupedData = {};

        data.forEach(item => {
            var {
                annee,
                quantite,
                unite
            } = item;

            if (!groupedData[unite]) {
                groupedData[unite] = {};
            }

            if (!groupedData[unite][annee]) {
                groupedData[unite][annee] = 0;
            }

            groupedData[unite][annee] += quantite;
        });

        var datasets = years.map(year => ({
            label: `Année ${year}`,
            data: Object.entries(groupedData).map(([unite, values]) => values[year] || 0),
            backgroundColor: yearColors[year],
            borderColor: yearColors[year].replace('0.6', '1'),
            borderWidth: 1
        }));

        var labels = Object.keys(groupedData);

        var ctx = document.getElementById(idCanvas).getContext('2d');
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: 'top'
                    }
                },
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Unités'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Quantité'
                        },
                        beginAtZero: true
                    }
                }
            }
        });
    };

    $(".btn-show-graph").on('click', function() {
        $(".tabl").addClass("hidden");
        $(".annalyse").removeClass('hidden');
        $(".btn-show-tableau").removeClass("hidden");
        $(".btn-show-graph").addClass("hidden");
    });

    $(".btn-show-tableau").on('click', function() {
        $(".annalyse").addClass('hidden');
        $(".tabl").removeClass("hidden");
        $(".btn-show-graph").removeClass("hidden");
        $(".btn-show-tableau").addClass("hidden");
    });

    if (links == 'vente_achat_phyto') {
        $(".vente_achat_phyto").removeClass("hidden");
        $(".btn-show-map").addClass("hidden");
        urls = {
            achats_substances: "https://hubeau.eaufrance.fr/api/v1/vente_achat_phyto/achats/substances?size=100",
            achats_produits: "https://hubeau.eaufrance.fr/api/v1/vente_achat_phyto/achats/produits?size=100",
            ventes_substances: "https://hubeau.eaufrance.fr/api/v1/vente_achat_phyto/ventes/substances?size=100",
            ventes_produits: "https://hubeau.eaufrance.fr/api/v1/vente_achat_phyto/ventes/produits?size=100"
        };
        fetchAllData(links);
    }

    // prelevement
    var groupedByCommune = [];
    var showDataToMap = (key, data) => {
        if (data.data && data.data.length > 0) {
            var donnes = data.data;

            var uniqueCommunes = [...new Set(donnes.map(item => item.nom_commune))];
            var params = {
                "communes": uniqueCommunes
            }

            groupedByCommune = donnes.reduce((acc, item) => {
                var commune = item.nom_commune;

                if (!acc[commune]) {
                    acc[commune] = [];
                }

                acc[commune].push(item);

                return acc;
            }, {});

            var eauMap = new CoMap({
                container: "#main-map",
                activePopUp: true,
                mapOpt: {
                    btnHide: false,
                    doubleClick: true,
                    scrollWheelZoom: false,
                    zoom: 3,
                },
                mapCustom: {
                    tile: "maptiler",
                    addElts: function(_context, data) {
                        let geoJson = [];
                        let elements = document.querySelectorAll('#main-map .leaflet-interactive');
                        let tooltip = document.getElementById('tooltip');
                        $.each(data, function(k, v) {
                            if (!_context.getOptions().data[k])
                                _context.getOptions().data[k] = v;
                            if (typeof v.geoShape != "undefined") {
                                let data = $.extend({}, v);
                                let geoData = {
                                    type: "Feature",
                                    properties: {
                                        name: v.name
                                    },
                                    geometry: v.geoShape
                                };
                                delete data.name;
                                delete data.getShape;
                                $.each(data, function(key, value) {
                                    if (typeof geoData.properties[key] == 'undefined') {
                                        geoData.properties[key] = value;
                                    }
                                })
                                geoJson.push(geoData);
                            }
                        })

                        _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                            onEachFeature: function(feature, layer) {
                                _context.addPopUp(layer, feature);
                                var keis = (feature.properties.name).toLocaleUpperCase();

                                layer.setStyle({
                                    color: "#1a72d0",
                                    opacity: 0.7
                                });

                                layer.on("click", function(event) {
                                    polygonClicked = true;
                                    setNote(feature.properties.name);
                                });

                                layer.on("mouseover", function(event) {
                                    this.setStyle({
                                        "fillOpacity": 1,
                                        "strokeOpacity": 1
                                    });
                                    let name = this.feature.properties.name;
                                    tooltip.innerHTML = name;
                                    tooltip.style.display = 'block';
                                    let x = event.containerPoint.x;
                                    let y = event.containerPoint.y;
                                    tooltip.style.left = `${x}px`;
                                    tooltip.style.top = `${y}px`;
                                });

                                layer.on("mouseout", function() {
                                    this.setStyle({
                                        "fillOpacity": 0.2,
                                        "strokeOpacity": 0.2,
                                    });
                                    tooltip.style.display = 'none';
                                });
                            }
                        });
                        _context.getMap().addLayer(_context.getOptions().geoJSONlayer);
                        _context.hideLoader();
                        _context.fitBounds(data);
                    }

                }
            });

            ajaxPost(
                null,
                baseUrl + "/costum/cocity/getcitiscommunes",
                params,
                function(results) {
                    eauMap.clearMap();
                    eauMap.addElts(results);

                    setNote();

                    $('#loader-loader-previews').addClass('hidden');
                    $('#backdrop-loader-previews').addClass('hidden');
                }
            );

        }
    };
    
    var lineChart;

var setNote = (ville = null) => {
    var container = document.querySelector(".col-md-6.note-one-data");
    container.innerHTML = "";

    var data = groupedByCommune[ville] || groupedByCommune["Abreschviller"];
    if (data.length === 0) {
        container.innerHTML = `<p>Aucune donnée disponible pour cette commune.</p>`;
        if (lineChart) lineChart.destroy(); // Supprimer la chart si aucune donnée
        return;
    }

    var noteTitle = ville == null ? `<h2 style="width: 100%;">Commune d'Abreschviller</h2>` : `<h2 style="width: 100%;">Commune d(e) ${ville}</h2>`;
    container.innerHTML = noteTitle;

    data.sort((a, b) => b.annee - a.annee);

    data.forEach((item) => {
        var card = document.createElement("div");
        card.classList.add("data-card");

        card.innerHTML = `
            <h2>${item.annee}</h2>
            <p><strong>Ouvrage :</strong> <a href="${item.uri_ouvrage}" target="_blank">${item.nom_ouvrage}</a></p>
            <p><strong>Usage :</strong> ${item.libelle_usage}</p>
            <p><strong>Volume :</strong> ${item.volume} m³</p>
            <p><strong>Coordonnées :</strong> ${item.latitude.toFixed(5)}, ${item.longitude.toFixed(5)}</p>
        `;

        container.appendChild(card);
    });

    $("#title-chart-prelevement").html(ville == null ? "Commune d'Abreschviller" : `Commune d(e) ${ville}`);

    var labels = data.map(entry => entry.annee); // Années
    var volumes = data.map(entry => entry.volume); // Volumes

    if (lineChart) {
        lineChart.destroy(); // Détruire le graphique existant
    }

    const ctx = document.getElementById('prevelement_chroniques').getContext('2d');
    lineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Volume par année',
                data: volumes,
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderWidth: 2,
                pointRadius: 5,
                pointBackgroundColor: 'rgba(75, 192, 192, 1)'
            }]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top'
                },
                tooltip: {
                    callbacks: {
                        label: function(context) {
                            var index = context.dataIndex;
                            var item = data[index]; // Utilisez "data"

                            return [
                                `Volume: ${item.volume} m³`,
                                `Usage: ${item.libelle_usage}`,
                                `Ouvrage: ${item.nom_ouvrage}`,
                                `Producteur: ${item.producteur_donnee}`,
                                `Statut volume: ${item.libelle_statut_volume}`,
                                `Qualification: ${item.libelle_qualification_volume}`,
                                `Mode d'obtention: ${item.libelle_mode_obtention_volume}`,
                                `Code usage: ${item.code_usage}`
                            ];
                        }
                    }
                }
            },
            scales: {
                x: {
                    title: {
                        display: true,
                        text: 'Année'
                    }
                },
                y: {
                    title: {
                        display: true,
                        text: 'Volume (m³)'
                    },
                    beginAtZero: true
                }
            }
        }
    });
};


    if (links == "prelevements") {
        $(".btn-show-map").addClass("hidden");
        $(".prelevements").removeClass("hidden");
        urls = {
            chroniques: "https://hubeau.eaufrance.fr/api/v1/prelevements/chroniques"
        }
        fetchAllData(links);
    }

    function populateCommuneFilter(data) {
        var communes = new Set(data.map((item) => item.nom_commune));
        var communeFilter = document.getElementById("communeFilter");

        communes.forEach((commune) => {
            var option = document.createElement("option");
            option.value = commune;
            option.textContent = commune;
            communeFilter.appendChild(option);
        });
    }

    function displayTable(data) {
        var container = document.getElementById("table-potable");
        container.innerHTML = "";

        if (data && data.length > 0) {
            var table = document.createElement("table");
            table.border = "1";
            table.style.width = "100%";
            table.style.borderCollapse = "collapse";

            // Colonnes à mettre en avant
            var highlightedColumns = [
                "nom_commune",
                "code_parametre",
                "code_parametre_se",
                "libelle_parametre",
                "code_type_parametre",
                "code_lieu_analyse",
                "resultat_alphanumerique",
                "libelle_unite",
                "conclusion_conformite_prelevement"
            ];

            // Générer l'en-tête du tableau
            var headerRow = document.createElement("tr");

            // Ajouter d'abord les colonnes mises en avant
            highlightedColumns.forEach(key => {
                if (key in data[0]) {
                    var th = document.createElement("th");
                    th.textContent = labels[key] || key; // Utiliser le libellé si disponible
                    th.style.padding = "8px";
                    th.style.backgroundColor = "#f2f2f2";
                    th.style.border = "1px solid #ddd";
                    headerRow.appendChild(th);
                }
            });

            // Ajouter les autres colonnes
            Object.keys(data[0]).forEach(key => {
                if (!highlightedColumns.includes(key)) {
                    var th = document.createElement("th");
                    th.textContent = labels[key] || key; // Utiliser le libellé si disponible
                    th.style.padding = "8px";
                    th.style.backgroundColor = "#f2f2f2";
                    th.style.border = "1px solid #ddd";
                    headerRow.appendChild(th);
                }
            });

            table.appendChild(headerRow);

            // Générer les lignes de données
            data.forEach(item => {
                var row = document.createElement("tr");

                // Ajouter les colonnes mises en avant
                highlightedColumns.forEach(key => {
                    if (key in item) {
                        var td = document.createElement("td");
                        td.textContent = item[key] || " "; // Afficher "N/A" si la valeur est absente
                        td.style.padding = "8px";
                        td.style.border = "1px solid #ddd";
                        row.appendChild(td);
                    }
                });

                // Ajouter les autres colonnes
                Object.entries(item).forEach(([key, value]) => {
                    if (!highlightedColumns.includes(key)) {
                        var td = document.createElement("td");
                        td.textContent = value || " "; // Afficher "N/A" si la valeur est absente
                        td.style.padding = "8px";
                        td.style.border = "1px solid #ddd";
                        row.appendChild(td);
                    }
                });

                table.appendChild(row);
            });

            container.appendChild(table);
        } else {
            container.textContent = "Aucune donnée disponible.";
        }

        $('#loader-loader-previews').addClass('hidden');
        $('#backdrop-loader-previews').addClass('hidden');
    }

    function filterByCommune(data) {
        var communeFilter = document.getElementById("communeFilter");
        var selectedCommune = communeFilter.value;

        var filteredData = selectedCommune ?
            data.filter((item) => item.nom_commune === selectedCommune) :
            data;

        displayTable(filteredData);
    }


    var setDataPotable = (key, data) => {
        populateCommuneFilter(data.data);
        displayTable(data.data);
        $("#communeFilter").on('change', function() {
            filterByCommune(data.data);
        });
    };
    if (links == "resultats_dis") {
        $(".btn-show-map").addClass("hidden");
        $(".potable").removeClass("hidden");
        $(".btn-show-graph").addClass("hidden");
        urls = {
            chroniques: "https://hubeau.eaufrance.fr/api/v1/qualite_eau_potable/resultats_dis?size=2000"
        }
        fetchAllData(links);
    }

    var showDataCoursEau = (key, data) => {
        if (data.data && data.data.length > 0) {
            var donnes =  data.data;

            if (address != "" && address != null) {
                donnes = donnes.filter(item => item.libelle_region === address);
            }

            donnes = donnes.length > 0 ? donnes : data.data;

            var uniqueCommunes = [...new Set(donnes.map(item => item.libelle_commune))];
            var params = {
                "communes": uniqueCommunes
            }

            groupedByCommune = donnes.reduce((acc, item) => {
                var commune = (item.libelle_commune).toLocaleUpperCase();

                if (
                    !acc[commune] ||
                    new Date(item.date_observation) > new Date(acc[commune].date_observation)
                ) {
                    acc[commune] = item;
                }

                return acc;
            }, {});

            var typeEcoulement = {
                "1": "Ecoulement visible",
                "1a": "Ecoulement visible acceptable",
                "1f": "Ecoulement visible faible",
                "2": "Ecoulement non visible",
                "3": "Assec"
            }
            var eauMap = new CoMap({
                container: "#cours_eau_map",
                activePopUp: true,
                mapOpt: {
                    btnHide: false,
                    doubleClick: true,
                    scrollWheelZoom: false,
                    zoom: 3,
                },
                mapCustom: {
                    tile: "maptiler",
                    getPopup: function(donne) {
                        var cleName = (donne.properties.name).toLocaleUpperCase();
                        
                        var element = groupedByCommune[cleName];
                        
                        var popup = `<div class="popup_map_previews"><h3>${donne.properties.name}</h3>`;
                        var properties = [{
                                label: "Station",
                                key: "libelle_station"
                            },
                            {
                                label: "Cours d'eau",
                                key: "libelle_cours_eau"
                            },
                            {
                                label: "Reseau",
                                key: "libelle_reseau"
                            },
                            {
                                label: "Date d'observation",
                                key: "date_observation"
                            },
                            {
                                label: "Statu d'écoulement",
                                key: "libelle_ecoulement"
                            },
                            {
                                label: "Code d'écoulement",
                                key: "code_ecoulement"
                            }
                        ];

                        properties.forEach(prop => {
                            if (element[prop.key] != null && element[prop.key] !== '') {
                                popup += `<p><b>${prop.label} : </b>${element[prop.key]}</p>`;
                            }
                        });
                        popup += `</div>`;

                        return popup;
                    },
                    addElts: function(_context, data) {
                        let geoJson = [];
                        let elements = document.querySelectorAll('#cours_eau_map .leaflet-interactive');
                        let tooltip = document.getElementById('tooltip');
                        $.each(data, function(k, v) {
                            if (!_context.getOptions().data[k])
                                _context.getOptions().data[k] = v;
                            if (typeof v.geoShape != "undefined") {
                                let data = $.extend({}, v);
                                let geoData = {
                                    type: "Feature",
                                    properties: {
                                        name: v.name
                                    },
                                    geometry: v.geoShape
                                };
                                delete data.name;
                                delete data.getShape;
                                $.each(data, function(key, value) {
                                    if (typeof geoData.properties[key] == 'undefined') {
                                        geoData.properties[key] = value;
                                    }
                                })
                                geoJson.push(geoData);
                            }
                        })

                        _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
                            onEachFeature: function(feature, layer) {
                                _context.addPopUp(layer, feature);
                                var keis = (feature.properties.name).toLocaleUpperCase();
                                var elmts = groupedByCommune[keis];
                                var colorsShape = "black";

                                if (elmts.code_ecoulement == "1") colorsShape = "#007bff";
                                if (elmts.code_ecoulement == "1a") colorsShape = "#28a745";
                                if (elmts.code_ecoulement == "1f") colorsShape = "#ffc107";
                                if (elmts.code_ecoulement == "2") colorsShape = "black";
                                if (elmts.code_ecoulement == "3") colorsShape = "#dc3545";
                                if (elmts.code_ecoulement == "" || elmts.code_ecoulement == null) colorsShape = "#6c757d";

                                layer.setStyle({
                                    color: colorsShape,
                                    opacity: 0.7
                                });
                                layer.on("mouseover", function(event) {
                                    this.setStyle({
                                        "fillOpacity": 1,
                                        "strokeOpacity": 1
                                    });
                                    let name = this.feature.properties.name;
                                    tooltip.innerHTML = name;
                                    tooltip.style.display = 'block';
                                    let x = event.containerPoint.x;
                                    let y = event.containerPoint.y;
                                    tooltip.style.left = `${x}px`;
                                    tooltip.style.top = `${y}px`;
                                });

                                layer.on("mouseout", function() {
                                    this.setStyle({
                                        "fillOpacity": 0.2,
                                        "strokeOpacity": 0.2,
                                    });
                                    tooltip.style.display = 'none';
                                });
                            }
                        });
                        _context.getMap().addLayer(_context.getOptions().geoJSONlayer);
                        _context.hideLoader();
                        _context.fitBounds(data);
                    }
                }
            });

            ajaxPost(
                null,
                baseUrl + "/costum/cocity/getcitiscommunes",
                params,
                function(results) {
                    eauMap.clearMap();
                    eauMap.addElts(results);
                    generateLegend();
                    $('#loader-loader-previews').addClass('hidden');
                    $('#backdrop-loader-previews').addClass('hidden');
                }
            );

        }
    }

    function generateLegend() {
        const typeEcoulement = {
            "1": {
                label: " Visible",
                color: "#007bff"
            },
            "1a": {
                label: " Visible acceptable",
                color: "#28a745"
            },
            "1f": {
                label: " Visible faible",
                color: "#ffc107"
            },
            "2": {
                label: " Non visible",
                color: "black"
            },
            "3": {
                label: "Assec",
                color: "#dc3545"
            },
            "4": {
                label: "Inconnu",
                color: "#6c757d"
            }
        };
        const container = document.getElementById("legende-container");
        if (!container) {
            toastr.error(`Container with id legende-container not found.`);
            return;
        }

        // Créer le conteneur principal de la légende
        const legend = document.createElement("div");
        legend.style.border = "1px solid #ccc";
        legend.style.padding = "10px";
        legend.style.width = "200px";
        legend.style.backgroundColor = "#f9f9f9";
        legend.style.fontFamily = "Arial, sans-serif";

        // Titre de la légende
        const title = document.createElement("h5");
        title.textContent = "Légende (Ecoulement)";
        title.style.margin = "0 0 10px";
        legend.appendChild(title);

        // Parcourir l'objet typeEcoulement pour créer chaque élément
        Object.values(typeEcoulement).forEach(({
            label,
            color
        }) => {
            const item = document.createElement("div");
            item.style.display = "flex";
            item.style.alignItems = "center";
            item.style.marginBottom = "8px";

            // Carré de couleur
            const colorBox = document.createElement("span");
            colorBox.style.display = "inline-block";
            colorBox.style.width = "20px";
            colorBox.style.height = "20px";
            colorBox.style.backgroundColor = color;
            colorBox.style.marginRight = "10px";
            colorBox.style.border = "1px solid #ccc";
            item.appendChild(colorBox);

            // Label
            const labelText = document.createElement("span");
            labelText.textContent = label;
            item.appendChild(labelText);

            legend.appendChild(item);
        });

        // Ajouter la légende au conteneur
        container.appendChild(legend);
    }

    if (links == "observations") {
        $(".btn-show-map").addClass("hidden");
        $(".btn-show-graph").addClass("hidden");
        $(".observations_cours_eau").removeClass("hidden");
        urls = {
            chroniques: "https://hubeau.eaufrance.fr/api/v1/ecoulement/observations"
        }
        fetchAllData(links);
    }
</script>