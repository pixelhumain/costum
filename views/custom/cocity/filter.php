<?php 
$el = "";
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}
$filliaireCategories = CO2::getContextList("filliaireCategories"); 
$cities = "";
if($el != "" &&  $el["costum"]["typeCocity"] == "epci") {
	$cities = PHDB::findOne(Zone::COLLECTION,array(
		"name" => $el["address"]["level5Name"]
	));
}
?> 
<style>
	.tag-list a span {
		color: red !important;
	}
</style>
<script type="text/javascript">
	var dataCostum = <?php echo json_encode( $el ); ?> ;
	var citiesArray = <?php echo json_encode($cities); ?>;
	var thematic = null;

	if(typeof dataCostum.thematic != "undefined" && dataCostum.thematic != "") {
		thematic = dataCostum.thematic;
	}

	var filliaireCategories = <?php echo json_encode(@$filliaireCategories); ?>;
	var pageApp = "<?= @$page?>";
	var typeClassified=<?php echo json_encode(@$_GET["type"]); ?>;
	var sectionClassified=<?php echo json_encode(@$_GET["section"]); ?>;
	if(notNull(costum) && typeof costum["app"]["#annonces"] != "undefined" && typeof costum["app"]["#annonces"]["filters"] != "undefined" &&  typeof costum["app"]["#annonces"]["filters"]["type"] != "undefined"){
		typeClassified=costum["app"]["#annonces"]["filters"]["type"];
	}
	sectionsClassified={};
	categoriesList={};
	if(notEmpty(typeClassified)){
		$.each(modules[typeClassified].categories.sections, function(k,v){
			sectionsClassified[k]=(typeof tradCategory[v.labelFront] != "undefined") ? tradCategory[v.labelFront] : v.label; 
		});

		$.each(modules[typeClassified].categories.filters, function(k,v){
			labelCat=(typeof tradCategory[k] != "undefined") ? tradCategory[k] : v.label;
			categoriesList[k]={label : labelCat, icon : v.icon};
			if(typeof v.subcat != "undefined"){
				categoriesList[k]["subList"]={};
				$.each(v.subcat, function(e,val){
					labelCat=(typeof tradCategory[e] != "undefined") ? tradCategory[e] : val.label;
					categoriesList[k].subList[e]={label : labelCat, icon : val.icon};
				});			
			}
		});
	}
	var paramsFilter = {
		 container : "#filters-nav",
		defaults : {
			notSourceKey:true,
			indexStep : 0,
			filters : {
				$or :{
					"source.keys" : "<?=$el["slug"]?>",
					"links.memberOf.<?= (string)$el['_id'] ?>": {'$exists': true}
				}				
			},
		},
		results : {
			smartGrid : true,
			renderView : "directory.elementPanelHtmlSmallCard"
		},
		loadEvent:{
            default:"scroll"
        },
		filters : {
			
		}
	}
	<?php if(isset($el["address"])){ ?>
		var idAddress = {};
		if(costum.typeCocity == "region"){
			idAddress = {"address.level3" : dataCostum.address['level3']};
		} else if(costum.typeCocity == "ville") {
			idAddress = {"address.localityId": dataCostum.address['localityId'] };
		} else if(costum.typeCocity == "departement" || costum.typeCocity == "district") {
			idAddress = {"address.level4": dataCostum.address['level4'] };
		} else if(costum.typeCocity == "epci") {
			costum.citiesArray = citiesArray.cities;
			idAddress = {"address.localityId": {$in : costum.citiesArray} };
		}
		var addressFilters = {
			"source.keys": "<?= $el['slug'] ?>",
			...idAddress,
			"links.memberOf.<?= (string)$el['_id'] ?>": {'$exists': true}
		};

		paramsFilter.defaults.filters["$or"] = addressFilters;
	<?php } ?>

	if(thematic != null) {
		paramsFilter.filters.theme ={
			view : "megaMenuDropdown",
			type : "tags",
			remove0: false,
			name : "<?php echo Yii::t("common", "search by theme")?>",
			event : "tags",
			keyValue: true,
			list : thematic
		
		}
	}

	if(pageApp=="search"){
		paramsFilter.defaults.types =["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"];
		paramsFilter.filters.text =true;
		paramsFilter.filters.scope =true;
		paramsFilter.filters.types ={
				lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources","events"]
		}
		
	} else {
		var typeSearch = "";
		typeSearch = pageApp.split("=")[1];
		paramsFilter.defaults.types =[typeSearch];
	}
	
	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		mylog.log("filterSearchfilterSearch",paramsFilter)
	});
</script>