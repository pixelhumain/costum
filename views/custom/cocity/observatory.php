<?php 
	$cssAnsScriptFilesTheme = array(

		'/plugins/jQCloud/dist/jqcloud.min.js',
		'/plugins/jQCloud/dist/jqcloud.min.css',

	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme,Yii::app()->request->baseUrl);
	$tagsPoiList = array();
	if(!empty($allTags)){
		shuffle($allTags);
		foreach ($allTags as $key => $elem){
			$found = false;
			foreach ($tagsPoiList as $ix => $value) {
				$value["text"]; 
				if(	$value["text"] == $elem)
					$found = $ix;
			}
			if ( !$found )
				array_push($tagsPoiList,array(
					"text"=>$elem,
					"weight"=>1,
					"link"=>array(
						"href" => "javascript:;",
						"class" => "favElBtn ".InflectorHelper::slugify2($elem)."Btn",
						"data-tag" => InflectorHelper::slugify2($elem)
					)
				));
			else
				$tagsPoiList[$found]["weight"]++;
		}
	}
?>
<style type="text/css">
	.observacity {
		 background-color: #EBE8EE; 
	}
	.card-box {
		background-color: #fff ;
		padding: 1.25rem ; 
		-webkit-box-shadow: 0 0 35px 0 rgba(73,80,87,.15) ; 
		box-shadow: 0 0 35px 0 rgba(73,80,87,.15) ; 
		margin-bottom: 24px ; 
		border-radius: 0.5rem; 
		margin: 1rem ;
	}
	.card-box h6{
		font-size: 14px ; 
		color: #3ea3b4; 
		font-weight: bold ;
		padding: 10px;
		text-align: center;
	} 
	.observacityContainer {
		padding-left: 5%;
		padding-right: 5%;
	} 
	/*  */
	.counter{
		font-family: 'Nunito Sans', sans-serif;
		text-align: center;
		height: 210px;
		width: 210px;
		padding: 7px 4px 0;
		margin: 0 auto;
		position: relative;
		z-index: 1;
		cursor: pointer;
	}
	.counter:before{
		content: '';
		background-color: #fff;
		height: 157px;
		width: 157px;
		border-radius: 15px;
		border: 5px solid #fff;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.36) inset;
		transform: translateX(-50%) translateY(-50%) rotate(45deg);
		position: absolute;
		left: 50%;
		top: 50%;
		z-index: -1;
	}
	.counter .counter-icon{
		color: #fff;
		background: linear-gradient(#e1a336,#d78c0b);
		line-height: 70px;
		font-size: 35px;
		height: 70px;
		width: 70px;
		margin: 0 auto 8px;
		border-radius: 19px 0 50px;
		transform: rotate(45deg);
	}
	.counter .counter-icon i{ transform: rotate(-45deg); }
	.counter .counter-value{
		font-size: 25px;
		font-weight: 700;
		letter-spacing: 1px;
		margin: 0 0 13px 0;
		display: block;
	}
	.counter h3{
		color: #fff;
		background: linear-gradient(#F9B21A,#d78c0b);
		font-size: 18px;
		font-weight: 600;
		letter-spacing: 1px;
		text-transform: capitalize;
		padding: 10px 5px;
		margin: 0;
		border-radius: 0 0 20px 20px;
		position: relative;
	}
	.counter h3:before,
	.counter h3:after{
		content: "";
		background: linear-gradient(to right bottom, transparent 49%,#ee8434fc 50%);
		width: 20px;
		height: 20px;
		position: absolute;
		top: -20px;
		left: 0;
		z-index: -2;
	}
	.counter h3:after{
		transform: rotateY(180deg);
		left: auto;
		right: 0;
	}
	.counter.green .counter-icon,
	.counter.green h3{
		background: linear-gradient(#467ea4, #1b4e71);
	}
	.counter.green h3:before,
	.counter.green h3:after{
		background: linear-gradient(to right bottom,transparent 49%,#195b87 50%);
	}
	.counter.purple .counter-icon,
	.counter.purple h3{
		background: linear-gradient(#8C5AA1,#340669);
	}
	.counter.purple h3:before,
	.counter.purple h3:after{
		background: linear-gradient(to right bottom,transparent 49%,#5a07bf 50%);
	}
	.counter.blue .counter-icon,
	.counter.blue h3{
		background: linear-gradient(#93C020,#5f8206);
	}
	.counter.blue h3:before,
	.counter.blue h3:after{
		background: linear-gradient(to right bottom,transparent 49%,#b1f701 50%);
	}
	/*  */
	.chartjs-size-monitor .chartjs-render-monitor {
		height: 350px;
    	width: 98%;
	}
	.headerLine {
		width: 160px;
		height: 2px;
		display: inline-block;
		background: #101F2E;
	}
	/*  */
	.existFiliers {
		background-color: #e5ded45c;
		padding-bottom: 50px;
		margin-top: 25px;
		display: flex;
		justify-content: center;
		align-items: center;
		flex-wrap: wrap;
	}
	.existFiliers .container-fluid {
		border: 3px solid #84d70f;
		border-radius: 10px;
		height: 110px;
		margin-bottom: 10px;
	}

	.existFiliers .effectif:hover {
        transform: scale(1.05); /* Ajoute un effet de zoom au survol */
        transition: transform 0.3s ease; /* Ajoute une transition douce */
    }

	.existFiliers .container-fluid i {
		font-size: 37px;
		padding: 0.2em;
		margin-top: 0.3em;
		margin-bottom: auto;
		color: #84d70f;
	}

	.existFiliers .container-fluid h1 {
		color: #84d70f;
	}
	.existFiliers .container-fluid h5 {
		max-width: 100%; 
		overflow-wrap: break-word;
	}

</style>
<div class="observacity">
	
	<div class="observacityContainer ">
		<div class="">
			<div class="">
				<article class="">
					<div class="col-md-12" style="margin-bottom: 40px; ">
						<div class="col-md-offset-2 col-md-8 text-center">
							<h1 class="" style="text-transform: none;">OBSERVATOIRE </h1>
							<span class="headerLine"></span>
						</div>
					</div>
				</article>
			</div>
		</div>
		<div class="col-md-12 text-center">

			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="counter purple" data-type="projects">
								<div class="counter-icon">
									<i class="fa fa-rocket"></i>
								</div>
								<span class="counter-value"><?php echo count($projects) ?></span>
								<h3>Projets</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="counter blue" data-type="organizations">
								<div class="counter-icon">
									<i class="fa fa-users"></i>
								</div>
								<span class="counter-value"><?php echo count($organization) ?></span>
								<h3>Organisations</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<!--  -->
						<div class="col-md-3 col-sm-6">
							<div class="counter green" data-type="citoyens">
								<div class="counter-icon">
									<i class="fa fa-user"></i>
								</div>
								<span class="counter-value"><?php echo count($citoyens) ?></span>
								<h3>Personnes</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class=" col-md-3   col-sm-6 col-xs-12 ">
				<div class="container-fluid  stat-card">
					<div class="row">
						<!--  -->
						<div class="col-md-3 col-sm-6">
							<div class="counter" data-type="events">
								<div class="counter-icon">
									<i class="fa fa-calendar"></i>
								</div>
								<span class="counter-value"><?php echo count($event) ?></span>
								<h3>Evènement</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="col-lg-6 col-sm-12 col-md-12 col-xs-12 " style="margin-top:25px;">
			<div class="card-box card tilebox-one">
				<?php foreach ($blocks as $key => $value) {
					if(isset($value["graph"])){
						if($value["graph"]["key"] == "pieManyactivityFiliere"){
							?>
							<div>
								<h6 class=""  ><?php echo $value["title"]?> 
								</h6>
							</div>
							<div class="counterBlock" id="<?php echo $key?>" > </div> 
						<?php }
					}

				}; ?>
			</div>
		</div>
		<div class=" col-lg-6 col-sm-12 col-md-12 col-xs-12 " style="margin-top:25px;">
			<div class="card-box card tilebox-one">
				<?php foreach ($blocks as $key => $value) {
					if(isset($value["graph"])){
						if($value["graph"]["key"] == "pieManynombrecocity"){
							?>
							<div>
								<h6 class="" ><?php echo $value["title"]?> </h6>
							</div>
						<div class="counterBlock" id="<?php echo $key?>" > </div> 
					<?php }
					}       
				}; ?>
			</div>
		</div>
		<div class="row col-sm-12 existFiliers">
			
		</div>
		<div class="col-sm-12 canvasCocity"></div>
	</div>
</div>

<script type="text/javascript">
	var elCostum = <?= json_encode($el) ?>;
	<?php  
	foreach ($blocks as $id => $d) {
		if( isset($d["graph"]) ) {
			?>
			var <?php echo $d["graph"]["key"]."Data" ?> = <?php echo ( isset( $d["graph"]['data'] ) ) ? json_encode( $d["graph"]["data"] ) : "null" ?>;
			mylog.log('url', 'graphs data', '<?php echo $d["graph"]["key"] ?>Data',<?php echo $d["graph"]["key"] ?>Data);
		<?php }
	} ?>
	var poiListTags = <?= json_encode($tagsPoiList)?>;
	var newListP =  [];

	$.each(poiListTags,function(kT, vT){
		var nElt = {
			text : vT.text,
			weight : vT.weight,
			link : vT.link,
			handlers: {
				click: function(e) {
					location.hash = "#search?tags="+e.target.dataset.tag;
					urlCtrl.loadByHash(location.hash);
				}
			},
		};
		newListP.push(nElt);
	});

	function obsThemaCity(data){

		if(typeof costum.htmlobservaThematiqueCocity != "undefined" && costum.htmlobservaThematiqueCocity != '') {
			$(".existFiliers").html(costum.htmlobservaThematiqueCocity);
		} else {
			if(Object.keys(data).length > 0) {
				costum.htmlobservaThematiqueCocity = '';
				var src = `<div class="col-sm-12" style="margin-bottom: 25px; color: #00b5c0;"><h3 class="text-center">Thématiques communes</h3></div>`;
				$.each(data, function( index, value ) {
					if(typeof value.name != "undefined"){
						var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
						src += `
							<div class="col-md-2 col-sm-4 col-xs-6">
								<div class="container-fluid effectif">
									<div class="row">
										<div class="col-md-12">	
											<h5>${nameTheme}</h5>
										</div>
										<div class="col-md-4">
											<i class="fa ${value.icon}"></i>
										</div>
										<div class="col-md-8">
											<h1 class="element element${index} text-center">${getTotalNumberElement(index, elCostum.address, costum.typeCocity)}</h1>
										</div>
									</div>
								</div>
							</div>
						`;
					}
				});
				$(".existFiliers").html(src);
				costum.htmlobservaThematiqueCocity = src;
			}
		}
	}
	jQuery(document).ready( function() { 
		<?php  foreach ($blocks as $id => $d) {
			if( isset($d["graph"]) ) { ?>
				ajaxPost('#<?php echo $id?>', baseUrl+'<?php echo $d["graph"]["url"]?>'+"/id/<?php echo $d["graph"]["key"] ?>", null, function(){},"html");
			<?php }
		} ?>
		$('.canvasCocity').jQCloud(newListP, {
			height: 700,
			autoResize: true,
			shape: 'rectangular',
			colors: ["#8ABF32", "#1aa5b7", "#005E6F"],
			fontSize: { 
				from: 0.04,
				to: 0.01
			}
		});

		// $(".counter").on('click', function(){
		// 	var types = $(this).attr("data-type");
		// 	location.hash = "#search?types="+types;
		// 	urlCtrl.loadByHash(location.hash);
		// });

		if(costum.existFiliers && costum.existFiliers != [] && costum.existFiliers != null) {
			obsThemaCity(costum.existFiliers);
		} else {
			var params = {
				"contextId" : costum.contextId
			}
			ajaxPost(
				null,
				baseUrl+"/costum/cocity/getfiliere",
				params,
				function(data){ 
					costum.existFiliers = data;
					obsThemaCity(data);
				}
			) 
		}
	});

</script>