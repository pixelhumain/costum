<?php ?>
<style>

    .table-container {
        width: 100%;
        overflow-x: auto;
        max-height: 570px;
        overflow-y: auto; 
    }

    .table {
        width: 100%;
        border-collapse: collapse;
        margin: 20px 0;
        font-size: 16px;
        text-align: left;
    }

    .table thead {
        background-color: #f2f2f2;
        position: sticky;
        top: 0;
        z-index: 1;
    }

    .table th, .table td {
        border: 1px solid #ddd;
        padding: 8px;
    }

    .table th {
        padding-top: 12px;
        padding-bottom: 12px;
        background-color: #4CAF50;
        color: white;
        cursor: pointer;
    }

    .table tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    .table tr:hover {
        background-color: #ddd;
    }

    #close-graph-modal {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 12pt;
        margin-top: -30px;
        margin-right: -25px;
    }

    #modal-preview-coop {
        left: 0% !important;
        padding: 2%;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0,0,0,0.1);
    }

    .main-map {
        margin-top: 15px;
        height: 630px;
        position: fixed;
        width: 100%;
    }

    .btn-show-map, .btn-show-tableau, .btn-show-graph {
        margin: 10px 3px;
        font-size: 14px;
        width: 40%;
        border-radius: 4px;
        height: 34px;
        background-color: #0095FF;
        color: white;
        border: 2px solid transparent;
        float: right;
    }

    .btn-show-map:hover, .btn-show-tableau:hover, .btn-show-graph:hover {
        background-color: white;
        color: #0095FF;
        border-color: #0095FF;
    }  

    .leaflet-popup-content {
        background-color: #f9f9f9;
        border: 1px solid #ccc;
        border-radius: 8px; 
        padding: 16px; 
        font-size: 12px; 
        font-family: Arial, sans-serif;
        max-width: 400px;
        color: #333; 
    }

    .leaflet-popup-content h3 {
        margin-top: 0;
        font-size: 14px;
        font-weight: bold; 
    }

    .leaflet-popup-content .item {
        margin-bottom: 8px;
    }

    .leaflet-popup-content .label-popup {
        font-weight: bold;
        color: #555;
    }

    .leaflet-popup-content .value {
        color: #000;
        margin-left: 4px;
    }

    .graph-energie {
        overflow: auto;
    }

    .select-container {
        margin: 20px 0;
    }

    label {
        font-size: 13px;
        margin-bottom: 4px;
        display: block;
    }

    .custom-select {
        width: 100%;
        padding: 5px;
        border: 1px solid #007BFF;
        border-radius: 4px;
        background-color: white;
        font-size: 12px;
        color: #333;
        transition: border-color 0.3s ease;
    }

    .custom-select:hover,
    .custom-select:focus {
        border-color: #0056b3;
        outline: none;
    }

    .custom-select option {
        padding: 10px;
        color: #333;
    }

    #options-type {
        height: 30px;
    }
    .type-chart-buttons {
        display: flex;
        flex-direction: column;
        align-items: flex-start;
        gap: 10px;
    }

    .btn-type-chart {
        background-color: #f0f0f0;
        border: 1px solid #ccc;
        padding: 8px 12px;
        cursor: pointer;
        transition: background-color 0.3s, border-color 0.3s;
        font-size: 14px;
        width: 100%;
        text-align: center;
        border-radius: 4px;
        font-weight: bold;
    }

    .btn-type-chart.active {
        background-color: #0c2035;
        color: white;
        border-color: #0c2035;
    }

    .btn-type-chart:hover {
        background-color: #e0e0e0;
    }

    @media (max-width: 768px) {
        .type-chart-buttons {
            flex-direction: row;
            flex-wrap: wrap;
        }
        .btn-type-chart {
            flex: 1 1 48%;
            text-align: center;
        }
    }
</style>
<div class="allpreviews">
    <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <div class="row">
        <div class="col-md-10">
            <h3 class="section-title previews-title"></h3>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn-show-map hidden-xs list-affiche" id="showMap">
                <i class="fa fa-map-marker"></i> &nbsp;&nbsp;&nbsp; Carte
            </button>
            <button type="button" class="btn-show-tableau hidden-xs map-affiche hidden" id="showTableau">
                <i class="fa fa-calendar"></i> &nbsp;&nbsp;&nbsp; Tableau
            </button>
            <button type="button" class="btn-show-graph hidden-xs graph-affiche" id="showGraphe">
                <i class="fa fa-bar-chart"></i> &nbsp;&nbsp;&nbsp; Analyse
            </button>
        </div>
    </div>
    <div class="table-container table list-affiche">

    </div>
    <div class="main-map previews-map map-affiche hidden" id="map-energie">

    </div>
    <div class="graph-energie hidden">
        <div class="row">
            <div class="axe-y col-md-2 col-sm-6 col-xs-12">
                <div class="select-axe-y-func select-container">
                    <label for="options">Axe Y func :</label>
                    <select class="custom-select" id="axey-func">
                        <option value="AVG">Moyenne</option>
                        <option value="COUNT">Nombre</option>
                        <option value="Min">Minimum</option>
                        <option value="Max">Maximum</option>
                        <option value="STDDEV">Ecart type</option>
                        <option value="SUM" selected="selected">Somme</option>
                        <option value="QUANTILES">Percentile</option>
                        <option value="CONSTANT">Valeurs constante</option>
                    </select>
                </div>
            </div>
            <div class="axe-y col-md-2 col-sm-6 col-xs-12">
                <div class="select-axe-y select-container">
                </div>
            </div>
            <div class="axe-x col-md-2 col-sm-6 col-xs-12">
                <div class="select-axe-x select-container">
                </div>
            </div>
            <div class="ventiler col-md-2 col-sm-6 col-xs-12">
                <div class="select-ventiler select-container">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-sm-3 col-xs-12">
                <div class="type-chart-buttons">
                    <button class="btn-type-chart active" style="display:none" data-type="default">Par defaut</button>
                    <button class="btn-type-chart" data-type="column">Colonnes</button>
                    <button class="btn-type-chart" data-type="line">Ligne</button>
                    <button class="btn-type-chart" data-type="spline">Courbe</button>
                    <button class="btn-type-chart" data-type="area">Zone</button>
                    <button class="btn-type-chart" data-type="areaspline">Zone et Courbe</button>
                    <button class="btn-type-chart" data-type="bar">Barres</button>
                    <button class="btn-type-chart" data-type="polar">Polaire</button>
                </div>
            </div>
            <div class="col-md-10 col-sm-9 col-xs-12">
                <div id="graphContainer">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    let title = <?= json_encode($title) ?>;
    let region = <?= json_encode($region) ?>;
    let idDataset = "";
    let optionsPossible = [];
    let allColors = ['#FF5733','#33FF57','#3357FF','#FF33A6','#FF8C33','#33FFF5','#F5FF33','#A633FF','#33FF8C','#FF3333','#33FFBD','#FFC433','#8C33FF','#33A6FF','#FF3369'];
    let chartType = "column";
    var mapEnergie = new CoMap({
        container : "#map-energie",
        activePopUp : true,
        mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true,
            zoom : 2,
                scrollWheelZoom: false
        },
        mapCustom: {
            tile : "maptiler",
            getPopup: function(donnees) {
                let contenuPopup = '';

                for (let [label, valeur] of Object.entries(donnees)) {
                    if (typeof valeur === 'object') {
                        valeur = JSON.stringify(valeur);
                    }
                    if(label != 'geo' && label != 'geoPosition' && label != 'id' && label != 'localisation' && label != "geo_point_2d" && label != 'geo_shape')
                        contenuPopup += `<span class="label-popup">${label} : </span> <span class="value"> ${valeur}\n </span><br>`;
                }

                return contenuPopup;
            }
        },
    });

    let baseUrl = "";
    if(region != "Réunion") {
        baseUrl = `https://opendata.edf.fr`;
    } else {
        baseUrl = 'https://opendata-reunion.edf.fr';
    }

    function getFullGraphInterface(datasetId, width = 1500, height = 500) {
        // let baseUrl = 'https://opendata-reunion.edf.fr';

        let options = optionsPossible.reduce((acc, variable) => {
            acc.push(`disjunctive.${variable}`);
            return acc;
        }, []).join('&');
        
        
        let analyzeUrl = `${baseUrl}/explore/embed/dataset/${datasetId}/analyze/?${options}`;
        
        let iframe = `<iframe src="${analyzeUrl}" width="${width}" height="${height}" frameborder="0"></iframe>`;
        
        document.getElementById('graphContainer').innerHTML = iframe;
    }

    function generateGraphIframe(datasetId, variables, xAxis, yAxis, yAxisFunction, breakdown, width = 1500, height = 500) {
        // let baseUrl = 'https://opendata-reunion.edf.fr';

        let options = variables.reduce((acc, variable) => {
            acc[`disjunctive.${variable}`] = true;
            return acc;
        }, {});

        let uniqueValues = new Set();
        let data = costum.paramsOneJDD.records;
        data.forEach(d => uniqueValues.add(d[breakdown]));

        let colorMap = {};
        uniqueValues.forEach((value, index) => {
            colorMap[value] = allColors[index % allColors.length];
        });

        let series = [...uniqueValues].map(value => ({
            "type": chartType,
            "yAxis": yAxis,
            "xAxis": xAxis,
            "seriesBreakdown": breakdown,
            "color": colorMap[value],
            "func": yAxisFunction
        }));

        let dataChart = btoa(JSON.stringify({
            "queries": [{
                "charts": series,
                "xAxis": xAxis,
                "maxpoints": null,
                "seriesBreakdown": breakdown,
                "stacked": "normal",
                "config": {
                    "dataset": datasetId,
                    "options": options
                }
            }],
            "timescale": "",
            "displayLegend": true, 
            "alignMonth": true  
        }));

        let iframeUrl = `${baseUrl}/explore/embed/dataset/${datasetId}/analyze/?dataChart=${dataChart}&static=false&datasetcard=false`;

        let iframe = `<iframe src="${iframeUrl}" width="${width}" height="${height}" frameborder="0"></iframe>`;
        document.getElementById('graphContainer').innerHTML = iframe;
    }

    function correctHeader(field) {
        const corrections = {
            'turbines_a_combustion': 'Turbines à Combustion',
            'bioenergies': 'Bioénergies',
            'charbon': 'Charbon',
            'diesel': 'Diesel',
            'eolien': 'Éolien',
            'hydraulique': 'Hydraulique',
            'photovoltaique': 'Photovoltaïque',
            'statut': 'Statut',
            'stockage': 'Stockage',
            'total': 'Total',
            'jour': 'Jour',
            'date': 'Date',
            'annee' : 'Année'
        };
        return corrections[field] || field.charAt(0).toUpperCase() + field.slice(1);
    }

    function generateTable(records) {
        if (!records || records.length === 0) return;

        const fields = Object.keys(records[0].fields);
        let valuesToRemove = ["localisation", "geo_shape"];
        optionsPossible = fields.filter(item => !valuesToRemove.includes(item));
        idDataset = records[0].datasetid;
        
        let tableHtml = '<table class="table">';
        tableHtml += '<thead><tr>';
        fields.forEach(field => {
            if(field != 'geo_shape' && field != 'territoire')
                tableHtml += `<th onclick="sortTable('${field}')">${correctHeader(field)}</th>`;
        });
        tableHtml += '</tr></thead>';

        tableHtml += '<tbody id="table-body">';
        records.forEach(record => {
            tableHtml += '<tr>';
            fields.forEach(field => {
                if(field != 'geo_shape' && field != 'territoire')
                    tableHtml += `<td>${record.fields[field] || ''}</td>`;
            });
            tableHtml += '</tr>';
        });
        tableHtml += '</tbody>';

        tableHtml += '</table>';

        document.querySelector('.table-container').innerHTML = tableHtml;
        $(".previews-title").html(title);
    }

    function sortTable(field) {
        const tableBody = document.getElementById('table-body');
        const rows = Array.from(tableBody.getElementsByTagName('tr'));
        
        let sortedAsc = tableBody.getAttribute('data-sort') === field && tableBody.getAttribute('data-order') === 'asc';

        rows.sort((rowA, rowB) => {
            const cellA = rowA.querySelector(`td:nth-child(${getFieldIndex(field) + 1})`).innerText;
            const cellB = rowB.querySelector(`td:nth-child(${getFieldIndex(field) + 1})`).innerText;

            const valA = isNaN(cellA) ? cellA : parseFloat(cellA);
            const valB = isNaN(cellB) ? cellB : parseFloat(cellB);

            if (valA < valB) return sortedAsc ? 1 : -1;
            if (valA > valB) return sortedAsc ? -1 : 1;
            return 0;
        });

        tableBody.innerHTML = '';
        rows.forEach(row => tableBody.appendChild(row));

        tableBody.setAttribute('data-sort', field);
        tableBody.setAttribute('data-order', sortedAsc ? 'desc' : 'asc');
    }

    function getFieldIndex(field) {
        const headers = document.querySelectorAll('th');
        return Array.from(headers).findIndex(th => th.textContent.toLowerCase() === correctHeader(field).toLowerCase());
    }

    function setMapEnergieData(records) {
        let mapData = {};
        const fields = Object.keys(records[0].fields);
        records.forEach(record => {
            if(typeof record.geometry != 'undefined') {
                let coordinates = record.geometry.coordinates;
                let paramsMap = {};
                paramsMap['geo'] = {
                    "@type" : "GeoCoordinates",
                    "latitude" : String(coordinates[1]),
                    "longitude" : String(coordinates[0])
                };

                paramsMap['geoPosition'] = {
                    "type" : "Point",
                    "float" : "true",
                    "coordinates" : [ 
                        coordinates[0], 
                        coordinates[1]
                    ]
                };
                fields.forEach(field => {
                    paramsMap[field] = record.fields[field];
                    // paramsMap['name'] = record.fields['poste'];
                });

                mapData[record.recordid] = paramsMap;
            }
        });
        mapEnergie.clearMap();
        mapEnergie.addElts(mapData);
    }

    function generateConfigSelect(options, div_class, id, labelText) {
        let labelElement = document.createElement('label');
        labelElement.setAttribute('for', id);
        labelElement.textContent = labelText; 

        let selectElement = document.createElement('select');
        selectElement.classList.add('custom-select');
        selectElement.id = id ;
        options.forEach(optionText => {
            let optionElement = document.createElement('option');
            optionElement.value = optionText; 
            optionElement.textContent = optionText; 
            selectElement.appendChild(optionElement);
        });

        $("." + div_class).html(labelElement).append(selectElement);
    }

    jQuery(document).ready(function() {
        if(typeof costum.paramsOneJDD != "undefined") {
            generateTable(costum.paramsOneJDD.records);
        }

        $("#showMap").on('click', function(){
            $(".list-affiche").addClass('hidden');
            $(".map-affiche").removeClass('hidden');
            $(".btn-show-graph").removeClass('hidden');
            $(".graph-energie").addClass('hidden');
            setMapEnergieData(costum.paramsOneJDD.records);
        });

        $("#showTableau").on('click', function(){
            $(".map-affiche").addClass('hidden');
            $(".graph-energie").addClass('hidden');
            $(".list-affiche").removeClass('hidden');
            $(".btn-show-graph").removeClass('hidden');
        });

        $("#showGraphe").on('click', function(){
            $(".table-container").addClass('hidden');
            $(".previews-map").addClass('hidden');
            $(".btn-show-graph").addClass('hidden');

            $(".btn-show-map").removeClass('hidden');
            $(".btn-show-tableau").removeClass('hidden');
            $(".graph-energie").removeClass('hidden');

            getFullGraphInterface(idDataset);
            generateConfigSelect(optionsPossible, 'select-axe-y', 'option-axeY', 'Axe Y');
            generateConfigSelect(optionsPossible, 'select-axe-x', 'option-axeX', 'Axe X');
            generateConfigSelect(optionsPossible, 'select-ventiler', 'option-ventiler', 'Ventiler les séries :');

            $(".custom-select").on('change', function(){
                let axeX = $("#option-axeX").val();
                let axeY = $("#option-axeY").val();
                let axeY_func = $("#axey-func").val();
                let ventiler = $("#option-ventiler").val();
                generateGraphIframe(idDataset, optionsPossible, axeX, axeY, axeY_func, ventiler);
            });

            $(".btn-type-chart").on('click', function(){
                document.querySelector('.btn-type-chart.active').classList.remove('active');
                this.classList.add('active');
                chartType = this.getAttribute('data-type');
                let axeX = $("#option-axeX").val();
                let axeY = $("#option-axeY").val();
                let axeY_func = $("#axey-func").val();
                let ventiler = $("#option-ventiler").val();
                generateGraphIframe(idDataset, optionsPossible, axeX, axeY, axeY_func, ventiler);
            });
        });
    });
</script>