<?php
    $_dataCity = [];
    $_annuaires = [];
    if(isset($_id)) {
        $_dataCity = Element::getElementById($_id,Organization::COLLECTION);

        $_paramsFilters = [
            'searchType' => ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
            'notSourceKey' => true,
            'filters' => [
                '$or' => [
                    'source.keys' => $_dataCity['slug'],
                    'address.level3' => $_dataCity['address']['level3']
                ]
            ],
            'countType' => ['events', 'organizations', 'poi', 'projects', 'classifieds', 'citoyens', 'ressources'],
            'indexStep' => 0,
            'onlyCount' => true,
            'count' => true
        ];

        $_annuaires = SearchNew::globalAutoComplete($_paramsFilters, null, true);

        // if(isset($_dataCity['costum']['typeCocity'])) {}
    }
?>
<style>
    #modal-preview-coop {
        left: 0% !important;
    }

    #close-modal-previews-eau {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 12pt;
    }

    .fiche {
        width: 85%;
        max-width: 1200px;
        margin: 20px auto;
        background: #fff;
        border-radius: 10px;
        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        overflow: auto;
        height: 80vh;
    }

    .fiche .cote_cote {
        display: flex;
        overflow: hidden;
    }

    .cote_cote .fiche-left {
        flex: 1;
        max-width: 35%;
        background: #f0f0f0;
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 20px;
    }

    .cote_cote .fiche-img {
        /* width: 100%;
        height: auto;
        object-fit: cover; */
        width: auto;
        max-height: 260px;
        object-fit: cont
    }

    .cote_cote .fiche-right {
        flex: 2;
        padding: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }

    .cote_cote .fiche-title {
        font-size: 1.6em;
        font-weight: bold;
        color: #333;
        margin-bottom: 5px;
        margin-top: 10px !important;
    }

    .cote_cote .fiche-address {
        font-size: 1.3em;
        color: #666;
        margin-bottom: 10px;
    }

    .cote_cote .fiche-tags {
        margin-bottom: 15px;
    }

    .cote_cote .tag {
        display: inline-block;
        background: #043a6f;
        color: #fff;
        padding: 6px 12px;
        border-radius: 5px;
        font-size: 1em;
        margin: 5px 3px;    
        font-weight: 600;
    }

    .fiche-elements {
        margin-top: 2px;
        padding: 5px;
        margin-left: 10px;
    }

    .fiche-elements h3 {
        font-size: 1.2em;
        color: #043a6f;
    }

    .fiche-elements .sec_title {
        border-left: 5px solid #007bff;
        padding: 10px;
    }

    #fiche-annuaire, #fiche-thematique {
        display: flex;
        flex-wrap: wrap;
        gap: 10px;
        justify-content: center;
        padding: 20px;
    }

    #fiche-annuaire .card, #fiche-thematique .card {
        width: 165px;
        height: 100px; 
        background-color: #ccc;
        color: black;
        display: flex;
        flex-direction: column;
        align-items: center;
        /* justify-content: center; */
        text-align: center;
        border-radius: 10px;
        box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.5);
        font-size: 1em;
        font-weight: 600;
    }

    #fiche-annuaire .card h3, #fiche-thematique .card h3 {
        text-transform: none;
        margin: 1px 0px;
    }

    .sec_desc {
        font-size: 1.2em;
        text-align: justify;
    }

    .main-section {
        width: 100%;
        display: flex;
        font-size: 1.1em;
        justify-content: center;
    }

    .main-section .fa {
        color: #043a6f;
        margin-left: 6px;
        margin-top: 2px;
        margin-right: 2px;
    }

    .count_annuaire {
        height: 40px;
        width: 100%;
        background-color: #083557;
        padding-top: 10px;
        color: white;
        font-weight: bold;
    }
</style>
<div class="all-previews-annuaire">
    <button id="close-modal-previews-eau" class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <div class="fiche-container" id="fiche-container">

    </div>
</div>
<script>
    var _id = <?= json_encode($_id) ?>;
    var _dataCitie = <?= json_encode($_dataCity) ?>;
    var _annuaire = <?= json_encode($_annuaires) ?>;
    
    if(_dataCitie != null && _dataCitie != []) {
        var img = new Image();
        img.src = _dataCitie.profilImageUrl;
        var urlImgProfil = "";
        img.onload = function() {
            setImgCitie(_dataCitie.profilImageUrl);
        };

        img.onerror = function() { 
            setImgCitie(`${modules.costum.url}/images/thumbnail-default.jpg`); 
        };

        function setImgCitie(imgUrl) {
            generateFiche(_dataCitie, imgUrl)
        }
    }

    function generateFiche(data, imgUrl) {
        var container = document.getElementById("fiche-container");
        let tagsList = "";
        if (data.tags && data.tags.length > 0) {
            tagsList = data.tags.map(theme => `<span class="tag">${theme}</span>`).join(" ");
        }

        let descript = '';
        if(typeof data.shortDescription != 'undefined') descript = `<p class="sec_desc">${data.shortDescription}</p>`;
        
        container.innerHTML = `
            <div class="fiche">
                <div class="cote_cote">
                    <div class="fiche-left">
                        <img src="${imgUrl}" alt="Profil Image" class="fiche-img">
                    </div>
                    <div class="fiche-right">
                        <h2 class="fiche-title">${data.name}</h2>
                        <p class="fiche-address"><i class='fa fa-map-marker'></i> &nbsp;&nbsp; ${data.address.level3Name}, ${data.address.level1Name}</p>
                        ${descript}
                        <div class="fiche-tags">
                            ${tagsList}
                        </div>
                    </div>
                </div>
                <div class="fiche-elements">
                    <h3 class="sec_title">Les éléments existant dans ce territoire.</h3>
                    <div id="fiche-annuaire">
                    
                    </div>

                    <h3 class="sec_title">Les thématiques actives et leur nombre d'éléments associés.</h3>
                    <div id="fiche-thematique">
                    
                    </div>
                </div>
            </div>
        `;

        if (typeof _annuaire.count !== "undefined" && Object.keys(_annuaire.count).length > 0)  {
            var container = document.getElementById("fiche-annuaire");
            container.innerHTML = "";
            for (var col in _annuaire.count) {
                if(_annuaire.count[col] > 0) {
                    var card = document.createElement("div");
                    card.classList.add("card");
                    card.innerHTML = `<p class="count_annuaire">${_annuaire.count[col]}</p>
                                    <h3 class="name_annuaire"> ${trad[col]} </h3>`

                    container.appendChild(card);
                };
            }
        }

        if(typeof data.filiere != "undefined" && Object.keys(data.thematic).length > 0) {
            var positionF = typeof data.address != "undefined" ? data.address : "";
            $.each(data.filiere, function(index, value){
                getDataOneThema(data, value, index, positionF);
            })
        }
    };

    function getDataOneThema(data, value, thematic, positionF) {
        var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
        tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
        : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
        var params = {
            "cocity": data._id['$id'],
            "thematic" : nameTheme,
            "thema" : thematic,
            "typeCocity" : data.costum.typeCocity,
            "tags" : value.tags,
            "address" : positionF
        }

        ajaxPost(
            null,
            baseUrl+"/costum/cocity/getorgafiliere",
            params,
            function(data){
                var counts = [];
                if (data.countCitoyens) counts.push(`<i class="fa fa-user" aria-hidden="true"></i> ${data.countCitoyens}`);
                if (data.countEvent) counts.push(`<i class="fa fa-calendar" aria-hidden="true"></i> ${data.countEvent}`);
                if (data.countOrga) counts.push(`<i class="fa fa-users" aria-hidden="true"></i> ${data.countOrga}`);
                if (data.countProject) counts.push(`<i class="fa fa-lightbulb-o" aria-hidden="true"></i> ${data.countProject}`);

                var htmlNbElm = counts.length ? `<div class="main-section">${counts.join(" ")}</div>` : '';

                var html = `<div class="card">
                                <p class="count_annuaire">${nameTheme}</p>
                                ${htmlNbElm ? `<p>${htmlNbElm}</p>` : ''}
                            </div>`;

                $("#fiche-thematique").append(html);
            }
        )
    }

</script>