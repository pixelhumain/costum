<?php ?>
<style>

    #modal-preview-coop {
        left: 0% !important;
    }

    .previewair_container {
        overflow: auto;
        height: 90vh;
    }

    .previewair_container::-webkit-scrollbar{
        width: 8px;
    }

    .previewair_container::-webkit-scrollbar-thumb{
        background: #ccc;
        border-radius: 5px;
    }

    .previewair_container::-webkit-scrollbar-thumb:hover {
        background: #aaa;
    }

    .previewair_container::-webkit-scrollbar-track {
        background: #f4f4f4;
        border-radius: 5px;
    }


    #chartCanvasSensor {
        /* width: 80%; */
        max-height: 80vh;
    }

    #close-modal-previews-air {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 12pt;
    }

    .filter-container {
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        /* justify-content: center; */
        gap: 10px;
        margin-bottom: 10px;
        margin-top: 10px;
        margin-left: 20px;
    }

    .input-group {
        position: relative;
        width: 200px;
    }

    .input-group select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-image: url('data:image/svg+xml,%3csvg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"%3e%3cpath fill="none" stroke="%23666" d="M4 7l6 6 6-6"%3e%3c/path%3e%3c/svg%3e');
        background-repeat: no-repeat;
        background-position: right 10px center;
        background-size: 12px;
    }

    .input-group input,
    .input-group select {
        width: 100%;
        padding: 10px 10px 5px;
        font-size: 16px;
        border: 1px solid #ccc;
        border-radius: 5px;
        outline: none;
        transition: border 0.3s ease;
    }

    .input-group label {
        position: absolute;
        left: 10px;
        top: 50%;
        transform: translateY(-50%);
        background: white;
        padding: 0 5px;
        font-size: 14px;
        color: #777;
        transition: all 0.3s ease;
        pointer-events: none;
    }

    .input-group input:focus,
    .input-group select:focus,
    .input-group input:not(:placeholder-shown),
    .input-group select:not(:placeholder-shown) {
        border-color: #126595;
    }

    .input-group input:focus + label,
    .input-group select:focus + label,
    .input-group input:not(:placeholder-shown) + label,
    .input-group select:not(:placeholder-shown) + label {
        top: 0;
        font-size: 12px;
        color: #126595;
    }


    /* Responsivité */
    @media (max-width: 480px) {
        .filter-container {
            flex-direction: column;
            gap: 5px;
        }

        .input-group {
            width: 100%;
        }
    }

    .title_sensor {
        text-transform: none;
    }
</style>
<div class="previewair_container">
    <button id="close-modal-previews-air" class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <h3 class="text-center title_sensor"></h3>
    <hr>
    <div class="filter-container">
        <div class="input-group">
            <input type="datetime-local" id="heureDebut" name="heureDebut">
            <label for="heureDebut">Heure de début</label>
        </div>

        <div class="input-group">
            <input type="datetime-local" id="heureFin" name="heureFin">
            <label for="heureFin">Heure de fin</label>
        </div>

        <div class="input-group">
            <select id="interval" name="interval">
                <option value="15">15 minutes</option>
                <option value="30">30 minutes</option>
                <option value="60" selected>1 heure</option>
                <option value="300">5 heures</option>
            </select>
            <label for="interval">Intervalle</label>
        </div>
    </div>
    <canvas id="chartCanvasSensor"></canvas>
</div>
<script>
    var _id_sensor = <?= json_encode($id_sensor) ?>;
    $(".title_sensor").html(`Capteur ${_id_sensor}`);

    var seuilInstant = {
        pm1: [10, 20, 30],       
        pm25: [12, 35.4, 55.4], 
        pm10: [20, 50, 100]      
    };

    var seuilJournal = {
        pm1: [8, 17, 26], 
        pm25: [7.5, 15, 25],
        pm10: [22.5, 45, 75]
    }

    function getColor(value, type, isDaily = false) {
        var colors = ["green", "yellow", "orange", "red"];
        var seuils = isDaily ? seuilJournal : seuilInstant; 
        
        if (!seuils[type]) return "gray";

        var seuil = seuils[type];

        if (value <= seuil[0]) return "green"; 
        if (value <= seuil[1]) {
            let ratio = (value - seuil[0]) / (seuil[1] - seuil[0]);
            return interpolateColor("green", "yellow", ratio);
        }
        if (value <= seuil[2]) { 
            let ratio = (value - seuil[1]) / (seuil[2] - seuil[1]);
            return interpolateColor("yellow", "orange", ratio);
        }

        return "red";
    }

    function interpolateColor(color1, color2, ratio) {
        function hexToRgb(hex) {
            let bigint = parseInt(hex.slice(1), 16);
            return [(bigint >> 16) & 255, (bigint >> 8) & 255, bigint & 255];
        }

        function rgbToHex(r, g, b) {
            return `#${((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)}`;
        }

        var colorMap = {
            "green": "#00FF00",
            "yellow": "#FFFF00",
            "orange": "#FFA500",
            "red": "#FF0000"
        };

        let rgb1 = hexToRgb(colorMap[color1]);
        let rgb2 = hexToRgb(colorMap[color2]);

        let r = Math.round(rgb1[0] + (rgb2[0] - rgb1[0]) * ratio);
        let g = Math.round(rgb1[1] + (rgb2[1] - rgb1[1]) * ratio);
        let b = Math.round(rgb1[2] + (rgb2[2] - rgb1[2]) * ratio);

        return rgbToHex(r, g, b);
    }


    function evaluateAirQuality(pm1, pm25, pm10, isDaily = false) {
        var seuil = isDaily ? seuilJournal : seuilInstant;
        
        function getQualityLevel(value, thresholds) {
            if (value <= thresholds[0]) return 0; 
            if (value <= thresholds[1]) return 1; 
            if (value <= thresholds[2]) return 2; 
            return 2;
        }

        var pm1Level = getQualityLevel(pm1, seuil.pm1);
        var pm25Level = getQualityLevel(pm25, seuil.pm25);
        var pm10Level = getQualityLevel(pm10, seuil.pm10);

        var levels = [pm1Level, pm25Level, pm10Level];
        var levelCounts = [0, 0, 0]; 
        levels.forEach(level => levelCounts[level]++);

        if (levelCounts[0] === 1 && levelCounts[1] === 1 && levelCounts[2] === 1) {
            return "Qualité de l'air moyenne"; 
        }

        var majorityLevel = levelCounts.indexOf(Math.max(...levelCounts));
        
        switch (majorityLevel) {
            case 0:
                return "Bonne qualité de l'air"; 
            case 1:
                return "Qualité de l'air moyenne"; 
            case 2:
                return "Qualité de l'air mauvaise"; 
            default:
                return "Inconnu";
        }
    }

    function setChartCapteur(id_sensor, heureDebut, heureFin, interval = 60) {
        var sensorData = costum.armotrackData.filter(item => item.id_sensor === id_sensor);
        interval = parseInt(interval, 10);

        if (sensorData.length === 0) {
            toastr.warning("Aucune donnée trouvée pour le capteur " + id_sensor);
            return;
        }

        if (heureDebut && heureFin) {
            var debut = new Date(heureDebut);
            var fin = new Date(heureFin);
            sensorData = sensorData.filter(item => {
                var itemDate = new Date(item.time);
                return itemDate >= debut && itemDate <= fin;
            });
        } else {
            var datelimitmin = new Date(sensorData[sensorData.length - 1].time);
            var datelimitmax = new Date(sensorData[0].time);

            $("#heureDebut").val(datelimitmin.toISOString().slice(0, 16));
            $("#heureFin").val(datelimitmax.toISOString().slice(0, 16));
        }

        sensorData.sort((a, b) => new Date(a.time) - new Date(b.time));
        
        var filteredData = [];
        var lastAddedTimestamp = null;

        sensorData.forEach((item) => {
            var itemDate = new Date(item.time);
            var addData = false;

            switch (interval) {
                case 15:
                    if (lastAddedTimestamp === null || itemDate.getMinutes() % 15 === 0) {
                        addData = true;
                    }
                    break;
                case 30:
                    if (lastAddedTimestamp === null || itemDate.getMinutes() % 30 === 0) {
                        addData = true;
                    }
                    break;
                case 60:
                    if (lastAddedTimestamp === null || (itemDate.getMinutes() === 0 && itemDate.getSeconds() === 0)) {
                        addData = true;
                    }
                    break;
                case 300:
                    if (lastAddedTimestamp === null || (itemDate.getHours() % 5 === 0 && itemDate.getMinutes() === 0)) {
                        addData = true;
                    }
                    break;
                default:
                    addData = true;
            }

            if (addData) {
                filteredData.push(item);
                lastAddedTimestamp = itemDate;
            }
        });

        var timestamps = filteredData.map(item => new Date(item.time));

        var labels = [];
        var previousLabel = "";

        timestamps.forEach((date) => {
            var heures = date.getHours();
            var minutes = date.getMinutes();
            var label = `${date.getDate()}/${date.getMonth() + 1} ${heures}h${minutes === 0 ? '' : minutes}`;

            if (label !== previousLabel) {
                labels.push(label);
                previousLabel = label;
            }
        });

        var pollutants = {};
        filteredData.forEach(item => {
            Object.entries(item.data).forEach(([key, value]) => {
                if (!pollutants[key]) {
                    pollutants[key] = [];
                }
                pollutants[key].push(value);
            });
        });
        
        var colors = ["blue", "orange", "green", "#4bc0c0", "#9966ff", "#ff9f40"];
        var datasets = Object.keys(pollutants).map((key, index) => ({
            label: key.toUpperCase(),
            data: pollutants[key],
            borderColor: colors[index % colors.length],
            backgroundColor: colors[index % colors.length],
            pointBackgroundColor: pollutants[key].map(value => getColor(value, key, false)),
            pointBorderColor: pollutants[key].map(value => getColor(value, key, false)),
            fill: false
        }));

        let canvas = document.getElementById("chartCanvasSensor");
        if (!canvas) {
            canvas = document.createElement("canvas");
            canvas.id = "chartCanvasSensor";
            document.body.appendChild(canvas);
        } else {
            canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
        }

        if (Chart.getChart("chartCanvasSensor")) {
            Chart.getChart("chartCanvasSensor").destroy();
        }

        new Chart(canvas, {
            type: "line",
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: "top"
                    }
                },
                scales: {
                    x: { title: { display: true, text: "Temps" } },
                    y: { title: { display: true, text: "Valeur" } }
                }
            }
        });
    }

    var heureDebutInput = $('#heureDebut');
    var heureFinInput = $('#heureFin');
    var intervalDropdown = $('#interval');

    heureDebutInput.on('change', function() {
        var heureDebut = heureDebutInput.val();
        var heureFin = heureFinInput.val();
        var interval = intervalDropdown.val();

        if (!heureDebut || !heureFin) {
            toastr.warning("Veuillez saisir les heures de début et de fin.");
            return;
        }
        setChartCapteur(_id_sensor, heureDebut, heureFin, interval);
    });

    heureFinInput.on('change', function() {
        var heureDebut = heureDebutInput.val();
        var heureFin = heureFinInput.val();
        var interval = intervalDropdown.val();

        if (!heureDebut || !heureFin) {
            toastr.warning("Veuillez saisir les heures de début et de fin.");
            return;
        }
        setChartCapteur(_id_sensor, heureDebut, heureFin, interval);
    });

    intervalDropdown.on('change', function() {
        var interval = intervalDropdown.val();
        var heureDebut = heureDebutInput.val();
        var heureFin = heureFinInput.val();

        if (!heureDebut || !heureFin) {
            setChartCapteur(_id_sensor, null, null, interval);
            return;
        }

        setChartCapteur(_id_sensor, heureDebut, heureFin, interval);
    })

    // var heureDebutInput = document.getElementById('heureDebut');
    // var heureFinInput = document.getElementById('heureFin');
    // var boutonFiltrer = document.getElementById('boutonFiltrer');

    // boutonFiltrer.addEventListener('click', () => {
    //     var heureDebut = heureDebutInput.value;
    //     var heureFin = heureFinInput.value;

    //     if (!heureDebut || !heureFin) {
    //         toastr.warning("Veuillez saisir les heures de début et de fin.");
    //         return;
    //     }
    //     setChartCapteur(_id_sensor, heureDebut, heureFin);
    // });

    // var timestamps = sensorData.map(item => new Date(item.time));

    // var labels = timestamps.map((date, index) => {
    //     return index === 0 || date.getMinutes() === 0 ? `${date.getHours()}h` : "";
    // }); || ou

    // var labels = sensorData.map(item => new Date(item.time).toLocaleString());

    setChartCapteur(_id_sensor);

</script>