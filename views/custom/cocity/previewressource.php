<?php ?>
<style>
    h4 {
        text-align: center;
        text-transform: none;
        color: #333;
    }

    #modal-preview-coop {
        left: 50% !important;
        padding: 2%;
        border-radius: 8px;
        box-shadow: 0 4px 8px rgba(0,0,0,0.1);
    }

    .resourcePreview {
        padding: 20px;
        padding: 20px;
        max-height: 600px;
        overflow-y: auto;
        position: relative;
        flex-direction: column;
        justify-content: flex-start;
    }

    .resourcePreview span {
        font-size: 15px;
        color: #0c2035;
    }
    .resourcePreview .btn-tag-panel span {
        font-size: 13px;
        color: #ffefef;
        background: #00b5c0;
    }

    .resourcePreview hr {
        height: 2px;
        background: #000;
        margin: 10px 35%;
    }

    .image-placeholder {
        height: 15vh;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: #e9e9e9;
        border: 2px dashed #ccc;
        margin-bottom: 20px;
    }

    .image-placeholder img {
        max-height: 100%;
        max-width: 100%;
    }

    .btn-tag-preview {
        font-weight: 300;
        padding: 5px 10px;
        background: #f3f3f3;
        color: #333;
        border: 1px solid #ccc;
        margin-top: 5px;
        border-radius: 5px;
        display: inline-block;
    }

    .tags-list {
        padding: 0;
        list-style-type: none;
    }

    .tags-list a {
        text-decoration: none;
    }

    .address {
        margin-top: 20px;
        font-size: 16px;
        color: #0c2035;
        text-align: center;
    }

    .address i {
        margin-right: 5px;
        font-weight: bold;
    }

    #close-graph-modal {
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
        margin-top: -25px;
        margin-right: -15px;
    }

    .div-img img {
        max-width: 60%;
        border-radius: 10px;
    }

    .div-img {
        margin-bottom: 15px;
        padding-top: 5px;
        padding-bottom: 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .resourcePreview h4 {
        position: relative;
        padding: 10px 20px;
        margin: 1px 0px;
        margin-left: -15px;
        margin-right: -15px;
        line-height: 15px;
        font-weight: bold;
        color: #fff;
        background: #2BB0C6;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        zoom: 1;
        text-transform: none;
        width: -webkit-fill-available;
    }

    .resourcePreview h4::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 0;
        border-width: 0 14px 12px 0;
        border-style: solid;
        border-color: transparent #115052;
    }

    .resourcePreview h4::after {
        right: 0 !important;
        border-width: 0 0 12px 14px !important;
        left: auto;
        content: "";
        position: absolute;
        top: 100%;
        border-style: solid;
        border-color: transparent #115052;
    }

    .resourcePreview .address h5 {
        text-transform: none;
        font-size: 17px;
    }

    .resourcePreview .address h5 span {
        text-transform: none;
        font-size: 17px;
    }

    .resourcePreview .typeElement {
        margin-bottom: 10px;
    }

    .previews-elements hr.hr10 {
        margin-bottom: 10px;
        margin-top: 10px;
        border: 0;
        border-top: 1px solid #eee;
    }

    .previews-elements .btn-link {
        color: #fff;
        background-color: #9fbd38;
        border: 1px solid #fff;
        border-radius: 4px;
        margin: 4px;
        min-width: 105px;
    }

    .resourcePreview::-webkit-scrollbar {
        width: 5px; 
    }

    .resourcePreview::-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1);
    }

    .resourcePreview::-webkit-scrollbar-thumb {
        background-color: rgba(255, 255, 255, 0.5); 
        border-radius: 10px; 
    }

    .resourcePreview::-webkit-scrollbar-thumb:hover {
        background-color: rgba(255, 255, 255, 0.8); 
    }

</style>
<div class="previews-elements">
    <div>
        <?php if(@Yii::app()->session["userId"] && Yii::app()->params['rocketchatEnabled'] )
	  			{
	  				$creator = Person::getById($params["creator"]);
	  	  	?>
			<button class="btn btn-link bg-azure" id="btn-private-contact" data-name-el="<?php echo $creator['name']; ?>" data-username="<?php echo @$creator['username']; ?>" data-id="<?php echo $params['creator']; ?>" >
				<i class="fa fa-comments"></i> <?php echo Yii::t("ressources", "Send a private message to the author");  ?>
			</button>
		<?php } ?>
        <?php if(isset($params['collection']) && isset($params['type'])) { ?>
            <button id="btn-share-<?= $params['type'] ?>" class="btn btn-link btn-info btn-share-panel" data-ownerlink="share" data-id="<?= $params['_id']['$id'] ?>" data-type="<?= $params['collection'] ?>">
                <i class="fa fa-share"></i> <?php echo Yii::t("common", "Share")?>
            </button>
        <?php } ?>
        <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
    </div>
    <hr class="hr10">
    <div id="resourcePreview" class="resourcePreview">
        <div class="typeElement">
            <?php if (isset($params['section'])): ?>
                <span> <?php echo Yii::t("category",$params['section']) ?></span>
            <?php endif; ?>

            <?php if (isset($params['categorie'])): ?>
                <span> > <?php echo Yii::t("category",$params['category']) ?></span>
            <?php endif; ?>

            <?php if (isset($params['subtype'])): ?>
                <span> > <?php echo Yii::t("category",$params['subtype']) ?></span>
            <?php endif; ?>    
        </div>
        <?php if (isset($params['name'])): ?>
            <div class="col-md-12 no-padding title text-left">
                <h4 class="pull-left"><?= htmlspecialchars($params['name']) ?></h4>
            </div>
        <?php endif; ?>
        <?php if (isset($params['profilImageUrl'])){ ?>
            <div class="div-img text-center" style="height:auto; min-height: 160px">
                <img src="<?= htmlspecialchars($params['profilImageUrl']) ?>" alt="">
            </div>
        <?php } else {?> 
            <div class="div-img" style="height:auto; min-height: 160px">
                <i class="fa fa-image fa-2x"></i>
            </div>
        <?php } ?>
        <!-- <hr> -->
        <?php if (isset($params['description'])): ?>
            <p style="text-align: justify;"><?= htmlspecialchars($params['description']) ?></p>
        <?php endif; ?>
        <!-- <hr> -->
        <?php if (isset($params['address'])): ?>
            <div class="address">
                <h5>
                    <i class="fa fa-map-marker text-bold"> </i>
                    <span>
                        <?= htmlspecialchars($params['address']['addressLocality'] ?? '') ?>,
                        <?= htmlspecialchars($params['address']['postalCode'] ?? '') ?>,
                        <?= htmlspecialchars($params['address']['level4Name'] ?? '') ?>,
                        <?= htmlspecialchars($params['address']['level3Name'] ?? '') ?>,
                        <?= htmlspecialchars($params['address']['level1Name'] ?? '') ?>
                    </span>
                </h5>
            </div>
        <?php endif; ?>
        <?php if (isset($params['tags']) && is_array($params['tags']) && count($params['tags']) > 0): ?>
            <ul class="tags-list text-center">
                <?php foreach ($params['tags'] as $tag): ?>
                    <a href="javascript:;" class="btn-tag-panel">
                        <span class="badge btn-tag-preview commonClassTags">#<?= htmlspecialchars($tag) ?></span>
                    </a>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
<script>
	$("#btn-private-contact").click(function(){
        var nameElo = $(this).data("name-el");
        var idEl = $(this).data("id");
        var usernameEl = $(this).data("username");
        var ctxData = {
            name 	 : nameElo,
            type  	 : "citoyens",
            username : usernameEl,
            id 		 : idEl
        };

        rcObj.loadChat(nameElo ,"citoyens" ,true ,true, ctxData );
    });

    $('.btn-share-panel').off().click(function () {
        directory.showShareModal($(this).attr('data-type'), $(this).attr('data-id'));
    });
</script>