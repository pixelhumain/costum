<?php 
    $orga = "";
    $slug = "";
    $adres = "";
    $descriptionPersonne = "";
    $linksdbpedia = "";

    
    if(isset($address) && $address != "") {
        $orga = PHDB::findOne(Organization::COLLECTION,array(
			"name" => $name,
			"address" => $address
		));
        if($orga != "") {
            $slug = $orga["slug"];
            $adres = $orga["address"];
        }
    }

    if($type == 'personne') {
        $endpointUrl = 'http://dbpedia.org/sparql';

        $nom_formate = str_replace(' ', '_', $name);

        $sparqlQuery = "
        SELECT ?description 
            WHERE {
                <http://dbpedia.org/resource/" . $nom_formate . "> dbo:abstract ?description.  
                FILTER (LANG(?description) = 'fr')
            }
        ";
        $queryParams = [
            'query' => $sparqlQuery,
            'format' => 'application/json',
        ];
        $url = $endpointUrl . '?' . http_build_query($queryParams);
        $response = file_get_contents($url);
        $dbpedia_data = json_decode($response, true);

        if($dbpedia_data["results"]["bindings"] != null) {
            $value = $dbpedia_data["results"]["bindings"][0];
            $descriptionPersonne = $value["description"]["value"];
        } 

        $linksdbpedia = "http://dbpedia.org/resource/".$nom_formate;
    }
?>
<style type="text/css">

    #preview-containaire {
        overflow: auto;
        max-height: 90vh; 
        height: 85vh;
    }

    #prview-content {
        margin: 5%;
    }

    @media (max-width: 767px) {
        #prview-content {
            margin-top: 15%;
        }
    }

    #preview-containaire::-webkit-scrollbar {
        width: 2px; 
    }

    #preview-containaire::-webkit-scrollbar-thumb {
        background-color: #888; 
    }

    .pull-right {
        margin: 5px;
    }

    .description-orga {
        text-align: Justify;
        margin-top: 1%;
    }

    #preview-containaire .image-content {
        margin-bottom: 2%;
    }

    #preview-containaire .image-content img {
        width: 50%;
        max-height: 500px;
    }

</style>

<div id="preview-containaire" class="text-center">
    <div class="pull-right">
        <button class="btn btn-default btn-close-preview">
            <i class="fa fa-times"></i>
        </button>
    </div>
<?php if(isset($type) && $type == "orga") { ?>
    <?php if($orga == "" || $orga == null || $adres != $address) { ?>
        <div class="pull-right">
            <button class="btn btn-primary btn-add-orga btn-close-preview" id="btn-add-orga-orga" onclick="addOrgaforOrga(`<?= $name ?>`, `<?= $links ?>` , `<?= substr($description, 0, 120) ?>`, `<?= $urlImg ?>`)"> 
                Créer une Organisation
            </button>
        </div>
    <?php } else { ?> 
        <div class="pull-right">
            <a href="#@<?= $slug ?>" target="_blank">
                <button class="btn btn-primary btn-add-orga btn-close-preview"> 
                    Aller sur la Page
                </button>
            </a>
        </div>
    <?php } ?>
<?php } ?>
    <div id="prview-content">
        <h1 style="text-transform:none"><?= $name ?></h1>
        <div class="image-content">
            <img src="<?= $urlImg ?>" alt="" onerror="handleImageError(this)">
        </div>
        <a class="links-orga" href="<?= $links ?>" target="_blank"><?= $links ?></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="links-orga" href="<?= $linksdbpedia ?>" target="_blank"><?= $linksdbpedia ?></a>
        <?php if(isset($datedebut) && $datedebut != ""){ ?>
            <p class="description-orga" > <t style="font-weight: bold;">Né le : </t>  <?= $datedebut ?>  <?= isset($lieudebut) ? " à ".$lieudebut : "" ?></p>
        <?php } ?>
        <?php if(isset($datefin) && $datefin != ""){ ?>
            <p class="description-orga"> <t style="font-weight: bold;">Décédé le :</t> <?= $datefin ?> <?= isset($lieufin) ? " à ".$lieufin : "" ?> </p>
        <?php } ?>
        <?php if(isset($genre) && $genre != "") { ?>
            <p class="description-orga"> <t style="font-weight: bold;">Genre :</t> <?= $genre ?> </p>
        <?php } ?>
        <?php if(isset($compagnon) && $compagnon != "") { ?>
            <p class="description-orga"> <t style="font-weight: bold;"> Compagnon :</t> <?= $compagnon ?> </p>
        <?php } ?>
        <?php if(isset($occupation) && $occupation != "") { ?>
            <p class="description-orga"> <t style="font-weight: bold;"> Occupation :</t> <?= $occupation ?> </p>
        <?php } ?>
        <?php if($type == "personne" && $descriptionPersonne != "") { ?> 
            <p class="description-orga"> <t style="font-weight: bold;"> Description :</t>  <?= $descriptionPersonne ?></p>
        <?php } else if(isset($description) && $description != ""){ ?>
            <p class="description-orga"> <?= $description ?> </p>
        <?php } ?>
    </div>
</div>