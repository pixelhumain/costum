<style type="text/css">
    html{
        overflow: hidden !important;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <div style="display:flex; justify-content:center;">
            <a class="bg-dark text-white btn-open-form" href="javascript:;" data-form-type="badge" style="text-align:center; margin-left: 10px; margin-right: 10px; margin-top:10px;margin-bottom: 10px; border-radius: 5px; max-width:120px; text-overflow: wrap; display:block; padding: 20px;">
                <i class="fa fa-2x fa-bookmark"></i>  <br> 
                <?php echo Yii::t("badge", "Add and assign badge") ?>
            </a> 

            <?php if($email){ ?>
                 <a onclick="dyFObj.openForm('externalbadge');" class="bg-dark text-white" href="javascript:addAndAssignBadge();" style="text-align:center; margin-left: 10px; margin-right: 10px; margin-top:10px;margin-bottom: 10px; border-radius: 5px; max-width:120px; text-overflow: wrap; display:block; padding: 20px;">
                    <i class="fa fa-2x fa-external-link"></i>  <br> 
                    <?php echo Yii::t("badge", "Add badge from other platform") ?>
                 </a> 
             <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id='filterContainer' class='searchObjCSS'></div>
            <div class='headerSearchIncommunity no-padding col-xs-12'></div>
            <div class='bodySearchContainer margin-top-30'>
                <div class='no-padding col-xs-12' id='dropdown_search'></div>
            </div>
            <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
        </div>
    </div>
</div>

<script>
    var assign = false;
    function addAndAssignBadge() {
        dyFObj.openForm('badge',null,null, null, {
            afterSave: function(data) {
                dyFObj.commonAfterSave(data, () => {
                    bootbox.hideAll();
                    bootbox.alert({
                        message: "<center><b><?= Yii::t("badge", "Badge added, now assign it") ?></b></center>",
                        backdrop: true,
                        callback: function () {
                            dyFObj.openForm("assignbadge",null, {badgeId: data.id}, null, {
                                afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                                    openBadgeFinder();
                                })
                            });
                            if(filterGroupbadge){
                                filterGroupbadge.search.init(filterGroupbadge);
                            }
                        }
                    })
                });
            }
        });
    }
    directory.badgeElementPanelHtml = function(params){
		mylog.log("badgeElementPanelHtml","Params",params);		
		var dateStr="";
		if(typeof params.updated != "undefined" && notNull(params.updated))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.updated,30)/*+'</div>'*/;
		else if(typeof params.created != "undefined" && notNull(params.created))
      		dateStr += /*'<div class="dateUpdated dateUpdated-sm date-position">'+*/directory.showDatetimePost(params.collection, params.id, params.created,30)/*+'</div>'*/;
		var str=''; 
		str +='<div id="entity_'+params.collection+'_'+params.id+'" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 searchEntityContainer '+params.containerClass+'">'+
				'<div class="item-slide">'+
						'<div class="entityCenter" style="position: absolute;">'+
							'<span><i class="fa fa-'+params.icon+' bg-'+params.color+'"></i></span>'+
						'</div>'+
						'<div class="img-back-card">'+
							params.imageProfilHtml +
							'<div class="text-wrap searchEntity">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="uppercase '+params.hashClass+'">'+params.name+'</a>'+
								'</h4>'+
								'<div class="small-infos-under-title text-center">';
									if (typeof params.type != 'undefined') {
		str +=							'<div class="text-center entityType">'+	
											'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
										'</div>';
									}
									if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
									if(notEmpty(params.rolesHtml))
		str += 							'<div class=\'rolesContainer elipsis\'>'+params.rolesHtml+'</div>';
									if (notNull(params.localityHtml)) {
		str +=							'<div class="entityLocality no-padding">'+
											'<span>'+params.localityHtml+'</span>'+
										'</div>';
									}
		str +=					'</div>'+
								'<div class="entityDescription"></div>'+
							'</div>'+
						'</div>'+
						//hover
						'<div class="slide-hover co-scroll">'+
							'<div class="text-wrap">'+
								'<h4 class="entityName">'+
									'<a href="'+params.hash+'" class="'+params.hashClass+'">'+params.name+'</a>'+
								'</h4>';
                                if(typeof params.tags != 'undefined'){
                                    $.each(params.tags, function(i, v){
        str +=                          "<span class='badge margin-right-5 text-success margin-top-5 margin-bottom-5'>"+v+"</span>";
                                    })
                                }
        
								if (typeof params.type != 'undefined') {
		str +=						'<div class="entityType">'+	
										'<span class="text-white">'+((typeof tradCategory[params.type] != "undefined") ? tradCategory[params.type] : params.type)+'</span>'+
									'</div>';
								}
								if(notEmpty(params.statusLinkHtml))
		str+=							params.statusLinkHtml;	
								if(notEmpty(params.rolesHtml))
		str += 						'<div class=\'rolesContainer\'>'+params.rolesHtml+'</div>';
								if(typeof params.edit  != 'undefined' && notNull(params.edit))
		str += 						directory.getAdminToolBar(params);

								if (notNull(params.localityHtml)) {
		str +=						'<div class="entityLocality text-center">'+
										'<span> '+params.localityHtml+'</span>'+
									'</div>';
								}
//		str +=					'<hr>';
		str +=					'<p class="p-short">'+params.descriptionStr+'</p>';
        str +=                  `<p ${contextData && contextData.badges && contextData.badges[params.id] ? "" : 'style="display:none"' } class="p-short already-assigned-text"><?php echo Yii::t("badge","Badge already assigned") ?></p>`

        str +=                  `<div ${!contextData || !contextData.badges || !contextData.badges[params.id] || !(typeof params.id != 'undefined' && typeof params.collection != 'undefined') ? "" : 'style="display:none"' } class="btn-assign-container" style="text-align: center;"> <a class="btn btn-success" onclick="handleButtonAssign(event,'${params.id}');"><?php echo Yii::t("badge", "Assign") ?></a> </div>`
        str+=				'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
		return str;
	};
</script>

<script>
    var filterGroupbadge
    $(function() {
        var paramsFilterBadge = {
            urlData: baseUrl + "/co2/search/globalautocomplete",
            container: "#filterContainer",
            loadEvent:{
                default:"scroll"
            },
            header: {
                dom: ".headerSearchIncommunity",
                options: {
                    left: {
                        classes: 'col-xs-8 elipsis no-padding',
                        group: {
                            count: true,
                            types: true
                        }
                    }
                },
            },
            defaults: {
                indexStep: 10,
                <?= ( isset($sourceKey) && ($sourceKey=="true"))?"":"notSourceKey: true," ?>
                types: ["badges"],
                forced: {
                    filters: {
                        'preferences.private':  false
                    }
                },
            },
            results: {
                dom: ".bodySearchContainer",
                smartGrid: true,
                renderView: "directory.badgeElementPanelHtml",
                map: {
                    active: false
                }
            },
            filters: {
                text: true,
                isParcours : {
                    name : "Parcours ?",
                    view : "dropdownList",
                    type : "filters",
                    event : "filters",
                    action : "filters",
                    keyValue: false,
                    field: "isParcours",
                    list : {
                        "true" : trad.yes,
                        "false" : trad.no
                    }
	 		    },
            }
        };
        filterGroupbadge = searchObj.init(paramsFilterBadge);
        filterGroupbadge.search.init(filterGroupbadge);

        coInterface.bindLBHLinks();
    })
</script>