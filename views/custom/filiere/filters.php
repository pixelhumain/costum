<?php
	$elementsCostum = "";
	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
		$elementsCostum = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
	}

	$cities = "";
	if(isset($elementsCostum["costum"]["typeCocity"])) {
		if($elementsCostum != "" &&  $elementsCostum["costum"]["typeCocity"] == "epci") {
			$cities = PHDB::findOne(Zone::COLLECTION,array(
				"name" => $elementsCostum["address"]["level5Name"]
			));
		}
	}
?>
<style>
	.p-short {
		color: white;
		text-align: justify;
		margin-left: 5%;
		margin-right: 5%;
	}
	.tag-list a span {
		color: red !important;
	}
</style>

<script type="text/javascript">
	var dataCostum = <?php echo json_encode( $elementsCostum ); ?> ;
	var citiesArray = <?php echo json_encode($cities); ?>;
    var pageApp = window.location.href.split("#")[1];
		
	var paramsFilter= {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
		defaults : {
			notSourceKey: true,
			indexStep: 0,
			filters : {
				$or : {
					"source.keys" : dataCostum.slug,
					["links.memberOf."+dataCostum._id["$id"]] : {$exists:true},
				}
			}
		},
		filters : {
			text : true,
			scope : true
		},
		loadEvent:{
			default:"scroll"
		},
		results : {
			renderView: "directory.elementPanelHtmlSmallCard",
			smartGrid : true
		},
	};

	if(pageApp=="search"){
		paramsFilter.defaults.types =["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"];
		paramsFilter.filters.types ={
				lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources"]
		};
		paramsFilter.filters.tags = {
			list : {},
			view:"dropdownList",
			event:'tags',
			type:'tags',
			name:'mot clé'
		}	
	} else {
		var typeSearch = pageApp.split("=")[0];
		typeSearch = typeSearch.split('?')[1];
		var paramsSearch = pageApp.split("=")[1];

		if(typeSearch == "searchType") {
			paramsFilter.defaults.types =[paramsSearch];
		} else if(typeSearch == "tags"){
			paramsFilter.defaults.types =["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"];
		}
		paramsFilter.filters.types ={
			lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources"]
		};
		paramsFilter.filters.tags = {
			view:"dropdownList",
			event:'tags',
			type:'tags',
			list : {},
			name:'mot clé'
		}		
	}

	if(typeof dataCostum.thematic != "undefined" && dataCostum.thematic != "") {
		if(dataCostum.thematic == "Associations") {
			paramsFilter.defaults.types = ["NGO"];
		}
		paramsFilter.defaults.filters["$or"].$and = setFiltersdataAndThematic(dataCostum.thematic);
		paramsFilter.defaults.tags = [dataCostum.thematic];
		getTagsFilters(dataCostum.thematic);
	}

	function setFiltersdataAndThematic(thematic) {
		var dataAnd = [];
		var idAddress = {};
		if(typeof costum.typeCocity != 'undefined') {
			if(costum.typeCocity == "region"){
				idAddress = {"address.level3" : dataCostum.address["level3"]};
			} else if(costum.typeCocity == "ville") {
				idAddress = {"address.localityId": dataCostum.address['localityId']};
			} else if(costum.typeCocity == "departement" || costum.typeCocity == "district") {
				idAddress = {"address.level4": dataCostum.address['level4']};
			} else if(costum.typeCocity == "epci") {
				costum.citiesArray = citiesArray.cities;
				idAddress = {"address.localityId": {$in : costum.citiesArray}};
			}
		}
		if(thematic == "Tiers lieux" || thematic == "Pacte") {
			var source = {};
			var sourcekey =  thematic == "Tiers lieux" ?  "franceTierslieux" : "siteDuPactePourLaTransition";
			var tags = thematic == "Tiers lieux" ? "TiersLieux" : thematic.toLowerCase();
			source["$or"] = [
				{"source.keys" : sourcekey},
				{"reference.costum" : sourcekey},
				// {"tags" : tags}
			];
			var addressParams = [
				idAddress,
				source
			];
			dataAnd = addressParams;

		} else {
			dataAnd = [
				idAddress,
				// {"tags" : thematic.toLowerCase()}
			];
			// dataCostum.address["postalCode"] != "" ? dataAnd.push({"address.postalCode" : dataCostum.address["postalCode"]}) : "";
		} ;
		return dataAnd;
	}

	function getTagsFilters(thematic) {
		var params = {
			searchType: paramsFilter.defaults.types,
			notSourceKey: true,
			filters: paramsFilter.defaults.filters,
			fields : ["tags","name"],
			indexStep: 0,
			searchTags : [thematic]
		};

		ajaxPost(
			null,
			baseUrl + "/" + moduleId + "/search/globalautocomplete",
			params,
			function(data) {
				let datas = data.results
				let tags = {};
				for (var key in datas) {
					let value = datas[key];
					if (value.tags && Array.isArray(value.tags)) {
						value.tags.forEach(function(tag) {
							tags[tag] = tag;
						});
					}
				};

				let sortedTags = Object.keys(tags).sort();

				let sortedTagsObj = {};
				sortedTags.forEach(function(tag) {
					sortedTagsObj[tag] = tag;
				});
				
				paramsFilter.filters.tags.list = sortedTagsObj;

			},
			function(error){},
          	"json", 
          	{
				async : false
			}
		);
	}

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
	});
</script>