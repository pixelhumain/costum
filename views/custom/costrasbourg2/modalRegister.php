<?php $logo = (@$this->costum["logo"]) ? $this->costum["logo"] : $this->module->getParentAssetsUrl()."/images/logoLTxt.jpg"; ?>
<style type="text/css">
	.modal-content{
		position: unset;
	}
</style>
<div class="col-xs-12 no-padding">
	<div class="portfolio-modal modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="col-lg-offset-3 col-sm-offset-2 col-lg-6 col-sm-8 col-xs-12">
	        <div class="close-modal" data-dismiss="modal">
	            <div class="lr">
	                <div class="rl">
	                </div>
	            </div>
	        </div>
	        <div style="display: initial;" class="contentcharte col-xs-12">
				<iframe width="100%" height="750px" src="https://docs.google.com/document/d/e/2PACX-1vR10n9JZqlgcVA7omYg93i2-HLTD7V3AGq38x1f1w3lzDag7LxAdQnmTsrHDE-07GQyZWpD0BbF1kti/pub?embedded=true"></iframe>

				<div class="check">
					<div>
						<input class="response" type="checkbox" id="accept" name="accept">
						<label for="accept">J'accepte</label>
					</div>

					<div>
					  <input class="response" type="checkbox" id="refuse" name="refuse">
					  <label for="refuse">Je n'accepte pas</label>
					</div>

					<button class="continu" type="button">Continuer</button>
				</div>
			</div>
	        <form style="visibility: hidden;" class="modal-content form-register box-register padding-top-15"  >
	            <div class="row">
	                <div class="col-lg-12">
	                    <span class="name" >
	                        
	                            <div class="col-md-12 col-sm-12 col-xs-12">
	                                <div class="loginLogo">
	                                
	                                    <img style="width:150px; border: 10px solid white; border-bottom-width:0px;max-height: inherit;margin:auto" class="logoLoginRegister" src="<?php echo $logo;?>"/>
	                                
	                                </div>
	                            </div>
	                    </span>
	                </div>
	            </div>
	            <div class="text-left form-register-inputs col-xs-12 margin-bottom-50">
	                <div class="pull-left form-actions no-margin" style="width:100%; padding:10px;">
	                    <div class="errorHandler alert alert-danger no-display registerResult pull-left " style="width:100%;">
	                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","Please verify your entries.") ?>
	                    </div>
	                    <div class="alert alert-success no-display pendingProcess" style="width:100%;">
	                        <i class="fa fa-check"></i> <?php echo Yii::t("login","You've been invited : please resume the registration process in order to log in.") ?>
	                    </div>
	                </div>
	                <div class="nameRegister">
	                    <label class="letter-black"><i class="fa fa-address-book-o"></i> <?php echo Yii::t("login","Name and surname") ?> *</label>
	                    <input class="form-control" id="registerName" name="name" type="text" placeholder="<?php echo Yii::t("login","name and surname") ?>"><br/>
	                </div>
	                <div class="usernameRegister">
	                <label class="letter-black"><i class="fa fa-user-circle-o"></i> <?php echo Yii::t("login","User name") ?> *</label><br>
	                <input class="form-control" id="username" name="username" type="text" placeholder="<?php echo Yii::t("login","user name") ?>"><br/>
	                </div>
	                <div class="emailRegister">
	                <label class="letter-black"><i class="fa fa-envelope"></i> <?php echo Yii::t("login","Email") ?> *</label><br>
	                <input class="form-control" id="email3" name="email3" type="text" placeholder="<?php echo Yii::t("login","email") ?>"><br/>
	                </div>
	                <div class="passwordRegister">
	                <label class="letter-black"><i class="fa fa-key"></i> <?php echo Yii::t("login","Password") ?> *</label><br/>
	                <input class="form-control" id="password3" name="password3" type="password" placeholder="<?php echo Yii::t("login","password") ?>"><br/>
	                </div>
	                <div class="passwordAgainRegister">
	                <label class="letter-black"><i class="fa fa-key"></i> <?php echo Yii::t("login","Password again") ?> *</label><br/>
	                <input class="form-control" id="passwordAgain" name="passwordAgain" type="password" placeholder="<?php echo Yii::t("login","password (confirmation)") ?>">
	                </div>
	                <input class="form-control" id="isInvitation" name="isInvitation" type="hidden" value="false">
	                <hr>
	                <div class="form-group pull-left no-margin agreeContent" style="width:100%;">
	                    <div class="checkbox-content pull-left no-padding">
	                        <label for="agree" class="">
	                            <input type="checkbox" class="agree" id="agree" name="agree">
	                            <span class="cr pull-left"><i class="cr-icon fa fa-check"></i></span>
	                         <span class="agreeMsg checkbox-msg letter-red pull-left">
	                            <?php echo Yii::t("login","I agree to the Terms of") ?> 
	                            <a href="https://www.communecter.org/doc/Conditions Générales d'Utilisation.pdf" target="_blank" class="bootbox-spp text-dark">
	                                <?php echo Yii::t("login","Service and Privacy Policy") ?>
	                            </a>
	                        </span>
	                        </label>

	                    </div>
	                   

	                    <!--<div>
	                        <label for="agree" class="checkbox-inline letter-red">
	                            <input type="checkbox" class="grey agree" id="agree" name="agree">
	                            <?php echo Yii::t("login","I agree to the Terms of") ?> 
	                            <a href="https://www.communecter.org/doc/Conditions Générales d'Utilisation.pdf" target="_blank" class="bootbox-spp text-dark">
	                                <?php echo Yii::t("login","Service and Privacy Policy") ?>
	                            </a>
	                        </label>
	                    </div>-->
	                </div>

	                <br><hr>

	                <div class="pull-left form-actions no-margin" style="width:100%; padding:10px;">
	                    <div class="errorHandler alert alert-danger no-display registerResult pull-left " style="width:100%;">
	                        <i class="fa fa-remove-sign"></i> <?php echo Yii::t("login","Please verify your entries.") ?>
	                    </div>
	                </div>

	                <a href="javascript:" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Yii::t("common","Back") ?></a>
	                <button class="btn btn-success text-white pull-right createBtn"><i class="fa fa-sign-in"></i> <?php echo Yii::t("login","Create account") ?></button>
	                
	                
	                <!--<div class="col-md-12 margin-top-50 margin-bottom-50"></div>-->
	            </div>      
	        </div>
	    </form>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		var response = "";
	});

	$(".response").click(function(){
			if ($(this).attr('id') == "accept") {
				document.getElementById("accept").checked = true;
				document.getElementById("refuse").checked = false;
				response = $(this).attr('id');
			}
			else{
				document.getElementById("accept").checked = false;
				document.getElementById("refuse").checked = true;
				response = "refuse";
			}
		});


		$(".continu").click(function(){
			if (response == "accept") {
				$(".contentcharte").css("display","none");
				$(".form-register").css("visibility","visible");
			}
			else{
				alert("Afin de continuer, il vous faut accepter les conditions");
			}
		});

		$(".close-modal").click(function(){
			response = "";
			$(".contentcharte").css("display","initial");
			$(".form-register").css("visibility","hidden");
			document.getElementById("accept").checked = false;
			document.getElementById("refuse").checked = false;
		});

		$(".btn-danger").click(function(){
			response = "";
			$(".contentcharte").css("display","initial");
			$(".form-register").css("visibility","hidden");
			document.getElementById("accept").checked = false;
			document.getElementById("refuse").checked = false;
		});
</script>