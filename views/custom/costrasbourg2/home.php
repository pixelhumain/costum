<?php
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

    $blockCmss = PHDB::find(Cms::COLLECTION, 
              array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                     "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                     "page" => "welcome") );
    // var_dump($blockCmss);exit;
    $blockCmsById = [];

    foreach ($blockCmss as $key => $value) {
        if (isset( $value["id"] )) {
            $blockCmsById[$value["id"]] = $value;
        }
    }
}
$page = "welcome";

$params = [  "tpl" => "costrasbourg2","slug"=>$this->costum["slug"],"canEdit"=>false,"el"=>$el ];
echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );  
?>

<style>
section{
    background-image : url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/templateCostumDesTiersLieux/background.jpg);
}

.header{
        background-image : url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/costrasbourg/header.jpg);
        background-size: cover;
        padding: 25%;
        margin-top : -3%;
    }
</style>

<!-- HEADER -->
<div class="header text-center">
</div>
<div class="logo">
<img style="position: relative;" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/costrasbourg/logo.png" class="img-responsive">
</div>

<div class="bloc txt-header">
    <div class="container">
        <h2 style="margin-top: 5%;margin-bottom: 5%;" class="text-center">
            Un site pour les gens qui se bougent à Strasbourg <br> <a style="font-weight: 900;color:white;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" href="javascript:;" data-hash="#dda" class="lbh-menu-app "> Proposer </a>
            des solutions /
            <a style="color:white;font-weight: 900;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" title="Découvrir l'espace d'entraide" href="javascript:;" data-hash="#live" class="lbh-menu-app "> Discuter </a> / <a style="color: white;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" title="Découvrir l'espace d'entraide" href="javascript:;" data-hash="#annonce" class="lbh-menu-app "> Se prêter </a>
            du matériel /
            <a style="color:white;font-weight: 900;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" title="S'informer des activités sur le site" href="javascript:;" data-hash="#search" class="lbh-menu-app">
            S'informer </a> / <a style="color:white;font-weight: 900;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" title="Découvrir l'agenda" href="javascript:;" data-hash="#agenda" class="lbh-menu-app "> S'organiser </a> / <a  style="color:white;font-weight: 900;font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;" title="Découvrir l'espace d'entraide" href="javascript:;" data-hash="#annonces" class="lbh-menu-app "> S'entraider </a> 
        </h2>
    </div>
</div>

<!-- Cartographie --> 
<div class="bloc">
    <h1 class="text-center h1color bloc-h1"><i class="fa fa-map-marker" aria-hidden="true"></i>
    ASSOS ET COLLECTIFS LOCAUX</h1>
    <div class="container bloc-container"> 
        <!--Ajout de la map -->
        <div style="margin-top: -1vw;" class="col-xs-12 mapBackground no-padding" id="mapPossession">
        </div>
        <div class="text-center" style="color : #8ec641; font-size: 5vw; padding: 1%;">
            <a title="Agrandir la carte" href="javascript:;" data-hash="#map" class="lbh-menu-app " style="text-decoration : none; color: #8ec641;margin-top: 1%;"> 
                <i style="font-size: 50px" class="fa fa-plus-circle" aria-hidden="true">
                   <br><p style="font-size: 30px;margin-top: 5%">Agrandir la carte</p> 
                </i> 
            </a>
        </div>
    </div>
</div>


<!-- Démocratie -->
<div class="bloc" style="margin-bottom : 4%">

    <!-- <h1 class="text-center bloc-h1 h1nocolor">
    <span style="text-decoration: underline; text-underline-position: under;"><i class="fa fa-gavel" aria-hidden="true"></i>
    DONNER</span> VOTRE AVIS</h1> -->

    <div class="bloc-container col-xs-12">

    <?php 
    $paramsDda = [
        "canEdit"=>true, 
        "blockCms" => @$blockCmsById["costrasbourg2.welcome.appDda.0"],
        "idToPath" => "costrasbourg2.welcome.appDda.0",
        "page" => $page, 
        "limit" => 4,
        "el"=>$el
    ];

    echo $this->renderPartial( "costum.views.tpls.app.appDda",$paramsDda ); 
    ?>
    </div>

    <div class="text-center " style="color : #8ec641; font-size: 5vw; padding: 1%;"> 
        <a href="javascript:;" data-hash="#dda" class="lbh-menu-app " style="text-decoration : none; color: #8ec641;"> 
            <i style="font-size: 50px;" class="fa fa-plus-circle" aria-hidden="true">
                <br><p style="font-size: 30px;margin-top: 5%;">Accéder aux votes</p>
            </i> 
        </a>
    </div>

</div>

<!-- Actualité --> 

<div class="bloc">
    <!-- <h1 class="text-center h1color bloc-h1"><i class="fa fa-map-marker" aria-hidden="true"></i>
    MUR D'EXPRESSION</h1> -->
    <div title="Voir toute les publications" class="container bloc-container">
    <center>
        <?php
        $paramsNews = [
            "canEdit"=>true, 
            "blockCms" => @$blockCmsById["costrasbourg2.welcome.basicnews.0"],
            "idToPath" => "costrasbourg2.welcome.basicnews.0",
            "page" => $page, 
            "limit" => 4,
            "el"=>$el
        ];

        echo $this->renderPartial("costum.views.tpls.blockCms.news.basicnews", $paramsNews); 
        ?>
    </center>
    </div>

    <div class="text-center" style="color : #8ec641; font-size: 5vw; padding: 1%;">
        <a title="Découvrir l'espace d'entraide" href="javascript:;" data-hash="#live" class="lbh-menu-app " style="text-decoration : none; color: #8ec641;"> 
            <i style="font-size: 50px" class="fa fa-plus-circle" aria-hidden="true">
                <br><p style="font-size: 30px;margin-top: 5%;">Plus d'actualités</p>
            </i> 
        </a>
    </div>
</div>

<!-- Espace entraide --> 
<div class="bloc">

    <!-- <h1 class="text-center bloc-h1 h1nocolor">
    <span style="text-decoration: underline; text-underline-position: under;"><i class="fa fa-cubes" aria-hidden="true"></i>
    PETITES</span> ANNONCES</h1> -->

    <div class="container bloc-container">
    <?php 
    
    $paramsRessources = [
        "canEdit"=>true, 
        "blockCms" => @$blockCmsById["costrasbourg2.welcome.appRessource.0"],
        "idToPath" => "costrasbourg2.welcome.appRessource.0",
        "page" => $page, 
        "limit" => 4,
        "el"=>$el
    ];

    echo $this->renderPartial("costum.views.tpls.app.appRessource",$paramsRessources); 
    ?>
    </div>

    <div class="text-center" style="color : #8ec641; font-size: 5vw; padding: 1%;">
        <a title="Découvrir l'espace d'entraide" href="javascript:;" data-hash="#annonces" class="lbh-menu-app " style="text-decoration : none; color: #8ec641;"> 
            <i style="font-size: 50px" class="fa fa-plus-circle" aria-hidden="true">
                <br><p style="font-size: 30px;margin-top: 5%;">Parcourir l'espace d'entraide</p>
            </i> 
        </a>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function(){
    
    setTimeout(function(){
    	$(".coop-wraper").removeClass("col-xs-12");
        $(".coop-wraper").addClass("col-xs-4");
    },2000);

	var mapPossessionHome = {};
	var paramsMapPossession  = {};

    initPossessionMapView();
    setTitle("COStrasbourg");
    // initCoStrabourgMapView();

    contextData = <?php echo json_encode($el); ?>;
    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName=<?php echo json_encode($el["name"]); ?>;
    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
});

var possession={
	initScopeObj : function(){
		$(".content-input-scope-possession").html(scopeObj);
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["FR"]
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='#page.type."+data.type+".id."+id+"' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup +=  "Accéder à la page";
					popup += '</div></a>';
				popup += '</div>';
            popup += '</div>';
			return popup;
		};
	}
}


function initPossessionMapView(){
	possession.initScopeObj();
	paramsMapPossession = {
		container : "mapPossession",
		activePopUp : true,
		tile : "maptiler",
		menuRight : true,
	};

	mapPossessionHome = mapObj.init(paramsMapPossession);
	dataSearchPossession=searchInterface.constructObjectAndUrl();
	dataSearchPossession.searchType = ["projects","organizations","events","classifieds"];
	dataSearchPossession.indexStep=0;
	possession.mapDefault();
	dataSearchPossession.private = true;
	dataSearchPossession.sourceKey = costum.contextSlug;
	var donnee = mapPossessionHome.data;
	ajaxPost(
		null,
		baseUrl+'/'+moduleId+'/search/globalautocomplete',
		dataSearchPossession,
      	function(data){ 
	       mylog.log(">>> success autocomplete search !!!! ", data); //mylog.dir(data);
			if(!data){ 
				toastr.error(data.content); 
			} 
			else{ 
				// $('#mapContent').html('');
				mapPossessionHome.addElts(data.results, true);
				setTimeout(function(){
					mapPossessionHome.map.panTo([ 48.60198580 , 7.78352170]);
					mapPossessionHome.map.setZoom(3);
				},2000);
			}
      	},
      	function(data){
            mylog.log(">>> error autocomplete search"); 
			mylog.dir(data);   
			$("#dropdown_search").html(data.responseText);  
			//signal que le chargement est terminé
			loadingData = false; 
        }
    );
	
}

</script>