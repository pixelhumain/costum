<?php 
	$emailList = array();
	$members = array();

	if(!isset($id)){
		$id = $this->costum["contextId"];
	}

	if(!isset($type)){
		$type = $this->costum["contextType"];
	}

    $reseau = PHDB::findOneById(Organization::COLLECTION, $id, ["address"]);

	//if($this->costum["contextType"]==$type){
	$linkPath = "links.memberOf.".$id;
	
	if($type!="organizations"){
		$linkPath = "links.$type.".$id;
	}

    $queryTL = array(   
        "address.level3" => $reseau["address"]["level3"],
        '$or' => array(
            array("source.keys" => array('$in' => ["franceTierslieux"])),
            array("reference.costum"  => array('$in' => ["franceTierslieux"]))
        )
    );

    $contactsTl = PHDB::distinct( Organization::COLLECTION, "email", $queryTL);

?>

<style>
	#mailing-mobilisation{
		margin-top: 55px;
	}
	#mailing-mobilisation .form-control{
		box-shadow: none !important;
		outline: none !important;
		background: #fefefe;
	}

	#mailing-mobilisation h3{
		margin-bottom: 2%;
		text-align: center;
		color: #ACCD5C;
	}
	#mailing-mobilisation .icon-title{
		font-size: 48pt;
		color: <?= isset($this->costum["colors"]) && isset($this->costum["colors"]["main1"])?$this->costum["colors"]["main1"]:"inherit" ?>;
	}
	#mailing-mobilisation option{
		padding: 0.4em 0.2em;
	}
	#mailing-mobilisation select{
		padding: 0.2em 0em !important;
	}
	#mailing-mobilisation .contact-image{
		text-align: center;
	}
	#mailing-mobilisation .btnSend{
       	-webkit-transition: 0.5s ease;
       	-moz-transition: 0.5s;
       	-ms-transition: 0.5s;
       	-o-transition: 0.5s;
       	transition: 0.5s;
       	width: 40%;
       	cursor: pointer;
       	text-transform: uppercase;
		border-radius: 5rem;
		padding: 0.6em 0.6em 0.5em 0.6em;
		background: #FFF;
		border: 3px solid <?= isset($this->costum["colors"]) && isset($this->costum["colors"]["main1"])?$this->costum["colors"]["main1"]:"inherit" ?>;
		font-weight: 800;
		color: <?= isset($this->costum["colors"]) && isset($this->costum["colors"]["main1"])?$this->costum["colors"]["main1"]:"inherit" ?>;
	}
    #mailing-mobilisation #btnClose{
        -webkit-transition: 0.5s ease;
       	-moz-transition: 0.5s;
       	-ms-transition: 0.5s;
       	-o-transition: 0.5s;
       	transition: 0.5s;
       	width: 40%;
       	cursor: pointer;
       	text-transform: uppercase;
		border-radius: 5rem;
		padding: 0.6em 0.6em 0.5em 0.6em;
		background: #FFF;
		border: 3px solid #ddd;
		font-weight: 800;
		color: #444;
    }
	#mailing-mobilisation .btnSend:hover {
        background:<?= isset($this->costum["colors"]) && isset($this->costum["colors"]["main1"])?$this->costum["colors"]["main1"]:"inherit" ?>;
        color:white;
    }
	#mailing-mobilisation .btnSend:focus{
		outline: none;
	}

    .btn-colored{
        background-color : <?= isset($this->costum["colors"]) && isset($this->costum["colors"]["main1"])?$this->costum["colors"]["main1"]:"inherit" ?>;
    }
</style>

<div class="container">
    <h2>Mobilisation du réseau</h2>
    <p>
        Mobilisez les tiers-lieux de votre région en leur envoyant un e-mail pré-rempli afin de donner à voir ceux qui se bougent en cette période cruciale. <br/>
        Personnalisez l'email en envoyez-le. <br/>
        Les tiers-lieux seront alors invité à se référencer. 
    </p>

    <a data-toggle="modal" data-target="#mailing-mobilisation" class="btn btn-primary  padding-20 btn-colored"><i class="fa fa-envelope"></i>
        Je mobilise mon réseau
    </a>
</div>

<!-- Modal Mailing-mobilisation-->
<div id="mailing-mobilisation" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="contact-image">
				<br/>
				<span class="fa fa-envelope icon-title"></span>
			</div>
			<div>
				<h3>MAIL  DE MOBILISATION</h3>
				<!-- p class="text-center">Séléctionner le destinataire(s) de votre email.</p -->
			</div>
	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					
					<div class="form-group">
						<textarea id="txtMsg" name="txtMsg" class="form-control markdown" placeholder="Votre Message *" cols="10" rows="10">Tiers-lieux de France, c’est le moment de montrer notre rôle politique dans le quotidien de nos territoires. C’est le moment de se mobiliser pour accueillir les débats et les revendications politiques et sociales. Nous avons plus que jamais besoin de nos tiers-lieux !</textarea>
					</div>
					<div class="form-group text-center">
						<button id="btnSend" class="btnSend" data-dismiss="modal">Envoyer</button>
						<button id="btnClose" type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
	$(".headerSearchright").prepend('<a data-toggle="modal" data-target="#mailing-mobilisation" class="btn btn-primary btn-xs"><i class="fa fa-envelope"></i> Envoyer un mail</a>&nbsp; &nbsp;');

	// Mailing-mobilisation
	var emailOfTL = <?php echo (isset($contactsTl) ? json_encode($contactsTl) : "[]" ) ?>;

	// Send email
	$("#btnSend").on("click", function(){
			var isValid = false;

			if ($("#txtMsg").val() != "" && emailOfTL.length!=0){
				isValid = true;
			}

			if(isValid){
				var paramsmail = {
					tpl : "mobilise",
					tplObject : "Tiers Lieux de France mobilisons-nous !",//$("#txtMsg").val(),
					tplMail : emailOfTL,
					html:"<p style='white-space:pre-line'>"+$("#txtMsg").val()+"</p><br>",
					btnRedirect : {
						hash : "#answer.index.id.new.form.666c245dd4968131956aa91e.mode.w.standalone.true",
						label : "Je me mobilise"
					},
                    costumTpl : "costum.views.custom.reseauTierslieux.emails.mobilisation"
				};

				ajaxPost(
					null,
					baseUrl+"/co2/mailmanagement/createandsend",
					paramsmail,
					function(data){ 
						//$("#txtMsg").val("");
						toastr.success("Votre mail a été bien envoyé");
					},
					function(data){
						toastr.error("Une problème s'est produite, envoie du mail est intérrompu");
					}
				);
			}else{
				toastr.warning("Votre email n'est pas envoyé. aucun déstinataire séléctionné ou vous n'avez pas écrit de message.");
			}
	});

	$("#txtMsg").on("change", function(){
		if($(this).val()!=""){
			$("#btnSend").attr("disabled", false);
		}else{
			$("#btnSend").attr("disabled", true);
		}
	});
});
</script>
