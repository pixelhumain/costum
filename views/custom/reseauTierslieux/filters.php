<style type="text/css">
	.leaflet-bar a:hover {
		border-color: #FF286B;
		background-color: #0044cc;
		color: #fff;
	}

	.leaflet-bar a:hover {
		background-color: #fff;
		color: #0044cc;
	}
</style>

<div class="modal fase" id="downloading-processing" style="display:none">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body">
				Téléchargement du recensement en cours, cela peut prendre quelques instants. Merci de patienter.
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	var pageApp = <?php echo json_encode(@$page); ?>;
	var mapShown = (pageApp == "search") ? true : false;
	var typePlaceNoOther = costum.lists.typePlace;
	typePlaceNoOther.pop();
	var manageModelNoOther = costum.lists.manageModel;
	manageModelNoOther.pop();
	var paramsFilter = {
		container: "#filters-nav",
		options: {
			tags: {
				verb: '$all'
			}
		},
		loadEvent: {
			default: "scroll"
		},
		results: {
			renderView: "directory.elementPanelHtml",
			smartGrid: true,
			map: {
				active: mapShown
			}
		},
		header: {
			options: {
				left: {
					classes: "col-xs-6 no-padding",
					group: {
						count: false
					}
				},
				right: {
					classes: "col-xs-5 no-padding",
					group: {
						map: true
					}
				}
			}
		},
		// 		views : {
		// 			map : function(fObj,v){
		// 			return  '<button class="btn-show-map-search pull-right bg-main1" style="" title="yoyo" alt="yoyoyo">'+
		// 						'<i class="fa fa-map-marker"></i> heyyou</button>';
		// 		}
		// 	}
		// },	
		defaults: {
			indexStep: 0,
			types: ["organizations"]
		},
		filters: {
			text: {
				placeholder: "Nom du tiers-lieu recherché"
			},
			typePlace: {
				view: "dropdownList",
				type: "tags",
				name: "Famille de tiers-lieux",
				event: "tags",
				list: typePlaceNoOther
			},
			manageModel: {
				view: "dropdownList",
				type: "tags",
				name: "Mode de gestion",
				event: "tags",
				list: manageModelNoOther
			},

			role: {
				view: "dropdownList",
				type: "tags",
				name: "Rôle",
				event: "tags",
				keyValue : false,
				list: {
					"TiersLieux" : "Tiers Lieux",
					"Bureau" : "Bureau",
					"Partenaire" : "Partenaire",
					"Accompagnateur" : "Accompagnateur"
				}
			},
			// greeting : {
			// 	view : "selectList",
			// 	type : "tags",
			// 	name : "Accueil",
			// 	event : "tags",
			// 	list : costum.lists.greeting
			// },
			// manageModel : {
			// 	view : "dropdownList",
			// 	type : "tags",
			// 	name : "Portage",
			// 	event : "tags",
			// 	list : costum.lists.manageModel
			// },
			// state : {
			// 	view : "selectList",
			// 	type : "tags",
			// 	name : "Etat",
			// 	event : "tags",
			// 	list : costum.lists.state
			// },
			// spaceSize : {
			// 	view : "dropdownList",
			// 	type : "tags",
			// 	name : "Taille",
			// 	event : "tags",
			// 	list : costum.lists.spaceSize
			// },
			// certification : {
			// 	view : "dropdownList",
			// 	type : "tags",
			// 	name : "Lauréats Fabriques",
			// 	event : "tags",
			// 	list : costum.lists.certification
			// },
			// network : {
			// 	view : "tags",
			// 	type : "category",
			// 	name : "Réseaux",
			// 	event : "selectList",
			// 	field : "category"
			// }
		}
	};

	if (typeof costum.address != "undefined" && costum.address == "FR") {
		paramsFilter.filters.scope = true;
		paramsFilter.scopeList = {
			params: {
				countryCode: scopeCountryParam,
				level: scopeLevelParam,
			}
		};
		if (typeof upperLevelId != "undefined") {
			paramsFilter.filters.scopeList.params.upperLevelId = upperLevelId;
		}
	}

	if (mapShown) {
		delete paramsFilter.loadEvent;
	}


	if (typeof costum.settings != "undefined" && typeof costum.settings.compagnonFtl != "undefined" && costum.settings.compagnonFtl == true) {
		paramsFilter.filters.compagnon = {
			view: "tags",
			type: "tags",
			name: "Compagnons",
			event: "tags",
			list: costum.lists.compagnon
		};
	}



	//function lazyFilters(time){
	//if(typeof searchObj != "undefined" )
	//filterGroup = searchObj.init(paramsFilter);
	//else
	//setTimeout(function(){
	//lazyFilters(time+200)
	//}, time);
	//}


	var filterSearch = {};

	function is_empty(__input) {
		return typeof __input === 'undefined' || typeof __input === 'object' && (__input === null || __input instanceof Array && __input.length === 0) || typeof __input === 'string' && __input === '';
	}

	function html_special_chars(text) {
		return text
			.replace(/&/g, "&amp;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#039;");
	}

	$(".download-orga-profile").off().on("click", function() {
		var settings = {
			// excluded_inputs : [
			// 	"tpls.forms.titleSeparator",
			// 	"tpls.formssectionTitle",
			// 	"tpls.forms.sectionDescription",
			// 	"tpls.forms.cplx.list",
			// 	"tpls.forms.costum.cressReunion.element" ,
			// 	"tpls.forms.cplx.validateStep",
			// 	"tpls.forms.costum.franceTierslieux.validateStepFtl",
			// // 	"tpls.forms.costum.franceTierslieux.validateStep",
			// // 	"tpls.forms.sectionTitle"
			// // ], 
			// is_exporting_following_answer: false,
			// following_answer_id: "followingAnswerId",
			is_exporting_dynform: true,
			// export_dynform_after: "franceTierslieux1522023_1352_1le5ofi0x733rc1gmbk4",
			is_exporting_answer_id: false,
			completed_answers_only: true,
			is_exporting_user_email: false,
			is_custom_checkbox_aside: true,
			split_steps: true,
			settings: {
				address: "multicolumn",
				checkbox: "multicolumn",
				finder: "complete",
				finder_column: "multicolumn"
			},
			query: {
				form: "63e0a8abeac0741b506fb4f7",
				answers: {
					'$exists': true
				}
			},
			steps: {},
			champs: [],
			// region_filters:[costum.address.level3Name],
		};
		if ($(this).data("level3") != "undefined") {
			settings.region_filters = [$(this).data("level3")];
		}
		exportCsvRecensement(settings);

	});


	function exportCsvRecensement(post) {
		// mylog.log("parsing post",JSON.parse(post));
		// return;
		const url = `${baseUrl}/survey/exportation/${post.split_steps ? 'multiple-file-csv-coform' : 'csv-coform'}`;
		ajaxPost(null, url, post, function(__response) {
				$('#csv-export span').removeClass();
				if (post.split_steps) {
					let time_manager = 0;
					$.each(__response, (__, __csv) => {
						const {
							heads,
							bodies,
							name
						} = __csv;
						if (!is_empty(heads) && !is_empty(bodies)) {
							setTimeout(() => {
								export_file(heads, bodies, name);
							}, time_manager * 500);
							time_manager += 1;
						}
					});
				} else {
					const {
						heads,
						bodies
					} = __response;
					export_file(heads, bodies);
				}
				$("#downloading-processing").modal("hide");
			},
			function(error) {
				toastr.warning("Un problème a été rencontré. Veuillez vous rapprocher de l'administrateur");
				$("#downloading-processing").modal("hide");
			}
		);
	}

	function export_file(__headers, __body, __specification = '') {
		if (!is_empty(json2csv) && !is_empty(json2csv.Parser)) {
			const parser = json2csv.Parser;
			const fields = __headers;
			const exportation_preparation = new parser({
				fields,
				delimiter: (notNull(document.getElementById('csv_delimiter'))) ? document.getElementById('csv_delimiter').value : ';'
			});
			const csv = exportation_preparation.parse(__body);
			let filename = (notNull(document.getElementById('csv_exportation_name'))) ? document.getElementById('csv_exportation_name').value : "BDFTL_2023";
			if (!is_empty(filename)) {
				if (!is_empty(__specification)) filename += '_' + __specification;
				const splited_filename = filename.split('.');
				// const file_signature = moment().format('YYYYMMDD-HHmm');
				const file_signature = '';
				filename = filename.length > 200 ? filename.substring(0, 200) + '...' : filename;
				filename = splited_filename.length > 1 && splited_filename[splited_filename.length - 1].toLowerCase() === 'csv' ? `${splited_filename.slice(-1).join('')}_${file_signature}.csv` : `${filename} ${file_signature}.csv`;

				//var universalBOM = "\uFEFF";
				const blob = new Blob([new Uint8Array([0xEF, 0xBB, 0xBF]), csv], {
					type: 'text/csv;charset=utf-8;'
				});
				if (!is_empty(navigator) && !is_empty(navigator.msSaveBlob)) { // IE 10+
					navigator.msSaveBlob(blob, filename);
				} else {
					const link = document.createElement('a');
					if (link.download !== undefined) { // feature detection
						// Browsers that support HTML5 download attribute
						const url = URL.createObjectURL(blob);
						link.href = url;
						link.download = filename;
						link.style.display = 'hidden';
						document.body.appendChild(link);
						link.dispatchEvent(
							new MouseEvent('click', {
								bubbles: true,
								cancelable: true,
								view: window
							})
						);
						document.body.removeChild(link);
						URL.revokeObjectURL(url);
					}
				}
			} else {
				toastr.error('Vous devez spécifier un nom pour votre fichier dans l\'onglet de paramètre');
			}
		}
	}

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		if (!($("#menuRight").find("a").hasClass("changelabel"))) {
			$("#menuRight").find("a").addClass("changelabel")
		}

		$("#menuRightmapContent").hide();
		$(".BtnFiltersLieux").show();

		var strDL = "";
		if (costum.address && costum.address.level3)
			strDL += '<a class="pull-left btn-menu-connect" href="' + baseUrl + '/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/' + costum.address.level3 + '/extraFormat/csv" target="_blank">Téléchargement</a>';
		// strDL +=  '<a class="pull-left btn-menu-connect" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/'+costum.address.level3+'/extraFormat/csv" target="_blank">Recensement 2023></a>';
		$("#menuTopCenter .cosDyn-app").html(strDL);

		if (costum.isCostumAdmin && typeof costum.country=="undefined") {
			ajaxPost(null, baseUrl + '/' + moduleId + '/default/render?url=survey.views.custom.exportation-csv.coform', {
					form: "63e0a8abeac0741b506fb4f7"
				},
				function(data) {
					$("#page-top").append(data);
					var post = {
						excluded_inputs: [
							"tpls.forms.titleSeparator",
							"tpls.formssectionTitle",
							"tpls.forms.sectionDescription",
							"tpls.forms.cplx.list",
							"tpls.forms.costum.cressReunion.element",
							"tpls.forms.cplx.validateStep",
							"tpls.forms.costum.franceTierslieux.validateStepFtl",
							"tpls.forms.costum.franceTierslieux.validateStep",
							"tpls.forms.sectionTitle"
						],
						is_exporting_following_answer: false,
						following_answer_id: "followingAnswerId",
						is_exporting_dynform: true,
						export_dynform_after: "franceTierslieux1522023_1352_1le5ofi0x733rc1gmbk4",
						is_exporting_answer_id: true,
						completed_answers_only: true,
						is_exporting_user_email: false,
						is_custom_checkbox_aside: true,
						split_steps: true,
						settings: {
							address: "multicolumn",
							checkbox: "multicolumn",
							finder: "complete",
							finder_column: "multicolumn"
						},
						query: {
							form: "63e0a8abeac0741b506fb4f7",
							answers: {
								'$exists': true
							}
						},
						steps: {},
						champs: [],
					};
					if (costum.address && costum.address.level3Name)
						post.region_filters = [costum.address.level3Name];
					$('.csv-field-export').each(function() {
						const champ = this;
						if (champ.checked) {
							post.steps[champ.dataset.step] = post.steps[champ.dataset.step] ? post.steps[champ.dataset.step] : [];
							post.steps[champ.dataset.step].push({
								value: champ.value,
								label: champ.dataset.label,
								type: champ.dataset.type,
							});

							// lately to delete
							post.champs.push({
								step: champ.dataset.step,
								label: champ.dataset.label,
								type: champ.dataset.type,
								value: champ.value
							});
						}
					});

					mylog.log("data params post", post);
					var strDL = ``;
					// strDL +=  '<a class="pull-left btn-menu-connect" href="'+baseUrl+'/api/organization/get/key/franceTierslieux/format/ftl/limit/1000/level3/'+costum.address.level3+'/extraFormat/csv" target="_blank">Téléchargement></a>';
					strDL += `<a id="download-recensement" class="pull-left btn-menu-connect" href="javascript:;">Recensement 2023</a>`;
					// onclick="exportCsvRecensement(`+JSON.stringify(JSON.stringify(post))+`)"
					$("#menuTopCenter .cosDyn-app").append(strDL);

					$("#download-recensement").off().on("click", function() {
						$("#downloading-processing").modal("show");
						exportCsvRecensement(post);

						// $("#csv-exportation").modal("show");
					});
				},
				"html");
		}

		// var typeObjOrga=costum.typeObj;
		// delete typeObjOrga.organization;
		// delete typeObjOrga.projects;
		// delete typeObjOrga.events;
		// costum.init(typeObjOrga);


		//	lazyFilters(0);

	});
</script>