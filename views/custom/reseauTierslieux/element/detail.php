<?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
        //'/css/element/about.css'

    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }
?>

<?php if($edit){?>
    <div class="container-fluid  margin">
        <button class="themeBtn editElement"><i class="fa fa-pencil"></i> Editer les informations</button>
    </div>
<?php } ?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 about-section1 contain-char">
    <div class="col-xs-12 no-padding">
        <div class="why-edit-box">
            <h3><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>Description courte</i>'; ?></h3>
            <p class="activeMarkdown">
                <?php 
                $Parsedown = new Parsedown();
                echo (@$element["description"]) ? $Parsedown->text($element["description"]) : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </p>
            <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/franceTierslieux/book-icon.png" alt="Book Icon">
        </div>
    </div>
    <div class="col-xs-12 contain-char">
        <h2 class="title-section"><?php echo Yii::t("common","Caractérisques") ?></h2>
        <?php $tabSpec=array("typePlace"=>
											["name"=>"Famille de tiers-lieux",
											"icon"=>"briefcase"
											],
										// "services"=>
										// 	["name"=>"Services proposés",
										// 	"icon"=>"tags"
										// 	],
										"manageModel"=>	
											["name"=>"Mode de gestion",
											"icon"=>"adjust"
											],
										// "state"=>	
										// 	["name"=>"Etat du projet",
										// 	"icon"=>"cubes"
										// 	],
										"spaceSize"=>	
											["name"=>"Taille du lieu",
											"icon"=>"expand"
											],
										// "certification"=>	
										// 	["name"=>"Label obtenu",
										// 	"icon"=>"medal"
										// 	],
										// "greeting"=>	
										// 	["name"=>"Actions d'accueil",
										// 	"icon"=>"info"
										// 	],
										"network" =>	
											["name"=>"Réseau d'affiliation",
											"icon"=>"users"
											]
										);

			if(isset($element["tags"]))	{		
             ?>			
				<div class="col-xs-10 col-xs-offset-1 margin-top-20"> 
						<table class='col-xs-12'>
				<?php	foreach($tabSpec as $k => $v){ 

							$strTags="";
							$tagsList="";
							$listCurrent=$this->costum["lists"][$k];
							if(!empty($element["tags"])){
								foreach($element["tags"] as $tag){
									if(in_array($tag, $listCurrent)){
										$strTags.=(empty($strTags)) ? $tag : "<br>".$tag; 
									}
									
									
								}

							}
							if(!empty($strTags)){
							?>
							
								
									<tr>
										<td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
											<?php  
											if(isset($v["icon"])){
												echo "<i class='fa fa-".$v["icon"]." tableIcone'></i>";
											}
											?>
										</td>	
										<td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
											<?php  
											if(isset($v["name"])){
												echo $v["name"];
											}
											
											?>		
										</td>
										<td class="col-xs-7" style="padding:8px;"><?php echo $strTags; ?></td>
									</tr>	
									
						
							
								
						<?php 
						}}
						 ?>
						</table>	
						

						
                        </div>
                        <?php	} ?>        
    
         
    </div>
    <div class="col-xs-12 contain-char">
        <h2 class="title-section">Adresse</h2>
        <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
        <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
    </div>     

    <div class="col-xs-12 contain-char">
        <div class="item-tags">
            <div class="item-content-block">
                <!--<div class="block-title"><i class="fa fa-tags gradient-fill"></i> Tags</div>-->
                <h2 class="title-section">Tags</h2>
            </div>
            <div class="item-content-block tags">
             <?php
                if(@$element["tags"]){
                    $element["tags"] = array_unique($element["tags"]);
                    foreach ($element["tags"] as $key => $tag) { ?>
                        <a  href="#search?tags=<?php echo $tag; ?>"  class="btn-tag" ><?php echo $tag; ?></a>
                    <?php }
                } ?>

            </div>
        </div>
    </div>
</div>

<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 about-section2 contain-char">
    <div class="col-xs-12 no-padding module come-in">
        <div class="con1">
            <h2 class="title-section">Contact</h2>
            <div class="tags-info">
                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-at gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>E-mail</span>
                        <p><?php echo (@$element["email"]) ? $element["email"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-phone gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Contact</span>
                        <?php 
                          $contact = Yii::t("common","Not specified");
                          if(!empty($element["telephone"])){
                            if(is_array($element["telephone"])){
                                if(!empty($element["telephone"]["fixe"]) && is_array($element["telephone"]["fixe"])){
                                    $contact=$element["telephone"]["fixe"][0];
                                }else if(!empty($element["telephone"]["mobile"]) && is_array($element["telephone"]["mobile"])){
                                    $contact=$element["telephone"]["mobile"][0];
                                }
                            }else if(is_string($element["telephone"])){
                                $contact=$element["telephone"];
                            }
                          }else if(!empty($element["mobile"])){
                            $contact=$element["mobile"];
                          }
                          
                        ?>
                        <p><?php echo $contact; ?></p>

                    </div>
                    <div class="clear"></div>
                </div>

                <div class="tg">
                    <div class="tgimg ti-2x"><i class="fa fa-globe gradient-fill"></i></div>
                    <div class="tgcon">
                        <span>Site web</span>
                        <p>
                            <?php echo (isset($element["url"])) ? "<a href='".$element["url"]."' target='_blank' id='urlWebAbout' style='cursor:pointer;'>".$element["url"]."</a>" : "<i>".Yii::t('common','Not specified')."</i>" ?>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-xs-12 no-padding margin-top-20">
        <h2 class="title-section">Horaires d'ouverture</h2>
            <?php 
            if (in_array($type, [Event::COLLECTION, Project::COLLECTION, Organization::COLLECTION])) {
                $modeDate = "recurrency";
                $title = Yii::t("common", "When");
                $emptyval = Yii::t("common", "No date");
                if ($type == Organization::COLLECTION) {
                    $title = Yii::t("common", "Opening hours");
                    $emptyval = Yii::t("common", "No opening hours");
                    $modeDate = "openingHours";
                } else if ($type == Project::COLLECTION) {
                    $modeDate = "date";
                }
             echo  $this->renderPartial('co2.views.pod.dateOH', array("element" => $element, "title" => $title, "emptyval" => $emptyval, "edit" => false, "openEdition" => false));
           }; 
           ?>
    </div>
    
    <div class="col-xs-12 no-padding margin-top-20">
        <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
        <?php echo $this->renderPartial('co2.views.pod.newSocialNetwork', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
    </div>
</div>
<?php $levelTer=isset($element["level"]) ? $element["level"] : ""; ?>
<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });


    function afficheMap(){
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container : "divMapContent",
            latLon : contextData.geo,
            activeCluster : false,
            zoom : 16,
            activePopUp : false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt : {
                id : contextData.id,
                collection : contextData.type,
                geo : contextData.geo
            },
            center : true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };

    jQuery(document).ready(function() {
        bindDynFormEditable();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });


        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });

        coInterface.bindLBHLinks();

        //edit element
        pageProfil.bindViewActionEvent();
        $(".editElement").click(function(){
            dyFObj.editMode=true;
            uploadObj.update = true;
            dyFObj.currentElement={type : contextData.type, id : contextData.id};

            var levelTer= <?php echo json_encode($levelTer) ?> ;
            if (levelTer !=="" && typeof costum.lists.level !="undefined" && exists(costum.lists.level[levelTer])){
                contextData.level=costum.lists.level[levelTer];
            }
            var dataElem = jQuery.extend(true, {},contextData);
            dataElem.type=contextData.typeElement;
            var formType= (typeof contextData.category!="undefined" && contextData.category=="network") ? "reseau" : typeObj[contextData.type].sameAs;
            var editDynF=jQuery.extend(true, {},costum.typeObj[contextData.type].dynFormCostum);
	        editDynF.onload.actions.hide.similarLinkcustom=1;
            dyFObj.editElement(contextData.type,contextData.id, null, editDynF, {}, "afterLoad");
        });
        $("#menuRight").find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
        $("#menuRight").find("a").toggleClass("changelabel");
        $(".BtnFiltersLieux").hide();
    });

    $(document).ready(function() {
        
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });

</script>
