<?php
    
    $cssAnsScriptFilesModule = array(
        '/js/admin/panel.js',
        '/js/admin/admin_directory.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $cssAnsScriptFilesModule = [
        '/plugins/handsometable/js/handsontable.full.js',
        '/plugins/handsometable/css/handsontable.full.min.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getBaseUrl(true));
    $adminConstruct = $this->appConfig["adminPanel"];
    $adminConstruct = $this->appConfig["adminPanel"];
    if (isset($this->costum)) {
        $slugContext = isset($this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"];
        $cssJsCostum = array();
        if (isset($adminConstruct["js"])) {
            $jsCostum = ($adminConstruct["js"] === true) ? "admin.js" : $adminConstruct["js"];
            array_push($cssJsCostum, '/js/' . $slugContext . '/' . $jsCostum);
        }
        if (isset($adminConstruct["css"]))
            array_push($cssJsCostum, '/css/' . $slugContext . '/admin.css');
        if (!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule("costum")->getAssetsUrl());
    }
    // $logo = (@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";

?>
<!--defualt modal-->
<!-- /.modal -->
<!--defualt modal-->
<div class="filtersGlobal text-right">
    <span class="labelFilterGlobal">Espaces contributeurs : </span>
    <a class="filterGlobal" data-key="base">Accueil</a>
    <a class="filterGlobal" data-key="formation">Formation</a>
    <a class="filterGlobal" data-key="animation"> Animation de réseaux </a>
</div>

<div class='col-xs-12 searchBars-container'></div>
<div id='filterContainer' class='searchObjCSS'>

</div>
<div class='headerSearchIncommunity no-padding col-xs-12'></div>

 <div class='bodySearchContainer'>
    <div class='col-xs-12' id='dropdown_search'></div>
    <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
</div>
<div id="adminDirectory"></div>
<!-- <div class="col-md-12 padding-20 margin-20" style="border-radius:10px;border:solid 1px #2C3E50">
    <h2>Tableau de bord statistiques : </h2>
    <div class="col-md-6">
        <div class=" col-md-12 stat-card">
            <div class="card-box card tilebox-one">
                <span class="pull-right">
                    <i class="fa fa-users" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px; "></i>
                </span>
                <h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important; ">Nombre d'adhérent</h6>
                <h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; color: #74b976">2</h3>
                <div class="">
                    <span class=""></span>
                    <span class="" style="color: rgba(0,0,0,.4);">Données pour l'année 2023</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class=" col-md-12 stat-card">
            <div class="card-box card tilebox-one">
                <span class="pull-right">
                    <i class="fa fa-calendar" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px; "></i>
                </span>
                <h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important; ">Mise à jour récente</h6>
                <h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; color: #74b976">5</h3>
                <div class="">
                    <span class=""></span>
                    <span class="" style="color: rgba(0,0,0,.4);">Dans les 7 derniers jours</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class=" col-md-12 stat-card">
            <div class="card-box card tilebox-one">
                <span class="pull-right">
                    <i class="fa fa-university" style="color: #3ea3b4; font-weight: bold !important; font-size: 50px; "></i>
                </span>
                <h6 class="" style="font-size: 14px !important; color: #3ea3b4; font-weight: bold !important; ">Nombre de personnes formées</h6>
                <h3 class="" data-plugin="counterup" style="padding-top: 5px; font-size: 40px !important; color: #74b976">4</h3>
                <div class="">
                    <span class=""></span>
                    <span class="" style="color: rgba(0,0,0,.4);">Toutes formations confondues</span>
                </div>
            </div>
        </div>
    </div>
</div> -->

<style>
    #mainNav, .navbar-top{
        box-shadow:none;
    }
    h6 {
        font-size: 1rem !important;
        font-weight: 500 !important;

    }

    h3 {
        font-size: 2.5rem !important;
        font-weight: 600;
    }

    h1 {
        font-size: 2em;
    }

    .card-box {
        background-color: #fff !important;
        padding: 1.25rem !important;;
        -webkit-box-shadow: 0 0 35px 0 rgba(73, 80, 87, .15) !important;;
        box-shadow: 0 0 35px 0 rgba(73, 80, 87, .15) !important;;
        margin-bottom: 24px !important;;
        border-radius: 0.5rem !important;;
        margin: 1rem !important;;
    }

    .card-box > .icon {
        float: right;
    }

    .card-box h3,
    h6 {
        margin: 0;
    }

    h6 {
        font-size: 1rem;
        font-weight: 500;
    }

    h3 {
        font-size: 2.5rem;
        font-weight: 600;
    }

    .header > h1,
    p {
        margin: 0;
    }

    .header > h1 {
        font-weight: 600;
    }

    .btnCsvExport {
        display: none;
        padding-top: 6px;
        margin-left: 4px;
        cursor: pointer;
    }

    .handsontable th{
        height:50px !important;
        border:none !important;
        background-color : #2C3E50 !important;
        color: white;
        font-family: 'customFontfontrfflabsLexend-Light.ttf';
    }

    @media (max-width: 1000px) {
        .stat-card {
            width: 100% !important;
        }

        /* header {
            height: 150px;
            clear: both;
        } */

        .select2-selection__choice__remove {
            display: none !important;
        }

        .mybtngroup {
            display: none;
        }

        .contbntngroup:hover .mybtngroup {
            display: inline-block;
        }
    }

    #session-modal {
        z-index: 999999;
    }

    #session-modal .modal-dialog {
        width: calc(100% - 20px);
    }

    #session-modal .modal-header {
        background-color: white !important;
    }
</style>


<style>

    .countResults{
        margin: unset;
    font-size: 13px;
    font-weight: lighter;
    }

    #adminDirectory  > .pageTable ~ .pageTable{
        display:block;
        padding:0px 15px;
    }
    #adminDirectory .pageTable{
        display:none;
    }

    .headerSearchright, .headerSearchleft{
        display:flex;
        align-items:center;
    }

    .headerSearchleft{
        justify-content: start;
    }

    .headerSearchright{
        justify-content: end;
    }
    .searchBar-filters .search-bar,
    .searchBar-filters .main-search-bar-addon {
        /* border: unset;
        border-bottom: solid 1px #2C3E50;
        border-radius: unset;
        background-color: white !important;
        padding-bottom: 0px;
        text-align: left; */
    }

    .searchBar-filters {
        margin-right: 20px;
        margin-bottom: 10px;
    }

    .searchBar-filters .main-search-bar-addon > i:before {
        content: '\1F50E';
        color: #2C3E50;
    }

    #filterContainer .dropdown .btn-menu,
    #filters-nav .dropdown .btn-menu,
    #activeFilters .filters-activate {
        background-color: #2C3E50;
        color: white !important;
        border: none;
        border-radius: 5px;
        font-size: 14px;
        padding: 8px 15px;
    }

    #filterContainer .dropdown .dropdown-menu,
    #filters-nav .dropdown .dropdown-menu {
        border: none;
    }

    @media (max-width: 767px) {

        #filterContainer .dropdown .dropdown-menu,
        #filters-nav .container-filters-menu .dropdown .dropdown-menu {
            border: none !important;
        }
    }

    #filterContainer .dropdown .dropdown-menu button,
    #filters-nav .dropdown .dropdown-menu button {
        border: none;
        background: #ffffff;
        color: #2C3E50;
        text-align: left;
        /* border-bottom: 1px solid #929da8; */
        text-align: left;
        position: relative;
    }

    #filterContainer .dropdown .dropdown-menu button:after,
    #filters-nav .dropdown .dropdown-menu button:after {
        content: '';
        position: absolute;
        border-bottom: solid 1px #2C3E50;
        width: 90%;
        left: 5%;
        bottom: 0;
        opacity: 0.2;
    }

    #filterContainer .dropdown .dropdown-menu button:hover,
    #filters-nav .dropdown .dropdown-menu button:hover {
        background-color: unset;
        font-size: 1.4em;
        font-weight: 700;
    }

    /* .searchBar-filters .main-search-bar-addon{
        border: unset;
        border-bottom: solid 1px #939ea9;
        border-radius: unset;
    } */


    .panel-body > div {
        width: 100%;
        padding-right:10px;
        /* margin-bottom: 15px; */
        overflow-y: hidden;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        /* border: 1px solid #ddd; */
        /* Mozilla firefox */

    }

    /* .content-td, .content-th{
        height:10vh;
    } */

    .panel-body div table.table-striped {
        table-layout: auto !important;
        word-wrap: normal !important;
    }

    .panel-body div::-webkit-scrollbar,
    .list-filters::-webkit-scrollbar {
        width: 8px;
    }

    /* Ascenseur */
    .panel-body div::-webkit-scrollbar-thumb,
    .list-filters::-webkit-scrollbar-thumb {
        background: #2C3E50;
        border-radius: 8px;
    }

    .panel-body div::-webkit-scrollbar-track {
        position: relative;
        box-shadow: inset 0 0px 0px 2px white, inset 0 2px 10px 2px #2C3E50;
    }

    .panel-body div::-webkit-scrollbar-button:single-button {
        display: block;
        border-style: solid;
        height: 16px;
        width: 20px;
    }

    .panel-body div::-webkit-scrollbar-button:single-button:horizontal:increment {
        border-width: 8px 0px 8px 12px;
        border-color: transparent transparent transparent #2C3E50;
    }

    .panel-body div::-webkit-scrollbar-button:single-button:horizontal:decrement {
        border-width: 8px 12px 8px 0px;
        margin-right: 1px;
        border-color: transparent #2C3E50 transparent transparent;
    }

    .simple-pagination {
        border-bottom: none;
    }

    #panelAdmin {
        border: none;
    }

    /* #panelAdmin thead {
        color: #2C3E50;
        font-size: 13px;
        position: relative;
    } */

    /* #headerTable:after {
        width: 98%;
        content: '';
        left: 1%;
        bottom: 0;
        border-top: 1px solid #2C3E50;
        z-index: 20;
        position: absolute;
        margin: auto;
    } */

    #panelAdmin th,
    #panelAdmin td {
        border: none;
        vertical-align: middle !important;
        min-width: 10vw;
        padding: 0.5em;
        font-size: 14px;
    }

    #panelAdmin td{
        font-size:13px;
        /* text-align:left; */
    }


    #panelAdmin tr > td:first-child {
        font-weight: bold;
    }


    @media screen and (max-width: 767px) {
    }

    .btn-contact .dismissBtn {
        margin-right: 5px;
    }

    .btn-contact,
    .btn-formation {
        display: inline-flex;
        align-items: center;
        justify-content: start;
        width: 100%;
    }

    .btn-formation {
        justify-content: start;
    }

    .editFomation {
        cursor: pointer !important;
    }

    .pageTable.col-md-12 {
        display: none;
    }

    #tableauDeBord #input {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;

    }

    #openModal.modal {
        left: 30%;
        right: 0px;
        box-shadow: -2px 4px 10px 0px rgb(0 0 0 / 50%);
    }

    #tableauDeBord {
        padding-left: 1%;
        padding-right: 1%;
    }

    #tableauDeBord .form-group {
        width: 47%;
    }

    .portfolio-modal .modal-content {
        text-align: left;
    }

    #tableauDeBord .territorialCercleselect,
    #tableauDeBord .heldActionselect {
        width: 49.5% !important;
    }

    #tableauDeBord .ptlCommenttextarea {
        width: 97% !important;
    }

    #tableauDeBord #input .form-group {
        flex-basis: auto;
    }

    #tableauDeBord .select2-container {
        height: auto;
        padding: 0;
    }

    #tableauDeBord .flex-row {
        display: flex;
        flex-direction: row;
    }

    #tableauDeBord .flex-column {
        flex: 1;
        margin-right: 10px;
    }

    #tableauDeBord .flex-container {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
    }

    #search-content,
    #filters-nav,
    .bodySearchContainer .pageTable {
        display: none !important;
    }

    .labelFilterGlobal {
        font-size: 13px;
        margin: 10px;
        display: inline-block;
    }

    @media screen and (max-width: 767px) {
        .labelFilterGlobal {
            float: left;
        }

        .filtersGlobal .filterGlobal {
            display: inline-grid
        }
    }

    .filtersGlobal {
        /* background-color: black; */
        /* padding: 10px 10px 0 0; */
        /* color: white; */
        color: #2f2f2f;
    }

    .filtersGlobal .filterGlobal {
        background: white;
    text-decoration: none;
    font-size: 16px;
    padding: 10px 20px 10px 20px;
    cursor: pointer;
    /* padding-bottom: 5px; */
    /* border-right: solid 1px var(--main1); */
    margin-right: 15px;
    border-radius: 0px 0px 10px 10px;
    color: #2b3b51;
    border: 1px solid #2b3b51;
    border-top: none;
    }

    .filtersGlobal a.filterGlobal.active {
        background-color: #f3d361;
        /* color: black; */
        /* border:none; */
        /* padding:23px; */
    }

    #tableauDeBord .notEditable .well:hover {
        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    }

    #tableauDeBord .notEditable .well {
        border-radius: 20px;
        margin-top: 30px;
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    }

    #tableauDeBord .editable .well,
    .editable-input.well {
        border-radius: 20px;
        border: 2px solid #f3d361;
        margin-top: 30px;
        box-shadow: 0 1px 6px #f3d361, 0 3px 6px rgba(0, 0, 0, 0.23);
        padding: 10px 25px;
        display: block;
        margin-top: 2%;

    }

    #tableauDeBord .title h3 {
        margin: 0 !important;
    }

    #tableauDeBord .notEditable .title {
        margin-top: -40px;

    }

    #tableauDeBord .title {
        height: auto;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 8px 0px;
        border-radius: 5px;
        margin-bottom: 16px;
        background-color: #f3d361;
        width: 250px;
        margin-top: -33px;
    }

    #trainer button,
    #divnewsletterSubscription button,
    #supportCercle button,
    #actionContributor button,
    #referentTerritorialCercle button {
        width: 48%
    }

    #trainer,
    #divnewsletterSubscription,
    #supportCercle,
    #actionContributor,
    #referentTerritorialCercle {
        display: flex;
        justify-content: space-around;
    }

    .select2-search-choice-close::before {
        color: red !important;
    }

    .processingLoader {
        display: none;
    }

    .btn-dyn-checkbox.bg-green-k,
    .btn-dyn-checkbox.bg-red,
    form .selectParent {
        background-color: #2C3E50 !important;
        border-color: #2C3E50;
    }

    .btn-dyn-checkbox.letter-red,
    .btn-dyn-checkbox.letter-green {
        color: #333 !important;
        background-color: #fff;
        border: 1px solid #ccc;
    }

    .btn-dyn-checkbox,
    .btn-dyn-checkbox:hover {
        font-size: 14px;
        height: 30px;
        font-weight: 400;
        border: none;
    }

    /* #tableauDeBord .flex-container,
		#tableauDeBord .flex-row{
			display: block;
		} */


    .detailContactCard .card-img-top {
        min-height: 140px;
    }

    .detailContact {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: 15px;
        box-shadow: 3px 4px 5px 3px rgb(0 0 0 / 20%);
        margin-bottom: 20px;
        margin-left: 0px;
        margin-right: 0px;
    }

    .content-img {
        padding: 18px;
    }

    .div-img {
        background: #ccc;
        height: 100px;
        padding: 20px;
        border-radius: 50%;
        width: 100px;
    }

    .detailContactCard .div-img i.fa {
        border-radius: 5px 0 21px 0;
        font-size: 4em;
    }

    .detailContactCard img {
        border-radius: 50%;
        width: 100px;
        height: 100px;
        background-color: transparent;
        object-fit: cover;
        object-position: center;
    }

    .detail-box h4 {
        text-transform: none;
    }

    .detailContactCard .detail-box .entityLocality {
        margin-bottom: 5px;
        min-height: 20px;
    }

    .smartgrid-slide-element ul.tag-list .btn-tag {
        background: #939ea9;
        color: white !important;
    }

    #dropdown_search {
        /* margin-top: 5px; */
        min-height:auto !important
    }

    .change-views label, .btnCsvExport label, .btnPdfContact span {
        font-size: 12px;
        margin-left: 5px;
        color:#2b3b51 !important;
        font-weight: lighter;
        margin-bottom:0px;
    }

    .change-views .fa, .btnCsvExport .fa,.btnPdfContact .fa {
        font-size: 20px;
        /* margin-bottom: 5px; */
        
        color:#2b3b51;
    }

    .change-views, .btnPdfContact{
        padding-top: 6px;
        margin-left: 4px;
        display: flex;
        align-items: center;

    }

    .entityLocality fa {
        color: #f3d361;
    }

    @media screen and (max-width: 767px) {

        #tableauDeBord .flex-container,
        #tableauDeBord .flex-row,
        #tableauDeBord #input {
            display: block;
        }

        #tableauDeBord .form-group {
            width: 100% !important;
        }

        .btn-contact {
            padding-top: 15px;
        }

        #tableauDeBord #input .form-group {
            width: 100% !important;
        }

        /* .panel-body div {
			width: 100%;
			margin-bottom: 15px;
			overflow-y: hidden;
			overflow-x: auto;
			-ms-overflow-style: -ms-autohiding-scrollbar;
			border: 1px solid #ddd;
		}
		.panel-body div table.table-striped{
			table-layout: auto !important;
			word-wrap: normal !important;
		} */
        .filtersGlobal .filterGlobal {
            padding: 5px;
            font-size: 15px;
        }

        .filtersGlobal {
            /* padding: 8px; */
        }

        .searchObjCSS .searchBar-filters {
            width: 100% !important;
            margin-bottom: 3px;
        }

        #filterContainer .dropdown,
        #filters-nav .dropdown {
            text-align: center;
        }

        #filterContainer .dropdown .dropdown-menu,
        #filters-nav .dropdown .dropdown-menu {
            padding: 0px;
            top: 8px;
            width: 100%;
        }

        .searchBar-filters .search-bar {
            border-left: 1px solid;
            border-radius: 4px 0 0 4px !important;
        }

        .searchBar-filters .main-search-bar-addon {
            border-right: 1px solid #6b6b6b;
            border-radius: 0 4px 4px 0 !important;
        }

        #filterContainer {
            margin-top: 0px;
        }

        #filterContainerInside {
            min-height: unset !important;
        }

        #tableauDeBord .title h3 {
            font-size: 18px;
        }

        .content-img {
            padding: 20px 10px;
        }

        .detailContactCard img {
            width: 50px !important;
            height: 50px !important;
            margin-left: auto;
            margin-right: auto;
        }

        .detailContactCard .div-img i.fa {
            font-size: 2em;
        }

        .content-img {
            padding: 20px 10px;
        }

        .div-img {
            height: 50px;
            padding: 10px;
            width: 50px;
        }

        .smartgrid-slide-element ul.tag-list .btn-tag {
            text-align: left;
            white-space: break-spaces;
            margin-bottom: 2px;
        }
    }
    .listSameName a {
        background-color : #2C3E50;
        border: none;
    }
    .listSameName a:hover{
        background-color :var(--main1);

    }
    .listSameName a span{
        color : white;
    }
    .listSameName > div > span{
        color : #2C3E50;
    }
</style>
<div class="top-scrollbar-container" style='width:100%;'>
    <div class="outter-container-top"></div>
</div>
<script>
    var hot={};
    var pageFilter = "base";
    var tableHeaders;
    adminDirectory.initTable = function () {
        var aObj = this;
        var str = "";
        mylog.log("adminDirectory.initTable ", aObj.actionsStr, aObj.panelAdmin);
        if (typeof aObj.panelAdmin.table != "undefined" && aObj.panelAdmin.actions != null) {
            var classCol = 'col-xs-1 text-center';
            if (typeof aObj.panelAdmin.table.actions != "undefined") {
                if (typeof aObj.panelAdmin.table.actions.class != "undefined") {
                    classCol = aObj.panelAdmin.table.actions.class;
                }
            }
            str += "<th class='" + classCol + "'>Actions</th>";
        }
        if (typeof aObj.panelAdmin.table != "undefined" &&
            aObj.panelAdmin.table != null) {
            //mylog.log("adminDirectory.initTable table ", aObj.panelAdmin.table);
            $.each(aObj.panelAdmin.table, function (key, value) {
                mylog.log("adminDirectory.initTable each ", key, value, (key != "actions"));
                if (key != "actions") {
                    mylog.log("adminDirectory.initTable if ");
                    if (value === "true") {
                        str += "<th>" + key + "</th>";
                    } else if (typeof value != "undefined" && typeof value.name != "undefined") {
                        var classCol = ((typeof value.class != "undefined") ? value.class : " text-center ");
                        var sort = "";
                        if (typeof value.sort != "undefined" && typeof value.sort.field != "undefined") {
                            if (typeof value.sort.up != "undefined") {
                                sort += "<a href='javascript:;' data-sort='up' data-field='" + value.sort.field + "' class='sortAdmin' > <i class='fa fa-arrow-up'></i> </a>";
                            }
                            if (typeof value.sort.down != "undefined") {
                                sort += "<a href='javascript:;' data-sort='down' data-field='" + value.sort.field + "' class='sortAdmin' > <i class='fa fa-arrow-down'></i> </a>";
                            }
                        }
                        str += "<th class='" + classCol + "'><div class='content-th'>" + value.name + " " + sort + "</div></th>";
                    }
                }

            });

        }

        $("#" + aObj.container + " #headerTable").append(str);
        coInterface.bindLBHLinks();
    };
    adminDirectory.buildDirectoryLine = function (e, collection, icon) {
        mylog.log("adminDirectory.buildDirectoryLine", e, collection, icon);
        var aObj = this;
        var strHTML = "";
        if (typeof e._id == "undefined" ||
            ((typeof e.name == "undefined" || e.name == "") &&
                (e.text == "undefined" || e.text == ""))) {
            return strHTML;
        }
        var actions = "";
        var classes = "";
        var id = e._id.$id;
        var status = [];
        strHTML += '<tbody><tr id="' + e.collection + id + '" class="' + e.collection + ' line">';
        var actionsStr = aObj.actions.init(e, id, e.collection, aObj);
        mylog.log("adminDirectory.buildDirectoryLine actionsStr", actionsStr);
        if (actionsStr != "") {
            aObj.actionsStr = true;
            mylog.log("adminDirectory.buildDirectoryLine aObj.actionsStr", aObj.actionsStr);
            strHTML += '<td class="">' +
                '<div class="btn-group">' +
                actionsStr +
                '</div>' +
                '</td>';
        }
        strHTML += aObj.columTable(e, id, e.collection);

        strHTML += '</tr></tbody';
        return strHTML;
    };

    adminDirectory.values.surname = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.surname != "undefined") {
            if (pageFilter != "base") {
                str = "<a href='javascript:;' class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.surname + "</a>"
            } else {
                str = e.surname;
            }
        }
        return str;
    };

    adminDirectory.values.firstName = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.firstName != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.firstName + "</a>"
            } else {
                str = e.firstName;
            }
        }
        return str;
    };
    adminDirectory.values.telephone = function (e, id, type, aObj) {
        var str = "";
        var telephone=(typeof e.telephoneCie != "undefined") ? e.telephoneCie : ((typeof e.telephone != "undefined" && typeof e.telephone.mobile != "undefined") ? e.telephone.mobile : "");
        if (typeof telephone!="undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + telephone + "</a>"
            } else {
                str = telephone;
            }
        }
        return str;
    };
    adminDirectory.values.email = function (e, id, type, aObj) {
        var str = "";
        var mail=(typeof e.emailCie!="undefined") ? e.emailCie : ((typeof e.otherEmail!="undefined") ? e.otherEmail : e.email);
        if (typeof mail != "undefined") {
            
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + mail + "</a>"
            } else {
                str = mail;
            }
        }
        return str;
    };
    adminDirectory.values.status = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.status != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.status + "</a>"
            } else {
                str = e.status;
            }
        }
        return str;
    };
    adminDirectory.values.linkedThirdPlace = function (e, id, type, aObj) {
        mylog.log("linkedThirdPlace values",e);
        var str = "";
        if (typeof e.linkedThirdPlace != "undefined" && Object.keys(e.linkedThirdPlace).length>0) {
            $.each(e.linkedThirdPlace,function(idRes,valRes){
                // if (pageFilter != "base") {
                    if(str!==""){
                            str+="<br>";
                        }
                        str += "<a href='#page.type."+valRes.type+".id."+idRes+"' class='lbh-preview-element'>" + valRes.name + "</a>"
                // } else {
                //     str = valRes.name;
                // }
            });
            coInterface.bindLBHLinks(); 
            
        }
        return str;

    };
    adminDirectory.values.otherLinkedStructure = function (e, id, type, aObj) {
        mylog.log("otherLinkedStructure values",e);
        var str = "";
        if (typeof e.otherLinkedStructure != "undefined" && Object.keys(e.otherLinkedStructure).length>0) {
            $.each(e.otherLinkedStructure,function(idRes,valRes){
                // if (pageFilter != "base") {
                    if(str!==""){
                            str+="<br>";
                        }
                        str += "<a href='#page.type."+valRes.type+".id."+idRes+"' class='lbh-preview-element'>" + valRes.name + "</a>"
                // } else {
                //     str = valRes.name;
                // }
            });
            coInterface.bindLBHLinks(); 
            
        }
        return str;

    };
    // adminDirectory.values.linkedStructure = function (e, id, type, aObj) {
    //     thirdPlaceQuery={
    //         searchType:["organizations"],
    //         sourceKey : "franceTierslieux"
    //     };
    //     thirdPlaceQuery.filters={};
    //     thirdPlaceQuery.filters["links.members."+id]={'$exists' :true};
    //     // var mapData=data;
    //     var str = "";
    //     // if (typeof e.linkedStructure != "undefined") {
    //     //     if (pageFilter != "base") {
    //     //         str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.linkedStructure + "</a>"
    //     //     } else {
    //     //         str = e.linkedStructure;
    //     //     }
    //     // }
        
    //     ajaxPost(
    //         null,
    //         baseUrl+"/" + moduleId + "/search/globalautocomplete",
    //         thirdPlaceQuery,
    //         function(res){
    //             res=res.results;
    //             if(Object.keys(res).length>0){
    //                 $.each(res,function(idRes,valRes){
    //                     if(str!==""){
    //                         str+="<br>";
    //                     }
    //                     str += "<a href='#page.type."+valRes.collection+".id."+idRes+"' class='lbh-preview-element'>" + valRes.name + "</a>"


    //                 });
    //             }
    //             $("#citoyens"+id+" .linkedStructure").append(str);
    //             coInterface.bindLBHLinks();                
    //         }
            
    //     );
        
    //     return str;
    // };
    adminDirectory.values.functionThirdPlace = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.functionThirdPlace != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.functionThirdPlace + "</a>"
            } else {
                str = e.functionThirdPlace;
            }
        }
        return str;
    };
    adminDirectory.values.functionOtherStructure = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.functionOtherStructure != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.functionOtherStructure + "</a>"
            } else {
                str = e.functionOtherStructure;
            }
        }
        return str;
    };
    adminDirectory.values.adherent = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.adherent != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.adherent + "</a>"
            } else {
                str = "<span>" + e.adherent + "</span>";
            }
        }
        return str;
    };
    adminDirectory.values.keyMoment = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.keyMoment != "undefined") {
            if (pageFilter != "base") {
                str = "<a class='editTbContact' data-id='" + id + "' data-filter ='" + pageFilter + "' data-type='" + type + "'>" + e.keyMoment + "</a>"
            } else {
                str = e.keyMoment;
            }
        }
        return str;
    };
    adminDirectory.values.newsletterSubscription = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.newsletterSubscription != "undefined") {
            if (e.newsletterSubscription == "true" || e.newsletterSubscription == true) {
                str = trad.yes;
            }
            if (e.newsletterSubscription == "false" || e.newsletterSubscription == false) {
                str = trad.no;
            }
        }
        return str;
    };
    adminDirectory.values.trainer = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.trainer != "undefined") {
            if (e.trainer == "true" || e.trainer == true) {
                str = trad.yes;
            }
            if (e.trainer == "false" || e.trainer == false) {
                str = trad.no;
            }
        }
        return str;
    };
    adminDirectory.values.presentationSession = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.presentationSession != "undefined") {
            str = e.presentationSession;
        }
        return str;
    };
    adminDirectory.values.interestingTraining = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.interestingTraining != "undefined") {
            str = e.interestingTraining;
        }
        return str;
    };
    adminDirectory.values.ptlConsideredTraining = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.ptlConsideredTraining != "undefined") {
            str = e.ptlConsideredTraining;
        }
        return str;
    };
    adminDirectory.values.competence = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.competence != "undefined") {
            str = e.competence;
        }
        return str;
    };
    adminDirectory.values.profileCandidate = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.profileCandidate != "undefined") {
            str = e.profileCandidate;
        }
        return str;
    };
    adminDirectory.values.ptlComment = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.ptlComment != "undefined") {
            str = e.ptlComment;
        }
        return str;
    };
    adminDirectory.values.supportCercle = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.supportCercle != "undefined") {
            if (e.supportCercle == "true" || e.supportCercle == true) {
                str = trad.yes;
            }
            if (e.supportCercle == "false" || e.supportCercle == false) {
                str = trad.no;
            }
        }
        return str;
    };
    adminDirectory.values.supportedStructures = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.supportedStructures != "undefined") {
            $.each(e.supportedStructures, function (ks, vs) {
                str += vs.name;
            })
        }
        return str;
    };
    adminDirectory.values.actionContributor = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.actionContributor != "undefined") {
            if (e.actionContributor == "true" || e.actionContributor == true) {
                str = trad.yes;
            }
            if (e.actionContributor == "false" || e.actionContributor == false) {
                str = trad.no;
            }
        }
        return str;
    };
    adminDirectory.values.networkContributor = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.networkContributor != "undefined") {
            str = e.networkContributor;
        }
        return str;
    };
    adminDirectory.values.mentor = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.mentor != "undefined") {
            $.each(e.mentor, function (ke, ve) {
                str += "- " + ve.name + "</br>";
            })
        }
        return str;
    };
    adminDirectory.values.heldAction = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.heldAction != "undefined") {
            str = e.heldAction;
        }
        return str;
    };
    adminDirectory.values.territorialCercle = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.territorialCercle != "undefined") {
            str = e.territorialCercle;
        }
        return str;
    };

    adminDirectory.values.referentTerritorialCercle = function (e, id, type, aObj) {
        var str = "";
        if (typeof e.referentTerritorialCercle != "undefined") {
            if (e.referentTerritorialCercle == "true" || e.referentTerritorialCercle == true) {
                str = trad.yes;
            }
            if (e.referentTerritorialCercle == "false" || e.referentTerritorialCercle == false) {
                str = trad.no;
            }
        }
        return str;
    };
    adminDirectory.actions.edit = function (e, id, type, aObj) {
        var str = "";
        if (isInterfaceAdmin) {
            str = '<button data-id="' + id + '" data-type="citoyens" class="col-xs-12 editContact btn bg-green-k text-white"><i class="fa fa-edit"></i> Modifier</button>';
        } else if (userId && userId == id) {
            str = '<button data-id="' + id + '" data-type="citoyens" class="col-xs-12 editContact btn bg-green-k text-white"><i class="fa fa-edit"></i> Modifier</button>';
        }
        return str;
    };
    adminDirectory.bindCostum = function (aObj) {
        // $("#" + aObj.container + " .editTbContact").on("click", function () {
        //     var src = "";
        //     var filter = $(this).data("filter");
        //     var id = $(this).data("id");
        //     var type = $(this).data("type");
        //     var data = aObj.getElt(id, type);
        //     mylog.log("dnfdnfsd", data);
        //     $("#openModalContent").addClass("container");
        //     var input = {};
        //     // $("#tableauDeBord .editable-input .title").css("display","flex")
        //     src += `<div id="tableauDeBord">
		// 		<div id="simple-input">
		// 		<div class="text-center">
		// 			<h2> Fiche contact </h2></div>
		// 			<div class="notEditable flex-container ">
		// 				<div class="well">
		// 					<div class="title text-center"> <h3> Zone verrouillé </h3></div>
		// 					<div class="flex-row">
		// 						<div class="form-group flex-column">
		// 							<label for="surname"> <i class="fa fa-chevron-down"></i> Nom</label>
		// 							<input type="text" class="form-control" id="surname" value="${(typeof data.surname != "undefined") ? data.surname : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="firstName"> <i class="fa fa-chevron-down"></i> Prénom</label>
		// 							<input type="text" class="form-control" id="firstName" value="${(typeof data.firstName != "undefined") ? data.firstName : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="email"> <i class="fa fa-chevron-down"></i> Email</label>
		// 							<input type="text" class="form-control" id="email" value="${(typeof e.emailCie!="undefined") ? e.emailCie : ((typeof e.otherEmail!="undefined") ? e.otherEmail : e.email)}" disabled>
		// 						</div>
		// 					</div>
		// 					<div class="flex-row">
		// 						<div class="form-group flex-column">
		// 							<label for="status"> <i class="fa fa-chevron-down"></i> Statut</label>
		// 							<input type="text" class="form-control" id="status" value="${(typeof data.status != "undefined") ? data.status : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="typeLinkedStructure"><i class="fa fa-chevron-down"></i> Rattachement structure</label>
		// 							<input type="text" class="form-control" id="typeLinkedStructure" value="${(typeof data.typeLinkedStructure != "undefined") ? data.typeLinkedStructure : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="function"><i class="fa fa-chevron-down"></i> Fonction dans la structure</label>
		// 							<input type="text" class="form-control" id="function" value="${(typeof data.function != "undefined") ? data.function : ""}" disabled>
		// 						</div>
		// 					</div>
		// 					<div class="flex-row">
		// 						<div class="form-group flex-column">
		// 							<label for="adherent"><i class="fa fa-chevron-down"></i> Adhérent</label>
		// 							<input type="text" class="form-control" id="adherent" value="${(typeof data.adherent != "undefined") ? data.adherent : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="keyMoment"><i class="fa fa-chevron-down"></i> Participation temps forts</label>
		// 							<input type="text" class="form-control" id="keyMoment" value="${(typeof data.keyMoment != "undefined") ? data.keyMoment : ""}" disabled>
		// 						</div>
		// 						<div class="form-group flex-column">
		// 							<label for="newsletterSubscription"><i class="fa fa-chevron-down"></i> Intéressé par les newsletters</label>
		// 							<div id="divnewsletterSubscription">
		// 								<button type="button" disabled data-value="true" class="btn ${(data.newsletterSubscription == "true" || data.newsletterSubscription == true) ? "btn-primary btn-active" : "btn-default"}" data-toggle="tooltip" data-placement="top" title="Oui"><span>Oui</span></button>
		// 								<button type="button"  disabled data-value="false" class="btn ${(data.newsletterSubscription == "false" || data.newsletterSubscription == false) ? "btn-primary btn-active" : "btn-default"}" data-toggle="tooltip" data-placement="top" title="Non"><span>Non</span></button>
		// 							</div>
		// 						</div>
		// 					</div>
		// 				</div>
		// 			</div>
		// 		</div>
		// 		<div class="editable-input well">
		// 			<div class="title text-center"> <h3> Zone modifiable</h3> </div>
		// 			<form id="input" class></form>
		// 		</div>
		// 	</div>`;

        //     smallMenu.open(src);
        //     $("#openModalContent").removeClass("container");
        //     if (filter == "formation") {
        //         input = {
        //             trainer : {
        //                 inputType : "checkboxSimple",
        //                 params : {
        //                     onText : trad.yes,
        //                     offText : trad.no,
        //                     onLabel : "Formateurice",
        //                     offLabel : "Non formateurice"
        //                 },

        //                 label : "Formateur",
        //                 value : data.trainer
        //             },
        //             interestingTraining : {
        //                 inputType : "select",
        //                 label : "Intérêt pour les formations",
        //                 placeholder : "Intérêt pour les formations",
        //                 list : "interestingTraining",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 value : data.interestingTraining
        //             },
        //             presentationSession : {
        //                 inputType : "select",
        //                 label : "Participation visio",
        //                 placeholder : "Participation visio",
        //                 list : "presentationSession",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 value : data.presentationSession
        //             },
        //             ptlConsideredTraining : {
        //                 inputType : "select",
        //                 label : "Participation formation",
        //                 placeholder : "Participation formation",
        //                 list : "ptlConsideredTraining",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 value : data.ptlConsideredTraining
        //             },
        //             profileCandidate : {
        //                 inputType : "textarea",
        //                 label : "Candidature formation",
        //                 value : data.profileCandidate
        //             },
        //             competence : {
        //                 inputType : "select",
        //                 label : "Compétences acquises",
        //                 placeholder : "Compétences acquises",
        //                 list : "competence",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 value : data.competence
        //             },
        //             ptlComment : {
        //                 inputType : "textarea",
        //                 label : "Champ libre informations d’inscriptions",
        //                 value : data.ptlComment
        //             },
        //             id : {
        //                 inputType : "hidden",
        //                 value : id
        //             },
        //             filter : {
        //                 inputType : "hidden",
        //                 value : "formation"
        //             }
        //         }
        //     } else if (filter == "animation") {
        //         input = {
        //             // supportCercle : {
        //             //     inputType : "checkboxSimple",
        //             //     params : {
        //             //         onText : trad.yes,
        //             //         offText : trad.no,
        //             //         onLabel : "Membre du cercle des accompagnements",
        //             //         offLabel : "Non membre du cercle des accompagnements"
        //             //     },

        //             //     label : "Membre cercle",
        //             //     value : data.supportCercle
        //             // },
        //             // supportedStructures : {
        //             //     inputType : "finder",
        //             //     initMe : false,
        //             //     label : "Accompagnement structure",
        //             //     multiple : true,
        //             //     initType : [
        //             //         "organizations"
        //             //     ],
        //             //     openSearch : true
        //             // },
        //             // actionContributor : {
        //             //     inputType : "checkboxSimple",
        //             //     params : {
        //             //         onText : trad.yes,
        //             //         offText : trad.no,
        //             //         onLabel : "Contributeurices aux actions du réseau HdF",
        //             //         offLabel : "Non contributeurices aux actions du réseau HdF"

        //             //     },
        //             //     label : "Contributeur",
        //             //     value : data.actionContributor
        //             // },
        //             networkContributor : {
        //                 inputType : "select",
        //                 label : "Contributeurices du réseau Hdf",
        //                 placeholder : "Contributeurices du réseau HdF",
        //                 list : "networkContributor",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 value : data.networkContributor
        //             },
        //             mentor : {
        //                 inputType : "finder",
        //                 initMe : false,
        //                 multiple : true,
        //                 initType : [
        //                     "citoyens"
        //                 ],
        //                 openSearch : true,
        //                 label : "Parrain/Maraine"
        //             },
        //             referentTerritorialCercle : {
        //                 inputType : "checkboxSimple",
        //                 params : {
        //                     onText : trad.yes,
        //                     offText : trad.no,
        //                     onLabel : "Réferent(e) cercle territorial",
        //                     offLabel : "Non réferent(e) cercle territorial",
        //                 },

        //                 label : "Référent cercle territorial",
        //                 value : data.referentTerritorialCercle,
        //             },
        //             territorialCercle : {
        //                 inputType : "select",
        //                 list : "territorialCercle",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 label : "Membre d'un cercle territorial",
        //                 placeholder : "Si contributeur, membre d'un cercle territoriale ?",
        //                 value : data.territorialCercle
        //             },
        //             heldAction : {
        //                 inputType : "select",
        //                 list : "heldAction",
        //                 groupOptions : false,
        //                 groupSelected : false,
        //                 optionsValueAsKey : true,
        //                 noOrder : false,
        //                 select2 : {
        //                     multiple : true
        //                 },
        //                 label : "Projet/ action/ activités portée au sein du réseau",
        //                 placeholder : "Si contributeur, projet/action ou activité porté au sein du réseau ?",
        //                 value : data.heldAction
        //             },
        //             id : {
        //                 inputType : "hidden",
        //                 value : id
        //             },
        //             filter : {
        //                 inputType : "hidden",
        //                 value : "animation"
        //             }
        //         }
        //     }
        //     $.each(input, function (kk, vv) {
        //         if (vv.inputType == "finder") {
        //             var formValues = {};
        //             formValues[kk] = {};
        //             if (typeof data[kk] == "object") {
        //                 $.each(data[kk], function (k, v) {
        //                     formValues[kk][k] = {};
        //                     formValues[kk][k]["name"] = v.name;
        //                     formValues[kk][k]["type"] = v.type;
        //                 });
        //             }
        //             dyFObj.buildInputField("#tableauDeBord #input", kk, vv, formValues, null);
        //             dyFObj.initFieldOnload[kk + "Finder"]();
        //         } else {
        //             dyFObj.buildInputField("#tableauDeBord #input", kk, vv, null, null);

        //         }

        //     })
        //     if (isInterfaceAdmin) {
        //         $("#tableauDeBord #input").append(`<div class="btn-contact">
		// 			<button id="save-contact" data-id="${id}"class="btn btn-default text-bold pull-right letter-green">Valider <i class="fa fa-arrow-circle-right"></i></button>
		// 		</div>`)
        //     }
        //     $("#tableauDeBord #input #save-contact").click(function () {
        //         $("#input").submit();
        //     });
        //     dyFObj.bindDynFormEvents({
        //         formObj : {
        //             jsonSchema : {
        //                 properties : input
        //             }
        //         },
        //         formId : "#input",
        //         onSave : function () {
        //             let formData = $("#input").serializeFormJSON();
        //             formData = dyFObj.formatData(formData, "citoyens", "person");
        //             mylog.log("formData contact", formData);

        //             if (typeof data.tags == "undefined" || !notEmpty(data.tags)) {
        //                 data.tags = [];
        //             }

        //             var rolesUpdate = [];
        //             if (typeof data.links != "undefined" && typeof data.links.memberOf != "undefined" && typeof data.links.memberOf[costum.contextId] != "undefined" && typeof data.links.memberOf[costum.contextId].roles != "undefined") {
        //                 rolesUpdate = data.links.memberOf[costum.contextId].roles;
        //             }
        //             $.each(input, function (field, values) {
        //                 if (typeof formData[field] != "undefined" && values.inputType == "checkboxSimple") {
        //                     if (formData[field] == "true" || formData[field] == true) {
        //                         if ((data.tags).indexOf(values.params.onLabel) == -1) {
        //                             data.tags.push(values.params.onLabel);
        //                         }
        //                         if ((data.tags).indexOf(values.params.offLabel) > -1) {
        //                             delete data.tags[(data.tags).indexOf(values.params.offLabel)];
        //                         }
        //                         if ((rolesUpdate).indexOf(values.params.onLabel) == -1) {
        //                             rolesUpdate.push(values.params.onLabel);
        //                         }
        //                         if ((rolesUpdate).indexOf(values.params.offLabel) > -1) {
        //                             delete rolesUpdate[(rolesUpdate).indexOf(values.params.offLabel)];
        //                         }
        //                     } else {
        //                         if ((data.tags).indexOf(values.params.offLabel) == -1) {
        //                             data.tags.push(values.params.offLabel);
        //                         }
        //                         if ((data.tags).indexOf(values.params.onLabel) > -1) {
        //                             delete data.tags[(data.tags).indexOf(values.params.onLabel)];
        //                         }
        //                         if ((rolesUpdate).indexOf(values.params.offLabel) == -1) {
        //                             rolesUpdate.push(values.params.offLabel);
        //                         }
        //                         if ((rolesUpdate).indexOf(values.params.onLabel) > -1) {
        //                             delete rolesUpdate[(rolesUpdate).indexOf(values.params.onLabel)];
        //                         }
        //                     }
        //                 }
        //                 if (typeof data[field] != "undefined" && values.inputType == "select" && typeof formData[field] == "undefined") {
        //                     formData[field] = "";
        //                 }
        //             });
        //             mylog.log("roles", rolesUpdate);
        //             if (notEmpty(rolesUpdate)) {
        //                 if (typeof data.links != "undefined" && typeof data.links.memberOf != "undefined" &&
        //                     typeof data.links.memberOf[costum.contextId] != "undefined") {
        //                     ajaxPost(
        //                         null,
        //                         baseUrl + "/" + moduleId + "/link/removerole", {
        //                             contextId : costum.contextId,
        //                             contextType : costum.contextType,
        //                             childId : data._id.$id,
        //                             childType : type,
        //                             roles : rolesUpdate,
        //                             connectType : "members"
        //                         },
        //                         function (data) {

        //                         }
        //                     );
        //                 } else {
        //                     var afterConnectCallback = function () {};
        //                     links.connectAjax(costum.contextType, costum.contextId, id, type, "members", rolesUpdate, afterConnectCallback);
        //                 }
        //             }
        //             if (data.tags.indexOf("Contact" + costum.slug) == -1) {
        //                 data.tags.push("Contact" + costum.slug);
        //             }
        //             formData.tags = data.tags;
        //             if (typeof formData.mentor == "undefined" || (typeof formData.mentor != "undefined" && Object.keys(formData.mentor).length == 0)) {
        //                 formData.mentor = {};
        //             }
        //             if (typeof formData.supportedStructures == "undefined" || (typeof formData.supportedStructures != "undefined" && Object.keys(formData.supportedStructures).length == 0)) {
        //                 formData.supportedStructures = {};
        //             }
        //             var id = formData.id;
        //             var filt = formData.filter;
        //             var dataContact = {
        //                 id : formData.id,
        //                 collection : "citoyens",
        //                 path : "allToRoot",
        //                 value : formData
        //             }
        //             delete dataContact.value.id;
        //             delete dataContact.value.filter;
        //             delete dataContact.value.key;
        //             if (typeof dataContact.value == "undefined") {
        //                 toastr.error('value cannot be empty!');
        //             } else {
        //                 dataHelper.path2Value(dataContact, function (params) {
        //                     toastr.success("<?php echo Yii::t('cms', 'Edition success') ?>");
        //                     if (filt == "formation") {
        //                         data.trainer = formData.trainer;
        //                         data.interestingTraining = formData.interestingTraining;
        //                         data.presentationSession = formData.presentationSession;
        //                         data.ptlConsideredTraining = formData.ptlConsideredTraining;
        //                         data.profileCandidate = formData.profileCandidate;
        //                         data.competence = formData.competence;
        //                         data.ptlComment = formData.ptlComment;

        //                         aObj.setElt(data, id, type);
        //                         $("#" + type + id + " .trainer").html(aObj.values.trainer(data, id, type, aObj));
        //                         $("#" + type + id + " .interestingTraining").html(aObj.values.interestingTraining(data, id, type, aObj));
        //                         $("#" + type + id + " .presentationSession").html(aObj.values.presentationSession(data, id, type, aObj));
        //                         $("#" + type + id + " .ptlConsideredTraining").html(aObj.values.ptlConsideredTraining(data, id, type, aObj));
        //                         $("#" + type + id + " .profileCandidate").html(aObj.values.profileCandidate(data, id, type, aObj));
        //                         $("#" + type + id + " .competence").html(aObj.values.competence(data, id, type, aObj));
        //                         $("#" + type + id + " .ptlComment").html(aObj.values.ptlComment(data, id, type, aObj));
        //                     } else if (filt == "animation") {
        //                         data.supportCercle = formData.supportCercle;
        //                         data.actionContributor = formData.actionContributor;
        //                         data.networkContributor = formData.networkContributor;
        //                         data.heldAction = formData.heldAction;
        //                         data.territorialCercle = formData.territorialCercle;
        //                         data.referentTerritorialCercle = formData.referentTerritorialCercle;
        //                         data.mentor = formData.mentor;
        //                         data.supportedStructures = formData.supportedStructures;
        //                         aObj.setElt(data, id, type);
        //                         $("#" + type + id + " .supportCercle").html(aObj.values.supportCercle(data, id, type, aObj));
        //                         $("#" + type + id + " .actionContributor").html(aObj.values.actionContributor(data, id, type, aObj));
        //                         $("#" + type + id + " .networkContributor").html(aObj.values.networkContributor(data, id, type, aObj));
        //                         $("#" + type + id + " .heldAction").html(aObj.values.heldAction(data, id, type, aObj));
        //                         $("#" + type + id + " .territorialCercle").html(aObj.values.territorialCercle(data, id, type, aObj));
        //                         $("#" + type + id + " .referentTerritorialCercle").html(aObj.values.referentTerritorialCercle(data, id, type, aObj));
        //                         $("#" + type + id + " .mentor").html(aObj.values.mentor(data, id, type, aObj));
        //                         $("#" + type + id + " .supportedStructures").html(aObj.values.supportedStructures(data, id, type, aObj));
        //                     }
        //                     $('#openModal').modal('hide');
        //                 }, true);
        //             }

        //         }
        //     }, "");
        // })
        $("#" + aObj.container + " .editContact, #" + aObj.container + " .editTbContact").on("click", function () {

            var id = $(this).data("id");
            var collection = $(this).data("type");
            var editDynF = jQuery.extend(true, {}, typeObj.person.dynForm.jsonSchema, costum.typeObj.persons.dynFormCostum);
            var pagefilt=$(".filterGlobal.active").data("key");
            if(pagefilt=="animation"){
                costum.animationForm=$.extend(true,{},costum.minimumContactForm,costum.networkContactForm);
                editDynF.properties=costum.animationForm;
            }
    
    // $.extend(true,typeObj.person.dynForm.jsonSchema.properties,costum.typeObj.persons.dynFormCostum.beforeBuild.properties);
    //             jQuery.extend(true, {}, typeObj.person.dynForm.jsonSchema, costum.typeObj.persons.dynFormCostum);
            // }
            // mylog.log("editDynF",editDynF);
            // editDynF.onload = {
            //     actions : {
            //         hide : {
            //             "otherEmailtext" : 0,
            //             "similarLinkcustom" : 1
            //         },
            //         setTitle : "<i class='fa fa-user'></i> Ajouter un contact"
            //     }
            // };
            editDynF.afterBuild = function (data) {
                   
                // $("#ajaxFormModal .surnametext label").append(" (modifier si nécessaire)");
                // $("#ajaxFormModal .firstNametext label").append(" (modifier si nécessaire)");
                // $("#ajaxFormModal .emailtext label").append(" (saisie à l'inscription)");
                // $("#ajaxFormModal #email, #ajaxFormModal #name").attr("disabled", "disabled");
            };
            if (userId && userId == id || isInterfaceAdmin) {
                dyFObj.editElement(collection, id, null, editDynF, {}, "afterLoad");
            } else {
                toastr.error("Vous n'êtes pas autorisé a modifier cet contact");
            }
        });

    }

    var filterSearch = {};

    var contactCie = {
        init : function (key = "base", view = "table") {
            
            $('.btnCsvExport').css('display', '');
            $('.searchBars-container').empty();
            filterSearch = {};
            var paramsFilter = contactCie.getParams(key, view);
            // $(".searchBars-container").html("");
            searchObj.filters.initViews = function(fObj, pInit){
    			var str = '<div class="visible-xs">'+ fObj.filters.views.xs() +'</div>';
    			if(typeof pInit.subHeader != "undefined") {
    				var moreClass = pInit.subHeader.class ? pInit.subHeader.class : '';
    				str += `<div id="${pInit.subHeader.dom ? pInit.subHeader.dom : 'btnResetActiveFilters'}" class="col-xs-12 no-padding hidden-xs ${ moreClass }">`
    				if(typeof pInit.subHeader.views != 'undefined') {
    					$.each(pInit.subHeader.views, function(k, v) {
    						if(fObj.filters.views[v['view']]) {
    							str += fObj.filters.views[v['view']](k, v);
    						}
    					})
    				}
    				str += '</div>';
    			}
    			str+=`<div id="filterContainerInside" class="panel-group container-filters-menu menu-filters-xs ${pInit.layoutDirection == "horizontal" ? "container-filters-horizontal":""}">`;
    			str+='<div class="count-result-xs col-xs-12 visible-xs"></div>';
    			if(typeof pInit.filters != "undefined"){
    				$.each(pInit.filters,function(k,v){
    					//if(k != "text"){
    						var filterView="";
    						mylog.log("searchObj.initViews each", k,v);
    						if(typeof v.view != "undefined" && typeof fObj.filters.views[v.view] != "undefined"){
    							filterView = fObj.filters.views[v.view](k,v, fObj);
    						}else if(typeof fObj.filters.views[k] == "function"){
    							filterView = fObj.filters.views[k](k,v,fObj);
    						}
    						if(v.event == "buttonD3"){
    							var config = {
    								"view": "labelMap",
                        			"type" : "labelmap",
    								"name" : "Legende de la map",
    								"event" : "changeLabel",
    								"lists" : Object.fromEntries(Object.entries(pInit.filters).filter(([key, val]) => {
    									return (typeof val.view != "undefined" && val.view == "dropdownList" && typeof val.event != "undefined" && val.event != "buttonD3")
    								}))
    							}
    							filterView += fObj.filters.views[config.view](null, config, fObj)
    						}
    						if(typeof v.dom != "undefined")
    							$(v.dom).append(filterView);  
    						else
    							str+=filterView;
    					//}
    				});
    			}
    			str+='<div id="activeFilters" class="col-xs-12 no-padding" style="display:none;">';
    			str+=    '<div class="pull-left margin-right-5" style="height:30px;">';
    			str+=	    '<div class="activeFilter-label" style="display: flex;align-items: center;height: inherit;">';
    			str+=		    '<span><i class="fa fa-filter" style="color:#9fbd38;"></i> '+trad.activeFilters+' : </span>';
    			str+=	    '</div>';
    			str+=	'</div>';
    			str+='</div>';
    			if(typeof pInit.aapFooter != "undefined"){
    				var moreClass = pInit.aapFooter.class ? pInit.aapFooter.class : '';
    				str += `<div id="${pInit.aapFooter.subdom ? pInit.aapFooter.subdom : 'btnSaveActiveFilters'}" class="col-xs-12 no-padding ${ moreClass }">`
    				strMore = `<div id="${pInit.aapFooter.subdom ? pInit.aapFooter.subdom : 'btnSaveActiveFilters'}" class="col-xs-12 no-padding">`;
    				if(typeof pInit.aapFooter.views != 'undefined') {
    					$.each(pInit.aapFooter.views, function(k, v) {
    						if(fObj.filters.views[v['view']]) {
    							str += fObj.filters.views[v['view']](k, v);
    							strMore += fObj.filters.views[v['view']](k, v);
    							
    						}
    					})
    				}
    				fObj['pInit']['subdom'] = pInit.aapFooter.subdom ? '#'+pInit.aapFooter.subdom : "#btnSaveActiveFilters"
    				str += '</div>';
    				strMore += '</div>';
    				$("#centerBtnSaveFilter").append(strMore)
    			}
    			str += '</div>';
    			$(fObj.filters.dom).html(str);
    		};
            mylog.log("search.init filter contact",filterSearch);
            searchObj.admin.init = function(fObj){
                mylog.log("admin init","fObj.search.obj.text", fObj.search.obj);
                if(notEmpty(fObj.search.obj.text) && notEmpty(fObj.search.obj.textPath) && fObj.search.obj.textPath.indexOf("addressLocality")<0){
                    var searchKey=(fObj.search.obj.textPath=="username") ? "text" : fObj.search.obj.textPath;
                        fObj.filters.actions.text.launch(fObj, searchKey, null, ".searchBars-container", false);
                }
            };

            searchObj.filters.actions.text.launch = function(fObj, key, e=null, dom=null, launchIt=true){
                
                    mylog.log("text launch",fObj, key, e, dom);
					var htmlDom=(notNull(dom)) ? dom : fObj.filters.dom;
					searchValue=$(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").val();
					var textPath = $(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").data('text-path');
					var searchBy = $(htmlDom+" .searchBar-filters .main-search-bar[data-field='"+key+"']").data('search-by');
					// var launchIt = true;
                    var fieldKey=key;
                    // alert(key +" "+textPath+" "+fieldKey)
					if(key=="text" || textPath !=""){
                        // alert("here "+key);
                        
						if(textPath !="")
							key ="text";
						if(searchValue.charAt(0)=="#"){
							if((e.keyCode==13 || e==null) && searchValue.length>2 && !fObj.search.obj["tags"].includes(searchValue.replace("#",""))){
								const tagValue = searchValue.replace("#","")
								fObj.search.obj["tags"].push(tagValue);
								fObj.filters.manage.addActive(fObj,{type : "tags", value : tagValue, label : tagValue});
								launchIt = true;
							}else{
								launchIt = false;
							}
						}else{
                            mylog.log("else textpath text", key, fieldKey, searchValue);
							fObj.search.obj[key] = searchValue;
							if(textPath !="") fObj.search.obj.textPath = textPath;
							if(searchBy !="") fObj.search.obj.searchBy = searchBy;
                            // alert(key);
                            // if($('#activeFilters .filters-activate[data-field="'+fieldKey+'"]').length>0){
                                $('.main-search-bar[data-field!="'+fieldKey+'"]').val("");
                                $('#activeFilters .filters-activate[data-field!="'+key+'"]').remove()
                                $('#activeFilters .filters-activate[data-field="'+key+'"]').remove();
                            // }
                            fObj.filters.manage.addActive(fObj,{field: fieldKey,type : "text", value : searchValue, label : searchValue});
                            $('#activeFilters .filters-activate[data-field="'+fieldKey+'"]').on("click",function(){
                                // alert("del "+key);
                                $('.main-search-bar[data-field="'+fieldKey+'"]').val("");
                            });
                            // $(fObj.filters.dom+" .filters-activate").off().on("click",function(){
    						// 	$(".tooltip[role='tooltip']").remove();
    						// 	if(centerContent) {
    						// 		var that = this;
    						// 		var keyToRemove;
    						// 		$.each($(fObj.filters.dom+" .filters-activate"), function(k,v){
    						// 			if(that == this) {
    						// 				keyToRemove = k;
    						// 			}
    						// 		});
    						// 		$.each($(activeFiltersContainer).children(), function(k,v){
    						// 			if(k == keyToRemove) {
    						// 				$(this).fadeOut().remove();
    						// 			}
    						// 		})
    						// 	}
    						// 	$(this).fadeOut().remove();
    						// 	const filterBtnElm = $(fObj.filters.dom+` [data-type="${$(this).attr('data-type')}"][data-key="${$(this).attr('data-key')}"][${typeof $(this).attr('data-field') != 'undefined' ? 'data-field="'+$(this).attr('data-field')+'"' : 'data-value="'+$(this).attr('data-value')+'"'}]`);
    						// 	filterBtnElm.removeAttr('data-active-elem');
    						// 	filterBtnElm.removeClass('active');
    						// 	fObj.filters.manage.xsActiveFilter(fObj);
    						// 	fObj.filters.manage.removeActive(fObj, $(this));//$(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"), $(this).data("event"));
    						// 	coInterface.initHtmlPosition();
    						// });
    					}
					}else{
                        // alert("other "+key);
						if(notEmpty(searchValue))
							fObj.search.obj.filters[key] = searchValue;
						else if(typeof fObj.search.obj.filters[key] != "undefined")
							delete fObj.search.obj.filters[key];
					}
                    
					if(launchIt){
						fObj.results.map.changedfilters=true;
						fObj.filters.actions.text.spin(fObj, true, key);
						fObj.search.init(fObj);
					}
            	};
                
            searchObj.filters.views.searchExist = function(k, v){
				placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : trad.whatlookingfor;
				const icon = typeof v.icon != "undefined" ? v.icon : 'fa-arrow-circle-right';
				customClass = typeof v.customClass != "undefined" ? v.customClass : '';
				inputAlias = typeof v.inputAlias != "undefined" ? v.inputAlias : '';
				str='<div class="searchBar-filters pull-left '+ customClass +'">'+
	                '<input type="text" class="form-control pull-left text-center main-search-bar search-bar" data-field="'+k+'" data-text-path="'+(typeof v.field != "undefined" ? v.field : "" )+'" placeholder="'+placeholder+'">'+
	                '<span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="'+k+'" data-icon="'+icon+'" '+(inputAlias != '' ? 'data-alias="'+inputAlias+'"' : "")+' >'+
	                    '<i class="fa '+icon+'"></i>'+
	                '</span>'+
                    '<div class="dropdown similarLink '+customClass+'" style="display:none; z-index: 1;height:50vh;border: 1px solid #2b3b51;background-color: white;"><div class="listSameName '+customClass+'" style="overflow-y: scroll; height:inherit;display:none;" ></div>'+
                    // '<a href="javascript:;" style="display:none;" class="btn btn-primary margin-top-5" onclick="document.getElementsByClassName(\'similarLink\')[0].style.display=\'none\'; ">Cliquez ici si aucun élément dans la liste ne correspond à ma réponse</a>'+
                    '</div>'+
	            	'</div>';
	            	return str;
            };
            searchObj.filters.events.searchExist=function (fObj, domFilters, k, v){
                mylog.log("dom filter searchexist event",domFilters,  k, v);
                bindHideSimilarLink=function(dataField){  
                    mylog.log("similarlink field",dataField);
                    var waitSim = setInterval(function () {
                        
                        if ($(".similarLink."+dataField+" a").length>0) {
                            mylog.log("if loop waitsim");
                            var container = $(".similarLink."+dataField);
                            var structurePlaceholder=(dataField=="linkedThirdPlace") ? "Choisissez parmi les tiers-lieux référencés" : "Choisissez parmi les structures référencées";
                            $(".listSameName."+dataField+" div").first().html("<span>"+structurePlaceholder+" : </span>");
                            clearInterval(waitSim);
    
                        // if the target of the click isn't the container nor a descendant of the container
                            // if (!container.is(event.target) && container.has(event.target).length === 0) {
                            //     container.hide();
                            // }   
                        }
                        mylog.log("loop waitsim");
                    });        
                    // $(document).mouseup(function(event) {
                    //     var container = $(".similarLink."+dataField);
                    //     var structurePlaceholder=(dataField=="linkedThirdPlace") ? "Choisissez parmi les tiers-lieux référencés" : "Choisissez parmi les structures référencées";
                    //     $(".listSameName."+dataField+" div").first().html(structurePlaceholder);

                    // // if the target of the click isn't the container nor a descendant of the container
                    //     if (!container.is(event.target) && container.has(event.target).length === 0) {
                    //         container.hide();
                    //     }
                    // });
                };
               $(domFilters+" ."+k+" .main-search-bar").off().on("keyup",delay(function (e) {
                //    alert(k);
                   if(e.keyCode != 13){
                       dataField=$(this).data("field");
                       searchValue=$(domFilters+" .searchBar-filters .main-search-bar[data-field='"+dataField+"']").val();
                       costum[dataField]={
                            searchExist:function(type,id,name,slug,email){
                                mylog.log("filter search filters, search value",filterSearch.search.obj.filters, searchValue);
                                filterSearch.search.obj.filters["links.memberOf."+id]={'$exists':1};
                                filterSearch.search.init(filterSearch);
                                var container = $(".similarLink."+dataField);
                                container.hide();
                        
                            }    
                       };

                        if(searchValue<3){
                            mylog.log("inféreur à 3");
                            $(this).parent().find(".similarLink").hide();
                            if(typeof filterSearch.search.obj.filters!="undefined"){
                                $.each(filterSearch.search.obj.filters,function(kf,vf){
                                    if(kf!=="tags"){
                                        delete filterSearch.search.obj.filters[kf];
                                    }
                                    

                                });
                                $(this).parent().find(".similarLink").hide();                               
                                filterSearch.search.init(filterSearch);
                            }
                        }else{
                            mylog.log("supr à 3");
                            dyFObj.searchExist(searchValue,["organizations"],null,dataField);
                            
                        }
                       
                        bindHideSimilarLink(dataField);
                   }
               }, 750)).on("keyup", function(e){
                   if(e.keyCode != 13){
                       dataField=$(this).data("field");
                       searchValue=$(domFilters+" .searchBar-filters .main-search-bar[data-field='"+dataField+"']").val();
                       costum[dataField]={
                            searchExist:function(type,id,name,slug,email){
                                mylog.log("filter search filters, search value",filterSearch.search.obj.filters, searchValue);
                                filterSearch.search.obj.filters["links.memberOf."+id]={'$exists':1};
                                filterSearch.search.init(filterSearch);
                            }    
                       };

                        if(searchValue<3){
                            mylog.log("inféreur à 3");
                            $(this).parent().find(".similarLink").hide();
                            if(typeof filterSearch.search.obj.filters!="undefined"){
                                $.each(filterSearch.search.obj.filters,function(kf,vf){
                                    if(kf!=="tags"){
                                        delete filterSearch.search.obj.filters[kf];
                                    }
                                    

                                });
                                $(this).parent().find(".similarLink").hide();                               
                                filterSearch.search.init(filterSearch);
                            }
                        }else{
                            mylog.log("supr à 3");
                            dyFObj.searchExist(searchValue,["organizations"],null,dataField);
                            
                        }
                        bindHideSimilarLink(dataField);
                   }

               });
                

            
               

           };
          
           if(key=="formation"){
                urlCtrl.loadByHash("#listing-sessions");
                setTimeout(function(){}, 3000);
            }else{
                filterSearch = searchObj.init(paramsFilter);
                contactCie.extraFilterParams(filterSearch);
            }
            
            
                
            
            
            contactCie.initView();
            var strBtnAdd = ``;
            if (key == "formation") {
                
                strBtnAdd = '<a class="col-xs-12 col-sm-5 pull-right btn btn-default margin-left-10 margin-right-15 btn-add text-white bg-nightblue lbh"  href="#listing-formations"><i class="fa fa-list"></i> <span class="formation">Listing des formations</span></a>';
                // strBtnAdd += '<a class="col-xs-12 col-sm-5 margin-bottom-10 pull-right btn btn-default btn-add text-white bg-orange openSession" data-type="' + dataType + '"  href="javascript:;"><i class="fa fa-calendar"></i> <span class="formation">Sessions</span></a>';
            }else if(key=="animation"){
                dataType = "event";
                strBtnAdd = '<a data-form-type="'+dataType+'" style="overflow:hidden;text-overflow:ellipsis;" class="col-xs-12 col-sm-5 pull-right btn btn-default margin-left-10 text-white bg-blue btn-open-form"><i class="fa fa-calendar"></i> <span>Ajouter un événement</span></a>';
            }
           
            filterSearch.search.init(filterSearch);
            if ($(".btn-contact").length < 1) {
                strBtnViews=`<div class="btn-contact">
					<a href="javascript:;" class="btn btn-default change-views" data-views="table"><i class="fa fa-table"></i><label class="hidden-xs">Tableau</label></a>
					<a href="javascript:;" class="btn btn-default change-views" data-views="excel"><i class="fa fa-download"></i><label class="hidden-xs">Excel</label></a>
					<a href="javascript:;" class="btn btn-default btnCsvExport text-red" data-views="excel"><i class="fa fa-file-text-o"></i> <label class="hidden-xs">CSV</label></a>`;
					// <a href="javascript:;" class="btn btn-default change-views" data-views="annuaire"><i class="fa fa-th-list"></i><label class="hidden-xs">Annuaire</label></a>
				   
					
                strBtnViews+=`</div>`;
                $(".headerSearchleft").append(strBtnViews);
                $(".headerSearchright").append(strBtnAdd);
            }
            
            // <a class="pull-right btn btn-default margin-left-5" href="javascript:;"><i class="fa fa-tachometer"></i> <span class="dashboard">Tableau de bord</span></a>` +
            // $(".addContribElem").on("click", function () {
            //     urlCtrl.loadByHash("#tableau-formations");
            //     // var type = $(this).data("type");
            //     // smallMenu.openAjaxHTML(baseUrl + '/costum/lacompagniedestierslieux/gestionformation');


            // });
           
            $(".change-views").off().on("click", function () {
                var self = $(this);
                var view = self.data("views");
                $(".headerSearchIncommunity").html("");
                $(".headerSearchContainerAdmin").html("");
                if ($(".change-views").hasClass("active")) {
                    $(".change-views").removeClass("active")
                }
                if (!self.hasClass("active")) {
                    self.addClass("active");
                }
                // if(typeof filterSearch!="undefined" && typeof filterSearch.search!="undefined" && typeof filterSearch.search.obj!="undefined" ){
                //     var appliedFilters=filterSearch.search.obj;
                // mylog.log(filterSearch.search.obj,"before change views");
                // var appliedFilters=filterSearch.search.obj;
                    
                contactCie.init(pageFilter, view);
                //     var paramsSearch = contactCie.getParams(pageFilter, view);
                //     paramsSearch.defaults=jQuery.extend(paramsSearch.defaults, filterSearch.search.obj);
                //     mylog.log(filterSearch.search.obj,"after change views");
                //     mylog.log(paramsSearch,"paramsSearch change views");
                //     // searchFilt.search.obj=appliedFilters;
                //     filterSearch=searchObj.init(paramsSearch);
                //     filterSearch.search.init(filterSearch);
                // // }
                
                
                
            })
            $(".btnPdfContact").off().on("click", function () {
                var paramsSearch = searchObj.search.constructObjectAndUrl(filterSearch);
                paramsSearch = jQuery.extend(paramsSearch, filterSearch.search.obj);
                mylog.log("paramasss", paramsSearch)
                var url = baseUrl + '/costum/reseautierslieux/allcontactpdf?slug=' + costum.contextSlug + '&page=' + key;
                var paramsSearchpdf = "";
                // var paramsSearchpdflocality = "";
                // if (typeof paramsSearch.locality != "undefined"){
                // 	paramsSearchpdflocality = paramsSearch.locality;
                // }
                $.each(paramsSearch.filters, function (k, v) {
                    paramsSearchpdf = paramsSearch.filters[k];
                    url += '&' + k + '=' + paramsSearchpdf;
                })
                // if(paramsSearchpdflocality != ""){
                // 	url += '&locality='+paramsSearchpdflocality;
                // }
                paramsSearch.slug = costum.contextSlug;
                paramsSearch.page = key;
                window.open(url);


            })

        },
        getParams : function (key, view) {
            var filters = {}
            if (key == "base") {
                filters = {
                    text : {
                        placeholder : "Nom",
                        view : "text",
                        field : "username",
                        event : "text",
                        dom:".searchBars-container"
                    },
                    firstName : {
                        view : "text",
                        placeholder : "Prénom",
                        field : "firstName",
                        event : "text",
                        dom:".searchBars-container"
                    },
                    addressLocality : {
                        view : "text",
                        placeholder : "Ville",
                        field : "addressCie.addressLocality",
                        event : "text",
                        placeholder : "Ville",
                        dom:".searchBars-container"
                    },
                    scopeList : {
                        name : "Région",
                        params : {
                            countryCode : ["FR", "RE", "MQ", "GP", "GF", "YT"],
                            level : ["3"],
                            sortBy : "name"
                        }
                    },
                    linkedThirdPlace : {
                        view : "searchExist",
                        placeholder : "Tiers-Lieu",
                        onBlur : "linkedThirdPlace",
                        event : "searchExist",
                        // placeholder : "Tiers-",
                        dom:".searchBars-container",
                        customClass : "linkedThirdPlace"

                    },
                    otherLinkedStructure : {
                        view : "searchExist",
                        placeholder : "Autre structure",
                        onBlur : "otherLinkedStructure",
                        event : "searchExist",
                        // placeholder : "Tiers-",
                        dom:".searchBars-container",
                        customClass : "otherLinkedStructure"

                    },
                    expertise : {
                        view : "dropdownList",
                        type : "filters",
                        field : "expertise",
                        name : "Domaine d'expertise",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.expertise
                    },
                    adherent : {
                        view : "dropdownList",
                        type : "filters",
                        field : "adherent",
                        name : "Adhérent.e à la compagnie",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.adherent
                    },

                //     newsletterSubscriber : {
                //         view : "labelAndActionBtn",
				//         field :"type",
				//         label : "Inscrit à la newsletter",
                //         btnAction: {
                //             // actionIcon = sub.icon ? sub.icon : "fa-pencil";
				// 			action : "action",
				// 			classParent : "classParent"
                //         },
				//         typeList : "object",
				//         event : "filters",
				//         classList : "pull-left favElBtn btn", 
				//         list :  {
				//         	"Promesse de don" : "pledge",
				//         	"Don" : "donation"
				//         }
				// //     
                //     }
                    // newsletterSubscriber : {
                    //     view : "dropdownList",
                    //     type : "filters",
                    //     field : "newsletterSubscription",
                    //     name : "Inscrit.e à la newsletter",
                    //     event : "selectList",
                    //     keyValue : true,
                    //     list : costum.lists.newsletterSubscription
                    // }
                }
            } else if (key == "animation") {
                filters = {
                    text : {
                        placeholder : "nom",
                        view : "text",
                        field : "username",
                        event : "text",
                        dom:".searchBars-container"
                    },
                    firstName : {
                        view : "text",
                        placeholder : "Prénom",
                        field : "firstName",
                        dom:".searchBars-container"
                    },
                    addressLocality : {
                        view : "text",
                        placeholder : "Ville",
                        field : "addressCie.addressLocality",
                        event : "text",
                        placeholder : "Ville",
                        dom:".searchBars-container"
                    },
                    scopeList : {
                        name : "Région",
                        params : {
                            countryCode : ["FR", "RE", "MQ", "GP", "GF", "YT"],
                            level : ["3"],
                            sortBy : "name"
                        }
                    },
                    linkedThirdPlace : {
                        view : "searchExist",
                        placeholder : "Tiers-Lieu",
                        onBlur : "linkedThirdPlace",
                        event : "searchExist",
                        // placeholder : "Tiers-",
                        dom:".searchBars-container",
                        customClass : "linkedThirdPlace"

                    },
                    otherLinkedStructure : {
                        view : "searchExist",
                        placeholder : "Autre structure",
                        onBlur : "otherLinkedStructure",
                        event : "searchExist",
                        // placeholder : "Tiers-",
                        dom:".searchBars-container",
                        customClass : "otherLinkedStructure"

                    },
                    expertise : {
                        view : "dropdownList",
                        type : "filters",
                        field : "expertise",
                        name : "Domaine d'expertise",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.expertise
                    },
                    territorialCercle : {
                        view : "dropdownList",
                        type : "filters",
                        field : "territorialCercle",
                        name : "Cercle Territoriale",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.territorialCercle
                    }
                }
            } else if (key == "formation") {
                filters = {
                    text : {
                        placeholder : "nom",
                        view : "text",
                        field : "username",
                        event : "text",
                        dom:".searchBars-container"
                    },
                    firstName : {
                        view : "text",
                        placeholder : "Prénom",
                        field : "firstName",
                        dom:".searchBars-container"
                    },
                    addressLocality : {
                        view : "text",
                        placeholder : "Ville",
                        field : "addressCie.addressLocality",
                        event : "text",
                        placeholder : "Ville",
                        dom:".searchBars-container"
                    },
                    scopeList : {
                        name : "Région",
                        params : {
                            countryCode : ["FR", "RE", "MQ", "GP", "GF", "YT"],
                            level : ["3"],
                            sortBy : "name"
                        }
                    },
                    trainer : {
                        view : "dropdownList",
                        type : "filters",
                        field : "trainer",
                        name : "Formateur",
                        event : "selectList",
                        keyValue : false,
                        list : {
                            true : "Formateurice",
                            false : "Non formateurice"
                        }
                    },
                    interestingTraining : {
                        view : "dropdownList",
                        type : "filters",
                        field : "interestingTraining",
                        name : "Intérêt pour les formations",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.interestingTraining
                    },
                    // presentationSession : {
                    // 	view : "dropdownList",
                    // 	type : "filters",
                    // 	field : "presentationSession",
                    // 	name : "Participation visio formation",
                    // 	event : "selectList",
                    // 	keyValue : true,
                    // 	list : costum.lists.presentationSession
                    // },
                    ptlConsideredTraining : {
                        view : "dropdownList",
                        type : "filters",
                        field : "ptlConsideredTraining",
                        name : "Participation formations",
                        event : "selectList",
                        keyValue : true,
                        list : costum.lists.ptlConsideredTraining
                    }
                    // competence : {
                    //     view : "dropdownList",
                    //     type : "filters",
                    //     field : "competence",
                    //     name : "Compétences formations acquises",
                    //     event : "selectList",
                    //     keyValue : true,
                    //     list : costum.lists.competence
                    // }
                }
            }
            var field = ["address", "surname", "firstName", "telephone", "telephoneCie" , "email", "otherEmail", "emailCie", "status", "linkedThirdPlace", "functionThirdPlace", "otherLinkedStructure", "functionOtherStructure", "adherent", "keyMoment", "newsletterSubscription", "links"]
            if (['table', 'excel'].includes(view)) {
                var table = {
                    surname : {
                        class : "text-center",
                        name : "Nom"
                    },
                    firstName : {
                        class : "text-center",
                        name : "Prénom"
                    },
                    telephone : {
                        class : "text-center",
                        name : "Télephone"
                    },
                    email : {
                        class : "text-center",
                        name : "Email"
                    },
                    status : {
                        class : "text-center",
                        name : "Statut"
                    },
                    linkedThirdPlace : {
                        class : "text-center",
                        name : "Tiers-lieu"
                    },
                    functionThirdPlace : {
                        class : "text-center",
                        name : "Fonction dans le tiers-lieu"
                    },
                    otherLinkedStructure : {
                        class : "text-center",
                        name : "Autre structure"
                    },
                    functionOtherStructure : {
                        class : "text-center",
                        name : "Fonction dans l'autre structure"
                    },
                    adherent : {
                        class : "text-center",
                        name : "Adhérent"
                    },
                    keyMoment : {
                        class : "text-center",
                        name : "Participation temps forts"
                    },
                    newsletterSubscription : {
                        class : "col-xs-1 text-left",
                        name : "Inscrit.e à la newsletter"
                    },
                }
                var unsetFieldCol = ["status", "linkedThirdPlace", "function", "adherent", "keyMoment", "newsletterSubscription"];


                if (key == "formation") {
                    $.each(unsetFieldCol, function (i, f) {
                        delete table[f];
                    });
                    field.push("trainer", "interestingTraining", "ptlConsideredTraining", "presentationSession", "profileCandidate", "competence", "ptlComment")
                    table["trainer"] = {
                        class : "text-center",
                        name : "Formateur"
                    }
                    table["interestingTraining"] = {
                        class : "text-center",
                        name : "Intérêt pour les formations"
                    }
                    table["presentationSession"] = {
                        class : "text-center",
                        name : "Participation visio formation"
                    }
                    table["ptlConsideredTraining"] = {
                        class : "text-center",
                        name : "participation formations"
                    }
                    table["profileCandidate"] = {
                        class : "text-center",
                        name : "candidat formation"
                    }
                    table["competence"] = {
                        class : "text-center",
                        name : "Compétences formations acquises"
                    }
                    table["ptlComment"] = {
                        class : "text-center",
                        name : "Champ libre informations d’inscription"
                    }
                } else if (key == "animation") {
                    $.each(unsetFieldCol, function (i, f) {
                        delete table[f];
                    });
                    field.push("supportCercle", "supportedStructures", "actionContributor", "networkContributor", "mentor", "heldAction", "territorialCercle", "referentTerritorialCercle")
                    // table["supportCercle"] = {
                    //     class : "text-center",
                    //     name : "membre cercle"
                    // }
                    // table["supportedStructures"] = {
                    //     class : "text-center",
                    //     name : "accompagnement structure"
                    // }
                    // table["actionContributor"] = {
                    //     class : "text-center",
                    //     name : "contributeur"
                    // }
                    delete table["otherLinkedStructure"];
                    delete table["functionOtherStructure"];
                    table["territorialCercle"] = {
                        class : "text-center",
                        name : "membre d’un cercle territorial"
                    }
                    table["referentTerritorialCercle"] = {
                        class : "text-center",
                        name : "référent cercle territorial"
                    }
                    table["networkContributor"] = {
                        class : "text-center",
                        name : "Contributeurices du réseau HdF"
                    }
                    table["mentor"] = {
                        class : "text-center",
                        name : "parrain/marraine"
                    }
                    table["heldAction"] = {
                        class : "text-center",
                        name : "projet/ action / activité portée au sein du réseau"
                    }
                }
                tableHeaders = table;
                var data = {
                    paramsFilter : {
                        defaults : {
                            types : ["citoyens"],
                            sortBy : {
                                "name" : 1
                            }
                        },
                    },
                    table : table
                }
                if (userId && (pageFilter == "base" || pageFilter=="animation") ) {
                    data.actions = {};
                    data.actions = {
                        edit : true
                    }
                }
            }
            var isExcelView = view === 'excel';
            var paramsFilter = {
                container : "#filterContainer",
                options : {
                    tags : {
                        verb : '$all'
                    }
                },
                results : {
                    renderView : 'contactCie.' + (isExcelView ? 'HandsontableView' : 'views'),
                    smartGrid : true,
                    unique : isExcelView,
                    map : {
                        active : false,
                        show : false
                    }
                },
                defaults : {
                    indexStep : 25,
                    types : ["citoyens"],
                    fields : field,
                    sortBy : {
                        name : 1
                    },
                    filters : {
                        
                    },
                    notSourceKey : true
                },
                header : {
                    // dom : ".headerSearchIncommunity",
                    //dom : ".headerSearchContainerAdmin",
                    options : {
                        left : {
                            classes : 'col-xs-12 col-sm-6 elipsis no-padding',
                            group : {
                                count : true
                            }
                        },
                        right : {
                            classes : 'col-xs-12 col-sm-6 text-right no-padding',
                            group : {
                                add : true
                            }
                        }
                    }
                },
                filters : filters
            }
            // paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};

            paramsFilter.defaults.filters['$or']={};
            // paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId+".isInviting"]={};
            paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId]={'$exists':1};
            paramsFilter.defaults.filters['$or']["tags"]={'$in':["Contact" + costum.contextSlug]};
            paramsFilter.defaults.filters['$or']["tagsCie"]={'$in':["Contact" + costum.contextSlug]};
            paramsFilter.defaults.filters['$or']["source.keys"]={'$in':[costum.contextSlug]};

            // paramsFilter.defaults.filters['$or']["links.memberOf."+costum.contextId+".isInviting"]=true;
            // paramsFilter.defaults.filters['$ne']["links.memberOf."+costum.contextId+".isInviting"]=true;
            // paramsFilter.defaults.filters['$or']["tags"] = "Contact" + costum.contextSlug;
            // ["links.memberOf."+costum.contextId]={'$exists':true};
            // paramsFilter.defaults["community"]=true;
            if (view == "table") {
                paramsFilter.loadEvent = {}
                paramsFilter.loadEvent = {
                    default : "admin",
                    options : {
                        results : {},
                        initType : ["citoyens"],
                        panelAdmin : data
                    }
                };
                paramsFilter.header.dom = ".headerSearchContainerAdmin"
            } else if (['excel', 'annuaire'].includes(view)) {
                $("#adminDirectory").html("");
                paramsFilter.defaults.indexStep=0;
                paramsFilter.header.dom = ".headerSearchIncommunity"
            }
            return paramsFilter;
        },
        extraFilterParams : function(filterSearch){
            filterSearch.admin.event=function(fObj){
            mylog.log("gestion formation searchObj.admin.event");
			if(fObj.search.obj.indexStep > 0){
				if(fObj.results.numberOfResults%fObj.search.obj.indexStep == 0){
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep);
				}else{
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep)+1;
				}
			    $('.pageTable').pagination({
			        items: numberPage,
			        itemOnPage: fObj.search.obj.indexStep,
			        currentPage: (fObj.search.obj.nbPage+1),
			        hrefTextPrefix:"?page=",
			        cssStyle: 'light-theme',
			        prevText: '<<',
			        nextText: '>>',
			        onInit: function () {

			        },
			        onPageClick: function (page, evt) {
			        	coInterface.simpleScroll(0, 10);
			        	fObj.search.obj.nbPage=(page-1);
			            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
			            fObj.search.start(fObj);
			        }
			    });
			}
            };
        },
        initView : function () {
            $("#tableauDeBord #input").html("")
            $("#tableauDeBord .editable-input .title").css("display", "none")
            $("#tableauDeBord .editable-input").removeClass("well")
            $("#tableauDeBord #simple-input").html("")
        },
        views : function (params) {
            mylog.log("dedede", params);
            var str = '';
            str += `<div id="entity_${params.collection}_${params.id}" class="detailContactCard searchEntityContainer smartgrid-slide-element mt-20 col-lg-4 col-md-4 col-sm-6 col-xs-12 " >
					<div class="  card detailContact">
						<div  class=" col-lg-4 col-md-4 col-sm-3 col-xs-3 card-img-top no-padding">
							<div>
								
								<a href="javascript:;" class="editTbContact" data-id="${params.id}" data-filter="${pageFilter}" data-type="${params.collection}" >
									<div class="content-img">
										${params.imageProfilHtml}
									</div>
								</a>
							</div>
						</div>
						<div class="card-body no-padding  col-lg-8 col-md-8 col-sm-9 col-xs-9">
							<div class="detail-box ">
								<a href="javascript:;" class="editTbContact" data-id="${params.id}" data-filter="${pageFilter}" data-type="${params.collection}">
									<h4>`
            if (typeof params.surname != "undefined") {
                str += params.surname + " ";
            }
            if (typeof params.firstName != "undefined") {
                str += params.firstName + " ";
            }
            if (typeof params.surname == "undefined" && typeof params.firstName == "undefined") {
                str += params.name;
            }
            str += `</h4>
								</a> `
            str += `<div class="entityLocality">`
            if (notNull(params.localityHtml)) {
                str += `<span> ${params.localityHtml}</span>`;
            }
            str += `</div>`;
            if (typeof params.tagsHtml != "undefined") {
                str += `<ul class="tag-list no-padding">${params.tagsHtml} </ul>`;
            }
            str += `</div>
						</div>
					</div>
				</div>`;
            return str;
        },
        HandsontableView(params) {
            setTimeout(function () {
                function safeHtmlRenderer(instance, td, row, col, prop, value, cellProperties) {
                    var safeValue = !['number', 'string'].includes(typeof value) || value === null ? '' : value;
                    td.innerHTML = safeValue.replace(/&amp;/g, '&');
                    return td;
                }

                var hotDom = document.getElementById('excel-view');
                var columns = [];
                $.each(tableHeaders, function (id, tableDefinition) {
                    columns.push({
                        data : id,
                        title : tableDefinition.name,
                        renderer : safeHtmlRenderer
                    });
                });
				var hotData = [];
				var column_includes = ['referentTerritorialCercle', 'supportCercle', 'actionContributor', 'newsletterSubscription', 'trainer'];
				$.each(params, function (_, person) {
					hot_line = {};
					$.each(columns, function (index, value) {
						var populate = person[value.data] ? person[value.data] : '';
                        mylog.log('loop excel',populate,value.data);
                        if(value.data=="email"){
                            populate=(typeof person.emailCie!="undefined") ? person.emailCie : ((typeof person.otherEmail!="undefined") ? person.otherEmail : populate);
                        }
                        if(value.data=="telephone"){
                            populate=(typeof person.telephoneCie != "undefined") ? person.telephoneCie : ((typeof person.telephone != "undefined" && notEmpty(person.telephone.mobile)) ? person.telephone.mobile[0] : ((typeof person.telephone != "undefined" && notEmpty(person.telephone.fixe)) ? person.telephone.fixe[0] : populate));
                        }
                         
                        populate = populate !== '' && populate && column_includes.includes(value.data) ? (JSON.parse(populate) ? trad.yes : trad.no) : populate;
						if (populate && ['supportedStructures', 'mentor', 'linkedThirdPlace','otherLinkedStructure'].includes(value.data)) {
							populate = [];
							$.each(person[value.data], function (_, structure) {
								populate.push('- ' + structure.name);
							});
							populate = populate.join('\n');
						}
						hot_line[value.data] = populate;
					});
					hotData.push(hot_line);
				});
                var hotOptions = {
                    columns,
                    data : hotData,
                    rowHeaders : false,
                    colHeaders : true,
                    manualColumnMove : true,
                    manualColumnResize : true,
                    manualRowResize : true,
                    editor : false,
                    fillHandle : false,
                    autoRowSize : true,
                    height : '500px',
                    licenseKey : 'non-commercial-and-evaluation',
                }
                hot = new Handsontable(hotDom, hotOptions);
                hot.addHook('afterRender', function () {
                    $('#dropdown_search').css('height', '100%');
                });

                $('.btnCsvExport').css('display', 'inline-block')
					.off('click')
					.on('click', function (event) {
						event.preventDefault();
						var exportPlugin = hot.getPlugin('exportFile');
						exportPlugin.downloadFile('csv', {
							bom : true,
							columnDelimiter : ';',
							columnHeaders : true,
							exportHiddenColumns : false,
							exportHiddenRows : false,
							fileExtension : 'csv',
							filename : 'Contact ' + moment().format(' YYYY-MM-DD_HH-mm-ss'),
							mimeType : 'text/csv',
							rowDelimiter : '\r\n',
							rowHeaders : false
						});
					});
            }, 500);
            return '<div id="excel-view" style="margin-top:10px;width:100%"></div>';
        }
    }
    jQuery(document).ready(function () {
        $("#menuBottom").hide();
        adminDirectory.initView = function(){
    		mylog.log("adminDirectory.initView");
    		var aObj = this;
    		var str = "";
    		if(typeof aObj.panelAdmin.title != "undefined" && aObj.panelAdmin.title != null)
    			str += '<div class="col-xs-12 padding-10 text-center"><h2>'+aObj.panelAdmin.title+'</h2></div>';
    
    		str += '<div class="headerSearchContainerAdmin no-padding col-xs-12"></div>';
    		str += '<div class="no-padding col-xs-12 searchObjCSS" id="filters-nav-admin"></div>';
    		str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
    		
    		str +=	'<div class="panel-body">'+
    			'<div>'+
    				'<table style="" class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin">'+
    					'<thead>'+
    						'<tr id="headerTable">'+
    						'</tr>'+
    					'</thead>'+
    					// '<tbody class="directoryLines">'+
    						
    					// '</tbody>'+
    				'</table>'+
    			'</div>'+
    		'</div>'+
    		'<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';
    
    		$("#"+this.container).html(str);
    	};
    adminDirectory.initViewTable = function(aObj){
		//var aObj = this;
		mylog.log("adminDirectory.initViewTable", aObj.container);
		$("#"+aObj.container+" #panelAdmin thead").nextAll().remove();
		mylog.log("adminDirectory.initViewTable !", aObj.results);
		var sumColumn="";
		var posSumColumn = 0;
		var sum=null;
		var tot="";
		var totalColumn=0;

		//vérifie si une entrée du tableau contribut l'attribut sum pour faire la somme
		$.each(aObj.panelAdmin.table, function(k, v){
			mylog.log("yiyi",Object.keys(v));
			if($.inArray("sum",Object.keys(v))>-1){
				sumColumn=k;
				posSumColumn=Object.keys(aObj.panelAdmin.table).indexOf(k);
			}
		});

		if(Object.keys(aObj.results).length > 0){
			$.each(aObj.results ,function(key, values){
				mylog.log("adminDirectory.initViewTable !!! ", key, values);
				var entry = aObj.buildDirectoryLine( values, values.collection);
				// var selector=(entry.match(/tbody/).length>0) ? "thead tbody": "thead"; 
				$("#"+aObj.container+" #panelAdmin").append(entry);
				// Addition des valeurs de la colonne
				if(sumColumn){
					if(typeof values[sumColumn]!="Number")
						sum = Number(values[sumColumn]);
					totalColumn = totalColumn + sum ;
				}	
			});	
			//Ligne supplémentaire pour faire la somme
			if(sumColumn){
				tot += '<tr style="height: 20px;"></tr>';
				tot += '<tr style="border-top:solid;border-bottom:solid;"><td style="border-left:none !important;border-right:none !important;font-weight:800;">TOTAL</td</tr>';


				for(var p=0;p<posSumColumn-1;p++){
					tot+='<td style="border-left:none !important;border-right:none !important;"></td>';
				}
				tot += '<td style="border-left:none !important;border-right:none !important;">'+totalColumn+'</td>';
				$("#"+aObj.container+" #panelAdmin .directoryLines").append(tot);
			}		
		}	

		aObj.bindAdminBtnEvents(aObj);
	};
    adminDirectory.columTable = function(e, id, collection){
		mylog.log("adminDirectory.columTable ", id, collection);
		var str = "" ;
		var aObj = this ;
		if(	typeof aObj.panelAdmin.table != "undefined" && 
			aObj.panelAdmin.table != null ){

			$.each(aObj.panelAdmin.table, function(key, value){
				if(key != "actions"){
					str += '<td class="center '+key+'">';
					str += '<div class="content-td">';
                    if(typeof aObj.values[key] != "undefined" && value === "true") {
						str += aObj.values[key](e, id, collection, aObj);
					} else if(typeof aObj.values[key] != "undefined") {
						str += aObj.values[key](e, id, collection, aObj, value);
					}
                    str+='</div>';
					str += '</td>';
				}
			});
		}
		return str ;
	};
	// adminDirectory.buildDirectoryLine = function( e, collection, icon ){
	// 	mylog.log("adminDirectory.buildDirectoryLine",  e, collection, icon);
	// 	var aObj = this;
	// 	var strHTML="";
	// 	if( typeof e._id == "undefined" || 
	// 		( (typeof e.name == "undefined" || e.name == "") && 
	// 		  (e.text == "undefined" || e.text == "") ) )
	// 		return strHTML;
	// 	var actions = "";
	// 	var classes = "";
	// 	var id = e._id.$id;
	// 	var status=[];
	// 	strHTML += '<tbody><tr id="'+e.collection+id+'" class="'+e.collection+' line">';
	// 		strHTML += aObj.columTable(e, id, e.collection);
	// 		var actionsStr = aObj.actions.init(e, id, e.collection, aObj);
	// 		mylog.log("adminDirectory.buildDirectoryLine actionsStr",  actionsStr);
	// 		if(actionsStr != ""){
	// 			aObj.actionsStr = true;
	// 			mylog.log("adminDirectory.buildDirectoryLine aObj.actionsStr",  aObj.actionsStr);
	// 			strHTML += 	'<td class="center">'+ 
	// 							'<div class="btn-group">'+
	// 							actionsStr +
	// 							'</div>'+
	// 						'</td>';
	// 		}
	// 	strHTML += '</tr></tbody>';
	// 	return strHTML;
	// };
        
        $(".filterGlobal").click(function () {
            $(".headerSearchIncommunity").html("");
            $(".headerSearchContainerAdmin").html("");
            var key = $(this).data("key");
            pageFilter = key;
            if ($(".filterGlobal").hasClass("active")) {
                $(".filterGlobal").removeClass("active")
            }
            if (!$(this).hasClass("active")) {
                $(this).addClass("active")
            }
            contactCie.initView();
            contactCie.init(pageFilter);
            coInterface.bindButtonOpenForm();
        })
        $(".addContribElem").on("click", function () {
            var type = $(this).data("type");
            smallMenu.openAjaxHTML(baseUrl + '/costum/lacompagniedestierslieux/gestionformation');
        });
        contactCie.init("base");
        $("#mapContent").hide();
        coInterface.bindButtonOpenForm();
    });
</script>