<?php
    $linstInputBase = [    
        "surname" => "Nom",
        "firstName" =>  "Prénom",
        "telephone" => "Télephone",
        "email" => "Email",
        "status" =>"Statut",
        "typeLinkedStructure" => "Rattachement structure",
        "function" => "Fonction dans la structure",
        "adherent" =>"Adhérent",
        "keyMoment" =>  "Participation temps forts",
        "newsletterSubscription" =>"Intéressé par la newsletters"
    ];
    $linstInputBaseFormation = [
        "surname" => "Nom",
        "firstName" =>  "Prénom",
        "telephone" => "Télephone",
        "email" => "Email",
        "trainer" => "Formateur",
        "interestingTraining" =>  "Intérêt pour les formations",
        "presentationSession" => "Participation visio",
        "ptlConsideredTraining" => "Participation formation",
        "profileCandidate" => "Candidature formation",
        "competence" =>  "Compétences acquises",
        "ptlComment" => "Champ libre informations d’inscriptions"
    ];
    $linstInputBaseAnimation = [
        "surname" => "Nom",
        "firstName" =>  "Prénom",
        "telephone" => "Télephone",
        "email" => "Email",
        "supportCercle" => "Membre cercle",
        "supportedStructures" => "Accompagnement structure",
        "actionContributor" =>  "Contributeur",
        "networkContributor" => "Contributeurices du réseau Hdf",
        "mentor" => "Parrain/Maraine",
        "referentTerritorialCercle" => "Référent cercle territorial",
        "territorialCercle" => "Membre d'un cercle territorial",
        "heldAction" => "Projet/ action/ activités portée au sein du réseau"
    ];
?>
<style>
    th {
        font-size : 14px;
        background-color : #939ea9;
        font-weight : bold;
    }
    td {
        font-size : 12px;
        text-align : left;
    }
    .title {
        text-align : center;
        font-size : 30px;
        font-weight : bold;
        text-decoration : underline;
    }
    .filter{
        font-size : 18px;
        font-weight : bold;

    }
</style>  

<p class="title">Liste des contacts</p>

<?php 
    $filter = [];
    foreach($query as $k => $v){
        if($k == "expertise") {
            $filter["expertise"] = [];
            $filter["expertise"]["label"] = "Domaine d'expertise ";
            $filter["expertise"]["value"] = $v['$in'];
        }
        if($k == "territorialCercle"){
            $filter["territorialCercle"] = [];
            $filter["territorialCercle"]["label"] = "Cercle Territoriale ";
            $filter["territorialCercle"]["value"] = $v['$in'];

        }
        if($k == "trainer"){
            $filter["trainer"] = [];
            $filter["trainer"]["label"] = "Formateur";
            foreach($v['$in'] as $ktr => $vtr){
                $filter["trainer"]["value"] = [];
                if($vtr === true || $vtr === "true")
                    $filter["trainer"]["value"][$ktr] = "Formateurice";
                if($vtr === false || $vtr === "false")
                    $filter["trainer"]["value"][$ktr] = "Non formateurice";
            }
        }
        if($k == "interestingTraining"){
            $filter["interestingTraining"] = [];
            $filter["interestingTraining"]["label"] = "Intérêt pour les formations ";
            $filter["interestingTraining"]["value"] = $v['$in'];

        }
        if($k == "ptlConsideredTraining"){
            $filter["ptlConsideredTraining"] = [];
            $filter["ptlConsideredTraining"]["label"] = "Participation formations ";
            $filter["ptlConsideredTraining"]["value"] = $v['$in'];

        }
        if($k == "competence"){
            $filter["competence"] = [];
            $filter["competence"]["label"] = "Compétences formations acquises ";
            $filter["competence"]["value"] = $v['$in'];

        }
    }
    foreach($filter as $kf => $vf){ ?>
        <p ><span class="filter"><?= $vf["label"] ?> : </span>
        <?php foreach($vf["value"] as $ke => $ve){ 
            echo  "<span>" . $ve .", </span>";   ?>        
        <?php } ?></p>
    <?php } 
   
?>


<?php if($page == "base"){?>
    <table border="1"  cellpadding="2">
        <tr>
            <?php foreach($linstInputBase as $kl => $vl){ ?>
                <th><?php echo $vl ?> </th>
            <?php } ?>
        </tr>
        <?php foreach($contactList as $kContact => $vContact){ ?>    
            <tr> 
                <?php foreach($linstInputBase as $kl => $vl){ ?>
                    <td>
                        <?php 
                            if(isset($vContact[$kl]) && is_string($vContact[$kl] ) && $kl != "newsletterSubscription") 
                                echo $vContact[$kl];    
                            if($kl == "newsletterSubscription" && isset($vContact[$kl])){                    
                                if($vContact["newsletterSubscription" ] === "true" || $vContact["newsletterSubscription" ] === true){
                                    echo Yii::t("common","Yes"); 
                                }
                                elseif( $vContact["newsletterSubscription" ] === "false" || $vContact["newsletterSubscription" ] === false){
                                    echo Yii::t("common","No") ;
                                }
                            }
                        ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
<?php }else if($page == "formation") {?>
    <table border="1"  cellpadding="2">
        <tr>
            <?php foreach($linstInputBaseFormation as $kl => $vl){ ?>
                <th><?php echo $vl ?> </th>
            <?php } ?>
        </tr>
        <?php foreach($contactList as $kContact => $vContact){ ?>    
            <tr> 
                <?php foreach($linstInputBaseFormation as $kl => $vl){ ?>
                    <td><?php if(isset($vContact[$kl])){
                        if(is_string($vContact[$kl] )) {
                            if($kl == "trainer"){
                                if($kl == "trainer" && isset($vContact[$kl])){                    
                                    if($vContact["trainer" ] === "true" || $vContact["trainer" ] === true){
                                        echo Yii::t("common","Yes"); 
                                    }
                                    elseif( $vContact["trainer" ] === "false" || $vContact["trainer" ] === false){
                                        echo Yii::t("common","No") ;
                                    }
                                }
                            } else
                                echo $vContact[$kl];   
                        }
                        else if(is_array($vContact[$kl])){
                            foreach($vContact[$kl] as $ka => $va)
                                echo $va ."<br>";
                        }
                    }?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
<?php } else if($page == "animation") {?>
    <table border="1"  cellpadding="2">
        <tr>
            <?php foreach($linstInputBaseAnimation as $kl => $vl){ ?>
                <th><?php echo $vl ?> </th>
            <?php } ?>
        </tr>
        <?php foreach($contactList as $kContact => $vContact){ ?>    
            <tr> 
                <?php foreach($linstInputBaseAnimation as $kl => $vl){ ?>
                    <td><?php if(isset($vContact[$kl])){
                        if(is_string($vContact[$kl] )) {
                            if(($kl == "supportCercle" || $kl == "actionContributor" || $kl == "referentTerritorialCercle") && isset($vContact[$kl])){ 
                                if($vContact[$kl] === "true" || $vContact[$kl] === true){
                                    echo Yii::t("common","Yes"); 
                                }
                                elseif( $vContact[$kl] === "false" || $vContact[$kl] === false){
                                    echo Yii::t("common","No") ;
                                }
                            }else
                                echo $vContact[$kl];   
                        
                        } else if(is_array($vContact[$kl])){
                            foreach($vContact[$kl] as $ka => $va){
                                if(is_string($va)){
                                    echo $va ."<br>";
                                }else {
                                    if(isset($va["name"]))
                                        echo $va["name"]."<br>";                                    
                                }
                            }
                        }
                    }?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
<?php }?>
