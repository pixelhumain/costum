<style type="text/css">
	/*#filters-nav{
		right: 0px;
	    text-align: center;
	    padding-left: 0px;
	    box-shadow: none;
	    border-bottom: none;
	}
	#filters-nav > *{
		display: inline-grid;
		margin-left: 5px;
    	margin-right: 5px;
	}
	#filters-nav .searchBar-filters{
	    width: 100%;
	    padding-left: 15%;
	    padding-right: 15%;
	    margin-bottom: 15px;
	    display: inline-flex;
	}
	#activeFilters{
		margin:0px; 
		display: block;
		text-align: left;
	}*/

	.horizontalList .btn-filters-select{
		vertical-align:middle;
		margin-bottom: 4px;
		min-width:175px;
		float: left;
	}

	/*.horizontalList{
		text-align:left;
	}*/

	.horizontalList span{
		font-size: 16px;
		/*font-variant: small-caps;*/
		/*line-height:3;*/
	}

	#filters-nav{
		background-color: white !important;
	}

	.fc-unthemed .fc-popover {
	   	position: absolute;
	    top: 30px !important;
	    left: 40% !important;
	}
</style>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	
	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var eventList=<?php echo json_encode(Event::$types); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	urlData : baseUrl+"/co2/search/agenda",
	 	loadEvent: {
	 		default : "agenda"
	 	},
	 	defaults : {
 			types : ["events"],
 			 filters : {}
 		}, 

 		header : {
	 		options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true
					}
				},
				right : {
					classes : 'col-xs-4 text-right no-padding',
					group : {
						calendar : true,
						map : true
					}
				}
			}
	 	},
 		results : {
 			renderView : "directory.eventPanelHtml"
 		},
	 	filters : {
	 		text : true,
	 		scope : true,
	 		type : {
	 			name : trad.category,
	 			view : "dropdownList",
	 			event : "filters",
	 			type : "type",
	 			keyValue : false,
	 			list : eventList
	 		},
	 		source : {
	 			view : "horizontalList",
	 			type : "filters",
	 			name : "Sources (à filtrer):",
	 			action : "filters",
	 			typeList : "object",
	 			event : "inArray",
	 			list :  {
	 			    "reseau" : {
	 			        label: "Le réseau",
	 			        field : "source.keys",
	 			        value : costum.slug,
	 			        key : costum.slug,
	 			        keyValue : false
	 			    },
	 			    "members" : {
	 			        label: "Les tiers-lieux membres",
	 			        field : "reference.costum",
	 			        value : costum.slug,
	 			        key : costum.slug,
	 			        keyButton : false
	 			    }
	 		    }
	 		} 
	 		// 	 		source : {
	 		// 	 		  view : "dropdownList",
	 		// 	 		  //type : "guess",
	 		// 	 		  name : "Source",
	 		// 	 		  //event : "selectList",
	 		// 	 		  type: "source.key.bourgogne",
	 		// 	 		  //action : "filters",
	 		//               event : "exists",
	 		// 	 		  keyValue : false,
	 		// 	 		  list : {
	 		//                 1 : "réseau",
	 		//                 0 : "Membre"
	 		// 	 		  }
	 		// 	 		}	 		
	 	}
	};
	if(typeof  appConfig.calendarOptions != "undefined"){
		paramsFilter.agenda={
			options : {
				calendarConfig : appConfig.calendarOptions 
			}
		}
	}
	// if(notNull(costum) && typeof costum.filters != "undefined" && typeof costum.filters.sourceKey != "undefined")
	// 	paramsFilter.defaults.sourceKey=costum.slug;

	if(typeof appConfig.filters != "undefined"){
		//alert(appConfig.filters.costumContext);
		if(typeof appConfig.filters.costumContext != "undefined" && appConfig.filters.costumContext)		
			paramsFilter.defaults.filters['organizer.'+costum.contextId] = {'$exists':1};
		
	}
	if(typeof appConfig.results != "undefined")
		$.extend(paramsFilter.results,appConfig.results);
	if(typeof appConfig.extendFilters != "undefined"){
		$.extend(paramsFilter.filters, appConfig.extendFilters);
	}

	// searchObj.agenda.event= function(fObj){
 //         $(window).off("scroll", fObj.agenda.scroll).on("scroll", fObj, fObj.agenda.scroll);
 //         filterSearch.filters.manage.addActive(filterSearch,$("#filters-nav .btn-filters-select.source[data-label='Publié depuis la présente plateforme réseau']"),true);
	// };

	var filterSearch={};
	jQuery(document).ready(function() {
		mylog.log("AGENDA filters.php");
		filterSearch = searchObj.init(paramsFilter);
		// filterSearch.filters.manage.addActive(filterSearch,$("#filters-nav .btn-filters-select.source[data-label='Publié depuis la présente plateforme réseau']"),true);

	});
</script>