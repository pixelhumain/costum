<div class="col-xs-12">
	<h2>Qui sommes nous ? D'où vient le projet ?</h2>
	<h3 class="text-yellow">Les origines de Chtitube</h3>
	<span>La plate-forme Chtitube est née de la volonté de citoyens du Pas-de-Calais de soutenir et d’outiller sur leur territoire, à travers le numérique, les dynamiques citoyennes et les collectifs qui les portent. Pour cela, nous avons candidaté et avons été lauréats du budget citoyen du Pas-de-Calais qui vise à donner un coup de pouce financier aux initiatives portés par et pour les habitants du département.</span></br></br>
	
	<span>Pour co-construire notre plate-forme numérique citoyenne du Pas-de-Calais, nous avons voulu nous appuyer sur des collectifs engagés sur leur territoire et demandeur d’outils qui facilitent leur pratiques quotidiennes. En ce sens, nous avons animé des ateliers de co-construction au sein de collectifs (conseils citoyens, collectifs locaux de l’économie sociale et solidaire, bars coopératifs, …) répartis sur 3 territoires principaux : Boulogne-sur-Mer, Béthune et Arras.  </span>

	

	<h3 class="text-yellow">Et le jeu, il sert à quoi au final ?</h3>
	<span>La plate-forme Chtitube propose une approche ludique pour faciliter la participation du plus grand nombre à une cartopartie (référencement participatif sur un support cartographique commun) d’acteurs, d’événements, d’initiatives existantes et de projets d’avenir, afin d’initier les citoyens à la participation en ligne. Tout cela est réalisé sur le logiciel Communecter (logiciel libre) et a pour but de donner à voir la richesse du territoire en créant de la donnée ouverte.</span>
	</br></br>
	
	<span>A l’issue de ce maillage territorial réalisé par et pour les habitants au coeur de leur lieu de vie, nous avons l’ambition de proposer aux citoyens et acteurs référencés un outil collaboratif capable de les aider à co-construire et mener ensemble des projets fédérateurs pour l’avenir de leur territoire.</span>

	

</div>