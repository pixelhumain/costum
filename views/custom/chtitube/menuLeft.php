<div id="menuApp" class="col-md-3 menuLeft menuLeft-chtitube">
<div class="content-menu-left-chtitube col-xs-12 no-padding">
	<a href="javascript:;" class="col-xs-12 visible-xs show-menu-xs"><i class="fa fa-th"></i> Menu</a>
  <a href="#" class="lbh col-xs-12 hidden-xs">
  	<img class="img-responsive start-img" style="max-height: 350px;margin:auto;" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/logo.png'>
  </a>
  <?php if(Yii::app()->session["userId"]!=""){ ?>
	  <a href="#" data-hash="#" class="col-xs-12 lbh-menu-app btn-menu-xs">Tableau de bord</a> 
	  <a href="#play" data-hash="#play" class="col-xs-12 lbh-menu-app btn-menu-xs">Participer</a>
	  <a href="#references" data-hash="#references" class="col-xs-12 lbh-menu-app btn-menu-xs">Mes contributions</a>
	   <a href="#classement" data-hash="#classement" class="col-xs-12 lbh-menu-app btn-menu-xs">Classement</a>
  <?php }else{ ?> 
    <a href="javascript:;" class="btn-register col-xs-12 btn-menu-xs" data-toggle="modal" data-target="#modalLogin">Participer</a>
  <?php } ?>
  <a href="#search" data-hash="#search" class="col-xs-12 lbh-menu-app btn-menu-xs">Carto du 62</a>
  <a href="#agenda" data-hash="#agenda" class="col-xs-12 lbh-menu-app btn-menu-xs">Agenda du 62</a>
<!--  <a href="#regles" data-hash="#regles" class="col-xs-12 lbh-menu-app btn-menu-xs">Règles</a>-->
  <a href="#lots" data-hash="#lots" class="col-xs-12 lbh-menu-app btn-menu-xs">Les lots gagnés</a>
	<?php if(Yii::app()->session["userId"]!=""){ 
		if(Authorisation::isInterfaceAdmin()){ ?>
		  	<a href="#admin" data-hash="#admin" class="col-xs-12 lbh-menu-app btn-menu-xs">Admin-space</a>
		<?php } ?>
		<a href="<?php echo Yii::app()->createUrl("/co2/person/logout/") ?>" class="col-xs-12 btn-menu-xs">Déconnexion</a>
	<?php } ?>

</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		//coInterface.initHtmlPosition();
		$(".btn-menu-xs").click(function(){
			$(this).addClass("active");
		});
		$(".show-menu-xs").click(function(e){
			if($(".btn-menu-xs").is(":visible")){
				$(".show-menu-xs").html('<i class="fa fa-th"></i> Menu</a>');
				$(".btn-menu-xs").fadeOut();
				$(".menuLeft-chtitube").off();
			}
			else{
				$(".show-menu-xs").html('<i class="fa fa-close"></i> Fermer</a>');
				$(".btn-menu-xs").fadeIn();		
			}

		});
	});	
</script>