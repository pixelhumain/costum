<?php 
    $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">

	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	#filters-nav .dropdown-menu-complexe {
	    border-radius: 5px;
	    border: 1px solid #2c407a;
	    padding: 10px;
	}
	
	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList{
    background: transparent;
    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList-subsub:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .button-complexeSelectList:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe{
	    color: #2c407a;
	}

	#filterContainer .dropdown .btn-menu, #filters-nav .complexeSelect .btn-menu ,#filters-nav .dropdown .btn-menu, #filters-nav .filters-btn {
	    padding: 10px 15px;
	    font-size: 16px;
	    border: 1px solid #2c407a;
	    color: #2c407a;
	    top: 0px;
	    position: relative;
	    border-radius: 20px;
	    text-decoration: none;
	    background: white;
	    line-height: 20px;
	}

	#filterContainer #input-sec-search .input-group-addon, #filters-nav #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
	    padding: 10px 0px 10px 10px;
	    font-size: 18px;
	    font-weight: 400;
	    line-height: 1;
	    color: #2c407a;
	    margin-top: 5px;
	    text-align: center;
	    background-color: white !important;
	    border-right-width: 0px;
	    border: 1px solid #2c407a;
	    border-right-width: 1px;
	    border-radius: 0px 20px 20px 0px;
	    border-right-width: 0px;
	    width: 30px;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlOne:hover{
	    background-color : #2c407a;
	    color: white;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo{
	    background: transparent;
	    border: transparent;
	}

	#filters-nav .dropdown-menu-complexe .list-filters .lvlTwo:hover{
	    background-color : #2c407a;
	    color: white;
	}


	#filterContainer #input-sec-search .input-global-search, #filters-nav #input-sec-search .input-global-search, .searchBar-filters .search-bar {
	    border-left-width: 0px;
	    height: 40px;
	    margin-top: 5px;
	    border-radius: 20px 0px 0px 20px;
	    box-shadow: none;
	    width: inherit;
	    border-color: #2c407a;
	}

	.form-control::placeholder {
	    color: #2c407a;
	    opacity: 1;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	if (pageApp == "agenda") {
		var paramsFilter= {
		 	container : "#filters-nav",
		 	calendar : true,
		 	loadEvent: {
		 		default : "agenda"
		 	},
		 	defaults : {
		 		types : ["events"]
		 	}
		};
	}else if (pageApp == "actus"){
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["poi"]
		 	}
		};
	}else if (pageApp == "dda"){
		var paramsFilter= {
		 	container : "#filters-nav",
		 	defaults : {
		 		types : ["proposals"]
		 	},
		 	filters : {
		 		thematique : {
		 			view : "dropdownList",
		 			type : "filters",
		 			field : "thematique",
		 			name : "Thematique",
		 			event : "filters",
		 			list : {
		 				"Café littéraire": "Café littéraire",
			            "La Fabrique a bd" : "La Fabrique a bd",
			            "Blablabla" : "Blablabla",
			            "Bien-être en famille" : "Bien-être en famille",
			            "Carte Blanche" : "Carte Blanche",
			            "En lien avec Trébeurden" : "En lien avec Trébeurden"
		    		}
		 		}
	 		}
		};
	} 

	jQuery(document).ready(function() {
		mylog.log("Cmcas filters.php");
		filterSearch = searchObj.init(paramsFilter);

		if (pageApp == "dda") {
			$(".headerSearchright").css("display" , "none");
		}
	});
</script>