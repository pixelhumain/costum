<style>
    #subTitleDash.lead{
      font-size:18px;
    }
 </style>  
<?php 
   $user=PHDB::findOne(Person::COLLECTION,array("_id"=>new MongoId(Yii::app()->session["userId"])));
   // var_dump($user);exit;
   $explain=(isset(Yii::app()->session["userId"])) ? "Vous êtes administrateur.ice de ces structures et vous pouvez accéder directement à leur page." : "Vous devez vous connecter</a> pour accéder à votre tableau de bord.<br/><a href='javascript:;'class='btn btn-primary margin-top-10' onclick='Login.openLogin()'>Je me connecte</a>" ; 
   $addOrga=(isset(Yii::app()->session["userId"])) ? '<a class="btn btn-primary-ekitia" href="javascript:," onclick="costum[costum.slug].chooseOrgaType();"> CRÉER UNE NOUVELLE STRUCTURE </a>' : "" ; 
   $adminButton="";
   // $notMember="";
   if(!empty($user["links"]["memberOf"][$this->costum["contextId"]]["isAdmin"]) && $user["links"]["memberOf"][$this->costum["contextId"]]["isAdmin"]){
   $adminButton="<span><a href='#@".$this->costum["slug"]."' class='margin-bottom-5 btn-primary btn lbh'>Accéder à la page Lakou</a></span><br>"
      ."<span><a href='#admin' class='btn btn-primary lbh margin-bottom-5'>Accéder au panel d'administration</a></span>";
   }else if(empty($user["links"]["memberOf"][$this->costum["contextId"]])){
      $adminButton="<div class='col-xs-12 col-md-8 col-md-offset-2 margin-bottom-20'>
         Votre compte utilisateur est validé, mais vous n'avez pas encore fait la demande pour devenir membre du réseau Lakou et être davantage informé.e et impliqué.e activement dans la vie du réseau.
         <br/>
      <a href='javascript:;' class='ask-member-lakou btn btn-primary' >
						<i class='fa fa-user-plus'></i> Devenir membre
				</a>

      </div>";
   }else if(!empty($user["links"]["memberOf"][$this->costum["contextId"]]["toBeValidated"]) && $user["links"]["memberOf"][$this->costum["contextId"]]["toBeValidated"]){
      $adminButton="<div class='col-xs-12 col-md-8 col-md-offset-2 margin-bottom-20'>
         Votre demande pour devenir membre du réseau Lakou est en train d'être étudiée.

      </div>";
   }else if(!empty($user["links"]["memberOf"][$this->costum["contextId"]]) && empty($user["links"]["memberOf"][$this->costum["contextId"]]["toBeValidated"])){
      $adminButton="Vous êtes membre du réseau Lakou.";
   }
?>

<div class="basic-banner text-center">
    <h1 class="font-weight-100"><?= $pageDetail["name"]["fr"] ?></h1> 
    <?= $adminButton."<br>".$addOrga;?>
    <p id="subTitleDash" class="lead margin-top-20"> <?= $explain; ?></p>
</div>

<script>

   jQuery(document).ready(function() {

      $(".ask-member-lakou").off().on("click",function(){
         links.connectAjax(costum.contextType,costum.contextId,userId, 'citoyens', 'member');
         $(this).parent().html("Votre demande pour devenir membre du réseau Lakou est en train d'être étudiée")
      })

   });

</script>