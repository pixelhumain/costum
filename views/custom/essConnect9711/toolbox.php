<style>
	#filters-nav-tool{
		position: sticky !important;
	}

	#filters-nav{
		display:none !important
	}

	.main-container{
		padding-top: 56px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS, #search-content{
		padding-top: 12px !important;
		border-top: none !important;
	}
	.searchObjCSS .dropdown .btn-menu, .searchBar-filters .search-bar, .searchBar-filters .input-group-addon, .btn-primary-ekitia{
		border-radius :  2px !important;
		border: 1px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.searchBar-filters .input-group-addon{
		background-color: var(--primary-color) !important;
		color: white !important;
		padding: 10px 14px !important;
		height: 44px;
	}

	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.banner-outer-tool {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
		height: 250px;
		
	}
	.basic-banner {
		width: 100%;
		height: 100%;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
		display:flex;
		align-items: center;
		justify-content: space-around;
		flex-wrap: wrap;
    	align-content: center;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-yellow-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer{
		justify-content: space-around !important;
	}
	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	.bootbox.modal .modal-body{
		padding: 0px;
	}

	#dropdownFilters{
		position:static;
		bottom:0;
	}
	.activeFilter-label{
		display: none !important;
	}
	.headerSearchContainer{
		display: none
	}
	#tab-page{
		margin-bottom: 12px;
	}
	#filters-nav-container{
		position: relative;
		z-index: 9;
	}

	@media (min-width:600px)  { 
		#filters-nav-container{
			padding-left: 35px;
		}

		.ethic-container{
			padding: 10px 50px;
		}
	}
</style>

<!--div id="bannerSearch" class="banner-outer-tool ">
	<div class="basic-banner">
		<h1 class="bg-yellow-point text-center"> <?= ""//"Ressources partagées"//(($appConfig["name"]["fr"])??$appConfig["subdomainName"]??"Ressources partagées") ?></h1>
		<div class="col-md-10 col-xs-12" style="width: 100%;">
			<p class="text-center padding-right-35 padding-left-35">
				<?= ""//(($appConfig["metaDescription"])??($appConfig["metaDescription"])??"Vous êtes invités à  contribuer à la boîte à outils en partageant vos outils, ressources bibliographiques, retours d’expérience au sein de celle-ci. Pour cela, il vous faudra indiquer un lien  internet vers l’outil  ou un lien vers netexplorer (sur lequel l’outil aura été déposé auparavant).") ?>
			</p>
		</div>
	</div>
</div-->
<div id="tab-page" class="tab-outer bg-white" style="position:sticky; top:90px; z-index:99"> 
	<!-- <div id="tabs-container" class="tabs left large-tab">
		<div class="term-item tab-page active">
			<a type="button" class="tabs lbh" data-type="all" href="#boites-a-outils" data-filterk="tabs">
				<span class="ico-circle"><i class="fa fa-list"></i></span> Toutes les ressources
			</a>
		</div>
		<div class="term-item tab-page">
			<a type="button" class="tabs" data-type="ethic" id="tab-chart-ethic" data-filterk="tabs">
				<span class="ico-circle"><i class="fa fa-file-text"></i></span> Boîte à outils charte éthique
			</a>
		</div>
	</div> -->
</div>

<div id="filters-nav-container" class="searchObjCSS" style="z-index:8">
	<div class="ethic-container">
		<p class="padding-right-30 padding-left-5 margin-top-30" style="text-align:justify; text-justify: inter-word;">
		    Vous trouverez dans cette page des ressources sur les thématiques mises en avant par la réseau Lakou. 
			Nous vous invitons à enrichir l’Espace Ressources avec vos productions, retours d’expérience, bonnes pratiques, trames documentaires, rapports de veille … en vue de monter en maturité ensemble sur les sujets de résilience et de transition de notre territoire.
		</p>
	</div>
	<div id="filters-nav-tool"></div>
</div>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filtersByPage = {};
	var filterFields = ["name", "toolType", "toolLevel", "isEthicCharte", "thematic", "tags", "ethicCharte", "isEthicCharte"];
	var pageDirectory = "directory.cards";//coopPanelHtml
	var defaultFilters = {'$or':{}};
	var hasRoleLecteur = false;

	if(userConnected!=null && costum!=null &&
        typeof userConnected.links != "undefined" && 
        typeof userConnected.links.memberOf!="undefined" && 
        typeof userConnected.links.memberOf[costum.contextId]!="undefined" &&
        (typeof userConnected.links.memberOf[costum.contextId].isAdmin=="undefined" || (typeof userConnected.links.memberOf[costum.contextId].roles!="undefined" && userConnected.links.memberOf[costum.contextId].roles.includes("Contributeur")))){
			$.each(userConnected.links.memberOf, (index, value) => {
				if(value.roles && value.roles.includes("Lecteur")){
					hasRoleLecteur = true;
				}else{
					console.log("Has role lecteur", value.roles)
				}
			})
	}
	
	if(pageApp.indexOf("?")!=-1){
		pageApp = pageApp.split("?")[0];
	}

	sortResultBy = {"created":-1, "date":-1};
	defaultFilters["type"] = "tool";
	// defaultFilters['$or']["isEthicCharte"] = false;
	// defaultFilters['$or']["isEthicCharte"] =  {'$exists':false};
	filtersByPage = {
		text:{
			placeholder:"Rechercher une ressource",
			searchBy:"ALL"
		},
		// source : {
		// 	type: "filters",
		// 	name:"Structure",
		// 	view:"dropdownList",
		// 	event:"exists",
		// 	field:"parent",
		// 	keyValue:false,
		// 	list:sortAlphaNumeric(costum.lists["communities"]||{})
		// },
		toolType: {
			name:"Type de ressources",
			type:"filters",
			view:"dropdownList",
			event:"filters",
			field:"toolType",
			typeList:"object",
			list: Object.values(costum.lists.toolType).reduce((a, v) => ({ ...a, ...v}), {})
		},
		thematic: {
			name:"Thématique",
			type:"tags",
			view:"dropdownList",
			event:"tags",
			list:costum.lists.category
		},
		niveau : {
			name:"Niveau",
			type:"filters",
			view:"dropdownList",
			event:"filters",
			field:"toolLevel",
			list:costum.lists.toolLevel
		}
	}
	
	var paramsFilter = {
		container : "#filters-nav-tool",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			// notSourceKey: true,
		 	types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	filters: defaultFilters,
			indexStep: 900,
			sortBy: sortResultBy
		},
		filters : filtersByPage,
	 	results : {
			//dom:"results-tool",
			renderView: pageDirectory
		}
	}

	if(filterFields.length!=0){
		paramsFilter.defaults["fields"] = filterFields;
	}
	 
	jQuery(document).ready(function(){

		/*searchObj.filters.views["tabs"] = function(k,v, fObj){
			str = `<div class="tab-outer"> 
						<div id="tabs-container" class="tabs left large-tab">`;
			if(v.list){
				$.each(v.list, function(key, item){
					let dataFilter = "";
					if(item.page){
						dataFilter =  'href="'+item.page+'"';
					}else{
						dataFilter = `data-type="${item.type??v.type??"filters"}" 
							data-key="${item.key??key}" 
							data-value="${item.value??item.label??key}" 
							data-field="${item.field??v.field??"tags"}"
							data-renderview="${item.renderView??""}",
							data-dfilters="${item.filters}"
							`;
						//if(item.type && item.type=="filters"){
							dataFilter+=` data-label="${item.label??key}" `;
							dataFilter+=` data-event="${item.event??v.event??"filters"}" `;
						//}
					}
					let icon = `<span class="ico-circle"><i class="fa ${(item.icon!="")?item.icon:"fa-tag"}"></i></span>`;
					if(item.icon==null){
						icon="";
					}
					str+= `<div class="term-item ${item.class??""}">
						<a type="button" class="${item.page?"lbh":"btn-filters-select"} tabs" ${dataFilter} data-filterk="tabs">${icon} ${item.label||key}</a>
					</div>`;
				});
			}
			str+=`</div>
			</div>`;

			return str;
		}

		searchObj.filters.events["tabs"] = function(fObj, domFilters, k){
			$(".btn-filters-select").off().on("click", function(event){
				let type = $(this).data("type");
				let field = $(this).data("field");
				let renderView = $(this).data("renderview");data("typ
				let filters = $(this).data("dfilters");

				if(type != "types"){
					if(field == "tags"){
						fObj.search.obj.tags = [$(this).data("value")];
					}else{
						fObj.search.obj.filters[field] = $(this).data("value")
					}
					fObj.search.obj.types = appConfig.filters.types||["news", "events", "poi"];
					fObj.filters.actions.types.initList = appConfig.filters.types||["news", "events"];
				}else{
					fObj.search.obj.tags = [];
					fObj.search.obj.types = [$(this).data("value")];
					fObj.filters.actions.types.initList =  [$(this).data("value")];
				}

				if(typeof filters != "undefined"){
					try {
						fObj.search.obj.filters = JSON.parse(filters.replaceAll("'", '"'));
					} catch (error) { }
				}

				if(renderView!="" && renderView!="undefined"){
					fObj.results.renderView = renderView;
				}

				$(".term-item").each(function(){
					$(this).removeClass("active");
				});
				$(this).parent().addClass("active")
				fObj.search.init(fObj);
			});
		}*/

		searchObj.scroll.callBack = function(fObj){
			//searchObj.scroll.callBack(fObj);
			if($(this).data("renderview") == "directory.tableOfContent"){
				costum[costum.slug].listedPrincipe = {};
			}
		}

		filterSearch =  searchObj.init(paramsFilter);
		loadAfterInit();

		$(".btn-filters-select").on("click", function(){
			if($(this).data("renderview") == "directory.tableOfContent"){
				costum[costum.slug].listedPrincipe = {};
			}
		})

		$("#dropdownFilters").off().on("click", function() {
			$("#filtersInDropdown").toggle("show", function(e){
				if(this.style.display == "block"){
					$("#bannerSearch").hide();
					$('html,body').animate({
						scrollTop: $("#filters-nav-tool").offset().top},
					'slow');
				}else{
					$("#bannerSearch").show();
					$('html,body').animate({
					scrollTop: $("#bannerSearch").offset().top},
				'slow');
				}
			});
		});

		$(".tab-page").on("click", function(){
			$(".term-item").each(function(){
				$(this).removeClass("active");
			});
			$(this).addClass("active");

			if($(this).data("type")=="all"){
				$("#filters-nav-container").show();
			}else{
				$("#filters-nav-container").hide();
			}
			$("#search-content").empty();
			let chartEthicHTML = ""
			let ec = {}
			$.each(costum.lists["charteEthique"], function(index, ce){
				let itemIndex = Object.keys(costum.lists["charteEthique"]).indexOf(index);
    			chartEthicHTML += '<div id="principe'+itemIndex+'" class="col-xs-12 padding-left-35">'+
						'<div class="margin-top-5 margin-left-35">'+
							'<h3 class="margin-bottom-10 margin-left-35">PRINCIPE <span class="h1" style="color:var(--primary-color)">'+(itemIndex+1)+ "</span> : "+index+'</h3>'+
						'</div>'+
					'</div>';
				ec[index] = index;
				$.each(ce, function(ceIndex, ceValue){
					ec[ceIndex] = ceIndex;
					let subIndex = Object.keys(costum.lists["charteEthique"][index]).indexOf(ceIndex);
					chartEthicHTML += '<div class="col-xs-12  padding-left-35">'+
						'<div class="margin-left-35">'+
							'<a href="javascript:;" data-indexp="'+(itemIndex+1)+'" data-indexi="'+(subIndex+1)+'" data-name="'+ceIndex+'" data-principe="'+index+'" class="ethicLink margin-left-35"><h4 class="padding-left-35" style="color:var(--primary-color)">'+(itemIndex+1)+"."+(subIndex+1)+". " +ceIndex+'</h4></a>'+
						'</div>'+
					'</div>';
				})
			});
			$("#search-content").append(`<div id='ethic-container' class='searchObjCSS'>
				<div class="ethic-container">
				    <p class="margin-top-20 padding-right-25 padding-left-25" style="text-align: justify;">
						Vous trouverez sur cette page la charte du réseau Lakou.
					</p>
					${chartEthicHTML}
				</div>`);
		});

		$(document).off("click", ".ethicLink").on("click", ".ethicLink", function(){
			let name = $(this).data("name");
			let principe = $(this).data("principe");
			let indexi = $(this).data("indexi");
			let indexp = $(this).data("indexp");
			urlCtrl.openPreview("/view/url/costum.views.custom.essConnect9711.ethicPreview", {"indexi":indexi, "indexp":indexp, "principe":principe, "name":name, "description":costum.lists["charteEthique"][principe][name].description})
		});

		function loadAfterInit(){
			if(["boites-a-outils"].indexOf(pageApp)!=-1){
				$(".btn-show-map").hide();
				$("#filters-nav-tool").css("padding", "0px");
				$("#filters-nav-tool").css("border-bottom", "0px");
				//$("#filterContainerInside").css("padding", "0px");
				//$("#filters-nav-tool").style("padding", "0em .5em 0em .5em !important");
			}else{
				$("#filters-nav-tool").css("border-bottom", "1px solid rgba(100, 100, 100, 0.1)");
				$("#filters-nav-tool").css("padding-right", "5px");
				$("#filters-nav-tool").css("padding-bottom", "9px");
				$("#filters-nav-tool").css("padding-left", "5px");
				$("#filters-nav-tool").css("padding-top", "12px");
				$(".btn-show-map").addClass("btn-primary-ekitia btn-primary-outline-ekitia").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo("#right-side");
			}

			//$("#search-content").appendTo("#filters-nav-tool")

			if($(".tab-outer").length!=0){
				if(!hasRoleLecteur && appConfig && typeof appConfig.filters !="undefined" && typeof appConfig.filters.types!="undefined"){
					let addBtnDropdown = "";
					if(appConfig.filters.types.length==1){
						if(appConfig.filters.types[0]=="answers" && typeof costum.lists!="undefined" && costum.lists["numericServiceForms"]){
							addBtnDropdown = prepareAddBtnDropdown(costum.lists["numericServiceForms"]);
						}else{
							let typeByPage = appConfig.filters.types[0];
							if(pageApp == "boites-a-outils"){
								typeByPage = "tools";
							}else if(pageApp=="actus"){
								typeByPage = "article";
							}
							addBtnDropdown = '<div class="term-item right-content"><button onclick="costum[costum.slug].beforePost(``, `'+typeByPage+'`);" class="btn btn-primary-ekitia btn-primary-outline-ekitia">PARTAGER UNE RESSOURCE OU UN OUTIL</button></div>';
						}
					}else if(appConfig.filters.types.length>1){
						addBtnDropdown = prepareAddBtnDropdown(appConfig.filters.types);
					}
					$(".tab-outer").append(addBtnDropdown);
				}
			}else{
				$("#right-side").prepend("")
				$("#right-side").insertBefore("#activeFilters");
			}
			//$("#activeFilters").hide();
			//$("#filterContainerInside").append("<button class='btn btn-primary-ekitia'>POSTER</button>")
			
			$(".filters-activate[data-type='preview']").click();
		}

		function prepareAddBtnDropdown(config){
			let addBtnDropDown = `<div class="btn-group">
			<button class="btn btn-primary-ekitia btn-primary-outline-ekitia dropdown-toggle" type="button" data-toggle="dropdown">POSTER
			<span class="caret"></span></button>
			<ul class="dropdown-menu pull-right">`;
			$.each(config, function(index, type){
				/*if(type=="news"){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`#live`);" class="padding-10">'+(type)+'</a></li>';
				}else*/ if(typeof type=="object" && type!=null){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`'+type.value+'`);" class="padding-10">'+(type.name||type.label||"Formaulaire")+'</a></li>';
				}else{
					let label = type;
					if(type=="news"){
						type="poi"; label="Actualité"
					}
					if(type=="newsletter"){
						return;
					}
					if(label=="poi"){
						label=typeObj[label].name;
					}
					let t = ((typeObj[type] && typeObj[type].sameAs)?typeObj[type].sameAs:type);
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(``, `'+t+'`);" class="padding-10" data-form-type="'+t+'">'+label+'</a></li>';
				}
			});
			addBtnDropDown+="</ul></div>";
			return addBtnDropDown;
		}
	})

	function sortAlphaNumeric(data){
		let sorted = Object.values(data).sort((a, b) => {
			let fa = a.replace(" ", "").trim().toLowerCase(),
				fb = b.replace(" ", "").trim().toLowerCase();

			return ('' + fa).localeCompare(fb);
		});
		
		let sortedResults = {}
		$.each(sorted, function(i, val){
			const k= Object.keys(data).filter(key => data[key]==val);
			if(k[0]){
				sortedResults[k[0]] = val;
			}
		});
		return sortedResults;
    	};
</script>