<table class="row masthead" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: white;width: 100%;position: relative;display: table;">
    <tbody>
    <tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Masthead -->
        <th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                <tr style="padding: 0;vertical-align: top;text-align: left;">
                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                        <center style="width: 100%;min-width: 532px;">
                            <?php if(!empty($logo2)){ ?>
                                <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logo2 ?>" valign="bottom" alt="Logo Communecter" align="center" class="text-center" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;float: none;text-align: center;">
                            <?php } ?>
                        </center>
                    </th>
                    <th class="expander" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0 !important;margin: 0;text-align: left;line-height: 19px;font-size: 15px;visibility: hidden;width: 0;"></th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;">
    <tbody>
    <tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
        <th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                <tr style="padding: 0;vertical-align: top;text-align: left;">
                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                        <!-- <a href="http://www.communecter.org" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;"><img align="right" width="200" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true)."/images/bdb.png"?>" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;border: none;" alt="Intelligence collective"></a> -->
                        <b>
                            <h5 style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;">
                                <!-- Aussi connecté que Facebook et aussi ouvert que Wikipédia, rejoignez le mouvement ! -->
                                <?php
                                if( empty($title) || $title == "Communecter"){
                                    echo Yii::t("mail","Open Like Wikipedia and as connected as Facebook join the movement!") ;
                                }
                                ?>
                            </h5>
                        </b><br/>
                        <?php echo "Vous avez été invité.e à rejoindre la solution ESS Connect 971 par <b>\"$invitorName\"</b> en tant que collaborateur d".(isset($target["name"]) && isset($target["type"]) && $target["type"] != Person::COLLECTION && in_array(strtolower(substr($target["name"], 0, 1)), ["a", "o", "i", "u", "e", "h", "y"])? "'" : "e");
                        if( !empty($target) && !empty($target["type"]) && $target["type"] != Person::COLLECTION )
                            echo " <b>\"".$target["name"]."\"</b>"; ?>
                        .<br/><br/>
                        <?php if($target["type"]!= Person::COLLECTION){ ?>
                            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">

                                <tr style="padding: 0;vertical-align: top;text-align: left;">
                                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: right;width:50%;line-height: 19px;font-size: 15px;">
                                        <a href="<?php echo str_replace("co2/link/", "costum/essConnect9711/",$urlActions)."/answer/true" ?>" style="color: white;
						                    font-family: Helvetica, Arial, sans-serif;
						                    font-weight: normal;
						                    padding: 10px 40px;
						                    margin: 0;
						                    text-align: left;
						                    line-height: 1.3;
						                    text-decoration: none;
						                    width: 40%;
						                    margin-right: 2%;
						                    border-radius: 3px;
						                    background-color: #84d802;
						                    font-size: 15px;
						                    font-weight: 800;"><?php echo Yii::t("common", "Accept") ?></a>
                                    </th>
                                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;width:50%;">

                                        <a href="<?php echo str_replace("co2/link/", "costum/essConnect9711/",$urlActions)."/answer/false" ?>" style="color: white;
						                  font-family: Helvetica, Arial, sans-serif;
						                  font-weight: normal;
						                  padding: 0;
						                  margin: 0;
						                  text-align: left;
						                  line-height: 1.3;
						                  text-decoration: none;
						                  width: 40%;
						                  margin-left: 2%;
						                  border-radius: 3px;
						                  background-color: #e33551;
						                  font-size: 15px;
						                  font-weight: 800;
						                  padding: 10px 40px;"><?php echo Yii::t("common", "Refuse") ?></a>
                                    </th>

                                </tr>
                            </table>
                            <br/><br/>
                        <?php } ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                            <tr>
                                <td>
                                    <p>Lakou est la plateforme collaborative dédiée aux membres de son réseau qui vous permettra de réaliser les actions suivantes :</p>
                                    <ul>
                                        <li>Identifier de potentiels partenaires</li>
                                        <li>Accéder à une boîte à outils pour monter en maturité sur les enjeux de la transition</li>
                                        <li>Mettre en vitrine vos ressources numériques et méthodologiques</li>
                                        <li>Échanger avec les autres membres ...</li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
        </th>
    </tr>

    </tbody>
</table>

