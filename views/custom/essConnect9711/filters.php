<style>
	#dropdown_search{
        height:unset !important;
    }
	
	#filters-nav{
		position: sticky !important;
	}

	#search-content.shadow{
		box-shadow: unset;
	}

	.main-container{
		padding-top: 56px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS{
		padding-top: 12px !important;
	}
	.searchObjCSS .dropdown .btn-menu, .searchBar-filters .search-bar, .searchBar-filters .input-group-addon, .btn-primary-ekitia{
		border-radius :  2px !important;
		border: 1px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.searchBar-filters .input-group-addon{
		background-color: var(--primary-color) !important;
		color: white !important;
		padding: 10px 14px !important;
		height: 44px;
	}

	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
		width: 320px !important;
	}
	.dropdown-title{
		display: none;
	}
	.thin {
		padding: 1em;
		font-size: 16px;
		text-transform: uppercase;
		width: 100%;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
		height: 250px;
		
	}
	.basic-banner {
		width: 100%;
		height: 100%;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
		display:flex;
		align-items: center;
		justify-content: space-around;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-yellow-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.tab-outer{
		justify-content: space-around !important;
	}

	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	.bootbox.modal .modal-body{
		padding: 0px;
	}

#filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn {
    margin-left: 0px !important;
}
</style>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	appConfig.useFooter=false;
	var filtersByPage = {};
	var filterFields = [];
	var pageDirectory = "directory.classifiedPanelHtml";
	var defaultFilters = {'$or':{}};
	var sortResultBy = {"name":1};
	// Organizations
	defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["parentId"] = costum.contextId;
	defaultFilters['$or']["source.keys"] = costum.slug;
	defaultFilters['$or']["reference.costum"] = costum.slug;
	defaultFilters['$or']["links.contributors."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};

    var affichage = "";

	// Not validated
	defaultFilters["toBeValidated"]={'$exists':false};
	
	if(pageApp.indexOf("?")!=-1){
		pageApp = pageApp.split("?")[0];
	}

	if(["actus", "datacatalogue", "boites-a-outils"].indexOf(pageApp)==-1){
		var activity = null;
		var organizationType = null;
		
		if(typeof costum.lists != "undefined" && typeof costum.lists.activity != "undefined"){
			activity = {};
			$.each(costum.lists.activity, function(index, sector){
				activity[index] = {};
				activity[index][sector] = sector;
			})
		}
		
		if(typeof costum.lists != "undefined" && typeof costum.lists.structure != "undefined"){
			organizationType  = {};
			Object.assign(organizationType , costum.lists.structure);
		}
	}else{
		sortResultBy = { "created":-1, "date":-1 };
	}
	
	if(pageApp=="datacatalogue"){
		var numericServices = {};
		if(typeof costum.lists != "undefined" && typeof costum.lists.numericServiceForms != "undefined"){
			Object.assign(numericServices, costum.lists.numericServiceForms)
		}
	}

	// var defaultScopeList = ["FR"]; /** Not for the mement */
	if(pageApp == "recherche" || pageApp=="search"){

		// header definition
		// var headerByPage={
		// 	options : {
    	// 		right : {
    	// 			class : "col-xs-6",
    	// 			group : {
    	// 				custom : {
    	// 					html : "<a class='btn btn-default'>Je we god</a>"
    	// 				}
    	// 			}
    	// 		}
		//     }
		// };

		pageDirectory="directory.cards";
		// pageDirectory = "directory.logos";
		filtersByPage = {
			text : {
				placeholder : "Recherche de membres ou de mots-clés"
			},
			structure : {
				view : "dropdownList",
				type : "tags",
				trad:false,
				name : "Typologie de structure",
				event : "tags",
				keyValue : false, 
				list : {...{"TiersLieux" : "Tiers-lieu"},...costum.lists.structure}
			}
		}

		// if(typeof costum.typeObj != "undefined" && 
		// 	(typeof costum.typeObj.organizations != "undefined" || typeof costum.typeObj.organization!="undefined") && 
		// 	costum.lists){
		// 	var costumInputs = costum.typeObj.organizations;
		// 	if(costumInputs){
		// 		costumInputs = costumInputs.dynFormCostum.beforeBuild.properties
		// 	}else{
		// 		costumInputs = costum.typeObj.organization.dynFormCostum.beforeBuild.properties;
		// 	}
		// 	$.each(costumInputs, function(index, input){
		// 		var hideFilterKey=["activity"];
		// 		if(typeof input.list != "undefined" && costum.lists[input.list] && hideFilterKey.indexOf(input.list)==-1){
		// 			let optionList = Array.isArray(costum.lists[input.list])?costum.lists[input.list].reduce((a, v) => ({ ...a, [v]: v}), {}):costum.lists[input.list];
		// 			let filterView = "dropdownList";
		// 			// to do add condition for specific filterView
		// 			filtersByPage[input.list] = {
		// 				view : filterView,
		// 				type : "tags",
		// 				trad:false,
		// 				name : (input.label.length < 30)?input.label:index,
		// 				event : "tags", 
		// 				list : optionList
		// 			}
		// 		}
		// 	})
		// }


		

		// let numericServicesFilter = {};
		// $.each(costum.lists.numericServiceForms, function(nsIndex, nsValue){
		// 	console.log(" lopp ", nsIndex, nsValue.label);
		// 	numericServicesFilter[nsIndex] = {"key": nsIndex, "value":nsIndex, "label": nsValue.label};
		// })
		
		// filtersByPage["services"] = {
		// 	view : "dropdownList",
		// 	type : "filters",
		// 	field : "numericService",
		// 	trad:false,
		// 	name : "Type de ressources et services numériques proposés",
		// 	event : "filters",
		// 	typeList: "object",
		// 	list : numericServicesFilter
		// }
	}

	if(pageApp == "projets-territoriaux"){
		pageDirectory="directory.cards";
		// pageDirectory = "directory.logos";
		filtersByPage = {
			text : {
				placeholder : "Rechercher un projet territorial"
			}
		}
	}

	if(pageApp == "actus"){
		pageDirectory = "directory.cards";
		filterFields = ["mediaImg", "text", "organizer", "date", "parent", "created"];
		// defaultFilters["type"]={'$ne':"activityStream"};
		defaultFilters["type"]={'$nin':["activityStream", "tool", "doc"]};
		filtersByPage = {
			tabs : {
	 			view : "tabs",
	 			type : "tags",
				event : "tabs",
	 			list : {
					/*"Toutes les actualités": {
						icon : null,
						page : "#"+pageApp,
						class: "active"
					},*/
					"ACTUALITÉS" : {
						icon: "fa-calendar",
						type:"types",
						value:"news",
                        class: "active"
					},
					"ÉVÉNEMENTS" : {
						icon: "fa-calendar",
						type:"types",
						value:"events"
					}
				}
	 		}
		};
	}

	if(pageApp == "datacatalogue"){
		pageDirectory = "directory.answers";
		filterFields = [
			"form", 
			"source.date",
			"links.organizations",
			"date",
			"answers"/*
			"answers.ekitia25102023_918_0.ekitia25102023_918_0lo5d934pla097xvfvnk",
			"answers.ekitia25102023_942_0.ekitia25102023_942_0lo5e3czhfhla9u6o74m",
			"answers.ekitia25102023_954_0.ekitia25102023_954_0lo5eiwo956j2a3f57u4"*/
		];
		defaultFilters["answers"]={'$exists':true};
		delete numericServices["Autres"];
		numericServices = {
			"all": {
				label: "Toutes",
				page:"#"+pageApp,
				class: "active"
			}, ...numericServices
		}
		filtersByPage = {
			/*format:{
				name: "Format",
				view : "dropdownList",
				field: ["answers.ekitia25102023_918_0"],
				event : "filters",
				list: {"json":"json", "csv":"csv"}
			},*/
			numericServices : {
				view : "tabs",
				field: "form",
				event : "tabs",
				list: numericServices
			}
			
		}		
	}

	/*if(pageApp == "boites-a-outils"){
		pageDirectory = "directory.cards";
		filtersByPage = {
			tools : {
				view : "tabs",
	 			type : "tags",
				event : "tabs",
				list: {
					"Toutes les productions": {
						label : "",
						icon : null,
						filters : "#"+pageApp,
						class: "active"
					},
					"ECONOMIQUE" : {
						label : "Economique",
						icon : "fa-bar-chart",
					},
					"ETHIQUE & JURIDIQUE" : {
						label : "Ethique & Juridique",
						icon : "fa-balance-scale"
					},
					"TECHNIQUE" : {
						label : "Technique",
						icon : "fa-cogs",
					},
					"NEWSLETTER" : {
						label : "Newsletter",
						icon : "fa-envelope-open",
					}
				}
			},
		}
	}*/

	if(pageApp=="projects"){
		pageDirectory = "directory.cards";
	}

	if(pageApp=="citoyens"){
		pageDirectory = "directory.profils";
	}

	var paramsFilter = {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	filters: defaultFilters,
			indexStep: 30,
			sortBy: sortResultBy
		},
		filters : filtersByPage,
	 	results : {
			renderView: pageDirectory,
		 	smartGrid : true
		}
	}

	if(pageApp == "recherche" || pageApp=="search"){
		paramsFilter.options.tags.verb='$in';
	}

	if(typeof headerByPage!="undefined"){
		paramsFilter.header=headerByPage;
	}

	if(filterFields.length!=0){
		paramsFilter.defaults["fields"] = filterFields;
	}
	var actualityData = {}
	 
	jQuery(document).ready(function(){
		
		searchObj.filters.views["tabs"] = function(k,v, fObj){
			str = `<div class="tab-outer"> 
						<div id="tabs-container" class="tabs left large-tab">`;
			if(v.list){
				$.each(v.list, function(key, item){
					let dataFilter = "";
					if(item.page){
						dataFilter =  'href="'+item.page+'"';
					}else{
						dataFilter = `data-type="${item.type??v.type??"filters"}" 
							data-key="${item.key??key}" 
							data-value="${item.value??item.label??key}" 
							data-field="${item.field??v.field??"tags"}" 
							`;
						//if(item.type && item.type=="filters"){
							dataFilter+=` data-label="${item.label??key}" `;
							dataFilter+=` data-event="${item.event??v.event??"filters"}" `;
						//}
					}
					let icon = `<span class="ico-circle"><i class="fa ${(item.icon!="")?item.icon:"fa-tag"}"></i></span>`;
					if(item.icon==null){
						icon="";
					}
					str+= `<div class="term-item ${item.class??""}">
						<a type="button" class="${item.page?"lbh":"btn-filters-select"} tabs" ${dataFilter} data-filterk="tabs">${icon} ${item.label||key}</a>
					</div>`;
				});
			}
			str+=`</div>
			</div>`;

			return str;
		}

		searchObj.filters.events["tabs"] = function(fObj, domFilters, k){
			$(".btn-filters-select").off().on("click", function(event){
				let type = $(this).data("type");
				let field = $(this).data("field");
				if(type!="types"){
					if(field=="tags"){
						fObj.search.obj.tags = [$(this).data("value")];
					}else{
						fObj.search.obj.filters[field] = $(this).data("value")
					}
					fObj.search.obj.types = appConfig.filters.types||["news", "events", "poi"];
					fObj.filters.actions.types.initList = appConfig.filters.types||["news", "events"];
				}else{
					fObj.search.obj.tags = [];
					fObj.search.obj.types = [$(this).data("value")];
					fObj.filters.actions.types.initList =  [$(this).data("value")];
					/*$(".last-item").remove();
					$("#tabs-container").append("<div class='term-item last-item' style='margin-left:auto'></div>");
					$(".btn-show-map").addClass("btn-primary-ekitia").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo(".last-item");
					$(".btn-show-map").show();*/
				}
				$(".term-item").each(function(){
					$(this).removeClass("active");
				});
                if(pageApp == "actus"){
                    affichage = "tml";
                }
				$(this).parent().addClass("active")
				fObj.search.init(fObj);
			});
		}

        if(pageApp == "recherche" || pageApp == "search"){
            searchObj.header.events.map = function(fObj){
                $(".btn-show-map").off().on("click", function(e){
                    fObj.helpers.toggleMapVisibility(fObj);
                    var text = filterSearch.results.map.active ? '<i class="fa fa-list-alt"></i> '+tradCms.directoryPage : '<i class="fa fa-map-marker"></i> '+trad.map;
                    $(".btn-show-map").html(text);
                });
            }
			$(".footer-cms").hide();
			// $("#search-content").removeClass("shadow");
        }

		if(pageApp == "actus"){
            paramsFilter.defaults.types = ["news"];
			searchObj.search.callBack = function (fObj, results) {
				actualityData = results;
                if((JSON.stringify(fObj.search.obj.types) === JSON.stringify(["events"])) && (affichage == "tml")){
                    loadActualityView()[affichage]($(".tab-outer button.btn-actuality-switch"));
                }
				fObj.search.currentlyLoading = false;
				fObj.search.obj.count=false;
				fObj.search.obj.countType=[];
				fObj.filters.actions.text.spin(fObj, false);
				if(typeof results != 'undefined' && Object.keys(results).length < fObj.search.obj.indexStep){
					fObj[fObj.search.loadEvent.active].stopPropagation=true;
					$(fObj.results.dom+" .processingLoader").remove();
				}
				if(fObj.results.smartGrid)
					fObj.results.smartCallBack(fObj, 0);
				if(typeof fObj[fObj.search.loadEvent.active].callBack != "undefined") fObj[fObj.search.loadEvent.active].callBack(fObj,results);
			}
        }

		searchObj.search.sortAlphaNumeric = function(results){
    		mylog.log("searchObj.search.callBack tosort", results);
			let sorted = Object.values(results).sort((a, b) => {
				let fa = a.name.replace(" ", "").trim().toLowerCase(),
					fb = b.name.replace(" ", "").trim().toLowerCase();

				return ('' + fa).localeCompare(fb);
			});
			let sortedResults = {}
			$.each(sorted, function(i, val){
				sortedResults[val._id.$id] = val;
			});
			return sortedResults;
    	}

		searchObj["scroll"].render = function(fObj, results, data){
			results = searchObj.search.sortAlphaNumeric(results);
      		//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				// if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				// }else{
				// 	toastr.error("Aucun résultat trouvé. Essayez avec un terme plus précis.");					
				// }	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique);
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
					}
					imageObserver.observe(v);
				});
			}
		}

		filterSearch = searchObj.init(paramsFilter);
		$("#filterContainerInside").append("<div id='right-side'></div>");
		if(["actus", "datacatalogue", "boites-a-outils"].indexOf(pageApp)!=-1){
			$(".btn-show-map").hide();
			$("#filters-nav").css("padding", "0px");
			$("#filters-nav").css("border-bottom", "0px");
			$("#filterContainerInside").css("padding", "0px");
			//$("#filters-nav").style("padding", "0em .5em 0em .5em !important");
		}else{
			$("#filters-nav").css("border-bottom", "1px solid rgba(100, 100, 100, 0.1)");
			$("#filters-nav").css("padding-right", "5px");
			$("#filters-nav").css("padding-bottom", "9px");
			$("#filters-nav").css("padding-left", "5px");
			$("#filters-nav").css("padding-top", "12px");
			$(".btn-show-map").addClass("btn-primary-ekitia btn-primary-outline-ekitia margin-bottom-5").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo("#right-side");
			if(pageApp=="search"){
			    $("<a href='javascript:;' class='pull-right orga-choice-modal btn btn-primary-ekitia btn-primary-outline-ekitia'><i class='fa fa-plus margin-right-5'></i> Ajouter une structure</a>").insertAfter(".btn-show-map");
			}else{
				$("<a href='javascript:;' data-form-type='project' class='pull-right btn-open-form btn btn-primary-ekitia btn-primary-outline-ekitia'><i class='fa fa-plus margin-right-5'></i> Ajouter un projet</a>").insertAfter(".btn-show-map");
			}
		}
		$(".orga-choice-modal").off().on("click",function(){
			costum[costum.slug].chooseOrgaType();
		});

		if($(".tab-outer").length!=0){
			if(appConfig && typeof appConfig.filters !="undefined" && typeof appConfig.filters.types!="undefined"){
				let addBtnDropdown = "";
				if(appConfig.filters.types.length==1){
					if(appConfig.filters.types[0]=="answers" && typeof costum.lists!="undefined" && costum.lists["numericServiceForms"]){
						addBtnDropdown = prepareAddBtnDropdown(costum.lists["numericServiceForms"]);
					}else{
						addBtnDropdown = '<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-open-form" data-form-type="'+appConfig.filters.types[0]+'">POSTER</button></div>';
					}
				}else if(appConfig.filters.types.length>1){
					addBtnDropdown = prepareAddBtnDropdown(appConfig.filters.types);
				}
				$(".tab-outer").append(addBtnDropdown);
				// if ($.inArray("poi", appConfig.filters.types) && pageApp == "actus") {
				// 	$(".tab-outer").append('<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				// }
			}
		}else{
			$("#right-side").prepend("")
			$("#right-side").insertBefore("#activeFilters");
		}
		$("#activeFilters").hide();
		//$("#filterContainerInside").append("<button class='btn btn-primary-ekitia'>POSTER</button>")
		$(".activeFilter-label").remove();
		// if(pageApp!=="recherche"){
			$(".headerSearchContainer").remove();
		// }
		
		
		$(".banner-outer").remove();

		/*
			$(`<div id="bannerSearch" class="banner-outer ">
				<div class="basic-banner">
					<h1 class="bg-yellow-point text-center">${appConfig.bannerTitle||appConfig.subdomainName||appConfig.metaDescription||""}</h1>
				</div>
			</div>`).insertBefore("#filters-nav")
		*/

		// Events
		
		$(".btn-show-map, .btn-filters-select").on("click", function(){
			if(filterSearch.results.map.active){
				$("#bannerSearch").hide();
				$('html,body').animate({
					scrollTop: $("#filters-nav").offset().top},
				'slow');
			}else{
				$("#bannerSearch").show();
			}
            if($("#show-filters-xs").is(":visible")){
                $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
            }
			if ($("div.btn-event-actus-ctn").length > 0)
				$("div.btn-event-actus-ctn").remove();
			if ($(this).attr("data-value") == "events" && pageApp == "actus") {
				$(".tab-outer").append('<div class="term-item btn-event-actus-ctn"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				$(".tab-outer button.btn-actuality-switch").click( function() {
					var viewDisplayed = $(this).attr("data-switch");
                    affichage = viewDisplayed;
					loadActualityView()[viewDisplayed](this);
				})
			}
				
		});
        $("#show-filters-xs").on("click", function(){
            $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
        })
		$(".filter-btn-hide-map").on("click", function(){
            $(".btn-show-map").html('<i class="fa fa-map-marker"></i> '+trad.map);
			$("#bannerSearch").show();
		})

		/**
		 * Manage timeline view
		 */
		function loadActualityView () {
			var views = {};
			views.change = function (selector, name="", viewType="tml", icon="fa-th-list") {
				$(selector).html('<i class="fa ' + icon + '"></i> ' + name);
				$(selector).attr("data-switch", viewType);
			};
			views.tml = function (selector) {
				views.change(selector, "Vue vignette", "crd", "fa-th-list");
				if (notEmpty(views.timelineConetent())) {
					$("#dropdown_search").html(`<div class="container-timeline"><div id="timeline" class="timeline-ctn"></div></div>`);
					$(".timeline-ctn").html(views.timelineConetent());
					coInterface.bindLBHLinks();
				}
			};
			views.crd = function (selector) {
				views.change(selector, "Vue timeline", "tml");
				filterSearch.results.renderView = "directory.cards";
				filterSearch.search.init(filterSearch);
			};
			views.timelineConetent = function () {
				var resultHtml = "";
				var direction = "";
				$.each(actualityData, function(k,v) {
					var hash = "";
					var image = (v.profilMediumImageUrl != undefined) ?  v.profilMediumImageUrl : "";
					if (v.collection == "events") {
						resultHtml += `
							<div class="timeline-item">
								<div class="timeline-icon">
									<div class="date-timeline"> 
										<b>${moment.unix(v.created).local().locale('fr').format('DD')}</b>/${moment.unix(v.created).local().locale('fr').format('MM')} 
										<br><b>${moment.unix(v.created).local().locale('fr').format('YYYY')}</b>
									</div>
								</div>
								<div class="timeline-content ${direction}">
									<div class="image-with-title">
										<div class="image-timeline-content">
											<img width="150" height="150" src="${image}" style="border-radius: 5px;" class="img-responsive" alt="" decoding="async">
										</div>
										<div class="info-timeline">
											<h4>${(v.name != undefined) ? v.name : "Il n'y a pas de titre"}</h4>
											${(v.type != undefined) ? '<h5 class="type-timeline"><i class="fa fa-chevron-right"></i> ' + v.type + '</h5>' : ""} 
											<div class="interval-timeline"> 
												<i class="fa fa-calendar"></i> ${moment.unix(v.startDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
												<i class="fa fa-long-arrow-right"> </i> ${moment.unix(v.endDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
											</div>
										</div>
									</div>
									<div class="timeline-desc">`;
						if (jsonHelper.getValueByPath(v, "organizer") != undefined && Object.keys(jsonHelper.getValueByPath(v, "organizer")).length > 0) {
							var organizerId = Object.keys(jsonHelper.getValueByPath(v, "organizer"))[0];
							var organizerName = jsonHelper.getValueByPath(v, "organizer")[organizerId].name;
							hash = `#page.type.organizations.id.${organizerId}`;
						resultHtml += 	`<div> 
											<a href="${hash}" class="lbh-preview-element padding-5 timeline-prev"><span class="ico-circle"></span> ${organizerName}</a>
										</div>`
						}	
						resultHtml +=	`<p class="timeline-short-desc">
											${(v.shortDescription != undefined && !(v.shortDescription == "")) ? v.shortDescription : "<i>Description non renseignée</i>"}
										</p>`		
						if (v.tags != undefined && Array.isArray(v.tags) && v.tags.length > 0) {
						resultHtml += `<div class="tag-content">`;
							v.tags.forEach(function(tagContent) {
								resultHtml += `<p class="tag-element">#${tagContent}</p>`;
							})
						resultHtml += `</div>`;
						}	
						if (v._id.$id != undefined) {
							resultHtml += `
										<div class="link-bordered">
											<a href="#page.type.events.id.${v._id.$id}" class="lbh-preview-element">En savoir plus</a>
										</div>
									`;
						}
						resultHtml +=
									`</div>
								</div>
							</div>
						`;
						direction = (direction == "") ? "right" : "";
					}
				})
				return resultHtml;
			};
			return views;
		}

		function prepareAddBtnDropdown(config){
			let addBtnDropDown = `<div class="btn-group">
			<button class="btn btn-primary-ekitia btn-primary-outline-ekitia dropdown-toggle" type="button" data-toggle="dropdown">POSTER
			<span class="caret"></span></button>
			<ul class="dropdown-menu pull-right">`;
			$.each(config, function(index, type){
				/*if(type=="news"){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`#live`);" class="padding-10">'+(type)+'</a></li>';
				}else*/ if(typeof type=="object" && type!=null){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`'+type.value+'`);" class="padding-10">'+(type.name||type.label||"Formaulaire")+'</a></li>';
				}else{
					let label = type;
					if(type=="news"){
						type="poi"; label="Actualité"
					}
					if(type=="newsletter"){
						return;
					}
					if(label=="poi"){
						label=typeObj[label].name;
					}
					let t = ((typeObj[type] && typeObj[type].sameAs)?typeObj[type].sameAs:type);
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(``, `'+t+'`);" class="padding-10" data-form-type="'+t+'">'+label+'</a></li>';
				}
			});
			addBtnDropDown+="</ul></div>";
//co2/app/view/url/news.views.co.formCreateNews
			return addBtnDropDown;
		}

	})
</script>