<style type="text/css">
	/* #modal-preview-coop {
        display: none;
        position: fixed;
        padding-top: 100px;
        left: 0;
        top: 0 !important;
        width: 100%;
        height: 100%;
        overflow: hidden;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0,0.8);
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
    } */
	.preview-main-container{
        padding: 0px 40px;
    }

    .preview-main-container > div{
        margin-top:15px;
    }

    .btn-edit-preview:hover .show-hover, .btn-delete-element-preview:hover .show-hover{
        display: inline-block;
    }

    .show-hover{
        display:none;
    }
    /* .show-hover:hover{
        display:inline-block;
    } */
    
    .social-share-button img{
        margin-right: 10px;
    }

    .listsItemsPod{
        text-align:left;
        /* width: fit-content !important;
        float: left; */
        margin-left:30px;
    }

    .listsItemsPod a{
        margin-left: 10px;
    }

    .listsItemsPod h4{
        display:none;
    }

    .container-preview h3, 
    .container-preview h4, 
    .container-preview  h5{
        text-transform:none;
    }
    #modal-preview-coop h3, 
    .morelink, 
    .text-highlight, 
    .text-highlight li, 
    .text-highlight li a, 
    .pod-info-Address span, 
    #modal-interesting li{
        color:  var(--color4);
        font-size: 14px;
    }
    #modal-interesting a{
        color:  var(--color4);
        font-size: 20px;
    }
    .pod-info-Address{
        float:left;
        width:100%;
    }
    .container-preview h4, 
    .container-preview h5,
    .container-preview  .section-title,  .morecontent strong,  
    .morecontent li::marker{
        color : #1b3b53;
        font-weight:700;
        font-size:16px !important;
        text-transform: none;
    }
    .container-preview .section-title{
        color: #1b3b53;
        font-weight: 700;
        font-size: 19pt !important;
        text-transform: none;
        /*border-bottom: 5px solid #219c90;
        border-bottom-right-radius: 20px;*/
        padding-right: 20px;
        padding-bottom: 5px;
        width: max-content; 
    }
    .container-preview .section-title.no-border{
        border-bottom: none;

    }
    /* .section-title:first-letter{
        color:white;
        font-size:larger;
    } */

    .section-title:before{
        /* content: "";
        position: absolute;
        margin-top: -5px;
        z-index: -1;
        margin-left: -15px;
        width: 2em;
        height: 2em;
        background-color: var(--primary-color);
        border-radius: 50%;
        content: "";
        position: absolute;
        margin-top: -5px;
        z-index: -1;
        margin-left: -40px;
        width: 1.5em;
        height: 1.5em;
        background-color: var(--primary-color);
        border-radius: 5px; */
    }
    .section-content{
        margin-top:20px;
    }
    .section-title{
        margin-top:20px;
        color: var(--primary-color) !important;
    }
    p, .morecontent p, .morecontent ul, .morecontent li{
        color:#575756;
        font-size:16px !important;
    }
    .community-pod{
        /* display: inline-flex; */
    }
    #modalLogin, #modalRegister{
        z-index:200001;
    }
    /* .list-session li .session{
        font-size : 14px;
    } */
	@media screen and (min-width: 1360px) {
		.container-preview  .basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.container-preview  .basic-banner {
			background-size: 15% auto;
		}
	}
    .container-preview  .basic-banner {
		/* padding-top: 50px;
		padding-bottom: 100px;		 */
		background: var(--primary-color);
	}
    .container-preview .container-banner {
        display: flex;
        width: 100%;
        /* height: 250px; */
        background: #219c90;
		justify-content: center;
    }
    .container-preview .basic-info{
        display: flex;
        justify-content: start;
        align-items: end;
        padding-bottom: 40px;
    }
    .container-preview .basic-img{
        display: flex;
        justify-content: end;
        align-items: end;
        /* padding-right: 50px; */
        padding-bottom: 20px;
        padding-left: 60px; 
        text-align: right;
        padding-right: 40px;
    }
    .basic-img .box-img{
        background-color: #fff;
        border-radius: 6px;
        min-height: 160px;
        --shadow: #5ce4d6 0px 0px 0px 2px inset, #219c90 -10px -10px 3px -3px, #5ce4d6 -10px -10px, rgb(255, 255, 255) -20px -20px 0px -3px, #5ce4d6 -20px -20px, #219c90 -30px -30px 0px -3px, #5ce4d6 -30px -30px, rgb(255, 255, 255) -40px -40px 0px -3px, #5ce4d6 -40px -40px;
        /* --shadow: #5ce4d6 0px 0px 0px 2px inset, #219c90 -10px -10px 3px -3px, #5ce4d6 -10px -10px, rgb(255, 255, 255) -20px -20px 0px -3px; */
        box-shadow: var(--shadow);
        display: flex;
        padding: 10px;
        justify-content: center;
        align-items: center;
    }

    .basic-img img {
        width: auto;
        height: 150px;
        object-fit: contain;
		/* margin-left: -1em;
		width: 15em;
		height: 15em;
		border-radius: 10%;
        object-fit :cover; */
	}
    .title-banner h1{
        text-transform: none;
        font-size: 25px;
        color: white;
        border-bottom: 5px solid #fff;
        border-bottom-right-radius: 20px;
        padding-right: 20px;
        padding-bottom: 5px;
        /* text-align: right; */
        font-size: 30px;
    }
    .title-banner h1,
    .title-banner h1 a{
        text-transform : none;
        font-size : 25px;
        color : white;
    }
    @media (max-width: 768px){
        .container-preview .container-banner{
            height: 180px;
        }
        .basic-img .box-img{
            min-height: 80px;
        }
        .basic-img img {
            /* margin-left: -2em;
            width: 10em;
            height: 10em; */
            height: 80px;
        }
        .title-banner h1{
            font-size : 15px;
        }
        .container-preview  .basic-banner {
            /* padding-top: 20px;
            padding-bottom: 100px; */
        }

    }
	.left-preview-header{
		float:left;

	}

    .left-preview-header a:hover{
        font-style: italic;
  		font-weight: bold;
    }
    .header-address{
        font-size: 14px;
        font-weight: 900;
    }
    .section-content padding-left-10{
        align-items: baseline;
        margin-top: 30px;
        padding-left: 50px;
        padding-right: 50px;
    }
    .container-preview .content{
        font-size: 16px;
        padding-left: 15px;
    }
    .avatar-formation{
        object-fit: contain;
        width: 50px;
        height: auto;
    }

    .bg-dark{
        background-color: var(--secondary-color) !important;
    }

    .bg-primary-color{
        background-color: var(--primary-color) !important;
    }

    .text-primary-color{
        color: var(--primary-color) !important;
    }

    .text-bold{
        font-weight: bolder;
    }
    .toolsMenu{
        border-bottom: 0px;
    }
    .btn{
        font-size: medium !important
    }

    #modal-preview-coop {
        display: none; 
        position: fixed; 
        padding-top: 100px; 
        left: 0;
        top: 0 !important;
        width: 100%; 
        height: 100%; 
        overflow: hidden; 
        background-color: rgb(0,0,0); 
        background-color: rgba(0,0,0,0.8); 
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
        /* z-index: 300 !important; */
    }

    /* Modal Content */
    .modal-content {
        margin: auto;
        width: 70%;
    }

    .container-preview{
        border-start-start-radius: 5px;
        border-end-start-radius: 5px;
        background-color:white;
        z-index: 200002 !important;
    }
    .choose-date{
        background-color: white;
        margin-bottom: 10px;
        box-shadow: 3px 1px 5px 2px #e2e2e2;
        cursor: pointer;
        width: max-content;
        padding-right: 10px;
    }
    .choose-visio-date i{
        padding: 10px;
        background: #01305e;
        color: white;
    }
    #modal-interesting .modal-content{
        width: 100%;
    }

	.blockFontPreview {
		font-size: 14px;
		color: #ec5d85;
	}

	h2 {
		font-size: unset;
	}

	#description{
		text-align: justify;
  		text-justify: inter-word;
	}

	#socialNetwork {
		font-size: 20px;
	}

	div.community-text {
		font-size: 21px;
    	margin-top: 2vh;
		text-align: justify;
	}

	.media-body {
		width: auto !important;
	}

	.m-title-desc {
		margin: auto 8vh;
		margin-top: 1em;
	}

	.mx-1 {
		margin: auto 1vh;
	}

	.space-top-prev {
		margin-top: 2vh;
	}

	.faw-size {
		font-size: xx-large;
	}

	.flex-disp, #socialNetwork {
		display: flex;
		justify-content: center;
	}

	.font-21 {
		font-size: 21px;
	}

	.btn-network {
		display: inline-block;
		width: 50px;
		height: 50px;
		background: #fff/*#f1f1f1*/;
		margin: 5px;
		border-radius: 30%;
		color: #1da1f2;
		overflow: hidden;
		position: relative;
		border: 1px solid #1d3b58;
	}

	.btn-network i {
		line-height: 50px;
		font-size: 25px;
		transition: 0.2s linear;
	}


	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	.basic-banner {
		padding-top: 80px;
		padding-bottom: 100px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.event-banner{
		padding-top: 20px;
		padding-bottom: 20px;
		background-color: var(--primary-color);
		color:white;
	}

	.bg-ekitia{
		background-color: var(--primary-color);
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	/* .container-banner:before {
		display: table;
		content: " ";
	} */

	.text-center {
		text-align: center;
	}

	/* .container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	} */

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
		.community-text img {
			 position:absolute;
			 border-radius: 5px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	.basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

	.title-banner h1::before {
		content: "";
		position: absolute;
		/* left: 37%; */
		left: 40%;
		top: 100%;
		/* bottom: 50%; */
		z-index: -1;
		margin-left: -1em;
		width: 6em;
		height: 6em;
		background-image: url("<?php echo (!empty($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/".($type!="citoyens"?"thumbnail-default.jpg":"thumb/default_citoyens.png")) ?>");
		background-repeat: no-repeat;
		background-color: transparent;
		background-position-x: center;
		background-position-y: center;
		background-size: contain;
		border-radius: 0%;
	}

	.title-section h3::before {
		content: "";
		position: absolute;
		top: -13px;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.title-section {
		position: relative;
	}

	.font-weight-100 {
		font-weight: 100;
	}

	.title-banner {
		position: sticky;
	}

	.m-title-contact {
		margin: auto 8vh;
    	margin-top: 2vh;
	}

	.dv-contact-content {
		margin-top: 3vh;
		margin-bottom: 10vh;
	}
    .wrap-ekitia-btn-inscrire{
        margin-right: 20px;
        margin-top: 20px;
    }
    .wrap-ekitia-btn-inscrire .ekitia-btn-inscrire{
        background: #ba1c24;
        color: white;
        padding-top: 8px;
        padding-left: 40px;
        padding-right: 40px;
        padding-bottom: 8px;
        font-size: 18px;
        border: 2px solid transparent;
    }
    .wrap-ekitia-btn-inscrire .ekitia-btn-inscrire:hover{
        color: #ba1c24;
        border: 2px solid #ba1c24;
        background: transparent;
    }

    @media (max-width: 628px) {
        .media-body {
            width: auto !important;
            display: flex;
            margin-top: 20px
        }

        .media-object {
            margin: auto 20%;
            width: 200px !important;
            height: 200px !important;
        }

        .media {
            margin: 4vh 8vh auto;
        }

        p.community-text {
            font-size: initial;
            margin-top: 6vh;
        }

        .media-left {
            display: inherit !important;
        }

        .space-top-prev:first-child {
            margin-top: 8vh;
        }
        .space-top-prev {
            margin-top: 2vh;
        }
        .contact-title-content{
            margin-bottom: 10vh;
        }
        .title-banner h1::before{
            height: 5em;
            width: 5em;
            left: 45%;
        }
        .m-title-contact {
            margin: 2vh 4vh auto;
        }
    }
    @media screen and (max-width: 567px){
        .media {
            margin: 12vh 4vh auto;
        }
        .m-title-desc {
            margin: 14vh 4vh auto;
        }
    }
</style>

<?php
$cssAnsScriptFilesModule = array(
	'/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
$previewConfig = @$this->appConfig["preview"];
$auth = (Authorisation::canEditItem(@Yii::app()->session["userId"], $type, $element["_id"])) ? true : false;

$iconColor = (isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
$descriptionPrev = (isset($element["description"])) ? $element["description"]: (isset($element["shortDescription"]) ? $element["shortDescription"] : "<i>Il n'y a pas de description pour le moment.</i>") ;
$descriptionPrev = preg_replace('/\*\*(.*?)\*\*/', '<strong>$1</strong>', $descriptionPrev);
$descriptionPrev = preg_replace('/###(.*?)/', '<h2>$1</h2>', $descriptionPrev);
$descriptionPrev = preg_replace('/_(.*?)_/', '<em>$1</em>', $descriptionPrev);
$phone = (isset($element["telephone"]["fixe"][0])) ? ((isset($element["telephone"]["mobile"][0])) ? $element["telephone"]["mobile"][0] : $element["telephone"]["fixe"][0]) : "";
$userId = Yii::app()->session["userId"]??null;

if(isset($element["telephone"]) && is_string($element["telephone"])){
	$phone=$element["telephone"];
}

$elementAdmin=Element::getCommunityByTypeAndId($element["collection"],(string)$element["_id"],Person::COLLECTION,"isAdmin");
// var_dump($elementAdmin);
// var_dump($element["collection"]);exit;
// $type=($element["collection"]=="organizations" && $element["mainTag"]=="TiersLieux") ? "tiersLieux" : $type;
$subtype=($element["collection"]=="organizations" && isset($element["mainTag"]) && $element["mainTag"]=="TiersLieux") ? "tiersLieux" : "";

$isAdmin = Authorisation::isElementAdmin($element["_id"], $element["collection"], Yii::app()->session["userId"], false, false) || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]);

if(!isset($costum)){
	$costum = CacheHelper::getCostum();
}

?>
<div class="modal-content">
	<div id="preview-elt-<?php echo $type ?>-<?php echo (string)$element["_id"] ?>" class="fit-modal-content col-xs-12 no-padding">
		<div class="col-xs-12 no-padding margin-bottom-10 toolsMenu">
			<div class="left-preview-header">
				<?php if($element["collection"]!="citoyens" ){ ?>
					<a href="#@<?php echo @$element["slug"] ?>" class="lbh btn pull-left bg-primary-color text-white margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
				<?php }?>
			</div>	
			<div class="right-preview-header">
				<button class="btn btn-default pull-right btn-close-preview">
					<i class="fa fa-times"></i>
				</button>
				<?php if( $element["collection"]!=="citoyens" && ($auth || Authorisation::canEdit($userId, (string)$element["_id"], "events")|| Authorisation::isCostumAdmin())){ ?>
					<a href="javascript:;" class="btn btn-primary pull-right margin-right-10 btn-edit-element-ekitia" data-type="<?php echo $type ?>" data-subtype="<?php echo $subtype ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Modifier">
						<i class="fa fa-pencil"></i>
				</a>
					<button class="btn btn-danger pull-right margin-right-10 btn-delete-element-ekitia" data-type="<?php echo $type ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Supprimer">
						<i class="fa fa-trash"></i>
					</button>
				<?php } ?>
				<?php if($type!==Person::COLLECTION && $type!==Event::COLLECTION && !isset($element["links"]["members"][Yii::app()->session["userId"]]) && empty($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])){ ?>
					<a href="javascript:links.connect('<?=(string)$element["collection"]?>', '<?=(string)$element["_id"]?>', '<?=Yii::app()->session["userId"]?>', 'citoyens', 'member');" class="btn btn-primary pull-right margin-right-10" data-type="<?php echo $type ?>" data-subtype="<?php echo $subtype ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Modifier">
						<i class="fa fa-user-plus"></i> Devenir membre
				</a>
					
				<?php } else if($type==Event::COLLECTION && !isset($element["links"]["members"][Yii::app()->session["userId"]]) && empty($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])){ ?>
					<a href="javascript:links.connect('<?=(string)$element["collection"]?>', '<?=(string)$element["_id"]?>', '<?=Yii::app()->session["userId"]?>', 'citoyens', 'attendee');" class="btn btn-primary pull-right margin-right-10" data-type="<?php echo $type ?>" data-subtype="<?php echo $subtype ?>" data-id="<?php echo (string)$element["_id"] ?>" title="Modifier">
						<i class="fa fa-user-plus"></i> S'inscrire
				</a>
					
				<?php }elseif(isset($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])){?>
					<span><i class="fa fa-hourglass"></i> Demande pour devenir membre en cours de validation</span>

				<?php } 
				if(($element["collection"]=="citoyens") && $userId==(string)$element["_id"]){ ?>
					<button class="btn btn-primary btn-update-user-info pull-right margin-right-10" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" title="Modifier">
						<i class="fa fa-pencil"></i>
					</button>
				<?php } ?>

				
				<?php
				if (isset($previewConfig["toolBar"]["edit"]) && $previewConfig["toolBar"]["edit"] && $auth) { ?>
					<button class="btn btn-default pull-right margin-right-10 btn-edit-preview" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo $element["type"] ?>">
						<i class="fa fa-pencil"></i>
					</button>
				<?php } ?>
			</div>
		</div>
		<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
			
			<?php if(($element["collection"] == Organization::COLLECTION) && $auth && !(isset($element["preferences"]["toBeValidated"][$this->costum["slug"]]) || (isset($element["reference"]["costum"]) && in_array($this->costum["slug"], $element["reference"]["costum"])) || (isset($element["source"]["keys"]) && in_array($this->costum["slug"], $element["source"]["keys"])))){ ?>
				<div class="text-right wrap-ekitia-btn-inscrire">
					<button class="ekitia-btn-inscrire">Rejoindre le réseau Lakou</button>
				</div>
			<?php }?>

			<?php 
                if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"]))){
                    // $url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
                    $url= Yii::app()->theme->baseUrl . '/assets/img/background-onepage/connexion-lines.jpg';
                }else{
                    $url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
                }    
                ?> 
                <!-- div class="banner-outer ">
                    <div class="basic-banner">
                        <div class="basic-banner-inner">
                            <div class="container-banner"> 
                                <div class="basic-img">
                                    <div class="box-img"> 
                                        <img src="<?php echo (isset($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>" alt="">
                                    </div>
                                   </div>
                                <div class="basic-info">
                                    <div class="title-banner">
                                        <?php if($element["collection"]==Event::COLLECTION) {  
                        
                                            if(isset($element["links"]["organizer"])){
                                                foreach($element["links"]["organizer"] as $idorg => $valorg){
                                                    if($valorg["type"]==Project::COLLECTION){
                                                        $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                                                        $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                                                        break;
                                                    }
                                                }
                                            
                                            } ?> 
                                            <h1 class="font-weight-100">
                                                <?php 
                                                    if(!empty($breadCrumbFormation)){
                                                        echo $breadCrumbFormation;
                                                    }
                                                    echo $element["name"] 
                                                ?>   
     
                                                   </h1> 
                                        <?php } else { ?>
                                            <h1 class="font-weight-100"><?php echo $element["name"]??"" ?></h1>
                                        <?php } ?>
                                    </div>					
                                </div>
                            </div>
                        </div>
                    </div>
                </div -->
     
                       <div class="padding-left-15 padding-right-5">
                    <img src="<?php echo (isset($element["profilMediumImageUrl"]) && !empty(Yii::app()->createUrl('/' . $element["profilMediumImageUrl"])) ? Yii::app()->createUrl('/' . $element["profilMediumImageUrl"]) : Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg") ?>" alt="Image" style="width:100%;object-fit:cover;height:250px;border-radius:4px" />
                </div>
                <div class="text-dark padding-15" style="font-size: 22pt; font-weight:bolder">
                    <?php echo $element["name"]; ?>
                </div>
                <div class="col-xs-12 margin-bottom-20 ">
                    <?php if($element["collection"]==Event::COLLECTION) { 
                        if(isset($element["links"]["organizer"])){
                            foreach($element["links"]["organizer"] as $idorg => $valorg){
                                if($valorg["type"]==Project::COLLECTION){
                                    $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                                    $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                                    break;
                                }
                            }
                           
                        }
                        $descr= (@$element["description"]) ? $element["description"] : ""; 
                        ?>
                        <div class="section-content padding-left-10">
                            <h4 class="section-title" style="font-weight:700;">Description</h4>
                            <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
                        </div>
                        <div class="section-content padding-left-10" id="public">
                            <?php $public=(isset($element["preferences"]["private"]) && $element["preferences"]["private"]==false) ? "Publique" : "Privée" ;?>
                            <span class="section-title  no-border">Visibilté dans l'annuaire public des sessions :</span> <span class="text-highlight content"><?=$public?></span>
                            
                        </div>
                        <div class="no-padding" id="dates" style="">
                            <?php 
                            $format = "d M Y"; //H:i
                            $startDate=date_format(date_create($element["startDate"]), $format);
                            $endDate=date_format(date_create($element["endDate"]), $format);
                            //var_dump($element["startDate"]);exit;
                            ?>
                                <div class=" section-content padding-left-10"> 
                                    <span class="section-title no-border">Date de début de session :</span>
                                    <span class="text-highlight startDate content"><?= date('d/m/Y à H:i', strtotime(str_replace("/","-",$element["startDate"])))?></span>
                                </div>
                                <div class="section-content padding-left-10"> 
                                    <span class="no-padding section-title  no-border">Date de fin de session :</span>
                                    <span class="text-highlight endDate content"><?= date('d/m/Y à H:i', strtotime(str_replace("/","-",$element["endDate"])))?></span>
                                </div>
                            <?php if(isset($element["duration"])){ ?>
                                <div class="section-content padding-left-10"> 
                                    <span class=" section-title  no-border">Durée de la session : </span>
                                    <span class="text-highlight content"><?=$element["duration"]?> jours</span>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if(!empty($element["email"])){?>
     
                                   <div class="section-content padding-left-10" id="email"> 
                                <span class="no-padding section-title  no-border">Email de contact: </span>
                                <span class="text-highlight content"><?=$element["email"]?></span>
                            </div>
                        <?php } ?>
                        <?php if(!empty($visioDates)){?>
                            <div class="section-content padding-left-10" id="url" style="margin-bottom: 10px;">
                                <?php ?>
                                <p class="section-title">Date des visioconférences d'information
                                <?php 
                                    echo "<ul>";                 
                                        foreach($visioDates as $i=>$date){
                                            echo '<span class="text-highlight content"><li>Le '.date('d/m/Y à H:i', strtotime(str_replace("/","-",$date))).'</li> </span>';
                                        }
                                    echo "</ul>"   
                                
                                ?>
                                </p>
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["gatheringDates"])){?>
                            <div class="section-content padding-left-10" id="url" style="margin-bottom: 10px;">
                                <?php ?>
                                <p class="section-title">Les journées de formation
                                    <?php 
                                        echo "<ul>";                 
                                            foreach($element["gatheringDates"] as $i=>$date){
                                                // var_dump(str_replace("/","-",$date),strtotime($date),strtotime(str_replace("/","-",$date)));
                                                echo '<span class="text-highlight"><li>Le '.date('d/m/Y', strtotime(str_replace("/","-",$date))).'</li> </span>';
                                            }
                                        echo "</ul>"   
                                    
                                    ?>
                                </p>
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["url"])){?>
                            <div class="section-content padding-left-10" id="url">
                                <span class="section-title">Lien vers le formulaire d'inscription </span><span class="text-highlight content"><?=$element["url"]?></span>
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["links"]["attendees"])){ ?>
                            <div class="section-content padding-left-10 community-pod" id="organizer-pod">
                                <p class="section-title">Les participants  </p>
                                <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les participants", "links"=>$element["links"]["attendees"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                                
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["trainer"])){?>
                            <div class="section-content padding-left-10 community-pod" id="trainer-pod" >
                                <p class="section-title">Les intervenants  </p>
                                <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                                
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["partner"])){?>
                            <!-- <div class="col-xs-6 community-pod" id="organizer-pod" style="margin-bottom: 10px;">
                            <p class="col-xs-12 no-padding pull-left section-title">Les partenaires : </p>
                                <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                                
                            </div> -->
                        <?php }  
                        
                        ?>
                        <?php if(!empty($element["address"])){?>
                            <div class="col-md-10 col-xs-12 section-content padding-left-10" id="address" style="margin-bottom: 100px;">
                                <p class="section-title">Adresse  </p>
                                <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                                
                            </div>
     
                               <?php } ?>    
                    
                    <?php } else if($element["collection"]==Project::COLLECTION) {  
                        
                        if(isset($element["links"]["organizer"])){
                            foreach($element["links"]["organizer"] as $idorg => $valorg){
                                if($valorg["type"]==Project::COLLECTION){
                                    $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                                    $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                                    break;
                                }
                            }
                           
                        }
                        // var_dump($formation);exit;
                        
                        // $formation=PHDB::findOne(Project::COLLECTION,array());
                        $descr= (@$element["description"]) ? $element["description"] : ""; 
                        ?>
                        <h3 class="col-xs-12 text-primary-color padding-left-10" style="font-weight:700;font-size:19pt">Contenu, objectifs pédagogiques</h3>
                        <div class="content more" id="descriptionAbout" style="margin-top:none; font-size:14pt">  <?php echo $descr ?> </div>
     
                               <?php if (isset($element["parent"])){ ?>
                            <div class="section-content padding-left-10" style="margin-bottom: 10px;"> 
                                <p class="section-title">Organismes de formation  </p>
                                <?php foreach($element["parent"] as $k => $v){ ?>
                                    <a href="#page.type.<?php echo $v["type"] ?>.id.<?php echo $k; ?>" class="hover-underline lbh" data-toggle="popover" title="<?php echo $v["name"] ?>">
                                        <img width="50" height="50"  alt="image" src="<?php echo $v["profilThumbImageUrl"] ?>">
                                    </a>
                                <?php } ?>
                            </div>  
                        <?php } ?>
     
                               <?php if (isset($element["urlsDoc"])){ ?>
                            <div class=" section-content padding-left-10" id="urlsDoc" style="margin-bottom: 10px;"> 
                                <?php 
                                $urlsDoc="<ul class ='text-highlight' >";
                                foreach($element["urlsDoc"] as $ind=>$l){
                                    if($l != ""){
                                        $urlsDoc.="<li><a href='".$l."' target='_blank' class='content'>".$l."</a></li>";
                                    }
     
                                       } 
                                $urlsDoc.="</ul>";
                                ?>
                                <p class="section-title">Liens vers la documentation, le programme  <span class="text-highlight content"><?= $urlsDoc ?></span></p>
                            </div>
                        <?php } ?>
                        <?php if (isset($element["rate"])){ ?>
                            <div class="section-content padding-left-10" id="rate" style="margin-bottom: 10px;">
                                
                                <p class="section-title">Tarif :</p>
                                <div class="content"><?=$element["rate"]?> euros </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($element["email"])){ ?>
                            <div class="section-content padding-left-10" id="email" style="margin-bottom: 10px;">
                                
                                <p class="section-title">Pour plus d'informations : <span class="text-highlight"><?=$element["email"]?></span></p>
                            </div>
                        <?php } ?>
                        <?php if (isset($element["trainer"])){ ?>
                            <!-- <div class="col-xs-6 community-pod" id="trainer-pod" style="margin-bottom: 10px;">
                            <p class="col-xs-12 no-padding pull-left section-title">Les intervenants : </p>
                                <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                                
                            </div> -->
                        <?php } ?>
                        <?php if (isset($element["partner"])){ ?>
                            <div class="col-xs-12 community-pod section-content padding-left-10" id="organizer-pod" style="margin-bottom: 10px;">
                                <p class="col-xs-12 no-padding pull-left section-title">Les partenaires : </p>
                                <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
                                
                            </div>
                        <?php } ?>
                        <?php if(!empty($element["address"])){?>
                            <div class="section-content padding-left-10" id="address" style="margin-bottom: 10px;">
                                <p class="pull-left section-title">Adresse  </p>
                                <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                            </div>
     
                               <?php } ?>   
                        <div class="col-xs-12 list-session section-content padding-left-10"></div> 
                    
                    <?php }  ?>
                </div>
                <?php if($element["collection"]==Organization::COLLECTION){ ?>
                    <div class="preview-element-info col-xs-12">
                        <?php 
                        if (isset($element["socialNetwork"])){?>
                            <div id="socialNetwork" class="col-xs-10 section-content padding-left-10 margin-top-10 text-center">
                                <?php 
                                    if (isset($element["socialNetwork"]["facebook"])){ ?>		
                                    <span id="divFacebook" class="margin-right-10">
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"];?>" target="_blank" id="facebookAbout" >
                                            <i class="fa fa-facebook" style="color:#1877f2;"></i>
                                        </a>
                                    </span>
                                <?php }
                                    
                                if (isset($element["socialNetwork"]["twitter"])){ ?>	
                                    <span id="divTwitter">
                                        <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"];?>" target="_blank" id="twitterAbout" >
                                        <i class="fa fa-twitter" style="color:#1da1f2;"></i>
                                        </a>
                                    </span>
                                <?php } ?>
                            </div>  
                        <?php
                        }						
                        if(isset($element["description"])) { 
                            $descr= (@$element["description"]) ? $element["description"] : ""; 
                            ?>
                            <div class="section-content padding-left-10">
                                <h2 class="section-title"> Description</h2>
                                <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
                            </div>
                        <?php } ?>
                        <?php if(isset($element["tags"]) && in_array("Organisme de formation",$element["tags"]) && isset($element["trainingQualification"]))	{ ?>
                            <div class="section-content padding-left-10 "> 
                                <h2 class="section-title"> Qualification de la formation</h2>
                                <?php $trainingQualification = explode(",", $element["trainingQualification"]); ?>
                                <ul class="text-highlight">
                                    <?php foreach ($trainingQualification as $k => $v){
                                        echo '<li class="content">'.$v .'</li>';
                                    } ?>
                                </ul>
                            </div>
                        <?php } ?>	
                            
                        <?php
                        if(isset($element["url"])){ ?>
                            <div class="section-content padding-left-10">
                                <h2 class="section-title"> Url principale</h2>
                                <?php $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ; ?>
                                <a class="content" href="<?php echo $scheme.$element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo $element["url"] ?></a>
                            </div>
                        <?php } ?>	
                        <?php if(isset($element["tags"]))	{                 
                            $tabSpec=array(
                                "typePlace"=>
                                    [
                                        "name"=>"Typologie d'espace de travail",
                                        "icon"=>"briefcase"
                                    ],
                                "services"=>
                                    [
                                        "name"=>"Services proposés",
                                        "icon"=>"tags"
                                    ],
                                "manageModel"=>	
                                    [
                                        "name"=>"Mode de gestion",
                                        "icon"=>"adjust"
                                    ],
                                "state"=>	
                                    [
                                        "name"=>"Etat du projet",
                                        "icon"=>"cubes"
                                    ],
                                "spaceSize"=>	
                                    ["name"=>"Taille du lieu",
                                    "icon"=>"expand"
                                    ],
                                "certification"=>	
                                    [
                                        "name"=>"Label obtenu",
                                        "icon"=>"medal"
                                    ],
                                "greeting"=>	
                                    [
                                        "name"=>"Actions d'accueil",
                                        "icon"=>"info"
                                    ],
                                "network" =>	
                                    [
                                        "name"=>"Réseau d'affiliation",
                                        "icon"=>"users"
                                    ]
                            );
                            ?>			
                            <div class="section-content padding-left-10"> 
                                <?php if(in_array("TiersLieux",$element["tags"])){ ?>	
                                    <h2 class="section-title">Caractéristiques du lieu</h2>
                                <?php } else if(in_array("Organisme de formation",$element["tags"])){ ?>	
                                    <h2 class="section-title">Mot clef</h2>
                                <?php } ?>
                                <table class='col-xs-12'>
                                    <?php	
                                        foreach($tabSpec as $k => $v){ 
     
                                                   $strTags="";
                                            $tagsList="";
                                            $listCurrent = [];
                                            if(isset( $this->costum["lists"][$k])){
                                                $listCurrent=$this->costum["lists"][$k];
                                            }
                                            if(!empty($element["tags"])){
                                                foreach($element["tags"] as $tag){
                                                    if(in_array($tag, $listCurrent)){
                                                        $strTags.=(empty($strTags)) ? $tag : "<br>".$tag; 
                                                    }
                                                    
                                                    
                                                }
     
                                                   }
                                            if(!empty($strTags)){ ?> 
                                                <tr>
                                                    <td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
                                                        <?php  
                                                        if(isset($v["icon"])){
                                                            echo "<i class='fa fa-".$v["icon"]." tableIcone'></i>";
                                                        }
                                                        ?>
                                                    </td>	
                                                    <td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
                                                        <?php  
                                                        if(isset($v["name"])){
                                                            echo $v["name"];
                                                        }
                                                        
                                                        ?>		
                                                    </td>
                                                    <td class="col-xs-7" style="padding:8px;"><?php echo $strTags; ?></td>
                                                </tr>	 
                                            <?php 
                                            }
                                        }?>
                                </table>	                                
                                <?php  foreach($element["tags"] as $tag) {	
                                    if(!in_array($tag, $listCurrent)) {
                                        $tagsList.= "#".$tag." ";	
                                    }
                                }?>
                                <span class="tagList content"><?php echo $tagsList ?></span>
                            </div>	
                        <?php } ?>	
                        <div class="section-content padding-left-10 " id="list-formation"> </div> 
                        <?php if(!empty($element["address"])){?>
                            <div class="section-content padding-left-10" id="address">
                                <h2 class="section-title"> Adresse  </h2>
                                <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
                            </div>
                        <?php } ?>   
     
                               <div class="section-content padding-left-10">
                            <h2 class="section-title">Les membres</h2>
                            <?php 
                            if(isset($element["links"]) && isset($element["links"]["members"])){ 
                                echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"]));
                            } else{?>
								<div><span>Aucun membre rattaché</span></div>
							<?php } ?>
                        </div> 
                            
                    
                    </div>
                <?php } ?>
                <?php if($element["collection"]==Person::COLLECTION){ ?>
                    <div class="preview-element-info col-xs-12">
                        				
                        <?php if(isset($element["description"])) { 
                            $descr= (@$element["description"]) ? $element["description"] : ""; 
                            ?>
                            <div class="section-content padding-left-10">
                                <h2 class="section-title"> Description</h2>
                                <div class="content more no-padding" id="descriptionAbout" style="margin-top: none;">  <?php echo (@$element["description"]) ? $element["description"] : ''; ?></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
     
       			
		</div>
</div>
	<script type="text/javascript">
		var eltPreview = <?php echo json_encode($element); ?>;
		var typePreview = <?php echo json_encode($type); ?>;
		var idPreview = <?php echo json_encode($id); ?>;
		jQuery(document).ready(function() {
			setTimeout(function(){ 
				var modalScreen=$("#modal-preview-coop").height() 
				var modalHeader=$("#modal-preview-coop .toolsMenu").outerHeight(true) 
				$("#modal-preview-coop .container-preview").height(modalScreen-modalHeader)
				$("#modal-preview-coop .container-preview").css("padding-bottom","30px")
			}, 1000);

			$("#modal-preview-coop").mouseup(function(e){
                var container = $("#modal-preview-coop .fit-modal-content");
                if (!container.is(e.target) && container.has(e.target).length === 0){
                    $(".btn-close-preview").trigger("click");
                }
            });

            var str = directory.getDateFormated(eltPreview, null, true);
			var reference = {
				load: function() {
					this.manageSourceData();
				},
				manageSourceData : function(){
					mylog.log("bindReferenceBtnEvents");
					$(".setSourceAdmin").off().on("click", function(){
						var $btnClick=$(this);
						if($btnClick.data("setkey")=="source" && $btnClick.data("action")=="remove"){
							bootbox.confirm("BE carefull, remove a source from an element is not revokable !!<br/>Are you sure to continue ?", function(result) {
								if (result) {
									reference.setSourceDataAction($btnClick);
								}
							});
						}else{
							reference.setSourceDataAction($btnClick);
						}	
					});
				},
				setSourceDataAction : function($btnClick){
					var action=$btnClick.data("action");
					var setKey=$btnClick.data("setkey");
					var params={
						id:$btnClick.data("id"),
						type:$btnClick.data("type")
					};
					if(typeof costum != "undefined" && notNull(costum)){
						params.origin="costum";
						params.sourceKey=(typeof costum.isTemplate !="undefined" && costum.isTemplate) ? costum.contextSlug : costum.slug;
					} 
					ajaxPost(
						null,
						baseUrl+"/"+moduleId+"/admin/setsource/action/"+action+"/set/"+setKey,
						params,
						function(data){ 
							if ( data && data.result ) {
								toastr.success(data.msg);
								$(".btn-ref-" + params.id).fadeOut();
							} else {
							toastr.error("something went wrong!! please try again.");
							}

						}
					);
				}
			};
			if(eltPreview.startDate){
				$("#eventDate").append(`
					<div>${moment(eltPreview.startDate).locale("fr").format("DD/MM/YYYY HH:mm")}</div>
				`);
			}

			$(".btn-delete-element-ekitia").off().on("click", function () {
				$("#modal-preview-coop").hide(300);
				$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
				directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), function(){
					urlCtrl.loadByHash(window.location.hash);
				});
			});

			$(".btn-edit-element-ekitia, .btn-edit-element").off().on("click", function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				$("#modal-preview-coop").hide(300);
				$("#modal-preview-coop").html("");
				var idEl=$(this).data("id");
				var typeEl=$(this).data("type");
				var subtypeEl=$(this).data("subtype");
				var dynformTL=(subtypeEl=="tiersLieux") ? costum.typeObj.tiersLieux.dynFormCostum : null;
				if(notEmpty(dynformTL)){
				    dynformTL.onload.actions.hide.similarLinkcustom=1;
				}	
				// typeEl=(typeof typeObj[typeEl].sameAs!="undefined" && typeEl!="tiersLieux") ? typeObj[typeEl].sameAs : typeEl;
				dyFObj.editElement(typeEl,idEl,subtypeEl,dynformTL,{},"afterLoad");
				// let thisEl = <?php echo json_encode($element); ?>;

				// if(thisEl && thisEl.startDate){
				// 	thisEl.startDate = moment(thisEl.startDate).locale("fr").format("DD/MM/YYYY HH:mm")
				// }
				// if(thisEl && thisEl.endDate){
				// 	thisEl.endDate = moment(thisEl.endDate).locale("fr").format("DD/MM/YYYY HH:mm")
				// }
				// let defautDyFInputsInit = dyFInputs.init;
                // dyFInputs.init = function(){
                //     typeObj[thisEl.collection.substring(0, thisEl.collection.length - 1)].dynForm.jsonSchema.properties.shortDescription = dyFInputs.textarea(tradDynForm.shortDescription, "...", {})
                //     typeObj[thisEl.collection.substring(0, thisEl.collection.length - 1)].dynForm.jsonSchema.properties.description = dyFInputs.textarea(tradDynForm.shortDescription, "...", {})
                //     defautDyFInputsInit()
                // }
				// dyFObj.editMode = true;
				// dyFObj.editElement(thisEl.collection.substring(0, thisEl.collection.length - 1), null, {...thisEl, id:$(this).data("id")});
			});

			$(".btn-update-user-info").off().on("click", function() {
				$("#modal-preview-coop").hide(300);
				$("#modal-preview-coop").html("");
				var form = {
					saveUrl: baseUrl + "/" + moduleId + "/element/updateblock/",
					dynForm: {
						jsonSchema: {
							title: "Mettre à jour vos informations",
							properties: {
								"fonction": {
									"label" : "Fonction"
								},
								"telephone": {
									"label" : "Téléphone",
									"inputType": "phone"
								},
								"email": {
									"label" : "Email",
									"inputType": "email"
								},
								"description": {
									"label" : "Description",
									"inputType": "textarea"
								}
							},
							save : function(data){
								delete data.collection;
								delete data.scope;
								
								let dataToUpDate = {
									id: contextData.id,
									collection: "citoyens",
									path:"allToRoot",
									value : data
								}
								dataHelper.path2Value(dataToUpDate, function(params) {
									urlCtrl.loadByHash(window.location.hash)
								});
							}
						}
					}
				}

				var dataUpdate = {
					"fonction" : contextData.fonction,
					"telephone" : contextData.telephone,
					"description" : contextData.description,
					"email" : contextData.email
				}
				dyFObj.openForm(form, null, dataUpdate);
			});

			reference.load();
			$(".event-infos-header").html(str);
			directory.bindBtnElement();

            $(".ekitia-btn-inscrire").off().on("click", function(){
                if((typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin)){
                    sendReferences();
                }else{
                    bootbox.confirm("Vous essayez de rejoindre la communauté Lakou. Êtes-vous sûr.e ? ", function(result){
                        if(result){
                            sendReferences();
                        }
                    })
                }
            });
		});
        function sendReferences(){
            ajaxPost(
                null,
                baseUrl+"/costum/essconnect9711/references",
                {
                    elementType: typePreview,
                    elementId: idPreview,
                    isAdmin: (typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin)
                },
                function(result){
                    if(result.result){
                        dyFObj.closeForm();
                        var toastText = (typeof costum.communityLinks != "undefined" && typeof costum.communityLinks.members != "undefined" && typeof costum.communityLinks.members[userId] != "undefined" && typeof costum.communityLinks.members[userId].isAdmin != "undefined" && costum.communityLinks.members[userId].isAdmin) ? "Structure ajoutée comme membre du réseau Lakou" : "Votre demande pour rejoindre la communauté Lakou a été envoyée aux administrateurs";
                        toastr.success(toastText);
                        $(".btn-close-preview").click();
                    }
                }
            );
        }
	</script>