
<style type="text/css">
    .simple-pagination li a, .simple-pagination li span {
        border: none;
        box-shadow: none !important;
        background: none !important;
        color: #2C3E50 !important;
        font-size: 16px !important;
        font-weight: 500;
    }
    .simple-pagination li.active span{
        color: #d9534f !important;
        font-size: 24px !important;
    }
    .list-group-item {
        padding: 0;
        transition: .3s all ease;
    }
    .list-group-item a {
        display: block;
        padding: 10px 15px;
        width: 100%;
    }
    .list-group-item a:focus,.list-group-item.activeTab a:focus {
        text-decoration: none !important;
    }
    .list-group-item.activeTab{
        background-color: #93c020;
    }

</style>
<div class="divFilteringReference col-xs-12 no-padding">
    <ul class="col-xs-12 list-group no-padding">
        <li class="col-xs-4 list-group-item activeTab">
            <a href="javascript:;" class="sourceFilter" style="cursor:pointer;">
                <i class="fa fa-at "></i>
                <span class="hidden-xs"><?php echo Yii::t("admin", "Créé.e.s sur ".$this->costum['title']); ?></span>
                <span id="admin-count-source" class="badge"><?php echo (@$countMenu && @$countMenu["source"]) ? $countMenu["source"]: 0 ; ?></span>
            </a>
        </li>
        <li class="col-xs-4 list-group-item">
            <a href="javascript:;" class="referenceFilter" style="cursor:pointer;">
                <i class="fa fa-link "></i>
                <span class="hidden-xs"><?php echo Yii::t("admin", "Reference"); ?></span>
                <span id="admin-count-reference" class="badge"><?php echo (@$countMenu && @$countMenu["reference"]) ? $countMenu["reference"]: 0 ; ?></span>
            </a>
        </li>
        <li class="col-xs-4 list-group-item">
            <a href="javascript:;" class="searchOpenFilter" style="cursor:pointer;">
                <i class="fa fa-search "></i>
                <span class="hidden-xs"><?php echo Yii::t("admin", "search & reference"); ?></span>
            </a>
        </li>
    </ul>
</div>
<div id="contentReferenceResults" class="panel panel-white col-xs-12 no-padding">
</div>
<script type="text/javascript">

    var initType = <?php echo json_encode($typeDirectory) ?>;
    var originCross=<?php echo json_encode($originCross) ?>;
    var sourceKey=<?php echo json_encode($sourceKey) ?>;
    var filters = <?php echo  json_encode($filters)?>;
    var tableSource = {name : "source"};
    jQuery(document).ready(function() {
        startReferenceSearch("source");
        initMenuReference();
    });
    function initMenuReference(){

        //$(".list-group-item:first-child").css("background","#93c020");
        $(".sourceFilter").off().on("click", function(){
            //searchAdmin.mode="source";
            startReferenceSearch("source");
            $(".list-group-item").removeClass('activeTab');
            $(this).parent().addClass('activeTab');
        });
        $(".referenceFilter").off().on("click", function(){
            startReferenceSearch("reference",tableSource);
            $(".list-group-item").removeClass('activeTab');
            $(this).parent().addClass('activeTab');
        });

        $(".searchOpenFilter").off().on("click", function(){
            startReferenceSearch("open",tableSource);
            $(".list-group-item").removeClass('activeTab');
            $(this).parent().addClass('activeTab');
        });
    }
    function startReferenceSearch(key){
        var data={
            title : "",
            types : initType[0],
            table : {
                name: {
                    name : "Nom",
                    preview : true
                },
                address : {
                    name : trad.address
                },
                type : {
                    name : "Type"
                },
                tags : {
                    name : "Mots-clés"
                },
                source : tableSource
            },
            paramsFilter : {
                defaults : {
                    types : [initType[0]],
                    fields : [ "name", "address", "collection", "tags","source", "reference", "profilMediumImageUrl" ],
                    sort : { name : 1 },
                    filters : {}
                },
                filters : {

                }
            }
        };

        if( filters != ""){
            data.paramsFilter.filters = filters
        }else{
            data.paramsFilter.filters ={
                text:true,
                types :{
                    lists : initType
                },
                scope : true
            }
        }
        if(key=="open"){
            data.actions={
                addReference : {
                    event: "manageSourceData"
                }
            };
            // Attribut _notSourceKey_ pour ouvrir la recherche aux autres résultats non référencés sur le costum
            data.paramsFilter.defaults.notSourceKey=true;
            data.paramsFilter.defaults.filters={
                "preferences.isOpenData":true,
            };
            data.paramsFilter.defaults.filters["reference."+originCross]={'$nin' : sourceKey};
            data.paramsFilter.defaults.filters["source.keys"]={'$nin' : sourceKey};
        }else if(key=="source"){
            data.actions={
                removeSource : {
                    event: "manageSourceData"
                }
            };

            data.paramsFilter.defaults.filters={
                "source.keys": sourceKey
            };
        }
        else if(key=="reference"){
            data.paramsFilter.defaults.filters["reference."+originCross]=sourceKey;
            data.actions={
                removeReference : {
                    event: "manageSourceData"
                }
            };

        }
        data.page = "costum.views.custom.essConnect9711.admin.directory";
        ajaxPost('#contentReferenceResults', baseUrl+'/co2/admin/directory/', data, function(){},"html");
    }
</script>