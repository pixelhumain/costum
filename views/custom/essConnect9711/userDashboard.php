<?php
if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}
?>
<style>
    #filters-nav{
        display: none !important;
    }
    .banner-outer {
        background-color: #f9f9f9;
        background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
        background-position: bottom right;
        background-repeat: no-repeat;
        background-size: 25% auto;
        height: 250px;
        display:flex;
        align-items: center;
        justify-content: space-around;
    }
	.img-profil-entity{
		margin:auto;
		width:50%;
	}
	#dropdown_search{
        height:unset !important;
    }


</style>
<script type="text/javascript">
	if(userId!==""){
        var appConfig=<?php echo json_encode(@$appConfig); ?>;
		var page=<?php echo json_encode($page); ?>;
		// var configAnswers = {
		// 	"equipement" : {
        //         "label" : "Equipement",
        //         "forms" : "63e0a8abeac0741b506fb4f7",
        //         "path" : "franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4",
        //         "visible" : [ 
        //             "Aucun équipement", 
        //             "Bureautiques", 
        //             "Salle(s) de travail", 
        //             "Numériques", 
        //             "Outils destinés à la production (artistique, artisanale, alimentaire, etc)", 
        //             "Espaces destinés à la production artistique", 
        //             "Équipements pour des évènements grand public", 
        //             "Sportifs", 
        //             "Enfants", 
        //             "Habitats légers", 
        //             "Jardinage et bricolage", 
        //             "Hébergements", 
        //             "Autre"
        //         ]
        //     }
		// };
		// var answersOrga = searchObj.helpers.getAnswersWithOrga(configAnswers);
		// mylog.log("answersOrga",answersOrga);


		// var paramsAnsw={
		// 	params : {
		// 		path : "franceTierslieux1822023_1721_4.multiCheckboxPlusfranceTierslieux1822023_1721_4lea64x9zyzijgmpwk4",
		// 		fieldPath : "links.organizations"
		// 	}
		// 	"path" : 
		// }
		// $paramsGlobal["path"] = $paramsGlobal["params"]["thematicPath"];
        //     if(isset($paramsGlobal["params"]["finderPath"])){
        //         $paramsGlobal["finderPath"] = $paramsGlobal["params"]["finderPath"];
        //     }

		// ajaxPost(
		//                 null,
		//                 baseUrl+"/"+moduleId+"/search/globalautocomplete",
		//                 {	"filters":fObj.search.obj.filters,
		//                 	"searchType" : fObj.search.obj.types,
		//                 	"notSourceKey" : fObj.search.obj.notSourceKey,
		//                 	"indexStep" : "0",
		//                 	"fields" : ["name", "tags", "extraInfo","answers"]
		//                 },
		//                 function(data){
		                
		// 				}
		// );
    
    	var defaultFilters = {
            "$or":{
                "source.keys":costum.slug, 
                "reference.costum":costum.slug
            }
        }
        defaultFilters["links.members."+userId+".isAdmin"] = {'$exists':true};

	costum[costum.slug].dashboard= function(params){ // Logo listing design
		mylog.log("dasboard", "Params", params);
		let str = '';
		let imageUrl = baseUrl + moduleUrl + "/images/thumbnail-default.jpg";
		if (typeof params.profilMediumImageUrl !== "undefined" && params.profilMediumImageUrl !== "") {
			imageUrl = baseUrl + params.profilMediumImageUrl;
		}
		str += '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item">'+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>';
			
		if(params.collection == "citoyens"){
			str = '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 padding-10 logo-item tooltips" data-emplacement = "center" data-toggle = "tooltip" data-original-title="'+params.name+'"> '+
			'<a  href="' + params.hash + '" class="text-center '+params.hashClass+'" style="/*height:100%; display:flex*/">'+
				'<img src="'+imageUrl+'" style="max-width: 100%;display: block;margin: auto;" alt="logo">'+
			'</a>'+
			'</div>';
        }
        // if(notEmpty(params.answers)){
            str+='<div id="answersDirectory" class="col-xs-12">';
			
            $.each(costum.lists.forms,function(idf,form){
				if(notEmpty(params.answers)){
    				$.each(params.answers,function(ida,ans){
    					if(ans.form==idf){
                           str+='<span>Mofidier <a href="#answer.index.id.'+ida+'" target="_blank" class="btn btn-defaults">'+form.label+'</a></span>';
                        }    
    				})
    			}else{
					str+='<span>Répondre à  <a href="#answer.index.id.new.form.'+idf+'?eltid='+params._id.$id+'" target="_blank" class="btn btn-defaults">'+form.label+'</a></span>';
				}
                
            })
            str+='</div>'
        // }
        str+='</div>';

		return str;
	};
    
    	var paramsFilter = {
			urlData : baseUrl+"/costum/essconnect9711/getelementanswers",
    		container : "#filters-nav",
    		options : {
    			tags : {
    				verb : '$all'
    			}
    		},
			header : {
				options : {}
			},
    	 	loadEvent : {
    	 		default : "scroll"
    	 	},
    		defaults : {
    			notSourceKey: true,
    		 	types : ["organizations"],
    		 	filters: defaultFilters,
				fields : ["links"],
    			indexStep: 30,
    			sortBy: {"name": 1}
    		},
    	 	results : {
				renderView : "directory.cards",
    			// renderView: "costum.essConnect9711.dashboard",
    		 	smartGrid : true,
				callBack : function (fObj){
					if(typeof fObj.results.data!="undefined" && typeof fObj.results.data.results!="undefined" && Object.keys(fObj.results.data.results).length==0){
						mylog.log("fObj.results.data callback",fObj.results.data);
						$("#subTitleDash").text("Vous n'êtes administrateur.ice d'aucune structure pour le moment.")
					    $(".divEndOfresults").hide();
					}
					$(".blog-item .item-inner.isAdminPending").parent().append('<div class="BannerIsAdminPending padding-30 margin-5" style="position:absolute;left:0;top:30%;font-size: large;text-shadow: #144894 1px 1px 1px;background-color: #14489482;font-weight: bolder;"><span class="bold text-white">Droit d\'administration en attente de validation</span></div>')
					$(".blog-item .item-inner.isAdminPending").css({"filter":"blur(2px)"});
					$(".blog-item .item-inner.isAdminPending .link-bordered").css({"cursor":"not-allowed","pointer-events": "none"})
					// $(".blog-item .item-inner.isAdminPending").append('<div class="mask" style="position:absolute;width:100%;height:100%;background-color: #14489480;top: 0;cursor: not-allowed;"></div>')
				}
    		}
    	}
    
    	jQuery(document).ready(function(){
    		    searchObj.results.smartCallBack = function(fObj,timeOut, last){
    				mylog.log($(".smartgrid-slide-element img.isLoading").length, "length img not loaded", $(".smartgrid-slide-element.last-item-loading").length);
    				if($(".smartgrid-slide-element img.isLoading").length <= 0){
    					fObj.results.$grid.masonry({itemSelector: ".smartgrid-slide-element"});
    				}else{
    					setTimeout(function(){
    						fObj.results.smartCallBack(fObj, (timeout+100));
    					}, (timeout+100));
    				}
    				// if($(".logo-item").length==1){
    				// 	$(".banner-outer").remove();
    				// 	$(".logo-item a").click();
    				// }
    
    				// if($(".logo-item").length==0){
    				// 	$("#subTitleDash").text("Vous n'êtes membre ou administrateur d'aucune structure pour le moment.")
    				// }
    			};
				searchObj.scroll.beforeRender = function(fObj, results, data){
    				mylog.log("scroll.beforeRender");
					if(page=="mesinfos"){
						fObj.results.data=data;
					}
    				
    			};
    
    
    		filterSearch = searchObj.init(paramsFilter);
    		// $(".headerSearchContainer").remove();
    
    		// $(".banner-outer").remove();
            // $(`<div class="banner-outer ">
    		// 	<div class="basic-banner text-center">
            //         <h1 class="font-weight-100">${appConfig.bannerTitle||appConfig.subdomainName||appConfig.metaDescription||""}</h1>
            //         <p id="subTitleDash" class="lead">Vous êtes administrateur de ces structures et vous pouvez accéder à leur tableau de bord.</p>
    		// 		${(costum.isCostumAdmin==true)?'<a class="btn btn-primary-ekitia btn-open-form" href="javascript:;" data-form-type="organization" data-redirecturl=""> CRÉER UNE NOUVELLE STRUCTURE </a>':""}
    		// 	</div>
    		// </div>`).insertBefore("#filters-nav");
    	});
	}else{
		$("#search-content").append('<button  data-toggle="modal" data-target="#modalLogin" class="col-xs-12 btn-main-menu text-center" >Je me connecte</button>');
	}	
    
</script>