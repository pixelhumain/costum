<?php 
    $title="";
    if($page=='listing-formations'){
        // $totle=
        $formSpec="formation";
        $labelFormSpec="formation";
        $title="Listing des formations";
        $colorCode="#004e87";
        $colorClass="nightblue";
        $deleteLabel="Supprimer la formation";
        $editLabel="Editer la formation";
    }else{
        $formSpec="sessionFormation";
        $labelFormSpec="session de formation";
        $title="Listing des sessions de formation";
        $colorCode="orange !important";
        $colorClass="orange";
        $deleteLabel="Supprimer la session";
        $editLabel="Editer la session";
    } 
?>



<style>



#adminDirectoryFormation #panelAdmin td{
    border: none;
    vertical-align: middle !important;
    min-width: unset !important;
    /* padding: 0.5em;*/
    font-size:1em;
}
#openModal.modal {
    top:0 !important;
    left:0 !important;
    z-index:2000000 !important;
}
#modal-preview-coop{
    z-index:200001!important;
}

#panelAdmin thead tr{
    /* border-radius: 8px; */
    box-shadow: 0px 0px 0px 5px <?=$colorCode?>;
    background: <?=$colorCode?>;
    }
</style>

<style>
    h6 {
        font-size: 1rem !important;
        font-weight: 500 !important;

    }

    h3 {
        font-size: 2.5rem !important;
        font-weight: 600;
    }

    h1 {
        font-size: 2em;
    }

    .card-box {
        background-color: #fff !important;
        padding: 1.25rem !important;;
        -webkit-box-shadow: 0 0 35px 0 rgba(73, 80, 87, .15) !important;;
        box-shadow: 0 0 35px 0 rgba(73, 80, 87, .15) !important;;
        margin-bottom: 24px !important;;
        border-radius: 0.5rem !important;;
        margin: 1rem !important;;
    }

    .card-box > .icon {
        float: right;
    }

    .card-box h3,
    h6 {
        margin: 0;
    }

    h6 {
        font-size: 1rem;
        font-weight: 500;
    }

    h3 {
        font-size: 2.5rem;
        font-weight: 600;
    }

    .header > h1,
    p {
        margin: 0;
    }

    .header > h1 {
        font-weight: 600;
    }

    .btnCsvExport {
        display: none;
        padding-top: 6px;
        margin-left: 4px;
        cursor: pointer;
    }

    @media (max-width: 1000px) {
        .stat-card {
            width: 100% !important;
        }

        /* header {
            height: 150px;
            clear: both;
        } */

        .select2-selection__choice__remove {
            display: none !important;
        }

        .mybtngroup {
            display: none;
        }

        .contbntngroup:hover .mybtngroup {
            display: inline-block;
        }
    }

    #session-modal {
        z-index: 999999;
    }

    #session-modal .modal-dialog {
        width: calc(100% - 20px);
    }

    #session-modal .modal-header {
        background-color: white !important;
    }
</style>


<style>
   

    #filterContainer .dropdown .btn-menu,
    #filters-nav .dropdown .btn-menu,
    #activeFilters .filters-activate {
        background-color: #2C3E50;
        color: white !important;
        border: none;
        border-radius: 5px;
    }

    #filterContainer .dropdown .dropdown-menu,
    #filters-nav .dropdown .dropdown-menu {
        border: none;
    }

    @media (max-width: 767px) {

        #filterContainer .dropdown .dropdown-menu,
        #filters-nav .container-filters-menu .dropdown .dropdown-menu {
            border: none !important;
        }
    }

    #filterContainer .dropdown .dropdown-menu button,
    #filters-nav .dropdown .dropdown-menu button {
        border: none;
        background: #ffffff;
        color: #2C3E50;
        text-align: left;
        /* border-bottom: 1px solid #929da8; */
        text-align: left;
        position: relative;
    }

    #filterContainer .dropdown .dropdown-menu button:after,
    #filters-nav .dropdown .dropdown-menu button:after {
        content: '';
        position: absolute;
        border-bottom: solid 1px #2C3E50;
        width: 90%;
        left: 5%;
        bottom: 0;
        opacity: 0.2;
    }

    #filterContainer .dropdown .dropdown-menu button:hover,
    #filters-nav .dropdown .dropdown-menu button:hover {
        background-color: unset;
        font-size: 1.4em;
        font-weight: 700;
    }

    /* .searchBar-filters .main-search-bar-addon{
        border: unset;
        border-bottom: solid 1px #939ea9;
        border-radius: unset;
    } */


    /* .panel-body div {
        width: 100%;
        margin-bottom: 15px;
        overflow-y: hidden;
        overflow-x: auto;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        border: 1px solid #ddd;


    } */

    .panel-body div table.table-striped {
        table-layout: unset !important;
        word-wrap: normal !important;
    }

    .panel-body div::-webkit-scrollbar,
    .list-filters::-webkit-scrollbar {
        width: 8px;
    }

    /* Ascenseur */
    .panel-body div::-webkit-scrollbar-thumb,
    .list-filters::-webkit-scrollbar-thumb {
        background: #2C3E50;
        border-radius: 8px;
    }

    .panel-body div::-webkit-scrollbar-track {
        position: relative;
        box-shadow: inset 0 0px 0px 2px white, inset 0 2px 10px 2px #2C3E50;
    }

    .panel-body div::-webkit-scrollbar-button:single-button {
        display: block;
        border-style: solid;
        height: 16px;
        width: 20px;
    }

    .panel-body div::-webkit-scrollbar-button:single-button:horizontal:increment {
        border-width: 8px 0px 8px 12px;
        border-color: transparent transparent transparent #2C3E50;
    }

    .panel-body div::-webkit-scrollbar-button:single-button:horizontal:decrement {
        border-width: 8px 12px 8px 0px;
        margin-right: 1px;
        border-color: transparent #2C3E50 transparent transparent;
    }

    .simple-pagination {
        border-bottom: none;
    }

    #panelAdmin {
        border: none;
    }

    

    /* #headerTable:after {
        width: 98%;
        content: '';
        left: 1%;
        bottom: 0;
        border-top: 1px solid #2C3E50;
        z-index: 20;
        position: absolute;
        margin: auto;
    } */

    #panelAdmin th,
    #panelAdmin td {
        border: none;
        vertical-align: middle !important;
        min-width: 10vw;
        padding: 0.5em;

    }

    #panelAdmin tr > td:first-child {
        font-weight: bold;
    }


    @media screen and (max-width: 767px) {
    }

    .btn-contact .dismissBtn {
        margin-right: 5px;
    }

    .btn-contact,
    .btn-formation {
        display: flex;
        justify-content: end;
        width: 100%;
    }

    .editFomation {
        cursor: pointer !important;
    }

    .pageTable.col-md-12 {
        display: none;
    }

    #tableauDeBord #input {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;

    }

    /* #openModal.modal {
        left: 30%;
        right: 0px;
        box-shadow: -2px 4px 10px 0px rgb(0 0 0 / 50%);
    } */

    #tableauDeBord {
        padding-left: 1%;
        padding-right: 1%;
    }

    #tableauDeBord .form-group {
        width: 47%;
    }

    .portfolio-modal .modal-content {
        text-align: left;
    }

    #tableauDeBord .territorialCercleselect,
    #tableauDeBord .heldActionselect {
        width: 49.5% !important;
    }

    #tableauDeBord .ptlCommenttextarea {
        width: 97% !important;
    }

    #tableauDeBord #input .form-group {
        flex-basis: auto;
    }

    #tableauDeBord .select2-container {
        height: auto;
        padding: 0;
    }

    #tableauDeBord .flex-row {
        display: flex;
        flex-direction: row;
    }

    #tableauDeBord .flex-column {
        flex: 1;
        margin-right: 10px;
    }

    #tableauDeBord .flex-container {
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
    }

    #search-content,
    #filters-nav,
    .bodySearchContainer .pageTable {
        display: none !important;
    }

    .labelFilterGlobal {
        font-size: 16px;
        margin: 10px;
        display: inline-block;
    }

    @media screen and (max-width: 767px) {
        .labelFilterGlobal {
            float: left;
        }

        .filtersGlobal .filterGlobal {
            display: inline-grid
        }
    }

    .filtersGlobal {
        /* background-color: black; */
        padding: 10px 10px 0 0;
        /* color: white; */
    }

    .filtersGlobal .filterGlobal {
        color: #2f2f2f;
        text-decoration: none;
        font-size: 20px;
        padding: 10px 20px 10px 20px;
        cursor: pointer;
        /* padding-bottom: 5px; */
        border-right: solid 1px var(--main1);
        /* margin-right: 20px; */
    }

    .filtersGlobal a.filterGlobal.active {
        background-color: #f3d361;
        /* color: black; */
        /* border:none; */
        /* padding:23px; */
    }

    #tableauDeBord .notEditable .well:hover {
        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    }

    #tableauDeBord .notEditable .well {
        border-radius: 20px;
        margin-top: 30px;
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    }

    #tableauDeBord .editable .well,
    .editable-input.well {
        border-radius: 20px;
        border: 2px solid #f3d361;
        margin-top: 30px;
        box-shadow: 0 1px 6px #f3d361, 0 3px 6px rgba(0, 0, 0, 0.23);
        padding: 10px 25px;
        display: block;
        margin-top: 2%;

    }

    #tableauDeBord .title h3 {
        margin: 0 !important;
    }

    #tableauDeBord .notEditable .title {
        margin-top: -40px;

    }

    #tableauDeBord .title {
        height: auto;
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 8px 0px;
        border-radius: 5px;
        margin-bottom: 16px;
        background-color: #f3d361;
        width: 250px;
        margin-top: -33px;
    }

    #trainer button,
    #divnewsletterSubscription button,
    #supportCercle button,
    #actionContributor button,
    #referentTerritorialCercle button {
        width: 48%
    }

    #trainer,
    #divnewsletterSubscription,
    #supportCercle,
    #actionContributor,
    #referentTerritorialCercle {
        display: flex;
        justify-content: space-around;
    }

    .select2-search-choice-close::before {
        color: red !important;
    }

    .processingLoader {
        display: none;
    }

    .btn-dyn-checkbox.bg-green-k,
    .btn-dyn-checkbox.bg-red,
    form .selectParent {
        background-color: #2C3E50 !important;
        border-color: #2C3E50;
    }

    .btn-dyn-checkbox.letter-red,
    .btn-dyn-checkbox.letter-green {
        color: #333 !important;
        background-color: #fff;
        border: 1px solid #ccc;
    }

    .btn-dyn-checkbox,
    .btn-dyn-checkbox:hover {
        font-size: 14px;
        height: 30px;
        font-weight: 400;
        border: none;
    }

    /* #tableauDeBord .flex-container,
		#tableauDeBord .flex-row{
			display: block;
		} */


    .detailContactCard .card-img-top {
        min-height: 140px;
    }

    .detailContact {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: 15px;
        box-shadow: 3px 4px 5px 3px rgb(0 0 0 / 20%);
        margin-bottom: 20px;
        margin-left: 0px;
        margin-right: 0px;
    }

    .content-img {
        padding: 18px;
    }

    .div-img {
        background: #ccc;
        height: 100px;
        padding: 20px;
        border-radius: 50%;
        width: 100px;
    }

    .detailContactCard .div-img i.fa {
        border-radius: 5px 0 21px 0;
        font-size: 4em;
    }

    .detailContactCard img {
        border-radius: 50%;
        width: 100px;
        height: 100px;
        background-color: transparent;
        object-fit: cover;
        object-position: center;
    }

    .detail-box h4 {
        text-transform: none;
    }

    .detailContactCard .detail-box .entityLocality {
        margin-bottom: 5px;
        min-height: 20px;
    }

    .smartgrid-slide-element ul.tag-list .btn-tag {
        background: #939ea9;
        color: white !important;
    }

    #dropdown_search {
        margin-top: 2%;
    }

    .change-views label {
        font-size: 15px;
        margin-left: 5px;
    }

    .change-views .fa {
        font-size: 18px;
    }

    .change-views {
        padding-top: 6px;
        margin-left: 4px;

    }

    .entityLocality fa {
        color: #f3d361;
    }

    @media screen and (max-width: 767px) {

        #tableauDeBord .flex-container,
        #tableauDeBord .flex-row,
        #tableauDeBord #input {
            display: block;
        }

        #tableauDeBord .form-group {
            width: 100% !important;
        }

        .btn-contact {
            padding-top: 15px;
        }

        #tableauDeBord #input .form-group {
            width: 100% !important;
        }

        /* .panel-body div {
			width: 100%;
			margin-bottom: 15px;
			overflow-y: hidden;
			overflow-x: auto;
			-ms-overflow-style: -ms-autohiding-scrollbar;
			border: 1px solid #ddd;
		}
		.panel-body div table.table-striped{
			table-layout: auto !important;
			word-wrap: normal !important;
		} */
        .filtersGlobal .filterGlobal {
            padding: 5px;
            font-size: 15px;
        }

        .filtersGlobal {
            padding: 8px;
        }

        .searchObjCSS .searchBar-filters {
            width: 100% !important;
            margin-bottom: 3px;
        }

        #filterContainer .dropdown,
        #filters-nav .dropdown {
            text-align: center;
        }

        #filterContainer .dropdown .dropdown-menu,
        #filters-nav .dropdown .dropdown-menu {
            padding: 0px;
            top: 8px;
            width: 100%;
        }

        .searchBar-filters .search-bar {
            border-left: 1px solid;
            border-radius: 4px 0 0 4px !important;
        }

        .searchBar-filters .main-search-bar-addon {
            border-right: 1px solid #6b6b6b;
            border-radius: 0 4px 4px 0 !important;
        }

        #filterContainer {
            margin-top: 0px;
        }

        #filterContainerInside {
            min-height: unset !important;
        }

        #tableauDeBord .title h3 {
            font-size: 18px;
        }

        .content-img {
            padding: 20px 10px;
        }

        .detailContactCard img {
            width: 50px !important;
            height: 50px !important;
            margin-left: auto;
            margin-right: auto;
        }

        .detailContactCard .div-img i.fa {
            font-size: 2em;
        }

        .content-img {
            padding: 20px 10px;
        }

        .div-img {
            height: 50px;
            padding: 10px;
            width: 50px;
        }

        .smartgrid-slide-element ul.tag-list .btn-tag {
            text-align: left;
            white-space: break-spaces;
            margin-bottom: 2px;
        }
    }
</style>

<?php 


$cssAnsScriptFilesModule = array(
	'/js/admin/panel.js',
	'/js/admin/admin_directory.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl() );
$adminConstruct=$this->appConfig["adminPanel"];
	if(isset($this->costum)){
		$slugContext=isset($this->costum["assetsSlug"]) ? $this->costum["assetsSlug"] : $this->costum["slug"] ;
		$cssJsCostum=array();
		if(isset($adminConstruct["js"])){
			$jsCostum=($adminConstruct["js"]===true) ? "admin.js" : $adminConstruct["js"];
			array_push($cssJsCostum, '/js/'.$slugContext.'/'.$jsCostum);
		}
		if(isset($adminConstruct["css"]))
			array_push($cssJsCostum, '/css/'.$slugContext.'/admin.css');
		if(!empty($cssJsCostum))
			HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
	}
	$logo = (@$this->costum["logo"]) ? $this->costum["logo"] : Yii::app()->theme->baseUrl."/assets/img/LOGOS/CO2/logo-min.png";
    
?>
<div class="col-xs-12">
    <h2 class="text-center text-<?= $colorClass ?>" style="text-transform:none;font-size:30px;"><?=$title?></h2>
</div>
<div class="col-xs-12" style="color:inherit;">
<div id='filterFormation' class='searchObjCSS col-xs-12 col-sm-6'><div><a href="#sessions-publics" class="menu-sessions-admin">Espace de gestion des sessions</a></div></div>
<div class='headerSearchInFormation no-padding col-xs-12 col-sm-6'></div>
<!-- <div class='bodySearchContainer margin-top-10'>
	 <div class='no-padding col-xs-12' id='dropdown_search'></div> 
	<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div> 
</div> -->
<div id="adminDirectoryFormation" class="col-xs-12"></div>
</div>
		
	
	


<script type="text/javascript">

var currentPage="<?= $page ?>";
var formSpec="";
var labelFormSpec="";
var labelFormSpecPluri="";
var colorCode="";
var colorClass="";
var deleteLabel="";
var editLabel="";
var otherPage="";
var typeAutocpl="";
if(currentPage=='listing-formations'){
    formSpec="formation";
    typeAutocpl="projects";
    labelFormSpec="formation";
    labelFormSpecPluri="formations";
    otherLabelFormSpecPluri="sessions de formation";
    colorCode="#004e87";
    colorClass="nightblue";
    otherPage="listing-sessions";
    otherColorClass="orange";
}else{
    formSpec="sessionFormation";
    labelFormSpec="session de formation";
    typeAutocpl="events";
    labelFormSpecPluri="sessions de formation";
    otherLabelFormSpecPluri="formations";
    colorCode="orange";
    colorClass="orange";
    otherPage="listing-formations";
    otherColorClass="nightblue";
}   

adminDirectory.values.organizer=function(e,id,type,aObj){
    mylog.log("values organizer",e, id, type, aObj);
    str="";
    if(typeof e.organizer!="undefined" && Object.keys(e.organizer).length>0){
        
        $.each(e.organizer,function(id,e){
            if(str!==""){
                str+="</br>";
            }
            str+=str+="<a href='#page.type."+e.type+".id."+id+"' style='display:inline-block' class='lbh-preview-element'>"+e.name+"</a>";

        });
    }
    return str;
    
};
adminDirectory.values.handleSession=function(e,id,type,aObj){
    mylog.log("values handleSession",e, id, type, aObj);
    str='<button class="btn pull-left margin-left-20 bg-orange handleSession" data-id="'+id+'"fa fa-cog"></i> Gérer cette session</button>';
    // if(typeof e.organizer!="undefined" && Object.keys(e.organizer).length>0){
        
    //     $.each(e.organizer,function(id,e){
    //         if(str!==""){
    //             str+="</br>";
    //         }
    //         str+=str+="<a href='#page.type."+e.type+".id."+id+"' style='display:inline-block' class='lbh-preview-element'>"+e.name+"</a>";

    //     });
    // }
    return str;
    
};   

adminDirectory.values.dates=function(e,id,type,aObj){
    mylog.log("values dates",e, id, type, aObj);
    str='Du '+moment(e.startDate.sec*1000).format('L')+' au '+moment(e.endDate.sec*1000).format('L');

    return str;
    
};  




adminDirectory.values.session=function(e,id,type,aObj){
    mylog.log("values session",e, id, type, aObj);
    var strR="";
    ajaxPost(
        null,
        baseUrl+'/'+moduleId+'/element/getalllinks/type/'+type+'/id/'+id,
        {},
        function(data){
            var str='Aucune session associée</br>'+
                '<a href="javascript:;" data-type="'+type+'" data-id="'+id+'" data-name="'+e.name+'" style="background-color:#004e87;" class="add-session bg-orange btn pull-left text-white" '+
		            '<i class="fa fa-plus-circle"></i> Ajouter une nouvelle session de formation'+
		        '</a>';
            if(typeof data!="undefined"){
                var prevCounter=0;
                // str="";
                $.each(data,function(k,e){
                    var toggleClass ="";
                    if(typeof e.collection!="undefined" && e.collection=="events"){
                        if(moment(e.endDate) < moment()){
                            toggleClass="hidden";
                            prevCounter++;
                        }
                        if(!str.includes("sessionLinks")){
                            str="";
                        }
                        str+="<a href='#page.type."+e.collection+".id."+k+"' style='display:grid' class='sessionLinks lbh-preview-element "+toggleClass+"'>"+e.name+"</a>";
                    }   
                });
                if(prevCounter>0){
                   str+="<a href='javascript:;' style='padding:3px;font-size:10px' class='btn btn-default showPreviousSession'>Voir les sessions précédentes</a>"
                }
            }  
            strR=str;  

        },
        null,
        null,
        {async : false}
    );
    return strR;
    
}   

adminDirectory.bindCostum=function(aObj){
    mylog.log("adminDirectory.events.session", aObj);
    $("#"+aObj.container+" .showPreviousSession").off().on("click",function(){
        $(this).siblings(".hidden").removeClass("hidden");
        $(this).hide();
    });
    $("#"+aObj.container+" .handleSession").off().on("click",function(e){
     
                // var  = ';
                mylog.log("handlesession");
                var formation = $(this).data("id");
                var modalDom = $('#session-modal');
                ajaxPost(null, baseUrl + '/costum/formationgenerique/gestionsession/sessionId/' + formation, null, function (html) {
                    modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                        modalDom.css('padding', '');
                    });
                    modalDom.find('.modal-body').empty().html(html);
                    modalDom.modal('show');
                });
                e.stopImmediatePropagation();


    });
    $("#"+aObj.container+" .add-session").off().on("click",function(){
        var dataOrga={};
            dataOrga.organizer={};
            dataOrga.organizer[$(this).data("id")]={
                type : $(this).data("type"),
                name : $(this).data("name")
            };
        // var dataOrga=$(this).data("organizer");
        mylog.log("dataOrga",dataOrga);
        dyFObj.openForm(`sessionFormation`,`afterLoad`,dataOrga);
    });

    
};


    var data={
        paramsFilter : { 
            container : "headerSearchInFormation",
			defaults : {
				types : [formSpec],
				sortBy: {"name": 1}
			} 
		},
		table : {
            name : {
                name : "Nom de la "+labelFormSpec,
                preview : true
            }
            // shortDescription : {
            //     name : "Description courte"
            // }            

        }
        // actions : {
        //     update : true,
        // 	delete : true
        // }
    };   
    if(currentPage=="listing-formations"){ 
        data.table.session ={
            name : "Sessions de formation"
        };
        data.table.shortDescription = {
                name : "Description courte"
        }; 
        
    }else{
        data.table.dates ={
            name : "Période"
        };
        data.table.organizer ={
            name : "Formation rattachée"
        };
        data.table.handleSession ={
            name : "Gérer"
        };

    }    
        var paramsFilter = {
            container : "#filterFormation",
            header : {
               dom : ".headerSearchInFormation",
               options : {
                //    left : {
                //        classes : 'col-xs-6 elipsis no-padding',
                //        group:{
                //            count : true
                //        }
                //    },
                   right : {
                       classes : 'col-xs-12 text-right no-padding',
                       group : { 
                        //    add : {
                        //        label : "Ajouter une nouvelle formation"
                        //    },
                           custom : {
                                html :'<div class="margin-right-20" style="display:inline-flex;margin-top:8px;">'+
                                '<a href="javascript:;" onclick="dyFObj.openForm(`'+formSpec+'`)" style="background-color:#004e87;" class="bg-'+colorClass+' btn pull-left text-white" '+
		                            '<i class="fa fa-plus-circle"></i> Ajouter une nouvelle '+labelFormSpec+''+
		                        '</a>'+
                                '<a href="#'+otherPage+'" style="margin-left:15px;" class="bg-'+otherColorClass+' text-white lbh btn menu-sessions-admin">Listing des '+otherLabelFormSpecPluri+'</a>'+
                                '</div>'
                                // events : function(){alert("eventcustom")}
                           }    
                       }
                   }
               }
            },
            defaults : {
                types : [ typeAutocpl ],
                fields : [ "name", "organizer", "parent", "collection", "email", "tags", "shortDescription", "links" ],
                filters : {
                }
            },
            filters : {
                text : true
            },
            loadEvent : {
					default : "admin",
					options : {
                        container : "adminDirectoryFormation",
                        directory : {},
						results : {},
						initType : [typeAutocpl],
						panelAdmin : data
					}
			}

        };
        
	
    // paramsFilter.defaults.filters["parent."+costum.contextId]={"$exists":1};
    // var paramsFilter = contactCie.getParams(key,view);		
			

jQuery(document).ready(function() {
    adminDirectory.initView = function(){
		mylog.log("adminDirectory.initView");
		var aObj = this;
		var str = "";
		if(typeof aObj.panelAdmin.title != "undefined" && aObj.panelAdmin.title != null)
			str += '<div class="col-xs-12 padding-10 text-center"><h2>'+aObj.panelAdmin.title+'</h2></div>';

			str += '<div class="headerSearchContainerAdmin no-padding col-xs-12"></div>';
			str += '<div class="no-padding col-xs-12 searchObjCSS" id="filters-nav-admin"></div>';
		str += 	'<div class="pageTable col-xs-12 padding-10"></div>';
		
		str +=	'<div class="panel-body">'+
			'<div>'+
				'<table style="table-layout: fixed; word-wrap: break-word;" class="table table-striped table-bordered table-hover directoryTable" id="panelAdmin">'+
					'<thead>'+
						'<tr id="headerTable">'+
						'</tr>'+
					'</thead>'+
					// '<tbody class="directoryLines">'+
						
					// '</tbody>'+
				'</table>'+
			'</div>'+
		'</div>'+
		'<div class="pageTable col-md-12 col-sm-12 col-xs-12 padding-20"></div>';

		$("#"+this.container).html(str);
	};
    adminDirectory.initViewTable = function(aObj){
		//var aObj = this;
		mylog.log("adminDirectory.initViewTable", aObj.container);
		$("#"+aObj.container+" #panelAdmin thead").nextAll().remove();
		mylog.log("adminDirectory.initViewTable !", aObj.results);
		var sumColumn="";
		var posSumColumn = 0;
		var sum=null;
		var tot="";
		var totalColumn=0;

		//vérifie si une entrée du tableau contribut l'attribut sum pour faire la somme
		$.each(aObj.panelAdmin.table, function(k, v){
			mylog.log("yiyi",Object.keys(v));
			if($.inArray("sum",Object.keys(v))>-1){
				sumColumn=k;
				posSumColumn=Object.keys(aObj.panelAdmin.table).indexOf(k);
			}
		});

		if(Object.keys(aObj.results).length > 0){
			$.each(aObj.results ,function(key, values){
				mylog.log("adminDirectory.initViewTable !!! ", key, values);
				var entry = aObj.buildDirectoryLine( values, values.collection);
				// var selector=(entry.match(/tbody/).length>0) ? "thead tbody": "thead"; 
				$("#"+aObj.container+" #panelAdmin").append(entry);
				// Addition des valeurs de la colonne
				if(sumColumn){
					if(typeof values[sumColumn]!="Number")
						sum = Number(values[sumColumn]);
					totalColumn = totalColumn + sum ;
				}	
			});	
			//Ligne supplémentaire pour faire la somme
			if(sumColumn){
				tot += '<tr style="height: 20px;"></tr>';
				tot += '<tr style="border-top:solid;border-bottom:solid;"><td style="border-left:none !important;border-right:none !important;font-weight:800;">TOTAL</td</tr>';


				for(var p=0;p<posSumColumn-1;p++){
					tot+='<td style="border-left:none !important;border-right:none !important;"></td>';
				}
				tot += '<td style="border-left:none !important;border-right:none !important;">'+totalColumn+'</td>';
				$("#"+aObj.container+" #panelAdmin .directoryLines").append(tot);
			}		
		}	

		aObj.bindAdminBtnEvents(aObj);
	};
	adminDirectory.buildDirectoryLine = function( e, collection, icon ){
		mylog.log("adminDirectory.buildDirectoryLine",  e, collection, icon);
		var aObj = this;
		var strHTML="";
		if( typeof e._id == "undefined" || 
			( (typeof e.name == "undefined" || e.name == "") && 
			  (e.text == "undefined" || e.text == "") ) )
			return strHTML;
		var actions = "";
		var classes = "";
		var id = e._id.$id;
		var status=[];
		strHTML += '<tbody><tr id="'+e.collection+id+'" class="'+e.collection+' line">';
			strHTML += aObj.columTable(e, id, e.collection);
			var actionsStr = aObj.actions.init(e, id, e.collection, aObj);
			mylog.log("adminDirectory.buildDirectoryLine actionsStr",  actionsStr);
			if(actionsStr != ""){
				aObj.actionsStr = true;
				mylog.log("adminDirectory.buildDirectoryLine aObj.actionsStr",  aObj.actionsStr);
				strHTML += 	'<td class="center">'+ 
								'<div class="btn-group">'+
								actionsStr +
								'</div>'+
							'</td>';
			}
		strHTML += '</tr></tbody>';
		return strHTML;
	};
    // adminPanel.views.projects();
    filterFormation = searchObj.init(paramsFilter);
    // filterFormation.container="adminDirectoryFormation";
    filterFormation.admin.event=function(fObj){
        mylog.log("gestion formation searchObj.admin.event");
			if(fObj.search.obj.indexStep > 0){
				if(fObj.results.numberOfResults%fObj.search.obj.indexStep == 0){
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep);
				}else{
					var numberPage=Math.floor(fObj.results.numberOfResults/fObj.search.obj.indexStep)+1;
				}
			    $('.pageTable').pagination({
			        items: numberPage,
			        itemOnPage: fObj.search.obj.indexStep,
			        currentPage: (fObj.search.obj.nbPage+1),
			        hrefTextPrefix:"?page=",
			        cssStyle: 'light-theme',
			        prevText: '<<',
			        nextText: '>>',
			        onInit: function () {

			        },
			        onPageClick: function (page, evt) {
			        	coInterface.simpleScroll(0, 10);
			        	fObj.search.obj.nbPage=(page-1);
			            fObj.search.obj.indexMin=fObj.search.obj.indexStep*fObj.search.obj.nbPage;
			            fObj.search.start(fObj);
			        }
			    });
			}
    };
    filterFormation.search.init(filterFormation);

    // setTimeout($("#openModalContent").addClass("col-xs-12"),5000);
	
	

});










</script>