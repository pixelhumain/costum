<?php 
$cssAnsScriptFilesModule = array(
    '/js/default/profilSocial.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

?>
<style type="text/css">
#ajax-modal.portfolio-modal.modal{
	z-index: 1000000;
}


.tooltip-arrow,
.btn-participate-modal + .tooltip > .tooltip-inner {background-color: white;color:black;}

#modal-delete-element .modal-title{
	color:white !important;
}

/* #myAccount-settings div .div-btn-setting:nth-child(2), #myAccount-settings div .div-btn-setting:nth-child(3){
    display:none;
} */

.notificationsPod, .confidentialityPod{
	display:none;
}


.eventLi{
		box-shadow: 1px 2px 3px -1px white;
		margin-bottom:15px;
	}
	
	.eventLi .btn-open-chat-session{
		/* color: red; */
	    font-size: 14px;
	    font-weight: 800;
	    padding: 3px 10px;
	    /* background-color: #254c97; */
	}
	.eventLi .titleEvent{
		color: #92bbe2;
	    font-size: 22px;
	    text-transform: inherit;
	}
	.eventLi .info-event > span, .visioDate{
	    color: #92bbe2;
		font-weight:bolder;

	}
	.eventLi .info-event{
		font-size: 14px;
		color:white;
	}
	.eventLi .container-infos{
		padding:0px;
    	padding-bottom: 5px !important;
    }


    .tl-list{
    	padding:5px;
    	display: flex;
    }

	.notifications-dash .notifLi{
		background-color: rgba(208, 249, 208, 1);
	}
	.notifications-dash .notifLi.read{
		background-color: red !important;
	}
	.notifications-dash ul.notifListElement {
	    padding: 0px !important;
	    max-height: 350px;
	    overflow-y: scroll;
	    border-radius: 5px;
	}
	.notifications-dash a.notif{
		font-size : 11px;
		font-weight: bold;
	}
	.notifications-dash a.notif .content-icon{
		position: absolute;
	}
	.notifications-dash a.notif .content-icon span.label{
		background-color: #56c557b5 !important;
	}
	.notifications-dash a.notif .notif-text-content{
		width: 91.66666667% !important; 
		float: right;
	}
	.notifications-dash .pageslide-title{
		width: 100%;
    	padding-bottom: 5px;
    	text-transform: uppercase !important;
    	font-size: 18px !important;
	}
	.notifications-dash .pageslide-title i.fa-angle-down{
		display:none;
	}
	.notifications-dash .btn-notification-action{
		display: none;
	}

	@media (max-width: 792px){
		.menu-dashboard{
			display:none;
			width: 90%;
		}
	}
	.dashboard-modal-content h1, .dashboard-modal-content h2, .dashboard-modal-content h3, .menu-dashboard a{
		color: white;
	}
	/* .dashboard-modal-content .dash-answers{
		display: none;
	} */
	/* .dashboard-modal-content .eventLi{    
	    color: "costum.views.custom.laCompagnieDesTierslieux.accountModal";
    	text-transform: initial;
    	box-shadow: 1px 2px 3px -1px #f4f4f44f;
    	font-weight: initial;
	}
	.dashboard-modal-content .eventLi .titleEvent {
	    color: #67b04c;
	    font-size: 20px;
	}
	.dashboard-modal-content .eventLi b {
    	color: #a2ea87;
	}
	.dashboard-modal-content .eventLi:hover {
    	background-color: #3d4f3c;
	} */
</style>
 <div class="col-md-3 col-sm-4 margin-bottom-20 menu-dashboard">
	<?php $imgPath=(isset(Yii::app()->session["user"]["profilMediumImageUrl"]) && !empty(Yii::app()->session["user"]["profilMediumImageUrl"])) ? Yii::app()->createUrl(Yii::app()->session["user"]["profilMediumImageUrl"]): $this->module->getParentAssetsUrl()."/images/thumbnail-default.jpg"; ?>  
	<img class="img-responsive profil-img" src="<?php echo $imgPath ?>">
	<?php if (Authorisation::isInterfaceAdmin()){ ?>
		<a href="#admin" class="lbh btn-dash-link col-xs-12">Administration</a>
	<?php	} ?>
	<!-- <a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh btn-dash-link col-xs-12">Mon profil</a>-->
	
	<!-- <a href="javascript:;" class="btn-dash-link open-modal-settings col-xs-12">Mes paramètres</a>  -->
	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>.view.settings" onclick="$('.dashboard-account.modal .close-modal').trigger('click');" class="lbh btn-dash-link col-xs-12">Mes paramètres</a>  

	<!-- <a href="javascript:;" onclick="rcObj.loadChat("","citoyens", true, true)" class="btn-dash-link col-xs-12"> Ma messagerie</a> -->
	<!-- <div class="col-xs-12 no-padding notifications-dash"></div> -->
	<a href="javascript:;" class="btn-dash-link logoutBtn col-xs-12 bg-red" style="border-bottom: inherit;
    border-radius: 5px;"><i class="fa fa-sign-out"></i> Déconnexion</a>
</div>
<?php 
$tplSlug=isset($this->costum["slug"]) ? $this->costum["slug"] : $this->costum["assetsSlug"];
//var_dump("<pre>",$this->costum,"</pre>");
?>
<div class="col-md-9 col-sm-8 col-xs-12 dashboard-modal-content">
	<div class="col-xs-12 no-padding"><h2><?php echo Yii::app()->session["user"]["name"] ?></h2></div>
	
<?php 
	
	$holdingTL=PHDB::find(Organization::COLLECTION,array("tags"=>"TiersLieux","links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
 
	 foreach($holdingTL as $id => $tl){
        $ansTL=PHDB::find(Form::ANSWER_COLLECTION,array("links.organizations.".(string)$tl["_id"] =>array('$exists'=>1)));
		$typeElt=$tl["type"] ?? "";
		$img= (isset($tl["profilThumbImageUrl"])) ? $tl["profilThumbImageUrl"] : "";	
	 }
	?>


    <?php
    $mapTradCat=[
        "sessionFormation"=>"Session de formation"
    ];
    $myEvents=PHDB::findAndSort(Event::COLLECTION,array("startDate"=>array('$gt'=>new MongoDate(time())),"source.key"=>$this->costum["slug"],"links.attendees.".Yii::app()->session["userId"]=>array('$exists'=>1)),array("startDate"=>1));
	// var_dump($myEvents);
    ?>
	<div id="modal-account-settings" class="col-xs-12 margin-bottom-10" style="border: solid 1px grey;">
       
        <!-- <p style="font-style: italic;"><i class="fa fa-info-circle"></i> Pour entrer des informations dans le tableau, double-cliquer sur la cellule correspondantes</p>
        <p>
            <h5>Petit guide de la bonne utilisation de ce fichier</h5>
            <span>Mettre à jour :</span>
            <ol>
            <li style="font-size: 15px;">Les dates & type de contact, la motivation</li>
            <li style="font-size: 15px;">Le résumé  du profil</li>
            <li style="font-size: 15px;">Suivi de visio</li>
            </ol>
            <span>Ecrire les infos qui ne correspondent à aucune des cases dans "Autres Commentaires" - dernière colonne</span>
        </p> -->
    </div> 
	<div class="col-xs-12 no-padding margin-bottom-20" style="border-bottom: 1px solid #92bbe2;"><h3>Mes événements (<?php echo count($myEvents) ?>)</h3></div>
	<?php  
		if(empty($myEvents)){
			echo "<span class='col-xs-12 text-white no-padding' style='font-style: italic;font-weight:bolder'>Je ne suis inscrit.e à aucun événement</span>";
		}else{
			echo "<a href='#agenda' class='margin-right-5 btn btn-success lbh margin-top-10'><i class='fa fa-plus'></i> Consulter l'agenda du réseau</a>".
            "<a href='#sessions-publics' class='btn btn-success lbh margin-top-10'><i class='fa fa-plus'></i> Consulter l'annuaire des sessions de formation</a>";
		
		?>

			<div class="col-xs-12">
		<?php		
			foreach($myEvents as $idEvent => $event){
				$isFollowed = false;
                if (isset($event["links"]["attendees"][Yii::app()->session["userId"]])) {
                    $isFollowed = true;
                }
				$isShared = false;
                $tip = "S'inscrire";
                $actionConnect = 'follow';
                $icon = 'chain';
                // $urlVisio = (typeof dbSubevent.url != 'undefined') ? dbSubevent.url : "";
                $tipVisio = "Rejoindre la visio-conférence";
                $labelVisio = "Rejoindre";
                $attendees = (isset($event["links"]["attendees"])) ? json_encode($event["links"]["attendees"])  : "{}";
                // $expectedAttendees = (typeof dbSubevent.expectedAttendees != "undefined" && parseInt(dbSubevent.expectedAttendees) != NaN) ? parseInt(dbSubevent.expectedAttendees) : 0;
                $classBtn = '';
                if ($isFollowed) {
                                $actionConnect = 'unfollow';
                                $icon = 'unlink';
                                $classBtn = 'text-green';
                                $tip = "Je ne veux plus être inscrit.e";
                }
				$btnLabel = ($actionConnect == "follow") ? "S'inscrire" : "Je souhaite me désinscrire";
				
			?>	
				<div class="eventLi col-xs-12 no-padding margin-bottom-10" <?php echo "" ?> data-id="<?php echo $idEvent ?>">
				<!-- <div class="col-xs-4 padding-top-10">
					<img src="<?php echo "" ?>" class="img-responsive margin-auto">
				</div> -->
				<div class="col-xs-12 container-infos">
					<div class="col-xs-12 no-padding">
						<h3 class="margin-top-5 titleEvent"> <?php echo $event["name"] ?></h3> 
					</div>
                    <span class="info-event">Catégorie : <span><?php echo (isset($event["category"])) ? $mapTradCat[$event["category"]] : ((isset($event["type"])) ? Yii::t("event",$event["type"]) : "Non renseignée")?></span></span><br/>
					<span class="info-event"> <i class="fa fa-calendar"></i> Début : <span><?php echo "Le " .date("d.m.y à H:i",$event["startDate"]->sec) ?></span></span></br>
                    <span class="info-event"> <i class="fa fa-calendar"></i> Fin : <span><?php echo "Le " .date("d.m.y à H:i",$event["endDate"]->sec) ?></span></span>
					<?php if ( isset($event["updated"] )) {?>
					<br/><span class="info-event"> <i class="fa  fa-edit"></i> Dernière modification le : <span><?php echo "Le " .date("d.m.y à H:i",$event["updated"]); ?></span></span>
					<?php } ?>

					<!-- <span class="info-answ col-xs-12 no-padding"> <b>Déposé par :</b> <?php echo "nameprop"; ?></span> -->

					<!-- <span class="info-answ col-xs-12 no-padding"> <b>Description :</b> <?php echo "desnamepropcirption"; ?></span> -->
					<br/>	
					<span class="info-event">
						<i class="fa fa-step"></i>
						    Votre statut : 
							<span><?php echo (!empty($event["links"]["attendees"][Yii::app()->session["userId"]]["roles"])) ? implode(", ",$event["links"]["attendees"][Yii::app()->session["userId"]]["roles"]) : "Participant.e"?></span>
				            <button class="btn btn-default btn-sm tooltips btn-participate-modal"
                                    data-toggle="tooltip" data-placement="right" data-original-title="<?=$tip?>" data-expected-attendees="" data-attendees='<?=$attendees?>'
                                    data-ownerlink="<?=$actionConnect?>" data-id="<?=(string)$event["_id"]?>" data-type="events" data-name="<?=$event['name']?>"
                                    data-isFollowed="<?=$isFollowed?>"><?=$btnLabel?></button>
				    </span>
					
					<br/>
					<div class="col-xs-12 no-padding">
						
						<a href="javascript:;" class="bg-orange margin-top-5 btn btn-open-chat-session" data-slug="<?= $event['slug'] ?>" data-type="<?= $event['collection'] ?>" data-name="<?= $event['name'] ?>" data-event="<?= htmlspecialchars(json_encode($event))?>">
							<i class="fa fa-comment"></i> Ouvrir la messagerie publique
						</a>
						
					</div>
				<?php
				    if(isset($event["links"]["attendees"][Yii::app()->session["userId"]]["visioDate"])){
					    $visioDate=$event["links"]["attendees"][Yii::app()->session["userId"]]["visioDate"];
						// if(isset($event["visioDate"]["alldates"]) && !empty($event["visioDate"]["alldates"])){
						// 	$allvio
						// 	foreach($event["visioDate"]["alldates"] as $date=>$hours){
						// 		foreach($hours as $id=>$hour){

						// 		}
						// 	}
						// }
						// echo '<span class="text-white">Le '.date("d.m.y à H:i", $visioDate->sec).'</span>';
				?>
					<div class="col-xs-12 margin-top-15" style="font-size:18px">
						
						<i class="fa fa-warning text-white"></i>
						<span class="text-white">Ne loupez pas votre visio d'information le </span>
						<span class="visioDate"><?=date("d.m.y à H:i", $visioDate->sec)?></span>
					
					</div>
				<?php
					}
				?>
				</div>
		</div>
		<?php 
		       
			}
			?>

			
		<?php	
	       // echo $this->renderPartial("costum.views.custom.deal.newAnswers",$params);
		 }
	 ?> 
</div>
<script type="text/javascript">
	function event_participation(dom) {
        var that = dom;


        // let eventData = JSON.parse(JSON.stringify()).find(function(find) {
        //     return find['id'] == $(that).data("id")
        // });
        thisElement = $(that);
        let isExpAttFull = (thisElement.data("expected-attendees") > 0 && Object.keys(thisElement.data("attendees")).length == eventData.expectedAttendees);

        if (isExpAttFull == false) {
            if (userId) {
                subscribeToEvent(thisElement);
            } else {
                $("#modalLogin").on('show.bs.modal', function (e) {
                    if ($("#infoBL").length == 0) {
                        $("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
                    }
                });
                $("#modalLogin").on('hide.bs.modal', function (e) {
                    $("#infoBL").remove();
                });
                toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
                Login.openLogin();
            }
        } else {
            if (userId) {
                subscribeToEvent(thisElement);
            }
        }

        // if (typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink") == "follow") {
        //     bootbox.dialog({
        //         message: `<div class="alert-white text-center"><br>
        //                     <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
        //                     <br><br>
        //                     <button class="btn btn-default bootbox-close-button">OK</button>
        //                 </div>`
        //     });
        // }
    }


    // function updateFilterEventPeriodChronology() {
    //     var state = sessionStorage.getItem(dataHelper.printf('{{block}}-showAllEvents', {block : blockKey}));
    //     state = state ? state : 'false';
    //     $(dataHelper.printf('#chronology_container{{blockKey}} .event-filter', {blockKey : blockKey})).each(function () {
    //         var self = $(this);
    //         if (self.data('state').toString() === state.toString()) {
    //             self.css('display', 'none');
    //         } else {
    //             self.css('display', '');
    //         }
    //     });
    // }

    function subscribeToEvent(eventSource, theUserId = null) {
        var labelLink = "";
        var parentId = eventSource.data("id");
        var parentType = eventSource.data("type");
        var childId = (theUserId) ? theUserId : userId;
        var childType = "citoyens";
        var name = eventSource.data("name");
        var id = eventSource.data("id");
        //traduction du type pour le floopDrawer
        eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
        var connectType = (parentType == "events") ? "connect" : "follow";

        if (eventSource.data("ownerlink") == "follow") {
            callback = function () {
                labelLink = (parentType == "events") ? "DÉJÀ INSCRIT(E)" : trad.alreadyFollow;
                if (eventSource.hasClass("btn-add-to-directory")) {
                    labelLink = "";
                }
                $(`[data-id=${id}]`).each(function () {
                    $(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
                    $(this).data("ownerlink", "unfollow");
                    $(this).data("original-title", labelLink);
                });
            }
            if (parentType == "events") {
                links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
            } else {
                links.follow(parentType, parentId, childId, childType, callback);
            }
        } else {
            //eventSource.data("ownerlink")=="unfollow"
            connectType = (parentType == "events") ? "attendees" : "followers";
            callback = function () {
                labelLink = (parentType == "events") ? "S'inscrire" : "Déjà inscrit.e";
                if (eventSource.hasClass("btn-add-to-directory")) {
                    labelLink = "";
                }
                $(`[data-id=${id}]`).each(function () {
                    $(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
                    $(this).data("ownerlink", "follow");
                    $(this).data("original-title", labelLink);
                    $(this).removeClass("text-white");
                });
            };
            links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
        }
    }
jQuery(document).ready(function() {
	coInterface.bindLBHLinks();
	mylog.log("pageProfil.views.notifications");
	var url = "element/notifications/type/citoyens/id/"+userId;
    $('.btn-open-chat-session').off().on('click', function() {
        var slug=$(this).data("slug");
        var name=$(this).data("name");
        var type=$(this).data("type");
        var event=$(this).data("event");
        var openChat=true;
        var hasRc=false;
        // mylog.log(JSON.parse(event));
        mylog.log(event);
        rcObj.loadChat(slug, type, openChat, hasRc, event);
    });

	$(".btn-participate-modal").off().on("click",function(){
		event_participation($(this));
	});
	$(".open-modal-settings").off().on("click",function(e){
		// e.preventDefault();
		e.stopImmediatePropagation();
		
		
        getAjax(null, baseUrl+'/'+moduleId+'/settings/myAccount/type/citoyens/id/'+userId,
			function(data){
				ajaxPost(null, baseUrl+'/'+moduleId+'/default/render?url=co2.views.element.confirmDeleteModal',
					{id:userId,type:"citoyens"},
					function(res){
						mylog.log("view views/element/confirmDeleteModal.php",res);
						$("#page-top").append(res);
					});
				mylog.log("callback view account settings",data);
				str='<div style="position: absolute;right: 10px;top: 5px; font-size: 22px;cursor: pointer;"><a onclick="event.stopPropagation();document.getElementById(`modal-account-settings`).style.display = `none`;"><i class="text-white fa fa-times"></i></a></div>';
				strprofil='<div class="col-sm-6 col-xs-12 div-btn-setting">'+
			            '<a data-id="'+userConnected._id.$id+'" data-type="'+userConnected.collection+'" class="btn btn-setting-update icon-btn-update btn-primary" id="btn-update-profil" href="javascript:;"><span class="btn-u-icon fa fa-pencil img-circle text-primary"></span>Mettre à jour mes données de compte</a>'+
						'</div>';
				$("#modal-account-settings").html("").css("display","block").append(str+data);
				$("#myAccount-settings").append(strprofil);

				$("#btn-delete-element-setting").on("click", function(e){
					e.stopImmediatePropagation();
					contextData=$.extend(true,{},userConnected);
	               contextData.id=userConnected._id.$id;
				   contextData.type=userConnected.collection;
	    	        $("#modal-delete-element").modal("show");
	            });
				$("#btn-update-profil").on("click",function(e){
					e.stopImmediatePropagation();
	    	        var id = $(this).data("id");
                    var collection = $(this).data("type");

                    var editDynF = {
				        dynForm : typeObj.person.dynForm
			        };

			
                
            
                    mylog.log("dynform minimum",editDynF);
                    editDynF.dynForm.jsonSchema.properties=costum.minimumContactForm;
            // editDynF.dynForm.jsonSchema=costum.minimumContactForm;

			// jQuery.extend(true, {}, );
            // mylog.log("editDynF",editDynF);
                    editDynF.onload = {
                        actions : {
                            hide : {
                                "otherEmailtext" : 0,
                                "similarLinkcustom" : 1
                            },
                            setTitle : "<i class='fa fa-user'></i> Mettre à jour mes informations"
                        }
                    };
            
			        ajaxPost(
	                    null,
	                    baseUrl+"/"+moduleId+"/element/get/type/"+collection+"/id/"+id+"/update/true",
	                    {},
	    	            function (data) {
				            if (userId && userId == id || isInterfaceAdmin) {
                                dyFObj.openForm(editDynF,"afterLoad",data.map);
                            } else {
                                toastr.error("Vous n'êtes pas autorisé a modifier cet contact");
                            }
			            }
			        );
		        });
			},"html"
		);
	});	
			
	

});
</script>