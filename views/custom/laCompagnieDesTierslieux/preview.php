<?php

$cssAnsScriptFilesModule = array(
    '/js/default/preview.js',
    '/js/default/profilSocial.js',
    '/js/links.js'


);

// var_dump($type,$element);exit;
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

	$financers=[];
    $partners=[];
    $porteurCTE=[];
    $porteurAction=[];
    
    $params["nbContributors"]=0;


 
    $where=array("id"=>@$id, "type"=>$element["collection"], "doctype"=>"image", "contentKey"=> "slider");
    $images = Document::getListDocumentsWhere($where, "image");

    $visioDates=[];
    
    if(isset($element["visioDate"]["alldates"])){
        foreach($element["visioDate"]["alldates"] as $date =>$sched){ 
                
            foreach($sched as $i=>$start){
                // var_dump($date,$start);exit;   
                $visioDates[]=$date." ".$start; 
            }    
        }
    }
    

$colorCode="";
$colorClass="dark";
$deleteLabel="";
$editLabel="";
if(isset($element["category"])){
if($type==Event::COLLECTION && $element["category"]=="sessionFormation"){
    $colorCode="orange !important";
    $colorClass="orange";
    $deleteLabel="Supprimer la session";
    $editLabel="Editer la session";
}else if($type==Project::COLLECTION && $element["category"]=="formation"){
    $colorCode="#004e87 !important";
    $colorClass="nightblue";
    $deleteLabel="Supprimer la formation";
    $editLabel="Editer la formation";

}
}
?>

<style type="text/css">

.preview-main-container{
    padding: 0px 40px;
}

.preview-main-container > div{
    margin-top:15px;
}

.btn-edit-preview:hover .show-hover, .btn-delete-element-preview:hover .show-hover{
    display: inline-block;
}

    .show-hover{
        display:none;
    }
    /* .show-hover:hover{
        display:inline-block;
    } */
    
    .social-share-button img{
        margin-right: 10px;
    }

    .listsItemsPod{
        text-align:left;
        /* width: fit-content !important;
        float: left; */
        margin-left:30px;
    }

    .listsItemsPod a{
        margin-left: 10px;
    }

    .listsItemsPod h4{
        display:none;
    }

    h3, h4, h5{
        text-transform:none;
    }
    #modal-preview-coop h3, .morelink, .text-highlight, .pod-info-Address span, #modal-interesting li{
        color:#009aba;
    }
    .pod-info-Address{
        float:left;
        width:100%;
    }
    h4, h5, .section-title, .morecontent strong,  .morecontent li::marker{
        color : #1b3b53;
        font-weight:700;
        font-size:16px !important;
    }

    .section-title:first-letter{
        color:white;
        font-size:larger;
    }

    .section-title:before{
        content: "";
        position: absolute;
        margin-top: -5px;
        z-index: -1;
        margin-left: -20px;
        width: 2em;
        height: 2em;
        background-color: <?=$colorCode?>;
        border-radius: 50%;
    }
    .section-title{
        margin-top:20px;
    }
    p, .morecontent p, .morecontent ul, .morecontent li{
        color:#575756;
        font-size:16px !important;
    }
    .community-pod{
        /* display: inline-flex; */
    }
    #modalLogin, #modalRegister{
        z-index:200001;
    }
</style>
<div class="col-xs-12 padding-10 toolsMenu">
    <div class="left-preview-header">
    <?php 
    if($type==Event::COLLECTION && isset($element["category"]) && $element["category"]=="sessionFormation"){
        if($canEdit){
    ?>
        <button class="btn pull-left margin-left-20 bg-orange openSession" data-id="<?= $element['_id'] ?>"><i class="fa fa-cog"></i> Gérer cette session</button>
    <?php } ?>
    <?php 
    
     $interestedStatus='<button class="btn btn-default pull-left bg-orange margin-left-20 interesting-session">
     <i class="fa fa-heart"></i> Je suis intéressé.e par cette session de formation</button>';
 
     if(!empty(Yii::app()->session["userId"]) && isset($element["links"]["attendees"][Yii::app()->session["userId"]]["roles"]) && in_array("Intéressé.e",$element["links"]["attendees"][Yii::app()->session["userId"]]["roles"])){
        $interestedStatus='<button class="btn btn-default pull-left bg-orange margin-left-20 disabled">
        <i class="fa fa-heart"></i> Je suis déjà intéressé.e</button>';
     }
    //  if(!Authorisation::isElementAdmin($element["_id"], $element["collection"] ,Yii::app()->session["userId"])){
        echo $interestedStatus;
    //  }

     
     ?>    
    
    <?php }
    // var_dump($type,$element);exit;
    if($type==Project::COLLECTION && isset($element["category"]) && $element["category"]=="formation"){
        if($canEdit){
    ?>
        <button class="btn pull-left margin-left-20 bg-orange" onclick="urlCtrl.closePreview();dyFObj.openForm('sessionFormation','afterLoad');" data-parent-name="<?= $element['name'] ?>" data-parent-id="<?= $element['_id'] ?>" data-parent-type="<?= $element["collection"] ?>">Ajouter une session pour cette formation</button>
    <?php } 
    }
    ?>
    <?php 
    // var_dump($type,$element);exit;
    if($type==Organization::COLLECTION ){
        if($canEdit){
    ?>
        <a href="#@<?= $element["slug"] ?>" class="lbh btn pull-left margin-left-20 bg-dark">Aller sur la fiche d'identité</a>
    <?php } 
    }
    ?>
    </div>
    <div class="right-preview-header">
	<button class="btn btn-default pull-right btn-close-preview">
		<i class="fa fa-times"></i>
	</button>
    <?php 
		if (isset($canEdit) && $canEdit) { 
            ?>
			<button class="btn btn-default pull-right margin-right-10 text-white btn-edit-preview bg-<?=$colorClass?>" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo (($type==Event::COLLECTION || $type==Project::COLLECTION)&& isset($element["category"])) ? $element["category"] : $type ?>">
				<i class="fa fa-pencil"></i> <span class="show-hover"> <?= $editLabel ?></span>
			</button>
            <button style="background-color:<?=$colorCode?>" class="btn btn-default pull-right margin-right-10 btn-delete-element-preview bg-red text-white" data-type="<?php echo $type ?>" data-id="<?php echo $element["_id"] ?>" data-subtype="<?php echo ($type==Event::COLLECTION && isset($element["category"])) ? $element["category"] : $type ?>">
				<i class="fa fa-trash"></i><span class="show-hover"> <?= $deleteLabel ?></span>
			</button>
		<?php }?>
    </div>    
    
</div>


<div class="container-preview col-xs-12 margin-bottom-20" style="overflow-y: scroll">
<?php 
    if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"]))){
        // $url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
        $url= Yii::app()->theme->baseUrl . '/assets/img/background-onepage/connexion-lines.jpg';
    }else{
        $url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
    }    
    ?> 
        
    <div class="col-xs-12 no-padding" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
        <?php 
            $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
                alt="'.Yii::t("common","Banner").'" 
                src="'.$url.'">';
            if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
                $imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
                            class="thumb-info"  
                            data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
                            data-lightbox="all">'.
                            $imgHtml.
                        '</a>';
            }
            // echo $imgHtml;
        ?>      
    </div>
    <div class="content-img-profil-preview col-xs-6 col-xs-offset-3 col-lg-4 col-lg-offset-4">
        <?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?> 
        <a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
        class="thumb-info"  
        data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
        data-lightbox="all">
            <!-- <img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" /> -->
        </a>
        <?php }else{ ?>
            <!-- <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/> -->
        <?php } ?>
    </div>
    <div class="col-xs-12 margin-bottom-20 preview-main-container">
        <?php if($element["collection"]==Event::COLLECTION) {  
            
            if(isset($element["links"]["organizer"])){
                foreach($element["links"]["organizer"] as $idorg => $valorg){
                    if($valorg["type"]==Project::COLLECTION){
                        $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                        $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                        break;
                    }
                }
               
            }
            // var_dump($formation);exit;
            
            // $formation=PHDB::findOne(Project::COLLECTION,array());
            $descr= (@$element["description"]) ? $element["description"] : ""; 
            ?>
         
        <div class="col-xs-12 title-event">    
        <?php 
            if(!empty($breadCrumbFormation)){
                echo $breadCrumbFormation;
            }
        ?>    
            <h3 class="title margin-bottom-20  text-<?= $colorClass ?>" style="display:inline-flex;text-transform: none;">
        <?php echo $element["name"] ?>  
        </h3>
        </div>
        <h4 class="col-xs-12 section-title" style="font-weight:700;">Description</h4>
        <div class="col-xs-12 more" id="descriptionAbout" style="margin-top: none;">  <?php echo $descr ?>
        </div>
        
        <div class="col-xs-12" id="public" style="margin-bottom: 10px;">
            <?php $public=(isset($element["preferences"]["private"]) && $element["preferences"]["private"]==false) ? "Publique" : "Privée" ;?>
            <p class="section-title">Visibilté dans l'annuaire public des sessions : <span class="text-highlight"><?=$public?></span></p>
        </div>
        <div class="col-xs-12" id="dates" style="margin-bottom: 10px;">
            <?php 
            $format = "d M Y"; //H:i
            $startDate=date_format(date_create($element["startDate"]), $format);
            $endDate=date_format(date_create($element["endDate"]), $format);
            // var_dump($startDate);exit;
            ?>
            <p class="col-xs-4 no-padding section-title">Date de début de session : <br/><span class="text-highlight"><?=$startDate?></span></p>
            <p class="col-xs-4 section-title">Date de fin de session : </br><span class="text-highlight"><?= $endDate?></span></p>
            <?php if(isset($element["duration"])){ ?>
                <p class="col-xs-4 section-title">Durée de la session : </br><span class="text-highlight"><?=$element["duration"]?> jours</span></p>
            <?php } ?>
            </div>
        <?php if(!empty($element["email"])){?>
        <div class="col-xs-12" id="email" style="margin-bottom: 10px;">
            <?php ?>
            <p class="section-title">Email de contact: <span class="text-highlight"><?=$element["email"]?></span></p>
        </div>
        <?php } ?>
        <?php if(!empty($visioDates)){?>
        <div class="col-xs-12" id="url" style="margin-bottom: 10px;">
            <?php ?>
            <p class="section-title">Date des visioconférences d'information
             <?php 
                echo "<ul>";                 
                    foreach($visioDates as $i=>$date){
                        echo '<span class="text-highlight"><li>Le '.date('d/m/Y à H:i', strtotime(str_replace("/","-",$date))).'</li> </span>';
                    }
                 echo "</ul>"   
             
             ?>
            </p>
        </div>
        <?php } ?>
        <?php if(!empty($element["gatheringDates"])){?>
        <div class="col-xs-12" id="url" style="margin-bottom: 10px;">
            <?php ?>
            <p class="section-title">Les journées de formation
             <?php 
                echo "<ul>";                 
                    foreach($element["gatheringDates"] as $i=>$date){
                        echo '<span class="text-highlight"><li>Le '.date('d/m/Y', strtotime($date)).'</li> </span>';
                    }
                 echo "</ul>"   
             
             ?>
            </p>
        </div>
        <?php } ?>
        <?php if(!empty($element["url"])){?>
        <div class="col-xs-12" id="url" style="margin-bottom: 10px;">
            <?php ?>
            <p class="section-title">Lien vers le formulaire d'inscription<span class="text-highlight"><?=$element["url"]?></span></p>
        </div>
        <?php } ?>
        <?php if(!empty($element["links"]["attendees"])){ ?>
        <div class="col-xs-6 community-pod" id="organizer-pod" style="margin-bottom: 10px;">
        <p class="col-xs-12 no-padding pull-left section-title">Les participants : </p>
            <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les participants", "links"=>$element["links"]["attendees"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
            
        </div>
        <?php } ?>
        <?php if(!empty($element["trainer"])){?>
        <div class="col-xs-12 community-pod" id="trainer-pod" style="margin-bottom: 10px;">
        <p class="col-xs-12 no-padding pull-left section-title">Les intervenants : </p>
            <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
            
        </div>
        <?php } ?>
        <?php if(!empty($element["partner"])){?>
        <!-- <div class="col-xs-6 community-pod" id="organizer-pod" style="margin-bottom: 10px;">
        <p class="col-xs-12 no-padding pull-left section-title">Les partenaires : </p>
            <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
            
        </div> -->
        <?php } ?>
        <?php if(!empty($element["address"])){?>
            <div class="col-xs-12" id="address" style="margin-bottom: 10px;">
            <p class="pull-left section-title">Adresse : </p>
            <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
            
            </div>

        <?php } ?>    
        
        <?php } else if($element["collection"]==Project::COLLECTION) {  
            
            if(isset($element["links"]["organizer"])){
                foreach($element["links"]["organizer"] as $idorg => $valorg){
                    if($valorg["type"]==Project::COLLECTION){
                        $formation=PHDB::findOne(Project::COLLECTION,array("_id"=>new MongoId($idorg)),array("name","collection"));
                        $breadCrumbFormation=!empty($formation) ? '<a style="display:inline-flex" href="#page.type.'.$formation["collection"].'.id.'.(string)$formation["_id"].'" class="lbh-preview-element">'.$formation["name"].'</a> <span> > </span>' : null;
                        break;
                    }
                }
               
            }
            // var_dump($formation);exit;
            
            // $formation=PHDB::findOne(Project::COLLECTION,array());
            $descr= (@$element["description"]) ? $element["description"] : ""; 
            ?>
         
        <div class="col-xs-12 title-event">    
        <?php 
            if(!empty($breadCrumbFormation)){
                echo $breadCrumbFormation;
            }
        ?>    
            <h3 class="title margin-bottom-20 text-<?= $colorClass ?>" style="display:inline-flex;text-transform: none;">
        <?php echo $element["name"] ?>  
        </h3>
        </div>
        <h4 class="col-xs-12" style="font-weight:700;">Contenu, objectifs pédagogiques</h4>
        <div class="col-xs-12 more" id="descriptionAbout" style="margin-top:none;">  <?php echo $descr ?>
        </div>
        <?php if (isset($element["urlsDoc"])){ ?>
        <div class="col-xs-12" id="urlsDoc" style="margin-bottom: 10px;"> 
            <?php 
             $urlsDoc="<ul>";
            foreach($element["urlsDoc"] as $ind=>$l){
                $urlsDoc.="<li style='font-size:12px'><a href='".$l."' target='_blank'>".$l."</a></li>";

            } 
            $urlsDoc.="</ul>";
            ?>
            <p class="section-title">Liens vers la documentation, le programme : <span class="text-highlight"><?= $urlsDoc ?></span></p>
        </div>
        <?php } ?>
        <?php if (isset($element["rate"])){ ?>
        <div class="col-xs-12" id="rate" style="margin-bottom: 10px;">
            
            <p class="section-title">Tarif : <span class="text-highlight"><?=$element["rate"]?></span> euros</p>
        </div>
        <?php } ?>
        <?php if (isset($element["email"])){ ?>
        <div class="col-xs-12" id="email" style="margin-bottom: 10px;">
            
            <p class="section-title">Pour plus d'informations : <span class="text-highlight"><?=$element["email"]?></span></p>
        </div>
        <?php } ?>
        <?php if (isset($element["trainer"])){ ?>
        <!-- <div class="col-xs-6 community-pod" id="trainer-pod" style="margin-bottom: 10px;">
        <p class="col-xs-12 no-padding pull-left section-title">Les intervenants : </p>
            <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les intervenants", "links"=>$element["trainer"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
            
        </div> -->
        <?php } ?>
        <?php if (isset($element["partner"])){ ?>
        <div class="col-xs-12 community-pod" id="organizer-pod" style="margin-bottom: 10px;">
        <p class="col-xs-12 no-padding pull-left section-title">Les partenaires : </p>
            <?php echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les partenaires", "links"=>$element["partner"], "connectType"=>"attendees", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"])); ?>
            
        </div>
        <?php } ?>
        <?php if(!empty($element["address"])){?>
            <div class="col-xs-12" id="address" style="margin-bottom: 10px;">
            <p class="pull-left section-title">Adresse : </p>
            <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => false , "openEdition" => false)); ?>
            
            </div>

        <?php } ?>    
        
        <?php }  ?>
    </div>
    <?php if($element["collection"]==Organization::COLLECTION){ ?>
    <div class="preview-element-info col-xs-12">
		<?php 

					if(isset($element["name"])){ ?>
						<h3 class="text-center margin-top-40"><?php echo $element["name"] ?></h3>
					<?php } ?>
					<div class="col-xs-12 address"> 
			<?php		if(isset($element["address"]["addressLocality"])){ ?>
						<div class="header-address col-xs-12 text-center blockFontPreview margin-top-20">
							<i class="fa fa-map-marker"></i> 
							<?php
								echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
								echo !empty($element["address"]["postalCode"]) ? 
										$element["address"]["postalCode"].", " : "";
								echo $element["address"]["addressLocality"];
							?>
						</div>
					<?php } ?>
				</div>
				<?php
				if(isset($element["url"])){ ?>
					<div class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
						<?php $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ; ?>
						<a href="<?php echo $scheme.$element['url'] ?>" target="_blank" id="urlWebAbout" style="cursor:pointer;"><?php echo $element["url"] ?></a>
					</div>
				<?php } ?>
				<?php 
				if (isset($element["socialNetwork"])){?>
					<div id="socialNetwork" class="col-xs-10 col-xs-offset-1 margin-top-10 text-center">
				<?php 

					if (isset($element["socialNetwork"]["facebook"])){ ?>		
			          <span id="divFacebook" class="margin-right-10">
			              <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["facebook"];?>" target="_blank" id="facebookAbout" >
			                <i class="fa fa-facebook" style="color:#1877f2;"></i>
			              </a>
			          </span>
			  <?php }
			         
			          if (isset($element["socialNetwork"]["twitter"])){ ?>	
			          <span id="divTwitter">
			              <a class="contentInformation btn-network tooltips" href="<?php echo $element["socialNetwork"]["twitter"];?>" target="_blank" id="twitterAbout" >
			                <i class="fa fa-twitter" style="color:#1da1f2;"></i>
			              </a>
			          </span>
			          <?php } ?>
			        </div> 
				
				<?php

					
				}						
					if(isset($element["shortDescription"])) { ?>
						<div class="col-xs-10 col-xs-offset-1 margin-top-10">
							<h2> Description courte</h2>
							<span class="col-xs-12 text-center" id="shortDescriptionHeader"><?php echo ucfirst(substr(trim(@$element["shortDescription"]), 0, 180)); ?>
							</span>	
						</div>
					<?php } ?>
					<?php $tabSpec=array("typePlace"=>
											["name"=>"Typologie d'espace de travail",
											"icon"=>"briefcase"
											],
										"services"=>
											["name"=>"Services proposés",
											"icon"=>"tags"
											],
										"manageModel"=>	
											["name"=>"Mode de gestion",
											"icon"=>"adjust"
											],
										"state"=>	
											["name"=>"Etat du projet",
											"icon"=>"cubes"
											],
										"spaceSize"=>	
											["name"=>"Taille du lieu",
											"icon"=>"expand"
											],
										"certification"=>	
											["name"=>"Label obtenu",
											"icon"=>"medal"
											],
										"greeting"=>	
											["name"=>"Actions d'accueil",
											"icon"=>"info"
											],
										"network" =>	
											["name"=>"Réseau d'affiliation",
											"icon"=>"users"
											]
										);

			if(isset($element["tags"]))	{		
?>			
				<div class="col-xs-10 col-xs-offset-1 margin-top-20"> 
						<h2>Caractéristiques du lieu</h2>
						<table class='col-xs-12'>
				<?php	foreach($tabSpec as $k => $v){ 

							$strTags="";
							$tagsList="";
							$listCurrent=$this->costum["lists"][$k];
							if(!empty($element["tags"])){
								foreach($element["tags"] as $tag){
									if(in_array($tag, $listCurrent)){
										$strTags.=(empty($strTags)) ? $tag : "<br>".$tag; 
									}
									
									
								}

							}
							if(!empty($strTags)){
							?>
							
								
									<tr>
										<td class="col-xs-1 text-center" style="vertical-align: top;padding:10px;">
											<?php  
											if(isset($v["icon"])){
												echo "<i class='fa fa-".$v["icon"]." tableIcone'></i>";
											}
											?>
										</td>	
										<td class="col-xs-4" style="vertical-align: top;padding:8px;font-weight: bold;font-size: 17px;font-variant: small-caps;">
											<?php  
											if(isset($v["name"])){
												echo $v["name"];
											}
											
											?>		
										</td>
										<td class="col-xs-7" style="padding:8px;"><?php echo $strTags; ?></td>
									</tr>	
									
						
							
								
						<?php 
						}}
						 ?>
						</table>	
						

						
			<?php 	
						foreach($element["tags"] as $tag) {	
							if(!in_array($tag, $listCurrent)) {
								$tagsList.= "#".$tag." ";	
							}
						}	  
					?>
					<span class="tagList"><?php echo $tagsList ?></span>

				</div>	
			<?php } ?>		



				<div class="col-xs-12">
			<?php 
				if(isset($element["links"]) && isset($element["links"]["members"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$element["collection"]));
				} ?>
			</div> 
				
		
	</div>
    <?php } ?>
    <!-- Modal -->
<div id="modal-delete-element-preview" class="modal fade" role="dialog" style="z-index :1000000;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-white"><?php echo Yii::t("common","Delete")?></h4>
      </div>
      <div class="modal-body text-dark">
        <p>

        	<?php 

        		if($id == Yii::app()->session["userId"] && $type == Person::COLLECTION){
        			echo "Si vous supprimer votre comptes, toutes vos informations personnelles et vos messages seront supprimer";
        		} else {
        			echo Yii::t('common',"Are you sure you want to delete"). " <span style='font-weight:bolder'>". $element["name"]."</span>" ;
        		}

        	?>	
        </p>
        <!-- <br>
        	<?php 
            // echo Yii::t('common','You can add bellow the reason why you want to delete this element :') ;
            ?>
        <textarea id="reason" class="" rows="2" style="width: 100%" placeholder="Laisser une raison... ou pas (optionnel)"></textarea> -->
      </div>
      <div class="modal-footer">
				<!-- Utilisation du bouton confirmDeleteElement -->
       <button id="confirmDeleteElementPreview" data-id="<?php echo $element["_id"]?>" data-type="<?php echo $element["collection"]?>" type="button" class="btn btn-warning"><?php echo Yii::t('common','I confim the delete !');?></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
      </div>
    </div>

  </div>
</div>

<div id="modal-interesting" class="modal fade" data-dismiss="modal" role="dialog" style="z-index :1000000;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-white"><?php echo Yii::t("common","Inscription à la visio d'information")?></h4>
      </div>
      <div class="modal-body text-dark">
        <p>

        	Des visio-conférences sont régulièrement organisées pour vous informer en détails des modalités relatives aux contenus et au déroulé de la session.
            Dans la liste ci-après choisissez, la visio-conférence d'informations qui vous conviendrait.
        </p>
        <?php
    //  "visioDate" : {
    //     "alldates" : {
    //         "12/10/2023" : [ 
    //             "8:00"
    //         ],
    //         "13/10/2023" : [ 
    //             "11:00"
    //         ],
    //         "14/10/2023" : [ 
    //             "15:00"
    //         ]
    //     },
    //     "eventduration" : "1"
    // },   
        if(isset($element["visioDate"]["alldates"])){
            echo "<ul>";
        foreach($element["visioDate"]["alldates"] as $date =>$sched){
            
            foreach($sched as $i=>$start){
                $dayDate=str_replace("/","-",$date);
                $formatedDate=date('Y-m-d\TH:i:s\Z', strtotime($dayDate." ".$start));
                // date_format(date_create($date." ".$start), 'd/m/Y H:i');
                // .moment("12/10/2023 11:00",'DD/MM/YYYY hh:mm').toISOString();
                echo "<a href='javascript:;' class='choose-visio-date' data-date='".$formatedDate."'><li>Le ".$date." à ".$start."</li></a>";

                
            }
            // var_dump($formatedDate);
            
              
// $ssm2 = strtotime("3:33") - $midnight;

//             echo
        }
            echo "</ul>";
        
        if(isset($element["visioDate"]["eventduration"])){
            echo "<i class='fa fa-info'></i> Durée : ".$element["visioDate"]["eventduration"]." h";
       

                
            
        }
    }

        
 
        
        ?>
        <!-- <br>
        	<?php 
            // echo Yii::t('common','You can add bellow the reason why you want to delete this element :') ;
            ?>
        <textarea id="reason" class="" rows="2" style="width: 100%" placeholder="Laisser une raison... ou pas (optionnel)"></textarea> -->
      </div>
      <!-- <div class="modal-footer">
				Utilisation du bouton confirmDeleteElement
       <button id="confirmDeleteElementPreview" data-id="<?php echo $element["_id"]?>" data-type="<?php echo $element["collection"]?>" type="button" class="btn btn-warning"><?php echo Yii::t('common','I confim the delete !');?></button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo Yii::t('common','No');?></button>
      </div> -->
    </div>

  </div>
</div>

<script type="text/javascript">
    var typePreview=<?php echo json_encode($element["collection"]); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
    var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$element["collection"]) ); ?>; 
	
    function getDateFormated(params){
       
            params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
            params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
            params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
            params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
            params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
        
            params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
            params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
            params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
            params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
           
            params.startMonth = directory.getMonthName(params.startMonth);
            params.endMonth = directory.getMonthName(params.endMonth);        

            mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
       
        
            var str = '';

            if(params.startDate != null){
                    str += params.startDay + ' ' + params.startMonth + 
                    ' ' + params.startYear + '';
            }   
            return str;
        
    }

    function verifyMail(contextElement){
        // $(domEl2Close).modal("hide");
        // alert(typeof contextElement);
        // alert(contextElement);
        bootbox.prompt({
                title: " <span class='text-white'>Renseignez votre adresse email</span>",
                message: '<p>Pour vous inscrire à la visio d\'informations et nous permettre de vous recontacter, nous vous invitons à créer un compte.</p>',
                callback: function (result) {
                    if(result){
                        // ajaxPost(
                        //     null,
                        //     baseUrl+`/co2/link/answerwithemail/`,
                        //     {
                        //         id : params.id,
                        //         form : params.form,
                        //         email : result,
                        //         resLocation : params['redirect'] ? params['redirect'] : ""
                        //     }
                        //     , function(res){

                        //     if(res.status && notNull(res.location)){
                        //        window.location.assign(res.location);
                        //        window.location.reload();
                        //     }else{
                        //         toastr.error(res.msg);
                        //     }
                        //     },
                        //     null,
                        //     null,
                        //     {"async": false}
                        // )
                    }
                }
            });

            var emailLastVal = "";
            var lastStat = false;
            var bootboxModal = setInterval(function() {
                if($('.bootbox.modal.fade.bootbox-prompt').is(':visible')) {
                    const emailRegex = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
                    $('.bootbox-input.bootbox-input-text.form-control').on('keyup', delay(function(e) {
                        var self = this;
                        var mailValue=$(this).val();
                        if($(this).val().match(emailRegex) && emailLastVal != $(this).val()) {
                            emailLastVal = $(this).val();
                            lastStat = true;
                            var resVal = false;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : "",
                                    form : "",
                                    email : $(self).val()
                                },
                                function(res){
                                    if(res ) {
                                        lastStat = false;
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);

                                        // && res.duplicated default condition
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" onclick="document.getElementById('email-login').value='`+mailValue+`';">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res.tobeactivated) {
                                            lastStat = false;
                                            msgError = trad.ATemporaryAccountExists;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="document.getElementById('email2').value='`+mailValue+`';">
                                                    <i class="fa fa-envelope"></i>
                                                    ${trad.ReceiveAnotherValidationEmail}
                                                </a>
                                            `;
                                        }else if(!res.duplicated){
                                            msgError = "Créez votre compte en cliquant sur le bouton ci-dessous";
                                            lastStat = false;
                                            var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalRegister" data-toggle="modal" data-dismiss="modal" onclick='document.getElementById("email3").value="`+mailValue+`";document.getElementById("callbackRegister").setAttribute("element",`+JSON.stringify(contextElement)+`);document.getElementById("callbackRegister").dataset.action="connect";document.getElementById("sessionRegister").checked=true;'>
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.registration}
                                            </a>`;
                                            
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $("#modal-preview-coop").css("display","none"); 
			                                $("#modal-preview-coop").html("");
                                            $('.bootbox.modal.fade.bootbox-prompt').hide();
                                        });
                                        resVal = e.which !== 13;
                                    }
                                    //  else if(res && !res.duplicated) {
                                    //     $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                    //     $('.bootbox-form-error').remove();
                                    //     resVal = true
                                    // }
                                },
                                null,
                                null,
                                {
                                    async:false
                                }
                            );
                            return resVal;
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                                return lastStat
                            } else {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                return e.which !== 13;
                            }
                        }
                    }, 350));
                    $('.bootbox-input.bootbox-input-text.form-control').on('blur', function() {
                        var self = this;
                        var mailValue=$(this).val();
                        if($(self).val() == '') {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        } else if($(self).val().match(emailRegex) && emailLastVal != $(self).val()) {
                            $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                            emailLastVal = $(self).val();
                            lastStat = true;
                            ajaxPost(
                                null,
                                baseUrl+`/survey/answer/checkemailuser/`,
                                {
                                    id : "",
                                    form : "",
                                    email : $(self).val()
                                }
                                , function(res){
                                    if(res ) {
                                        // && res.duplicated default condition
                                        $('.btn.btn-primary.bootbox-accept').prop('disabled', true);
                                        lastStat = false;
                                        var msgError = trad.AnActiveAccountExists;
                                        var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalLogin" data-toggle="modal" data-dismiss="modal" onclick="document.getElementById('email-login').value='`+mailValue+`';">
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.login}
                                            </a>`;
                                        if(res.tobeactivated) {
                                            msgError = trad.ATemporaryAccountExists;
                                            lastStat = false;
                                            var btnHTML = `
                                                <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10 openModalResendEmail" data-toggle="modal" data-target="#modalSendActivation" onclick="document.getElementById('email2').value='`+mailValue+`';">
                                                    <i class="fa fa-envelope"></i>
                                                    Vous figurez parmi les contacts de la Compagnie des Tiers-Lieux. Cliquez sur le bouton pour recevoir un e-mail et ainsi créer/finaliser votre compte.
                                                </a>
                                            `;
                                        }else if(!res.duplicated){
                                            msgError = "Créez votre compte en cliquant sur le bouton ci-dessous";
                                            lastStat = false;
                                            var btnHTML = `
                                            <a href="javascript:;" class="btn btn-default bg-white letter-blue bold margin-top-10" data-target="#modalRegister" data-toggle="modal" data-dismiss="modal" onclick='document.getElementById("email3").value="`+mailValue+`";document.getElementById("callbackRegister").setAttribute("element",`+JSON.stringify(contextElement)+`);document.getElementById("callbackRegister").dataset.action="connect";document.getElementById("sessionRegister").checked=true;'>
                                                <i class="fa fa-sign-in"></i>
                                                ${tradCms.registration}
                                            </a>`;
                                            
                                        }
                                        $('.bootbox-form-error').remove();
                                        $('.bootbox-form').append(`
                                            <div class="bootbox-form-error text-center">
                                                <span class="bg-white alert-danger margin-top-10">${msgError}</span><br />
                                                ${btnHTML}
                                            </div>
                                        `).animate({ opacity:1 }, 300 );
                                        $('.openModalResendEmail').off().on('click', function() {
                                            $("#modal-preview-coop").css("display","none"); 
			                                $("#modal-preview-coop").html("");
                                            $('.bootbox.modal.fade.bootbox-prompt').hide();
                                        });
                                    }
                                    //  else if(res && !res.duplicated) {
                                    //     $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                    //     $('.bootbox-form-error').remove();
                                    // }
                                }
                            );
                        } else {
                            if(lastStat) {
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', false);
                                $('.bootbox-form-error').remove();
                            } else
                                $('.btn.btn-primary.bootbox-accept').prop('disabled', true)
                        }
                    })
                    clearInterval(bootboxModal)
                }
            },200)
    }
	jQuery(document).ready(function() {
        
        inintDescs();
        $("#divMapContent").show(function () {
            afficheMap();
        });
          
        var eltPreview=<?php echo json_encode($element); ?>;
		coInterface.bindTooltips();
        coInterface.bindLBHLinks();
        $(".markdown-txt").each(function(){
            descHtml = dataHelper.markdownToHtml($(this).html()); 
            var tmp = document.createElement("DIV");
            tmp.innerHTML = descHtml;
            descText = tmp.textContent || tmp.innerText || "";
            $(this).html(descText);
        });

        $(".interesting-session").off().on("click",function(){
            $("#modal-interesting").modal("show");

        });

        $(".choose-visio-date").off().on("click",function(){
            // var refLink=(typeof contextData.interestedLinked!="undefined") ? contextData.interestedLinked.split("connect")[1] : "";
            var chosenDate = $(this).data("date");
            var contextElement={
                chosenDate : chosenDate,
                id : contextData._id.$id,
                interestedLink : contextData.interestedLink,
                collection : contextData.collection,
                category : contextData.category
            };
            // contextData.chosenDate=$(this).data("date");
            if(userConnected!=null && typeof userConnected._id!="undefined" && typeof userConnected._id.$id!="undefined"){
                // params2Save={
                //     value:chosenDate,
                //     path: "links.events."+contextData._id.$id+".visioDate",
                //     id : userConnected._id.$id,
                //     collection : userConnected.collection
                // }
                var refLink=(typeof contextElement.interestedLink!="undefined") ? contextElement.interestedLink.split("connect")[1] : "";
                var visioDate=(typeof contextElement.chosenDate!="undefined") ? contextElement.chosenDate : "";
                var extraUrlConnect="?visioDate="+visioDate;
                links.connectAjax(contextData.collection, contextData._id.$id, userConnected._id.$id, "citoyens", "attendees", ["Intéressé.e"], function(){urlCtrl.loadByHash(location.hash);},extraUrlConnect);
                // mylog.log("params2Save",params2Save);
                
                

                // dataHelper.path2Value(params2Save,function(data){

                // });
            }else{
                var contextElementStr=JSON.stringify(contextElement);
                verifyMail(contextElementStr);

            }
            $("#modal-interesting").modal("hide");

        })

        $(".openSession").off().on('click', function () {
                var formation = $(this).data("id");
                // var  = ';
                var modalDom = $('#session-modal');
                ajaxPost(null, baseUrl + '/costum/lacompagniedestierslieux/gestionsession/sessionId/' + formation, null, function (html) {
                    modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                        modalDom.css('padding', '');
                    });
                    modalDom.find('.modal-body').empty().html(html);
                    modalDom.modal('show');
                })
        });
        // $(".addNewSession").off().on("click",function(){
        //     var idFormation=$(this).data("parent-id");
        //     var typeFormation=$(this).data("parent-type");
        //     var nameFormation=$(this).data("parent-name");
        //     var formationObj={};
        //     formationObj[idFormation]={
        //         type : typeFormation,
        //         name : nameFormation
        //     };
        //     dyFObj.openForm("sessionFormation", "afterLoad", {organizer:formationObj});
        // });
        
        $(".container-preview .social-share-button").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : [{"type":"facebook"}, {"type":"twitter"} ], "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
        resizeContainer();

        $(".btn-edit-preview").off().on("click",function(){
			// alert("heure");
			$("#modal-preview-coop").hide(300);
			$("#modal-preview-coop").html("");
            var typeElem=$(this).data("type");
            var idElem=$(this).data("id");
            var subElem=$(this).data("subtype");
            var ctrlElem=typeObj[typeObj[typeElem].sameAs].ctrl;
            var extTypObj=(typeElem!==subElem) ? subElem : typeElem;
            mylog.log(typeElem,subElem,extTypObj);
            var editPrev=jQuery.extend(true, {},costum.typeObj[extTypObj].dynFormCostum);
            // editPrev.onload.actions.hide.patternselect = 1;
			// editPrev.onload.actions.hide.similarLinkcustom = 1;
            // // if(costum.isCostumAdmin){
            //     editPrev.beforeBuild.properties.template=dyFInputs.checkboxSimple("false", "template", {
            //         "onText": trad.yes,
            //         "offText": trad.no,
            //         "onLabel": tradDynForm.public,
            //         "offLabel": tradDynForm.private,
            //         "labelText": "Atelier récurrent (enregistrer en tant que modèle d'atelier)",
            //         "labelInformation": tradDynForm.explainvisibleevent
            //     })
            // // }  
			mylog.log("editPrev",editPrev);     

                        
            dyFObj.editElement(typeElem,idElem, subElem, editPrev);  
        });
        $("#confirmDeleteElementPreview").off().on("click", function(){
	    var url = baseUrl+"/"+moduleId+"/element/delete/id/"+contextData.id+"/type/"+contextData.type;
	    mylog.log("deleteElement", url);
		var param = new Object;
		// param.reason = $("#reason").val();
		ajaxPost(
			null,
		    url,
		    param,
		    function(data){ 
		        if(data.result){
						toastr.success(data.msg);
						
						if (data.status == "deleted"){
                            $(".btn-close-preview").trigger("click");
                            urlCtrl.loadByHash("#search"); //envoie l'utilisateur la barre de recherche
                        }					
						else 
							urlCtrl.loadByHash("#page.type."+contextData.type+".id."+contextData.id); //Une autre page
						}
					else{
		    		toastr.error(data.msg); 
		    	}
		    }
		);
	    });
        $(".btn-delete-element-preview").off().on("click", function(){
             $("#modal-delete-element-preview").modal("show");
        });      
	});

    function afficheMap(){
        mylog.log("afficheMap");

        //Si contextData.geo est undefined caché la carte
        if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
        };

        var paramsMapContent = {
            container : "divMapContent",
            latLon : contextData.geo,
            activeCluster : false,
            zoom : 16,
            activePopUp : false
        };


        mapAbout = mapObj.init(paramsMapContent);

        var paramsPointeur = {
            elt : {
                id : contextData.id,
                collection : contextData.type,
                geo : contextData.geo
            },
            center : true
        };
        mapAbout.addMarker(paramsPointeur);
        mapAbout.hideLoader();


    };
 
	  function inintDescs() {
        mylog.log("inintDescs");
        if($("#descriptionAbout").length > 0){
            descHtmlToMarkdown();
            var descHtml = "<i>"+trad.notSpecified+"</i>";
            if($("#descriptionAbout").html().length > 0){
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
            }
            
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
            AddReadMoreOther();
        }
    }
    function AddReadMoreOther() {
        mylog.log("readmore");
        var showChar = 300;
        var ellipsestext = "...";
        var moretext = "Lire la suite";
        var lesstext = "Lire moins";
        $('.more').each(function() {
            var content = $(this).html();
            
            var textcontent = $(this).text();
            mylog.log("text description",textcontent);

            if (textcontent.length > showChar) {

                var c = textcontent.substr(0, showChar);
                //var h = content.substr(showChar-1, content.length - showChar);

                var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span style="display:none;" class="morecontent">' + content + '</span>';

                $(this).html(html);
                $(this).append('<a href="" class="morelink less">' + moretext + '</a>');
            }

        });

        $(".morelink").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(lesstext);
                $(this).siblings('.morecontent').fadeToggle(100, function() {
                    $(this).prev().fadeToggle(100);
                });

            } else {
                $(this).addClass("less");
                $(this).html(moretext);
                $(this).parent().children('.container').fadeToggle(100, function() {
                    $(this).next().fadeToggle(100);
                });
            }
            //$(this).prev().children().fadeToggle();
            //$(this).parent().prev().prev().fadeToggle(500);
            //$(this).parent().prev().delay(600).fadeToggle(500);

            return false;
        });
    }
</script> 