<?php
    $cssAnsScriptFilesModule = [
        '/plugins/handsometable/js/handsontable.full.js',
        '/plugins/handsometable/css/handsontable.full.min.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getBaseUrl(true));
?>
<style>
    .breadcrumb {
        user-select: none;
        background-color:#ffe9c0!important;
    }

    .breadcrumb>li+li:before{
        color:orange;
    }

    .breadcrumb .active a:hover {
        text-decoration: none;
    }

    .breadcrumb>li+li:before:{
        color: orange !important;
    }

    #gestion-session{
       margin-bottom: 30px;
    }

    #session-modal {
        z-index: 999999;
    }

    #session-modal .modal-dialog {
         width: calc(100% - 20px) !important;
        /* height: calc(100% - 20px) !important;  */
    }


    #session-modal .modal-content, #session-modal .modal-body{
        /* height: inherit; */
    }

    #session-modal .modal-header {
        background-color: white !important;
    }



    h3.popover-title {
        padding: 8px 14px;
        margin: 0;
        font-size: 18px !important;
        text-transform: unset;
        color: inherit !important;
        background-color: #f7f7f7 !important;
        border-bottom: 1px solid #ebebeb !important;
        border-radius: 5px 5px 0 0;
        text-align: left !important;
    }

    .popover-content span {
        display: inline-block;
        vertical-align: top;
    }

    .popover-content .text {
        font-size: 14px !important;
    }

    .popover-content > .fa {
        color: #b9b9b9;
        font-size: 20px;
        margin-right: 5px;
    }

    .htDropdownMenu:not(.htGhostTable) {
        z-index: 9999999;
    }

    .htFiltersConditionsMenu:not(.htGhostTable), .htDatepickerHolder {
        z-index: 10000000 !important;
    }

    .popover-edit {
        float: right;
        padding: 5px;
        line-height: 1.2px;
        border: none;
        outline: none;
        background: transparent;
        transition: .2s;
        border-radius: 5px;
    }

    .popover-edit:hover {
        background: #c9c9c9;
    }

    #session-modal {
        background-color: #2f2f2f5e;
        overflow:scroll;
    }

    #selected-row-person {
        display: none;
    }

    .handsontable th{
        height:50px !important;
        border:none !important;
        background-color : #ffe9c0 !important;
        color: #2C3E50;
        font-family: 'customFontfontrfflabsLexend-Light.ttf';
    }

    .handsontable .htFiltersMenuCondition{
        display:none;
    }


</style>

<?php
// var_dump($sessionsList);exit;
$visioDatesStr="";
// var_dump($session["visioDates"]);exit;
if(!empty($session["visioDates"])){
	foreach($session["visioDates"] as $ind=>$visioDate){
        $visioDatesStr= (!empty($visioDatesStr)) ? $visioDatesStr.", ".$visioDate : $visioDate ;	
    } 
}    
$gatheringDatesStr="";
if(!empty($session["gatheringDates"])){
    foreach($session["gatheringDates"] as $ind=>$gathDate){
        $gatheringDatesStr= (!empty($gatheringDatesStr)) ? $gatheringDatesStr.", ".$gathDate : $gathDate ;
    }
   
}

//  var_dump($visioDatesStr);exit;
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li><a href="#page.type.projects.id.<?= (string)$parent['id'] ?>" class="lbh-preview-element"><?= $parent['name'] ?></a></li>
        <li class="active" style="font-weight:700;"><a href="#page.type.events.id.<?= $session['id'] ?>" class="lbh-preview-element"><?= $session['name'] ?></a></li>
    </ol>
</nav>
<div>
    <div id="info-tableau-session" class="col-xs-12 margin-bottom-10" style="border: solid 1px grey;">
       <div style="position: absolute;right: 10px;top: 5px; font-size: 22px;cursor: pointer;"><a onclick="document.getElementById('info-tableau-session').style.display = 'none';"><i class="fa fa-times"></i></a></div>
        <p style="font-style: italic;"><i class="fa fa-info-circle"></i> Pour entrer des informations dans le tableau, double-cliquer sur la cellule correspondantes</p>
        <p>
            <h5>Petit guide de la bonne utilisation de ce fichier</h5>
            <span>Mettre à jour :</span>
            <ol>
            <li style="font-size: 15px;">Les dates & type de contact, la motivation</li>
            <li style="font-size: 15px;">Le résumé  du profil</li>
            <li style="font-size: 15px;">Suivi de visio</li>
            </ol>
            <span>Ecrire les infos qui ne correspondent à aucune des cases dans "Autres Commentaires" - dernière colonne</span>
        </p>
    </div> 
</div>
<div class="margin-bottom-20" style="display:inline-block;width: 100%; text-align:right">
    <div class="col-xs-12 col-sm-8 text-left">
    <?php 
        if(empty($gatheringDatesStr)){
            $gatheringDatesStr="Aucune date effective n'a été renseignée pour cette session.<br/>";
        }
        if(empty($visioDatesStr)){
            $visioDatesStr="Aucune visio d'informations n'a été planifiée pour le moment.<br/>";
        }

    ?>
        <div class='margin-5'>
            <span style="font-style:italic;font-weight:600;">Les dates de rassemblement : </span><span><?= $gatheringDatesStr?> </span>
            <a href="javascript:;" class="add-gathering-date btn btn-default">Ajouter une date de rassemblement</a>
        </div>
        <div class='margin-5'>      
            <span style="font-style:italic;font-weight:600;">Les dates de visio-conférence : </span><span><?= $visioDatesStr?> </span>
            <a href="javascript:;" class="add-visio-date btn btn-default">Ajouter un créneau</a>
        </div>
        <?php if(!empty($session["interestedLink"])){ 
            $interestedLink=Yii::app()->getRequest()->getBaseUrl(true).$session["interestedLink"];
        ?>
       <div class='margin-5'>      
            <span style="font-style:italic;font-weight:600;">Lien de partage pour les prospects : </span>
            <a href="javascript:;"  data-clipboard-text="<?= $interestedLink ?>" data-clipboard-action="copy" class="copy-interestedLink btn btn-default"><i class="fa fa-link"></i> <?= $interestedLink ?></a>
        </div> 
        <?php } ?>       
    </div>        
   
    <div class="col-xs-12 col-sm-4 text-right">
       <a href="javascript:;" onclick="smallMenu.openAjaxHTML(baseUrl+'/costum/lacompagniedestierslieux/inviteinsession/type/events/id/<?= $session['id']?>')" id="inviteInSession" class="bg-pink btn btn-default lbhp text-white">Ajouter quelqu'un à la session</a>
    </div>
</div>

<div id="user-info-container"></div>
<input type="text" id="selected-row-person">
<div id="gestion-session"></div>

<script>    
    function loadSession(handsontableInstance, data, sessionId, mode) {
        var columns = [
            {data : 'name', title : 'Nom', readOnly : true},
            {
                data : 'motivation',
                title : 'Motivation',
                editor : 'select',
                selectOptions : [
                    'Hyper motivé(e)',
                    'Très motivé(e)',
                    'Motivé(e)',
                    'Pas interessé(e)',
                    'Pour la prochaine promo'
                ]
            },
            {
                data : 'state',
                title : 'État',
                editor : 'select',
                selectOptions : [
                    "Intéressé.e", "Inscrit.e" , "abandon"
                ]
            },
            {
                data : 'actionToDo',
                title : 'Action à réaliser',
                editor : 'select',
                selectOptions : [
                    "À appeler",
                    "Email visio info",
                    "Rechercher coordo",
                    "Ne rien faire",
                    "En attente d’un retour",
                    "À orienter sur une autre forma/compagnonage",
                    "À recontacter pour la prochaine session",
                    "Mail perso",
                    "Autres régions"
                ]
            },
            {data : 'dateLastContact', title : 'Date du dernier contact', type : 'date', dateFormat : 'DD/MM/YYYY'},
            {
                data : 'contactTypology',
                title : 'Type de contact',
                editor : 'select',
                selectOptions : ["Email", "Message Vocal", "Echange Tél", "Visio", "En personne"]
            },
            {data : 'lastExchangeSummary', title : 'Résumé du dernier échange'},
            {data : 'informationOrigin', title : 'A su l\'info par'},
            {
                data : 'visioFollowed', title : 'A suivi une info visio', editor : 'select',
                selectOptions : ['Oui', 'Non']
            },
            {data : 'visioDate', title : 'Date de visio',editor : 'select',
                selectOptions : session.visioDates},
            {data : 'funding', title : 'Financement'},
            {data : 'otherComments', title : 'Autres commentaires'},
        ];
        var hotOption = {
            columns : columns,
            data : data,
            readOnly: mode !== 'w',
            fixedColumnsStart : 3,
            rowHeaders : false,
            colHeaders : true,
            manualColumnResize : true,
            manualRowResize : true,
            fillHandle : false,
            autoRowSize : true,
            filters : true,
            dropdownMenu : ['filter_by_condition', 'filter_by_value', 'filter_action_bar'],
            multiColumnSorting : {
                headerAction : true,
                sortEmptyCells : false,
                indicator : true,
                initialConfig : [
                    {
                        column : 0,
                        sortOrder : 'asc',
                    },
                    {
                        column : 1,
                        sortOrder : 'asc',
                    },
                ]
            },
            height : 'auto',
            licenseKey : 'non-commercial-and-evaluation',
            cells(row, col) {
                var property = {};
                if (this.prop === 'visioFollowed') {
                    var data = this.instance.getDataAtCell(this.instance.toPhysicalRow(row), col);
                    switch (data) {
                        case 'Oui':
                            property.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                                Handsontable.renderers.TextRenderer.apply(this, arguments);
                                td.style.backgroundColor = '#c9f7bb';
                                td.style.fontWeight = '600';
                                td.style.color = '#83bc8b';
                            }
                            break;
                        case'Non':
                            property.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                                Handsontable.renderers.TextRenderer.apply(this, arguments);
                                td.style.backgroundColor = '#c00000';
                                td.style.fontWeight = '600';
                                td.style.color = 'white';
                            }
                            break;
                    }
                }
                return property;
            },
            afterChange(change, source) {
                if (source === 'loadData') {
                    return true;
                }
                var update = change[0];
                if (update[2] === update[3]) {
                    return true;
                }
                var sessionDataLine = hot.getSourceDataAtRow(hot.toPhysicalRow(update[0]));
                
                var value = update[3];
                setType = null;
                if (value.match(/^\d{2}\/\d{2}\/\d{4}$/)) {
                    setType = 'isoDate'
                }
                var path2Value = {
                    id : sessionId,
                    value : value,
                    collection : 'events',
                    path : 'links.attendees.' + sessionDataLine.id + '.' + update[1],
                    setType : setType
                }
                dataHelper.path2Value(path2Value, function (response) {
                    if (response.result) {
                        toastr.success(trad["updated"]);
                    }
                });
            }
        };
        var hot = new handsontableInstance(document.getElementById('gestion-session'), hotOption);
        hot.addHook('afterSelection', function (rowStart, colStart, rowEnd, colEnd) {
            var popoverView = $('#user-info-container .popover');
            if (rowStart === rowEnd && colStart === colEnd && colStart === 0 && rowStart >= 0) {
                var dataAtRow = hot.getSourceDataAtRow(hot.toPhysicalRow(rowStart));
                $('#selected-row-person').val(rowStart);
                var person = attendees[dataAtRow.id];
                var name = '';
                ['firstName', 'lastName'].forEach(function (field) {
                    if (person[field]) {
                        name += person[field] + ' ';
                    }
                });
                if (!name) {
                    name = person.name;
                }
                var content = '';
                content += '<span class="fa fa-font"></span><span class="text">' + name + '</span><br>';
                if (person.email) {
                    content += '<span class="fa fa-at"></span><span class="text">' + person.email + '</span><br>';
                }
                if (typeof person.telephone!="undefined" && typeof person.telephone.mobile!="undefined" && person.telephone.mobile.length) {
                    content += '<span class="fa fa-mobile"></span><span class="text">' + person.telephone.mobile.join(', ') + '</span><br>';
                }
                if (typeof person.telephone!="undefined" && typeof person.telephone.fixe!="undefined" && person.telephone.fixe.length) {
                    content += '<span class="fa fa-phone"></span><span class="text">' + person.telephone.fixe.join(', ') + '</span><br>';
                }
                if (person.address) {
                    content += '<span class="fa fa-home"></span><span class="text">' + person.address.streetAddress + '<br>' + person.address.addressLocality + ', ' + person.address.level1Name + '</span><br>';
                }
                var tableDefinitionDom = $(hot.getCell(rowStart, colStart));
                tableDefinitionDom.popover({
                    animation : true,
                    placement : 'top',
                    html : true,
                    content : content,
                    title : name + (mode === 'w' ? '<button class="popover-edit" data-id="' + dataAtRow.id + '"><span class="fa fa-edit"></span></button>' : ''),
                    container : '#user-info-container'
                });
                if (popoverView.length > 0) {
                    popoverView.data('bs.popover').$element.off('hidden.bs.popover').on('hidden.bs.popover', function () {
                        tableDefinitionDom.popover('show');
                        $(this).off('hidden.bs.popover');
                    });
                } else {
                    tableDefinitionDom.popover('show');
                }
            }
            if (popoverView.length) {
                popoverView.data('bs.popover').$element.popover('hide').popover('destroy');
            }
        });
        $("#gestion-session").before('<div style="width:100%;"><hr/><a style="font-size:18px;" class="btn btn-default reloadSession margin-right-5"><i class="fa fa-rotate-right"></i></a><a href="javascript:;" class="btn btn-default btnCsvExportSession text-dark"><i class="fa fa-download"></i> <label class="">CSV</label></a></div>');
        $('.btnCsvExportSession').css('display', 'inline-block').off('click').on('click', function (event) {
            event.preventDefault();
            var exportPlugin = hot.getPlugin('exportFile');
            exportPlugin.downloadFile('csv', {
                bom : true,
                columnDelimiter : ';',
                columnHeaders : true,
                exportHiddenColumns : false,
                exportHiddenRows : false,
                fileExtension : 'csv',
                filename : 'Contact ' + moment().format(' YYYY-MM-DD_HH-mm-ss'),
                mimeType : 'text/csv',
                rowDelimiter : '\r\n',
                rowHeaders : false
            });
        });
        $('.reloadSession').off().on('click',function(){
            // loadSession(Handsontable, data, session.id, mode);
            var modalDom = $('#session-modal');
            ajaxPost(null, baseUrl + '/costum/lacompagniedestierslieux/gestionsession/sessionId/' + session.id, null, function (html) {
                modalDom.off('shown.bs.modal').on('shown.bs.modal', function () {
                    modalDom.css('padding', '');
                });
                modalDom.find('.modal-body').empty().html(html);
                modalDom.modal('show');
            });
        });
        $(document.body).off('click', popoverCheck).on('click', popoverCheck);
        $('#user-info-container').on('click', '.popover-edit', function () {
            $('#user-info-container .popover').data('bs.popover').$element.popover('hide').popover('destroy');
            var userId = $(this).data('id');
            var user = $.extend({}, attendees[userId]);
            var defaultData = {
                name : user.name,
                firstName : user.firstName,
                lastName : user.lastName,
                mobile : (typeof user.telephone!="undefined" && typeof user.telephone.mobile!="undefined") ? user.telephone.mobile.join(', ') : "",
                telephone : (typeof user.telephone!="undefined" && typeof user.telephone.fixe!="undefined") ? user.telephone.fixe.join(', ') : "",
                email : user.email,
                address : user.address,
                geo : user.geo,
                geoPosition : user.geoPosition,
                collection : "citoyens",
                id:userId
            };
            var dynform = {
                dynForm : typeObj.person.dynForm
                
            };
            mylog.log("dynform minimum",dynform);
            dynform.dynForm.jsonSchema.properties=costum.minimumContactForm;
            // dynform.dynForm.jsonSchema.properties.collection="citoyens";
            dynform.dynForm.jsonSchema.onLoads.onload =function() {
                    $("#ajax-modal-modal-title").html("<i class='fa fa-user'></i> Mettre à jour  un contact");
                    $("#ajaxFormModal .similarLinkcustom").hide();
                    $("#ajaxFormModal #name").prop("disabled",true);
                    $("#ajaxFormModal #email").prop("disabled",true);
                    $("#ajaxFormModal #collection").val("citoyens");

                    // $("#ajaxFormModal .otherEmailtext").hide();
                };
            dynform.dynForm.jsonSchema.afterSave =function() {
                $('.reloadSession').trigger("click");
                
            };    
            mylog.log("dynform minimum",dynform);
            dyFObj.openForm(dynform, "afterLoad", defaultData, null, null, {
                type : 'bootbox'
            });
        });

        bindSessionManagementEvents();


  
    }

    function popoverCheck(event) {
        var popoversDom = $('#user-info-container').children();
        if (popoversDom.length && $(event.target).parents('#gestion-session').length === 0 && $(event.target).parents('#user-info-container').length === 0) {
            popoversDom.eq(0).data('bs.popover').$element.popover('hide');
        }
    }

    var data = JSON.parse(JSON.stringify(<?= json_encode($sessionsList ?? []) ?>));
    var attendees = JSON.parse(JSON.stringify(<?= json_encode($attendees ?? []) ?>));
    var session = JSON.parse(JSON.stringify(<?= json_encode($session ?? []) ?>));
    var mode = '<?= $mode ?>';

    $(function () {
        function bindSessionManagementEvents(){
            $('.add-gathering-date').on('click', function () {

            
var sessionData=$.extend({},session);
sessionData.collection="events";
mylog.log("sessionData",sessionData);
// var dynform = {
//     dynForm : typeObj.sessionFormation.dynFormCostum
    
// };
 var dynform = {
    // dynForm : {
        jsonSchema : {
            title : "dates rassemblement",
            properties : {
                gatheringDates :{
                    "inputType": "array",
                    "label": "Dates effectives (rassemblements) de la session de formation",
                    "info": "Cliquer sur l'icône calendrier pour sélectionner une date",
                    "initOptions": {
                        "type": "date",
                        "placeholder": "Exemple : 16/11/2023",
                        "labelAdd": "Ajouter une date de rassemblement",
                        "class": "dateInput"
                    }
                }
            },
            onLoads : {
                // ------ rules on gathering dates ranger before and after parent event
                onload:function(){
                    $("#ajaxFormModal #collection").val("events");
                    bindAddpropGatheringDate=function(){
                    $('.addPropBtn[data-id=gatheringDates]').off().on("click",function(){
                        // alert("ok");
                       mylog.log("addPropBtn", $(this).data('id'));
                        var field = $(this).data('id');
                        var typeExtract = $(this).data('type');
                        var inputType=$(this).data('input-type');
                        if( $('.'+field+' .inputs .addmultifield:visible').length==0 || ( $("."+field+" .addmultifield:last").val() != "" && $( "."+field+" .addmultifield1:last" ).val() != "") ){
                            dyFObj.init.addfield('.'+$(this).data('container'),'',field, typeExtract,inputType);
                            $(".gatheringDatesarray .addmultifield:last").addClass("dateInput");
                            mylog.log("init dateInput");
                            jQuery.datetimepicker.setLocale('fr');
                            $(".dateInput").datetimepicker({
                                autoclose: true,
                                 lang: "fr",
                                 format: "d/m/Y",
                               timepicker:false
                             });
                        }else{
                              toastr.info("please fill properties first");
                         }    
                    });
                    }
                   bindAddpropGatheringDate()
                    // if($.validator.methods.greaterThan("16/01/2024 09:49",null,["#ajaxFormModal #startDate"])===false){
                    //     var key="endDatedatetime"
                    //     var msg=jQuery.validator.messages.greaterThan;
                    //      $("."+key+" .error").empty();
                    //     $("."+key+" .error").append(msg);
                    //         $("."+key+" .error").append("<br/>");
                    //         $("."+key+" .error").show();

                    //     dyFObj.showError("endDatedatetime", );
                    // }
                }
            },
            afterSave : function(){
                $('.reloadSession').trigger("click");
            }
        }    
    // }
    
};
  
mylog.log("dynform minimum",dynform);
dyFObj.openForm(dynform, "afterLoad", sessionData, null, null, {
    type : 'bootbox'
});
});

$('.add-visio-date').on('click', function () {


var sessionData=$.extend({},session);
sessionData.collection="events";




mylog.log("sessionData",sessionData);
// var dynform = {
//     dynForm : typeObj.sessionFormation.dynFormCostum

// };
var dynform = {
// dynForm : {
jsonSchema : {
title : "Dates visio",
properties : {
    "visioDate" : {
                    "label" : "Dates des visioconférences d'information",
                    "info" : "Les personnes intéressées par cette session pourront se pré-inscrire à l'une des ces réunions d'information.",
                    "inputType" : "codate",
                    "timeRange" : [ 
                        ""
                    ],
                    "dateRange" : [ 
                        ""
                    ],
                    "eventDuration" : 1
                },
},
onLoads : {
    // ------ rules on gathering dates ranger before and after parent event
    onload:function(){
        $("#ajaxFormModal #collection").val("events");
        sessionData.visioDate={
            "alldates" : {},
            "eventduration" : "1"
        };
        $.each(session.visioDates,function(ind,date){
            var dayDate=moment(date,'DD/MM/YYYY HH:mm').format('DD/MM/YYYY');
            var hourDate=moment(date,'DD/MM/YYYY HH:mm').format('HH:mm');
            if(typeof sessionData.visioDate.alldates[dayDate]=="undefined"){
                sessionData.visioDate.alldates[dayDate]=[hourDate];
            }else{
                sessionData.visioDate.alldates[dayDate].push(hourDate);
            }

        });
        
        var fieldHTML="";

        $.each(sessionData.visioDate.alldates,function(date, timeRange){
            const idContainerUnik = Date.now().toString(36) + Math.random().toString(36).substring(2);
            const idInputPrefUnik = Date.now().toString(36) + Math.random().toString(36).substring(2);
            let timeInputs = '';
            let timeInputsId = '';
            const newInputDate = date;
            // if(typeof timeRange != "undefined" && fieldObj.timeRange[elem])
            // 	timeRange = fieldObj.timeRange[elem];
            const endBoucle = (typeof timeRange!="undefined" && typeof timeRange.length!="undefined") ? timeRange.length : 3
            for(let i = 0; i < endBoucle; i++) {
                let lastCreneauVal = timeRange[i] ? timeRange[i] : '';
                const inputPref = Date.now().toString(36) + Math.random().toString(36).substring(2);
                timeInputs += `<input type="text" class="input-pref form-control myTimeInput" id="inputPref_${inputPref}" placeholder="créneau ${i+1}" value="${lastCreneauVal}" data-inputparent="date_${idContainerUnik}" data-parent="inputsPref${idInputPrefUnik}"/>`
                timeInputsId += 'inputPref_'+inputPref;
                if(i < endBoucle -1) {
                    timeInputsId += ',';
                }
            }
    fieldHTML += `
        <div id="dateContainer${idContainerUnik}" class="col-xs-12 mb-2 p-xs-0">
            <div class="col-xs-12 p-xs-0" id="blockdate${idContainerUnik}">
                <div id="dateinput${idContainerUnik}" class="col-xs-10 pl-xs-0">
                    <label for="date_${idContainerUnik}">Date</label>
                    <input type="text" id="date_${idContainerUnik}" data-id="date_${idContainerUnik}" value="${newInputDate}" placeholder="${tradForm.selectDate}" class="my-form-input date-from-value myDateTimeInput dateInput"/>
                    <span class="border"></span>
                </div>
                <div id="blockoption${idContainerUnik}" class="col-xs-2 pr-xs-0" style="position: absolute; left: 10%">
                    <a class="btn btn-xs btn-danger removeDateBlock" data-id="dateContainer${idContainerUnik}" href="javascript:;">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 p-xs-0" id="blockPref${idContainerUnik}">
                <div class="col-xs-10 pl-xs-0 timeInputContainer" id="inputsPrefBlock${idInputPrefUnik}">
                    ${timeInputs}
                </div>
                <div class="col-xs-2 pr-xs-0" style="padding-top: 8px">
                    <a class="btn btn-xs btn-danger removeTimeElmt" id="inputsPref${idInputPrefUnik}Remove" data-idparent="inputsPrefBlock${idInputPrefUnik}" data-keyparent="date_${idContainerUnik}" data-key="${idInputPrefUnik}" data-id="${timeInputsId}" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a class="btn btn-xs btn-success addTimeElmt" id="inputsPref${idInputPrefUnik}Add" data-idparent="inputsPrefBlock${idInputPrefUnik}" data-keyparent="date_${idContainerUnik}" data-key="${idInputPrefUnik}" data-id="${timeInputsId}" href="javascript:;">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    `;
});

        $(".addDateBlock").parent().prev().remove();
        $(".addDateBlock").parent().before(fieldHTML);
        $("#ajaxFormModal .visioDatecodate label[for=eventDurationcodate-visioDate]").text("Durée (en heure) des visios d'information relatives à cette session");
        $("#ajaxFormModal .removeDateBlock").parent().css({"position": "absolute","left":"-10px","top": "2px"});
    }
},
afterBuild : function(){
    
},
afterSave : function(){
    $('.reloadSession').trigger("click");
}
}    
// }

};

mylog.log("dynform minimum",dynform);
dyFObj.openForm(dynform, "afterLoad", sessionData, null, null, {
type : 'bootbox'
});
});

lazyLoad(baseUrl + '/plugins/clipboard/clipboard.min.js', '',
    function () {
        $('.copy-interestedLink').on().click(function () {
            var clipboard = new ClipboardJS('.copy-interestedLink');
            clipboard.on('success', function (e) {
                // mylog.info('Action:', e.action);
                // mylog.info('Text:', e.text);
                // mylog.info('Trigger:', e.trigger);
                e.clearSelection();
                toastr.success(trad.copy);
            });

            clipboard.on('error', function (e) {
                // mylog.error('Action:', e.action);
                // mylog.error('Trigger:', e.trigger);
            });
        })
    });
        }


        var intervalLoader = setInterval(function () {
            if (typeof Handsontable === 'function') {
                clearInterval(intervalLoader);
                mylog.log("data gestion session", data, notEmpty(data));
                if(notEmpty(data)){
                    loadSession(Handsontable, data, session.id, mode);
                }else{
                    var msgEmpty="<p>Pour le moment, aucun contact n'est associé à cette session. Ajoutez-en un en cliquant sur le bouton \"Ajouter quelqu'un à la session\" ci-dessus.</p>";
                    $("#gestion-session").append(msgEmpty);
                }
                
            }
        });
        $('.preview-modal-close').on('click', function () {
            $(this).parents('.modal').find('[data-dismiss="modal"]').trigger('click');
        });
        coInterface.bindLBHLinks();
    });
</script>
