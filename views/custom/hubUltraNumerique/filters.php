<?php 
    $cssAnsScriptFilesModule = array(
    	'/css/filters.css',
	);
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
?> 
<style type="text/css">

	#filters-nav #input-sec-search .input-global-search{
		border-radius: 0px 20px 20px 0px !important;
	}

	.footerSearchContainer {
		display: none;
	}

	.smartgrid-slide-element .text-wrap div.counter {
		position: initial;
	}

	.smartgrid-slide-element .counter .list-dash {
		width : 100%;
	}

</style>

<script type="text/javascript">
var pageApp=<?php echo json_encode(@$page); ?>;
var appConfig=<?php echo json_encode(@$appConfig); ?>;
var eventList=<?php echo json_encode(Event::$types); ?>;

var paramsFilter = {
	container : "#filters-nav",
 	header : {
 		options : {
			left : {
				classes : 'col-xs-8 elipsis no-padding',
				group:{
					count : true,
					types : true
				}
			},
			right : {
				classes : 'col-xs-4 text-right no-padding',
				group : {
					map : true
				}
			}
		}
 	}
};

if (pageApp == "annuaire") {
 	paramsFilter.inteface = {
		events : {
			scroll : true,
			scrollOne : true
		}
	};

	paramsFilter.results = {
		renderView: "directory.elementPanelHtml",
		smartGrid : true
	};

 	paramsFilter.defaults = {
	 	types : ["organizations"],
	 	community : {
	 		"roles" : "mediationNum"
	 	},
	 	indexStep : 0
	};
}

if (pageApp == "projects") {
 	paramsFilter.inteface = {
		events : {
			scroll : true,
			scrollOne : true
		}
	};

	paramsFilter.results = {
		renderView: "directory.elementPanelHtml"
	};

 	paramsFilter.defaults = {
	 	types : ["projects"],
	 	sourceKey : [
	 		"laReunionPourUnNumeriqueInclusif",
			"mayottePourUnNumeriqueInclusif",
			"wallisetfutunaPourUnNumeriqueInclusif",
			"polynesieFrancaisePourUnNumeriqueInclusif",
			"nouvellecaledoniePourUnNumeriqueInclusif"
	 	],
	 	indexStep : 0
	};
}

if (pageApp == "agenda") {
 	paramsFilter.urlData = baseUrl+"/co2/search/agenda";
	paramsFilter.calendar = true;
	paramsFilter.loadEvent = {
 		default : "agenda"
 	};

 	paramsFilter.defaults = {
 		types : ["events"],
 		sourceKey : [
	 		"laReunionPourUnNumeriqueInclusif",
			"mayottePourUnNumeriqueInclusif",
			"wallisetfutunaPourUnNumeriqueInclusif",
			"polynesieFrancaisePourUnNumeriqueInclusif",
			"nouvellecaledoniePourUnNumeriqueInclusif"
 		]
	};

	paramsFilter.results = {
		renderView: "directory.eventPanelHtml"
	};

	paramsFilter.filters = {
 		text : true,
 		scope : true,
 		type : {
 			view : "dropdownList",
 			event : "selectList",
 			type : "type",
 			list : eventList
 		}
 	};
}

jQuery(document).ready(function() {
	filterSearch = searchObj.init(paramsFilter);
});
</script>