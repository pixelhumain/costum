<footer style="background-color:#fafafa" class="text-center col-xs-12  col-lg-12 col-sm-12 no-padding">
    <div class="row" style="margin-top: 3%;">
        <div class="col-xs-12 col-sm-12 col-lg-12 text-center col-footer col-footer-step">
            <a class="col-lg-4 col-sm-4 col-xs-12" style="color:#172881" href="http://solidarnum.org" class="lbh-menu-app">
                <center>
                	<img style="width:7vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/Logo-SOLIDARNUM.jpg">
                	</br>
                </center>
            </a>
            <a class="col-lg-4 col-sm-4 col-xs-12" style="color: #172881" href="https://societenumerique.gouv.fr/" class="lbh-menu-app">
                <center>
                	<img style="width:23vw;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubUltraNumerique/logosonum.png">
                	</br>
                </center>
            </a>
            <a class="col-lg-4 col-sm-4 col-xs-12" style="color: #172881" href="https://www.banquedesterritoires.fr/" class="lbh-menu-app">
                <center>
                    <img style="width: 21vw;margin-top: 13px;" class="img-responsive" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubUltraNumerique/banque-territoire.jpg">
                    </br>
                </center>
            </a>
        </div>
    </div>
</footer>
