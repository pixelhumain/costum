<?php
echo $this->renderPartial('@themes/CO2/views/layouts/modalRegister');
?>

<script>

var paramsArr=location.hash.split("?")[1].split("&");
paramsArr;
var user="";
var action="";
var val=[];
paramsArr.forEach(function(v,i){
    val=v.split("=");
    window[val[0]]=val[1];  
});
// mylog.log(user,action)
console.log("params costum register",user,action);

if(action=="passwordRetreive"){
setTimeout(
	function () {
        var formDyfAfterLogin = {
				saveUrl : baseUrl+"/"+moduleId+"/person/changepassword",
				dynForm : {
					jsonSchema : {
						title : trad["Change password"],
						icon : "fa-key",
						onLoads : {
					    	//pour creer un subevnt depuis un event existant
					    	onload : function(){
					    		//dyFInputs.setHeader("bg-green");
					    		$("#ajax-modal .modal-header").addClass("bg-dark");
				                $("#ajax-modal .infocustom p").addClass("text-dark");
				    	   	}
				    	},
						afterSave : function(data){
							location.reload();
						},
						afterBuild: function(){
							$(".icon-eye").on('click', function(){
								$(this).toggleClass("fa-eye fa-eye-slash");
								var input = $("#"+$(this).data("field"));
								if (input.attr("type") == "password") {
									input.attr("type", "text");
								} else {
									input.attr("type", "password");
								}
							})
						},
						properties : {
							mode : dyFInputs.inputHidden(),
							userId : dyFInputs.inputHidden(),
							oldPassword : dyFInputs.password(trad["Old password"]),
							newPassword : dyFInputs.password("", { required : true, minlength : 8 } ),
							newPassword2 : dyFInputs.password(trad["Repeat your new password"], {required : true, minlength : 8, equalTo : "#ajaxFormModal #newPassword"})	
						}
					}
				}
			};
			//alert("btn-update-password");
			var dataUpdate = {
				mode : "changePassword",
		        userId : user
		    };
    dyFObj.openFormAfterLogin = {
        type : formDyfAfterLogin,
        afterLoad : null,
        data : dataUpdate
    };  

	window.history.pushState("Password Retreive", 
		"Password Retreive", 
		location.href.replace(location.hash,"#"));
    
    Login.openLogin();
    
},5000);
}

</script>