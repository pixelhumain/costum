<?php if(Meir::isAdminPending(Yii::app()->session["userId"],$element) || Meir::isAdminInviting(Yii::app()->session["userId"],$element) || Authorisation::isInterfaceAdmin() || Authorisation::isElementAdmin((String)$element["_id"],$element["collection"],Yii::app()->session["userId"])){?>
    <style>
        #social-header {
            display : none;
        }
        .container-filters-menu{
            background-color : transparent;
        }
        .membersActor h3{
            color: #093081;
            text-transform : none;
        }
        .searchBar-filters .search-bar{
            border-color : #093081 !important;
        }
        .searchBar-filters .input-group-addon{
            border : none !important;
        }
        .divEndOfresults{
            display : none;
        }
        .portfolio-modal .close-modal .lr, 
        .portfolio-modal .close-modal .lr .rl{
            background-color : #2C3E50;
        }
        @media screen and (max-width: 767px){
            .membersActor h3{
                font-size : 18px;
            }   
            .central-section{
                padding-top : 0;
                margin-top : 0;
            }
            .initialisSearch{
                font-size : 12px;
            }
            .filterContainerInside, .filterContainer{
                display : none;
            }
        
        }
    </style> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 membersActor">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-9 no-padding">
            <h3>Membres de l'organisation <?= $element["name"]?></h3>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3">
            <div class="col-xs-12 text-right padding-15">
                <?php if(Meir::isAdminFiche(Yii::app()->session["userId"],$element)){?>
                    <a href="#element.invite.type.organizations.id.<?= (String)$element["_id"]?>" class="btn btn-default  text-center link-banner lbhp "><i class="fa fa-user-plus"></i> Inviter quelqu'un pour être membre</a>  
                <?php } ?>
                <a class="btn btn-default initialisSearch"><i class="fa fa-arrow-left"></i> Retour</a>  
            </div>
        </div>
    </div>
    <hr class="col-xs-12" style="margin-top: 4px;margin-bottom: 0px;border: 0;border-top: 2px solid #093081;">
    <div id='communitySearchContent' class='col-xs-12 no-padding'>
        <div id='filterContainer' class='searchObjCSS'></div>
        <div class='headerSearchIncommunity no-padding col-xs-12'></div>
        <div class='bodySearchContainer margin-top-10'></div>
        <div class='no-padding col-xs-12' id='dropdown_search'> </div>
    </div>
    <script>
        var element = <?php echo json_encode($element);?>;
        var isAdminFiche = <?php echo json_encode(Meir::isAdminFiche(Yii::app()->session["userId"],$element));?>;
        $(".initialisSearch").on("click", function(){
            urlCtrl.loadByHash("#@"+element.slug);
        })
        var paramsFilter= {
            container : "#filterContainer",
            header : {
                dom : ".headerSearchIncommunity",
                options :{
                    left :{classes : 'col-xs-8 elipsis no-padding',
                        group :{
                            
                        }
                    } 
                }
            },
            defaults : {
                notSourceKey : true,
                types : ["citoyens"],
                forced:{
                    filters:{}
                }
            }, 
            results : {
                smartGrid : true,
                renderView : "directory.elementPanelHtml",
                community : {
                    links : (typeof contextData.links != "undefined") ? contextData.links : null,
                    connectType : "members"
                }
            },
            // filters : {
            //     text : true
            // }
        };
        if(isAdminFiche)
            paramsFilter.results.community.edit = true
        paramsFilter.defaults.forced.filters["links.memberOf."+contextData.id] = {'$exists' :true};
        filterGroup = searchObj.init(paramsFilter);
        filterGroup.search.init(filterGroup);
    </script>
<?php }else{?>
	<script>
		$("#social-header").css("display","none");
		$("#menu-top-profil-social").css("display","none");
		$(".social-main-container").css("min-height","0");
		$(".acteurMeir .divNotConnected").css("height","900px !important"); 
		$(".acteurMeir").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
	</script>
<?php }?>