
<?php 
// if(Meir::isAdminPending(Yii::app()->session["userId"],$element) || 
// Meir::isAdminInviting(Yii::app()->session["userId"],$element) || 
// Authorisation::isInterfaceAdmin() || 
// Authorisation::isElementAdmin((String)$element["_id"],$element["collection"],Yii::app()->session["userId"])||
// Yii::app()->session["userId"]
// ){?>
	<style type="text/css">
		.ficheMetier body {
			background-color: #f5f7fb;
		}
		.ficheMetier .title-subtitle {
			text-align: center;
		}
		.ficheMetier div[class^="illustration-border"] {
			position: absolute;
			background-color: #c5d117;
		}
		.ficheMetier .card-title {
			margin-top: 5px;
			margin-bottom: 5px;
			font-size : 20px;
			font-weight: 700;
			text-decoration: underline;
		}
		.ficheMetier .card-subtitle {
			margin-top: 5px;
			margin-bottom: 10px;
			font-size: 25px;
			text-transform : uppercase;
			font-weight: 700;
		}
		.ficheMetier .illustration-border-left-1 {
			width: 4px;
			height: 60px;
			top: 0;
			left: 0;
		}
		.ficheMetier .illustration-border-left-2 {
			width: 10%;
			height: 4px;
			top: 0;
			left: 0;
		}
		.ficheMetier .illustration-border-right-1 {
			width: 4px;
			height: 60px;
			bottom: 0;
			right: 0;
		}
		.ficheMetier .illustration-border-right-2 {
			width: 10%;
			height: 4px;
			bottom: 0;
			right: 0;
		}
		.ficheMetier .fiche-img {
			width: 100%;    
			height: 300px;
			object-fit: contain;
			object-position: center;
		}
		.ficheMetier .well-fiche.text-blue-fiche p {
			font-size: 20px !important;
		}
		.ficheMetier .fiche-parag {
			color: #093081;
			font-size: 25px;
			margin: 15px 0px;
			text-align: center;
		}
		.ficheMetier .bg-blue-fiche {
			background-color: #90b2f8;
		}
		.ficheMetier .bg-white {
			background-color: #ffffff;
		}
		.ficheMetier .text-blue-fiche {
			color: #093081;
		}
		.ficheMetier .well-fiche {
			margin: 15px 0px;
			font-size: 20px;
			border-radius: 15px;
			word-break: break-word;
		}
		.ficheMetier .label-fiche {
			border-radius: 0px; 
			padding: 5px 60px;
			position: relative;
			min-width: 45%;
			text-align: center;
			display: inline-block;
			margin-bottom: 25px;
			font-size: 20px;
			margin-top:5px;
		}
		.ficheMetier .well-fiche .read-more {
			display: block;
			width: 45px;
			height: 45px;
			line-height: 45px;
			border-radius: 50%;
			background: #fff;
			color: #90b2f8;
			border: 1px solid #e7e7e7;
			font-size: 18px;
			margin: 0 auto;
			position: absolute;
			top: -9px;
			left: 0;
			right: 0;
			transition: all 0.3s ease-out 0s;
			text-align: center;
			font-weight: 700;
			text-decoration: none;
		}
		.ficheMetier .fiche-logo .logo {
			height: 100px;
			margin: auto;
			margin-bottom: 30px;
		}
		.ficheMetier .fiche-logo {
			text-align: center;
		}
		.ficheMetier .fiche-logo .description {
			font-weight: 700;
			margin-top: 20px;
		}

		@media (max-width:767px)  {
			.ficheMetier .card-title {
				font-size: 16px;
			}
			.ficheMetier .card-subtitle {
				font-size: 20px;
			}
			.ficheMetier .fiche-parag {
				font-size: 20px;
			}
			.ficheMetier .well-fiche {
				font-size: 15px;
			}
			.ficheMetier .label-fiche {
				padding: 5px 10px;
				margin-bottom: 20px;
				font-size: 14px;
			}
			.ficheMetier .well-fiche .read-more {
				top: -23px;
			}
			.ficheMetier .fiche-themetique {
				margin-top: 30px;
			}
			.ficheMetier .fiche-logo .logo {
				height: 70px;
			}
		}
		.ficheMetier .btnClass .btnstyle {
			margin-bottom : 2px;
			border: 2px solid #90b2f8;
			font-size : 16px;
		}
		.ficheMetier .btnClass .green {
			margin-bottom : 2px;
			border: 2px solid #34a853;
			font-size : 16px;
		}
		.ficheMetier .btn-delete {
			border: 2px solid red;
			margin-bottom : 2px;
			color: red;
			font-size : 16px;
		}
		.ficheMetier .btnClass{
			margin-top:10px;
		}
		.ficheMetier.tag-fiche-metier{
			font-size: 15px;
			
		}
		.link-fiche{
			text-decoration: underline;
			color: #093081;
		}

		.close-modal, .close-modal:hover, .btn-close a, a[data-dismiss="modal"], a[data-dismiss="modal"]:hover{
			background-color: #093081 !important;
			color:white !important;
			padding: 5px 10px;
			text-decoration: none; 
			font-size: 14px;
			border-radius: 5px;
		}

	</style>
	<div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 ficheMetier ">
		<div class="col-xs-12 no-padding  margin-bottom-15 text-right btn-close close-container">
			<a class="btn btn-default initialisSearch"><i class="fa fa-arrow-left"></i> Retour</a>
			<!--hr class="separator" style="margin-top: 4px;margin-bottom: 0px;border: 0;border-top: 1px solid #4285f4;"-->
		</div>
		<div class="row margin-top-50">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="title-subtitle col-xs-12">
					<div class="illustration-border-left-1"></div>
					<div class="illustration-border-left-2"></div>
					<div class="card-subtitle text-blue-fiche">
						<?php if (isset($element["jobFamily"])) {
							if(is_array($element["jobFamily"])){
								foreach($element["jobFamily"] as $kjobf => $vjobf){
									echo $vjobf."</br>";
								}
							}else {
								echo $element["jobFamily"];
							}
						}?>
					</div>
					<div class="card-title text-blue-fiche">
						<?php
							if((isset($element["category"]) && $element["category"] == "acteurMeir" ) || 
							isset($element["reference"]["costum"]) && in_array("meir",$element["reference"]["costum"])|| 
							!isset($element["category"])) 
								echo "Présentation de l'acteur : ".$element["name"]; 
							else if(isset($element["category"]) && $element["category"] == "startup")
								echo "Les fiches startup"
						?>
					</div>
					<div class="card-subtitle text-blue-fiche">
						<?php if(isset($element["thematic"]) && isset($element["category"]) && $element["category"] == "startup"){
							if(is_array($element["thematic"])){
								foreach($element["thematic"] as $kjobf => $vjobf){
									echo $vjobf."</br>";
								}
							}else {
								echo $element["thematic"];
							}
						}?>
					</div>
					
					<div class="illustration-border-right-1"></div>
					<div class="illustration-border-right-2"></div>

					
				</div>
				<div class="fiche-parag text-blue-fiche col-xs-12">
					<div class="list-tag">
						<?php if(isset($element["tags"])){
							$tagss = array_unique($element["tags"]);
							foreach($tagss as $kTags => $vTags){?>
								<a href = "#search?tags=<?=$vTags?>" class="lbh" >
									<span class="btn-tag  bg-blue-fiche text-blue-fiche badge tag-fiche-metier btn-tag tag">
										#<?=$vTags?>
									</span>
								</a>
							<?php }
						} ?>
					</div>
					<div class="col-xs-12 btnClass">
						<a href="javascript:;" onclick="window.open('<?php echo Yii::app()->createUrl("/costum").'/meir/fichepdf/id/'.(String)$element["_id"].'/type/organizations' ?>', '_blank').focus();" data-url="<?php echo Yii::app()->createUrl("/costum").'/meir/fichepdf/id/'.(String)$element["_id"].'/type/organizations' ?>" class="btn  pull-left btnstyle letter-blue margin-right-10" ><i class="fa fa-file-pdf-o"></i> Télécharger le pdf </a>
						<!-- btn accept acteur -->
						<?php if(Authorisation::isInterfaceAdmin() && isset($element["toBeValidated"])){?>
							<a href="javascript:;" data-id='<?= (string)$element["_id"]?>' data-type="organizations" class=" acceptActor btn bg-green-k text-white btn  pull-left green margin-right-10"><i class="fa fa-check-circle"></i>  Afficher dans la mapping </a>	
						<?php } ?>	

						<!-- btn édition -->
						<?php if((Meir::canEditFiche(Yii::app()->session["userId"],$element))){?>
								<a href="javascript:;" class="btn-edit-element pull-left btn margin-right-10 letter-blue btnstyle" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-pencil"></i> Modifier les informations</a>	 
						<?php }else{?>
							<?php if(Authorisation::isInterfaceAdmin() && (!isset($element["category"]) || (isset($element["category"]) && $element["category"] == "acteurMeir"))){?>
								<a href="javascript:;" class="btn-edit-element pull-left btn margin-right-10 letter-blue btnstyle" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-pencil"></i> Modifier les informations</a>
							<?php }?>
						<?php } ?>

						<!-- btn être membre -->
						<?php if(isset($element["links"]["members"][Yii::app()->session["userId"]]["isAdminPending"]) 
								&& isset($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])){?>
							<p><i class="fa fa-info"></i> Votre demande d'être admin est en attente de validation</p>
						<?php } else if(isset($element["links"]["members"][Yii::app()->session["userId"]]["isInviting"])){?>
								<a class="btn letter-blue btnstyle acceptAsBtn pull-left" data-type="citoyens" data-id="<?php echo Yii::app()->session["userId"]?>" data-connect-validation="isInviting" data-parent-hide="2"><i class="fa fa-link"></i> Accepter l'invitation d'être admin de cette fiche</a>
						<?php } else if(Authorisation::isInterfaceAdmin() && isset($element["category"]) /*&& $element["category"] == "acteurMeir" */&& !isset($element["links"]["members"][Yii::app()->session["userId"]])){ ?>
							<a href="javascript:links.connect('organizations','<?= (string)$element["_id"]?>','<?= Yii::app()->session["userId"]?>','citoyens','member',`<?= $element["name"] ?>`,true,null,'SOUHAITEZ-VOUS ÊTRE ADMINISTRATEUR DE LA FICHE','Merci de confirmer votre demande ')" class="pull-left btn margin-right-10 letter-blue btnstyle"> 
								<i class="fa fa-link"></i> Être membre de l'organisation
							</a>
						<?php }			 
						?>
						<!-- Btn voir autre membre-->
						<?php if(Yii::app()->session["userId"]){?>
							<a href="javascript:;" class="showMembers btnstyle letter-blue pull-left btn margin-right-10" data-isadmin="" data-isadminpending = "<?= Meir::isAdminPending(Yii::app()->session["userId"],$element) ?>" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations" data-name="<?= $element["name"] ?>" data-slug="<?= $element["slug"] ?>"><i class="fa fa-eye"></i> Découvrez les membres de l'organisation </a>
						<?php } ?>
						<!-- btn supprimer -->
						<?php if(Authorisation::isInterfaceAdmin() && isset($element["reference"]["costum"]) && array_search("meir", $element["reference"]["costum"]) !== false && isset($element["category"]) && $element["category"] == "acteurMeir"){?>
							<a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="noadmin" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
						<?php }else if(Authorisation::isInterfaceAdmin() && isset($element["source"]["keys"]) && array_search("meir", $element["source"]["keys"]) !== false && isset($element["category"]) && $element["category"] == "acteurMeir" && !isset($element["toBeValidated"])){?>
							<a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="withsource"  data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
						<?php }else if(Authorisation::isInterfaceAdmin() && isset($element["category"]) && $element["category"] == "acteurMeir" && isset($element["toBeValidated"]) && $element["toBeValidated"] == "true") {?>
							<a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="delete" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
						<?php } ?>

						<?php if(Authorisation::canEditItem(Yii::app()->session["userId"], @$element["collection"] ,(string)$element["_id"])){ ?>
							<?php  if(isset($element["category"]) && $element["category"] == "startup"){ ?>
								 <a href="javascript:;" class="btn-edit-startup pull-left btn margin-right-10 letter-blue btnstyle" data-id="<?php echo (string)$element["_id"] ?>" data-type="startup"><i class="fa fa-pencil"></i> Modifier les informations</a>
							<?php } ?>
							<?php if(isset($element["source"]["key"]) && $element["source"]["key"] == "meir" && isset($element["category"]) && $element["category"] == "startup"){?>   
								<a href="javascript:;" class=" btn btn-delete pull-left margin-right-10  delete" data-deletetype="withsource" data-id="<?php echo (string)$element["_id"]?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer</a>
							<?php }?> 
						<?php }?>
					</div>	
				</div>
				<div class="well well-fiche col-xs-12 text-blue-fiche bg-blue-fiche" >

					<?php if (isset($element["address"])){?>
						<b>Adresse : </b> 
							<?php if (isset($element["address"]["streetAddress"] ))	
								echo $element["address"]["streetAddress"];
							echo " ".$element["address"]["postalCode"]." ".$element["address"]["addressLocality"];	
							?>
					<?php }?><br>			
					<?php if (isset($element["otherLocality"])) { ?>
						<b>Autre(s) adresse(s) :</b>
					<?php
						echo "<span class='markdown'>" .$element["otherLocality"] ."</span><br><br>";  
					}?>	
					<?php if (isset($element["telephone"]["mobile"])){ ?>
						<b>Téléphone : </b> <?php 
							$replaced = str_replace(' ', '', $element["telephone"]["mobile"][0]);
							if (strpos($element["telephone"]["mobile"][0],"+") !== false){
								$result = sprintf("%s %s %s %s %s %s",
								substr($replaced, 0, 4),
								substr($replaced, 4, 1),
								substr($replaced, 5, 2),
								substr($replaced, 7, 2),
								substr($replaced, 9, 2),
								substr($replaced, 11));

							} else{
								$result = sprintf("%s %s %s %s %s %s",
								substr($replaced, 0, 2),
								substr($replaced, 2, 2),
								substr($replaced, 4, 2),
								substr($replaced, 6, 2),
								substr($replaced, 8, 2),
								substr($replaced, 10));
							}
							echo $result; ?>
						<br><br>
								
					<?php } ?>
					
					<?php if (isset($element["email"])) {?>
						<b>Mail : </b><?php 
						echo $element["email"]; ?>
						<br><br>
					<?php } ?>
					<?php if (isset($element["link"])){
						if(!str_contains($element["link"], "http") && $element["link"]!=""){
							$element["link"] = "https://".$element["link"];
						}
						?>
						 <b>Lien : </b>  <a class="link-fiche" href="<?php echo $element['link']?>" target="_blank"> 
								<?php echo $element["link"]; ?>
							
						</a> 
						<br><br>
					<?php } ?>
					<?php if(isset($element["category"]) && $element["category"] == "startup" &&  isset($element["yearEntryInpri"])){ ?>
						<b>Année d'entrée au sein de l'Incubateur de la Recherche Publique : </b> 
						<?php 
							echo $element["yearEntryInpri"]; ?>
					<?php } ?>
				</div>
			</div>
			<?php 
			if(isset($element["profilImageUrl"])) 
				$src = $element["profilImageUrl"];
			else 
				$src =  Yii::app()->getModule("costum")->assetsUrl."/images/meir/TIERS-LIEUX.png";
			?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img  class="img-responsive fiche-img" src = "<?= $src?>">
				<div class="well well-fiche text-blue-fiche bg-white" >
					<?php if(isset($element["category"]) && $element["category"] == "startup"){?>
						<?php if(isset($element["areaOfIntervention"])) {?>
							<b>Domaine d'intervention :  
								<?php if (isset($element["areaOfIntervention"])) 
									echo $element["areaOfIntervention"]; 
								?>
							</b><br><br>
						<?php } ?>
						<?php if (isset($element["namesStartupers"])) {?>
							<b>Nom(s) du(es) Startuper(uses)s :
								<br>
									<?php foreach($element["namesStartupers"] as $kName => $vName)
										if(isset($vName["nameStartuper"])) echo "- ".$vName["nameStartuper"]."<br>"; 
								?>
							</b><br>
						<?php } ?>
						<?php if (isset($element["objectiveOdd"]))  {?>
							<b>Objectif(s) ONUSIEN de Développement Durable :
								<br>
								<?php 
									if(is_array($element["objectiveOdd"])){
										foreach($element["objectiveOdd"] as $kodd => $vOdd){
											echo "-". $vOdd."</br>";
										}
									}else {
										echo $element["objectiveOdd"];
									}
								?>
							</b>
						<?php } ?>
					<?php }else{ ?>
						<?php  if (isset($element["legalStatus"])) {?>
							<b>Statut Juridique :
								<br>
								<?php echo $element["legalStatus"];  ?>
							</b>
							<br><br>
						<?php } ?>
						<?php if (isset($element["responsable"])) { ?>
							<b>Responsable : <br>
								<?php 
									echo $element["responsable"]; 
								?>
							</b>
							<br><br>
						<?php } ?>
						<?php if (isset($element["thematic"]) && $element["thematic"] != "" && isset($element["category"]) && $element["category"] == "acteurMeir") {?>
							<b>Secteurs d'activités : <br>
								<?php
									if(is_array($element["thematic"])){
										foreach($element["thematic"] as $kt => $vt){
											echo "-". $vt."</br>";
										}
									}else {
										echo $element["thematic"];
									}
								
								?>
							</b>
						<?php } 
					} ?>
				</div>
			</div>

		</div>


		<?php if (isset($element["objective"]) && isset($element["category"]) && $element["category"] == "acteurMeir"){?>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche"> OBJECTIF(S)</span>
						</div>
						<span class="markdown"><?php echo $element["objective"]; ?></span>
						</p>
					</div>
				</div>
			</div>     
		<?php }  else if(isset($element["description"]) && isset($element["category"]) && $element["category"] == "startup"){?>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche">OFFRES DE SERVICES </span>
						</div>
						<span class="markdown">
							<?php echo $element["description"]; ?>
						</span>
						</p>
					</div>
				</div>
			</div>   
		<?php } ?>
			
		<?php if (isset($element["objective"]) && isset($element["category"]) && $element["category"] == "acteurMeir"){ ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche">
								OFFRES DE SERVICES
							</span>
						</div>
						<p>
						<span class="markdown"><?php if (isset($element["serviceOffers"])) 
								echo $element["serviceOffers"]; 
							?>
						</span>
						</p>
					</div>
				</div>
			</div>  
		<?php } ?>
		<?php if (isset($element["linkWithInnovation"]) && isset($element["category"]) && $element["category"] == "acteurMeir"){ ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche">
								LIEN AVEC L'INNOVATION
							</span>
						</div>
						<p>
						<span class="markdown"><?php if (isset($element["linkWithInnovation"])) 
								echo $element["linkWithInnovation"]; 
							?>
						</span>
						</p>
					</div>
				</div>
			</div>  
		<?php } ?>
		<?php if (isset($element["serviceOffersInnovativeCompanies"]) && isset($element["category"]) && $element["category"] == "acteurMeir"){ ?>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche">
								SERVICE(S) PROPOSÉ(S) AUX ENTREPRISE INNOVANTES
							</span>
						</div>
						<p>
						<span class="markdown"><?php if (isset($element["serviceOffersInnovativeCompanies"])) 
								echo $element["serviceOffersInnovativeCompanies"]; 
							?>
						</span>
						</p>
					</div>
				</div>
			</div>  
		<?php } ?>
		<?php if( isset($element["jobFamily"]) && ($element["jobFamily"] == "Financement" || (is_array($element["jobFamily"]) && in_array("Financement", $element["jobFamily"])))){?>   
		
		<?php if (isset($element["linkFinancialDevice"]) && $element["linkFinancialDevice"] != []) {?>
			<div class="text-center">
				<span class="label label-fiche bg-blue-fiche "> DISPOSITIF(S) FINANCIER(S) </span> 
			</div>
			<div class="row">
				<?php foreach($element["linkFinancialDevice"] as $kLink => $vLink){ ?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="well well-fiche text-blue-fiche bg-white fiche-themetique" >
							<a  class="read-more" title="1"><?= $kLink+1 ?></a>
							<p class="text-center">
								<a class="link-fiche" href="<?= @$vLink["link"]?>" target="_blank"><?= @$vLink["financialDevice"]?></a>
							</p>
						</div>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
		
		<div class="row">
			<?php if (isset($element["financialPartners"]) && $element["financialPartners"] != []) { ?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="well well-fiche text-blue-fiche bg-white" >
						<div class="text-center">
							<span class="label label-fiche bg-blue-fiche">
								PARTENAIRE(S) FINANCIER(S)
							</span>
							<div class="row">						
								<?php foreach($element["financialPartners"] as $kFin => $vFin){
								?>
									<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 fiche-logo">
										<div class="description">
											<?php if (isset($vFin["financialPartner"])) echo $vFin["financialPartner"];?>
										</div>
									</div>
								<?php } ?>						
							</div>
						</div>
					</div>
				</div>
			<?php } ?>		
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="well well-fiche text-blue-fiche bg-white" >
					<p>
						<?php if (isset($element["modality"])) {?>	
							<b>Modalités : </b><br> 
							<?php  echo $element["modality"];  ?><br>
						<?php } ?>
						<?php if (isset($element["typeFinancing"])) {?>	
							<b>Financements : </b><br> 
							<?php  echo $element["typeFinancing"];  ?><br>
						<?php } ?>
						<?php if (isset($element["maximumAmount"])) { ?>
							<b>Montant maximal : </b><br>
							<?php echo $element["maximumAmount"];?><br>
						<?php } ?>
						<?php if (isset($element["publicCible"]) && $element["publicCible"] != "") {?>	
							<b>Public cible : </b><br> 
								<?php if(is_array($element["publicCible"])){
									foreach($element["publicCible"] as $kt => $vt){
										echo "-". $vt."</br>";
									}
								}else {
									echo $element["publicCible"];
								} ?> 
						<?php } ?>
					</p>
				</div>
			</div>
		</div>
		<div class="row">			
			<div class="row">
				<div class=" col-lg-2 col-md-3 col-sm-4 col-xs-6 fiche-logo pull-right">
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/meir/LOGO_FOND_BLANC.png" class="img-responsive logo">
				</div>
			</div>

		</div>

		<?php } ?>
	</div>
	<script>
		jQuery(document).ready(function() {
			// $.each($(".markdown"), function(k,v){
			// 	descHtml = dataHelper.markdownToHtml($(v).html()); 
			// 	$(v).html(descHtml);
			// });
			if(location.hash == "#mapping"){
				$(".btn-close").css("display","none")
			}
				
			setTitle("", "", costum.metaTitle);
			directory.bindBtnElement();
			$(".initialisSearch").on("click", function(){
				urlCtrl.loadByHash("#search");
			})
			$(".acceptActor").off().on("click", function(){
				var tplCtx = {};
				tplCtx.id = $(this).data("id");
				tplCtx.collection = $(this).data("type");
				tplCtx.value = null;
				tplCtx.path = "toBeValidated";
				dataHelper.path2Value(tplCtx, function(params) {
					toastr.success("Acteur ajouté"); 
					urlCtrl.loadByHash(location.hash);
				} );
		
			});
			$(".ficheMetier .showMembers").off().on("click",function () {
				var isAdmin = <?= json_encode(Meir::isAdminFiche(Yii::app()->session["userId"],$element))?>;
				var isAdminPending = <?= json_encode(Meir::isAdminPending(Yii::app()->session["userId"],$element))?>;
				var isAdminInviting =<?= json_encode(Meir::isAdminInviting(Yii::app()->session["userId"],$element))?>;
				var type = $(this).data("type")
				var name = $(this).data("name")
				var id = $(this).data("id")
				var slug = $(this).data("slug")
				if(isAdmin){
					urlCtrl.loadByHash("#@"+slug+".view.directory.dir.members")
				}else if(isAdminPending){
					bootbox.confirm("Vous devez être administrateur pour accéder à ces informations !! <br>Votre demande d'être admin est en attente de validation",
						function(result)
						{
							if (!result) {
								return;
							} else {
								return;
							}
						}
					);
				}else if(isAdminInviting){
					bootbox.dialog({ 
						message: "Vous devez être administrateur pour accéder à ces informations ! Veuillez accepter la demande !!",
						buttons: {
							success: {
								label: "Accepter la demande",
								className: "btn-primary",
								callback: function () {
									links.validate(type,id, userId,'citoyens','isInviting')
								}
							},
							cancel: {
								label: "Refuser la demande",
								className: "btn-danger",
								callback: function() {
									links.disconnect(type,id,userId,'citoyens','members',null,'','Etes-vous sûr-e de refuser cette invitation')
								}
							}
						}
					});
				}
				else{
					bootbox.dialog({ 
						message: "Vous devez être administrateur pour accéder à ces informations !!",
						buttons: {
							success: {
								label: "Dévenir admin",
								className: "btn-primary",
								callback: function () {
									links.connect(type,id,userId,'citoyens','member',name,true,null,'SOUHAITEZ-VOUS ÊTRE ADMINISTRATEUR DE LA FICHE','Merci de confirmer votre demande ')
								}
							},
							cancel: {
								label: trad["cancel"],
								className: "btn-secondary",
								callback: function() {
									$(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
								}
							}
						}
					});
				}
			})
			$(".ficheMetier .delete").off().on("click",function () {
				$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
				var btnClick = $(this);
				var id = $(this).data("id");
				var type = "organizations";
				var deleteType = $(this).data("deletetype");
				var params = [];
				var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
				if(deleteType == "noadmin" ){
					urlToSend = baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/reference";
						params = {
						id:  $(this).data("id"),
						type: $(this).data("type"),
						origin: "costum",
						sourceKey: "meir"
					}
				}
				if(deleteType == "withsource" ){
					urlToSend = baseUrl+"/costum/meir/deletewithsource";
					params = {
						id:  $(this).data("id"),
						type: $(this).data("type")
				}
				}
				if(deleteType == "delete" ){
					urlToSend =  baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
					params = null
				}
				bootbox.confirm("voulez vous vraiment supprimer cet acteur !!",
					function(result)
					{
						if (!result) {
							btnClick.empty().html('<i class="fa fa-trash"></i>  Supprimer');
							return;
						} else {
							ajaxPost(
								null,
								urlToSend,
								params,
								function(data){
									if ( data && data.result ) {
										toastr.success("acteur effacé");
										$("#"+type+id).remove();
										urlCtrl.loadByHash("#mapping");
									} else {
										toastr.error("something went wrong!! please try again.");
									}
								}
							);
						}
					}
				);
			});
			var dyfStartup = {
				"beforeBuild": {
					"properties": {
						"name": {
							"label": "Dénomination",
							"inputType": "text",
							"placeholder": "Raison Sociale de l’organisme"    
						},
						"namesStartupers": {
							"label": "Nom(s) du(es) Startuper(uses)s ",
							"inputType": "lists",
							"entries": {
								"key": {
									"type": "hidden",
									"class": ""
								},
								"nameStartuper": {
									"label": "Nom du Startuper(use)",
									"type": "text",
									"class": "col-md-11 col-sm-11 col-xs-12"
								}
							},
							"order": 3
						},
						"yearEntryInpri": {
							"label": "Année d'entrée au sein de l'Incubateur de la Recherche Publique",
							"inputType": "monthyear",
							"order": 4
						},
						"areaOfIntervention": {
							"label": "Domaine d'intervention",
							"inputType": "text",
							"order": 5
						},
						"link": {
							"label": "Lien WEB",
							"inputType": "text",
							"placeholder": "Lien WEB de l’organisme",
							"order": 6
						},
						"formLocality": {
							"label": "Adresse postale complète de l’organisme"
						},
						"description": {
							"label": "Offres de services",
							"inputType": "textarea",
							"order": 7
						},
						"email": {
							"label": "Mail",
							"placeholder": "Adresse(s) électronique(s) de l’organisme",
							"order": 8
						},
						"mobile": {
							"label": "Téléphone",
							"inputType": "text",
							"placeholder": "Téléphone fixe ou portable de l’organisme",
							"order":9
						},
						"objectiveOdd": {
							"label": "A quel(s) objectif(s) ONUSIEN de Développement Durable répondez-vous…?",
							"inputType": "selectMultiple",
							"placeholder": "",
							"class": "multi-select",
							"isSelect2": true,
							"list": "objectiveOdd",
							"order":8
						},
						"thematic": {
							"label": "Secteurs",
							"inputType": "selectMultiple",
							"placeholder": "",
							"class": "multi-select",
							"isSelect2": true,
							"list": "themes",
							"order": 11 
						},
						"image": {
							"label":"Télécharger votre LOGO en Haute Définition (HD)",
							"rules" : {
								"required": true
							}
						},
						"category": {
							"inputType": "hidden",
							"value": "startup"
						}
					}
				},
				"afterSave": "costum.meir.startup.afterSave",
				"afterBuild" :{},
				"onLoads" : {
					"onload" :{}
				},
				"onload": {
					"actions": {
						"setTitle": "PRÉSENTEZ VOUS",
						"html": {
							"infocustom": "<br/><?php echo Yii::t('cms', 'Fill in the field')?>"
						},
						"hide": {
							"shortDescriptiontextarea": 1,  
							"breadcrumbcustom" : 1,
							"parentfinder" : 1,                        
							"urltext": 1,
							"typeselect": 1,
							"roleselect": 1
						}
					}
				}
			}
			$(".ficheMetier .btn-edit-startup").off().on('click',function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				dyFObj.editElement('organizations',id,null,dyfStartup);
			});

			var dyfOrgaNoAdmin = {
				"beforeBuild": {
					"properties": {
						"name": {
							"label": "Dénomination",
							"inputType": "text",
							"placeholder": "Raison Sociale de l’organisme"
						},
						"legalStatus": {
							"label": "Statut juridique",
							"inputType": "tags",
							"class": "form-control",
							"info": "Si autre, vous pouvez écrire directement",
							"tagsList": "legalStatus",
							"maximumSelectionLength": 1,
							"order": 3,
							"rules": {
								"required": true
							}
						},
						"responsable": {
							"label": "Responsable",
							"inputType": "text",
							"placeholder": "Nom et prénom",
							"order": 4,
							"rules": {
								"required": true
							}
						},
						"formLocality": {
							"label": "Adresse postale complète de l’organisme",
							"rules": {
								"required": true
							}
						},
						"otherLocality" : {
							"label" : "Autre(s) adresse(s) postale(s)",
							"order" : 5
						},
						"mobile": {
							"label": "Téléphone",
							"inputType": "text",
							"placeholder": "Téléphone fixe ou portable de l’organisme",
							"order": 6,
							"rules": {
								"required": true
							}
						},
						"email": {
							"label": "Mail",
							"placeholder": "Adresse(s) électronique(s) de l’organisme",
							"order": 7,
							"rules": {
								"required": true
							}
						},
						"link": {
							"label": "Lien WEB",
							"inputType": "text",
							"placeholder": "Lien WEB de l’organisme ",
							"order": 8,
							"rules": {
								"required": true
							}
						},
						"jobFamily": {
							"label": "Quel est(sont) votre(vos) coeur(s) de métier(s)?",
							"inputType": "selectMultiple",
							"placeholder": "",
							"class": "multi-select",
							"isSelect2": true, 
							"list": "family",
							"order": 9,
							"rules": {
								"required": true
							}
						},
						"objective": {
							"label": "Objectif",
							"inputType": "textarea",
							"placeholder": "Présentation de l’organisme et de ses missions",
							"order": 10,
							"rules": {
								"required": true
							}
						},
						"serviceOffers": {
							"label": "Offres de services",
							"inputType": "textarea",
							"placeholder": "",
							"order": 11,
							"rules": {
								"required": true
							}
						},
						"thematic": {
							"label": "Secteurs",
							"inputType": "selectMultiple",
							"placeholder": "",
							"class": "multi-select",
							"isSelect2": true,
							"list": "themes",
							"order": 12
						},
						"statusActor" : {
							"inputType": "selectMultiple",
							"placeholder": "",
							"class": "multi-select",
							"isSelect2": true, 
							"label" : "A qui répond cet acteur ?",
							"list" :"status" ,
							"order" : 13
						},
						"linkFinancialDevice": {
							"label": "Lien(s) WEB(s) permettant d’accéder au(x) dispositif(s) financier(s) proposé(s) par votre organisme",
							"inputType": "lists",
							"entries": {
								"key": {
									"type": "hidden",
									"class": ""
								},
								"financialDevice": {
									"label": "Dispositif financier",
									"type": "textarea",
									"class": "col-md-6 col-sm-6 col-xs-12"
								},
								"link": {
									"label": "Lien",
									"type": "text",
									"class": "col-md-5 col-sm-5 col-xs-12"
								}
							},
							"order": 14,
							"rules": {
								"required": true
							}
						},
						"modality": {
							"label": "Modalités",
							"class": "form-control",
							"info": "Si autre, vous pouvez écrire directement",
							"inputType": "tags",
							"placeholder": "Modalités de sollicitation d’un financement",
							"tagsList": "modality",
							"order": 15
						},
						"typeFinancing": {
							"label": "Financements",
							"placeholder": "",
							"info": "Si autre, vous pouvez écrire directement",
							"inputType": "tags",
							"class": "form-control",
							"tagsList": "typeFinancing", 
							"order": 16,
							"rules": {
								"required": true
							}
						},
						"financialPartners": {
							"label": "Autres organismes financiers susceptibles d’adosser un co-financement aux côtés de celui de votre organisme",
							"inputType": "lists",
							"entries": {
								"key": {
									"type": "hidden",
									"class": ""
								},
								"financialPartner": {
									"label": "Partenaire financier",
									"type": "textarea",
									"class": "col-md-11 col-sm-11 col-xs-12"
								}
							},
							"order": 17,
							"rules": {
								"required": true
							}
						},
						"maximumAmount": {
							"label": "Montant maximal",
							"inputType": "text",
							"placeholder": "Précise le plafond maximal de l’enveloppe financière qui peut être sollicité ",
							"order": 18
						}, 
						"publicCible": {
							"label": "Public cible",
							"class": "multi-select",
							"inputType": "selectMultiple",
							"isSelect2": true, 
							"placeholder": "Précise le statut juridique du public éligible au financement",
							"list": "publicCible",
							"order": 19,
							"rules": {
								"required": true
							}
						},
						"image": {
							"label": "Télécharger votre LOGO en Haute Définition (HD) et à fond transparent",
							"rules": {
								"required": true
							}
						},
						"category": { 
							"inputType": "hidden",
							"value": "acteurMeir"
						}
					}
				},
				"afterSave": "costum.meir.organizations.afterSave",
				"beforeSave": "costum.meir.organizations.beforeSave",
				"afterBuild": {},
				"onLoads": {
					"onload": {}
				},
				"onload": {
					"actions": {
						"setTitle": "PRÉSENTEZ VOUS",
						"html": {
							"infocustom": "<br/>Devenir admin pour pouvoir éditer les autres champs<br>En remplissant ces champs, vous contribuez à la communauté de Communecter"
						},
						"hide": { 
							"nametext": 1,
							"mobiletext": 1,
							"formLocalityformLocality": 1,
							"locationlocation": 1,
							"shortDescriptiontextarea": 1,
							"urltext": 1,
							"tagstags": 1
						}
					}
				}
			}
			dyfOrgaNoAdmin.afterBuild = function(data){  
				if(typeof contextData != "undefined"){ 
					if(contextData.creator == userId ){
						$(".roleselect").show(); 
					}else
						$(".roleselect").hide();
				}else{
					$(".roleselect").hide();
				} 
				if($("#statusActor").val() == null){
					$("#statusActor").val("Une personne morale");
				}
				if($("#email").val() != " "){
					$(".emailtext").hide();
				}
				if($("#jobFamily").val() != "Financement" || $.inArray("Financement",$("#jobFamily").val()) ==-1){
					$(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").hide();
				}
				if($("#jobFamily").val() == "Financement" || $.inArray("Financement",$("#jobFamily").val()) >= 0){ 
					$(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").show();
				}
				$("#jobFamily").change(function(){
					setTimeout(function(){
						if($("#jobFamily").val() == "Financement" || $.inArray("Financement",$("#jobFamily").val()) >= 0){
							$(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").show();
						}else {
							$(".linkFinancialDevicelists,.modalitytags,.typeFinancingtags,.maximumAmounttext,.publicCibleselectMultiple,.financialPartnerslists").hide(); //cacher les champs financement
						}       
					}, 500)
				});
			}
			$(".ficheMetier .btn-edit-elementnoadmin").off().on('click',function(){
				var id = $(this).data("id");
				var type = $(this).data("type");
				dyFObj.editElement('organizations',id,null,dyfOrgaNoAdmin);
			});
		})
	</script>
<?php //}else{ ?>
	<!-- <script>
		$("#social-header").css("display","none");
		$("#menu-top-profil-social").css("display","none");
		$(".social-main-container").css("min-height","0");
		$(".acteurMeir .divNotConnected,.startup .divNotConnected").css("height","900px !important"); 
		$(".acteurMeir , .startup").html("<div class='text-center divNotConnected'> BIENTOT DISPONIBLE </div>")
	</script> -->
<?php // }?>