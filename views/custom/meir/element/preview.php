<style>
    #modal-preview-coop{
        overflow: auto;
    }
    .short-description{
        font-size:20px;
        text-align: justify;
    }
    
    .div-preview{
        text-align: justify;
        margin-left: 5%;
        margin-right: 5%;
        font-size: 18px !important;
    }
    .description-preview.activeMarkdown p,
    .area-preview.activeMarkdown p,
    .description-preview.activeMarkdown li,
    .area-preview.activeMarkdown li{
        font-size: 14px !important;
    }
    .link-files{
        border-radius: 3px;
        padding: 5px 10px;
    }




    .heading img{
        width: 10%;
    }

    .preview-info{
        padding: 5%;
        background:#fff;
        box-shadow: 0px 0px 4px 0px #b0b3b7;
        margin-top: 10px;
        margin-bottom: 20px;
    }
    .preview-image{
        text-align:center;
    }
    .preview-image img{
        border-radius:10%;
    }
    .preview-content{
        text-align:left;
    }
    .preview-content .title-preview {
        font-weight:600;
        text-transform: none;
    }
    .preview-content .preview-shortDescription {
        font-size: 18px;
    }
    .preview-content .header-address {
        font-size: 18px;
        font-weight: 500;
        color: #428bca;
        padding: 5px;
    }
    
    .preview-content .item-content-block{
        padding:20px; 
        border-top:2px solid #f6f6f2; 
        background-color:transparent; 
        display:block;
    }
    .preview-content .tags a{
        padding: 8px;
        color: #1A2660;
        background: #f3f3f3;
        display: inline-block;
        font-size: 13px;
        line-height: 11px;
        border-radius: 10px;
        margin-bottom: 5px;
        margin-right: 2px;
        text-decoration: none;
    }
    .preview-content .tags a:hover{
        background-color: #1A2660;
        color: white;
    }
    .blockFontPreview{
        font-size : 18px
    }
    @media (max-width: 1199px) {
        .blockFontPreview{
            text-align: left;
        }
        .preview-content{
            text-align:center;
        }
        .preview-content .title-preview {
            text-align: center;
            font-size: 24px;
        }
        .preview-content .header-address {
            text-align: center;
        }
        .preview-content .preview-shortDescription {
            text-align: center;
        }
    }
</style>
<?php

    if(isset($element)){
        $element = $element;
    }else{
        $element=PHDB::findOne($collection,array("_id"=> new MongoId($id)));
    }
//var_dump($element);
?>
<div class="margin-top-25 margin-bottom-50 col-xs-12">
    <div class="col-xs-12 no-padding toolsMenu">
        <button class="btn btn-default pull-right btn-close-preview" >
            <i class="fa fa-times"></i>
        </button>
        <?php if($element["collection"] != Poi::COLLECTION){?>
            <a href="#@<?php echo $element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10" ><?php echo Yii::t("common", "Go to the item") ?></a>
        <?php } ?>   
        </div>

    <div class="col-xs-12 preview-info">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xl-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="preview-image">
                            <!--<img src="<?php /*echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) */?>" alt="image" />-->
                            <?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?>
                                <a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
                                   class="thumb-info"
                                   data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
                                   data-lightbox="all">
                                    <img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
                                </a>
                            <?php }else{ ?>
                                <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xl-12">
                <div class="preview-content">
                    <h1 class="title-preview"><?php echo $element["name"]; ?></h1>
                    <?php if(isset($element["shortDescription"]) && !empty($element["shortDescription"]) ) echo "<div class='col-xs-12 preview-shortDescription markdown-text'>".$element["shortDescription"]."</div>"; ?>
                    <?php
                    if(isset($element["address"]["addressLocality"]) && !empty($element["address"]["addressLocality"])){ ?>
                        <div class="header-address col-xs-12 blockFontPreview">
                            <?php
                            echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
                            echo !empty($element["address"]["postalCode"]) ?
                                $element["address"]["postalCode"].", " : "";
                            echo $element["address"]["addressLocality"];
                            ?>
                        </div>
                    <?php } ?>
                    <div class=" col-xs-12 item-content-block tags">
                        <?php
                        if(isset($element["tags"]) && $element["collection"]!=Project::COLLECTION){
                            foreach ($element["tags"] as $key => $tag) { ?>
                                <a  href="#search?tags=<?= $tag?>"  class="lbh" style="vertical-align: top;">#<?php echo $tag; ?></a>
                            <?php }
                        } ?>
                    </div>
                    <?php if(isset($element["namesStartupers"]) && !empty($element["namesStartupers"])){ ?>
                        <div class=" col-xs-12 blockFontPreview">
                            Nom(s) du(es) Startuper(uses)s : 
                            <span class="header-address"> 
                                <?php 
                                    echo "<br>";
                                    foreach($element["namesStartupers"] as $knam => $vnam){
                                        echo "- " .$vnam["nameStartuper"]."</br>";
                                    }                                
                                ?>
                            </span>                            
                        </div>
                    <?php } ?>
                    <div class=" col-xs-12 blockFontPreview">
                        Année d'entrée au sein de l'Incubateur de la Recherche Publique : 
                        <span class="header-address">
                            <?php if(isset($element["yearEntryInpri"])) echo $element["yearEntryInpri"]; ?>
                        </span>
                    </div>
                    <?php if(isset($element["thematic"]) && !empty($element["thematic"])){ ?>
                        <div class=" col-xs-12 blockFontPreview">
                        Secteurs : 
                        <span class="header-address"> 
                            <?php 
                                if(is_array($element["thematic"])){
                                    echo "<br>";
                                    foreach($element["thematic"] as $kthem => $vthem){
                                        echo "- " .$vthem."</br>";
                                    }
                                }else {
                                    echo $element["thematic"];
                                }
                                ?>
                        </span>
                            
                        </div>
                    <?php } ?>
                    <?php if(isset($element["link"]) && !empty($element["link"])){ ?>
                        <div class=" col-xs-12 blockFontPreview">
                            Lien WEB de l’organisme :
                            <span class="header-address"> 
                                <a class="header-address" href="<?php echo $element["link"];?>" target="_blank"><?php echo $element["link"];?></a>
                                                            </span>                            
                        </div>
                    <?php } ?>
                    <?php if(isset($element["email"]) && !empty($element["email"])){ ?>
                        <div class=" col-xs-12 blockFontPreview">
                            Mail de l’organisme :    
                            <span class="header-address"> 
                                <?php echo $element["email"];?>
                            </span>                            
                        </div>
                    <?php } ?>
                    <?php if(isset($element["telephone"]["mobile"]) && !empty($element["telephone"]["mobile"])){ ?>
                        <div class=" col-xs-12 blockFontPreview">
                        Telephone de l’organisme : 
                            <span class="header-address"> 
                                <?php 
                                foreach($element["telephone"]["mobile"] as $kPhone => $vPhone){
                                    echo $vPhone ."<br>";
                                }
                                ?>
                            </span>                            
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
    <?php if(isset($element["objectiveOdd"]) && !empty($element["objectiveOdd"])){ ?>
        <div class=" col-xs-12 div-preview">
            <b>Objectifs Développement Durable :</b>
            <?php
                if(is_array($element["objectiveOdd"])){
                    echo "<br>";
                    foreach($element["objectiveOdd"] as $kobodd => $vobodd){
                        echo "- " .$vobodd."</br>";
                    }
                }else {
                    echo $element["objectiveOdd"];
                }
            ?><br>
        </div>
    <?php } ?>
    <div class="col-xs-12 div-preview area-preview markdown-txt"><br><?php  if(isset($element["areaOfIntervention"])) echo "<b>Domaine d'intervention :</b>  <br>". $element["areaOfIntervention"]; ?></div>
    <div class="col-xs-12 div-preview description-preview markdown-txt">  <br><?php  if(isset($element["description"])) echo "<b>Offres de services : </b>". $element["description"]; ?></div>
</div>
<script>
    jQuery(document).ready(function() {
        $.each($(".markdown-txt"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html()); 
            $(v).html(descHtml);
        });      

    });

</script>


