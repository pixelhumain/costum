<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>

<style>

	#filterContainer #input-sec-search .input-global-search, .searchBar-filters .search-bar, #filterContainer #input-sec-search .input-group-addon, .searchBar-filters .input-group-addon {
		margin-top:0px !important;	
	}

	.searchObjCSS .searchBar-filters {
		margin-top: 5px;
	}
	@media (min-width: 1338px){
		.searchObjCSS .searchBar-filters{
			width: 37%!important;
		}
		.searchBar-filters .main-search-bar-addon{
			width: 7% !important;
		}

	}
	@media screen and (min-width: 1200px) and (max-width: 1337px) { 
		.searchObjCSS .searchBar-filters{
			width: 44%!important;
		}
		.searchBar-filters .main-search-bar-addon{
			width: 7% !important;
		}

	}
</style>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var thematic = null;
	var typeFinancing = null;
	var greatFamily = null;
	var statu = null;
	/*
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.secteur != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.secteur);
	}
	*/
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.themes != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.themes);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.accompanimentTypeFilter != "undefined"){
		greatFamily = {};
		Object.assign(greatFamily, costum.lists.accompanimentTypeFilter);
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.status != "undefined"){
		statu  = {};
		Object.assign(statu , costum.lists.status);
	}
	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.typeFinancing != "undefined"){
		typeFinancing  = {};
		
		Object.assign(typeFinancing , costum.lists.typeFinancing);
	}

	var defaultScopeList = ["RE"];

	var defaultFilters = {'$or':{}};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["source.keys"] = costum.slug;
    defaultFilters['$or']["reference.costum"] = costum.slug;
    defaultFilters['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    defaultFilters["toBeValidated"]={'$exists':false};
    defaultFilters["category"] = {'$ne':"startup"};
	let paramString = pageApp.split('?')[1];
	let queryString = new URLSearchParams(paramString);

	for (let pair of queryString.entries()) {
		if(pair[0] != "tags"){
			if(pair[0] != "text"){
				defaultFilters[pair[0]]=pair[1].replace("%", "");
			}
		}else{ // remove this if & are removed from tags
			searchObj.search.obj.tags = [];
		}
	}

    /*defaultFilters['$or']['$and'] = [
    	{"tags" = ["numérique"]}
    ];*/

	var paramsFilter= {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : ["organizations"],//(appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	fields: ["name","objectiveOdd", "category", "type", "typeFinancing"],
		 	filters: defaultFilters,
			sortBy: {"name": 1},
			indexStep:0
		},
		filters : {
			// secteurFamily:{
			// 	view : "megaMenuDropdown",
	 		// 	type : "tags",
	 		// 	remove0: false,
	 		// 	countResults: true,
	 		// 	name : "<?php //echo Yii::t("common", "search by theme")?>",
	 		// 	event : "tags",
	 		// 	keyValue: true,
	 		// 	list : {"secteurs":thematic, "Familles":greatFamily}
			// },
			status : {
				view : "dropdownList", 
		 		type : "tags",
				name : "Je suis",
				event : "tags",
	 			keyValue: false, 
				list : statu		
			},
		 	family : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : "Je cherche",
	 			keyValue: false,
				event : "tags",
		 		list : greatFamily
		 	},
			
		 	secteur : {
	 			view : "dropdownList",
	 			type : "tags",
	 			countResults: true,
	 			name : "Secteurs",
				event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		}, 
		 	typeFinancing : {
	 			view : "dropdownList",
	 			type : "tags",
				trad:false,
				field : "typeFinancing",
	 			name : "Financements",
				event : "tags", 
	 			list : typeFinancing
	 		},
 			/*scopeList : {
	 			name : "<?php //echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList, 
	 				level : ["3"]
	 			}
	 		},
	 		
			category : {
				view : "dropdownList",
				type : "category",
				name : "Catégorie",
				event : "filters",
				keyValue : false,
				list : {
					"startup" : "Startups",
					"acteurMeir" : "Acteurs du mapping"
				}
			},*/
	 		text : {
				searchBy:"name",
				placeholder : "Quel acteur recherchez-vous ?"
			}
	 	},
	 	results : {
			renderView: "meirFilter.elementPanelHtmlFullWidth",
		 	smartGrid : false
		}
	}
	var meirFilter = {
		elementPanelHtmlFullWidth : function (params) {
		mylog.log("elementPanelHtmlFullWidth", "Params", params);

		var str = '';

		str += '<div class="container-lib-item">' +
			'<div id="entity_' + params.collection + '_' + params.id + '" class="col-md-12 col-xs-12 col-sm-12 no-padding lib-item searchEntityContainer ' + params.containerClass + '" data-category="view">' +
			'<div class="lib-panel">' +
			'<div class="row box-shadow">' +
			'<div class="col-xs-12 col-sm-3 col-md-2 no-padding">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' +
			params.imageProfilHtml +
			'</a>' +
			'</div>' +
			'<div class="col-xs-12 col-sm-9 col-md-10">' +
			'<div class="lib-row lib-header">' +
			'<a href="' + params.hash + '" class="' + params.hashClass + '">' + params.name + '</a>' +
			'<div class="lib-header-seperator"></div>' +
			'</div>';

		if (notNull(params.localityHtml)) {
			str += '<div class="entityLocality">' +
				'<span> ' + params.localityHtml + '</span>' +
				'</div>';
		}
		if (typeof params.tagsHtml != "undefined")
			str += '<ul class="tag-list">' + params.tagsHtml + '</ul>';
		str += '</div>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		return str;
	}
	} 
	if(pageApp == "projects"){
		delete paramsFilter.filters["type"];
	}

	if(thematic==null || pageApp=="projects"){
		delete paramsFilter.filters["secteur"];
	}

	if(pageApp=="projects"){
		delete paramsFilter.filters["families"];
	}
	 
	jQuery(document).ready(function() { 
		keyboardNav.keyMapCombo = null
		if(typeof paramsFilter.defaults.filters != "undefined" && typeof paramsFilter.defaults.filters.typeFinancing != "undefined"){  
			paramsFilter.defaults.filters.typeFinancing = paramsFilter.defaults.filters.typeFinancing.split(',');
		}

		searchObj.search.sortAlphaNumeric = function(results){
    		mylog.log("searchObj.search.callBack tosort", results);
			let sorted = Object.values(results).sort((a, b) => {
				let fa = a.name.replace(" ", "").trim().toLowerCase(),
					fb = b.name.replace(" ", "").trim().toLowerCase();

				return ('' + fa).localeCompare(fb);
			});
			let sortedResults = {}
			$.each(sorted, function(i, val){
				sortedResults[val._id.$id] = val;
			});
			return sortedResults;
    	};

		searchObj["scroll"].render = function(fObj, results, data){
			results = searchObj.search.sortAlphaNumeric(results);
      		//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				// if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				// }else{
				// 	toastr.error("Aucun résultat trouvé. Essayez avec un terme plus précis.");					
				// }	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique);
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
					}
					imageObserver.observe(v);
				});
			 }
		}

		filterSearch = searchObj.init(paramsFilter);
		
		//$("div[data-key*='etC']").hide()
		$(".count-badge-filter").remove();

		$(".searchBar-filters").append("<div class='text-right hidden-xs' style='padding-left: 24px;'><a href='javascript:;' class='initialisSearch btn '>Nouvelle recherche</a></div>");
		$(".menu-filters-xs").append("<div class='text-right hidden-lg hidden-md  visible-xs' style=''><a href='javascript:;' class='initialisSearch btn '>Nouvelle recherche</a></div>");
		/*$("#filterContainerInside").append(`
			<button id="toggleDisplayMode" type="button" class="btn text-dark float-right margin-top-5" style="" title="affichage par ligne"><i class="fa fa-list"></i></button>
		`);

		$("#toggleDisplayMode").click(function(e){
			if(filterSearch.results.renderView == "directory.elementPanelHtmlFullWidth"){
				filterSearch.results.renderView = "directory.classifiedPanelHtml";
				filterSearch = filterSearch.init({results:{renderView:"directory.classifiedPanelHtml"}});
			}else{
				filterSearch.results.renderView = "directory.elementPanelHtmlFullWidth";
				filterSearch = filterSearch.init({results:{renderView:"directory.elementPanelHtmlFullWidth"}});
			}
		})*/

		$(".initialisSearch").on("click", function(){
			urlCtrl.loadByHash("#search");
		})
		$(".filters-activate[data-key='startup'], .filters-activate[data-type='objectiveOdd']").on("click", function(){
			urlCtrl.loadByHash(location.hash);
		})
		
	});

</script>