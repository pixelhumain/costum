<?php 
	$urlSetting = $urlRedirect."/#setting.redirect";
	$users = [];
	if(isset($userId)){  
		$urlSetting .="?user=".$userId."&action=passwordRetreive";
		$users = PHDB::findOne(Person::COLLECTION,array("_id"=> new MongoId($userId)));
	}

?>
<table class="row masthead" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: white;width: 100%;position: relative;display: table;">
	<tbody>
		<tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Masthead -->
			<th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: NotoSans-Regular;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
				<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: NotoSans-Regular;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<h1 class="text-center" style="color: #e33551;font-family: NotoSans-Regular;font-weight: normal;padding: 35px 0px 15px 0px;margin: 0;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 0px;font-size: 34px;">Mail de Réinitialisation – Mapping</h1>
						</th>
					</tr>
				</table>
			</th>
		</tr>
	</tbody>
</table>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;"><tbody><tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
	<th class="small-12 large-12 columns first" style="color: #3c5665;font-family: NotoSans-Regular;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
		<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
			<tr style="padding: 0;vertical-align: top;text-align: left;">
				<th style="color: #3c5665;font-family: NotoSans-Regular;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
				<p style="text-align:center;font-size:17px; line-height: 30px;">RÉINITIALISATION DE VOTRE MOT DE PASSE MAPPING </p><br>
				<p style="text-align:justify;font-size:17px; line-height: 30px;">
					Bonjour <?= @$users["name"]?> , <br>
					Nous avons reçu une demande de réinitialisation du mot de passe de votre compte du Mapping Digital de l’Innovation. Veuillez cliquer sur le bouton ci-dessous pour le réinitialiser. <br>
				</p>
				<p style="text-align:justify;font-size:17px; line-height: 30px;">
					<?php echo Yii::t("mail","To connect you, you can use this password now") ?> : <br>
					<h5 style="color:#e33551;font-family: NotoSans-Regular;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;"><?php echo $pwd?></h5>
				</p><br>
				<p style="text-align:justify;font-size:19px; line-height: 30px; font-family: NotoSans-Regular;">
					Ce bouton de réinitialisation du mot de passe expirera dans 30 minutes. <br><br>
					<p style = "text-align:center; font-family: NotoSans-Regular;">
						<a href="<?= $urlSetting?>" style="padding: 8px;background-color : #0E163C; color: white;font-family: NotoSans-Regular;font-weight: normal;margin: 0;text-align: center;line-height: 1.3;text-decoration: none;"> CHANGER DE MOT DE PASSE</a>
					</p><br>
				<p  style="text-align:justify;font-size:18px; line-height: 30px; font-family: NotoSans-Regular;">Si vous n’avez pas demandé la réinitialisation de votre mot de passe, veuillez ignorer cet e-mail ou contacter <a href="mailto:mapping@technopole-reunion.com" style = "color:#3c5665 ;font-family: NotoSans-Regular;"><u>notre équipe d’assistance</u> </a> si vous avez des questions.  </p><br>
				</th>
			</tr>
		</table>
	</th>
</tr></tbody></table>