<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<?php 
    $timestamp = $created->sec;
    setlocale(LC_TIME, 'fr_FR.utf8');
    $dateString = strftime("%e %B %Y", $timestamp);
    setlocale(LC_TIME, 'C');
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="body" style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <tr>
        <td bgcolor="#f4f4f4 " align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#f4f4f4 " align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;background-color: #0a464e;">
                        <?php if (!empty($logo)) { ?>
                            <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logo?>" style="width: 30%;display: block;border: 0px;" />
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;padding-bottom: 50px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                        <div style="margin: 0;white-space: pre-line;text-align: center;">
                            <span style="text-align: left;"><?php echo Utils::getServerName()?></span>
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <p>Vous avez reçu un message pour <a href="<?php echo $baseUrl; ?>"><?php echo $baseUrl; ?></a></p>
                                        <p style="font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; ">Envoyé par : <a href="mailto:<?php echo $emailSender; ?>"><?php echo $emailSender; ?></a></p>
                                        <p style="font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px;">Le : <?php echo $dateString; ?></p>
                                        <h4>Objet : <?php echo $subject; ?></h4>
                                        <p style="font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;" ><?php echo $message; ?></p>
                                    
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" align="left" style="padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 25px;">
                        <p style="margin: 0;white-space: pre-line;"><br><?= @$sign ?></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>