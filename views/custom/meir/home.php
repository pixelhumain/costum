<?php 
  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
    $cmsList = Cms::getCmsWithChildren($this->costum["contextId"], "welcome", array("position" => 1));
//if(Authorisation::isInterfaceAdmin() || Yii::app()->session["userId"]){ 
  $params = [
    "tpl" => $el["slug"],
    "slug"=>$this->costum["slug"],
    "canEdit"=>true,
    "cmsList" => $cmsList,
    "page"=> "welcome",
    "el"=>$el,
    "tplUsingId"     => "",
    "tplKey"   => "",
    "tplInitImage"   => "",
    "nbrTplUser"     => 0,
    "nbrTplViewer"   => 0,
    "paramsData"     => [],
    "insideTplInUse" => array(),
    "newCmsId"       => array(),
    "costum"         => $this->costum,
    "cmsInUseId"     => array()
  ]; 
    echo $this->renderPartial("costum.views.cmsBuilder.tplEngine.index", $params,true );
//} else{ ?>
  <!-- <div class="text-center divNotConnected"> BIENTOT DISPONIBLE </div> -->
<?php } //} ?>
