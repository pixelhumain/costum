<?php
$cssAnsScriptFilesModule = array(
    '/js/default/preview.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());

	$financers=[];
    $partners=[];
    $porteurCTE=[];
    $porteurAction=[];
    
    $params["nbContributors"]=0;
 
    $where=array("id"=>@$id, "type"=>$type, "doctype"=>"image", "contentKey"=> "slider");
    $images = Document::getListDocumentsWhere($where, "image");
?>
<style type="text/css">
    .social-share-button img{
        margin-right: 10px;
    }
</style>
<div class="col-xs-12 padding-10 toolsMenu">
	<button class="btn btn-default pull-right btn-close-preview">
		<i class="fa fa-times"></i>
	</button>
	<a href="#@<?php echo $element["slug"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?></a>
	<?php if ($element["collection"] === "projects") { ?>
		<button class="btn btn-default pull-right btn-close-preview margin-right-10">
			Valider
		</button>
	<?php } ?>
</div>
<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
<?php 
	if (!@$element["profilBannereUrl"] || (@$element["profilBannereUrl"] && empty($element["profilBannereUrl"])))
		$url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
	else
		$url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]); 
	?> 
    	
	<div class="col-xs-12 no-padding" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
		<?php 
			$imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
				alt="'.Yii::t("common","Banner").'" 
				src="'.$url.'">';
			if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
				$imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
							class="thumb-info"  
							data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
							data-lightbox="all">'.
							$imgHtml.
						'</a>';
			}
			echo $imgHtml;
		?>		
	</div>
	<div class="content-img-profil-preview col-xs-6 col-xs-offset-3 col-lg-4 col-lg-offset-4">
		<?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?> 
		<a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
		class="thumb-info"  
		data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
		data-lightbox="all">
			<img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
		</a>
		<?php }else{ ?>
			<img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
		<?php } ?>
	</div>
	<div class="col-xs-12 margin-bottom-20 no-padding">
		<h3 class="title text-gray col-xs-12 text-center margin-bottom-20"><?php echo $element["name"] ?></h3>
        <span class="col-xs-12 text-center blockFontPreview"> 
            <?php if($type==Project::COLLECTION){ 
                $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Territoire en CTE" : "Action";
                $iconColor=(isset($element["category"]) && $element["category"]=="cteR") ? $iconColor : "purple";
                $icon=(isset($element["category"]) && $element["category"]=="cteR") ? "map-marker" : "lightbulb-o";
                ?> 
                
                <span class="text-<?php echo $iconColor; ?>"><i class="fa fa-<?php echo $icon ?>"></i> <?php echo $label; ?></span>
            <?php }else{ ?>
                <span class="text-<?php echo @$iconColor; ?>"><?php echo strtoupper(Yii::t("common", Element::getControlerByCollection($type))); ?></span>
            <?php } ?>
            <?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){ 
                if($type==Organization::COLLECTION)
                    $typesList=Organization::$types;
                else
                    $typesList=Event::$types;
                ?>
                    <i class="fa fa-x fa-angle-right margin-left-10"></i>
                    <span class="margin-left-10"><?php echo Yii::t("category", $typesList[$element["type"]]) ?></span>
            <?php } ?>
            
        </span>
        
        <?php 
            if(!empty($element["address"]["addressLocality"])){ ?>
                <div class="header-address col-xs-12 text-center blockFontPreview">
                    <?php
                        echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
                        echo !empty($element["address"]["postalCode"]) ? 
                                $element["address"]["postalCode"].", " : "";
                        echo $element["address"]["addressLocality"];
                    ?>
                </div>
            <?php } ?>
            <div class="header-tags col-xs-12 text-center blockFontPreview">
                <?php 
                if(@$element["tags"] && $type!=Project::COLLECTION){ 
                    foreach ($element["tags"] as $key => $tag) { ?>
                        <a  href="javascript:;"  class="letter-red" style="vertical-align: top;">#<?php echo $tag; ?></a>
                    <?php } 
                } ?>
                </div>
            </div>
        <?php if($type==Event::COLLECTION){ ?>
            <div class="event-infos-header text-center margin-top-10 col-xs-12 blockFontPreview"  style="font-size: 14px;font-weight: none;"></div>
        <?php } ?>
        <?php if(isset($element["filRouge"])){ 
            $descr= (strlen($element["filRouge"]) > 250) ? substr($element["filRouge"], 0, 250)." ..." : $element["filRouge"]; ?>
            <div class="col-xs-12 text-center margin-bottom-20 markdown-txt"><?php echo $descr ?></div>

        <?php } ?> 
        <div class="social-share-button col-xs-12 text-center margin-bottom-20"></div> 
		<?php if(isset($element["shortDescription"]) && !empty($element["shortDescription"]) && $element["shortDescription"] != "Nouveau Candidat au CTE") echo "<span class='col-xs-12 text-center'>".$element["shortDescription"]."</span>"; ?>
    	<?php if(@$element["category"]=="cteR"){ 
            $descr= (@$element["why"]) ? substr($element["why"], 0, 400)." ..." : ""; ?>
    		<div class="col-xs-12 text-center markdown-txt"><?php echo $descr ?></div>
	    <?php	}else{ 
                $descr= (@$element["description"]) ? substr($element["description"], 0, 400)." ..." : ""; ?>

				<div class="col-xs-12 text-center markdown-txt"><?php echo $descr ?></div>
		<?php	}?>
		<div class="col-xs-12">
			<?php 
				if(isset($element["links"]) && isset($element["links"]["contributors"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les contributeurs", "links"=>$element["links"]["contributors"], "connectType"=>"contributors", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 
				if(isset($element["links"]) && isset($element["links"]["members"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les membres", "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 
				if(isset($element["links"]) && isset($element["links"]["projects"])){ 
                    if($type==Project::COLLECTION){
                        $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Les actions" : "Développé sur"; 
                    }
                    else{
                        $label="Contribue à";
                    }
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>$label, "links"=>$element["links"]["projects"], "connectType"=>"projects", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
				} 

				if(isset($element["links"]) && isset($element["links"]["memberOf"])){ 
					echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Membre de", "links"=>$element["links"]["memberOf"], "connectType"=>"organizations", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10", "contextId"=>$id, "contextType"=>$type));
				} 
			?>
		</div>
		
	</div>
	

?>
</div>
<script type="text/javascript">
    var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
	jQuery(document).ready(function() {
		coInterface.bindTooltips();
        $(".markdown-txt").each(function(){
            descHtml = dataHelper.markdownToHtml($(this).html()); 
            var tmp = document.createElement("DIV");
            tmp.innerHTML = descHtml;
            descText = tmp.textContent || tmp.innerText || "";
            $(this).html(descText);
        });
        
        $(".container-preview .social-share-button").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : [{"type":"facebook"}, {"type":"twitter"} ], "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
        resizeContainer();
	});
	
/*
{
    "_id" : ObjectId("6045c3ddda83ef2d058b456a"),
    "name" : "university",
    "type" : "LocalBusiness",
    "role" : "admin",
    "preferences" : {
        "isOpenData" : "true",
        "isOpenEdition" : "true"
    },
    "costum" : {
        "slug" : "university",
        "css" : {
            "urls" : [ 
                "index.css"
            ],
            "cssCode" : "",
            "font" : {
                "useUploader" : false,
                "url" : "/font/jaona/DroidSans.ttf"
            }
        },
        "app" : {
            "#observatory" : {
                "hash" : "#app.view",
                "subdomainName" : "OBSERVATORY",
                "urlExtra" : "/page/observatory/url/costum.views.custom.university.observatory",
                "icon" : "tachometer",
                "img" : "",
                "useHeader" : false,
                "useFooter" : false,
                "useFilter" : false,
                "inMenu" : false
            },
            "#coform" : {
                "module" : "survey",
                "hash" : "#answer.index.id.604f11d1da83ef25058b456e",
                "icon" : "pencil",
                "useHeader" : "true",
                "isTemplate" : "true",
                "useFooter" : "true",
                "useFilter" : "false",
                "subdomainName" : "coform",
                "staticPage" : "true"
            }
        },
        "htmlConstruct" : {
            "header" : {
                "menuTop" : {
                    "right" : {
                        "buttonList" : {
                            "xsMenu" : {
                                "buttonList" : {
                                    "app" : {
                                        "label" : false,
                                        "icon" : false,
                                        "spanTooltip" : false,
                                        "labelClass" : "padding-left-10",
                                        "buttonList" : {
                                            "#observatory" : true,
                                            "#coform" : true
                                        }
                                    }
                                }
                            },
                            "app" : {
                                "label" : false,
                                "icon" : false,
                                "spanTooltip" : false,
                                "labelClass" : "padding-left-10",
                                "buttonList" : {
                                    "#observatory" : true,
                                    "#coform" : true
                                }
                            },
                            "logout" : true,
                            "login" : true,
                            "userProfil" : {
                                "name" : true,
                                "img" : true
                            }
                        },
                        "addClass" : "margin-top-5"
                    },
                    "left" : {
                        "buttonList" : {
                            "searchBar" : {
                                "construct" : "searchBar",
                                "dropdownResult" : false,
                                "class" : "pull-left margin-top-5 margin-left-15 hidden-xs"
                            }
                        }
                    }
                }
            },
            "menuLeft" : {
                "buttonList" : {
                    "app" : {
                        "label" : false,
                        "icon" : true,
                        "spanTooltip" : true,
                        "labelClass" : "padding-left-10",
                        "buttonList" : {
                            "#filiere" : true,
                            "#live" : true,
                            "#agenda" : true,
                            "#search" : true,
                            "#observatory" : true
                        }
                    },
                    "add" : {
                        "construct" : "createElement",
                        "label" : false,
                        "icon" : "plus-circle",
                        "id" : "show-bottom-add",
                        "class" : "show-bottom-add bg-green text-white",
                        "spanTooltip" : "Add something"
                    }
                },
                "addClass" : "bg-default align-middle"
            },
            "menuBottom" : {
                "addClass" : "fit-content"
            }
        },
        "typeObj" : {
            "organization" : {
                "color" : "black",
                "name" : "Organization",
                "createLabel" : "Orga",
                "icon" : "users",
                "add" : true
            },
            "projects" : {
                "color" : "blue",
                "name" : "Project",
                "createLabel" : "Projet",
                "icon" : "lightbulb-o",
                "add" : true
            },
            "events" : {
                "color" : "turq",
                "name" : "Events",
                "createLabel" : "Evénement",
                "icon" : "calendar-o",
                "add" : true
            }
        }
    },
    "collection" : "organizations",
    "modified" : ISODate("2021-07-07T08:41:44.000Z"),
    "updated" : 1626179702,
    "creator" : "6034fca2da83ef28058b4568",
    "created" : NumberLong(1615184878),
    "slug" : "university",
    "links" : {
        "members" : {
            "6034fca2da83ef28058b4568" : {
                "type" : "citoyens",
                "isAdmin" : true
            },
            "604b31adda83ef98308b4569" : {
                "type" : "organizations",
                "roles" : [ 
                    "Partenaire"
                ]
            },
            "60362295da83ef411f8b457d" : {
                "type" : "citoyens",
                "roles" : [ 
                    "Partenaire"
                ]
            }
        },
        "events" : {
            "60460b3fda83ef601c8b4577" : {
                "type" : "events"
            },
            "60db3fbf5059ce64a119328f" : {
                "type" : "events"
            },
            "60db4019ee24ac29104253ca" : {
                "type" : "events"
            }
        },
        "projects" : {
            "60460d30da83efda2c8b4589" : {
                "type" : "projects"
            },
            "60e5676beb7a4e440d58f339" : {
                "type" : "projects"
            }
        }
    },
    "profilImageUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/Logo-EMIT.png",
    "profilMarkerImageUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/thumb/profil-marker.png",
    "profilMediumImageUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/medium/Logo-EMIT.png",
    "profilThumbImageUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/thumb/profil-resized.png?t=1625132017",
    "profilBannerUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/banner/resized/banner.png?t=1625132989",
    "profilRealBannerUrl" : "/upload/communecter/organizations/6045c3ddda83ef2d058b456a/banner/unnamed.jpg"
}*/




/*
"costum" : {
        "slug" : "university",
        "htmlConstruct" : {
            "header" : {
                "menuTop" : {
                    "left" : {
                        "buttonList" : {
                            "logo" : {
                                "width" : "60",
                                "height" : "50"
                            },
                            "searchBar" : {
                                "construct" : "searchBar",
                                "dropdownResult" : false,
                                "class" : "pull-left margin-top-5 margin-left-15 hidden-xs"
                            }
                        }
                    },
                    "right" : {
                        "buttonList" : {
                            "networkFloop" : true,
                            "notifications" : true,
                            "chat" : true,
                            "login" : true,
                            "userProfil" : {
                                "name" : true,
                                "img" : true
                            },
                            "dropdown" : {
                                "buttonList" : {
                                    "admin" : true,
                                    "logout" : true
                                }
                            }
                        }
                    }
                }
            },
            "menuLeft" : {
                "buttonList" : {
                    "app" : {
                        "label" : false,
                        "icon" : true,
                        "spanTooltip" : true,
                        "labelClass" : "padding-left-10",
                        "buttonList" : {
                            "#search" : true,
                            "#agenda" : true,
                            "#live" : true,
                            "#observatory" : true
                        }
                    },
                    "add" : {
                        "construct" : "createElement",
                        "label" : false,
                        "icon" : "plus-circle",
                        "id" : "show-bottom-add",
                        "class" : "show-bottom-add bg-green text-white",
                        "spanTooltip" : "Add something"
                    }
                },
                "addClass" : "bg-default align-middle"
            }
        },
        "typeObj" : {
            "organization" : {
                "color" : "azure",
                "name" : "Organization",
                "createLabel" : "Orga",
                "icon" : "users",
                "add" : true
            },
            "projects" : {
                "color" : "blue",
                "name" : "Project",
                "createLabel" : "Projet",
                "icon" : "lightbulb-o",
                "add" : true
            },
            "events" : {
                "color" : "turq",
                "name" : "Events",
                "createLabel" : "Evénement",
                "icon" : "calendar-o",
                "add" : true
            }
        },
        "app" : {
            "#observatory" : {
                "isTemplate" : true,
                "staticPage" : true,
                "hash" : "#app.view",
                "icon" : "tachometer",
                "urlExtra" : "/page/observatory/url/costum.views.custom.university.observatory",
                "subdomainName" : "observatory"
            },
            "#live" : {
                "useFilter" : true,
                "icon" : "newspaper-o"
            },
            "#search" : {
                "useFilter" : true,
                "hash" : "#app.search",
                "icon" : "search",
                "urlExtra" : "/page/search",
                "results" : {
                    "renderView" : "directory.elementPanelHtml"
                },
                "filters" : {
                    "types" : [ 
                        "events", 
                        "organizations", 
                        "poi", 
                        "projects", 
                        "classifieds"
                    ]
                },
                "searchObject" : {
                    "indexStep" : "Recherche",
                    "sortBy" : ""
                }
            },
            "#agenda" : {
                "useFilter" : true,
                "hash" : "#app.agenda",
                "icon" : "calendar",
                "urlExtra" : "/page/agenda",
                "subdomainName" : "agenda"
            }
        }
    },*/
</script> 


