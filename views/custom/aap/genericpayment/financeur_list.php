<?php
foreach ($myfinanceurorga as $tl_id => $tl) {
    $tl['id'] = $tl_id;

    // Filtre les éléments de $fundings en fonction de la condition pour obtenir uniquement les enregistrements pour le financeur courant
    $filteredFundings = array_filter($fundings, function($fun) use ($tl) {
        return isset($fun["financeurId"]) && $fun["financeurId"] == (string)$tl["_id"];
    });

    // Calcule la somme des paiements totaux dans les enregistrements filtrés
    $tl['fund_financed'] = array_sum(array_map(function($fun) {
        return $fun["totalFinanced"];
    }, $filteredFundings));

    $tl['fund_payment'] = array_sum(array_map(function($fun) {
        return $fun["totalOwed"];
    }, $filteredFundings));

    $tl['classname'] = \PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper::className([
        'input-option',
        'flex',
    ]);
    $myfinanceurorga[$tl_id] = $tl;

    // Calcule la somme des paiements totaux dans les enregistrements filtrés
    $tl['total'] = array_sum(array_map(function($fun) {
        return $fun["totalOwed"];
    }, $filteredFundings));

    $tl['classname'] = \PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper::className([
        'input-option',
        'flex',
    ]);
    $myfinanceurorga[$tl_id] = $tl;
}
?>
<style>
    :root {
        --aap-primary-ftl-color: #46c6b5;
        --aap-secondary-ftl-color: #4623C9;
    }

    .depensefinancerdropdown {
        text-align: left;
    }

    .mm-dropdown-container {
        height: 350px;
        display: unset;
        padding: unset;
        width: 100%;
        padding: 0 !important;
        display: block;
    }

    .mm-dropdown-container .mm-dropdown {
        width: 100%;
    }

    .mm-dropdown-container .inputfirstsearch {
        height: 45px;
        border: 1px solid rgb(196, 189, 189);
        width: 200px;
        margin-left: 0 !important;
        border-radius: 10px;
        padding: 5px;
    }

    .mm-dropdown-container ul {
        list-style: none;
        min-height: 260px;
        max-height: 390px;
        width: 100% !important;
        margin: 10px 0 0 0 !important;
        padding: 10px;
        overflow-y: scroll;
        border: 1px solid rgb(196, 189, 189);
        border-radius: 5px;
        background-color: #eeeeee;
    }

    .mm-dropdown-container ul li {
        list-style: none;
        height: 40px;
        background-color: #fff;
        margin: 5px;
    }

    .mm-dropdown-container ul li img {
        display: inline-block;
        overflow: hidden;
        width: 40px;
        height: 40px;
        border-radius: 15px;
        vertical-align: middle;
        border: 3px solid #fff;
    }

    .mm-dropdown-container ul li:hover,
    .mm-dropdown-container ul li.active {
        background-color: var(--aap-primary-ftl-color);
        cursor: pointer;
    }

    .input-option.flex {
        display: flex;
        font-family: inherit;
        align-items: center;
        gap: 5px;
        padding: 0px 5px;
    }

    .input-option.flex .tl-name {
        flex: 1;
    }

    .modal-body.cos-payementlist {
        padding: 0 !important;
    }

    .join-tls-invite {
        text-align: center;
    }

    .card-container-fin {
        display: flex;
        flex-wrap: wrap;
        gap: 20px;
        padding: 20px;
        justify-content: center;
    }

    .card-fin {
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 8px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        overflow: hidden;
        transition: transform 0.3s ease, box-shadow 0.3s ease;
    }

    .card-fin:hover {
        transform: translateY(-5px);
        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.2);
    }

    .card-image-fin {
        width: 100%;
        height: 180px;
        object-fit: cover;
    }

    .card-info-fin {
        padding: 15px;
    }

    .card-info-fin h3 {
        margin: 0 0 10px;
        color: #333;
        font-size: 15px !important;
    }

    .card-info-fin p {
        margin: 5px 0;
        color: #666;
        font-size: 12px !important;
    }

    .mm-dropdown-container ul li.card-fin {
        height: auto !important;
    }

    .mm-dropdown-container ul.card-cont-fin {
        background-color: #fff;
        overflow-y: auto;
    }
</style>
<div class="depensefinancerdropdown">
    <h2 class="cart-stepper-title"></h2>
    <p>Paiement de <b><?= $candidatInfo["name"] ?></b></p>
    <?php if (!count($myfinanceurorga)): ?>
        <div class="join-tls-invite">
            <p>Vous n'est pas admin d'un organisation financeur'</p>
        </div>
    <?php else : ?>
        <div class="mm-dropdown-container">
            <div class="mm-dropdown">
                <!--<div class="textfirst"></div>-->
                <div class="inputfirst">
                    <input class="inputfirstsearch" name="inputfirstsearch" placeholder="Filtrer">
                </div>
                <ul class="card-cont-fin">
                    <?php foreach ($myfinanceurorga as $tl_id => $tl) { ?>
                        <li class="<?= $tl['classname'] ?> card-fin" data-id="<?= $tl_id ?>">
                            <img src="<?= $tl['profilImageUrl'] ?>" alt="Photo Organisation" class="card-image-fin">
                            <div class="card-info-fin">
                                <h3><?= $tl['name'] ?></h3>
                                <p><strong>Promesse de financement :</strong> <?= $tl['fund_financed'] ?>€</p>
                                <p><strong>Financement dépensé :</strong> <?= $tl['fund_payment'] ?>€</p>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <input type="hidden" class="option" placeholder="Rechercher" name="namesubmit" value="" />
            </div>
        </div>
    <?php endif; ?>
</div>
<script>
    (function(X, $) {
        var dom,
            php,
            cart_next_dom,
            cart_prev_dom,
            cart,
            first_tl_id,
            camp_proc;

        php = {
            tls: JSON.parse(JSON.stringify(<?= json_encode($myfinanceurorga) ?>)),
            selected: JSON.parse(JSON.stringify(<?= json_encode(null) ?>)),
            can_checkout: JSON.parse(JSON.stringify(<?= json_encode(true) ?>)),
            candidatInfo: JSON.parse(JSON.stringify(<?= json_encode($candidatInfo) ?>))
        };

        cart = X.g_cos_payementlist;
        dom = $('.depensefinancerdropdown');
        cart_next_dom = $('.cos-payementlist-next');
        cart_prev_dom = $('.cos-payementlist-prev');
        camp_proc = cart.campagne_process();
        camp_proc.reset();
        camp_proc.tl_len(Object.keys(php.tls).length);
        $('[data-actualstep]').removeClass('active');
        $('[data-actualstep="tl-list"]').addClass('active');
        $('.cart-stepper-title').text($('[data-actualstep].active').text());
        cart_next_dom.show()
            .prop('disabled', !php.selected);
        cart_prev_dom
            .prop('disabled', true)
            .hide();
        dom.find('.input-option:visible')
            .on('click', function() {
                var self;
                self = $(this);
                dom.find('.input-option').removeClass('active');
                self.addClass('active');
                cart_next_dom.prop('disabled', false)
                    .trigger('click');
            }).each(function() {
            var self;

            self = $(this);
            self.data('name', php.tls[self.data('id')].name);
        });
        dom.find('input.inputfirstsearch').on('input', function() {
            var self, option_dom, search;

            self = $(this);
            option_dom = dom.find('.input-option');
            search = self.val();
            if (!search)
                option_dom.show();
            else {
                search = new RegExp(search, 'ig');
                option_dom.hide();
                option_dom.each(function() {
                    var self;

                    self = $(this);
                    if (self.data('name').match(search))
                        self.show();
                });
            }
        });
        console.log('cart_next_dom.declare',cart_next_dom);
        cart_next_dom.off('click')
            .on('click', function() {
                var self,
                    popup_container_dom;

                self = $(this);
                if (self.prop('disabled'))
                    return;
                camp_proc.tl(dom.find('.input-option.active').data('id'));
                cart_next_dom.prop('disabled', true);
                cart_prev_dom.prop('disabled', true);
                popup_container_dom = $(".co-popup-campcart-menu+div");
                popup_container_dom.removeClass("co-popup-campcart-normal")
                    .addClass("co-popup-campcart-center");
                cart.dom('.modal-body.cos-payementlist')
                    .empty()
                    .html(coInterface.showCostumLoader());
                //22222
                cart.xhr()
                    .camp_proc_commun()
                    .then(function(html) {
                        cart.dom('.modal-body.cos-payementlist')
                            .empty()
                            .html(html);
                    }).catch(function() {
                        cart.xhr()
                            .camp_proc_fin()
                            .then(function(html) {
                                cart.dom('.modal-body.cos-payementlist')
                                    .empty()
                                    .html(html)
                            });
                    }).finally(function() {
                        popup_container_dom.addClass("co-popup-campcart-normal")
                            .removeClass("co-popup-campcart-center")
                    });
            });
        var active_passed = 0;

        $('[data-actualstep]').each(function() {
            var self;

            self = $(this);
            self.off('click');
            if (self.hasClass('active'))
                active_passed = 1;
            if (!active_passed)
                self.addClass('clickable')
                    .on('click', function() {
                        var self;

                        self = $(this);
                        cart.xhr()[self.data('method')]()
                            .then(function(html) {
                                cart.dom('.modal-body.cos-payementlist')
                                    .empty()
                                    .html(html);
                            })
                    });
            else
                self.removeClass('clickable');
        });

        /*
        // rediriger autimatiquement s'il n'y a q'un tiers-lieu
        first_tl_id = Object.keys(php.tls)
        if (typeof customFinObj === 'object' && notEmpty(customFinObj.data.lineToPay) && notEmpty(customFinObj.data.currentOrga.id)) {
            first_tl_id = customFinObj.data.currentOrga.id;
            //111111111
            //dom.find('.input-option[data-id="' + customFinObj.data.currentOrga.id + '"]').trigger('click');
        } else
        if (first_tl_id.length === 1) {
            first_tl_id = first_tl_id[0];
            dom.find('.input-option[data-id="' + first_tl_id + '"]').trigger('click');
        }
        */
        if (php.can_checkout) {
            $(".cart-hide-before-date")
                .removeClass("disable")
                .addClass("enable");
            $(".alert.cart-alert-date").removeClass("show");
        } else {
            $(".cart-hide-before-date")
                .removeClass("enable")
                .addClass("disable");
            $(".alert.cart-alert-date").addClass("show");
        }
    })(window, jQuery);
</script>