<style>
    .modal-body.cos-cart {
        text-align: left;
    }

    .pay-meth-icon {
        display: inline-block;
        width: 50px;
        vertical-align: middle;
        font-size: 4rem;
        margin-right: 5px;
    }

    .pay-method {
        line-height: 2px;
        min-height: 100px;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 15px;
    }

    .pay-meth-desc {
        display: none;
        margin-top: 20px;
    }

    .iban.row .iban {
        font-family: 'Courier New', Courier, monospace;
        text-align: center;
        background-color: #d5d5d5;
        padding: 15px 0;
        font-size: 2rem;
    }
</style>
<h2 class="cart-stepper-title"></h2>
<p>Choisissez un mode de paiement</p>
<div class="row">
    <?php foreach ($paymentMethods as $key => $value) { ?>
        <?php if ($value["type"] == "stripe") { ?>
            <div class="col-md-6 col-xs-12">
                <div class="pay-method btn btn-default">
                    <input type="radio" name="payment-method" hidden value="stripe">
                    <span class="pay-meth-icon">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.87 28.87" id="stripe">
							<g>
								<g>
									<rect width="28.87" height="28.87" rx="6.48" ry="6.48" fill="#6772e5"></rect>
									<path d="M13.3 11.2c0-.69.57-1 1.49-1a9.84 9.84 0 0 1 4.37 1.13V7.24a11.6 11.6 0 0 0-4.36-.8c-3.56 0-5.94 1.86-5.94 5 0 4.86 6.68 4.07 6.68 6.17 0 .81-.71 1.07-1.68 1.07A11.06 11.06 0 0 1 9 17.25v4.19a12.19 12.19 0 0 0 4.8 1c3.65 0 6.17-1.8 6.17-5 .03-5.21-6.67-4.27-6.67-6.24z" fill="#fff" fill-rule="evenodd"></path>
								</g>
							</g>
						</svg>
					</span>
                    Stripe
                </div>
            </div>
        <?php } else if ($value["type"] == "helloasso") { ?>
            <div class="col-md-6 col-xs-12">
                <div class="pay-method helloasso-pay btn btn-default">
                    <input type="radio" name="payment-method" hidden value="helloasso">
                    <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl() ?>/images/aap/helloasso_btn.svg" alt="pay with HelloAsso" width="100%">
                </div>
            </div>
        <?php } else if ($value["type"] == "virement") {
            $config = $value["config"]; ?>
            <div class="col-md-6 col-xs-12">
                <div class="pay-method btn btn-default">
                    <input type="radio" name="payment-method" hidden value="bank">
                    <span class="pay-meth-icon">
						<i class="fa fa-university"></i>
					</span>
                    Virement bancaire
                </div>
            </div>
        <?php } ?>

    <?php } ?>
</div>
<div class="iban row pay-meth-desc" data-target="bank">
    <div class="col-xs-12">Compte pour le virement :</div>
    <div class="col-xs-12 iban">
        <?= $config['iban'] ?? '(Aucun compte de virement pour le moment)' ?>
    </div>
</div>
<div class="helloasso row pay-meth-desc" data-target="helloasso">
    <p class="col-xs-12">
        Le modèle solidaire de HelloAsso garantit que 100% de votre paiement sera versé à l’association choisie.
        Vous pouvez soutenir l’aide qu’ils apportent aux associations en laissant une contribution volontaire à HelloAsso au moment de votre paiement.
    </p>
</div>

<script>
    (function(X, $) {
        var cart,
            php,
            cart_next_dom,
            cart_prev_dom,
            camp_proc,
            active_passed,
            pay_meth_rad_dom;

        function l_disable_btns() {
            cart_next_dom.prop('disabled', true)
                .show();
            cart_prev_dom.prop('disabled', true)
                .show();
        }

        function l_show_loader() {
            var popup_container_dom;

            popup_container_dom = $(".co-popup-campcart-menu+div");
            popup_container_dom.removeClass("co-popup-campcart-normal")
                .addClass("co-popup-campcart-center");
            cart.dom('.modal-body.cos-cart')
                .empty()
                .html(coInterface.showCostumLoader());
        }

        function l_hide_loader() {
            var popup_container_dom;

            popup_container_dom = $(".co-popup-campcart-menu+div");
            popup_container_dom.addClass("co-popup-campcart-normal")
                .removeClass("co-popup-campcart-center");
        }

        php = {};
        cart = X.g_cos_cart;
        cart_next_dom = $('.cos-cart-next');
        cart_prev_dom = $('.cos-cart-prev');
        pay_meth_rad_dom = $('input[type=radio][name="payment-method"]');
        camp_proc = cart.campagne_process();
        camp_proc.payment_method(null);
        cart_next_dom.off('click')
            .on('click', function() {
                var self;

                self = $(this);
                if (self.prop('disabled'))
                    return;
                l_disable_btns();
                l_show_loader();
                cart.xhr()
                    .camp_proc_info()
                    .then(function(html) {
                        cart.dom('.modal-body.cos-cart')
                            .empty()
                            .html(html);
                    }).catch(function() {
                    cart.xhr()
                        .camp_proc_commun()
                        .then(function(html) {
                            cart.dom('.modal-body.cos-cart')
                                .empty()
                                .html(html)
                        })
                }).finally(l_hide_loader);
            });
        cart_prev_dom.off('click').on('click', function() {
            var self;

            self = $(this);
            if (self.prop('disabled'))
                return;
            l_disable_btns();
            l_show_loader();
            cart.xhr()
                .camp_proc_commun()
                .then(function(html) {
                    cart.dom('.modal-body.cos-cart')
                        .empty()
                        .html(html);
                }).finally(l_hide_loader);
        });
        pay_meth_rad_dom.on('change', function() {
            var self;

            self = $(this);
            $(".pay-meth-desc").each(function() {
                var self = $(this);

                if (self.data("target") === pay_meth_rad_dom.filter(":checked").val())
                    self.show();
                else
                    self.hide();
            });
            pay_meth_rad_dom.each(function() {
                var self;

                self = $(this);
                if (self.is(':checked'))
                    self.parents('.pay-method')
                        .removeClass('btn-default')
                        .addClass('btn-primary');
                else
                    self.parents('.pay-method')
                        .addClass('btn-default')
                        .removeClass('btn-primary');
            });
            cart_next_dom.prop('disabled', false);
            camp_proc.payment_method(self.val());
        });
        $('.pay-method').on('click', function(e) {
            var self;

            self = $(this);
            e.stopPropagation();
            if (!$(e.target).is(self.find('input[type=radio]')))
                self.find('input[type=radio]').click();
        });
        l_disable_btns();
        cart_prev_dom.prop('disabled', false);
        $('[data-actualstep]').removeClass('active');
        $('[data-actualstep="payment-method"]').addClass('active');
        $('.cart-stepper-title').text($('[data-actualstep].active').text());
        active_passed = 0;
        $('[data-actualstep]').each(function() {
            var self;

            self = $(this);
            self.off('click');
            if (self.hasClass('active'))
                active_passed = 1;
            if (!active_passed)
                self.addClass('clickable')
                    .on('click', function() {
                        var self;

                        self = $(this);
                        l_show_loader();
                        cart.xhr()[self.data('method')]()
                            .then(function(html) {
                                cart.dom('.modal-body.cos-cart')
                                    .empty()
                                    .html(html);
                            })
                            .finally(l_hide_loader);
                    });
            else
                self.removeClass('clickable');
        });
    })(window, jQuery);
</script>