<style>
	.cart-payment-processing,
	.cart-payment-validation,
	.cart-payment-error,
	.cart-payment-success {
			text-align: center;
	}
</style>
<div class="cart-payment-processing">
	Traitement en cours <i class="fa fa-spinner fa-spin"></i>
</div>
<div class="cart-payment-validation">
	Validation en cours <i class="fa fa-spinner fa-spin"></i>
</div>
<div class="cart-payment-error">
	<div class="alert alert-danger" role="alert">Une erreur est survenue lors de la validation de votre paiement.<br>Veuillez recommencer, ou si le problème persiste contactez un administrateur.</div>
</div>
<div class="cart-payment-success">
	<h4>Paiement effectué <i class="fa fa-check"></i></h4>
<!--	<a href="--><?php //= Yii::app()->baseUrl . '/co2/aap/camp-tl-bill/tl/' . $link['tl'] . '/bill/' . $link['bill'] ?><!--" class="btn btn-default">-->
<!--		<i class="fa fa-download"></i>-->
<!--		Télécharger la facture-->
<!--	</a>-->
</div>
<script>
	(function(X, $) {
		var cart,
			cart_next_dom,
			cart_prev_dom,
			wsCO,
			container_dom,
			camp_proc,
			processing_dom,
			validation_dom,
			success_dom,
			error_dom,
			php;

		php = {
			bill: JSON.parse(JSON.stringify(<?= json_encode($link["bill"]) ?>)),
		}
		cart = X.g_cos_cart;
		cart_next_dom = $('.cos-cart-next');
		cart_prev_dom = $('.cos-cart-prev');
		container_dom = $('.cart-payment-processing');
		processing_dom = container_dom;
		validation_dom = $('.cart-payment-validation');
		success_dom = $('.cart-payment-success');
		error_dom = $('.cart-payment-error');
		camp_proc = cart.campagne_process();
		cart_next_dom.hide();
		cart_prev_dom.hide();
		if (X.wsCO && X.wsCO.connected)
			wsCO = X.wsCO;
		else if (X.io)
			wsCO = X.io.connect(X.coWsConfig.serverUrl);
		$('[data-actualstep]').removeClass('active');
		$('[data-actualstep="payment"]').addClass('active');
		$('.cart-stepper-title').text($('[data-actualstep].active').text());
		cart_prev_dom.off('click')
			.on('click', function() {
				var self;

				self = $(this);
				if (self.prop('disabled'))
					return;
				camp_proc.tl(dom.find('.input-option.active').data('id'));
				cart.dom('.modal-body.cos-cart')
					.empty()
					.html(coInterface.showCostumLoader());
				cart.xhr()
					.camp_proc_commun()
					.then(function(html) {
						cart.dom('.modal-body.cos-cart')
							.empty()
							.html(html);
					});
			});
		var active_passed = 0;

		$('[data-actualstep]').each(function() {
			var self;

			self = $(this);
			self.off('click');
			if (self.hasClass('active'))
				active_passed = 1;
			if (!active_passed)
				self.addClass('clickable')
				.on('click', function() {
					var self;

					self = $(this);
					cart.xhr()[self.data('method')]()
						.then(function(html) {
							cart.dom('.modal-body.cos-cart')
								.empty()
								.html(html);
						})
				});
			else
				self.removeClass('clickable');
		});
		validation_dom.detach();
		success_dom.detach();
		error_dom.detach();
		function success_payment() {
			var aap = new aapv2();
			
			if (typeof X.popup !== "undefined" && typeof X.popup.close === "function") {
				X.popup.close();
				delete X.popup;
			}
			container_dom.empty()
			             .append(success_dom);
			X.urlCtrl.loadByHash(location.hash);
			camp_proc.reset();
			refresh_camp_count();
			$.each(camp_proc.communs(), function(_, commun) {
				var need_refresh_dom;
				
				need_refresh_dom = $("[data-need-refreshblock]");
				if (commun.ans === aap.url().dot("communId")) {
					if (need_refresh_dom.length > 0) {
						need_refresh_dom.each(function() {
							var id = $(this).attr("data-need-refreshblock")
							if (typeof refreshBlock === "function")
								setTimeout(refreshBlock.bind(X, id, ".cmsbuilder-block[data-id='" + id + "']"), 500);
						})
					}
				}
			});
		}
		
		function error_payment(data) {
				if (typeof X.popup !== "undefined" && typeof X.popup.close === "function") {
					X.popup.close();
					delete X.popup;
				}
				if (data)
					error_dom.text(JSON.stringify(data));
				container_dom.empty()
				             .append(error_dom);
		}
		if (wsCO) {
			wsCO.off('credit-card-pay-success')
			    .off("credit-card-payment-error")
				  .on('credit-card-pay-success', success_payment)
				  .on('credit-card-pay-error', error_payment);
		}
		$(".co-popup-campcart-menu+div").removeClass("co-popup-campcart-normal")
			.addClass("co-popup-campcart-center");
		
		function checkout_check() {
			setTimeout(() => {
				const url = baseUrl + "/co2/aap/camp-tl-check-status/bill/" + php.bill;
				const post = {
					communs: camp_proc.communs()
				}
				
				ajaxPost(null, url, post, response => {
					if (response.success) {
						if (response.content === "paid")
							success_payment();
						else
							checkout_check();
					} else
						error_payment();
				}, error_payment);
			}, 5000);
		}
		checkout_check();
	})(window, jQuery);
</script>