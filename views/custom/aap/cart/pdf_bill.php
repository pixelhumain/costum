<?php
$total = array_reduce($lines, fn($prev, $curr) => ($prev + $curr['amount']), 0);
// $double = array_reduce($lines, fn($prev, $curr) => ($prev + $curr['double']), 0);
?>
<style>
	table.bordered,
	.bordered th,
	.bordered thead, 
	.bordered tfoot {
		border-top: 1px solid black;
		border-bottom: 1px solid black;
		border-collapse: collapse;
		padding: 6px 3px;
		color: #073B4C;
	}

	.designation-field {
		width: 550px;
	}

	.amount-field {
		width: 100px;
		text-align: right;
	}

	.tl-image img {
		width: 100%;
		height: auto;
	}
	.bordered tbody {
		opacity: .8;
	}
</style>
<table>
	<tr>
		<td style="width: 350px;"></td>
		<td style="text-align: right;">
			<?php if (!empty($image)) : ?>
				<img style="height: 100px;" src="<?= $image ?>" alt="<?= $name ?>">
			<?php endif; ?>
		</td>
	</tr>
</table>
<div class="owner-info" style="color:#073B4C;font-size : 12px;">
<table style="padding: 5px 0;">
	<tr>
		<td><strong>FACTURE N° <?= sprintf('%08d', $number) ?></strong></td>
	</tr>
	<tr>
		<td>Date: <?= $created_at ?> </td>
	</tr>
	<tr>
		<td>Tiers-lieu: <?= $name ?>  </td>
	</tr>
	<tr>
		<td>Adresse : <?= $address ?>  </td>
	</tr>
</table>
</div>
<table class="bordered" style="font-size : 12px;">
	<thead>
		<tr style="background-color: #d8f4f0;">
			<td class="designation-field"><b>Ligne de dépense</b></td>
			<td class="amount-field"><b>Montant</b></td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($lines as $line): ?>
			<tr>
				<td class="designation-field"><?= $line['line'] ?></td>
				<td class="amount-field"><?= $line['amount'] ?> <i class="fa fa-eur"></i></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr style="background-color: #d8f4f0;">
			<td class="designation-field"><b>Total</b></td>
			<td class="amount-field"><b><?= $total ?></b></td>
		</tr>
	</tfoot>
</table>