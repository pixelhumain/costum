<?php
	$tls = $tls ?? [];
	$orgas = $orgas ?? [];
	$selected = $selected ?? null;
	$tls_view = array_filter($tls, fn($t) => $t['fund_number']);
	$orgas_view = array_filter($orgas, fn($o) => $o['fund_number']);
	function prepare_data(&$organizations, $selected = null) {
		foreach ($organizations as $id => &$orga) {
			$orga['id'] = $id;
			$orga['classname'] = \PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper::className([
				'input-option',
				'flex',
				'active' => !empty($selected) && $selected === $id,
			]);
		}
	}
	prepare_data($tls_view, $selected);
	prepare_data($orgas_view, $selected);
	HtmlHelper::registerCssAndScriptsFiles([
		'/js/aap/jointl.js',
	], Yii::app()->getModule('costum')->getAssetsUrl());
?>
<style>
	:root {
      --aap-primary-ftl-color: #46c6b5;
      --aap-secondary-ftl-color: #4623C9;
  }

  .depensefinancerdropdown {
      text-align: left;
  }

  .mm-dropdown-container {
      height: 350px;
      display: unset;
      padding: unset;
      width: 100%;
      padding: 0 !important;
      display: block;
  }

  .mm-dropdown-container .mm-dropdown {
      width: 100%;
  }

  .mm-dropdown-container .tl-list ul {
      list-style: none;
      width: 100% !important;
      height: unset !important;
      margin: 10px 0 0 0 !important;
		  min-height: unset !important;
      padding: 10px;
      overflow-y: auto !important;
      border: 1px solid rgb(196, 189, 189);
      border-radius: 5px;
      background-color: #eeeeee;
  }

  .mm-dropdown-container .tl-list ul li {
      list-style: none;
      background-color: #fff;
      margin: 5px;
  }

  .mm-dropdown-container .tl-list ul li img {
      display: inline-block;
      overflow: hidden;
      width: 40px;
      height: 40px;
      border-radius: 15px;
      vertical-align: middle;
      border: 3px solid #fff;
  }

  .mm-dropdown-container .tl-list ul li:hover,
  .mm-dropdown-container .tl-list ul li.active {
      background-color: var(--aap-primary-ftl-color);
      cursor: pointer;
  }

  .input-option.flex {
      display: flex;
      font-family: inherit;
      align-items: center;
      gap: 5px;
      padding: 0px 5px;
  }

  .input-option.flex .tl-name {
      flex: 1;
  }

  .modal-body.cos-cart {
      padding: 0 !important;
  }

  .join-tls-invite {
      text-align: center;
  }
</style>
<div class="depensefinancerdropdown">
	<h2 class="cart-stepper-title"></h2>
	<p>Vos tiers-lieux et/ou organisations avec des promesses en attente de validation</p>
	<?php if (!count($tls)): ?>
		<div class="join-tls-invite">
			<p>Vous ne faites partie d'aucun tiers-lieu pour le moment. Cliquez sur le bouton ci-dessous pour rejoindre un tiers-lieu et devenir administrateur.</p>
			<button class="btn btn-default">Rejoindre un Tiers-lieu</button>
		</div>
	<?php else : ?>
		<div class="mm-dropdown-container tl-list">
			<div class="mm-dropdown tl-list">
				<ul>
					<?php foreach ($tls_view as $id => $tl) { ?>
						<li class="<?= $tl['classname'] ?>" data-id="<?= $id ?>" data-group="tls">
							<img src="<?= $tl['profilImageUrl'] ?>" alt="" width="20" height="20"/>
							<span class="tl-name">
								<?= $tl['name'] ?>
							</span>
							<label class="badge">
								<?= $tl['fund_number'] ?>
							</label>
						</li>
					<?php } ?>
					<?php foreach ($orgas_view as $id => $orga) { ?>
						<li class="<?= $orga['classname'] ?>" data-id="<?= $id ?>" data-group="orgas">
							<img src="<?= $orga['profilImageUrl'] ?>" alt="" width="20" height="20"/>
							<span class="tl-name">
								<?= $orga['name'] ?>
							</span>
							<label class="badge">
								<?= $orga['fund_number'] ?>
							</label>
						</li>
					<?php } ?>
				</ul>
				<input type="hidden" class="option" placeholder="Rechercher" name="namesubmit" value=""/>
			</div>
		</div>
	<?php endif; ?>
</div>
<script>
	(function(X, $){
		var dom,
			php,
			cart_next_dom,
			cart_prev_dom,
			cart,
			first_tl_id,
			camp_proc;
		
		php = {
			tls : JSON.parse(JSON.stringify(<?= json_encode($tls_view) ?>)),
			orgas : JSON.parse(JSON.stringify(<?= json_encode($orgas_view) ?>)),
			selected : JSON.parse(JSON.stringify(<?= json_encode($selected ?? null) ?>)),
			can_checkout : JSON.parse(JSON.stringify(<?= json_encode($can_checkout ?? false) ?>)),
		};
		cart = X.g_cos_cart;
		dom = $('.depensefinancerdropdown');
		cart_next_dom = $('.cos-cart-next');
		cart_prev_dom = $('.cos-cart-prev');
		camp_proc = cart.campagne_process();
		camp_proc.reset();
		camp_proc.tl_len(Object.keys(php.tls).length + Object.keys(php.orgas).length);
		$('[data-actualstep]').removeClass('active');
		$('[data-actualstep="tl-list"]').addClass('active');
		$('.cart-stepper-title').text($('[data-actualstep].active').text());
		cart_next_dom.show()
		             .prop('disabled', !php.selected);
		cart_prev_dom
			.prop('disabled', true)
			.hide();
		dom.find('.input-option:visible')
		   .on('click', function(){
			   var self;
			   
			   self = $(this);
			   dom.find('.input-option').removeClass('active');
			   self.addClass('active');
			   cart_next_dom.prop('disabled', false)
			                .trigger('click');
		   }).each(function(){
			var self;
			
			self = $(this);
			self.data('name', php[self.data("group")][self.data('id')].name);
		});
		cart_next_dom.off('click')
		             .on('click', function(){
			             var self,
				             popup_container_dom;
			             
			             self = $(this);
			             if (self.prop('disabled'))
				             return;
			             camp_proc.tl(dom.find('.input-option.active').data('id'));
			             cart_next_dom.prop('disabled', true);
			             cart_prev_dom.prop('disabled', true);
			             popup_container_dom = $(".co-popup-campcart-menu+div");
			             popup_container_dom.removeClass("co-popup-campcart-normal")
			                                .addClass("co-popup-campcart-center");
			             cart.dom('.modal-body.cos-cart')
			                 .empty()
			                 .html(coInterface.showCostumLoader());
			             cart.xhr()
			                 .camp_proc_commun()
			                 .then(function(html){
				                 cart.dom('.modal-body.cos-cart')
				                     .empty()
				                     .html(html);
			                 }).catch(function(){
				             cart.xhr()
				                 .camp_proc_tl()
				                 .then(function(html){
					                 cart.dom('.modal-body.cos-cart')
					                     .empty()
					                     .html(html)
				                 });
			             }).finally(function(){
				             popup_container_dom.addClass("co-popup-campcart-normal")
				                                .removeClass("co-popup-campcart-center")
			             });
		             });
		$('.join-tls-invite .btn').on('click', function(){
			join_tl();
			$(".close-campcart").trigger("click");
		})
		var active_passed = 0;
		
		$('[data-actualstep]').each(function(){
			var self;
			
			self = $(this);
			self.off('click');
			if (self.hasClass('active'))
				active_passed = 1;
			if (!active_passed)
				self.addClass('clickable')
				    .on('click', function(){
					    var self;
					    
					    self = $(this);
					    cart.xhr()[self.data('method')]()
						    .then(function(html){
							    cart.dom('.modal-body.cos-cart')
							        .empty()
							        .html(html);
						    })
				    });
			else
				self.removeClass('clickable');
		});
		
		// rediriger autimatiquement s'il n'y a q'un tiers-lieu
		first_tl_id = Object.keys(php.tls)
		if (typeof customFinObj === 'object' && notEmpty(customFinObj.data.lineToPay) && notEmpty(customFinObj.data.currentOrga.id)) {
			first_tl_id = customFinObj.data.currentOrga.id;
			// dom.find('.input-option[data-id="' + first_tl_id + '"]').trigger('click');
		} else if (first_tl_id.length === 1) {
			first_tl_id = first_tl_id[0];
			// dom.find('.input-option[data-id="' + first_tl_id + '"]').trigger('click');
		}
		if (php.can_checkout) {
			$(".cart-hide-before-date")
				.removeClass("disable")
				.addClass("enable");
			$(".alert.cart-alert-date").removeClass("show");
		} else {
			$(".cart-hide-before-date")
				.removeClass("enable")
				.addClass("disable");
			$(".alert.cart-alert-date").addClass("show");
		}
	})(window, jQuery);
</script>