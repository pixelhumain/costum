<style>
	.cap-cart-info {
		text-align: left;
	}

	.help-block {
		display: none;
	}
</style>
<div class="cap-cart-info">
	<h2 class="cart-stepper-title"></h2>
	<p>Ces informations serviront à la facturation et vous l'envoyer par l'adresse mail ci-dessous</p>
	<form id="cart-info-form">
		<div class="form-group">
			<label for="cart-info-email">Adresse mail</label>
			<input type="email" class="form-control" id="cart-info-email" placeholder="Mail" required pattern="^[a-zA-Z0-9._%+\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,}$">
			<span class="help-block"></span>
		</div>
		<div class="form-group">
			<label for="cart-info-name">Nom du tiers-lieu</label>
			<input type="text" class="form-control" id="cart-info-name" placeholder="Nom du tiers-lieu" required>
			<span class="help-block"></span>
		</div>
		<div class="form-group">
			<label for="cart-info-address">Adresse</label>
			<input type="text" class="form-control" id="cart-info-address" placeholder="Adresse" required>
			<span class="help-block"></span>
		</div>
	</form>
</div>
<script>
	(function(X, $) {
		var cart,
			cart_next_dom,
			cart_prev_dom,
			form,
			php,
			camp_proc,
			popup;

		function l_external_checkout(resp) {
			var left,
				top,
				width,
				height,
				popup_param;

			if (resp.bill_id)
				camp_proc.info({
					id: resp.bill_id
				});
			width = 500;
			height = 800;
			left = (X.outerWidth / 2) - (width / 2);
			top = (X.outerHeight / 2) - (height / 2);
			popup_param = "popup,width=" + width + ",height=" + height + ",left=" + left + ",top=" + top + ",scrollbars=yes,resizable=no,toolbar=no,alwaysRaised=yes";
			X.popup = X.open(resp.url, "_blank");
			if (!X.popup)
				alert("Votre navigateur a bloqué le module de paiement.\nVeuillez autoriser l'ouverture de fenêtre flottante dans le paramètre de votre navigateur et ensuite revenir à cette étape.")
			cart.dom('.modal-body.cos-cart')
				.empty()
				.html(resp.render);
		}

		function l_show_loader() {
			var popup_container_dom;

			popup_container_dom = $(".co-popup-campcart-menu+div");
			popup_container_dom.removeClass("co-popup-campcart-normal")
				.addClass("co-popup-campcart-center");
			cart.dom('.modal-body.cos-cart')
				.empty()
				.html(coInterface.showCostumLoader());
		}

		function l_hide_loader() {
			var popup_container_dom;

			popup_container_dom = $(".co-popup-campcart-menu+div");
			popup_container_dom.addClass("co-popup-campcart-normal")
				.removeClass("co-popup-campcart-center");
		}

		function l_catch() {
			cart.xhr()
				.camp_proc_info()
				.then(function(html) {
					cart.dom('.modal-body.cos-cart')
						.empty()
						.html(html);
				}).finally(l_hide_loader);
		}
		cart = X.g_cos_cart;
		cart_next_dom = $('.cos-cart-next');
		cart_prev_dom = $('.cos-cart-prev');
		form = {
			email: $('#cart-info-email'),
			name: $('#cart-info-name'),
			address: $('#cart-info-address')
		};
		php = {
			tl: JSON.parse(JSON.stringify(<?= json_encode($tl) ?>))
		}
		camp_proc = cart.campagne_process();
		// événement
		cart_prev_dom.off('click').on('click', function() {
			var self;

			self = $(this);
			if (self.prop('disabled'))
				return;
			cart_next_dom.prop('disabled', true);
			cart_prev_dom.prop('disabled', true);
			l_show_loader();
			cart.xhr()
				.camp_proc_payment_method()
				.then(function(html) {
					cart.dom('.modal-body.cos-cart')
						.empty()
						.html(html);
				}).finally(l_hide_loader);
		});
		cart_next_dom.off('click').on('click', function() {
			var self;

			self = $(this);
			if (self.prop('disabled'))
				return;
			$('#cart-info-form').trigger('submit');
		});
		$.each(form, function(_, e) {
			e.on('input', function() {
				var self;

				self = $(this);
				self.next('.help-block').hide();
			})
		});
		$('#cart-info-form').on('submit', function(e) {
			var valid;

			e.preventDefault();
			valid = true;
			$.each(form, function(_, e) {
				if (!e.get(0).checkValidity()) {
					e.next('.help-block')
						.text(e.get(0).validationMessage)
						.show();
					valid = false;
				}
			});
			if (valid) {
				camp_proc.info({
					email: form.email.val(),
					name: form.name.val(),
					address: form.address.val()
				});
				cart_next_dom.prop('disabled', true);
				cart_prev_dom.prop('disabled', true);
				l_show_loader();
				switch (camp_proc.payment_method()) {
					case 'stripe':
						cart.xhr()
							.camp_proc_stripe_pay()
							.then(l_external_checkout)
							.catch(l_catch);
						break;
					case 'helloasso':
						cart.xhr()
							.camp_proc_helloasso_pay()
							.then(l_external_checkout)
							.catch(l_catch);
						break;
					case 'bank':
						cart.xhr()
							.camp_proc_bank_pay()
							.then(function(resp) {
								cart.dom('.modal-body.cos-cart')
									.empty()
									.html(resp);
							}).catch(l_catch);
						break;
				}
			}
		});
		// actions
		$.each([cart_next_dom, cart_prev_dom], function(_, e) {
			e.show().prop('disabled', false);
		});
		$('[data-actualstep]').removeClass('active');
		$('[data-actualstep="bill-info"]').addClass('active');
		$('.cart-stepper-title').text($('[data-actualstep].active').text());
		$.each(php.tl, function(key, e) {
			form[key].val(e);
		});
		var active_passed = 0;

		$('[data-actualstep]').each(function() {
			var self;

			self = $(this);
			self.off('click');
			if (self.hasClass('active'))
				active_passed = 1;
			if (!active_passed)
				self.addClass('clickable')
				.on('click', function() {
					var self;

					self = $(this);
					l_show_loader();
					cart.xhr()[self.data('method')]()
						.then(function(html) {
							cart.dom('.modal-body.cos-cart')
								.empty()
								.html(html);
						})
						.finally(l_hide_loader);
				});
			else
				self.removeClass('clickable');
		});
	})(window, jQuery)
</script>