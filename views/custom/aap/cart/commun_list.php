<?php
$communs = !empty($fundings['funds']) && is_array($fundings['funds']) ? end($fundings['funds']) : [];
$campagne_date = $camp_conf["panierdate"] ?? null;
if (!empty($campagne_date))
	$campagne_date = $campagne_date
		->toDateTime()
		->setTimeZone(new DateTimeZone(
			Yii::app()->session["timezone"] ?? "UTC"
		))->format("d/m/Y H:i");
if (empty($communs['communs'])):
?>
	Le panier est vide
<?php else : ?>
	<style>
		.modal-body.cos-cart {
			text-align: left;
		}

		.radio-item label {
			display: block;
			padding: 18px 20px;
			background: #e2eaf2;
			border-radius: 10px;
			cursor: pointer;
			font-size: 12px;
			font-weight: 400;
			min-width: 220px;
			white-space: nowrap;
			position: relative;
			transition: 0.4s ease-in-out 0s;
		}

		.radio-item label .lnlist small {
			font-size: 14px;
			color: <?= !empty($camp_conf["campColor"]) ? $camp_conf["campColor"] : "inherit" ?> !important;
		}

		.radio-item label .lnlist {
			display: flex;
			justify-content: space-around;
			align-items: center;
		}

		.radio-item [type="checkbox"] {
			display: none;
		}

		.cart-commun-container {
			overflow-y: auto;
			margin-bottom: 1rem;
		}

		@media (min-width: 720px) {
			.h5action {
				flex: 1;
				white-space: initial;
				width: unset !important;
				display: block !important;
			}
		}

		.cos-cart-amount {
			padding: 10px;
			border: 1px solid #43c9b7;
			border-radius: 10px;
			position: relative;
			box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
			border-left: 8px solid #43c9b7;
			margin-bottom: 10px;
		}

		.cos-cart-amount .icon-montant {
			position: absolute;
			display: flex;
			align-items: center;
			justify-content: center;
			font-size: 27px;
			width: 45px;
			height: 45px;
			border-radius: 50%;
			background-color: #e2eaf2;
			color: #43c9b7;
			right: 20px;
			top: 8px;
		}

		.cos-cart-amount .sub-total {
			margin-top: 5px;
			opacity: .9;
			font-size: 20px;
			font-weight: 400;
			margin-left: 5px;
		}

		.cos-cart-amount .total {
			margin-top: 5px;
			font-size: 25px;
			font-weight: 600;
			margin-left: 5px;
		}

		.cos-cart-amount .sub-total i {
			font-weight: 400;
			color: #43c9b7;
			font-size: 16px;
		}

		.cos-cart-amount .total i {
			font-weight: 400;
			color: #43c9b7;
			font-size: 21px;
		}

		.cos-cart-amount .sub-total .text,
		.cos-cart-amount .total .text {
			font-weight: 600;
			color: #43c9b7;
		}

		.cart-commun-action {
			display: flex;
			position: sticky;
			top: 10px;
			z-index: 1;
			justify-content: flex-end;
		}

		.btn.btn-outline-danger {
			color: #d9534f;
			background-color: #fff;
			border-color: #d9534f;
		}

		.alert.cart-alert-date {
			display: none;
		}

		.alert.cart-alert-date.show {
			display: block;
		}
	</style>
	<h2 class="cart-stepper-title"></h2>
	<p>Listes des promesses à valider pour <b><?= $tl['name'] ?></b></p>
	<div class="alert alert-warning cart-alert-date" role="alert">
		<strong>Rappel :</strong>
		<span>Vous ne pouvez pas procéder au paiement qu'après <?= $campagne_date ?>. Jusque là, vous pouvez consuler la liste de votre financement ou en enlever certains.</span>
	</div>
	<p class="cart-commun-action">
		<button class="btn btn-outline-danger delete-action"><i class="fa fa-trash"></i> Supprimer la sélection</button>
	</p>
	<div class="cart-commun-container cs-scroll scroll-small"></div>
	<div class="cos-cart-amount">
		<span class="icon-montant"><i class="fa fa-credit-card"></i></span>
		<!-- <div class="sub-total">Sous-total : <span class="text"></span> <i class="fa fa-eur"></i></div> -->
		<!-- <div class="double">Doublonage FTL : <span class="text"></span> <i class="fa fa-eur"></i></div> -->
		<div class="total">Total : <span class="text"></span> <i class="fa fa-eur"></i></div>
	</div>
	<div class="radio-item dom-template">
		<label>
			<input class="inputpreconfig" type="checkbox">
			<span for="lineindex" class="lnlist">
				<h5 class="h5action"> post </h5>
				<h5 class="pull-right">
					<span class="cos-cart-finance"></span> <i class="fa fa-eur"></i>
				</h5>
			</span>
		</label>
	</div>
<?php endif; ?>
<script>
	(function(X, $) {
		var cart,
			php,
			cart_next_dom,
			cart_prev_dom,
			line_template_dom,
			container_dom,
			resume_dom,
			funds,
			total_amount,
			camp_proc,
			action_delete_dom;

		function l_show_loader() {
			var popup_container_dom;

			popup_container_dom = $(".co-popup-campcart-menu+div");
			popup_container_dom.removeClass("co-popup-campcart-normal")
				.addClass("co-popup-campcart-center");
			cart.dom('.modal-body.cos-cart')
				.empty()
				.html(coInterface.showCostumLoader());
		}

		function l_hide_loader() {
			var popup_container_dom;

			popup_container_dom = $(".co-popup-campcart-menu+div");
			popup_container_dom.addClass("co-popup-campcart-normal")
				.removeClass("co-popup-campcart-center");
		}

		php = {
			fundings: JSON.parse(JSON.stringify(<?= json_encode($fundings) ?>)),
			tl: JSON.parse(JSON.stringify(<?= json_encode($tl) ?>)),
			unique_tl: JSON.parse(JSON.stringify(<?= json_encode($unique_tl) ?>)),
			camp_conf: JSON.parse(JSON.stringify(<?= json_encode($camp_conf) ?>)),
			can_checkout: JSON.parse(JSON.stringify(<?= json_encode($can_checkout) ?>)),
		};
		cart = X.g_cos_cart;
		line_template_dom = $('.radio-item.dom-template');
		container_dom = $('.cart-commun-container');
		resume_dom = $('.cos-cart-amount');
		action_delete_dom = $('.cart-commun-action .delete-action');
		line_template_dom.detach();
		cart_next_dom = $('.cos-cart-next');
		cart_prev_dom = $('.cos-cart-prev');
		camp_proc = cart.campagne_process();
		cart_prev_dom.off('click')
			.on('click', function() {
				var self;

				self = $(this);
				if (self.prop("disabled"))
					return;
				l_show_loader();
				cart.xhr()
					.camp_proc_tl()
					.then(function(html) {
						cart.dom('.modal-body.cos-cart')
							.empty()
							.html(html);
					}).finally(l_hide_loader);
			});
		$('[data-actualstep]').removeClass('active');
		$('[data-actualstep="commun-list"]').addClass('active');
		$('.cart-stepper-title').text($('[data-actualstep].active').text());
		funds = php.fundings.funds.shift();
		container_dom.empty().html('');
		if (funds && funds.communs.length) {
			var commun_dom;

			$.each(funds.communs, function(_, commun) {
				var commun_dom,
					total,
					title_html,
					doubles;

				commun_dom = line_template_dom.clone();
				commun_dom.removeClass('dom-template');
				total = commun.depense.reduce(function(reduce, current) {
					return (
						reduce + current.financer.reduce(function(reduce, current) {
							return (reduce + current.amount);
						}, 0)
					);
				}, 0);
				doubles = commun.depense.reduce(function(reduce, current) {
					return (reduce + current.financer.reduce(function(reduce, current) {
						return (reduce + current.double);
					}, 0))
				}, 0);
				title_html = commun.titre;
				if (doubles)
					title_html += "<br><small>Doublonnage france tiers-lieu : + " + doubles + " <i class=\"fa fa-eur\"></i></small>";
				commun_dom.find('h5.h5action').empty().html(title_html);
				commun_dom.find('span.cos-cart-finance')
					.text(total);
				commun_dom.find('input.inputpreconfig')
					.data('amount', total)
					.data('commun-id', commun.id)
					.data('commun-name', commun.titre)
					.data('tl', funds.id);
				container_dom.append(commun_dom);
			});
		}
		container_dom.find('input.inputpreconfig').on('change', function() {
			var remaining_double,
				self,
				checked_dom,
				sub_total,
				sub_half;

			self = $(this);
			total_amount = 0;
			checked_dom = container_dom.find('input.inputpreconfig:checked');
			camp_proc.reset_communs();
			checked_dom.each(function() {
				var self,
					amount;

				self = $(this);
				amount = self.data('amount');
				total_amount += amount;
				camp_proc.communs({
					ans: self.data('commun-id'),
					name: self.data('commun-name'),
					amount: amount,
					tl: self.data('tl'),
					tlname: php.tl.name,
				});
			});
			camp_proc.amount(total_amount);
			cart_next_dom.prop('disabled', checked_dom.length === 0 || !php.can_checkout);
			action_delete_dom.prop('disabled', checked_dom.length === 0);
			resume_dom.find('.total .text').text(total_amount);
		});
		resume_dom.find('.sub-total, .double, .total').each(function() {
			var self;

			self = $(this);
			self.find('.text')
				.text('0');
		});
		cart_next_dom.off('click')
			.on('click', function() {
				var self;

				self = $(this);
				if (self.prop('disabled'))
					return;
				cart_next_dom.prop('disabled', true);
				cart_prev_dom.prop('disabled', true);
				l_show_loader();
				cart.xhr()
					.camp_proc_payment_method()
					.then(function(html) {
						cart.dom('.modal-body.cos-cart')
							.empty()
							.html(html);
					}).catch(function() {
						cart.xhr()
							.camp_proc_commun()
							.then(function(html) {
								cart.dom('.modal-body.cos-cart')
									.empty()
									.html(html)
							})
					}).finally(l_hide_loader);
			});
		cart_prev_dom.off('click')
			.on('click', function() {
				var self;

				self = $(this);
				if (self.prop('disabled'))
					return;
				cart_next_dom.prop('disabled', true);
				cart_prev_dom.prop('disabled', true);
				l_show_loader();
				cart.xhr()
					.camp_proc_tl()
					.then(function(html) {
						cart.dom('.modal-body.cos-cart')
							.empty()
							.html(html);
					});
			});
		action_delete_dom.on('click', function() {
			var self,
				aap;

			aap = new aapv2();
			self = $(this);
			if (self.prop('disabled'))
				return;
			$.confirm({
				title: "Supprimer les financements",
				content: "Cette action supprimera tous les financements sélectionnés et ce sera irreversible.",
				buttons: {
					yes: {
						text: 'Supprimer',
						btnClass: 'btn btn-danger',
						action: function() {
							action_delete_dom.prop("disabled", true);
							cart_next_dom.prop("disabled", true);
							l_show_loader();
							cart.xhr()
								.camp_del()
								.then(function() {
									// reload context
									cart.xhr()
										.camp_proc_commun()
										.then(function(html) {
											cart.dom('.modal-body.cos-cart')
												.empty()
												.html(html);
										});
									// refresh cart count
									refresh_camp_count();
									$.each(camp_proc.communs(), function(_, commun) {
										var need_refresh_dom;

										need_refresh_dom = $("[data-need-refreshblock]");
										if (commun.ans === aap.url().dot("communId")) {
											if (need_refresh_dom.length > 0) {""
												need_refresh_dom.each(function() {
													var id = $(this).attr("data-need-refreshblock");
													if (typeof refreshBlock === "function")
														setTimeout(refreshBlock.bind(X, id, ".cmsbuilder-block[data-id='" + id + "']"), 500);
												})
											}
										}
									});
								}).finally(l_hide_loader);
						}
					},
					no: {
						text: coTranslate('cancel'),
						btnClass: 'btn btn-default'
					}
				}
			});
		});
		var active_passed = 0;

		$('[data-actualstep]').each(function() {
			var self;

			self = $(this);
			self.off('click');
			if (self.hasClass('active'))
				active_passed = 1;
			if (!active_passed)
				self.addClass('clickable')
				.on('click', function() {
					var self;

					self = $(this);
					l_show_loader()
					cart.xhr()[self.data('method')]()
						.then(function(html) {
							cart.dom('.modal-body.cos-cart')
								.empty()
								.html(html);
						})
						.finally(l_hide_loader);
				});
			else
				self.removeClass('clickable');
		});
		camp_proc.tl(php.tl.id);
		action_delete_dom.prop('disabled', true);
		cart_next_dom.show()
			.prop('disabled', true);
		cart_prev_dom.prop('disabled', false)
			.show();
		if (camp_proc.tl_len() <= 1)
			cart_prev_dom.hide();

		// présélectionner les communs à financer
		if (typeof customFinObj === 'object' && notEmpty(customFinObj.data.lineToPay) && notEmpty(customFinObj.data.final.titre)) {
			$.when(
					$('.co-popup-campcart .inputpreconfig').filter(function() {
						return $(this).data('communName') == customFinObj.data.final.titre;
					}).trigger('click'))
				.then($('.co-popup-campcart .cos-cart-next').trigger('click'));
			setTimeout(function() {
				customFinObj.data.lineToPay = {}
			}, 10);
		}
		if (php.can_checkout) {
			$(".cart-hide-before-date")
				.removeClass("disable")
				.addClass("enable");
			$(".alert.cart-alert-date").removeClass("show");
		} else {
			$(".cart-hide-before-date")
				.removeClass("enable")
				.addClass("disable");
			$(".alert.cart-alert-date").addClass("show");
		}
	})(window, jQuery);
</script>