<style>
	.tnx .fa {
		display: block;
		font-size: 4rem;
	}
</style>
<h2 class="tnx">Merci <i class="fa fa-heart"></i></h2>
<p>
	Nous vous remercions pour votre participation. Votre virement est pris en compte.
	Une fois que votre virement sera validé, la facture vous sera envoyée par mail au
	<strong><?= $bill['email'] ?></strong>
</p>
<script>
	(function(X, $) {
		var cart,
			cart_next_dom,
			cart_prev_dom,
			active_passed;

		cart = X.g_cos_cart;
		cart_next_dom = $('.cos-cart-next');
		cart_prev_dom = $('.cos-cart-prev');
		active_passed = 0;

		cart_next_dom.prop('disabled', true).hide();
		cart_prev_dom.prop('disabled', true).hide();
		// indicateur d'étape
		$('[data-actualstep]').removeClass('active');
		$('[data-actualstep="payment"]').addClass('active');
		$('.cart-stepper-title').text($('[data-actualstep].active').text());
		$('[data-actualstep]').each(function() {
			var self;

			self = $(this);
			self.off('click');
			if (self.hasClass('active'))
				active_passed = 1;
			if (!active_passed)
				self.addClass('clickable')
				.on('click', function() {
					var self;

					self = $(this);
					cart.xhr()[self.data('method')]()
						.then(function(html) {
							cart.dom('.modal-body.cos-cart')
								.empty()
								.html(html);
						})
				});
			else
				self.removeClass('clickable');
		});

		// mettre à jour le compteur de panier
		X.refresh_camp_count();
	})(window, jQuery);
</script>