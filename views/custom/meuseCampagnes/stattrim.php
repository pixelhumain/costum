<?php

//assets from ph base repo
$cssAnsScriptFilesTheme = array(
    // datetimepicker
    '/plugins/xdan.datetimepicker/jquery.datetimepicker.min.css',
    '/plugins/xdan.datetimepicker/jquery.datetimepicker.min.js',
    '/plugins/xdan.datetimepicker/jquery.datetimepicker.full.min.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<style>
    .row-style {

        padding: 20px;
        text-align: center;
    }

    .card {
        box-shadow: 0 3px 9px 0 rgba(169,184,200,.15);
        -webkit-box-shadow: 0 3px 9px 0 rgba(169,184,200,.15);
        -moz-box-shadow: 0 3px 9px 0 rgba(169,184,200,.15);
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid #e9ecef;
        border-radius: .25rem;
    }

    .card, .card-group {
        margin-bottom: 30px;
    }

    .card-group {
        display: flex;
        flex-direction: column;
    }

    .card-group>.card {
        margin-bottom: 15px;
    }

    @media (min-width: 576px)
    {
        .card-group {
            flex-flow: row wrap;
        }

        .card-group>.card:not(:last-child) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-group>.card {
            flex: 1 0 0%;
            margin: 5px;
        }
    }

    .border-right {
        border: 2px solid #edf2f9!important;
    }

    .card-body {
        flex: 1 1 auto;
        padding: 25px;
    }

    align-items-center {
        align-items: center!important;
    }
    @media (min-width: 992px)
    {
        .d-lg-flex {
            display: flex!important;
        }

        .mt-lg-0, .my-lg-0 {
            margin-top: 0!important;
        }
    }

    align-items-center {
        align-items: center!important;
    }



    @media (min-width: 768px)
    {
        .d-md-block {
            display: block!important;
        }

        .mt-md-3, .my-md-3 {
            margin-top: 1rem!important;
        }
    }

    .d-flex {
        display: flex!important;
    }

    .d-inline-flex {
        display: inline-flex!important;
    }

    .ml-auto, .mx-auto {
        margin-left: auto!important;
    }

    .error {
        outline: 1px solid #f00 !important;
    }

</style>

<div class="container">
    <div class="row row-style" style=" margin-top: 20px;">
        <h3><strong>Statisques trimestriels </strong></h3><br>
        <span class='col-sm-offset-2 col-sm-2 letter-azure' style="margin-top: 7px;"> choissisez vos dates : </span>
        <div class='col-sm-4'>
            <div class="form-group">
                <div class="input-group input-daterange" id="fromToDate" >
                    <input type="text" class="form-control input dateTimeInput" id="startDate">
                    <span class="input-group-addon"> à </span>
                    <input type="text" class="form-control input dateTimeInput" id="endDate">
                </div>
            </div>
            <p class="help-block" id="diverror"></p>
        </div>
        <div class="col-sm-2">
            <button class="btn" id="generer"> générer </button>
        </div>

    </div><br>

    <div class="card-group">
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="stat text-dark mb-1 font-weight-medium"><span id="organizations">~~</span></h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq ">Organisations</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"> <i class="fa fa-users text-purple fa-3x"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><span id="projects">~~</span></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq">Projets
                        </h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"> <i class="fa fa-lightbulb-o text-azure fa-3x"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium"><span id="events">~~</span></h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq">Evenements</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i class="fa fa-calendar text-green-poi fa-3x"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 font-weight-medium"><span id="classifieds">~~</span></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq">Annonces</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i class="fa fa-bullhorn text-orange fa-3x"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card-group col-sm-offset-2 col-sm-8">

             <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><span id="poi">~~</span></h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-green-poi">Points d'intérêt
                            </h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"> <i class="fa fa-map-marker text-green-poi fa-3x"></i></span>
                        </div>

                    </div>
                </div>
            </div>

            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="stat text-dark mb-1 font-weight-medium"><span id="news">~~</span></h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq ">News</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"> <i class="fa fa-newspaper-o text-turq fa-3x"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h2 class="text-dark mb-1 w-100 text-truncate font-weight-medium"><span id="reactions">~~</span></h2>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq">Réactions
                            </h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"> <i class="fa fa-heart-o text-red fa-3x"></i></span>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h2 class="text-dark mb-1 font-weight-medium"><span id="comments">~~</span></h2>
                            </div>
                            <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate text-turq">Commentaires</h6>
                        </div>
                        <div class="ml-auto mt-md-3 mt-lg-0">
                            <span class="opacity-7 text-muted"><i class="fa fa-comment text-yellow fa-3x"></i></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>


<script>
    $(".dateTimeInput").datetimepicker({
        autoclose: true,
        lang: "fr",
        format: "d/m/Y",
        timepicker:false
    });

    $(document).ready(function () {
        $("#startDate").blur(function () {
            if(!$(this).val()){
                $(this).addClass(" error");
            } else {
                $(this).removeClass("error");
            }
        });

        $("#endDate").blur(function () {
            if(!$(this).val()){
                $(this).addClass(" error");
            } else {
                $(this).removeClass("error");
            }
        });
    })

    $("#generer").click(function () {

        if((!$("#startDate").val()) || (!$("#endDate").val()) )
        {
            $("#diverror").html("veuillez renseigner les champs");
        } else {
            $("#diverror").html("");
            var startDate = $("#startDate").val();
            var endDate = $("#endDate").val();
            var params = {startDate : startDate, endDate : endDate}
            ajaxPost(
                null,
                baseUrl+"/costum/meusecampagnes/stattrim",
                params,
                function(data){
                    $("#organizations").html(data.organizations);
                    $("#projects").html(data.projects);
                    $("#events").html(data.events);
                    $("#classifieds").html(data.classifieds);
                    $("#news").html(data.news);
                    $("#poi").html(data.poi);
                    $("#comments").html(data.comments);
                    $("#reactions").html(data.reactions);
                },
                null,
                "json"
            );
        }
    })
</script>