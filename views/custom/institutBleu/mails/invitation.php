<table class="row masthead" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: white;width: 100%;position: relative;display: table;">
    <tbody>
    <tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Masthead -->
        <th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                <tr style="padding: 0;vertical-align: top;text-align: left;">
                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                        <center style="width: 100%;min-width: 532px;">
                            <?php if(!empty($logo2)){ ?>
                                <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logo2 ?>" valign="bottom" alt="Logo Communecter" align="center" class="text-center" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;float: none;text-align: center;">
                            <?php } ?>
                        </center>
                    </th>
                    <th class="expander" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0 !important;margin: 0;text-align: left;line-height: 19px;font-size: 15px;visibility: hidden;width: 0;"></th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;">
    <tbody>
    <tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
        <th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                <tr style="padding: 0;vertical-align: top;text-align: left;">
                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
                        <b>
                            <h5 style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: center;line-height: 1.3;word-wrap: normal;margin-bottom: 10px;font-size: 20px;">
                                <?php
                                if( empty($title) || $title == "Communecter"){
                                    echo Yii::t("mail","Open Like Wikipedia and as connected as Facebook join the movement!") ;
                                }
                                ?>
                            </h5>
                        </b><br/>
                        <?php echo "Votre structure a été identifiée comme appartenant au secteur de l’économie bleue et
l’Institut Bleu souhaite recueillir votre approbation pour apparaître dans l’annuaire";
                        if( !empty($target) && !empty($target["type"]) && $target["type"] != Person::COLLECTION )
                            echo " <b>\"".$target["name"]."\"</b>"; ?>
                        .<br/><br/>
                        <?php if($target["type"]!= Person::COLLECTION){ ?>
                            <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">

                                <tr style="padding: 0;vertical-align: top;text-align: left;">
                                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: right;width:50%;line-height: 19px;font-size: 15px;">
                                        <a href="<?php echo $baseUrl ."#invitationanswer.true".(isset($invitedUserId)?"?invited=".$invitedUserId:"") ?>" style="color: white;
						                    font-family: Helvetica, Arial, sans-serif;
						                    font-weight: normal;
						                    padding: 10px 40px;
						                    margin: 0;
						                    text-align: left;
						                    line-height: 1.3;
						                    text-decoration: none;
						                    width: 40%;
						                    margin-right: 2%;
						                    border-radius: 3px;
						                    background-color: #84d802;
						                    font-size: 15px;
						                    font-weight: 800;"><?php echo Yii::t("common", "Accept") ?></a>
                                    </th>
                                    <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;width:50%;">

                                        <a href="<?php echo $baseUrl ."#invitationanswer.false".(isset($invitedUserId)?"?invited=".$invitedUserId:"") ?>" style="color: white;
						                  font-family: Helvetica, Arial, sans-serif;
						                  font-weight: normal;
						                  padding: 0;
						                  margin: 0;
						                  text-align: left;
						                  line-height: 1.3;
						                  text-decoration: none;
						                  width: 40%;
						                  margin-left: 2%;
						                  border-radius: 3px;
						                  background-color: #e33551;
						                  font-size: 15px;
						                  font-weight: 800;
						                  padding: 10px 40px;"><?php echo Yii::t("common", "Refuse") ?></a>
                                    </th>

                                </tr>
                            </table>
                            <br/><br/>
                        <?php } ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
                            <tr>
                                <td>
                                    <p>
                                    L’institut Bleu est une association loi 1901, visant à soutenir, coordonner et fédérer les acteurs socio-
professionnels, institutionnels et scientifiques de l’économie bleue durable sur le territoire de La Réunion.
                        </p>
                        <p>
Pour cela, l’association assure la promotion, le développement, la recherche, l’innovation et le transfert aux
acteurs professionnels de l’écosystème maritime. Ses actions partenariales se développement à l’échelle
locale, mais également sur l’ensemble du territoire français, européen et international.
                        </p>
                        <p>
L’une des actions inscrites dans le programme d’action 2025 de l’Institut Bleu concerne la création d’un
annuaire recensant de la manière la plus exhaustive possible les acteurs du secteur de l’économie bleue. Cet
outil sera consultable librement et gratuitement sur le site internet de l’Institut Bleu (www.institutbleu.re).
Ce projet est financé par la Région Réunion dans le cadre du financement du plan d’action 2025 de l’Institut
Bleu et par le Secrétariat d’Etat chargé de la Mer et de la Biodiversité dans le cadre du Fonds d’Intervention
Maritime (FIM).
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </th>
                </tr>
            </table>
        </th>
    </tr>
    </tbody>
</table>

