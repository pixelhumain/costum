
<style>

#filterContainer .dropdown .dropdown-menu, 
.searchObjCSS .dropdown .dropdown-menu {
    margin: 0;
    padding-top: 0;
    padding-bottom: 0;
    border: 0;
    top: 100%;
    left: 1% !important;
	width: 98%;
}

.position-static {
	position: static !important;
}

.filter-item-with-image{
	height:100%;
	width:100%;
	display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: stretch;
}

	#filters-nav{
		position: sticky !important;
	}

	.main-container{
		padding-top: 56px !important;
	}

	#right-side{
		padding: .4em .5em .6em .5em !important;
	}
	.community{
		box-shadow: none !important;
	}
	.searchObjCSS{
		padding-top: 12px !important;
	}
	.searchObjCSS .dropdown .btn-menu, .searchBar-filters .search-bar, .searchBar-filters .input-group-addon, .btn-primary-ekitia{
		border-radius :  2px !important;
		border: 1px solid var(--primary-color) !important;
		font-size: 18px !important;
		line-height: 20px;
	}

	.searchBar-filters .input-group-addon{
		background-color: var(--primary-color) !important;
		color: white !important;
		padding: 10px 14px !important;
		height: 44px;
	}

	.title-header:before{
		border-top: 0px !important;
	}
	.searchBar-filters .search-bar{
		height: 44px;
		width: 320px !important;
	}
	.dropdown-title{
		display: none;
	}
	.thin {
		padding: 1em;
		font-size: 16px;
		text-transform: uppercase;
		width: 100%;
	}
	.badge-theme-count{
		background-color: var(--primary-color) !important;
	}
	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
		height: 250px;
		
	}
	.basic-banner {
		width: 100%;
		height: 100%;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
		display:flex;
		align-items: center;
		justify-content: space-around;
	}
	.title-banner h1::before {
		content: "";
		position: absolute;
		top: 15%;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}
	.tab-outer .tabs .term-item {
		padding: 0px 15px;
		text-transform: uppercase;
		font-size: 24pt !important;
	}
	.tab-outer .tabs .term-item.active a {
		border-bottom: 4px solid var(--primary-color);
		font-weight: bolder;
	}
	.tab-outer .tabs .term-item a {
		margin-bottom: -3px;
		border-bottom: 4px solid transparent;
		padding: 15px 0px;
	}
	.title-section h1::before {
		left: 14%;
	}
	.basic-banner h1.bg-yellow-point::after {
		content: "";
		position: absolute;
		left: 50%;
		top: 0;
		z-index: -1;
		margin-left: -1em;
		width: 2em;
		height: 2em;
		background-color: #F9B000;
		border-radius: 50%;
	}

	.tab-outer{
		justify-content: space-around !important;
	}

	span.category{
		text-transform: uppercase;
		color:var(--primary-color);
		font-weight: bold;
	}
	span.date{
		font-weight: bold;
	}

	.btn-filters-select{
		padding-top: -1px !important;
	}

	.bootbox.modal .modal-body{
		padding: 0px;
	}

	#filterContainer .dropdown .btn-menu, .searchObjCSS .dropdown .btn-menu, .searchObjCSS .filters-btn {
		margin-left: 0px !important;
	}
	.initialisSearch{
		display: none;
	}
	#openModal .portfolio-modal.modal, .portfolio-modal.modal {
    	background: rgba(32, 42, 95, 0.7) !important;
	}
	@media (min-width: 768px) {
		#openModal .modal-content {
			width: 70%;
			margin-left: 15%;
		}
	}
	.item-text {
		margin-bottom: 5px;
	}
	.item-inner .thematic {
		background-color: #01305e;
		padding: 3px;
		border-radius: 0%;
		margin-bottom: 5px;
		font-size: 16px;
		color: white;
	}
	.status a{
		color: white;
		font-weight: 700;
		font-size: 16px;
	}
	.status{
		position: absolute;
		right: 4%;
		background: green;
		padding: 3px;
		background: #93C020;
	}
	.link-bordered {
		display: flex;
		flex-wrap: wrap; 
		gap: calc((.10rem)* 4);
	}
	/*mailing*/

	#mailing{
		margin-top: 55px;
	}
	#mailing .form-control{
		box-shadow: none !important;
		outline: none !important;
		background: #fefefe;
		padding: 20px 10px;
	}

	#mailing textarea.form-control{
		padding: 10px 10px !important;
	}

	#txtMsg{
		height: 100px !important
	}

	#mailing h3{
		margin-bottom: 2%;
		text-align: center;
		color: #ACCD5C;
	}
	#mailing .icon-title{
		font-size: 48pt;
		color: #1E56A2;
	}
	#mailing option{
		padding: 0.4em 0.2em;
	}
	#mailing select{
		padding: 0.2em 0em !important;
	}
	#mailing .contact-image{
		text-align: center;
	}
	#mailing .btnSend{
       	-webkit-transition: 0.5s ease;
       	-moz-transition: 0.5s;
       	-ms-transition: 0.5s;
       	-o-transition: 0.5s;
       	transition: 0.5s;
       	width: 50%;
       	cursor: pointer;
       	text-transform: uppercase;
		border-radius: 5rem;
		padding: 0.6em 0.6em 0.5em 0.6em;
		background: #FFF;
		border: 3px solid #1E56A2;
		font-weight: 800;
		color: #1E56A2;
	}
	#mailing .btnSend:hover {
        background:#1E56A2;
        color:white;
    }
	#mailing .btnSend:focus{
		outline: none;
	}

	#listEmailGrid{
		max-height: 200px;
		overflow-y: auto;
		display: flex;
		flex-wrap: wrap;
	}
	
	.email-badge{
		border-radius: 20px;
		padding: 1px 8px;
		margin: 2px;
		font-weight: bold;
	}

	#mailing {
        background-color: rgb(0,0,0); 
        background-color: rgba(0,0,0,0.8); 
        backdrop-filter: blur(2px);
        -webkit-backdrop-filter: blur(2px);
    }
	.w-100{
		width: 100% !important;
	}
</style>
<!-- Modal Mailing-->
<div id="mailing" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="contact-image">
				<br/>
				<span class="fa fa-envelope icon-title"></span>
			</div>
			<div>
				<h3>COMMUNAUTÉ MAILING</h3>
				<!--p class="text-center">Séléctionner le destinataire(s) de votre email.</p -->
			</div>
	
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row form-group">
						<div class="col-md-12 recipientTextArea">
							<textarea id="txtTo" name="txtTo" class="form-control margin-bottom-5 activeMarkdown" placeholder="Copier la liste des e-mails ici" rows="2" required></textarea>
							<div id="listEmailGrid" class=""></div>
						</div>
					</div>	
					<div class="form-group">
						<input id="txtObj" name="txtObj" class="form-control activeMarkdown" placeholder="Objet du mail *" required />
					</div>
					<div class="form-group text-center">
						<button id="btnSend" class="btnSend" data-dismiss="modal">Envoyer</button>
						<!--button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;
    var oddList=<?php echo json_encode(CO2::getContextList("odd")); ?>;
	var filtersByPage = {};
	var filterFields = [];
	var pageDirectory = "directory.classifiedPanelHtml";
	var defaultFilters = {'$or':{}};
	var sortResultBy = {"name":1};
	// Organizations
	defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["parentId"] = costum.contextId;
	defaultFilters['$or']["source.keys"] = costum.slug;
	defaultFilters['$or']["reference.costum"] = costum.slug;
	defaultFilters['$or']["links.contributors."+costum.contextId] = {'$exists':true};
	defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    var affichage = "";

	if(appConfig && typeof appConfig.sourceKey !== "undefined"){
		defaultFilters['$or']["source.keys"] = [costum.slug, appConfig.sourceKey];	
	}

	if(appConfig && typeof appConfig.sourceId !== "undefined"){
		defaultFilters['$or']["parentId"] = [costum.contextId, appConfig.sourceId];
		defaultFilters['$or']["links.memberOf."+appConfig.sourceId] = {'$exists':true};
	}

	// Not validated
	if(!isInterfaceAdmin)
		defaultFilters["preferences.toBeValidated."+costum.slug]={'$exists':false};

	
	if(pageApp.indexOf("?")!=-1){
		pageApp = pageApp.split("?")[0];
	}

	if(["actus", "datacatalogue", "boites-a-outils"].indexOf(pageApp)==-1){
		var activity = null;
		var organizationType = null;
		
		if(typeof costum.lists != "undefined" && typeof costum.lists.activity != "undefined"){
			activity = {};
			$.each(costum.lists.activity, function(index, sector){
				activity[index] = {};
				activity[index][sector] = sector;
			})
		}
		
		if(typeof costum.lists != "undefined" && typeof costum.lists.structure != "undefined"){
			organizationType  = {};
			Object.assign(organizationType , costum.lists.structure);
		}
	}else{
		sortResultBy = { "created":-1, "date":-1 };
	}

	// var defaultScopeList = ["FR"]; /** Not for the mement */
	if(pageApp == "search"){
		pageDirectory = "directory.cards";
		defaultFilters['displayAuth'] = true;
		filtersByPage = {
			text : {
				placeholder : "Recherche de membres"
			}
		}

		filterFields = ["thematic"]
		
		if(typeof costum.typeObj != "undefined" && 
			(typeof costum.typeObj.organizations != "undefined" || typeof costum.typeObj.organization!="undefined") && 
			costum.lists){
			var costumInputs = costum.typeObj.organizations||costum.typeObj.organization;
			if(costumInputs && costumInputs.dynFormCostum && costumInputs.dynFormCostum.beforeBuild && costumInputs.dynFormCostum.beforeBuild.properties){
				costumInputs = costumInputs.dynFormCostum.beforeBuild.properties
			}else if(costumInputs && costumInputs.dynFormCostum && costumInputs.dynFormCostum.beforeBuild){
				costumInputs = costum.typeObj.organization.dynFormCostum.beforeBuild.properties;
			}
			$.each(costumInputs, function(index, input){
				if(typeof input.list != "undefined" && costum.lists[input.list]){
					let optionList = Array.isArray(costum.lists[input.list])?costum.lists[input.list].reduce((a, v) => ({ ...a, [v]: v}), {}):costum.lists[input.list];
					let filterView = "dropdownList";
					if(index.toLowerCase().includes("odd")){
						filterView = "largeMenuDropdown";
						optionList = oddList;
					}
					// to do add condition for specific filterView
					filtersByPage[input.list] = {
						view : filterView,
						type : "filters",
						trad:false,
						field : 'thematic',
						countFieldPath : 'thematic',
						countOnlyPath : 'thematic',
						name : (input.label.length < 30)?input.label:index,
						event : "filters", 
						activateCounter : true,
						list : optionList
					}
					if(index.toLowerCase().includes("odd")){
						filtersByPage[input.list].colClass = "col-md-2 col-sm-4 col-xs-6 padding-5";
					}
				}
			})
		}
	}

	if(pageApp == "actus"){
		pageDirectory = "directory.cards";
		filterFields = ["mediaImg", "text", "organizer", "date", "parent", "created"];
		// defaultFilters["type"]={'$ne':"activityStream"};
		defaultFilters["type"]={'$nin':["activityStream", "tool", "doc"]};
		filtersByPage = {
			tabs : {
	 			view : "tabs",
	 			type : "tags",
				event : "tabs",
	 			list : {
					/*"Toutes les actualités": {
						icon : null,
						page : "#"+pageApp,
						class: "active"
					},*/
					"ACTUALITÉS" : {
						icon: "fa-calendar",
						type:"types",
						value:"news",
                        class: "active"
					},
					"ÉVÉNEMENTS" : {
						icon: "fa-calendar",
						type:"types",
						value:"events"
					}
				}
	 		}
		};
	}

	if(pageApp=="projects"){
		pageDirectory = "directory.cards";
	}

	if(pageApp=="citoyens"){
		pageDirectory = "directory.profils";
	}

	var paramsFilter = {
		container : "#filters-nav",
		options : {
			tags : {
				verb : '$all'
			}
		},
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
			notSourceKey: true,
		 	types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		 	filters: defaultFilters,
			indexStep: 900,
			sortBy: sortResultBy
		},
		filters : filtersByPage,
	 	results : {
			renderView: pageDirectory,
		 	smartGrid : true
		}
	}
	if(isInterfaceAdmin){
		filterFields = [
                "name", "email","preferences","thematic","collection", "slug", "fromActivityPub", "attributedTo", "objectId", "created", "updated", "profilThumbImageUrl",
                "profilImageUrl","profilMediumImageUrl", "shortDescription", "parent", "tags", "type", "section", "category", "startDate", "endDate", "openingHours",
                "address","addresses", "scope", "geo", "geoPosition", "links", "description", "isSpam", "userSpamDetector", "dateSpamDetector","type"]
		
	}

	if(filterFields.length!=0){
		paramsFilter.defaults["fields"] = filterFields;
	}
	if(isInterfaceAdmin){
		
		paramsFilter.urlData = baseUrl+"/co2/search/globalautocompleteadmin";
	}
	var actualityData = {}
	
	jQuery(document).ready(function(){
		coInterface.bindLBHLinks();
		searchObj.filters.views["tabs"] = function(k,v, fObj){
			str = `<div class="tab-outer"> 
						<div id="tabs-container" class="tabs left large-tab">`;
			if(v.list){
				$.each(v.list, function(key, item){
					let dataFilter = "";
					if(item.page){
						dataFilter =  'href="'+item.page+'"';
					}else{
						dataFilter = `data-type="${item.type??v.type??"filters"}" 
							data-key="${item.key??key}" 
							data-value="${item.value??item.label??key}" 
							data-field="${item.field??v.field??"tags"}" 
							`;
						//if(item.type && item.type=="filters"){
							dataFilter+=` data-label="${item.label??key}" `;
							dataFilter+=` data-event="${item.event??v.event??"filters"}" `;
						//}
					}
					let icon = `<span class="ico-circle"><i class="fa ${(item.icon!="")?item.icon:"fa-tag"}"></i></span>`;
					if(item.icon==null){
						icon="";
					}
					str+= `<div class="term-item ${item.class??""}">
						<a type="button" class="${item.page?"lbh":"btn-filters-select"} tabs" ${dataFilter} data-filterk="tabs">${icon} ${item.label||key}</a>
					</div>`;
				});
			}
			str+=`</div>
			</div>`;

			return str;
		}

		searchObj.filters.events["tabs"] = function(fObj, domFilters, k){
			$(".btn-filters-select").off().on("click", function(event){
				let type = $(this).data("type");
				let field = $(this).data("field");
				if(type!="types"){
					if(field=="tags"){
						fObj.search.obj.tags = [$(this).data("value")];
					}else{
						fObj.search.obj.filters[field] = $(this).data("value")
					}
					fObj.search.obj.types = appConfig.filters.types||["news", "events", "poi"];
					fObj.filters.actions.types.initList = appConfig.filters.types||["news", "events"];
				}else{
					fObj.search.obj.tags = [];
					fObj.search.obj.types = [$(this).data("value")];
					fObj.filters.actions.types.initList =  [$(this).data("value")];
					/*$(".last-item").remove();
					$("#tabs-container").append("<div class='term-item last-item' style='margin-left:auto'></div>");
					$(".btn-show-map").addClass("btn-primary-ekitia").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo(".last-item");
					$(".btn-show-map").show();*/
				}
				$(".term-item").each(function(){
					$(this).removeClass("active");
				});
                if(pageApp == "actus"){
                    affichage = "tml";
                }
				$(this).parent().addClass("active")
				fObj.search.init(fObj);
			});
		}

        if(pageApp == "search"){
            searchObj.header.events.map = function(fObj){
                $(".btn-show-map").off().on("click", function(e){
                    fObj.helpers.toggleMapVisibility(fObj);
                    var text = filterSearch.results.map.active ? '<i class="fa fa-list-alt"></i> '+tradCms.directoryPage : '<i class="fa fa-map-marker"></i> '+trad.map;
                    $(".btn-show-map").html(text);
                });
            }
        }

		searchObj.search.sortAlphaNumeric = function(results){
    		mylog.log("searchObj.search.callBack tosort", results);
			let sorted = Object.values(results).sort((a, b) => {
				let fa = a.name.replace(" ", "").trim().toLowerCase(),
					fb = b.name.replace(" ", "").trim().toLowerCase();

				return ('' + fa).localeCompare(fb);
			});
			let sortedResults = {}
			$.each(sorted, function(i, val){
				sortedResults[val._id.$id] = val;
			});
			return sortedResults;
    	}

		searchObj["scroll"].render = function(fObj, results, data){
			results = searchObj.search.sortAlphaNumeric(results);
      		//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				// if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				// }else{
				// 	toastr.error("Aucun résultat trouvé. Essayez avec un terme plus précis.");					
				// }	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique);
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
					}
					imageObserver.observe(v);
				});
			}
		}
		searchObj.filters.initDefaults = function(fObj, pInit){
			mylog.log("searchObj.filters.init pInit overrided", pInit);
			if(typeof pInit.filters != "undefined"){
				// Initialisation des filtres themes & scopeObj par exemple [voir si pertinent ici]
				$.each(pInit.filters,function(k,v){
					mylog.log("searchObj.filters.init each", k,v);
					if(v.view == "scopeList"){
						fObj.filters.defaults["scopeList"](fObj, k,v);
					}
					if(typeof fObj.filters.defaults[k] != "undefined" ){
						fObj.filters.defaults[k](fObj, k,v);
					}
				});
			}
			if(notEmpty(fObj.search.obj.tags)){
				$.each(fObj.search.obj.tags, function(e,v){
					labelTags = (typeof tradCategory[v] != "undefined") ? tradCategory[v] : v;
					fObj.filters.manage.addActive(fObj,{type : "tags", value : v, label : labelTags});
				});
			}

			// Mode dans localStorage
			modeFilter = localStorage.getItem("filtersmodeView");
			if ( notNull(costum) && typeof costum.slug != "undefined")
				modeFilter = localStorage.getItem("filtersmodeView"+costum.slug); 
			if(typeof fObj.header != "undefined" 
				&& typeof fObj.header.options != "undefined" 
				&& typeof fObj.header.options.right != "undefined" 
				&& typeof fObj.header.options.right.group != "undefined" 
				&& typeof fObj.header.options.right.group.switchView != "undefined" 
				&& fObj.header.options.right.group.switchView 
				&&  notNull(modeFilter)
			){
				$(".changeViews").removeClass("active");
				$(".changeViews[data-mode^="+modeFilter+"]").addClass("active");
				fObj.search.obj.mode = modeFilter;
				if(modeFilter != "table"){
					var viewMode = "elementPanelHtmlSmallCard"
					if(modeFilter == "list")
						viewMode = "elementPanelHtmlMiWidth" 
					fObj.results.renderView = "directory."+viewMode;
					if(fObj.search.loadEvent.default == "table"){
						fObj.search.loadEvent.default = "scroll";
						fObj.results.$grid = null;
						$(".bodySearchContainer").html(`
							<div class="no-padding col-xs-12" id="dropdown_search"> </div>
							<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div> 
						`);
					}
					fObj.results.multiCols = true;
				}else{
					fObj.search.loadEvent.default = "table";
					fObj.results.multiCols = false;
					fObj.results.smartGrid = false;
				}
			}

			// Analyse des params GET dans l'url afin de les initialiser dans la vue filtres actives
			getParamsUrls=location.hash.split("?");
			if(typeof getParamsUrls[1] != "undefined" ){
				//var parts = getParamsUrls[1].split("&");
				var parts = new URLSearchParams(getParamsUrls[1]);
		        var $_GET = {};
		        var initScopesResearch={"key":"open","ids":[]};
		        
				for (let entry of parts.entries()) {
				//for (var i = 0; i < parts.length; i++) {
		            //var temp = parts[i].split("=");
		            //$_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
					$_GET[entry[0]] = entry[1];
		        }

		        if(Object.keys($_GET).length > 0){
		        	var paramsGetExclude=["scopeType"];

		            $.each($_GET, function(e,v){
		            	if($.inArray(e, paramsGetExclude) < 0){
			                //v=decodeURI(v);
			                if(e=="text"){
								var domSearchText= (typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined" && typeof pInit.filters[e].dom != "undefined") ? pInit.filters[e].dom : fObj.filters.dom;
			                	fObj.search.obj[e]=v;
			                	$(domSearchText+" .searchBar-filters .main-search-bar[data-field='text']").val(v);
			                }
			                else if(e=="types"){
			                	fObj.search.obj.types=[];
			                	values=v.split(",");
			                	$.each(values, function(i, val){
		                			elt=directory.getTypeObj(val);
		                			fObj.search.obj.types.push(val);
		                			fObj.filters.manage.addActive(fObj,{type:"types",value:elt.name, field:val, key:val});
			                	});
			                }
							else if(e=="thematic"){
								fObj.search.obj.filters.thematic=[];
								values=v.split(",");
								$.each(values, function(i, val){
									fObj.search.obj.filters.thematic.push(val);
									fObj.filters.manage.addActive(fObj,{type:"filters", field: "thematic",value:val, label:val});
								});
								mylog.log("searchObj.filters.init thematic", v, fObj.search.obj.filters.thematic);
							}
			                else if(e=="valueArray"){
								values=v.split(",");
								const addArrayFilter = (valueArray, element) => {
									var inArrayValue = (Array.isArray(valueArray)) ? valueArray : [valueArray];
									if(!exists(fObj.search.obj.filters))
										fObj.search.obj.filters = {};
									if(element.data("otherfield")){
										if(exists(fObj.search.obj.filters) && !exists(fObj.search.obj.filters['$or'])){
											var filtersData = $.extend({}, fObj.search.obj.filters)
											fObj.search.obj.filters = {};
											fObj.search.obj.filters['$or'] = filtersData;
										}
										if(!exists(fObj.search.obj.filters['$or']))
											fObj.search.obj.filters['$or'] = {};
										if(!exists(fObj.search.obj.filters['$or'][element.data("field")]))
											fObj.search.obj.filters['$or'][element.data("field")] = {'$in': inArrayValue};
										else if(!exists(fObj.search.obj.filters['$or'][element.data("field")]['$in']))
											fObj.search.obj.filters['$or'][element.data("field")]['$in']= inArrayValue;
										else
											if (Array.isArray(valueArray))
												fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(...valueArray);
											else 
												fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(valueArray);
			
										if(!exists(fObj.search.obj.filters['$or'][element.data("otherfield")]))
											fObj.search.obj.filters['$or'][element.data("otherfield")] = {'$in': [element.data("label")]};
										else if(!exists(fObj.search.obj.filters['$or'][element.data("otherfield")]['$in']))
											fObj.search.obj.filters['$or'][element.data("otherfield")]['$in'] = element.data("label");
										else
											fObj.search.obj.filters['$or'][element.data("otherfield")]['$in'].push(element.data("label"));
									}else {
										if(exists(fObj.search.obj.filters['$or'] ) && exists(fObj.search.obj.filters['$or']["tags"])){
											if(!exists(fObj.search.obj.filters['$or'][element.data("field")]))
												fObj.search.obj.filters['$or'][element.data("field")] = {'$in': inArrayValue};
											else if(!exists(fObj.search.obj.filters['$or'][element.data("field")]['$in']))
												fObj.search.obj.filters['$or'][element.data("field")]['$in']= inArrayValue;
											else
												if (Array.isArray(valueArray))
													fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(...valueArray);
												else 
													fObj.search.obj.filters['$or'][element.data("field")]['$in'].push(valueArray);	
										}else{
											if(!exists(fObj.search.obj.filters[element.data("field")]))
												fObj.search.obj.filters[element.data("field")] = {'$in': inArrayValue};
											else if(!exists(fObj.search.obj.filters[element.data("field")]['$in']))
												fObj.search.obj.filters[element.data("field")]['$in']= inArrayValue;
											else
												if (Array.isArray(valueArray))
													fObj.search.obj.filters[element.data("field")]['$in'].push(...valueArray);
												else 
													fObj.search.obj.filters[element.data("field")]['$in'].push(valueArray);
										}
									}
									// if(!exists(fObj.search.obj.filters[element.data("field")]))
									// 	fObj.search.obj.filters[element.data("field")] = {'$in': inArrayValue};
									// else if(!exists(fObj.search.obj.filters[element.data("field")]['$in']))
									// 	fObj.search.obj.filters[element.data("field")]['$in']= inArrayValue;
									// else
									// 	if (Array.isArray(valueArray))
									// 		fObj.search.obj.filters[element.data("field")]['$in'].push(...valueArray);
									// 	else 
									// 		fObj.search.obj.filters[element.data("field")]['$in'].push(valueArray);
								}
								$.each(values, function(key, value){
									if($(`button[data-key='${value}']`).length > 0){
										var $this = $(`button[data-key='${value}']`);
										var arrayValue = ($this.data("value").includes("\\")) ? $this.data("value").split("\\") : [$this.data("value")];
										addArrayFilter(arrayValue, $this);
										if(typeof fObj.search.obj[$this.data("type")] != "undefined" && exists(fObj.search.obj[$this.data("type")]))
											if(fObj.search.obj[$this.data("type")].includes(value))
												fObj.search.obj[$this.data("type")].splice(fObj.search.obj[$this.data("type")].indexOf(value), 1)
											else
												fObj.search.obj[$this.data("type")].push(value)
										else
											fObj.search.obj[$this.data("type")] = [value];

										fObj.filters.manage.addActive(fObj, $this);
									}
								});
			                }else if(e=="tags"){
			                	values=v.split(",");
			                	$.each(values, function(i, val){
			                		fObj.search.obj.tags.push(val);
			                		fObj.filters.manage.addActive(fObj,{type: e, value : val});
			                	});
			                }
			                else if($.inArray(e,["cities","zones","cp"]) > -1){
			                	$.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
			                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
			                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
			                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
					                    	$.each(myScopes.open, function(i, scope){
					                    		if(typeof scope.active != "undefined" && scope.active)
					                				fObj.filters.manage.addActive(fObj, {type:"scope",value:scope.name,key:i});
					                		});
					                	}
			                    	});

			                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
			                    	$.each(myScopes.open, function(i, scope){
			                    		if(typeof scope.active != "undefined" && scope.active)
			                				fObj.filters.manage.addActive(fObj, {type:"scope",value:scope.name,key:i});
			                		});
			                	}
			                }else if(e=="nbPage"){
			                	fObj.search.obj.nbPage=(Number(v)-1);
			                	fObj.search.obj.forced.page=true;
			                }else if(e=="map"){
			                	if(v == "true" || v == true){
									fObj.results.map.active=true;
									$(fObj.mapObj.parentContainer).fadeIn();
									// fObj.helpers.toggleMapVisibility(fObj);
								}
			                }else if(e=="mode"){
								$(".changeViews").removeClass("active");
								$(".changeViews[data-mode^="+v+"]").addClass("active");
			                	fObj.search.obj.mode = v;
								if(v != "table"){
									var viewMode = "elementPanelHtmlSmallCard"
									if(v == "list")
										viewMode = "elementPanelHtmlMiWidth" 
									fObj.results.renderView = "directory."+viewMode;
									fObj.search.obj.nbPage = 0;
									if(fObj.search.loadEvent.default == "table"){
										fObj.search.loadEvent.default = "scroll";
										fObj.results.$grid = null;
										$(".bodySearchContainer").html(`
											<div class="no-padding col-xs-12" id="dropdown_search"> </div>
											<div class="no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element"></div> 
										`);
									}
									fObj.results.multiCols = true;
								}else{
									fObj.search.loadEvent.default = "table";
									fObj.results.multiCols = false;
									fObj.results.smartGrid = false;
								}
			                }else if(e=="preview"){
								mylog.log("preview",v)
			                }else{
			                	if(v.indexOf(",") >= 1 ){
			                		values=v.split(",");
			                		if(typeof fObj.search.obj[e] == "undefined") fObj.search.obj[e]=[];
				                	$.each(values, function(i, val){
				                		fObj.search.obj[e].push(val);
				                		tradValue=val;
										var key = val;
										if(typeof fObj.pInit.filters[e] != "undefined"){
											if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["keyValue"] != "undefined" && !fObj.pInit.filters[e]["keyValue"]){
												if(typeof fObj.pInit.filters[e]["list"][val] != "undefined"){
													val = fObj.pInit.filters[e]["list"][val];
													tradValue = val;
												}
											}
										}
				                		if(typeof tradCategory[val] != "undefined")
				                			tradValue= tradCategory[val];
				                		else if(typeof trad[val] != "undefined")
				                			tradValue= trad[val];
										var dataFilter = {
											key: key,
											value:val.name,
											label:tradValue
										}
										if(typeof val.name != "undefined")
											dataFilter.value = val.name
										else if (typeof val == "string")
											dataFilter.value = val

										if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].type != "undefined")
											dataFilter.type = pInit.filters[e].type;
										else
											dataFilter.type = e;
										if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].field != "undefined")
											dataFilter.field = pInit.filters[e].field 

				                		fObj.filters.manage.addActive(fObj,dataFilter);
				                	});
			                	}else{
									if (
										typeof fObj.pInit.filters[e] != "undefined" && 
										typeof fObj.pInit.filters[e].type != "undefined" && 
										typeof fObj.pInit.filters[e].toUrlHistory != "undefined"
									) {
										const filterType = fObj.pInit.filters[e].type;
										const filterField = typeof fObj.pInit.filters[e].field != "undefined" ? fObj.pInit.filters[e].field : e;
										if (typeof fObj.search.obj[filterType] == "filters") {
											fObj.search.obj[filter] = {};
										}
										fObj.search.obj[filterType][filterField] = [v];
									}
									fObj.search.obj[e]=[v];
				                	tradValue=v;
									var key = v;
									if(typeof fObj.pInit.filters[e] != "undefined"){
										if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["keyValue"] != "undefined" && !fObj.pInit.filters[e]["keyValue"]){
											if(typeof fObj.pInit.filters[e]["list"][v] != "undefined"){
												v = fObj.pInit.filters[e]["list"][v];
												tradValue = v;
											}
										}
									}
			                		if(typeof tradCategory[v] != "undefined")
			                			tradValue= tradCategory[v];
			                		else if(typeof trad[v] != "undefined")
			                			tradValue= trad[v];
									else if (typeof tradValue === "string" && /^[a-f\d]{24}$/i.test(tradValue)) {
										if (typeof fObj.pInit.filters[e] != "undefined") {
											if(typeof fObj.pInit.filters[e]["list"] != "undefined" && typeof fObj.pInit.filters[e]["list"][v] == "string") {
												tradValue = fObj.pInit.filters[e]["list"][v];
												if (typeof tradCategory[tradValue] != "undefined") {
													tradValue = tradCategory[tradValue];
												}
												else if (typeof trad[tradValue] != "undefined") {
                                                    tradValue = trad[tradValue];
                                                }
											}
										}
									}
									var dataFilter = {
										key: key,
										value:v,
										label:tradValue
									}
									
									if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].type != "undefined")
										dataFilter.type = pInit.filters[e].type;
									else
										dataFilter.type = e;
									if(typeof pInit.filters != "undefined" && typeof pInit.filters[e] != "undefined"  && typeof pInit.filters[e].field != "undefined")
										dataFilter.field = pInit.filters[e].field 
									fObj.filters.manage.addActive(fObj,dataFilter);
				                }
			                }
		                }
		         	});
		        }
		    }
			// manage active toActivate
			if(typeof pInit.toActivate != 'undefined') {
				for(let [key, value] of Object.entries(pInit.toActivate)) {
					var dataFilter = {
						key : null,
						value : null,
						type : "filters"
					}
					if(value["value"]) dataFilter["value"] = value["value"];
					if(value["field"]) dataFilter["field"] = value["field"];
					if(value["type"]) dataFilter["type"] = value["type"];
					if(value["key"]) dataFilter["key"] = value["key"];
					if(dataFilter.type != "filters" && typeof value["likeFilters"] != "undefined") dataFilter["likeFilters"] = value["likeFilters"]
					fObj.filters.manage.addActive(fObj,dataFilter);
				}
			}
		}

		filterSearch = searchObj.init(paramsFilter);
		$("#openModalContent").addClass("w-100")

		$("#filterContainerInside").append("<div id='right-side'></div>");
		if(isInterfaceAdmin)
			$("#right-side").after(`<div class="text-right" style="margin-top:-8px"> <a data-toggle="modal" data-target="#mailing" class="btn btn-primary hidden-xs btn-primary-ekitia btn-primary-outline-ekitia"><i class="fa fa-envelope"></i> Inviter</a></div>`);
		
		if(costum.htmlConstruct.adminPanel.menu.community.recipientTextArea){
			$(".recipientSelect").remove();
		}else{
			$(".recipientTextArea").remove()
		}
		if(["actus", "datacatalogue", "boites-a-outils"].indexOf(pageApp)!=-1){
			$(".btn-show-map").hide();
			$("#filters-nav").css("padding", "0px");
			$("#filters-nav").css("border-bottom", "0px");
			$("#filterContainerInside").css("padding", "0px");
		}else{
			$("#filters-nav").css("border-bottom", "1px solid rgba(100, 100, 100, 0.1)");
			$("#filters-nav").css("padding-right", "5px");
			$("#filters-nav").css("padding-bottom", "9px");
			$("#filters-nav").css("padding-left", "5px");
			$("#filters-nav").css("padding-top", "12px");
			$(".btn-show-map").addClass("btn-primary-ekitia btn-primary-outline-ekitia").html("<i class='fa fa-map-marker margin-right-5'></i> CARTE").appendTo("#right-side");
		}

		if($(".tab-outer").length!=0){
			if(appConfig && typeof appConfig.filters !="undefined" && typeof appConfig.filters.types!="undefined"){
				let addBtnDropdown = "";
				if(appConfig.filters.types.length==1){
					if(appConfig.filters.types[0]=="answers" && typeof costum.lists!="undefined" && costum.lists["numericServiceForms"]){
						addBtnDropdown = prepareAddBtnDropdown(costum.lists["numericServiceForms"]);
					}else{
						addBtnDropdown = '<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-open-form" data-form-type="'+appConfig.filters.types[0]+'">POSTER</button></div>';
					}
				}else if(appConfig.filters.types.length>1){
					addBtnDropdown = prepareAddBtnDropdown(appConfig.filters.types);
				}
				$(".tab-outer").append(addBtnDropdown);
				// if ($.inArray("poi", appConfig.filters.types) && pageApp == "actus") {
				// 	$(".tab-outer").append('<div class="term-item"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				// }
			}
		}else{
			$("#right-side").prepend("")
			$("#right-side").insertBefore("#activeFilters");
		}
		//$("#activeFilters").hide();
		//$("#filterContainerInside").append("<button class='btn btn-primary-ekitia'>POSTER</button>")
		$(".activeFilter-label").remove();
		$(".headerSearchContainer").remove();
		
		$(".banner-outer").remove();
		// Events
		$(".btn-show-map, .btn-filters-select").on("click", function(){
			if(filterSearch.results.map.active){
				$("#bannerSearch").hide();
				$('html,body').animate({
					scrollTop: $("#filters-nav").offset().top},
				'slow');
			}else{
				$("#bannerSearch").show();
			}
            if($("#show-filters-xs").is(":visible")){
                $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
            }
			if ($("div.btn-event-actus-ctn").length > 0)
				$("div.btn-event-actus-ctn").remove();
			if ($(this).attr("data-value") == "events" && pageApp == "actus") {
				$(".tab-outer").append('<div class="term-item btn-event-actus-ctn"><button class="btn btn-primary-ekitia btn-primary-outline-ekitia btn-actuality-switch" data-switch="tml"><i class="fa fa-th-list"></i> Vue Timeline</button></div>');
				$(".tab-outer button.btn-actuality-switch").click( function() {
					var viewDisplayed = $(this).attr("data-switch");
                    affichage = viewDisplayed;
					loadActualityView()[viewDisplayed](this);
				})
			}
				
		});
        $("#show-filters-xs").on("click", function(){
            $("#filters-nav").get(0).scrollIntoView({behavior: 'smooth'});
        })
		$(".filter-btn-hide-map").on("click", function(){
            $(".btn-show-map").html('<i class="fa fa-map-marker"></i> '+trad.map);
			$("#bannerSearch").show();
		})

		/**
		 * Manage timeline view
		 */
		function loadActualityView () {
			var views = {};
			views.change = function (selector, name="", viewType="tml", icon="fa-th-list") {
				$(selector).html('<i class="fa ' + icon + '"></i> ' + name);
				$(selector).attr("data-switch", viewType);
			};
			views.tml = function (selector) {
				views.change(selector, "Vue vignette", "crd", "fa-th-list");
				if (notEmpty(views.timelineConetent())) {
					$("#dropdown_search").html(`<div class="container-timeline"><div id="timeline" class="timeline-ctn"></div></div>`);
					$(".timeline-ctn").html(views.timelineConetent());
					coInterface.bindLBHLinks();
				}
			};
			views.crd = function (selector) {
				views.change(selector, "Vue timeline", "tml");
				filterSearch.results.renderView = "directory.cards";
				filterSearch.search.init(filterSearch);
			};
			views.timelineConetent = function () {
				var resultHtml = "";
				var direction = "";
				$.each(actualityData, function(k,v) {
					var hash = "";
					var image = (v.profilMediumImageUrl != undefined) ?  v.profilMediumImageUrl : "https://www.ekitia.fr/wp-content/uploads/2023/02/photo-atelier-sicoval-bellecolline-2-Modifiee-450x222.jpg";
					if (v.collection == "events") {
						resultHtml += `
							<div class="timeline-item">
								<div class="timeline-icon">
									<div class="date-timeline"> 
										<b>${moment.unix(v.created).local().locale('fr').format('DD')}</b>/${moment.unix(v.created).local().locale('fr').format('MM')} 
										<br><b>${moment.unix(v.created).local().locale('fr').format('YYYY')}</b>
									</div>
								</div>
								<div class="timeline-content ${direction}">
									<div class="image-with-title">
										<div class="image-timeline-content">
											<img width="150" height="150" src="${image}" style="border-radius: 5px;" class="img-responsive" alt="" decoding="async">
										</div>
										<div class="info-timeline">
											<h4>${(v.name != undefined) ? v.name : "Il n'y a pas de titre"}</h4>
											${(v.type != undefined) ? '<h5 class="type-timeline"><i class="fa fa-chevron-right"></i> ' + v.type + '</h5>' : ""} 
											<div class="interval-timeline"> 
												<i class="fa fa-calendar"></i> ${moment.unix(v.startDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
												<i class="fa fa-long-arrow-right"> </i> ${moment.unix(v.endDate.sec).format('DD/MM/YYYY')} ( <i class="fa fa-clock-o"></i> ${moment.unix(v.startDate.sec).format('HH:mm')} ) 
											</div>
										</div>
									</div>
									<div class="timeline-desc">`;
						if (jsonHelper.getValueByPath(v, "organizer") != undefined && Object.keys(jsonHelper.getValueByPath(v, "organizer")).length > 0) {
							var organizerId = Object.keys(jsonHelper.getValueByPath(v, "organizer"))[0];
							var organizerName = jsonHelper.getValueByPath(v, "organizer")[organizerId].name;
							hash = `#page.type.organizations.id.${organizerId}`;
						resultHtml += 	`<div> 
											<a href="${hash}" class="lbh-preview-element padding-5 timeline-prev"><span class="ico-circle"></span> ${organizerName}</a>
										</div>`
						}	
						resultHtml +=	`<p class="timeline-short-desc">
											${(v.shortDescription != undefined && !(v.shortDescription == "")) ? v.shortDescription : "<i>Description non renseignée</i>"}
										</p>`		
						if (v.tags != undefined && Array.isArray(v.tags) && v.tags.length > 0) {
						resultHtml += `<div class="tag-content">`;
							v.tags.forEach(function(tagContent) {
								resultHtml += `<p class="tag-element">#${tagContent}</p>`;
							})
						resultHtml += `</div>`;
						}	
						if (v._id.$id != undefined) {
							resultHtml += `
										<div class="link-bordered">
											<a href="#page.type.events.id.${v._id.$id}" class="lbh-preview-element">En savoir plus</a>
										</div>
									`;
						}
						resultHtml +=
									`</div>
								</div>
							</div>
						`;
						direction = (direction == "") ? "right" : "";
					}
				})
				return resultHtml;
			};
			return views;
		}

		function prepareAddBtnDropdown(config){
			let addBtnDropDown = `<div class="btn-group">
			<button class="btn btn-primary-ekitia btn-primary-outline-ekitia dropdown-toggle" type="button" data-toggle="dropdown">POSTER
			<span class="caret"></span></button>
			<ul class="dropdown-menu pull-right">`;
			$.each(config, function(index, type){
				/*if(type=="news"){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`#live`);" class="padding-10">'+(type)+'</a></li>';
				}else*/ if(typeof type=="object" && type!=null){
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(`'+type.value+'`);" class="padding-10">'+(type.name||type.label||"Formaulaire")+'</a></li>';
				}else{
					let label = type;
					if(type=="news"){
						type="poi"; label="Actualité"
					}
					if(type=="newsletter"){
						return;
					}
					if(label=="poi"){
						label=typeObj[label].name;
					}
					let t = ((typeObj[type] && typeObj[type].sameAs)?typeObj[type].sameAs:type);
					addBtnDropDown+='<li><a href="javascript:costum[costum.slug].beforePost(``, `'+t+'`);" class="padding-10" data-form-type="'+t+'">'+label+'</a></li>';
				}
			});
			addBtnDropDown+="</ul></div>";
//co2/app/view/url/news.views.co.formCreateNews
			return addBtnDropDown;
		}


		/* email*/
		
		var selectedMailByRole = [];
		let listInvitation = {};

		/*$("#txtTo").on("change", function(){
			selectedMailByRole = $(this).val();
		});*/

		$("#txtTo").on("input", function(){
			let input = $(this).val().replaceAll('"', "");
			let emails = input.split(/[\s,;+/]+/).filter( e => e.trim() !== "");
			let validEmails = [];
			let invalidEmails = [];
			listInvitation = {};
			$.each(emails, function(index, email) {
				if(valideEmail(email)) {
					validEmails.push(email);
					selectedMailByRole = validEmails.join(",");
					listInvitation["listInvitation."+slugify(email, email)] = {
						"email": email, 
						"status": "Non lu"
					}
				} else {
					invalidEmails.push(email);
				}
			});
			displayEmails(validEmails, invalidEmails);
		})

		function valideEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		function displayEmails(validEmails, invalidEmails) {
			let validEmailsStr = validEmails.join(", ");
			let invalidEmailsStr = invalidEmails.join(", ");
			let html = "";
			$.each(validEmails, function(index, email) {
				html += '<span class="email-badge bg-success">' + email + '</span>';
			});

			$.each(invalidEmails, function(index, email) {
				html += '<span class="email-badge bg-danger invalid" onclick="boldText(this)">' + email + '</span>';
			});

			$("#listEmailGrid").html(html);
		}

		
		function boldText(el) {
			//alert("here we go"+JSON.stringify($(el).attr("class")));
			var start = txtarea.selectionStart;
			var end = txtarea.selectionEnd;
			var sel = txtarea.value.substring(start, end);
			var finText = txtarea.value.substring(0, start) + '[b]' + sel + '[/b]' + txtarea.value.substring(end);
			txtarea.value = finText;
			txtarea.focus();
		}

		// Send email
		$("#btnSend").on("click", function(){
			var isValid = false; 
			if (selectedMailByRole.length!=0 && $("#txtObj").val()!= ""){
				isValid = true;
			}
			mailType = "invitation";
			if(isValid){
				var paramsmail = {
					tpl : mailType,
					tplObject : $("#txtObj").val(),
					tplMail : selectedMailByRole,
					html:"<p style='white-space:pre-line'>"+$("#txtMsg").val()+"</p><br>",
					language: costum.language||"fr"
				};

				if(mailType == "invitation"){
					paramsmail.target = {
						type:costum.contextType,
						id:costum.contextId,
						name: costum.title
					}
					paramsmail.urlRedirect = baseUrl+"/costum/co/index/slug/"+costum.contextSlug;
					paramsmail.urlValidation = baseUrl+"/costum/co/index/slug/"+costum.contextSlug;
				}

				if (notNull(costum) && exists(costum.admin) && exists(costum.admin.email) && costum.admin.email != ""){
					paramsmail.replyTo = costum.admin.email;
				}

				ajaxPost(
					null,
					baseUrl+"/co2/mailmanagement/createandsend",
					paramsmail,
					function(data){ 
						$("#listEmailGrid").html("");
						$("#txtTo").val("");
						$("#txtObj").val("");
						toastr.success("Votre mail a été bien envoyé");
						dataHelper.path2Value(
						{
							id:costum.contextId,
							collection:costum.contextType,
							path:"allToRoot",
							value:listInvitation,
							// updateCache : true
						}, function(){});
					},
					function(data){
						toastr.error("Une problème s'est produite, envoie du mail est intérrompu");
					}
				);
			}else{
				toastr.warning("Votre email n'est pas envoyé. le mail n'a pas d'objet ou aucun déstinataire séléctionné ou vous n'avez pas écrit de message. Veuillez vérifier les champs et réessayer.");
			}
		});

		$("#txtMsg").on("change", function(){
			if($(this).val()!=""){
				$("#btnSend").attr("disabled", false);
			}else if(mailType!="invitation"){
				$("#btnSend").attr("disabled", true);
			}
		});

		})
</script>