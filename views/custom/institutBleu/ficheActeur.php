<?php 
$signature = Document::getListDocumentsWhere(
    array(
        "id"=> (String)$element["_id"],
        "type"=>'organizations',
        "subKey"=>'signature',
    ), "image"
);
$signaturePath  = "";
if($signature){
    foreach($signature as $ks => $vs){
        $signaturePath = $vs["imagePath"];

    }
}
?>
<style type="text/css">
    .ficheActeur body {
        background-color: #f5f7fb;
    }
    .ficheActeur .title-subtitle {
        text-align: center;
    }
    .ficheActeur div[class^="illustration-border"] {
        position: absolute;
        background-color: #c5d117;
    }
    .ficheActeur .card-title a{
        margin-top: 5px;
        margin-bottom: 5px;
        text-transform: uppercase;
        font-size: 20px;
        font-weight: 700;
        text-decoration: underline;
    }
    .ficheActeur .card-subtitle {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 25px;
        /* text-transform : uppercase; */
        font-weight: 700;
    }
    .ficheActeur .illustration-border-left-1 {
        width: 4px;
        height: 60px;
        top: 0;
        left: 0;
    }
    .ficheActeur .illustration-border-left-2 {
        width: 10%;
        height: 4px;
        top: 0;
        left: 0;
    }
    .ficheActeur .illustration-border-right-1 {
        width: 4px;
        height: 60px;
        bottom: 0;
        right: 0;
    }
    .ficheActeur .illustration-border-right-2 {
        width: 10%;
        height: 4px;
        bottom: 0;
        right: 0;
    }
    .ficheActeur .fiche-img {
        width: 100%;    
        height: 300px;
        object-fit: contain;
        object-position: center;
    }
    .ficheActeur .well-fiche.text-blue-fiche p {
        font-size: 20px !important;
    }
    .ficheActeur .fiche-parag {
        color: #093081;
        font-size: 25px;
        margin: 15px 0px;
        text-align: center;
    }
    .ficheActeur .bg-blue-fiche {
        background-color: #90b2f8;
    }
    .ficheActeur .bg-white {
        background-color: #ffffff;
    }
    .ficheActeur .card-title.text-blue-fiche a,
    .ficheActeur .text-blue-fiche {
        color: #093081;
    }
    .ficheActeur .fiche-logo{
        text-align: right;
    }
    .ficheActeur .fiche-logo,
    .ficheActeur .well-fiche {
        margin: 15px 0px;
        font-size: 20px;
        border-radius: 15px;
        word-break: break-word;
    }
    .ficheActeur .label-fiche {
        border-radius: 0px; 
        padding: 5px 60px;
        position: relative;
        min-width: 45%;
        text-align: center;
        display: inline-block;
        margin-bottom: 25px;
        font-size: 20px;
        margin-top:5px;
    }
    .ficheActeur .well-fiche .read-more {
        display: block;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 50%;
        background: #fff;
        color: #90b2f8;
        border: 1px solid #e7e7e7;
        font-size: 18px;
        margin: 0 auto;
        position: absolute;
        top: -9px;
        left: 0;
        right: 0;
        transition: all 0.3s ease-out 0s;
        text-align: center;
        font-weight: 700;
        text-decoration: none;
    }
    .ficheActeur .fiche-logo .logo {
        height: 100px;
        margin: auto;
        margin-bottom: 5px;
        object-fit: contain;
    }
    .ficheActeur .fiche-logo {
        /* text-align: center; */
    }
    .ficheActeur .fiche-logo .description {
        font-weight: 700;
        margin-top: 20px;
    }

    @media (max-width:767px)  {
        .ficheActeur .card-title a{
            font-size: 14px;
        }
        .ficheActeur .card-subtitle {
            font-size: 18px;
        }
        .ficheActeur .fiche-parag {
            font-size: 20px;
        }
        .ficheActeur .well-fiche {
            font-size: 15px;
        }
        .ficheActeur .label-fiche {
            padding: 5px 10px;
            margin-bottom: 20px;
            font-size: 14px;
        }
        .ficheActeur .well-fiche .read-more {
            top: -23px;
        }
        .ficheActeur .fiche-themetique {
            margin-top: 30px;
        }
        .ficheActeur .fiche-logo .logo {
            height: 70px;
        }
    }
    .ficheActeur .btnClass .btnstyle {
        margin-bottom : 2px;
        border: 2px solid #90b2f8;
        font-size : 16px;
    }
    .ficheActeur .btnClass .green {
        margin-bottom : 2px;
        border: 2px solid #34a853;
        font-size : 16px;
    }
    .ficheActeur .btn-delete {
        border: 2px solid red;
        margin-bottom : 2px;
        color: red;
        font-size : 16px;
    }
    .ficheActeur .btnClass{
        margin-top:10px;
    }
    .ficheActeur.tag-fiche-metier{
        font-size: 15px;
        
    }
    .link-fiche{
        text-decoration: underline;
        color: #093081;
    }

    .close-modal, .close-modal:hover, .btn-close a, a[data-dismiss="modal"], a[data-dismiss="modal"]:hover{
        background-color: #093081 !important;
        color:white !important;
        padding: 5px 10px;
        text-decoration: none;
        font-size: 14px;
        border-radius: 5px;
    }
    .portfolio-modal .modal-content{
        text-align: inherit;
    }
    .otherSociaNetworks a{
        font-size: 40px;
        margin-left: 10px;
        color: #093081;
    }
</style>
<div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 ficheActeur ">
    <div class="col-xs-12 no-padding  margin-bottom-15 text-right btn-close close-container">
         <a class="btn btn-default initialisSearch"><i class="fa fa-arrow-left"></i> Retour</a>
        <!--hr class="separator" style="margin-top: 4px;margin-bottom: 0px;border: 0;border-top: 1px solid #4285f4;"-->
    </div>
    <div class="row margin-top-50">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="title-subtitle col-xs-12">
                <div class="illustration-border-left-1"></div>
                <div class="illustration-border-left-2"></div>
                <div class="card-subtitle text-blue-fiche">
                    <?php 
                        echo "Présentation de l'acteur : ".(isset($element["acronym"])?$element["acronym"] : '')." (".$element["name"]." )"; 
                    ?>
                </div>
                <div class="card-title text-blue-fiche">
                    <?php if (isset($element["thematic"])) {
                        if(is_array($element["thematic"])){
                            foreach($element["thematic"] as $kjobf => $vjobf){
                                echo "<a href='#search?thematic=".$vjobf."' class='lbh'>".$vjobf."</a></br>";
                            }
                        }else {
                            echo "<a href='#search?thematic=".$element["thematic"]."' class='lbh'>".$element["thematic"]."</a></br>";
                        }
                    }?>
                </div>
                
                <div class="illustration-border-right-1"></div>
                <div class="illustration-border-right-2"></div>

                
            </div>
            <div class="fiche-parag text-blue-fiche col-xs-12">
                <div class="list-tag">
                    <?php if(isset($element["tags"])){
                        if(isset($element["tags"])){
                            foreach($element["tags"] as $k => $tag){
                                if(in_array($tag, array_keys($this->costum["lists"]["family"]))){
                                    unset($element["tags"][$k]);
                                }
                            }
                        }
                        $tagss = array_unique($element["tags"]);
                        foreach($tagss as $kTags => $vTags){?>
                            <!-- <a href = "#search?thematic=<?=$vTags?>" class="lbh" > -->
                                <span class="btn-tag  bg-blue-fiche text-blue-fiche badge tag-fiche-metier btn-tag tag">
                                    #<?=$vTags?>
                                </span>
                            <!-- </a> -->
                        <?php }
                    } ?>
                </div>
                <div class="col-xs-12 btnClass">
                    <!-- <a href="javascript:;" onclick="window.open('<?php echo Yii::app()->createUrl("/costum").'/institutbleu/fichepdf/id/'.(String)$element["_id"].'/type/organizations' ?>', '_blank').focus();" data-url="<?php echo Yii::app()->createUrl("/costum").'/institutbleu/fichepdf/id/'.(String)$element["_id"].'/type/organizations' ?>" class="btn  pull-left btnstyle letter-blue margin-right-10" ><i class="fa fa-file-pdf-o"></i> Télécharger le pdf </a>   -->
                    <!-- btn accept acteur -->
                    <?php if(Authorisation::isInterfaceAdmin() && isset($element["preferences"]["toBeValidated"][$this->costum["slug"]])){?>
                        <a href="javascript:;" data-id='<?= (string)$element["_id"]?>' data-type="organizations" data-email="<?= $element["email"]??'' ?>" class=" acceptActor btn bg-green-k text-white btn  pull-left green margin-right-10"><i class="fa fa-check-circle"></i>  Valider la fiche </a>	
                    <?php } ?>	

                    <!-- btn édition -->
                    <?php if ( isset($element["collection"]) && $element["collection"] == "organizations" && Authorisation::isInterfaceAdmin() || Authorisation::isElementAdmin((string)$element["_id"], @$element["collection"], Yii::app()->session['userId'])/* && (!isset($element["category"]) || (isset($element["category"]) && $element["category"] == "acteurInsitutBleu"))*/){?>
                        <a href="javascript:;" class="btn-edit-elementnoadmin pull-left btn margin-right-10 letter-blue btnstyle" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-pencil"></i> Modifier les informations</a>
                    <?php }?>

                    <!-- btn être membre -->
                    <!-- <?php //if(isset($element["links"]["members"][Yii::app()->session["userId"]]["isAdminPending"]) 
                            //&& isset($element["links"]["members"][Yii::app()->session["userId"]]["toBeValidated"])){?>
                        <p><i class="fa fa-info"></i> Votre demande d'être admin est en attente de validation</p>
                    <?php //} //else if(isset($element["links"]["members"][Yii::app()->session["userId"]]["isInviting"])){?>
                            <a class="btn letter-blue btnstyle acceptAsBtn pull-left" data-type="citoyens" data-id="<?php echo Yii::app()->session["userId"]?>" data-connect-validation="isInviting" data-parent-hide="2"><i class="fa fa-link"></i> Accepter l'invitation d'être admin de cette fiche</a>
                    <?php //} else if(Authorisation::isInterfaceAdmin() && isset($element["category"]) /*&& $element["category"] == "acteurInstitutBleu" */&& !isset($element["links"]["members"][Yii::app()->session["userId"]])){ ?>
                        <a href="javascript:links.connect('organizations','<?= (string)$element["_id"]?>','<?= Yii::app()->session["userId"]?>','citoyens','member',`<?= $element["name"] ?>`,true,null,'SOUHAITEZ-VOUS ÊTRE ADMINISTRATEUR DE LA FICHE','Merci de confirmer votre demande ')" class="pull-left btn margin-right-10 letter-blue btnstyle"> 
                            <i class="fa fa-link"></i> Être membre de l'organisation
                        </a>
                    <?php //}			 
                    ?> -->
                    <!-- Btn voir autre membre-->
                    <!-- <?php if(Yii::app()->session["userId"]){?>
                        <a href="javascript:;" class="showMembers btnstyle letter-blue pull-left btn margin-right-10" data-isadmin="" data-isadminpending = "<?= Meir::isAdminPending(Yii::app()->session["userId"],$element) ?>" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations" data-name="<?= $element["name"] ?>" data-slug="<?= $element["slug"] ?>"><i class="fa fa-eye"></i> Découvrez les membres de l'organisation </a>
                    <?php } ?> -->
                    <!-- btn supprimer -->
                    <?php if(Authorisation::isInterfaceAdmin() && isset($element["source"]["key"]) && $element["source"]["key"] == $this->costum["slug"]){?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="delete" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }else if(Authorisation::isInterfaceAdmin() && isset($element["reference"]["costum"]) && array_search($this->costum["slug"], $element["reference"]["costum"]) !== false ){?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="noadmin" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }else if(Authorisation::isInterfaceAdmin() && isset($element["source"]["keys"]) && array_search($this->costum["slug"], $element["source"]["keys"]) !== false  && !isset($element["preferences"]["toBeValidated"][$this->costum["slug"]])){?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="withsource"  data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }else if(Authorisation::isInterfaceAdmin() && isset($element["source"]["key"]) && $element["source"]["key"] == $this->costum["slug"] && isset($element["preferences"]["toBeValidated"][$this->costum["slug"]]) && $element["preferences"]["toBeValidated"][$this->costum["slug"]] == "true") {?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="delete" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }else if(!Authorisation::isInterfaceAdmin() && isset($element["creator"]) && Yii::app()->session["userId"] == $element["creator"] && isset($element["source"]["key"]) && $element["source"]["key"] == $this->costum["slug"]) {?>     
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="delete" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }else if(Authorisation::isInterfaceAdmin()){?>
                        <a href="javascript:;" class="delete btn-delete pull-left btn margin-right-10 " data-deletetype="noadmin" data-id="<?php echo (string)$element["_id"] ?>" data-type="organizations"><i class="fa fa-trash"></i> Supprimer </a>
                    <?php }?>
                </div>	
            </div>
            <?php if(isset($element["address"]) ||isset($element["telephone"]["mobile"]) || isset($element["email"]) ||isset($element["link"])){?>
                <div class="well well-fiche col-xs-12 text-blue-fiche bg-blue-fiche" >

                    <?php if (isset($element["address"])){?>
                        <b>Adresse du siège : </b> 
                            <?php if (isset($element["address"]["streetAddress"] ))	
                                echo $element["address"]["streetAddress"];
                            if(isset($element["address"]["postalCode"]))
                                echo " ".$element["address"]["postalCode"]." ";	
                            echo $element["address"]["addressLocality"];	
                            ?><br>
                    <?php }?>	
                    <?php if (isset($element["addresses"])){?>
                        <b>Autre(s) adresse(s) : </b> 
                            <?php foreach($element["addresses"] as $address){?>
                            <?php echo "<br> -";
                                 if (isset($address["address"]["streetAddress"] ))	
                                        echo " ".$address["address"]["streetAddress"];
                                    if(isset($address["address"]["postalCode"]))
                                        echo " ".$address["address"]["postalCode"];	
                                    echo " ".$address["address"]["addressLocality"];			
                                ?>
                        <?php }?>			
                    <?php }?>		
                    <?php if (isset($element["telephone"]["mobile"]) && $element["telephone"]["mobile"] != " "){ ?>
                        <b>Téléphone : </b> <?php 
                            $replaced = str_replace(' ', '', $element["telephone"]["mobile"][0]);
                            if (strpos($element["telephone"]["mobile"][0],"+") !== false){
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 4),
                                substr($replaced, 4, 1),
                                substr($replaced, 5, 2),
                                substr($replaced, 7, 2),
                                substr($replaced, 9, 2),
                                substr($replaced, 11));

                            } else{
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 2),
                                substr($replaced, 2, 2),
                                substr($replaced, 4, 2),
                                substr($replaced, 6, 2),
                                substr($replaced, 8, 2),
                                substr($replaced, 10));
                            }
                            echo $result; ?>
                        <br>
                                
                    <?php } ?>
                    
                    <?php if (isset($element["email"])) {?>
                        <b>Mail : </b><?php 
                        echo $element["email"]; ?>
                        <br>
                    <?php } ?>
                    <?php if (isset($element["link"])){
                        if(!str_contains($element["link"], "http") && $element["link"]!=""){
                            $element["link"] = "https://".$element["link"];
                        }
                        ?>
                            <b>Lien vers le site internet : </b>  <a class="link-fiche" href="<?php echo $element['link']?>" target="_blank"> 
                                <?php echo $element["link"]; ?>
                            
                        </a> 
                        <br>
                    <?php } ?>
                </div>
            <?php } ?> 
        </div>
        <?php 
        if(isset($element["profilImageUrl"])) 
            $src = $element["profilImageUrl"];
        else 
            $src =  Yii::app()->getModule("costum")->assetsUrl."/images/meir/TIERS-LIEUX.png";
        ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <img  class="img-responsive fiche-img" src = "<?= $src?>">
            <?php if(isset($element["legalStatus"]) || isset($element["objectiveOdd"])){?>
                <div class="well well-fiche text-blue-fiche bg-white" >
                    <?php  if (isset($element["legalStatus"])) {?>
                        <b>Forme juridique :</b>
                            <br>
                            <?php echo $element["legalStatus"];  ?>
                        <br><br>
                    <?php } ?> 
                    <?php  if (isset($element["operatingLocation"])) {?>
                        <b>Lieu d’exploitation :</b>
                            <br>
                            <?php echo $element["operatingLocation"];  ?>
                        <br><br>
                    <?php } ?> 
                    <?php  if (isset($element["objectiveOdd"])) {?>
                        <b>Objectif(s) ONUSIEN de Développement Durable :</b>
                            <br>
                        <?php if (isset($element["objectiveOdd"])) {
                            if(is_array($element["objectiveOdd"])){
                                foreach($element["objectiveOdd"] as $kthem => $vthem){
                                    echo "- " .$vthem."</br>";
                                }
                            }else {
                                echo $element["objectiveOdd"];
                            }
                        }?>
                    <?php } ?> 
                </div>
            <?php } ?> 
        </div>

    </div>


    <?php if (isset($element["description"])){?>
        <div class="row">
            <div class="col-xs-12">
                <div class="well well-fiche text-blue-fiche bg-white" >
                    <div class="text-center">
                        <span class="label label-fiche bg-blue-fiche"> Description de la structure et de ses activités</span>
                    </div>
                    <span class="markdown"><?php echo $element["description"]; ?></span>
                    </p>
                </div>
            </div>
        </div>     
    <?php }  ?>
    
    <div class="row">
        <?php if (isset($element["otherSociaNetworks"]) && $element["otherSociaNetworks"] != []) { 
            $countSN = 0;
            foreach($element["otherSociaNetworks"] as $kSocial => $vSocial){
                if(isset($vSocial["link"]) && $vSocial["link"] != ""){
                    $countSN++;
                }
            }
            if($countSN > 0){?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="well well-fiche text-blue-fiche bg-white" >
                        <div class="text-center">
                            <span class="label label-fiche bg-blue-fiche">
                                Réseaux sociaux
                            </span>
                            <div class="row text-center otherSociaNetworks">						
                                <?php foreach($element["otherSociaNetworks"] as $kSocial => $vSocial){
                                    $icon = "";
                                    if(stripos($vSocial["type"],"Facebook") !== false)
                                        $icon = "facebook";
                                    if(stripos($vSocial["type"],"linkedin") !== false)
                                        $icon = "linkedin";
                                    if(stripos($vSocial["type"],"Instagram") !== false)
                                        $icon = "instagram";
                                    if(stripos($vSocial["type"],"Twitter") !== false)
                                        $icon = "twitter";
                                ?>
                                    <?php if(isset($vSocial["type"]) && isset($vSocial["link"]) && $vSocial["link"] != "") {?>
                                        <a href="<?= $vSocial["link"]?>" class="tooltips" data-original-title='<?= $vSocial["type"]; ?>'  data-toggle="tooltip" data-placement="top"><i class="fa fa-<?= $icon?>"></i></a>
                                    <?php } ?>	
                                <!-- <div class="description">
                                        <?php if(isset($vSocial["type"]) && isset($vSocial["link"]) && $vSocial["link"] != "") {?>
                                            <p><i class="fa fa-<?= $icon?>"></i>  <?= $vSocial["type"]; ?>   : <a href="<?= $vSocial["link"]?>" target="_blank"><?= $vSocial["link"]?></a></p><br>
                                        <?php } ?>											
                                    </div> -->
                                <?php } ?>						
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>	
        <?php } ?>	
    </div>
    <!-- <?php if($signaturePath != "" ){?>
        <div class="row">			
            <div class="row">
                <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-8 fiche-logo pull-right text-blue-fiche">
                    <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
                        <?php  if (isset($element["locationSign"])) {?>
                            <b>Fait à:</b>
                                <?php echo $element["locationSign"];  ?>
                        <?php } ?>
                        <?php  if (isset($element["dateSign"])) {?>
                            <b>, le:</b>
                                <?php echo $element["dateSign"];  ?>
                            <br><br>
                        <?php } ?> 
                    </div>	
                    <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                            <img src="<?php echo $signaturePath ?>" class="img-responsive logo"><br>
                    </div>
                    <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
                        <?php  if (isset($element["surnameSign"])) {?>
                            <?php echo $element["surnameSign"];  ?>
                        <?php } ?>
                        <?php  if (isset($element["nameSign"])) {?>
                            <?php echo $element["nameSign"];  ?><br>
                        <?php } ?>  
                        <?php  if (isset($element["functionSign"])) {?>
                            <?php echo $element["functionSign"];  ?>
                        <?php } ?> 	
                    </div>
                </div>
            </div>
        </div>
    <?php } ?> -->

</div>
<script>
    jQuery(document).ready(function() {
        if(localStorage.getItem("reloadAfterSave")!=null){
            localStorage.removeItem("reloadAfterSave");
            location.reload();
        }
        $.each($(".markdown"), function(k,v){
        	descHtml = dataHelper.markdownToHtml($(v).html()); 
        	$(v).html(descHtml);
        });
        if(location.hash == "#mapping"){
            $(".btn-close").css("display","none")
        }
            
        setTitle("", "", costum.metaTitle);
        directory.bindBtnElement();
        $(".initialisSearch").on("click", function(){
            urlCtrl.loadByHash("#search");
        })
        $(".acceptActor").off().on("click", function(){
            var tplCtx = {};
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("type");
            let email = $(this).data("email");
            tplCtx.value = null;
            tplCtx.path = "preferences.toBeValidated."+costum.slug;
            dataHelper.path2Value(tplCtx, function(params) {
                toastr.success("Acteur ajouté"); 
                if(email && typeof email != "undefined" && email != "" ){
                    dataHelper.path2Value(
                        {
                            id:costum.contextId,
                            collection:costum.contextType,
                            path:"listInvitation."+slugify(email, email),
                            value: {
                                email : email,
                                status: "Accepté"
                            },
                            // updateCache : true
                        }, function(){
                            urlCtrl.loadByHash(location.hash);
                        }
                    );
                }
            } );
        });
        $(".ficheActeur .showMembers").off().on("click",function () {
            var isAdmin = <?= json_encode(Meir::isAdminFiche(Yii::app()->session["userId"],$element))?>;
            var isAdminPending = <?= json_encode(Meir::isAdminPending(Yii::app()->session["userId"],$element))?>;
            var isAdminInviting =<?= json_encode(Meir::isAdminInviting(Yii::app()->session["userId"],$element))?>;
            var type = $(this).data("type")
            var name = $(this).data("name")
            var id = $(this).data("id")
            var slug = $(this).data("slug")
            if(isAdmin){
                urlCtrl.loadByHash("#@"+slug+".view.directory.dir.members")
            }else if(isAdminPending){
                bootbox.confirm("Vous devez être administrateur pour accéder à ces informations !! <br>Votre demande d'être admin est en attente de validation",
                    function(result)
                    {
                        if (!result) {
                            return;
                        } else {
                            return;
                        }
                    }
                );
            }else if(isAdminInviting){
                bootbox.dialog({ 
                    message: "Vous devez être administrateur pour accéder à ces informations ! Veuillez accepter la demande !!",
                    buttons: {
                        success: {
                            label: "Accepter la demande",
                            className: "btn-primary",
                            callback: function () {
                                links.validate(type,id, userId,'citoyens','isInviting')
                            }
                        },
                        cancel: {
                            label: "Refuser la demande",
                            className: "btn-danger",
                            callback: function() {
                                links.disconnect(type,id,userId,'citoyens','members',null,'','Etes-vous sûr-e de refuser cette invitation')
                            }
                        }
                    }
                });
            }
            else{
                bootbox.dialog({ 
                    message: "Vous devez être administrateur pour accéder à ces informations !!",
                    buttons: {
                        success: {
                            label: "Dévenir admin",
                            className: "btn-primary",
                            callback: function () {
                                links.connect(type,id,userId,'citoyens','member',name,true,null,'SOUHAITEZ-VOUS ÊTRE ADMINISTRATEUR DE LA FICHE','Merci de confirmer votre demande ')
                            }
                        },
                        cancel: {
                            label: trad["cancel"],
                            className: "btn-secondary",
                            callback: function() {
                                $(".disconnectBtnIcon").removeClass("fa-spinner fa-spin").addClass("fa-unlink");
                            }
                        }
                    }
                });
            }
        })
        $(".ficheActeur .delete").off().on("click",function () {
            $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
            var btnClick = $(this);
            var id = $(this).data("id");
            var type = "organizations";
            var deleteType = $(this).data("deletetype");
            var params = [];
            var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
            if(deleteType == "noadmin" ){
                urlToSend = baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/reference";
                    params = {
                    id:  $(this).data("id"),
                    type: $(this).data("type"),
                    origin: "costum",
                    sourceKey: costum.slug,
                }
            }
            if(deleteType == "withsource" ){
                urlToSend = baseUrl+"/costum/institutbleu/deletewithsource";
                params = {
                    id:  $(this).data("id"),
                    type: $(this).data("type")
                }
            }
            if(deleteType == "delete" ){
                urlToSend =  baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
                params = null
            }
            bootbox.confirm("voulez vous vraiment supprimer cet acteur !!",
                function(result)
                {
                    if (!result) {
                        btnClick.empty().html('<i class="fa fa-trash"></i>  Supprimer');
                        return;
                    } else {
                        ajaxPost(
                            null,
                            urlToSend,
                            params,
                            function(data){
                                if ( data && data.result ) {
                                    ajaxPost(
                                        null,
                                        baseUrl+"/co2/link/disconnect",
                                        {
                                            childId : id,
                                            childType : "organizations",
                                            parentType :"organizations",
                                            parentId :costum.contextId,
                                            connectType : "members"
                                        }
                                    )
                                    toastr.success("acteur effacé");
                                    $("#"+type+id).remove();
                                    $(".close-modal").click()
                                    urlCtrl.loadByHash("#search");
                                } else {
                                    toastr.error("something went wrong!! please try again.");
                                }
                            }
                        );
                    }
                }
            );
        });

        var dyfOrgaNoAdmin = {
            "beforeBuild": {
            "properties" : {
                "infoGeneral" : {
                    "inputType" : "custom",
                    "html" : "<p class='text-left margin-bottom-0' style='font-size:16px;'><b>Votre structure a été identifiée comme appartenant au secteur de l’économie bleue et l’Institut Bleu souhaite recueillir votre approbation pour apparaître dans l’annuaire.</b></p>",
                    "order" : 1
                },
                "displayAuth" : {
                    "label" : "Vous souhaitez que votre structure et ses informations apparaissent dans l’annuaire de l’économie bleue développé par l’Institut Bleu.",
                    "inputType" : "checkboxSimple",
                    "params" : {
                        "onText" : "Oui",
                        "offText" : "Non",
                        "onLabel" : "Oui",
                        "offLabel" : "Non"
                    },
                    "checked" : true,
                    "rules" : {
                        "required" : true
                    },
                    "order" : 3
                },
                "sextionInfo" : {
                    "inputType" : "custom",
                    "html" : "<p class='text-left margin-bottom-0' style='font-size:16px;'><b>Si vous souhaitez que votre structure figure dans l’annuaire de l’économie bleue, veuillez indiquer ci-après les informations que vous souhaitez voir apparaître sur votre « page acteur ».</p> </hr>",
                    "order" : 4
                },
                "name" : {
                    "label" : "Nom de la structure",
                    "inputType" : "text",
                    "order" : 5
                },
                "similarLink" : {
                    "inputType" : "custom",
                    "order" : 6
                },
                "acronym" : {
                    "label" : "Acronyme",
                    "inputType" : "text",
                    "order" : 7
                },
                "image" : {
                    "label" : "Logo",
                    "order" : 8
                },
                "legalStatus" : {
                    "label" : "Forme juridique",
                    "inputType" : "tags",
                    "class" : "form-control",
                    "info" : "Si autre, vous pouvez écrire directement",
                    "tagsList" : "legalStatus",
                    "maximumSelectionLength" : 1,
                    "order" : 9
                },
                "description" : {
                    "label" : "Description de la structure et de ses activités",
                    "inputType" : "textarea",
                    "markdown" : true,
                    "order" : 10
                },
                "formLocality" : {
                    "label" : "Adresse du siège",
                    "order" : 11
                },
                "location" : {
                    "order" : 12
                },
                "operatingLocation" : {
                    "label" : "Lieu d’exploitation",
                    "order" : 13
                },
                "mobile" : {
                    "label" : "Téléphone",
                    "inputType" : "text",
                    "placeholder" : "Téléphone",
                    "order" : 14
                },
                "email" : {
                    "label" : "Mail",
                    "placeholder" : "Adresse(s) électronique(s) de l’organisme",
                    "order" : 15
                },
                "link" : {
                    "label" : "Lien vers le site internet",
                    "inputType" : "text",
                    "order" : 16
                },
                "otherSociaNetworks" : {
                    "label" : "Réseaux sociaux",
                    "inputType" : "lists",
                    "entries" : {
                        "type" : {
                            "label" : "Nom du reseau social",
                            "type" : "text",
                            "class" : "col-md-6 col-sm-6 col-xs-12"
                        },
                        "link" : {
                            "label" : "Lien",
                            "type" : "text",
                            "class" : "col-md-5 col-sm-5 col-xs-12"
                        }
                    },
                    "value" : [ 
                        {
                            "type" : "Lien vers le profil LinkedIn",
                            "link" : ""
                        }, 
                        {
                            "type" : "Lien vers le profil Facebook",
                            "link" : ""
                        }, 
                        {
                            "type" : "Lien vers le profil Instagram",
                            "link" : ""
                        }, 
                        {
                            "type" : "Lien vers le profil Twitter",
                            "link" : ""
                        }
                    ],
                    "order" : 17
                },
                "thematic" : {
                    "label" : "Domaine(s) d'activité(s)",
                    "inputType" : "selectMultiple",
                    "placeholder" : "",
                    "class" : "multi-select",
                    "isSelect2" : true,
                    "list" : "family",
                    "order" : 18
                },
                "tags" : {
                    "label" : "mots-clés pour décrire plus en détail votre activité",
                    "order" : 19
                },
                // "signature" : {
                //     "inputType" : "custom",
                //     "html" : "<p class='text-left margin-bottom-0'><b>En signant ce formulaire :</b><br> - Je donne mon consentement libre, spécifique, éclairé et univoque, conformément à <a href='https://www.cnil.fr/fr/la-loi-informatique-et-libertes' class='' target ='_blank'> <u>la loi informatique et libertés </u></a> et aux <a href='https://www.cnil.fr/fr/les-bases-legales/consentement' class='' target ='_blank'> <u> conformités RGPD </u></a> , afin que les données indiquées dans le présent formulaire soient publiées dans l’annuaire de l’économie bleue, sur le site internet de l’Institut Bleu (<a href='www.institutbleu.re/' class='' target ='_blank'> <u>www.institutbleu.re</u></a>). <br>- Je suis conscient que je peux retirer mon consentement à tout moment par simple demande à l’adresse <a href:'mailto:contact@institutbleu.re'><u>contact@institutbleu.re</u></a> et que les données seront supprimées par l’Institut Bleu dans les plus brefs délais. <br>- Je certifie que les informations sont exactes et complètes.</p>",
                //     "order" : 20
                // },
                // "infosign" : {
                //     "inputType" : "custom",
                //     "html" : "<h3 class='text-left margin-bottom-0'><i>Information du signataire <i></h3>",
                //     "order" : 21
                // },
                // "nameSign" : {
                //     "inputType" : "text",
                //     "label" : "NOM",
                //     "order" : 22
                // },
                // "surnameSign" : {
                //     "inputType" : "text",
                //     "label" : "PRENOM",
                //     "order" : 23
                // },
                // "functionSign" : {
                //     "inputType" : "text",
                //     "label" : "FONCTION",
                //     "order" : 22
                // },
                // "locationSign" : {
                //     "inputType" : "text",
                //     "label" : "Fait à",
                //     "order" : 23
                // },
                // "dateSign" : {
                //     "inputType" : "date",
                //     "label" : "Le",
                //     "order" : 26
                // },
                // "sign" : {
                //     "label" : "Signature",
                //     "inputType" : "uploader",
                //     "docType" : "image",
                //     "showUploadBtn" : false,
                //     "template" : "qq-template-gallery",
                //     "filetypes" : [ 
                //         "jpeg", 
                //         "jpg", 
                //         "gif", 
                //         "png"
                //     ],
                //     "contentKey" : "slider",
                //     "endPoint" : "/subKey/signature",
                //     "domElement" : "signature",
                //     "itemLimit" : 1,
                //     "order" : 24
                // },
                // "category" : {
                //     "inputType" : "hidden",
                //     "value" : "acteurInstitutBleu"
                // },
                "type" : {
                    "value" : "LocalBusiness"
                },
                "role" : {
                    "value" : "admin"
                }
            }
            },
            "afterSave": "costum.institutBleu.organizations.afterSave",
            "beforeSave": "costum.institutBleu.organizations.beforeSave",
            "afterBuild": {},
            "onLoads": {
                "onload": {}
            },
            "onload": {
                "actions": {
                    "setTitle": "Mofification de l'information de la structure",
                    "html" : {
                        "infocustom" : ""
                    },
                    "hide": { 
                        "urltext" : 1,
                        "typeselect" : 1,
                        "shortDescriptiontextarea" : 1,
                        "roleselect" : 1
                    }
                }
            }
        }
        $(".ficheActeur .btn-edit-elementnoadmin").off().on('click',function(){
            var id = $(this).data("id");
            var type = $(this).data("type");
            $("#ajax-modal").addClass(costum.slug+"Modal");
            $(".mainDynFormCloseBtn , .close-modal").off("click").on("click" , function(){
                $("#ajax-modal").removeClass(costum.slug+"Modal");
            })
            dyFObj.editElement('organizations',id);
        });
    })
</script>