<?php 

$auth = (Authorisation::canEditItem(@Yii::app()->session["userId"], $element["collection"], $element["_id"])) ? true : false;
?>
<style>
	.section-badges{
		background-color: unset !important;
	}

	.menu-btn-follow{
		display:none !important;
	}

	.boxBtnLink{
		background-color: #FF286B;
		border:solid 1px #fff;
	}

	/*.boxBtnLink:hover{
		opacity:0.75;
	}*/
	.menu-linksBtn:hover{
		background-color: unset;
		opacity:unset;
	}
	.header-address-tags .badge {
		margin: 5px;
		width: fit-content;
		height: 20px;
		background: #ea4335;
		color: #fff;
	}
	.header-address-tags {
		height: 100%;
		display: flex;
		align-items: end;
	}
	.header-tags .badge {
		margin: 5px;
		width: fit-content;
		height: 20px;
		background: #fff;
	}
	@media (min-width: 768px) {
		.header-tags .badge {
			margin-left: 3px;
			font-size: 13px;
		}
	}

	.banner-outer {
		background-color: #f9f9f9;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-right-bg.png' ?>"); 
		background-position: bottom right;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	@media screen and (min-width: 1360px) {
		.basic-banner {
			background-size: auto auto;
		}
	}
	@media screen and (min-width: 768px) {
		.basic-banner {
			background-size: 15% auto;
		}
	}

	#social-header .basic-banner {
		padding-top: 70px;
		padding-bottom: 130px;
		background-image: url("<?= Yii::app()->controller->module->assetsUrl.'/images/templ-banner-left-bg.png' ?>");
		background-position: top left;
		background-repeat: no-repeat;
		background-size: 25% auto;
	}

	.container-banner.max-950 {
		max-width: 980px;
	}

	.container-banner:before {
		display: table;
		content: " ";
	}

	.text-center {
		text-align: center;
	}

	.container-banner:after {
		clear: both;
		display: table;
    	content: " ";
	}

	@media (min-width: 1200px) {
		.container-banner {
			width: 1170px;
		}
	}

	@media (min-width: 992px) {
		.container-banner {
			width: 970px;
		}
	}

	@media (min-width: 768px) {
		.container-banner {
			width: 750px;
		}
	}

	#social-header .basic-banner-inner {
		display: flex;
    	justify-content: center;
	}

    .title-banner h1::before {
        content: "";
        position: absolute;
        z-index: -1;
        width: 2em;
        height: 2em;
        background-color: var(--color3);
        border-radius: 50%;
        top: -50%;
    }

	.contentHeaderInformation {
    	background-color: transparent !important;
		height: -webkit-fill-available;
	}

	.link-banner {
		margin: 0px 5px;
		font-weight: bold;
    	color: white;
	}

	.social-main-container, #central-container{
		min-height: 0px !important;
	}

	.btn-menu-citoyen{
		padding: 50px;
    	display: inline-table; 
    	padding: 5px 22px; 
    	font-size: 22px; 
    	margin-bottom: 50px !important; 
    	font-weight: 500; 
    	text-transform: uppercase; 
    	border-bottom: 1px solid #cccccc !important;
	}


	.bg-ekitia{
		background-color: var(--primary-color) !important;
	}

	a.ssmla.btn-menu-tooltips.active{
		font-size: 22px;
		margin-bottom: 50px !important;
		color: var(--primary-color) !important;
		border-bottom: 4px solid var(--primary-color) !important;
		font-weight: 500;
		text-transform: uppercase;
	}
	a.ssmla.btn-menu-tooltips {
		display: inline-table;
		padding: 5px 22px;
		font-size: 22px;
		margin-bottom: 50px !important;
		font-weight: 500;
		text-transform: uppercase;
		border-bottom: 1px solid #cccccc !important;
	}
	.item-inner .thematic {
		background-color: #01305e;
		padding: 3px;
		border-radius: 0%;
		font-size: 16px;
		color: white;
	}
	.status{
		position: absolute;
		right: 4%;
		background: green;
		color: white;
		padding: 3px;
		color: #fff;
		background: #93C020;
		font-weight: 700;
		font-size: 14px;
	}
	.link-bordered {
		display: flex;
		flex-wrap: wrap; 
		gap: calc((.10rem)* 4);
	}
</style>

<div class="banner-outer ">
    <div class="basic-banner">
        <div class="basic-banner-inner">
            <div class="container-banner max-950"> 
                <div class="basic-info text-center">
                    <div class="title-banner">
                        <h1 class="font-weight-100" style="position: sticky;"><?php echo $element["name"]; ?></h1>
                    </div>                         
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
	<!-- <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-white pull-right margin-bottom-5">
			<?php if (@$element["status"] == "deletePending") { ?> 
				<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
			<?php } ?>
			<h4 class="text-left padding-left-15 pull-left no-margin">
				<span id="nameHeader">
					<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
						
					</div>
					<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
					<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
				</span>
				<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
						$typesList=Event::$types;
				?>
					<span id="typeHeader" class="margin-left-10 pull-left">
						<i class="fa fa-x fa-angle-right pull-left"></i>
						<div class="type-header pull-left">
					 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
					 	</div>
					</span>
				<?php } ?>
			</h4>			
	</div> -->
<?php 
	$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" );

	if(!empty($cteRParent))
		$classAddress = "";
?>
	<div class="header-address-tags col-xs-12 col-sm-9 col-md-9 col-lg-9 pull-right margin-bottom-5 <?php echo $classAddress ; ?>">
		<div class="address-tags-content">
			<?php /*
			if(!empty($element["address"]["addressLocality"])){ ?>
				<div class="header-address badge letter-white bg-red margin-left-5 pull-left col-xl-12">
					<?php
						echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
						echo !empty($element["address"]["postalCode"]) ? 
								$element["address"]["postalCode"].", " : "";
						echo $element["address"]["addressLocality"];
					?>
				</div>
			<?php
			} */ ?>	

			<div class="header-tags pull-left  col-xs-12" style="margin-top: 10px;" >

				<?php

				if(!empty($element["tags"])){
					$countTags=count($element["tags"]);
					$nbTags=1;
					foreach ($element["tags"]  as $key => $tag) {
						if($nbTags < 8){
							echo '<span class="badge tag-banner-element letter-red bg-white" style="margin-top: 3px;"><i class="fa fa-hashtag"></i>&nbsp;'.$tag.'</span>';
							$nbTags++;
						}else{
							if(($countTags-$nbTags) > 0) echo "<span class='badge indicate-more-tags letter-red bg-white'>+".($countTags-$nbTags)."</span>";
							break;
						}
					}
				} ?>
			</div>
			<!-- div>
				<a href="javascript:;" class="btn btn-info bg-white ssmla" data-view="settings" style="text-transform:uppercase">
					<i class="fa fa-cogs"></i> Mes Paramètres
				</a >
			</div -->
		</div>
	</div>

<?php 
	echo $this->renderPartial('co2.views.element.menus.answerInvite', 
    			array(  "invitedMe"      => $invitedMe,
    					"element"   => $element
    					) 
    			); 
 	?>
</div>

<script>
	var elem = <?= json_encode($element) ?>;
	var auth = <?= json_encode($auth) ?>;

	$(function(){
		var banner = {
			init: function() {
				this.views.init();
				this.events.init();
				if(elem.collection == "citoyens"){
					$('a[data-view="directory"]').hide();
					$('a[data-view="newspaper"]').hide();
					$('a[data-view="detail"]').hide();
				}
			},
			events: {
				init: function () {
					banner.events.createFormsEvent();
				},
				createFormsEvent: function () {
					$(".ssmlCreate").off().on("click", function(){
						$("#div-select-create").show(200);
						banner.views.ameliorateCreateView("#div-select-create");
						$("#central-container").hide(200);
						$("#btn-close-select-create").click(function() {
							$("#central-container").show(200);
						});
						
						$(".ssmla.elementButtonHtml").on("click", function(){ 
							if (!$("#central-container").is(":visible")) {
								$("#central-container").show(200);
							}
						})
					});
				}
			},
			views : {
				init: function () {
					// banner.views.ameliorateCreateView();
				},
				ameliorateCreateView: function(contentId) {
					$(contentId + " h4.text-center img").hide();
					$(contentId + " h4.text-center .name-header").hide();
					$(contentId + " h4.text-center > i").hide();
					$(contentId + " .elementCreateButton .addBtnFoot").attr("class", "addBtnFoot btn btn-default col-xs-12 col-sm-4 col-md-2 col-lg-3 uppercase btn-open-form")
						.attr("style","padding-top: 4%;padding-bottom: 4%;");
					$(contentId + " .elementCreateButton .addBtnFoot i").attr("style", "color: red !important;background-color: #ffffff !important;border: 1px solid;");
					$(contentId + " .elementCreateButton .addBtnFoot span").attr("style", "font-size: 16px; font-weight: 600;");
					$(contentId + " .elementCreateButton").attr("style", "display: flex; flex-wrap: wrap; justify-content: space-around;margin: 2% 0%;");
					$(contentId + " .btn-link").attr("style", "padding: 0px");
					$(contentId + " .shadow2").removeClass("bg-white").attr("style", "background-color: #f9f9f9;");
					// addBtnFoot btn btn-default col-xs-12 col-sm-4 col-md-2 col-lg-2 uppercase btn-open-form
					// $(contentId + " h4.text-center").text("PUBLIER DU CONTENU SUR CETTE PAGE");
				}
			}
		};
		banner.init();
	})

	$(document).ready(function(){
		if(typeof elem!="undefined" && elem.collection == "citoyens" && typeof elem.slug != "undefined"){
			$("#menu-top-btn-group").append(`
				<a  href="#@${elem.slug}.view.detail" data-view = "detail" class="elementButtonHtml ssmla btn btn-default btn-menu-tooltips  place-align hidden-xs" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">Aller sur communecter</span>
					<span>A propos</span>
				</a>
				<a href="#@${elem.slug}.view.structure" data-view = "structure" class="elementButtonHtml ssmla btn btn-default btn-menu-tooltips  place-align hidden-xs" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">Aller sur communecter</span>
					<span>Tableau de bord</span>
				</a>
			`);

			// <a href="#@${elem.slug}.view.settings" class="btn-menu-citoyen lbh btn btn-default btn-menu-tooltips place-align hidden-xs border-bottom-ekitia" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
			// 		<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">Aller sur communecter</span>
			// 		<span>Gérer mes paramètres</span>
			// 	</a>
		}
		if(auth){
			if(typeof elem !="undefined" && typeof elem.category !="undefined" && elem.category == "formation" ){
				$("#menu-top-btn-group").append(`<a href="javascript:dyFObj.editElement('${elem.collection}', '${elem._id.$id}','formation');"class="btn-menu-citoyen btn btn-default btn-menu-tooltips place-align hidden-xs letter-blue btnstyle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span><i- class="fa fa-pencil"></i>  Editer la formation</span>
				</a>`);
			}
			if(typeof elem !="undefined" && typeof elem.tags != "undefined" && (elem.tags).indexOf("Centre de formation") > -1){
				$("#menu-top-btn-group").append(`<a href="javascript:dyFObj.editElement('organizations', '${elem._id.$id}','organismeFormation');"class="btn-menu-citoyen btn btn-default btn-menu-tooltips place-align hidden-xs letter-blue btnstyle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span><i- class="fa fa-pencil"></i>  Editer</span>
				</a>`);
			}
			if(typeof elem !="undefined" && typeof elem.tags != "undefined" && (elem.tags).indexOf("TiersLieux") > -1){
				$("#menu-top-btn-group").append(`<a href="javascript:dyFObj.editElement('organizations', '${elem._id.$id}','tiersLieux');"class="btn-menu-citoyen btn btn-default btn-menu-tooltips place-align hidden-xs letter-blue btnstyle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span><i- class="fa fa-pencil"></i>  Editer</span>
				</a>`);
			}
			if(typeof elem !="undefined" && typeof elem.tags != "undefined" && (elem.tags).indexOf("Formateur") > -1){
				$("#menu-top-btn-group").append(`<a href="javascript:dyFObj.editElement('citoyens', '${elem._id.$id}','citoyens');"class="btn-menu-citoyen btn btn-default btn-menu-tooltips place-align hidden-xs letter-blue btnstyle" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="">
					<span><i- class="fa fa-pencil"></i>  Editer</span>
				</a>`);
			}
		}
		
	})
</script>

