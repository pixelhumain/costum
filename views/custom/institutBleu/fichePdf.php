<?php 
$signature = Document::getListDocumentsWhere(
    array(
        "id"=> (String)$element["_id"],
        "type"=>'organizations',
        "subKey"=>'signature',
    ), "image"
);
$signaturePath  = "";
if($signature){
    foreach($signature as $ks => $vs){
        $signaturePath = $vs["imagePath"];

    }
} ?>
<style type="text/css">
     .well-fiche {
        font-size: 12px;
        border-radius: 15px;
        word-break: break-word;
    }
    .label-fiche-title{
        text-align: center;
        color :  #275283;
        font-weight : bold;
        font-size: 14px;

    }
    .label-fiche1{
        color :  #093081;
        font-weight : bold;
        font-size : 14px;
    }
    a {
       color : #275283;
    }
    .fichePdf{
        background-color :#f8f8f8;
    }
    .text-blue-fiche{
        color: #093081;
        font-size : 12px;
        font-weight : 700;
    }
</style>
<div class="fichePdf"> 
    <table   cellspacing="2" cellpadding="2" >
        <tr>
            <td colspan="2" >
                
                <p class="text-blue-fiche" style="font-size:20px;text-align:center" ><b><i><?=$element["name"]?></i></b></p>
                <div>
                    <?php if (isset($element["thematic"])) {
                        if(is_array($element["thematic"])){
                            foreach($element["thematic"] as $kjobf => $vjobf){ ?> 
                               <span class="text-blue-fiche "> <b><?php echo $vjobf ;?></b></span><br>
                                <?php
                            }
                        }else {?>
                            <span class="text-blue-fiche "> <?php echo $element["thematic"];?></span>  
                        <?php }
                    }       
                    ?>

                    <?php if(isset($element["thematic"]) && isset($element["category"]) && $element["category"] == "startup"){?>
                        
                            <?php if(is_array($element["thematic"])){
                                foreach($element["thematic"] as $kjobf => $vjobf){?>
                                    <span class="text-blue-fiche"> <?= $vjobf?></span>
                                <?php }?>
                            <?php }else {?>
                                <span class="text-blue-fiche"> <?= $element["thematic"]?></span>                           
                            <?php } ?>
                    <?php } ?>
                </div>                    
            </td>
            
            <td colspan="2" style="text-align:right;float:right">
                <br><br>
            <?php 
                if(isset($element["profilImageUrl"])) 
                    $src = $element["profilImageUrl"];
                else 
                    $src =  Yii::app()->getModule("costum")->assetsUrl."/images/meir/TIERS-LIEUX.png";
                ?>
                <img  style="height: auto; width: auto;object-fit: contain;object-position: center; " src = "<?= $src?>">
            </td>            
        </tr>
        <tr>
            <td colspan="2" style=" background-color: #90b2f8 ;">
                    <p class ="text-blue-fiche"><b>Adresse :</b>
                        <?php if (isset($element["address"])){?> 
                            <?php if (isset($element["address"]["streetAddress"] ))	
                                    echo $element["address"]["streetAddress"]." ";
                                echo $element["address"]["postalCode"]." ".$element["address"]["addressLocality"]."<br>"; 
                            ?> 

                        <?php } ?>
                                			
                        <?php if (isset($element["addresses"])) { ?>
                            <br><b>Adresse du lieu d'exploitation :</b>
                        <?php
                            foreach($element["addresses"] as $address){
                                if (isset($address["address"]["streetAddress"] ))	
                                    echo $address["address"]["streetAddress"];
                                echo " ".$address["address"]["postalCode"]." ".$address["address"]["addressLocality"];
                            }
                        }?>     
                     </p>
                            
                    <?php  if (isset($element["telephone"])){?>
                        <p class ="text-blue-fiche"><b>Téléphone :</b>  <?php
                            $replaced = str_replace(' ', '', $element["telephone"]["mobile"][0]);
                            if (strpos($element["telephone"]["mobile"][0],"+") !== false){
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 4),
                                substr($replaced, 4, 1),
                                substr($replaced, 5, 2),
                                substr($replaced, 7, 2),
                                substr($replaced, 9, 2),
                                substr($replaced, 11));

                            } else{
                                $result = sprintf("%s %s %s %s %s %s",
                                substr($replaced, 0, 2),
                                substr($replaced, 2, 2),
                                substr($replaced, 4, 2),
                                substr($replaced, 6, 2),
                                substr($replaced, 8, 2),
                                substr($replaced, 10));
                            }
                                
                            echo $result; 
                       
                        ?>
                    </p>
                    <?php } ?>
                    <?php if (isset($element["email"])) { ?>
                        <p class ="text-blue-fiche"><b>Mail :</b> <?php 
                        echo $element["email"]; ?> </p>
                    <?php } ?>
                    <?php if (isset($element["link"])) { ?>
                        <p class ="text-blue-fiche"><b>Lien vers le site internet: </b>  <span class="markdown"><u><?php 
                                echo  trim($element['link']);
                            ?> </u></span> 
                        </p><br>
                    <?php } ?>
            </td>
            <td colspan="2" style=" background-color: #ffffff;    border: 2px solid #e3e3e3;">   
            <br>
                <?php if(isset($element["category"]) && $element["category"] == "acteurInstitutBleu"){?>      
                    <?php  if (isset($element["legalStatus"])) {?> <p class ="text-blue-fiche"><b>Statut Juridique : </b><?php echo $element["legalStatus"];?>  </p>  <?php } ?> 
                    <br>
                    <?php  if (isset($element["objectiveOdd"])) {?><p class ="text-blue-fiche"><b>Objectif(s) ONUSIEN de Développement Durable :</b>
                        <br>
                        <?php if(is_array($element["objectiveOdd"])){
                                foreach($element["objectiveOdd"] as $kthem => $vthem){
                                   echo "<br>- " .$vthem;
                                }
                            }else {
                                echo "<br>". $element["objectiveOdd"];
                            }?>
                        </p>                        
                    <?php } ?>
                    <?php if (isset($element["responsable"]))  {?><p class ="text-blue-fiche"><b>Responsable :</b> <?php echo $element["responsable"]; ?></p>  <?php } ?> 
                <?php }?>
            </td>
        </tr>
    </table>
    <?php if(isset($element["category"]) && $element["category"] == "acteurInstitutBleu"){?>
        <?php if (isset($element["description"])){?>
            <p class="label label-fiche-title " >Objectif(s)</p>    
            <p class ="text-blue-fiche" style="text-align:justify;  font-size:12px"><?php  echo   str_replace("\n","<br>",$element["description"]); ?> </p>
        <?php } ?>
        <?php if (isset($element["otherSociaNetworks"]) && $element["otherSociaNetworks"] != []){?>
            <p class="label label-fiche-title " >Réseaux sociaux</p>    
            <?php foreach($element["otherSociaNetworks"] as $kSocial => $vSocial){?>
                <p class ="text-blue-fiche">
                    <?php if(isset($vSocial["type"]) && isset($vSocial["link"])) {?>
                        - <?= $vSocial["type"]; ?> : <a href="<?= $vSocial["link"]?>" target="_blank"><?= $vSocial["link"]?></a><br>
                    <?php } ?>											
                </p>
            <?php } ?>	
        <?php } ?>
    
    <?php } ?>
    <br />
    <?php if($signaturePath != "" ){?>
        <div style="text-align: right; float: right;"> 
            <img   style="height:70px; object-fit:contain" src = "<?= $signaturePath?>">
        </div>
    <?php } ?>	

</div> 