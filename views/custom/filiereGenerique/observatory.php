<?php
  // $cssAndScriptFilesModule = array(
  //   '/js/default/profilSocial.js'
  // );
  // HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
  // $graphAssets = [
  //   '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
  // ];
  // HtmlHelper::registerCssAndScriptsFiles(
  //   $graphAssets,
  //   Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  // );

  $elementsCostum = "";
  if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $elementsCostum = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
  } 
  $cities = "";
  if($elementsCostum != "" &&  $elementsCostum["costum"]["typeCocity"] == "epci") {
      $cities = PHDB::findOne(Zone::COLLECTION,array(
          "name" => $elementsCostum["address"]["level5Name"]
      ));
  }

  // $acteurs = $orgaMembers = $answers = $projects = $graphe_data = array();
  // $is_member = false;

  // # Get Events
  // $events = PHDB::find("events", array('source.keys' => $this->costum["slug"]));
  // # Get members (organization) 
  // $orgaMembers = PHDB::find("organizations", array('source.keys'=>$this->costum["slug"]));
  // # Get answers
  // $projects = PHDB::find("projects", array("source.keys"=>$this->costum['slug']));

  // if(count($orgaMembers)>0){
  //   foreach ($orgaMembers as $km => $aMember) {
  //     # Collect statistic data
  //     if(array_key_exists("type", $aMember)){
  //       count_distinct($graphe_data, Yii::t("common", $aMember["type"]));
  //     }else if(array_key_exists("category", $aMember)){
  //       count_distinct($graphe_data, Yii::t("common", $aMember["category"]));
  //     }

  //     # Get the anwers of a member
  //     $answerKey = "";

  //     if(isset($aMember["creator"])){
  //       $myAnswers = PHDB::find("answers", array("source.key"=>$this->costum['slug'], "user" => $aMember["creator"], "draft"=>['$exists' => false ]));
  //       foreach($myAnswers as $makey => $answer) {
  //         $answerKey = $makey;
  //       }
  //     }

  //     if(isset($aMember["name"])){
  //       $aMember["id"]=$km;

  //       $acteurs[$aMember['name']] = array(
  //         "member" => $aMember,
  //         "form" => $answerKey
  //       );
  //     }
  //   }
  // }
?>
  <style>
		a:hover, a:focus{
			text-decoration: none;
			outline: none;
		}

    .project-item{
       min-height: 10em;
    }

    .list-item{
       margin-left: 0.5em;
       padding-top: 0.4em;
       padding-bottom: 0.3em;
    }

    .acteur-item{
       margin-bottom: 1.5em;
    }

    #answerModal{
       z-index: 10000000 !important;
    }

		.text-green-theme{
			color: #7cb927;
		}

    #searchInput {
      background-image: url('/images/search.png'); 
      background-position: 15px center;  
      background-repeat: no-repeat;
      background-color: #f1f1f1;
      border-radius: 30px;
      font-size: 16px; 
      padding: 5px 20px 5px 40px; 
      border: 1px solid #ddd;
      margin-bottom: 12px;
      margin-left: 12px;
    }

	  .effectif{
      padding: 0.2em;
      background: #eee;
      border-radius: 0.3em;
    }

		.dash-icon{
			font-size: 2em;
			border-radius: 50%;
			padding: 0.8em;
			margin-top: 0.3em;
			margin-bottom: auto;
			background: #ddd;
		}

		.nav-tabs > li {
		    float:none;
		    display:inline-block;
		    zoom:1;
		}

		.nav-tabs {
		    text-align:center;
        text-transform: uppercase;
		}

		#obs-content .tabbable-obs-panel {
      border:1px solid #eee;
      padding: 10px;
    }

     /* Default mode */
     #obs-content .tabbable-obs-line > .nav-tabs {
         border: none;
         margin: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li {
         margin-right: 2px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a {
         border: 0;
         margin-right: 0;
         color: #737373;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li > a > i {
         color: #a6a6a6;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li {
         display: inline-block;
         color: #000;
         text-decoration: none;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li::after {
         content: '';
         display: block;
         width: 0;
         height: 4px;
         background: #7cb927;
         margin-bottom: -4px;
         transition: width .3s;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li:hover::after {
         width: 100%;
     }

     #obs-content .tabbable-obs-line > .nav-tabs > li.open {
         border-bottom: 3px solid #7cb927;
     }
     
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a {
         border-bottom: 2px solid #7cb927;
         background: none !important;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open > a > i, #obs-content .tabbable-obs-line > .nav-tabs > li:hover > a > i {
         color: #a6a6a6;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.open .dropdown-menu, #obs-content .tabbable-obs-line > .nav-tabs > li:hover .dropdown-menu {
         margin-top: 0px;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active {
         border-bottom: 3px solid #7cb927;
         position: relative;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a {
         border: 0;
         color: #333333;
     }
     #obs-content .tabbable-obs-line > .nav-tabs > li.active > a > i {
         color: #404040;
     }
     .media{
       border: 1px solid #eee;
       padding: 0.5em;
     }
     #obs-content .tabbable-obs-line > .tab-content {
      margin-top: -3px;
        background-color: #fff;
         border: 0;
         border-top: 1px solid #eee;
         padding: 13px 0;
     }
     #obs-content .tab-pane{
       overflow: hidden;
     }

     .graph-container{
       height: 70vh;
     }
     div[id$="-container"]{
       height: 100%;
       width: 100%;
     }

     .count_list_data {
      margin-bottom: 10px;
     }

     .headerLine {
        width: 160px;
        height: 2px;
        display: inline-block;
        background: #101F2E;
      }
</style>

<div class="container">
  <div class="sectiontitle text-center">
    <h2>Observatoire</h2>
    <span class="headerLine"></span>
  </div>
  <br><br>
  <div class="row text-center">
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=citoyens">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-user dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "citoyens")?></h5>
            <h1 id="count_citoyens" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=NGO">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-group dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "NGO")?></h5>
            <h1 id="count_NGO" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=LocalBusiness">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-industry dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "LocalBusiness")?></h5>
            <h1 id="count_LocalBusiness" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=Group">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-circle-o dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "Group")?></h5>
            <h1 id="count_Group" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=GovernmentOrganization">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-university dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "GovernmentOrganization")?></h5></h5>
            <h1 id="count_governmentOrganization" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=Cooperative">
        <div class="container-fluid effectif">
          <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-industry dash-icon"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "Cooperative")?></h5>
            <h1 id="count_Cooperative" class="text-green-theme"></h1>
          </div>
        </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=projects">
      <div class="container-fluid  effectif">
        <div class="row">
          <div class="col-md-5">
            <i class="fa text-green-theme fa-lightbulb-o dash-icon" style="padding-right: 1em; padding-left: 1em;"></i>
          </div>
          <div class="col-md-7">
            <h5><?php echo Yii::t("common", "Projects")?></h5>
            <h1 id="count_projects" class="text-green-theme"></h1>
          </div>
        </div>
      </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=events">
        <div class="container-fluid effectif">
          <div class="row">
            <div class="col-md-5">
              <i class="fa text-green-theme fa-calendar dash-icon"></i>
            </div>
            <div class="col-md-7">
              <h5><?php echo Yii::t("common", "Events")?></h5>
              <h1 id="count_events" class="text-green-theme"></h1>
            </div>
          </div>
        </div>
      </a>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12 count_list_data">
      <a class="lbh" href="#search?searchType=events">
        <div class="container-fluid effectif">
          <div class="row">
            <div class="col-md-5">
              <i class="fa text-green-theme fa-map-marker dash-icon"></i>
            </div>
            <div class="col-md-7">
              <h5><?php echo Yii::t("common", "Point of interest")?></h5>
              <h1 id="count_poi" class="text-green-theme"></h1>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <canvas id="lineChart" style="display: block; box-sizing: border-box; height: 635px; width: 1271px;" width="1033" height="516"></canvas>
</div>
<?php  if(isset($_SESSION["userId"])){ ?>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
	<script type="text/javascript">
    var dataCostum = <?php echo json_encode( $elementsCostum ); ?> ;
    var citiesArray = <?php echo json_encode($cities); ?>;

    function filiereObservatoryData() {
      var params= {
        searchType : ["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"],
        notSourceKey:true,
        filters : {
          $or :{
            "source.keys" : ["<?=$elementsCostum['slug']?>"],
            "links.memberOf.<?=(String)$elementsCostum['_id']?>" : {$exists:true}
          }
        },
        count: true,
        countType : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative", "ressources", "poi", "projects","classifieds","citoyens", "events"]
      };

      if(typeof dataCostum.thematic != "undefined" && dataCostum.thematic != "") {

        // params.filters["$or"].tags = (dataCostum.thematic).toLowerCase().replace(/ /g,'');

        if(dataCostum.thematic == "Associations") {
          params.searchType = ["NGO"];
        }

        var thema = dataCostum.thematic === "Tiers lieux" ? "TiersLieux" : dataCostum.thematic;
        params.searchTags = [thema];
        params.filters["$or"].$and = setFiltersdataAndThematic(dataCostum.thematic);
      }

      ajaxPost(
				null,
				baseUrl + "/" + moduleId + "/search/globalautocomplete",
				params,
				function(data){
          var counts = data.count;
          insertCountData(counts);
				}
			);
    }

    function setFiltersdataAndThematic(thematic) {
      var sourcekey = "";
      // var tags = thematic.toLowerCase();
      var dataAnd = [];
      
      var addressLevels = {
        "region": { "address.level3": dataCostum.address["level3"] },
        "ville": { "address.localityId": dataCostum.address['localityId'] },
        "departement": { "address.level4": dataCostum.address['level4'] },
        "district": { "address.level4": dataCostum.address['level4'] },
        "epci": { "address.localityId": { $in: (costum.citiesArray = citiesArray.cities) } },
      };

      var idAddress = addressLevels[costum.typeCocity] || addressLevels['region'];

      if (thematic === "Tiers lieux" || thematic === "Pacte") {
        sourcekey = thematic === "Tiers lieux" ? "franceTierslieux" : "siteDuPactePourLaTransition";
        // tags = thematic === "Tiers lieux" ? "TiersLieux" : tags;

        var source = {
            "$or": [
                { "source.keys": sourcekey },
                { "reference.costum": sourcekey },
                // { "tags": tags }
            ]
        };
      }

      var postalCodeFilter = dataCostum.address["postalCode"] ? { "address.postalCode": dataCostum.address["postalCode"] } : null;
      
      dataAnd = postalCodeFilter ? [postalCodeFilter, idAddress] : [idAddress];
      
      if (sourcekey) {
          dataAnd.push(source);
      } 
      // else {
      //     dataAnd.push({ "tags": tags });
      // }

      return dataAnd;
    }

    function insertCountData(counts) {
      const countKeys = {
        NGO: "#count_NGO",
        LocalBusiness: "#count_LocalBusiness",
        Group: "#count_Group",
        Cooperative: "#count_Cooperative",
        GovernmentOrganization: "#count_governmentOrganization",
        projects: "#count_projects",
        poi: "#count_poi",
        citoyens: "#count_citoyens",
        events: "#count_events"
      };

      for (let key in countKeys) {
        const selector = countKeys[key];
        const count = counts[key];

        if (count > 0) {
          $(selector).html(count).closest(".count_list_data").removeClass("hide");
        } else {
          $(selector).closest(".count_list_data").addClass("hide");
        }
      }

      generateLineChart(counts);
    }

    function generateLineChart(counts) {
      const labels = Object.keys(counts);
      const data = Object.values(counts);

      let canvas = document.getElementById("lineChart");
      if (!canvas) {
        canvas = document.createElement("canvas");
        canvas.id = "lineChart";
        document.body.appendChild(canvas);
      }

      new Chart(canvas, {
        type: 'line',
        data: {
          labels: labels,
          datasets: [{
            label: 'Comptes',
            data: data,
            borderColor: 'rgba(75, 192, 192, 1)',
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderWidth: 2,
            fill: true
          }]
        },
        options: {
          responsive: true,
          scales: {
            x: {
              beginAtZero: true,
              title: {
                display: true,
                text: 'Categories'
              }
            },
            y: {
              beginAtZero: true,
              title: {
                display: true,
                text: 'Values'
              }
            }
          },
          plugins: {
            legend: {
              display: true,
              position: 'top'
            }
          }
        }
      });
    }
      
    $(document).ready(function(){
      filiereObservatoryData();
    });
  </script>
<?php } else { ?>
	<div class="container">
		<hr>
	</div>
	<div class="text-center">
		<h4 class="text-light text-center">Veuillez vous connecter pour plus d'informations</h4>
		<br>
		<div>ou</div>
		<br>
		<button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalRegister">
			<i class="fa fa-plus-circle"></i> Créer Un Compte <b>Citoyen</b>
		</button>
	</div>
<?php } ?>
