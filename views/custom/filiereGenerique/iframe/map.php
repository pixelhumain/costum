<?php
$cssAnsScriptFilesModule = array(
  '/js/filiereGenerique/filiereGenerique_index.js',
  '/js/filiereGenerique/filters.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>

<style>
  .main-container{
    padding-left: 0px !important;
    padding-top: 0px !important;
  }

  #search-content{
    padding-left:0px !important;
    margin-left:0px !important;
    min-height: 10px !important;
    overflow: hidden !important
  }

  .container-filters-menu, #filters-nav{
    left: 0px !important;
  }

  button.filter-btn-hide-map, .btn-show-map{
    display: none !important;
  }

  .leaflet-popup-tip-container {
    bottom: -29px;
  }

  #mapContent{
    min-height: 400px !important;
    left: 0px !important;
    top: 0px !important;
  }

</style>

<script type="text/javascript">
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const elRete = urlParams.get('reteofapartment');

  createFilters("#filters-nav", true);

  jQuery(document).ready(function() {
    $("#search").remove();
    $("#menuLeft, .btn-more, #vertical, .modal").remove();
    $(".headerSearchContainer").remove();
    $(".footerSearchContainer").remove();
    filterSearch = searchObj;
    $(".count-badge-filter").remove();

    if(typeof paramsFilter.filters["network"] != "undefined"){
      filterSearch.filters.actions.themes.callBack = function(data){
        filterSearch.filters.actions.themes.isLoaded=false;
        let network = [];
        for (const [key, value] of Object.entries(data)) {
          if(typeof value.extraInfo != "undefined" && typeof value.extraInfo.reteofapartment != "undefined" && value.extraInfo.reteofapartment != ""){
            if(Array.isArray(value.extraInfo.reteofapartment)){
              let rete = value.extraInfo.reteofapartment;
              network.push(...rete);
            }else{
              network.push(value.extraInfo.reteofapartment);
            }
          }
        }
        paramsFilter.filters["network"]["list"] = Array.from(new Set(network))
        filterSearch.filters.initViews(filterSearch, paramsFilter);
        filterSearch.filters.initEvents(filterSearch, paramsFilter);
        filterSearch.filters.initDefaults(filterSearch, paramsFilter);

        filterSearch.filters.actions.themes.setThemesCounts(filterSearch);
        filterSearch.filters.actions.themes.callBack = function(res){}
      }
    }

    filterSearch = filterSearch.init(paramsFilter);
    //$("#filters-nav").append('<button id="reteFilter" class="btn-filters-select network col-xs-12" data-type="filters" data-key="'+elRete+'" data-value="'+elRete+'" data-field="extraInfo.reteofapartment" data-event="filters" data-filterk="network">'+elRete+'</button>')
    setTimeout(()=>{
      //$("#reteFilter").click();
      $("[data-value='"+elRete+"']").hide();
    }, 3000)


  });
</script>
