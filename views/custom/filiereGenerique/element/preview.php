<?php
$cssAnsScriptFilesModule = array(
    '/js/default/preview.js'
);

HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $me = Yii::app()->session["userId"] ;
    
    $previewConfig=@$this->appConfig["preview"];
    
    $iconColor=(isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
     
    /*echo "<pre>";
    var_dump($element);
    echo "</pre>";*/
?>
<?php 
$edit = true;
?>
<style type="text/css">
    .social-share-button img{
        margin-right: 10px;
    }
.demo-bg{
    background: #4ab5a12e;
    margin-top: 60px;
}
.business-hours {
    background: #222; 
    padding: 40px 14px;
    margin-top: -15px;
    position: relative;
}
.business-hours:before{
    content: '';
    width: 23px;
    height: 23px;
    background: #111;
    position: absolute;
    top: 5px;
    left: -12px;
    transform: rotate(-45deg);
    z-index: -1;
}
.business-hours .title {
    font-size: 20px;
    color: #BBB;
    text-transform: uppercase;
    padding-left: 5px;
    border-left: 4px solid #ffac0c; 
}
.business-hours li {
    color: #888;
    line-height: 30px;
    border-bottom: 1px solid #333; 
}
.business-hours li:last-child {
    border-bottom: none; 
}
.business-hours .opening-hours li.today {
    color: #ffac0c; 
}
</style>
<script type="text/javascript">
    var dimensions = "";
    var workers = "";
    var volunteers = "";           
    var transformativehorizons = new Array;
    var contacts = new Array;
    var certifications = new Array;
    var reteofapartment = [];
</script>
<div class="col-xs-12 padding-10 toolsMenu">
    <button class="btn btn-default pull-right btn-close-preview">
        <i class="fa fa-times"></i>
    </button>
    <button href="#page.type.<?php echo @$element["collection"] ?>.id.<?php echo @$element["_id"] ?>" class="lbh btn btn-primary pull-right margin-right-10"><?php echo Yii::t("common", "Go to the item") ?>
</button>
<?php if((Authorisation::isInterfaceAdmin() || $me == array_keys($element["links"]["members"])[0]) && isset($this->costum["typeObj"]["organizations"]["dynFormCostum"]) && $type == Organization::COLLECTION && $this->costum["slug"]=="ries"){ ?>
<button onclick="edit_org();" class="btn btn-default pull-right margin-right-10 btn-close-preview">
    <i class="fa fa-pencil"></i> <?php echo Yii::t("common", "Edit") ?>
</button>
<?php } ?>
</div>
<div class="container-preview col-xs-12 no-padding margin-bottom-20" style="overflow-y: scroll">
    <?php 
        if(isset($previewConfig["banner"]) && !empty($previewConfig["banner"])){
            if (isset($element["profilBannerUrl"]) && !empty($element["profilBannerUrl"])){ 
                $url=Yii::app()->createUrl('/'.$element["profilBannerUrl"]);
            }else{
                 if(!empty($this->costum) && isset($this->costum["htmlConstruct"]) 
                    && isset($this->costum["htmlConstruct"]["element"])
                        && isset($this->costum["htmlConstruct"]["element"]["banner"]["img"]))
                    $url=Yii::app()->getModule( "costum" )->assetsUrl.$this->costum["htmlConstruct"]["element"]["banner"]["img"];
                else
                    $url=Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
            }
            ?> 
            <div class="col-xs-12 no-padding" id="col-banner" style="border-top: 1px solid #e7e7e7;border-bottom: 1px solid #e7e7e7;">
                <?php 
                if(isset($previewConfig["banner"]["edit"]) && $previewConfig["banner"]["edit"] && $auth){ ?> 
                    <?php echo $this->renderPartial("co2.views.element.modalBanner", array(
                            "type"=>$type, 
                            "id"=>(string)$element["_id"], 
                            "name"=>$element["name"],
                            "edit" => $canEdit,
                            "openEdition" => $openEdition,
                            "profilBannerUrl"=>@$element["profilBannerUrl"])); 
                } ?> 
                <div id="contentBanner" class="col-xs-12 no-padding">
                    <?php $imgHtml='<img class="col-md-12 col-sm-12 col-xs-12 no-padding img-responsive" 
                        alt="'.Yii::t("common","Banner").'" 
                        src="'.$url.'">';
                    if (@$element["profilRealBannerUrl"] && !empty($element["profilRealBannerUrl"])){
                        $imgHtml='<a href="'.Yii::app()->createUrl('/'.$element["profilRealBannerUrl"]).'"
                                    class="thumb-info"  
                                    data-title="'.Yii::t("common","Cover image of")." ".$element["name"].'"
                                    data-lightbox="all">'.
                                    $imgHtml.
                                '</a>';
                    }
                    echo $imgHtml; 
                    ?>  
                </div>
            </div>
            <?php if(isset($previewConfig["banner"]["imgProfil"]) && $previewConfig["banner"]["imgProfil"]){ ?>
                <div class="content-img-profil-preview col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                <?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?> 
                        <a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
                        class="thumb-info"  
                        data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
                        data-lightbox="all">
                            <img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);margin: auto;max-height: 300px;" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
                        </a>
                <?php }else{ ?>
                        <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="margin: auto;box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumb/default_<?php echo $type ?>.png"/>
                <?php } ?>
                </div>
        <?php } 
        } ?>
    <div class="col-xs-12 margin-bottom-20 no-padding">
        <h3 class="title text-gray col-xs-12 text-center margin-bottom-20"><?php echo $element["name"] ?></h3>
        <span class="col-xs-12 text-center blockFontPreview"> 
            <?php if($type==Project::COLLECTION){ 
                $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Territoire en CTE" : "Action";
                $iconColor=(isset($element["category"]) && $element["category"]=="cteR") ? $iconColor : "purple";
                $icon=(isset($element["category"]) && $element["category"]=="cteR") ? "map-marker" : "lightbulb-o";
                ?> 
                
                <span class="text-<?php echo $iconColor; ?>"><i class="fa fa-<?php echo $icon ?>"></i> <?php echo $label; ?></span>
            <?php }else{ ?>
                <span class="text-<?php echo $iconColor; ?>"><?php echo strtoupper(Yii::t("common", Element::getControlerByCollection($type))); ?></span>
            <?php } ?>
            <?php if(($type==Organization::COLLECTION || $type==Event::COLLECTION) && @$element["type"]){ 
                if($type==Organization::COLLECTION)
                    $typesList=Organization::$types;
                else
                    $typesList=Event::$types;
                ?>
                <i class="fa fa-x fa-angle-right margin-left-10"></i>
                <span class="margin-left-10"><?php echo Yii::t("form",@$element["extraInfo"]["dimensions"]); echo"<script> dimensions = '".@$element["extraInfo"]["dimensions"]."'</script>"; ?>
                <i class="fa fa-x fa-angle-right margin-left-10"></i>
                <?php echo Yii::t("category", $element["type"]) ?></span>
            <?php } ?>
            
        </span>
        <?php 
        if(!empty($element["address"]["addressLocality"])){ ?>
            <div class="header-address col-xs-12 text-center blockFontPreview">
                <?php
                echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
                echo !empty($element["address"]["postalCode"]) ? 
                $element["address"]["postalCode"].", " : "";
                echo $element["address"]["addressLocality"];
                ?>
            </div>
        <?php } ?>
        <div class="header-tags col-xs-12 text-center blockFontPreview">
            <?php 
            if(@$element["tags"] && $type!=Project::COLLECTION){ 
                foreach ($element["tags"] as $key => $tag) { ?>
                    <a  href="javascript:;"  class="letter-red" style="vertical-align: top;">#<?php echo Yii::t("form",$tag); ?></a>
                <?php } 
                 } ?>
        </div>
        <div class="col-md-12 padding-10" style="background-color: #f7f7f7;">
            <div class="col-md-8 text-left">

                <?php if($type==Event::COLLECTION){ ?>
                    <div class="event-infos-header  margin-top-10 col-xs-12 blockFontPreview"  style="font-size: 14px;font-weight: none;"></div>
                <?php } ?>
                <?php if(isset($element["filRouge"])){ 
                    $descr= (strlen($element["filRouge"]) > 250) ? substr($element["filRouge"], 0, 250)." ..." : $element["filRouge"]; ?>
                    <div class="col-xs-12  margin-bottom-20 markdown-txt"><?php echo $descr ?></div>

                <?php } ?> 
                <?php if(isset($element["shortDescription"]) && !empty($element["shortDescription"]) && $element["shortDescription"] != "Nouveau Candidat au CTE") echo "<p class='col-xs-12'>".$element["shortDescription"]."</p>"; ?>
                <?php if(@$element["category"]=="cteR"){ 
                    $descr= (@$element["why"]) ? substr($element["why"], 0, 400)." ..." : ""; ?>
                    <div class="col-xs-12 markdown-txt"><?php echo $descr ?></div>
                <?php   }else{ 
                    $descr= (@$element["description"]) ? substr($element["description"], 0, 400)." ..." : ""; ?>

                    <div class="col-xs-12  markdown-txt"><?php echo $descr ?></div>
                <?php   }?>



            </div>
            <?php if(!empty($element["openingHours"])) { ?>
            <div class="col-md-4 padding-top-10 padding-bottom-10" style="box-shadow: 0px 0px 0px 1px #ccc;">
                <div class="padding-5" style="background-color: #a7e0d6;">
                    <h4 id="dateTimezone" href="javascript:;" class="tooltips text-dark" data-original-title="" data-toggle="tooltip"  data-placement="right">
                        <span class="text-nv-3 text-center text-dark-blue">
                            <i class="fa fa-clock-o"></i> <?php echo Yii::t("form","Opening hours") ?>

                        </span>
                    </h4>
                    <div>
                        <?php if((!empty($element["startDate"])) && (!empty($element["startDate"])) ) { ?>
                            <div class="col-md-12 padding-bottom-20">
                                <div class="tl-item contentInformation" id="divStartDate" >
                                    <div class="item-title bold" ><?php echo Yii::t("event","From") ?></div>
                                    <div class="item-detail" id="startDateAbout"><?php echo (isset($element["startDate"]) ? $element["startDate"] : "" ); ?></div>
                                </div>
                                <div class="tl-item contentInformation" id="divEndDate" >
                                    <div class="item-title bold"><?php echo Yii::t("common","To") ?></div>
                                    <div class="item-detail" id="endDateAbout"><?php echo (isset($element["endDate"]) ? $element["endDate"] : "" ); ?></div>
                                </div>
                            </div>
                        <?php } ?> 
                        <div class="tl">
                            <?php
                            if(!empty($element["openingHours"])){
                             foreach ($element["openingHours"]  as $key => $temp) { 
                                if (!empty($temp["dayOfWeek"])){
                                    $numDate = $key + 1;
                                    echo '<div class="col-md-12" style="padding-top: 10px;border: 2px solid #ffffff00;border-top-color: #eee;">
                                    <div style="text-align: right;" class="col-md-4 item-title bold uppercase" id="dayOfWik_'.$numDate.'">'.$temp["dayOfWeek"].'</div>';
                                    foreach ($temp["hours"] as $kop => $vop) {
                                        echo'<div class="col-md-8 item-detail" >'.$vop["opens"].'&nbsp;:&nbsp;'.$vop["closes"].'<br>';
                                        echo '</div>';
                                    }
                                    echo'</div>';
                                }
                                else{
                                    $numDate = $key + 1;
                                }


                            } 
                        };

                        ?>
                    </div>
                </div>
            </div>

        </div>
        <?php } ?>
    </div>

            <div class="col-md-12 text-left padding-10" style="box-shadow: 0px 0px 0px 1px #ccc;">
                <?php if (isset($element["address"])) { ?>
                <div class="col-md-4">

                    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => Organization::COLLECTION, "edit" => true, "openEdition" => true)); ?>

                </div>
                <?php } ?>
                <?php if (isset($element["extraInfo"]["contacts"])) { ?>
                    <div class="col-lg-4">
                        <p class="letter-red"><?php echo Yii::t("form","Addressed to") ?></p> 
                        <?php foreach ($element["extraInfo"]["contacts"] as $key => $value) {
                            echo"<script> contacts.push('".$value."')</script>";
                            echo "<span style='text-transform: capitalize;'>".Yii::t("form",$value).", </span>";
                        } ?>                       
                        <?php
                        if (is_string($element["extraInfo"]["contacts"])) {
                            echo"<script> contacts.push('".$element["extraInfo"]["contacts"]."')</script>";
                            echo "<span style='text-transform: capitalize;'>".Yii::t("form",$element["extraInfo"]["contacts"])."</span>";
                        }
                        ?>
                        <hr>
                    </div>
                        <?php if (isset($element["extraInfo"]["dimensions"])) { ?>
                    <div class="col-lg-4">
                            <p class="letter-red"><?php echo Yii::t("form","Dimensions") ?></p>   
                            <?php 
                            echo "<span style='text-transform: uppercase;'>".$element["extraInfo"]["dimensions"]."</span>";
                            echo"<script> dimensions = '".$element["extraInfo"]["dimensions"]."'</script>";
                            ?>   
                        <hr>          
                    </div>   
                        <?php } ?>    
                        <?php if (isset($element["certificationsAwards"])) { ?>                            
                    <div class="col-lg-4">
                            <p class="letter-red"><?php echo Yii::t("form","Certifications or awards") ?></p>             
                            <?php 
                            foreach ($element["certificationsAwards"] as $key => $value) {
                                echo"<script> certifications.push('".@$value["certAward"]."')</script>";
                                echo "<span style='text-transform: uppercase;'>".@$value["certAward"].", </span>";
                            }

                            ?>   
                        <hr>          
                    </div>
                    <?php } ?>  


                    <?php if (isset($element["extraInfo"]["reteofapartment"])) { ?>
                        <div class="col-md-4">
                            <p class="letter-red"><?php echo Yii::t("form","Supply chain network") ?></p> 
                            <?php foreach ($element["extraInfo"]["reteofapartment"] as $key => $value) {
                                echo "<span style='text-transform: capitalize;'>".$value.", </span>";
                                echo"<script> reteofapartment.push('".$value."')</script>";
                            } 
                            if (is_string($element["extraInfo"]["reteofapartment"])) {
                                echo "<span style='text-transform: capitalize;'>".$element["extraInfo"]["reteofapartment"]."</span>";
                                echo"<script> reteofapartment.push('".$element["extraInfo"]["reteofapartment"]."')</script>";
                            }
                            ?>     
                            <hr>      
                        </div>
                    <?php } ?> 

                    <?php if (isset($element["extraInfo"]["transformativehorizons"])) { ?>
                        <div class="col-md-4">           
                            <p class="letter-red"><?php echo Yii::t("form","Transformative horizons") ?></p>   
                            <?php foreach ($element["extraInfo"]["transformativehorizons"] as $key => $value) {
                                echo "<span style='text-transform: capitalize;'>".Yii::t("form",$value).", </span>";
                                echo"<script> transformativehorizons.push('".$value."')</script>";
                            } 
                            if (is_string($element["extraInfo"]["transformativehorizons"])) {
                                echo "<span style='text-transform: capitalize;'>".Yii::t("form",$element["extraInfo"]["transformativehorizons"])."</span>";
                                echo"<script> transformativehorizons.push('".$element["extraInfo"]["transformativehorizons"]."')</script>";
                            }
                            ?> 
                            <hr>                    
                        </div>
                    <?php } ?> 

                <?php if (isset($element["extraInfo"]["peopleinvolved"]["workers"]) || isset($element["extraInfo"]["peopleinvolved"]["volunteers"])) { ?>
                    <div class="col-md-4">
                        <?php if (isset($element["extraInfo"]["peopleinvolved"]["workers"])) { 
                            echo"<script> workers = '".$element["extraInfo"]["peopleinvolved"]["workers"]."'</script>";
                            ?>
                            <p class="letter-red" style="white-space: nowrap;"><?php echo Yii::t("form","Workers") ?>: <?= $element["extraInfo"]["peopleinvolved"]["workers"] ?></p>
                        <?php } 
                        if (isset($element["extraInfo"]["peopleinvolved"]["volunteers"])) { 
                            echo"<script> volunteers = '".$element["extraInfo"]["peopleinvolved"]["volunteers"]."'</script>";?>
                            <p class="letter-red" style="white-space: nowrap;"><?php echo Yii::t("form","Volunteers") ?>: <?= $element["extraInfo"]["peopleinvolved"]["volunteers"] ?></p>
                        <?php } ?> 
                        <hr> 
                    </div>   
                <?php } ?>   
                </div>
            <?php } ?>          
        </div>

        <div class="col-xs-12 bg-white shadow2">
            <div class="panel panel-white col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding shadow2">
                <div class="panel-heading border-light col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #dee2e680;">
                    <h4 class="panel-title pull-left"> 
                        <?php echo Yii::t("common","Contact"); ?>
                    </h4>
                </div>
                <div class="panel-body no-padding">
                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
                        <div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
                            <span><i class="fa fa-envelope"></i></span> <?php echo Yii::t("common","E-mail"); ?>
                        </div>
                        <div id="emailAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
                            <span class="visible-xs pull-left margin-right-5"><i class="fa fa-envelope"></i> <?php echo Yii::t("common","E-mail"); ?> :</span><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
                        <div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
                            <span><i class="fa fa-desktop"></i></span> <?php echo Yii::t("common","Website URL"); ?>
                        </div>
                        <div id="webAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
                            <span class="visible-xs pull-left margin-right-5"><i class="fa fa-desktop"></i> <?php echo Yii::t("common","Website URL"); ?> :</span>
                            <?php 
                            if(@$element["url"]){
                        //If there is no http:// in the url
                                $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
                                echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
                            }else
                            echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding">
                        <div class="col-md-4 col-sm-4 col-xs-4 hidden-xs labelAbout padding-10">
                            <span><i class="fa fa-mobile"></i></span> <?php echo Yii::t("common","Mobile"); ?>
                        </div>
                        <div id="mobileAbout" class="col-md-8 col-sm-8 col-xs-12 valueAbout padding-10">
                            <span class="visible-xs pull-left margin-right-5">
                                <i class="fa fa-mobile"></i> <?php echo Yii::t("common","Mobile"); ?> :
                            </span>
                            <?php
                            $mobile = '<i>'.Yii::t("common","Not specified").'</i>';
                            if( !empty($element["telephone"]["mobile"]))
                                $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);  
                            echo $mobile;
                            ?>  
                        </div>
                    </div>
                </div>  
            </div>
            <?php
            $skype = (!empty($element["socialNetwork"]["skype"])? $element["socialNetwork"]["skype"]:"javascript:;") ;
            $telegram =  (!empty($element["socialNetwork"]["telegram"])? "https://web.telegram.org/#/im?p=@".$element["socialNetwork"]["telegram"]:"javascript:;") ;
            $diaspora =  (!empty($element["socialNetwork"]["diaspora"])? $element["socialNetwork"]["diaspora"]:"javascript:;") ;
            $mastodon =  (!empty($element["socialNetwork"]["mastodon"])? $element["socialNetwork"]["mastodon"]:"javascript:;") ;
            $facebook = (!empty($element["socialNetwork"]["facebook"])? $element["socialNetwork"]["facebook"]:"javascript:;") ;
            $twitter =  (!empty($element["socialNetwork"]["twitter"])? $element["socialNetwork"]["twitter"]:"javascript:;") ;
            $googleplus =  (!empty($element["socialNetwork"]["googleplus"])? $element["socialNetwork"]["googleplus"]:"javascript:;") ;
            $github =  (!empty($element["socialNetwork"]["github"])? $element["socialNetwork"]["github"]:"javascript:;") ;
            $instagram =  (!empty($element["socialNetwork"]["instagram"])? $element["socialNetwork"]["instagram"]:"javascript:;") ;
            ?>
            <?php if (isset($element["socialNetwork"])) { ?>
            <div id="socialAbout" class="panel panel-white col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding shadow2">
                <div class="panel-heading border-light col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #dee2e680;">
                    <h4 class="panel-title pull-left"> 
                        <?php echo Yii::t("common","Socials"); ?>
                    </h4>
                </div>
                <div class="panel-body no-padding">

                    <?php if ($diaspora != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Diaspora">
                            <span><i class="fa fa-diaspora"></i></span> 
                            <span id="divDiaspora">
                                <a href="<?php echo $diaspora ; ?>" target="_blank" id="diasporaAbout" class="socialIcon "><?php echo  $diaspora ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($mastodon != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Mastodon">
                            <span><i class="fa fa-mastodon"></i></span> 
                            <span id="divMastodon">
                                <a href="<?php echo $mastodon ; ?>" target="_blank" id="mastodonAbout" class="socialIcon "><?php echo  $mastodon ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($facebook != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Facebook">
                            <span><i class="fa fa-facebook"></i></span>
                            <span id="divFacebook">
                                <a href="<?php echo $facebook ; ?>" target="_blank" id="facebookAbout" class="socialIcon "><?php echo  $facebook ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($twitter != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Twitter">
                            <span><i class="fa fa-twitter"></i></span>
                            <span id="divTwitter">
                                <a href="<?php echo $twitter ; ?>" target="_blank" id="twitterAbout" class="socialIcon" ><?php echo $twitter ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($instagram != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Instagram">
                            <span><i class="fa fa-instagram"></i></span> 
                            <span id="divInstagram">
                                <a href="<?php echo $instagram ; ?>" target="_blank" id="instagramAbout" class="socialIcon" ><?php echo $instagram ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($skype != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="Skype">
                            <span><i class="fa fa-skype"></i></span>
                            <span id="divSkype">
                                <a href="<?php echo $skype ; ?>" target="_blank" id="skypeAbout" class="socialIcon" ><?php echo $skype ; ?></a>
                            </span>
                        </div>
                    <?php } ?>

                    <?php if ($github != "javascript:;"){ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 contentInformation social padding-10 tooltips" data-toggle="tooltip" data-placement="left" title="GitHub">
                            <span><i class="fa fa-github"></i></span>
                            <span id="divGithub">
                                <a href="<?php echo $github ; ?>" target="_blank" id="githubAbout" class="socialIcon" ><?php echo $github  ; ?></a>
                            </span>
                        </div>
                    <?php } ?>
                </div>  
            </div> 
            <?php } ?>
        </div>
        <div class="col-xs-12">

            <?php 
                if(isset($element["links"]) && isset($element["links"]["contributors"])){ 
                    echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Les contributeurs", "links"=>$element["links"]["contributors"], "connectType"=>"contributors", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
                } 
                if(isset($element["links"]) && isset($element["links"]["members"])){ 
                    echo $this->renderPartial('co2.views.pod.listItems', array("title"=>Yii::t("common", "the members"), "links"=>$element["links"]["members"], "connectType"=>"members", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
                } 
                if(isset($element["links"]) && isset($element["links"]["projects"])){ 
                    if($type==Project::COLLECTION){
                        $label=(isset($element["category"]) && $element["category"]=="cteR") ? "Les actions" : "Développé sur"; 
                    }
                    else{
                        $label=Yii::t("common", "contribute to");
                    }
                    echo $this->renderPartial('co2.views.pod.listItems', array("title"=>$label, "links"=>$element["links"]["projects"], "connectType"=>"projects", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10","contextId"=>$id, "contextType"=>$type));
                } 

                if(isset($element["links"]) && isset($element["links"]["memberOf"])){ 
                    echo $this->renderPartial('co2.views.pod.listItems', array("title"=>"Membre de", "links"=>$element["links"]["memberOf"], "connectType"=>"organizations", "number"=> 12, "titleClass"=>"col-xs-12 title text-gray", "heightWidth" => 50, "containerClass"=>"text-center no-padding margin-top-10 margin-bottom-10", "contextId"=>$id, "contextType"=>$type));
                } 
            ?>
        </div>
        
    </div>
    
</div>

<script type="text/javascript">
    var org_selected=<?php echo json_encode($element); ?>;
    var tags = org_selected.tags;
    receiveInfo = (exists(org_selected["extraInfo"]) && exists(org_selected["extraInfo"]["receiveInfo"])) ? org_selected["extraInfo"]["receiveInfo"] : false;
    receiveInfo = (receiveInfo === "true")
    if(costum.typeObj.organizations && costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"]){
        costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"]["checked"] = receiveInfo;
    }
   
    var typePreview=<?php echo json_encode($type); ?>;
    var idPreview=<?php echo json_encode($id); ?>;
    jQuery(document).ready(function() {
        coInterface.bindTooltips();
        $(".markdown-txt").each(function(){
            descHtml = dataHelper.markdownToHtml($(this).html()); 
            var tmp = document.createElement("DIV");
            tmp.innerHTML = descHtml;
            descText = tmp.textContent || tmp.innerText || "";
            $(this).html(descText);
        });
        
        $(".container-preview .social-share-button").html(directory.socialBarHtml({"socialBarConfig":{"btnList" : [{"type":"facebook"}, {"type":"twitter"} ], "btnSize": 40 }, "type": typePreview, "id" : idPreview  }));
        resizeContainer();
    });


    // $.each() 

 /*   var reteofapartment = org_selected.extraInfo.reteofapartment;*/

 function edit_org(){      
    var org_selected=<?php echo json_encode($element); ?>;
    var tags = org_selected.tags;
    receiveInfo = (exists(org_selected["extraInfo"]) && exists(org_selected["extraInfo"]["receiveInfo"])) ? org_selected["extraInfo"]["receiveInfo"] : false;
    receiveInfo = (receiveInfo === "true");
    if(costum.typeObj.organizations && costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"]){
        costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"]["checked"] = receiveInfo;
    }
    var not_a_tags = [];
    var properties = costum.typeObj.organizations.dynFormCostum.beforeBuild.properties;  
    /*var tags = [];*/
    var sectionDyf = {};
    // console.log("Proper", org_selected.extraInfo.reteofapartment);
    /************Give value each input************/
    setTimeout(function(){    
        $("#group").val(org_selected.type);
        $(".workers").val(workers); 
        $(".contacts").val(contacts);
        $(".volunteers").val(volunteers);
        $("#extraInfo-dimensions").val(dimensions);
        $("#extraInfo-reteofapartment").val(reteofapartment);
        $("#extraInfo-transformativehorizons").val(transformativehorizons);
        if(org_selected.extraInfo.budget){
            $("#extraInfo-budget").val(org_selected.extraInfo.budget);
        }
        
/*
        $(".groupselect").hide();*/
   /*     if ($("#type").val() == "Group") {
            $(".groupselect").show()
        }*/
/*        $("#type").change(function(){  
            if ($("#type").val() == "Group") {
                $(".groupselect").show()
            }else{
                $(".groupselect").hide();
                $("#group").prop("selected", false);
            }
        });*/
    }, 600);

        properties["tags"] = {
            "label" : trad.tags, 
            "inputType" : "tags",
            "select2" : true
        }
        properties["advance"] = {
            "inputType" : "custom",
            html : `<a href="#page.type.<?php echo @$element["collection"] ?>.id.<?php echo @$element["_id"] ?>.view.detail" class="lbh btn btn-primary margin-right-10"><?php echo Yii::t("form","Advanced settings") ?>`
        }
        delete properties["extraInfo-agree"];
        sectionDyf.Params = {
      "jsonSchema" : {    
        "title" : trad.edit+" "+org_selected.name,
        "description" : "Personnalisation de votre fond",
        "icon" : "fa-cog",
        properties,
        save : function (data) { 
                console.log("k data", data);
          tags_elt = [];
          tplCtx = {};
          tplCtx.value = {};
          tplCtx.value.extraInfo = {};
          tplCtx.value.timetables = {};
          tplCtx.value.extraInfo.peopleinvolved = {};
          tplCtx.id = idPreview;
          tplCtx.path = "allToRoot";
          tplCtx.collection = org_selected.collection;
          tplCtx.value.extraInfo.contacts = $(".contacts").val();
          tplCtx.value.extraInfo.receiveInfo = $("#extraInfo-receiveInfo").val();
          tplCtx.value.extraInfo.reteofapartment = $("#extraInfo-reteofapartment").val().split(",");
          tplCtx.value.extraInfo.transformativehorizons = $("#extraInfo-transformativehorizons").val();
          tplCtx.value.extraInfo.dimensions = $("#extraInfo-dimensions").val();
          tplCtx.value.extraInfo.peopleinvolved.workers = $(".workers").val();
          tplCtx.value.extraInfo.peopleinvolved.volunteers = $(".volunteers").val();
          tplCtx.value.extraInfo.budget = $("#extraInfo-budget").val();

          $.each( sectionDyf.Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "certificationsAwards")
                    tplCtx.value[k] = data.certificationsAwards;
                if(k.indexOf("-") != -1) {  
                    delete tplCtx.value[k];
                }     
          });
/*
          if (Array.isArray(data.group)) {
              $.each(data.group , function(k,val) {
                  tags_elt.push(val);
                  console.log("organy tag",val);                
              });
          }else{            
            tags_elt.push(data.group);
          }*/
          if (Array.isArray(data.theme)) {
              $.each(data.theme , function(k,val) {
                  tags_elt.push(val);                
              });
          }else{            
            tags_elt.push(data.theme);
          }
          if (Array.isArray(data.tags)) {
              $.each(data.tags , function(k,val) {
                  tags_elt.push(val);                
              });
          }else{            
            tags_elt.push(data.tags);
          }

          newTag = [];
          $.each(tags_elt , function(k,val) {
               if (typeof val !== 'undefined') 
                newTag.push(val);                
            });
          tplCtx.value.tags = newTag;
          tplCtx.value.type = $("#group").val();;
          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                 /* urlCtrl.loadByHash(location.hash);*/
                });
              } );
            }
        }
      }
    }; 
    //   tplCtx.id = $(this).data("id");
    //   tplCtx.collection = $(this).data("collection");
    //   tplCtx.path = "allToRoot";

    org_selected.tags = getTrueTag();
      dyFObj.openForm( sectionDyf.Params,null, org_selected);
    // });

      function getTrueTag(){
        allTags = tags;
        if (Array.isArray(allTags)) {
            $.each( allTags , function(k,valAllTag) { 
                var exists = false; 
                $(org_selected.group).each(function(k_group,v_group){
                    if (v_group === valAllTag) {
                        not_a_tags.push(valAllTag); 
                    }
                }); 
                $(org_selected.theme).each(function(k_theme,v_theme){
                    if (v_theme === valAllTag) {
                        not_a_tags.push(valAllTag);                    

                    }
                });         
            });
        }
        const uniqueTags = not_a_tags.filter((x, i, a) => a.indexOf(x) == i)
        $.each( uniqueTags , function(k,valUniqueTags) { 
           for( var i = 0; i < allTags.length; i++){ 

            if ( allTags[i] === valUniqueTags) { 
                allTags.splice(i, 1); 
                i--; 
            }

        }        
    });
        properties.theme.value = not_a_tags;
        properties.group.value = not_a_tags;
       properties["extraInfo-reteofapartment"].value = reteofapartment;
        return allTags
    }


    }

  
    function afficheMap(){
        mylog.log("afficheMap");

        if(typeof org_selected.geo !== "undefined"){

            var paramsMapContent = {
                container : "divMapContent",
                latLon : org_selected.geo,
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };

            mapAbout = mapObj.init(paramsMapContent);

            var paramsPointeur = {
                elt : {
                    id : org_selected.id,
                    type : org_selected.type,
                    geo : org_selected.geo
                },
                center : true
            };
            mapAbout.addMarker(paramsPointeur);
            mapAbout.hideLoader();

        };
    };
    afficheMap();
</script> 