<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>
<style>
	.community{
	    all: revert !important;
	}
</style>
<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
		var appConfig=<?php echo json_encode(@$appConfig); ?>;
    var domId=<?php echo json_encode(@$domId); ?>;

	var thematic = null;

	var defaultType = {};

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined"){
		defaultType = Object.keys(costum.lists.types);
		//defaultType.push("organizations");
		//Object.assign(defaultType, costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"]["options"]);
	}else{
		defaultType = Object.keys({"NGO" : trad.ong,
						"Cooperative" : trad.servicepublic,
						"Group":trad.group,
						"LocalBusiness":trad.LocalBusiness,
						"GovernmentOrganization":trad.GovernmentOrganization
					});
		//defaultType = Object.keys(costum.lists.types);
	}

	if(costum && typeof costum.lists == "undefined"){
		costum["lists"] = {}
	}

	if(typeof costum.lists.theme == "undefined"){
		costum.lists["theme"] = {};
	}

	thematic = {};
	Object.assign(thematic, costum.lists.theme);

	var defaultScopeList = [];

	if(costum && costum.slug && costum.slug=="ries"){
		defaultScopeList = ["IT"];
	}else{
		defaultScopeList = ["FR", "RE"];
	}

	var paramsFilter= {
		container : (domId)?domId:"#filters-nav",
	 	loadEvent : {
	 		default : "scroll"
	 	},
		 header: {
			options: {
				left: {
					classes: 'col-xs-8 elipsis no-padding',
					group: {
						count: true,
						types: true
					}
				},
				right: {
					classes:"col-xs-4 text-right no-padding",
					group: {  
						switchView : true,
						map: true
					}
				}
			},
		},
		defaults : {
		 	types : (typeof appConfig.filters !="undefined" && exists(appConfig.filters.types))?appConfig.filters.types:"organizations",
			filters:{},
			sortBy: {"name":1},
			indexStep:0
		},
		filters : {
		 	theme : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			remove0: true,
	 			countResults: true,
	 			name : "<?php echo Yii::t("common", "search by theme")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		},
	 		category : {
		 		view : "dropdownList",
		 		type : "filters",
				field: "type",
		 		name : "<?php echo Yii::t("common", "search by type")?>",
		 		event : "filters",
		 		list : defaultType
		 	},
		 	network : {
		 		view : "dropdownList",
		 		type : "filters",
		 		field: "extraInfo.reteofapartment",
		 		name : "<?php echo Yii::t("common", "search by network")?>",
		 		event : "filters",
		 		list : ["RIES", "RESS ROMA"]
		 	},
 			scopeList : {
	 			name : "<?php echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList,
	 				level : ["3"]
	 			}
	 		},
	 		text : {
				placeholder: "Cerca per #tag o testo"
			}
	 	},
	 	results : {
			renderView: "directory.elementPanelHtmlSmallCard",
		 	smartGrid : true
		}
	}

	if(pageApp == "projects"){
		paramsFilter.defaults.types = ["projects"];
		delete paramsFilter.filters["types"];
		//paramsFilter.results.renderView = "directory.elementPanelHtmlSmallCard";
	}

	if(thematic==null || pageApp=="projects"){
		delete paramsFilter.filters["theme"];
	}

	if(costum.slug!="ries" || pageApp=="projects"){
		delete paramsFilter.filters["network"];
	}

	if(costum["dataSource"]){
		paramsFilter["defaults"]["sourceKey"] = costum["dataSource"];
	}

	jQuery(document).ready(function() {
		filterSearch = searchObj;
		$(".count-badge-filter").remove();
		if(typeof paramsFilter.filters["network"] != "undefined"){
			filterSearch.filters.actions.themes.callBack = function(data){
				filterSearch.filters.actions.themes.isLoaded=false;
				let network = [];
				for (const [key, value] of Object.entries(data)) {
					if(typeof value.extraInfo != "undefined" && typeof value.extraInfo.reteofapartment != "undefined" && value.extraInfo.reteofapartment != ""){
						if(Array.isArray(value.extraInfo.reteofapartment)){
							let rete = value.extraInfo.reteofapartment;
							network.push(...rete);
						}else if(value.extraInfo.reteofapartment.includes(",")){
							network.push(...value.extraInfo.reteofapartment.split(","));
						}else{
							network.push(value.extraInfo.reteofapartment);
						}
					}
				}
				paramsFilter.filters["network"]["list"] = Array.from(new Set(network))
				filterSearch.filters.initViews(filterSearch, paramsFilter);
				filterSearch.filters.initEvents(filterSearch, paramsFilter);
				filterSearch.filters.initDefaults(filterSearch, paramsFilter);
				$(filterSearch.container+' .badge-theme-count').each(function(index) {
	              if($(this).text()=="0"){
	                filterSearch.filters.actions.themes.isLoaded = false; 
	              }
	            });
				if(network.length > 0 && (typeof costum.lists.network == "undefined" || (typeof costum.lists.network != "undefined" && costum.lists.network.length != network.length))){
					dataHelper.path2Value({
						id: costum.contextId,
						collection: costum.contextType,
						path: "costum.lists.network",
						value: Array.from(new Set(network))
					}, function(params) {
						costum.lists["network"] = Array.from(new Set(network));
					});
				}
				filterSearch.filters.actions.themes.setThemesCounts(filterSearch);
				filterSearch.filters.actions.themes.callBack = function(res){}
			}
		}

		searchObj.search.sortAlphaNumeric = function(results){
    		mylog.log("searchObj.search.callBack tosort", results);
			let sorted = Object.values(results).sort((a, b) => {
				let fa = a.name.replace(" ", "").trim().toLowerCase(),
					fb = b.name.replace(" ", "").trim().toLowerCase();

				return ('' + fa).localeCompare(fb);
			});
			let sortedResults = {}
			$.each(sorted, function(i, val){
				sortedResults[val._id.$id] = val;
			});
			return sortedResults;
    	};

		searchObj["scroll"].render = function(fObj, results, data){
			results = searchObj.search.sortAlphaNumeric(results);
      		//$(fObj.results.dom).html(JSON.stringify(data));
     		mylog.log("searchObj.results.render", fObj.search.loadEvent.active, fObj.agenda.options, results, data);
     		if(fObj.results.map.active){
				// if(notEmpty(results)){
     			    fObj.results.addToMap(fObj, results, data);
				// }else{
				// 	toastr.error("Aucun résultat trouvé. Essayez avec un terme plus précis.");					
				// }	
     		}else{
				str = "";
				if( fObj.search.loadEvent.active == "agenda" &&
					(  Object.keys(results).length > 0 ) ){
					str = fObj.agenda.getDateHtml(fObj);
				}
				if(Object.keys(results).length > 0){
					//parcours la liste des résultats de la recherche
					str += directory.showResultsDirectoryHtml(results, fObj.results.renderView, fObj.results.smartGrid, fObj.results.community, fObj.results.unique);
					if(Object.keys(results).length < fObj.search.obj.indexStep && fObj.search.loadEvent.active != "agenda")
						str += fObj.results.end(fObj);
				}else if( fObj.search.loadEvent.active != "agenda" )
					str += fObj.results.end(fObj);
				if(fObj.results.smartGrid){
					$str=$(str);
					fObj.results.$grid.append($str).masonry( 'appended', $str, true ).masonry( 'layout' ); ;
				}else
					$(fObj.results.dom).append(str);

				fObj.results.events(fObj);
				if(fObj.results.map.sameAsDirectory)
					fObj.results.addToMap(fObj, results);

				const arr = document.querySelectorAll('img.lzy_img')
				arr.forEach((v) => {
					if (fObj.results.smartGrid) {
						v.dom = fObj.results.dom;
					}
					imageObserver.observe(v);
				});
			 }
		}

		filterSearch = filterSearch.init(paramsFilter);
	});


</script>
