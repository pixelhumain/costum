<?php 
/**
 * TPLS QUI PERMET AFFICHAGE DES NEWS
 */
$keyTpl = "appRessource";

$paramsData = [ "title" => "Ressources",
                "color" => "#000000",
                "icon"  => "",
                "background" => "#FFFFFF",
                "limit" => 3
                ];

if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}
?>

<h1 style="color:<?= $paramsData["color"] ?>;background-color: <?= $paramsData["background"] ?>;" class="text-center">
    <span style="text-decoration: underline; text-underline-position: under;">
        <i class="fa <?= @$paramsData['icon'] ?>" aria-hidden="true"></i>
        <?= $paramsData["title"] ?>
    </span>
</h1>

<div id="content-results-ressources">

</div>
<div class="col-xs-12">
    <center>
    <?php  echo $this->renderPartial("costum.views.tpls.editTplBtns", ["path" => @$pathId , "canEdit" => $canEdit, "keyTpl"=>$keyTpl, "page" => @$page, "id" => (String) @$blockCms["_id"]]); ?>
    </center>
</div>
<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
page = <?= json_encode(@$page); ?>;
type = 'tpls.blockCms.app.<?= $keyTpl ?>';

$(document).ready(function(){
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section les sondages",
            "description" : "Personnaliser votre section sur les sondages",
            "icon" : "fa-cog",
            onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : {
                "title" : {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                icon : { 
                    label : "Icone",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.icon
                },
                "color" : {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                "limit" : {
                    label : "Limite de post",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.limit
                },
                background: {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            save : function () {  
                tplCtx.value = {};

                if (typeof idToPath != "undefined") {
                    tplCtx.value["id"] = idToPath;
                }

                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= @$page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.news.<?= $keyTpl ?>';
                
                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
        idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

    var params = {
        "source" : costum.contextSlug,
        "limit" : <?= $limit ?>
    };

    ajaxPost(
        null,
        baseUrl+"/costum/costumgenerique/getressources",
        params,
        function(data){
            mylog.log("success", data);
            var str = "";
            if(data.result == true)
            {
                var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                $(data.element).each(function(key,value){

                    var img = (typeof value.profilMediumImageUrl  != "undefined" && value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;

                    str += "<div style='' class='card-info col-md-4 text-center'>";

                    str += "<center><img src='"+img+"' class='img-responsive' style='border-radius : 50%; width: 150px; height: 150px;'></center>";
                    str += "<h2 style='color : black !important;' class='color-h2'>"+value.name+"</h2>";
                    str += "<p style='color : black !important;'>"+value.description+"</p>";
                    str += "<p><a href='#page.type.classifieds.id."+value.id+"' class='lbh-preview-element col-xs-12 no-padding'><i title='Ouvrir cette annonce' class='fa fa-plus-circle' aria-hidden='true'></i></a></p>";
                    str += "</div>";
                });
            }
            else
            {
                str += "Il n'éxiste aucune annonce";
            }
            $("#content-results-ressources").html(str);
        },
        null,
        "json",
        {async : false}
    );

// pageProfil.views.directoryRessources = function(){
//     //Ressource
// 	getAjax('', baseUrl+'/'+moduleId+'/element/getdatadetail/type/'+costum.contextType+'/id/'+costum.contextId+'/dataName/ressources/limit/<?= $limit ?>?tpl=json',
// 		function(data){ 
// 			var type = ($.inArray(donnee["pageProfil.params.dir"], ["poi","ressources","vote","actions","discuss"]) >=0) ? donnee["pageProfil.params.dir"] : null;

// 			if(typeof donnee["pageProfil.params.dir"] != "undefined" && donnee["pageProfil.params.dir"])
// 				canEdit=donnee["pageProfil.params.dir"];

// 			mylog.log("DATA HERE", data);
// 			var html = "";
// 			html = html + directory.showResultsDirectoryHtml(data, null, 3, canEdit);

// 			$("#content-results-ressources").html(html);
// 			if(typeof mapCO != "undefined"){
// 				mapCO.clearMap();
// 				mapCO.addElts(data);
// 				mapCO.map.invalidateSize();
// 			}
// 			coInterface.bindButtonOpenForm();
// 		}
// 	,"html");
//     };
//     pageProfil.views.directoryRessources();
});
        </script>