<?php
$cssAndScriptFilesModule = array(
    '/js/default/profilSocial.js',
  );
HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl()); 
/**
 * TPLS QUI PERMET AFFICHAGE DES ANNUAIRES
 */
$keyTpl = "appDda";

$paramsData = [ "title"         =>  "Sondages",
                "color"         =>  "#000000",
                "icon"          =>  "",
                "surligneTitle" => true,
                "background"    =>  "#ffffff",
                "limit"         =>  3
                ];

if (isset($blockCms)) {
    $tplsCms = ( !empty($blockCms["tpls"][$keyTpl]) && isset($blockCms["tpls"][$keyTpl]) ) ? $blockCms["tpls"][$keyTpl] : $blockCms;
    foreach ($paramsData as $e => $v) {
        if ( !empty($tplsCms[$e]) && isset($tplsCms[$e]) ) {
            if ($e == "surligneTitle") {
                if ($tplsCms[$e] == "true") {
                    $underline = "underline";
                    $underlinePosition = "under";
                }else{
                    $underline = "none";
                    $underlinePosition = "none";
                }
            }
            $paramsData[$e] = $tplsCms[$e];
        }
    }
}

?>
<h1 style="color:<?= $paramsData["color"] ?>;background-color: <?= $paramsData["background"] ?>;" class="text-center">
    <span style="text-decoration: <?= $underline ?>; text-underline-position: <?= $underlinePosition ?>;">
        <i class="fa <?= @$paramsData['icon'] ?>" aria-hidden="true"></i>
        <?= $paramsData["title"] ?>
    </span>
</h1>

<div id="content-results-dda" class="row">

</div>
<center>
<?php  echo $this->renderPartial("costum.views.tpls.editTplBtns", ["path" => @$pathId , "canEdit" => $canEdit, "keyTpl"=>$keyTpl, "page" => @$page, "id" => (String) @$blockCms["_id"]]); ?>
</center>

<script type="text/javascript">
sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
tplCtx = {};
sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
page = <?= json_encode(@$page); ?>;
type = 'tpls.blockCms.app.<?= $keyTpl ?>';
$(document).ready(function(){

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Configurer la section d'annonce",
            "description" : "Personnaliser votre section sur les annonces",
            "icon" : "fa-cog",
            onLoads : {
                onload : function(){
                    $(".parentfinder").css("display","none");
                }
            },
            "properties" : {
                title: {
                    label : "Titre",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.title
                },
                surligneTitle : {
                    "inputType" : "checkboxSimple",
                    "label" : "Souligner le titre",
                    "params" : {
                        "onText" : "Oui",
                        "offText" : "Non",
                        "onLabel" : "Oui",
                        "offLabel" : "Non",
                        "labelText" : "Souligner le titre"
                    },
                    "checked" : sectionDyf.<?php echo $keyTpl ?>ParamsData.surligneTitle
                },
                icon : { 
                    label : "Icone",
                    inputType : "select",
                    options : <?= json_encode(Cms::$icones); ?>,
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.icon
                },
                limit: {
                    label : "Limite de post",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.limit
                },  
                color: {
                    label : "Couleur du texte",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.color
                },
                background: {
                    label : "Couleur du fond",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.background
                },
                parent : {
                    inputType : "finder",
                    label : tradDynForm.whoiscarrytheproject,
                    multiple : true,
                    rules : { required : true, lengthMin:[1, "parent"]}, 
                    initType: ["organizations"],
                    openSearch :true
                }
            },
            save : function () {  
               tplCtx.value = {};
                
                if (typeof idToPath != "undefined") {
                    tplCtx.value["id"] = idToPath;
                }

                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent") {
                        tplCtx.value["parent"] = formData.parent ;
                    }
                });

                tplCtx.value["page"] = '<?= $page ?>';
                tplCtx.value["type"] = 'tpls.blockCms.article.<?= $keyTpl ?>';

                mylog.log("save tplCtx",tplCtx);
                
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = (typeof $(this).data("path") != "undefined" && $(this).data("path") != "") ? $(this).data("path") : "allToRoot";
        idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

    var data = {
      "searchType" : ["proposals"] ,
      "indexMin" : 0, 
      "initType" : "proposals" , 
      "count" : "true" , 
      "countType" : ["proposals"], 
      "sourceKey" : [costum.contextSlug] , 
      "indexStep" : 10,
      "costumSlug" : costum.contextSlug
    };

    var str = "";
    ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data,
        function(data){
            if(!data){
                toastr.error(data.content);
            }
            else{
                console.dir(data);
                $.each(data,function(k,v){
                    // str += directory.coopPanelHtml(v);
                    str += directory.showResultsDirectoryHtml(v);
                });
                mylog.log("str here",str);
                $("#content-results-dda").html(str);
                $.unblockUI();
            }
        },
        function (data){
            mylog.log("data",data);
        },
        "json"
    );
});
</script>