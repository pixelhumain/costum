<script type="text/javascript">

		//contains all dynform definitions for complexe inputs
		//it also carries sub dynforms, when an input needs extra parameters to 
		var sectionDyf = {};
		var tplCtx = {};
		
		var DFdata = {
			'tpl'  : '<?php echo $tpl ?>',
			"id"   : "<?php echo $this->costum["contextId"] ?>"
		};
		costum.col = "<?php echo $this->costum["contextType"]  ?>";
		costum.ctrl = "<?php echo Element::getControlerByCollection($this->costum["contextType"]) ?>";

		var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
		var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
</script>
		
<?php
if( !isset( $el["costum"]["slug"] ) || (isset( $el["costum"]["slug"]) && $el["costum"]["slug"] != $tpl ) ) 
  $test=$tpl;
if( isset($test) || $canEdit ) 
{
    ?>

<script type="text/javascript">

    // function getdynFormTpl (keyTpl , id, collection, path, idToPath) {
    //     $('.edit'+keyTpl+"Params").off().on('click', function() {
    //         if (typeof id != "undefined" && id != "") {
    //             tplCtx.id = id;
    //         }
    //         if (typeof collection != "undefined" && collection != "") {
    //             tplCtx.collection = collection;
    //         }
    //         if (typeof path != "undefined" && path != "") {
    //             tplCtx.path = path;
    //         }
    //         idToPath = (typeof <?= json_encode(@$idToPath) ?> != "undefined" ) ? <?= json_encode(@$idToPath) ?> : "";

    //         dyFObj.openForm(sectionDyf.+keyTpl+Params,null,sectionDyf.+keyTpl+ParamsData);
    //     });
    // }

	function saveThisTpl (slug) { 
		data = {
			collection : costum.contextType,
			id : costum.contextId,
			path : "costum.slug",
			value : '<?php echo $tpl ?>'
		}
		mylog.log(".saveThisTpl","data",data);
		dataHelper.path2Value( data, function(params) { 
			var s = location.href;
			var a = s.split('/');
			s = s.replace(a[a.length-2] + '/', '');
			s = s.replace(a[a.length-1], '');
			location.href = s;
		} );
	}
	function previewTpl () { 
		$('#acceptAndAdmin,.editBtn, .editThisBtn, .editQuestion,.addQuestion,.deleteLine, .previewTpl').fadeOut(); 
	}

jQuery(document).ready(function() {

    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dataObj = {parent : {} };
        dataObj.parent[costum.contextId] = {collection : costum.col , name : costum.title}; 
        dataObj.structags = $(this).data("tag");

        dyFObj.openForm('cms',null,dataObj,null,dynFormCostumCMS)
    });


    mylog.log("render","/modules/costum/views/tpls/acceptAndAdmin.php");
        $(".editBtn").off().on("click",function() { 
            var activeForm = {
                "jsonSchema" : {
                    "title" : "Template config",
                    "type" : "object",
                    "properties" : {
                        
                    }
                }
            };
            if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
            	activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
            else
            	activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };
            
            if($(this).data("label"))
            	activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
            if($(this).data("type")){
            	activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
            	if($(this).data("type") == "textarea" && $(this).data("markdown") )
            		activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
            }

            
            tplCtx.id = contextData.id;
			tplCtx.collection = contextData.type;
            tplCtx.key = $(this).data("key");
            tplCtx.path = $(this).data("path");
            
            activeForm.jsonSchema.save = function () {  
                tplCtx.value = $( "#"+tplCtx.key ).val();
                mylog.log("activeForm save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                	toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        location.reload();
                    } );
                }

        	}
            dyFObj.openForm( activeForm );
    });

    $(".deleteLine").off().on("click",function (){
        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
        var id = $(this).data("id");
        var type = $(this).data("collection");
        var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
        bootbox.confirm(trad.areyousuretodelete,
            function(result){
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } 
                else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.success("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                            urlCtrl.loadByHash( location.hash );
                        },
                        null,
                        "json"
                    );
                }
            }
        );
    });
    //used to not delete an elemetn 
    $(".unsetLine").off().on("click",function (){
        alert(".unsetLine")
        $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
        var linePath = $(this).data("id");
        var id = $(this).data("id");
        var type = $(this).data("collection");

        bootbox.confirm(trad.areyousuretodelete,
            function(result){
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } 
                else {
                    alert("unsetLine "+linePath);
                    tplCtx = {
                        collection : "answers" ,
                        id : answerId ,
                        path : linePath ,
                        value : null
                    }
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#"+type+id).remove();
                    } );
                }
            }
        );
    });

	$(".editTpl").off().on("click",function() { 
		var activeForm = {
            "jsonSchema" : {
                "title" : "Edit Question config",
                "type" : "object",
                "properties" : {}
            }
        };
    
    	$.each( formTpls [ $(this).data("key") ] , function(k,val) { 
    		mylog.log("formTpls",k,val); 
    		activeForm.jsonSchema.properties[ k ] = {
    			label : k,
    			value : val
    		};
    	 });
        
        mylog.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
        tplCtx.id = $(this).data("id");
        tplCtx.key = $(this).data("key");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path");
        
        activeForm.jsonSchema.save = function () {  
            tplCtx.value = {};
            $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        		tplCtx.value[k] = $("#"+k).val();
        	});
            mylog.log("save tplCtx",tplCtx);
            if(typeof tplCtx.value == "undefined")
            	toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#ajax-modal").modal('hide');
                    location.reload();
                } );
            }

    	}
        dyFObj.openForm( activeForm );
    });

    $(".addTpl").off().on("click",function() { 

        var optionsJs = <?= json_encode(Cms::$option); ?>;
        var page = <?= json_encode(@$page); ?>;

        var activeForm = {
            "jsonSchema" : {
                "title" : "Ajouter une section",
                "type" : "object",
                "properties" : {
                    type : { 
                        label : "Choisir le template",
                        inputType : "select",
                        options : optionsJs,
                        value : "text" 
                    },
                    page : {
                        inputType : "hidden",
                        value : page
                    }
                }
            }
        };          
        
        activeForm.jsonSchema.afterBuild = function(){
            dyFObj.setMongoId(activeForm,function(){
                uploadObj.gotoUrl
            });
        };
        tplCtx.key = $(this).data("key");
        tplCtx.collection = $(this).data("collection");
        
        activeForm.jsonSchema.save = function () {
            tplCtx.value = {
                type : $("#type").val(),
                page : $("#page").val(),
            };

            tplCtx.value.parent = {};
            tplCtx.value.parent[costum.contextId] = { type : costum.contextType, name : costum.slug };

            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                mylog.log("activeForm save tplCtx",tplCtx);
                dataHelper.path2Value( tplCtx, function(params) {
                    toastr.success("<i class='fa fa-check' aria-hidden='true'></i> Élément bien ajouter");
                    $("#ajax-modal").modal('hide');
                    urlCtrl.loadByHash( location.hash )
                } );
            }
        }
        dyFObj.openForm( activeForm );
    });
});
</script>

<div class="padding-20 alert-danger text-center"  id="acceptAndAdmin" >

  	<?php if(isset($test)  ){ ?>
  	<strong>Hey!</strong> Is this what you want ? 
	<a href="javascript:;" onclick=" saveThisTpl('<?php echo $tpl ?>') " class="btn btn-danger"> Save This Template</a> <a href="/costum/co/index/slug/<?php echo $slug ?>/test/costumBuilder" class="btn btn-default"> Try another Template </a>
	
	<?php } else if(isset($this->costum["dynForm"])) {
        ?>
	<a class="btn btn-default" href="javascript:previewTpl();"><i class="fa fa-eye"></i> Preview</a>
    <a class="btn btn-default" href="#@<?= $this->costum["contextSlug"] ?>" data-hash="#page.type.<?= $this->costum["contextType"] ?>.id.<?= $this->costum["contextId"] ?>" target="_blank"><i class="fa fa-id-card-o" aria-hidden="true"></i>
Element </a>
    <?php echo $this->renderPartial("costum.views.tpls.blocCss", array("canEdit" => $canEdit));   ?>
    <?php echo $this->renderPartial("costum.views.tpls.blocApp", array("canEdit" => $canEdit));   ?>
    <?php echo $this->renderPartial("costum.views.tpls.blocEditJson", array("canEdit" => $canEdit));   ?>
	<?php } else  { ?>
	<strong>hey petit pixel</strong> rajoute une balise dynform sur ton template pour le rendre editable. 
	<?php } ?>
</div>

<?php } ?>