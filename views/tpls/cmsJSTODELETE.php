<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.cmsJS",'tag : <?php echo $tag ?>');
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    bindCMSBtns(dynFormCostumCMS);

});

function bindCMSBtns(dFCostum){
    alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dFCostum)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('cms',null,{structags:$(this).data("tag") },null,dFCostum)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                    ajaxPost(
                        null,
                        urlToSend,
                        null,
                        function(data) {
                            if ( data && data.result ) {
                                toastr.info("élément effacé");
                                $("#"+type+id).remove();
                            } else {
                                toastr.error("something went wrong!! please try again.");
                            }
                        },
                        null,
                        "json"
                    );
                }
            });

    });
}
</script>