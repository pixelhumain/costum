<?php 
    $keyTpl = "map";

    $paramsData = [
        "info"=>[
            "show"=>true,
            "title"=>"",
            "description"=>""
        ],
        "filterConfig"=>[
            "defaults"=>[
                "indexStep"=>0,
                "types"=>[]
            ],
            "filters"=>[
                "scope"=>true,
                "text"=>true
            ]
        ],
        "embeded" => [
            "hideHeader" => false,
            "hideFooter" => false
        ]
    ];

    if( isset($this->costum[$keyTpl]) ) {
        $costumParams = $this->costum[$keyTpl];
        if(isset($costumParams["info"]))
            $paramsData["info"] = $costumParams["info"];
        if(isset($costumParams["filterConfig"]))
            $paramsData["filterConfig"] = $costumParams["filterConfig"];
    }
?>

<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>'
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'
    >
        <i class="fa fa-map-o" aria-hidden="true"></i> Map
    </a>
<?php } ?>

<script>
    var path2valuePayload = {}
    var <?= $keyTpl ?>Helper = {
        addFilter: function(name){
            var filterKey = `filter_${Date.now()}`,
                sectionDyfParamData = {
                    "view" : "",
                    "type" : "tags",
                    "name" : name,
                    "event" : "tags",
                    "list" : []
                },
                sectionDyfParamView = {
                    "label":"<?php echo Yii::t('cms', 'View type')?>",
                    "inputType":"select",
                    "options":{
                        dropdownList:"<?php echo Yii::t('cms', 'Dropdown list')?>"
                    }
                },
                sectionDyfParamTags = {
                    "label":"<?php echo Yii::t('common', 'Tags')?>",
                    "inputType":"tags"
                }
            
            //append sectionDyf paramsData;
            sectionDyf.<?= $keyTpl ?>ParamsData.filterConfig.filters[filterKey] = sectionDyfParamData

            //append sectionDyf params
            sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties[filterKey + "_view"] = sectionDyfParamView;
            sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties[filterKey + "_list"] = sectionDyfParamTags;

            //reopen form
            dyFObj.openForm(sectionDyf.<?= $keyTpl ?>Params, null,sectionDyf.<?= $keyTpl ?>ParamsData);

            //add fiedset
            <?= $keyTpl ?>Helper.addFieldset([{key:filterKey, name:name}])

            //append btn add more filter
            <?= $keyTpl ?>Helper.appendBtnAddMoreFilter()
            
        },
        addFieldset: function(groups){
            var styles = ""
            groups.forEach((item)=>{
                var filterKey = item.key, name = item.name,
                    classes = [`.${filterKey}_viewselect`, `.${filterKey}_listtags`], 
                    fildsetContentClass = `${filterKey}_fieldset_fildsetParentClass`;
                wrapToDiv(classes, "<?= $keyTpl ?>", fildsetContentClass, 12, "", "costum.<?= $keyTpl ?>", "filterConfig.filters."+filterKey)
                //add label to the fieldset
                styles += ` .${fildsetContentClass}:before{content:"${name}"}`
            })
            //append fieldset label
            $("head").append(`<style>${styles}</style>`)
        },
        initSectionDyfParams: function(){
            var paramsData = <?php echo json_encode( $paramsData ); ?>;

            var params = {
                jsonSchema:{
                    title:"<?php echo Yii::t('cms', 'Map configuration')?>",
                    icon:"fa-map-o",
                    properties:{
                        title:{
                            label: "<?php echo Yii::t('cms', 'Title')?>",
                            inputType: "text",
                            value: paramsData.info.title
                        },
                        description:{
                            label: "<?php echo Yii::t('common', 'Description')?>",
                            inputType: "textarea",
                            value: paramsData.info.description
                        },
                        types:{
                            label: "<?php echo Yii::t('common', 'Type')?>",
                            inputType:"selectMultiple",
                            isSelect2:true,
                            options: {
                                projects:"<?php echo Yii::t('common', 'Projects')?>",
                                organizations:"<?php echo Yii::t('common', 'Organizations')?>",
                                poi:trad.pointsinterests
                            },
                            value: paramsData.filterConfig.defaults.types
                        },
                        search:{
                            label: "<?php echo Yii::t('cms', 'Show search by location')?>",
                            checked: paramsData.filterConfig.filters.scope,
                            inputType:"checkboxSimple",
                            params:{
                                onLabel:"<?php echo Yii::t('common', 'Yes')?>",
                                offLabel:"<?php echo Yii::t('common', 'No')?>",
                                offText:"<?php echo Yii::t('common', 'NO')?>",
                                onText:"<?php echo Yii::t('common', 'YES')?>"
                            },
                        },
                        text:{
                            label:"<?php echo Yii::t('cms', 'Show filter by text')?>",
                            checked:paramsData.filterConfig.filters.text,
                            inputType:"checkboxSimple",
                            params:{
                                onLabel:"<?php echo Yii::t('common', 'Yes')?>",
                                offLabel:"<?php echo Yii::t('common', 'No')?>",
                                offText:"<?php echo Yii::t('common', 'NO')?>",
                                onText:"<?php echo Yii::t('common', 'YES')?>"
                            }
                        },
                        hideHeader:{
                            label:"<?php echo Yii::t('cms', 'Hide top menu')?>",
                            checked:paramsData.embeded.hideHeader,
                            inputType:"checkboxSimple",
                            params:{
                                onLabel:"<?php echo Yii::t('common', 'Yes')?>",
                                offLabel:"<?php echo Yii::t('common', 'No')?>",
                                offText:"<?php echo Yii::t('common', 'NO')?>",
                                onText:"<?php echo Yii::t('common', 'YES')?>"
                            }
                        },
                        hideFooter:{
                            label:"<?php echo Yii::t('cms', 'Hide footer')?>",
                            checked:paramsData.embeded.hideFooter,
                            inputType:"checkboxSimple",
                            params:{
                                onLabel:"<?php echo Yii::t('common', 'Yes')?>",
                                offLabel:"<?php echo Yii::t('common', 'No')?>",
                                offText:"<?php echo Yii::t('common', 'NO')?>",
                                onText:"<?php echo Yii::t('common', 'YES')?>"
                            }
                        }
                    },
                    save:<?= $keyTpl ?>Helper.onSubmitForm
                }
            }

            $.each(paramsData.filterConfig.filters, function(key, value){
                if(["scope", "text"].indexOf(key) < 0){
                    params.jsonSchema.properties[key + "_view"] = {
                        label:"<?php echo Yii::t('cms', 'View type')?>",
                        inputType:"select",
                        options:{
                            dropdownList:"<?php echo Yii::t('cms', 'Dropdown list')?>"
                        },
                        value: value.type
                    }
                    params.jsonSchema.properties[key + "_list"] = {
                        label:"<?php echo Yii::t('common', 'Tags')?>",
                        inputType:"tags",
                        value: value.list
                    }
                }
            })

            sectionDyf.<?php echo $keyTpl ?>ParamsData = paramsData;
            sectionDyf.<?php echo $keyTpl ?>Params = params;         
        },
        appendBtnAddMoreFilter: function(){
            var $btnAddMoreFilter = $(`
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-top-35">
                    <button type="button" class="btn btn-block btn-lg btn-success"><?php echo Yii::t('cms', 'Add more filter')?></button>
                </div>
            `)
            $(".form-actions").before($btnAddMoreFilter);

            $btnAddMoreFilter.find("button").click(function(){
                bootbox.prompt("<?php echo Yii::t('cms', 'Enter the name of the filter')?>", function(result){
                    if(result){
                        <?= $keyTpl ?>Helper.addFilter(result)
                    }
                })
            })
        },
        openForm: function(){
            //init params
            <?= $keyTpl ?>Helper.initSectionDyfParams()
            var paramsData = sectionDyf.<?php echo $keyTpl ?>ParamsData;

            //open form
            dyFObj.openForm(
                sectionDyf.<?php echo $keyTpl ?>Params,
                null, 
                paramsData
            );
            //append fieldset after form open
            $.each(paramsData.filterConfig.filters, function(key, value){
                if(["scope", "text"].indexOf(key) < 0){
                    <?= $keyTpl ?>Helper.addFieldset([{key:key, name:value.name}])
                }
            })

            //append btn add filter after form open
            <?= $keyTpl ?>Helper.appendBtnAddMoreFilter()
        },
        onSubmitForm:function(formData){
            var data = sectionDyf.<?php echo $keyTpl ?>ParamsData;

            data.info = {
                show:true,
                title: formData.title,
                description: formData.description
            }
            data.embeded = {
                hideHeader : formData.hideHeader,
                hideFooter : formData.hideFooter
            }
            var elTypes=(typeof formData.types=="string") ? [formData.types] : formData.types;
            
            data.filterConfig.defaults = {
                indexStep:0,
                types:elTypes
            }
            data.filterConfig.filters.scope = formData.search
            data.filterConfig.filters.text = formData.text

            $.each(formData, function(key, value){
                //test if the key is like filter_1626093387638_view or filter_1626093387638_list
                if(/^(filter_)\d+_(list|view)/.test(key)){
                    var keyParts = key.split("_"),
                        filterKey = `${keyParts[0]}_${keyParts[1]}`;

                    switch(keyParts[2]){
                        case "view":
                            data.filterConfig.filters[filterKey].view = value
                        break;
                        case "list":
                            data.filterConfig.filters[filterKey].list = value.split(",")
                        break;
                    }
                }
            })

            path2valuePayload.value = data
            path2valuePayload.removeCache = true
            dataHelper.path2Value(path2valuePayload, function() { 
                $("#ajax-modal").modal('hide');
                toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                location.reload();
            });
        }
    }

    $(function(){
        $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
            path2valuePayload = {
                id: $(this).data("id"),
                collection: $(this).data("collection"),
                path: $(this).data("path")
            }
            <?= $keyTpl ?>Helper.openForm()
        });
    })
    
</script>