<?php 
    $keyTpl = "logo";
    $paramsData = [];

    if( isset($this->costum["htmlConstruct"]) )
    	$paramsData = $this->costum["htmlConstruct"];
?>
<style>
    .<?php echo $keyTpl ?>.left:before {
        content: "Haut à gauche";
    }
    .<?php echo $keyTpl ?>.right:before {
        content: "Haut à droite";
    }
    .<?php echo $keyTpl ?>.menuLeft:before {
        content: "Menu à gauche";
    } 
    #menuTopLeft .lbh-menu-app{

    }
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Logo')?>",
            "icon" : "fa-picture-o",
            "properties" : {
                "header-menuTop-left-buttonList-logo-width": {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo.width)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo.width : "50"
                },
                "header-menuTop-left-buttonList-logo-height": {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Height')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
                    	 sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo.height)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo.height : "50"
                },
                "header-menuTop-right-buttonList-logo-width": {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList) &&
                    		sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo.width)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo.width : "50"
                },
                "header-menuTop-right-buttonList-logo-height": {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Height')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList) &&
                    		sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo.height)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo.height : "50"
                },
                "menuLeft-buttonList-logo-width" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList) &&
                    		sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo.width)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo.width : "50"               	
                },
                "menuLeft-buttonList-logo-height" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Width')?>",
                    "rules" : {
                    	"number" : true,
                    	"min" : 0

                    },
                    value : (exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft) &&
                    	sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList) &&
                    		sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList != null &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo) &&
                    	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo.height)) ? sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo.height : "50"               	
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                kt = k.split("-").join("][");
                if(isActiveDivParent)        
                    tplCtx.value[kt] = $("#"+k).val();

                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    if(exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left) &&
	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left != null &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList) &&
	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList != null &&
	(typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.left.buttonList.logo == "undefined")){
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["header-menuTop-left-buttonList-logo-width"];
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["header-menuTop-left-buttonList-logo-height"];
	}

	if(exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header) &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop) &&
		sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop != null &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right) &&
		sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right != null &&
	exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList) &&
	sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList != null &&
	(typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.header.menuTop.right.buttonList.logo == "undefined")){
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["header-menuTop-right-buttonList-logo-width"];
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["header-menuTop-right-buttonList-logo-height"];
	}

	if(!exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft) &&
		sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft != null &&
	!exists(sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList) &&
	sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList != null &&
	(typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.menuLeft.buttonList.logo == "undefined")){
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["menuLeft-buttonList-logo-width"];
    	delete sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties["menuLeft-buttonList-logo-height"];		
	}

    $(".edit-logo").off().on("click",function() {
        tplCtx.id = '<?= $this->costum["contextId"]; ?>';
        tplCtx.collection = '<?= $this->costum["contextType"]; ?>';
        tplCtx.path = "costum.htmlConstruct";
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);

        var logoLeft = [],logoRight = [],logoMenuLeft = [];
        $.each(sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) {

            var key = k.split("-");
            if(key[2] == "left")
                logoLeft.push('.'+k+val.inputType);
            else if(key[2] == "right")
                logoRight.push('.'+k+val.inputType);
            else if(key[0] == "menuLeft")
                logoMenuLeft.push('.'+k+val.inputType);
        })

        wrapToDiv(logoLeft,"logo","left",12,"",tplCtx.path,'header.menuTop.left.buttonList.logo');
        wrapToDiv(logoRight,"logo","right",12,"",tplCtx.path,'header.menuTop.right.buttonList.logo');
        wrapToDiv(logoMenuLeft,"logo","menuLeft",12,"",tplCtx.path,'menuLeft.buttonList.logo');
	});
});
</script>

