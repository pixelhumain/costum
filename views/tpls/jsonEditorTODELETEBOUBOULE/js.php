<?php 
    $keyTpl = "js";

    $paramsData = [
        "urls" => [],
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }   
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-code" aria-hidden="true"></i> <?php echo $keyTpl ?> <span class="text-danger">(Developer only)</span>
    </a>
<?php }?>


<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "Fichier javascript",
            "icon" : "fa-cog",
            "properties" : {
                "urls": {
                    "inputType" : "array",
                    "label" : "<?php echo Yii::t('cms', 'Urls')?>",
                    "placeholder" : "javascript_filename.js",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.urls,
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array"){
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    }else{
                        tplCtx.value[k] = $("#"+k).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(document).on("click",".edit<?php echo $keyTpl ?>Params",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
