<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl.$type;

    $paramsData = [
        "background" => "#ffffff",
        "boxShadow" => "",
        "marginTop" => "",
        "app" => "",
        "button" => "",
        "scopeBtn" => "",
        "filterBtn" => "",
        "badge" => "",
        "connectBtn" => "",
        "userProfil" => "",
        "xsMenu" => "",
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$type][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$type][$i];   
        }
    }

    $width = "auto";
    $marginTop = "5";
    $paddingBottom = "10";
    $borderRadiusUserProfil = "50px";
    $displayUserProfil = "block";
    $textAlignUserProfil= "";
    $marginLeftUserProfil = "10";
    $widthUserProfil = "";
    if($type == "menuTop")
        $typeName ="Menu en haut";
    if($type == "menuLeft"){
        $typeName = "Menu à gauche";
        $width = "100%";
         $marginTop = "0";
         $borderRadiusUserProfil = "0";
         $displayUserProfil = "inline-block";
         $textAlignUserProfil = "center";
         $marginLeftUserProfil = "0";
         $widthUserProfil = "100%";
          //$paddingBottom = "";
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:void(0);' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>.<?php echo $type ?>'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> <?php echo $typeName ?>
    </a>
<?php }?>
<style>
    .<?php echo $subkeyTpl ?>.app-css:before {
        content: "Menu";
    }
    .<?php echo $subkeyTpl ?>.xsMenu-css:before {
        content: "Menu Xs";
    }
    .<?php echo $subkeyTpl ?>.button-css:before {
        content: "Boutton";
    }
    .<?php echo $subkeyTpl ?>.badge-css:before {
        content: "Badge";
    } 
    .<?php echo $subkeyTpl ?>.scopeBtn-css:before {
        content: "Boutton de recherche";
    }
    .<?php echo $subkeyTpl ?>.filterBtn-css:before {
        content: "Boutton filtre";
    }
    .<?php echo $subkeyTpl ?>.connectBtn-css:before {
        content: "Boutton de connexion";
    }
    .<?php echo $subkeyTpl ?>.design-css:before {
        content:"Design du menu";
    }
    .<?php echo $subkeyTpl ?>.design-xs-css:before {
        content:"Design du menu XS";
    }
    .<?php echo $subkeyTpl ?>.userProfil-css:before {
        content:"Profil d'utilisateur";
    }
</style>

<script type="text/javascript">

jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo $typeName; ?>",
            "icon" : "fa-cog",
            "properties" : {
                "background": {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    values : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.background,
                },
                "boxShadow": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Shadow')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "1px 3px 1px #ddd",
                    value : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.boxShadow
                },
                "marginTop": {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Margin top')?>",
                    values : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.marginTop,
                },
                //app*******
                "app-button-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "rules" : {
                        "number": true,
                        "required" :true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontSize : "24"
                },
                "app-button-fontWeight": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font weight')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontWeight !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.fontWeight : "normal"
                },
                "app-button-textTransform": { 
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Text transformation')?>",
                    "options" : {
                        "capitalize" : "<?php echo Yii::t('cms', 'Capitalize')?>",
                        "uppercase" : "<?php echo Yii::t('cms', 'Uppercase')?>",
                        "lowercase" : "<?php echo Yii::t('cms', 'Lowercase')?>"
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.textTransform !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.textTransform : "capitalize"
                },
                "app-button-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Label color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.color : "#000000"
                },
                //desing******************
                "app-button-design-clipPath": { 
                    "inputType" : "select",
                    "label":"Forme",
                    "options" : {
                        "polygon(16% 0, 83% 0, 100% 30%, 100% 70%, 87% 100%, 14% 100%, 0% 70%, 0% 30%)" : "Octogone",
                        "polygon(4% 0, 100% 0%, 95% 99%, 0% 100%)" : "Parallelogram",
                        "polygon(94% 0, 100% 50%, 94% 100%, 0% 100%, 6% 49%, 0% 0%)" : "Right Chevron",
                        "polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%)" :"Rabbet",
                        "polygon(13% 0, 100% 0, 100% 100%, 0% 100%)":"Trapèze",
                        "polygon(0 0, 100% 0%, 100% 100%, 0 100%)" : "Rectangle"
                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.clipPath)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.clipPath : ""
                },
                "app-button-design-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.background : ""
                },
                "app-button-design-width": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.width)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.width : "<?= $width ?>"
                },
                "app-button-design-height": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.height)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.height : "auto"
                }, 
                "app-button-design-paddingLeft": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingLeft)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingLeft : "10"
                },
                "app-button-design-paddingRight": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingRight)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingRight : "10"
                },
                "app-button-design-paddingTop": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingTop : "10"
                },
                "app-button-design-paddingBottom": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingBottom)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.paddingBottom : "<?= $paddingBottom ?>"
                },
                "app-button-design-display": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.display)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.display : "inline-block"
                },
                "app-button-design-hover-background": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.background : "grey"
                },
                "app-button-design-hover-color": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Text color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.color)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.color : "white"
                },
                "app-button-design-marginTop": { 
                    "inputType" : "text",
                    "label":"<?php echo Yii::t('cms', 'Margin top')?>",
                    rules : {
                        number:true,
                        min:0,
                        max:100,

                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.marginTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.marginTop : "<?= $marginTop ?>"
                },
                "app-button-design-textAlign": { 
                    "inputType" : "select",
                    "label":"<?php echo Yii::t('cms', 'Alignment')?>",
                    options : {
                        left:"<?php echo Yii::t('cms', 'Left')?>",
                        right:"<?php echo Yii::t('cms', 'Right')?>",
                        center:"<?php echo Yii::t('cms', 'Center')?>"

                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.textAlign)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.textAlign : "center"
                },          
                "app-button-design-hover-textDecoration": { //hover
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.textDecoration)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.app.button.design.hover.textDecoration : "none"
                },
                //Menu XS ***************************
                "xsMenu-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background menu xs')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.background !="undefined" ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.background : "24"
                },
                "xsMenu-button-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "rules" : {
                        "number": true,
                        "required" :true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.fontSize : "24"
                },
                "xsMenu-button-fontWeight": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font weight')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.fontWeight !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.fontWeight : "normal"
                },
                "xsMenu-button-textTransform": { 
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Text transformation')?>",
                    "options" : {
                        "capitalize" : "<?php echo Yii::t('cms', 'Capitalize')?>",
                        "uppercase" : "<?php echo Yii::t('cms', 'Uppercase')?>",
                        "lowercase" : "<?php echo Yii::t('cms', 'Lowercase')?>"
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.textTransform !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.textTransform : "capitalize"
                },
                "xsMenu-button-color": { 
                    "inputType" : "colorpicker",
                    "label" : "Couleur",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button !="undefined" &&
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.color : "#000000"
                },

                //DESIGN MENU XS-----------------------------------------
                //desing******************
                "xsMenu-button-design-clipPath": { 
                    "inputType" : "select",
                    "label":"Forme",
                    "options" : {
                        "polygon(16% 0, 83% 0, 100% 30%, 100% 70%, 87% 100%, 14% 100%, 0% 70%, 0% 30%)" : "Octogone",
                        "polygon(4% 0, 100% 0%, 95% 99%, 0% 100%)" : "Parallelogram",
                        "polygon(94% 0, 100% 50%, 94% 100%, 0% 100%, 6% 49%, 0% 0%)" : "Right Chevron",
                        "polygon(0% 15%, 15% 15%, 15% 0%, 85% 0%, 85% 15%, 100% 15%, 100% 85%, 85% 85%, 85% 100%, 15% 100%, 15% 85%, 0% 85%)" :"Rabbet",
                        "polygon(13% 0, 100% 0, 100% 100%, 0% 100%)":"Trapèze",
                        "polygon(0 0, 100% 0%, 100% 100%, 0 100%)" : "Rectangle"
                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.clipPath)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.clipPath : ""
                },
                "xsMenu-button-design-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.background : ""
                },
                /*"xsMenu-button-design-width": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.xsMenu.button.design.width)
                            ) ?
                            sectionDyf.<?php //echo $subkeyTpl ?>ParamsData.xsMenu.button.design.width : "<?= $width ?>"
                },*/
                "xsMenu-button-design-height": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.height)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.height : "auto"
                }, 
                "xsMenu-button-design-paddingLeft": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingLeft)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingLeft : "10"
                },
                "xsMenu-button-design-paddingRight": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingRight)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingRight : "10"
                },
                "xsMenu-button-design-paddingTop": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingTop : "10"
                },
                "xsMenu-button-design-paddingBottom": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingBottom)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.paddingBottom : "<?= $paddingBottom ?>"
                },
                "xsMenu-button-design-display": { 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.display)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.display : "inline-block"
                },
                "xsMenu-button-design-hover-background": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.background)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.background : "grey"
                },
                "xsMenu-button-design-hover-color": { //hover
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Text color on hover')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.color)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.color : "white"
                },
                "xsMenu-button-design-marginTop": { 
                    "inputType" : "text",
                    "label":"<?php echo Yii::t('cms', 'Margin top')?>",
                    rules : {
                        number:true,
                        min:0,
                        max:100,

                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.marginTop)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.marginTop : "<?= $marginTop ?>"
                },
                "xsMenu-button-design-textAlign": { 
                    "inputType" : "select",
                    "label":"<?php echo Yii::t('cms', 'Alignment')?>",
                    options : {
                        left:"<?php echo Yii::t('cms', 'Left')?>",
                        right:"<?php echo Yii::t('cms', 'Right')?>",
                        center:"<?php echo Yii::t('cms', 'Center')?>"

                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                                exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.textAlign)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.textAlign : "center"
                },          
                "xsMenu-button-design-hover-textDecoration": { //hover
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover) &&
                            exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.textDecoration)
                            ) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.xsMenu.button.design.hover.textDecoration : "none"
                },
                //END DESIGN MENU XS-------------------------------------
                /****************************************/
                //button*************
                "button-paddingTop": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Padding Top')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.paddingTop !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.paddingTop : "0"
                },
                "button-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "rules" : {
                        "number" :true,
                        "required":true
                    },
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize : "30"
                },
                "button-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color : "#000"
                },
                //scopeBtn*************
                "scopeBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.scopeBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.scopeBtn.color : "#000"
                },
                //filter
                "filterBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.filterBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.filterBtn.color : "#000"
                },
                //badge
                "badge-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.background !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.background : "#fff"
                },
                "badge-border": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border')?>",
                    "placeholder" : "50px solid white",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.border !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.badge.border : ""
                },
                //connectBtn*************
                "connectBtn-background": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.background !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.background : ""
                },
                "connectBtn-color": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.color : ""
                },
                "connectBtn-fontSize": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    "placeholder" : "18",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.fontSize : "18"
                },
                "connectBtn-borderRadius": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                    "placeholder" : "0",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.borderRadius !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.borderRadius : "0"
                },
                "connectBtn-padding": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Padding')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "8px 8px",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.padding) && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.padding !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.connectBtn.padding : ""
                },
                //userProfil***************
                "userProfil-background":{ 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background color')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.background)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.background : "#808080"
                },
                "userProfil-color":{ 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Name color')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.color)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.color : "#ffffff"
                },
                "userProfil-borderRadius":{ 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Border radius')?>",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.borderRadius)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.borderRadius : "<?= $borderRadiusUserProfil ?>"
                },
                "userProfil-display":{ 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.display)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.display : "<?= $displayUserProfil ?>"
                },
                "userProfil-textAlign":{ 
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Alignment')?>",
                    options : {
                        left: "<?php echo Yii::t('cms', 'Left')?>",
                        right: "<?php echo Yii::t('cms', 'Right')?>",
                        center: "<?php echo Yii::t('cms', 'Center')?>"
                    },
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.textAlign)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.textAlign : "<?= $textAlignUserProfil ?>"
                },
                "userProfil-marginLeft":{ 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.marginLeft) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.marginRight)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.marginLeft : "<?= $marginLeftUserProfil ?>"
                },
                "userProfil-width":{ 
                    "inputType" : "hidden",
                    value : (exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.width) && exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.marginRight)) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.userProfil.width : "<?= $widthUserProfil ?>"
                },
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                    if(isActiveDivParent){
                        kt = k.split("-").join("][");
                        tplCtx.value[kt] = $("#"+k).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        dyFObj.closeForm();
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    <?php if ($type == "menuLeft") { ?>
    $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
            kt = k.split("-");
            if((kt[0] == "app" && kt[2] != "design") || kt[0] == "scopeBtn" || kt[0] == "filterBtn" || kt[0] == "badge" || kt[0] == "connectBtn" || kt[0] == "xsMenu")
                delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties[k]
        });
    <?php } ?>

    <?php if ($type == "menuTop") { ?>
    $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
            kt = k.split("-");
            if(kt[0] == "marginTop")
                delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties[k]
        });
    <?php } ?>

    $(document).on("click",".edit<?php echo $subkeyTpl ?>Params",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        if(isDeveloper==false){
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.boxShadow;
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["button][paddingTop]"];
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties["connectBtn][padding]"];
        }
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        var arrApp = [], arrButton = [],arrScopeBtn = [],arrFilterBtn = [],arrBadge = [],arrConnectBtn = [],arrSelectDesign = [],arrUserProfil = [],arrXsMenu = [],arrXsSelectDesign = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {                
            kt = k.split("-");
            if (kt[0]=="app" && kt[1] == "button" && kt[2] == "design")
                arrSelectDesign.push('.'+k+val.inputType);
            else if (kt[0]=="app" && kt[1] == "button" && kt[2] != "design")
                arrApp.push('.'+k+val.inputType);
            else if(kt[0]=="button")
                arrButton.push('.'+k+val.inputType);
            else if(kt[0]=="badge")
                arrBadge.push('.'+k+val.inputType);
            else if(kt[0]=="scopeBtn")
                arrScopeBtn.push('.'+k+val.inputType);
            else if(kt[0]=="filterBtn")
                arrFilterBtn.push('.'+k+val.inputType);
            else if(kt[0]=="badge")
                arrBadge.push('.'+k+val.inputType);
            else if(kt[0]=="connectBtn")
                arrConnectBtn.push('.'+k+val.inputType);
            else if(kt[0]=="userProfil")
                arrUserProfil.push('.'+k+val.inputType);
            else if((kt[0]=="xsMenu" && kt[1] == "button" && kt[2] != "design") || kt[0]=="xsMenu" && kt[1] == "background" )
                arrXsMenu.push('.'+k+val.inputType); 
            else if (kt[0]=="xsMenu" && kt[1] == "button" && kt[2] == "design")
                arrXsSelectDesign.push('.'+k+val.inputType);
        });
        wrapToDiv(arrApp,"<?php echo $subkeyTpl ?>","app-css",3);
        wrapToDiv(arrSelectDesign,"<?php echo $subkeyTpl ?>","design-css",4,"",tplCtx.path,"app.button.design");
        wrapToDiv(arrButton,"<?php echo $subkeyTpl ?>","button-css",4);
        wrapToDiv(arrScopeBtn,"<?php echo $subkeyTpl ?>","scopeBtn-css",6,3);
        wrapToDiv(arrFilterBtn,"<?php echo $subkeyTpl ?>","filterBtn-css",6,3);
        wrapToDiv(arrBadge,"<?php echo $subkeyTpl ?>","badge-css",6);
        wrapToDiv(arrConnectBtn,"<?php echo $subkeyTpl ?>","connectBtn-css",2);   
        wrapToDiv(arrUserProfil,"<?php echo $subkeyTpl ?>","userProfil-css",3);
        wrapToDiv(arrXsMenu,"<?php echo $subkeyTpl ?>","xsMenu-css",3);
        wrapToDiv(arrXsSelectDesign,"<?php echo $subkeyTpl ?>","design-xs-css",4,"",tplCtx.path,"xsMenu.button.design");              
    });
});
</script>

