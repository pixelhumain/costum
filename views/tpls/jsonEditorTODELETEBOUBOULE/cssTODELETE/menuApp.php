<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."menuApp";

    $paramsData = [
        "background" => "",
        "button" => ""
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl]["menuApp"][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl]["menuApp"][$i];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>.menuApp'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Menu App
    </a>
<?php }?>
<style>
    .<?php echo $subkeyTpl ?>.button-css:before {
        content: "button";
    }
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Menu App')?>",
            "icon" : "fa-cog",
            "properties" : {
                "background": {
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Background')?>",
                    values : sectionDyf.<?php echo $subkeyTpl ?>ParamsData.background,
                },
                "button][fontSize]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Font size')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.fontSize : "30"
                },
                "button][color]": { 
                    "inputType" : "colorpicker",
                    "label" : "<?php echo Yii::t('cms', 'Color')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.color : "#000"
                },
                "button][hover][borderBottom]": { 
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'hover:border bottom')?>",
                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.hover !="undefined" && 
                            typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.hover.borderBottom !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.button.hover.borderBottom : "#000"
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array"){
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    }else{
                        if(k.indexOf("[") && k.indexOf("]"))                
                            kt = k.split("[").join("\\[").split("]").join("\\]");
                            tplCtx.value[k] = $("#"+kt).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        var arrButton = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
            if(k.indexOf("[") && k.indexOf("]"))                
                kt = k.split("[").join("\\[").split("]").join("\\]");
            if (k.split("]")[0]=="button")
                arrButton.push('.'+kt+val.inputType);   
        });
        wrapToDiv(arrButton,"<?php echo $subkeyTpl ?>","button-css",4);            
    });
});
</script>

