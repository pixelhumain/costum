<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."color";

    $paramsData = [
        "color" => [],
    ];
    if( isset($this->costum[$keyTpl]["color"]) ) {
        foreach($this->costum[$keyTpl]["color"] as $i => $v) {
                array_push($paramsData["color"],array("colorName" => $i,"colorValue" => $v));
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>.color'>
        <i class="fa fa-paint-brush" aria-hidden="true"></i> Couleurs
    </a>
<?php }?>


<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Colors')?>",
            "icon" : "fa-cog",
            "properties" : {
                "color" : {
                    "label" : "<?php echo Yii::t('cms', 'Colors')?>",
                    "inputType" : "lists",
                    "entries":{
                        "colorName":{
                            "type":"select",
                            "label" :"<?php echo Yii::t('cms', 'Color name')?>",
                            "class":"col-xs-5"
                        },
                        "colorValue":{
                            "type":"color",
                            "label" :"<?php echo Yii::t('cms', 'Value')?>",
                            "class":"col-xs-5"
                        }
                    }
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(k == "color"){
                        $.each(data.color,function(k,v){
                            var key = data.color[k]["colorName"];
                            var value = data.color[k]["colorValue"];
                            tplCtx.value[key] = value;
                        });
                        
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format = true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(document).on("click",".edit<?php echo $subkeyTpl ?>Params", function(event) {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        createColorSelect();
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
        event.stopImmediatePropagation()
    });
});

function createColorSelect(){
    dyFObj.init.buildEntryList = function(num, entry,field, value){
        mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
        countEntry=Object.keys(entry).length;
        defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
        
        str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
            $.each(entry, function(e, v){
                name=field+e+num;
                classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                str+="<div class='"+classEntry+"'>";
                if(typeof v.label != "undefined"){
                    str+='<div class="padding-5 col-xs-12">'+
                        '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                    '</div>'+
                    '<div class="space5"></div>';
                }
                valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                if(v.type=="hidden")
                    str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="text")
                    str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                else if(v.type=="textarea")
                    str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                else if(v.type=="select"){
                    str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                    //This is the select Options-----------------//
                    str+= createColorSelectOptions(valueEntry);
                    str+='</select>';             
                }
                else if(v.type=="color")
                    str+='<input type="color" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'"/>';                
                str+="</div>";
            });
            str+='<div class="col-sm-1">'+
                    '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                '</div>'+
            '</div>';
        return str;
    }; 
}

function createColorSelectOptions(current){
    var html = "";
    $.each(colorCommunecter,function(k,v){
        html+='<option value="'+v+'" '+((current == v) ? "selected" : "" )+' >'+v+'</option>';
    });
    return html;
}
</script>
