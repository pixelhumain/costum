
<script type="text/javascript">

jQuery(document).ready(function() {
    mylog.log("render","/modules/costum/views/tpls/elemCmsScript.php");
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout

    
    // /alert("costum.views.tpls.text");
    $(".editThisElemDataBtn").off().on("click",function (){
        var id = $(this).data("id");
        var col = $(this).data("type");
        var field = $(this).data("field");
        var property = $(this).data("property");
        mylog.log( "editThisElemDataBtn", col, id, field, property );
        if(typeof dyFObj.dynFormCostumCMS != "undefined")
            dyFObj.dynFormCostum = dyFObj.dynFormCostumCMS;
        
        dyFObj.editProperty(col,id,field,property);
    });

    $(".openAllAtOnce").off().on("click",function (){
        var id = $(this).data("id");
        var col = $(this).data("type");
        mylog.log( "openAllAtOnce", col, id );
        dyFObj.editProperty(col,id,null,null,true);
    });
    
    

    // $(".deleteThisElemDataBtn").off().on("click",function (){
    //     mylog.log("deleteThisBtn click");
    //       $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    //       var btnClick = $(this);
    //       var id = $(this).data("id");
    //       var type = $(this).data("type");
    //       var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
    //       bootbox.confirm(trad.areyousuretodelete,
    //         function(result) 
    //         {
    //             if (!result) {
    //               btnClick.empty().html('<i class="fa fa-trash"></i>');
    //               return;
    //             } else {
    //               $.ajax({
    //                     type: "POST",
    //                     url: urlToSend,
    //                     dataType : "json"
    //                 })
    //                 .done(function (data) {
    //                     if ( data && data.result ) {
    //                       toastr.info("élément effacé");
    //                       $("#"+type+id).remove();
    //                     } else {
    //                        toastr.error("something went wrong!! please try again.");
    //                     }
    //                 });
    //             }
    //         });

    // });

});
</script>