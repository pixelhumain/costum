<?php
$keyTpl = "thematique";
$paramsData = [
    "title" => "Titre block",
    "background_color" => "transparent",
    "background_color2" => "#1A2660",
    "collapse"=> " "

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style type="text/css">
    .card<?= $kunik?>{
        padding:30px 0px;
    }
    .card<?= $kunik?> .box-title h1 {
        color: #49CBE3;
    }

    .card<?= $kunik?> .card-container {
        -webkit-perspective: 800px;
        -moz-perspective: 800px;
        -o-perspective: 800px;
        perspective: 800px;
        margin-bottom: 30px;
    }
    /* flip the pane when hovered */
    .card<?= $kunik?> .card-container:not(.manual-flip):hover .card,
    .card<?= $kunik?> .card-container.hover.manual-flip .card{
        -webkit-transform: rotateY( 180deg );
        -moz-transform: rotateY( 180deg );
        -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
    }

    /* flip speed goes here */
    .card<?= $kunik?> .card {
        -webkit-transition: -webkit-transform .5s;
        -moz-transition: -moz-transform .5s;
        -o-transition: -o-transform .5s;
        transition: transform .5s;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
        position: relative;
    }

    /* hide back of pane during swap */
    .card<?= $kunik?> .front, .card<?= $kunik?> .back {
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        -o-backface-visibility: hidden;
        backface-visibility: hidden;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #1A2660;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.14);
    }

    /* front pane, placed above back */
    .card<?= $kunik?> .front {
        z-index: 2;
        touch-action: manipulation;
        cursor: pointer;
    }

    /* back, initially hidden pane */
    .card<?= $kunik?> .back {
        -webkit-transform: rotateY( 180deg );
        -moz-transform: rotateY( 180deg );
        -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
        z-index: 3;
    }

    .card<?= $kunik?> .back .btn-simple{
        /*position: absolute;*/
        left: 0;
        bottom: 4px;
    }
    /*        Style       */


    .card<?= $kunik?> .card{
        background: none repeat scroll 0 0 #FFFFFF;
        border-radius: 4px;
        color: #444444;
    }
    .card<?= $kunik?> .card-container, .card<?= $kunik?> .front, .card<?= $kunik?> .back {
        width: 100%;
        height: 350px;
        border-radius: 15px;
        -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
        /*box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);*/
        box-shadow: 0px 0px 6px silver;
    }
    .card<?= $kunik?> .card .cover{
        height: 280px;
        overflow: hidden;
        border-radius: 15px 15px 0px 0px;
    }
    .card<?= $kunik?> .card .cover img{
        width: 100%;
        height: 280px;
        object-fit: cover;
        object-position: center;
        /*touch-action: manipulation;
        cursor: pointer;*/
    }

    .card<?= $kunik?> .card .back .content .main {
        height: 210px;
        color: #fff;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .card .back .content .main p{
        font-size: 14px;
    }
    .card .back .content .main h4{
        text-transform: none;
    }

    .card<?= $kunik?> .card h5{
        margin: 5px 0;
        font-weight: 400;
        line-height: 20px;
    }

    .card<?= $kunik?> .card .footer {
        border-top: 1px solid #EEEEEE;
        color: #999999;
        margin: 15px 0 0;
        padding: 10px 0 0;
        text-align: center;
    }

    .card<?= $kunik?> .front .footer {
        margin: 0px 0 0;
    }
    .card<?= $kunik?> .front .footer h4 {
        font-family: 'NotoSans-Bold';
        text-transform: none;
        color: #ffffff;
    }
    .card<?= $kunik?> .card .footer .btn-simple{
        font-family: 'NotoSans-Regular';
        margin-top: -6px;
        color: #fff;
    }
    .card<?= $kunik?> .card .header {
        padding: 15px 20px;
        height: 75px;
        position: relative;
    }
    .card<?= $kunik?> .card .motto{
        font-family: 'NotoSans-Bold';
        border-bottom: 1px solid #EEEEEE;
        color: #49CBE3;
        font-size: 14px;
        font-weight: 400;
        padding-bottom: 10px;
        text-align: center;
        text-transform: none;
    }

    /*      Just for presentation        */



    .card<?= $kunik?> .btn-simple{
        opacity: .8;
        color: #666666;
        background-color: transparent;
    }

    .card<?= $kunik?> .btn-simple:hover,
    .card<?= $kunik?> .btn-simple:focus{
        background-color: transparent;
        box-shadow: none;
        opacity: 1;
    }
    .card<?= $kunik?> .btn-simple i{
        font-size: 16px;
    }

    .card<?= $kunik?> .co-scroll::-webkit-scrollbar {
        width: 3px;
        background-color: #F5F5F5;
        color: #49CBE3;
    }

    .card<?= $kunik?> ul {
        padding: 0.5em 1em 0.5em 2.3em;
        position: relative;
    }

    .card<?= $kunik?> ul li {
        line-height: 1.5;
        padding: 5px 0;
        list-style-type: none!important;
        margin-left: 5px;
        font-size: 16px;
    }

    .card<?= $kunik?> ul li:before {
        font-family: FontAwesome;
        content: "\f138";
        position: absolute;
        left : 1em;
        color: #49CBE3;
    }

    .card<?= $kunik?> .co-scroll ul li {
        font-family: 'NotoSans-Regular';
    }
    /*       Fix bug for IE      */

    @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
        .card<?= $kunik?> .front, .card<?= $kunik?> .back{
            -ms-backface-visibility: visible;
            backface-visibility: visible;
        }

        .card<?= $kunik?> .back {
            visibility: hidden;
            -ms-transition: all 0.2s cubic-bezier(.92,.01,.83,.67);
        }
        .card<?= $kunik?> .front{
            z-index: 4;
        }
        .card<?= $kunik?> .card-container:not(.manual-flip):hover .back,
        .card<?= $kunik?> .card-container.manual-flip.hover .back{
            z-index: 5;
            visibility: visible;
        }
    }



</style>

<div class="card<?= $kunik?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h2 class="title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h2>
    <div class="text-center edit<?= $kunik?> editSectionBtns">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
            <div class="text-center addElement<?= $kunik ?>">
                <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add content')?></button>
            </div>  
            <?php } ?>
        </div>
    </div> 
    <div class="sp-bg rotateCard" data-id="<?= $blockKey ?>" data-field="background_color2" data-value="<?= $paramsData['background_color2']; ?>" data-sptarget="bd" data-kunik="<?= $kunik?>" data-team="bg1">
    <?php foreach ($paramsData["collapse"] as $key => $value) { 
        ${'initFilesImage' . $key}= Document::getListDocumentsWhere(
          array(
            "id"=> $blockKey,
            "type"=>'cms',
            "subKey"=> (string)$key 
          ), "image"
        );
        ${'arrFileImage' . $key}= [];
        foreach (${'initFilesImage' . $key} as $k => $v) {
          ${'arrFileImage' . $key}[] =$v['imagePath'];
        }
        if(isset($value["items"])){
            foreach($value["items"] as $kIt => $vIt){
                $value["items"][$kIt]["name"] = str_replace("'","ʼ",$vIt["name"]);
            }
        }
        ?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card-container manual-flip">
                <div class="card">
                    <div class="front bg1" onclick="rotateCard(this)" data-sptarget="background">
                        <div class="cover">
                            <?php if (count(${'arrFileImage' . $key})!=0) {?>
                                <img  src="<?= ${'arrFileImage' . $key}[0]?>"/>
                            <?php }else{ ?>
                                <img  src="https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg"/>
                            <?php } ?>
                        </div>

                        <?php if(Authorisation::isInterfaceAdmin()){ ?>
                            <div class="text-center editSectionBtns hiddenPreview" >
                                <a  href="javascript:;"class="btn  btn-primary editElement<?= $blockCms['_id'] ?>"
                                    data-key="<?= $key ?>" 
                                    data-title="<?= $value["title"] ?>" 
                                    data-image='<?php echo json_encode(${"initFilesImage" . $key}) ?>' 
                                    data-items='<?php echo json_encode(@$value["items"]) ?>' 
                                >
                                    <i class="fa fa-edit"></i>
                                </a>
                                    
                                <a  href="javascript:;"
                                class="btn  bg-red text-center deleteElement<?= $kunik?> "
                                data-id ="<?= $blockKey ?>"
                                data-path="collapse.<?= $key ?>"
                                data-collection = "cms"
                                >
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>					
                        <?php } ?>
                        <div class="footer">
                            <h4 class="text-center "><?= @$value["title"] ?></h4>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back bg1" data-sptarget="background">
                        <div class="header">
                            <h5 class="motto"><?= @$value["title"] ?></h5>
                        </div>
                        <div class="content">
                            <div class="main co-scroll">
                                <ul>
                                    <?php foreach (@$value["items"] as $ki => $vi) { ?>
                                        <li class="sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="collapse.<?= $key?>.items.<?= $ki?>.name"><?php echo @$vi["name"] ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="footer">
                            <button class="btn btn-simple" rel="tooltip" title="Flip Card" onclick="rotateCard(this)">
                                <i class="fa fa-reply"></i> Retour
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
</div>
<script type="text/javascript">
    $().ready(function(){
        $('[rel="tooltip"]').tooltip();
    });

    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {

                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();

                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });
        $(".addElement<?= $kunik ?>").click(function() {        
            var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($paramsData["collapse"]); ?>);
            var	lastContentK = 0; 
            if (keys<?= $blockCms['_id'] ?>.length!=0) 
                lastContentK = parseInt((keys<?= $blockCms['_id'] ?>[(keys<?= $blockCms['_id'] ?>.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockKey ?>";
            tplCtx.collection = "cms";

            tplCtx.path = "collapse."+(lastContentK+1);
            var obj = {
                subKey : (lastContentK+1),
                image :     $(this).data("image"),
                title :     $(this).data("title"),
                items:    $(this).data("items")
                
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add content')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                        $(".parentfinder").css("display","none");
                    }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) { 
                            tplCtx.value[k] = $("#"+k).val();
                            if(k == "items")
                                tplCtx.value[k] = data.items;
                        });
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }
                }
            };
            dyFObj.openForm( activeForm );
        });
        function getProperties(obj={}){
            var props = {
                title : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",
                    inputType : "text",
                    value : obj["title"]
                },
                items : {
                    inputType : "lists",
                    label : "<?php echo Yii::t('cms', 'Description')?>",
                    entries:{
                        key:{
                            "type":"hidden",
                            "class":""
                        },
                        name:{
                            "label":"Liste",
                            "type":"text",
                            "class":"col-xs-11"
                        }
                    },
                    value : obj["items"]
                    
                },
                image : {
                    inputType : "uploader",	
                    label : "<?php echo Yii::t('cms', 'Image')?>",
                    docType: "image",
                    contentKey : "slider",
                    domElement : obj["subKey"],		
                    filetype : ["jpeg", "jpg", "gif", "png"],
                    showUploadBtn: false,
                    endPoint :"/subKey/"+obj["subKey"],
                    initList : obj["image"]
                }
            };
            return props;
        }

        $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
            var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockCms['_id'] ?>"
            tplCtx.collection = "cms";
            tplCtx.path = "collapse."+(key);
            var obj = {
                subKey : key,
                title : 		$(this).data("title"),
                items:    $(this).data("items"),
                image:      		$(this).data("image"),
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Edit content')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?php echo $blockKey ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) { 
                            tplCtx.value[k] = $("#"+k).val();
                            if(k == "items")
                                tplCtx.value[k] = data.items;
                        });

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }
                }
            };          
            dyFObj.openForm( activeForm );				 
        });

        $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",

				function(result){
					if (!result) {
						return;
					}else {
						dataHelper.path2Value( deleteObj, function(params) {
							mylog.log("deleteObj",params);
							toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
							// urlCtrl.loadByHash(location.hash);
						});
					}
				}); 
			
		});
    });
</script>