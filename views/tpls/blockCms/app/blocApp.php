<?php 
  $keyTpl = "blocApp";
  $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
  $language =  "FR";
// if(isset($costum["contextType"]) && isset($costum["contextId"])){
//   $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
// }
?>
<style type="text/css" id="<?= $kunik ?>blocApp">
  
  .bloc-app-cocity_<?= $kunik?> .title-1 {
    margin-top: 5%;
    margin-bottom: 0;
  }
  .bloc-app-cocity_<?= $kunik?> h2{
    margin-top: 5%;
    margin-bottom: 5%;
    /* color: white; */
  }
  .bloc-app-cocity_<?= $kunik?> p{
    margin-top: 5%;
    /* color: white; */
    font-size: 16px;
    margin-left: 5%;
    margin-right: 9%;
  }
  .bloc-app-cocity_<?= $kunik?> .bouton{
    margin-top: 2%; 
    width: 85%;
    left: 8%;
    margin-bottom: 8%;
  }
  
  .colonne1 {
    background: #0A96B5;
    margin-bottom: 2%;
    max-height: 600px; 
    min-height: 520px;
  }
  .colonne2{
    background: #8ABF32;
    max-height: 600px; 
    min-height: 520px;
  }

  .bloc-app-cocity_<?= $kunik?> {
    padding-left: 100px;
    padding-right: 100px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne {
    padding-top: 1% ;
  }
  
  .bloc-app-cocity_<?= $kunik?> .bg1 {
    height: 370px;
    background: #d8dcd7;
    opacity: 0.2;
  }
  .bloc-app-cocity_<?= $kunik?> .bg2 {
    height: 276px;
    /* background: black;
    opacity: 0.5; */
    padding-top: 15%
  }
  /* .bloc-app-cocity_< ?= $kunik?> .btn-bloc{
    width : 215px;
    background: transparent;
    font-weight: bold;
    color: white !important;
    margin-top: 10px;
    text-transform:uppercase;
  } */
  .bloc-app-cocity_<?= $kunik?> .colonne1ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe18.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne2ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe19.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne3ligne1 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe20.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne1ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe23.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne2ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe22.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  .bloc-app-cocity_<?= $kunik?> .colonne3ligne2 {
    background: url(<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/cocity/Groupe21.jpg); 
    background-size: cover; 
    background-repeat: no-repeat;
    height: 269px;
  }
  @media (max-width: 978px) {
   
    .bloc-app-cocity_<?= $kunik?> h2{
      margin-top: 3%;
      font-size: 30px !important;
      margin-bottom: 5%;
    }
    .bloc-app-cocity_<?= $kunik?> p{
      font-size: 16px;
      margin-bottom: 4%;
    }
    .bloc-app-cocity_<?= $kunik?> {
      padding-left: 20px !important;
      padding-right: 20px !important;
    }
  }

  .centred-column {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap; /* Permet aux éléments de passer à la ligne si nécessaire */
  }

  .bg-vine {
    background-color: #092434 !important;
  }

</style>
<script type="text/javascript">
  var dyfOrga = {};
  var element = "";
  var cocityCoordonate = {};
  <?php  if(isset($el)){?>
    element = <?= json_encode($el)?>;
    cocityCoordonate["address"] = element.address;
    cocityCoordonate["geo"] = element.geo;
    cocityCoordonate["geoPosition"] = element.geoPosition;
  <?php } ?>
</script>
<div class="<?= $kunik?> bloc-app-cocity_<?= $kunik?>">
  <!-- <h2 class="title-1 sp-text img-text-bloc title< ?= $myCmsId ?>" id="sp-< ?= $blockKey ?>" data-id="< ?= $blockKey ?>" data-field="title"></h2> -->
  <div class="row centred-column">
    <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
      <div class=" colonne1ligne1">
        <div class="" >
          <div class="bg2" >
            <div class="teste">
              <h2 class="h2-bloc title-2 titre ">
                <?php echo Yii::t("cms", "I need help")?>
              </h2>
              <p class="para-bloc title-6 texte ">
                  <?php echo Yii::t("cms", "need help with your projects? call us!")?>
              </p>
            </div>
            <div class="">
              <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'},null,dyfOrga);" class="boutton btn btn-lg btn-default">
                  <?php echo Yii::t("cms", "Propose a request")?>
              </a>
            </div>
            <!-- <div class=" " >
              <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                  < ?php echo Yii::t("cms", "See more")?>
              </a>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
      <div class=" colonne2ligne1">
        <div class="" >
          <div class="bg2" >
            <div class="teste">
              <h2 class="h2-bloc title-2 titre ">
                  <?php echo Yii::t("cms", "I offer my services")?>
            </h2>
            <p class="para-bloc title-6 texte ">
                <?php echo Yii::t("cms", "need help with your projects? call us!")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'offer'},null,dyfOrga);" class="boutton btn btn-lg btn-default">
                <?php echo Yii::t("cms", "Make an offer")?>
            </a>
          </div>
          <!-- <div class=" " >
            <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                < ?php echo Yii::t("cms", "See more")?>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne3ligne1">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 titre ">
                <?php echo Yii::t("cms", "Local resources")?>
            </h2>
            <p class="para-bloc title-6 texte ">
                <?php echo Yii::t("cms", "Discover the vraks resources available in the commune")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, {'section' : 'need'},null,dyfOrga);" class="boutton btn btn-lg btn-default"><?php echo Yii::t("cms", "Make an offer")?></a>
          </div>
          <!-- <div class=" " >
            <a href="javascript:;" data-hash="#annonce"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                < ?php echo Yii::t("cms", "See more")?>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row centred-column">
  <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne1ligne2">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 titre ">
                <?php echo Yii::t("common", "Project")?>
            </h2>
            <p class="para-bloc title-6 texte ">
                <?php echo Yii::t("cms", "Do you have a project that is close to your heart? Share it")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('project',null,cocityCoordonate,null,dyfOrga);"  class="boutton btn btn-lg btn-default"><?php echo Yii::t("cms", "Propose a project")?></a>
          </div>
          <!-- <div class=" " >
            <a href="javascript:;" data-hash="#project"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                < ?php echo Yii::t("cms", "See more")?>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>


  <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne2ligne2">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 titre ">
                <?php echo Yii::t("common", "Event")?>
            </h2>
            <p class="para-bloc title-6 texte ">
                <?php echo Yii::t("cms", "You want to organize an event and are looking for a way to share it?")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('event',null,cocityCoordonate,null,dyfOrga);"  class="boutton btn btn-lg btn-default"><?php echo Yii::t("common", "Propose an event")?></a>
          </div>
          <!-- <div class=" " >
            <a href="javascript:;" data-hash="#agenda"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                < ?php echo Yii::t("cms", "See more")?>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12   text-center colonne " >
    <div class=" colonne3ligne2">
      <div class="" >
        <div class="bg2" >
          <div class="teste">
            <h2 class="h2-bloc title-2 titre ">
                <?php echo Yii::t("common", "Organization")?>
            </h2>
            <p class="para-bloc title-6 texte ">
                <?php echo Yii::t("cms", "Are you an organization and want to be listed? Share it.")?>
            </p>
          </div>
          <div class="">
            <a href="javascript:;" onclick="dyFObj.openForm('organization',null,cocityCoordonate,null,dyfOrga);" class="btn btn-lg btn-default boutton"><?php echo Yii::t("cms", "Propose an organization")?>
            </a>
          </div>
          <!-- <div class=" " >
            <a href="javascript:;" data-hash="#organization"  class="btn btn-lg btn-bloc btn-default lbh-menu-app">
                < ?php echo Yii::t("cms", "See more")?>
            </a>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script type="text/javascript">
  var str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#<?= $kunik ?>blocApp").append(str);
  if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var blocApp = {
			configTabs : {
        style : {
          inputsConfig:[
            {
              type : "section",
              options : {
                  name : "titre",
                  label : tradCms.title,
                  inputs : [
                    "color",
                    "fontSize"
                  ]
              }
            },
            {
              type : "section",
              options : {
                  name : "texte",
                  label : tradCms.text,
                  inputs : [
                    "color",
                    "fontSize"
                  ]
              }
            },
            {
              type : "section",
              options : {
                  name : "boutton",
                  label : tradCms.button,
                  inputs : [
                    "color",
                    "fontSize",
                    "backgroundColor"
                  ]
              }
            }
          ]
        },
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
      }
		}
		cmsConstructor.blocks.blocApp<?= $myCmsId ?> = blocApp;
  }

  // appendTextLangBased(".title< ?= $myCmsId ?>", < ?= isset($costum["costumLangActive"]) ?  json_encode($costum["costumLangActive"]) : json_encode($language) ?>, < ?= json_encode($blockCms["title"]) ?>,"< ?= $blockKey ?>");
</script>