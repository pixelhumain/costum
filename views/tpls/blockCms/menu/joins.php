<style>
    #joins-card-<?= $contextId ?>,
    #joins-card-<?= $contextId ?> h1,
    #joins-card-<?= $contextId ?> h2,
    #joins-card-<?= $contextId ?> h3,
    #joins-card-<?= $contextId ?> h4,
    #joins-card-<?= $contextId ?> h5,
    #joins-card-<?= $contextId ?> h6,
    #joins-card-<?= $contextId ?> button,
    #joins-card-<?= $contextId ?> input,
    #joins-card-<?= $contextId ?> select,
    #joins-card-<?= $contextId ?> textarea,
    #joins-card-<?= $contextId ?> a,
    #joins-card-<?= $contextId ?> p,
    #joins-card-<?= $contextId ?> span {
        font-family: Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue" !important;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
    }

    #joins-card-<?= $contextId ?> {
        display: none;
        font-size: 1.3rem;
        position: fixed;
        bottom: 20px;
        right: 20px;
        padding: 5px;
        border-radius: 10px;
        box-shadow: 0 0 20px rgba(0, 0, 0, .3);
        background-color: white;
        width: 400px;
        max-width: calc(100vw - 40px);
        z-index: 100000;
    }

    #joins-qr-container<?= $contextId ?> {
        position: fixed;
        bottom: 80px;
        right: 20px;
        z-index: 9999;
    }

    #joins-qr-button-<?= $contextId ?> {
        display: block;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        border: none;
        outline: none;
        box-shadow: 0 0 10px rgba(0, 0, 0, .5);
        transition: box-shadow .2s ease;
        background-color: white;
        overflow: hidden;
    }

    #joins-btncontainer<?= $contextId ?> {
        position: fixed;
        bottom: 140px;
        right: 20px;
        z-index: 9999;
    }

    #joins-button-<?= $contextId ?> {
        display: block;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        border: none;
        outline: none;
        box-shadow: 0 0 10px rgba(0, 0, 0, .5);
        transition: box-shadow .2s ease;
        background-color: white;
        overflow: hidden;
    }

    #joins-header<?= $contextId ?> {
        display: grid;
        grid-template-columns: 10fr 1fr;
        grid-column-gap: 2px;
        padding: 10px;
        border-radius: 7px;
        font-size: 1.5rem;
        align-items: center;
        margin-bottom: 5px;
        box-shadow: 0 0px 3px rgba(0, 0, 0, .3);
    }

    #joins-card<?= $contextId ?> {
        display: none;
        font-size: 1.3rem;
        position: fixed;
        bottom: 80px;
        right: 20px;
        padding: 5px;
        border-radius: 10px;
        box-shadow: 0 0 20px rgba(0, 0, 0, .3);
        background-color: white;
        width: 400px;
        z-index: 100000;
    }

    /* #joins-header<?= $contextId ?> {
        overflow: hidden;
        display: flex;
        flex-direction: row;
        align-items: flex-start;
    } */
    #joins-header<?= $contextId ?>.joins-actions > * {
        font-size: 1.8em;
        color: #000;
        cursor: pointer;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        text-align: center;
        transition: background-color .15s ease;
        line-height: 0;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }

    #joins-header<?= $contextId ?> .joins-title {
        font-weight: bold;
        font-size: 1em;
    }

    .invitation-link-qr-node-container<?= $contextId ?> {
        margin-top: 10px;
        border-radius: 4px;
        width: 200px;
        height: 200px;
        position: absolute;
        bottom: 0;
    }

    .invitation-link-qr-node<?= $contextId ?> {
        width: 100%;
        height: 100%;
    }

    .btn-download-link-qr-node-container<?= $contextId ?> {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .3);
        display: none;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: .3s;
    }

    .btn-download-link-qr-node-container<?= $contextId ?>:hover {
        opacity: 1;
    }

    .btn-download-link-qr-node-container<?= $contextId ?> button {
        border: none;
        width: 50px;
        height: 50px;
        background-color: black;
        color: white;
        text-align: center;
        line-height: 50px;
        font-size: 20px;
        border-radius: 100%;
    }

    @media screen and (max-width: 768px) {
        #joins-card555eba56c655675cdd65bf19{
            width: 250px;
        }
        
    }
</style>

<div id="joins-<?= $contextId ?>">
    <div id="joins-card<?= $contextId ?>">
        <div id="joins-header<?= $contextId ?>">
            <div class="joins-title">
                Rejoindre la communauté
            </div>
            <div class="joins-actions">
                <span class="joins-close">
                    <span class="fa fa-times"></span>
                </span>
            </div>
        </div>
        <div id="joins-body<?= $contextId ?>" class="co-scroll" style="position: relative;">
            <div class="btn-download-link-qr-node-container<?= $contextId ?>">
                <button id="btn-download-link-qr-node<?= $contextId ?>"><i class="fa fa-download" aria-hidden="true"></i></button>
            </div>
            <div class="invitation-link-qr-node<?= $contextId ?>">
            </div>
        </div>
    </div>
    <div id="joins-qr-container<?= $contextId ?>">
        <button id="joins-qr-button-<?= $contextId ?>">
            <i class="fa fa-qrcode fa-2x"></i>
        </button>
    </div>
    <div id="joins-btncontainer<?= $contextId ?>" class="<?= (!isset($linksBtn["isMember"]) || $linksBtn["isMember"] == true) ? "hidden" : "" ?>">
        <button id="joins-button-<?= $contextId ?>" title="Rejoindre la communauté">
            <i class="fa fa-user-plus fa-2x"></i>
        </button>
    </div>
</div>
<script>
    $(function() {
        const joins_button_dom = $("#joins-qr-container<?= $contextId ?>");
        const _animate_duration = 180;
        const joins_card_dom = $('#joins-card<?= $contextId ?>');
        joins_button_dom.on('click', function(__e) {
            __e.preventDefault();
            $(joins_button_dom).hide();
            joins_card_dom.show();
        });
        joins_card_dom.find('.joins-close').on('click', function(__e) {
            __e.preventDefault();
            joins_button_dom.fadeIn(_animate_duration);
            joins_card_dom.animate({
                width: 0,
                height: 0
            }, _animate_duration, function() {
                joins_card_dom.css({
                    display: '',
                    width: '',
                    height: ''
                });
            });
        });
        createQR();
    })

    function createQR() {
        new AwesomeQR.AwesomeQR({
            text: "<?= Yii::app()->getBaseUrl(true) . "/co2/link/connect/ref/" . $links["ref"] ?>",
            size: 200,
            // logoImage: assetPath + '/images/logos/logo-min.png',
            logoImage: (jsonHelper.pathExists("contextData.profilMediumImageUrl") && contextData.profilMediumImageUrl.replace(" ", "") !== "") ? contextData.profilMediumImageUrl : (notNull(costum) && typeof costum.logo != "undefined" && costum.logo != "" && costum.logo.replace(" ", "") !== "") ? costum.logo : assetPath +'/images/logos/logo-min.png',
            logoScale: 0.25,
            dotScale: 0.6
        }).draw().then(function(dataUrl) {
            var image = $(`<img src="${dataUrl}" style="display: block; margin: auto;"/>`)
            $('.invitation-link-qr-node<?= $contextId ?>').html(image)
            $(".btn-download-link-qr-node-container<?= $contextId ?>").css({
                display: "flex"
            })

            $("#btn-download-link-qr-node<?= $contextId ?>").click(function() {
                var link = document.createElement('a');
                link.href = dataUrl;
                link.download = `qrlink-${contextData.id}-${moment().format("DD-MM-YYYY")}.png`;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            })
        })
    }
    $("#joins-btncontainer<?= $contextId ?>").off("click").on('click', function(){
        links.connect('<?= $contextType ?>','<?= $contextId ?>','<?= Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','<?php echo $linksBtn["connectAs"] ?>')
    });
</script>