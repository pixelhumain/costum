<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
  $defaultUrlImg = Yii::app()->getModule("co2")->assetsUrl . '/images/thumbnail-default.jpg';
?>
<style id="<?= $kunik ?>sousfilieres">
    .<?= $kunik ?> .category-card {
        display: inline-block;
        width: 150px;
        height: auto;
        margin: 10px;
        padding: 15px;
        text-align: center;
        position: relative;
        /* border-radius: 10px; */
        box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
        transition: transform 0.3s ease, box-shadow 0.3s ease;
        cursor: pointer; 
        /*clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);*/
    }
    .<?= $kunik ?> .category-content{
        /* padding: 0px; */
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .<?= $kunik ?> .category-card:hover {
        transform: scale(1.05);
        box-shadow: 0px 6px 8px rgba(0, 0, 0, 0.2);
    }

     
    .<?= $kunik ?> .category-image {    
        width: 80%;
        position: absolute;
        left: 10%;
        right: 10%;
        z-index: 1;
        top: -23%;
        height: 150px;
        object-fit: cover;
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }

    .<?= $kunik ?> .category-title {
        padding: 10px;
        border-radius: 0px 0 10px 10px;
        /* background-color: rgba(0, 0, 0, 0.5); */
        color: white;
        font-size: 16px;
        text-transform: capitalize;
        position: absolute;
        /* bottom: 0; */
        /* top: 40%; */
        width: 100%;
        font-weight: bold;
    }

    .<?= $kunik ?> h2 {
        text-align: center;
    }

    .<?= $kunik ?> #thematic-container {
        margin-top: 2%;
        margin-left: 4%;
        text-align: center;
    }
    .<?= $kunik ?> .section-title {
        font-size: 22px;          
        font-weight: bold;       
        color: #2C3E50;          
        text-align: center;       
        margin-bottom: 10px;      
        text-transform: uppercase;
        margin-top: 1%; 
        text-transform: none;
    }
    @media (max-width: 900px) {
        .<?= $kunik ?> .category-card{
            /* margin-top: 10%; */
        }
    }
</style>
<div class="<?= $kunik ?> hidden">
    <h2 class="section-title"></h2>
    <div id="thematic-container">

    </div>
</div>
<script>5
    var dataCostum = <?= json_encode($elCostum) ?>;
    var tagArray = <?= json_encode($tagArray) ?>;
    var tagsToShow= <?= json_encode($blockCms["tagsToShow"]) ?>;
    costum.tagsToShow = tagsToShow;
    if(tagArray.length > 0) {
        $(".<?= $kunik ?>").removeClass("hidden");
        if(typeof dataCostum.thematic != "undefined") {
            $(".section-title").html('Quelques activités en rapport avec les éléments de la filière '+dataCostum.thematic+".");
        } else {
            $(".section-title").html('Quelques activités en rapport avec cet élément');
        }
    }
    
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#<?= $kunik ?>sousfilieres").append(str);
    if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var menuSousFiliere = {
			configTabs : {
                general : {
                    inputsConfig : [
                        {
                          type : "selectMultiple",
                          options : {
                            name : "tagsToShow",
                            label : tradDynForm.roles,
                            canDragAndDropChoise : true,
                            options: $.map(tagArray, function(value, index) {
                                        return { value: value, label: value };
                                    })
                          }
                        },
                    ]
                },
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
            onchange: function (path,valueToSet,name,payload,value,id){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                if (name === 'tagsToShow'){
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
                
                costumSocket.emitEvent(wsCO, "update_component", {
                    functionName: "refreshBlock",
                    data:{ id: id}
                })
            }
		}
		cmsConstructor.blocks.menuSousFiliere<?= $myCmsId ?> = menuSousFiliere;
	}

    var accessKey = '3x1tu9e0tgLVgh38robI6g8qggt29GLonv4x0U0UNpk';

    function fetchImageForWord(word, callback) {
        fetch(`https://api.unsplash.com/photos/random?query=${word}&client_id=${accessKey}`)
            .then(response => response.json())
            .then(data => {
                var imageUrl = data.urls.small; 
                callback(imageUrl);
            })
            .catch(error => {
                callback('<?= $defaultUrlImg ?>');
            });
    }

    function displaySportsTags(sportTags) {
        var container = document.getElementById('thematic-container');
        container.innerHTML = '';
        var nomberElement = sportTags.length ;  var colors = [
            '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', 
            '#e67e22', '#f1c40f', '#16a085', '#27ae60', '#2980b9',
            '#8e44ad', '#c0392b', '#d35400', '#f39c12', '#3083D6'
        ];
        var position = 0;
        sportTags.slice(0, nomberElement).forEach(tag => {
            position++;
            var card = document.createElement('div');
            card.style.background = colors[position % colors.length];
            card.classList.add('category-card');

            var card_content = document.createElement('div');
            card_content.classList.add('category-content');

            var idSec = tag.replace(/\s+/g, '')
                            .replaceAll("'", '')
                            .replaceAll(/['()]/g, '')
                            .replace(/[\/\\]/g, '');

            card.id = `dataks-${idSec}`;
            card.setAttribute('data-tag', tag);

            // var img = document.createElement('img');
            // img.classList.add('category-image');


            var title = document.createElement('div');
            title.classList.add('category-title');
            title.textContent = tag;

            // card_img.appendChild(img);
            // card.appendChild(card_img);
            card_content.appendChild(title);
            card.appendChild(card_content);
            // card.appendChild(title);
            container.appendChild(card);

            // fetchImageForWord(tag, function(imageUrl) {
            //     img.src = imageUrl;
            //     img.alt = tag;
            // });

            $("#dataks-"+idSec).on('click', function(){
                var tags = $(this).attr("data-tag");
                var encodedTags = encodeURIComponent(tags);
                location.hash = "#search?tags=" + encodedTags;
                urlCtrl.loadByHash(location.hash);
            });
        });
    }
    jQuery(document).ready(function() {
        displaySportsTags(tagsToShow);

        var maxHeight = Math.max.apply(null, $(".category-title").map(function() {
            return $(this).height();
        }).get());
        $(".category-content").css("height", maxHeight);
    });
</script>