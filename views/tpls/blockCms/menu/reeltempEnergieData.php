<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style>
    .cards-container {
        display: flex;
        flex-wrap: wrap;
        gap: 20px;
        justify-content: flex-start;
        max-width: 1200px;
        margin: 0 auto;
    }

    .dataset-card {
        border: 1px solid #ddd;
        border-radius: 8px;
        padding: 24px;
        flex: 1 1 calc(33.33% - 20px);
        background-color: #fff;
        transition: box-shadow 0.3s ease;
        margin-bottom: 20px;
        box-sizing: border-box; 
    }

    .dataset-card:hover {
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
    }

    .dataset-header h3 {
        font-size: 1.7rem; 
        color: #ff8800; 
        margin-bottom: 12px;
        cursor: pointer;
    }

    .dataset-header p {
        font-size: 1.5rem;
        color: #555;
        margin-bottom: 20px;
    }

    .dataset-details p {
        font-size: 1.3rem;
        margin: 6px 0;
    }

    .dataset-details strong {
        color: #333;
    }

    .dataset-tags {
        margin-top: 12px;
        margin-bottom: 20px;
    }

    .tag {
        display: inline-block;
        background-color: #0066cc;
        color: white;
        padding: 6px 12px;
        font-size: 1.1rem;
        border-radius: 4px;
        margin-right: 6px;
        transition: background-color 0.3s ease;
        cursor: pointer;
    }

    .tag:hover {
        background-color: #0055aa; 
    }

    .dataset-links {
        display: flex;
        justify-content: space-between;
    }

    .dataset-links .link {
        font-size: 1rem;
        color: #0066cc;
        text-decoration: none;
        padding: 6px 10px;
        border-radius: 4px;
        transition: background-color 0.3s ease, color 0.3s ease;
    }

    .dataset-links .link:hover {
        background-color: #0066cc;
        color: white;
    }

    .tags-filters {
        background-color: #ffa008 !important;
    }

    .data-containte-jdd {
        margin-left: 15%;
        margin-right: 15%;
        margin-top: 2%;
        margin-bottom: 5%;
    }

    .<?= $kunik ?> .section-title {
        font-size: 22px;          
        font-weight: bold;       
        color: #2C3E50;          
        text-align: center;       
        margin-bottom: 10px;      
        text-transform: uppercase;
        margin-top: 1%; 
        margin-left: 8%;
        margin-right: 8%;
        text-transform: none;
    }
    .<?= $kunik ?> .section-description {
        font-size: 16px;          
        color: #7F8C8D;           
        text-align: center;       
        margin-bottom: 20px;      
        line-height: 1.5;         
        font-style: italic;  
        margin-left: 5%;
        margin-right: 5%;    
    }
    .loader-loader<?= $kunik ?> {
        position: fixed;
        top: 20%;
        right: 0;
        bottom: 0;
        left: 1%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
    }
    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    /* .section-btn-filters {
        margin-top: 1%;
    } */
    .section-btn-filters .filter-observatory {
        font-size: 12px;
        border-radius: 25px;
        height: 30px;
        color: #000;
        background-color: #bbd7d1;
        border-color: #76c9b9;
        font-weight: 700;
    }

    .section-btn-filters .filter-observatory:hover, .section-btn-filters .actif {
        color: white;
        background-color: #128f76;
        border-color: #11866f;
    }



    #filter-container {
        margin-top: 10px;
        margin-left: 20px;
        margin-right: 20px;
        padding: 15px; 
        background-color: #f9f9f9;
        border: 1px solid #ddd; 
        border-radius: 8px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1); 
    }

    label {
        margin-right: 15px;
        font-weight: bold; 
        color: #333;
        cursor: pointer;
        transition: color 0.2s; 
    }

    label:hover {
        color: #007bff;
    }

    input[type="checkbox"] {
        margin-right: 5px;
        transform: scale(1.2); 
    }

    @media (max-width: 768px) {
        #filter-container {
            padding: 10px;
        }
        
        label {
            display: inline-block; 
            margin-bottom: 10px; 
        }
    }

    #cards-container {
        display: flex; 
        flex-wrap: wrap;
        justify-content: space-between; 
    }

    .card {
        flex: 1 1 calc(20% - 20px); 
        margin: 10px;
        border-radius: 8px;
        padding: 20px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2); 
        color: white; 
        transition: transform 0.2s, box-shadow 0.2s; 
        position: relative; 
    }

    .card:hover {
        transform: scale(1.05);
        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
    }

    .card h3 {
        margin-top: 0;
        font-size: 1.5em;
        color: #fff; 
        text-transform: none;
    }

    .card p {
        font-size: 1.1em;
        margin: 5px 0;
        color: #ddd;
    }

    .card p:first-of-type {
        font-weight: bold;
    }

    .card p:not(:first-of-type) {
        padding-left: 15px;
        border-left: 2px solid #007bff;
        margin-top: 5px;
    }

    @media (max-width: 768px) {
        .card {
            width: calc(50% - 20px);
        }
    }

    @media (max-width: 480px) {
        .card {
            width: 100%; 
        }
    }

    #myChart {
        max-height: 500px; 
        width: 100%; 
    }

    .nav {
        display: grid;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }
    .nav .nav-item {
        width: 90%;
        margin: 2%;
    }
    .nav-pills .nav-link {
        color: #2f3c4e !important;
        margin: auto 15px;
        border: 4px solid #e9ecef;
        border-radius: 38px;
        padding: 0px 10px;
    }

    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        background-color: #ff8c00;
        color: #ffffff !important;
        border: 4px solid #ff8c00;
        -webkit-box-shadow: 0 0 3px rgba(47, 60, 78, 0.15);
        box-shadow: 0 0 3px rgba(47, 60, 78, 0.15);
    }

    .nav-pills a .screenshot .title {
        font-size: 14px;
    }
    .map-commune {
        margin-top: 10px;
        height: 680px;
        width: 100%;
    }
    .<?= $kunik ?> .tooltip {
      position: absolute;
      background-color: white;
      color: black;
      padding: 5px;
      border-radius: 5px;
      display: none;
      pointer-events: none;
      opacity: 1 !important;
      font-weight: bold;
      padding: 15px;
    }
</style>
<div class="<?= $kunik ?>">

    <div class="row mt-4 pt-2 align-items-center">
        <div class="col-lg-2 col-md-4 col-12">
            <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                <li class="nav-item mb-4 pt-2 active">
                    <a class="nav-link rounded-pill" id="showList" data-toggle="pill" role="tab" href="#">
                        <div class="screenshot text-center pt-2 pb-2">
                            <h4 class="title font-weight-normal mb-0">Jeux de données</h4>
                        </div>
                    </a>
                </li>
                <!-- < ?php if(isset($el) && $el['address']['level3Name'] == "Réunion") {?> -->
                    <li class="nav-item mb-4 pt-2">
                        <a class="nav-link rounded-pill" id="observatory" data-toggle="pill" role="tab" href="#">
                            <div class="screenshot text-center pt-2 pb-2">
                                <h4 class="title font-weight-normal mb-0">Observatoire</h4>
                            </div>
                        </a>
                    </li>
                <!-- < ?php } ?>  -->
            </ul>
        </div>
        <div class="col-lg-10 col-mg-8 col-12">
            <h3 class="section-title">Découvrez les données en temps réel sur la production et la consommation d'énergie dans la région <?= isset($el['address']['level3Name']) ? $el['address']['level3Name'] : "La Réunion" ?>.</h3>
            <p class="section-description hidden-xs">Ces données, issues d'OpenData d'EDF, sont régulièrement mises à jour pour refléter la situation énergétique actuelle et aider les citoyens à s'engager dans des initiatives durables.</p>
        </div>
    </div>
     <div class="row data-containte-jdd">
        <div class="loader-loader<?= $kunik ?>" id="loader-loader<?= $kunik ?>" role="dialog">
            <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
        </div>
    </div>
    <div class="observatory-containte hidden">
        <div class="section-btn-filters text-center">
            <button class="btn btn-success filter-observatory" id="filters-reel" data-id="prod-electricite-temps-reel">
                Production d'électricité par filière en temps réel
            </button>
            <button class="btn btn-success filter-observatory" id="filters-annuel" data-id="production-annuelle-delectricite-par-filiere">
                Production annuelle d'électricité par filière
            </button>
            <button class="btn btn-success filter-observatory" id="filters-commune" data-id="consommation-annuelle-par-commune0">
                Consommation annuelle par commune
            </button>
        </div>
        <div class="section-data-container" id="section-observatory-data">
            <div id="filter-container" class="hidden"></div>
            <div class="map-commune commune-map hidden" id="map-commune">

            </div>
            <div id="tooltip" class="tooltip"></div>
            <canvas id="myChart"></canvas>
            <div id="cards-container">
                <div class="loader-loader<?= $kunik ?>" id="loader-<?= $kunik ?>" role="dialog">
                    <div class="content-loader<?= $kunik ?>" id="observatory-loader<?= $kunik ?>"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var allDatasetId = [];
    var elCostum = <?= json_encode($el) ?>;
    var cities = <?php echo json_encode($cities); ?>;
    var allVilles = [];
    $.each(cities, function(key, value){
        allVilles.push(value.name);
    });
    var  linksApi = "";
    if(typeof elCostum.address != "undefined" && typeof elCostum.address.level3Name != "undefined") {
        linksApi = elCostum.address.level3Name != "Réunion" ? `https://opendata.edf.fr/api/datasets/1.0/search/?rows=100&refine.territoire=${elCostum.address.level3Name}` : `https://opendata-reunion.edf.fr/api/datasets/1.0/search/?rows=100`;
    }
    
    function getallJeuxdeDonnees() {
        coInterface.showLoader('#content-loader<?= $kunik ?>');
        fetch(linksApi)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Erreur dans la récupération des jeux de données');
                }
                return response.json();
            })
            .then(data => {
                if (typeof data.datasets !== "undefined") {
                    var alljdd = data.datasets;
                    var htmlJDD = "";
                    alljdd.forEach((element, index) => {
                        allDatasetId.push(element.datasetid);
                        var metas = element.metas;
                        var keyWords = metas.keyword;
                        var htmlKey = "";
                        if (keyWords.length > 0) {
                            keyWords.forEach(tag => {
                                htmlKey += `<span class="tag tags-${tag}" onclick="filterTags(event)">#${tag}</span>`;
                            });
                        }
                        htmlJDD += `<div class="dataset-card">
                                        <div class="dataset-header">
                                            <h3 class="title-detail" onclick="getOnedetailleJeuxdeDonnees('${element.datasetid}','previews','${(metas.title).replace(/'/g, "\\'")}')" data-id="${element.datasetid}">${metas.title}</h3>
                                            <p>${metas.description}</p>
                                        </div>
                                        <div class="dataset-details">
                                            <p><strong>Producteur:</strong> ${metas.publisher}</p>
                                            <p><strong>Licence:</strong> ${metas.license}</p>
                                        </div>
                                        <div class="dataset-tags">
                                            ${htmlKey}
                                        </div>
                                    </div>`;
                    });

                    $(".data-containte-jdd").html(htmlJDD);
                    
                }
            })
            .catch(error => {
                mylog.warn('Une erreur est survenue:', error);
            });
    }

    var activeFilters = [];

    function filterTags(event) {
        var tagElement = event.target;
        var tagText = tagElement.textContent.slice(1);
        
        if (tagElement.classList.contains("tags-filters")) {
            $('.tags-'+tagText).removeClass("tags-filters");
            activeFilters = activeFilters.filter(tag => tag !== tagText);
        } else {
            $('.tags-'+tagText).addClass("tags-filters");
            activeFilters.push(tagText);
        }
        applyFilters();
    }

    function applyFilters() {
        var cards = document.querySelectorAll('.dataset-card');

        cards.forEach(card => {
            var tags = Array.from(card.querySelectorAll('.tag')).map(tag => tag.textContent.slice(1));

            if (activeFilters.length === 0 || tags.some(tag => activeFilters.includes(tag))) {
                card.style.display = 'block';
            } else {
                card.style.display = 'none';
            }
        });
    }

    costum.paramsOneJDD = "";
    var recordsTemp = [];
    function getOnedetailleJeuxdeDonnees(params, type , title = null) {
        coInterface.showLoader('#observatory-loader<?= $kunik ?>');
        var linksOneJdd = "";
        linksOneJdd = elCostum.address.level3Name != "Réunion" ? `https://opendata.edf.fr/api/records/1.0/search/?dataset=${params}&q=&refine.region=${elCostum.address.level3Name}&rows=100` : `https://opendata-reunion.edf.fr/api/records/1.0/search/?dataset=${params}&q=&rows=100`;
        fetch(linksOneJdd) 
            .then(response => {
                if (!response.ok) {
                throw new Error('Erreur dans la récupération des données');
                }
                return response.json();
            })
            .then(data => {
                costum.paramsOneJDD = data;
                
                if(type == "previews" && typeof costum.paramsOneJDD != "undefined" && costum.paramsOneJDD != "") {
                    urlCtrl.openPreview("view/url/costum.views.custom.cocity.previewenergie", {"title" : title, "region" : elCostum.address.level3Name});
                } else if(type == 'observatory') {
                    var records = elCostum.address.level3Name != "Réunion" ? (data.records).filter(record => record.fields.territoire_open_data === elCostum.address.level3Name) : data.records;
                    recordsTemp = records;

                    if(params == 'production-annuelle-delectricite-par-filiere') {
                        if (!$('.map-commune').hasClass('hidden')) {
                            $('.map-commune').addClass('hidden');
                        }
                        var years = [...new Set(records.map(record => record.fields.annee_prod))];
                        createYearFilter(years);
                        var lastThreeYears = getLastThreeYears(records);
                        displayData(records, lastThreeYears);
                        displayAnnualChart(records, lastThreeYears);
                    }

                    if(params == 'prod-electricite-temps-reel') {
                        if (!$('.map-commune').hasClass('hidden')) {
                            $('.map-commune').addClass('hidden');
                        }
                        var now = new Date();
                        var sortedRecords = records.sort((a, b) => new Date(b.fields.date) - new Date(a.fields.date));

                        var uniqueHours = [];
                        for (var record of sortedRecords) {
                            var recordHour = new Date(record.fields.date).getHours();
                            if (!uniqueHours.includes(recordHour)) {
                                uniqueHours.push(recordHour);
                            }
                            if (uniqueHours.length === 3) {
                                break;
                            }
                        }
                        createHourFilter(records);
                        displayRealTimeData(records, uniqueHours);
                        displayRealTimeChart (records, uniqueHours);
                    }

                    if(params == 'consommation-annuelle-par-commune0') {
                        setDataCommunes(records);
                        setChartDataCommunes(records);
                    }
                }
            })
            .catch(error => {
                mylog.warn('Une erreur est survenue:', error);
            });
    }

    // async function getEnergyDataForAllCities() {
    //     var allResults = {}; // Stocker les résultats par ville

    //     try {
    //         for (var cityName of allVilles) {
    //             var cityResults = {}; // Stocker les résultats pour chaque ville

    //             for (var dataset of allDatasetId) {
    //                 var fields = '';
    //                 if(dataset == "registre-national-des-installations-de-production-et-de-stockage-d-electricite") {
    //                     fields = "commune,datedebutversion,datemiseenservice,dateraccordement,energieannuelleglissanteinjectee,energieannuelleglissantesoutiree,filiere,gestionnaire,moderaccordement,nbinstallations,nominstallation,puismaxinstallee,regime,technologie,tensionraccordement"
    //                 }
    //                 var apiUrl = `https://opendata-reunion.edf.fr/api/records/1.0/search/?dataset=${dataset}&q=${cityName}&rows=100&fields=${fields}`;

    //                 var response = await fetch(apiUrl);
    //                 if (!response.ok) {
    //                     throw new Error(`Erreur lors de la récupération des données pour ${dataset}`);
    //                 }

    //                 var data = await response.json();
    //                 if (data.records && data.records.length > 0) {
    //                     mylog.log(`Données pour ${cityName} dans le dataset ${dataset}:`, data.records);
    //                     cityResults[dataset] = data.records; // Ajouter les données du dataset pour la ville
    //                 }
    //             }

    //             // Stocker les résultats de la ville dans l'objet global allResults
    //             if (Object.keys(cityResults).length > 0) {
    //                 allResults[cityName] = cityResults;
    //             } else {
    //                 mylog.log(`Aucune donnée trouvée pour ${cityName}`);
    //             }
    //         }

    //         mylog.log("Tous les résultats pour chaque ville:", allResults);
    //         return allResults; // Retourner tous les résultats

    //     } catch (error) {
    //         mylog.warn("Erreur:", error);
    //     }
    // }

    // Par heur
    function getUniqueHours(data) {
        var hours = data.map(record => new Date(record.fields.date).getHours());
        return [...new Set(hours)].sort((a, b) => a - b);
    }

    function displayRealTimeData(records, selectedHours) {
        var container = document.getElementById('cards-container');
        container.innerHTML = '';
            
        var filteredRecords = records.filter(record => {
            var recordHour = new Date(record.fields.date).getHours(); 
            return selectedHours.includes(recordHour);
        });

        var fields = {
            bioenergies: { label: "Bioénergie", unit: "MWh", image: "https://www.voseconomiesdenergie.fr/uploads/news/header/shutterstock-1612014604-1.jpg" },
            charbon: { label: "Charbon", unit: "MWh", image: "https://www.sciencesetavenir.fr/assets/img/2016/12/13/cover-r4x3w1200-585005c23fafd-Capture%20d%E2%80%99e%CC%81cran%202016-12-13%20a%CC%80%2015.24.04.png" },
            eolien: { label: "Éolien", unit: "MWh", image: "https://www.expertise-energie.fr/wp-content/uploads/sites/4/2022/03/energie-eolienne-renouvelable.jpg" },
            hydraulique: { label: "Hydraulique", unit: "MWh", image: "https://www.optima-energie.fr/wp-content/uploads/2022/07/format-banniere-blog-hydraulique.png" },
            diesel: { label: "Moteurs Diesel", unit: "MWh", image: "https://www.quebecscience.qc.ca/wp-content/uploads/2018/09/20110914152037-raffinerieptrole.jpg" },
            photovoltaique: { label: "Solaire Photovoltaïque", unit: "MWh", image: "https://conseils-thermiques.org/contenu/images/panneau-mono.png" },
            turbines_combustion: { label: "Turbines à Combustion Fioul", unit: "MWh", image: "https://www.edf.fr/sites/groupe/files/contrib/groupe-edf/espaces-dedies/espace-pedagogie/home/header/230336.jpg" }
        };

        Object.keys(fields).forEach(field => {
            var card = document.createElement('div');
            card.className = 'card';

            card.style.backgroundImage = `url('${fields[field].image}')`;
            card.style.backgroundSize = 'cover';
            card.style.backgroundPosition = 'center';
            card.style.position = 'relative';
            card.style.color = 'white';

            var overlay = document.createElement('div');
            overlay.style.position = 'absolute';
            overlay.style.top = '0';
            overlay.style.left = '0';
            overlay.style.right = '0';
            overlay.style.bottom = '0';
            overlay.style.backgroundColor = 'rgba(0, 0, 0, 0.6)';

            var cardContent = document.createElement('div');
            cardContent.style.position = 'relative';
            cardContent.style.zIndex = '2';
            cardContent.innerHTML = `<h3>${fields[field].label} (MWh)</h3>`;

            var fieldRecords = filteredRecords
                .filter(record => record.fields[field] !== undefined)
                .sort((a, b) => new Date(b.fields.date) - new Date(a.fields.date));

            fieldRecords.forEach(record => {
                var dateTime = new Date(record.fields.date);
                var hours = String(dateTime.getHours()).padStart(2, '0');
                var minutes = String(dateTime.getMinutes()).padStart(2, '0');
                // var seconds = String(dateTime.getSeconds()).padStart(2, '0');

                var timeDisplay = `${hours}:${minutes}`;
                var hourData = `${timeDisplay} -> ${record.fields[field].toLocaleString()}`;
                cardContent.innerHTML += `<p>${hourData}</p>`;
            });

            card.appendChild(overlay);
            card.appendChild(cardContent);
            container.appendChild(card);
        });
    }

    function createHourFilter(records) {
        var uniqueHours = getUniqueHours(records);

        var currentDate = new Date();
        var currentHour = currentDate.getHours();

        var lastThreeHours = [
            (currentHour - 2 + 24) % 24,
            (currentHour - 1 + 24) % 24, 
            currentHour 
        ];

        uniqueHours.sort((a, b) => b - a);
        $("#filter-container").removeClass('hidden');
        var filterContainer = document.getElementById('filter-container');
        filterContainer.innerHTML = '';

        uniqueHours.forEach(hour => {
            var checked = lastThreeHours.includes(hour) ? 'checked' : '';
            var label = document.createElement('label');
            label.innerHTML = `<input type="checkbox" name="hour" value="${hour}" onchange="filterByHour()" ${checked}> ${hour}:00`;
            filterContainer.appendChild(label);
        });
    }

function filterByHour() {
    var selectedHours = Array.from(document.querySelectorAll('input[name="hour"]:checked')).map(input => parseInt(input.value));
    displayRealTimeData(recordsTemp, selectedHours);
    displayRealTimeChart(recordsTemp, selectedHours);
}


// chart par heur
function displayRealTimeChart(records, selectedHours) {
    var filteredRecords = records.filter(record => {
        var recordHour = new Date(record.fields.date).getHours(); 
        return selectedHours.includes(recordHour);
    });

    var labels = [];
    var dataSets = {
        bioenergies: [],
        charbon: [],
        eolien: [],
        hydraulique: [],
        diesel: [],
        photovoltaique: [],
        turbines_combustion: []
    };

    filteredRecords.forEach(record => {
        var dateTime = new Date(record.fields.date);
        var timeLabel = `${String(dateTime.getHours()).padStart(2, '0')}:${String(dateTime.getMinutes()).padStart(2, '0')}:${String(dateTime.getSeconds()).padStart(2, '0')}`;
        
        if (!labels.includes(timeLabel)) {
            labels.push(timeLabel);
        }

        Object.keys(dataSets).forEach(field => {
            dataSets[field].push(record.fields[field] || 0);
        });
    });

    var combinedData = labels.map((label, index) => {
        return {
            label: label,
            data: Object.keys(dataSets).map(field => dataSets[field][index] || 0)
        };
    });

    combinedData.sort((a, b) => {
        var timeA = a.label.split(':').map(Number);
        var timeB = b.label.split(':').map(Number);
        return new Date(0, 0, 0, timeA[0], timeA[1], timeA[2]) - new Date(0, 0, 0, timeB[0], timeB[1], timeB[2]);
    });

    labels = combinedData.map(item => item.label);
    var sortedDataSets = Object.keys(dataSets).reduce((acc, field, index) => {
        acc[field] = combinedData.map(item => item.data[index]);
        return acc;
    }, {});

    var ctx = document.getElementById('myChart').getContext('2d');

    if (window.myChart instanceof Chart) {
        window.myChart.destroy(); 
    }

    window.myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: Object.keys(sortedDataSets).map((field, index) => ({
                label: field.charAt(0).toUpperCase() + field.slice(1),
                data: sortedDataSets[field],
                borderColor: getRandomColor(index),
                fill: false,
                tension: 0.1
            }))
        },
        options: {
            responsive: true,
            scales: {
                x: {
                    title: {
                        display: true,
                        text: 'Heure (HH:MM:SS)',
                    },
                    type: 'category',
                    labels: labels,
                },
                y: {
                    title: {
                        display: true,
                        text: 'Production (MWh)',
                    },
                },
            },
        },
    });
}

function getRandomColor(index) {
    var colors = [
        'rgba(75, 192, 192, 1)', 
        'rgba(255, 99, 132, 1)', 
        'rgba(54, 162, 235, 1)', 
        'rgba(255, 206, 86, 1)', 
        'rgba(153, 102, 255, 1)', 
        'rgba(255, 159, 64, 1)', 
        'rgba(255, 99, 71, 1)'
    ];
    return colors[index % colors.length];
}

    // Par année
    function getLastThreeYears(data) {
        var sortedYears = data.map(record => record.fields.annee_prod).sort().reverse();
        return sortedYears.slice(0, 3);
    }

    function displayData(records, selectedYears) {
        var container = document.getElementById('cards-container');
        container.innerHTML = '';

        var filteredRecords = records.filter(record => selectedYears.includes(record.fields.annee_prod));

        var fields = {
            bioenergie: { label: "Bioénergie", unit: "MWh", image: "https://www.voseconomiesdenergie.fr/uploads/news/header/shutterstock-1612014604-1.jpg" },
            charbon: { label: "Charbon", unit: "MWh", image: "https://www.sciencesetavenir.fr/assets/img/2016/12/13/cover-r4x3w1200-585005c23fafd-Capture%20d%E2%80%99e%CC%81cran%202016-12-13%20a%CC%80%2015.24.04.png" },
            eolien: { label: "Éolien", unit: "MWh", image: "https://www.expertise-energie.fr/wp-content/uploads/sites/4/2022/03/energie-eolienne-renouvelable.jpg" },
            hydraulique: { label: "Hydraulique", unit: "MWh", image: "https://www.optima-energie.fr/wp-content/uploads/2022/07/format-banniere-blog-hydraulique.png" },
            moteurs_diesel: { label: "Moteurs Diesel", unit: "MWh", image: "https://www.quebecscience.qc.ca/wp-content/uploads/2018/09/20110914152037-raffinerieptrole.jpg" },
            solaire_photovoltaique: { label: "Solaire Photovoltaïque", unit: "MWh", image: "https://conseils-thermiques.org/contenu/images/panneau-mono.png" },
            turbines_a_combustion_fioul: { label: "Turbines à Combustion Fioul", unit: "MWh", image: "https://www.edf.fr/sites/groupe/files/contrib/groupe-edf/espaces-dedies/espace-pedagogie/home/header/230336.jpg" }
        };

        Object.keys(fields).forEach(field => {
            var card = document.createElement('div');
            card.className = 'card';
            
            card.style.backgroundImage = `url('${fields[field].image}')`;
            card.style.backgroundSize = 'cover';
            card.style.backgroundPosition = 'center';
            card.style.position = 'relative';
            card.style.color = 'white'; 

            var overlay = document.createElement('div');
            overlay.style.position = 'absolute';
            overlay.style.top = '0';
            overlay.style.left = '0';
            overlay.style.right = '0';
            overlay.style.bottom = '0';
            overlay.style.backgroundColor = 'rgba(0, 0, 0, 0.6)'; 

            var cardContent = document.createElement('div');
            cardContent.style.position = 'relative';
            cardContent.style.zIndex = '2';
            cardContent.innerHTML = `<h3>${fields[field].label} (MWh)</h3>`;

            var fieldRecords = filteredRecords
                .filter(record => record.fields[field] !== undefined)
                .sort((a, b) => b.fields.annee_prod - a.fields.annee_prod)

            fieldRecords.forEach(record => {
                var yearData = `${record.fields.annee_prod}: ${record.fields[field].toLocaleString()}`; 
                cardContent.innerHTML += `<p>${yearData}</p>`;
            });

            card.appendChild(overlay); 
            card.appendChild(cardContent); 
            container.appendChild(card); 
        });
    }

    function createYearFilter(years) {
        var lastThreeYears = getLastThreeYears(recordsTemp); 
        $("#filter-container").removeClass('hidden');
        var filterContainer = document.getElementById('filter-container');
        filterContainer.innerHTML = ''; 

        years.sort((a, b) => b - a);

        years.forEach(year => {
            var label = document.createElement('label');
            var isChecked = lastThreeYears.includes(year); 
            label.innerHTML = `<input type="checkbox" name="year" value="${year}" ${isChecked ? 'checked' : ''} onchange="filterByYear()"> ${year}`;
            filterContainer.appendChild(label);
        });
    }

    function filterByYear() {
        var selectedYears = Array.from(document.querySelectorAll('input[name="year"]:checked')).map(input => input.value);
        displayData(recordsTemp, selectedYears);
        displayAnnualChart(recordsTemp, selectedYears);
    }

    // chart annuel
    function displayAnnualChart(records, selectedYears) {
        var filteredRecords = records.filter(record => selectedYears.includes(record.fields.annee_prod));

        var labels = [];
        var dataSets = {
            bioenergie: [],
            charbon: [],
            eolien: [],
            hydraulique: [],
            moteurs_diesel: [],
            solaire_photovoltaique: [],
            turbines_a_combustion_fioul: []
        };

        // Remplir les labels et les datasets
        filteredRecords.forEach(record => {
            var year = record.fields.annee_prod;

            if (!labels.includes(year)) {
                labels.push(year);
            }

            Object.keys(dataSets).forEach(field => {
                dataSets[field].push(record.fields[field] || 0);
            });
        });
        
        labels.sort((a, b) => a - b);

        var ctx = document.getElementById('myChart').getContext('2d');

        if (window.myChart instanceof Chart) {
            window.myChart.destroy();
        }

        window.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: Object.keys(dataSets).map((field, index) => ({
                    label: field.charAt(0).toUpperCase() + field.slice(1),
                    data: dataSets[field],
                    borderColor: getRandomColor(index),
                    fill: false,
                    tension: 0.1
                }))
            },
            options: {
                responsive: true,
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Année'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Production (MWh)'
                        }
                    }
                }
            }
        });
    }

    var communes = {};
    function setDataCommunes(records) {
        $("#filter-container").addClass('hidden');
        var cardsContainer = document.getElementById('cards-container');
        cardsContainer.innerHTML = '';
        communes = {};
        records.forEach(record => {
            var commune = record.fields.commune;
            var secteur = record.fields.secteur;
            var annee = record.fields.annee;
            var consommation = record.fields.consommation_mwh;

            if (!communes[commune]) {
                communes[commune] = {};
            }
            if (!communes[commune][secteur]) {
                communes[commune][secteur] = [];
            }
            communes[commune][secteur].push({ annee, consommation });
        });
        
        // Créer les cartes
        Object.keys(communes).forEach(commune => {
            var card = document.createElement('div');
            card.className = 'card';
            card.style.backgroundColor = '#258689';
            card.innerHTML = `<h3>${commune}</h3>`;

            Object.keys(communes[commune]).forEach(secteur => {
                var secteurDiv = document.createElement('div');
                secteurDiv.className = 'sector';
                secteurDiv.innerHTML = `<p style="color:#fff"><strong>Secteur: ${secteur}</strong></p>`;

                communes[commune][secteur].forEach(data => {
                    var yearDataDiv = document.createElement('p');
                    yearDataDiv.style.color = "#fff";
                    yearDataDiv.style.borderLeft = "2px solid #33e4d4";
                    yearDataDiv.innerHTML = `${data.annee}: ${data.consommation.toLocaleString()} MWh`;
                    secteurDiv.appendChild(yearDataDiv);
                });

                card.appendChild(secteurDiv);
            });

            cardsContainer.appendChild(card);
        });
        $('.map-commune').removeClass('hidden');
        <?= $kunik ?>mapCommune.clearMap();
        <?= $kunik ?>mapCommune.addElts(cities);
    }

    function setChartDataCommunes(records) {
        var communes = {};
        var secteursSet = new Set(); 

        records.forEach(record => {
            var commune = record.fields.commune;
            var secteur = record.fields.secteur;
            var consommation = record.fields.consommation_mwh;

            secteursSet.add(secteur);

            if (!communes[commune]) {
                communes[commune] = {};
            }
            if (!communes[commune][secteur]) {
                communes[commune][secteur] = 0;
            }
            communes[commune][secteur] += consommation;
        });

        var labels = Object.keys(communes);
        var secteursArray = Array.from(secteursSet);
        var secteurMap = {};

        labels.forEach(commune => {
            var secteurs = communes[commune];
            secteursArray.forEach(secteur => {
                if (!secteurMap[secteur]) {
                    secteurMap[secteur] = [];
                }
                
                secteurMap[secteur].push(secteurs[secteur] || 0);
            });
        });

        var datasets = Object.keys(secteurMap).map(secteur => {
            return {
                label: secteur,
                data: secteurMap[secteur],
                backgroundColor: getRandomColor(),
            };
        });

        var ctx = document.getElementById('myChart').getContext('2d');
        if (window.myChart instanceof Chart) {
            window.myChart.destroy();
        }
        
        window.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets,
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Consommation MWh par commune et secteur'
                    },
                    tooltip: {
                        callbacks: {
                            label: function(tooltipItem) {
                                var commune = tooltipItem.label; 
                                var secteur = tooltipItem.dataset.label;
                                var consommations = records.filter(record => 
                                    record.fields.commune === commune &&
                                    record.fields.secteur === secteur
                                );
                                var consommationDetails = consommations.map(record => {
                                    return `${record.fields.annee} : ${record.fields.consommation_mwh.toLocaleString()} MWh`;
                                });
                                var total = consommations.reduce((sum, record) => {
                                    return sum + record.fields.consommation_mwh;
                                }, 0);

                                if (consommations.length > 1) {
                                    consommationDetails.push(`Total : ${total.toLocaleString()} MWh`);
                                }
                                return consommationDetails;
                            }
                        }
                    }
                },
                scales: {
                    x: {
                        stacked: true
                    },
                    y: {
                        stacked: true,
                        title: {
                            display: true,
                            text: 'Consommation (MWh)'
                        }
                    }
                }
            }
        });
    };

    var <?= $kunik ?>mapCommune = new CoMap({
        container : "#map-commune",
        activePopUp : true,
        mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true,
            zoom : 2,
                scrollWheelZoom: false
        },
        mapCustom: {
            tile : "maptiler",
            getPopup: function(donnee) {
                let contenuPopup = '';
                var commune = (donnee.properties.name).toUpperCase().replaceAll('-', ' ').replace('SAINT', 'ST').replace('SAINTE', 'STE').replaceAll('É','E').replaceAll('Î','I');
                
                if (commune == 'ETANG SALE') commune = 'L ETANG SALE';
                if (commune == 'PLAINE DES PALMISTES') commune = 'LA PLAINE DES PALMISTES';
                if (commune == 'POSSESSION') commune = 'LA POSSESSION';
                if (commune == 'PORT') commune = 'LE PORT';
                if (commune == 'TAMPON') commune = "LE TAMPON";
                if (commune == 'AVIRONS') commune = "LES AVIRONS";
                if (commune == 'TROIS BASSINS') commune = 'LES TROIS BASSINS';
                
                contenuPopup = `<div class="card" style="background-color: #fff;">
                                    <h3 style="color: #000;">${donnee.properties.name}</h3>`;
                
                Object.keys(communes[commune]).forEach(secteur => {
                    contenuPopup += `<div class="sector">
                                        <p style="color: #000;"><strong>Secteur: ${secteur}</strong></p>`;
                    
                    communes[commune][secteur].forEach(data => {
                        contenuPopup += `<p style="color: #000; border-left: 2px solid #33e4d4;">
                                            ${data.annee}: ${data.consommation.toLocaleString()} MWh
                                        </p>`;
                    });
                    
                    contenuPopup += `</div>`;
                });
                
                contenuPopup += `</div>`;
                
                return contenuPopup;
            }
            // addElts: function(_context, data){
            //         let geoJson = [];
            //         let elements = document.querySelectorAll('#map-commune .leaflet-interactive');
            //         let tooltip = document.getElementById('tooltip');
            //         $.each(data, function(k, v){
            //             if(! _context.getOptions().data[k])
            //                  _context.getOptions().daxta[k] = v;
            //             if(typeof v.geoShape != "undefined"){
            //                 let data = $.extend({}, v);
            //                 let geoData = {
            //                         type: "Feature",
            //                         properties: {
            //                             name: v.name
            //                         },
            //                         geometry: v.geoShape
            //                 };
            //                 delete data.name;
            //                 delete data.getShape;
            //                 $.each(data, function(key,value) {
            //                     if(typeof geoData.properties[key] == 'undefined'){
            //                         geoData.properties[key] = value;
            //                     }
            //                 })
            //                 geoJson.push(geoData);
            //             }
            //         })
            
            //          _context.getOptions().geoJSONlayer = L.geoJSON(geoJson, {
            //             onEachFeature: function(feature, layer){
            //                 _context.addPopUp(layer, feature);
            //                 layer.setStyle({
            //                     color: typeof feature.properties.cocityId != 'undefined' ? "#1a72d0" : '#777777f0',
            //                     opacity: 0.5
            //                 });
            //                 layer.on("mouseover", function(event){
            //                     this.setStyle({
            //                         "fillOpacity" : 1,
            //                         "strokeOpacity" : 1
            //                     });
            //                     let name = this.feature.properties.name;
            //                     tooltip.innerHTML = name;
            //                     tooltip.style.display = 'block';
            //                     let x = event.containerPoint.x;
            //                     let y = event.containerPoint.y;
            //                     tooltip.style.left = `${x}px`;
            //                     tooltip.style.top = `${y}px`;
            //                 });

            //                 layer.on("mouseout", function(){
            //                     this.setStyle({
            //                         "fillOpacity" : 0.2,
            //                         "strokeOpacity" : 0.2,
            //                     });
            //                     tooltip.style.display = 'none';
            //                 });
            //             }
            //         });
            //         _context.getMap().addLayer( _context.getOptions().geoJSONlayer);
            //         _context.hideLoader();
            //         _context.fitBounds(data);
            //     }
        },
    });

    jQuery(document).ready(function() {
        getallJeuxdeDonnees();

        $("#observatory").on('click', function(){
            $(".data-containte-jdd").addClass('hidden');
            $(".observatory-containte").removeClass('hidden');
            $('.filter-observatory').removeClass('actif');
            if(elCostum.address.level3Name == "Réunion") {
                getOnedetailleJeuxdeDonnees('prod-electricite-temps-reel', 'observatory');
                $('#filters-reel').addClass('actif');
            } else {
                getOnedetailleJeuxdeDonnees('production-annuelle-delectricite-par-filiere', 'observatory');
                $('#filters-annuel').addClass('actif');
                $("#filters-reel").prop('disabled', true);
                $("#filters-commune").prop('disabled', true);
            }
        });

        $("#showList").on('click', function(){
            $(".data-containte-jdd").removeClass('hidden');
            $(".observatory-containte").addClass('hidden');
        });

        $(".filter-observatory").on('click', function(){
            var datasetId = $(this).data('id');
            $('.filter-observatory').removeClass('actif');
            $(this).addClass('actif');
            getOnedetailleJeuxdeDonnees(datasetId, 'observatory');
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>