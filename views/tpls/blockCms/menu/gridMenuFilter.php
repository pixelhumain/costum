<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style type="text/css" id="<?= $kunik ?>gridMenuFilter">
	#grid-menu-container<?= $kunik ?> .col-md-2{
		height: 14em;
		max-height: 14em;
	}	
	#grid-menu-container<?= $kunik ?> a{
		text-decoration: none;
	}
	.counter<?=$kunik?>{
      position: absolute;
      bottom: 10px;
      right: 30px;
      font-size: 40px;
    }
	.btn-tag{
		background: #010f43 !important;
	}
	<?php if(isset($blockCms["field"]) && $blockCms["field"] == "tags") { ?>
		.searchEntityContainer {
			margin-top: 5px;
			height: 245px;
		}
	<?php } ?>
</style>
<div class="container-fluid <?= $kunik ?>">
	<div id="grid-menu-container<?= $kunik ?>" class="row"></div>
</div>
<script type="text/javascript">
	var str = "";
	var elCostum = <?= json_encode($elementOrga) ?>;
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#<?= $kunik ?>gridMenuFilter").append(str);
	var paramsData<?= $kunik ?> = <?php echo json_encode( $blockCms ) ?>;
	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = paramsData<?= $kunik ?>;
		var gridMenuFilter = {
			configTabs : {
				general: {
					inputsConfig: [
						{
							type: "select",
							options: {
								name: "field",
								label: "Type",
								options: [
									{
										label: "Tags",
										value: "tags"
									},
									{
										label: "Objectif ODD",
										value: "objectiveOdd"
									}
								]
							}
						},
						{
                            type: "selectMultiple",
                            options: {
                                name: "elementsType",
                                label: tradCms.mapElement,
                                options: [
                                    {
										value : "organizations", 
										label: trad.organizations
									},
                                    {
										value : "citoyens" , 
										label: tradCategory.citizen
									},
                                    {
										value : "projects" , 
										label: trad.projects
									},
                                    {
										value : "poi" , 
										label: trad.poi
									}
                                ]
                            }
                        }
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
          		cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
      		}
		}
		cmsConstructor.blocks.gridMenuFilter<?= $myCmsId ?> = gridMenuFilter;
	};

	if(typeof costum !="undefined" && typeof costum.lists != "undefined" && typeof costum.lists.objectiveOdd !="undefined" && typeof costum.lists.objectiveOddImg != "undefined"){
		let contextId = costum.contextId;
		
		if(contextData && contextData.id){
			contextId = contextData.id;
		}

		if(costum && costum._id && costum._id.$id){
			contextId = costum._id.$id;
		}

		var uri = paramsData<?= $kunik ?>.field == "tags" ? "/field/tags" : "";
		var elements = paramsData<?= $kunik ?>.elementsType != "" ? paramsData<?= $kunik ?>.elementsType : "";
		var oddviews = {
			views : function(params) {
				str = '';
				str += `<div class='col-lg-3 col-md-4 col-sm-6 col-xs-6 searchEntityContainer simplePanelHtml ${params.containerClass}  contain_${params.collection}_${params.id}'>
						<div class="searchEntity" id="entity ${params.id} ">
							<a href=' ${params.hash} ' class='container-img-profil ${params.hashClass} '>${params.imageProfilHtml} </a>
							<div class='entityRight profil no-padding'>
								<div class='entityCenter no-padding'>
									<a href=' ${params.hash} ' class='pull-right ${params.hashClass}'><i class="fa fa-${params.icon} bg-${params.color}"></i></a>
								</div>
								<a  href=' ${params.hash} ' class='entityName bold text-dark ${params.hashClass} '>
									${params.name}
								</a>`;
				str += '<div class=\'entityDescription\'>' + ((params.shortDescription == null) ? '' : params.shortDescription) + '</div>';
				if (typeof params.tagsHtml != "undefined") {
					str += '<div class=\'tagsContainer\'>' + params.tagsHtml + '</div>';
				} else str += '<div class=\'tagsContainer text-red\' style=\'height:20px\'></div>';

				str += `</div> </div> </div> </div> </div>`;
				return str;
			}
		}
		if(typeof costum.countDataODD != "undefined") {
			fetchViewsODD(costum.countDataODD);
		} else {
			var paramsFilters = {
				"odd": Object.keys(costum.lists.objectiveOdd),
				"elements" : elements,
				"_id" : costum.contextId,
				"slug" : costum.slug
			};

			if(typeof elCostum.address != "undefined" && elCostum.address != "")
				paramsFilters.address = elCostum.address

			if(typeof elCostum.costum.typeCocity != "undefined" && typeof elCostum.costum.cocity != "undefined")
				paramsFilters.typezone = elCostum.costum.typeCocity;

			ajaxPost(
				null, 
				baseUrl+"/costum/cocity/getdataoddzone", 
				paramsFilters,
				function(results){
					costum.countDataODD = results;
					fetchViewsODD(results);
				}
			)
		}
		function fetchViewsODD(data) {
			$("#grid-menu-container<?= $kunik ?>").empty();
			const keysArray = Object.keys(costum.lists.objectiveOdd);
			for (var index in costum.lists.objectiveOdd) {
				const description = paramsData<?= $kunik ?>.field == "tags" ? costum.lists.objectiveOdd[index].description : "";
				const indexOfValue = keysArray.indexOf(index);
				const oddImg = (typeof costum.lists != "undefined" && costum.lists.objectiveOddImg && costum.lists.objectiveOddImg[index])?"#"+costum.lists.objectiveOddImg[index].replace(/\.[^/.]+$/, ""):"transparent";
				$("#grid-menu-container<?= $kunik ?>").append(`
					<div class="col-md-2 col-sm-4 col-xs-6 padding-5">
						<div style="background-color:${oddImg}; height:100%" class="padding-right-10 padding-left-10">
							<a class="btn-filters-select" href="javascript:;" data-description="${description}" data-type="filters" data-field="objectiveOdd" data-key="${index}" data-value="${index}" data-event="filters">
								<h5 style="display:flex; color:white"><b class="margin-right-5" style="font-size:24pt; font-weight:bolder">${indexOfValue+1} </b> <span> ${index}</span></h5>
								<div class="text-center">
									<img height="85em" src="${assetPath+"/images/costumize/odd/"+costum.lists.objectiveOddImg[index]}">
								</div>
								<h3 class="text-right counter<?=$kunik?> text-white">${data[indexOfValue]}</h3>
							</a>
						<div>	
					</div>
				`);
			}

			$("#grid-menu-container<?= $kunik ?>").append(`
				<div class="col-md-2 col-sm-4 col-xs-6 padding-5	">
					<div style="background:white; height:100%" class="padding-10">
						<div class="text-center" style="margin-top:1em !important">
							<img height="100%" width="100%" src="${assetPath+"/images/costumize/odd/ffffff.png"}">
						</div>
					<div>
				</div>
			`);
			var descripts = "";
			$(".btn-filters-select").unbind().on("click", function(){
				if(paramsData<?= $kunik ?>.field == "tags" && elements != ""){
					var paramsFilter = {};
					// descripts += descripts.includes(costum.lists.objectiveOdd[$(this).data("key")].description) ? "" : costum.lists.objectiveOdd[$(this).data("key")].description + " ";
					descripts = costum.lists.objectiveOdd[$(this).data("key")].description;
					var src = "";
					src += `<div id='filterContainer' class='searchObjCSS'></div>
							<div id='filterDescription' style="font-size:16px; text-align: justify;">${descripts}</div>
							<div class='headerSearchContainer no-padding col-xs-12'></div>
							<div class='bodySearchContainer margin-top-10'>
								<div class='no-padding col-xs-12' id='dropdown_search'></div>
								<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div>
							</div>`;
					smallMenu.open(src);

					var criterFilters = {
						'objectiveOdd': $(this).data("key"),
						'$or' : {
							"source.key" : "<?= $elementOrga["slug"] ?>",
							"links.memberOf.<?=(String)@$elementOrga["_id"]?>" : true
						}
					}; 
					if(elCostum.address != undefined) {
						var idAddress = {};
						if(costum.typeCocity == "region"){
							idAddress = {"address.level3" : elCostum.address["level3"]};
						} else if(costum.typeCocity == "ville") {
							idAddress = {"address.localityId": elCostum.address['localityId'] };
						} else if(costum.typeCocity == "departement" || costum.typeCocity == "district") {
							idAddress = {"address.level4": elCostum.address['level4'] };
						} else if(costum.typeCocity == "epci") {
							idAddress = {"address.localityId": {$in : costum.citiesArray} };
						}
						criterFilters["$or"] = {
							"source.key" : "<?= $elementOrga["slug"] ?>",
							...idAddress,
							"links.memberOf.<?=(String)@$elementOrga["_id"]?>" : true
						};
					}
					
					<?php if(@$elementOrga["address"]["postalCode"] != "") { ?>
						criterFilters["$or"].$and.push({"address.postalCode":"<?=@$elementOrga["address"]["postalCode"]?>"});
					<?php } ?>

					var filterSearch = {};
					paramsFilter = {
						container : "#filterContainer",
						interface : {
							events : {
								scroll : true,
								scrollOne : true
							}
						},
						defaults:{
							notSourceKey:true,
							types : elements,
							filters : criterFilters,
							// tags: [$(this).data("key")]
						},
						loadEvent:{
							default:"scroll"
						},
						results : {
							map : {
								show : false
							},
							renderView : 'oddviews.views'
						},
						filters : {
							text : true,
							scope : true,
							types : {
								lists : elements
							}
						}
					};
					history.replaceState(null, null, location.hash.split('?')[0])
					filterSearch = searchObj.init(paramsFilter);
					filterSearch.search.init(filterSearch);
					$(".btn-show-map").css("display", "none");
				} else {
					if(costum.slug == "meir"){
						urlCtrl.loadByHash("#search?category=startup&objectiveOdd="+$(this).data("key"));
					}else{
						urlCtrl.loadByHash("#search?tags="+$(this).data("key"));
					}
				}
			})
			coInterface.bindLBHLinks();
		}
	}
</script>