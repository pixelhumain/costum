<?php
$blockKey = $blockKey ?? sha1(date('Y-m-d'));
$params_data = [];

if (!empty($blockCms)) {
	$keys = array_keys($params_data);
	foreach ($keys as $key) {
		if (isset($blockCms[$key])) {
			$params_data[$key] = $blockCms[$key];
		}
	}
}
$connected_user = Yii::app()->session['userId'];

// load kanban library
$kanbanAssets = [
	'/plugins/kanban/kanban.js',
	'/plugins/kanban/kanban.css'
];
HtmlHelper::registerCssAndScriptsFiles(
	$kanbanAssets,
	Yii::app()->request->baseUrl
);

// load action_modal modules
$css_js = [
	'/plugins/jquery-confirm/jquery-confirm.min.css',
	'/plugins/jquery-confirm/jquery-confirm.min.js',
	'/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.css',
	'/plugins/jQuery-contextMenu/dist/jquery.contextMenu.min.js',
	'/plugins/jQuery-contextMenu/dist/jquery.ui.position.min.js'
];
HtmlHelper::registerCssAndScriptsFiles($css_js, Yii::app()->request->baseUrl);
$modal_js = [
	'/js/action_modal/action_modal.js'
];
HtmlHelper::registerCssAndScriptsFiles($modal_js, Yii::app()->getModule(Costum::MODULE)->getAssetsUrl());
HtmlHelper::registerCssAndScriptsFiles([
	"/js/cd_refresh_automation.js"
], Yii::app()->getModule("co2")->getAssetsUrl());

// vérifier s'il est développeur
$is_costum_admin = false;
$can_see = false;
$admin_only = false;
if (!empty($connected_user) && $this->costum !== null) {
	$costum = $this->costum;
	$db_costum = PHDB::findOneById($costum['contextType'], $costum['contextId'], ['links', 'costum']);
	$admin_only = $db_costum['costum']['cd_project']['admin_only'] ?? false;
	$links = ['members', 'contributors', 'attendees', 'organizer'];
	$user_scopes = [
		Organization::COLLECTION => ['members', 'organizer'],
		Project::COLLECTION      => ['contributors'],
		Event::COLLECTION        => ['attendees']
	];
	$where = array_map(function ($__link) use ($connected_user) {
		return ['$and' => [["links.$__link.$connected_user.isAdmin" => ['$exists' => true]], ["links.$__link.$connected_user.isAdmin" => true]]];
	}, $links);

	if (!empty($user_scopes[$costum['contextType']])) {
		foreach ($user_scopes[$costum['contextType']] as $link_type) {
			if (!empty($db_costum['links'][$link_type][$connected_user])) {
				$can_see = true;
				break;
			}
		}
	}
	foreach ($links as $link) {
		if (!empty($db_costum['links'][$link][$connected_user]['isAdmin']) && filter_var($db_costum['links'][$link][$connected_user]['isAdmin'], FILTER_VALIDATE_BOOLEAN)) {
			$is_costum_admin = true;
			break;
		}
	}
}

$is_developper = false;
if (!empty($connected_user)) {
	$db_my_roles = PHDB::findOne(Organization::COLLECTION, ['slug' => 'openAtlas'], ["links.members.$connected_user.roles"]);
	if (!empty($db_my_roles['links']) && !empty($db_my_roles['links']['members']) && !empty($db_my_roles['links']['members']) && !empty($db_my_roles['links']['members'][$connected_user]) && !empty($db_my_roles['links']['members'][$connected_user]['roles'])) {
		$roles = array_map(function ($__role) {
			return str_replace(' ', '', trim($__role));
		}, $db_my_roles['links']['members'][$connected_user]['roles']);
		if (in_array('Développeur', $roles)) $is_developper = true;
	}
}

if (!$admin_only && $can_see || $is_costum_admin || $is_developper) {
	$script_css = [
		'/plugins/jquery-confirm/jquery-confirm.min.css',
		'/plugins/jquery-confirm/jquery-confirm.min.js'
	];
	HtmlHelper::registerCssAndScriptsFiles($script_css, Yii::app()->request->baseUrl);
?>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&family=Oxygen:wght@300;400;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
	<style>

		#corner_dev<?= "$blockKey " ?>,
		#corner_dev<?= "$blockKey " ?>h1,
		#corner_dev<?= "$blockKey " ?>h2,
		#corner_dev<?= "$blockKey " ?>h3,
		#corner_dev<?= "$blockKey " ?>h4,
		#corner_dev<?= "$blockKey " ?>h5,
		#corner_dev<?= "$blockKey " ?>h6,
		#corner_dev<?= "$blockKey " ?>button,
		#corner_dev<?= "$blockKey " ?>input,
		#corner_dev<?= "$blockKey " ?>select,
		#corner_dev<?= "$blockKey " ?>textarea,
		#corner_dev<?= "$blockKey " ?>a,
		#corner_dev<?= "$blockKey " ?>p,
		#corner_dev<?= "$blockKey " ?>span {
			font-family: Inter, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue" !important;
			user-select: none;
			-webkit-user-select: none;
			-moz-user-select: none;
		}

		#corner_dev<?= "$blockKey " ?>.fa {
			font-family: FontAwesome !important;
		}

		#corner-button-container<?= $blockKey ?> {
			position: fixed;
			bottom: 20px;
			right: 20px;
			z-index: 9999;
		}

		#corner-button<?= $blockKey ?> {
			display: block;
			width: 50px;
			height: 50px;
			border-radius: 50%;
			border: none;
			outline: none;
			box-shadow: 0 0 10px rgba(0, 0, 0, .5);
			transition: box-shadow .2s ease;
			background-color: white;
			overflow: hidden;
		}

		.cd-int-pastille {
			position: absolute;
			right: -10px;
			top: -10px;
			background: red;
			color: white;
			border-radius: 10px;
			padding: 1px 2px;
			min-width: 20px;
			text-align: center;
		}

		#corner-button<?= $blockKey ?>:hover {
			box-shadow: 0 0 25px rgba(0, 0, 0, .5);
		}

		#corner-card<?= $blockKey ?> {
			display: none;
			font-size: 1.3rem;
			position: fixed;
			bottom: 20px;
			right: 20px;
			padding: 5px;
			border-radius: 10px;
			box-shadow: 0 0 20px rgba(0, 0, 0, .3);
			background-color: white;
			width: 400px;
			max-width: calc(100vw - 40px);
			z-index: 100000;
		}

		#corner-header<?= $blockKey ?> {
			display: grid;
			grid-template-columns: 1fr 10fr 1fr;
			grid-column-gap: 2px;
			padding: 2px;
			border-radius: 7px;
			align-items: center;
			margin-bottom: 5px;
			box-shadow: 0 0px 3px rgba(0, 0, 0, .3);
		}

		#corner-header<?= "$blockKey " ?>.corner-title {
			grid-column-start: 2;
			font-weight: bold;
			font-size: 1.2em;
		}


		#corner-header<?= "$blockKey " ?>.corner-actions>*,
		#corner-header<?= "$blockKey " ?>.turn-back, .project-change {
			font-size: 1.8em;
			color: #000;
			cursor: pointer;
			border-radius: 50%;
			width: 40px;
			height: 40px;
			text-align: center;
			transition: background-color .15s ease;
			line-height: 0;
			display: flex;
			flex-direction: column;
			justify-content: center;
		}

		#corner-header<?= "$blockKey " ?>.corner-actions>*:hover,
		#corner-header<?= "$blockKey " ?>.turn-back:hover, .project-change:hover {
			background-color: #f2f2f2;
		}

		#corner-header<?= "$blockKey " ?>.turn-back {
			display: none;
		}

		#corner-body<?= $blockKey ?> {
			overflow: hidden;
			display: flex;
			flex-direction: row;
			align-items: flex-start;
		}

		#corner-body<?= "$blockKey " ?>>div {
			min-width: 100%;
		}

		#context-container<?= $blockKey ?> {
			overflow-y: auto;
		}

		.corner-page-link,
		.corner-page-button {
			display: grid;
			font-size: 1.2em;
			padding: 10px;
			border-radius: 5px;
			margin-bottom: 5px;
			grid-template-columns: 1fr 60px;
			align-items: center;
			cursor: pointer;
			transition: background-color .15s ease;
		}

		.corner-page-link.disabled,
		.corner-page-button.disabled {
			color: rgba(44, 43, 43, .5);
			cursor: default;
		}

		.corner-page-link:last-child,
		.corner-page-button:last-child {
			margin-bottom: 0;
		}

		.corner-page-link:not(.disabled):hover,
		.corner-page-button:not(.disabled):hover {
			background: #f2f2f2;
		}

		.corner-element {
			display: flex;
			flex-direction: column;
			font-size: 1.2em;
			padding: 10px;
			border-radius: 5px;
			transition: background-color .15s ease;
			margin: 0 5px 10px 5px;
			box-shadow: 0 0 5px #828282;
		}

		.corner-element .dropdown-menu [role=presentation] {
			font-size: 14px;
		}

		.corner-element-footer {
			display: flex;
			flex-direction: row;
			margin: 0 -10px -10px -10px;
			overflow: hidden;
			border-top: 1px solid rgba(0, 0, 0, .1);
		}

		.corner-element-footer>* {
			flex: 1;
		}

		.corner-element:last-child {
			margin-bottom: 5px;
		}

		.corner-filter-container {
			margin-top: 5px;
			position: relative;
		}

		.corner-filter-input,
		.corner-filter-input:focus {
			border: 1px solid #cacaca;
			border-radius: 20px;
			line-height: 1.8rem;
			padding: 10px 10px 10px 35px;
			width: 100%;
			outline: none;
			margin-bottom: 10px;
		}

		.corner-filter-container .fa {
			position: absolute;
			top: calc(50% - 5px);
			left: 10px;
			transform: translateY(-50%);
			color: #cacaca;
			font-size: 2rem;
		}

		.action-participation,
		.action-image-uploader,
		.action-add-link {
			padding: 5px 10px;
			transition: background-color .2s ease;
			transition: color .2s ease;
			cursor: pointer;
		}

		.action-participation.green:hover {
			color: white;
			background-color: #e4514e;
		}

		.action-participation.red:hover {
			color: white;
			background-color: #9fbd38;
		}

		.action-image-uploader:hover,
		.action-add-link:hover {
			background-color: rgb(51, 51, 51);
			color: white;
		}

		#corner_dev<?= "$blockKey " ?>.dropdown {
			display: inline;
		}

		.screenshot-container {
			display: none;
			margin-top: 10px;
		}

		.screenshot-container>div {
			margin-bottom: 10px;
		}

		.screenshot-container>.image-container {
			border: 1px solid rgb(227, 227, 227);
			overflow: hidden;
		}

		.screenshot-container img {
			transition: transform .2s ease;
		}

		.screenshot-container img:hover {
			transform: scale(1.2);
		}

		#corner-screenshot-preview<?= $blockKey ?> {
			position: fixed;
			display: none;
			top: 0;
			left: 0;
			width: 100vw;
			height: 100vh;
			background: rgba(0, 0, 0, .5);
			z-index: 9999999;
		}

		.controlls<?= $blockKey ?> {
			position: inherit;
			top: 0;
			left: 0;
			background-color: transparent;
			display: flex;
			flex-direction: row;
			color: white;
			padding: 20px;
		}

		.controlls<?= "$blockKey " ?>[data-target=close] {
			padding: 10px 14px;
			border-radius: 50%;
			background: rgba(233, 233, 233, .5);
			cursor: pointer;
		}

		.image-container<?= $blockKey ?> {
			display: grid;
			grid-template-columns: repeat(3, 1fr);
			grid-template-rows: repeat(3, 1fr);
			align-items: center;
			width: 100vw;
			height: 100vh;
		}

		#screenshot-preview<?= $blockKey ?> {
			-webkit-user-drag: none;
			grid-column-start: 2;
			grid-row-start: 2;
		}

		.action-text {
			text-overflow: ellipsis;
			overflow: hidden;
			display: block;
		}

		.action-text:hover {
			text-decoration: underline;
			cursor: pointer;
		}

		.action-tracking {
			cursor: pointer;
			font-size: 2rem;
		}

		.action-tracking .fa {
			transition: color .2s ease;
		}

		.action-tracking:hover .fa-star-o,
		.action-tracking.active .fa {
			color: #ffc900;
		}

		.tags-container {
			margin-bottom: 5px;
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			gap: 5px;
		}

		.label {
			line-height: 1.4;
		}

		.label.label-disabled {
			background-color: grey;
		}

		@media screen and (max-width: 768px) {
			#corner-card<?= $blockKey ?> {}
		}

		.action-tag {
			cursor: default;
		}

		#action-list-types<?= $blockKey ?> {
			background-color: rgba(0, 0, 0, .1);
			border-radius: 5px;
		}

		.corner-page-button .fa-chevron-down {
			transition: transform .2s ease-in;
		}

		.corner-page-button[aria-expanded=true] .fa-chevron-down {
			transform: rotate(180deg);
		}

		.status-filter {
			display: flex;
			gap: 5px;
			flex-wrap: wrap;
			margin: 0 5px 10px 5px;
		}

		.corner-page-link.disabled>span:nth-child(2) {
			display: none !important;
		}

		.corner-page-link>.corner-page-actions,
		.corner-page-button>.corner-page-actions {
			display: flex;
			flex-direction: row;
			align-items: center;
			justify-content: end;
			gap: 5px;
		}

		.corner-actions {
			display: flex;
			flex-direction: row;
		}

		.corner-actions>*:hover,
		.corner-actions>*:active,
		.corner-actions>*:focus {
			text-decoration: none;
		}

		[data-target=costum_setting] {
			display: none;
		}

		#modalKanbanAction {
			z-index: 9999999;
			width: 100% !important;
			padding: 0 !important;
		}

		#modalKanbanAction .modal-body {
			max-height: calc(100vh - 191px);
			overflow-y: auto;
			min-height: calc(100vh - 191px)
		}

		#modalKanbanAction .modal-lg {
			width: 100% !important;
		}

		#modalKanbanAction .modal-dialog {
			margin: 0 !important;
			padding-top: 53px !important;
		}

		#toast-container {
			z-index: 999999999 !important;
		}

		#corner-action-preview {
			z-index: 999999999;
			position: relative;
		}

		.cd-popover {
			position: absolute;
			background: white;
			right: 60px;
			bottom: 0;
			max-width: 300px;
			border-radius: 8px;
			box-shadow: 0 0 10px #00000073;
		}

		.cd-popover::before {
			content: " ";
			display: block;
			position: absolute;
			bottom: 15px;
			right: -10px;
			width: 0;
			height: 0;
			border-top: 10px solid transparent;
			border-left: 15px solid #fff;
			border-bottom: 10px solid transparent;
		}

		.cd-popover .create-act {
			white-space: nowrap;
		}

		.cd-popover-content {
			padding: 10px;
			min-width: 200px;
		}

		.cd-popover-head {
			display: flex;
			padding: 5px;
			flex-direction: row;
			justify-content: space-between;
			background: #c5c5c5;
			border-radius: 8px 8px 0 0;
		}

		.cd-popover-head-right {
			display: flex;
			align-items: center;
			justify-content: center;
			gap: 2px;
		}

		.cd-popover-action {
			cursor: pointer;
			display: flex;
			width: 20px;
			height: 20px;
			justify-content: center;
			align-items: center;
			border-radius: 50%;
		}

		.cd-popover-action:hover {
			background-color: white;
		}

		.cd-popover-action.add-action,
		.cd-popover-action.add-action:hover {
			background-color: var(--ds-background-brand-bold, #0c66e4);
			color: white;
		}

		.cd-int-action-list {
			background-color: var(--ds-surface-raised, #fff);
			box-shadow: var(--ds-shadow-raised, 0 1px 1px #091e4240, 0 0 1px #091e424f);
			color: var(--ds-text, #172b4d);
			cursor: pointer;
			display: block;
			border-radius: 8px;
			max-width: 300px;
			min-height: 32px;
			position: relative;
			scroll-margin: 8px;
			padding: 5px;
			z-index: 10;
			user-select: none;
			--webkit-user-select: none;
			--moz-user-select: none;
			--o-user-select: none;
			box-sizing: border-box;
			margin-bottom: 8px;
			transition: transform 2s;
		}

		.cd-int-action-list-container.dragging .cd-int-action-list {
			cursor: move;
		}

		.cd-int-footer {
			display: flex;
			flex-direction: row-reverse;
			margin: 5px 0;
		}

		.cd-int-footer-action {
			display: block;
			width: auto;
			height: auto;
			border: none;
			margin-left: 2px;
			background: transparent;
			border-radius: 6px;
			padding: 5px;
			line-height: 20px;
			font-size: 16px;
			transition: .3s ease background;
			cursor: pointer;
			background-clip: padding-box;
			background-color: var(--ds-surface-raised-hovered, #f1f2f4);
			opacity: .7;
			white-space: nowrap;
		}

		.cd-int-contributors,
		.cd-int-tasks,
		.cd-int-statuses {
			display: none;
		}

		.cd-int-contributor-list,
		.cd-int-task-list-resume,
		.cd-int-statuses-list {
			padding: 0;
			margin: 0;
			width: 100%;
			list-style: none;
		}

		.cd-int-contributor-info,
		.cd-int-task-item {
			border-radius: 15px;
			margin-bottom: 2px;
			display: flex;
			flex-direction: row;
			align-items: center;
			gap: 5px;
			padding: 0;
			position: relative;
		}

		.cd-int-statuses-item {
			display: block;
		}

		.cd-int-statuses-item:first-child {
			border-radius: 8px 8px 0 0;
		}

		.cd-int-statuses-item:last-child {
			border-radius: 0 0 8px 8px;
		}

		.cd-int-contributor-info:hover,
		.cd-int-task-item:hover,
		.cd-int-statuses-item:hover {
			opacity: 1;
			background-color: var(--ds-surface-raised-pressed, #dcdfe4);
		}

		.cd-int-task-item:hover .cd-int-task-actions {
			display: flex;
		}

		.cd-int-contributor-info:last-child,
		.cd-int-task-item:last-child {
			margin-bottom: 8px;
		}

		.cd-int-contributor-image {
			width: 30px;
			height: 30px;
			overflow: hidden;
			border-radius: 50%;
			align-self: flex-start;
		}

		.cd-int-task-name,
		.cd-int-statuses-item {
			padding: 5px 10px;
		}

		.cd-int-task-name.checked {
			text-decoration: line-through;
		}

		.cd-int-contributor-name,
		.cd-int-task-name,
		.cd-int-statuses-item {
			margin: 0;
			flex: 1;
			font-size: 14px;
		}

		.cd-int-task-actions {
			display: none;
			position: absolute;
			gap: 2px;
			right: 3px;
			top: 50%;
			transform: translateY(-50%);
		}

		.cd-int-task-action {
			display: flex;
			border: none;
			overflow: hidden;
			border-radius: 50%;
			width: 25px;
			height: 25px;
			outline: none;
			text-align: center;
			font-size: 11px;
			color: white;
			padding-left: 5px;
			justify-content: center;
			align-items: center;
		}

		.cd-int-task-action[data-target=delete] {
			background-color: #CC0000;
		}

		.cd-int-task-action[data-target=contribution] {
			background-color: #172B4D;
		}

		.cd-int-create-action {
			display: flex;
			justify-content: space-between;
			align-items: stretch;
		}

		.cd-int-new-action,
		.cd-int-close-prompt {
			cursor: pointer;
			background-color: var(--ds-background-brand-bold, #0c66e4);
			border: none;
			box-shadow: none;
			color: var(--ds-text-inverse, #fff);
			align-items: center;
			border-radius: 3px;
			box-sizing: border-box;
			font-family: var(--font-family);
			font-size: 14px;
			font-weight: 400;
			justify-content: center;
			line-height: 20px;
			padding: 6px 12px;
			text-decoration: none;
			transition-duration: 85ms;
			transition-property: background-color, border-color, box-shadow;
			transition-timing-function: ease;
			white-space: nowrap;
		}

		.cd-int-close-prompt {
			background: #b1b1b1;
		}

		.cd-int-action-name {
			overflow: hidden;
			overflow-wrap: break-word;
			resize: none;
			height: 54px;
			font-size: 15px;
			line-height: 20px;
			font-weight: 400;
			box-sizing: border-box;
			width: 100%;
			border: 1px #ebebeb solid;
			outline: none;
		}

		.cd-int-content-loader {
			width: 100%;
			text-align: center;
		}

		.cd-int-status-filter * {
			font-size: 14px;
		}

		.cd-int-status-filter button {
			padding: 0;
			display: flex;
			justify-content: center;
			align-items: center;
			width: 20px;
			height: 20px;
		}

		.cd-int-status-item,
		.cd-int-popover-title {
			display: inline-block;
		}

		.cd-int-status-item::first-letter,
		.cd-int-popover-title::first-letter,
		.cd-int-statuses-item::first-letter {
			text-transform: uppercase;
		}

		.cd-int-title {
			display: flex;
			flex-direction: row;
			justify-content: space-between;
			align-items: flex-start;
			gap: 5px;
		}

		.cd-int-title .cd-int-drag-drop {
			cursor: grab !important;
		}

		.cd-int-title .text {
			flex: 1;
		}

		.cd-int-title .fa {
			display: block;
			padding: 3px;
			border-radius: 5px;
		}

		.cd-int-title .fa:hover {
			background-color: #d1d1d1;
			cursor: pointer;
		}

		.cd-int-title-tag {
			overflow: hidden;
			text-overflow: ellipsis;
		}
		[data-kanbantheme="dark"] {
			--switch-background: #232424;
			--switch-color: #fff;
			--switch-second-color: #5c5d5d;
			--switch-hover-color: #3b3d3d;
		}
		
		[data-kanbantheme="light"] {
			--switch-background: #fff;
			--switch-color: #172b4d;
			--switch-second-color: #ebecf0;
			--switch-hover-color: whitesmoke;
		}
		.close-modal-cornerDev:hover {
			opacity: 1;
		}

		#modalKanbanAction .modal-content {
			background-color: var(--switch-background);
			
		}
		#modalKanbanAction .modal-header {
			border-bottom: 1px solid var(--switch-second-color);
		}

		#modalKanbanAction .modal-header .modal-title {
			color: var(--switch-color);
		}
		#modalKanbanAction .modal-header .close {
			color: var(--switch-color);
		}

		#modalKanbanAction .modal-footer {
			border-top: 1px solid var(--switch-second-color);
		}
		#modalKanbanAction .modal-footer .btn-default {
			color: var(--switch-color);
			border-color: var(--switch-background);
			background-color: var(--switch-second-color);
		}
		#kanbanActionContainer {
			height: 77vh !important;
		}
		#kanbanActionContainer .kanban-list-content { 
			background-color: var(--switch-second-color);
		}
		#kanbanActionContainer .kanban-list-card-detail {
			color: var(--switch-color);
			background-color: var(--switch-background);
		}
		#kanbanActionContainer .kanban-list-card-detail:hover {
			background-color: var(--switch-hover-color);
		}
		.kanban-overlay.active {
			z-index: 99999999999999999999;
		}
		#kanbanActionContainer .kanban-list-header {
			color: var(--switch-color);
		}
		#kanbanActionContainer .kanban-list-card-edit, #kanbanActionContainer .kanban-list-card-switch, #kanbanActionContainer .card-duplicate, #kanbanActionContainer .contributors-preview, #kanbanActionContainer .card-action, #kanbanActionContainer .kanban-themed-button {
			background-color: var(--switch-second-color);
		}
		#kanbanActionContainer .kanban-action-dropdown {
			display: none;
		}

		#modalKanbanAction .switch-dark-light {
			position: absolute;
			right: 0;
			top: -2px;
			z-index: 9999;
		}

		#modalKanbanAction .changetheme {
			opacity: 0;
			position: absolute;
		}

		#modalKanbanAction .changetheme-label {
			background-color: var(--switch-color);
			width: 49px;
			height: 26px;
			border-radius: 50px;
			position: relative;
			cursor: pointer;
			display: flex;
			justify-content: space-between;
			align-items: center;
		}

		#modalKanbanAction .changetheme-label i {
			font-size: 1.3em;
		}

		#modalKanbanAction .fa-moon-o {
			color: #f1c40f;
			padding-left: 5px;
		}

		#modalKanbanAction .fa-sun-o {
			color: #f39c12;
			padding-right: 5px;
		}

		#modalKanbanAction .changetheme-label .ball {
			background-color: var(--switch-second-color);
			width: 24px;
			height: 24px;
			position: absolute;
			border-radius: 50%;
			transition: transform 0.2s linear;
		}

		#modalKanbanAction .changetheme:checked + .changetheme-label .ball {
			transform: translateX(24px);
		}

		#modalKanbanAction .switch-dark-light {
			position: absolute;
			right: 70px;
			top: 22px;
		}
		#modalKanbanAction .modal-dialog {
			padding-top: 0 !important;
			height: 100vh;
		}
		#modalKanbanAction .modal-content {
			height: 100%;
		}
	</style>
	<div id="corner-screenshot-preview<?= $blockKey ?>">
		<div class="controlls<?= $blockKey ?>">
			<span data-target="close"><span class="fa fa-times"></span></span>
		</div>
		<div class="image-container<?= $blockKey ?>">
			<img id="screenshot-preview<?= $blockKey ?>" src="" />
		</div>
	</div>
	<div id="corner-action-preview"></div>
	<div id="corner_dev<?= $blockKey ?>">
		<div id="corner-card<?= $blockKey ?>" class="cornerdev-card">
			<div id="corner-header<?= $blockKey ?>">
				<span class="project-change">
					<i class="fa fa-exchange"></i>
				</span>
				<span class="turn-back">
					<span class="fa fa-arrow-left"></span>
				</span>
				<div class="corner-title">
					Corner dev
				</div>
				<div class="corner-actions">
					<a href="" target="_blank" class="disabled" data-target="project-detail">
						<span class="fa fa-info"></span>
					</a>
					<?php
					if ($is_costum_admin || $is_developper) { ?>
						<span data-target="setting">
							<span class="fa fa-cog"></span>
						</span>
					<?php
					} ?>
					<span class="corner-close">
						<span class="fa fa-times"></span>
					</span>
				</div>
			</div>
			<div id="corner-body<?= $blockKey ?>" class="co-scroll">
				<div id="link-container<?= $blockKey ?>">
					<?php
					if ($is_costum_admin || $is_developper) { ?>
						<span class="corner-page-link" data-target="costum_setting">
							<span>Reglage pour le costum</span>
							<span class="corner-page-actions">
								<span class="fa fa-chevron-right right"></span>
							</span>
						</span>
					<?php
					}
					if ($is_costum_admin || $is_developper) { ?>
						<span class="corner-page-link" data-target="project_list">
							<span>Projet : <span id="active-project<?= $blockKey ?>">(Aucun projet sélectionné)</span></span>
							<span class="corner-page-actions">
								<span class="badge" id="project-list-badge<?= $blockKey ?>">0</span>
								<span class="fa fa-chevron-right right"></span>
							</span>
						</span>
					<?php
					} ?>
					<span class="corner-page-button" style="grid-template-columns: 1fr 150px;" data-toggle="collapse" data-target="#action-list-types<?= $blockKey ?>">
						<span>Liste des actions</span>
						<span class="corner-page-actions">
							<span class="label label-danger all-action-badge<?= $blockKey ?>">0</span>
							<span class="label label-warning mine-action-badge<?= $blockKey ?>">0</span>
							<span class="label label-default context-linked-action-badge<?= $blockKey ?>">0</span>
							<span class="fa fa-chevron-down right"></span>
						</span>
					</span>
					<div id="action-list-types<?= $blockKey ?>" class="collapse out">
						<span class="corner-page-link" data-target="action_list" data-scope="all">
							<span data-toggle="modal" data-kanban="all" data-title="Tous les actions"><i class="fa fa-trello fa-lg">&ensp;</i> Toutes les actions</span>
							<span class="corner-page-actions">
								<span class="label label-danger all-action-badge<?= $blockKey ?>">0</span>
								<span class="fa fa-chevron-right right"></span>
							</span>
						</span>
						<span class="corner-page-link" data-target="action_list" data-scope="mine">
							<span data-toggle="modal" data-kanban="mine" data-title="Mes actions"><i class="fa fa-trello fa-lg">&ensp;</i> Mes actions</span>
							<span class="corner-page-actions">
								<span class="label label-warning mine-action-badge<?= $blockKey ?>">0</span>
								<span class="fa fa-chevron-right right"></span>
							</span>
						</span>
						<span class="corner-page-link" data-target="action_list" data-scope="">
							<span data-toggle="modal" data-kanban="actions" data-title="Les actions liées à cet url"><i class="fa fa-trello fa-lg">&ensp;</i> Liées à cet url</span>
							<span class="corner-page-actions">
								<span class="label label-default context-linked-action-badge<?= $blockKey ?>">0</span>
								<span class="fa fa-chevron-right right"></span>
							</span>
						</span>
					</div>
					<span class="corner-page-button" id="new-action<?= $blockKey ?>">Ajouter une action/Déclarer un bug</span>
				</div>
				<div id="context-container<?= $blockKey ?>">
				</div>
			</div>
		</div>
		<div id="corner-button-container<?= $blockKey ?>" class="cornerdev-button">
			<div class="cd-popover">
				<div class="cd-popover-head">
					<div class="cd-popover-head-left">
						<!-- Single button -->
						<div class="cd-int-status-filter btn-group dropup" data-filter="tracking">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#" class="cd-int-status-item" data-target="discuter">à discuter</a></li>
								<li><a href="#" class="cd-int-status-item" data-target="todo">à faire</a></li>
								<li class="active"><a href="#" class="cd-int-status-item" data-target="tracking">en cours</a></li>
								<li><a href="#" class="cd-int-status-item" data-target="totest">à tester</a></li>
								<li><a href="#" class="cd-int-status-item" data-target="done">terminé</a></li>
							</ul>
						</div>
						<span class="cd-int-popover-title"></span>
					</div>
					<div class="cd-popover-head-right">
						<span class="cd-popover-action add-action">
							<i class="fa fa-plus"></i>
						</span>
						<span class="cd-popover-action hide-msg">
							<i class="fa fa-times"></i>
						</span>
					</div>
				</div>
				<div class="cd-popover-content"></div>
			</div>
			<span class="cd-int-pastille">0</span>
			<button id="corner-button<?= $blockKey ?>">
				<img style="width: 100%; height: 100%" src="<?= Yii::app()->getModule('costum')->getAssetsUrl() ?>/images/corner_dev/oceco.png" alt="oceco.png">
			</button>
		</div>
	</div>
	<!-- Modal du kanban -->
	<div class="modal fade" id="modalKanbanAction" role="dialog" data-kanbantheme="light">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close close-modal-cornerDev" data-dismiss="modal"><i class="fa fa-times-circle-o fa-2x"> </i></button>
					<h4 class="modal-title">Les actions</h4>
					<div class="switch-dark-light">
						<input type="checkbox" class="changetheme" id="inputChangethemeK" checked>
						<label for="inputChangethemeK" class="changetheme-label">
							<i class="fa fa-moon-o" aria-hidden="true"></i>
							<i class="fa fa-sun-o" aria-hidden="true"></i>
							<span class="ball"></span>
						</label>
					</div>
				</div>
				<div class="modal-body">
					<div id="kanbanActionContainer">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		(function($, W) {
			// Début section des variables globales
			// var serverUrl = 'https://communecter.rinelfi.me';
			var serverUrl = 'https://www.communecter.org';
			// var serverUrl = baseUrl;
			var path2ValueOption = {};
			path2ValueOption.url = serverUrl;
			trad['everyone'] = 'Tout le monde';
			$(".changetheme").change(function() {
				var valueCheckbox = ($(this).is(":checked")) ? true : false;
				$("#modalKanbanAction").attr("data-kanbantheme", `${(valueCheckbox) ? "light" : "dark"}`)
			})
			var socket_handlers = {
				".action-order": function(data) {
					var after_dom;
					var moved_dom = $('.corner-element.action[data-id=' + data.moved + ']');
					if (!moved_dom.length)
						return (true);
					moved_dom.attr('data-status', data.group);
					update_action_status(data.moved, data.group);
					if (data.after !== null) {
						after_dom = $(".corner-element.action[data-id=" + data.after + "]");
						after_dom.before(moved_dom);
					} else
						$('#context-container<?= $blockKey ?>').append(moved_dom);
				}
			};

			const _storage_version = 'v4';
			const _animate_duration = 180;
			let _last_input_value = '';
			let _current_location = W.location.href;
			const _is_costum = typeof costum === 'object' && costum !== null;
			const _is_costum_admin = JSON.parse(JSON.stringify(<?= json_encode($is_costum_admin) ?>));
			const _is_developper = JSON.parse(JSON.stringify(<?= json_encode($is_developper) ?>));

			function req_milestone_data(id) {
				return new Promise(function(resolve, reject) {
					var url = serverUrl + "/costum/project/action/request/milestone_data";
					var post = {
						project: id
					};

					ajaxPost(null, url, post, function(resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 401,
								msg: resp.content
							});
					}, function(arg0, arg1, arg2) {
						reject({
							code: 402,
							msg: [arg0, arg1, arg2]
						});
					});
				});
			}

			function xhr_get_create_data(id) {
				return new Promise(function(resolve, reject) {
					var url = serverUrl + "/costum/project/action/request/action_create_data";
					var post = {
						project: id
					};

					ajaxPost(null, url, post, function(resp) {
						if (resp.success)
							resolve(resp.content);
						else
							reject({
								code: 401,
								msg: resp.content
							});
					}, function(arg0, arg1, arg2) {
						reject({
							code: 402,
							msg: [arg0, arg1, arg2]
						});
					});
				});
			}

			const _open_context_container = {
				costum_setting: function() {
					const corner_header = $('#corner-header<?= $blockKey ?> .corner-title');
					const context_container_dom = $('#context-container<?= $blockKey ?>');

					const storage = cd_get_storage();

					corner_header.fadeIn(_animate_duration).text('Réglage du costum');
					context_container_dom.empty().html(`
                    <div class="form-group">
                        <label>Le bouton est visible par</label>
                        ${['everyone', 'administrator'].map(function (__user) {
                            return `
                            <div class="radio">
                                <label>
                                    <input type="radio" name="cd_visibility" value="${trad[__user]}" ${storage.cs_visibility === trad[__user] ? 'checked' : ''}>
                                    ${trad[__user]}
                                </label>
                            </div>
                            `;
                        }).join('')}
                    </div>
                    `);
					adjust_view();
				},
				project_list: function() {
					const corner_header_dom = $('#corner-header<?= $blockKey ?> .corner-title');
					const context_container_dom = $('#context-container<?= $blockKey ?>');

					corner_header_dom.fadeIn(_animate_duration).text('Liste des projets');

					function load_view() {
						var filter_value = $('.corner-filter-input').length > 0 ? $('.corner-filter-input').val() : '';
						context_container_dom.css('align-self', 'center').empty().html('<div class="view_loader" style="width: 100%; text-align: center"><img src="' + baseUrl + '/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>');
						let promise;
						if (filter_value) promise = get_project_names(filter_value);
						else promise = get_project_names();
						promise.then(function(ps) {
							var html = '<div class="corner-filter-container"><i class="fa fa-search"></i><input type="search" placeholder="Recherche" class="corner-filter-input" value="' + filter_value + '"></div>';

							if (Object.keys(ps).length) {
								var storage = cd_get_storage();
								html += '<form id="project-selection<?= $blockKey ?>">';
								$.each(Object.keys(ps), function(__, __key) {
									var project = ps[__key];
									var checked = storage['active_project'] && storage['active_project']['id'] === __key ? 'checked="cheched"' : '';
									var container_style = project['background'] ? 'style="background: url(\'' + serverUrl + project['background'] + '\') no-repeat; background-size: cover;"' : '';
									html += '<div class="corner-element" ' + container_style + '><div class="checkbox" style="background-color: rgba(255, 255, 255, .7); border-radius: 5px"><label style="padding: 0; padding: 0 7px;"><input type="radio" value="' + __key + '" ' + checked + ' name="project-selection<?= $blockKey ?>" data-name="' + project['name'] + '" data-slug="' + project['slug'] + '"> ' + project['name'] + '</label></div></div>';
								});
								html += '</form>';
							} else {
								if (_is_costum_admin) html += '<div class="corner-element corner-element" data-target="create_project">Créer le projet "' + filter_value + '"</div>';
								else html += '<div class="corner-element center">Aucun résultat à afficher</div>';
							}

							context_container_dom.empty().html(html).css('align-self', '');

							// mettre le curseur à la fin
							$('.corner-filter-input').off('focus').on('focus', function() {
								var value = this.value ? this.value : '';
								this.setSelectionRange(value.length, value.length);
							});
							$('.corner-filter-input').trigger('focus');

							var timeout = null;
							$('.corner-filter-input').on('input', function(__e) {
								var self = this;
								if (timeout != null) {
									clearTimeout(timeout);
									timeout = null;
								}
								timeout = setTimeout(function() {
									if (_last_input_value !== self.value) {
										load_view();
										_last_input_value = self.value;
									}
								}, 1000);
							});

							$('[name="project-selection<?= $blockKey ?>"]').on('change', function(__e) {
								const self = $(this);
								const name = self.data('name');
								const slug = self.data('slug');
								$('#active-project<?= $blockKey ?>').text(name);
								$(".corner-title").text(name);
								$('[data-target=project-detail]').attr('href', `${serverUrl}/#@${slug}`);
								$('.corner-page-link, .corner-page-button').removeClass('disabled');
								// Revenir à l'écran d'ouverture
								$('#corner_dev<?= $blockKey ?> .turn-back').trigger('click');

								W.cd_set_storage({
									active_project: {
										id: self.val(),
										name,
										slug
									}
								}).then();
							});
							// adaptation de l'affichage
							adjust_view();
						}).catch(function() {
							$('.turn-back').trigger('click');
						});
					}

					load_view();
				},
				action_list: function(__scope) {
					// définition des variables à utiliser notament des éléments du DOM
					const corner_header = $('#corner-header<?= $blockKey ?> .corner-title');
					const context_container = $('#context-container<?= $blockKey ?>');

					// afficher le titre du corner
					corner_header.fadeIn(_animate_duration).text('Liste des actions');

					/**
					 * Charger la vue et initialiser l'événement de recherche
					 * Il ne nécessite pas de paramètre
					 * @return void
					 */
					function load_view() {
						// stocker la valeur de l'input de recherche
						var filter_value = $('.corner-filter-input').length > 0 ? $('.corner-filter-input').val() : '';
						// nettoyer le contenu et afficher à la place un loader
						context_container.css('align-self', 'center').empty().html('<div class="view_loader" style="width: 100%; text-align: center"><img src="' + baseUrl + '/plugins/lightbox2/img/loading.gif" alt="loader" style="width: 32px; height: 32px;" /></div>');

						// créer une variable de promise pour préparer la requête back end
						var promise = null;
						// vérifier si une recherche est faite
						if (filter_value) promise = get_filtered_actions(filter_value, __scope);
						else promise = get_filtered_actions('', __scope);

						// executer la requête
						promise.then(function(__actions) {
							// initier le HTML par le nouveau input à utiliser pour la recherche
							let html = '<div class="corner-filter-container"><i class="fa fa-search"></i><input type="search" placeholder="Recherche" class="corner-filter-input" value="' + filter_value + '"></div>';
							const status = ['discuter', 'todo', "next", 'tracking', 'totest', 'done'];
							html += `
                                <div class="status-filter">
                                    ${status.map(function (__status) {
                                    return `<span class="btn btn-primary btn-xs" data-target="${__status}">${coTranslate('act.' + __status)}</span>`
                                }).join('')}
                                </div>
                                `;

							// construire le squelette de la liste des actions
							if (__actions.length) {
								$.each(status, function(_, s) {
									var filtered = __actions.filter(a => coInterface.actions.get_action_status(a) === s);
									$.each(filtered, function(__, f) {
										html += action_template(f);
									});
								})
							} else
								html += '<div class="corner-element center">Aucun résultat à afficher</div>';
							context_container.empty().html(html).css('align-self', '');

							// Cacher tous les actions de status terminé
							$('.status-filter .btn[data-target=done]').trigger('click')

							// afficher le détail d'une action
							$('.action-text').off('click').on('click', function(__e) {
								var previewDom = $('#corner-action-preview');
								previewDom.empty();
								show_action_detail_modal($(this).data('target')).then(function(__html) {
									previewDom.html(__html);
								});
							});

							$('.cd-action-status [role=menuitem]').off('click').on('click', function(e) {
								var self = $(this);
								var split = self.attr('href').split('#');
								const action_status_map = {
									discuter: {
										classname: 'warning',
										name: 'discuter',
									},
									todo: {
										classname: 'info',
										name: 'à faire',
									},
									tracking: {
										classname: 'warning',
										name: 'en cours',
									},
									totest: {
										classname: 'info',
										name: 'à tester',
									},
									done: {
										classname: 'success',
										name: 'terminé',
									},
									next: {
										classname: "warning",
										name: 'Next',
									}
								};

								e.preventDefault();
								coInterface.actions.request_set_status({
									server_url: serverUrl,
									status: split[0],
									id: split[1]
								}).then(() => {
									update_action_status(split[1], split[0]);
								});
							});

							// evenement de participation
							$('.action-participation').off('click').on('click', function() {
								// décider de ne plus participer
								const action_id = $(this).data('target');
								if ($(this).hasClass('green')) {
									const self = this;
									const params = {
										id: action_id,
										collection: 'actions',
										path: 'links.contributors.' + userConnected['_id']['$id'],
										value: null
									};
									dataHelper.path2Value(params, function(resp) {
										if (resp['result']) {
											toastr.success('Vous ne participez plus à cette action');
											$(self).removeClass('green').addClass('red').empty().html('<span class="fa fa-link"></span> Participer');
										}
									}, path2ValueOption);
								} else
									// décider de participer
									if ($(this).hasClass('red')) {
										const self = this;
										const params = {
											id: action_id,
											collection: 'actions',
											path: 'links.contributors.' + userConnected['_id']['$id'],
											value: {
												type: 'citoyens'
											}
										};
										dataHelper.path2Value(params, function(resp) {
											if (resp['result']) {
												rocket_chat_contribution(action_id, [userConnected['_id']['$id']]);
												toastr.success('Vous êtes maintenant un participant de cette action');
												$(self).addClass('green').removeClass('red').empty().html('<span class="fa fa-unlink"></span> Se retirer');
											}
										}, path2ValueOption);
									}
							});

							// evenement sur les screenshots
							$('.action-image-uploader').on('click', function() {
								const action_id = $(this).data('target');
								const screenshot_container = $(this).parents('.corner-element').find('.screenshot-container');
								if (screenshot_container.is(':hidden')) {
									// fermer les fenêtres ouvrantes
									$('.screenshot-container').slideUp(_animate_duration);

									get_filtered_screenshots(action_id).then(function(__screenshots) {
										var screenshots = Object.keys(__screenshots.images).map(function(key) {
											return serverUrl + __screenshots.images[key].docPath;
										});
										var html = '';
										$.each(screenshots, function(_, img) {
											html += '<div class="image-container"><img src="' + img + '" alt="' + _ + '" style="border-radius: 0; width: 100%;" /></div>';
										});
										html += '<div style="text-align: center;"><button class="btn btn-primary add-screenshot" data-target="' + action_id + '"><span class="fa fa-plus"></span> Ajouter</button></div>'
										screenshot_container.empty().html(html).slideDown(_animate_duration, function() {
											$('.image-container img').each(function() {
												const image = new Image();
												image.onload = adjust_view;
												image.src = $(this).attr('src');
											});
											adjust_view();
										});

										// affichage aperçu d'une image
										$('.image-container img').on('click', function() {
											$('#screenshot-preview<?= $blockKey ?>').attr('src', $(this).attr('src'));
											$('#corner-screenshot-preview<?= $blockKey ?>').fadeIn(_animate_duration);
										});

										$('.add-screenshot').on('click', function(__e) {
											__e.preventDefault();
											screenshot_container.slideUp(_animate_duration, adjust_view);
											var self = this;
											dyFObj.openForm({
												jsonSchema: {
													title: 'Ajouter des captures',
													description: '',
													properties: {
														action_media: {
															inputType: 'uploader',
															label: 'Capture d\'écran',
															docType: 'image',
															itemLimit: 0,
															showUploadBtn: false,
															filetypes: [
																'png', 'jpg', 'jpeg', 'gif'
															],
														},
													},
													beforeBuild: function() {
														uploadObj.set('actions', action_id);
													},
													afterBuild: function() {
														setTimeout(function() {
															$('.qq-upload-button-selector.btn.btn-primary').css({
																position: 'absolute',
																left: '50%',
																transform: 'translateX(-50%)'
															});
														}, 200);
													},
													save: function() {
														dyFObj.commonAfterSave(null, function() {
															var params = {
																searchType: ["documents"],
																fields: ["id"],
																filters: {
																	'id': action_id,
																},
																notSourceKey: true,
															};
															var url = serverUrl + '/' + moduleId + '/search/globalautocomplete';
															ajaxPost(null, url, params, function(resp) {
																dataHelper.path2Value({
																	id: action_id,
																	collection: 'actions',
																	path: 'media.images',
																	value: Object.keys(resp.results)
																}, function(resp) {
																	if (resp['result'] && resp['result'] === true) {
																		toastr.success(resp['msg']);
																		dyFObj.closeForm();
																	}
																}, path2ValueOption)
															})
														});
													}
												}
											});
										});
									});
								} else {
									screenshot_container.slideUp(_animate_duration, adjust_view);
								}
							});

							// ajout de liens
							$('.action-add-link').on('click', function() {
								const action_id = $(this).data('target');
								get_selected_action(action_id).then(function(__action) {
									dyFObj.openForm({
										jsonSchema: {
											title: 'Modifier les URLS',
											description: 'Ajouter des nouveaux URLS ou en supprimer d\'autres pour l\'action ' + __action['name'],
											properties: {
												action_update_links: {
													label: 'Urls',
													inputType: 'array',
													value: __action['urls'] ? __action['urls'] : []
												}
											},
											save: function(form_data) {
												const path2value = {
													id: action_id,
													collection: 'actions',
													path: 'urls',
													value: getArray('.action_update_linksarray')
												};
												dataHelper.path2Value(path2value, function(resp) {
													if (resp['result'] && resp['result'] === true) {
														toastr.success(resp['msg']);
														dyFObj.closeForm();
													}
												}, path2ValueOption);
											}
										}
									});
								});
							});
							// mettre le curseur à la fin
							$('.corner-filter-input').off('focus').on('focus', function() {
								var value = this.value ? this.value : '';
								this.setSelectionRange(value.length, value.length);
							});
							$('.corner-filter-input').trigger('focus');
							// temps d'attende pour exécuter la recherche si on tape du texte dans l'input de recherche
							var timeout = null;
							$('.corner-filter-input').on('input', function(__e) {
								var self = this;
								if (timeout != null) {
									clearTimeout(timeout);
									timeout = null;
								}
								timeout = setTimeout(function() {
									if (_last_input_value !== self.value) {
										load_view();
										_last_input_value = self.value;
									}
								}, 1000);
							});

							// Animation et adaptation de l'affichage
							adjust_view();
						}).catch(function() {
							$('.turn-back').trigger('click');
						});
					}

					// charger la vue une fois
					load_view();
				}
			}
			// Fin section des variables globales

			// Début section des fonctions
			function init_socket(args) {
				var self = this;
				var socketConfigurationEnabled;
				if (typeof args !== "object")
					return (false);
				socketConfigurationEnabled = args.coWsConfig.enable && args.coWsConfig.serverUrl && args.coWsConfig.pingActionManagement;
				if (!socketConfigurationEnabled) {
					return (false);
				}
				args.wsCO = args.wsCO && args.wsCO.connected ? args.wsCO : (args.io ? args.io.connect(args.coWsConfig.serverUrl) : null);
				if (!wsCO) {
					return (false);
				}
				$.each(Object.keys(socket_handlers), function(i, event) {
					var fn = socket_handlers[event].bind(self);
					args.wsCO.on(event, fn);
				});
			}

			function get_project_info_from_slug(slug) {
				var url = serverUrl + '/costum/project/project/request/project_from_slug';
				var post = {
					slug: slug
				};
				return new Promise(function(resolve, reject) {
					ajaxPost(null, url, post, function(response) {
						if (response === null)
							reject();
						resolve(response);
					}, reject);
				});
			}
			/**
			 * Vérifier si la variable n'existe pas, n'est pas définie ou est vide
			 * @param {any} __input Variable à évaluer
			 * @returns {boolean}
			 */
			function is_empty(__input) {
				return typeof __input === 'undefined' || typeof __input === 'object' && (__input === null || __input instanceof Array && __input.length === 0) || typeof __input === 'string' && __input === '';
			}

			/**
			 * Sauvegarder dans la base de données les paramètres liés au costum
			 *
			 * @return {Promise.<boolean}
			 */
			function save_costum_setting() {
				storage = cd_get_storage();
				value = {};
				if (!is_empty(storage.active_project))
					value = {
						id: storage.active_project.id,
						name: storage.active_project.name,
						slug: storage.active_project.slug,
					};
				value['admin_only'] = storage.cs_visibility === trad['administrator'];

				const path2value = {
					id: costum.contextId,
					collection: costum.contextType,
					path: 'costum.cd_project',
					value
				};

				return new Promise(function(succeed) {
					dataHelper.path2Value(path2value, function(__response) {
						succeed(__response['result']);
					}, path2ValueOption);
				});
			}

			function update_action_status(id, status) {
				var dom = $('.corner-element.action[data-id=' + id + ']');
				var tag_status = ['totest', 'discuter'];
				var action_status_map = {
					discuter: 'warning',
					todo: 'info',
					tracking: 'warning',
					totest: 'info',
					done: 'success',
					closed: 'danger'
				};
				dom.attr('data-status', status);
				if (status === 'tracking') {
					dom.find('.action-tracking').addClass('active');
					dom.find('.action-tracking .fa').removeClass('fa-star-o').addClass('fa-star');
				} else {
					dom.find('.action-tracking').removeClass('active');
					dom.find('.action-tracking .fa').addClass('fa-star-o').removeClass('fa-star');
				}
				if (tag_status.includes(status)) {
					dom.find('.tags-container .badge').filter(function() {
						var data_status = this.dataset.content.substr(1);
						return (data_status !== status && tag_status.includes(data_status));
					}).remove();
					dom.find('.tags-container').append('<span class="badge action-tag" data-content="#' + status + '"><span class="fa fa-tag"></span> ' + status + '</span>');
				} else {
					dom.find('.tags-container .badge').filter(function() {
						return (tag_status.includes(this.dataset.content.substr(1)))
					}).remove();
				}
				dom.find('.label')
					.removeClass()
					.addClass('label')
					.addClass('label-' + action_status_map[status])
					.addClass('pull-right')
					.text(coTranslate('act.' + status))
			}

			/**
			 * Générer un template pour une action
			 * @param {Object} __action
			 * @param {boolean} __action.is_contributor L'utilisateur courant fait-il parti de cette action ou pas
			 * @param {string} __action.status Status de l'action
			 * @returns {string}
			 */
			function action_template(__action) {
				// Object regroupant les différents status d'une action
				const action_status_map = {
					discuter: {
						classname: 'warning',
						name: 'discuter',
					},
					todo: {
						classname: 'info',
						name: 'à faire',
					},
					tracking: {
						classname: 'warning',
						name: 'en cours',
					},
					totest: {
						classname: 'info',
						name: 'à tester',
					},
					done: {
						classname: 'success',
						name: 'terminé',
					},
					next: {
						classname: "warning",
						name: 'Next',
					}
				};

				const action_participation = '<span class="action-participation ' + (__action['is_contributor'] ? 'green' : 'red') + '" data-target="' + __action['id'] + '"><span class="fa fa-' + (__action['is_contributor'] ? 'unlink' : 'link') + '"></span> ' + (__action['is_contributor'] ? 'Se retirer' : 'Participer') + '</span>';
				const action_image_uploader = '<span class="action-image-uploader" data-target="' + __action['id'] + '"><span class="fa fa-picture-o"></span> Captures</span>';
				const action_add_link = '<span class="action-add-link" data-target="' + __action['id'] + '"><span class="fa fa-external-link"></span> URLS</span>';
				let tags_html = '';
				$.each(__action['tags'], function(__, __tag) {
					tags_html += '<span class="badge action-tag" data-content="#' + __tag + '"><span class="fa fa-tag"></span> ' + __tag + '</span> ';
				});
				const real_status = coInterface.actions.get_action_status(__action);
				return `
                        <div class="corner-element action" data-id="${__action.id}" data-status="${real_status}">
                            <div class="corner-element-body">
                                <div>
									<div class="dropdown pull-right">
										<span class="label label-${action_status_map[real_status]['classname']}" data-toggle="dropdown" data-status="${real_status}">${coTranslate('act.' + real_status)}</span>
										<ul class="dropdown-menu cd-action-status " role="menu" aria-labelledby="dropdownMenu1">
											${Object.keys(action_status_map).map(s => {
												return `<li role="presentation"><a role="menuitem" tabindex="-1" href="${s}#${__action.id}" >${action_status_map[s].name}</a></li>`;
											}).join('')}
										</ul>
									</div>
                                    <span class="action-tracking ${__action['tracking'] ? 'active' : ''}" data-id="${__action['id']}">
                                        <span class="fa fa-${__action['tracking'] ? 'star' : 'star-o'}"></span>
                                    </span>
                                    <span class="action-text" data-target="${__action['id']}">${__action['name']}</span>
                                    <div class="tags-container">${tags_html}</div>
                                </div>
                                <div class="screenshot-container"></div>
                            </div>
                            <div class="corner-element-footer">${action_participation + action_image_uploader + action_add_link}</div>
                        </div>`;
			}

			/**
			 * Ajuster la vue courante
			 */
			function adjust_view() {
				const link_container_dom = $('#link-container<?= $blockKey ?>');
				const context_container_dom = ['0px', '0%'].includes(link_container_dom.css('margin-left')) ? link_container_dom : $('#context-container<?= $blockKey ?>');
				let overflow = false;

				let current_height = $('#corner-body<?= $blockKey ?>').height();
				let auto_height = context_container_dom.css('height', 'auto').height();
				const top_offset = 60 + $('#corner-header<?= $blockKey ?> .corner-title').outerHeight();

				if (auto_height > $(W).height() - top_offset) {
					auto_height = $(W).height() - top_offset;
					overflow = true;
				}

				if ($('#corner-card<?= $blockKey ?>').is(':visible')) {
					$('#corner-body<?= $blockKey ?>').height(current_height).animate({
						height: auto_height,
					}, {
						duration: _animate_duration,
						complete: function() {
							if (overflow) $(this).css('overflow-y', 'auto');
							else $(this).css('overflow-y', '');
						}
					});
				}
			}

			/**
			 * Générer une clé pour le stockage des valeurs
			 * @returns {string}
			 */
			function generate_storage_key() {
				const user_connected = userConnected && userConnected['_id'] && userConnected['_id']['$id'] ? userConnected['_id']['$id'].substring(-12) : '';
				const costum_key = costum && costum['_id'] && costum['_id']['$id'] ? costum['_id']['$id'].substring(-12) : '';
				return user_connected + costum_key + _storage_version;
			}

			/**
			 * Lecture du stockage du navigateur pour corner_dev
			 * @returns {active_project: {id: string, name: string, slug: string}, cs_visibility: string}
			 */
			if (typeof W.cd_get_storage !== "function")
				W.cd_get_storage = function() {
					const key = generate_storage_key();
					let output = {
						active_project: null,
						cs_visibility: trad['everyone']
					};
					const storage = localStorage.getItem(key + 'corner_dev');
					if (storage !== null) {
						output = JSON.parse(storage);
					}
					return output;
				}

			/**
			 * Enregistrer une valeur dans le storage
			 * @return {Promise.<{active_project: {id: string, name: string, slug: string}, cs_visibility: string}>}
			 */
			if (typeof W.cd_set_storage !== "function")
				W.cd_set_storage = function(__object = {}) {
					const key = generate_storage_key();
					let storage = cd_get_storage();
					let output = $.extend(true, {}, storage, __object);
					localStorage.setItem(key + 'corner_dev', JSON.stringify(output));
					if (costum && !costum.type)
						save_costum_setting().then();
					return new Promise(function(resolve) {
						if (!is_empty(output['active_project'])) {
							const url = serverUrl + '/costum/project/project/request/room';
							const post = {
								project: output['active_project']['id'],
								connectedUser: userConnected._id.$id
							};
							ajaxPost(null, url, post, function(__room) {
								output.active_project.room = __room;
								localStorage.setItem(key + 'corner_dev', JSON.stringify(output));
								resolve(output);
							});
						} else
							resolve(output);
					});
				}

			/**
			 * Permet le tracking d'une action
			 * @param {boolean} __track - Status de l'action si elle est tracké ou pas
			 * @param {string} __action - Identifiant de l'action
			 */
			function track_action(__track, __action) {
				const params = {
					id: __action,
					collection: 'actions',
					path: 'tracking',
					value: __track
				};
				dataHelper.path2Value(params, function(__response) {
					if (!__response['result'])
						toastr.error(__response['msg']);
				}, path2ValueOption);
			}

			/**
			 * Affiche un modal contenant les détails d'une action.
			 * Possibilité d'ajouter des sous tâches et inviter des participants.
			 * @param {string} __id Identifiant de l'action à récupérer
			 * @return {Promise.<string>}
			 */
			function show_action_detail_modal(__id) {
				return new Promise(function(succeed) {
					const url = baseUrl + '/costum/project/action/request/action_detail_html';
					const post = {
						id: __id,
						connectedUser: userConnected._id.$id,
						server_url: serverUrl
					};
					ajaxPost(null, url, post, succeed, null, 'text');
				});
			}

			/**
			 * Récupère les données d'une action sélectionnée
			 * @param {string} __action identifiant de l'action à récupérer
			 * @return {Promise}
			 */
			function get_selected_action(__action) {
				return new Promise(function(succeed) {
					const url = serverUrl + '/costum/project/action/request/action';
					const post = {
						action: __action,
						connectedUser: userConnected._id.$id
					};
					ajaxPost(null, url, post, succeed);
				});
			}

			/**
			 * Récupère les tags d'une action sélectionnée
			 * @param {string} __action identifiant de l'action à récupérer
			 * @return {Promise.<string[]>}
			 */
			function get_tags(__action) {
				return new Promise(function(succeed) {
					const url = serverUrl + '/costum/project/action/request/tags';
					const post = {
						id: __action,
						connectedUser: userConnected._id.$id
					};
					ajaxPost(null, url, post, succeed);
				});
			}

			/**
			 * Récupère la liste de tous les screenshots associé à l'action
			 * @param {String} __action identifiant de l'action
			 * @return {Promise}
			 */
			function get_filtered_screenshots(__action) {
				return new Promise(function(succeed) {
					const url = serverUrl + '/costum/project/action/request/media';
					const post = {
						action: __action,
						connectedUser: userConnected._id.$id
					};
					ajaxPost(null, url, post, succeed);
				});
			}

			/**
			 * Récupère les actions filtrés sur l'URL courant.
			 * Récupère seulement les valeurs de retour au cas où la requête est un succès.
			 * Ignore les erreurs s'il y en a.
			 * @param {string} [__text] Terme à rechercher
			 * @param {string} [__scope] Encapsulation de la requête (all, mine, vide)
			 * @returns {Promise}
			 */
			function get_filtered_actions(__text, __scope) {
				return new Promise(function(succeed, failed) {
					// endpoint de la requête
					const url = `${serverUrl}/costum/project/action/request/actions${__scope ? `/scope/${__scope}` : ''}`;
					// construire la requête
					const post = {
						connectedUser: userConnected._id.$id,
						filters: {
							urls: {
								'$in': [_current_location]
							}
						}
					};
					/**
					 * Vérifier si un projet est sélectionné
					 * Si oui on filtre la liste des actions en fonction du projet sélectionné
					 * Sinon on sélectionne tout
					 */
					const storage = cd_get_storage();
					if (storage && storage['active_project']) {
						post['filters']['parentId'] = storage['active_project']['id'];
					} else {
						failed({
							side: 'client',
							msg: 'Incomplete argument'
						});
						return ;
					}
					if (__text) post['text'] = __text;
					// lance la requête
					ajaxPost(null, url, post, succeed, failed);
				});
			}

			/**
			 * Fonction servant à recevoir la liste de tous les projets
			 * @param {string} [__text=''] Contient le terme à chercher s'il s'agit d'une recherche
			 * @param {Object} [__params={}] Paramètres facultatifs
			 * @return {Promise.<Object.<string, {name: string, slug: string, background: string}>>}
			 */
			function get_project_names(__text = '', __params = {}) {
				const url = serverUrl + '/costum/project/project/request/project_names';
				const post = {
					connectedUser: userConnected._id.$id,
					params: {}
				};
				if (__text) post['text'] = __text;

				for (let key in __params) post.params[key] = __params[key];

				return new Promise(function(succeed, failed) {
					ajaxPost(null, url, post, succeed, failed);
				});
			}

			/**
			 * Envoyer une notification RocketChat sur la participations des citoyens à une action créée
			 * @param {String} __action identifiant de l'action auquel le contributeur participe
			 * @param {Array} __contributors liste des identifiants des participants
			 * @return {Promise.<{status: boolean, msg: string}[]>}
			 */
			function rocket_chat_contribution(__action, __contributors) {
				const url = serverUrl + '/costum/project/action/request/contribution_rocker_chat';
				const storage = cd_get_storage();
				const post = {
					channel: storage['active_project']['slug'],
					action: __action,
					project_name: storage['active_project']['name'],
					contributors: __contributors
				};
				return new Promise(function(succeed) {
					ajaxPost(null, url, post, succeed);
				});
			}

			/**
			 * Récupérer le paramètre de corner-dev par défaut du costum
			 * @return {Promise.<{id: string, slug: string, name: string}>}
			 */
			function get_costum_corner_dev() {
				const url = `${serverUrl}/costum/project/project/request/cd_from_costum`;
				const post = {
					collection: costum.contextType,
					id: costum.contextId
				};
				return new Promise(function(succeed, __reject) {
					ajaxPost(null, url, post, function(__response) {
						if (!is_empty(__response['id'])) succeed(__response);
						else __reject(null);
					}, __reject);
				})
			}

			/**
			 * Fonction pour l'initialisation de la vue sur le projet ainsi que le chargement des rooms
			 *
			 * @return {Promise}
			 */
			function init_view() {
				const project_list_dom = $('[data-target=project_list]');
				const project_detail_dom = $('[data-target=project-detail]');
				const setting_dom = $('.corner-actions [data-target=setting]');
				var link_match = W.location.hash.match(/#@([\w\d]{1,})/);
				project_list_dom.removeClass('disabled');
				$('.corner-page-link, .corner-page-button').removeClass('disabled');
				$('[data-target="project_list"]').hide();
				$('.project-change').css('display', '');
				setting_dom
					.prop("disabled", true)
					.hide();
				project_detail_dom.hide();

				return new Promise(function(succeed) {
					const storage = cd_get_storage();
					if (!is_empty(costum) && (!costum.type || costum.type !== 'aap')) {
						setting_dom
							.prop("disabled", false)
							.show();
						if (!_is_developper)
							project_list_dom.addClass('disabled');
						get_costum_corner_dev().then(function(p) {
							W.cd_set_storage({
								active_project: {
									id: p.id,
									name: p.name,
									slug: p.slug
								},
								cs_visibility: p['admin_only'] ? trad['administrator'] : trad['everyone']
							}).then();
							$('#active-project<?= $blockKey ?>').text(p.name);
							$(".corner-title").text(p.name);
							project_detail_dom.attr('href', `${serverUrl}/#@${p.slug}`).show();
							if (_is_costum_admin) project_list_dom.removeClass('disabled');
						}).catch(function() {
							const project = _is_costum && costum.contextType === 'projects' ? costum.contextId : '';
							if (!is_empty(project)) {
								W.cd_set_storage({
									active_project: {
										id: project,
										name: costum.title,
										slug: costum.slug
									}
								}).then();
								$('#active-project<?= $blockKey ?>').text(costum.title);
								$(".corner-title").text(costum.title);
								project_detail_dom.attr('href', `${serverUrl}/#@${costum.slug}`).show();
								succeed();
							} else if (_is_costum_admin)
								$(".corner-title").text('Corner dev');
						});
					} else if (link_match !== null && link_match.length >= 2) {
						get_project_info_from_slug(link_match[1]).then(function(project) {
							W.cd_set_storage({
								active_project: {
									id: project.id,
									name: project.name,
									slug: project.slug
								}
							}).then(function(storage) {
								$('#active-project<?= $blockKey ?>').text(storage.active_project.name);
								$(".corner-title").text(storage.active_project.name);
								$('.project-change').hide();
								succeed();
							});
						});
					} else if (storage['active_project']) {
						$('#active-project<?= $blockKey ?>').text(storage['active_project']['name']);
						$(".corner-title").text(storage.active_project.name);
						project_detail_dom.attr('href', `${serverUrl}/#@${storage.active_project.slug}`).show();
						succeed();
					} else {
						$(".corner-title").text('Corner dev');
						succeed();
					}
				});
			}

			// Fin section des fonctions

			/**
			 * DOM initialisé
			 */
			$(function() {
				/**
				 * Variables
				 */
				const corner_button_dom = $('#corner-button-container<?= $blockKey ?>');
				const corner_card_dom = $('#corner-card<?= $blockKey ?>');
				const corner_body_dom = $('#corner-body<?= $blockKey ?>');
				const corner_header_dom = $('#corner-header<?= $blockKey ?>');
				const link_container_dom = $('#link-container<?= $blockKey ?>');
				const context_container_dom = $('#context-container<?= $blockKey ?>');
				const turn_back_dom = $('#corner-header<?= "$blockKey " ?>.turn-back');

				function load_badgets() {
					const storage = cd_get_storage();
					if (!is_empty(storage) && !is_empty(storage.active_project)) {
						get_filtered_actions(':all').then(function(__actions) {
							const badge_dom = $('.all-action-badge<?= $blockKey ?>');
							badge_dom.each(function() {
								const self = $(this);
								const initial = self.text() ? parseInt(self.text(), 10) : 0;
								self.prop('Counter', initial).animate({
									Counter: __actions['length']
								}, {
									duration: 1000,
									easing: 'swing',
									step: function(__now) {
										self.text(Math.ceil(__now));
									}
								});
							});
						});
						get_filtered_actions('', 'mine').then(function(__actions) {
							const badge_dom = $('.mine-action-badge<?= $blockKey ?>');
							badge_dom.each(function() {
								const self = $(this);
								const initial = self.text() ? parseInt(self.text(), 10) : 0;
								self.prop('Counter', initial).animate({
									Counter: __actions['length']
								}, {
									duration: 1000,
									easing: 'swing',
									step: function(__now) {
										self.text(Math.ceil(__now));
									}
								});
							});
						});
						get_filtered_actions().then(function(__actions) {
							const badge_dom = $('.context-linked-action-badge<?= $blockKey ?>');
							badge_dom.each(function() {
								const self = $(this);
								const initial = self.text() ? parseInt(self.text(), 10) : 0;
								self.prop('Counter', initial).animate({
									Counter: __actions['length']
								}, {
									duration: 1000,
									easing: 'swing',
									step: function(__now) {
										self.text(Math.ceil(__now));
									}
								});
							});
						});
						get_project_names().then(function(ps) {
							const badge = $('#project-list-badge<?= $blockKey ?>');
							const initial = badge.text() ? parseInt(badge.text(), 10) : 0;
							badge.prop('Counter', initial).animate({
								Counter: Object.keys(ps)['length']
							}, {
								duration: 1000,
								easing: 'swing',
								step: function(__now) {
									$(this).text(Math.ceil(__now));
								}
							});
						});
					}
				}

				// Quand on appuye sur le bouton de corner dev
				corner_button_dom.on('click', '>button', function(e) {
					e.preventDefault();
					$(corner_button_dom).hide();
					corner_card_dom.show();
					load_badgets();
				});

				// Quand on ferme le menu du corner dev
				corner_card_dom
					.find('.corner-close')
					.on('click', function(__e) {
						__e.preventDefault();
						var sleep = 0;
						if (turn_back_dom.is(":visible")) {
							turn_back_dom.trigger("click");
							sleep = 200;
						}
						setTimeout(() => {
							corner_button_dom.fadeIn(_animate_duration);
							corner_card_dom.animate({
								width: 0,
								height: 0
							}, _animate_duration, function() {
								corner_card_dom.css({
									display: '',
									width: '',
									height: ''
								});
							});
						}, sleep);
					});

				// Quand on clique sur l'un des liens pour changer de page dans le menu
				corner_body_dom.on('click', '.corner-page-link:not(.disabled)', function(__e) {
					if ($(__e.target).parents('[data-toggle=modal]').length) return true;
					$('.project-change').hide();
					const self = this;
					corner_header_dom.find('.corner-title').fadeOut(_animate_duration);
					$('[data-target=setting]').hide();
					link_container_dom.animate({
						marginLeft: '-=100%'
					}, _animate_duration, 'easeInQuad', function() {
						corner_header_dom.find('.turn-back').css({
							'display': 'flex'
						});
						_open_context_container[$(self).data('target')]($(self).data('scope'));
					});

					// sync socket io
					if (W.wsCO) {
						W.wsCO.on("action-full-update", function(data) {
							var card_dom = $(".corner-element[data-id=" + data.id + "]");

							if (card_dom.length)
								card_dom.find(".action-text").text(data.name);
						});
					}
				}).on('click', '[data-kanban]', function(__f) {
					var storage = cd_get_storage();

					if (!storage.active_project)
						return ;
					$(this).attr('data-target', '');
					if ($(__f.target).parents('[data-toggle=modal]').length == 1) {
						loadKanbanAction(this.dataset.kanban, this.dataset.title)
						$(this).attr('data-target', '#modalKanbanAction')
					}
				});

				$('.project-change').on('click', function() {
					$('[data-target="project_list"]').trigger('click');
				});

				turn_back_dom.on('click', function() {
					const link_contents = link_container_dom.html();
					var link_match = W.location.hash.match(/#@([\w\d]{1,})/);
					var setting_dom = $('.corner-actions [data-target=setting]');
					corner_header_dom.find('.corner-title').fadeOut(_animate_duration);
					context_container_dom.empty();
					_last_input_value = '';

					if (!link_match) {
						$('.project-change').css('display', '');
						if (costum && !costum.type)
							$('[data-target=setting]').css('display', '');
					}
					
					link_container_dom.animate({
						marginLeft: '+=100%',
					}, _animate_duration, 'easeOutQuart', function() {
						var storage = cd_get_storage();
						corner_header_dom.find('.corner-title').fadeIn(_animate_duration);
						if (storage && storage.active_project)
							corner_header_dom.find('.corner-title').text(storage.active_project.name);
						corner_header_dom.find('.turn-back').css({
							display: ''
						});
						if (!setting_dom.prop("disabled"))
							setting_dom.fadeIn(_animate_duration);
						const current_height = corner_body_dom.height(),
							auto_height = link_container_dom.css('height', 'auto').outerHeight();
						corner_body_dom.height(current_height).animate({
							height: auto_height
						}, _animate_duration, function() {
							corner_body_dom.css('overflow-y', 'hidden');
							link_container_dom.empty().html(link_contents);
							load_badgets();
						});
					});
				});

				link_container_dom.off("click").on('click', '#new-action<?= $blockKey ?>:not(.disabled)', function(__e) {
					__e.preventDefault();
					corner_card_dom.find('.corner-close').trigger('click');
					const default_finder = {};
					const storage = cd_get_storage();

					default_finder[userConnected['_id']['$id']] = {
						type: 'citoyens',
						name: userConnected['name']
					};

					if (storage.active_project) {
						xhr_get_create_data(storage.active_project.id).then(function(data) {
							var milestone_data = {};
							var oceco_tags = {};
							const dateFields = ['startDate', 'endDate'];
							const milestones = {};

							$.each(data.milestones, (_, m) => {
								$.each(dateFields, (__, f) => {
									if (m[f])
										m[f] = moment(m[f]).format();
								});
								milestone_data[m.milestoneId] = m.name;
								milestones[m.milestoneId] = m;
							});
							$.each(data.tags, function(_, t) {
								oceco_tags[t] = t;
							});
							dyFObj.openForm({
								jsonSchema: {
									title: "Nouvelle action dans " + storage.active_project.name,
									properties: {
										action_id: dyFInputs.inputHidden(""),
										action_name: dyFInputs.name('action'),
										action_parentType: dyFInputs.inputHidden("projects"),
										action_parentId: dyFInputs.inputHidden(storage['active_project']['id']),
										action_creator: dyFInputs.inputHidden(userConnected['_id']['$id']),
										action_credits: dyFInputs.inputHidden(1),
										action_idUserAuthor: dyFInputs.inputHidden(userConnected['_id']['$id']),
										action_idParentRoom: dyFInputs.inputHidden(storage['active_project']['room']),
										action_tags: {
											inputType: 'select',
											isSelect2: true,
											select2: {
												multiple: true
											},
											label: trad.tags,
											options: oceco_tags
										},
										action_milestone: {
											inputType: "select",
											label: "Sélectionner un jalon",
											options: milestone_data
										},
										action_contributors: {
											inputType: 'finder',
											label: 'Assigné à qui ?',
											multiple: true,
											rules: {
												lengthMin: [1, 'contributors']
											},
											initType: ['citoyens'],
											values: default_finder,
											initBySearch: true,
											initMe: true,
											initContext: false,
											initContacts: false,
											openSearch: true
										},
										action_image: {
											inputType: "uploader",
											label: trad.images,
											docType: "image",
											itemLimit: 0,
											filetypes: ["jpeg", "jpg", "gif", "png"],
											showUploadBtn: false,
										},
										action_description: {
											inputType: "textarea",
											label: "Description",
										},
										action_urls: {
											label: 'Urls',
											inputType: 'array',
											value: [
												W.location.toString()
											]
										},
										action_status: dyFInputs.inputHidden('todo'),
									},
									beforeBuild: function() {
										dyFObj.setMongoId("actions");
									},
									afterBuild: function() {
										setTimeout(function() {
											$('.qq-upload-button-selector.btn.btn-primary').css({
												position: 'absolute',
												left: '50%',
												transform: 'translateX(-50%)'
											});
										}, 200);
									},
									save: function(form_data) {
										var value = {
											urls: getArray('.action_urlsarray'),
											status: "todo",
										};
										var key = "action_";
										var contributors = {};
										$.each(Object.keys(form_data['action_contributors']), function(__, __key) {
											contributors[__key] = {
												type: 'citoyens'
											};
										});
										$.each(['scope', 'collection', 'action_contributors', 'action_urls[]'], function(__, __key) {
											delete form_data[__key];
										});
										$.each(Object.keys(form_data), function(_, n) {
											var name;

											if (n.indexOf(key) >= 0)
												name = n.substring(key.length);
											else
												name = n;
											if (name === "milestone")
												value[name] = milestones[form_data[n]];
											else
												value[name] = form_data[n];
										});
										if (value.milestone) {
											if (value.milestone.startDate)
												setType.push({
													path: "milestone.startDate",
													type: "isoDate"
												});
											if (value.milestone.endDate)
												setType.push({
													path: "milestone.endDate",
													type: "isoDate"
												});
										}
										var params = {
											collection: 'actions',
											value: value,
										};
										dataHelper.path2Value(params, function(resp) {
											coInterface.actions.update_action_position({
												action: resp.saved.id,
												emiter: W.wsCO && W.wsCO.id ? W.wsCO.id : null,
												group: "todo",
												component: 'cornerdev',
												server_url: serverUrl
											}).then();
											// Envoyer une notification rocket chat pour la nouvelle tâche
											url = serverUrl + '/survey/answer/rcnotification/action/newaction';
											post = {
												projectname: storage['active_project']['name'],
												actname: value.name,
												channelChat: storage['active_project']['slug'],
											};
											ajaxPost(null, url, post);
											dyFObj.commonAfterSave(resp, function() {
												setTimeout(function() {
													var action_id,
														url,
														post;

													if (resp.result && resp.saved.result) {
														action_id = resp.saved.id;
														url = serverUrl + '/costum/project/action/request/add_image_file';
														post = {
															action: action_id
														};
														// enregistrer les images
														ajaxPost(null, url, post, function() {});
														// Enregistrer les contributeurs
														dataHelper.path2Value({
															id: action_id,
															collection: 'actions',
															path: 'links.contributors',
															value: contributors
														}, function(resp) {
															if (resp['result'] && resp['result'] === true) {
																rocket_chat_contribution(action_id, Object.keys(contributors));
																ajaxPost(null,
																	serverUrl + "/costum/project/action/request/socket-action-create", {
																		id: action_id
																	},
																	function(resp) {
																		mylog.log("Rinelfi socket action", resp);
																	}
																);
																toastr.success(resp.msg);
																dyFObj.closeForm();
															}
														}, path2ValueOption);
													}
												}, 2000);
											});
										}, path2ValueOption);
									}
								}
							});
						});
					} else
						toastr.error('Vous devez sélectionner un projet pour ajouter une action');
				});

				context_container_dom.on('click', '[data-target=create_project]', function() {
					dyFObj.openForm('project', null, {name: $('.corner-filter-input').val()}, null, {
						afterSave: function(resp) {
							W.cd_set_storage({
								active_project: {
									id: resp.id,
									name: resp.map.name,
									slug: resp.map.slug,
								}
							}).then(function () {
								if (is_empty(resp.map.parent[costum.contextId]) ||
										resp.map.parent[costum.contextId].type !== costum.contextType) {
									dataHelper.path2Value({
										id: resp.id,
										collection: resp.map.collection,
										path: 'parent.' + costum.contextId,
										value: {
											name: costum.title,
											type: costum.contextType
										}
									}, function(ret) {
										if (ret.result) {
											dyFObj.closeForm();
											init_view();
										}
									}, path2ValueOption)
								} else
									init_view();
							});
						}
					});
					$('.corner-close').trigger('click');
				});

				$(document).on('keydown', function(__e) {
					// Combinaison de la touche ctrl + shit + q
					const original_event = __e.originalEvent;
					if (original_event.ctrlKey && original_event.shiftKey && original_event.code === 'KeyA') {
						$('#corner_dev<?= $blockKey ?>').fadeToggle(_animate_duration);
					}

					if(__e.originalEvent.key && __e.originalEvent.key === "Escape") {
						$("#modalKanbanAction").modal('hide')
					}
				});

				$('.controlls<?= "$blockKey " ?> [data-target=close], .image-container<?= $blockKey ?>').on('click', function(__e) {
					if (this === __e.target) $('#corner-screenshot-preview<?= $blockKey ?>').fadeOut(_animate_duration);
				});

				context_container_dom.on('click', '.action-tag', function(__e) {
					const input = $('.corner-filter-input');
					const value = input.val();

					if (value.match(/^:[a-zA-Z0-9]*[a-zA-Z0-9\-\s]*[a-zA-Z0-9](,(\s)?:[a-zA-Z0-9]*[a-zA-Z0-9\-\s]*[a-zA-Z0-9])*$/))
						input.val(value + ',' + $(this).data('content'));
					else
						input.val($(this).data('content')).trigger('input');
				});

				context_container_dom.on('click', '.status-filter .btn', function() {
					const self = $(this);
					const target = self.data('target');
					const action_target_dom = $(`[data-status=${target}]`);

					if (self.hasClass(`btn-primary`)) {
						self.removeClass(`btn-primary`).addClass('btn-default');
						action_target_dom.each(function(__index) {
							if (__index === action_target_dom.length - 1)
								$(this).hide({ complete: adjust_view });
							else
								$(this).hide();
						});
					} else {
						self.removeClass('btn-default').addClass(`btn-primary`);
						action_target_dom.each(function(__index) {
							if (__index === action_target_dom.length - 1)
								$(this).show({ complete: adjust_view });
							else
								$(this).show();
						});
					}
				});

				context_container_dom.on('change', '[name=cd_visibility]', function() {
					W.cd_set_storage({
						cs_visibility: this.value
					}).then(function() {
						save_costum_setting().then();
					});
				});

				// tracking des actions
				context_container_dom.on('click', '.action-tracking', function(__e) {
					const id = $(this).data('id');
					const tracked = $(this).hasClass('active');
					track_action(!tracked, id);
					update_action_status(id, !tracked ? 'tracking' : 'todo');
				});

				corner_header_dom.on('click', '[data-target=setting]', function() {
					$('[data-target=costum_setting]').trigger('click');
					$(this).fadeOut(_animate_duration);
				});

				let adjust_view_on_resize_timeout = null;
				W.addEventListener('resize', function() {
					if (adjust_view_on_resize_timeout !== null)
						clearTimeout(adjust_view_on_resize_timeout);
					adjust_view_on_resize_timeout = setTimeout(adjust_view, 100);
				});

				// Evenements bootstraps
				const bootstrap_events = [{
						'hidden.bs.collapse': '#action-list-types<?= $blockKey ?>'
					},
					{
						'shown.bs.collapse': '#action-list-types<?= $blockKey ?>'
					}
				];
				$.each(bootstrap_events, function(__, __element) {
					const event = Object.keys(__element)[0];
					$.each([corner_body_dom, context_container_dom], function(__, __dom) {
						__dom.on(event, __element[event], function() {
							adjust_view();
						});
					});
				});

				function convertirTimestampEnDate(dateInput) {
					if(!dateInput || dateInput == 'Invalid date') return "";
					let date;
					// Vérifier si l'entrée est un nombre (timestamp Unix)
					if (!isNaN(dateInput)) {
						date = new Date(dateInput.toString().length === 10 ? dateInput * 1000 : dateInput);
					} else if(typeof dateInput === 'string') {
						date = new Date(dateInput);
						console.log("date",date)
					}

					// Vérifier si la date est valide
					if (isNaN(date.getTime())) {
						return "Date invalide";
					}

					// Formater la date en jj/mm/aa
					const day = String(date.getDate()).padStart(2, '0');
					const month = String(date.getMonth() + 1).padStart(2, '0');
					const year = String(date.getFullYear()).slice(-2);

					return `${day}/${month}/${year}`;
				}

				var kanbanHeader = [{
						id: 'discuss',
						label: 'A discuter',
						editable: false
					}, {
						id: 'todo',
						label: 'A faire',
						editable: false
					},
					{
						id: 'tracking',
						label: 'En cours',
						editable: false
					},
					{
						id: 'totest',
						label: 'A tester',
						editable: false
					},
					{
						id: 'done',
						label: 'Terminer',
						editable: false
					}
				]

				if (typeof window.loadKanbanAction !== 'function') {
					window.loadKanbanAction = function(linkAction, titleAction, costumParams=null) {
						var urlBase = serverUrl;
						var typeCostum = "";
						var idCostum = "";
						var slugCostum = "";

						if ( costumParams !== null ){
							idCostum = costumParams.id;
							typeCostum = costumParams.type;
							slugCostum = costumParams.slug;
						} else {
							if (costum !== null && typeof costum != 'undefined') {
								if (typeof costum.contextType != 'undefined') {
									idCostum = costum.contextId;
									typeCostum = costum.contextType;
									slugCostum = costum.slug;
								}
							}
							if (contextData != null && typeof contextData != 'undefined') {
								idCostum = contextData.id;
								typeCostum = contextData.type;
								slugCostum = contextData.slug
							}
						}
						
						let storageJudi = cd_get_storage();
						if (!storageJudi.active_project)
							return ;
						$('#modalKanbanAction .modal-header h4').text(titleAction)
						var dataAction = []
						var kanbanData = []	

						let paramsKanbanAction = {
							"connectedUser": userConnected._id.$id,
							"filters[urls][$in][]": location.href,
							"filters[parentId]": storageJudi['active_project']['id']
						};

						if( costumParams !== null ){
							paramsKanbanAction["filters[parentId]"] = costumParams.id
						}


						ajaxPost(
							null,
							baseUrl + "/costum/project/action/request/actions/scope/" + linkAction,
							paramsKanbanAction,
							function(data) {
								dataAction = data;
							}, null, null, {
								async: false
							}
						)

						for (let i = 0; i < dataAction.length; i++) {
							let startDate = typeof dataAction[i].startDate == "string" ? convertirTimestampEnDate(dataAction[i].startDate) : ( typeof dataAction[i].startDate == "object" && dataAction[i].startDate != null ?  convertirTimestampEnDate(dataAction[i].startDate['sec']) : convertirTimestampEnDate(dataAction[i].created));
							let endDate = typeof dataAction[i].endDate == "string" ? convertirTimestampEnDate(dataAction[i].endDate) : ( typeof dataAction[i].endDate == "object" && dataAction[i].endDate != null ?  convertirTimestampEnDate(dataAction[i].endDate['sec']) : '');
							if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "done") {
								let actionsItem = {
									_id: dataAction[i].id,
									id: dataAction[i].id + i,
									instanceIdentity: dataAction[i].id,
									title: '<b>' + dataAction[i].name + '</b>',
									name: dataAction[i].name,
									tags: (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
									header: "done",
									html: true,
									canMoveCard: true,
									actions: [
										{
											'icon': 'fa fa-trash',
											'bstooltip': {
												'text': "Supprimer l'action",
												'position': 'left'
											},
											'action': 'onRemoveAction'
										},
										{
											'badge' : startDate + ' - ' + endDate,
										}
								]
								}
								kanbanData.push(actionsItem)
							} else if (typeof dataAction[i].tags != "undefined" && dataAction[i].tags.indexOf('totest') > -1) {
								let actionsItem = {
									_id: dataAction[i].id,
									id: dataAction[i].id + i,
									instanceIdentity: dataAction[i].id,
									title: '<b>' + dataAction[i].name + '</b>',
									name: dataAction[i].name,
									tags: (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
									header: "totest",
									html: true,
									canMoveCard: true,
									actions: [
										{
											'icon': 'fa fa-trash',
											'bstooltip': {
												'text': "Supprimer l'action",
												'position': 'left'
											},
											'action': 'onRemoveAction'
										},
										{
											'badge' : startDate + ' - ' + endDate,
										}
								]
								}
								kanbanData.push(actionsItem)
							} else if (typeof dataAction[i].tracking != "undefined" && (dataAction[i].tracking == true || dataAction[i].tracking == 'true')) {
								let actionsItem = {
									_id: dataAction[i].id,
									id: dataAction[i].id + i,
									instanceIdentity: dataAction[i].id,
									title: '<b>' + dataAction[i].name + '</b>',
									name: dataAction[i].name,
									tags: (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
									header: "tracking",
									html: true,
									canMoveCard: true,
									actions: [
										{
											'icon': 'fa fa-trash',
											'bstooltip': {
												'text': "Supprimer l'action",
												'position': 'left'
											},
											'action': 'onRemoveAction'
										},
										{
											'badge' : startDate + ' - ' + endDate,
										}
								]
								}
								kanbanData.push(actionsItem)
							} else if (typeof dataAction[i].tags != "undefined" && dataAction[i].tags.indexOf('discuter') > -1) {
								let actionsItem = {
									_id: dataAction[i].id,
									id: dataAction[i].id + i,
									instanceIdentity: dataAction[i].id,
									title: '<b>' + dataAction[i].name + '</b>',
									name: dataAction[i].name,
									tags: (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
									header: "discuss",
									html: true,
									canMoveCard: true,
									actions: [
										{
											'icon': 'fa fa-trash',
											'bstooltip': {
												'text': "Supprimer l'action",
												'position': 'left'
											},
											'action': 'onRemoveAction'
										},
										{
											'badge' : startDate + ' - ' + endDate,
										}
								]
								}
								kanbanData.push(actionsItem)
							} else if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "todo") {
								let actionsItem = {
									_id: dataAction[i].id,
									id: dataAction[i].id + i,
									instanceIdentity: dataAction[i].id,
									title: '<b>' + dataAction[i].name + '</b>',
									name: dataAction[i].name,
									tags: (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
									header: "todo",
									html: true,
									canMoveCard: true,
									actions: [
										{
											'icon': 'fa fa-trash',
											'bstooltip': {
												'text': "Supprimer l'action",
												'position': 'left'
											},
											'action': 'onRemoveAction'
										},
										{
											'badge' : startDate + ' - ' + endDate,
										}
								]
								}
								kanbanData.push(actionsItem)
							}
						}

						let kanbanDom = $('#kanbanActionContainer')
							.kanban('destroy')
							.kanban({
								headers: kanbanHeader,
								data: kanbanData,
								editable: false,
								language: mainLanguage,
								canAddCard: true,
								editable: false,
								canAddColumn: false,
								canEditCard: false,
								canEditHeader: true,
								canMoveCard: true,
								canMoveColumn: true,
								readonlyHeaders: [],
								copyWhenDragFrom: [],
								endpoint: `${baseUrl}/plugins/kanban/`,
								defaultColumnMenus: [''],
								onRenderDone() {

								},
								onCardClick(action) {
									mylog.log(action)
									if (notEmpty(userConnected)) {
										var self = $(this);
										var actionModalDom = $('#corner-action-preview').empty().html('');
										if (typeof action.isClickable === 'undefined' || action.isClickable) {
											action.isClickable = false;
											$('*').css('cursor', 'wait');
											self.data('datum', action);
											var url = baseUrl + '/costum/project/action/request/action_detail_html/mode/r';
											var id = action._id;
											var post = {
												id: id
											};
											setTimeout(function() {
												ajaxPost(null, url, post, function(html) {
													actionModalDom.off('shown.bs.modal').on('shown.bs.modal', '.modal', function() {
														action.isClickable = true;
														self.data('datum', action);
													});
													actionModalDom.html(html);
												}, null, 'text');
											});
										}
									}
								},
								onRemoveAction: function(action, cardDom) {
									$.confirm({
										title: trad['Archive'],
										type: 'orange',
										content: trad['Please confirm archiving'],
										buttons: {
											no: {
												text: trad.no,
												btnClass: 'btn btn-default'
											},
											yes: {
												text: trad.yes,
												btnClass: 'btn btn-warning',
												action: function() {
													var params = {
														id: action._id,
														collection: 'actions',
														path: 'status',
														value: 'disabled'
													};

													dataHelper.path2Value(params, function(response) {
														if (response.result) {
															var index = kanbanDom.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(cardDom);
															kanbanDom.kanban('deleteData', {
																column: action.header,
																id: action.id
															});
															toastr.success('Action archiver avec succes', {
																closeButton: true,
																preventDuplicates: true,
																timeOut: 3000
															});
														}
													}, path2ValueOption);
												}
											}
										}
									});
								},
								onCardInsert: function (created) {
									console.log('created', created)
									created = $.extend({}, created);
									var post = {
										name: created.title,
										status: created.header,
										parentId: storageJudi['active_project']['id'],
										parentType: 'projects',
										component: 'kanban',
										serverUrl: serverUrl
									};
									if (wsCO && wsCO.id)
										post.emiter = wsCO.id;
									coInterface.actions.request_create_action(post).then(function (response) {
										if(linkAction == 'mine') {
											var updateCOntrib =  {
												id : response.data._id.$id,
												collection : 'actions',
												path : 'links.contributors.'+userConnected._id.$id,
												value : {"type" : "citoyens", "isAdmin" : true}
											}
											dataHelper.path2Value(updateCOntrib, function(resp) {
												if (resp.result) {
												
												}
											});

										}
										// send create notification
										var id_at_kanban = created.id;
										created.id = response.data._id.$id;
										created._id = response.data._id.$id;
										created.actions = [{
											icon: 'fa fa-trash',
											bstooltip: { text: trad.delete, position: 'top' },
											action: 'onRemoveAction',
											bstooltip: { text: trad['Archive'], position: 'top' }
										},
										{
											'badge' : convertirTimestampEnDate(response.data.created)+' - '
										}
										];
										$('#kanbanActionContainer').kanban('setData', { column: created.header, id: id_at_kanban, data: created });
										coInterface.actions.update_action_position({
											action: created.id,
											group: created.header,
											emiter: wsCO && wsCO.id ? wsCO.id : null,
											component: 'kanban',
											server_url: baseUrl
										}).then();
									}).catch(function (error) {
										toastr.error(coTranslate(error.data));
									});
								},
								onCardDrop(dropped, updates) {
									if (dropped.origin != dropped.target) {
										let path2ValueUrl = {};
										path2ValueUrl.url = urlBase;
										let actionObjet = {
											id: dropped.data.instanceIdentity,
											collection: 'actions',
											costumSlug: slugCostum,
											costumEditMode: 'false'
										};
										let resetObjet = {
											id: dropped.data.instanceIdentity,
											collection: 'actions',
											costumSlug: slugCostum,
											costumEditMode: 'false'
										};
										if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
											actionObjet['value'] = 'true'
											actionObjet['path'] = 'tracking'
											actionObjet['setType'] = 'boolean'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);
											resetObjet['value'] = 'todo'
											resetObjet['path'] = 'status'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
											actionObjet['value'] = 'true'
											actionObjet['path'] = 'tracking'
											actionObjet['setType'] = 'boolean'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);
											let index = dropped.data.tags.indexOf('totest')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'discuss') {
											actionObjet['value'] = 'true'
											actionObjet['path'] = 'tracking'
											actionObjet['setType'] = 'boolean'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);
											let index = dropped.data.tags.indexOf('discuter')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking') {
											actionObjet['value'] = 'true'
											actionObjet['path'] = 'tracking'
											actionObjet['setType'] = 'boolean'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes');
												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
											dropped.data.tags.push('totest')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'todo'
											resetObjet['path'] = 'status'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'discuss') {
											dropped.data.tags.push('totest')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('discuter')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
											dropped.data.tags.push('totest')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'false'
											resetObjet['path'] = 'tracking'
											resetObjet['setType'] = 'boolean'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'totest') {
											dropped.data.tags.push('totest')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes');
												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'discuss' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
											dropped.data.tags.push('discuter')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'todo'
											resetObjet['path'] = 'status'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'discuss' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
											dropped.data.tags.push('discuter')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'false'
											resetObjet['path'] = 'tracking'
											resetObjet['setType'] = 'boolean'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'discuss' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
											dropped.data.tags.push('discuter')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('totest')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'discuss') {
											dropped.data.tags.push('discuter')
											actionObjet['value'] = dropped.data.tags
											actionObjet['path'] = 'tags'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes');
												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
											actionObjet['value'] = 'done'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('totest')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'discuss') {
											actionObjet['value'] = 'done'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('discuter')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
											actionObjet['value'] = 'done'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes');
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'false'
											resetObjet['path'] = 'tracking'
											resetObjet['setType'] = 'boolean'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'done') {
											actionObjet['value'] = 'done'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);

										} else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
											actionObjet['value'] = 'todo'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('totest')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'discuss') {
											actionObjet['value'] = 'todo'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											let index = dropped.data.tags.indexOf('discuter')
											dropped.data.tags.splice(index, 1)
											resetObjet['value'] = dropped.data.tags
											resetObjet['path'] = 'tags'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
											actionObjet['value'] = 'todo'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);

											resetObjet['value'] = 'false'
											resetObjet['path'] = 'tracking'
											resetObjet['setType'] = 'boolean'
											dataHelper.path2Value(resetObjet, function(resp) {
												if (resp['result']) {

												}
											}, path2ValueUrl);
										} else if (typeof dropped.target != 'undefined' && dropped.target == 'todo') {
											actionObjet['value'] = 'todo'
											actionObjet['path'] = 'status'
											dataHelper.path2Value(actionObjet, function(resp) {
												if (resp['result']) {
													toastr.success('Action deplacer avec succes', {
														closeButton: true,
														preventDuplicates: true,
														timeOut: 3000
													});
												}
											}, path2ValueUrl);
										}
										mylog.log(dropped, updates, 'data drop')
									}
								}
							})
						let htm = `<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>`
						$('#modalKanbanAction .modal-footer').html(htm)
					}
				}

				// Détecter le changement d'URL
				setInterval(function() {
					if (_current_location !== W.location.href) {
						_current_location = W.location.href;
						if (turn_back_dom.is(':visible')) turn_back_dom.trigger('click');
						else init_view().then(load_badgets);
						cd_int({
							server_url: serverUrl
						});
					}
				});
				init_view();
				cd_int({
					server_url: serverUrl
				});
				var socket_interval = setInterval(function() {
					if (wsCO !== null) {
						clearInterval(socket_interval);
						init_socket({
							wsCO: wsCO,
							io: io,
							coWsConfig: coWsConfig
						});
					}
				});
			});
		})(jQuery, window);
	</script>
<?php
} ?>