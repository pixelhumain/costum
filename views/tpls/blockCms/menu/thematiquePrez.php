<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style>
  body {
    margin: 0;
    font-family: Arial, sans-serif;
    background-color: #f5f5f5;
    overflow-x: hidden;
  }

  .<?= $kunik ?> .container-text {
    width: 100%;
    overflow: hidden;
    padding: 20px 0;
  }

  .<?= $kunik ?> .scrolling-text {
    display: inline-block;
    white-space: nowrap;
    font-size: 3em;
    color: #333;
  }

  .<?= $kunik ?> .container {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    padding: 10px;
    gap: 10px;
  }

  .<?= $kunik ?> .theme-card {
    position: relative;
    flex: 1 1 23%;
    height: 300px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    padding: 20px;
    color: #021120;
    font-size: 24px;
    font-weight: bold;
    box-sizing: border-box;
    margin-bottom: 10px;
    cursor: pointer;
    font-family: Averia_Light;
  }

  .<?= $kunik ?> .theme-card h2 {
    position: absolute;
    top: 20px;
    left: 20px;
    font-size: 6em;
    color: rgba(0, 0, 0, 0.1);
    z-index: 1;
    opacity: 0.08;
    font-family: 'Arial';
  }

  .<?= $kunik ?> .theme-card .thematic {
    font-size: 1.8em;
    z-index: 2;
    margin: 0;
    padding-left: 20px;
    line-height: 1.4em;
    white-space: nowrap;
    color: #04353f;
    font-family: Averia_Light;
  }

  .<?= $kunik ?> .theme-card .description {
    padding-left: 20px;
    line-height: 1em;
    text-align: left;
    font-size: 0.9em;
    font-weight: 100;
    margin-left: 15px;
  }

  .<?= $kunik ?> .count {
    position: absolute;
    bottom: 10px;
    right: 47px;
    font-size: 2em;
    font-weight: bold;
    color: #333;
    opacity: 0.6;
  }

  @media (max-width: 1200px) {
    .<?= $kunik ?> .theme-card {
      flex: 1 1 48%; 
    }
  }

  @media (max-width: 900px) {
    .<?= $kunik ?> .theme-card {
        flex: 1 1 100%;
    }
  }

  .<?= $kunik ?> .container {
    -ms-overflow-style: none; 
    scrollbar-width: none;
  }

  .<?= $kunik ?> .container::-webkit-scrollbar {
      display: none;
  }

  .<?= $kunik ?> .texte-special {
    font-family: sans-serif;
    font-size: 5em;
    font-weight: bold;
    color: #6A0DAD; 
    letter-spacing: 5px;
    line-height: 1;
    padding-top: 2%;
  }

  .<?= $kunik ?> .texte-special::before,
  .<?= $kunik ?> .texte-special::after {
    content: "";
    display: inline-block;
    height: 5px;
    width: 200px;
    background-color: #6A0DAD;
    vertical-align: middle;
  }

  .<?= $kunik ?> .baner-titre {
    background-color: #F5F5F5;
  }

  .<?= $kunik ?> .baner-titre h3, h1{
    text-transform: none;
  }
  .<?= $kunik ?> h1 {
    margin-left: 1%;
  }

  .<?= $kunik ?> h1 i{
    font-size: 24px;
  }

  .<?= $kunik ?> .plus_thematic {
    border-bottom: 1px solid black;
    border-top: 1px solid black;
    margin: 2%;
    height: 80px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.<?= $kunik ?> .plus_thematic .title-container {
    flex-grow: 1;
    display: flex;
    justify-content: center;
}

.<?= $kunik ?> .plus_thematic h1 {
    text-transform: none;
    font-size: 4em;
    margin: 0;
}

.<?= $kunik ?> .plus_thematic i {
    font-size: 60px;
    margin-right: 20px;
    cursor: pointer;
}
.<?= $kunik ?> .plus_thematic i:hover {
  color: #a600b6;
  font-size: 65px;
}
.<?= $kunik ?> .div-mer {
  height: auto;
  width: 100%;
  padding: 3%;
  background-image: url("https://www.gasigasy.mg/wp-content/uploads/2024/01/big_artfichier_243973_6754908_201701033937554201-726x433.jpg");
  background-repeat: no-repeat;
  background-size: cover;
}

.<?= $kunik ?> .div-mer .text-decoratif {
  margin-left: 25%;
  margin-right: 25%;
  padding: 2%;
  background-color: #7a00af;
}

.<?= $kunik ?> .div-mer .text-decoratif h1 {
  font-size: 5em;
  text-align: center;
  color: white !important;
}


/*  */
  .<?= $kunik ?> .container-filieres {
    width: 100%;
    overflow: hidden;
    position: relative;
    padding: 20px 0;
    background: #6aedce;
  }

  .<?= $kunik ?> .logo-page-filieres {
    display: flex;
    animation: scroll 60s linear infinite;
    gap: 20px;
    padding: 0 10px;
  }

  .<?= $kunik ?> .logo-page-filieres:hover {
    animation-play-state: paused;
  }

  .<?= $kunik ?> .filiere-item {
    flex: 0 0 auto;
    width: 160px;
    height: 150px; 
  }

  .<?= $kunik ?> .filiere-item img {
    width: 100%;
    height: 100%;
    object-fit: contain;
    border-radius: 8px;
  }

  @keyframes scroll {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(calc(-200px * (var(--total-items) / 2)));
    }
  }

  .<?= $kunik ?> .filiere-item {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 150px;
    cursor: pointer;
    margin: 10px;
    text-align: center;
  }

  .<?= $kunik ?> .filtere-item-text {
    border: 1px solid #ccc; 
    background-color: #f9f9f9; 
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
    border-radius: 8%; 
  }

  .<?= $kunik ?> .filiere-item img {
    max-width: 100%;
    max-height: 100%;
  }

  .<?= $kunik ?> .filiere-item span {
    display: inline-block;
    font-size: 18px;
    font-weight: bold;
    text-transform: uppercase; 
    letter-spacing: 2px; 
    color: #333; 
    padding: 10px;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .<?= $kunik ?> .inclined-button {
    margin-top: 25px;
    margin-bottom: 25px;
    margin-left: 41%;
    padding: 3px;
    width: 18%;
    font-size: 2em;
    color: black;
    background-color: #9fe200;
    border: 2px solid #93c020;
    /* border-radius: 5px; */
    cursor: pointer;
    transform: rotate(-10deg);
    transition: transform 0.3s ease;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2);
    justify-content: center;
    align-items: center;
    text-align: center;
    flex-direction: column;
    word-wrap: break-word;
  }

  .<?= $kunik ?> .inclined-button:hover {
    transform: rotate(0deg);
    transition: transform 0.3s ease;
  }

  .<?= $kunik ?> .inclined-button:hover::after {
    transform: rotate(-10deg);
    transition: transform 0.3s ease 0.3s;
  }

  .bg-green {
    background-color: #2c3e50 !important;
  }
  .loader-loader<?= $kunik ?> {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 42%;
    overflow: hidden;
    -webkit-overflow-scrolling: touch;
    outline: 0;
    z-index: 9999999999999999 !important;
    padding-top: 180px !important;
      
  }
  .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
    width: auto;
    height: auto;
  }
  .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
    width : auto !important;
    height : auto !important;
  }
  .backdrop-loader<?= $kunik ?> {
    position: fixed;
    opacity: .8;
    background-color : #000;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height : 100vh;
    z-index: 999999999 !important;
  }

  .<?= $kunik ?> #filters-container {
    text-align: left;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
    width: 100%;
    z-index: 1111;
    background-color: white;
    height: auto !important;
    position: fixed;
  }

  .<?= $kunik ?> #div-results-filters{
    padding: 60px 0px 50px 0px;
    position: relative;
    display: block;
    top: 25px;
  }

  .<?= $kunik ?> #input-sec-search {
    display: grid;
    margin-bottom: 5px;
    float: left;
  }

  .<?= $kunik ?> #input-sec-search .shadow-input-header {
    display: inline-flex;
    height: 40px;
  }

  .<?= $kunik ?> #input-sec-search .input-group-addon {
    color: #9fbd38;
    border: 1px solid #9fbd38;
    border-right-width: 0px;
    padding: 10px 0px 10px 10px;
    font-size: 18px;
    font-weight: 400;
    line-height: 1;
    color: #6b6b6b;
    margin-top: 5px;
    text-align: center;
    background-color: white !important;
    border-right-width: 0px;
    border: 1px solid #6b6b6b;
    border-radius: 20px 0px 0px 20px;
    border-right-width: 0px;
    width: 30px;
    height: 40px;
  }

  .<?= $kunik ?> #input-sec-search .input-global-search {
    border-left-width: 0px;
    height: 40px;
    margin-top: 5px;
    border-radius: 0px 20px 20px 0px;
    box-shadow: none;
    width: inherit;
    border-color: #053c5a !important;
  }
  .<?= $kunik ?> .dropdown {
    margin-bottom: 5px;
    margin-top: 5px;
    margin-right: 10px;
    padding-top: 0px;
    padding-bottom: 0px;
    display: inline-block;
    /* float: left; */
  }
  .<?= $kunik ?> .dropdown-toggle{
    padding: 10px 15px;
    font-size: 16px;
    border: 1px solid #6b6b6b;
    color: #6b6b6b;
    top: 0px;
    position: relative;
    border-radius: 20px;
    text-decoration: none;
    background: white;
    line-height: 20px;
    display: inline-block;
    margin-left: 5px;
  }

  .<?= $kunik ?> .dropdown-menu {
    position: absolute;
    overflow-y: visible !important;
    top: 50px;
    left: 5px;
    width: 250px;
    border-radius: 2px;
    border: 1px solid #6b6b6b;
    padding: 0px;
  }

  .<?= $kunik ?> .dropdown-menu .list-filters {
    max-height: 300px;
    overflow-y: scroll;
  }

  .<?= $kunik ?> .dropdown-menu .list-filters .btn {
    border: none;
    background: white;
    text-align: left;
    font-size: 14px;
  }

  .filter-btn-hide-container {
    position: absolute;
    top: 90px;
    right: 20px;
    z-index: 9999;
    border-radius: 100%;
    width: 40px;
    height: 40px;
    font-size: 20px;
    border: 2px solid red;
    outline: none;
    background: red;
    /* color: white; */
    justify-content: center;
    align-items: center;
    transition: .3s;
  }

  .filter-btn-hide-container i {
    color: white !important;
  }

  .filter-btn-hide-container:hover i {
    color: red !important;
  }

  .filter-btn-hide-container i:hover {
    color: red !important;
  }
  .filters-container-count {
    position: absolute;
    top: 51px;
    width: 100%;
    z-index: 1;
  }

  .main-container .btn-show-map {
    width: 150px !important;
    margin: 5px 3px;
  }

  #mapContent {
    top: 135px !important;
  }
</style>
<div class="<?= $kunik ?>">
  <div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
		<div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
	</div>
	<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>
  <div class="filters-div hidden">
    <div id="filters-container"></div>
    <div class="filters-container-count"></div>
    <div id="div-results-filters"></div>
  </div>
  <div class="container-div">
    <div class="baner-titre text-center">
      <div class="texte-special"> Thématiques & Filières </div>
      <h3>
        Façonnez votre territoire en explorant diverses thématiques.
      </h3>
      <div class="container-text">
        <div class="scrolling-text">
          <marquee behavior="" direction="">Chaque territoire est unique et porte en lui une richesse de savoirs, d’expériences et de dynamiques locales. En explorant différentes thématiques, vous avez l’opportunité de structurer et de valoriser votre espace selon vos besoins et vos aspirations.</marquee>
        </div>
      </div>
    </div>
    <h1>Tous les thématiques <i class="fa fa-arrow-down"></i></h1>
    <div class="container" id="thematicContainer"></div>
    <div class="plus_thematic">
      <div class="title-container">
          <h1>Plus de thématique</h1>
      </div>
      <i class="fa fa-plus" title="Ajouter une nouvelle thématique"></i>
    </div>
    <div class="div-mer">
      <div class="text-decoratif">
        <h1>Plongez dans notre catalogue de filières : des chemins tout tracés vers votre réussite  </h1>
      </div>
    </div>
    <div class="container-filieres">
      <h1>Tous les pages filières <i class="fa fa-arrow-down"></i></h1>
      <div class="logo-page-filieres">

      </div>
      <!-- <button class="inclined-button" id="btn-new-pageFiliere">Initialiser une page Filière <i class="fa fa-arrow-right"></i> </button> -->
    </div>
    <div class="baner-titre text-center">
      <div class="texte-special"> Thématiques & Filières </div>
      <h3>
        Engagez-vous dans la dynamique citoyenne des initiatives communautaires et solidaires
      </h3>
      <div class="container-text">
        <div class="scrolling-text">
          <marquee behavior="" direction="">Nous sommes des citoyen·nes, des acteurs du changement et des promoteurs de l'économie sociale et solidaire. Ensemble, nous aspirons à bâtir un avenir où chaque voix compte et où les projets communs prennent vie. Notre mission est de valoriser la collaboration et l'innovation pour créer des solutions durables qui répondent aux besoins de notre communauté. Rejoignez-nous pour transformer nos idées en actions et contribuer à un monde plus juste et inclusif.</marquee>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var allThematics = <?php echo json_encode($allThematics) ?>;
  var pagesFilies = <?php echo json_encode($pagesFilies) ?>;
  
  var colors = [
    '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', 
    '#e67e22', '#f1c40f', '#16a085', '#27ae60', '#2980b9',
    '#8e44ad', '#c0392b', '#d35400', '#f39c12', '#3083D6'
  ];

  var defaultThem = [
    "Alimentation","Associations","Culture","Éducation","Santé","Citoyenneté","Économie",
    "Tiers lieux","Transport","Énergie","Sport","Déchets","Environnement","Numérique","Agriculture",
    "Justice","Spiritualité","Science","Communication","Innovations"
  ]

  var thematiques = [
    { thematique: "Alimentation", description: "Toutes les initiatives liées à l'alimentation durable et responsable." },
    { thematique: "Associations", description: "Les actions des associations locales et leurs projets solidaires." },
    { thematique: "Culture", description: "Les événements et projets culturels dans la communauté." },
    { thematique: "Éducation", description: "Les initiatives et projets éducatifs pour tous les âges." },
    { thematique: "Santé", description: "Les programmes et services visant à améliorer la santé publique." },
    { thematique: "Citoyenneté", description: "Encourager la participation citoyenne et l'engagement dans la société." },
    { thematique: "Économie", description: "Les projets et innovations économiques durables et locales." },
    { thematique: "Tiers lieux", description: "Espaces collaboratifs et partagés pour l'innovation et la rencontre." },
    { thematique: "Transport", description: "Les initiatives pour améliorer les transports durables." },
    { thematique: "Énergie", description: "Les projets autour des énergies renouvelables et de la transition énergétique." },
    { thematique: "Sport", description: "Les événements sportifs et les initiatives pour la santé et le bien-être." },
    { thematique: "Déchets", description: "Les actions pour la gestion et la réduction des déchets." },
    { thematique: "Environnement", description: "Les projets pour protéger et restaurer l'environnement." },
    { thematique: "Numérique", description: "Les innovations dans le domaine du numérique pour tous." },
    { thematique: "Agriculture", description: "Les initiatives liées à l'agriculture durable et locale." },
    { thematique: "Justice", description: "Les projets autour de la justice sociale et des droits humains." },
    { thematique: "Spiritualité", description: "Les pratiques spirituelles et initiatives pour le bien-être intérieur." },
    { thematique: "Science", description: "Les découvertes et projets scientifiques à l'échelle locale." },
    { thematique: "Communication", description: "Les innovations dans les médias et la communication au service de la communauté." },
    { thematique: "Innovations", description: "Les nouvelles idées et projets pour un futur durable et innovant." }
  ];

  function listThematics() {
    var thematicContainer = document.getElementById('thematicContainer');
    var thematicArray = Object.values(allThematics);

    thematicArray.forEach((themeObj, index) => {
      var card = document.createElement('div');
      card.classList.add('theme-card');

      var number = document.createElement('h2');
      number.textContent = index + 1;

      var title = document.createElement('p');
      title.classList.add('thematic');

      title.textContent = themeObj.thematic;

      var foundThematic = thematiques.find(thematiqueObj => thematiqueObj.thematique.toLowerCase() === themeObj.thematic.toLowerCase());

      var description = document.createElement('p');
      description.classList.add('description');
      description.textContent = foundThematic ? foundThematic.description : "Description non disponible";

      var count = document.createElement('p');
      count.classList.add('count');
      count.textContent = themeObj.count;

      card.appendChild(number);
      card.appendChild(title);
      card.appendChild(description);
      card.appendChild(count);

      card.style.backgroundColor = colors[index % colors.length];

      var img_name = themeObj.thematic.toLowerCase().replaceAll('é','e');
      
      card.style.backgroundImage = `url('${modules.costum.url}/images/cocity/icones_filieres/i_${img_name}.png')`;
      card.style.backgroundSize = "200px";
      card.style.backgroundPosition = "bottom right";
      card.style.backgroundRepeat = "no-repeat";

      thematicContainer.appendChild(card);

      $(".theme-card").on('click', function(e){
        e.stopImmediatePropagation();
        getDataFilters(themeObj.thematic);
        $(".container-div").addClass('hidden');
        $(".filters-div").removeClass('hidden');
      });
    });
  }
  
  function createCarouselItems(track) {
    $.each(pagesFilies, function(key, value) {
      let urlImage = "";
      let name = value.name;
      
      if (typeof value.costum.logo !== "undefined") {
        urlImage = value.costum.logo;

        var item = document.createElement('div');
        item.className = 'filiere-item';

        var link = document.createElement('a');
        var href = "#";
        if(typeof value.slug != "undefined")
          href = `<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/${value.slug}`;

        link.href = href;
        link.target = '_blank';

        var img = new Image();
        img.src = urlImage;
        img.alt = name;

        img.onload = function() {
            item.appendChild(img);
        };

        img.onerror = function() {
          item.className = 'filiere-item filtere-item-text';
          var nameElement = document.createElement('span');
          nameElement.textContent = name;
          item.appendChild(nameElement);
        };

        link.appendChild(item);

        track.appendChild(link);
      }
    });
  }

  function listFilieres() {
      var track = document.querySelector('.logo-page-filieres');
      let totalItems = 0;

      $.each(pagesFilies, function(key, value) {
        if(typeof value.costum.logo !== "undefined") {
          totalItems++;
        }
      });
      
      track.style.setProperty('--total-items', totalItems);

      createCarouselItems(track);
  }
  var check_them = "";
  function getDataFilters(thematic) {
    check_them = thematic;
    var paramsFilter = {
      header : {
        dom : ".filters-container-count",
        options : {
          left : {
            classes : 'col-xs-4 elipsis no-padding',
            group:{
              count : true
            }
          },
          right : {
            classes : 'col-xs-8 text-right no-padding',
            group : {
                map : true
            }
          }
        }
      },
		  container : "#filters-container",
      defaults : {
        notSourceKey:true,
        indexStep : 0,
        filters : {
            "thematic" : 	thematic,
            "costum.typeCocity": { 
              $exists: true 
            },
            "costum.cocity": { 
              $exists: false 
            }
        },
        types : ["events",  "organizations", "poi", "projects","classifieds","citoyens","ressources"]
      },
      results : {
        dom : "#div-results-filters",
        smartGrid : true,
        renderView : "elementPanelHtmlCard"
      },
      loadEvent:{
        default:"scroll"
      },
      filters : {
        text : true,
        scope : true,
        types  : {
          lists : ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative" ,"projects", "citoyens", "poi","classifieds","ressources","events"]
        }
      }
    };

    $(".filters-container-count").empty();
		filterSearch = searchObj.init(paramsFilter);
    filterSearch.search.init(filterSearch);

    var myDiv = document.getElementById('filters-container');
    var button = document.createElement('button');

    button.type = 'button';
    button.className = 'filter-btn-hide-container';
    button.id = 'hidenMap';

    var icon = document.createElement('i');
    icon.className = 'fa fa-times';
    icon.setAttribute('aria-hidden', 'true');

    button.appendChild(icon);

    button.style.position = 'absolute';
    button.style.top = '50%';
    button.style.right = '0';
    button.style.transform = 'translateY(-50%)';

    myDiv.appendChild(button);

    $(".filter-btn-hide-container").on('click', function(){
      $(".filters-div").addClass('hidden');
      $(".container-div").removeClass('hidden');
    });
  }

  var elementPanelHtmlCard =  function (params) {
		var str = '';
    var name_img = check_them.toLowerCase().replaceAll('é','e');
		str = `
		<div id="entity_${ params.collection}_${params.id}" class="col-lg-2 col-md-3 col-sm-4 col-xs-12 searchEntityContainer ${params.containerClass} click-element" data-href="${params.hash}" data-type-elt="${params.collection}" data-class="${params.hashClass}">
			<div class="small-card item-slide">
				<div class="icon-elm bg-${params.color}">
					<span><img src="${modules.costum.url}/images/cocity/icones_filieres/i_${name_img}.png" alt="" style="width:100%"></span>
				</div>
				<div class="block-card-img">
					<a href="${params.hash }" class=" ${params.hashClass}">
						<div class="card-img-container " >
							${params.imageProfilHtml}					
						</div>
					</a>
					<div class="block-card-body">						
						<h4 class="entityName"><a href="${params.hash }" class=" ${params.hashClass}">${params.name}</a></h4>`					
					str+= `</div>`
				str+= `</div>`
				str+= ` <div class="block-card-hover co-scroll">
							<div class="text-wrap">
								<h4 class="entityName"> <a href="${params.hash}" class="${params.hashClass}">${params.name}</a> </h4>`
								str+= directory.typeConfigAppend (params.type);
								if (notEmpty(params.statusLinkHtml))
									str += params.statusLinkHtml;

								str+= directory.roleConfigAppend (params.rolesHtml);

								if (typeof params.edit != 'undefined' && notNull(params.edit))
									str += directory.getAdminToolBar(params);
								if(params.id == userId && typeof params.rolesAndStatusLink != "undefined" && typeof params.rolesAndStatusLink["isInviting"] != "undefined" && typeof params.rolesAndStatusLink["invitorId"] != "undefined" && params.rolesAndStatusLink["invitorId"] != userId){
									str += directory.getUserInvitToolBar(params);
								}
								str+= directory.localityConfigAppend (params.localityHtml);
								str += directory.countLinksHtml(params);
								str += directory.socialToolsConfigAppend (params);
							str+= `</div>` 
					str += `</div>`  
			str += `</div> 
			</div> `;
		return str;
	}

  $(document).ready(function(){
    listFilieres();
    listThematics();
    coInterface.showLoader('#content-loader<?= $kunik ?>');
    var formInitpage = {
      "jsonSchema" : {
        "title" : "Initialiser votre page Filières",
        "description" : "Initialiser votre ville et le thématique pour la nouvel coctume",
        "properties" : {
          "filiereName" : {
            "inputType" : "tags",
            "maximumSelectionLength" : 1,
            "label" : "Choisis une thématique pour votre Filières",
            "values" : defaultThem,
            "value" : [],
            "rules" : {
              "required" : true
            }
          },
          "formLocality" : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality, {required: true})
        },
        afterBuild: function() {
          $("#btn-submit-form").html(`Valider la ville
            <i class="fa fa-arrow-circle-right"></i>`);
        },
        save: function(response) {
          var nameFiliere = response.filiereName;
          delete response.filiereName;
          dyFObj.closeForm();
          $('#loader-loader<?= $kunik ?>').removeClass('hidden');	
					$('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
          var dyfOrga = {
            "beforeBuild" : {
              "properties" : {
                "name" : {
                  "label" : "Nom de votre page Filière",
                  "inputType" : "text",
                  "placeholder" :"Nom de votre page Filière",
                  "value" : "",
									"order" : 1,
                  "rules" : {
                    "required" : true
                  }
                },
                "thematic":{
                  "inputType" : "tags",
                  "label" : "Choisis une thématique pour votre Filières",
                  "values" : defaultThem,
                  "value" : nameFiliere,
                  "order" : 2,
                  "rules" : {
                    "required" : true
                  }
                }
              }
            },
            "onload" : {
              "actions" : {
                "setTitle" : "Creer votre page Filière",
                "src" : {
                  "infocustom" : "<br/>Remplir le champ"
                },
                "hide" : {
                  "formLocalityformLocality" : 1,
                  "locationlocation" : 1
                } 
              }
            }
          };
          dyfOrga.afterSave = function(orga) {
            dyFObj.commonAfterSave(orga, function(){
              $('#loader-loader<?= $kunik ?>').removeClass('hidden');
              $('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
              coInterface.showLoader('#content-loader<?= $kunik ?>');
              var data = {
                collection : 'organizations',
                id: orga.id,
                path: 'allToRoot',
                value : {
                  "costum.slug" : "costumize",
                  "costum.typeCocity" : "ville"
                },
              };

              dataHelper.path2Value(data, function(){
                let loadpage = false;
                let themat = nameFiliere.toLowerCase();
                let params = {"thematic" : themat, "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection};
                cocity.importContent(params, loadpage);
              });
            });
          };
          setTimeout(() => {
            response.type = "GovernmentOrganization";
            response.role = "admin";
            dyFObj.openForm('organization',null,Object.assign(response, {}),null,dyfOrga);
            $('#loader-loader<?= $kunik ?>').addClass('hidden');
						$('#backdrop-loader<?= $kunik ?>').addClass('hidden');
          }, 1500);
        }
      }
    }

    $("#btn-new-pageFiliere").on('click', function(){
      dyFObj.openForm(formInitpage);
    })
  });
</script>