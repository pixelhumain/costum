<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>

<style>
    .<?= $kunik ?> .description {
        margin-left: 19%;
        margin-right: 19%;
        margin-top: 2%;
        text-align: center;
        padding: 1%;
        border: 2px #092434 solid;
        border-radius: 25px;
        font-size: 16px;
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    }

    .<?= $kunik ?> .description_activfilters {
        margin-top: 4% !important;
    }

    .<?= $kunik?> .btn-container {
        margin-top: 2%;
        display: flex;
        justify-content: center;
        align-items: center;
        gap: 10px;
    }

    .<?= $kunik?> .btn-container .button {
        padding: 10px 20px;
        background-color: #092434;
        color: white;
        border-radius: 5px;
        transition: background-color 0.3s, transform 0.1s;
        text-decoration: none;
        display: inline-block;
        text-align: center;
        font-size: 15px;
        height: 38px;
    }

    .<?= $kunik?> .btn-container .button:hover {
        border: 1px #092434 solid;
        color: #092434;
        background-color: white;
    }

    .<?= $kunik?> .btn-container .button:active {
        transform: scale(0.95);
    }

    .bg-vine {
        background-color: #092434 !important;
    }

    .<?= $kunik ?> .div_search_elements {
        text-align: left;
        box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
        width: 100%;
        z-index: 1111;
        background-color: white;
        height: auto;
    }

    .<?= $kunik ?> .div_search_elements_noedit {
        position: fixed;
    }

    .<?= $kunik ?> .dropdown {
        position: relative;
        display: inline-block;
        width: 100%;
        margin: 5px;
        height: 40px;
    }

    .<?= $kunik ?> .dropbtn, .categbtn {
        background-color: transparent;
        color: grey;
        padding: 10px;
        font-size: 15px;
        cursor: pointer;
        width: inherit;
        border-radius: 20px;
        border: 2px solid #092434;
        height: 40px;
    }

    .<?= $kunik ?> .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: calc(100% - 4px); 
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        max-height: 220px;
        overflow-y: auto;
        left: 0; 
    }
    .<?= $kunik ?> .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
        cursor: pointer;
    }

    .show {
        display: block;
    }

    .<?= $kunik ?> .dropdown-content a:hover{
        background-color: #80808075;
    }

    .elements-container {
        display: flex;
        flex-wrap: wrap;
    }

    .content {
        position: relative;
        width: 20%; 
        padding: 15px;
    }

    .item-slide {
        position: relative;
        overflow: hidden;
        border-radius: 10px;
        box-shadow: 0 4px 8px rgba(0,0,0,0.1);
        transition: transform 0.3s ease;
        background-color: rgba(0, 0, 0, 0.6); 
    }

    .item-slide:hover {
        transform: scale(1.05);
    }

    .img-back-card {
        position: relative;
        width: 100%;
        height: 300px; 
        background-size: cover;
        background-position: center;
        transition: background-color 0.3s ease;
    }

    .div-img {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;
        background-color: #f0f0f0;
    }

    .div-img i {
        color: #888;
        font-size: 48px;
    }

    .text-wrap-initial {
        position: absolute;
        bottom: 0;
        width: 100%;
        background: rgba(0, 0, 0, 0.8); 
        color: #fff;
        padding: 10px;
        box-sizing: border-box;
        z-index: 2;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: opacity 0.3s ease;
        display: block;
    }

    .item-slide:hover .text-wrap-initial {
        opacity: 0;
    }

    .text-wrap {
        position: absolute;
        bottom: -100%;
        width: 100%;
        height: 100%; 
        background: rgba(0, 0, 0, 0.8); 
        color: #fff;
        padding: 10px;
        box-sizing: border-box;
        z-index: 2;
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        opacity: 0;
        transition: bottom 0.5s ease, opacity 0.5s ease;
        overflow: auto;
    }

    .text-wrap::-webkit-scrollbar {
        width: 5px; 
    }

    .text-wrap::-webkit-scrollbar-track {
        background: rgba(255, 255, 255, 0.1);
    }

    .text-wrap::-webkit-scrollbar-thumb {
        background-color: rgba(255, 255, 255, 0.5);
        border-radius: 10px; 
    }

    .text-wrap::-webkit-scrollbar-thumb:hover {
        background-color: rgba(255, 255, 255, 0.8); 
    }
    .text-wrap hr {
        border: 0.5;
        height: 1px;
        background: #fff;
        margin: 10px 0;
    }

    .item-slide:hover .text-wrap {
        bottom: 0; 
        opacity: 1;
    }

    .entityName {
        font-size: 1.2em;
        font-weight: bold;
        cursor: pointer;
    }

    .entityType,
    .entityLocality,
    .p-short {
        margin-top: 5px;
        margin-bottom: 15px;
    }

    .p-short {
        font-size: 13px;
    }

    .entityCenter a {
        z-index: 1;
        position: absolute;
    }

    .entityCenter i {
        width: 45px;
        height: 45px;
        font-size: 20px;
        line-height: 40px;
        text-align: center;
        border-radius: 5px 0 21px 0;
        border-right: 2px solid #fff;
        border-bottom: 2px solid #fff;
        color: #fff;
    }

    .<?= $kunik ?> span.btn-tag {
        color: #fff;
        background: transparent;
        border: 1px solid #fff;
    }

    .letter-turq {
        color: #00b5c0 !important;
    }

    .tags-list {
        margin-left: -25px;
    }

    .<?= $kunik ?> .main-map {
        margin-top: 45px;
        height: 630px;
        position: fixed;
        width: 100%;
    }

    .filter-btn-hide-map {
        position: absolute;
        top: 90px;
        right: 20px;
        z-index: 9999;
        border-radius: 100%;
        width: 40px;
        height: 40px;
        font-size: 20px;
        border: 2px solid red;
        outline: none;
        background: red;
        color: white;
        justify-content: center;
        align-items: center;
        transition: .3s;
    }
    .<?= $kunik ?> .btn-link {
        color: #fff;
        background-color: #9fbd38;
        border: 1px solid #fff;
        border-radius: 4px;
        margin: 4px;
        min-width: 105px;
    }

    .<?= $kunik ?> .btn-link:hover {
        border: 1px solid #fff;
    }
</style>
<div class="<?= $kunik ?> catalogue">
    <div class="div_search_elements" id="champ-filters">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12" style="width: 13%">
                <div class="searchBar-filters pull-left">
                    <input type="text" id="main-search-ressources" class="form-control pull-left text-center main-search-bar search-bar" placeholder="Que recherchez-vous ?">
                    <span class="text-white input-group-addon pull-left main-search-bar-addon" >
                        <i class="fa fa-arrow-circle-right"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12" style="width: 10%">
                <div class="dropdown">
                    <button class="dropbtn">Types &nbsp;&nbsp;<i class="fa fa-angle-down"></i></button>
                    <div class="dropdown-content dropdown-content-type" id="typeDropdown">
                        <a data-level="initFilters" data-type="ressources" data-section="offer">Engagement</a>
                        <a data-level="initFilters" data-type="ressources" data-section="need">Don</a>
                        <a data-level="initFilters" data-type="classifieds" data-section="forsale">Vente</a>
                        <a data-level="initFilters" data-type="classifieds" data-section="forrent">Location</a>
                        <a data-level="initFilters" data-type="jobs" data-section="offer">Offre</a>                       
                        <a data-level="initFilters" data-type="jobs" data-section="need">Demande</a>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12" style="width: 12%">
                <div class="dropdown">
                    <button class="categbtn">Catégories &nbsp;&nbsp;<i class="fa fa-angle-down"></i></button>
                </div>
            </div>
            <di class="col-md-3 col-sm-3 col-xs-12" style="width: 39%;">
            </di>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="btn-container">
                    <a href="javascript:;" onclick="dyFObj.openForm('ressources', null, null, null, dyfOrga);" class="button"> <i class="fa fa-cubes"></i> Ajouter une Ressource</a>
                    <button type="button" class="btn-show-map hidden-xs list-affiche" id="showMap">
                        <i class="fa fa-map-marker"></i> &nbsp;&nbsp;&nbsp; Carte
                    </button>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 text-left btn-select-filtre" style="margin-bottom: 5px;">
            </div>
            <div class="col-sm-12 col-xs-12 categorie-filters" id="categorie-filters">
                <div class="dropdown-content dropdown-content-categorie" id="categorieDropdown">

                </div>
            </div>
        </div>
    </div>
    <div class="ressource-container" style="padding: 30px 0px 50px 0px; position: relative; display: block;">
        <div class="description list-affiche">
            <span>
                Cet espace vous propose un répertoire d'annonces entre citoyens, organisations, initiatives et événements. 
                Vous pouvez rechercher l'aide dont vous avez besoin, accéder à divers services ou trouver des ressources locales. 
                Que ce soit pour de l'assistance communautaire, des services de proximité, des ressources éducatives ou culturelles, 
                cet espace est conçu pour faciliter vos recherches et renforcer les liens locaux.
            </span>
        </div>
        <div class="elements-container list-affiche">

        </div>
        <div class="main-map map-affiche hidden" id="map-sources">

        </div>
        <button type="button" class="map-affiche filter-btn-hide-map hidden" id="hidenMap">
            <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
</div>
<script>
    var ressources = <?php echo json_encode($ressources); ?>;
    var _el = <?php echo json_encode($elementOrga); ?>;
    var dyfOrga = {};
    var dataPreviews = [];
    var selectedFilter = {
        type : [],
        section : [],
        category : [],
        subtype : [],
    }

    if(!costum.editMode) {
        $(".div_search_elements").addClass("div_search_elements_noedit")
    }
    var nameSearch = "";

    var mapSource = new CoMap({
        container : "#map-sources",
        activePopUp : true,
        mapOpt:{
            menuRight : false,
            btnHide : false,
            doubleClick : true,
            zoom : 2,
                scrollWheelZoom: false
        },
        mapCustom: {
            tile : "maptiler"
        },
    });

    function listeCatlogue(_data, isfiltre = false, level = "") {
        if(Object.keys(_data).length > 0) {
        
            dataPreviews = _data;
            mapSource.clearMap();
            
            let src = "";
            let filters = {};
            let categorieFilters = [];

            mapSource.addElts(_data);

            $.each(_data, function(key, value){
                let imgContent = value.profilImageUrl ? `
                    <div class="container-img-card">
                        <img class="img-responsive" src="${value.profilImageUrl}">
                    </div>
                ` : `
                    <div class="div-img">
                        <i class="fa fa-image fa-2x"></i>
                    </div>
                `;

                let address = "";
                if(typeof value.address != "undefined") {
                    if(typeof value.address.streetAddress != "undefined" && value.address.streetAddress != "") address += value.address.streetAddress+", ";
                    if(value.address.addressLocality) address += value.address.addressLocality+", ";
                    if(value.address.level4Name) address += value.address.level4Name+", ";
                    if(value.address.level3Name) address += value.address.level3Name;
                }

                let htmlTags = "";

                if(typeof value.tags != "undefined") {
                    $.each(value.tags, function(index, tag){
                        if(tag != "") {
                            htmlTags += `
                                <a href="javascript:;" class="btn-tag-panel" data-tags="${tag}">
                                    <span class="badge bg-transparent btn-tag tag commonClassTags">#${tag}</span>
                                </a>
                            `;
                        }
                    });
                }

                let section = "";
                if(value.section) section += tradCategory[value.section];

                if (typeof value.category !== "undefined" && value.category !== "") {
                    section += tradCategory[value.category] != undefined ? " > " + tradCategory[value.category] : " > "+value.categorie ;

                    if (isfiltre && level == "initFilters") {
                        if (!(value.category in filters)) {
                            filters[value.category] = [];
                        }

                        if (typeof value.subtype !== "undefined" && value.subtype !== "") {
                            if (!filters[value.category].includes(value.subtype)) {
                                filters[value.category].push(value.subtype);
                            }
                        }

                        if (!categorieFilters.includes(value.category)) {
                            categorieFilters.push(value.category);
                        }
                    }
                }

                if(typeof value.subtype  != "undefined" && value.subtype != "") {
                    let subtype = tradCategory[value.subtype] != undefined ? tradCategory[value.subtype] : value.subtype;
                    section += " > "+subtype;
                }
                let contact = "";
                if(value.contactInfo && value.contactInfo != "") {
                    contact = `<b>Contact : </b>`+value.contactInfo;
                }

                let partager = '';
                if(typeof value.collection != "undefined" && typeof value.type != "undefined") {
                    partager = ` <button id="btn-share-${value.type}" class="btn btn-link btn-info btn-share-panel" data-ownerlink="share" data-id="${value._id['$id']}" data-type="${value.collection}">
                        <i class="fa fa-share"></i> ${trad['share']}
                    </button>`;
                }
                let price = "";
                if(typeof value.price != "undefined" && value.price != "") {price = value.price;
                if(typeof value.devise != "undefined") price += " "+value.devise;}
                src += `
                    <div class="content col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="item-slide">
                            <div class="entityCenter">
                                <a class="pull-left">
                                    <i class="fa fa-briefcase bg-yellow-k"></i>
                                </a>
                            </div>
                            <div class="img-back-card" style="background-image: url('${value.profilImageUrl ? value.profilImageUrl : ""}');">
                                ${imgContent}
                                <div class="text-wrap-initial text-center">
                                    <div class="entityRight">
                                        <a href="javascript:;" onclick="openPreview('${key}')" class="entityName letter-turq">${value.name ? value.name : "Nom non disponible"} </a>
                                        ${price != "" ? "<br>"+price : ""}
                                    </div>
                                    <div class="entityType col-xs-12">
                                        <span class="type">${section != "" ? section : "Type non disponible"}</span>
                                    </div>
                                    <div class="entityLocality">
                                        <span>
                                            <i class="fa fa-map-marker text-bold"></i>
                                            ${address != "" ? address : "Adresse non disponible"}
                                        </span>
                                    </div>
                                </div>
                                <div class="text-wrap text-center">
                                    <div class="entityRight">
                                        <a href="javascript:;" onclick="openPreview('${key}')" class="entityName letter-turq">${value.name ? value.name : "Nom non disponible"}</a>
                                        ${price != "" ? "<br>"+price : ""}
                                    </div>
                                    <div class="entityType col-xs-12">
                                        <span class="type">${section != "" ? section : "Type non disponible"}</span>
                                    </div>
                                    <div class="entityLocality">
                                        <span>
                                            <i class="fa fa-map-marker text-bold"></i>
                                            ${address != "" ? address : "Adresse non disponible"}
                                        </span>
                                        <br>
                                        <span>
                                            ${contact}
                                        </span>
                                    </div>
                                    <hr>
                                    <p class="p-short">
                                        ${value.description ? value.description : "Description non disponible"}
                                    </p>
                                    <hr>
                                    <ul class="tags-list">
                                        ${htmlTags}
                                    </ul>
                                    ${partager}
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            });

            if (isfiltre && level == "initFilters") {
                let htmlCategorie = "";

                htmlCategorie = generateHTMLFilters(filters);

                $(".dropdown-content-categorie").html(htmlCategorie);
            }

            $(".elements-container").html(src);
        } else {
            let nullData = `<h4 style="text-transform:none; text-align: center; width: 100%; margin-top: 5%;">Il n'y a actuellement aucun contenu disponible à afficher.</h4>`;
            $(".elements-container").html(nullData);
        }
    }
    function filtersData(selectedFilter, nameSearch, level = "") {
        let params = {
            searchType: ["classifieds", "ressources"],
            notSourceKey : true,
            filters: {},
            count: true,
            countType : ["classifieds", "ressources"],
            fields : ["_id", "address", "category", "collection", "description", "geo", "geoPosition", "name", "section", "subtype", "tags", "type", "profilImageUrl", "contactInfo", "price", "devise", "creator"],
            indexStep : 0
        }

        if(nameSearch != "") {
            params.name = nameSearch;
        }

        if (selectedFilter.type.length > 0) {
            params.filters.type = {$in:selectedFilter.type};
        }

        if (selectedFilter.section.length > 0) {
            params.filters.section = {$in:selectedFilter.section};
        }

        if (selectedFilter.category.length > 0) {
            params.filters.category = {$in:selectedFilter.category};
        }

        if (selectedFilter.subtype.length > 0) {
            params.filters.subtype = {$in:selectedFilter.subtype};
        }

        ajaxPost(
            null,
            baseUrl + "/" + moduleId + "/search/globalautocomplete",
            params,
            function(responses) {
                listeCatlogue(responses.results, true, level);
                setFiltersActifs(selectedFilter);
            }
        );
    };

    function generateHTMLFilters(data) {
        let html = '';

        let noValueHTML = '';
        for (let key in data) {
            if (data[key].length > 0) {
                let valuesHTML = '';
                data[key].forEach(value => {
                    valuesHTML += `<a data-level="filtersInit" style="color:#00000073" data-subtype="${value}">${tradCategory[value] !== undefined ? tradCategory[value] : value}</a>`;
                });

                html += `
                    <div class="col-sm-2 col-md-2">
                        <div class="key-item">
                            <b><a data-level="filtersInit" data-category="${key}">${tradCategory[key] !== undefined ? tradCategory[key] : key}</a></b>
                        </div>
                        <div class="values-list">
                            ${valuesHTML}
                        </div>
                    </div>
                `;
            } else if(data[key].length === 0){
                noValueHTML += `
                    <div class="key-item">
                        <b><a data-level="filtersInit" data-category="${key}">${tradCategory[key] !== undefined ? tradCategory[key] : key}</a></b>
                    </div>
                `;
            }
        }

        if (noValueHTML) {
            html += `
                <div class="col-sm-2 col-md-2">
                    ${noValueHTML}
                </div>
            `;
        }

        dropdownClick(".categbtn", "categorieDropdown");

        return html;
    }

    function setFiltersActifs(selectedFilter) {
        let filtersContainer = $(".btn-select-filtre");
        let src = '';

        Object.entries(selectedFilter).forEach(([key, values]) => {
            values.forEach(value => {
                src += `<button style="margin-left: 5px" class="btn btn-success" data-field="${key}" data-value="${value}">
                    <i class="fa fa-times-circle delete_actif_filter" data-field="${key}" data-value="${value}"></i>
                    <span class="activeFilters">${tradCategory[value] != undefined ? tradCategory[value] : value}</span>
                </button>`;
            });
        });
        
        if(src != "" && src != null) {
            let filterText = '&nbsp;&nbsp;&nbsp;<?php echo Yii::t('cms', "Active filter") ?> : &nbsp;&nbsp;';
            filtersContainer.html(filterText + src);
            $(".description").addClass("description_activfilters");
        }

        $(".delete_actif_filter").on('click', function() {
            let field = $(this).data('field');
            let value = $(this).data('value');

            if (selectedFilter[field]) {
                selectedFilter[field] = selectedFilter[field].filter(item => item !== value);
            }

            let allEmpty = Object.values(selectedFilter).every(array => array.length === 0);

            if(allEmpty) {
                filtersContainer.empty();
                listeCatlogue(ressources);
                $(".description").removeClass("description_activfilters");
            } else {
                filtersData(selectedFilter, nameSearch);
            }
        });
    }

    function openPreview(cle) {
        let params = dataPreviews[cle];
        urlCtrl.openPreview("/view/url/costum.views.custom.cocity.previewressource", {"params" : params});
    }

    function dropdownClick(classDropdown, idDropdown) {
        const dropdownButton = document.querySelector(classDropdown);
        const dropdownContent = document.getElementById(idDropdown);

        dropdownButton.addEventListener("click", function(event) {
            $(".dropdown-content").removeClass('show');
            $("#"+idDropdown).addClass('show');
            event.stopPropagation();
        });

        window.addEventListener("click", function() {
            $("#"+idDropdown).removeClass('show');
        });
    }
    $(".dropdown-content").on('click', 'a', function(){
        let type = $(this).data('type') != undefined ? $(this).data('type') : "";
        let section = $(this).data('section') != undefined ? $(this).data('section') : "";
        let category = $(this).data('category') != undefined ? $(this).data('category') : "";
        let subtype = $(this).data("subtype") != undefined ? $(this).data('subtype') : "";
        let level = $(this).data("level") != undefined ? $(this).data('level') : "";

        nameSearch = $("#main-search-ressources").val();

        if (type !== "" && !selectedFilter.type.includes(type)) {
            selectedFilter.type.push(type);
        }

        if (section !== "" && !selectedFilter.section.includes(section)) {
            selectedFilter.section.push(section);
        }

        if (category !== "" && !selectedFilter.category.includes(category)) {
            selectedFilter.category.push(category);
        }

        if (subtype !== "" && !selectedFilter.subtype.includes(subtype)) {
            selectedFilter.subtype.push(subtype);
        }

        filtersData(selectedFilter, nameSearch, level);
    });

    var timeouts = 0;
    $("#main-search-ressources").on('input', function(){
        nameSearch = $(this).val();

        clearTimeout(timeouts);
        timeouts = setTimeout(function() {
            filtersData(selectedFilter, nameSearch);
        }, 1000);
    });
    $(document).ready(function(){
        listeCatlogue(ressources);
        dropdownClick(".dropbtn", "typeDropdown");
        $("#showMap").on('click', function(){
            $(".list-affiche").addClass("hidden");
            $(".map-affiche").removeClass("hidden");
            mapSource.clearMap();
            mapSource.addElts(dataPreviews);
        });

        $("#hidenMap").on("click", function(){
            $(".list-affiche").removeClass("hidden");
            $(".map-affiche").addClass("hidden");
        });

        $('.btn-share-panel').off().click(function () {
			directory.showShareModal($(this).attr('data-type'), $(this).attr('data-id'));
		});
    })
</script>