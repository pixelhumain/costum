<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
//   if(isset($el)) var_dump($el);
?>
<style>
.hub-eau-header {
    background-color: #f4f8fc; 
    border-left: 6px solid #0073e6;
    padding: 20px;
    border-radius: 8px;
    margin-bottom: 30px;
    text-align: center;
    font-family: Arial, sans-serif;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
}

.hub-eau-header h1 {
    font-size: 1.8rem;
    color: #0073e6;
    margin: 0 0 10px;
    font-weight: bold;
}

.hub-eau-header p {
    font-size: 1.4rem;
    color: #555;
    margin: 0;
    line-height: 1.5;
}
.container-card {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    gap: 20px;
    padding: 20px;
    max-width: 1200px;
    margin: 0 auto;
}

.card {
    position: relative;
    background-color: white;
    border-radius: 8px;
    box-shadow: 0 4px 8px rgb(0 0 0 / 41%);
    padding: 20px;
    width: calc(25% - 20px);
    box-sizing: border-box;
    text-align: center;
    transition: transform 0.2s;
    cursor: pointer;
}

.card:hover {
    transform: translateY(-5px);
    background-color: #65656542;
}

.icon-container {
    margin-bottom: 10px;
}

.icon {
    width: 50px;
    height: 50px;
}

.card-title {
    color: #0073e6;
    font-size: 1.5em;
    margin-bottom: 10px;
    text-transform: none;
}

.card-description {
    color: #555;
    font-size: 0.9em;
    line-height: 1.4;
    margin-bottom: 15px;
}

.card-highlight {
    font-weight: bold;
    color: #000;
}

@media (max-width: 1024px) {
    .card {
        width: calc(33.333% - 20px);
    }
}

@media (max-width: 768px) {
    .card {
        width: calc(50% - 20px);
    }
}

@media (max-width: 480px) {
    .card {
        width: 100%;
    }
}

/*  */
h1 {
  text-align: center;
}

#stations-table {
  margin: 20px auto;
  width: 80%;
  max-width: 1200px;
  border-collapse: collapse;
}

.table-container {
  max-height: 500px;
  overflow-x: auto;
  overflow-y: auto;
  width: 100%; 
}

table {
  width: 100%;
  border-collapse: collapse;
}

th, td {
  padding: 8px; 
  text-align: left; 
  border: 1px solid #ddd;
}

th {
  background-color: #f2f2f2;
  font-weight: bold; 
}

tr:hover {
  background-color: #f5f5f5; 
}
</style>
<div class="<?= $kunik ?>">
    <div class="hub-eau-header">
        <h1>Découvrir les jeux de données de Hub Eau France</h1>
        <p>
            Accédez à une sélection complète de données ouvertes sur l'eau, incluant 
            des informations précises sur la qualité de l'eau potable, l'écoulement 
            des cours d'eau et bien plus. Explorez les données, participez et 
            contribuez à une meilleure gestion des ressources en eau.
        </p>
    </div>
    <div class="container-card">
        <!-- Carte 1 -->
        <div class="card" data-links="vente_achat_phyto" onclick="openPreviewsEau('vente_achat_phyto', 'Vente et achat de produits phytopharmaceutiques')">
            <h3 class="card-title">Vente et achat de produits phytopharmaceutiques</h3>
            <p class="card-description">
                L’API Vente et achat de produits phytopharmaceutiques (PPP) donne accès aux données ouvertes de la BNV-D Traçabilité opérée par l’Office français de la biodiversité.
            </p>
            <p class="card-highlight">20 millions de transactions</p>
        </div>

        <!-- Carte 2 -->
        <div class="card" onclick="openPreviewsEau('observations', 'Ecoulement des cours de eau')">
            <h3 class="card-title" >Ecoulement des cours d'eau</h3>
            <p class="card-description">
                Données de campagnes d'observations visuelles de l’écoulement des petits et moyens cours d’eau.
            </p>
            <p class="card-highlight">250 000 observations</p>
        </div>

        <!-- Carte 3 -->
        <div class="card" onclick='openPreviewsEau("resultats_dis", "Qualité de eau potable")'>
            <h3 class="card-title">Qualité de l'eau potable</h3>
            <p class="card-description">
                Résultats du contrôle sanitaire de l’eau distribuée commune par commune.
            </p>
            <p class="card-highlight">+75 millions d’analyses</p>
        </div>

        <!-- Carte 4 -->
        <div class="card" onclick="openPreviewsEau('prelevements', 'Prélèvements en eau')">
            <h3 class="card-title">Prélèvements en eau</h3>
            <p class="card-description">
                Informations sur les volumes annuels directement prélevés sur la ressource en eau, déclinés par localisation et catégorie d’usage de l’eau.
            </p>
            <p class="card-highlight">900 000 volumes prélevés annuels</p>
        </div>

        <!-- Carte 5 -->
        <div class="card">
            <h3 class="card-title">Hydrobiologie</h3>
            <p class="card-description">
                Ensemble des informations liées à la qualité hydrobiologique des eaux superficielles continentales : cours d'eau et plans d'eau.
            </p>
            <p class="card-highlight">+18 millions d'observations</p>
        </div>

        <!-- Carte 6 -->
        <div class="card">
            <h3 class="card-title">Surveillance des eaux littorales</h3>
            <p class="card-description">
                Données de surveillance des eaux littorales et marines issues de QUADRIGE. Actuellement : Contaminants chimiques et écotoxicologie
            </p>
            <p class="card-highlight">+500 000 contaminants chimiques</p>
        </div>

        <!-- Carte 7 -->
        <div class="card">
            <h3 class="card-title">Hydrométrie</h3>
            <p class="card-description">
                L'API permet d'interroger le référentiel hydrométrique ainsi que les mesures quasi temps-réel (niveaux, débits) provenant du réseau de mesure français (environ 3000 stations hydrométriques).
            </p>
            <p class="card-highlight">+80 millions d'observations</p>
        </div>

        <!-- Carte 8 -->
        <div class="card">
            <h3 class="card-title">Température des cours d'eau</h3>
            <p class="card-description">
            Température relevées à des fréquences variant de une minute à quelques heures par des capteurs automatiques dans les cours d'eau
            </p>
            <p class="card-highlight">40 millions des mesures</p>
        </div>

        <!-- Carte 9 -->
        <div class="card">
            <h3 class="card-title">Qualité des cours d'eau</h3>
            <p class="card-description">
                Ensemble des informations liées à la qualité physico-chimique des eaux superficielles continentales : cours d'eau et plans d'eau
            </p>
            <p class="card-highlight">200 millions d'analyses</p>
        </div>

        <!-- Carte 10 -->
        <div class="card">
            <h3 class="card-title">Qualité des nappes d'eau souterraine</h3>
            <p class="card-description">
                Données de qualité physico-chimique des nappes d'eau souterraine françaises
            <p class="card-highlight">+115 millions d'analyses</p>
        </div>

        <!-- Carte 11 -->
        <div class="card">
            <h3 class="card-title">Piezométrie</h3>
            <p class="card-description">
                Niveau des nappes d'eau souterraine via les chroniques de hauteur d'eau dans les piézomètres
            <p class="card-highlight">+42 millions de mesures</p>
        </div>

        <!-- Carte 12 -->
        <div class="card">
            <h3 class="card-title">Indicateurs des services</h3>
            <p class="card-description">
                L'API Indicateurs réglementaires permet d'accéder aux indicateurs sur les services publics d'eau, d'assainissement et sur l'assainissement non collectif.
            <p class="card-highlight">+700 000 indicateurs</p>
        </div>

        <!-- Carte 13 -->
        <div class="card">
            <h3 class="card-title">Poisson</h3>
            <p class="card-description">
            L'API Poisson diffuse les données collectées lors d'opérations de pêches scientifiques à l'électricité (observations, stations de prélèvement, opérations, et indicateurs).
            <p class="card-highlight">+ 9 millions d'observations</p>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>

	var elCostum = <?= json_encode($el) ?>;
    var address = "";
    if(typeof elCostum.address != "undefined") address = elCostum.address["level3Name"];

    function openPreviewsEau(params, title) {
        urlCtrl.openPreview("view/url/costum.views.custom.cocity.previeweau", {"links" : params, "title" : title, "region" : address});
    }
</script>