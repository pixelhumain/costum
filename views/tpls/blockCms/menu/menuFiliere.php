<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
  $parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl","filiere","thematic","tags","address"]);

  $listFielere = isset($parent["filiere"])?$parent["filiere"]:[];
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style type="text/css" id="<?= $kunik ?>menuFiliere">
  .menuThemeCocity_<?= $kunik?>{
    width: 100%;
    height: 100px;
    padding: 2%;
    margin-left: 2%;
  }
  .menuThemeCocity_<?= $kunik?>  .titlemenu{
    margin-left: 2%;
  }

  .menuThemeCocity_<?= $kunik?> .initialise<?= $blockCms['_id'] ?>{
    color: #0b1a30 !important;
  }
  .menuThemeCocity_<?= $kunik?> .openCostum<?= $blockCms['_id'] ?>{
    color: white;
  }
  .container-<?= $kunik?>{
    min-height: 50px !important; 
  }
  /* .addElement<?= $blockCms['_id'] ?> {
    display: none;
  }
  .menuThemeCocity_<?= $kunik?>:hover .addElement<?= $blockCms['_id'] ?>{
    display: block;
    position: absolute;
    top:14%;
    left: 50%;
    transform: translate(-50%,-50%);
  } */
  .menuThemeCocity_<?= $kunik?> .card {
    margin: 10% auto;
    height: 150px;
    padding-top: 1%;
    border-radius: 10px;
    box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
    cursor: pointer;
    background: #092434;
    transition: 0.4s;
  }

  .menuThemeCocity_<?= $kunik?> .cards-list {
    z-index: 0;
    width: 100%;
    justify-content: space-around;
    flex-wrap: wrap;
  }
  .menuThemeCocity_<?= $kunik?> a {
    text-decoration: none;
    width: 100%;
  }
  .menuThemeCocity_<?= $kunik?>  .dropdown-menu, .menuThemeCocity_<?= $kunik?>  .dropdown-menu {
    position: initial;
    overflow-y: visible !important;
    top: 35px;
    width: 100%;
    z-index: 1;
    border-radius: 2px;
    padding: 0px;
  }
  .menuThemeCocity_<?= $kunik?> .titlemenu button{
    border: none;
    background: white;
  }
  .openCostumxs<?=$kunik?>{
    color : #3b9ca3 !important;
  }

  .cards-list .content-filiere{
    width: 100%;
  }
  .menuThemeCocity_<?= $kunik?> .main-section {
    text-align: right;
    line-height: 3.4em;
  }

  .menuThemeCocity_<?= $kunik?> .main-section i {
    margin-right: 4px;
    margin-left: 6px;
    font-size: 0.9em;
  }
  .initialise<?= $kunik ?> .element<?= $kunik ?>, .initialise<?= $kunik ?> .element<?= $kunik ?> i {
    color: aqua !important;
  }
  .menuThemeCocity_<?= $kunik?> .content-filiere {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
  }
  .theme-card {
    position: relative;
    flex: 1 1 23%;
    height: 300px;
    max-width: 23%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    padding: 20px;
    font-size: 24px;
    font-weight: bold;
    box-sizing: border-box;
    margin-bottom: 10px;
  }

  .theme-card h2 {
    position: absolute;
    top: 20px;
    left: 20px;
    font-size: 6em;
    color: rgba(0, 0, 0, 0.1);
    z-index: 1;
    opacity: 0.4;
    font-family: 'Arial';
  }

  .theme-card .thematic {
    font-size: 1.8em;
    z-index: 2;
    margin: 0;
    padding-left: 20px;
    line-height: 2.4em;
    white-space: nowrap;
    text-align: center;
  }

  .theme-card .description {
    line-height: 1em;
    text-align: center;
    font-size: 3em;
    font-weight: 100;
  }

  .theme-card .description i {
    font-size: 1em;
  }

  .count {
    position: absolute;
    bottom: 10px;
    right: 10px;
    font-size: 2em;
    font-weight: bold;
    color: #333;
    opacity: 0.6;
  }

  @media (max-width: 1200px) {
    .theme-card {
      flex: 1 1 48%; 
      max-width: 48%;
    }
  }

  @media (max-width: 900px) {
    .menuThemeCocity_<?= $kunik?> .theme-card {
      flex: 1 1 50%;
      max-width: 50%;
      padding: 10px;
      height: 160px;
      padding-top: 20px;
      font-size: 12px;
    }
    
    .menuThemeCocity_<?= $kunik?> .cards-list .inclined-button {
      width : 100% !important ;
      font-size : 1.6em !important;
    }
    .menuThemeCocity_<?= $kunik?> .main-section {
      text-align: right;
      line-height: 2.4em;
      font-size: 1.5em;
    }
    .menuThemeCocity_<?= $kunik?> .theme-card h2 {
      position: absolute;
      top: 30px;
      left: 10px;
      font-size: 5em;
      color: rgba(0, 0, 0, 0.1);
      z-index: 1;
      opacity: 0.4;
      font-family: 'Arial';
    }
  }

  .menuThemeCocity_<?= $kunik?> .inclined-button {
    margin-top: 25px;
    margin-bottom: 25px;
    padding: 3px;
    width: 18%;
    font-size: 2em;
    color: #ffff;
    background-color: #4c0555;
    border-radius: 5px;
    cursor: pointer;
    transform: rotate(-7deg);
    transition: transform 0.3s ease;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2);
    justify-content: center;
    align-items: center;
    text-align: center;
    flex-direction: column;
    word-wrap: break-word;
  }

  .menuThemeCocity_<?= $kunik?> .inclined-button:hover {
    transform: rotate(0deg);
    transition: transform 0.3s ease;
  }

  .menuThemeCocity_<?= $kunik?> .inclined-button:hover::after {
    transform: rotate(-10deg);
    transition: transform 0.3s ease 0.3s;
  }
</style>
<div class="menuThemeCocity_<?= $kunik?> container-<?= $kunik?>">
  <div class="titlemenu">
   <h2 class="title-1" style="text-transform:none">Thématiques & Filières <i class="fa fa-arrow-down"></i>
    <!-- <div class="dropdown pull-right  hidden-md hidden-lg ">
      <button class="  dropdown-toggle" type="button" id="dropdownMenuList2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-angle-double-down  fa-x2"></i>
      </button>
      <ul class="dropdown-menu dropdown-menu-left" aria-labelledby="dropdownMenuList2">
      </ul>
    </div> -->
   </h2>
  </div>
    <!-- <button class="btn btn-primary addElement<?= $blockCms['_id'] ?>">Ajouter un nouvel filière</button>   -->
    <div id="listFil_<?= $kunik?>" class="cards-list col-md-12 col-xs-12">
      <div class="content-filiere row"> 
          <div style="position: relative;width:100%;height: 100%">
            <p class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></p>
          </div>
      </div>  
      <button class="inclined-button addElement<?= $blockCms['_id'] ?>" >Ajouter un nouvel filière <i class="fa fa-arrow-right"></i> </button>
    </div>
</div>     

<script type="text/javascript">
  var elCostum = <?= json_encode($parent) ?>;
  var positionF = typeof elCostum.address != "undefined" ? elCostum.address : "";
  costum.existFiliers = []; 
  var str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#<?= $kunik ?>menuFiliere").append(str);

	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var menuFiliere = {
			configTabs : {
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
          cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
      }
		}
		cmsConstructor.blocks.menuFiliere<?= $myCmsId ?> = menuFiliere;
	};

  var nullfilier = "<p style='font-size: 22px; padding: 15px'>Aucune filière trouver</p>"
  var suggestName = [
    tradTags["food"],
    tradTags["health"] ,
    tradTags["waste"] ,
    tradTags["transport"] ,
    tradTags["education"],
    tradTags["citizenship"],
    tradTags["economy"],
    tradTags["energy"],
    tradTags["culture"],
    tradTags["environment"],
    tradTags["numeric"],
    tradTags["sport"],
    tradTags["associations"],
    tradTags["third places"],
    tradTags["pact"]
  ];
  var colors = [
    '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', 
    '#e67e22', '#f1c40f', '#16a085', '#27ae60', '#2980b9',
    '#8e44ad', '#c0392b', '#d35400', '#f39c12', '#3083D6'
  ];
  var nameFiliereExiste = [];
  var position = 0;

  var slugTiersLieux = {
    "Nouvelle-Aquitaine" : "laCooperativeTierslieux",
    "Hauts-de-France" : "laCompagnieDesTierslieux",
    "Bourgogne-Franche-Comté" : "tierslieuxBfc",
    "Auvergne-Rhône-Alpes" : "relief",
    "Guyane" : "reseauDesTierslieuxDeGuyane",
    "Provence-Alpes-Côte d'Azur" : "sudTierslieux",
    "Occitanie" : "laRosee",
    "Mayotte" : "reseauDesTierslieuxDeMayotte",
    "Corse" : "daLocu",
    "Martinique" : "reseauRegionalDesTierslieuxMartinique",
    "Guadeloupe" : "reseauRegionalDesTierslieuxGuadeloupe",
    "Île-de-France" : "consortiumIledefranceTierslieux",
    "Centre-Val de Loire" : "ambitionTiersLieuxValDeLoire",
    "Normandie" : "reseauNormandDesTierslieux",
    "Bretagne" : "bretagneTierslieux",
    "Grand Est" : "reseauDesTierslieuxDuGrandEst",
    "Pays de la Loire" : "capTierslieux",
    "Réunion" : "LaReunionDesTiersLieux"
  };

  var menu<?= $kunik?> = {
    listeFiliere:function(){
      if(costum.existFiliers && (costum.existFiliers).length > 0 && costum.htmlFiliers && costum.htmlFiliers != ''){
        $("#listFil_<?= $kunik?> .content-filiere").html(costum.htmlFiliers);
        $(".menuThemeCocity_<?= $kunik?> .dropdown-menu").html(costum.htmlXsFilieres);
      } else {
        var params = {
          "contextId" : costum.contextId
        }
        ajaxPost(
          null,
          baseUrl+"/costum/cocity/getfiliere",
          params,
          function(data){       
            if(Object.keys(data).length == 0) {
              $("#listFil_<?= $kunik?> .content-filiere").html(nullfilier);
            } else {
              $("#listFil_<?= $kunik?> .content-filiere").html('');
              position = 1;
              costum.existFiliers = data;
              costum.htmlFiliers = '';
              costum.htmlXsFilieres = '';
              $.each(data, function( index, value ) {
                if(typeof value.name != "undefined") {
                  if(typeof tradTags[value.name.toLowerCase()] != "undefined") {
                    nameFiliereExiste.push(tradTags[value.name.toLowerCase().replace("é", "e")]);
                  } else {
                    nameFiliereExiste.push(value.name.toLowerCase().replace("é", "e"));
                  }
                  var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
                  tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
                  : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
                  nameFil1 = nameTheme.replace('É', 'E');
                  nameFil= nameFil1.replace("é", "e");
                  nameOrga = ("<?= $parent["name"]?>").toLowerCase()+nameFil;
                  menu<?= $kunik?>.existOrgaFiliere(nameOrga, value, index);
                }
              })
            }
          },
          function(error){},
          "json", 
          {async : false}
        );
      }
    },  
    existOrgaFiliere:function(nameOrga, value, thema){
      var nameTheme = typeof tradTags[value.name.toLowerCase()] != "undefined" ? 
      tradTags[value.name.toLowerCase()].charAt(0).toUpperCase()+tradTags[value.name.toLowerCase()].slice(1) 
      : (value.name).charAt(0).toUpperCase() + (value.name).slice(1);
      var params = {
        "cocity" : costum.contextId,
        "thematic" :nameTheme,
        "thema" : thema,
        "typeCocity" : costum.typeCocity,
        "tags" : value.tags,
        "address" : positionF
      };
      if(typeof costum.typeCocity != "undefined" && costum.typeCocity == "region" && thema == "tiers lieux" && typeof elCostum.address != "undefined" && typeof elCostum.address.level3Name != "undefined" && typeof slugTiersLieux[elCostum.address.level3Name] != "undefined" && slugTiersLieux[elCostum.address.level3Name] != "") {
        params.tierslieuxslug = slugTiersLieux[elCostum.address.level3Name];
      }
      
      ajaxPost(
        null,
        baseUrl+"/costum/cocity/getorgafiliere",
        params,
        function(data){
          var htmlNbElm = '';
          var src ="";
          var menuXs = "";
          htmlNbElm += '<div class="main-section">';
          if (typeof data.countCitoyens != "undefined" &&  data.countCitoyens != 0)
              htmlNbElm += '<i class="fa fa-user" aria-hidden="true"></i>' + data.countCitoyens + " ";
          if (typeof data.countEvent != "undefined" && data.countEvent != 0)
              htmlNbElm += '<i class="fa fa-calendar" aria-hidden="true"></i>' + data.countEvent + " ";

          if (typeof data.countOrga != "undefined" && data.countOrga != 0)
              htmlNbElm += '<i class="fa fa-users" aria-hidden="true"></i>' + data.countOrga + " ";
          if (typeof data.countProject != "undefined" && data.countProject != 0)
              htmlNbElm += '<i class="fa fa-lightbulb-o" aria-hidden="true"></i>' + data.countProject + " ";

          htmlNbElm += '</div>';
          if (typeof data.orgaFiliere != "undefined" && data.orgaFiliere != null){
            var valueFil = data.orgaFiliere;
            if(typeof valueFil.costum !="undefined" && typeof valueFil.costum.slug != "undefined" /*&& (valueFil.costum.slug == "cocity" || valueFil.costum.slug == "filiereGenerique") && typeof valueFil.cocity !="undefined" && valueFil.cocity == costum.contextId*/) {               
              var verifLinks = "";
              if(typeof costum.typeCocity != "undefined" && costum.typeCocity == "region" && thema == "tiers lieux" && typeof elCostum.address != "undefined" && typeof elCostum.address.level3Name != "undefined" && typeof slugTiersLieux[elCostum.address.level3Name] != "undefined" && slugTiersLieux[elCostum.address.level3Name] != "") {
                verifLinks = slugTiersLieux[elCostum.address.level3Name];
              } else {
                verifLinks = valueFil.slug;
              }
              src = `
                <div class="theme-card" style="background-color: ${colors[position % colors.length]}">
                  <a class="openCostum<?= $blockCms['_id'] ?>" href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/${verifLinks}" title="${nameTheme}" target="_blank">
                    <h2>${position}</h2>
                    <p class="thematic">${nameTheme}</p>
                    <p class="description">
                      <i class="fa ${value.icon}">
                    </i></p>
                    <p class="count">${htmlNbElm}</p>
                  </a>
                </div>
              `;
              menuXs += '<li class="text-left">'+
              '<a class="openCostumxs<?=$kunik?>"  href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ verifLinks+'">'+
                    '<div class=" swiper-slide " >'+nameTheme+    
                    '</div> '+                                                                  
                  '</a>'+
              '</li>';
            } 
          } else {
            src = `
                <div class="theme-card" style="background-color: ${colors[position % colors.length]}">
                  <a class="initialise<?= $blockCms['_id'] ?>" data-tag="${value.icon}" href="javascript:;" data-name="${value.name}" title="${nameTheme}">
                    <h2>${position}</h2>
                    <p class="thematic">${nameTheme}</p>
                    <p class="description">
                      <i class="fa ${value.icon}">
                    </i></p>
                    <p class="count element<?= $kunik ?> hidden-xs element${thema}">${htmlNbElm}</p>
                  </a>
                </div>
              `;
            menuXs += '<li class="text-left">'+
            '<a href="javascript:;" data-name ="'+ value.name+'" class="initialisexs<?= $blockCms['_id'] ?> initialise<?= $blockCms['_id'] ?>"'+
                  '<div class=" swiper-slide ">'+nameTheme+    
                  '</div> '+                                                                  
                '</a>'+
            '</li>';
          }

          costum.htmlFiliers += src;
          $(".content-filiere").append(src);
          costum.htmlXsFilieres += menuXs;
          $(".menuThemeCocity_<?= $kunik?> .dropdown-menu").append(menuXs);
          position += 1;
          $(".initialise<?= $blockCms['_id'] ?>").off('click').on('click', function() { 
            nameOrga = "<?= $parent["name"]?>"+" "+$(this).data("name");
            nameFiliere = $(this).data("name");
              // bootbox.confirm("Aucune page n'est disponible pour cette filière. Voulez-vous la créer ?",
              //   function(result){
              //     if (!result) {
              //       return;
              //     }else {
              //       localStorage.setItem("paramsCocity",costum.contextId+".< ?= $parent["name"]?>."+nameFiliere+"."+costum.slug+"."+costum.typeCocity);
              //       // window.location="< ?php  echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez"; 
              //       window.open("< ?php echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez", "_blank");
              //     }
              //   }
              // );
              $.confirm({
                title: "Confirmation",
                type: 'orange',
                content: "Aucune page n'est disponible pour cette filière. Voulez-vous la créer ?",
                buttons: {
                  no: {
                    text: trad.no,
                    btnClass: 'btn btn-default'
                  },
                  yes: {
                    text: trad.yes,
                    btnClass: 'btn btn-success',
                    action: function() {
                      localStorage.setItem("paramsCocity",costum.contextId+".<?= $parent["name"]?>."+nameFiliere+"."+costum.slug+"."+costum.typeCocity);
                      window.open("<?php echo Yii::app()->createUrl('/costum')?>/co/index/slug/filierePrez", "_blank");
                    }
                  }
                }
              }); 
            }
          );
        },
        null,
        null,
        {
          async : true
        }
      );
    }
  }

  jQuery(document).ready(function() {
    menu<?= $kunik?>.listeFiliere();
    $(".addElement<?= $blockCms['_id'] ?>").click(function() {  
      var defaultName = nameFiliereExiste.length > 0 ? suggestName.filter(element => !nameFiliereExiste.includes(element)) : suggestName;
      var tplCtx = {};
      var activeForm = {
        "jsonSchema" : {
          "title" : "Ajouter nouvelle filière",
          "type" : "object",
          "properties" : {
            name :{
              "inputType" : "tags",
              "maximumSelectionLength" : 1,
              label : "Nom de la filière",
              values : defaultName,
              "value" : []
            },
            icon :{
              label : "icone de la filière",
              "inputType" : "select",
              options : fontAwesome,
              rules:{
                "required":true
              }
            },
            tags: {
              label : "Tags",
              "inputType" : "tags",
              rules:{
                "required":true
              }
            }
          }
        }
      };          

      activeForm.jsonSchema.save = function () {
        var lastTags = elCostum.tags ? elCostum.tags : [];
        var lastThem = elCostum.thematic ? elCostum.thematic : [];
        var name = $("#name").val();
        if (!lastTags.includes(name)) lastTags.push(name);
        if (!lastThem.includes(name)) lastThem.push(name);

        tplCtx.id = costum.contextId;
        tplCtx.collection = costum.contextType;
        tplCtx.path="allToRoot";
        tplCtx.value = {
          "thematic" : lastThem
        };
        $.each(activeForm.jsonSchema.properties , function(k,val) {
          filiere = "filiere."+name+"."+k;
          tplCtx.value[filiere] = $("#"+k).val();
          if(k == "tags"){
            var onTags = ($("#"+k).val()).split(",");
            tplCtx.value[filiere] = onTags;
            var newTags = onTags.filter(tag => !lastTags.includes(tag));
            Array.prototype.push.apply(lastTags, newTags);
          } 
          if(k=="icon") tplCtx.value[filiere] = "fa-"+$("#"+k).val();
        });
        tplCtx.value["tags"] = lastTags;

        if(typeof tplCtx.value == "undefined")
          toastr.error('value cannot be empty!');
        else {
          dataHelper.path2Value( tplCtx, function(params) { 
            toastr.success('Modification enregistrer');
            $("#ajax-modal").modal('hide');
            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
          } );
        } 
      }
      activeForm.jsonSchema.afterSave = function() {
        costum.existFiliers = [];
        costum.htmlFiliers = "";
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
      }
      dyFObj.openForm( activeForm );
    });
  });
      
</script>