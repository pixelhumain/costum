<?php 
	$userId = Yii::app()->session["userId"] ?? null;
	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
		"/plugins/Chart-2.8.0/chartjs-plugin-datalabels.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
	$styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
	$otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
	$otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
	$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
	$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
	$imgProjectDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/project.png";
	$imgEventDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/event.png";
	$imgBadgeDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/badge.png";
	$imgPoleOrga = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/poleOrga.png";
	$imgProjectOrga = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/projectOrga.png";
	
	// echo "<pre>";
	// var_dump($oceco["pole"]);die();
?>	

<style>
		.kl-block {
			display: block;
		}
		.kl-flex {
			display: flex;
		}
		.kl-justify-between {
			justify-content: space-between;
		}

		.kl-grid-2{
			gap: 1.5rem;
			grid-template-columns: repeat(2, minmax(0, 1fr));
			display: grid;
		}
		.kl-grid-3{
			gap: 1.5rem;
			grid-template-columns: repeat(3, minmax(0, 1fr));
			display: grid;
		}
		.kl-dashboard {
			margin: 0 auto;
			padding: 20px;
			display: grid;
			grid-template-columns: repeat(2, 1fr);
			grid-gap: 20px;
		}

		.kl-dashboard  h3 {
			color : #68ca91;
		}

		/* Cartes de statistiques */
		.kl-stats-cards {
			grid-column: span 2;
			display: flex;
			gap: 20px;
		}

		.kl-stats-statistique, .kl-title {
			grid-column: span 2;
			gap: 20px;
			cursor: pointer;
		}
		
		.kl-statistique{
			display: flex;
			background: #121010;
			justify-content: center;
			border-radius: 8px;
			padding: 10px;
			color: white;
			text-align: center;
		}

		.kl-card {
			background: #121010;
			border-radius: 8px;
			flex: 1;
			text-align: center;
		}

		.kl-card h3 {
			font-size: 18px;
			color: #f0f0f0;
			margin-bottom: 10px;
		}

		.kl-card .kl-value {
			font-size: 24px;
			font-weight: bold;
			color: #fff;
			margin: 10px 0;
		}

		.kl-card .kl-change {
			font-size: 14px;
		}

		.kl-card .kl-positive {
			color: #b9cebe;
		}

		.kl-card .kl-negative {
			color: #dc3545;
		}

		.kl-card .kl-view-more {
			display: inline-block;
			margin-top: 10px;
			text-decoration: none;
			color: #007bff;
			font-size: 14px;
		}

		/* Section performance */
		.kl-performance, .kl-oceco-statistique {
			background: #121010;
			border-radius: 8px;
			box-shadow: 0 4px 6px 0px 0px 12px 3px rgb(54 52 52 / 10%);
			padding: 20px;
			overflow: hidden;
		}

		.kl-performance.show, .kl-oceco-statistique.show {
			max-height: 1000px;
			opacity: 1;
		}

		.kl-performance h3, .kl-oceco-statistique h3 {
			font-size: 18px;
			margin-bottom: 15px;
			color: white; 
		}

		.kl-oceco-statistique{
			text-align: center;
		}

		.kl-chart, .kl-map p {
			color: #fff;
		}
		.kl-chart, .kl-map {
			display: flex;
			align-items: center;
			justify-content: center;
			background: #444;
			border-radius: 8px;
		}

		.kl-nb-view {
			padding: 20px;
			align-items: center;
			box-shadow: 0px 0px 12px 3px rgb(54 52 52 / 10%);
		}

		.kl-parent-icon {
			height: 50px;
			width: 50px;
			display: flex;
			justify-content: center;
			align-items: center;
			background: #2f2c2c;
			border-radius: 10px;
		}

		.kl-btn-see-more {
			border: 1px solid #201e1e;
			background-color: #201e1e;
			color: #fff;
			border-radius: 5px;
			padding: 10px 20px;
		}
		.kl-btn-see-more:hover {
			border: 1px solid #201e1e;
            background: #444;
		}

		.kl-create {
			border: 1px solid #201e1e;
			background-color: #68ca91;
			color: #fff;
			border-radius: 5px;
			padding: 10px 20px;
		}

		.kl-create:hover {
			border: 1px solid #201e1e;
			background-color: #201e1e;
		}

		.kl-parent-icon i.fa {
			font-size: 40px;
			color: white;
		}

		.kamban-icon {
			height: 50px;
			width: 50px;
			display: flex;
			justify-content: center;
			align-items: center;
			background: #2f2c2c;
			border-radius: 10px;
		}

		.kamban-icon:hover {
			background: green;
		}

		.kamban-icon i.fa {
			font-size: 32px;
			color: white;
		}

		.kl-detail-footer {
			height: 38%;
			padding: 20px;
			background: #444; 
			display: flex;
			align-items: center;
			justify-content: space-between;
			border-radius: 0 0 8px 8px;
		}
		/* Responsive design */
		@media screen and (max-width: 768px) {
			.kl-dashboard {
				grid-template-columns: 1fr;
			}

			.kl-stats-cards {
				flex-direction: column;
			}

			.kl-tooltip {
				width: 80vw !important;
			}

			.kl-statistique{
				margin-top: 10px;
			}

			.kamban-icon,.kl-parent-icon{
				width: 40px;
				height: 40px;
			}

			.kamban-icon,.kl-parent-icon i{
				font-size: 30px !important;
			}
		}


		.social-icons {
			display: flex;
		}

		.kl-tooltip {
			position: absolute;
			background: #323030;
			border-radius: 10px;
			top: 55px;
			max-height: 400px;
			width: 316px;
			right: 0;
			z-index: 2;
			overflow-y: auto;
			padding: 10px;
		}

		.kl-tooltip li {
			color: #f0f0f0;
		}

		.kl-arrow:after {
			content: "";
			width: 0;
			height: 0;
			border: solid;
			border-width: 0 10px 10px 10px;
			border-color: transparent transparent #323030 transparent; 
			transform: translate(100%, 120%);
			position: absolute;
			top: 34px;
			right: 25px;
		}

		.kl-tooltip-kamban {
		    text-align: center;
			position: absolute;
			background: #323030;
			border-radius: 10px;
			top: 125%;
			max-height: 400px;
			width: 220px;
			right: -35px;
			z-index: 2;
			overflow-y: auto;
			padding: 10px;
			color: white;
		}

		.kl-tooltip-kamban li {
			color: #f0f0f0;
		}

		.kl-arrow-kamban:after {
			content: "";
			width: 0;
			height: 0;
			border: solid;
			border-width: 0 10px 10px 10px;
			border-color: transparent transparent #323030 transparent;
			transform: translate(100%, 120%);
			position: absolute;
			top: 81%;
			right: 33px;
		}

		.kl-d-none {
			display: none;
		}

		.event {
			display: flex;
			align-items: center;
			padding: 10px 0;
			border-bottom: 1px solid #ddd;
		}

		.event:last-child {
		border-bottom: none;
		}

		.event-logo {
		width: 50px;
		height: 50px;
		object-fit: cover;
		border-radius: 50%;
		margin-right: 20px;
		}

		.event-details {
		display: flex;
		flex-direction: column;
		}

		.event-name {
			font-size: 0.8em;
			font-weight: bold;
			color: white !important;
			margin: 0px;
		}

		.event-dates {
			font-size: 0.9em;
			color: #666;
			margin-top: 0px;
		}
		
		.<?= $kunik ?>{
			background: #2d2b2b; 
			padding: 10px;
		}

		#title{
			font:bold 48pt "Trebuchet MS";
			text-align: center;
			color: #F95;
			letter-spacing: -2px;
		}

		#title span{
			font-weight: normal;
			color: #BBB;
		}

		#tags{
			-webkit-border-radius: 10px;
			-moz-border-radius: 10px;
			border-radius: 10px;
			padding: 10px;
			background: #121010;
			margin: auto;
		}

		#tag-typer{
			outline: none;
			border: none;
			padding: 6px;
			margin: 3px;
			margin-right: -25px;
			width: 100px;
			background-color: transparent;
			font-size: 14px;
		}

		#role{
			-webkit-border-radius: 10px;
			-moz-border-radius: 10px;
			border-radius: 10px;
			padding: 10px;
			background: #121010;
			margin: auto;
		}

		#role-typer{
			outline: none;
			border: none;
			padding: 6px;
			margin: 3px;
			margin-right: -25px;
			width: 140	px;
			background-color: transparent;
			font-size: 14px;
		}

		.tag{
			display: inline-block;
			background: #4445;
			color: #FFF;
			padding: 5px 10px;
			margin:2px 2px 2px 20px;
			font: normal 16px sans-serif;
			position: relative;
			cursor: default;
			box-shadow:1px 1px 0 rgba(0,0,0,.2);
			-webkit-transform-origin:0% 50%;
			-webkit-animation: swing 1s;
			-o-animation: swing 1s;
			animation: swing 1s ;
		}

		.tag:before{
			content: "";
			position: absolute;
			width: 0;
			background: inherit;
			height: 29px;
			border: 10px solid #121010;
			border-right-color: transparent;
			-webkit-border-radius: 10px 0 0 10px;
			-moz-border-radius:10px a0 0 10px;
			border-radius:10px 0 0 10px;
			left: -20px;
			top: 0;
		}

		.tag:after{
			content: "";
			width: 6px;
			height: 6px;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			background: #FFF;
			position: absolute;
			left: -3px;
			top: 12px;
			box-shadow:inset 1px 1px 0 #CCC;
		}

		.tag .deleteTag,.deleteRole{
			position: absolute;
			left: -5px;
			top: 1px;
			z-index: 3;
			font-size: larger;
			visibility: hidden;
		}

		.tag:hover .deleteTag{
			visibility: visible;
		}

		.tag:hover .deleteRole{
			visibility: visible;
		}

		.tag:hover:after {
			visibility: hidden;
		}

		.deleteTag:hover {
			color: red;
		}

		.deleteRole:hover {
			color: red;
		}

		/* Chrome, Safari, Opera */
		@-webkit-keyframes swing
		{
			0%   {-webkit-transform: rotate(100deg)}
			25%  {-webkit-transform: rotate(-25deg)}
			50%  {-webkit-transform: rotate(15deg)}
			100% {-webkit-transform: rotate(0deg)}
		}


		@keyframes swing
		{
			0%   {-webkit-transform: rotate(-200)}
			25%  {-webkit-transform: rotate(-70)}
			50%  {-webkit-transform: rotate(-185)}
			100% {-webkit-transform: rotate(-180)}
		}

			/* Modal Custom Styles */
	.event-container {
		display: flex;
		gap: 20px;
	}

	.event-image {	
		color: #ffffff;
	}

	.event-image img {
		object-fit: cover;
		border-radius: 10px;
	}

	.event-details {
		flex: 1;
		display: flex;
		flex-direction: column;
		color: #ffffff;
	}

	h2 {
		font-size: 24px;
		margin-bottom: 15px;
	}

	.event-time {
		display: flex;
		align-items: center;
		gap: 10px;
		margin-bottom: 15px;
	}

	label {
		font-size: 14px;
		margin-right: 5px;
	}

	input[type="datetime-local"] {
		background-color: #242B4A;
		color: #ffffff;
		border: 1px solid #555;
		border-radius: 5px;
		padding: 5px;
	}

	.timezone {
		margin-left: auto;
		font-size: 12px;
	}

	.event-location button,
	.event-description button {
		background-color: #2D2F48;
		color: #ffffff;
		border: none;
		border-radius: 5px;
		padding: 10px;
		margin-bottom: 10px;
		cursor: pointer;
	}

	.event-options {
		display: flex;
		gap: 15px;
		margin-bottom: 15px;
	}

	.option {
		flex: 1;
		display: flex;
		align-items: center;
		justify-content: space-between;
	}

	.option input[type="text"],
	.option input[type="checkbox"] {
		background-color: #242B4A;
		color: #ffffff;
		border: 1px solid #555;
		border-radius: 5px;
		padding: 5px;
	}

	.create-event-button,.create-project-button {
		background-color: #0091c6;
		color: #ffffff;
		border: none;
		border-radius: 5px;
		padding: 10px;
		cursor: pointer;
		align-self: flex-start;
	}

    /* modal createevent */
	.modal-boostrap-create-event {
		background: linear-gradient(90deg, rgba(27,27,27,1) 35%, rgba(45,43,43,1) 100%);
	}

	.modal-boostrap-create-event .modal-content{
		background-color: transparent !important;
	}

 
	.modal-boostrap-create-event .modal-body,.modal-footer{
		background-color: transparent;
		box-shadow: 0 3px 9px rgba(0,0,0,.5);
	}
	

	.modal-boostrap-create-event .modal-header {
		display: flex;
		align-items: center;
		justify-content: space-around;
		padding-bottom: 0;
		color: white;
		position: relative;
		padding-bottom: 15px;
	}

	.modal-boostrap-create-event .modal-header span {
		color: white;
	}

	.modal-boostrap-create-event .modal-header h5 {
		font-size: 20px;
	}

	.modal-boostrap-create-event .modal-header button.close {
		position: absolute;
		right: 15px;
		top: 18px;
		opacity: .75;
	}

	.modal-boostrap-create-event .modal-header::before,
	.modal-boostrap-create-event .modal-header::after {
		content: none;
	}

	.modal-boostrap-create-event .modal-header .tab-item {
		margin: 2px 5px 0px 5px;
		padding-bottom: 10px;
		cursor: pointer;
		text-transform: none;
		text-align: center;
		font-size: 17px;
		text-decoration: none;
	}

	.modal-boostrap-create-event .modal-body .link-to-doc-content {
		display: flex;
		align-items: center;
		justify-content: flex-end;
	}

	.modal-boostrap-create-event .modal-body a.link-to-doc {
		background-color: #1890FF;
		padding: 8px 15px;
		font-weight: bold;
		color: white;
		border-top-right-radius: 10px;
		border-top-left-radius: 10px;
		font-size: 1.38rem;
	}

	.modal-boostrap-create-event .modal-body a.link-to-doc i {
		margin-right: 5px;
	}

	.modal-boostrap-create-event .modal-header a:hover,
	.modal-boostrap-create-event .modal-header a.active {
		border-bottom: 1px solid #f3aa31;
	}

	.modal-boostrap-create-event .btn-submit {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
	}

	.modal-boostrap-create-event .btn-submit p {
		font-size: 13px;
		text-align: justify;
		font-weight: 400;
		flex: 0.70;
	}

	.modal-boostrap-create-event .btn-submit p a,
	.modal-boostrap-create-event .btn-submit p b {
		font-weight: bold;
	}

	.modal-boostrap-create-event .btn-submit p a {
		color: #1890FF;
	}

	.modal-boostrap-create-event #tabQuestion h5 {
		text-align: center;
		font-size: 14px;
		text-transform: none;
		font-weight: 500;
	}

	.modal-boostrap-create-event #tabQuestion h5 a {
		font-weight: bold;
	}

	.modal-boostrap-create-event .btn-submit button {
		min-width: 100px;
	}

	.modal-boostrap-create-event .form-group textarea {
		height: 150px;
	}

	.modal-boostrap-create-event .form-group .check-value {
		display: flex;
		align-items: center;
		justify-content: flex-start;
	}

	.modal-boostrap-create-event .form-group .check-value .form-check:not(:first-child) {
		margin-left: 10px;
	}


	.inputContainerCoEvent {
		background-color: #2d2b2b;
		color : white;
		padding: 10px 20px;
		border-radius: 8px;
		display: flex;
		flex-direction: column;
	}

	.inputContainerCoEvent  textarea{
		background-color: #2d2b2b ;
		color : white ;
	}

	.inputContainerCoEvent button{
		background-color: #0091c6 !important;
	}

	.inputContainerCoEvent .btn-remove-element{
		background-color: red !important;
	}

	.inputContainerCoEvent  .select2-container-multi .select2-choices .select2-search-field input  {
		background-color: #2d2b2b !important ;
		color : white ;
	}

	.inputContainerCoEvent  .select2-container-multi .select2-choices{
		background-color: #2d2b2b !important ;
	}


	.inputContainerCoEvent  input {
		background-color: #2d2b2b ;
		color : white ;
	}

	.inputContainerCoEvent  select {
		background-color: #2d2b2b !important;
		color : white !important;
	}

	.inputContainerCoEvent  textarea{
		height: 80px ;
	}

	.inputContainerCoEvent .select2-container,.select2-choices {
		background-color: #2d2b2b ;
		color : white;
		z-index: 999999;
	}

	.event-card {
		background-color: #2c2c2e;
		border-radius: 8px;
		padding: 20px;
		color: white ;
		justify-content: space-between;
		align-items: center;
		box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
    }

	.warningStartDate,.warningEndDate {
		color: red !important;
	}

	.coevent-program{
		font-family: Arial, sans-serif;
	}

	.modal-personnalise {
		width: 60%; 
		height: 80vh; 
	}

	.modal-personnalise-project{
		width: 40%; 
		height: 80vh; 
	}

    @media only screen and (max-width: 768px) {
		.modal-dialog.modal-personnalise.modal-dialog-centered.co-resposnive {
			width: auto !important;
		}

		.modal-dialog.modal-personnalise-project.modal-dialog-centered.co-resposnive {
			width: auto !important;
		}

		.event-detail.co-timeline-parent {
			padding-left: 2px;
			margin-right: 5px;
		}

		.kl-stats-statistique,.kamban-title{
			margin-top: 10px;
		}

		.co-into-content {
			display: block;
		}	

		.kl-detail-footer{
			padding: 5px;
		}
	
		.messageOceco {
        	font-size: 0.8em;
    	}
}


	.kamba-title {
		grid-column: span 2;
		gap: 20px;
		cursor: pointer;
	}

	.kamban-statistique {
		display: flex;
		background: #121010;
		justify-content: space-between;
		align-items: center;
		border-radius: 8px;
		padding: 10px;
		color: white;
	}

	.kamban-statistique,.activation-oceco h3 {
		flex-grow: 1;
		text-align: center;
	}

	.kamban-statistique,.activation-oceco button {
		margin-left: auto;
	}

	.activation-oceco {
		background: #121010;
		justify-content: space-between;
		align-items: center;
		border-radius: 8px;
		padding: 10px;
		color: white;
	}



    .messageOceco {
        font-size: 1.4em;
    }

	.messageOceco span {
		color: white;
	}

	.messageOceco a {
		color: white;
		font-weight: bold;
		text-decoration: none;
		transition: color 0.3s ease-in-out;
	}

	.messageOceco a:hover {
		color: white;
		text-decoration: none;
	}

	.title-role{
		width : 100%;
		text-align : center;
	}


	.custom-btn {
		width: 130px;
		height: 40px;
		color: #68ca91;
		border-radius: 5px;
		margin: 5px;
		padding: 10px 25px;
		font-family: 'Lato', sans-serif;
		font-weight: 500;
		background: transparent;
		cursor: pointer;
		transition: all 0.3s ease;
		position: relative;
		display: inline-block;
		box-shadow:inset 2px 2px 2px 0px #68ca91,
		7px 7px 20px 0px rgba(0,0,0,.1),
		4px 4px 5px 0px rgba(0,0,0,.1);
		outline: none;
	}

	.btn-3 {
		background: #121010;
		width: 130px;
		height: 40px;
		line-height: 42px;
		padding: 0;
		border: none;
	}
	.btn-3 span {
		position: relative;
		display: block;
		width: 100%;
		height: 100%;
	}
	.btn-3:before,
	.btn-3:after {
		position: absolute;
		content: "";
		right: 0;
		top: 0;
		background: #e54e5e;
		transition: all 0.3s ease;
	}
	.btn-3:before {
		height: 0%;
		width: 2px;
	}
	.btn-3:after {
		width: 0%;
		height: 2px;
	}
	.btn-3:hover{
		background: transparent;
		box-shadow: none;
	}
	.btn-3:hover:before {
		height: 100%;
	}
	.btn-3:hover:after {
		width: 100%;
	}
	.btn-3 span:hover{
		color: #e54e5e;
	}
	.btn-3 span:before,
	.btn-3 span:after {
		position: absolute;
		content: "";
		left: 0;
		bottom: 0;
		background: #e54e5e;
		transition: all 0.3s ease;
		border-radius: 5px;
	}
	.btn-3 span:before {
		width: 2px;
		height: 0%;
	}
	.btn-3 span:after {
		width: 0%;
		height: 2px;
	}
	.btn-3 span:hover:before {
		height: 100%;
	}
	.btn-3 span:hover:after {
		width: 100%;
	}

	.roleTag{
		cursor: pointer;
	}

	#calendarMD,#calendarSM {
        padding: 20px 10px 10px;
        background-color: #161b22;
        border-radius: 5px;
        font-size: 12px;
        position: relative;
        overflow-x: auto;
    }

	.calendar-header{
    	text-transform: capitalize;
    	font-size: 17px;
		color: white;
		padding: 0 5px 5px 5px;
	}

	.calendar-sm{
		justify-content: center;
	}

    .calendar-container {
        display: flex;
        padding-top: 20px;
        margin-bottom: 10px;
    }

	.btn-swipe-calendar {
        color: #00FF7F;
        background-color: transparent;
        border-color: #00FF7F;
        font-weight: 700;
        transition: all 0.3s ease-in-out;
        position: relative;

    }

    .months-header {
        display: flex;
        justify-content: flex-start;
        padding: 5px 10px;
        font-weight: bold;
        margin-left: 60px;
        font-size: 14px;
        color: #fff;
        position: relative;
    }

    .month-indicator {
        flex: 0 0 auto;
        text-align: center;
        position: absolute;
        top: 0;
        transform: translateX(-50%);
        white-space: nowrap;
    }
	
	.weekdays-column {
        display: flex;
        flex-direction: column;
        margin-right: 5px;
    }

    .weekday {
        height: 15px;
        width: 25px;
        color: #768390;
        font-size: 10px;
        text-align: right;
        padding-right: 5px;
        margin-bottom: 3px;
    }

    .week-column {
        display: flex;
        flex-direction: column;
        margin-right: 3px;
        position: relative;
        /* Pour positionner le mois */
    }

    .day {
        width: 15px;
        height: 15px;
        margin-bottom: 3px;
        border-radius: 2px;
        cursor: pointer;
		border: 1px solid black;
    }

	.contribution-count {
        margin-bottom: 5px;
        color: #c9d1d9;
        font-size: 14px;
    }

    .legend {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        gap: 5px;
        /* margin-top: 5px; */
        color: #768390;
        font-size: 12px;
    }

    .legend-color {
        width: 10px;
        height: 10px;
        border-radius: 2px;
    }

    .month-janv {
        left: 10px !important;
    }

	.calendar-navigation{
		display:flex;
	}
	

</style>

<style id="cojazz<?= $kunik ?>"></style>
	<div id="block-cojazz" class="<?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>" attrId="<?= $blockKey ?>">
		<div class="kl-dashboard">
			<!-- Section des cartes de statistiques -->

			<?php if ( !isset($oceco["organizationAction"]) && ( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) ): ?>
					<div class="kl-title kamban-title pannelActivation">				
						<div class="activation-oceco " >				
							<div class="title-role">
								<h3> <?= Yii::t("cms", "Activate Oceco: your ally for efficient task and action management!")?> </h3>
								<span> <?= Yii::t("cms", "Oceco is a tool designed to structure and optimize the management of tasks and actions within your organization. Whether you're tracking project progress, assigning responsibilities or ensuring better coordination, Oceco lets you centralize all your actions in one place. Thanks to an intuitive interface and functionalities tailored to your needs, you'll gain in clarity, efficiency and collaboration.")?> </span>
							</div>
							<div class="title-role">
								<button class="btn-activation custom-btn btn-3" ><span> <?= Yii::t("cms", "Activate now")?> </span></button>
							</div>
						</div>
					</div>
			<?php endif; ?>

			
			<div class="kl-title kamban-title pannelConfigPole" <?php if ( isset($oceco["pole"]) || !isset($oceco["organizationAction"]) ) : ?> style="display: none;"<?php endif; ?> >				
				<div class="activation-oceco " >				
					<div class="title-role">
						<h3> <?= Yii::t("cms", "Choose your organization!")?> </h3>
					</div>
					<div class="kl-stats-cards">
						<div class="kl-card">
							<div class="description-text"><span> <?= Yii::t("cms", "Project mode is designed to manage independent initiatives. Each project comprises actions to be carried out, assigned, monitored and validated. It's a simple, structured way of managing tasks autonomously, with the option of remunerating actions once they've been validated.")?> </span></div>
							<div><img class="costum-img-preview" src="<?= $imgProjectOrga ?>" style="width:100%;object-fit: contain;" /></div>
							<div><button class="btn-project custom-btn btn-3" ><span><?= Yii::t("cms", "Project")?></span></button></div>
						</div>
						<div class="kl-card">
							<div class="description-text"><span> <?= Yii::t("cms", "Pole mode is ideal if you have several projects linked to the same theme. For example, a Garden Cluster can group together beekeeping, vegetable gardening and composting projects, facilitating organization and collaboration. ")?> </span></div>
							<div><img class="costum-img-preview" src="<?= $imgPoleOrga ?>" style="width:100%;object-fit: contain;" /></div>
							<div><button class="btn-pole custom-btn btn-3" ><span>Pole</span></button></div>
						</div>
					</div>	
				</div>
			</div>
			
				
			<div class="kl-title pannelPole" <?php if ( !isset($oceco["pole"]) || !$oceco["pole"]): ?>style="display: none;"<?php endif; ?>  data-toggle="collapse" data-target="#collapsePole" aria-expanded="true" aria-controls="collapsePole">								
				<div class="kl-statistique">
					<h3>Pole</h3>
				</div>
			</div>
			
			<div class="kl-title collapse" id="collapsePole">								
				<div class="kl-statistique">
					<label for="tag-typer">
					<div id="tags">
						<?php foreach ($pole as $tag): ?>
							<span class="tag poleTag" data-tag="<?= $tag ?>" ><?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId)) { ?><span data-tag="<?= $tag ?>" class="deleteTag">&times;</span><?php } ?><?= ucfirst($tag) ?></span>
						<?php endforeach; ?>
						<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
							<input id="tag-typer" type="text" placeholder="Ajouter pole..."/>
						<?php } ?>
					</div>
					</label>
				</div>
			</div>

			<div class="kl-title kamban-title">								
				<div class="kamban-statistique">
					<div class="title-role" id="collapseRoleParent" data-toggle="collapse" data-target="#collapseRole" aria-expanded="true" aria-controls="collapseRole"> 
						<h3> <?php echo Yii::t("cms", "Organize your community")?> </h3>
						<span> <?php echo Yii::t("cms", "Establish roles in your community")?> </span>
					</div>
					
					<div class="btnKambanRole" style="position: relative;">
						<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
							<button id="btnKambanRole" type="button" data-toggle="tooltip" title="Gérer les rôles" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon btn-default">
								<i class="fa fa-trello"></i>
							</button>
						<?php } ?>
						<div class="kl-tooltip-kamban" style='display:none'>
							<span> <?php echo Yii::t("cms", "Maximize your team's efficiency by assigning the right members to new or existing roles. A clear division of responsibilities on Oceco guarantees smooth collaboration and faster project completion!")?> </span>
						</div>
						<div class="kl-arrow-kamban hidden-xs" style='display:none'></div>
					</div>
				</div>
			</div>

			<div class="kl-title collapse" id="collapseRole" >								
				<div class="kl-statistique">
					<label for="role-typer">
					<div id="roles">
						<?php foreach ($allRoles["uniqueRoles"] as $role): ?>
							<span class="tag roleTag" data-role="<?= $role ?>" ><?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?><span data-role="<?= $role ?>" class="deleteRole">&times;</span><?php } ?><?= ucfirst($role) ?></span>
						<?php endforeach; ?>
						<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
							<input id="role-typer" type="text" placeholder="<?php echo Yii::t("cms", "Add role") ?>..."/>
						<?php } ?>
					</div>
					</label>
				</div>
			</div>

			<div id="cojazzPannelBoard" class="kl-title pannelBord" <?php if ( count($oceco) <= 1 ): ?> style="display: none;"<?php endif; ?> data-toggle="collapse" data-target="#collapseTableau" aria-controls="collapseTableau">								
				<div class="kl-statistique">
					<h3> <?php echo Yii::t("cms", "Dashboard")?> </h3>
				</div>
			</div>

			<div class="kl-title collapse" id="collapseTableau" class="collapse in" >
				<div class="kl-stats-cards">
					<div class="kl-card">
						<div class="kl-flex kl-justify-between kl-nb-view" >
							<div>
								<h3> <?php echo Yii::t("cms", "Project")?> </h3>
								<p class="kl-value"><?= $countAllProjectChild ?></p>
							</div>
							<div class="kl-parent-icon">
								<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
							</div>
						</div>
						<div class="kl-detail-footer cojazzActiveProject">
							<div>
								<span class="kl-change kl-positive cojazzNumberActiveProject"> <?= count($allProjectActive) ?> <?php echo Yii::t("cms", "Active")?></span>
							</div>
							<div style="position: relative;">
								<?php if ( $countAllProjectChild > 0): ?>
									<button data-id='Project' class="kl-btn-see-more Project see-more1 social-icon--project-event">
										<?php echo Yii::t("cms", "See more")?>
									</button>
									<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
										<button data-type='Project' class="kl-create createProject createNew social-icon--project-event">
											+
										</button>
										<div class="kl-tooltip-kamban tooltip-project" style='display:none'>
										<span> <?php echo Yii::t("cms", "Click here to create project")?> </span>
										</div>
										<div class="kl-arrow-kamban arrow-project hidden-xs" style='display:none'></div>
									<?php } ?>
								<?php else: ?>
									<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
										<button data-type='Project' class="kl-create createProject createNew social-icon--project-event">
											+
										</button>
										<div class="kl-tooltip-kamban tooltip-project" style='display:none'>
										<span> <?php echo Yii::t("cms", "Click here to create project")?> </span>
										</div>
										<div class="kl-arrow-kamban arrow-project hidden-xs" style='display:none'></div>
									<?php } ?>
								<?php endif; ?>
								<div class="kl-tooltip kl-d-none">
									<ul style="list-style: none; padding-left: 10px;">
									<?php foreach ($allProjectActive as $project): ?>
										<li>
											<div class="projectActiveList" data-id="${project.id}"  style="padding:5px;display: flex ;border-bottom: 1px solid #fff;justify-content: start; align-items: center;">
												<div>
													<img src="<?= $project['profilImageUrl'] ?? $imgProjectDefault ?>"  onerror="this.src='<?= $imgProjectDefault ?>';" style="width:30px; height: 30px; border-radius: 50%; object-fit: cover;" />
												</div>
												<div class="projectActiveName" style="cursor: pointer;display: flex; align-items: center; padding-left: 10px; font-size: 14px" data-slug="<?= $project['slug'] ?>" ><?= htmlspecialchars($project['name'], ENT_QUOTES, 'UTF-8') ?></div>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
								</div>
								<div class="kl-arrow kl-d-none"></div>
							</div>
						</div>
					</div>

					<div class="kl-card">
						<div class="kl-flex kl-justify-between kl-nb-view">
							<div>
								<h3> <?php echo Yii::t("cms", "Events")?> </h3>
								<p class="kl-value"><?= $countAllEventChild ?></p>
							</div>
							<div class="kl-parent-icon">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
						</div>
						<div class="kl-detail-footer">
							<div>
								<span class="kl-change kl-positive"><?= count($allCommingEvent) ?> En cours et a venir </span>
							</div>
							<div style="position: relative;">
								<?php if ( $countAllEventChild > 0): ?>
									<button data-id='event' class="kl-btn-see-more see-more2 social-icon--project-event">
										<?php echo Yii::t("cms", "See more")?>
									</button>
									<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
										<button data-type='Event' class="kl-create createEvent createNew social-icon--project-event">
											+
										</button>
										<div class="kl-tooltip-kamban tooltip-event" style='display:none'>
										<span> <?php echo Yii::t("cms", "Click here to create event")?> </span>
										</div>
										<div class="kl-arrow-kamban arrow-event hidden-xs" style='display:none'></div>
									<?php } ?>
								<?php else: ?>
									<?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
										<button data-type='Event' class="kl-create createEvent createNew social-icon--project-event">
											+
										</button>
										<div class="kl-tooltip-kamban tooltip-event" style='display:none'>
										<span> <?php echo Yii::t("cms", "Click here to create event")?> </span>
										</div>
										<div class="kl-arrow-kamban arrow-event hidden-xs" style='display:none'></div>
									<?php } ?>
								<?php endif; ?>
								<div class="kl-tooltip kl-d-none">
									<ul style="list-style: none; padding-left: 10px;">
									<?php foreach ($allCommingEvent as $event): ?>
										<li class="event">
											<img src="<?= $event['profilImageUrl'] ?? $imgEventDefault ?>"  onerror="this.src='<?= $imgEventDefault ?>';" alt="Logo Événement 2" class="event-logo">
											<div class="event-details projectActiveName" style="cursor: pointer;" data-slug="<?= $event['slug'] ?>">
												<h2 class="event-name"><?= htmlspecialchars($event['name'], ENT_QUOTES, 'UTF-8') ?></h2>
												<p class="event-dates">
													<?php
													// Vérification pour startDate
													if (isset($event['startDate']->sec)) {
														$startDateFormatted = (new DateTime())->setTimestamp($event['startDate']->sec)->format('d/m/Y');
													} else {
														$startDateFormatted = "Date invalide";
													}

													// Vérification pour endDate
													if (isset($event['endDate']->sec)) {
														$endDateFormatted = (new DateTime())->setTimestamp($event['endDate']->sec)->format('d/m/Y');
													} else {
														$endDateFormatted = "Date invalide";
													}

													// Affichage des deux dates
													echo htmlspecialchars($startDateFormatted . " - " . $endDateFormatted, ENT_QUOTES, 'UTF-8');
													?>
												</p>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
								</div>
								<div class="kl-arrow kl-d-none"></div>
							</div>
						</div>
					</div>

					<div class="kl-card">
						<div class="kl-flex kl-justify-between kl-nb-view">
							<div>
								<h3>Badge(s)</h3>
								<p class="kl-value"><?= count($allBadges) ?></p>
							</div>
							<div class="kl-parent-icon">
								<svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" fill="white" ><g id="Promoted"><path d="M44.0156,45.197l-.0071.02c-.7031,2.0046-1.5782,4.4995-3.9845,5.2809a4.2663,4.2663,0,0,1-1.32.2062c-1.7457,0-3.2378-.9764-4.675-1.9628l3.5859,12.0972a1.3021,1.3021,0,0,0,2.3555.3154L42.5434,57A1.3018,1.3018,0,0,1,43.9,56.4078l4.7954.9372a1.302,1.302,0,0,0,1.37-1.9417Z"/><path d="M24.5873,50.704a4.2677,4.2677,0,0,1-1.32-.2061c-2.2195-.7209-3.1333-2.8957-3.8139-4.8026l-5.7546,9.708a1.302,1.302,0,0,0,1.37,1.9417l4.7955-.9372A1.302,1.302,0,0,1,21.2209,57l2.5728,4.1539a1.3019,1.3019,0,0,0,2.3552-.3154L29.855,48.3362c-.143.097-.2808.1912-.4012.2741C27.9575,49.64,26.41,50.704,24.5873,50.704Z"/><path d="M56.2544,24.71c0-2.4131-4.7523-4.0453-5.4594-6.2227-.7328-2.2565,2.1271-6.3656.76-8.2439-1.3807-1.8971-6.1814-.44-8.0784-1.82-1.8784-1.3672-1.9678-6.39-4.2244-7.123-2.1775-.7071-5.1933,3.281-7.6064,3.281S26.2173.5934,24.04,1.3005c-2.2566.7326-2.3461,5.7557-4.2244,7.1228-1.897,1.3805-6.6977-.0769-8.0784,1.82-1.367,1.8782,1.4927,5.9874.76,8.244-.707,2.1775-5.4595,3.81-5.4595,6.2229s4.7525,4.0453,5.46,6.2226c.7327,2.2565-2.1269,6.3656-.76,8.2439,1.3806,1.8971,6.1812.44,8.0783,1.82,1.8782,1.3671,1.9677,6.39,4.2244,7.123,2.1775.707,5.1932-3.281,7.6065-3.281s5.4287,3.988,7.6061,3.2811c2.2564-.7328,2.346-5.7559,4.2243-7.1228,1.8971-1.3806,6.6977.0768,8.0784-1.82,1.3671-1.8783-1.4926-5.9874-.76-8.244C51.5019,28.7557,56.2544,27.1235,56.2544,24.71ZM31.6521,37.8315A13.1212,13.1212,0,1,1,44.7733,24.71,13.1213,13.1213,0,0,1,31.6521,37.8315Z"/><path d="M37.4145,21.5491H34.9513a1.3375,1.3375,0,0,1-1.2721-.9242l-.7612-2.3427a1.3376,1.3376,0,0,0-2.5442,0l-.7611,2.3427a1.3376,1.3376,0,0,1-1.2721.9242H25.8774a1.3375,1.3375,0,0,0-.7862,2.42l1.9928,1.448a1.3376,1.3376,0,0,1,.4858,1.4954l-.7612,2.3426A1.3376,1.3376,0,0,0,28.8669,30.75L30.86,29.3023a1.3373,1.3373,0,0,1,1.5724,0L34.4249,30.75a1.3376,1.3376,0,0,0,2.0582-1.4955l-.7612-2.3426a1.3378,1.3378,0,0,1,.486-1.4954l1.9928-1.448A1.3375,1.3375,0,0,0,37.4145,21.5491Z"/></g></svg>
							</div>
						</div>
						<div class="kl-detail-footer">
							<div>
								<!-- <span class="kl-change kl-positive">+2.15% Last month</span> -->
							</div>
							<div style="position: relative;">
								<?php if (count($allBadges) > 0): ?>	
									<button data-id='Badge' class="kl-btn-see-more Project see-more1 social-icon--project-event">
										<?php echo Yii::t("cms", "See more")?>
									</button>
								<?php endif; ?>
								<div class="kl-tooltip kl-d-none">
									<ul style="list-style: none; padding-left: 10px;">
									<?php foreach ($allBadges as $badges): ?>
										<li>
											<div style="display: flex ;border-bottom: 1px solid #fff;justify-content: start; align-items: center;">
												<div>
													<img src="<?= $badges['profilImageUrl'] ?>"  onerror="this.src='<?= $imgBadgeDefault ?>';" style="width:30px; height: 30px; border-radius: 50%; object-fit: cover;" />
												</div>
												<div style="display: flex; align-items: center; padding-left: 10px; font-size: 14px"><?= htmlspecialchars($badges['name'], ENT_QUOTES, 'UTF-8') ?></div>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
								</div>
								<div class="kl-arrow kl-d-none"></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="kl-title pannelGraph" <?php if ( count($oceco) <= 1 ): ?> style="display: none;"<?php endif; ?> data-toggle="collapse" data-target="#collapseGraph" aria-controls="collapseGraph">								
				<div class="kl-statistique">
					<h3> <?php echo Yii::t("cms", "Evolution and breakdown of shares by status")?> </h3>
				</div>
			</div>

			<?php if (array_sum($statPerStatus) > 0) : ?>	
			<div class="kl-title collapse" id="collapseGraph" class="collapse in" >
				<div class="kl-stats-cards">
					<div class="kl-card">
						<h3> <?php echo Yii::t("cms", "Action progression statistics") ?></h3>
						<div class="kl-map" style="padding: 10px;">
							<canvas class="" id="pieChartProgression<?= $kunik ?>"></canvas>
						</div>
					</div>

					<div class="kl-card">
						<h3><?php echo Yii::t("cms", "Action statistics by status") ?></h3>
						<div class="kl-chart" style="padding: 10px;">
							<canvas class="" id="pieChartStatus<?= $kunik ?>"></canvas>
						</div>
					</div>
				</div>
				<div class="kl-stats-cards">
					<div class="kl-card">
						<h3> <?php echo Yii::t("cms", "Daily activity report") ?></h3>
						<div class="kl-map" style="padding: 10px;">
							<div class="calendar-container calendar-md mobile-hidde">
								<div class="calendar" id="calendarMD"></div>
							</div>
							<div class="calendar-container calendar-sm mobile-show">
								<div class="calendar" id="calendarSM"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php else: ?>	
				<div class="kl-title collapse" id="collapseGraph" class="collapse in">								
					<div class="kl-oceco-statistique messageOceco text-center">
						<span> <?php echo Yii::t("cms", "Create your actions with") ?> </span> 
						<a href="https://oce.co.tools/" target="_blank"> oceco </a> 
						<span> <?php echo Yii::t("cms", "now to track your progress and get a clear picture of your achievements.") ?> </span>
					</div>
				</div>
			<?php endif; ?>


		</div>
	</div>

	<div class="modal fade modal-boostrap-create-event" id="openModalCreateEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;overflow-y: auto;">
		<div class="modal-dialog modal-personnalise modal-dialog-centered co-resposnive" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?= Yii::t('cms', 'Create an event') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body col-xs-12">
					<div class="row">
						<div class="event-container  col-xs-12 col-md-4 col-lg-4">
							<div class="event-image">
								<div id="createvent-image-input"></div>
							</div>
						</div>
						<div class="col-xs-12 col-md-8 col-lg-8 create-event-details"></div>
					</div>
				</div>
				<div class="modal-footer col-xs-12">
					<button class="create-event-button" data-id="<?= $blockKey ?>" ><?= Yii::t('cms', 'Save') ?> <i class="fa fa-arrow-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade modal-boostrap-create-event" id="openModalCreateProject" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="z-index: 999999;overflow-y: auto;">
		<div class="modal-dialog modal-personnalise-project modal-dialog-centered co-resposnive" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?= Yii::t('cms', 'Create a project') ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="project-container col-xs-12 col-md-12 col-lg-12">
							<div class="event-image ">
								<div id="createproject-image-input"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer col-xs-12">
					<button class="create-project-button" data-id="<?= $blockKey ?>" ><?= Yii::t('cms', 'Save') ?> <i class="fa fa-arrow-circle-right"></i></button>
				</div>
			</div>
		</div>
	</div>

	<script>

		$(document).ready( function() {
			$("#tag-typer").keypress(function (event) {
				var key = event.which;
				if (key == 13 || key == 44) {
					event.preventDefault();
					var tag = $(this).val().trim();
					if (tag.length > 0) {
						let tags = [];
						$(".poleTag").each(function () {
							tags.push($(this).data("tag"));
						});

						if (!tags.includes(tag)) {
							let tagUpper = tag.charAt(0).toUpperCase() + tag.slice(1);
							$("<span class='tag poleTag' data-tag='" + tag + "' style='display:none'>"
								+ "<span class='deleteTag'>&times;</span> " + tagUpper + " </span>")
								.insertBefore(this)
								.fadeIn(100);
							$(this).val("");

							tags.push(tag);
							var editTags = {
								collection : costum.contextType,
								id : costum.contextId,
								path : "oceco.tags",
								value : tags                                                          
							}

							dataHelper.path2Value(editTags, function (params) {
								toastr.success(tradCms.editionSucces);
							});
						}else{
							toastr.error("pole existant");
						}
					}
				}
			});
		
			$("#tags").on("click", ".deleteTag", function() {
				const $poleProject = <?= json_encode($poleProject) ?>;
				if ($poleProject.includes($(this).data("tag"))){
					toastr.error("Action impossible car ce pole est encore utiliser");
				} else {
					$(this).parent("span").fadeOut(100);
					$(this).parent("span").remove();
					let tags = [];
					$(".poleTag").each(function() {
						tags.push($(this).data("tag"));
					});
					var editTags = {
						collection : costum.contextType,
						id : costum.contextId,
						path : "oceco.tags",
						value : tags                                                          
					}

					dataHelper.path2Value(editTags, function (params) {
						toastr.success(tradCms.editionSucces);
					});
				}
				
			});

			$("#role-typer").keypress( function(event) {
					var key = event.which;
					let roles = [];
					$(".roleTag").each(function() {
						roles.push($(this).data("role"));
					});

					if (key == 13 || key == 44){
					event.preventDefault();
					var role = $(this).val();
					if(role.length > 0){
						if (roles.includes(role)){
							toastr.error(tradCms.existantRole);
						} else {
							roleUpper = role.charAt(0).toUpperCase() + role.slice(1);
							$("<span class='tag roleTag' data-role='"+role+"' style='display:none'><span class='deleteTag'>&times;</span>"+roleUpper+" </span>").
							insertBefore(this).fadeIn(100);
							$(this).val("");
							roles.push(role);
							var editRoles = {
								collection : costum.contextType,
								id : costum.contextId,
								path : "roles",
								value : roles                                                          
							}

							dataHelper.path2Value(editRoles, function (params) {
								toastr.success(tradCms.editionSucces);
								$(".kl-tooltip-kamban").css('display','block');
								$(".kl-arrow-kamban").css('display','block');
								setTimeout(() => {
									$(".kl-tooltip-kamban").fadeOut(500);
									$(".kl-arrow-kamban").fadeOut(500);
								}, 10000);
								$("#roles").off("click").on("click", ".deleteRole", function() {
									var userRole =  <?= json_encode($allRoles["usersRoles"]) ?>;	
									if (userRole.includes($(this).data("role"))){
										toastr.error(tradCms.usedRole);
									} else {
										$(this).parent("span").fadeOut(100);
										$(this).parent("span").remove();
										let roles = [];
										$(".roleTag").each(function() {
											roles.push($(this).data("role"));
										});
										var editTags = {
											collection : costum.contextType,
											id : costum.contextId,
											path : "roles",
											value : roles                                                          
										}

										dataHelper.path2Value(editTags, function (params) {
											toastr.success(trasdCms.editionSucces);
										});
									}
				
								});
							});
						}	
					}
				}
			});

			$("#roles").off("click").on("click", ".deleteRole", function() {
				var userRole =  <?= json_encode($allRoles["usersRoles"]) ?>;	
				if (userRole.includes($(this).data("role"))){
					toastr.error("Action impossible car ce role est encore utiliser");
				} else {
					$(this).parent("span").fadeOut(100);
					$(this).parent("span").remove();
					let roles = [];
					$(".roleTag").each(function() {
						roles.push($(this).data("role"));
					});
					var editTags = {
						collection : costum.contextType,
						id : costum.contextId,
						path : "roles",
						value : roles                                                          
					}

					dataHelper.path2Value(editTags, function (params) {
						toastr.success(tradCms.editionSucces);
					});
				}
				
			});
		
		});

		if (costum.editMode){
		cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?> ;

		var cojazz = {
				configTabs : {
					general : {
						inputsConfig : [
							{
								type: "inputSimple",
								options: {
									name: "range",
									label: tradCms.numberOfDay,
								}
							},
							"backgroundColor"
						]
					},
					style : {
						inputsConfig : [
							"addCommonConfig"
						]
					},
					advanced: {
						inputsConfig: [
							"addCommonConfig"
						]
					}
				},
				onchange: function (path,valueToSet,name,payload,value,id){
					
				},
				afterSave: function(path,valueToSet,name,payload,value,id){
					if (name === 'parentType' || name === 'range'){
						cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
					}
				}
			}
			cmsConstructor.blocks.cojazz<?= $blockKey ?> = cojazz;

		}

		cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')
		str="";
		str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
		$("#cojazz<?= $kunik ?>").append(str);

		$(document).ready(function () {
			$(".kl-btn-see-more").on("click", function () {
				// Fermer toutes les autres tooltips sauf celle cliquée
				$(".kl-tooltip").not($(this).siblings(".kl-tooltip")).addClass("kl-d-none");
				$(".kl-arrow").not($(this).siblings(".kl-arrow")).addClass("kl-d-none");

				// Bascule de l'état de la tooltip et de la flèche correspondante
				const tooltip = $(this).siblings(".kl-tooltip");
				tooltip.toggleClass("kl-d-none");

				const arrow = $(this).siblings(".kl-arrow");
				arrow.toggleClass("kl-d-none");
			});
		});

		$(".projectActiveName").on("click", function(){
			const slug=$(this).data("slug");
			const url = baseUrl+"/#@"+slug;
			window.open(url);
		});

		var status = <?= $statPerStatus["total"] ?>;
		if ( status !== "0" ) {
			var statusChartData = [{
				data: [<?= $statPerStatus['todo'] ?>,<?= $statPerStatus['done'] ?>,<?= $statPerStatus['totest'] ?>,<?= $statPerStatus['tracking'] ?>,<?= $statPerStatus['next'] ?>,<?= $statPerStatus['discuter'] ?>],
				weight: 1,
				borderColor: '#444',
				datalabels : {
					anchor: 'end',
					align: 'end',
					color:['Red','Green','Blue','Yellow','Orange','Purple'],
					font : {
						size : 13,
						weight: 'bold'
					}
				},
				backgroundColor: ['#A9CEF4','#36494E','#000000','#7EA0B7','#597081','#393D3A'],

			}];

			var progressionChartData = [{
				data: [<?= $statPerStatus['done'] ?> ,<?= $statPerStatus['done'] ?> + <?= $statPerStatus['todo'] ?> + <?= $statPerStatus['totest'] ?> + <?= $statPerStatus['tracking'] ?> + <?= $statPerStatus['next'] ?> +<?= $statPerStatus['discuter'] ?>],
				weight: 1,
				borderColor: '#444',
				datalabels : {
					anchor: 'end',
					align: 'end',
					color:["A9CEF4","#36494E"],
					borderWidth: 0.1,
					font : {
						size : 13,
						weight: 'bold'
					}
				},
				backgroundColor: ["A9CEF4","#36494E"]

			}];

			var labelsConfig = [];
			if ( <?= $statPerStatus['todo'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "To do")?>");
			if ( <?= $statPerStatus['done'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "Done")?>");
			if ( <?= $statPerStatus['totest'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "To test")?>");
			if ( <?= $statPerStatus['tracking'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "Tracking")?>");
			if ( <?= $statPerStatus['next'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "Next")?>");
			if ( <?= $statPerStatus['discuter'] ?> > 0 ) labelsConfig.push("<?php echo Yii::t("cms", "To discuss")?>");

			var chart<?= $kunik ?> = document.getElementById('pieChartStatus<?= $kunik ?>').getContext('2d');
			window.myChart<?= $kunik ?> = new Chart(chart<?= $kunik ?>, {
				type: 'doughnut',
				data: {
					labels: labelsConfig,
					datasets: statusChartData
				},
				options: {
					scales: {
						y: { 
							beginAtZero: true
						},
					},
					legend: {
						display: true,
						labels: {
							fontColor: "white",
						}
					},
				}

			});

			var chartProgression<?= $kunik ?> = document.getElementById('pieChartProgression<?= $kunik ?>').getContext('2d');
			window.myChart<?= $kunik ?> = new Chart(chartProgression<?= $kunik ?>, {
				type: 'polarArea',
				data: {
					labels: [
						"<?php echo Yii::t("cms", "Done")?>",
						"<?php echo Yii::t("cms", "Total")?>",
					],
					datasets: progressionChartData
				},
				options: {
					scales: {
						y: { 
							beginAtZero: true
						}
					},
					legend: {
						display: true,
						labels: {
							fontColor: "white",
						}
					},
				}

			});
		}

		$(".createNew").on("click", function(e){
			$type = $(this).data("type");
			if ( $type === "Event"){
				var preferences = {
					isOpenData : true,
					isOpenEditional : true
				}
				var newEvent = {};
				dyFObj.setMongoId('events', function() {
						const newEventToAdd = {
							id : dyFObj.currentElement.id,
							public : true,
							scope:null,
							recurrency: false,
							costumSlug: costum.slug,
							costumEditMode: costum.editMode,
							preferences: preferences,
							key: "event",
							collection: "events",
							timeZone : moment.tz.guess()
						};

						Object.assign(newEvent, newEventToAdd);

						new CoInput({
							container: "#createvent-image-input",
							inputs: [
								{
									type: "inputFileImage",
									options: {
										name: "image",
										label: "Image",
										collection: "documents",
										defaultValue: "",
										canSelectMultipleImage : false,
										payload: {
											path: "logo",
										},
										icon: "chevron-down",
										class: "logoUploader inputContainerCoEvent",
										contentKey: "profil",
										uploadOnly: true,
										uploaderInitialized : true,
										endPoint: "",
										domElement: "profil",
									},
								},
								{
									type : "dateTimePicker",
									options: {
										name : "startDate",
										label: tradCms.startDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newStartDate",
									}
								},
								{
									type : "dateTimePicker",
									options: {
										name : "endDate",
										label: tradCms.endDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newEndDate",
									}
								},
								{
									type : "tags",
									options : {
										name : "tags",
										label : tradDynForm.keyWords,
										options : [""],
										defaultValue : "",
										class: "inputContainerCoEvent",
										icon : "chevron-down"
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "email",
										label : "E-mail",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "url",
										label : "URL",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								
							],
							onchange: function(name, value, payload) {
								if ( name === "image") {
									jsonHelper.setValueByPath(newEvent, "profilImageUrl", value);
								} else if ( name === "startDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment(); // Date et heure actuelles

									if (value.isBefore(currentDate)) {
										$(".warningStartDate").remove();
										$(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									} else {
										$(".warningStartDate").remove();
										var dateDebutConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
										jsonHelper.setValueByPath(newEvent, name, dateDebutConvertie);
									}
								} else if ( name === "endDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment();
									var startDate = moment(newEvent.startDate);
									var dateEndConvertie = value.tz(newEvent.timeZone);
									if (dateEndConvertie.isAfter(currentDate) && dateEndConvertie.isAfter(startDate)) {
										$(".warningEndDate").remove();
										jsonHelper.setValueByPath(newEvent, name, dateEndConvertie.format("YYYY-MM-DDTHH:mm:ssZ"));
									} else {
										$(".warningEndDate").remove();
										$(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + startDate.format("DD/MM/YYYY HH:mm") + " " + tradCms.and + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									}
								} else if ( name === "tags"){
									jsonHelper.setValueByPath(newEvent, name,value.split(','));
								} else {
									jsonHelper.setValueByPath(newEvent, name,value);
								}
							},
							onblur : function(name , value, payload){
							
							}
						});

						new CoInput({
								container: ".create-event-details",
								inputs: [
									{
										type : "inputSimple",
										options: {
											name : "name",
											label : trad.projectName +" *",
											defaultValue : "",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										}
									},
									{
										type : "select",
										options: {
											name : "type",
											label : tradDynForm.eventTypes +" *",
											options :  Object.keys(eventTypes).map(function(value) {
												return {
													label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
													value : value
												}
											}),
											icon : "chevron-down",
											class: "inputContainerCoEvent"
										}
									},
									{
										type : "finders",
										options: {
											name : "organizer",
											label: tradDynForm.whoorganizedevent,
											initType: ["projects", "organizations"],
											multiple: true,
											initMe: false,
											icon : "chevron-down",
											openSearch: true,
											initBySearch: true,
											class: "inputContainerCoEvent",
										}
									},
									{
										type : "formLocality",
										options: {
											name : "formLocality",
											label: tradCms.place,
											icon : "chevron-down",
											class: "inputContainerCoEvent",

										}
									},
									{
										type : "textarea",
										options: {
											name : "shortDescription",
											label : tradCms.shortDescription,
											defaultValue :"",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										},
									},
								],
								onchange: function(name, value, payload) {
									if ( name === "name" ){
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								},
								onblur: function(name, value, payload) {
									if ( name === "formLocality"){
											var address = [];
											var geo = [];
											var geoPosition = [];
											var addresses = [];
											value.forEach((location, index) => {
												if (location.center) {
													address = location.address;
													geo = location.geo;
													geoPosition = location.geoPosition;
												} else {
													addresses.push(location);
												}
											});
											jsonHelper.setValueByPath(newEvent, "address", address);
											jsonHelper.setValueByPath(newEvent, "geo", geo);
											jsonHelper.setValueByPath(newEvent, "geoPosition", geoPosition);
											jsonHelper.setValueByPath(newEvent, "addresses", addresses);
									} else {
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								}
						});
						$("#openModalCreateEvent").modal("show");
				});

				$(".create-event-button").off("click").on("click", function(){
						var idBlock =  $(this).data("id");
						delete newEvent.images;

						ajaxPost(
							null,
							baseUrl+"/co2/element/save",
							newEvent, 
							function (response) {  
								if (response.result){
									var events = {
										id: costum.contextId,
										collection: "events",
										path: "links.subEvents."+newEvent.id,
										value : {
											type : "events"
										},
										costumSlug: costum.slug,
										costumEditMode: costum.editMode                                                           
									}
									dataHelper.path2Value(events, function (params) {
										if (params.result){
											var attende = {
												id : newEvent.id,
												collection : "events",
												path : "links.attendees."+userId,
												value : {
													type : "citoyens"
												},
												costumSlug: costum.slug,
												costumEditMode: costum.editMode   
											}
											dataHelper.path2Value(attende, function (params) {
												$("#openModalCreateEvent").modal("hide");
												toastr.success(tradCms.editionSucces);
												refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']")
											});
										}
									});
								} else {
									toastr.error(response.msg);
								}                 
							}
						);
				});
			} else if ( $type === "Project") {
				var newProject = {};
				dyFObj.setMongoId('project', function() {
						const newProjectToAdd = {
							id : dyFObj.currentElement.id,
							public : true,
							scope:null,
							key: "project",
							collection: "projects",
						};

						Object.assign(newProject, newProjectToAdd);

						new CoInput({
							container: "#createproject-image-input",
							inputs: [
								{
									type: "inputFileImage",
									options: {
										name: "image",
										label: "Image",
										collection: "documents",
										defaultValue: "",
										canSelectMultipleImage : false,
										payload: {
											path: "logo",
										},
										icon: "chevron-down",
										class: "logoUploader inputContainerCoEvent",
										contentKey: "profil",
										uploadOnly: true,
										uploaderInitialized : true,
										endPoint: "",
										domElement: "profil",
									},
								},
								{
									type : "inputSimple",
									options: {
										name : "name",
										label : tradCms.eventName +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "finders",
									options: {
										name : "parent",
										label: tradDynForm.whoiscarrytheproject,
										initType: ["projects", "organizations"],
										multiple: true,
										initMe: false,
										icon : "chevron-down",
										openSearch: true,
										initBySearch: true,
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "formLocality",
									options: {
										name : "formLocality",
										label: tradCms.place,
										icon : "chevron-down",
										class: "inputContainerCoEvent",

									}
								},
								{
									type : "textarea",
									options: {
										name : "shortDescription",
										label : tradCms.shortDescription,
										defaultValue :"",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									},
								},
								{
									type : "tags",
									options : {
										name : "tags",
										label : "Pole",
										options : [""],
										defaultValue : "",
										class: "inputContainerCoEvent",
										icon : "chevron-down"
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "email",
										label : "E-mail",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "url",
										label : "URL",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								
							],
							onchange: function(name, value, payload) {
								if ( name === "name" ){
										jsonHelper.setValueByPath(newProject, name, value);
								} else if ( name === "image") {
									jsonHelper.setValueByPath(newProject, "profilImageUrl", value);
								} else if ( name === "tags"){
									jsonHelper.setValueByPath(newProject, name,value.split(','));
								} else {
									jsonHelper.setValueByPath(newProject, name,value);
								}
							},
							onblur : function(name , value, payload){
								if ( name === "formLocality"){
										var address = [];
										var geo = [];
										var geoPosition = [];
										var addresses = [];
										value.forEach((location, index) => {
											if (location.center) {
												address = location.address;
												geo = location.geo;
												geoPosition = location.geoPosition;
											} else {
												addresses.push(location);
											}
										});
										jsonHelper.setValueByPath(newProject, "address", address);
										jsonHelper.setValueByPath(newProject, "geo", geo);
										jsonHelper.setValueByPath(newProject, "geoPosition", geoPosition);
										jsonHelper.setValueByPath(newProject, "addresses", addresses);
								} else {
									jsonHelper.setValueByPath(newProject, name, value);
								} 
							}
						});

						$("#openModalCreateProject").modal("show");
				});

				$(".create-project-button").off("click").on("click", function(){
					var idBlock =  $(this).data("id");
					delete newProject.images;
					ajaxPost(
						null,
						baseUrl+"/co2/element/save",
						newProject, 
						function (response) {  
							if (response.result){
									$("#openModalCreateProject").modal("hide");
									toastr.success(tradCms.editionSucces);
									refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']")
							} else {
								toastr.error(response.msg);
							}      
						}
					);
				});
			}
		});

		$("#btnKambanRole").off().on("click", function(e){
			urlCtrl.openPreview("/view/url/costum.views.custom.kanban.kanban-roles", {"context":contextData, "options":{} });
		})

		$(".btn-activation").on("click", function(e){
			const activation = {
				id : costum.contextId,
				collection : costum.contextType,
				path: "oceco",
				value: {
					"organizationAction" : true,
					"projectAction" : true,
					"eventAction" : true,
					"commentsAction" : true,
					"memberAuto" : false,
					"memberValidationInviteAuto" : false,
					"contributorValidationInviteAuto" : false,
					"agenda" : false,
					"costum" : {
						"projects" : {
							"form" : {
								"geo" : false
							}
						},
						"events" : {
							"form" : {
								"geo" : false
							}
						},
						"actions" : {
							"form" : {
								"min" : false,
								"max" : false,
								"optionPodomoro" : false,
								"optionTask" : false
							}
						}
					},
					"spendView" : false,
					"notificationChat" : false,
					"membersAdminProjectAdmin" : false,
					"contributorAuto" : true,
					"attendeAuto" : true,
					"memberAddAction" : true,
					"spendNegative" : false,
					"subOrganization" : false,
					"milestonesProject" : false,
					"gitProject" : false,
					"organizationForms" : false,
					"projectForms" : false
				}
			}
			dataHelper.path2Value(activation, function (data) {
				if (data.result){
					$(".pannelActivation").slideUp(500);
					$(".pannelConfigPole").slideDown(500);
					toastr.success("<?= Yii::t("cms", "Activation success") ?>");
				} else {
					toastr.error("<?= Yii::t("cms", "Activation error") ?>");
				}
			});
		})

		$(".btn-pole").on("click", function(e){
			const activation = {
				id : costum.contextId,
				collection : costum.contextType,
				path: "oceco.pole",
				value: true
			}
			dataHelper.path2Value(activation, function (data) {
				if (data.result){
					$(".pannelConfigPole").slideUp(500);
					$(".pannelPole, .pannelBord, .pannelGraph").slideDown(500);
					
					toastr.success("<?= Yii::t("cms", "Pole mode active") ?>");
				} else {
					toastr.error("<?= Yii::t("cms", "Activation error") ?>");
				}
			});
		})

		$(".btn-project").on("click", function(e){
			const activation = {
				id : costum.contextId,
				collection : costum.contextType,
				path: "oceco.pole",
				value: false
			}
			dataHelper.path2Value(activation, function (data) {
				if (data.result){
					$(".pannelConfigPole").slideUp(500);
					$(".pannelBord, .pannelGraph").slideDown(500);
					toastr.success("<?= Yii::t("cms", "Project mode active") ?>");
				} else {
					toastr.error("<?= Yii::t("cms", "Activation error") ?>");
				}
			});
		})


		function setCalendarViews(data) {
			var calendar = $('#calendarMD');
			calendar.empty();

			var dataByDate = {};
			for (var date in data) {
				if (data.hasOwnProperty(date)) {
					dataByDate[date] = {
						date: date,
						nombre: data[date].nombre
					};
				}
			}

			var currentYear = new Date().getFullYear();
			var currentMonth = new Date().getMonth();

			var calendarContainer = $('<div class="calendar-container calendar-md"></div>');
			var weekdaysColumn = $('<div class="weekdays-column"></div>');
			var weekdays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
			weekdays.forEach(day => {
				weekdaysColumn.append(`<div class="weekday">${day}</div>`);
			});
			calendarContainer.append(weekdaysColumn);

			var startDate = new Date(currentYear, 0, 1);
			var endDate = new Date(currentYear, 11, 31);
			var firstDay = new Date(startDate);
			while (firstDay.getDay() !== 1) {
				firstDay.setDate(firstDay.getDate() - 1);
			}

			var totalWeeks = Math.ceil((endDate - firstDay) / (7 * 24 * 60 * 60 * 1000));
			var currentDate = new Date(firstDay);

			var contributionCount = 0;
			var monthsHeader = $('<div class="months-header"></div>');
			var monthPositions = {};

			for (var weekIndex = 0; weekIndex < totalWeeks; weekIndex++) {
				var weekColumn = $('<div class="week-column"></div>');
				for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
					var dateString = currentDate.toISOString().slice(0, 10); // Format YYYY-MM-DD
					var dayData = dataByDate[dateString];

					var className = dayData ? 'has-data' : 'empty';
					if (dayData) contributionCount++;

					var formattedDate = currentDate.toLocaleDateString("fr-FR", {
						day: "2-digit",
						month: "long",
						year: "numeric"
					});

					var apopover = "";
					var datavaleur = '';
					if (dayData) {
						apopover = `<a title="${formattedDate}" 
										href="javascript:;"
										data-toggle="popover" 
										data-trigger="focus"
										data-content="<b>Nombre d'actions : </b>${dayData.nombre}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>`;
						datavaleur = `data-nombre='${dayData.nombre}'`;

						if (dateString === new Date().toISOString().slice(0, 10)) {
							className += ' day-calendar-active';
						}
					}

					function getColorForNombre(nombre) {
						if (nombre === 0) {
							return '#f0f8ff'; // Bleu très clair (presque blanc)
						} else if (nombre > 0 && nombre <= 5) {
							return '#add8e6'; // Bleu clair
						} else if (nombre > 5 && nombre <= 10) {
							return '#87ceeb'; // Bleu moyen
						} else if (nombre > 10 && nombre <= 20) {
							return '#4682b4'; // Bleu foncé
						} else {
							return 'white'; // Bleu très foncé
						}
					}

					var dayCell = $(`<div class="day ${className}" 
						title="${dateString} (${dayData ? dayData.nombre + ' actions' : 'Pas de données'})" 
						data-date="${dateString}" ${datavaleur}
						style="${dayData ? `background-color: ${getColorForNombre(dayData.nombre)};` : ''}">
						${apopover}
					</div>`);

					dayCell.on('click', function(event) {
						$('.day').removeClass('day-calendar-active');
						var dateValue = $(this).data('date');
						$(this).addClass('day-calendar-active');
						event.stopPropagation();
					});

					dayCell.find('[data-toggle="popover"]').popover({
						html: true,
						template: '<div class="popover" role="tooltip">' +
							'<div class="arrow"></div>' +
							`<h3 class="popover-title ${className}"></h3>` +
							'<div class="popover-content"></div>' +
							'</div>'
					});

					if (dayData) dayCell.data('details', dayData);
					weekColumn.append(dayCell);

					var month = currentDate.toLocaleString('fr-FR', {
						month: 'short'
					});
					if (!monthPositions[month]) {
						monthPositions[month] = weekIndex;
					}
					currentDate.setDate(currentDate.getDate() + 1);
				}
				calendarContainer.append(weekColumn);
			}

			calendar.append(monthsHeader);
			calendar.append(calendarContainer);
			calendar.append(`<div class="contribution-count">Données de ${currentYear}</div>`);

			Object.keys(monthPositions).forEach(month => {
				var weekIndex = monthPositions[month];
				var monthIndicator = $(`<div class="month-indicator">${month}</div>`);
				monthsHeader.append(monthIndicator);
				monthIndicator.css('left', (weekIndex * 18) + 5 + 'px');
			});

			$('[data-toggle="popover"]').popover();

			// Fonction pour afficher un mois spécifique
			function renderCalendarMonthly(year, month) {
				var calendar = $('#calendarSM');
				calendar.empty();
				var calendarContainer = $('<div class="calendar-container calendar-sm"></div>');
				var weekdaysColumn = $('<div class="weekdays-column"></div>');
				var weekdays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
				weekdays.forEach(day => {
					weekdaysColumn.append(`<div class="weekday">${day}</div>`);
				});
				calendarContainer.append(weekdaysColumn);

				var startDate = new Date(year, month, 1); // Premier jour du mois
				var endDate = new Date(year, month + 1, 0); // Dernier jour du mois
				var firstDay = new Date(startDate);
				while (firstDay.getDay() !== 1) {
					firstDay.setDate(firstDay.getDate() - 1); // Revenir au lundi précédent
				}

				var totalWeeks = Math.ceil((endDate - firstDay) / (7 * 24 * 60 * 60 * 1000));
				var currentDate = new Date(firstDay);

				var contributionCount = 0;
				var monthPositions = {};

				for (var weekIndex = 0; weekIndex < totalWeeks; weekIndex++) {
					var weekColumn = $('<div class="week-column"></div>');
					for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
						var dateString = currentDate.toISOString().slice(0, 10); // Format YYYY-MM-DD
						var dayData = dataByDate[dateString];

						var className = dayData ? 'has-data' : 'empty';
						if (dayData) contributionCount++;

						var formattedDate = currentDate.toLocaleDateString("fr-FR", {
							day: "2-digit",
							month: "long",
							year: "numeric"
						});

						var apopover = "";
						var datavaleur = '';
						if (dayData) {
							apopover = `<a title="${formattedDate}" 
											href="javascript:;"
											data-toggle="popover" 
											data-trigger="focus"
											data-content="<b>Nombre d'actions : </b>${dayData.nombre}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>`;
							datavaleur = `data-nombre='${dayData.nombre}'`;

							if (dateString === new Date().toISOString().slice(0, 10)) {
								className += ' day-calendar-active';
							}
						}

						function getColorForNombre(nombre) {
							if (nombre === 0) {
								return '#f0f8ff'; // Bleu très clair (presque blanc)
							} else if (nombre > 0 && nombre <= 5) {
								return '#add8e6'; // Bleu clair
							} else if (nombre > 5 && nombre <= 10) {
								return '#87ceeb'; // Bleu moyen
							} else if (nombre > 10 && nombre <= 20) {
								return '#4682b4'; // Bleu foncé
							} else {
								return 'white'; // Blanc pour les valeurs supérieures
							}
						}

						var dayCell = $(`<div class="day ${className}" 
											title="${dateString} (${dayData ? dayData.nombre + ' actions' : 'Pas de données'})" 
											data-date="${dateString}" ${datavaleur}
											style="${dayData ? `background-color: ${getColorForNombre(dayData.nombre)};` : ''}">
											${apopover}
										</div>`);

						dayCell.on('click', function(event) {
							$('.day').removeClass('day-calendar-active');
							var dateValue = $(this).data('date');
							$(this).addClass('day-calendar-active');
							event.stopPropagation();
						});

						dayCell.find('[data-toggle="popover"]').popover({
							html: true,
							template: '<div class="popover" role="tooltip">' +
								'<div class="arrow"></div>' +
								`<h3 class="popover-title ${className}"></h3>` +
								'<div class="popover-content"></div>' +
								'</div>'
						});

						if (dayData) dayCell.data('details', dayData);
						weekColumn.append(dayCell);

						currentDate.setDate(currentDate.getDate() + 1);
					}
					calendarContainer.append(weekColumn);
				}

				// Ajouter le mois et l'année en en-tête
				var monthName = startDate.toLocaleString('fr-FR', { month: 'long' });
				var header = $(`<div class="calendar-header">${monthName} ${year}</div>`);
				
				var navigation = $('<div class="calendar-navigation"></div>');
				var prevButton = $('<button class="nav-button btn-swipe-calendar"> < </button>');
				var nextButton = $('<button class="nav-button btn-swipe-calendar"> > </button>');

				prevButton.on('click', function() {
					currentMonth--;
					if (currentMonth < 0) {
						currentMonth = 11;
						currentYear--;
					}
					renderCalendarMonthly(currentYear, currentMonth);
				});

				nextButton.on('click', function() {
					currentMonth++;
					if (currentMonth > 11) {
						currentMonth = 0;
						currentYear++;
					}
					renderCalendarMonthly(currentYear, currentMonth);
				});

				navigation.append(prevButton);
				navigation.append(header);
				navigation.append(nextButton);
				calendar.append(navigation);

				calendar.append(calendarContainer);
				$('[data-toggle="popover"]').popover();
			}

			// Afficher le mois actuel au chargement
			renderCalendarMonthly(currentYear, currentMonth);
		}

		// function setMobileCalendarViews(data) {
		// 	var calendar = $('#calendar');
		// 	calendar.empty();

		// 	// Convertir les données en un format utilisable
		// 	var dataByDate = {};
		// 	for (var date in data) {
		// 		if (data.hasOwnProperty(date)) {
		// 			dataByDate[date] = {
		// 				date: date,
		// 				nombre: data[date].nombre
		// 			};
		// 		}
		// 	}

		// 	// Extraire les années disponibles
		// 	var currentYear = new Date().getFullYear();
		// 	var currentMonth = new Date().getMonth(); // Mois actuel (0 pour janvier, 11 pour décembre)
		// 	var currentDate = new Date(currentYear, currentMonth, 1); // Début du mois actuel

		// 	// Fonction pour afficher un mois spécifique
		// 	function renderCalendarMonthly(year, month) {
		// 		calendar.empty();
		// 		var calendarContainer = $('<div class="calendar-container calendar-sm"></div>');
		// 		var weekdaysColumn = $('<div class="weekdays-column"></div>');
		// 		var weekdays = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];
		// 		weekdays.forEach(day => {
		// 			weekdaysColumn.append(`<div class="weekday">${day}</div>`);
		// 		});
		// 		calendarContainer.append(weekdaysColumn);

		// 		var startDate = new Date(year, month, 1); // Premier jour du mois
		// 		var endDate = new Date(year, month + 1, 0); // Dernier jour du mois
		// 		var firstDay = new Date(startDate);
		// 		while (firstDay.getDay() !== 1) {
		// 			firstDay.setDate(firstDay.getDate() - 1); // Revenir au lundi précédent
		// 		}

		// 		var totalWeeks = Math.ceil((endDate - firstDay) / (7 * 24 * 60 * 60 * 1000));
		// 		var currentDate = new Date(firstDay);

		// 		var contributionCount = 0;
		// 		var monthPositions = {};

		// 		for (var weekIndex = 0; weekIndex < totalWeeks; weekIndex++) {
		// 			var weekColumn = $('<div class="week-column"></div>');
		// 			for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
		// 				var dateString = currentDate.toISOString().slice(0, 10); // Format YYYY-MM-DD
		// 				var dayData = dataByDate[dateString];

		// 				var className = dayData ? 'has-data' : 'empty';
		// 				if (dayData) contributionCount++;

		// 				var formattedDate = currentDate.toLocaleDateString("fr-FR", {
		// 					day: "2-digit",
		// 					month: "long",
		// 					year: "numeric"
		// 				});

		// 				var apopover = "";
		// 				var datavaleur = '';
		// 				if (dayData) {
		// 					apopover = `<a title="${formattedDate}" 
		// 									href="javascript:;"
		// 									data-toggle="popover" 
		// 									data-trigger="focus"
		// 									data-content="<b>Nombre d'actions : </b>${dayData.nombre}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>`;
		// 					datavaleur = `data-nombre='${dayData.nombre}'`;

		// 					if (dateString === new Date().toISOString().slice(0, 10)) {
		// 						className += ' day-calendar-active';
		// 					}
		// 				}

		// 				function getColorForNombre(nombre) {
		// 					if (nombre === 0) {
		// 						return '#f0f8ff'; // Bleu très clair (presque blanc)
		// 					} else if (nombre > 0 && nombre <= 5) {
		// 						return '#add8e6'; // Bleu clair
		// 					} else if (nombre > 5 && nombre <= 10) {
		// 						return '#87ceeb'; // Bleu moyen
		// 					} else if (nombre > 10 && nombre <= 20) {
		// 						return '#4682b4'; // Bleu foncé
		// 					} else {
		// 						return 'white'; // Blanc pour les valeurs supérieures
		// 					}
		// 				}

		// 				var dayCell = $(`<div class="day ${className}" 
		// 									title="${dateString} (${dayData ? dayData.nombre + ' actions' : 'Pas de données'})" 
		// 									data-date="${dateString}" ${datavaleur}
		// 									style="${dayData ? `background-color: ${getColorForNombre(dayData.nombre)};` : ''}">
		// 									${apopover}
		// 								</div>`);

		// 				dayCell.on('click', function(event) {
		// 					$('.day').removeClass('day-calendar-active');
		// 					var dateValue = $(this).data('date');
		// 					updateViews(dateValue);
		// 					$(this).addClass('day-calendar-active');
		// 					event.stopPropagation();
		// 				});

		// 				dayCell.find('[data-toggle="popover"]').popover({
		// 					html: true,
		// 					template: '<div class="popover" role="tooltip">' +
		// 						'<div class="arrow"></div>' +
		// 						`<h3 class="popover-title ${className}"></h3>` +
		// 						'<div class="popover-content"></div>' +
		// 						'</div>'
		// 				});

		// 				if (dayData) dayCell.data('details', dayData);
		// 				weekColumn.append(dayCell);

		// 				currentDate.setDate(currentDate.getDate() + 1);
		// 			}
		// 			calendarContainer.append(weekColumn);
		// 		}

		// 		// Ajouter le mois et l'année en en-tête
		// 		var monthName = startDate.toLocaleString('fr-FR', { month: 'long' });
		// 		var header = $(`<div class="calendar-header">${monthName} ${year}</div>`);
		// 		calendar.append(header);

		// 		// Ajouter les boutons de navigation
		// 		var navigation = $('<div class="calendar-navigation"></div>');
		// 		var prevButton = $('<button class="nav-button">Précédent</button>');
		// 		var nextButton = $('<button class="nav-button">Suivant</button>');

		// 		prevButton.on('click', function() {
		// 			currentMonth--;
		// 			if (currentMonth < 0) {
		// 				currentMonth = 11;
		// 				currentYear--;
		// 			}
		// 			renderCalendar(currentYear, currentMonth);
		// 		});

		// 		nextButton.on('click', function() {
		// 			currentMonth++;
		// 			if (currentMonth > 11) {
		// 				currentMonth = 0;
		// 				currentYear++;
		// 			}
		// 			renderCalendar(currentYear, currentMonth);
		// 		});

		// 		navigation.append(prevButton);
		// 		navigation.append(nextButton);
		// 		calendar.append(navigation);

		// 		calendar.append(calendarContainer);
		// 		$('[data-toggle="popover"]').popover();
		// 	}

		// 	// Afficher le mois actuel au chargement
		// 	renderCalendar(currentYear, currentMonth);
		// }

		setCalendarViews(<?= json_encode($activityThisYear) ?>);

	</script>