<?php

$styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
$otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$imgAvatarDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/avatar.png";

?>

<style>
    .containerCoscrum {
        margin: 10px;
    }

    .profile {
        display: flex;
        align-items: center;
        margin-bottom: 30px;
    }

    .profile img {
        border-radius: 50%;
        width: 100px;
        height: 100px;
        margin-right: 20px;
    }

    .profile h2 {
        margin: 0;
        font-size: 1.8em;
        color: #007bff;
    }

    .profile p {
        margin: 0;
        color: #777;
    }

    .person {
        border-left: 1px solid #e54e5e;
        border-bottom: 2px solid #302c2c;
    }

    .person h3 {
        color: #007bff;
        font-size: 1.5em;
    }

    .task {
        display: flex;
        align-items: center;
        margin: 8px 0;
    }

    .task input[type="checkbox"] {
        margin-right: 10px;
    }
    
    .kl-flex li{
        background-color:#2d2b2b;
        color: white;
    }

    .task span {
        font-size: 1.1em;
        color: #272626;
    }

    .completed {
        text-decoration: line-through;
        color: #999;
    }

    .task input[type="checkbox"]:checked + span {
        text-decoration: line-through;
        color: #999;
    }

    .task input[type="checkbox"]:not(:checked) + span {
        color: #272626;
    }

    .chevron {
        float: inline-end;
    }

    /* .panel-body {
        padding-left: 40px !important;
    } */

    .filter<?= $kunik ?>LG{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .poleActionsList<?= $kunik ?> {
        height: 665px;
        overflow-y: auto;
    }

    .poleActionsList<?= $kunik ?> label{
        margin-top: 10px !important;
    }

    .<?= $kunik ?> {
        background: #2d2b2b; 
        padding: 10px; 
        display: flex; 
        justify-content: center;
    }
    .<?= $kunik ?> .category-card {
        display: inline-block;
        width: 120px;
        height: 120px;
        margin: 10px;
        text-align: center;
        position: relative;
        border-radius: 10px;
        /* background-color: yellow; */
        /* overflow: hidden; */
        box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
        transition: transform 0.3s ease, box-shadow 0.3s ease;
        cursor: pointer; 
    }

    .<?= $kunik ?> .category-card:hover {
        transform: scale(1.05);
        box-shadow: 0px 6px 8px rgba(0, 0, 0, 0.2);
    }

     
    .<?= $kunik ?> .category-image {    
        width: 80%;
        position: absolute;
        left: 10%;
        right: 10%;
        z-index: 1;
        top: -23%;
        height: 110px;
        object-fit: cover;
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }

    .<?= $kunik ?> .category-title {
        padding: 10px;
        border-radius: 0px 0 10px 10px;
        /* background-color: rgba(0, 0, 0, 0.5); */
        color: white;
        font-size: 14px;
        text-transform: capitalize;
        position: absolute;
        bottom: 0;
        width: 100%;
        font-weight: bold;
        overflow: auto; /* Ajouter des barres de défilement */
        max-height: 50px; /* Limiter la hauteur */
    }

    .<?= $kunik ?> h2 {
        text-align: center;
    }

    .<?= $kunik ?> #projectActives {
        margin-top: 2%;
        text-align: center;
    }
    .<?= $kunik ?> .section-title {
        font-size: 22px;          
        font-weight: bold;       
        color: #2C3E50;          
        text-align: center;       
        margin-bottom: 10px;      
        text-transform: uppercase;
        margin-top: 1%; 
        text-transform: none;
    }
    @media (max-width: 900px) {
        .<?= $kunik ?> .category-card {
            margin-top: 10%;
        }
    }

	.custom-block-cms:has(.poleList){
		background: #2d2b2b !important;
	}
	.<?= $kunik ?> .kl-container-co{
        width: 90%;
	}

    @media (min-width: 769px) {
        .<?= $kunik ?> .kl-container-co{
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }
    }

    #corner-action-preview {
        z-index: 10;
        position: relative;
	}

    .co-panel-heading,.co-panel-pole-heading {
        padding: 15px;
        display: flex;
        justify-content: space-between;
        cursor: pointer;
    }

    .co-see-details {
        padding: 5px 14px;
        border-radius: 9px;
        border: 1px solid #28a745;
        background: #415d4f;
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .co-see-details:hover {
        background: #156f2a;
        color: #fff;
    }
    .co-see-details i.fa-eye {    
        color: #fff;
        width: 15px;
        height: 15px;
        border-radius: 50%;
        display: flex;
        background: #28a745;
        align-items: center;
        justify-content: center;
        font-size: 10px;
    }

    .co-panel {
        background-color: #272626;
        border: 1px solid transparent;
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
        box-shadow: 0 1px 1px rgb(154 149 149 / 5%);
    }
    .co-panel:hover {
        background-color: #000;
    }

    .co-panel-body {
        background-color: #444;
    }

	.tasks{
		background-color: black;
		padding: 20px;
	}

    .co-to-expanded {
        display: none;
    }

    .expanded-view {
        z-index: 2;
        position: absolute;
		right: 90px;
    	width: max-content;
    	top: 48px;
    }

    .child-task{
        padding-left: 20px;
    }

	.<?= $kunik ?> .nav li a {
		color : white;
	}

	.tab-pane {
		height: 700px;
		overflow-y: auto;
	}

    .pole-containner{
        border: 1px solid white;
        border-radius: 4px;
    }

	.btn-list-project-pole {
		display: flex;
		color: white ;
		padding: 10px;
		border: 1px solid #000;
        margin-bottom: 5px;
		border-radius: 10px;
	}

	.btn-list-project-pole:hover{
		background: #000;
	}

    .btn-list-event {
		display: flex;
		color: white ;
		padding: 10px;
        margin-bottom: 5px;
		border: 1px solid #000;
		border-radius: 10px;
	}

	.btn-list-event:hover{
		background: #000;
	}

    .btn-list-pole {
		display: flex;
		color: white ;
		padding: 10px;
		border: 1px solid #000;
        margin-bottom: 5px;
		border-radius: 10px;
	}

	.btn-list-pole:hover{
		background: #000;
	}

	.co-project-active{
		background: #000 ;
		border: 1px solid white;
		border-bottom: 0px;
	}

	.child-project {
		display: flex; 
		background: #444;
        border-bottom: 1px solid #7f7e7e;
		padding: 7px; 
		height: 45px
	}

	.child-project:hover{
		background: #272626;		
	}

	.loadKamban i:hover{
		background: #337ab7;
	}
	.menu-tab-filter {
		padding: 0 20px;
		background: #272626;
		min-height: 70vh;
	}

	.kl-content-collapse {
		display: flex;
		justify-content: center;
		align-items: center;
		width: 100px;
	}
	.kl-content-collapse-child {
		display: flex;
		align-items: center;
		position: relative;
	}
    .mobile-show {
        display: none;
    }
    .mobile-hidde {
        display: block;
    }

    .panelEvent {
        width: 0px;
    }

    .sm-block {
        display: flex;
        align-items: center;
    }

	@media screen and (max-width: 770px) {
		.menu-tab-filter {
			min-height: min-content;
		}

		.tab-pane {
			height: auto;
		}
		.kl-dashboard{
			display: block;
		}
		.co-panel-heading{
			display: block;
		}
		.kl-content-collapse-child {
			justify-content: end;
		}

        .mobile-hidde {
            display: none !important;
        }
        .mobile-show {
            display: block;
        }
        .sm-padding {
            padding: 5px 0px !important;
        }

        .padding-20:has(.mobile-show){
            padding: 20px 10px !important;
        }

        .actionsName{
            width: 70%;
        }

        .actionsItems{
            padding: 0px;
        }
	}

    .actionsName{
        width: 90%;
    }
    
    select {
        -webkit-appearance:none;
        -moz-appearance:none;
        -ms-appearance:none;
        appearance:none;
        outline:0;
        box-shadow:none;
        border:0!important;
        background: #5c6664;
        background-image: none;
        flex: 1;
        padding: 0 .5em;
        color:#fff;
        cursor:pointer;
        font-size: 1em;
        font-family: 'Open Sans', sans-serif;
    }
    select::-ms-expand {
        display: none;
    }
    .select {
        position: relative;
        display: flex;
        width: 20em;
        height: 2em;
        line-height: 2;
        background: #5c6664;
        overflow: hidden;
        border-radius: .25em;
    }

    .status-pole{
        position: relative;
        display: flex;
        width: max-content;
        height: 1.7em;
        line-height: 1.5;
        margin-right: 2px;
        color: #00FF7F;
        border: 1px solid #00FF7F;
        overflow: hidden;
        border-radius: .25em;
        padding: 0 .5em;
        font-size: 1em;
        font-family: 'Open Sans', sans-serif;
    }
    .select::after {
        content: "\25bc";
        position: absolute;
        top: 0;
        right: 0;
        padding: 0 1em;
        width: auto;
        background: #2b2e2e;
        cursor:pointer;
        pointer-events:none;
        transition:.25s all ease;
    }
    .select:hover::after {
        color: #23b499;
    }

    @media screen and (max-width: 770px) {
        .select {
            width: 3em;
        }

        .col-lg-12.mobile-show {
            padding: 0px !important;
        }

        .pannelEvent{
            width: 100% !important;
        }

        .panelActionsList{
            padding: 20px 0px 0px 0px !important;
        }
    }

    /* .panelActionsList{
        width: 7-;
    } */

    .title{
        background: white;
        display: flex;
        justify-content: center;
        font-size: large;
        font-weight: bold;
        padding: 5px;
    }

    .display-list{
        cursor: pointer;
        display: block; 
        background: #272626;
        padding: 10px;
    }

    .pannelEvent{
        height: 700px;
        overflow-y: auto;
        width:30%;
        padding: 0px !important;
    }

    .create-action{
        background: linear-gradient(135deg, #00c6ff, #0072ff);
        color: #fff;
        font-size: 24px;
        border: none;
        border-radius: 12px;
        width: 100%;
        cursor: pointer;
        transition: all 0.3s ease-in-out;
        box-shadow: 0 4px 10px rgba(0, 114, 255, 0.3);
    }

    .create-action:hover {
        background: linear-gradient(135deg, #0072ff, #00c6ff);
        box-shadow: 0 6px 15px rgba(0, 114, 255, 0.5);
        transform: scale(1.1);
    }

    .btn-participateActions {
        color: #00FF7F;
        background-color: transparent;
        border-color: #00FF7F;
        font-weight: 700;
        transition: all 0.3s ease-in-out;
        position: relative;
    }

    .btn-participateActions:hover {
        color: black;
        background-color: #00FF7F;
        border-color: #00FF7F;
        transform: scale(1.1);
    }

    .btn-participateActions .compteur {
        display: inline-block;
        position: relative;
    }

    .btn-participateActions:hover .compteur::after {
        content: attr(data-text);
        color: black;
        font-weight: 700;
        margin-left: 20px;
        transition: all 0.3s ease-in-out;
        opacity: 1;
    }

    .btn-participateActions .compteur::after {
        content: "";
        opacity: 0;
        transition: all 0.3s ease-in-out;
    }



</style>
<style id="actionsByPole<?= $kunik ?>"></style>

<div id="blockActionsByPole" class="<?= $kunik ?> padding-20 other-css-<?= $kunik ?> <?= $otherClass ?>">
    <div class="kl-container-co">
        <div class="col-lg-12 mobile-show">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default accordeonProject" style="display: none; border-color: #272626;">
                    <div class="panel-heading" style="background-color: #272626; border-color: #272626; height: 60px;">
                        <h4 class="panel-title" style="margin-top:10px;">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" style="color: #fff; text-decoration: none;width: 100%; display: block;">
                                <i class="icon fa fa-lightbulb-o"></i> Pole </a>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div id="pills-pole-sm" class="panel-body" style="background-color: #272626; border-top-color: #000; color: #ddd;">
                            <?php foreach ($pole as $tag): ?>
                                <div class="btn-list-pole">                               
                                    <div class="poleList" data-tags="<?= $tag ?>"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                                        <div style="display: flex; align-items: center; padding-left: 10px;"><?= ucfirst($tag) ?></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default accordeonProject" style="display: none; border-color: #272626;">
                    <div class="panel-heading" style="background-color: #272626; border-color: #272626; height: 60px;">
                        <h4 class="panel-title" style="margin-top:10px;">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" style="color: #fff; text-decoration: none;width: 100%; display: block;">
                                <i class="icon fa fa-lightbulb-o"></i> <?php echo Yii::t("cms", "Projects") ?> </a>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div id="pills-projectPole-sm" class="panel-body" style="background-color: #272626; border-top-color: #000; color: #ddd;"></div>
                    </div>
                </div>
                <div class="panel panel-default accordeonEvent" style="display: none; border-color: #272626;">
                    <div class="panel-heading" style="background-color: #272626; border-color: #272626; height: 60px;">
                        <h4 class="panel-title" style="margin-top:10px;">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" style="color: #fff; text-decoration: none;width: 100%; display: block;">
                                <i class="icon fa fa-calendar"></i> <?php echo Yii::t("cms", "Events") ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div id="pills-eventPole-sm" class="panel-body" style="background-color: #272626; border-top-color: #000; color: #ddd;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 mobile-hidde" style="padding: 10px 0;">
            <div class="col-lg-4">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link navTab" id="pills-pole-tab" data-toggle="pill" href="#pills-pole" role="tab" aria-controls="pills-pole" aria-selected="true" ><i class="icon fa fa-code-fork"></i> Pole </a></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link navTab" id="pills-project-pole-tab" data-toggle="pill" href="#pills-project-pole" role="tab" aria-controls="pills-project-pole" aria-selected="true" style="display: none;"><i class="icon fa fa-lightbulb-o"></i> <?php echo Yii::t("cms", "Projects") ?></a></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 sm-padding">
            <div class="col-lg-3 col-xs-12 col-md-3 mobile-hidde" style="padding: 0px !important;">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane pole-containner co-scroll fade menu-tab-filter active in" id="pills-pole" role="tabpanel" aria-labelledby="pills-pole-tab">
                        <div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                            <?php foreach ($pole as $tag): ?>
                                <div class="btn-list-pole">                               
                                    <div class="poleList" data-tags="<?= $tag ?>"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                                        <div style="display: flex; align-items: center; padding-left: 10px;"><?= ucfirst($tag) ?></div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="tab-pane co-scroll pole-containner fade menu-tab-filter" id="pills-project-pole" role="tabpanel" aria-labelledby="pills-project-pole-tab">...</div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="col-sm-4 col-xs-4 pole-containner mobile-hidde co-scroll pannelEvent">
                    <div class='title mobile-hidde'>
                        <span><?php echo Yii::t("cms", "Project event(s)") ?></span>
                    </div>
                        <div style='color: white; text-align: center;'> <?php echo Yii::t("cms", "Please select a project to see his event") ?> </div>
                    </div>
                <div class="panelActionsList">
                    <div class='title mobile-hidde pole-containner'><span> Actions </span></div>
                    <div class="poleActionsList<?= $kunik ?> pole-containner co-scroll">
                        <!-- Liste de personnes avec leurs tâches -->
                        <div style='color: white; text-align: center;padding-top:10px;'> <?php echo Yii::t("cms", "Please select a project or event") ?> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	if (costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?> ;

	    var actionsByPole = {
			configTabs : {
                general : {
                    inputsConfig : [
                        "backgroundColor"
                    ]
                },
                style : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
            onchange: function (path,valueToSet,name,payload,value,id){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){

            }
		}
		cmsConstructor.blocks.actionsByPole<?= $blockKey ?> = actionsByPole;
    }

    cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#actionsByPole<?= $kunik ?>").append(str);

    var allProjectData = "";
    $(".poleList").on("click", function(e){
        e.preventDefault();
        $(".btn-list-pole").removeClass('co-project-active');
        $(this).parent(".btn-list-pole").addClass('co-project-active');
        var pole = $(this).data("tags");
        ajaxPost(
                null,
                baseUrl+'/'+moduleId+"/action/getprojectbypoleandparentid/",
                { parentId : costum.contextId , pole : pole },
                function(alldata){ 
                    mylog.log("alldataTest",alldata.data.allProjects);
                    createListProjectByPole(alldata.data.allProjects,pole);
                }
            );
    })

    function createListProjectByPole(projectClassed,pole) {

        $imgProjectDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/project.png";
        $imgEventDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/event.png";

            $("#pills-pole-tab").parent().first().removeClass("active");
            $("#pills-pole").removeClass("active in");
            $(".panelProjectList").css("display","block");
            $("#pills-project-pole-tab").css("display","block");
            $(".accordeonProject").css("display","block");
            let viewProject = `
                <div class="button-add-project-div" style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                    <button class="create-project-pole create-action" data-type="Project" > + </button>
                </div>
            `;
            Object.keys(projectClassed).forEach(key => {
                const project = projectClassed[key];
                const gif = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/totest.webp";
                viewProject += `
                    <div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                        <div class="btn-list-project-pole">                               
                            <div class="projectActiveList" data-id="${key}"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                                <div>
                                    <img src="${project.profilImageUrl}"  onerror="this.src='${$imgProjectDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                                </div>
                                <div style="display: flex; align-items: center; padding-left: 10px;">${project.name}</div>
                            </div>
                        </div>
                    </div>
                `;
            });
            
            $("#pills-project-pole").html(viewProject);
            $("#pills-projectPole-sm").html(viewProject);

            $(".projectActiveList").off("click").on('click', function(e){
                e.stopPropagation();
                const projectId = $(this).data("id");
                createListEventByProject(projectId,projectClassed[projectId],projectClassed[projectId].allEvents);
                $(".btn-list-project-pole").removeClass('co-project-active');
                $(this).parent(".btn-list-project-pole").addClass('co-project-active');
                item = '';
                $.each(projectClassed[projectId].allActions,function(k,v){ 
                        item += createActionItem(k, v);
                });
                item == '' ? item = "<div style='color: white; text-align: center;padding-top:10px;'> <?php echo Yii::t("cms", "No action for this event    ") ?> </div>" : item;
                $(".poleActionsList<?= $kunik ?>").html("");
                $(".poleActionsList<?= $kunik ?>").append(item);
            });

            $("#pills-project-pole-tab").parent().first().addClass("active");
            $("#pills-project-pole").addClass("active in");

            $(".create-project-pole").on("click", function(e){
                    var newProject = {};
                    dyFObj.setMongoId('project', function() {
                            const newProjectToAdd = {
                                id : dyFObj.currentElement.id,
                                public : true,
                                scope:null,
                                key: "project",
                                collection: "projects",
                                tags: [pole]
                            };

                            Object.assign(newProject, newProjectToAdd);

                            new CoInput({
                                container: "#createproject-image-input",
                                inputs: [
                                    {
                                        type: "inputFileImage",
                                        options: {
                                            name: "image",
                                            label: "Image",
                                            collection: "documents",
                                            defaultValue: "",
                                            canSelectMultipleImage : false,
                                            payload: {
                                                path: "logo",
                                            },
                                            icon: "chevron-down",
                                            class: "logoUploader inputContainerCoEvent",
                                            contentKey: "profil",
                                            uploadOnly: true,
                                            uploaderInitialized : true,
                                            endPoint: "",
                                            domElement: "profil",
                                        },
                                    },
                                    {
                                        type : "inputSimple",
                                        options: {
                                            name : "name",
                                            label : trad.projectName +" *",
                                            defaultValue : "",
                                            icon : "chevron-down",
                                            class: "inputContainerCoEvent",
                                        }
                                    },
                                    {
                                        type : "finders",
                                        options: {
                                            name : "parent",
                                            label: tradDynForm.whoiscarrytheproject,
                                            initType: ["projects", "organizations"],
                                            multiple: true,
                                            initMe: false,
                                            icon : "chevron-down",
                                            openSearch: true,
                                            initBySearch: true,
                                            class: "inputContainerCoEvent",
                                        }
                                    },
                                    {
                                        type : "formLocality",
                                        options: {
                                            name : "formLocality",
                                            label: tradCms.place,
                                            icon : "chevron-down",
                                            class: "inputContainerCoEvent",

                                        }
                                    },
                                    {
                                        type : "textarea",
                                        options: {
                                            name : "shortDescription",
                                            label : tradCms.shortDescription,
                                            defaultValue :"",
                                            icon : "chevron-down",
                                            class: "inputContainerCoEvent",
                                        },
                                    },
                                    {
                                        type : "tags",
                                        options : {
                                            name : "tags",
                                            label : "Pole",
                                            options : [""],
                                            defaultValue : "",
                                            class: "inputContainerCoEvent",
                                            icon : "chevron-down",
                                            defaultValue : [pole]
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options: {
                                            name : "email",
                                            label : "E-mail",
                                            defaultValue : "",
                                            icon : "chevron-down",
                                            class: "inputContainerCoEvent",
                                        }
                                    },
                                    {
                                        type : "inputSimple",
                                        options: {
                                            name : "url",
                                            label : "URL",
                                            defaultValue : "",
                                            icon : "chevron-down",
                                            class: "inputContainerCoEvent",
                                        }
                                    },
                                    
                                ],
                                onchange: function(name, value, payload) {
                                    if ( name === "name" ){
                                            jsonHelper.setValueByPath(newProject, name, value);
                                    } else if ( name === "image") {
                                        jsonHelper.setValueByPath(newProject, "profilImageUrl", value);
                                    } else if ( name === "tags"){
                                        jsonHelper.setValueByPath(newProject, name,value.split(','));
                                    } else {
                                        jsonHelper.setValueByPath(newProject, name,value);
                                    }
                                },
                                onblur : function(name , value, payload){
                                    if ( name === "formLocality"){
                                            var address = [];
                                            var geo = [];
                                            var geoPosition = [];
                                            var addresses = [];
                                            value.forEach((location, index) => {
                                                if (location.center) {
                                                    address = location.address;
                                                    geo = location.geo;
                                                    geoPosition = location.geoPosition;
                                                } else {
                                                    addresses.push(location);
                                                }
                                            });
                                            jsonHelper.setValueByPath(newProject, "address", address);
                                            jsonHelper.setValueByPath(newProject, "geo", geo);
                                            jsonHelper.setValueByPath(newProject, "geoPosition", geoPosition);
                                            jsonHelper.setValueByPath(newProject, "addresses", addresses);
                                    } else {
                                        jsonHelper.setValueByPath(newProject, name, value);
                                    } 
                                }
                            });

                            $("#openModalCreateProject").modal("show");
                    });

                    $(".create-project-button").off("click").on("click", function(){
                        var idCojazz = $("#block-cojazz").attr("attrId")
                        delete newProject.images;
                        $("#openModalCreateProject").css({
                            'z-index': '140000'
                        });
                        $("#firstLoader").show();
                        $("#firstLoader #loadingModal").css({
                            'opacity': '0.5',
                            'z-index': '9999999'
                        });
                        ajaxPost(
                            null,
                            baseUrl+"/co2/element/save",
                            newProject, 
                            function (response) {  
                                if (response.result){
                                        $("#openModalCreateProject").modal("hide");
                                        $("#firstLoader").hide();
                                        toastr.success(trad.save);
                                        refreshBlock(idCojazz, ".cmsbuilder-block[data-id='" + idCojazz + "']");
                                        appendProject(pole,response.map.name,response.map.profilImageUrl,response.id);
                                } else {
                                    toastr.error(response.msg);
                                }      
                            }
                        );
                    });
            });
 
    }

    function createListEventByProject(id,project,events) {
            $imgEventDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/event.png";
            var organizer = {
                [id]: {
                    "type" : "projects",
                    "name" : project.name,
                    "image" : project.profilImageUrl
                }
            };
            
            $(".pannelEvent").html("");
            if(events.length === 0){
                $(".pannelEvent").html(`
                    <div class='title mobile-hidde'>
                        <span><?php echo Yii::t("cms", "Project event(s)") ?></span>
                    </div>
                    <div class="button-add-list display-list" style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                            <button class="create-action create-event-pole" data-type="Project" > + </button>
                    </div>`
                );
            } else {
                let viewEvent =`
                    <div class="title mobile-hidde">
                        <span><?php echo Yii::t("cms", "Project event(s)") ?></span>
                    </div>
                    <div class="button-add-list display-list " style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                            <button class="create-action create-event-pole" data-type="Project" > + </button>
                    </div>`;
                Object.keys(events).forEach(key => {
                    const event = events[key];
                    viewEvent += `
                        <div class="display-list">
                            <div class="btn-list-event">                               
                                <div class="eventList" data-id="${key}"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                                    <div>
                                        <img src="${event.profilImageUrl}"  onerror="this.src='${$imgEventDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                                    </div>
                                    <div style="display: flex; align-items: center; padding-left: 10px;">${event.name}</div>
                                </div>
                            </div>
                        </div>
                    `;
                });

                $(".pannelEvent").html(viewEvent);
                $("#pills-eventPole-sm").html(viewEvent);
            }

            $(".eventList").off("click").on('click', function(e){
                e.stopPropagation();
                $(".btn-list-event").removeClass('co-project-active');
                $(this).parent(".btn-list-event").addClass('co-project-active');
                const eventId = $(this).data("id");
                ajaxPost(
                        null,
                        baseUrl+'/'+moduleId+"/action/getprojectbyidwithlimit/",
                        { parentId : eventId , limit: 100},
                        function(actionData){    
                            item = '';
                            $.each(actionData.data,function(k,v){ 
                                item += createActionItem(k, v);
                            });
                            item == '' ? item = "<div style='color: white; text-align: center;padding-top:10px;'> <?php echo Yii::t("cms", "No action for this event    ") ?> </div>" : item;
                            $(".poleActionsList<?= $kunik ?>").html("");
                            $(".poleActionsList<?= $kunik ?>").append(item);
                        }
                );
            });

            $("#pills-event-pole-tab").parent().first().addClass("active");
            $("#pills-event-pole").addClass("active in");

            $(".create-event-pole").on("click", function(e){
				var preferences = {
					isOpenData : true,
					isOpenEditional : true
				}

				var newEvent = {};
				dyFObj.setMongoId('events', function() {
						const newEventToAdd = {
							id : dyFObj.currentElement.id,
							public : true,
							scope:null,
							recurrency: false,
							costumSlug: costum.slug,
							costumEditMode: costum.editMode,
							preferences: preferences,
							key: "event",
							collection: "events",
							timeZone : moment.tz.guess(),
                            organizer: {
                                [id]: {
                                    "type" : "projects",
                                }
                            }
						};

						Object.assign(newEvent, newEventToAdd);

						new CoInput({
							container: "#createvent-image-input",
							inputs: [
								{
									type: "inputFileImage",
									options: {
										name: "image",
										label: "Image",
										collection: "documents",
										defaultValue: "",
										canSelectMultipleImage : false,
										payload: {
											path: "logo",
										},
										icon: "chevron-down",
										class: "logoUploader inputContainerCoEvent",
										contentKey: "profil",
										uploadOnly: true,
										uploaderInitialized : true,
										endPoint: "",
										domElement: "profil",
									},
								},
								{
									type : "dateTimePicker",
									options: {
										name : "startDate",
										label: tradCms.startDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newStartDate",
									}
								},
								{
									type : "dateTimePicker",
									options: {
										name : "endDate",
										label: tradCms.endDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newEndDate",
									}
								},
								{
									type : "tags",
									options : {
										name : "tags",
										label : tradDynForm.keyWords,
										options : [""],
										defaultValue : "",
										class: "inputContainerCoEvent",
										icon : "chevron-down"
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "email",
										label : "E-mail",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "url",
										label : "URL",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								
							],
							onchange: function(name, value, payload) {
								if ( name === "image") {
									jsonHelper.setValueByPath(newEvent, "profilImageUrl", value);
								} else if ( name === "startDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment(); // Date et heure actuelles

									if (value.isBefore(currentDate)) {
										$(".warningStartDate").remove();
										$(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									} else {
										$(".warningStartDate").remove();
										var dateDebutConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
										jsonHelper.setValueByPath(newEvent, name, dateDebutConvertie);
									}
								} else if ( name === "endDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment();
									var startDate = moment(newEvent.startDate);
									var dateEndConvertie = value.tz(newEvent.timeZone);
									if (dateEndConvertie.isAfter(currentDate) && dateEndConvertie.isAfter(startDate)) {
										$(".warningEndDate").remove();
										jsonHelper.setValueByPath(newEvent, name, dateEndConvertie.format("YYYY-MM-DDTHH:mm:ssZ"));
									} else {
										$(".warningEndDate").remove();
										$(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + startDate.format("DD/MM/YYYY HH:mm") + " " + tradCms.and + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									}
								} else if ( name === "tags"){
									jsonHelper.setValueByPath(newEvent, name,value.split(','));
								} else {
									jsonHelper.setValueByPath(newEvent, name,value);
								}
							},
							onblur : function(name , value, payload){
							
							}
						});

						new CoInput({
								container: ".create-event-details",
								inputs: [
									{
										type : "inputSimple",
										options: {
											name : "name",
											label : tradCms.eventName +" *",
											defaultValue : "",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										}
									},
									{
										type : "select",
										options: {
											name : "type",
											label : tradDynForm.eventTypes +" *",
											options :  Object.keys(eventTypes).map(function(value) {
												return {
													label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
													value : value
												}
											}),
											icon : "chevron-down",
											class: "inputContainerCoEvent"
										}
									},
									{
										type : "finders",
										options: {
											name : "organizer",
											label: tradDynForm.whoorganizedevent,
											initType: ["projects", "organizations"],
											multiple: true,
											initMe: false,
											icon : "chevron-down",
											openSearch: true,
											initBySearch: true,
											class: "inputContainerCoEvent",
                                            defaultValue: organizer
										}
									},
									{
										type : "formLocality",
										options: {
											name : "formLocality",
											label: tradCms.place,
											icon : "chevron-down",
											class: "inputContainerCoEvent",

										}
									},
									{
										type : "textarea",
										options: {
											name : "shortDescription",
											label : tradCms.shortDescription,
											defaultValue :"",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										},
									},
								],
								onchange: function(name, value, payload) {
									if ( name === "name" ){
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								},
								onblur: function(name, value, payload) {
									if ( name === "formLocality"){
											var address = [];
											var geo = [];
											var geoPosition = [];
											var addresses = [];
											value.forEach((location, index) => {
												if (location.center) {
													address = location.address;
													geo = location.geo;
													geoPosition = location.geoPosition;
												} else {
													addresses.push(location);
												}
											});
											jsonHelper.setValueByPath(newEvent, "address", address);
											jsonHelper.setValueByPath(newEvent, "geo", geo);
											jsonHelper.setValueByPath(newEvent, "geoPosition", geoPosition);
											jsonHelper.setValueByPath(newEvent, "addresses", addresses);
									} else {
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								}
						});
						$("#openModalCreateEvent").modal("show");
				});

				$(".create-event-button").off("click").on("click", function(){
						var idBlock =  $(this).data("id");
						delete newEvent.images;
                        $("#openModalCreateEvent").css({
                            'z-index': '140000'
                        });
                        $("#firstLoader").show();
                        $("#firstLoader #loadingModal").css({
                            'opacity': '0.5',
                            'z-index': '9999999'
                        });
						ajaxPost(
							null,
							baseUrl+"/co2/element/save",
							newEvent, 
							function (response) {  
								if (response.result){
                                    var idCojazz = $("#block-cojazz").attr("attrId")
                                    refreshBlock(idCojazz, ".cmsbuilder-block[data-id='" + idCojazz + "']")
                                    appendEvent(response.map.name,response.map.profilImageUrl,response.id)
									var events = {
										id: costum.contextId,
										collection: "events",
										path: "links.subEvents."+newEvent.id,
										value : {
											type : "events"
										},
										costumSlug: costum.slug,
										costumEditMode: costum.editMode                                                           
									}
									dataHelper.path2Value(events, function (params) {
										if (params.result){
											var attende = {
												id : newEvent.id,
												collection : "events",
												path : "links.attendees."+userId,
												value : {
													type : "citoyens"
												},
												costumSlug: costum.slug,
												costumEditMode: costum.editMode   
											}
											dataHelper.path2Value(attende, function (params) {
												$("#openModalCreateEvent").modal("hide");
                                                $("#firstLoader").hide();
												toastr.success(trad.save);
											});
										}
									});
								} else {
									toastr.error(response.msg);
                                    $("#firstLoader").hide();
								}                 
							}
						);
				});
	        });

    }

    function appendEvent(name,profilImageUrl,id){
        $imgEventDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/event.png";
        const item = `
            <div class="display-list">
                <div class="btn-list-event">                               
                    <div class="eventList" data-id="${id}"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                        <div>
                            <img src="${profilImageUrl}"  onerror="this.src='${$imgEventDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                        </div>
                        <div style="display: flex; align-items: center; padding-left: 10px;">${name}</div>
                    </div>
                </div>
            </div>
        `
        $(".button-add-list").after(item);
        $(".eventList").off("click").on('click', function(e){
            e.stopPropagation();
            $(".btn-list-event").removeClass('co-project-active');
            $(this).parent(".btn-list-event").addClass('co-project-active');
            const eventId = $(this).data("id");
            ajaxPost(
                    null,
                    baseUrl+'/'+moduleId+"/action/getprojectbyidwithlimit/",
                    { parentId : eventId , limit: 100},
                    function(actionData){    
                        var item = '';
                        $.each(actionData.data,function(k,v){ 
                            item += createActionItem(k, v);
                        });
                        item == '' ? item = "<div style='color: white; text-align: center;padding-top:10px;'> <?php echo Yii::t("cms", "No action for this event    ") ?> </div>" : item;
                        $(".poleActionsList<?= $kunik ?>").html("");
                        $(".poleActionsList<?= $kunik ?>").append(item);
                    }
            );
        });
    }

    function appendProject(pole,name,profilImageUrl,id){
        $imgProjectDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/project.png";
        const item = `
            <div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                <div class="btn-list-project-pole">                               
                    <div class="projectActiveList" data-id="${id}"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                        <div>
                            <img src="${profilImageUrl}"  onerror="this.src='${$imgProjectDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                        </div>
                        <div style="display: flex; align-items: center; padding-left: 10px;">${name}</div>
                    </div>
                </div>
            </div>
        `
        ajaxPost(
            null,
            baseUrl+'/'+moduleId+"/action/getprojectbypoleandparentid/",
            { parentId : costum.contextId , pole : pole },
            function(alldata){ 
                var oneData = alldata.data.allProjects;
                $(".button-add-project-div").after(item);
                $(".projectActiveList").off("click").on('click', function(e){
                    e.stopPropagation();
                    const projectId = $(this).data("id");
                    createListEventByProject(projectId,oneData[projectId],oneData[projectId].allEvents);
                    $(".btn-list-project-pole").removeClass('co-project-active');
                    $(this).parent(".btn-list-project-pole").addClass('co-project-active');
                    itemAction = '';
                    $.each(projectClassed[projectId].allActions,function(k,v){ 
                            itemAction += createActionItem(k, v);
                    });
                    itemAction == '' ? itemAction = "<div style='color: white; text-align: center;padding-top:10px;'> <?php echo Yii::t("cms", "No action for this event    ") ?> </div>" : item;
                    $(".poleActionsList<?= $kunik ?>").html("");
                    $(".poleActionsList<?= $kunik ?>").append(itemAction);
                });
            }
        );
    }


        /**
            <div class="expended-parent">
                <div class="expanded-view hidden" style="">
                    <button onclick="openExpended(this)" style="border: none; color: red; position: absolute; top: -10px; right: 0; background: #444; border-radius: 50%;">
                        <i class="fa fa-close"></i>
                    </button>
                    <div class="action-details">
                        <div class="kl-card">
                            <div class="kl-flex kl-justify-between kl-nb-view">
                            </div>
                        </div>
                    </div>
                </div>
                <button class="co-see-details"  data-toggle="tooltip" title="Voir les projets" onclick="openExpended(this)" data-key="${key}">
                    <i class="fa fa-eye" style="margin-right: 5px"></i>
                </button>
            </div>
        */

    function getActionStatus(action) {
		const hasTags = Array.isArray(action.tags) && action.tags.length > 0;
		const status = action.status || "";
		const isTracking = Boolean(action.tracking);
		const lowerTags = hasTags ? action.tags.map(tag => tag.toLowerCase()) : [];
		
		if (status === "done") return "done";
		if (lowerTags.includes("totest")) return "totest";
		if (isTracking) return "tracking";
		if (lowerTags.includes("next")) return "next";
		if (lowerTags.includes("discuter")) return "discuter";
		return "todo";
	}

    function createActionItem(key, value) {
                var view = "";
                const isChecked = value.status === "done" ? "checked" : "";
                const taskClass = value.status === "done" ? "completed" : "";
                const iconClass = value.links?.contributors?.[userId] !== undefined ? "fa-unlink" : "fa-link";
                const tooltip = value.links?.contributors?.[userId] !== undefined ? "<?php echo Yii::t("cms", "Stop participating") ?>"  : "<?php echo Yii::t("cms", "Participate") ?>" ;
                var taskChildren =`<span style='color:white;'> Aucune sous taches</span>`;
                var edition = ``;
                const actualStatus = getActionStatus(value);
                // create task children
                if ( typeof  value.tasks !== "undefined" )  {
                    value.tasks.forEach(task => {
                        taskChildren += `
                            <div class="task" style="color: white; display: flex; position: relative;">
                                <div style="width: 10px; display: flex; align-items:center; justify-content: center; margin-right: 10px;" id="task-${task.taskId}">
                                    <i class="fa ${task.checked ? "fa-check-square" : "fa-minus-square"} " style="${task.checked ? "background: #28a745; color: #fff" : "color: #fff; background: #272626"   } "></i>
                                </div>
                                <div style="font-size: 15px;">
                                    <label for="task-${task.taskId}" data-id="${task.taskId}" style="font-weight: normal;">${task.task}</label>
                                </div>
                            </div>
                        `
                    });
                }

                edition = `<span class="status-pole"> ${actualStatus} </span>`;


                // Retourner le HTML complet pour l'élément
                return `
                    <div class="person userScrumPanel<?= $kunik ?>">
                        <div class="co-panel panel-default">
                            <div class="co-panel-pole-heading" id="panelHeading<?= $kunik ?>">
                                <div style="display: flex; width: 90%">
                                    <div style="display: flex; padding-left: 10px; align-items: center">
                                        <div>
                                            <button class="btn btn-participateActions btn-sm" id="contribue-${key}" data-contribue="${Object.keys(value.links?.contributors ?? {}).length}" data-max="${value.max ?? "∞"}" onclick="contribueAction('${key}')" style="margin-right: 5px;" data-toggle="tooltip" title="${tooltip}">
                                                <span class="fa ${iconClass}"></span>
                                                <span class="compteur" data-text="${tooltip}">(${Object.keys(value.links?.contributors ?? {}).length}/${value.max ?? "∞"})</span>
                                            </button>
                                        </div>
                                        <div style="color: #fff;padding-left: 5px;font-size: 16px;" data-toggle="collapse" data-parent="#accordion" href="#collapse${key}">
                                            ${value.name}
                                        </div>
                                    </div>
                                </div>
                                <div class="kl-content-collapse-child">
                                    ${edition}
                                    <button class="status-pole btn btn-participateActions btn-sm" onclick="showDetailsPoleAction('${key}')" data-toggle="tooltip" title="Info" style="margin-right: 5px;">
                                        <span class="fa fa-info-circle" style="line-height: 1.5;"></span>
                                    </button>
                                </div>
                            </div>
                            <div id="collapse${key}" class="panel-collapse collapse">
                                <div class="panel-body co-panel-body">
                                    <span class="collapse-desc">
                                        <div class="tasks">
                                            ${taskChildren}
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
    }



    $("#pills-pole-tab").on("click", function(e){
        e.preventDefault();
        $(".pannelEvent").html("<div class='title mobile-hidde'><span><?php echo Yii::t("cms", "Project event(s)") ?></span></div><div style='color: white; text-align: center;'> <?php echo Yii::t("cms", "Please select a project to see his event") ?> </div>");
        $(".poleActionsList<?= $kunik ?>").html("<div style='color: white; text-align: center;padding-top:10px;'><?php echo Yii::t("cms", "Please select a project or event") ?></div>")
    })

    function showDetailsPoleAction(id){
        var previewDom = $('#corner-action-preview');
        previewDom.empty();
        show_action_detail_modal_pole(id).then(function(__html) {
            previewDom.html(__html);
        });
    }

    /**
     * Affiche un modal contenant les détails d'une action.
     * Possibilité d'ajouter des sous tâches et inviter des participants.
     * @param {string} __id Identifiant de l'action à récupérer
     * @return {Promise.<string>}
     */
    function show_action_detail_modal_pole(__id) {
        return new Promise(function(__resolve) {
            const url = baseUrl + '/costum/project/action/request/action_detail_html';
            const post = {
                id: __id,
                connectedUser: userConnected._id.$id,
                server_url: baseUrl
            };
            ajaxPost(null, url, post, __resolve, null, 'text');
        });
    }

    function contribueAction(id) {
        var self = $(`#contribue-${id}`);
        var contribue = parseInt(self.data('contribue'), 10) || 0;
        var max = self.data('max') || "∞";

        if (self.hasClass('disabled') || userConnected === null) return;

        const isParticipating = self.find('.fa').hasClass('fa-link');
        const participationIcons = ["fa-link", "fa-unlink"];
        self.prop('disabled', true);

        self.find(".fa").removeClass("fa-link fa-unlink").addClass("fa-spinner fa-spin");

        coInterface.actions.request_participation({
            action: id,
            contributor: userId,
            participate: isParticipating ? 1 : 0,
        }).then(response => {
            const contributors = {};
            $.each(response, (_, c) => {
                contributors[c.id] = { type: "citoyens" };
            });

            contribue = isParticipating ? contribue + 1 : contribue - 1;
            self.find(".fa").removeClass("fa-spinner fa-spin").addClass(participationIcons[+isParticipating]);
            self.find(".compteur").html(`(${contribue}/${max})`);
            self.data('contribue', contribue); 

            const newText = isParticipating ? "<?php echo Yii::t("cms", "Stop participating") ?>" : "<?php echo Yii::t("cms", "Participate") ?>" ;
            self.attr('data-original-title', newText);
            self.attr('title', newText);
            self.attr('aria-pressed', isParticipating ? "true" : "false");

            self.find(".compteur").attr("data-text", newText);

        }).catch(error => {
            console.error("Erreur retournée :", error);
            toastr.error(`${(error?.side || "Erreur").toUpperCase()} : ${error?.msg || "Une erreur est survenue."}`);

            self.find(".fa").removeClass("fa-spinner fa-spin").addClass(participationIcons[+!isParticipating]);

        }).finally(() => {
            self.prop('disabled', false);
        });
    }



</script>   