<?php  
	$userId = Yii::app()->session["userId"] ?? null;
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
	$imgProjectDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/project.png";
	$imgEventDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/event.png";
    $imgAvatarDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/avatar.png";

    function formatDate($date) {
        $dateTime = DateTime::createFromFormat('d/m/Y H:i', $date);
        return $dateTime ? $dateTime->format('d M Y - H:i') : $date;
    }

    $allTags = [];

    foreach ($allProjectChild as $project) {
        if (isset($project['tags']) && is_array($project['tags'])) {
            // Ajouter les tags du projet au tableau $allTags
            $allTags = array_merge($allTags, $project['tags']);
        }
    }
    $allTags = array_unique($allTags);

    $allTagsActive = [];

    foreach ($allActiveProjectChild as $project) {
        if (isset($project['tags']) && is_array($project['tags'])) {
            $allTagsActive = array_merge($allTagsActive, $project['tags']);
        }
    }

    $allTagsActive = array_unique($allTagsActive);

    // echo "<pre>";
    // var_dump($allEventChild);die();
    // var_dump($allProjectChild);die();

?>
<style>
        .<?= $kunik ?> {
            background-color: white;
            padding-top : 1rem ;
        }

        .<?= $kunik ?> .sticky-menu {
            background-color: #f8f9fa;
            padding: 20px 20px;
            position: sticky;
            top: auto;
            z-index: 1000;
            display: flex;
            justify-content: center;
        }

        .<?= $kunik ?> .sticky-menu nav {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .<?= $kunik ?> .sticky-menu nav ul {
            list-style: none;
            margin: 0;
            padding: 0;
            display: flex;
        }

        .<?= $kunik ?> .sticky-menu nav ul li {
            margin-left: 80px;
            margin-right: 80px;
        }

        .<?= $kunik ?> .sticky-menu nav ul li span {
            text-decoration: none;
            color: #333;
            font-weight: bold;
        }

        .sticky-menu-item.active {
            color: #68ca91 !important;
        }

        .<?= $kunik ?> .containerPrez {
            padding: 20px;
        }

    .<?= $kunik ?> section {
            padding: 60px 0;
        }

    .<?= $kunik ?> section .section-title {
        text-align: left;
        color: rgba(255,255,255,1);
        margin-bottom: 50px;
        text-transform: uppercase;
        font-weight: 800;
        font-size: 2.5rem;
        margin-left: 5rem;
    }

    .<?= $kunik ?> .cardPrez .card {
        border: none;
        background: #ffffff;
        border-radius: 1.5rem;
    }

    .<?= $kunik ?> .card{
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
        overflow: hidden;
        border-radius: 15px;
        transition: all 1s ease;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
    }

    .<?= $kunik ?> .card-body::before {
        content: '';
        position: absolute;
        top: -50%;
        left: -50%;
        width: 200%;
        height: 200%;
        background: linear-gradient(
            0deg, 
            transparent, 
            transparent 40%, 
            rgba(88,48,117,0.5)
        );
        transform: rotate(-45deg);
        transition: all 1.2s ease;
        opacity: 0;
    }

    .<?= $kunik ?> .card-body:hover::before {
        opacity: 1;
        transform: rotate(-45deg) translateY(100%);
    }

    .<?= $kunik ?> .hover-effect {
        position: relative;
        overflow: hidden;
        background: white;
        border-radius: 12px;
        transition: 1s ease-in-out;
    }

    .<?= $kunik ?> .hover-effect::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, #6b798f, #f8a3d5); /* Adjust gradient */
        border-radius: 50% 50% 0 0; /* Curved effect */
        transition: 1s ease-in-out;
    }

    .<?= $kunik ?> .hover-effect:hover::after {
        top: 0;
    }


    .<?= $kunik ?> .mainflip {
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -ms-transition: 1s;
        -moz-transition: 1s;
        -moz-transform: perspective(1000px);
        -moz-transform-style: preserve-3d;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
        position: relative;
    }

    .<?= $kunik ?> .frontside {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }

    .<?= $kunik ?> .frontside {
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        -ms-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -moz-transition: 1s;
        -moz-transform-style: preserve-3d;
        -o-transition: 1s;
        -o-transform-style: preserve-3d;
        -ms-transition: 1s;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
    }

    .<?= $kunik ?> .frontside .card {
        min-height: 312px;
    }

    .<?= $kunik ?> .backside .card a {
        font-size: 18px;
        color: #68ca91 !important;
    }

    .<?= $kunik ?> .frontside .card .card-title,
    .backside .card .card-title {
        color: #68ca91 !important;
        cursor: pointer;
    }

    .<?= $kunik ?> .frontside .card .card-body img {
        width: 120px;
        height: 120px;
        border-radius: 50%;
        object-fit: cover;
    }

    #projectPrez,#eventsPrez{
        background: #583075;
        background: -webkit-radial-gradient(circle, #583075 0%, #010101 100%);
        background: radial-gradient(circle, #583075 0%, #010101 100%);
        border-radius: 1rem;
        margin: 1rem;
        
    }

    #teamPrez{
        background: white;
        border-radius: 1rem;
        margin: 1rem;
        
    }

    .<?= $kunik ?> .cardPrez .containerPrez{
        padding-left: 1rem;
        padding-right: 1rem;
    }

    .<?= $kunik ?> .containerPrez .row {
        margin-left: 10rem;
        margin-right: 10rem;
    }

    .<?= $kunik ?> .containerPrez h5 {
        margin-left: 10rem;
        padding-left: 15px
    }

    .<?= $kunik ?> .containerPrez .section-description {
        padding-right: 15px;
        font-size: 2rem;
        color : white;
    }

    .<?= $kunik ?> .containerPrez .section-filtre {
        margin:  3rem 10rem 3rem 10rem;
        padding-left: 15px;
    }

    #teamPrez .section-description {
        color : black !important;
    }
    
    @media screen and (min-width: 768px) {
        .<?= $kunik ?> .containerPrezProject::before{
            content: "\f0eb";
        }

        .<?= $kunik ?> .containerPrezTeam::before{
            content: "\f0c0";
        }

        .<?= $kunik ?> .containerPrezEvents::before{
            content: "\f133";
        }

        .<?= $kunik ?> .containerPrez:before {
            font-family: "FontAwesome";
            background: white;
            display: flex;
            justify-content: center;
            align-items: center;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 8.6rem;
            width: 30px;
            height: 30px;
            z-index: 5;
        }

        .<?= $kunik ?> .cardPrez:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            top: 18rem;
            left: 10rem;
            width: 2px;
            height: 100%;
            bottom: 0;
            height: 89%;
            z-index: 4;
        }
    }

    .<?= $kunik ?> .containerPrezTeam h5{
        color: black;
    }

    @media screen and (max-width: 768px) {
        /* .<?= $kunik ?> .sticky-menu {
            display: none;
        } */

        .<?= $kunik ?> .sticky-menu nav ul li{
            margin-left: 30px;
            margin-right: 30px;
        }

        .<?= $kunik ?> .containerPrez{
            padding: 0;
        }
        
        .<?= $kunik ?> .cardPrez .containerPrez{
            padding: 0;
        }

        .<?= $kunik ?> .containerPrez .row{
            margin: 0;
        }

        .<?= $kunik ?> .containerPrez .section-filtre {
            margin: 0;
        }

        .<?= $kunik ?> .box {
            display: block !important;
            padding-bottom: 10px;
        }

        .filterItem{
            display: block !important;
        }

        .presentation67c69b93cf57f2f6a40e8c06 .containerPrez h5{
            padding-left: 0;
        }
    }


    .<?= $kunik ?> .box {
        border-radius: 20px;
        display: flex;
        justify-content: space-between;
    }

    .box select {
        appearance: none;
        outline: 10px red;
        border: 0;
        box-shadow: none;
        border-radius: 5px;
        flex: 1;
        padding: 0 1em;
        color: #fff;
        background-color: #27445D;
        background-image: none;
        cursor: pointer;
    }

    .box select::-ms-expand {
        display: none;
    }

    .selectRole {
        position: relative;
        display: flex;
        width: 13em;
        height: 1.5em;
        border-radius: .25em;
        overflow: hidden;
    }

    .selectRole::after {
        content: '\f0dd';
        position: absolute;
        font-family: "FontAwesome";
        top: -20px;
        font-size: large;
        right: 0;
        padding: 1em;
        background-color: #27445D;
        transition: .25s all ease;
        pointer-events: none;
    }

    .selectRole:hover::after {
        color: var(--darkgray);
    }
    
    .selectPole {
        position: relative;
        display: flex;
        width: 13em;
        height: 1.5em;
        border-radius: .25em;
        overflow: hidden;
        margin-right: 5px;
    }

    .selectPole select{
        background: #22c0e8;
    }

    .selectPole::after {
        content: '\f0dd';
        position: absolute;
        font-family: "FontAwesome";
        top: -20px;
        font-size: large;
        right: 0;
        padding: 1em;
        background-color: #22c0e8;
        transition: .25s all ease;
        pointer-events: none;
    }
    
    .selectPole:hover::after {
        color: var(--darkgray);
    }

    .slideThree {
        width: 90px;
        height: 26px;
        background: #22c0e8;
        margin-top: 10px;
        margin-bottom: 10px;
        margin-right: 10px;
        position:relative;
        border-radius: 50px;
        box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
        &:after {
            content: 'Old';
            color: #000;
            position: absolute;
            right: 10px;
            z-index: 0;
            font: 12px/26px Arial, sans-serif;
            font-weight: bold;
            text-shadow: 1px 1px 0px rgba(255,255,255,.15);
        }
        &:before {
            content: 'New';
            color: black;
            position: absolute;
            left: 10px;
            z-index: 0;
            font: 12px/26px Arial, sans-serif;
            font-weight: bold;
        }
        label {
            display: block;
            width: 45px;
            height: 20px;
            cursor: pointer;
            position: absolute;
            top: 3px;
            left: 3px;
            z-index: 1;
            background: #fcfff4;
            background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
            border-radius: 50px;
            transition: all 0.4s ease;
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
        }
        input[type=checkbox] {
            visibility: hidden;
            &:checked + label {
            left: 43px;
            }
        }    
    }

    .slideTwo {
        width: 110px;
        height: 26px;
        background: #22c0e8;
        position:relative;
        margin-top: 10px;
        margin-bottom: 10px;
        margin-right: 10px;
        border-radius: 50px;
        box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
        &:after {
            content: 'All';
            color: #000;
            position: absolute;
            right: 10px;
            z-index: 0;
            font: 12px/26px Arial, sans-serif;
            font-weight: bold;
            text-shadow: 1px 1px 0px rgba(255,255,255,.15);
        }
        &:before {
            content: 'Active';
            color: black;
            position: absolute;
            left: 10px;
            z-index: 0;
            font: 12px/26px Arial, sans-serif;
            font-weight: bold;
        }
        label {
            display: block;
            width: 62px;
            height: 20px;
            cursor: pointer;
            position: absolute;
            top: 3px;
            left: 3px;
            z-index: 1;
            background: #fcfff4;
            background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
            border-radius: 50px;
            transition: all 0.4s ease;
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
        }
        input[type=checkbox] {
            visibility: hidden;
            &:checked + label {
            left: 43px;
            }
        }    
    }

    .card {
        border-radius: 15px;
        overflow: hidden;
        transition: transform 0.3s ease, box-shadow 0.3s ease;
        background: white;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
    }

    .card:hover {
        transform: translateY(-5px);
        box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.2);
    }

    .card-body {
        padding: 20px;
    }

    .card-title {
        font-size: 1.4rem;
        font-weight: bold;
        color: #009688;
        text-transform: uppercase;
    }

    .card-text {
        font-size: 1.2rem;
        color: #666;
    }

    .badges-container {
        margin-top: 10px;
    }

    .badge {
        font-size: 1rem;
        padding: 5px 8px;
        margin: 3px;
        border-radius: 12px;
        background: #ddd;
        color: #333;
        display: inline-block;
    }

    .badge-primary {
        background: #27445D;
        color: white;
    }

    .projectPoleItem{
        cursor: pointer;
    }

    .filterItem{
        display: flex;
        align-items: center; 
        justify-content: center;
    }

    .selectPole{
        padding-left: 5px;
        background: #22c0e8;
    }

    .no-event-container {
        display: flex;
        width: 100%;
        justify-content: center;
        align-items: center;
        height: 312px;
        background: linear-gradient(135deg, #2b1055, #7597de);
        border-radius: 12px;
        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.3);
    }

    .no-event-card {
        text-align: center;
        color: #fff;
        font-size: 18px;
        font-weight: bold;
        padding: 20px;
    }

    .no-event-card i {
        font-size: 50px;
        animation: fadeIn 1.5s ease-in-out;
    }

    @keyframes fadeIn {
        0% { opacity: 0; transform: scale(0.9); }
        100% { opacity: 1; transform: scale(1); }
    }

    .kamban-icon-prez {
        height: 50px;
        background: #2f2c2c;
        border-radius: 10px;
    }

    .kamban-icon-prez:hover {
        background: green;
    }

    .kamban-icon-prez i.fa {
        font-size: 32px;
        color: white;
    }



</style>

<div class="<?= $kunik ?> padding-20 other-css-<?= $kunik ?> <?= $otherClass ?>">
    <div class="sticky-menu">
        <nav>
            <ul>
                <li><span class="sticky-menu-item" data-id="#projectPrez">PROJECTS</span></li>
                <li><span class="sticky-menu-item" data-id="#teamPrez">TEAM</span></li>
                <li><span class="sticky-menu-item" data-id="#eventsPrez">EVENTS</span></li>
            </ul>
        </nav>
    </div>
            
        <section id="projectPrez" class="cardPrez pb-5" data-id="#projectPrez">
            <div class="containerPrez containerPrezProject">
                <h5 class="section-title">PROJECT</h5>
                <div class="section-filtre">
                    <div class="box">
                        <h4 class="section-description"> <?= Yii::t("cms", "Projects linked to our organization.")?> </h4>
                        <div class="filterItem"> 
                            <div class="selectPole">
                                <select class="selectPoleProject">
                                    <option value="all"> <?= Yii::t("cms", "All")?> </option>
                                </select>
                            </div> 
                            <div class="slideTwo">
                                <input type="checkbox" value="new" id="slideTwo" name="check" checked />
                                <label for="slideTwo"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row projectRow">
                    <?php if (count($allActiveProjectChild) <= 0): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="no-event-container">
                                            <div class="no-event-card">
                                                <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                <p> <?= Yii::t("cms", "Create a project for your organisation")?> </p>
                                                <button type="button" data-toggle="tooltip" onclick="creationStep('project')" title="<?= Yii::t("cms", "Create a project for your organisation")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } else { ?>
                                                    <p> <?= Yii::t("cms", "No active project")?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php foreach ($allActiveProjectChild as $keyProject => $project):
                                $polesHtml = !empty($project["tags"]) 
                                    ? implode(' ', array_map(fn($pole) => "<span class='badge badge-primary projectPoleItem' data-pole='$pole' >$pole</span>", $project["tags"]))
                                    : '';
                        ?>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center" 
                                                data-slug="<?= $project["slug"] ?? "" ?>">
                                                <p>
                                                    <img class="img-fluid" 
                                                        src="<?= $project["profilImageUrl"] ?? $imgProjectDefault ?>" 
                                                        onerror="this.src='<?= $imgProjectDefault ?>';" 
                                                        alt="card image">
                                                </p>
                                                <h4 class="card-title cardTitle-project"  
                                                    data-id="<?= $project["id"] ?>" >
                                                    <?= $project["name"] ?>
                                                </h4>
                                                <p class="badges-container"><?= $polesHtml ?></p>
                                                <p class="card-text"><?= $project["shortDescription"] ?? "" ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section id="teamPrez" class="cardPrez pb-5" data-id="#teamPrez">
            <div class="containerPrez containerPrezTeam" >
                <h5 class="section-title">TEAM</h5>
                <div class="section-filtre">
                    <div class="box">
                        <h4 class="section-description"> <?= Yii::t("cms", "Members.")?> </h4>
                        <div class="selectRole">
                            <select class="selectUserRole">
                                <option value="all"> <?= Yii::t("cms", "All")?> </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row rowTeam">
                    <!-- Team member -->
                </div>
            </div>
        </section>
        <section id="eventsPrez" class="cardPrez pb-5" data-id="#eventsPrez">
            <div class="containerPrez containerPrezEvents">
                <h5 class="section-title">EVENTS</h5>
                <div class="section-filtre">
                    <div class="box">
                        <h4 class="section-description"> <?= Yii::t("cms", "Our team members organize conferences, meetups, and events.")?></h4>
                        <div class="slideThree">  
                            <input type="checkbox" value="new" id="slideThree" name="check" checked />
                            <label for="slideThree"></label>
                        </div>
                    </div>
                </div>
                <div class="row eventsRow">
                    <?php if (count($allEventChild) <= 0): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="no-event-container">
                                            <div class="no-event-card">
                                                <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                <p> <?= Yii::t("cms", "Create a event for your organisation")?> </p>
                                                <button type="button" data-toggle="tooltip" onclick="creationStep('event')" title="<?= Yii::t("cms", "Create a event for your organisation")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } else { ?>
                                                    <p> <?= Yii::t("cms", "No event active available")?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php foreach ($allEventChild as $event): ?>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="mainflip">
                                    <div class="frontside">
                                        <div class="card">
                                            <div class="card-body text-center cardEvent" data-id="<?= $event["_id"] ?>" data-slug="<?= $event["slug"] ?? "" ?>">
                                                <p><img class="img-fluid" src="<?= $event["profilImageUrl"] ?? "" ?>" onerror="this.src='<?= $imgEventDefault ?>';" alt="card image"></p>
                                                <h4 class="card-title"><?= $event["name"] ?></h4>
                                                <p class="card-text">
                                                    📅 <?= formatDate($event["startDate"]) ?> → <?= formatDate($event["endDate"]) ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
</div>

<script>
        let interval = null;
        if (costum.editMode){   
            cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
            presentation = {
                configTabs : {
                    general : {
                        inputsConfig : [
                            "background",
                            {
                                type : "selectMultiple",
                                options : {
                                    name : "roleToShow",
                                    label : tradDynForm.roles,
                                    canDragAndDropChoise : true,
                                }
                            },
                        ]
                    },
                },
                onChange : function(path,valueToSet,name,payload,value){
                    
                },
                afterSave: function(path,valueToSet,name,payload,value,id){
                    cmsConstructor.helpers.refreshBlock(id, ".cmsbuilder-block[data-id='"+id+"']");
                    costumSocket.emitEvent(wsCO, "update_component", {
                                    functionName: "refreshBlock", 
                                    data:{ id: id}
                    })
                }
                
            }
            cmsConstructor.blocks.presentation<?= $blockKey ?> = presentation; 
        }   

    $(document).ready(function() {

        function updateStickyMenu() {
            let mainNavHeight = $("#mainNav").outerHeight();
            $(".sticky-menu").css("top", mainNavHeight + "px");
        }

        updateStickyMenu();
        $(window).resize(updateStickyMenu);

        $(".cardPrez").on("mouseenter", function() {
            const id = $(this).attr("id");
            const activeItem = $(".sticky-menu-item.active");

            if (activeItem.length && activeItem.attr("data-id") === `#${id}`) {
                return;
            }

            $(".sticky-menu-item").removeClass("active");

            if (id) {
                $(".sticky-menu-item[data-id='#" + id + "']").addClass("active");
            }
        });



        $(".projectPoleItem").on("click", function(){
            const pole = $(this).data("pole");
            if( $('.poleList[data-tags="'+pole+'"]').length > 0 ){
                var target = $("#blockActionsByPole");
                var offset = $("#mainNav").height();

                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - offset
                    }, 800);
                }
                $(".poleList[data-tags='"+pole+"']").trigger("click");
            }
        });

        $(".cardTitle-project").on("click", function(){
            const id = $(this).data("id");
            urlCtrl.openPreview("/view/url/costum.views.custom.coevent.previeworga", 
									{
										"params" : 
										{
											"id" : id, 
											"type": "projects",
                                            "showBackevent": false
										}
									}
								);
        });

        $(".cardEvent").on("click", function(){
            const id = $(this).data("id");
            if($("#slideTwo").is(":checked")){
                var events = <?= json_encode($allEventChild) ?>;
            } else {
                var events =  <?= json_encode($allOldEventChild) ?>;
            }
            ajaxPost(
                null,
                baseUrl + "/co2/event/requestattendees",
                {
                    id: id,
                },
                function(data) {
                    if(data.result){
                        events[id]["links"]["attendees"] = data.attendees ?? [];
                        urlCtrl.openPreview("/view/url/costum.views.custom.coevent.preview", 
                            {
                                "params" : 
                                    {   
                                        "showEdit": false,
                                        "expectedAttendeesOne": false,
                                        "parentId" : "" ,
                                        "event": events[id],
                                        "actors": events[id]["organizer"],
                                        "isFollow": typeof userId !== "undefined" && typeof events[id]?.links?.attendees?.[userId] !== "undefined" ? true : false,
                                        "parentStart" : "", 
                                        "parentEnd" : ""
                                    }
                            }
                        );
                    } else {
                        toastr.error(tradCms.somethingWrong);
                    }
                },
                null,
                "json",
                {
                    async: false
                }
            )
        });

        $('.sticky-menu-item').click(function(event) {
            event.preventDefault();
            $(".sticky-menu-item").removeClass("active");
            $(this).addClass("active");
            var target = $($(this).data("id"));
            var offset = $("#mainNav").height();

            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - offset
                }, 800);
            }
        });

        $("#slideTwo").on("change", function(){
            var item = ``;
            var tags = [];
            $(".projectRow").html("");
            if($(this).is(":checked")){
                <?php if (count($allActiveProjectChild) <= 0): ?> 
                    item += `
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="no-event-container">
                                        <div class="no-event-card">
                                            <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                <p> <?= Yii::t("cms", "Create a project for your organisation")?> </p>
                                                <button type="button" data-toggle="tooltip" onclick="creationStep('project')" title="<?= Yii::t("cms", "Create a project for your organisation")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            <?php } else { ?>
                                                <p> <?= Yii::t("cms", "No result")?> </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
                <?php else: ?>
                    <?php foreach ($allActiveProjectChild as $project): ?>
                        item += creatGridItemProject(<?= json_encode($project) ?>,null);
                        tags = <?= json_encode($allTagsActive) ?>;
                    <?php endforeach; ?>
                <?php endif; ?>
            } else {
                <?php if (count($allProjectChild) <= 0): ?> 
                    item += `
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="no-event-container">
                                        <div class="no-event-card">
                                            <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                <p> <?= Yii::t("cms", "Create a project for your organisation")?> </p>
                                                <button type="button" data-toggle="tooltip" onclick="creationStep('project')" title="<?= Yii::t("cms", "Create a project for your organisation")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            <?php } else { ?>
                                                <p> <?= Yii::t("cms", "No result")?> </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
                <?php else: ?>
                    <?php foreach ($allProjectChild as $keyProject => $project): ?>
                        item += creatGridItemProject(<?= json_encode($project) ?>, "<?= $keyProject ?>");
                        tags = <?= json_encode($allTags) ?>;
                    <?php endforeach; ?>
                <?php endif; ?>
            }
            $(".selectPoleProject").html(`<option value="all" selected>All</option>`);
            $.map(tags, function(value, index) {
                if(value !== "" && value !== null && value !== undefined){
                    $(".selectPoleProject").append(`<option value="${value}">${value}</option>`);
                }
            });
            
            $(".projectRow").html(item);
            $(".cardTitle-project").on("click", function(){
                const id = $(this).data("id");
                urlCtrl.openPreview("/view/url/costum.views.custom.coevent.previeworga", 
                    {
                        "params" : 
                        {
                            "id" : id, 
                            "type": "projects",
                            "showBackevent": false
                        }
                    }
                );
            });

            $(".projectPoleItem").on("click", function(){
                const pole = $(this).data("pole");
                if( $('.poleList[data-tags="'+pole+'"]').length > 0 ){
                    var target = $("#blockActionsByPole");
                    var offset = $("#mainNav").height();

                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top - offset
                        }, 800);
                    }
                    $(".poleList[data-tags='"+pole+"']").trigger("click");
                }
            });
        
        });

        var tags = <?= json_encode($allTagsActive ?? []) ?>;
        $.map(tags, function(value, index) {
            if(value !== "" && value !== null && value !== undefined){
                $(".selectPoleProject").append(`<option value="${value}">${value}</option>`);
            }
        });

        $(".selectPoleProject").on("change", function(){
            const key = $(this).val();
            filterSelectionPole(key);
        })

        $("#slideThree").on("change", function(){
            var item = ``;
            var events = {};
            $(".eventsRow").html("");
            if($(this).is(":checked")){
                <?php if (count($allEventChild) <= 0): ?>
                    item +=`
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="no-event-container">
                                            <div class="no-event-card">
                                                <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                <p> <?= Yii::t("cms", "Create a event for your organisation")?> </p>
                                                <button type="button" data-toggle="tooltip" onclick="creationStep('event')" title="<?= Yii::t("cms", "Create a event for your organisation")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <?php } else { ?>
                                                    <p> <?= Yii::t("cms", "No event active available")?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`
                <?php else: ?>
                    <?php foreach ($allEventChild as $event): 
                        $event['startDate'] = formatDate($event['startDate']);
                        $event['endDate'] = formatDate($event['endDate']);
                    ?>
                        item += creatGridItemEvent(<?= json_encode($event) ?>, "<?= $blockKey ?>");
                    <?php endforeach; ?>
                <?php endif; ?>
                var events = <?= json_encode($allEventChild) ?>;
            } else {
                <?php if (count($allOldEventChild) <= 0): ?>
                    item +=`
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="no-event-container">
                                            <div class="no-event-card">
                                                <i class="fas fa-calendar-times"></i>
                                                <p> <?= Yii::t("cms", "No old event")?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`
                <?php else: ?>
                    <?php foreach ($allOldEventChild as $event): 
                        $event['startDate'] = formatDate($event['startDate']);
                        $event['endDate'] = formatDate($event['endDate']);
                    ?>
                        item += creatGridItemEvent(<?= json_encode($event) ?>, "<?= $blockKey ?>");
                    <?php endforeach; ?>
                <?php endif; ?>
                var events = <?= json_encode($allOldEventChild) ?>;
            }
            $(".eventsRow").html(item);
            $(".cardEvent").on("click", function(){
                const id = $(this).data("id");
                urlCtrl.openPreview("/view/url/costum.views.custom.coevent.preview", 
                    {
                        "params" : 
                            {   
                                "showEdit": false,
                                "expectedAttendeesOne": false,
                                "parentId" : "" ,
                                "event": events[id],
                                "actors": events[id]["organizer"],
                                "isFollow": false,
                                "parentStart" : "", 
                                "parentEnd" : ""
                            }
                    }
                );
            });
        });

        allData = "";
        //params filter for user 
        params<?= $kunik ?> = {};

        params<?= $kunik ?>["searchType"] = ["citoyens"];
        params<?= $kunik ?>["indexMin"] = 0;
        params<?= $kunik ?>["initType"] = "";
        params<?= $kunik ?>["count"] = true;
        params<?= $kunik ?>["countType"] = ["citoyens"];
        params<?= $kunik ?>["indexStep"] = 6000;
        params<?= $kunik ?>["notSourceKey"] = true;
        params<?= $kunik ?>["fediverse"] = false;

        filters<?= $kunik ?> = {'$or':{},'toBeValidated' : {'$exists':false}};
        filters<?= $kunik ?>['$or']["parent."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
        filters<?= $kunik ?>['$or']["source.keys"] = (costum.contextId)?costum.slug:contextData.slug;
        filters<?= $kunik ?>['$or']["links.projects."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
        filters<?= $kunik ?>['$or']["links.events."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
        filters<?= $kunik ?>['$or']["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};

        params<?= $kunik ?>['filters'] = filters<?= $kunik ?>

        fields<?= $kunik ?> = {};
        fields<?= $kunik ?>["tags"] = "tags";
        fields<?= $kunik ?>["collection"] = "collection"
        fields<?= $kunik ?>["name"] = "name"
        fields<?= $kunik ?>["email"] = "email"
        fields<?= $kunik ?>["socialNetwork"] = "socialNetwork"
        fields<?= $kunik ?>["userWallet"] = "userWallet"
        fields<?= $kunik ?>["links"] = "links"
        fields<?= $kunik ?>["profilMediumImageUrl"] = "profilMediumImageUrl"
        fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
        fields<?= $kunik ?>["type"] = trad.type;
        fields<?= $kunik ?>["address.level1Name"] = "Pays";
        fields<?= $kunik ?>["address.level3Name"] = "Région";
        fields<?= $kunik ?>["badges"] = "badges";
        fields<?= $kunik ?>["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)+".roles"] = "Rôles";
        if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
            fields<?= $kunik ?>["tags"] = trad.tags;
        }

        params<?= $kunik ?>['fields'] = fields<?= $kunik ?>;
        
        ajaxPost(
            null,
            baseUrl+"/co2/search/globalautocomplete",
            params<?= $kunik ?>,
            function(response){
                var tags = [];
                var item = ``;
                allData = response.results;
                var roleToShow = <?= json_encode($blockCms["roleToShow"] ?? []) ?>;
                $.each(response.results,function(k,v){
                    if ( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object"){
                        tags = mergeArrays(tags, jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles"));
                    }

                    if ( roleToShow.length > 0 ){
                        if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").indexOf(roleToShow[0]) >= 0 ){
                        item += createGridItemUsers(v, k);
                        }
                    } else {
                        item += createGridItemUsers(v, k);
                    }
                })

                $.map(tags, function(value, index) {
                    if(value !== "" && value !== null && value !== undefined){
                        $(".selectUserRole").append(`<option value="${value}">${value}</option>`);
                    }
                });

                if (costum.editMode) {
                    cmsConstructor.blocks.presentation<?= $blockKey ?>.configTabs.general.inputsConfig[1].options.options = 
                    $.map(tags, function(value, index) {
                        return { value: value, label: value };
                    });
                }

                //<button type="button" data-toggle="tooltip" onclick="creationStep('team')" title="<?= Yii::t("cms", "Add and manage your members role")?>" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                //<i class="fa fa-plus"></i>
                //</button>
                
                if ( item == ``){
                    $(".rowTeam").html(`
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="no-event-container">
                                            <div class="no-event-card">
                                                <i class="fas fa-calendar-times"></i>
                                                <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?> 
                                                    <p> <?= Yii::t("cms", "Establish roles in your community")?> </p>
                                                    <p> <?= Yii::t("cms", "Add and manage your members role")?> </p>
                                                <?php } else { ?>
                                                    <p> <?= Yii::t("cms", "No result")?> </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`
                    );
                }else{
                    $(".rowTeam").html(item);
                    $(".cardUser").on("click", function(){
                        var id = $(this).data("id");
                        params = {};
                        params["elementId"] = costum.contextId;
                        params["userId"] = id;
                        params["name"] = allData[id]["name"];
                        params["socialNetwork"] = typeof allData[id]["socialNetwork"] !== "undefined" ? allData[id]["socialNetwork"] : [];
                        params["profileImg"] = typeof allData[id]["profilMediumImageUrl"] !== "undefined" ? allData[id]["profilMediumImageUrl"] : "";
                        params["badges"] = typeof allData[id]["badges"] !== "undefined" ? allData[id]["badges"] : [];
                        params["userCredits"] = typeof allData[id]["userWallet"] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? allData[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
                        ajaxPost(
                            null,
                            baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                            params, 
                            function (response) {  
                            params["badgesDetails"] = response.badges;
                            params["projects"] = response.projects;
                            params["actionsCreated"] = response.actionsCreated;
                            params["actionsParticipated"] = response.actionsParticipated;
                            params["actionsLast12Month"] = response.actionsLast12Month;
                            params["countAll"] = response.countAll;

                            urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                            }
                        );
                    });
                }

                $(".selectUserRole").on("change", function(){
                    const key = $(this).val();
                    filterSelection(key,allData);
                })
            }
        );

    });

    $(".roleTag").on("click", function(){
        const key = $(this).data("role");
        var target = $("#teamPrez");
        var offset = $("#mainNav").height();

        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top - offset
            }, 800);
        }
        $(".selectUserRole").val(key).trigger("change");
    })

    function filterSelection(key,allData){
        var item = "";
        $(".rowTeam").html("");
        if( key === 'all'){
            $.each(allData,function(k,v){
                   item += createGridItemUsers(v, k, "<?= $blockKey ?>");
            });
        } else {
            $.each(allData,function(k,v){
                if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").includes(key)){
                    item += createGridItemUsers(v, k, "<?= $blockKey ?>");
                } 
            });
        }
        if ( item = ""){
            $(".rowTeam").html(`
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="no-event-container">
                                    <div class="no-event-card">
                                        <i class="fas fa-calendar-times"></i>
                                        <p> <?= Yii::t("cms", "Establish roles in your community")?> </p>
                                        <p> <?= Yii::t("cms", "Add and manage your members role")?> </p>
                                        <?php if( Role::isSuperAdmin(Role::getRolesUserId($userId)) || Authorisation::isElementAdmin($costum["contextId"], $costum["contextType"], $userId) ) { ?>
                                            <button id="btnKambanRole" type="button" data-toggle="tooltip" onclick="openKanbanRole()" title="Gérer les rôles" data-toggle="tooltip" data-placement="top" title="Tooltip on top" class="btn kamban-icon-prez btn-default">
                                                <i class="fa fa-trello"></i>
                                            </button>
                                        <?php } else { ?>
                                            <?= Yii::t("cms", "No result")?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
        } else {
            $(".rowTeam").html(item);
            $(".cardUser").on("click", function(){
                var id = $(this).data("id");
                params = {};
                params["elementId"] = costum.contextId;
                params["userId"] = id;
                params["name"] = allData[id]["name"];
                params["socialNetwork"] = typeof allData[id]["socialNetwork"] !== "undefined" ? allData[id]["socialNetwork"] : [];
                params["profileImg"] = typeof allData[id]["profilMediumImageUrl"] !== "undefined" ? allData[id]["profilMediumImageUrl"] : "";
                params["badges"] = typeof allData[id]["badges"] !== "undefined" ? allData[id]["badges"] : [];
                params["userCredits"] = typeof allData[id]["userWallet"] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? allData[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
                ajaxPost(
                    null,
                    baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                    params, 
                    function (response) {  
                    params["badgesDetails"] = response.badges;
                    params["projects"] = response.projects;
                    params["actionsCreated"] = response.actionsCreated;
                    params["actionsParticipated"] = response.actionsParticipated;
                    params["actionsLast12Month"] = response.actionsLast12Month;
                    params["countAll"] = response.countAll;

                    urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                    }
                );
            });
        }
    }

    function filterSelectionPole(key){
        var item = "";
        var data = [];
        $(".projectRow").html("");
        if($("#slideTwo").is(":checked")){
            data = <?= json_encode($allActiveProjectChild) ?>;
        } else {
            data = <?= json_encode($allProjectChild) ?>;
        }

        if( key === 'all'){
            $.each(data,function(k,v){
                if($("#slideTwo").is(":checked")){
                    item += creatGridItemProject(v, v.id);
                } else { 
                    item += creatGridItemProject(v, k);
                }
            });
        } else {
            $.each(data,function(k,v){
                if( typeof jsonHelper.getValueByPath(v,"tags") === "object" && jsonHelper.getValueByPath(v,"tags").includes(key)){
                    if($("#slideTwo").is(":checked")){
                        item += creatGridItemProject(v, v.id);
                    } else { 
                        item += creatGridItemProject(v, k);
                    }
                } 
            });
        }
        if( item === ""){
            $(".projectRow").html(`
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="no-event-container">
                                    <div class="no-event-card">
                                        <i class="fas fa-calendar-times"></i>
                                        <p> <?= Yii::t("cms", "No result")?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
        } else {
            $(".projectRow").html(item);
            $(".cardTitle-project").on("click", function(){
                const id = $(this).data("id");
                urlCtrl.openPreview("/view/url/costum.views.custom.coevent.previeworga", 
                    {
                        "params" : 
                        {
                            "id" : id, 
                            "type": "projects",
                            "showBackevent": false
                        }
                    }
                );
            });

            $(".projectPoleItem").on("click", function(){
                const pole = $(this).data("pole");
                if( $('.poleList[data-tags="'+pole+'"]').length > 0 ){
                    var target = $("#blockActionsByPole");
                    var offset = $("#mainNav").height();

                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top - offset
                        }, 800);
                    }
                    $(".poleList[data-tags='"+pole+"']").trigger("click");
                }
            });
        }
        
    }

    function createGridItemUsers(value, key) {
      let rolesHtml = value.links && value.links.memberOf && value.links.memberOf[costum.contextId] && value.links.memberOf[costum.contextId].roles && value.links.memberOf[costum.contextId].roles.length 
        ? value.links.memberOf[costum.contextId].roles.map(role => `<span class="badge badge-primary">${role}</span>`).join(' ') 
        : '';

      return `
        <div class="col-xs-12 col-sm-4 col-md-3">
            <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                <div class="mainflip">
                    <div class="frontside">
                        <div class="card">
                            <div class="card-body text-center cardUser" data-id="${key}">
                                <p><img class=" img-fluid" onerror="this.src='<?= $imgAvatarDefault ?>';" src="${value.profilMediumImageUrl}" alt="profile img"></p>
                                <h4 class="card-title">${value.name}</h4>
                                <p class="card-text">${rolesHtml}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      `;
    }

    function creatGridItemEvent(value, key) {
        return `
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="mainflip">
                    <div class="frontside">
                        <div class="card">
                            <div class="card-body text-center cardEvent" 
                                data-id="${value["id"]}" 
                                data-slug="${value["slug"] ?? ""}">
                                <p>
                                    <img class="img-fluid" 
                                        src="${value["profilImageUrl"] ?? '<?= $imgEventDefault ?>'}"
                                        onerror="this.src='<?= $imgEventDefault ?>';" 
                                        alt="card image">
                                </p>
                                <h4 class="card-title">${value["name"]}</h4>
                                <p class="card-text">${value["startDate"]} - ${value["endDate"]}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    function creatGridItemProject(value, key) {
        let polesHtml = value.tags ? value.tags.map(pole => `<span class="badge badge-primary projectPoleItem" data-pole="${pole}">${pole}</span>`).join(' ') : '';
        return `
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="mainflip">
                    <div class="frontside">
                        <div class="card">
                            <div class="card-body text-center" 
                                data-slug="${value["slug"] ?? ""}">
                                <p>
                                    <img class="img-fluid" 
                                        src="${value["profilImageUrl"] ?? '<?= $imgProjectDefault ?>'}"
                                        onerror="this.src='<?= $imgProjectDefault ?>';" 
                                        alt="card image">
                                </p>
                                <h4 class="card-title cardTitle-project" data-id="${ key !== null ? key : value["id"] ?? value["_id"]}" >${value["name"]}</h4>
                                <p class="card-text">${polesHtml}</p>
                                <p class="card-text"><?= $project["shortDescription"] ?? "" ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    function creationStep(option){
        // clearInterval(interval);
        // $(".kl-tooltip-kamban").css('display','none');
		// $(".kl-arrow-kamban").css('display','none');
        // if (option === "team"){
        //     var target = $("#collapseRoleParent");
        //     var offset = $("#mainNav").height();

        //     if (target.length) {
        //         clearInterval(interval);
        //         $('html, body').animate({
        //             scrollTop: target.offset().top - offset - 20
        //         }, 800);
        //         interval = setInterval(function() {
        //             $("#btnKambanRole").css("background-color", "#00FF7F");
        //             $("#btnKambanRole").find(".fa").css("color", "black");
                    
        //             setTimeout(function() {
        //                 $("#btnKambanRole").css("background-color", "#2f2c2c");
        //                 $("#btnKambanRole").find(".fa").css("color", "white");
        //             }, 1000);
        //         }, 2000);

        //         setTimeout(function() {
        //             clearInterval(interval);
        //         }, 10000);
        //         $(".kl-tooltip-kamban").css('display','block');
		// 		$(".kl-arrow-kamban").css('display','block');
        //     }
        // } else if ( option === "project" || option === "event") {
        //     var target = $("#cojazzPannelBoard");
        //     var offset = $("#mainNav").height();
        //     if (target.length) {
        //         if (!$("#collapseTableau").hasClass('in')) target.trigger('click');
        //         $('html, body').animate({
        //             scrollTop: target.offset().top - offset - 20
        //         }, 800);
        //         $(".tooltip-"+option).css('display','block');
		// 		$(".arrow-"+option).css('display','block');
        //         interval = setInterval(function() {
        //             const element = option === 'event' ? $(".createEvent") : $(".createProject");
        //             element.css("background-color", "#00FF7F");
        //             element.find(".fa").css("color", "black");
                    
        //             setTimeout(function() {
        //                 element.css("background-color", "#2f2c2c");
        //                 element.find(".fa").css("color", "white");
        //             }, 1000);
        //         }, 2000);

        //         setTimeout(function() {
        //             clearInterval(interval);
        //         }, 10000);
        //     }
        // }
        if ( option === "event"){
            var preferences = {
                isOpenData : true,
                isOpenEditional : true
            }
            var newEvent = {};
            dyFObj.setMongoId('events', function() {
                    const newEventToAdd = {
                        id : dyFObj.currentElement.id,
                        public : true,
                        scope:null,
                        recurrency: false,
                        costumSlug: costum.slug,
                        costumEditMode: costum.editMode,
                        preferences: preferences,
                        key: "event",
                        collection: "events",
                        timeZone : moment.tz.guess()
                    };

                    Object.assign(newEvent, newEventToAdd);

                    new CoInput({
                        container: "#createvent-image-input",
                        inputs: [
                            {
                                type: "inputFileImage",
                                options: {
                                    name: "image",
                                    label: "Image",
                                    collection: "documents",
                                    defaultValue: "",
                                    canSelectMultipleImage : false,
                                    payload: {
                                        path: "logo",
                                    },
                                    icon: "chevron-down",
                                    class: "logoUploader inputContainerCoEvent",
                                    contentKey: "profil",
                                    uploadOnly: true,
                                    uploaderInitialized : true,
                                    endPoint: "",
                                    domElement: "profil",
                                },
                            },
                            {
                                type : "dateTimePicker",
                                options: {
                                    name : "startDate",
                                    label: tradCms.startDate +" *",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent newStartDate",
                                }
                            },
                            {
                                type : "dateTimePicker",
                                options: {
                                    name : "endDate",
                                    label: tradCms.endDate +" *",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent newEndDate",
                                }
                            },
                            {
                                type : "tags",
                                options : {
                                    name : "tags",
                                    label : "tradDynForm.keyWords",
                                    options : [""],
                                    defaultValue : "",
                                    class: "inputContainerCoEvent",
                                    icon : "chevron-down"
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    name : "email",
                                    label : "E-mail",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    name : "url",
                                    label : "URL",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                }
                            },
                            
                        ],
                        onchange: function(name, value, payload) {
                            if ( name === "image") {
                                jsonHelper.setValueByPath(newEvent, "profilImageUrl", value);
                            } else if ( name === "startDate"){
                                var value = moment(value, "DD/MM/YYYY HH:mm");
                                var currentDate = moment(); // Date et heure actuelles

                                if (value.isBefore(currentDate)) {
                                    $(".warningStartDate").remove();
                                    $(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
                                } else {
                                    $(".warningStartDate").remove();
                                    var dateDebutConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
                                    jsonHelper.setValueByPath(newEvent, name, dateDebutConvertie);
                                }
                            } else if ( name === "endDate"){
                                var value = moment(value, "DD/MM/YYYY HH:mm");
                                var currentDate = moment();
                                var startDate = moment(newEvent.startDate);
                                var dateEndConvertie = value.tz(newEvent.timeZone);
                                if (dateEndConvertie.isAfter(currentDate) && dateEndConvertie.isAfter(startDate)) {
                                    $(".warningEndDate").remove();
                                    jsonHelper.setValueByPath(newEvent, name, dateEndConvertie.format("YYYY-MM-DDTHH:mm:ssZ"));
                                } else {
                                    $(".warningEndDate").remove();
                                    $(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + startDate.format("DD/MM/YYYY HH:mm") + " " + tradCms.and + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
                                }
                            } else if ( name === "tags"){
                                jsonHelper.setValueByPath(newEvent, name,value.split(','));
                            } else {
                                jsonHelper.setValueByPath(newEvent, name,value);
                            }
                        },
                        onblur : function(name , value, payload){
                        
                        }
                    });

                    new CoInput({
                            container: ".create-event-details",
                            inputs: [
                                {
                                    type : "inputSimple",
                                    options: {
                                        name : "name",
                                        label : tradCms.eventName +" *",
                                        defaultValue : "",
                                        icon : "chevron-down",
                                        class: "inputContainerCoEvent",
                                    }
                                },
                                {
                                    type : "select",
                                    options: {
                                        name : "type",
                                        label : tradDynForm.eventTypes +" *",
                                        options :  Object.keys(eventTypes).map(function(value) {
                                            return {
                                                label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
                                                value : value
                                            }
                                        }),
                                        icon : "chevron-down",
                                        class: "inputContainerCoEvent"
                                    }
                                },
                                {
                                    type : "finders",
                                    options: {
                                        name : "organizer",
                                        label: tradDynForm.whoorganizedevent,
                                        initType: ["projects", "organizations"],
                                        multiple: true,
                                        initMe: false,
                                        icon : "chevron-down",
                                        openSearch: true,
                                        initBySearch: true,
                                        class: "inputContainerCoEvent",
                                    }
                                },
                                {
                                    type : "formLocality",
                                    options: {
                                        name : "formLocality",
                                        label: tradCms.place,
                                        icon : "chevron-down",
                                        class: "inputContainerCoEvent",

                                    }
                                },
                                {
                                    type : "textarea",
                                    options: {
                                        name : "shortDescription",
                                        label : tradCms.shortDescription,
                                        defaultValue :"",
                                        icon : "chevron-down",
                                        class: "inputContainerCoEvent",
                                    },
                                },
                            ],
                            onchange: function(name, value, payload) {
                                if ( name === "name" ){
                                    jsonHelper.setValueByPath(newEvent, name, value);
                                } 
                            },
                            onblur: function(name, value, payload) {
                                if ( name === "formLocality"){
                                        var address = [];
                                        var geo = [];
                                        var geoPosition = [];
                                        var addresses = [];
                                        value.forEach((location, index) => {
                                            if (location.center) {
                                                address = location.address;
                                                geo = location.geo;
                                                geoPosition = location.geoPosition;
                                            } else {
                                                addresses.push(location);
                                            }
                                        });
                                        jsonHelper.setValueByPath(newEvent, "address", address);
                                        jsonHelper.setValueByPath(newEvent, "geo", geo);
                                        jsonHelper.setValueByPath(newEvent, "geoPosition", geoPosition);
                                        jsonHelper.setValueByPath(newEvent, "addresses", addresses);
                                } else {
                                    jsonHelper.setValueByPath(newEvent, name, value);
                                } 
                            }
                    });
                    $("#openModalCreateEvent").modal("show");
            });

            $(".create-event-button").off("click").on("click", function(){
                    var idBlock =  $(this).data("id");
                    delete newEvent.images;
                    $("#openModalCreateEvent").css({
                        'z-index': '140000'
                    });
                    $("#firstLoader").show();
                    $("#firstLoader #loadingModal").css({
                        'opacity': '0.5',
                        'z-index': '9999999'
                    });
                    ajaxPost(
                        null,
                        baseUrl+"/co2/element/save",
                        newEvent, 
                        function (response) {  
                            if (response.result){
                                var idBlock = "<?= $blockKey ?>";
                                refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']");
                                var events = {
                                    id: costum.contextId,
                                    collection: "events",
                                    path: "links.subEvents."+newEvent.id,
                                    value : {
                                        type : "events"
                                    },
                                    costumSlug: costum.slug,
                                    costumEditMode: costum.editMode                                                           
                                }
                                dataHelper.path2Value(events, function (params) {
                                    if (params.result){
                                        var attende = {
                                            id : newEvent.id,
                                            collection : "events",
                                            path : "links.attendees."+userId,
                                            value : {
                                                type : "citoyens"
                                            },
                                            costumSlug: costum.slug,
                                            costumEditMode: costum.editMode   
                                        }
                                        dataHelper.path2Value(attende, function (params) {
                                            $("#openModalCreateEvent").modal("hide");
                                            $("#firstLoader").hide();
                                            toastr.success(trad.save);
                                        });
                                    }
                                });
                            } else {
                                toastr.error(response.msg);
                            }                 
                        }
                    );
            });
        } else if ( option === "project") {
            var newProject = {};
            dyFObj.setMongoId('project', function() {
                    const newProjectToAdd = {
                        id : dyFObj.currentElement.id,
                        public : true,
                        scope:null,
                        key: "project",
                        collection: "projects",
                    };

                    Object.assign(newProject, newProjectToAdd);

                    new CoInput({
                        container: "#createproject-image-input",
                        inputs: [
                            {
                                type: "inputFileImage",
                                options: {
                                    name: "image",
                                    label: "Image",
                                    collection: "documents",
                                    defaultValue: "",
                                    canSelectMultipleImage : false,
                                    payload: {
                                        path: "logo",
                                    },
                                    icon: "chevron-down",
                                    class: "logoUploader inputContainerCoEvent",
                                    contentKey: "profil",
                                    uploadOnly: true,
                                    uploaderInitialized : true,
                                    endPoint: "",
                                    domElement: "profil",
                                },
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    name : "name",
                                    label : trad.projectName +" *",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                }
                            },
                            {
                                type : "finders",
                                options: {
                                    name : "parent",
                                    label: tradDynForm.whoiscarrytheproject,
                                    initType: ["projects", "organizations"],
                                    multiple: true,
                                    initMe: false,
                                    icon : "chevron-down",
                                    openSearch: true,
                                    initBySearch: true,
                                    class: "inputContainerCoEvent",
                                }
                            },
                            {
                                type : "formLocality",
                                options: {
                                    name : "formLocality",
                                    label: tradCms.place,
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",

                                }
                            },
                            {
                                type : "textarea",
                                options: {
                                    name : "shortDescription",
                                    label : tradCms.shortDescription,
                                    defaultValue :"",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                },
                            },
                            {
                                type : "tags",
                                options : {
                                    name : "tags",
                                    label : "Pole",
                                    options : [""],
                                    defaultValue : "",
                                    class: "inputContainerCoEvent",
                                    icon : "chevron-down"
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    name : "email",
                                    label : "E-mail",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                }
                            },
                            {
                                type : "inputSimple",
                                options: {
                                    name : "url",
                                    label : "URL",
                                    defaultValue : "",
                                    icon : "chevron-down",
                                    class: "inputContainerCoEvent",
                                }
                            },
                            
                        ],
                        onchange: function(name, value, payload) {
                            if ( name === "name" ){
                                    jsonHelper.setValueByPath(newProject, name, value);
                            } else if ( name === "image") {
                                jsonHelper.setValueByPath(newProject, "profilImageUrl", value);
                            } else if ( name === "tags"){
                                jsonHelper.setValueByPath(newProject, name,value.split(','));
                            } else {
                                jsonHelper.setValueByPath(newProject, name,value);
                            }
                        },
                        onblur : function(name , value, payload){
                            if ( name === "formLocality"){
                                    var address = [];
                                    var geo = [];
                                    var geoPosition = [];
                                    var addresses = [];
                                    value.forEach((location, index) => {
                                        if (location.center) {
                                            address = location.address;
                                            geo = location.geo;
                                            geoPosition = location.geoPosition;
                                        } else {
                                            addresses.push(location);
                                        }
                                    });
                                    jsonHelper.setValueByPath(newProject, "address", address);
                                    jsonHelper.setValueByPath(newProject, "geo", geo);
                                    jsonHelper.setValueByPath(newProject, "geoPosition", geoPosition);
                                    jsonHelper.setValueByPath(newProject, "addresses", addresses);
                            } else {
                                jsonHelper.setValueByPath(newProject, name, value);
                            } 
                        }
                    });

                    $("#openModalCreateProject").modal("show");
            });

            $(".create-project-button").off("click").on("click", function(){
                delete newProject.images;
                $("#openModalCreateProject").css({
                    'z-index': '140000'
                });
                $("#firstLoader").show();
                $("#firstLoader #loadingModal").css({
                    'opacity': '0.5',
                    'z-index': '9999999'
                });
                ajaxPost(
                    null,
                    baseUrl+"/co2/element/save",
                    newProject, 
                    function (response) {  
                        if (response.result){
                                $("#openModalCreateProject").modal("hide");
                                $("#firstLoader").hide();
                                toastr.success(trad.save);
                                var idBlock = "<?= $blockKey ?>";
                                refreshBlock(idBlock, ".cmsbuilder-block[data-id='" + idBlock + "']");
                            } else {
                            toastr.error(response.msg);
                        }      
                    }
                );
            });
        }     
    }


    function get_actors(__types = ['animator', 'organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName'],id,type) {
        var url = baseUrl + '/costum/coevent/get_events/request/actors/id/' + id + '/type/' + type;
        var parameter = {
            types: __types
        }
        return new Promise(function(__resolve, reject) {
            ajaxPost(null, url, parameter, __resolve, reject)
        });
    }


    

</script>