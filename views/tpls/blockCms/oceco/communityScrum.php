<?php

$styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
$otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$imgAvatarDefault = Yii::app()->getModule("costum")->assetsUrl."/images/blockOceco/avatar.png";
$linkToShow = "https://peertube.communecter.org/videos/embed/41f5ab8f-9ec9-4a3f-9c6c-94f5033357d0";   
?>

<style>
    .containerCoscrum {
        margin: 10px;
    }

    .profile {
        display: flex;
        align-items: center;
        margin-bottom: 30px;
    }

    .profile img {
        border-radius: 50%;
        width: 100px;
        height: 100px;
        margin-right: 20px;
    }

    .profile h2 {
        margin: 0;
        font-size: 1.8em;
        color: #007bff;
    }

    .profile p {
        margin: 0;
        color: #777;
    }

    .person {
        border-left: 1px solid #e54e5e;
        border-bottom: 2px solid #302c2c;
    }

    .person h3 {
        color: #007bff;
        font-size: 1.5em;
    }

    .task {
        display: flex;
        align-items: center;
        margin: 8px 0;
    }

    .task input[type="checkbox"] {
        margin-right: 10px;
    }

    .task span {
        font-size: 1.1em;
        color: #272626;
    }

    .completed {
        text-decoration: line-through;
        color: #999;
    }

    .task input[type="checkbox"]:checked + span {
        text-decoration: line-through;
        color: #999;
    }

    .task input[type="checkbox"]:not(:checked) + span {
        color: #272626;
    }

    .chevron {
        float: inline-end;
    }

    /* .panel-body {
        padding-left: 40px !important;
    } */

    .filter<?= $kunik ?>LG{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .userScrum<?= $kunik ?> label{
        margin-top: 10px !important;
    }

    .<?= $kunik ?> {
        background: #2d2b2b; 
        padding: 10px; 
        display: flex; 
        justify-content: center;
    }
    .<?= $kunik ?> .category-card {
        display: inline-block;
        width: 120px;
        height: 120px;
        margin: 10px;
        text-align: center;
        position: relative;
        border-radius: 10px;
        /* background-color: yellow; */
        /* overflow: hidden; */
        box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
        transition: transform 0.3s ease, box-shadow 0.3s ease;
        cursor: pointer; 
    }

    .<?= $kunik ?> .category-card:hover {
        transform: scale(1.05);
        box-shadow: 0px 6px 8px rgba(0, 0, 0, 0.2);
    }

     
    .<?= $kunik ?> .category-image {    
        width: 80%;
        position: absolute;
        left: 10%;
        right: 10%;
        z-index: 1;
        top: -23%;
        height: 110px;
        object-fit: cover;
        clip-path: polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%);
    }

    .<?= $kunik ?> .category-title {
        padding: 10px;
        border-radius: 0px 0 10px 10px;
        /* background-color: rgba(0, 0, 0, 0.5); */
        color: white;
        font-size: 14px;
        text-transform: capitalize;
        position: absolute;
        bottom: 0;
        width: 100%;
        font-weight: bold;
        overflow: auto; /* Ajouter des barres de défilement */
        max-height: 50px; /* Limiter la hauteur */
    }

    .<?= $kunik ?> h2 {
        text-align: center;
    }

    .<?= $kunik ?> #projectActives {
        margin-top: 2%;
        text-align: center;
    }
    .<?= $kunik ?> .section-title {
        font-size: 22px;          
        font-weight: bold;       
        color: #2C3E50;          
        text-align: center;       
        margin-bottom: 10px;      
        text-transform: uppercase;
        margin-top: 1%; 
        text-transform: none;
    }
    @media (max-width: 900px) {
        .<?= $kunik ?> .category-card {
            margin-top: 10%;
        }
    }

	.custom-block-cms:has(.projectActiveList){
		background: #2d2b2b !important;
	}
	.<?= $kunik ?> .kl-container-co{
        width: 1250px;
        /* max-width: 1536px;
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        grid-gap: 20px; */
	}

    @media (min-width: 769px) {
        .<?= $kunik ?> .kl-container-co{
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }
    }

    #corner-action-preview {
        z-index: 10;
        position: relative;
	}

    .co-panel-heading {
        padding: 15px;
        display: flex;
        justify-content: space-between;
        cursor: pointer;
    }

    .co-see-details {
        padding: 5px 14px;
        border-radius: 9px;
        border: 1px solid #28a745;
        background: #415d4f;
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .co-see-details:hover {
        background: #156f2a;
        color: #fff;
    }
    .co-see-details i.fa-eye {    
        color: #fff;
        width: 15px;
        height: 15px;
        border-radius: 50%;
        display: flex;
        background: #28a745;
        align-items: center;
        justify-content: center;
        font-size: 10px;
    }

    .co-panel {
        background-color: #272626;
        border: 1px solid transparent;
        -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
        box-shadow: 0 1px 1px rgb(154 149 149 / 5%);
    }
    .co-panel:hover {
        background-color: #000;
    }

    .co-panel-body {
        background-color: #444;
    }

	.tasks{
		background-color: black;
		padding: 20px;
	}

    .co-to-expanded {
        display: none;
    }

    .expanded-view {
        z-index: 2;
        position: absolute;
		right: 90px;
    	width: max-content;
    	top: 48px;
    }

    .child-task{
        padding-left: 20px;
    }

	.<?= $kunik ?> .nav li a {
		color : white;
	}

	.tab-pane {
		height: 700px;
		overflow-y: auto;
	}

	.btn-list-project {
		display: flex;
		color: white ;
		padding: 10px;
		border: 1px solid #000;
		border-radius: 10px 10px 0 0;
	}

	.btn-list-project:hover{
		background: #000;
	}

	.co-project-active{
		background: #000 ;
		border: 1px solid white;
		border-bottom: 0px;
	}

	.child-project {
		display: flex; 
		background: #444;
        border-bottom: 1px solid #7f7e7e;
		padding: 7px; 
		height: 45px
	}

	.child-project:hover{
		background: #272626;		
	}

	.loadKamban i:hover{
		background: #337ab7;
	}
	.menu-tab-filter {
		padding: 0 20px;
		background: #272626;
		min-height: 70vh;
	}

	.kl-content-collapse {
		display: flex;
		justify-content: center;
		align-items: center;
		width: 100px;
	}
	.kl-content-collapse-child {
		display: flex;
		align-items: center;
		position: relative;
	}
    .mobile-show {
        display: none;
    }
    .mobile-hidde {
        display: block;
    }

    .sm-block {
        display: flex;
        align-items: center;
    }

	@media screen and (max-width: 770px) {
		.menu-tab-filter {
			min-height: min-content;
		}

		.tab-pane {
			height: auto;
		}
		.kl-dashboard{
			display: block;
		}
		.co-panel-heading{
			display: block;
		}
		.kl-content-collapse-child {
			justify-content: end;
		}

        .mobile-hidde {
            display: none;
        }
        .mobile-show {
            display: block;
        }
        .sm-padding {
            padding: 5px 0px !important;
        }

        .padding-20:has(.mobile-show){
            padding: 20px 10px !important;
        }
	}

    .panel-heading .accordion-toggle:after {
        font-family: 'Glyphicons Halflings';
        content: "\e114";    
        float: right;       
        color: grey;        
    }
    .panel-heading .accordion-toggle.collapsed:after {
        content: "\e080";
    }
    
    select {
        -webkit-appearance:none;
        -moz-appearance:none;
        -ms-appearance:none;
        appearance:none;
        outline:0;
        box-shadow:none;
        border:0!important;
        background: #5c6664;
        background-image: none;
        flex: 1;
        padding: 0 .5em;
        color:#fff;
        cursor:pointer;
        font-size: 1em;
        font-family: 'Open Sans', sans-serif;
    }
    select::-ms-expand {
        display: none;
    }
    .select {
        position: relative;
        display: flex;
        width: 20em;
        height: 2em;
        line-height: 2;
        background: #5c6664;
        overflow: hidden;
        border-radius: .25em;
    }

    .status{
        position: relative;
        display: flex;
        width: max-content;
        height: 2em;
        line-height: 2;
        background: #5c6664;
        overflow: hidden;
        border-radius: .25em;
        padding: 0 .5em;
        font-size: 1em;
        font-family: 'Open Sans', sans-serif;
    }
    .select::after {
        content: "\25bc";
        position: absolute;
        top: 0;
        right: 0;
        padding: 0 1em;
        width: auto;
        background: #2b2e2e;
        cursor:pointer;
        pointer-events:none;
        transition:.25s all ease;
    }
    .select:hover::after {
        color: #23b499;
    }

    @media screen and (max-width: 780px) {
        .select {
            width: 3em;
        }

        .col-lg-12.mobile-show {
            padding: 0px !important;
        }

        .actionsName{
            width: 70%;
        }

        .actionsItems{
            padding: 0px;
        }

    }

    .imac-wrapper<?= $blockKey ?> {
        width: 100%;
        margin: 0 auto;
        padding-top: 10px;
        padding-bottom: 3%;
    }

    .imac-wrapper<?= $blockKey ?> .imac {
        padding: 3%;
        margin: 0 auto;
        max-width: 60%;
        margin-bottom: 8%;
        position: relative;
        border-radius: 10px;
        background: #303030;
        background: linear-gradient(to bottom left, #303030, #303030 35%, #1d1d1d 35.1%, #1d1d1d);
    }

    .imac-wrapper<?= $blockKey ?> .imac .imac-inner {
        position: relative;
        padding-bottom: 60%;
    }

    .imac-wrapper<?= $blockKey ?> .imac .imac-inner:before {
        content: ' ';
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 30, 60, 0.15);
    }

    .imac-wrapper<?= $blockKey ?> .imac .imac-inner iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 1;
        border-radius: 5px;
    }

    .imac-wrapper<?= $blockKey ?> .imac .imac-inner .imac-stand {
        position: absolute;
        top: 105%;
        width: 22%;
        left: 39%;
        height: 13%;
        background: #ecf2f4;
        background: linear-gradient(to bottom, #97a0ae, #c9cfda 45%, #ecf2f4 70%, #97a0ae);
        -webkit-transform-style: preserve-3d;
        transform-style: preserve-3d;
        -webkit-transform: perspective(300px) rotateX(20deg);
        transform: perspective(300px) rotateX(20deg);
        box-shadow: 0 25px 30px rgba(0, 0, 0, 0.15);
    }

    .imac-wrapper<?= $blockKey ?> .imac .imac-inner .imac-stand:after {
        content: ' ';
        display: block;
        top: 49%;
        width: 100%;
        height: 100%;
        background: #ecf2f4;
        background: linear-gradient(to bottom, #97a0ae, #97a0ae 75%, #808894);
        -webkit-transform-style: preserve-3d;
        transform-style: preserve-3d;
        border-radius: 0 0 10% 10%;
        -webkit-transform: perspective(150px) rotateX(50deg);
        transform: perspective(150px) rotateX(50deg);
        box-shadow: inset 0 0 3px #76797a, 0 2px #ecf2f4;
    }

    .tutorielOceco {
        display: grid;
    }

    .blink-textScrum {
      animation: blink 1s infinite alternate;
    }

    @keyframes blink {
        0% { color: red; }
        50% { color: white; }
    }

    .actionsName{
        width: 90%;
    }

    .coscrumCreateProject{
        margin-left: 10px;
    }

    .add-button {
        background: linear-gradient(135deg, #00c6ff, #0072ff);
        color: #fff;
        font-size: 24px;
        border: none;
        padding: 10px 15px;
        border-radius: 12px;
        cursor: pointer;
        transition: all 0.3s ease-in-out;
        box-shadow: 0 4px 10px rgba(0, 114, 255, 0.3);
    }

    .add-button:hover {
        background: linear-gradient(135deg, #0072ff, #00c6ff);
        box-shadow: 0 6px 15px rgba(0, 114, 255, 0.5);
        transform: scale(1.1);
    }

    .messageOceco{
        cursor: pointer;
    }

</style>
<style id="communityScrum<?= $kunik ?>"></style>

<div class="<?= $kunik ?> padding-20 other-css-<?= $kunik ?> <?= $otherClass ?>">
    <div class="kl-container-co">
        <div class="col-lg-12 mobile-show">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default accordeonProject" style="display: none; border-color: #272626;">
                    <div class="panel-heading" style="background-color: #272626; border-color: #272626; height: 60px;">
                        <h4 class="panel-title" style="margin-top:10px;">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #fff; text-decoration: none;width: 100%; display: block;">
                                <i class="icon fa fa-lightbulb-o"></i>  <?php echo Yii::t("cms", "Projects") ?> </a>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div id="pills-project-sm" class="panel-body" style="background-color: #272626; border-top-color: #000; color: #ddd;"></div>
                    </div>
                </div>
                <div class="panel panel-default accordeonEvent" style="display: none; border-color: #272626;">
                    <div class="panel-heading" style="background-color: #272626; border-color: #272626; height: 60px;">
                        <h4 class="panel-title" style="margin-top:10px;">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" style="color: #fff; text-decoration: none;width: 100%; display: block;">
                                <i class="icon fa fa-calendar"></i> <?php echo Yii::t("cms", "Events") ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div id="pills-event-sm" class="panel-body" style="background-color: #272626; border-top-color: #000; color: #ddd;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 mobile-hidde" style="padding: 10px 0;">
            <div class="col-lg-4">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link project-link" id="pills-project-tab" data-toggle="pill" href="#pills-project" role="tab" aria-controls="pills-project" aria-selected="true" style="display: none;"><i class="icon fa fa-lightbulb-o"></i>  <?php echo Yii::t("cms", "Projects") ?><button class="kl-create coscrumCreateProject add-button" data-type="Project" > + </button> </a>
                    </li>
                    <li class="nav-item" style="">
                        <a class="nav-link project-link" id="pills-event-tab" data-toggle="pill" href="#pills-event" role="tab" aria-controls="pills-event" aria-selected="false" style="display: none;"><i class="icon fa fa-calendar"></i>  <?php echo Yii::t("cms", "Events") ?> <button class="kl-create coscrumCreateProject add-button" data-type="Event" > + </button>  </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-12 sm-padding">
            <div class="col-lg-4 col-xs-12 col-md-4 mobile-hidde" style="padding: 0px !important;">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane co-scroll fade menu-tab-filter" id="pills-project" role="tabpanel" aria-labelledby="pills-project-tab">...</div>
                    <div class="tab-pane co-scroll fade menu-tab-filter" id="pills-event" role="tabpanel" aria-labelledby="pills-event-tab">...</div>
                </div>
            </div>
            <div class="panelActionsList col-lg-8 col-md-8 col-sm-12 col-xs-12">
                
                <div class="userScrum<?= $kunik ?> tab-pane co-scroll">
                    <!-- Liste de personnes avec leurs tâches -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Preview Actions -->
<div id="corner-action-preview"></div>

<script>
	if (costum.editMode){
      cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?> ;

	  var communityScrum = {
			configTabs : {
                general : {
                    inputsConfig : [
                        {
							type: "selectMultiple",
							options: {
								name: "parentType",
								label: "Actions parent type",
								options: $.map({ 
									"organizations" : "organizations",
									"projects" : "projects",
									"events" : "events"
								}, function (key, val) {
									return {value: val, label: key}
								})
							}
                        },
						{
							type: "inputSimple",
							options: {
								name: "range",
								label: "Nombre de jours",
							}
                        },
                        "backgroundColor"
                    ]
                },
                style : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
            onchange: function (path,valueToSet,name,payload,value,id){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                if (name === 'parentType' || name === 'range'){
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
            }
		}
		cmsConstructor.blocks.communityScrum<?= $blockKey ?> = communityScrum;

    }

    cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#communityScrum<?= $kunik ?>").append(str);

    //params filter for user 
    params<?= $kunik ?> = {};
    params<?= $kunik ?>["searchType"] = ["citoyens"];
    params<?= $kunik ?>["indexMin"] = 0;
    params<?= $kunik ?>["initType"] = "";
    params<?= $kunik ?>["count"] = true;
    params<?= $kunik ?>["countType"] = ["citoyens"];
    params<?= $kunik ?>["indexStep"] = 6000;
    params<?= $kunik ?>["notSourceKey"] = true;
    params<?= $kunik ?>["fediverse"] = false;
    filters<?= $kunik ?> = {'$or':{},'toBeValidated' : {'$exists':false}};
    filters<?= $kunik ?>['$or']["parent."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["source.keys"] = (costum.contextId)?costum.slug:contextData.slug;
    filters<?= $kunik ?>['$or']["links.projects."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["links.events."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    params<?= $kunik ?>['filters'] = filters<?= $kunik ?>;
    fields<?= $kunik ?> = {};
    fields<?= $kunik ?>["name"] = "name"
    params<?= $kunik ?>['fields'] = fields<?= $kunik ?>;

    var allData = "";
    
    ajaxPost(
        null,
        baseUrl+"/co2/search/globalautocomplete",
        params<?= $kunik ?>,
        function(response){
          var tags = [];
          var item = ``;
		  var parentTypeToFind = <?= json_encode($blockCms["parentType"]) ?>;
		  var range = <?= json_encode($blockCms["range"]) ?>;
          allData = response.results;
          ajaxPost(
                null,
            baseUrl+'/'+moduleId+"/action/getactionsbyallidsparentwithrange/",
                { costumType : costum.contextType , costumId : costum.contextId , parentType : parentTypeToFind , range : range},
                function(actionData){ 
                    if ( typeof actionData.data.allProjects !== "undefined" ) {
                        createListProjectClassed(actionData.data.allProjects);
                        
                        // displaySportsTags(actionData.data.allProjects);           ;
                        $.each(actionData.data.allActions,function(k,v){ 
                            if ( typeof allData[v.userId] !== "undefined" ){
                                item += createGridItem(k, v, "<?= $blockKey ?>" , allData[v.userId]);
                            }
                        });
                        $(".userScrum<?= $kunik ?>").append(item);

                        $(".actionChangeStatus").off("change").on("change", function () {
                            const actionId = $(this).attr("data-id");
                            const status = $(this).val();
                            coInterface.actions.request_set_status({
                                id: actionId,
                                status: status
                            }).then(function (response) {
                                toastr.success(coTranslate('Status change, success!'));
                                if ( status == "done" ) {
                                    var newDoneCounter = $("."+userId+"Counter").data("done") + 1;
                                    if ( newDoneCounter <= $("."+userId+"Counter").data("total") ){
                                        $("."+userId+"Counter").data("done", newDoneCounter);
                                        $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                        $("#task-"+actionId).html("<i class='fa fa-check-square' style='background: #28a745; color: #fff'></i>")
                                    }
                                } else {
                                    if ( $("."+userId+"Counter").data("done") != "0"){
                                        var newDoneCounter = $("."+userId+"Counter").data("done") - 1;
                                        $("."+userId+"Counter").data("done", newDoneCounter);
                                        $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                        $("."+userId+"Counter").data("done", newDoneCounter);
                                        $("#task-"+actionId).html("<i class='fa fa-minus-square' style='color: #fff; background: #272626 '></i>")
                                    }
                                }
                            }).catch(function (arg0) {
                                toastr.error(coTranslate('Something went wrong. Please contact an administrator.'));
                            });
                        });

                    } else {
                        $(".<?= $kunik ?>").html(`
                        <div class="tutorielOceco">
                            <div class="messageOceco text-center">
                                <span> <?= Yii::t('cms', 'Optimize your organization now by creating and assigning actions on') ?>  </span>
                                <a class="blink-textScrum" href="https://oce.co.tools/"  style="font-size: 1.4em;" target="_blank"> oceco !</a>
                                <span> <?= Yii::t('cms', 'Ensure effective follow-up and better collaboration to reach your goals faster.') ?> </span>
                            </div>
                            <div class="imac-wrapper<?= $blockKey ?>">
                                    <div class="imac">
                                        <div class="imac-inner">
                                            <!-- <img class="this-content-<?= $kunik ?>" src=""> -->
                                            <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="<?= $linkToShow ?>?title=0&warningTitle=0&controls=0&peertubeLink=0&loop=1&autoplay=1&muted=1" frameborder="0" allowfullscreen></iframe>
                                            <div class="imac-stand"></div>
                                        </div>
                                    </div>
                            </div>
                        </div>`)
                    }
                }
            );
        }
    );

    function createGridItem(key, value, blockKey, user) {
        // Générer les tâches dynamiquement à partir des actions
        //no child
        const tasksHtml = value.actions.map(action => {
            const isChecked = action.status === "done" ? "checked" : "";
            const taskClass = action.status === "done" ? "completed" : "";
            var taskChildren =``;
			var edition = ``;
			const actualStatus = getActionStatus(action);
            // create task children
            if ( typeof  action.tasks !== "undefined" )  {
                action.tasks.forEach(task => {
                    taskChildren += `
                        <div class="task" style="color: white; display: flex; position: relative;">
                            <div style="width: 10px; display: flex; align-items:center; justify-content: center; margin-right: 10px;" id="task-${task.taskId}">
                                <i class="fa ${task.checked ? "fa-check-square" : "fa-minus-square"} " style="${task.checked ? "background: #28a745; color: #fff" : "color: #fff; background: #272626"   } "></i>
                            </div>
                            <div style="font-size: 15px;">
                                <label for="task-${task.taskId}" data-id="${task.taskId}" style="font-weight: normal;">${task.task}</label>
                            </div>
                        </div>
                    `
                });
            }

			if (typeof userId !== "undefined" && userId === action.authorId) {
					edition = `
                            <div class="select">
                                <select name="format" id="format" class="actionChangeStatus" data-id="${action._id.$id}">
                                    <option value="discuter" ${actualStatus === "discuter" ? "selected" : ""} > To Discuss </option>
                                    <option value="next" ${actualStatus === "next" ? "selected" : ""} > Next </option>
                                    <option value="tracking" ${actualStatus === "tracking" ? "selected" : ""} > In Progress </option>
                                    <option value="totest" ${actualStatus === "totest" ? "selected" : ""} > To test </option>
                                    <option value="done" ${actualStatus === "done" ? "selected" : ""} > Done </option>
                                </select>
                            </div>
					`;
			} else {
                edition = `<span class="status"> ${actualStatus} </span>`;
            }

			if (actualStatus === "totest"){
				$(".totest"+action.parentId).css("display", "block");
			}

            return `
                <div class="task" style="position: relative;color: white; display: flex;">
                    <div style="width: 10px; display: flex; align-items:center; justify-content: center; margin-right: 10px;" id="task-${action._id.$id}">
                        <i class="fa ${isChecked ? "fa-check-square" : "fa-minus-square"} " style="${isChecked ? "background: #28a745; color: #fff" : "color: #fff; background: #272626"   } "></i>
                    </div>
                    <div class="actionsItems col-md-12 col-sm-12 col-xs-12 sm-block" style="font-size: 17px;">
						<div class="actionsName">
                        	<label for="task-${action._id.$id}" data-id="${action._id.$id}" onclick="showDetails('${action._id.$id}')" class="{taskClass}" style="font-size: 17px !important;font-weight: normal;cursor: pointer;">${action.name}</label>
						</div>
						${edition}
					</div>
                </div>
                <div class="child-task" style="color: white;">
                    ${taskChildren}
                </div>
            `;
        }).join(""); 


		//${isChecked ? "<strong style='position: absolute; right: 20px; top: 5px;'><i class='fa fa-check' style='color: green; background: transparent; font-size: 14px; float: right'></i></strong>" : "" }
        //have child
        const tasksWithChildHtml = value.actions.map(action => {
            const isChecked = action.status === "done" ? "checked" : "";
            const taskClass = action.status === "done" ? "completed" : "";
            return `
                <div class="task" style="color: white; display: block;">
                    <div class="task" style="color: white; display: flex;">
                        <div style="width: 10px; display: flex; align-items:center; justify-content: center; margin-right: 10px;" id="task-${action._id.$id}">
                            <i class="fa fa-minus-square " style="color: #fff; background: #fff"></i>
                        </div>
                        <div style="font-size: 17px;">
                            <label for="task-${action._id.$id}" data-id="${action._id.$id}" onclick="showDetails('${action._id.$id}')" class="{taskClass}" style="font-weight: normal;">${action.name}</label>
                        </div>
                    </div>
                    <div>
                        <div class="task" style="color: white; display: flex; position: relative;">
                            <div style="width: 10px; display: flex; align-items:center; justify-content: center; margin-right: 10px;" id="task-${action._id.$id}">
                                <i class="fa ${isChecked ? "fa-check-square" : "fa-minus-square"} " style="${isChecked ? "background: #28a745; color: #fff" : "color: #fff; background: #fff" }"></i>
                            </div>
                            <div style="font-size: 17px;">
                                <label for="task-${action._id.$id}" data-id="${action._id.$id}" onclick="showDetails('${action._id.$id}')" class="{taskClass}" style="font-weight: normal;">${action.name} 
									${isChecked ? "<strong style='position: absolute; right: 20px; top: 5px;'><i class='fa fa-check' style='color: green; background: transparent; font-size: 14px; float: right'></i></strong>" : "" }
								</label>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        }).join(""); // Convertir le tableau en une seule chaîne HTML

        // Les projet 
        const allProjectParent = value.distinctParentIds.map(project =>{
            return `
                <div style="display: block; background: #272626;">
                    <div class="child-project" style="">                                
                        <div class="kl-content-collapse">
                            <img src="${project.profilImageUrl}" style="width:40px; height: 40px; border-radius: 50%; object-fit: cover;" />
                        </div>
                        <div style="display: flex; align-items: center; padding-left: 10px;color: #fff; font-size: 14px;">${project.name}</div>
                    </div>
                </div>
            `
        }).join("");


        // Retourner le HTML complet pour l'élément
        return `
            <div class="person userScrumPanel<?= $kunik ?>">
                <div class="co-panel panel-default">
                    <div class="co-panel-heading" id="panelHeading${blockKey}">
                        <div style="display: flex; width: 90%" data-toggle="collapse" data-parent="#accordion" href="#collapse${key}">
                            <div class="kl-content-collapse">
                                <img src="${user.profilMediumImageUrl}" onerror="this.src='<?= $imgAvatarDefault ?>';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                            </div>
                            <div style="display: grid; padding-left: 10px; align-items: center">
                                <div style="color: #fff; font-size: 16px;">
                                    ${user.name}
                                </div>
                            </div>
                        </div>
                        <div class="kl-content-collapse-child">
                            <div class="expended-parent">
                                <div class="expanded-view hidden" style="">
                                    <button onclick="openExpended(this)" style="border: none; color: red; position: absolute; top: -10px; right: 0; background: #444; border-radius: 50%;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    <ul class="co-scroll" style="overflow-y: auto; max-height: 270px;padding: 10px; background:#444;border-radius: 10px;">
                                        <li>
                                            ${allProjectParent}
                                        </li>
                                    </ul>
                                </div>
                                <button class="co-see-details"  data-toggle="tooltip" title="Voir les projets" onclick="openExpended(this)" data-key="${key}">
                                    <i class="fa fa-eye" style="margin-right: 5px"></i>
                                    ${value.distinctParentIds.length}
                                </button>
                            </div>
                            <div style="margin-left: 10px; color: #fff;width: 90px;display: flex;justify-content: end;align-items: center;" data-toggle="collapse" data-parent="#accordion" href="#collapse${key}">
                                <span class="${value.userId}Counter" data-done="${value.doneCount}" data-total="${value.totalCount}" >${value.doneCount}/${value.totalCount}</span>
                                <i class="fa fa-chevron-down" style="margin-left:5px;"></i>
                            </div>
                        </div>
                    </div>
                    <div id="collapse${key}" class="panel-collapse collapse">
                        <div class="panel-body co-panel-body">
                            <span class="collapse-desc">
                                <div class="tasks">
                                    ${tasksHtml}
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

	function createListProjectClassed(projectClassed) {

		$imgProjectDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/project.png";
		$imgEventDefault = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/event.png";

		if ( projectClassed.projects.length > 0){
			$(".panelProjectList").css("display","block");
			$("#pills-project-tab").css("display","block");
			$(".accordeonProject").css("display","block");
			$(".panelActionsList").removeClass("col-sm-12 col-md-12 col-lg-12").addClass("col-sm-12 col-md-8 col-lg-8");
			const viewProject = projectClassed.projects.map(project =>{
				const backgroundStyle = `linear-gradient(to right, #337ab7 ${project.progression}%, #272626 ${project.progression}%)`;
				const gif = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/totest.webp";
				return `
					<div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
						<div class="btn-list-project">                               
							<div class="projectActiveList" data-id="${project.id}"  data-name="${project.name}" data-slug="${project.slug}" style="display: flex; width: 90% ;justify-content: start; align-items: center;">
								<div>
									<img src="${project.profilImageUrl}"  onerror="this.src='${$imgProjectDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
								</div>
								<div style="display: flex; align-items: center; padding-left: 10px;">${project.name}</div>
							</div>
							<div class="progression" style="margin-left: 10px;display: flex ;color: #fff;justify-content: end;align-items: center;">
									<span data-toggle="tooltip" title="progression" style="font-size:20px;margin-left:5px;margin-right:5px;">${project.progression}%</span>
									<img src="${gif}" data-id="${project.id}"  class="totest${project.id} actionChangeStatus showToTest" style="display: none;width:30px; heigth:30px; right: 4%; top: 5px;">
							</div>
							${ typeof isSuperAdmin !== "undefined" && isSuperAdmin ? `
								<div class="loadKamban loadKambanProject" data-name="${project.name}" data-slug="${project.slug}" data-tag="${project.id}" style="margin-left: 10px;color: #fff;display: flex;justify-content: end;align-items: center;">
									<i class="fa fa-trello fa-lg" data-toggle="tooltip" title="kamban" style="font-size:25px;margin-left:5px;"></i>
								</div>` : ``
							}
						</div>
						<div data-toggle="tooltip" title="progression ${project.progression}%" style="border-radius: 0 0 10px 10px;height: 2px;background: ${backgroundStyle}"></div>
					</div>
				`
			}).join("");

			$("#pills-project").html(viewProject);
			$("#pills-project-sm").html(viewProject);

			$(".projectActiveList").off("click").on('click', function(e){
                e.stopPropagation();
				const projectId = $(this).data("id");
				// $(this).addClass('co-list-active');
				$(".btn-list-project").removeClass('co-project-active');
				$(this).parent(".btn-list-project").addClass('co-project-active');
                const name = $(this).data("name");
                const slug = $(this).data("slug");
                findActionById(projectId,"project",name,slug);
			});

			$(".loadKambanProject").off("click").on('click', function(e){
				e.stopPropagation();
				var id = $(this).attr("data-tag");
				var name = $(this).attr("data-name");
                var slug = $(this).attr("data-slug")
				window.loadKanbanAction("all", name , { id : id , type : "project" , slug : slug });
                $("#modalKanbanAction").modal("show");
			});

            $(".showToTest").off("click").on('click', function(e){
				e.stopPropagation();
				var id = $(this).attr("data-id");
                var range = <?= json_encode($blockCms["range"]) ?>;
                var item = ``;
                ajaxPost(
                        null,
                        baseUrl+'/'+moduleId+"/action/getrangeactionsbyidparent/",
                        { parentId : id , range : range , status : true },
                        function(actionData){           ;
                            $.each(actionData.data.allActions,function(k,v){ 
                                if ( typeof allData[v.userId] !== "undefined" ){
                                    item += createGridItem(k, v, "<?= $blockKey ?>" , allData[v.userId]);
                                }
                            });
                            $(".userScrum<?= $kunik ?>").html("");
                            $(".userScrum<?= $kunik ?>").append(item);

                            $(".actionChangeStatus").off("change").on("change", function () {
                                const actionId = $(this).attr("data-id");
                                const status = $(this).val();
                                coInterface.actions.request_set_status({
                                    id: actionId,
                                    status: status,
                                    server_url: baseUrl,
                                }).then(function (response) {
                                    toastr.success(coTranslate('Status change, success!'));
                                    if ( status == "done" ) {
                                        var newDoneCounter = $("."+userId+"Counter").data("done") + 1;
                                        if ( newDoneCounter <= $("."+userId+"Counter").data("total") ){
                                            $("."+userId+"Counter").data("done", newDoneCounter);
                                            $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                            $("#task-"+actionId).html("<i class='fa fa-check-square' style='background: #28a745; color: #fff'></i>")
                                        }
                                    } else {
                                        if ( $("."+userId+"Counter").data("done") != "0"){
                                            var newDoneCounter = $("."+userId+"Counter").data("done") - 1;
                                            $("."+userId+"Counter").data("done", newDoneCounter);
                                            $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                            $("#task-"+actionId).html("<i class='fa fa-minus-square' style='color: #fff; background: #272626 '></i>")
                                        }
                                    }
                                }).catch(function (arg0) {
                                    toastr.error(coTranslate('Something went wrong. Please contact an administrator.'));
                                });
                            });
                        }    
                    );
			});



			$("#pills-project-tab").parent().first().addClass("active");
			$("#pills-project").addClass("active in");
		}

		if ( projectClassed.events.length > 0 ){
			$(".panelProjectList").css("display","block");
			$("#pills-event-tab").css("display","block");
			$(".accordeonEvent").css("display","block");
			$(".panelActionsList").removeClass("col-sm-12 col-md-12 col-lg-12").addClass("col-sm-8 col-md-8 col-lg-8");
			const viewEvent = projectClassed.events.map(event =>{
				const backgroundStyle = `linear-gradient(to right, #28a745 ${event.progression}%, #272626 ${event.progression}%)`;
				const gif = "<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/blockOceco/totest.webp";
				return `
					<div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
						<div class="btn-list-project">                               
							<div class="projectActiveList" data-id="${event.id}"  data-slug="${event.slug}" data-name="${event.name}" style="display: flex; width: 90% ;justify-content: start; align-items: center;">
								<div>
									<img src="${event.profilImageUrl}"  onerror="this.src='${$imgProjectDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
								</div>
								<div style="display: flex; align-items: center; padding-left: 10px;">${event.name}</div>
							</div>
							<div class="progression" style="margin-left: 10px;display: flex ;color: #fff;justify-content: end;align-items: center;">
									<span data-toggle="tooltip" title="progression" style="font-size:20px;margin-left:5px;margin-right:5px;">${event.progression}%</span>
									<img src="${gif}" data-id="${event.id}"  class="totest${event.id} actionChangeStatus showToTest"  style="display: none;width:30px; heigth:30px; right: 4%; top: 5px;">
							</div>
							${ typeof isSuperAdmin !== "undefined" && isSuperAdmin ? `
								<div class="loadKamban loadKambanEvent" data-name="${event.name}" data-slug="${event.slug}" data-tag="${event.id}" style="margin-left: 10px;color: #fff;display: flex;justify-content: end;align-items: center;">
									<i class="fa fa-trello fa-lg" data-toggle="tooltip" title="kamban" style="font-size:25px;margin-left:5px;"></i>
								</div>` : ``
							}
						</div>
						<div data-toggle="tooltip" title="progression ${event.progression}%" style="border-radius: 0 0 10px 10px;height: 2px;background: ${backgroundStyle}"></div>
					</div>
				`
			}).join("");

			$("#pills-event").html(viewEvent);
			$("#pills-event-sm").html(viewEvent);
            $(".projectActiveList").off("click").on('click', function(e){
                e.stopPropagation();
                $(".btn-list-project").removeClass('co-project-active');
				$(this).parent(".btn-list-project").addClass('co-project-active');
				const eventId = $(this).data("id");
                const name = $(this).data("name");
                const slug = $(this).data("slug");
                findActionById(eventId,"events",name,slug);
			});

			$(".loadKambanEvent").off("click").on('click', function(e){
				e.stopPropagation();
				var id = $(this).attr("data-tag");
				var name = $(this).attr("data-name");
                var slug = $(this).attr("data-slug")
				window.loadKanbanAction("all", name , { id : id , type : "events" , slug : slug });
                $("#modalKanbanAction").modal("show");
			});
            

            $(".showToTest").off("click").on('click', function(e){
				e.stopPropagation();
				var id = $(this).attr("data-id");
                var range = <?= json_encode($blockCms["range"]) ?>;
                var item = ``;
                ajaxPost(
                        null,
                        baseUrl+'/'+moduleId+"/action/getrangeactionsbyidparent/",
                        { parentId : id , range : range , status : true },
                        function(actionData){           ;
                            $.each(actionData.data.allActions,function(k,v){ 
                                if ( typeof allData[v.userId] !== "undefined" ){
                                    item += createGridItem(k, v, "<?= $blockKey ?>" , allData[v.userId]);
                                }
                            });
                            $(".userScrum<?= $kunik ?>").html("");
                            $(".userScrum<?= $kunik ?>").append(item);

                            $(".actionChangeStatus").off("change").on("change", function () {
                                const actionId = $(this).attr("data-id");
                                const status = $(this).val();
                                coInterface.actions.request_set_status({
                                    id: actionId,
                                    status: status,
                                    server_url: baseUrl,
                                }).then(function (response) {
                                    toastr.success(coTranslate('Status change, success!'));
                                    if ( status == "done" ) {
                                        var newDoneCounter = $("."+userId+"Counter").data("done") + 1;
                                        if ( newDoneCounter <= $("."+userId+"Counter").data("total") ){
                                            $("."+userId+"Counter").data("done", newDoneCounter);
                                            $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                            $("#task-"+actionId).html("<i class='fa fa-check-square' style='background: #28a745; color: #fff'></i>")
                                        }
                                    } else {
                                        if ( $("."+userId+"Counter").data("done") != "0"){
                                            var newDoneCounter = $("."+userId+"Counter").data("done") - 1;
                                            $("."+userId+"Counter").data("done", newDoneCounter);
                                            $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                            $("#task-"+actionId).html("<i class='fa fa-minus-square' style='color: #fff; background: #272626 '></i>")
                                        }
                                    }
                                }).catch(function (arg0) {
                                    toastr.error(coTranslate('Something went wrong. Please contact an administrator.'));
                                });
                            });
                        }    
                    );
			});
            

			if ( projectClassed.projects.length === 0 ){
				$("#pills-event-tab").parent().first().addClass("active");
				$("#pills-event").addClass("active in");
			}
		}
	}

    function openExpended(dom){
        var toShow = $(dom).parents(".expended-parent").find(".expanded-view");
        toShow.toggleClass('hidden');
    }


    function mergeArrays(arr1, arr2) {
      arr2.forEach(element => {
        // Check if the element already exists in arr1
        if (!arr1.includes(element)) {
          arr1.push(element); // Add it if it's not already there
        }
      });
      return arr1;
    }

    function showDetails(id){
        var previewDom = $('#corner-action-preview');
        previewDom.empty();
        show_action_detail_modal(id).then(function(__html) {
            previewDom.html(__html);
        });
    }
    /**
     * Affiche un modal contenant les détails d'une action.
     * Possibilité d'ajouter des sous tâches et inviter des participants.
     * @param {string} __id Identifiant de l'action à récupérer
     * @return {Promise.<string>}
     */
    function show_action_detail_modal(__id) {
        return new Promise(function(__resolve) {
            const url = baseUrl + '/costum/project/action/request/action_detail_html';
            const post = {
                id: __id,
                connectedUser: userConnected._id.$id,
                server_url: baseUrl
            };
            ajaxPost(null, url, post, __resolve, null, 'text');
        });
    }
    

	function findActionById(id,type,name,slug){
		  var range = <?= json_encode($blockCms["range"]) ?>;
		  var item = ``;
          ajaxPost(
                null,
                baseUrl+'/'+moduleId+"/action/getrangeactionsbyidparent/",
                { parentId : id , range : range , status : false },
                function(actionData){ 
                    if ( typeof actionData.data !== "undefined" && typeof actionData.data.allActions !== "undefined" ) {
                        $.each(actionData.data.allActions,function(k,v){ 
                            if ( typeof allData[v.userId] !== "undefined" ){
                                item += createGridItem(k, v, "<?= $blockKey ?>" , allData[v.userId]);
                            }
                        });
                        $(".userScrum<?= $kunik ?>").html("");
                        $(".userScrum<?= $kunik ?>").append(item);

                        $(".actionChangeStatus").off("change").on("change", function () {
                            const actionId = $(this).attr("data-id");
                            const status = $(this).val();
                            coInterface.actions.request_set_status({
                                id: actionId,
                                status: status,
                                server_url: baseUrl,
                            }).then(function (response) {
                                toastr.success(coTranslate('Status change, success!'));
                                if ( status == "done" ) {
                                    var newDoneCounter = $("."+userId+"Counter").data("done") + 1;
                                    if ( newDoneCounter <= $("."+userId+"Counter").data("total") ){
                                        $("."+userId+"Counter").data("done", newDoneCounter);
                                        $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                        $("#task-"+actionId).html("<i class='fa fa-check-square' style='background: #28a745; color: #fff'></i>")
                                    }
                                } else {
                                    if ( $("."+userId+"Counter").data("done") != "0"){
                                        var newDoneCounter = $("."+userId+"Counter").data("done") - 1;
                                        $("."+userId+"Counter").data("done", newDoneCounter);
                                        $("."+userId+"Counter").html(newDoneCounter+"/"+$("."+userId+"Counter").data("total"));
                                        $("#task-"+actionId).html("<i class='fa fa-minus-square' style='color: #fff; background: #272626 '></i>")
                                    }
                                }
                            }).catch(function (arg0) {
                                toastr.error(coTranslate('Something went wrong. Please contact an administrator.'));
                            });
                        });
                    } else {
                        $(".userScrum<?= $kunik ?>").html(`
                            <div class="kl-oceco-statistique messageOceco text-center" data-name="${name}" data-slug="${slug}" data-tag="${id}">
						        <span> <?php echo Yii::t("cms", "Create actions on this project with Oceco by clicking here") ?> </span>
					        </div>
                        `);

                        $(".messageOceco").off("click").on('click', function(e){
                            e.stopPropagation();
                            var id = $(this).attr("data-tag");
                            var name = $(this).attr("data-name");
                            var slug = $(this).attr("data-slug")
                            window.loadKanbanAction("all", name , { id : id , type : "project" , slug : slug });
                            $("#modalKanbanAction").modal("show");
                        });
                    } 
                }    
            );
	}

	function getActionStatus(action) {
		const hasTags = Array.isArray(action.tags) && action.tags.length > 0;
		const status = action.status || "";
		const isTracking = Boolean(action.tracking); // Converts the tracking value to a boolean
		const lowerTags = hasTags ? action.tags.map(tag => tag.toLowerCase()) : [];
		
		if (status === "done") return "done";
		if (lowerTags.includes("totest")) return "totest";
		if (isTracking) return "tracking";
		if (lowerTags.includes("next")) return "next";
		if (lowerTags.includes("discuter")) return "discuter";
		return "todo";
	}


    function appendProject(progression,name,profilImageUrl,slug,id,dom,type) {
        const backgroundStyle = `linear-gradient(to right, #337ab7 ${progression}%, #272626 ${progression}%)`;
        const item = `
            <div style="cursor: pointer;display: block; background: #272626;padding-top: 5px;padding-bottom: 5px;">
                <div class="btn-list-project">                               
                    <div class="projectActiveList" data-id="${id}" data-name="${name}" data-slug="${slug}"  style="display: flex; width: 90% ;justify-content: start; align-items: center;">
                        <div>
                            <img src="${profilImageUrl}"  onerror="this.src='${$imgProjectDefault}';" style="width:50px; height: 50px; border-radius: 50%; object-fit: cover;" />
                        </div>
                        <div style="display: flex; align-items: center; padding-left: 10px;">${name}</div>
                    </div>
                    <div class="progression" style="margin-left: 10px;display: flex ;color: #fff;justify-content: end;align-items: center;">
                            <span data-toggle="tooltip" title="progression" style="font-size:20px;margin-left:5px;margin-right:5px;">${progression}%</span>
                    </div>
                    ${ typeof isSuperAdmin !== "undefined" && isSuperAdmin ? `
                        <div class="loadKamban loadKambanProject" data-name="${name}" data-slug="${slug}" data-tag="${id}" style="margin-left: 10px;color: #fff;display: flex;justify-content: end;align-items: center;">
                            <i class="fa fa-trello fa-lg" data-toggle="tooltip" title="kamban" style="font-size:25px;margin-left:5px;"></i>
                        </div>` : ``
                    }
                </div>
                <div data-toggle="tooltip" title="progression ${progression}%" style="border-radius: 0 0 10px 10px;height: 2px;background: ${backgroundStyle}"></div>
            </div>
        `
        $("#"+dom).prepend(item);
		$("#"+dom+"-sm").prepend(item);

        $(".projectActiveList").off("click").on('click', function(e){
            e.stopPropagation();
            $(".btn-list-project").removeClass('co-project-active');
            $(this).parent(".btn-list-project").addClass('co-project-active');
            const projectId = $(this).data("id");
            const name = $(this).data("name");
            const slug = $(this).data("slug");
            findActionById(projectId,type,id,name,slug);
		});

        $(".loadKambanProject").off("click").on('click', function(e){
            e.stopPropagation();
            var id = $(this).attr("data-tag");
            var name = $(this).attr("data-name");
            var slug = $(this).attr("data-slug")
            window.loadKanbanAction("all", name , { id : id , type : type , slug : slug });
            $("#modalKanbanAction").modal("show");
        });
    }


    $(".coscrumCreateProject").on("click", function(e){
			$type = $(this).data("type");
			if ( $type === "Event"){
				var preferences = {
					isOpenData : true,
					isOpenEditional : true
				}
				var newEvent = {};
				dyFObj.setMongoId('events', function() {
						const newEventToAdd = {
							id : dyFObj.currentElement.id,
							public : true,
							scope:null,
							recurrency: false,
							costumSlug: costum.slug,
							costumEditMode: costum.editMode,
							preferences: preferences,
							key: "event",
							collection: "events",
							timeZone : moment.tz.guess()
						};

						Object.assign(newEvent, newEventToAdd);

						new CoInput({
							container: "#createvent-image-input",
							inputs: [
								{
									type: "inputFileImage",
									options: {
										name: "image",
										label: "Image",
										collection: "documents",
										defaultValue: "",
										canSelectMultipleImage : false,
										payload: {
											path: "logo",
										},
										icon: "chevron-down",
										class: "logoUploader inputContainerCoEvent",
										contentKey: "profil",
										uploadOnly: true,
										uploaderInitialized : true,
										endPoint: "",
										domElement: "profil",
									},
								},
								{
									type : "dateTimePicker",
									options: {
										name : "startDate",
										label: tradCms.startDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newStartDate",
									}
								},
								{
									type : "dateTimePicker",
									options: {
										name : "endDate",
										label: tradCms.endDate +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent newEndDate",
									}
								},
								{
									type : "tags",
									options : {
										name : "tags",
										label : "tradDynForm.keyWords",
										options : [""],
										defaultValue : "",
										class: "inputContainerCoEvent",
										icon : "chevron-down"
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "email",
										label : "E-mail",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "url",
										label : "URL",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								
							],
							onchange: function(name, value, payload) {
								if ( name === "image") {
									jsonHelper.setValueByPath(newEvent, "profilImageUrl", value);
								} else if ( name === "startDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment(); // Date et heure actuelles

									if (value.isBefore(currentDate)) {
										$(".warningStartDate").remove();
										$(".newStartDate").append(`<span class="warningStartDate" ><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									} else {
										$(".warningStartDate").remove();
										var dateDebutConvertie = value.tz(newEvent.timeZone).format("YYYY-MM-DDTHH:mm:ssZ");
										jsonHelper.setValueByPath(newEvent, name, dateDebutConvertie);
									}
								} else if ( name === "endDate"){
									var value = moment(value, "DD/MM/YYYY HH:mm");
									var currentDate = moment();
									var startDate = moment(newEvent.startDate);
									var dateEndConvertie = value.tz(newEvent.timeZone);
									if (dateEndConvertie.isAfter(currentDate) && dateEndConvertie.isAfter(startDate)) {
										$(".warningEndDate").remove();
										jsonHelper.setValueByPath(newEvent, name, dateEndConvertie.format("YYYY-MM-DDTHH:mm:ssZ"));
									} else {
										$(".warningEndDate").remove();
										$(".newEndDate").append(`<span class="warningEndDate"><i class='fa fa-warning'></i> ${tradCms.mustBeAfter + " " + startDate.format("DD/MM/YYYY HH:mm") + " " + tradCms.and + " " + currentDate.format("DD/MM/YYYY HH:mm")}.</span>`);
									}
								} else if ( name === "tags"){
									jsonHelper.setValueByPath(newEvent, name,value.split(','));
								} else {
									jsonHelper.setValueByPath(newEvent, name,value);
								}
							},
							onblur : function(name , value, payload){
							
							}
						});

						new CoInput({
								container: ".create-event-details",
								inputs: [
									{
										type : "inputSimple",
										options: {
											name : "name",
											label : tradCms.eventName +" *",
											defaultValue : "",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										}
									},
									{
										type : "select",
										options: {
											name : "type",
											label : tradDynForm.eventTypes +" *",
											options :  Object.keys(eventTypes).map(function(value) {
												return {
													label : typeof tradCategory[eventTypes[value]] == "String" ? tradCategory[eventTypes[value]] : eventTypes[value] ,
													value : value
												}
											}),
											icon : "chevron-down",
											class: "inputContainerCoEvent"
										}
									},
									{
										type : "finders",
										options: {
											name : "organizer",
											label: tradDynForm.whoorganizedevent,
											initType: ["projects", "organizations"],
											multiple: true,
											initMe: false,
											icon : "chevron-down",
											openSearch: true,
											initBySearch: true,
											class: "inputContainerCoEvent",
										}
									},
									{
										type : "formLocality",
										options: {
											name : "formLocality",
											label: tradCms.place,
											icon : "chevron-down",
											class: "inputContainerCoEvent",

										}
									},
									{
										type : "textarea",
										options: {
											name : "shortDescription",
											label : tradCms.shortDescription,
											defaultValue :"",
											icon : "chevron-down",
											class: "inputContainerCoEvent",
										},
									},
								],
								onchange: function(name, value, payload) {
									if ( name === "name" ){
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								},
								onblur: function(name, value, payload) {
									if ( name === "formLocality"){
											var address = [];
											var geo = [];
											var geoPosition = [];
											var addresses = [];
											value.forEach((location, index) => {
												if (location.center) {
													address = location.address;
													geo = location.geo;
													geoPosition = location.geoPosition;
												} else {
													addresses.push(location);
												}
											});
											jsonHelper.setValueByPath(newEvent, "address", address);
											jsonHelper.setValueByPath(newEvent, "geo", geo);
											jsonHelper.setValueByPath(newEvent, "geoPosition", geoPosition);
											jsonHelper.setValueByPath(newEvent, "addresses", addresses);
									} else {
										jsonHelper.setValueByPath(newEvent, name, value);
									} 
								}
						});
						$("#openModalCreateEvent").modal("show");
				});

				$(".create-event-button").off("click").on("click", function(){
						var idBlock =  $(this).data("id");
						delete newEvent.images;
                        $("#openModalCreateEvent").css({
                            'z-index': '140000'
                        });
                        $("#firstLoader").show();
                        $("#firstLoader #loadingModal").css({
                            'opacity': '0.5',
                            'z-index': '9999999'
                        });
						ajaxPost(
							null,
							baseUrl+"/co2/element/save",
							newEvent, 
							function (response) {  
								if (response.result){
                                    var idCojazz = $("#block-cojazz").attr("attrId")
                                    refreshBlock(idCojazz, ".cmsbuilder-block[data-id='" + idCojazz + "']")
                                    appendProject(0, response.map.name,response.map.profilImageUrl,response.map.slug,response.id,"pills-event","events")
									var events = {
										id: costum.contextId,
										collection: "events",
										path: "links.subEvents."+newEvent.id,
										value : {
											type : "events"
										},
										costumSlug: costum.slug,
										costumEditMode: costum.editMode                                                           
									}
									dataHelper.path2Value(events, function (params) {
										if (params.result){
											var attende = {
												id : newEvent.id,
												collection : "events",
												path : "links.attendees."+userId,
												value : {
													type : "citoyens"
												},
												costumSlug: costum.slug,
												costumEditMode: costum.editMode   
											}
											dataHelper.path2Value(attende, function (params) {
												$("#openModalCreateEvent").modal("hide");
                                                $("#firstLoader").hide();
												toastr.success(trad.save);
											});
										}
									});
								} else {
									toastr.error(response.msg);
								}                 
							}
						);
				});
			} else if ( $type === "Project") {
				var newProject = {};
				dyFObj.setMongoId('project', function() {
						const newProjectToAdd = {
							id : dyFObj.currentElement.id,
							public : true,
							scope:null,
							key: "project",
							collection: "projects",
						};

						Object.assign(newProject, newProjectToAdd);

						new CoInput({
							container: "#createproject-image-input",
							inputs: [
								{
									type: "inputFileImage",
									options: {
										name: "image",
										label: "Image",
										collection: "documents",
										defaultValue: "",
										canSelectMultipleImage : false,
										payload: {
											path: "logo",
										},
										icon: "chevron-down",
										class: "logoUploader inputContainerCoEvent",
										contentKey: "profil",
										uploadOnly: true,
										uploaderInitialized : true,
										endPoint: "",
										domElement: "profil",
									},
								},
								{
									type : "inputSimple",
									options: {
										name : "name",
										label : trad.projectName +" *",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "finders",
									options: {
										name : "parent",
										label: tradDynForm.whoiscarrytheproject,
										initType: ["projects", "organizations"],
										multiple: true,
										initMe: false,
										icon : "chevron-down",
										openSearch: true,
										initBySearch: true,
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "formLocality",
									options: {
										name : "formLocality",
										label: tradCms.place,
										icon : "chevron-down",
										class: "inputContainerCoEvent",

									}
								},
								{
									type : "textarea",
									options: {
										name : "shortDescription",
										label : tradCms.shortDescription,
										defaultValue :"",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									},
								},
								{
									type : "tags",
									options : {
										name : "tags",
										label : "Pole",
										options : [""],
										defaultValue : "",
										class: "inputContainerCoEvent",
										icon : "chevron-down"
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "email",
										label : "E-mail",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								{
									type : "inputSimple",
									options: {
										name : "url",
										label : "URL",
										defaultValue : "",
										icon : "chevron-down",
										class: "inputContainerCoEvent",
									}
								},
								
							],
							onchange: function(name, value, payload) {
								if ( name === "name" ){
										jsonHelper.setValueByPath(newProject, name, value);
								} else if ( name === "image") {
									jsonHelper.setValueByPath(newProject, "profilImageUrl", value);
								} else if ( name === "tags"){
									jsonHelper.setValueByPath(newProject, name,value.split(','));
								} else {
									jsonHelper.setValueByPath(newProject, name,value);
								}
							},
							onblur : function(name , value, payload){
								if ( name === "formLocality"){
										var address = [];
										var geo = [];
										var geoPosition = [];
										var addresses = [];
										value.forEach((location, index) => {
											if (location.center) {
												address = location.address;
												geo = location.geo;
												geoPosition = location.geoPosition;
											} else {
												addresses.push(location);
											}
										});
										jsonHelper.setValueByPath(newProject, "address", address);
										jsonHelper.setValueByPath(newProject, "geo", geo);
										jsonHelper.setValueByPath(newProject, "geoPosition", geoPosition);
										jsonHelper.setValueByPath(newProject, "addresses", addresses);
								} else {
									jsonHelper.setValueByPath(newProject, name, value);
								} 
							}
						});

						$("#openModalCreateProject").modal("show");
				});

				$(".create-project-button").off("click").on("click", function(){
                    var idCojazz = $("#block-cojazz").attr("attrId")
					delete newProject.images;
                    $("#openModalCreateProject").css({
                        'z-index': '140000'
                    });
                    $("#firstLoader").show();
                    $("#firstLoader #loadingModal").css({
                        'opacity': '0.5',
                        'z-index': '9999999'
                    });
					ajaxPost(
						null,
						baseUrl+"/co2/element/save",
						newProject, 
						function (response) {  
							if (response.result){
									$("#openModalCreateProject").modal("hide");
                                    $("#firstLoader").hide();
									toastr.success(trad.save);
									refreshBlock(idCojazz, ".cmsbuilder-block[data-id='" + idCojazz + "']")
                                    appendProject(0, response.map.name,response.map.profilImageUrl,response.map.slug,response.id,"pills-project","projects")
							} else {
								toastr.error(response.msg);
							}      
						}
					);
				});
			}
	});

</script>