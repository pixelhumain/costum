<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $defaultSocialnetwork = array(
        [
            "icon" => "facebook",
            "nom" => "facebook",
            "urlnetwork" => ""
        ],
        [
            "icon" => "youtube-play",
            "nom" => "Youtube",
            "urlnetwork" => ""
        ],
        [
            "icon" => "twitter",
            "nom" => "X",
            "urlnetwork" => ""
        ],
        [
            "icon" => "linkedin",
            "nom" => "linkedin",
            "urlnetwork" => ""
        ]
    );

    if($blockCms["socialNetwork"] == null || $blockCms["socialNetwork"] == "undefined")
    {
        $blockCms["socialNetwork"] = $defaultSocialnetwork;
    }
?>

<style id="kaffootercontact<?php echo $kunik ?>">
	.<?php echo $kunik ?>kaffootercontact .subtitle<?php echo $kunik ?>,  h4.contact-foot{
        font-weight: 400;
    }
    .<?php echo $kunik ?>kaffootercontact ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	.<?php echo $kunik ?>kaffootercontact ul.social-network li {
		display: inline;
		margin: 0 5px;
	}
	/* .<?php echo $kunik ?>kaffootercontact .social-circle li a:hover {
		background-color:< ?= $blockCms["css"]["social"]["color"] ?>;
	}
	.<?php echo $kunik ?>kaffootercontact .social-circle li a:hover i {
		color: < ?= $blockCms["css"]["social"]["backgroundColor"] ?>;
	} */
	.<?php echo $kunik ?>kaffootercontact .social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 50px;
		height: 50px;
		font-size:24px;
	}
	.<?php echo $kunik ?>kaffootercontact .social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}
	.<?php echo $kunik ?>kaffootercontact .social-circle li a:hover i, .triggeredHover {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms--transform: rotate(360deg);
		transform: rotate(360deg);
		-webkit-transition: all 0.2s;
		-moz-transition: all 0.2s;
		-o-transition: all 0.2s;
		-ms-transition: all 0.2s;
		transition: all 0.2s;
	}
	.<?php echo $kunik ?>kaffootercontact .social-circle .socialIcon i {
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}
</style>

<div class="<?php echo $kunik ?> <?php echo $kunik ?>kaffootercontact col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
	<h2 class="title<?php echo $kunik ?> title-1  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock">
    	<?php echo $blockCms["titleBlock"] ?>
  	</h2>
  	<hr>
  	<div class="col-xs-12">
  		<h3 class="subtitle<?php echo $kunik ?> title-2 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitleBlock">
		    <?php echo $blockCms["subtitleBlock"] ?>
		</h3>
  	</div>
  	<div class="row">
  		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  			<h4 class=" contact-foot title-3 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="contact"><i class="fa fa-phone"></i>&nbsp;<?php echo $blockCms["contact"] ?></h4>
  		</div>
  		<?php if (isset($blockCms["socialNetwork"])) { ?>  
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                <ul class="social-network social-circle">
                    <?php 
                    foreach ($blockCms["socialNetwork"] as $key => $value) { 
                        $chaine = $value["icon"];
                        $caractaire = "fa fa-";
                        $icone = "";
                        if(strpos($chaine, $caractaire) !== false )
                        {
                            $icone = $chaine;
                        } else {
                            $icone = "fa fa-".$chaine;
                        }
                    ?>
                        <li><a href="<?php echo isset($value['urlnetwork']) ? $value['urlnetwork'] : "#" ?>"  target="_blank" class="socialIcon social" title="<?php echo isset($value['nom']) ?  $value['nom'] : "" ?>"><i class="<?= $icone ?>"></i></a></li>
                    <?php } ?>
                </ul> 				
            </div>
		<?php } ?>	
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
  			<a href="mailto:<?php echo $blockCms["mail"] ?>" class="title-4 contact-foot"><i class="fa fa-at"></i>&nbsp;:&nbsp;<?php echo $blockCms["mail"] ?></a>
  		</div>
	</div>
</div>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#kaffootercontact<?= $kunik ?>").append(str);

    if( sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork != null && (sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork).length > 0)
    {
        var socialNet = sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork;
        for(var i = 0; i < socialNet.length; i++)
        {
            var chaine = socialNet[i].icon;
            var caractere = "fa fa-";

            if(!chaine.includes(caractere))
            {
                socialNet[i].icon = "fa fa-"+socialNet[i].icon+"";
            }
        }
        sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork = socialNet;
    }
    console.log("sectionDyf.<?php echo $kunik?>ParamsData.socialNetwork :", <?php echo json_encode( $blockCms["socialNetwork"] ); ?>);
    
    if(costum.editMode)
    {   
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik?>ParamsData;
        var footerContact = {
            configTabs : {
                general: {
                    inputsConfig: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "mail",
                                label: trad.Email
                            }
                        },
                        {
                            type: "inputMultiple",
                            options: {
                                name: "socialNetwork",
                                label: tradCms.socialnetworks,
                                inputs: [
                                    {
                                        type: "inputIcon",
                                        options: {
                                            name: "icon",
                                            label: tradCms.icon
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "nom",
                                            label: tradCms.nameofthesocialnetwork
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "urlnetwork",
                                            label: tradCms.pageName
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "social",
                                label: tradCms.socialnetworks,
                                inputs: [
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        },
                    ]
                },
                hover: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "social",
                                label: tradCms.networkicononhover,
                                inputs: [
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.footer_contact<?= $myCmsId ?> = footerContact;
    }
</script>