<?php

  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) ["$kunik-footer-area" => $objectCss];

  	$spText = ["titleColumn1","titleColumn2","titleSocialMedia","apropos"];

    foreach ($spText as $key) {
        if( !(is_array($blockCms[$key]))){
            ${$key} = ["fr" => $blockCms[$key]];
        } else {
            ${$key} = $blockCms[$key];
        }
	};

?>
<!-- < ?php var_dump($blockCms['socialLinks']['links']); die;  ?> -->

<style  id="footer-area<?= $kunik ?>" type="text/css">

	.f_social_wd_<?= $kunik?> .f_social a {
		font-size: 18px;
		-webkit-transition: all 0.3s linear;
		-o-transition: all 0.3s linear;
		transition: all 0.3s linear;
		margin-right: 20px;
	}

	/* .f_social_wd_<?= $kunik?> .f_social a:hover {
		color: #8ABF32;
	} */

	.f_social_wd_<?= $kunik?> .f_social a:last-child {
		margin-right: 0px;
	}

	.footer-title-<?= $kunik?>{
		font-size: 25px;
		color:  black;
	}

	.single-footer-widget_<?= $kunik?> p {
		margin-bottom: 0px;
		max-width: 90%;
	}
	
	.f_social_wd_<?= $kunik?> p {
		font-size: 14px;
		margin-bottom: 15px;
	}

	
	.footer_title_<?= $kunik?>{
		font-size: 25px;
	}

	@media (max-width: 978px){
		.footer-area-<?= $kunik?> {
			padding-top: 54px;
			padding-bottom: 40px;
		}
		.footer_title_<?= $kunik?>{
			font-size: 15px;
		}
		.single-footer-widget_<?= $kunik?> p {
			font-size: 14px;
		}
	}

	.icon<?= $kunik ?>{
		padding: 0.4em;
		border: 2px solid #999;
		border-radius: 40px;
	}

	.mx-3-<?= $kunik ?>{
		margin: 0.3em;
	}

	.<?= $kunik ?>-footer-area .apropos {
		color: black;
	}
	
</style>

<footer class="<?= $kunik ?>-footer-area <?= $kunik ?>" data-kunik="<?= $kunik ?>" data-name="footerWithCollabo" data-id="<?= $blockKey ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center single-footer-widget_<?= $kunik?>">
					<h6 class="titleColumn1 footer-title-<?= $kunik?> title sp-text img-text-bloc titleColumn1<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>"  data-field="titleColumn1"></h6>
					<div class="text-center">
						<?php foreach ($blockCms["logoPorteur"] as $ks => $imgs) {
							echo "<img src='".$imgs."' class='logo mx-3-".$kunik." logoPorteur' style='margin:1em'>";
						} ?>
					</div>
					<?php //echo  '<p class="description">'.$paramsData["apropos"].'</p>'?>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"height='".$blockCms["logoHeight"]."'>
				<div class="text-center single-footer-widget_<?= $kunik?>">
					<h6 class="titleColumn2 footer-title-<?= $kunik?> title sp-text img-text-bloc titleColumn2<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleColumn2"></h6>
					<div class="text-center">
						<?php foreach ($blockCms["logoSupporteur"] as $ks => $imgs) {
							echo "<img src='".$imgs."' class='logo mx-3-".$kunik." logoSupporteur' style='margin:1em'>";
						} ?>
					</div>
				</div>
			</div>	
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="text-center single-footer-widget_<?= $kunik?> f_social_wd_<?= $kunik?>">
					<h6 class="titleSocialMedia footer-title-<?= $kunik?> title sp-text img-text-bloc titleSocialMedia<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleSocialMedia"></h6>
					<br>
					<div class="f_social text-center">
						<?php foreach ($blockCms['socialLinks']['links'] as $lvalue) {
							$chaine = $lvalue["inputIcon"];
							$caractaire = "fa fa-";
							$icone = "";
							if(strpos($chaine, $caractaire) !== false )
							{
								$icone = $chaine;
							} else {
								$icone = "fa fa-".$chaine;
							}
							echo '<a target="_blank" href="'.$lvalue["link"].'">
							<i class="icone '.$icone.' icon'.$kunik.'"></i>
						</a>';
						}?>
					</div>
				</div>
			</div>						
		</div>
	</div>
	<br><br>
	<?php if ($blockCms["apropos"]!="") { ?>
		<hr>
		<div></div>
		<div class="apropos text-center sp-text img-text-bloc apropos<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="apropos">
		</div>
	<?php } ?>
</footer>

<script type="text/javascript">

	var styleCss = "";
    styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#footer-area<?= $kunik ?>").append(styleCss);

	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($blockCms) ?>;
	var slinks =  sectionDyf.<?php echo $kunik ?>ParamsData.socialLinks.links;

	if(slinks.length > 0) {
		for(var i = 0; i < slinks.length ; i++) {
			var chaine = slinks[i].inputIcon;
			var caractere = "fa fa-";
			if (!chaine.includes(caractere)) {
				slinks[i].inputIcon = "fa fa-"+slinks[i].inputIcon+"";
			}
		}
		sectionDyf.<?php echo $kunik ?>ParamsData.socialLinks.links = slinks;
	}

	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $blockKey ?>"] = sectionDyf.<?php echo $kunik ?>ParamsData;
		var footerWithCollabo = {
			configTabs: {
				general: {
					inputsConfig: [
						{
							type: "inputFileImage",
							options: {
								name: "logoPorteur",
								canSelectMultipleImage: true,
								maxImageSelected: 6,
								label: tradCms.porterLogo,
							},
						},
						{
							type: "inputFileImage",
							options: {
								name: "logoSupporteur",
								canSelectMultipleImage: true,
								maxImageSelected: 6,
								label: tradCms.supporterLogo,
							},
						},			
						
						"background"
					],
				},
				socialLinks: {
					key: "socialLinks",
					keyPath : "socialLinks", 
					icon: "fa fa-link icon",
					label: "Social media links",
					content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"socialLinks"),
					inputsConfig : [
						{
						type: "inputMultiple",
						options: {
							label: tradCms.otherLinks,
							name: "links",  
							inputs: [ 
								{
									type: "inputSimple",
									options: {
										name: "link",
										label: "lien du reseaux sociaux"
									},
								},
								{
									type: "inputIcon",
									options: {
										name: "inputIcon", 
									},
								}, 
							], 
						}
						}, 
					]
				}, 
				style: {
					inputsConfig: [
						{
							type: "section",
							options: {
								name: "logo",
								label: "Logo",
								inputs: [
									"height"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "icone",
								label: tradCms.icon,
								inputs:[
									"color",
									"backgroundColor",
									"borderColor"
								]
							}
						}
					]
				},
				hover: {
					inputsConfig: [
						{
							type: "section",
							options: {
								name: "icone",
								label: tradCms.icon,
								inputs:[
									"color",
									"backgroundColor",
									"borderColor"
								]
							}
						}
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
			cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			},
		}
		cmsConstructor.blocks.footerWithCollabo<?= $blockKey ?> = footerWithCollabo;
	}

	//sp-params and text append 

	appendTextLangBased(".titleColumn1<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleColumn1) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".titleColumn2<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleColumn2) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".titleSocialMedia<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleSocialMedia) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".apropos<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= "\n ".json_encode($apropos) ?>,"<?= $blockKey ?>");
</script>