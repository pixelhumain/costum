<?php 
$keyTpl = "introfooter";
$paramsData = [
  "title"     => "Intéressé?",
  "titleAlign"  => "center",
  "titleSize"     => "22",
  "titleColor"    => "#ffffff",
  "textAlign"   => "center",
  "textSize"      => "18",
  "textColor"     => "#ffffff",
  "bg_color"    => "#de4c7c",
  "link"    => "#",
  "text"      => "pour aller plus loin! A très vite…"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}

    $childForm = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));

   # If user is admin
    #$is_admin = false;
    #$admins = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"].".isAdmin"=>true));
    #if(count($admins)!=0){
    #    $is_admin = true;
    #}

    # If user has already joined
    $members = PHDB::find("organizations", array("links.members.".Yii::app()->session["userId"]=>['$exists' => true ], "slug"=>$costum['slug']));
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($paramsData["link"]!=""){
        $formId = $paramsData["link"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        $formId = "";
        foreach($form as $key => $value) { $formId = $key; }
    }

    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => Yii::app()->session["userId"], "draft"=>['$exists' => false ]));
    $has_answered = false;
    $new_answers = PHDB::find("answers", array("source.key"=>$costum['slug'], "seen"=>"false", "draft"=>['$exists' => false ]));
    $display_nbr_newAnswer = "none";
    $nbr_newAnswer = count($new_answers);
    if ($nbr_newAnswer > 0) {
      $display_nbr_newAnswer = "inline-block"; 
      if ($nbr_newAnswer > 99) {
        $nbr_newAnswer = "+99";
      }
    } 
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if(Yii::app()->session["userId"] == $value["user"]){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if(isset(Yii::app()->session["userId"])){
        
        $data = PHDB::findByIds("citoyens", [Yii::app()->session["userId"]] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[Yii::app()->session["userId"]]["links"]["memberOf"])){
            $links2 = $data[Yii::app()->session["userId"]]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>

<style type="text/css">

  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?> .btn-edit-delete .btn{
    box-shadow: 0px 0px 20px 3px #ffffff;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:50%;
    left: 50%;
    transform: translate(-50%,0%);
  }

  .logoLoginRegister{
    background-color: #44546b !important;
    color: #ffffff;
    border-radius: 10px;
    padding: 20px;
  }

  @media (max-width: 767px){
    .name img {
      max-height: 70px;
    }

    div {
      margin: 0 !important;
      padding: 0 !important;
    }
  }
  
</style>

<div class="row container<?php echo $kunik ?> ">
	<div style="text-align: <?= $paramsData['titleAlign'] ?>">
		<div style="padding-left: 100px;padding-right: 100px">
      <?php 
       if(isset(Yii::app()->session['userId'])){  ?>
      <?php if($is_member){ ?>
        <a class="btn btn-lg lbh" href="#observatoire" style="text-decoration : none;background-color: <?= $paramsData['bg_color'] ?>;color: <?= $paramsData['titleColor'] ?>"> 
          Acceder à l'obseratoire
        </a>
      <?php }else{ ?>
        <?php if(!$has_answered){ ?>
        <div class="padding-20">
          <font style="font-size: <?= $paramsData['textSize'] ?>px;color: <?= $paramsData['textColor'] ?>"><?= $paramsData['text'] ?></font>          
        </div>
           <a class="btn btn-lg lbh" href="#answer.index.id.new.form.<?= $formId; ?>" style="text-decoration : none;background-color: <?= $paramsData['bg_color'] ?>;color: <?= $paramsData['titleColor'] ?>"> 
            <?= $paramsData['title'] ?>
          </a>
      <?php }else{ ?>
        <a class="btn btn-lg lbh" href="#answer.index.id.<?= $myAnswer; ?>.mode.w" data-id="<?= $formId; ?>" style="text-decoration : none;background-color: <?= $paramsData['bg_color'] ?>;color: <?= $paramsData['titleColor'] ?>"> 
          Voir ma réponse
        </a>
      <?php } ?>
    <?php } 
      } else { ?>        
        <font style="font-size: <?= $paramsData['textSize'] ?>px;color: <?= $paramsData['textColor'] ?>"><?= $paramsData['text'] ?></font>  
         <div class="padding-20">
            <a class="btn btn-lg lbh"  data-toggle="modal" data-target="#modalLogin" style="text-decoration : none;background-color: <?= $paramsData['bg_color'] ?>;color: <?= $paramsData['titleColor'] ?>"> 
            <?php echo Yii::t("common","Please Login First") ?>
          </a>    
        </div>
      <?php }
    ?>
  </div>
</div>
</div>
  <script type="text/javascript">
<?php if(!Authorisation::isInterfaceAdmin()){ ?>
   $(".menu-xs-cplx").remove();
   $(".dropdown-menu-top").remove();
  <?php } ?>
function loadFormAfterLog() {
  //localStorage.setItem("loadFormAfterLog", "load");

   // urlCtrl.loadByHash("#answer.index.id.new.form.<?= $formId; ?>");
}

// if(localStorage.getItem("loadFormAfterLog") == "load") {   
//  bootbox.confirm(`<div role="alert">
//           <p style="font-size: <?= $paramsData['textSize'] ?>px; color: <?= $paramsData['textColor'] ?>; text-align: <?= $paramsData['textAlign'] ?>"> Cliquer <a href="#answer.index.id.new.form.<?= $formId; ?>" class="lbh"><b class="">ICI </b></a><?= $paramsData['text'] ?>
//         </p>
//   </div> `,
//   function(result){
//     if (!result) {
//       return;
//     }else{   

//     }
//     urlCtrl.loadByHash(location.hash);
//   }); 
//   localStorage.removeItem("loadFormAfterLog");
// }

  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

  jQuery(document).ready(function() {
    var ansNotif = <?= $nbr_newAnswer ?>;
   setTimeout(function(){ $("#menuTopRight a[href$='#observatoire']").html("Observatoire <span class='bg-green badge' style='display: <?= $display_nbr_newAnswer ?>'>"+ansNotif+"</span>");; }, 600);

    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          title : {
            label : "Label du bouton",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
          },
          link : {
            label : "Lien",
            "inputType" : "select",
            "options": {
              <?php 
              foreach($childForm as $key => $value) { 
                echo  '"'.$key.'" : "'.$value["name"].'",';
              }    
              ?>
            },
            value : sectionDyf.<?php echo $kunik ?>ParamsData.link
          },
          titleColor : {
            label : "Couleur du label de bouton",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
          },
          bg_color : {
            label : "Couleur du bouton",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.bg_color
          },
          text : {
            label : "Texte",
            "inputType" : "textarea",
            "markdown" : true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
          },
          textColor : {
            label : "Couleur du texte",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
          },  
          textSize : {
            label : "Taille du texte",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textSize
          },
          textAlign : {
            label : "Alignement du texte",
            inputType : "select",
            options : {
              "left"    : "À Gauche",
              "right" : "À droite",
              "center"  :"Centré",
              "justify"  :"Justifié"
            },
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.textAlign
          }
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";

      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>