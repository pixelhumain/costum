<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $defaultSocialnetwork = array(
        [
            "icon" => "facebook",
            "nom" => "facebook",
            "urlnetwork" => ""
        ],
        [
            "icon" => "youtube-play",
            "nom" => "Youtube",
            "urlnetwork" => ""
        ],
        [
            "icon" => "twitter",
            "nom" => "X",
            "urlnetwork" => ""
        ],
        [
            "icon" => "linkedin",
            "nom" => "linkedin",
            "urlnetwork" => ""
        ]
    );

    if($blockCms["socialNetwork"] == null || $blockCms["socialNetwork"] == "undefined")
    {
        $blockCms["socialNetwork"] = $defaultSocialnetwork;
    }
?>

<style id="reseauxSociaux<?php echo $kunik ?>">

    .<?php echo $kunik ?>reseauxSociaux .all-social {
        margin-top: 5%;
        margin-bottom: 5%;
    }
    .<?php echo $kunik ?>reseauxSociaux ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	.<?php echo $kunik ?>reseauxSociaux ul.social-network li {
		display: inline;
		margin: 0 5px;
	}

	.<?php echo $kunik ?>reseauxSociaux .social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		/* -moz-border-radius:50%; */
		/* -webkit-border-radius:50%; */
		/* border-radius:50%; */
		text-align:center;
		width: 50px;
		height: 50px;
		/* font-size:24px; */
	}
	.<?php echo $kunik ?>reseauxSociaux .social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}
	.<?php echo $kunik ?>reseauxSociaux .hover-social-circle li a:hover i, .triggeredHover {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms--transform: rotate(360deg);
		transform: rotate(360deg);
		-webkit-transition: all 0.2s;
		-moz-transition: all 0.2s;
		-o-transition: all 0.2s;
		-ms-transition: all 0.2s;
		transition: all 0.2s;
	}
	.<?php echo $kunik ?>reseauxSociaux .social-circle .socialIcon i {
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}
</style>

<div class="<?php echo $kunik ?> <?php echo $kunik ?>reseauxSociaux col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
  	<div class="row">
  		<?php if (isset($blockCms["socialNetwork"])) { ?>  
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center all-social">
                <ul class="social-network social-circle" id="<?= $kunik ?>SocialCircle">
                    <?php 
                    foreach ($blockCms["socialNetwork"] as $key => $value) { 
                        $chaine = $value["icon"];
                        $caractaire = "fa fa-";
                        $icone = "";
                        if(strpos($chaine, $caractaire) !== false )
                        {
                            $icone = $chaine;
                        } else {
                            $icone = "fa fa-".$chaine;
                        }
                    ?>
                        <li><a href="<?php echo isset($value['urlnetwork']) ? $value['urlnetwork'] : "#" ?>"  target="_blank" class="socialIcon social" title="<?php echo isset($value['nom']) ?  $value['nom'] : "" ?>"><i class="<?= $icone ?>"></i></a></li>
                    <?php } ?>
                </ul> 				
            </div>
		<?php } ?>	
	</div>
</div>

<script type="text/javascript">

    <?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#reseauxSociaux<?= $kunik ?>").append(str);

    if(<?php echo $kunik ?>ParamsData.iconRotation){
        $("#<?= $kunik ?>SocialCircle").addClass("hover-social-circle");
    } else {
        <?php echo $kunik ?>ParamsData.iconRotation = false;
        $("#<?= $kunik ?>SocialCircle").removeClass("hover-social-circle");
    }

    if( <?php echo $kunik?>ParamsData.socialNetwork != null && (<?php echo $kunik?>ParamsData.socialNetwork).length > 0)
    {
        var socialNet = <?php echo $kunik?>ParamsData.socialNetwork;
        for(var i = 0; i < socialNet.length; i++)
        {
            var chaine = socialNet[i].icon;
            var caractere = "fa fa-";

            if(!chaine.includes(caractere))
            {
                socialNet[i].icon = "fa fa-"+socialNet[i].icon+"";
            }
        }
        <?php echo $kunik?>ParamsData.socialNetwork = socialNet;
    }
    
    if(costum.editMode)
    {   
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo $kunik?>ParamsData;
        var reseauxSociaux = {
            configTabs : {
                general: {
                    inputsConfig: [
                        {
                            type: "inputMultiple",
                            options: {
                                name: "socialNetwork",
                                label: tradCms.socialnetworks,
                                inputs: [
                                    {
                                        type: "inputIcon",
                                        options: {
                                            name: "icon",
                                            label: tradCms.icon
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "nom",
                                            label: tradCms.nameofthesocialnetwork
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "urlnetwork",
                                            label: tradCms.pageName
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "social",
                                label: tradCms.socialnetworks,
                                inputs: [
                                    "color",
                                    "backgroundColor",
                                    "fontSize",
                                    "borderRadius",
                                    "width",
                                    "height"
                                ]
                            }
                        },
                    ]
                },
                hover: {
                    inputsConfig: [
                        {
                            type:"groupButtons",
                            options: {
                                name:"iconRotation",
                                label:"Rotation 360",
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ],
                                defaultValue: <?php echo $kunik ?>ParamsData.iconRotation
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "social",
                                label: tradCms.networkicononhover,
                                inputs: [
                                    "color",
                                    "backgroundColor",
                                    "fontSize",
                                    "borderRadius",
                                    "width",
                                    "height"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.reseauxSociaux<?= $myCmsId ?> = reseauxSociaux;
    }
</script>