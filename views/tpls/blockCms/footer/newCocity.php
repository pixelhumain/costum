<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
	$templateCocity = PHDB::findOne(Template::COLLECTION,array(
		"name" => "Template cocity",
		"source.key" => "templateCocity"
	));

	$templateFiliere = PHDB::findOne(Template::COLLECTION,array(
		"name" => "Template Filiere",
		"source.key" => "templateFiliere"
	));

	$_idTemplatecocity = isset($templateCocity["_id"]) ? (string)$templateCocity["_id"] : "";
	$_idTemplatefiliere = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : "";
?>
<style id="newcocity<?= $kunik ?>">
    .banner{
		position:relative;
		height: 15vh;
		overflow: hidden;
	}
	.banner .content{
		position: absolute; 
		left:0;
		right: 0;
		top:45%;
		text-align:center; 
		transform:translateY(-50%);
		z-index: 2;
	}
	#<?= $kunik ?>newCocity, .btn-get-started{
		padding: 10px 15px 15px 15px;
		font-weight: bolder !important;
		border-radius: 40px !important;
		margin-top: 10px;
	}
	.bg-green {
		background-color: #092434 !important;
	}
	.text-green {
		color: #092434 !important;
	}
	/* //css modal and loader */
	.loader-loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
        
    }
    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
        width : auto !important;
        height : auto !important;
    }
    .backdrop-loader<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }
</style>
<!--  --> 
<?php if($costum["typeCocity"] == "ville"){ ?>
	<div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
		<div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
	</div>
	<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>
	<!--  -->
	<div  class="banner newCocity<?= $kunik ?> <?= $kunik ?>"> 
		<div class="content sp-cms-container">
			<div class="col-md-8 col-md-offset-2 " id="newCocity<?= $kunik ?>">
				<button id="<?= $kunik ?>newCocity" class="<?= $kunik ?>newCocity btn btn-get-started bouton">
					Créer votre cocity
				</button>
			</div>
		</div>
	</div>
	<script>
		var styleCss = "";
		styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
		$("#newcocity<?= $kunik ?>").append(styleCss);
		if(costum.editMode) {
			cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode($blockCms) ?>;
			var newCocity = {
				configTabs: {
					style: {
						inputsConfig: [
							{
								type: "section",
								options: {
									name: "bouton",
									label: tradCms.button,
									inputs: [
										"fontSize",
										"color",
										"backgroundColor",
										"border"
									]
								}
							}
						]
					},
					hover: {
						inputsConfig: [
							{
								type: "section",
								options: {
									name: "bouton",
									label: tradCms.button,
									inputs: [
										"fontSize",
										"color",
										"backgroundColor",
										"border"
									]
								}
							}
						]
					},
					advanced: {
						inputsConfig: [
							"addCommonConfig"
						]
					}
				},
				afterSave: function(path, valueToSet, name, payload, value) {
					cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
				},
			}
			cmsConstructor.blocks.newCocity<?= $myCmsId ?> = newCocity;
		}
		
		jQuery(document).ready(function() {
			coInterface.showLoader('#content-loader<?= $kunik ?>');
			var formvalidecity = {
				"jsonSchema" : {
					"title" : "Initialiser une ville",
					"description" : "Initialiser votre ville pour la nouvel coctume",
					"icon": "",
					"properties" : {
						"nameCity" : {
							"label" : "Taper votre ville", 
							"inputType" : "text",
							"rules" :{
								"required" : true
							}
						},
						"formLocality" : dyFInputs.formLocality(tradDynForm.addLocality, tradDynForm.addLocality, {required: true})
					},
					beforBuild: function()
					{},
					afterBuild: function() {
						$("#btn-submit-form").html(`Valider la ville
							<i class="fa fa-arrow-circle-right"></i>`);
					},
					save: function(data){
						var locationObj = JSON.parse(JSON.stringify(dyFInputs.locationObj));
						var cityValue = data.nameCity;
						delete data.nameCity;
						dyFObj.closeForm();
						$('#loader-loader<?= $kunik ?>').removeClass('hidden');	
						$('#backdrop-loader<?= $kunik ?>').removeClass('hidden');

						var paramsCheck = {
							"thematic" : allSugThematic,
							"address" : data.address,
							"typeCity" : "ville"
						}
						ajaxPost(
							null,
							baseUrl+"/costum/cocity/checkdatafiliere",
							paramsCheck,
							function(results) {
								var dyfOrga={
									"beforeBuild":{
										"properties" : {
											"name" : {
												"label" : "Nom de votre ville", 
												"inputType" : "text",
												"placeholder" :"Nom de votre ville",
												"value" : cityValue,
												"order" : 1
											},
											"thematic":{
												"inputType" : "tags",
												"label" : "Sélectionner les filières que vous voulez dés la création de la page",
												"values" : allSugThematic,
												"value" : results,
												"order" : 3
											}
										}
									},
									"onload" : {
										"actions" : {
											"setTitle" : "Creer votre cocity",
											"src" : {
												"infocustom" : "<br/>Remplir le champ"
											},
										"hide" : {
											"formLocalityformLocality" : 1,
											"locationlocation" : 1
											} 
										}
									}   
								};
								dyfOrga.afterSave = function(orga) {
									dyFObj.commonAfterSave(orga, function(){
										$('#loader-loader<?= $kunik ?>').removeClass('hidden');
										$('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
										coInterface.showLoader('#content-loader<?= $kunik ?>');
										var thematicCocity = [];
										var allTags = [];
										if(orga.map.thematic && notNull(orga.map.thematic)) thematicCocity = (orga.map.thematic).split(",");
										if(orga.map.tags && notNull(orga.map.tags)) allTags = orga.map.tags;
										if(thematicCocity != []) {
											$.each(thematicCocity, function(k, value){
												var valeur = value.toLowerCase();
												
												if(!allTags.includes(valeur)) allTags.push(valeur);

												if (typeof  allThem[valeur] != "undefined" && notNull(allThem[valeur]["tags"])) {
													var tagsToAdd = allThem[valeur]["tags"];
													var newTags = tagsToAdd.filter(tag => !allTags.includes(tag));
													Array.prototype.push.apply(allTags, newTags);
												} 
											});
										}
										var data = {
											collection : 'organizations',
											id: orga.id,
											path: 'allToRoot',
											value : {
												"costum.slug" : "costumize",
												"costum.cocity" : true,
												"thematic" : thematicCocity,
												"tags" : allTags,
												"costum.typeCocity" : "ville"
											},
										}

										if (notNull(orga.map.thematic) ) {
											var thema = orga.map.thematic;
											thematic = thema.split(",");
											var filiers = {};
											var coordonateTemp = {
												"address" : orga.map.address,
												"geo" : orga.map.geo,
												"geoPosition" : orga.map.geoPosition
											};
											$.each(thematic , function(k,val) {
												var defaultTags = [];
												var valkey = val.toLowerCase();
												var valname = (val.toLowerCase()).charAt(0).toUpperCase() + (val.toLowerCase()).slice(1);
												
												if (typeof  allThem[valkey] != "undefined" && notNull(allThem[valkey]["tags"])) {
													defaultTags = allThem[valkey]["tags"]; 
													filiers[val] = allThem[valkey];
												} else {
													var oneThem = {
														"default" : {
															"name" : valname,
															"icon" : "fa-link",
															"tags" : [valname.toLowerCase()]
														}
													};
													filiers[valkey] = oneThem["default"];
												}
												var defaultName = orga.map.name +" "+ val;
												var paramscocity = {
													cocity : orga.id,
													ville : orga.map.name,
													typeZone : "ville",
													thematic : valname,
													keys: orga.map.slug,
													idTemplatefiliere : "<?= $_idTemplatefiliere ?>"
												};
												var params = {
													name: defaultName,
													collection : "organizations",
													type: "Group",
													role: "admin",
													image: "",
													tags: defaultTags,
													...coordonateTemp,
													addresses : undefined,
													shortDescription: "Filière "+val+" "+orga.map.name
												}
												createOrgaFiliere(params,paramscocity);  
											});
											if (filiers !== null && Object.keys(filiers).length == thematic.length){
												var tplCtx = {
													collection: "organizations",
													value: filiers,
													id: orga.id,
													path: "filiere",
												};
												dataHelper.path2Value(tplCtx, function (data){})
											}
										}

										dataHelper.path2Value(data, function(){
											<?php if($_idTemplatecocity != "") { ?>
												let tpl_params = {"idTplSelected" : "<?= $_idTemplatecocity ?>", "collection" : "templates", "action" : "", "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection};
												let loadpage = true;
												useTemplate(tpl_params, loadpage);
											<?php } ?>
										});
									});
								}
								setTimeout(() => {
									$(".processingLoader").remove();
									data.type = "GovernmentOrganization";
									data.role = "admin";
									dyFObj.openForm('organization',null,Object.assign(data, {}),null,dyfOrga);
									$('#loader-loader<?= $kunik ?>').addClass('hidden');
									$('#backdrop-loader<?= $kunik ?>').addClass('hidden');
								}, 1500);
								// 
							}
						)
					}
				}
			}
			$(".<?= $kunik ?>newCocity").on("click",function(){
				dyFObj.openForm(formvalidecity);
			});
		})
	</script>
<?php } ?>