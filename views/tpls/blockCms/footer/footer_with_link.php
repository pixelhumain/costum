<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) ["footer-area_$kunik" => $objectCss];

    $defaultSocialnetwork = array(
        [
            "icon" => "facebook",
            "nom" => "facebook",
            "urlnetwork" => ""
        ],
        [
            "icon" => "youtube-play",
            "nom" => "Youtube",
            "urlnetwork" => ""
        ],
        [
            "icon" => "twitter",
            "nom" => "X",
            "urlnetwork" => ""
        ],
        [
            "icon" => "linkedin",
            "nom" => "linkedin",
            "urlnetwork" => ""
        ]
    );

    if($blockCms["socialNetwork"] == null || $blockCms["socialNetwork"] == "undefined")
    {
        $blockCms["socialNetwork"] = $defaultSocialnetwork;
    }

    $defaultTexts = ["text1" => "texte 1", "text2" => "texte 2", "text3" => "texte 3"];

    foreach ($defaultTexts as $key => $defaultValue) {
        ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
        ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

        $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
    };

?>


<?php
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
        "id" => $blockKey,
        "type" => 'cms',
        "subKey" => 'pdf1',
    ),
    "file"
);
?>

<?php
$initFilesLogo = Document::getListDocumentsWhere(
    array(
        "id" => $blockKey,
        "type" => 'cms'
    ),
    "image"
);
$allLogo = [];
foreach ($initFilesLogo as $ke => $va) {
    $allLogo[$va["subKey"]] = $va;
}
?>

<style id="footer-area<?= $kunik ?>" type="text/css">

    .social-network<?= $kunik?>{
        margin-top : 40px;   
        margin-bottom : 20px;   
    }
    .social-network<?= $kunik?> ul.social-network {
		list-style: none;
		display: inline;
		margin-left:0 !important;
		padding: 0;
	}
	.social-network<?= $kunik?> ul.social-network li {
		display: inline;
		margin: 0 5px;
	}
    /* .social-network<?= $kunik?> .social-circle li a:hover {
		 background-color:< ?=$blockCms["css"]["iconSurvol"]["backgroundColor"] ?>;
	}
	.social-network<?= $kunik?> .social-circle li a:hover i {
		 color: < ?= $blockCms["css"]["iconSurvol"]["color"] ?>; 
	} */

	.social-network<?= $kunik?> .social-circle li a {
		display:inline-block;
		position:relative;
		margin:0 auto 0 auto;
		-moz-border-radius:50%;
		-webkit-border-radius:50%;
		border-radius:50%;
		text-align:center;
		width: 45px;
        height: 45px;
        font-size: 25px;
    }
	.social-network<?= $kunik?> .social-circle li i {
		margin:0;
		line-height:50px;
		text-align: center;
	}
	.social-network<?= $kunik?> .social-circle .socialIcon i {
		 /* color: < ?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $blockCms["css"]["icone"]["color"]; ?>; */
		-webkit-transition: all 0.8s;
		-moz-transition: all 0.8s;
		-o-transition: all 0.8s;
		-ms-transition: all 0.8s;
		transition: all 0.8s;
	}

	/* .social-network<?= $kunik?> .social-circle a.socialIcon {
	  background-color: < ?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["icone"]["backgroundColor"]; ?>;
	} */
    .footer-area_<?= $kunik?> a{
        text-decoration : none;
        cursor: pointer;
        
    }
    .footer-area_<?= $kunik?> {
        padding-left: 10%;
    }

    .footer-area_<?= $kunik?> #btnScrollTop {
        display: none;
        position: fixed;
        bottom: 20px;
        left: 100px;
        z-index: 99;
        font-size: 35px;
        border: none;
        outline: none;
        background-color: #BDD424;
        color: white;
        height: 55px;
        width: 55px;
        cursor: pointer;
        border-radius: 50%;
        line-height: 0;
    }

    @media (max-width: 767px) {
        .footer-area_<?= $kunik?> #btnScrollTop {
            left: 5px;
            height: 45px;
            width: 45px;
            font-size: 28px;
        }
    }
    /*#particl-js {
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: 50% 50%;
        position: absolute;
        top: 0px;
        z-index: 0;
    }*/

    .addFin<?= $kunik?>{
        color: white;
        background-color: #2C3E50;
        border-color: #2C3E50;
        font-weight: 700;
    }
    .financ<?= $kunik?> .btnAdd{
        margin-bottom : 10px;
    }
    .fin<?= $kunik?>{
        padding: 5px;
    }


    .financ<?= $kunik?> img {
        width: auto !important;
        height: 100px !important;
        object-fit : contain;
    }
 

    @media (min-width: 992px) and (max-width: 1199px) {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 70px !important;
        }
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 45px !important;
        }
    }
    @media  (max-width: 767px)   {
        .financ<?= $kunik?> img {
            width: auto !important;
            height: 75px !important;
        }
        .footer-area_<?= $kunik?> {
            padding-left: 1%;
        }
    }
    .allFin<?= $kunik?> {
        display: flex;
        flex-wrap: wrap;
    }

    .allFin<?= $kunik?> > div {
        flex-basis: auto; /* Les deux premiers divs occuperont 50% de la largeur chacun */
    }

    .footer-area_<?= $kunik?> .img-social{
        width: 35px;
        height: 35px;
        margin-top : -12px;
    }
</style>

<footer class="footer-area_<?= $kunik?>">
    <div class="row <?= $kunik?>">
        <div class="col-lg-5  col-md-5 col-sm-5 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc text1<?= $myCmsId ?> "  id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text1"></div>                
        </div>
        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc text2<?= $myCmsId ?> "  id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text2"></div>
        </div>
        <div class="col-lg-4  col-md-4 col-sm-3 col-xs-12 z-index-10">
            <div class="sp-text img-text-bloc text3<?= $myCmsId ?> " id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text3"></div>
            <?php 
                if (isset($blockCms["socialNetwork"])) { ?>  
                    <div class="social-network<?= $kunik?>">
                        <ul class="social-network social-circle">
                            <?php 
                            foreach ($blockCms["socialNetwork"] as $key => $value) {
                                $chaine = $value["icon"];
                                $caractaire = "fa fa-";
                                $icone = "";
                                if(strpos($chaine, $caractaire) !== false )
                                {
                                    $icone = $chaine;
                                } else {
								    $icone = "fa fa-".$chaine;
							    }
                            ?>
                                <li><a href="<?php echo isset($value['urlnetwork']) ? $value['urlnetwork'] : "#" ?>"  target="_blank" class="socialIcon icone" title="<?php echo $value['nom'] ?>"><i class="<?php echo $icone ?>"></i></a></li>
                            <?php } ?>
                        </ul> 				
                    </div>
            <?php } ?>
            <div class="financ<?= $kunik?>">
                <?php if($costum["editMode"] == "true"){ ?>
                    <div class="btnAdd">
                        <a class="hiddenPreview addFin<?= $kunik?>"> Ajouter un financeur</a>
                    </div> 
                <?php } ?>
                <div class="allFin<?= $kunik?> sortable-list" data-ref="financeur">

                    <?php 
                        foreach ($blockCms["financeur"] as $kF => $vF) {
                            ${'initFilesIcone' . $kF}= Document::getListDocumentsWhere(
                                array(
                                    "id"=> $blockKey,
                                    "type"=>'cms',
                                    "subKey"=> (string)$kF
                                ), "image"
                            );
                            ${'arrFileIcone' . $kF}= [];
                            foreach (${'initFilesIcone' . $kF} as $k => $v) {
                                ${'arrFileIcone' . $kF}[] =$v['imagePath'];
                            }?>

                        <div class="fin<?= $kunik?> draggable"  data-key-button="<?= $kF?>" >
                           <?php if($vF['link']!= ""){?>
                                <a href="<?= $vF['link']?> " target="_blank">
                            <?php 
                            }
                            if (count(${'arrFileIcone' . $kF})!=0) {?>
                                <img class="" src="<?php echo ${'arrFileIcone' . $kF}[0] ?>"  style=" width: <?= @$vF["width"]?>; height: <?= @$vF["height"] ?>">
                            <?php } ?>
                            <?php if($costum["editMode"] == "true"){ ?>
                                <div class="text-center editSectionBtns hiddenPreview" >
                                    <a  href="javascript:;"
                                    class="btn  btn-primary btn-editSection editFin<?= $kunik?>"
                                    data-key="<?= $kF ?>"
                                    data-link='<?= @$vF["link"] ?>'
                                    data-position='<?= $kF ?>'
                                    data-logo='<?php echo json_encode(${"initFilesIcone" . $kF}) ?>' >
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a  href="javascript:;"
                                    class="btn  bg-red text-center btn-editSection deleteElement<?= $kunik?> "
                                    data-id ="<?= $blockKey ?>"
                                    data-path="financeur.<?= $kF ?>"
                                    data-collection = "cms"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            <?php }
                            if($vF['link']!= ""){?>
                                </a>
                            <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>
        </div>
    </div>
    <button onclick="topFunction()" id="btnScrollTop" title="Go to top"> <i class="fa fa-angle-up"></i></button>
    <!--<div id="particl-js"></div>-->
</footer>


<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($blockCms) ?>;
    jQuery(document).ready(function() {
        $(".socialIcon[title^='twitter']").html('<i><img class=" img-social" src="'+assetPath + '/images/meir/twitter-blanc.png"></i>');
        var allFin = <?= json_encode($blockCms["financeur"])?>;
        var allLogo = <?= json_encode($allLogo)?>;
        if(costum.editMode)
            costumizer.htmlConstruct.events.draggable();
		let inc=0;
        var footer_with_link = {
            sortUpdate : function(){
                $(".sortable-list").each(function(){
                    sortable('.sortable-list')[inc].addEventListener('sortupdate', function(e) { 
                        e.stopImmediatePropagation();

                        let moveObj=jsonHelper.getValueByPath(sectionDyf.<?php echo $kunik ?>ParamsData,$(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
                        var inc=e.detail.destination.index;
                        if(e.detail.origin.index < e.detail.destination.index && e.detail.destination.index > 0 && e.detail.origin.index > 0)
                            inc--;
                        jsonHelper.deleteByPath(sectionDyf.<?php echo $kunik ?>ParamsData, $(e.detail.origin.container).data("ref")+"."+$(e.detail.item).data("key-button"));
                        jsonHelper.insertKeyValueByPathAndInc(sectionDyf.<?php echo $kunik ?>ParamsData, $(e.detail.origin.container).data("ref"),$(e.detail.item).data("key-button"), moveObj, inc);

                        var moveCtx = {
                            id : "<?= $blockKey?>",
                            value : sectionDyf.<?php echo $kunik ?>ParamsData.financeur,
                            collection : "cms",
                            path : "financeur"
                        }  
                        dataHelper.path2Value( moveCtx, function(params) { 
                                toastr.success("<?php echo Yii::t('cms', 'Edition success')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        } );   	
                    });
                    inc++;
                });
                sortable(".sortable-list", {
		            placeholderClass: "ph-class",
		            hoverClass: "hover",
		            acceptFrom : ".sortable-list, .add-to-list-sort"
		        });

            }
        }
        if(costum.editMode)
            footer_with_link.sortUpdate();
        
        $(".addFin<?= $kunik?>").on("click",function(){  
            var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($blockCms["financeur"]); ?>);
            var	lastContentK = 0; 
            if (keys<?= $blockCms['_id'] ?>.length!=0) {
                $.each(keys<?= $blockCms['_id'] ?>,function(k,v){
                    var key = parseInt(v.split("fin")[1],10);
                    if(key > lastContentK)
                        lastContentK = key
                })
            } 
            lastContentK = parseInt(lastContentK,10)+1; 
            var tplCtx = {};
            tplCtx.id = "<?= $blockKey ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "financeur."+("fin"+lastContentK);
            var obj = {
                subKey : ("fin"+lastContentK), 
                link :     $(this).data("link"),
                logo :     $(this).data("logo"),
                
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add a file')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".positiontext").css("display","none"); 
                        }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        position = "";
                        value = {}; 
                        $.each( activeForm.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        })
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                });
                            } );
                        }
                    }
                }
            }; 
            dyFObj.openForm(activeForm );
           
        });
        $(".editFin<?= $kunik?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($blockCms["financeur"]); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>" ;
			tplCtx.path = "financeur."+(key);
			tplCtx.collection = "cms"; 
			var obj = {
                subKey : key,
                link :     $(this).data("link"),
                logo :     $(this).data("logo"),
            };
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'File modification')?> "+$(this).data("title"),
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                        }
                    },
                    "properties" : getProperties(obj,key),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?php echo $blockKey ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {}; 
                        value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) {  
                           tplCtx.value[k] = $('#'+k).val()
                        });   
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                });
                            } );
                        }
                    }
                }
            };          
            dyFObj.openForm( activeForm );				 
        });
        $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",
            function(result){
                if (!result) {
                return;
                }else {
                dataHelper.path2Value( deleteObj, function(params) {
                    mylog.log("deleteObj",params);
                    toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                });
                }
            }); 
			
		});
        function getProperties(obj={}){
            var props = { 
                link : {
                    label : "<?php echo Yii::t('cms', 'Link')?>",   
                    inputType : "text",
                    value : obj["link"]
                },
				logo : {
					inputType : "uploader",	
					label : "<?php echo Yii::t('cms', 'Logo')?>",
                    docType: "image",
                    itemLimit : 1,
					contentKey : "slider",
					domElement : obj["subKey"],		
                    filetype : ["jpeg", "jpg", "gif", "png"],
                    showUploadBtn: false,
                    endPoint :"/subKey/"+obj["subKey"],
                    initList : obj["logo"]
				}
            };
            return props;
        }
        function createFontAwesomeSelect<?php echo $kunik ?>(){
            dyFObj.init.buildEntryList = function(num, entry,field, value){
                mylog.log("buildEntryList num",num,"entry", entry,"field",field,"value", value);
                countEntry=Object.keys(entry).length;
                defaultClass= (countEntry==1) ? "col-xs-10" : "col-xs-5";
            
                str="<div class='listEntry col-xs-12 no-padding' data-num='"+incEntry+"'>";
                    $.each(entry, function(e, v){
                        name=field+e+num;
                        classEntry=(typeof v.class != "undefined") ? v.class : defaultClass;
                        placeholder=(typeof v.placeholder != "undefined") ? v.placeholder : ""; 
                        str+="<div class='"+classEntry+"'>";
                        if(typeof v.label != "undefined"){
                            str+='<div class="padding-5 col-xs-12">'+
                                '<h6>'+v.label.replace("{num}", (num+1))+'</h6>'+
                            '</div>'+
                            '<div class="space5"></div>';
                        }
                        valueEntry=(notNull(value) && typeof value[e] != "undefined") ? value[e] : "";
                        if(v.type=="hidden")
                            str+='<input type="hidden" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                        else if(v.type=="text")
                            str+='<input type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" value="'+valueEntry+'" placeholder="'+placeholder+'"/>';
                        else if(v.type=="textarea")
                            str+='<textarea name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield form-control input-md col-xs-12 no-padding">'+valueEntry+'</textarea>';
                        else if(v.type=="select"){
                            str+='<select type="text" name="'+name+'" data-entry="'+e+'" id="'+field+num+'" class="addmultifield addmultifield0 form-control input-md col-xs-12 no-padding" >';
                            //This is the select Options-----------------//
                            str+= createSelectOptions(valueEntry,fontAwesome);
                            str+='</select>';             
                        }
                        str+="</div>";
                    });
                    str+='<div class="col-sm-1">'+
                            '<button class="pull-right removeListLineBtn btn bg-red text-white tooltips pull-right" data-num="'+num+'" data-original-title="Retirer cette ligne" data-placement="bottom" style="margin-top: 43px;"><i class=" fa fa-times"></i></button>'+
                        '</div>'+
                    '</div>';
                return str;
            }; 
        }
        function createSelectOptions(current,Obj){
            var html = "";
            $.each(Obj,function(k,v){
                html+='<option value="'+k+'" '+((current == k) ? "selected" : "" )+' >'+v+'</option>';
            });
            return html;
        }
    });      
    var mybutton = document.getElementById("btnScrollTop");
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    function topFunction() {
        $('html, body').animate({scrollTop : 0},600);
    }
    /*cmsEngine.callParticle("particl-js");*/
    var slinks = sectionDyf.<?php echo $kunik ?>ParamsData.socialNetwork;
    if (slinks.length > 0) {
        for (var i = 0; i < slinks.length; i++) {
            var chaine = slinks[i].icon;
            var caractere = "fa fa-";
            if (!chaine.includes(caractere)) {
                slinks[i].icon = "fa fa-" + slinks[i].icon + "";
            }
        }
        sectionDyf.<?php echo $kunik ?>ParamsData.socialNetwork = slinks;
    }
    var styleCss = "";
    styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#footer-area<?= $kunik ?>").append(styleCss);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>ParamsData;
        var footerWithLink = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "inputMultiple",
                            options: {
                                label: "<?php echo Yii::t('cms', 'Social networks') ?>",
                                name: "socialNetwork",
                                inputs: [
                                        {
                                            type: "inputSimple",
                                            options: {
                                                label: "<?php echo Yii::t('cms', 'Name of the social network') ?>",
                                                name: "nom",
                                            },
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                label: "<?php echo Yii::t('cms', 'Url of the page') ?>",
                                                name: "urlnetwork",
                                            },
                                        },
                                        {
                                            type: "inputIcon",
                                            options: {
                                                label:"<?php echo Yii::t('cms', 'Icon')?>",
                                                name: "icon",
                                            },
                                        },
                                ]
                            }
                        },
                    ],
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "icone",
                                label: tradCms.icon,
                                inputs: [
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        }
                    ]
                },
                hover: {
                    inputsConfig : [
                        {
                            type: "section",
                            options: {
                                name: "icone",
                                label: tradCms.networkicononhover,
                                inputs: [
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        },
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            },
        }
        cmsConstructor.blocks.footer_with_link<?= $myCmsId ?> = footerWithLink;
    }
    appendTextLangBased(".text1<?= $myCmsId ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($text1) ?>,"<?= $myCmsId ?>");
    appendTextLangBased(".text2<?= $myCmsId ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($text2) ?>,"<?= $myCmsId ?>");
    appendTextLangBased(".text3<?= $myCmsId ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($text3) ?>,"<?= $myCmsId ?>");
</script>