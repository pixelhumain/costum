<?php if(Authorisation::isInterfaceAdmin()){
    $defaultTexts = [
        "blockTitle" => "Les différents badges des membres du club",
        "blockSubTitle" => "En plus d'être membre, certains membres peuvents prendre des casquettes supplémentaires. Découvrez ce que signifie chaque badge et comment accéder à chaque rôle."
    ];

    foreach ($defaultTexts as $key => $defaultValue) {
        ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
        ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

        $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});

        ;
    };


    ?>

    <!--<div class="container">
    <div class="col-xs-12 col-sm-4">
        <img src="<?/*= Yii::app()->getModule('co2')->assetsUrl; */?>/images/badge.png" style="width: 100%; height: auto;" >
    </div>
    <div class="col-xs-12 col-sm-8">
        <h3 id="blockTitle<?/*= $blockKey */?>" class="sp-text blockTitle<?/*= $blockKey */?> default-text-params" data-id="<?/*= $blockKey */?>" data-field="blockTitle"></h3>
        <div id="blockSubTitle<?/*= $blockKey */?>" class="sp-text blockSubTitle<?/*= $blockKey */?> default-text-params " data-id="<?/*= $blockKey */?>" data-field="blockSubTitle"></div>
    </div>
</div>-->
    <div id="badgeLists<?= $blockKey ?>"></div>
    <script type="text/javascript">

        if (costum.editMode){
            cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
        }

        /*appendTextLangBased(".blockTitle<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockTitle) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".blockSubTitle<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockSubTitle) ?>,"<?= $blockKey ?>");
*/


        $("document").ready(function(){
            ajaxPost(
                null,
                baseUrl+'/co2/badges/finder/email/false/source/'+costum.contextSlug+'/parentId/'+costum.contextId,
                null,
                function(htmlData){
                    $("#badgeLists<?= $blockKey ?>").append(htmlData);
                    if(!costum.isCostumAdmin){
                        $("#badgeLists<?= $blockKey ?> .container .row")[0].remove();
                    }
                },
                null
            );
        });
    </script>
<?php } ?>