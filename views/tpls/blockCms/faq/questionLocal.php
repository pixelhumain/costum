<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style id="questionlocal<?= $kunik ?>">
   .<?= $kunik ?>_questionLocal .btn-position{
		text-align: <?=$blockCms["btnPosition"] ?> ;
	}
    .<?= $kunik ?>_questionLocal .<?= $kunik?>_addFaq {
		/* background: < ?= $blockCms["bouton"]["btnBg"] ?>; */
		margin-top: 5%;	
		/* color: < ?= $blockCms["bouton"]["btncolorLabel"] ?>;
		border-color : < ?= $blockCms["bouton"]["btnBorderColor"] ?>; */
		/* border-radius : < ?= $blockCms["btnBorderRadius"]?>px; */
		border-width: 1px;
		padding: 10px 15px;
		text-align: center;
		font-size: 20px;
	}
    @media (max-width: 978px) {
		.<?= $kunik ?>_questionLocal .<?= $kunik?>_addFaq {
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}
	}
    .<?= $kunik ?>_questionLocal .icon-<?=$kunik?>{
        padding:0;
        list-style:none
    }
    .<?= $kunik ?>_questionLocal .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        /* color: < ?= $blockCms["bouton"]["btncolorLabel"]; ?>; */
        font-size:18px;
        background:<?= $blockCms["css"]["question"]["backgroundColor"]; ?>;
        margin-right:10px;transition:all .5s ease 0s
    }
    .<?= $kunik ?>_questionLocal .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }

    .<?= $kunik ?>_questionLocal .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
    }
    .<?= $kunik ?>_questionLocal .icon-<?=$kunik?> li{
        display:inline-block;
        text-align: center;
    }
    .<?= $kunik ?>_questionLocal .faq-img {
		width:40%;
		height:auto;
		vertical-align:middle;
		float: left;
		padding-right:15px;
	}
    @media (max-width: 767px) {
		.<?= $kunik ?>_questionLocal .faq-img {
			width:100%;
			height:auto;
			vertical-align:middle;
			float: left;
			padding-right:0px;
			padding-bottom: 15px;
		}
		.<?= $kunik ?>_questionLocal .faq-container .panel-default {
			padding-right: 0px;
			padding-left: 0px;
		}
	}
    .<?=$kunik ?>_panel-group {
		margin-left: 5%;
		padding: 30px;
	}
    .<?=$kunik ?>_panel-group #accordion .panel {
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin: 0 0 15px 0px;
		background : transparent;
	}
	.<?=$kunik ?>_panel-group #accordion .panel-heading {
		border-radius: 30px;
		padding: 0;
	}
	.<?=$kunik ?>_panel-group #accordion .panel-title {
		text-transform: none;
	}
    .<?=$kunik ?>_panel-group #accordion .panel-title a {
		background: <?= $blockCms["css"]["question"]["backgroundColor"]; ?> ;
		border: 1px solid transparent;
		border-radius: 30px;
		color: #fff;
		display: block;
		font-size:<?php echo $blockCms["css"]["question"]["fontSize"]; ?>;
		font-weight: 600;
		padding: 12px 20px 12px 50px;
		position: relative;
		transition: all 0.3s ease 0s;
	}
    .<?=$kunik ?>_panel-group .panel-default>.panel-heading{
		background-color: transparent ;
	}
	.<?=$kunik ?>_panel-group #accordion .panel-title a.collapsed {
		background: transparent none repeat scroll 0 0;
		border: 1px solid #ffffff;
		color: <?= $blockCms["css"]["question"]["color"];?>;
	}
    .<?=$kunik ?>_panel-group #accordion .panel-title a::after,.<?=$kunik ?>_panel-group #accordion .panel-title a.collapsed::after {
		background: <?= $blockCms["css"]["question"]["backgroundColor"]; ?>;
		border: 1px solid transparent;
		border-radius: 50%;
		box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
		color: #fff;
		content: "";
		font-family: fontawesome;
		font-size: 25px;
		height: 55px;
		left: -20px;
		line-height: 55px;
		content: "";
		font-family: fontawesome;
		position: absolute;
		text-align: center;
		top: -5px;
		transition: all 0.3s ease 0s;
		width: 55px;
	}
    .<?=$kunik ?>_panel-group #accordion .panel-title a.collapsed::after {
		background: #fff none repeat scroll 0 0;
		border: 1px solid #ddd;
		box-shadow: none;
		color: #333;
		content: "";
	}
	.<?=$kunik ?>_panel-group #accordion .panel-body {
		background: transparent none repeat scroll 0 0;
		border-top: medium none;
		padding: 20px 25px 10px 9px;
		color: <?= $blockCms["css"]["text"]["color"];?>;
		font-size: <?= $blockCms["css"]["text"]["fontSize"]?>;
		position: relative;
	}
	.<?=$kunik ?>_panel-group #accordion .panel-body p, .<?=$kunik ?>_panel-group #accordion .panel-body  text-response-faq {
		border-left: 1px dashed #8c8c8c;
		padding-left: 25px;
		font-size: <?= $blockCms["css"]["question"]["fontSize"]?>;
	}
	.<?=$kunik ?>_panel-group .text-response-faq {
		text-align: justify;
	}
</style>
<div class="<?=$kunik?>_questionLocal <?=$kunik?>" id="questionlocal-section" data-kunik="<?= $kunik ?>" data-name="questionlocal" data-id="<?= $myCmsId ?>">
    <div class="questionlocal-css">
        <div class="questionlocal-content">
            <?php if(Authorisation::isInterfaceAdmin()){ ?>
                <div class="btn-position">
                    <a href="javascript:;" class="btn btn-xs <?= $kunik?>_addFaq bouton" >
                        <i class="fa fa-plus"></i>
						<?php echo Yii::t('cms', "Add question") ?>
                    </a>
                </div>
            <?php } ?>
            <div class="<?=$kunik ?>_panel-group">
			
	        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $blockCms ) ?>;
	var dyfPoi={
		"beforeBuild":{
			"properties" : {
				"name" : {
					"label" : tradCms.yourQuestion,	
					"inputType" : "textarea",
					"placeholder" : "",
					"order" : 1
				},
				"shortDescription" : {
					"inputType" : "textarea",
					"markdown" : true,
					"label" : tradCms.answer,
					"order" : 2
				},
				"sizeInColonne" : {
					"inputType" : "select", 
					"label" : tradCms.questionDisplay,
					"options" : {
						1 : "Une ligne",
						2 : "Deux colonne"
					},
					"order" : 2
				}
			}
		},
		"onload" : {
			"actions" : {
				"setTitle" : tradCms.addQuestion,
				"src" : {
					"infocustom" : "<br/>"+tradCms.fillinthefield
				},
				"presetValue" : {
					"type" : "faq",
				},
				"hide" : {
					"locationlocation" : 1,
					"breadcrumbcustom" : 1,
					"urlsarray" : 1,
					"parent" : 1,
					"tagstags" : 1,
					"formLocalityformLocality" : 1,
					"descriptiontextarea" : 1,
				}
			}
		}

	};
	dyfPoi.afterSave = function(data){
		dyFObj.commonAfterSave(data, function(){
			urlCtrl.loadByHash(location.hash);
		});
	}
	var kunik = <?= json_encode($kunik)?>; 
	var allPoi = {};
	var faqObj<?= $kunik?> = {
		//defaultValue : [],
		views : function(index,value,i){
			var src = ""
			var classNumberLign = (typeof value.sizeInColonne != "undefined" && value.sizeInColonne == 1)? "col-md-12" : "col-md-6";
			if (typeof value.shortDescription != "undefined"){
				shortDescription = value.shortDescription;
			} else {
				shortDescription ="";
			}
			if (i==0 && typeof sectionDyf.<?php echo $kunik?>ParamsData.questionAccordion != "undefined" && sectionDyf.<?php echo $kunik?>ParamsData.questionAccordion == "false") {
				src += '<div class="panel panel-default col-xs-12 col-sm-12 '+classNumberLign+'">';
				src += '<div class="panel-heading sp-bg" data-id="<?= $blockKey ?>" data-field="background_color2" data-value="<?= $blockCms['background_color2']; ?>" data-sptarget="bd" data-kunik="<?= $kunik?>" role="tab" id="heading'+index+""+kunik+'">';
				src += '<h4 class="panel-title">';
				src += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+""+kunik+'" aria-expanded="false" aria-controls="collapse'+index+""+kunik+'">' +
				value.name;

				src += '</a>';

				src += '</h4>';

				src += '</div>';
				src += '<div id="collapse'+index+""+kunik+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+index+""+kunik+'">';
				src += '<div class="panel-body">';
				if (value.profilMediumImageUrl != null) {
					src += '<img class="faq-img" src="'+value.profilMediumImageUrl+'">';
				}

				
				src += '<div class="text-response-faq markdown" id="markdown'+value.id+'">'+shortDescription+'</div>';

				if(isInterfaceAdmin && costum.editMode)
					src +='<ul class="hiddenPreview icon-<?=$kunik?>">'+
				'<li>'+
				'<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-edit"></i></a>'+
				'</li>'+
				'<li>'+
				'<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-trash"></i></a>'+
				'</li>'+
				'</ul>';
				src += '</div>';
				src += '</div>';
				src += '</div>';

			
			}else{
				src += '<div class="panel panel-default col-xs-12 col-sm-12 '+classNumberLign+'">';
				src += '<div class="panel-heading sp-bg" data-id="<?= $blockKey ?>" data-field="background_color2" data-value="<?= $blockCms['background_color2']; ?>" data-sptarget="bd" data-kunik="<?= $kunik?>" role="tab" id="heading'+index+""+kunik+'">';
				src += '<h4 class="panel-title ">';
				src += '<a class="collapsed " role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+index+""+kunik+'" aria-expanded="false" aria-controls="collapse'+index+'">' +
				value.name;
				src += '</a>';
				src += '</h4>';
				src += '</div>';
				src += '<div id="collapse'+index+""+kunik+'" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading'+index+""+kunik+'">';
				src += '<div class="panel-body">';
				mylog.log("profill", value.profilImageUrl);
				if (value.profilMediumImageUrl != null) {
					src += '<img class="faq-img" src="'+value.profilMediumImageUrl+'">';
				}
				
				src += '<div class="text-response-faq markdown" id="markdown'+value.id+'">'+shortDescription+'</div>';
				if(isInterfaceAdmin && costum.editMode)
				src +='<ul class="hiddenPreview icon-<?=$kunik?>">'+
				'<li>'+
				'<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-edit"></i></a>'+
				'</li>'+
				'<li>'+
				'<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value.id+'" ><i class="fa fa-trash"></i></a>'+
				'</li>'+
				'</ul>';
				src += '</div>';
				src += '</div>';
				src += '</div>';
			}
			setTimeout(function(){
				descHtml = dataHelper.markdownToHtml($("#shortDescription"+value.id).html()); 
				$("#shortDescription"+value.id).html(descHtml);
			},100)	
			setTimeout(function(){
				descHtml = dataHelper.markdownToHtml($("#markdown"+value.id).html()); 
				$("#markdown"+value.id).html(descHtml);
			},100)
			return src;

		},
		getFaqs : function(){
			var params = {
				source : cmsBuilder.config.context.slug,
				type: "faq",
				limit : ""
			}
			var allFaq = {};
			var el = {}
			ajaxPost(
				null,
				baseUrl+"/costum/costumgenerique/getpoi",
				params,
				function(data){ 
					if((sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0) { 
						el = data.element;
						$.each(sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay, function(ke, ve){
							if(typeof el[ve] != "undefined"){ 
								allFaq[ve]=el[ve]["name"]
								allPoi[ve]=el[ve]
								delete el[ve];  
							}
						})			
						$.each(el, function(kel, vel){
							allFaq[kel] = vel.name; 
							allPoi[kel] = vel;
						})           
					}else{ 
						allPoi = data.element;
						$.each(allPoi,function(kf,vf){
							allFaq[kf] = vf.name
							//faqObj< ?= $kunik?>.defaultValue.push(kf);
						})
					}
					var src = "";
					src +='<div class="row">';
					src +='<div class="col-md-12">';
					src += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';	
					if (data.length != 0){
						if((sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0){ 
							var i = 0;
							$.each(sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay,function(k,v){	
								if(exists(allPoi[v])){ 
									src += faqObj<?= $kunik?>.views(v,allPoi[v],i);
									i+=1;				
								}
							})	
							coInterface.bindLBHLinks();	 
						}else{ 
							var i = 0;
							$.each(allPoi, function( index, value ) { 
								src += faqObj<?= $kunik?>.views(index,value,i);
								i+=1;				
							});	
							coInterface.bindLBHLinks();		
						}

					}else {
						src += "<p class='text-center question'>" + tradCms.thereisnoquestion + " </p>";
					}

					src += '</div>';
					src += '</div>';
					src += '</div>';	
					$(".<?php echo $kunik ?>_panel-group").html(src);
					$(".<?=$kunik?>_panel-group .edit").off().on('click',function(){
						var id = $(this).data("id");
						var type = $(this).data("type");
						dyFObj.editElement('poi',id,type,dyfPoi);
					});
					$(".<?=$kunik?>_panel-group .delete").off().on("click",function () {
						$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
						var btnClick = $(this);
						var id = $(this).data("id");
						var type = "poi";
						var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
						
						bootbox.confirm(tradCms.doyoureallywanttodeletethisnews,
						function(result) 
						{
						if (!result) {
							btnClick.empty().html('<i class="fa fa-trash"></i>');
							return;
						} else {
							ajaxPost(
								null,
								urlToSend,
								null,
								function(data){ 
									if ( data && data.result ) {
										toastr.success("élément effacé");
										$("#"+type+id).remove();
										urlCtrl.loadByHash(location.hash);
									} else {
									toastr.error("something went wrong!! please try again.");
									}
								}
							);
						}
						});
					});
				} ,null,null,{"async":false}
			); 
			return allFaq;
		}
	}

    $(".<?= $kunik ?>_addFaq").click(function(){
        dyFObj.openForm('poi',null, null,null,dyfPoi); 
    });
	$(function(){
		faqObj<?= $kunik?>.getFaqs();
	})
</script>
<script>
    var str = "";
	var btnpositions = {
		"center" : tradCms.center,
		"left" : tradCms.left,
		"right" : tradCms.right
	};
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#questionlocal<?= $kunik ?>").append(str);
	allFaqs = faqObj<?= $kunik?>.getFaqs();
	var datadefault = [];
	$.each( allFaqs, function( key, val ) {
		datadefault.push(key);
	});
	sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay = (sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay).length != 0 ? sectionDyf.<?php echo $kunik?>ParamsData.faqDisplay : datadefault;
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik?>ParamsData;
        var questionLocal = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "select",
                            options: {
                                name: "btnPosition",
                                label: tradCms.buttonposition,
                                options: $.map( btnpositions, function( key, val ) {
											return {value: val, label: key}
										})
                            }
                        },
                        {
                            type:"groupButtons",
                            options: {
                                name:"questionAccordion",
                                label:tradCms.closeallquestion,
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ]
                            }
                        },
						{
							type : "selectMultiple",
							options : {
								name : "faqDisplay",
								label : tradCms.questiontodisplay,
								canDragAndDropChoise : true,
								options : $.map( allFaqs, function( key, val ) {
									return {value: val, label: key}
								})
							},
						}
                    ],
                },
				style: {
					inputsConfig: [
						{
							type: "section",
							options: {
								name: "question",
								label: "Question",
								inputs: [
									"fontSize",
									"color",
									"backgroundColor"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "bouton",
								label: tradCms.button,
								inputs: [
									"backgroundColor",
									"color",
									"borderColor",
									"borderRadius"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "text",
								label: "Text",
								inputs: [
									"fontSize",
									"color"
								]
							}
						},
					]
				},
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },

            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.questionLocal<?= $myCmsId ?> = questionLocal;
    }
</script>