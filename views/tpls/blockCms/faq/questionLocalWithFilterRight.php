<?php 
$keyTpl = "questionLocalWithFilterRight";
$paramsData = [
	"btnBg" => "#3dad6f",
	"btnPosition" => "right",
	"btnEditBg"=>"#8ABF32",
	"btncolorLabel" =>"#fff"

];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}  
?>
<style> 
	.<?= $kunik?> .annuaire-card {
		background: #f2f2f2;
		border-radius: 9px;
		overflow: hidden;
		box-shadow: 0 3px 6px #00000029;
		margin-bottom: 10px;
		margin-top: 10px;
		padding-left: 20px;
		padding-right: 20px;
		padding-top: 15px;
		padding-bottom: 15px;
	}
	.<?= $kunik?> .annuaire-card .annuaire-title {
		color: #38414d;
		font-size: 16px;
		font-weight: 700;
	}
	.<?= $kunik?> .annuaire-card .annuaire-content {
		color: #38414d;
		font-size: 14px;
		margin-top: 25px;
		font-weight: 400;
		line-height: 25px;
	}
	#allFaq<?= $kunik?> {
		display: flex;
		flex-wrap: wrap;
		padding: 10px;
		justify-content: left;
	}
	.<?=$kunik?> .annuaire-card .annuaire-contact  {
	 
		text-align: right;
		cursor: pointer;
	}
	.<?=$kunik?> .annuaire-card .annuaire-contact a{
		color: #3dad6f;
		font-size: 16px;
		font-weight: 700;
		margin-top: 20px; 
	}
	.<?=$kunik?> {
		padding-bottom: 5%; 
		box-shadow: 0px 0px 10px -8px silver;

	}
	.<?=$kunik?> .addFaq<?= $kunik?>{
		background: <?= $paramsData["btnBg"]?>;	
		color: <?= $paramsData["btncolorLabel"]?>;
		margin-top: 1%;
		border-width: 1px;
		padding: 5px 10px;
		text-align: center;
		font-size: 15px;
	}
	@media (max-width: 978px) {
		.<?=$kunik?> .addFaq<?= $kunik?>{
			margin-top: 5%;
			margin-left: 10%;
			padding: 10px;
			text-align: center;
			font-size: 17px;
		}
	} 
	.<?= $kunik?> .annuaire-card:hover  .icon-<?=$kunik?>{
		display:block;
	}
	.<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
		display: none;
        list-style:none
    }
    .<?=$kunik?> .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        color: <?php echo $paramsData["btncolorLabel"]; ?> ;
        font-size:18px;
        background:<?php echo $paramsData["btnEditBg"]; ?>;
         margin-right:10px;transition:all .5s ease 0s
    }
    .<?=$kunik?> .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }
    /* .<?=$kunik?>{
        box-shadow:0 0 3px rgba(0,0,0,.3)
    } */
    .<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
    }
    .<?=$kunik?> .icon-<?=$kunik?> li{
        display:inline-block;
        text-align: center;
    }
	.<?= $kunik?> .deleteThisBtn ,.btn-edit-preview{
		display : none;
	}
	#filterfaq<?=$kunik?>{
		margin-top: 5px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown .btn-menu, 
	div#filters-nav .dropdown .btn-menu, 
	div#filters-nav .filters-btn, 
	#content-admin-panel .filterContainer .dropdown .btn-menu {
		font-size: 16px;
		text-decoration: none;
		padding: 20px;
		height: 40px;
		border-radius: 4px 4px 0 0!important;  
		color: #1e1e1e!important; 
	}
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown, #filterfaq<?=$kunik?> #filterContainerInside .dropdown {
		float: left;
		display: grid;
		height: 40px;
		margin-bottom: 5px;
		margin-right: 10px;
	}	
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown{
		min-height: 56px;
		border: 1px solid #c3c5c9;
		border-radius: 9px;
		font-size: 16px; 
    	width: 30%;
		background-color: #f0f0f0;  
		color: black;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu .list-filters {
		max-height: 300px;
		overflow-y: scroll;
	}
	#filterfaq<?=$kunik?> #filterContainerInside  .dropdown .dropdown-menu {
		position: absolute;
		overflow-y: visible !important;
		top: 50px;
		left: 5px;
		width: 250px;
		border-radius: 2px;
		padding: 0px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu button {
		border: none;
		background: white;
		color: black;
		font-weight: 100 ;
		text-align: left;
		border-radius: 0;
		font-size: 14px !important;
		padding: 5px;
		padding-left: 15px;
	}
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu:before,
	#filterfaq<?=$kunik?> #filterContainerInside .dropdown .dropdown-menu:after {
		bottom: 100%;
		margin-top: -20px;
		border: solid transparent;
		content: " ";
		height: 0px;
		left: 19px;
		width: 0;
		border-width: 11px;
		position: absolute;
	}
	.<?= $kunik?> .list-filters{
		background-color: white;
		border: 1px solid #aaa;
		border-radius: 4px;
		box-sizing: border-box;
		display: block;
		position: absolute; 
		width: 100%;
		z-index: 1051;
		border-top: none;
		border-top-left-radius: 0;
		border-top-right-radius: 0;
	}
	.<?= $kunik?> .container-filters-menu #activeFilters .filters-activate{
		background: #3cad6f!important;
    	color: #fff!important;
		padding-left: 9px!important;
		padding-right: 9px!important;
		border-radius: 16px!important;
		margin-right: 10px!important;
	}
	.<?= $kunik?> .btn-filters-select:hover {
		background: #3cad6f!important;
    	color: #fff!important;
	}
	.btn-position<?= $kunik?>{
		text-align: <?=$paramsData["btnPosition"] ?> ;
	} 


    #filterfaq<?= $kunik?> #input-sec-search .input-global-search, .searchBar-filters .search-bar {
        font-size: 16px;
        box-shadow: inset 0 -3px 0 0 #2d328d;
        border-radius: 4px 0px 0px!important;
        background-color: #f0f0f0;
        margin-top: 0px;
        color: #1e1e1e;
        border: 1px solid #f0f0f0!important;
    }
    #filterfaq<?= $kunik?> .searchBar-filters{
        width: 100%;
    }
    #filterfaq<?= $kunik?> #filterContainerInside .main-search-bar-addon {
        border: 1px solid #dadada !important;
        background-color: #2D328D !important;
        color: #fff!important;
        font-size: 14px;
        height: 35px;
        padding: 9px 7px;
        border-radius: 0 4px 0 0!important;
        border-left: 0px !important;
    }
    .horizontalList .btn-filters-select{
        background-color: transparent;
        box-shadow: inset 0 0 0 1px #000091; 
        color: #000091;
        border: none;
    }
    .horizontalList button.btn-filters-select:hover{
        background-color: transparent;
        box-shadow: inset 0 0 0 1px #000091; 
        color: #000091;
        border: none;
    }
    .horizontalList .btn-filters-select.active {
        background: #000091;
        border: none;
        color: white;
    }
</style>
<div class=" <?= $kunik?> col-xs-12"> 
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
		<div class="btn-position<?= $kunik?>">
			<a href="javascript:;" class="btn btn-xs addFaq<?= $kunik?>" >
				<i class="fa fa-plus"></i>
				Ajouter une question
			</a>
		</div>
	<?php } ?>
    <div class="col-xs-8"> 
        <div class="row">
            <div class="col-md-12">
        		<div class='headerSearch<?= $kunik ?> no-padding col-xs-12'></div>
                <div id="allFaq<?= $kunik?>"></div> 
            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div id="filterfaq<?= $kunik?>"></div>
        <div id="listCategory"></div>
        <div id="listThematic"></div>
        <div id="listFamily"></div>
    </div>	
</div>
<script>

	var statusList = {
        "Une personne morale" : "personne morale (Entreprise)",
        "Personne physique" : "personne physique (Particuliers)",
        "PME/GE" : "PME/GE"
    }
    var accompanimentType = {
        "Accompagnement - Support": "Un accompagmnement mentorat, des conseils",
        "Financement": "Un financement (lister les types)",
        "Formation": "Une formation ",
        "Mise en réseau": "Des partenaires,du réseau, un associé etc.",
        "Pilotage": "Pilotage", 
        "Tiers Lieux": "Un local, un local commercial, un bureau"
    } 
	var paramsFilter = {
		container : "#filterfaq<?= $kunik?>",
		results :{
			dom : '#allFaq<?= $kunik?>',
			renderView : "faqObj.views",
			map : {
				active : false,
				sameAsDirectory:true,
				showMapWithDirectory:true
			}
		},
		header :{
			dom: ".headerSearch<?= $kunik ?>",
			options:{
				left:{
					group :{
						count : true
					}
				}
			}
		},
		defaults : {
			fields : ["type","name","shortDescription","description","profilImageUrl","profilThumbImageUrl"],
			types : ["poi"],
			filters : {
				type : "faq"				
			}
		},
		filters : {
            text :true,
            status :{
                view : "horizontalList",
                type : "tags",
                dom : "#listCategory",
                name : "Je suis<br>", 
                active : true,
	 			keyValue: false,
                event : "tags",
                classList : "pull-left favElBtn btn", 
                list : statusList
            }, 
			family :{
                view : "horizontalList",
                dom : "#listThematic", 
                active : true,
				type : "tags",
                classList : "pull-left favElBtn btn", 
				name : "Je cherche",
				event : "tags",
	 			keyValue: false,
				list : accompanimentType 

			},
			thematic :{
				view : "horizontalList", 
                dom : "#listFamily", 
                active : true,
				type : "tags",
				name : "Famille",
				event : "tags",
                classList : "pull-left favElBtn btn", 
	 			keyValue: false,
				list : costum.lists.themes

			}
		}
	};
	var filterSearch={};
	var dyfPoi={
		"beforeBuild":{
			"properties" : {
				"name" : {
					"label" : "Votre question",	
					"inputType" : "textarea",
					"placeholder" : "",
					"order" : 1
				},
				"shortDescription" : {
					"inputType" : "textarea",
                    "markdown" : true,
					"label" : "Réponse",
					"order" : 2
				}
			}
		},
		"onload" : {
			"actions" : {
				"setTitle" : "Ajouter un(e) Question",
				"src" : {
					"infocustom" : "<br/>Remplir le champ"
				},
				"presetValue" : {
					"type" : "faq",
				},
				"hide" : {
					"parentfinder" : 1,
					"locationlocation" : 1,
					"breadcrumbcustom" : 1,
					"urlsarray" : 1,
					"parent" : 1,
					"tagstags" : 1,
					"formLocalityformLocality" : 1,
					"descriptiontextarea" : 1,
				}
			}
		}

	};
	dyfPoi.afterSave = function(data){
		dyFObj.commonAfterSave(data, function(){
			urlCtrl.loadByHash(location.hash);
		});
	}; 
	var faqObj = {
		edit<?= $kunik?> : function(type,id){
			dyFObj.editElement('poi',id,type,dyfPoi);
		},
		delete<?= $kunik?> : function(id){
			$(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');		
			var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/poi/id/"+id;
		
			bootbox.confirm("voulez vous vraiment supprimer cette question !!",
				function(result) {
					if (!result) { 
						return;
					} else {
						ajaxPost(
							null,
							urlToSend,
							null,
							function(data){ 
								if ( data && data.result ) {
									toastr.success("élément effacé");
									$("#"+type+id).remove();
									urlCtrl.loadByHash(location.hash);
								} else {
									toastr.error("something went wrong!! please try again.");
								}
							}
						);
					}
				}
			);
		},
		views : function(params){
			var src = "";
			src+= "<div class='annuaire-card col-xs-12 col-sm-12 col-md-4'>"+
						"<div class='annuaire-title'> "+params.name+"</div>"+
						"<div class='annuaire-content'> "+params.shortDescription+"</div>";
						if(isInterfaceAdmin == true){
								src +="<ul class='hiddenPreview icon-<?=$kunik?>'>"+
									"<li>"+
									"<a href='javascript:;'  onclick='faqObj.edit<?= $kunik?>("+"\""+params.type+"\""+","+"\""+params.id+"\""+")'   ><i class='fa fa-edit'></i></a>"+
									"</li>"+
									"<li>"+
									"<a href='javascript:;'  onclick='faqObj.delete<?= $kunik?>("+"\""+params.id+"\""+")'   ><i class='fa fa-trash'></i></a>"+
									"</li>"+
								"</ul>";
							}
							src +="<div class='annuaire-contact' >";
							
							src +="<a href='#page.type.poi.id."+params.id+"' class='lbh-preview-element' >En savoir plus</a>"+
						"</div>"+
					"</div>";
		
			return src;
		}
	}
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.search.init(filterSearch,0);
	
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section1",
				"icon" : "fa-cog",
				"properties" : {
					"btnEditBg" : {
						"label" : "Couleur du background du bouton édition et suppression",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnEditBg
					},
					"btnBg" : {
						"label" : "Couleur du background du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBg
					},					
					"btncolorLabel" : {
						"label" : "Couleur du label du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btncolorLabel
					},
					
					"btnPosition" : {
						"label" : "Position du bouton",
						inputType : "select",
						"options" : {
							"center" : "Au centre",
							"left" : "À gauche",
							"right" : "À droite"
						},
						values :  sectionDyf.<?php echo $kunik?>ParamsData.btnPosition
					}
				},
				beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("Élément bien ajouté");
	                      $("#ajax-modal").modal('hide');
	                      urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
					
				}
			}
		};
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			 alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"question",4,6,null,null,"Propriété du question","green","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton","green","");
		});
		$(".addFaq<?= $kunik?>").click(function(){
            dyFObj.openForm('poi',null, null,null,dyfPoi); 
        })
		
	});
</script>