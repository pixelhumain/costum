
<?php 
    $blockCms["data"] = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
?>

<!-- ****************get image uploaded************** -->
<?php 

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );
    $image = [];

    foreach ($initImage as $key => $value) {
        $image[]= $value["imagePath"];
    }

    if($image!=[]){
        $blockCms["image"] = $image[0];
    }
    
 ?>
<!--****************end get image uploaded**************-->

<?php 

    $userId = "";

    if(isset(Yii::app()->session["userId"])){
        $userId = Yii::app()->session["userId"];
    }


    $formId = "";

    # If user has already joined
    $members = array();
    if($userId!="" && strlen($userId)>1){
        $members = PHDB::find("organizations", array("links.members.".$userId=>['$exists' => true ], "source.keys"=>$costum['slug']));
    }
    
    $is_member = false;
    if(count($members)!=0){
        $is_member = true;
    }

    if($blockCms["formId"]!=""){
        $formId = $blockCms["formId"];
    }else{
        # Get the form id
        $form = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        foreach($form as $key => $value) { $formId = $key; }
    }

    if(!empty($formId))
        $formParams=PHDB::findOneById(Form::COLLECTION,$formId);

    # If user has submited the coform
    $answers = PHDB::find("answers", array("source.keys" => $costum['slug'], 'form' => $formId ,  "user" => $userId, "draft"=>['$exists' => false ]));
    $has_answered = false;
    
    if(count($answers)!=0){
        $has_answered = true;
    }

    # Get the user's answer Id
    $myAnswer = "";
    foreach ($answers as $key => $value) {
        if($userId == $value["user"] && isset($value['_id']->{'$id'})){
            $myAnswer = $value['_id']->{'$id'};
        }
    }

    if($userId!=""){
        
        $data = PHDB::findByIds("citoyens", [$userId] ,["links.follows", "links.memberOf"]);
        
        $memberOf_ids = array();

        if(isset($data[$userId]["links"]["memberOf"])){
            $links2 = $data[$userId]["links"]["memberOf"];
            foreach ($links2 as $key => $value) {
                array_push($memberOf_ids, $key);
            }
            $organizations = PHDB::findByIds("organizations", $memberOf_ids ,["name", "profilImageUrl"]);
        }
    }
?>
<style type="text/css" id="#css-<?= $kunik ?>">
    .btn<?= $kunik ?> {
       -webkit-transition: 0.5s ease;
       -moz-transition: 0.5s;
       -ms-transition: 0.5s;
       -o-transition: 0.5s;
       transition: 0.5s;
       font-size: clamp(8px, 10px, 12px)
    }

    .btn<?= $kunik ?>.btn-action {
        font-size: 16px; 
        /*border: 2px solid <?= $blockCms["buttonColor"] ?>; */
        background:transparent;
        padding:15px 40px; 
        text-transform:uppercase;
        border-radius:30px;
        color:<?= $blockCms["buttonColor"] ?>;
    }

    .btn<?= $kunik ?>.btn-coform{
        color: white;
        font-weight: bold;
        background:<?= $blockCms["buttonColor"] ?>;
    }

    .btn<?= $kunik ?>.btn-action:hover {
        background:<?= $blockCms["buttonColor"] ?>;
        border-radius:10px;
        /*border: 2px solid <?= $blockCms["buttonColor"] ?>;*/
        color:white;
    }
</style>

<section id="AssoLibre" class="row">
    <div class="text-center col-xs-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        <h1 class=" padding-20 text-center sp-text title-1" data-id="<?= $blockKey ?>" data-field="contentTitle"> 
            <?php echo $blockCms["contentTitle"]; ?>
        </h1>
        <div class="description text-center col-xs-12 sp-text" data-id="<?= $blockKey ?>" data-field="introduction">
            <?php echo "\n".$blockCms["introduction"]; ?>
        </div>

        <?php if(isset($_SESSION["userId"]) || (isset($formParams) && isset($formParams["temporarymembercanreply"]) && ($formParams["temporarymembercanreply"]==true || $formParams["withconfirmation"]==true))){ 
            $myAnswer = PHDB::findOne("answers", array("user"=>$userId, "form"=>$formId, "draft" => ['$exists' => false ]));
            $formString = "";
            if(isset($myAnswer["_id"]) && !empty($myAnswer["_id"]) && $formParams["oneAnswerPerPers"]==true){
                $formString = "#answer.index.id.".$myAnswer["_id"]->{'$id'}.".mode.w";
            }else{
                $formString = "#answer.index.id.new.form.".$formId;
            }
        ?>
        <div class="col-xs-12 margin-top-20">
            <?php if($formId!=""){ ?>
                <a class="btn<?= $kunik ?> btn-coform btn-action btn-yes btn-lg lbh sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $blockCms['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>" href="<?php echo $formString ?>" style="text-decoration : none;"> 
                    <?php echo $blockCms["buttonCoform"]; ?>
                </a>
            <?php } ?>

            <?php if($blockCms["buttonInfos"]!=""){ 

                $urlAction = "href='javascript:void(0);'";
                
                if(filter_var($blockCms["linkInfos"], FILTER_VALIDATE_URL)){
                    $urlAction = 'target="_blank" href="'.$blockCms["linkInfos"].'"';
                } ?>

                &nbsp; &nbsp;
                <a class="btn<?= $kunik ?> btn-action btn-primary btn-lg btn-plusinfo sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $blockCms['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>"  <?php echo $urlAction; ?> style="text-decoration : none;"> 
                    <?php echo $blockCms["buttonInfos"] ?>
                </a>
            <?php } ?>
        </div>
        <?php }else if($formId!=""){ ?>
            <div class="text-center">
                <button class="btn<?= $kunik ?> btn-primary btn-action sp-bg" data-id="<?= $blockKey ?>" data-field="buttonColor" data-value="<?= $blockCms['buttonColor']; ?>" data-sptarget="background" data-kunik="<?= $kunik?>" data-toggle="modal" data-target="#modalLogin" style="padding: .8em;">
                    <i class="fa fa-sign-in" style="margin-right: .6em"></i>
                    <?php echo Yii::t("common","Please Login First") ?>
                </button>
            </div>
        <?php } ?>
    </div>
</section>
<br/>
<br/>

<script type="text/javascript">

    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
    var data_formID = sectionDyf.<?php echo $kunik ?>blockCms.data;
    jQuery(document).ready(function() {
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
            var textWithCoformButtonInput = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type : "inputSimple",
                                options : {
                                    name : "buttonCoform",
                                    label : tradCms.textButtonAccessCoform,
                                    collection : "cms"
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    name : "buttonInfos",
                                    label : tradCms.textButtonMoreInformation,
                                    collection : "cms"
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    name : "linkInfos",
                                    label : tradCms.linkMoreInformation,
                                    collection : "cms"
                                }
                            },
                            {
                                type : "select",
                                options : {
                                    name : "formId",
                                    label : tradCms.forms,
                                    options : $.map( data_formID, function( val ) {
                                        return {
                                            value: val._id.$id,
                                            label: val.name
                                        }
                                    })
                                }
                            }
                        ]
                    },
                    style: {
                        inputsConfig: [
                            {
                                type : "colorPicker",
                                options : {
                                    name : "buttonColor",
                                    label : tradCms.chooseThemeColor,
                                    collection : "cms",
                                    defaultValue : sectionDyf.<?php echo $kunik ?>blockCms.buttonColor
                                }
                            }
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                },
            }
            cmsConstructor.blocks.texteWithCoformButton = textWithCoformButtonInput;
        }
        
    });
    
</script>
