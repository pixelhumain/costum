<?php
	$defaultStyle = [];
	$myCmsId = (string)$blockCms['_id'];
	$blockCms["css"] = array_replace_recursive($defaultStyle, $blockCms["css"] ?? []);
	$styleCss = (object)[$kunik => $blockCms["css"] ?? []];
?>
<style>
	.region-selector ul {
		max-height: 300px;
		overflow-y: auto;
	}

	.region-selector li {
		font-size: 14px;
	}

	.region-selector .dropdown-toggle {
		font-size: 20px;
	}

	.andra-kl {
		display: flex;
		align-items: end;
	}

</style>
<script>
	function tl_regions(W, $, php) {
		var input_dom = $('.region-selector[data-id=' + php.cms_id + ']');
		var default_filter = get_session_data();

		function get_session_data() {
			var session_data = sessionStorage.getItem('coevent-' + php.context.id + php.context.type);
			var filter = session_data ? JSON.parse(session_data) : {
				tags: [],
				regions: [],
				type: null
			};
			return (filter);
		}

		function set_session_data(data) {
			sessionStorage.setItem('coevent-' + php.context.id + php.context.type, JSON.stringify(data));
		}

		function parse_url_to_obj() {
			var output = {};
			var get_params = W.location.hash.indexOf('?');
			if (get_params > 1) {
				var get_string = W.location.hash.substr(get_params + 1);
				var values = get_string.split('&');
				$.each(values, function(i, value) {
					var index_cut = value.indexOf('=');
					var key = value.substr(0, index_cut);
					var val = value.substr(index_cut + 1);
					var is_array = key.search(/\[\]$/) > 1
					if (is_array)
						key = key.substr(0, index_cut - 2);
					if (typeof output[key] === 'undefined' && is_array)
						output[key] = [];
					if (is_array)
						output[key].push(val);
					else
						output[key] = val;
				});
			}
			return (output);
		}

		function parse_obj_to_url(regions) {
			var params = parse_url_to_obj();
			params.regions = regions;
			var hash = location.hash.match(/\#[\w\-]+/);
			if (!hash)
				hash = '';
			else
				hash = hash[0];
			hash += '?'
			var key;
			for (key in params) {
				if (params[key] === null)
					continue;
				if (typeof params[key] === 'string')
					hash += key + '=' + params[key] + '&';
				else
					$.each(params[key], function(i, param) {
						hash += key + '[]=' + param + '&';
					});
			}
			hash = hash.replace(/.$/, '');
			hash = W.location.origin + W.location.pathname + hash;
			return (hash);
		}

		input_dom.on('click', 'a[role=menuitem]', function(e) {
			var self = $(this);
			e.preventDefault();
			var filter = get_session_data();
			var value = self.attr('href');
			var index_of_region = filter.regions.findIndex(function(region) {
				return (region.id === value);
			});
			if (index_of_region >= 0)
				filter.regions.splice(index_of_region, 1);
			else
				filter.regions.push({
					id: value,
					name: self.text()
				});
			set_session_data(filter);
			mylog.log("tlRegion filter", filter);
			W.history.pushState({}, '', parse_obj_to_url(filter.regions.map(function(region) {
				return (region.id);
			})));
			$('.coevent-program').trigger('coevent-filter');
		});
		$('.coevent-program').on('coevent-filter', function() {
			var filter = get_session_data();
			input_dom.find('li.active').removeClass('active');
			$.each(filter.regions, function(i, region) {
				input_dom.find('a[role=menuitem][href="' + region.id + '"]').parent().addClass('active');
			});
		});
		var get_params = parse_url_to_obj();
		if (Object.keys(get_params).includes('regions')) {
			default_filter.regions = Object.keys(php.regions).map(function(id) {
				return ({
					id: id,
					name: php.regions[id].name
				});
			}).filter(function(region) {
				return (get_params.regions.includes(region.id));
			});
			set_session_data(default_filter);
		}
		$.each(default_filter.regions, function(i, region) {
			input_dom.find('a[role=menuitem][href="' + region.id + '"]').parent().addClass('active');
		});
	}
</script>
<div class="dropdown region-selector" data-id="<?= $myCmsId ?>">
	<button class="btn btn-default dropdown-toggle" type="button" id="drop<?= $myCmsId ?>" data-toggle="dropdown">
		<?= Yii::t('common', 'Regions') ?>
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu" aria-labelledby="drop<?= $myCmsId ?>">
		<?php foreach ($regions as $id => $region) : ?>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $id ?>"><?= $region['name'] ?></a></li>
		<?php endforeach; ?>
	</ul>
</div>
<script>
	(function(x, $) {
		var php = {
			cms_id: '<?= $myCmsId ?>',
			context: JSON.parse(JSON.stringify(<?= json_encode($parent) ?>)),
			regions: JSON.parse(JSON.stringify(<?= json_encode($regions) ?>)),
		};
		mylog.log("coevent php data region", php);
		$(function() {
			tl_regions(window, $, php);
		});
	})(window, jQuery);
</script>