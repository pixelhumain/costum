<?php
$myCmsId = $blockCms["_id"]->{'$id'};
$graphAssets = [
    '/plugins/chartJs/chart.js'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );

$typeChart = $blockCms['typeChart'] ?? "barAndLine";

if (isset($defaultData)) {
    $blockCms = array_replace_recursive($defaultData, $blockCms);
}

$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) ['css' => $objectCss];
?>
<div>
  <canvas id="mixedChart<?= $kunik ?>" class="mixedChart <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
  </canvas>
</div>
<style id="mixedChartLine<?= $kunik ?>">
</style>
<script>
  var typeChart = {
        "barAndLine": "Bar et Line",
        "line": "line",
        "bar": "bar",
    };
  let myChartMixed;
  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
  $("#mixedChartLine<?= $kunik ?>").append(str);
  if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var mixedChartLine = {
            configTabs: {
                general: {
                    inputsConfig: [
                      {
                        type: "select",
                        options: {
                            name: "typeChart",
                            label: "Type",
                            options: $.map(typeChart, function(key, val) {
                                return {
                                    value: val,
                                    label: key
                                }
                            })
                        }
                      },
                    ],
                },
                style: {
                    inputsConfig: [
                        "background",
                        "color",
                        "addCommonConfig"
                    ],
                }
            },
            onChange: function(path, valueToSet, name, payload, value) {
              if (name == "typeChart") {
                   createChartMixed(value)
                } 
            },
        }
        cmsConstructor.blocks.mixedChartLine = mixedChartLine;
    }
    createChartMixed("<?= $typeChart ?>");
    function createChartMixed(type = "barAndLine") {
  if (myChartMixed) {
    myChartMixed.destroy();
  }
  const lineChart = document.getElementById('mixedChart<?= $kunik ?>');
  let datasets = [];
  
  if (type === "bar" || type === "barAndLine") {
    datasets.push({
      type: 'bar',
      label: 'title2',
      data: [70, 60, 50, 40, 30, 20],
      borderWidth: 1
    });
  }
  
  if (type === "line" || type === "barAndLine") {
    datasets.push({
      type: 'line',
      label: 'title',
      data: [20, 30, 40, 50, 60, 70],
      borderWidth: 1
    });
  }
  
  myChartMixed = new Chart(lineChart, {
    data: {
      datasets: datasets,
      labels: ['2018', '2019', '2020', '2021', '2022', '2023'],
    },
  });
}

</script>
