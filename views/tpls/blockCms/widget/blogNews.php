<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [ "#".$kunik."-css" => $blockCms["css"] ?? [] ];
?>
<style id="css-<?= $kunik ?>" >
    .content-news<?= $kunik ?> .card-news {
        border-radius: 24px;
        height: 490px;
        margin-bottom: 24px;
        overflow: hidden;
    }
    .content-news<?= $kunik ?> .image-news {
        webkit-clip-path: polygon(calc(100% - 60px) 0, 100% 50%, calc(100% - 60px) 100%, 0 100%, 0 50%, 0 0);
        clip-path: polygon(calc(100% - 60px) 0, 100% 50%, calc(100% - 60px) 100%, 0 100%, 0 50%, 0 0);
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        height: 490px;
    }
    .content-news<?= $kunik ?> .card-news:hover .image-news {
        webkit-clip-path: polygon(100% 0, 100% 50%, 100% 100%, 0 100%, 0 50%, 0 0);
        clip-path: polygon(100% 0, 100% 50%, 100% 100%, 0 100%, 0 50%, 0 0);
        transition: -webkit-clip-path .2s cubic-bezier(.645,.045,.355,1);
        transition: clip-path .2s cubic-bezier(.645,.045,.355,1);
        transition: clip-path .2s cubic-bezier(.645,.045,.355,1), -webkit-clip-path .2s cubic-bezier(.645,.045,.355,1);
    }

    .content-news<?= $kunik ?> .card-news .info-news {
        align-items: flex-start;
        color: #fff;
        display: flex;
        flex: 1;
        flex-direction: column;
        justify-content: flex-end;
        height: 490px;
    }

    .content-news<?= $kunik ?> .card-news .info-news .title-news {
        -webkit-box-orient: vertical;
        display: -webkit-box;
        -webkit-line-clamp: 5;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: normal;
        width: 100%;
        font-size: 25px;
        font-weight: bold;
    }
    @media (max-width: 767px) {
        .content-news<?= $kunik ?> .card-news {
            height: auto;
        }
        .content-news<?= $kunik ?> .image-news {
            webkit-clip-path: polygon(0 0, 100% 0, 100% 75%, 50% 100%, 0 75%);;
            clip-path: polygon(0 0, 100% 0, 100% 75%, 50% 100%, 0 75%);;
            height: 300px;;
        }
        .content-news<?= $kunik ?> .card-news:hover .image-news {
            webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 50% 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% 100%, 50% 100%, 0 100%);
        }
        .content-news<?= $kunik ?> .card-news .info-news {
            height: auto;
        }
        .content-news<?= $kunik ?> .card-news .info-news .title-news {
            font-size: 20px;
            text-align: center;
        }
    }
    .content-news<?= $kunik ?> .card-news .info-news .title-news.simpleTitle {
        font-size: 20px;
    }
    .content-news<?= $kunik ?> .card-news .info-news i{
        font-size: 15px;
    }
    .content-news<?= $kunik ?> .card-news .info-news .number {
        font-size: 14px;
        margin-bottom: 0px;
    }
    .content-news<?= $kunik ?> .card-news  .info-news .vote-count {
        border-left: 1px solid;
        border-right: 1px solid;
    }
    .content-news<?= $kunik ?> .card-news .info-news .comment-count,
    .content-news<?= $kunik ?> .card-news .info-news  .vote-count,
    .content-news<?= $kunik ?> .card-news .info-news  .shared-count{
        margin-top: 15px;
        margin-bottom: 15px;}
    .content-news<?= $kunik ?> .tag-list {
        margin-bottom: 20px;
    }
    .content-news<?= $kunik ?> .card-news  .info-news .btn-tag {
        border: 1px solid;
        border-radius: 6px;
    }
    .content-news<?= $kunik ?> .card-news .news-popover {
        width: 100%;
        background-color: transparent;
        border: 2px solid #fff;
        color: #fff;
        margin-bottom: 10px;
        margin-top: 10px;
    }
    .content-news<?= $kunik ?> #modalNews.in .modal-dialog {
        top: 110px;
    }
    .content-news<?= $kunik ?> #modalNews.modal {
        background-color: rgb(5 5 6 / 80%);
    }
    .content-news<?= $kunik ?> #modalNews .btn-tag {
        color: black;
        border: 1px solid #000000;
        font-size: 13px;
    }
    .content-news<?= $kunik ?> .card-news .div-img {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .content-news<?= $kunik ?> .card-news .div-img i{
        font-size: 150px;
    }

</style>
<div  id="<?= $kunik ?>-css" class="content-news<?= $kunik ?>" >
    <!--<div id="headerSearch" class='headerSearch no-padding col-xs-12'></div>-->
    <div id='filterContainers<?= $kunik ?>' class='searchObjCSS configFilter'></div>
    <div class='bodySearchContainer margin-top-10 '>
        <div class='no-padding col-xs-12 dropdown-search-<?= $kunik ?>' id='dropdown_search'></div>
        <div class='no-padding col-xs-12 text-left footerSearchNewsContainer smartgrid-slide-element'></div>
    </div>
</div>
<script type="text/javascript">
    <?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    mylog.log("ParamsData",<?php echo $kunik ?>ParamsData);


    jQuery(document).ready(function() {
        var styleCss="";
        styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(styleCss);
        var imgKey = {},
            documentList = {};
            colorList = ["#7468ff","#e86776","#00997c","#d34341","#9d1453","#436b5a","#3cb9d2","#001b3b","#879da3","#07c17f"];
        if (typeof costum.css.color == "object") {
            colorList = $.map(costum.css.color, function(value, key) {
                return value;
            })
        }
        var colorIndex = 0;

        directory.newsPanelFullWidth = function(params){
            var bgColor = colorList[colorIndex];
            var imgContain  = "";
            colorIndex = (colorIndex + 1) % colorList.length;
            var imgBgBloc = "";
            if (typeof params.mediaImg != 'undefined' && typeof params.mediaImg["images"] != 'undefined' && typeof params.listImage != 'undefined'){
                //imgBgBloc = getImageDocument(costum.contextId,costum.contextType,params.mediaImg["images"][0])

                //imgKey[params._id.$id] = params.mediaImg["images"];
                var firstKey = Object.keys(params.listImage)[0];
                imgBgBloc = 'background-image: url(/upload/communecter/'+params.listImage[firstKey].folder+'/'+params.listImage[firstKey].name+')';

            }else if (typeof params.media != 'undefined' && typeof params.media["content"] != 'undefined'  && typeof params.media["content"]["image"] != 'undefined' ) {
                imgBgBloc = 'background-image: url('+baseUrl+params.media["content"]["image"]+')';
            }
            else {
                imgBgBloc = "background-color: #ffffff";
                imgContain = "<div class='div-img'><i class='fa fa-image fa-2x' style='color: "+bgColor+"'></i></div>";
            }

            const textAndCreated = JSON.stringify({
                text: params.text,
                created: params.created,
                tagsHtml : params.tagsHtml,
                markdownActive : params.markdownActive
            }).replace(/"/g, '&quot;');
            var str = `
                <div class="swiper-slide">
                    <div class="card-news commonConfigCard col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 no-padding " style="background-color: ${bgColor};border : 1px solid ${bgColor}">
                        <div id="${params._id.$id}" class="image-news col-md-8 col-sm-6 col-xs-12" style="${imgBgBloc};">${imgContain}</div>
                        <div class="info-news col-md-4 col-sm-6 col-xs-12">
                            <!--<h2>SNCF - Questions de voyageurs</h2>-->
                            <div class="date-post">${directory.showDatetimePost(null, null, params.created["sec"])}</div>
                            <div class="title-news"> ${params.text}</div>
                            <a class="btn btn-default news-popover ${(params["text"].length < 90 ? 'hide' : '')}" onclick="modalNews(${textAndCreated})" href="javascript:void(0);"> Lire la suite</a>
                            <div class="" style="width: 100%">
                                <div  class="col-xs-4 padding-5 text-center comment-count">
                                    <i class="fa fa-comment"></i>
                                    <h4 class="number">${(typeof params.commentCount != "undefined") ? params.commentCount : "0"}</h4>
                                </div>
                                <div  class="col-xs-4 padding-5 text-center vote-count">
                                    <i class="fa fa-heart"></i>
                                    <h4 class="number">${(typeof params.vote == 'object') ? Object.keys(params.vote).length : "0"}</h4>
                                </div>
                                <div  class="col-xs-4 padding-5 text-center shared-count">
                                    <i class="fa fa-share"></i>
                                    <h4 class="number">${(typeof params.sharedBy == 'object') ? Object.keys(params.sharedBy).length : "0"}</h4>
                                </div>`;
                                if (typeof params.tagsHtml != "undefined")
                                    str += '<div class="tag-list">' + params.tagsHtml + '</div>';

             str+=          `
                            </div>
                        </div>
                    </div>
                </div>
            `;




            return str;
        }

        var viewFilter = {
            "text" : "searchBar",
            "dropdownList" : "dropdownList",
            "buttonList" : "buttonList",
            "horizontalList" : "horizontalList",
            "accordionList" : "accordionList",
            "megaMenuAccordion" : "megaMenuAccordion",
        }
        var typesEventFilter = {
            "text" : "text",
            "tags" : "tags",
            "filters" : "filters",
            /*"exists" : "exists",*/
            /*"selectList" : "selectList",*/
           /* "sortBy" : "sortBy"*/
        }




        if (costum.editMode){
            var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [])?>;
            swiperObj.inputsOnEdit(paramsDataSwiper);
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
            elementInput = {
                configTabs : {
                    general : {
                        inputsConfig : [
                            {
                                type: "section",
                                options: {
                                    name: "query",
                                    label: "<?= Yii::t("cms", "Query") ?>",
                                    showInDefault: true,
                                    inputs: [
                                        /*{
                                            type: "selectMultiple",
                                            options: {
                                                name: "types",
                                                label: "<?= Yii::t("cms", "Type of element") ?>",
                                                options: $.map(elTypeOptions, function (key, val) {
                                                    return {value: val, label: key}
                                                })
                                            },
                                        },*/
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "indexStep",
                                                label: "<?= Yii::t("cms", "Number of results displayed") ?>",
                                            },
                                        },

                                    ]
                                }
                            },

                            {
                                type: "section",
                                options: {
                                    name: "filtersPersonal",
                                    label: tradCms.customFilter,
                                    showInDefault: true,
                                    payload: {
                                        path: "filters"
                                    },
                                    inputs: [
                                        {
                                            type: "inputMultiple",
                                            options: {
                                                name: "filterPersonal",
                                                label: "",
                                                class: "multiple-input-dark",
                                                single : true,
                                                inputs: [
                                                    {
                                                        type: "inputSimple",
                                                        options: {
                                                            label: tradCms.label,
                                                            name: "label",
                                                        },
                                                    },
                                                    {
                                                        type: "inputSimple",
                                                        options: {
                                                            label : tradCms.targetField,
                                                            name: "field",
                                                        },
                                                    },
                                                    {
                                                        type: "select",
                                                        options: {
                                                            label: tradCms.view,
                                                            name: "view",
                                                            options : $.map( viewFilter, function( key, val ) {
                                                                return {value: val,label: key}
                                                            })
                                                        },
                                                    },
                                                    {
                                                        type: "select",
                                                        options: {
                                                            label: trad.events,
                                                            name: "events",
                                                            options : $.map( typesEventFilter, function( key, val ) {
                                                                return {value: val,label: key}
                                                            })
                                                        },
                                                    },
                                                    {
                                                        type: "select",
                                                        options: {
                                                            label: tradCms.enterDataOrUseExisting,
                                                            name: "lists",
                                                            class: "listsData hide",
                                                            options : [
                                                                {
                                                                    value: "manuel",
                                                                    label: tradCms.enterData,
                                                                },
                                                                {
                                                                    value: "keyValue",
                                                                    label: "Clé valuer",
                                                                }
                                                            ]
                                                        }
                                                    },
                                                    {
                                                        type: "inputWithSelect",
                                                        options: {
                                                            name: "listTags",
                                                            label: trad.List,
                                                            class: "hide blogList"
                                                        }
                                                    },
                                                    {
                                                        type: "inputMultiple",
                                                        options: {
                                                            name: "listObject",
                                                            label: "Entrer des key valeur",
                                                            class: "hide",
                                                            inputs: [
                                                                {
                                                                    type: "inputSimple",
                                                                    options: {
                                                                        name: "objectkey",
                                                                        label: "Clé"
                                                                    }
                                                                },
                                                                {
                                                                    type: "inputSimple",
                                                                    options: {
                                                                        name: "objectvalue",
                                                                        label: "valeur"
                                                                    }
                                                                }
                                                            ],
                                                        },
                                                    },
                                                ],
                                                defaultValue: cmsConstructor.sp_params["<?= $myCmsId ?>"].filters && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal ? cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal : []
                                            }
                                        }
                                    ]

                                }
                            },



                        ]
                    },
                    /*configCard : {
                        key: "configCard",
                        keyPath : "css.commonConfigCard",
                        icon: "list-alt",
                        label: "<?= Yii::t("cms", "Card") ?>",
                        content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"configCard"),
                        inputsConfig : [
                            "addCommonConfig",
                        ],
                    },*/
                    hover: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    },
                    advanced: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    }
                },
                beforeLoad: function(){
                    // hide some input if not needed
                    if (typeof initInterval == "undefined" || initInterval == null) {
                        initInterval = setInterval(() => {
                            if ($('.section-filtersPersonal [name="events"]').length > 0) {
                                if (cmsConstructor.sp_params["<?= $myCmsId ?>"] && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters) {
                                    if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal) {
                                        $($('.section-filtersPersonal [name="events"]')).each((index, elem) => {
                                            if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index] && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].events && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].events != "text") {
                                                $(".listsData").removeClass("hide")
                                                if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists) {
                                                    if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "manuel") {
                                                        $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").removeClass("hide")
                                                    }
                                                    if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "keyValue") {
                                                        $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").removeClass("hide")
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                                clearInterval(initInterval);
                                initInterval = null;
                            }
                        }, 300);
                    }
                },
                onchange: function(path, valueToSet, name, payload, value) {
                    if (path == "filters.filtersPersonal" && name == "filterPersonal") {
                        $($('.section-filtersPersonal [name="events"]')).each((index, elem) => {
                            if (value[index].events && value[index].events != "text") {
                                $(".listsData").removeClass("hide")
                                if (value[index].lists) {
                                    if (value[index].lists == "manuel") {
                                        $(".blogList").removeClass("hide");
                                    } else {
                                        $(".blogList").addClass("hide")
                                    }
                                    if (value[index].lists == "keyValue") {
                                        $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").removeClass("hide")
                                    } else {
                                        $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").addClass("hide")
                                    }
                                }
                            } else if (value[index].events && value[index].events == "text") {
                                $(".listsData").addClass("hide")
                                $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").addClass("hide")
                                $(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").addClass("hide")
                            }
                        })
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            };
            cmsConstructor.blocks.blogNews =  elementInput;
        }
        /*setTimeout(function() {
            var listid = [].concat(...Object.values(imgKey));
            getImageDocument(listid);

        },1500);
        function getImageDocument (ids){
            var newsImage="";
            ajaxPost(
                null,
                baseUrl+"/costum/tierslieuxgenerique/getbyids",
                {ids:ids , collection:"documents",fields:["folder","name"]},
                function(data){
                    if(data.results){
                        $.each(imgKey, (key, value) => {
                            let doc = data.results[value[0]]
                            $("#"+key).css("background-image", "url('/upload/communecter/"+doc["folder"]+"/"+doc["name"]+"')");
                        });
                        documentList = data.results;
                    }
                }
            )

        }*/


    });

    function modalNews (params) {
        $("#modalNewsContainer").remove();
        $('.content-news<?= $kunik ?>').append('<div id="modalNewsContainer"></div>');

        var titleHtml = params.text,
            classTitle = "simpleTitle";
        if(typeof params.markdownActive!= 'undefined' && params.markdownActive){
            titleHtml = dataHelper.markdownToHtml(params.text);
            classTitle = "markdownActive";
        }
        var str = `<div tabindex="-1" class="modal fade" id="modalNews" role="dialog">
                       <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                             <div class="modal-header">
                                <button class="close" aria-hidden="true" type="button" data-dismiss="modal">×</button>
                                <!--<h3>News Preview</h3>-->
                             </div>
                             <div class="modal-body">
                                <div class="text ${classTitle}">${titleHtml}</div>
                             </div>`;
                             if (typeof params.tagsHtml != "undefined") {
                                  str += ` <div class="modal-footer">${params.tagsHtml}</div>`;
                             }
         str += `         </div>
                       </div>
                    </div>`;

        $('#modalNewsContainer').html(str);
        $("#modalNews").modal();
    }

        configSearchObj.query(<?php echo $kunik ?>ParamsData,null,"<?= $kunik ?>");




    </script>
