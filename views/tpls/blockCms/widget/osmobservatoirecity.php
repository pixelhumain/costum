<?php
$keyTpl = "";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];


?>

<style id="style-obs-osm-city" type="text/css">
    .container<?= $kunik ?>-css {
        height: max-content;
        width: 45%;
        border-bottom-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-top-left-radius: 50%;
        border-top-right-radius: 50%;
        justify-content: center;
        align-content: center;
        padding-top: 10%;
        padding-left: 0;
        padding-right: 0;
        padding-bottom: 10%;
    }
</style>

<div class="<?= $kunik ?>">


    <div class="col-xs-12 no-padding container-osm-observatoire-city">

        <div id="container-value-city-<?= $kunik ?>">
            <div class="col-lg-6 col-md-4">
                <div class="cmsbuilder-block cmsbuilder-block-droppable  container<?= $kunik ?>-css cmsbuilder-block-container whole-container<?= $kunik ?> spCustomBtn sp-cms-std  std container<?= $kunik ?> super-cms  other-css-container<?= $kunik ?> text-left 650ab925b91e9d71764220be" style="order: 0;" data-kunik="container<?= $kunik ?>" data-id="650ab925b91e9d71764220be" data-noteditable="" data-blocktype="column" data-name="Colonne" data-bloc-name="Container-650ab925b91e9d71764220be" data-anchor-target="">

                    <div class="cmsbuilder-block stop-propagation col-md-12 col-xs-12 no-padding sp-elt-<?= $kunik ?> super-cms" data-blocktype="custom" data-kunik="textWithValue<?= $kunik ?>" data-id="<?= $kunik ?>">
                        <div class="textWithValue<?= $kunik ?>">
                            <div class="chartviz">
                                <div class="text-center">
                                    <span class="padding-10 padding-top-40" style="background:transparent">
                                        <span class="textWithValuetextWithValue<?= $kunik ?> count-ville textValue margin-top-10 titre-osm-observatoire-city">70%</span>
                                    </span>
                                </div>
                            </div>
                            <table id="tableDatatextWithValue<?= $kunik ?>" class="table table-striped table-bordered tableviz margin-top-10" style="display: none;">
                                <tbody>
                                    <tr>
                                        <td>Valeur :</td>
                                        <td class="textWithValuetextWithValue<?= $kunik ?>"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--  Required for old block inside a container -->
                    <a href="javascript:;" style="display:none;" class="edittextWithValue<?= $kunik ?>Params" ,="" data-path="tpls.blockCms.graph.textWithValue" data-id="<?= $kunik ?>" data-collection="cms">
                    </a>

                    <div class="cmsbuilder-block super-cms default-text-params  text<?= $kunik ?> other-css-text<?= $kunik ?> text<?= $kunik ?>-css" data-blocktype="text" data-kunik="text<?= $kunik ?>" data-name="text" data-id="<?= $kunik ?>">
                        <div lang="de" id="sp-<?= $kunik ?>" style="cursor: default; hyphens: auto;" class="editable sp-text sp-text-<?= $kunik ?>" data-id="<?= $kunik ?>" data-field="text" data-kunik="text<?= $kunik ?>" data-lang="fr">
                            <div style="text-align:center;">
                                <font class="titre-osm-observatoire-city">Au total</font>
                            </div>
                        </div>
                        <div class="markdown-container hidden" data-id="<?= $kunik ?>">
                            <div class="sp-markdown-<?= $kunik ?> markdown"></div>
                        </div>
                    </div>
                    <script type="text/javascript">

                    </script>
                    <!--  Required for old block inside a container -->
                    <a href="javascript:;" style="display:none;" class="editsupertext<?= $kunik ?>Params" ,="" data-path="tpls.blockCms.superCms.elements.supertext" data-id="<?= $kunik ?>" data-collection="cms">
                    </a>
                    <script>
                    </script>
                </div>
            </div>
            <?php if ($blockCms != null && isset($blockCms["location"]) && $blockCms["location"] != null) {
                for ($i = 0; $i < count($blockCms["location"]); $i++) {
                    if (isset($blockCms["location"][$i])) { ?>
                        <div class="col-lg-6 col-md-4">
                            <div class="cmsbuilder-block cmsbuilder-block-droppable  container<?= $kunik ?>-css cmsbuilder-block-container whole-container<?= $kunik ?> spCustomBtn sp-cms-std  std container<?= $kunik ?> super-cms  other-css-container<?= $kunik ?> text-left 650ab925b91e9d71764220be" style="order: 0;" data-kunik="container<?= $kunik ?>" data-id="650ab925b91e9d71764220be" data-noteditable="" data-blocktype="column" data-name="Colonne" data-bloc-name="Container-650ab925b91e9d71764220be" data-anchor-target="">

                                <div class="cmsbuilder-block stop-propagation col-md-12 col-xs-12 no-padding sp-elt-<?= $kunik ?> super-cms" data-blocktype="custom" data-kunik="textWithValue<?= $kunik ?>" data-id="<?= $kunik ?>">
                                    <div class="textWithValue<?= $kunik ?>">
                                        <div class="chartviz">
                                            <div class="text-center">
                                                <span class="padding-10 padding-top-40" style="background:transparent">
                                                    <span class="textWithValuetextWithValue<?= $kunik ?> count-ville textValue margin-top-10 titre-osm-observatoire-city" >70%</span>
                                                </span>
                                            </div>
                                        </div>
                                        <table id="tableDatatextWithValue<?= $kunik ?>" class="table table-striped table-bordered tableviz margin-top-10" style="display: none;">
                                            <tbody>
                                                <tr>
                                                    <td>Valeur :</td>
                                                    <td class="textWithValuetextWithValue<?= $kunik ?>"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--  Required for old block inside a container -->
                                <a href="javascript:;" style="display:none;" class="edittextWithValue<?= $kunik ?>Params" ,="" data-path="tpls.blockCms.graph.textWithValue" data-id="<?= $kunik ?>" data-collection="cms">
                                </a>

                                <div class="cmsbuilder-block super-cms default-text-params  text<?= $kunik ?> other-css-text<?= $kunik ?> text<?= $kunik ?>-css" data-blocktype="text" data-kunik="text<?= $kunik ?>" data-name="text" data-id="<?= $kunik ?>">
                                    <div lang="de" id="sp-<?= $kunik ?>" style="cursor: default; hyphens: auto;" class="editable sp-text sp-text-<?= $kunik ?>" data-id="<?= $kunik ?>" data-field="text" data-kunik="text<?= $kunik ?>" data-lang="fr">
                                        <div style="text-align:center;">
                                            <font class="titre-osm-observatoire-city">Ville de <?= $blockCms["location"][$i] ?></font>
                                        </div>
                                    </div>
                                    <div class="markdown-container hidden" data-id="<?= $kunik ?>">
                                        <div class="sp-markdown-<?= $kunik ?> markdown"></div>
                                    </div>
                                </div>
                                <script type="text/javascript">

                                </script>
                                <a href="javascript:;" style="display:none;" class="editsupertext<?= $kunik ?>Params" ,="" data-path="tpls.blockCms.superCms.elements.supertext" data-id="<?= $kunik ?>" data-collection="cms">
                                </a>
                                <script>
                                </script>
                            </div>
                        </div>
            <?php }
                }
            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var type = [];
    var locationValue = [];
    var allData;
    var dataElts = [];
    var madatlasContribution = [];
    var blockCms = {};

    function refreshMadatasContribution() {
        var tmp = [];
        let allcount = 0;
        let contributionCount = 0;
        if (allData != null && typeof allData.element != "undefined" && allData.element != null) {

            $.each(allData.element, function(key, data) {
                allcount++;
                if (data != null && typeof data.madatlasContribution != "undefined" && data.madatlasContribution) {
                    tmp.push(data);
                    contributionCount++;
                }
            });
        }

        if (allcount > 0) {
            let contributionPourcent = (contributionCount * 100) / allcount;
            contributionPourcent = contributionPourcent.toFixed(1);
            $.each($(".count-ville"), function(key, val) {
                $(val).html(contributionPourcent + " %");
            });
        }

        madatlasContribution = tmp;
    }

    function refreshListe() {
        refreshMadatasContribution();
    }

    function getData(changeFiltreCity = false) {
        lieuToSend = locationValue;
        var params = {
            location: lieuToSend,
            types: typeDataToGet,
            osmAllTYpe: osmAllTYpe,
            idBlock: "<?= $myCmsId ?>",
            selectedElement: <?= json_encode($blockCms["selectedElement"]) ?>,
            changeFiltreCity: changeFiltreCity,
        };

        ajaxPost(
            null,
            baseUrl + '/api/convert/osm',
            params,
            function(data) {
                /* if (data != null && typeof data.element != "undefined") {
                    if(data != null && typeof data.element != "undefined" && data.element.length > 0) {
                        allData = data;
                        refreshListe();
                    }
                }

                */
            }, {
                async: false
            }
        );
    }


    function getNameOfType(type, getType) {
        var str = "";
        $.each(getType, function(key, val) {
            if (val != null && typeof val.osmType != "undefined" && val.osmType == type && typeof val.name != "undefined")
                str = val.name;
        });

        return str;
    }

    function getChildFormInFilter(parent, block = []) {
        var res = [];
        let i = 0;
        if (parent != null) {
            $.each(parent, function(key, child) {
                elementChild = [];

                if (child != null && typeof child.name != "undefined" && typeof child.element != "undefined") {
                    let params = {};
                    var show = false;
                    if (block != null && typeof block[key] != "undefined") {

                        params = block[key];
                        show = true;
                    } else if (block != null && typeof block["typeTag"] != "undefined" && block["typeTag"] != null && typeof block["typeTag"][key] != "undefined") {
                        params = block["typeTag"][key];
                        show = true;
                    }

                    elementChild = getChildFormInFilter(child.element, params);
                    res[i] = {
                        type: "section",
                        options: {
                            label: child.name,
                            name: key,
                            showInDefault: show,
                            inputs: elementChild
                        },
                    };
                } else if (child != null && typeof child.name != "undefined" && typeof child.type != "undefined" && typeof child.tag != "undefined") {
                    var defaultValue = false;
                    if (block != null && typeof block["allType"] != "undefined" && (block["allType"] == "true" || block["allType"] == true))
                        defaultValue = true;
                    else if (block != null && typeof block[key] != "undefined")
                        defaultValue = block[key];

                    if (defaultValue == "true")
                        defaultValue = true;

                    if (i == 0) {
                        res[i] = {
                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: "allType",
                                label: "Tous selectionne",
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };

                    } else {
                        res[i] = {

                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: key,
                                label: child.name,
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };
                    }

                }

                i++;
            });
        }

        return res;
    }

    function filters() {
        var option = {};
        $.each(<?= json_encode($blockCms["selectedElement"]) ?>, function(kk, vv) {
            if (typeof osmAllTYpe[vv] != "undefined")
                option[vv] = osmAllTYpe[vv];
        });

        return getChildFormInFilter(option, <?= json_encode($blockCms) ?>);
    }

    function getOptionTagElementTypeFilter() {
        var option = [];
        $.each(osmAllTYpe, function(kk, vv) {
            if (vv != null && typeof vv.name != "undefined" && vv.element != "undefined")
                option.push({
                    value: kk,
                    label: vv.name
                });
        });
        return option;
    }

    function getChildType(child, res = {
        type: [],
        nameT: []
    }) {
        $.each(child, function(key, type) {
            if (type != null && typeof type.element != "undefined")
                getChildType(type.element, res);
            else if (type != null && typeof type.type != "undefined") {
                res.type.push(type.type);
                res.nameT.push(type.name);
            }
        });
    }

    function getTypeChildOfThematique(thematique) {
        var res = {
            name: "",
            type: [],
            nameT: [],
        };
        let i = 0;
        if (thematique != null && thematique != "") {
            $.each(osmAllTYpe, function(key, type) {
                if (key == thematique && type != null && typeof type.name != "undefined") {
                    res["name"] = type.name;
                    getChildType(type.element, res);
                }
            });
        }

        return res;
    }

    jQuery(document).ready(function() {
        blockCms = <?= json_encode($blockCms) ?>;
        if (blockCms != null) {
            if (typeof blockCms.location != "undefined")
                locationValue = blockCms.location;
            if (typeof blockCms.typeTag != "undefined")
                typeDataToGet = blockCms.typeTag;
            if (typeof blockCms.elts != "undefined") {
                allData = blockCms.elts;
                dataElts = blockCms.elts;
            }

        }
        refreshListe();
        var coleurPrimairy = "#00a041";

        if(blockCms != null && typeof blockCms.css != "undefined" && blockCms.css != null && typeof blockCms.css["color-principal-city"] != "undefined" && blockCms.css["color-principal-city"] != null && typeof blockCms.css["color-principal-city"].color != "undefined" && blockCms.css["color-principal-city"].color != "")
            coleurPrimairy = blockCms.css["color-principal-city"].color;

        $(".container<?= $kunik ?>-css").css("background-color", coleurPrimairy);
        //getData();

    });
</script>

<script type="text/javascript">
    str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#style-obs-osm-city").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;

        osmobservatoire = {
            configTabs: {
                general: {
                    inputsConfig: [{
                            type: "section",
                            options: {
                                name: "container-osm-observatoire-city",
                                label: "container",
                                showInDefault: true,
                                inputs: [
                                    "width",
                                    "height",
                                    "background",
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "color-principal-city",
                                label: "Couleur Principal",
                                showInDefault: true,
                                inputs: [
                                    "color"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "titre-osm-observatoire-city",
                                label: "Titre",
                                showInDefault: true,
                                inputs: [
                                    "color",
                                    "fontSize",
                                    "fontFamily",
                                    "fontStyle",
                                    "textTransform",
                                    "fontWeight",
                                    "textAlign",
                                ]
                            }
                        },
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig",
                    ],
                },
                hover: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                }
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        };
        cmsConstructor.blocks.<?= $kunik ?> = osmobservatoire;
    }
</script>