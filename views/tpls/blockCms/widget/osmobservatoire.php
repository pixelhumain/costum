<?php
$keyTpl = "";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>

<style id="style-obs-osm<?= $kunik ?>" type="text/css">
    @media (max-width: 800px) {
        .whole-container<?= $kunik ?> {
            height: auto !important;
        }
    }

    .container<?= $kunik ?>-css {
        width: auto;
        justify-content: center;
        align-content: center;
        padding-left: 0;
        padding-right: 0;
        padding-bottom: 8%;
    }

    @media (min-width : 768px) and (max-width: 991px) {
        .container<?= $kunik ?>-css {
            width: 100%;
            background-size: contain;
            margin-left: auto;
            margin-right: auto;
        }
    }

    @media screen and (max-width: 767px) {
        .container<?= $kunik ?>-css {
            width: 100%;
            background-size: contain;
            margin-left: auto;
            margin-right: auto;
        }
    }

    @media (max-width: 800px) {
        .whole-container<?= $kunik ?> {
            height: auto !important;
        }
    }

    .container<?= $kunik ?>-css {
        width: 55%;
    }

    @media (min-width : 768px) and (max-width: 991px) {
        .container<?= $kunik ?>-css {
            width: 100%;
            background-size: contain;
            margin-left: auto;
            margin-right: auto;
        }
    }

    @media screen and (max-width: 767px) {
        .container<?= $kunik ?>-css {
            width: 100%;
            background-size: contain;
            margin-left: auto;
            margin-right: auto;
        }
    }

    .other-css-text<?= $kunik ?> {
        line-height: 0.9;
    }

    .<?= $kunik ?> {
        background-image: url("<?= Yii::app()->getModule("costum")->assetsUrl ?>/images/madatlas/bg-madatlas1.png");
    }

    .main-container-osm-observatoire {
        margin-top: 5%;
    }

    .border-green {
        border: 1px solid #9fbd38;
    }

    .effectif .row {
        padding: 1em 0.2em;
        border: 2px solid #ddd
            /*#002861*/
        ;
        border-radius: 2px;
        margin: 2px;
        /*background: #ddd;*/
    }


    .dash-icon {
        font-size: 2em;
        border-radius: 50%;
        padding: 0.8em;
        margin-top: 0.3em;
        margin-bottom: auto;
        background: #eee;
    }

    .text-theme {
        color: #4E54C9;
    }

    .container-filter-search-osm-name {
        margin-bottom: 25px;
    }

    .dropdown-osm-filter-observatoire {
        margin-bottom: 5px;
        margin-top: 5px;
        margin-right: 10px;
        padding-top: 0px;
        padding-bottom: 0px;
        display: inline-block;
        float: left;
    }

    .container-osm-filter .container-filter-search-osm-name .input-group-addon {
        background-color: #9fbd38 !important;
    }

    .dropdown-osm-filter-observatoire .btn-menu,
    .container-osm-filter .search-bar {
        border: 1px solid #9fbd38 !important;
    }

    .dropdown-osm-filter-observatoire .btn-menu {
        padding: 10px 15px;
        font-size: 16px;
        border: 1px solid #6b6b6b;
        color: #6b6b6b;
        top: 0px;
        position: relative;
        border-radius: 20px;
        text-decoration: none;
        background: white;
        line-height: 20px;
        display: inline-block;
        margin-left: 5px;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu {
        position: absolute;
        overflow-y: visible !important;
        top: 50px;
        left: -55px;
        width: 280px;
        border-radius: 2px;
        border: 1px solid #6b6b6b;
        padding: 0px;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu .list-filters {
        max-height: 300px;
        overflow-y: scroll;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu {
        font-size: 14px;
        text-align: left;
        list-style: none;
    }

    .container-osm-filter .dropdown-menu .list-filters button:hover {
        background-color: #e8e8e8;
    }

    .osm-filter-switch-container .switch-text-label {
        min-width: 85% !important;
        max-width: 85%;
    }

    .wborder {
        border: 2px solid #ddd
            /*#002861*/
        ;
        border-radius: 0px;
    }

    h1, h2, h3, h4, h5, h6
    .titre-osm-observatoire, span, .percent{
        font-family:'Nunito-ExtraBold' !important
    }
</style>

<div class="<?= $kunik ?>">
    

    <div id="all-block-container-<?= $kunik ?>" class="col-xs-12 no-padding"> 
        <div  style="font-family:'Nunito-ExtraBold'; padding: 0em 10em">
            <div>
                <div style="text-align:center;font-size:48pt;">
                    <div class="titre-osm-observatoire">
                        <font class="coleur-principal titre-osm-observatoire">O</font><font class="couleur-secondaire titre-osm-observatoire">B<font class="coleur-principal titre-osm-observatoire">S</font>ERVATOIRE</font>
                        <br />DE 
                        <font class="coleur-principal titre-osm-observatoire">M</font>ADATLAS</font>
                    </div>
                    <div class="description-osm-observatoire">
                        <font  class="color-secondary description-osm-observatoire">CARTOGRAPHIE</font>
                    </div>
                </div>
            </div>
            
            <div>
                <div id="sp-<?= $kunik ?>" style="cursor: default; hyphens: auto;" class="padding-right-35 padding-left-35 editable sp-text sp-text-<?= $kunik ?>" data-id="<?= $kunik ?>" data-field="text" data-kunik="text<?= $kunik ?>" data-lang="fr">
                    <p  class="couleur-secondaire description-osm-observatoire">Découvrez l'observatoire, issu du contribution de MadAtlas sur Open Street Map, dans la ville de <?php  if($blockCms != null && isset($blockCms["location"]) && $blockCms["location"] != null) { $separator = ""; for($i = 0; $i < count($blockCms["location"]); $i++) { if(isset($blockCms["location"][$i])) {echo $separator.$blockCms["location"][$i]; $separator = ", "; } } }?>. Pour le(s) thémathique(s) <span id="list-thematique"></span>. Une démarche collective réalisée pour Produire une information cartographique de qualité et pour Promovoir l’équité sociale et de genre.</p>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    var type = [];
    var locationValue = [];
    var allData;
    var dataElts = [];
    var madatlasContribution = [];
    var blockCms = {};

    jQuery(document).ready(function() {
        blockCms = <?= json_encode($blockCms) ?>;
        if (blockCms != null) {
            if (typeof blockCms.location != "undefined")
                locationValue = blockCms.location;
            if (typeof blockCms.typeTag != "undefined")
                typeDataToGet = blockCms.typeTag;
            if (typeof blockCms.elts != "undefined") {
                allData = blockCms.elts;
                dataElts = blockCms.elts;
            }
                
        }
    });
</script>


<script type="text/javascript">

  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#style-obs-osm<?= $kunik ?>").append(str);
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;

    osmobservatoire = {
      configTabs: {
        general: {
          inputsConfig: [
            "width",
            "height",
            "background",
            {
              type: "section",
              options: {
                name: "coleur-principal",
                label: "Couleur Principale",
                showInDefault: true,
                inputs: [
                  "color",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "couleur-secondaire",
                label: "Couleur Secondaire",
                showInDefault: true,
                inputs: [
                  "color",
                ]
              }
            },
            {
				type: "section",
				options: {
                name: "titre-osm-observatoire",
                label: "Titre",
                showInDefault: true,
                inputs: [
					"background",
					"fontSize",
					"fontFamily",
					"fontStyle",
					"textTransform",
					"fontWeight",
					"textAlign",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "description-osm-observatoire",
                label: "Description",
                showInDefault: true,
                inputs: [
					"color",
					"background",
					"fontSize",
					"fontFamily",
					"fontStyle",
					"textTransform",
					"fontWeight",
					"textAlign",
                ]
              }
            },
          ]
        },
        style: {
          inputsConfig: [
            "addCommonConfig",
          ],
        },
        hover: {
          inputsConfig: [
            "addCommonConfig"
          ],
        },
        advanced: {
          inputsConfig: [
            "addCommonConfig"
          ],
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    };
    cmsConstructor.blocks.<?= $kunik ?> = osmobservatoire;
  }
</script>