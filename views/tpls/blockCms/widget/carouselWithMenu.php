<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$blockChildren = (isset($blockCms["blockChildren"])) ? $blockCms["blockChildren"] : [];
$container = "content-".$kunik." .swiper-container";
$styleCss      = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
//$styleMenuActive = (object) [ $kunik."-menu-css.active" => $blockCms["css"][$kunik."-menu-css"]["activeMenu"] ?? [] ];
?>

<style id="css-<?= $kunik ?>">

    .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .carousel-caption {
        left:0;
        right:0;
        bottom:0;
        text-align:left;
        padding:10px;
        background:rgba(0,0,0,0.6);
        text-shadow:none;
    }

    .container-<?= $kunik ?> {
        min-height: 200px;
    }

    .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .list-group {
        position:absolute;
        top:0;
        left:0;
    }
    /*.container-<?//= $kunik ?> #carouselMenu<?= $kunik ?> .list-group-item {
        border-radius:0px;
        cursor:pointer;
        background-color: <?//=$blockCms["bgMenu"]?>;
        color: <?//= $blockCms["colorMenu"]?>;
    }*/
    .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .list-group-item h4{
        text-transform: none;
    }
   /* .container-<?//= $kunik ?> #carouselMenu<?= $kunik ?> .list-group .active {
        background-color:#eee;
    }*/

    @media (min-width: 992px) {
        .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> {padding-left:33.3333%;}
        .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .carousel-controls {display:none;}
    }
    @media (max-width: 991px) {
        .container-<?= $kunik ?> .carousel-caption p,
        .container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .list-group {display:none;}
    }

    .navigation-input-form .btn-remove-input-content {
        margin-top: -15px;
    }
</style>


<div class="container-<?= $kunik ?> <?= $kunik ?> <?= $kunik ?>-css">
    <div id="carouselMenu<?= $kunik ?>" class="carousel slide" data-ride="carousel">
        <ul class="list-group col-sm-4">
        <?php
            if (!empty($blockCms) && isset($blockCms["carouselData"])) { 
                foreach ($blockCms["carouselData"] as $key => $carousel) { ?>
                    <li data-target="#carouselMenu<?= $kunik ?>" data-slide-to="<?= $key ?>" class="<?= $kunik ?>-menu-css list-group-item<?= $key === 0 ? " activeMenu" : "" ?>"><h4><?= $carousel["title"] ?></h4></li>
        <?php        
                }
        } ?>
        </ul>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php
                if (!empty($blockChildren)) {    
                    $i = 0;
                    foreach ($blockChildren as $key => $value) {
                        $pathExplode = explode('.', $value["path"]);
                        $count = count($pathExplode);
                        $superKunik = $pathExplode[$count-1].$value["_id"];
                        $blockKey = (string)$value["_id"];
                        $cmsElement = $value["path"];       
                        $params = [
                            "blockCms"  =>  $value,
                            "kunik"     =>  $superKunik,
                            "blockKey"  =>  $blockKey,
                            "content"   =>  array(),
                            "el"        =>  @$el,
                            "costum"    =>  $costum,
                            "page"      =>  $page
                        ];
                        ?>
                        <div class="item<?= $i === 0 ? " active" : "" ?>">
                            <div style="width: 100%; min-height: 400px">
                                <?= $this->renderPartial("costum.views.".$cmsElement,$params); ?>
                            </div>
                        </div>
                <?php 
                    $i++;
                }
            }
            ?>
        </div>
        <!-- Controls -->
        <div class="carousel-controls">
            <a class="left carousel-control" href="#carouselMenu<?= $kunik ?>" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carouselMenu<?= $kunik ?>" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>

    </div><!-- End Carousel -->
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        var clickEvent = false;
        var setInterval = false;
        
        if (!costum.editMode){
            setInterval = 4000;
        }

        $('.container-<?= $kunik ?> #carouselMenu<?= $kunik ?>').carousel({
            interval:   setInterval
        }).on('click', '.list-group li', function() {
            clickEvent = true;
            $('.container-<?= $kunik ?> .list-group li').removeClass('activeMenu');
            $(this).addClass('activeMenu');
            if(costum.editMode){
                const slideIndex = parseInt($(this).attr('data-slide-to')) + 1;
                const carouselContainer = $(`.container-<?= $kunik ?> .carousel-inner`);
                carouselContainer.children().removeClass('active');
                carouselContainer.children().eq(slideIndex - 1).addClass('active');
            }
        }).on('slid.bs.carousel', function(e) {
            if(!clickEvent) {
                var count = $('.list-group').children().length -1;
                var current = $('.list-group li.activeMenu');
                current.removeClass('activeMenu').next().addClass('activeMenu');
                var id = parseInt(current.data('slide-to'));
                if(count == id) {
                    $('.container-<?= $kunik ?> .list-group li').first().addClass('activeMenu');
                }
            }
            clickEvent = false;
        });

        if (costum.editMode) {
            var blockChildren = <?= json_encode($blockChildren) ?>;
            var optionsOfSlide = swiperObj.incrementeNumberOfOptions(Object.keys(blockChildren).length);
            var optionsOfSlideSlice = optionsOfSlide.slice(1);
            var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [] ) ?>;
            swiperObj.inputsOnEdit(paramsDataSwiper);
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
            var galleryInput = {
                configTabs : {
                    general : {
                        inputsConfig: [
                            {
                                type: "inputMultiple",
                                options: {
                                    name: "carouselData",
                                    label: "Navigation",
                                    class: "navigation-input-form",
                                    inputs: [
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "title",
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    style: {
                        inputsConfig: [
                            "backgroundType",
                            "addCommonConfig",
                            {
                                type: "section",
                                options: {
                                    name: "<?= $kunik ?>-menu-css",
                                    label: "Style du menu",
                                    showInDefault: false,
                                    inputs: [
                                        "background",
                                        "color",
                                        "fontSize",
                                        "margin",
                                        "padding",
                                        "boxShadow",
                                        "border",
                                        "borderRadius",
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "activeMenu",
                                    label: "Active Menu",
                                    showInDefault: false,
                                    inputs: [
                                        "background",
                                        "color",
                                        "boxShadow",
                                        "border",
                                    ]
                                }
                            }
                        ]
                    },
                    hover: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    },
                    advanced: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    if (name == "carouselData") {
                        const isAllInputEmpty = value.every(item => item.title && item.title.trim() !== "");
                        if (isAllInputEmpty) {
                            ajaxPost(
                                null,
                                baseUrl + "/co2/cms/insertcontainercarousel",
                                {
                                    carouselData: value,
                                    blockParent: "<?= $myCmsId ?>",
                                    page: cmsBuilder.config.page
                                },
                                function(response) {
                                    mylog.log("dataResponse", response);
                                }
                            )
                        }
                    }
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks["<?= $kunik ?>"] =  galleryInput;
        }
    })

    $(window).load(function() {
        var boxheight = $('.container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .carousel-inner').innerHeight();
        var itemlength = $('.container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .item').length;
        var triggerheight = Math.round(boxheight/itemlength+1);
        $('.container-<?= $kunik ?> #carouselMenu<?= $kunik ?> .list-group-item').outerHeight(triggerheight);
    });
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
</script>
