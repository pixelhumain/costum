<?php
$keyTpl = "";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];

$cssAnsScriptFilesModule = array(
    '/plugins/Chart-2.8.0/circles.min.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);

?>

<style id="style-<?= $kunik ?>" type="text/css">
    .progress-contain<?= $kunik ?> .progress-type {
        font-size: 16pt;
        padding-left: 1em;
        padding-bottom: 1em;
        z-index: 4;
        position: absolute;
        margin-top: 0px;
    }

    .flex-left<?= $kunik ?> {
        position: relative;
        font-weight: bolder;
        font-size: 16pt;
    }

    .sr-only {
        position: absolute;
        width: 1px;
        height: 1px;
        padding: 0;
        margin: -1px;
        clip: rect(0,0,0,0);
        border: 0;
    }

    #thematique-<?= $kunik ?> {
        border : none;
        text-align: left;
    }
    .color-secondary{
        color: #e7352f;
    }
</style>

<div class="<?= $kunik ?>">
    <div class="col-xs-12 padding-top-50 padding-bottom-50 container-osm-observatoire-thematique"> 
        <div>
            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 text-left margin-top-30">
                <div style="width:80px; height:8px; border-radius:20px; background-color:<?="#00a041" //$styleCss->color-secondaire->color??"#00a041"; ?>"></div>
                <h2 class="title-osm-observatoire-diversity" id="thematique-<?= $kunik ?>">CONTRIBUTION SUR LA DIVERSITÉ DES THEMATIQUES</h2>
            </div>
            
            <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                <div class="row" id="container-osm-thematique-graph<?= $kunik?>">
                        
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var type = [];
    var locationValue = [];
    var allData;
    var dataElts = [];
    var madatlasContribution = [];
    var blockCms = {};
    var dataShow = [];
    var graph = [];
    var allChildType = [];
    

    function randomCOlor() {
        const colors = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", 'D', "E", "F"];
        var color = "#";
        for(var i = 1; i <= 6; i++) {
            let index = Math.floor(Math.random() * colors.length);
            color += colors[index];
        }
        return color;
    }

    function refreshMadatasContribution() {
        var tmp = [];
        let allcount = 0;
        let contributionCount = 0;
        if(allData != null && typeof allData.element != "undefined" && allData.element != null) {
            
            $.each(allData.element, function(key, data) {
                allcount++;
                if(data != null && typeof data.madatlasContribution != "undefined" && data.madatlasContribution) {
                    tmp.push(data);
                    contributionCount++;
                }
            });
        }

        if(allcount > 0) {
            let contributionPourcent = (contributionCount * 100) / allcount;
            contributionPourcent = contributionPourcent.toFixed(1);
            $.each($(".count-ville"), function(key, val) {
                $(val).html(contributionPourcent + " %");
            });
        }
        
        madatlasContribution = tmp;
    }

    function refreshListe() {
        refreshMadatasContribution();
        if(blockCms != null && typeof blockCms.graphShow != "undefined" && blockCms.graphShow == "circle" && typeof Circles != "undefined" && Circles != null)
            setOsmThemathiqueGraphCircle();
        else if(blockCms != null && typeof blockCms.graphShow != "undefined" && blockCms.graphShow == "line")
            setOsmThemathiqueGraphLine();
    }

    function getData(changeFiltreCity = false) {
        lieuToSend = locationValue;
        var params = {
            location: lieuToSend,
            types: typeDataToGet,
            osmAllTYpe: osmAllTYpe,
            idBlock: "<?= $myCmsId ?>",
            selectedElement: <?= json_encode($blockCms["selectedElement"]) ?>,
            changeFiltreCity: changeFiltreCity,
        };

        ajaxPost(
            null,
            baseUrl + '/api/convert/osm',
            params,
            function(data) {
                /* if (data != null && typeof data.element != "undefined") {
                    if(data != null && typeof data.element != "undefined" && data.element.length > 0) {
                        allData = data;
                        refreshListe();
                    }
                }

                */
            }, {
                async: false
            }
        );
    }


    function getNameOfType(type, getType) {
        var str = "";
        $.each(getType, function(key, val) {
            if (val != null && typeof val.osmType != "undefined" && val.osmType == type && typeof val.name != "undefined")
                str = val.name;
        });

        return str;
    }

    function getNameByKey(key) {
        var name = "";
        $.each(osmAllTYpe, function(kk, vv){
            if(kk == key && vv != null && typeof vv.name != "undefined")
                name = vv.name;
        });
        return name;
    }

    function getChildFormInFilter(parent, block = []) {
        var res = [];
        let i = 0;
        if (parent != null) {
            $.each(parent, function(key, child) {
                elementChild = [];

                if (child != null && typeof child.name != "undefined" && typeof child.element != "undefined") {
                    let params = {};
                    var show = false;
                    if (block != null && typeof block[key] != "undefined") {

                        params = block[key];
                        show = true;
                    } else if (block != null && typeof block["typeTag"] != "undefined" && block["typeTag"] != null && typeof block["typeTag"][key] != "undefined") {
                        params = block["typeTag"][key];
                        show = true;
                    }

                    elementChild = getChildFormInFilter(child.element, params);
                    res[i] = {
                        type: "section",
                        options: {
                            label: child.name,
                            name: key,
                            showInDefault: show,
                            inputs: elementChild
                        },
                    };
                } else if (child != null && typeof child.name != "undefined" && typeof child.type != "undefined" && typeof child.tag != "undefined") {
                    var defaultValue = false;
                    if (block != null && typeof block["allType"] != "undefined"  && (block["allType"] == "true" || block["allType"] == true))
                        defaultValue = true;
                    else if (block != null && typeof block[key] != "undefined")
                        defaultValue = block[key];
                    
                    if(defaultValue == "true" )
                        defaultValue = true;

                    if (i == 0) {
                        res[i] = {
                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: "allType",
                                label: "Tous selectionne",
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };

                    } else {
                        res[i] = {
                            
                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: key,
                                label: child.name,
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };
                    }

                }

                i++;
            });
        }

        return res;
    }

    function filters() {
        var option = {};
        $.each(<?= json_encode($blockCms["selectedElement"]) ?>, function(kk, vv){
            if(typeof osmAllTYpe[vv] != "undefined")
                option[vv] = osmAllTYpe[vv];
        });

        return getChildFormInFilter(option, <?= json_encode($blockCms) ?>);
    }

    function getChildType(child, res = { type: [], nameT : [] }) {
        $.each(child, function(key, type) { 
            if (type != null && typeof type.element != "undefined")
                getChildType(type.element, res);
            else if (type != null && typeof type.type != "undefined") {
                res.type.push(type.type);
                res.nameT.push(type.name);
            }
        });
    }

    function getTypeChildOfThematique(thematique) {
        var res = {
            name : "",
            type : [],
            nameT : [],
        };
        let i = 0;
        if (thematique != null && thematique != "" ) {
            $.each(osmAllTYpe, function(key, type) { 
                if(key == thematique && type != null && typeof type.name != "undefined") {
                    res["name"] = type.name;
                    getChildType(type.element, res);
                }
            });
        }

        return res;
    }            

    function setOsmThemathiqueGraphCircle() {
        if(allData != null && typeof allData.count != "undefined" && allData.count != null) {
            getAllChildElementShow();
            $("#container-osm-thematique-graph<?= $kunik?>").html("");
            var str = "";
            if(typeof Circles != "undefined" && Circles != null) {
                $.each(allData.count, function(key, val) {
                    if(val != null && typeof val.name != "undefined" && typeof val.type != "undefined" && allChildType.indexOf(val.type) != -1 && typeof val.count != "undefined") {
                        value = 0;
                        $.each(madatlasContribution, function(kk, vv){
                            var tmpTrouve = false;

                            if (vv != null && typeof vv.tags != "undefined" && vv.tags != null && typeof vv.tags.length != "undefined") {
                                for(let i = 0; i < vv.tags.length; i++) {
                                    if(vv.tags[i] == val.type)
                                        tmpTrouve = true;
                                }
                            }
                            if(tmpTrouve)
                                value++;
                        });
                        if(value > 0) {
                            str = `
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 margin-bottom-20">
                                    <div class="text-center color-principal" id="osm-thematique-graphe-`+ key +`-<?= $kunik?>"></div>
                                    <div class="text-center margin-bottom-10">
                                        <font class="label-osm-observatoire-diversity color-principal">
                                            <span><b>`+ val.name +`</b></span>
                                        </font>
                                    </div>
                                </div>
                            `;
                            

                            $("#container-osm-thematique-graph<?= $kunik?>").append(str);
                            /* value = (value * val.count) / 100; 
                            value = value.toFixed(0); */
                            var coleur = [ "#aaa", "#00a041"];
                            if(blockCms != null && typeof blockCms.css != "undefined" && blockCms.css != null && typeof blockCms.css["color-principal"] != "undefined" && blockCms.css["color-principal"] != null && typeof blockCms.css["color-principal"].color != "undefined" && blockCms.css["color-principal"].color != "")
                                coleur[1] = blockCms.css["color-principal"].color;
                            if(blockCms != null && typeof blockCms.css != "undefined" && blockCms.css != null && typeof blockCms.css["color-secondaire"] != "undefined" && blockCms.css["color-secondaire"] != null && typeof blockCms.css["color-secondaire"].color != "undefined" && blockCms.css["color-secondaire"].color != "")
                                coleur[0] = blockCms.css["color-secondaire"].color;

                            Circles.create({
                                id:           'osm-thematique-graphe-'+ key+"-<?= $kunik?>" ,
                                radius:       75,
                                value:        value,
                                maxValue:    val.count,
                                width:        15,
                                text:         function(value){return Math.round(value*100/val.count) ;},
                                colors:       coleur,
                                duration:     400,
                                wrpClass:     'circles-wrp color-principal',
                                textClass:    'circles-text percent color-principal',
                                styleWrapper: true,
                                styleText:    true
                            });
                        }
                        
                    }
                });
            }
        }
    }

    function setOsmThemathiqueGraphLine() {
        if(allData != null && typeof allData.count != "undefined" && allData.count != null) {
            getAllChildElementShow();
            $("#container-osm-thematique-graph<?= $kunik?>").html("");
            var str = "";
            var coleurPrimairy = "#00a041";
            var coleurSecondary = "#aaa";

            if(blockCms != null && typeof blockCms.css != "undefined" && blockCms.css != null && typeof blockCms.css["color-principal"] != "undefined" && blockCms.css["color-principal"] != null && typeof blockCms.css["color-principal"].color != "undefined" && blockCms.css["color-principal"].color != "")
                coleurPrimairy = blockCms.css["color-principal"].color;
            if(blockCms != null && typeof blockCms.css != "undefined" && blockCms.css != null && typeof blockCms.css["color-secondaire"] != "undefined" && blockCms.css["color-secondaire"] != null && typeof blockCms.css["color-secondaire"].color != "undefined" && blockCms.css["color-secondaire"].color != "")
                coleurSecondary = blockCms.css["color-secondaire"].color;

            $.each(allData.count, function(key, val) {
                if(val != null && typeof val.name != "undefined" && typeof val.type != "undefined" && allChildType.indexOf(val.type) != -1 && typeof val.count != "undefined") {
                    value = 0;
                    $.each(madatlasContribution, function(kk, vv){
                        var tmpTrouve = false;

                        if (vv != null && typeof vv.tags != "undefined" && vv.tags != null && typeof vv.tags.length != "undefined") {
                            for(let i = 0; i < vv.tags.length; i++) {
                                if(vv.tags[i] == val.type)
                                    tmpTrouve = true;
                            }
                        }
                        if(tmpTrouve)
                            value++;
                    });
                    if(value > 0) {
                        let pourcent = (value * 100 ) / val.count ; 
                        pourcent = pourcent.toFixed(0);
                        str = `
                        <div class="progress-contain<?= $kunik ?> margin-top-20">
                            <div class="color-principal text-left"><span>`+ val.name +`</span></div>
                            <div class="progress-type flex-container">
                                <span class="flex-left<?= $kunik ?> padding-right-10 percentColor text-white">`+pourcent+` %</span>
                                <span class="flex-right">
                                    <span data-id="<?= $kunik ?>" data-field="textOnProgressBar" style="cursor: default;"></span>
                                </span>
                            </div>
                            <div class="mp-progress padding-0">
                                <div class="progress emptyColor progress<?= $kunik ?>" style="height:30px;background-color:`+coleurSecondary+`">
                                    <div class="progress-bar completeColor color-principal" role="progressbar" aria-valuenow="`+pourcent+`" aria-valuemin="0" aria-valuemax="`+val.count+`" style="width: `+pourcent+`% !important;background-color:`+coleurPrimairy+`" data-toggle="tooltip" data-placement="top" title="`+pourcent+` %">
                                        <span class="sr-only color-secondaire margin-top-20">
                                            `+pourcent+` %
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                        

                        $("#container-osm-thematique-graph<?= $kunik?>").append(str);
                    }
                    
                }
            });
        }
    }

    function getAllChildElementShow() {
        allChildType = [];
        if(blockCms != null && typeof blockCms.selectedShow != "undefined" && blockCms.selectedShow != null && blockCms.selectedShow != "") {
            $.each(osmAllTYpe, function(key, val) {
                if(key == blockCms.selectedShow && val != null && typeof val.element != "undefined") {
                    $.each(val.element , function(kk, vv) {
                        if(vv != null && typeof vv.type != "undefined") {
                            allChildType.push(vv.type);
                        }               
                    });
                }
            });
        }
    } 

    function getOptionTagElementTypeFilterToShow() {
        var option = [];
        
        if(blockCms != null && typeof blockCms.selectedElement != "undefined" && blockCms.selectedElement != null) {
            $.each(blockCms.selectedElement, function(kk, vv) {
                option.push({value : vv, label: getNameByKey(vv)});
            });        
        }
    
        return option;
    }

    jQuery(document).ready(function() {
        blockCms = <?= json_encode($blockCms) ?>;
        if (blockCms != null) {
            if (typeof blockCms.location != "undefined")
                locationValue = blockCms.location;
            if (typeof blockCms.typeTag != "undefined")
                typeDataToGet = blockCms.typeTag;
            if (typeof blockCms.elts != "undefined") {
                allData = blockCms.elts;
                dataElts = blockCms.elts;
            }
            
            if(blockCms != null && typeof blockCms.selectedShow != "undefined" && blockCms.selectedShow != "")
                $(".<?= $kunik ?> #thematique-<?= $kunik ?>").html(getNameByKey(blockCms.selectedShow));
                
        }
        refreshListe();
        //getData();

    });
</script>

<script type="text/javascript">

  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#style-<?= $kunik ?>").append(str);
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
    let optionTagElementTypeFilter = getOptionTagElementTypeFilterToShow();

    osmobservatoire = {
      configTabs: {
        general: {
          inputsConfig: [
            {
                type: "select",
                options: {
                    name: "selectedShow",
                    label: "Element a afficher",
                    options: optionTagElementTypeFilter,
                }
            },
            {
                type: "select",
                options: {
                    name: "graphShow",
                    label: "Type de graphe",
                    options: [
                        {
                            value: "line",
                            label: "Progresse Bar"
                        },
                        {
                            value: "circle",
                            label: "Circle"
                        },
                    ],
                }
            },
          ]
        },
        style: {
          inputsConfig: [

            {
				type: "section",
				options: {
                    name: "container-osm-observatoire-thematique",
                    label: "container",
                    showInDefault: true,
                    inputs: [
                        "width",
                        "height",
                        "background",
                    ]
                }
            },
            {
				type: "section",
				options: {
                    name: "color-principal",
                    label: "Couleur Principal",
                    showInDefault: true,
                    inputs: [
                        "color"
                    ]
                }
            },
            {
				type: "section",
				options: {
                    name: "color-secondaire",
                    label: "Couleur Secondaire",
                    showInDefault: true,
                    inputs: [
                        "color"
                    ]
                }
            },
            {
				type: "section",
				options: {
                name: "title-osm-observatoire-diversity",
                label: "Titre",
                showInDefault: true,
                inputs: [
                    "color",
					"background",
					"fontSize",
					"fontFamily",
					"fontStyle",
					"textTransform",
					"fontWeight",
					"textAlign",
                ]
              }
            },

            {
				type: "section",
				options: {
                name: "label-osm-observatoire-diversity",
                label: "Description",
                showInDefault: true,
                inputs: [
                    "color",
					"background",
					"fontSize",
					"fontFamily",
					"fontStyle",
					"textTransform",
					"fontWeight",
					"textAlign",
                ]
              }
            },
            "addCommonConfig",
          ],
        },
        hover: {
          inputsConfig: [
            "addCommonConfig"
          ],
        },
        advanced: {
          inputsConfig: [
            "addCommonConfig"
          ],
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    };
    cmsConstructor.blocks.<?= $kunik ?> = osmobservatoire;
  }
  
</script>