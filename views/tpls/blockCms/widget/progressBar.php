<?php
  $myCmsId = $blockCms["_id"]->{'$id'};
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  
  $title = $blockCms['title'] ?? "Loremmmm";
  $pourcentage = $blockCms['pourcentage'] ?? "80";
  
  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) ['css' => $objectCss];
?>

<div class="progress <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
  <div class="progress-bar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?= $pourcentage ?>%;">
    <?= $title ?>
  </div>
  <span style="display: flex;justify-content: flex-end; margin-right: 10px;"><?= $pourcentage ?>%</span>
</div>
<style id="progressBar<?= $kunik ?>">
</style>
<script>
  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
  $("#progressBar<?= $kunik ?>").append(str);
  if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var progressBar = {
            configTabs: {
                general: {
                    inputsConfig: [
                      {
                        type: "inputSimple",
                        options: {
                            name: "title",
                            label: "Nom",
                        },
                      },
                      {
                        type: "inputSimple",
                        options: {
                            name: "pourcentage",
                            label: "Pourcentage",
                        },
                      },
                        "width",
                        "height"
                    ],
                },
                style: {
                    inputsConfig: [
                        "background",
                        "color",
                        "addCommonConfig"
                    ],
                }
            },
            onChange: function(path, valueToSet, name, payload, value) {
                if (name == "title") {
                    baliseToReplace = (typeof cmsConstructor.sp_params[cmsConstructor.spId].title != "undefined" && notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].title)) ? cmsConstructor.sp_params[cmsConstructor.spId].title : "Loremmmm";
                    newH = document.querySelector("." + cmsConstructor.kunik).outerHTML.replaceAll(baliseToReplace, value);
                    $("." + cmsConstructor.kunik).replaceWith(newH);
                }
                if (name == "pourcentage") {
                    baliseToReplace = (typeof cmsConstructor.sp_params[cmsConstructor.spId].pourcentage != "undefined" && notEmpty(cmsConstructor.sp_params[cmsConstructor.spId].pourcentage)) ? cmsConstructor.sp_params[cmsConstructor.spId].pourcentage : "80";
                    newH = document.querySelector("." + cmsConstructor.kunik).outerHTML.replaceAll(baliseToReplace, value);
                    $("." + cmsConstructor.kunik).replaceWith(newH);
                }
            },
        }
        cmsConstructor.blocks.progressBar = progressBar;
    }
</script>