<?php
$keyTpl = "badge_parent_block";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>

<style id="badgeparent<?= $kunik ?>">
  .container-badge-parent<?= $kunik ?> {
    width: 100%;
  }

  #selectBadgeParent<?= $kunik ?> {
    margin: 10px;
    border-radius: 5px;
    max-width: 120px;
    text-overflow: wrap;
    display: block;
    padding: 20px;
  }

  .badge-child-item<?= $kunik ?> {
    height: 100%;
  }

  .badge-child-item<?= $kunik ?> .child-item<?= $kunik ?> {
    position: relative;
    box-shadow:4px 4px 8px 4px rgba(0, 0, 0, 0.2);
    border-radius: 10px 10px 10px 10px;
  }

  .badge-child-item<?= $kunik ?> .child-item<?= $kunik ?> .child-<?= $kunik ?> {
    position: relative;
    overflow: hidden;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .badge-child-item<?= $kunik ?> .child-item<?= $kunik ?> .child-<?= $kunik ?> img{
    margin: 0 auto;
  }

  .badge-child-item<?= $kunik ?> .child-item<?= $kunik ?> .child-<?= $kunik ?> .item<?= $kunik ?> {
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: rgba(0, 0, 0, 0.2);
    opacity: 0.5;
    z-index: 10
  }

  .badge-child-item<?= $kunik ?> .child-item<?= $kunik ?> .child-<?= $kunik ?> .prarent-of-child<?= $kunik ?> {
    position: absolute;
    z-index: 11;
    color: white;
    border-radius: 20px;
    padding-left: 10px;
    padding-right: 10px;
    top: 10px;
    left: 10px;
  }

  .title-badge-child<?= $kunik ?> {
    font-size: 20px;
    color: #ffffff;
  }

  .img-badge-child<?= $kunik ?> {
    width: 100%;
    height: 100%;
    object-fit: contain;
    position: relative;
  }

  .name-badge-child-<?= $kunik ?> {
    font-weight: bold;
  }
  

  .container-info-badge-child<?= $kunik ?> {
    height: 50%;
    position: relative;
  }

  .container-badge-parent-item:hover .item<?= $kunik ?>{
    display: none;
  }

  ::-webkit-scrollbar {
    width: 10px;
    height: 10px;
  }

  ::-webkit-scrollbar-track {
    background-color: white;
  }

  ::-webkit-scrollbar-thumb {
    background: #7c7c7c96;
    border-radius: 10px;
  }
</style>
<div class="badgeparent <?= $kunik ?> badgeparent<?= $kunik ?>">
  <div id="container-badge-parent<?= $kunik ?>" class="row container-badge-parent<?= $kunik ?>">
    <?php if (isset($blockCms) && isset($blockCms["badge"]) && isset($blockCms["badge"]["parent"])) { ?>

    <?php } else { ?>
      <a id="selectBadgeParent<?= $kunik ?>" class="bg-dark text-white" href="javascript:;">
        <i class="fa fa-2x fa-bookmark"></i> <br>
        Selectionner les parents du badge a afficher
      </a>
    <?php } ?>
  </div>
</div>

<script type="text/javascript">
  const classContainerIdBadgeInDynform = "element-finder-";
  var idBadgeParent = [];
  var mybadgeInBlockParent = null;


  jQuery(document).ready(function() {
    var parentBadgeInBlock = {};
    const myBlockCmsInBadgeParent = <?= json_encode($blockCms) ?>;
    const kunikJs = "<?php echo $kunik ?>";

    function bindClickSelectParentBadge() {
      $("#container-badge-parent" + kunikJs + " #selectBadgeParent" + kunikJs).off().on("click", function() {
        showDynForm();
      });
    }

    function bindValidateDynForm() {
      $(".btn-send-dynform-" + kunikJs).off().on("click", function() {
        idBadgeParent = [];
        parentBadgeInBlock = {};
        $.each($(".element-finder"), function(key, val) {
          getIdAndNameParent($(val).attr("class"));
        });
        hideDynForm();
        getChild();
        setParentBadgeInBlockCms(parentBadgeInBlock);
      });
    }

    function getIdAndNameParent(allClassBadge) {
      let classBadge = allClassBadge.split(" ");
      for (var i = 0; i < classBadge.length; i++) {
        let id = classBadge[i].split(classContainerIdBadgeInDynform);

        if (id.length > 1 && typeof id[1] != "undefined") {
          idBadgeParent.push(id[1]);
          parentBadgeInBlock[id[1]] = $("." + classContainerIdBadgeInDynform + id[1] + " .info-contact .name-contact").html();
        }

      }
    }

    function getChild() {
      var params = {
        "parent": idBadgeParent
      };

      ajaxPost(
        null,
        baseUrl + "/co2/badges/getchild",
        params,
        function(data) {
          refresh(data);
        },
        null
      );
    }

    function refresh(data = null) {
      var str = "";
      if (data != null && typeof data.data != "undefined" && notEmpty(data.data) && typeof data.res != "undefined" && data.res) {
        var lenData = 0;
        $.each(data.data, function(key, badge) {
          if (badge != null) {

            lenData++;
            str += `
                    <div class="badge-child-item` + kunikJs + `" >
                        <div class="child-item` + kunikJs + ` container-badge-parent-item row">
                            <div class="child-` + kunikJs + ` image-badge-parent col-md-5 col-lg-5 col-sm-12 col-xs-12">
                                <div class="item` + kunikJs + `"></div>`;
            var parentBackgroundColor = "";
            $.each(parentBadgeInBlock, function(id, parent) {
              if (parent === "Licence") {
                parentBackgroundColor = "#00baff";
              } else if (parent === "Master") {
                parentBackgroundColor = "#ffa600";
              }

              str += `<div class="prarent-of-child` + kunikJs + `" style="background-color:${parentBackgroundColor}; ">`;

              if (parent == "Licence" || parent == "Master") {
                str += `<a class="title-badge-child` + kunikJs + ` " target="_blank" href="` + baseUrl + `/costum/co/index/slug/`+costum.slug+`#page.type.badges.id.` + id + `.views.all" >${parent}</a>`;
              }

              str += `</div>`;
            });

            str += `
                    <img class="img-badge-child` + kunikJs + ` " src="` + badge.profilImageUrl + `">
                </div>
                <div class="container-info-badge-child` + kunikJs + `  col-sm-12 col-md-7 col-lg-7 col-xs-12">
                    <a target="_blank" href="` + baseUrl + `/costum/co/index/slug/`+costum.slug+`#page.type.badges.id.` + key + `.views.all"><div class="name-badge-child-` + kunikJs + ` titre-badge-parent">` + badge.name + `</div></a>`;


            if (typeof badge.description != "undefined")
              str += `<div class="description-badge-parent">` + badge.description + `</div>`;

            str += `</div></div></div>`;
          }
        });
      } else {
        str += `
                <a id="selectBadgeParent` + kunikJs + `" class="bg-dark text-white" href="javascript:;">
                    <i class="fa fa-2x fa-bookmark"></i> <br>
                    Selectionner les parents du badge a afficher
                </a>
            `;
      }

      $("#container-badge-parent" + kunikJs).html(str);
      let width = $(".badgeparent"+ kunikJs).width();
      (width > 800 && lenData > 1) ? $(".badge-child-item" + kunikJs).addClass("col-lg-6 col-md-6 ") : $(".badge-child-item" + kunikJs).removeClass("col-lg-6 col-md-6 ").css("width", "100%");
      
      bindClickSelectParentBadge();
    }

    function hideDynForm() {
      $("#ajax-modal").modal("toggle");
    }

    function setParentBadgeInBlockCms(parentId) {
      //if (typeof myBlockCmsInBadgeParent == "undefined" ||  (typeof myBlockCmsInBadgeParent != "undefined" && typeof myBlockCmsInBadgeParent.badge == "undefined") || (typeof myBlockCmsInBadgeParent != "undefined" && typeof myBlockCmsInBadgeParent.badge != "undefined" && typeof myBlockCmsInBadgeParent.badge.parent == "undefined")) {
      var params = {
        "parent": parentId,
        "id": "<?= $myCmsId ?>"
      };

      ajaxPost(
        null,
        baseUrl + "/co2/badges/setbadgeparentinblockcms",
        params,
        function(data) {},
        null
      );
      // }
    }

    function showDynForm() {
      myDynForm = {
        jsonSchema: {
          title: "SELECTIONNER LE(S) BADGE(S) PARENT(S)",
          icon: " bookmark-o",
          type: "object",
          onLoads: {
            onload: function(data) {

            }
          },
          beforeBuild: function() {},

          beforeSave: function(data, callB) {},
          afterSave: function(data, callB) {
            dyFObj.commonAfterSave(data, callB);
          },
          properties: (() => {

              return {
                parent: {
                  inputType: "finder",
                  label: "badge parent",
                  buttonLabel: "Changer le(s) badge(s) parent(s)",
                  //multiple : false,
                  initMe: false,
                  placeholder: tradBadge.findExistingBadge,
                  rules: {
                    required: true
                  },
                  initType: ["badges"],
                  initBySearch: true,
                  openSearch: true,
                }
              }
            })
            ()
        }
      };

      let initDataParent = {}
      for (let i = 0; i < idBadgeParent.length; i++) {
        initDataParent[idBadgeParent[i]] = idBadgeParent[i];
      }

      dyFObj.openForm(myDynForm, null, initDataParent);
      $("#ajaxFormModal .form-actions").hide();

      $("#ajaxFormModal").append(`
            <div class=" form-group parentfinder">
                <div class="finder-parent">
                    <a href="javascript:;" class="form-control col-xs-6 btn btn-success btn-send-dynform-` + kunikJs + ` margin-bottom-10">Valider <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        `);

      bindValidateDynForm();

    }
    
    bindClickSelectParentBadge();
    if (typeof myBlockCmsInBadgeParent != "undefined" && myBlockCmsInBadgeParent != null && typeof myBlockCmsInBadgeParent.badge != "undefined" && typeof myBlockCmsInBadgeParent.badge.parent != "undefined" && myBlockCmsInBadgeParent.badge.parent != null) {
      mybadgeInBlockParent = myBlockCmsInBadgeParent.badge.parent;
      idBadgeParent = [];
      parentBadgeInBlock = {};
      $.each(mybadgeInBlockParent, function(key, val) {
        idBadgeParent.push(key);
        parentBadgeInBlock[key] = val;
      });
      getChild();
    }
  });
</script>

<script type="text/javascript">

  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#badgeparent<?= $kunik ?>").append(str);
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;

    badgeparent = {
      configTabs: {
        general: {
          inputsConfig: [
            "width",
            "height",
            "background",
            {
              type: "section",
              options: {
                name: "container-badge-parent-item",
                label: "Container",
                showInDefault: true,
                inputs: [
                  "background",
                  "width",
                  "height",
                ]
              }
            },
          ]
        },
        style: {
          inputsConfig: [
            "addCommonConfig",
            {
              type: "section",
              options: {
                name: "image-badge-parent",
                label: "Image",
                showInDefault: true,
                inputs: [
                  "width",
                  "height",
                  "border",
                  "borderRadius",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "titre-badge-parent",
                label: "Titre",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                  ""
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "description-badge-parent",
                label: "Description",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                  ""
                ]
              }
            },
          ],
        },
        hover: {
          inputsConfig: [
            "addCommonConfig"
          ],
        },
        advanced: {
          inputsConfig: [
            "addCommonConfig"
          ],
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    };
    cmsConstructor.blocks.<?= $kunik ?> = badgeparent;
  }
</script>