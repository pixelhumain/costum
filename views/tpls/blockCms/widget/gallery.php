<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $blockChildren = (isset($blockCms["blockChildren"])) ? $blockCms["blockChildren"] : [];
  $container = "content-".$kunik." .swiper-container";
  $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
  $otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
  $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
  $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
?>

<style id="css-<?= $kunik ?>">
 .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  
  .content-<?php echo $kunik ?> .swiper-container {
    width: 100%;
    position: relative;
  }
  .swiper-slide img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  .dark .input-section .panel {
    background-color: #3a3a3a00 !important ;
  }
  .content-<?php echo $kunik ?> .swiper-container .cmsbuilder-block-action {
    bottom: auto !important;
  }
  
 .content-<?php echo $kunik ?> .gallery-image-view {
     position: relative;
     display: inline;
 }
 .content-<?php echo $kunik ?> #image-galler {
     z-index: 999999;
 }
 .content-<?php echo $kunik ?> #image-galler img {
     width: 100%;
     height: auto;
 }

</style>

<div class="gallery content-<?= $kunik ?>">
  <div class="swiper-container">
    <div class="swiper-wrapper">
    <?php
      if (!empty($blockChildren)) {    
          $i = 1;
          foreach ($blockChildren as $key => $value) {
              $pathExplode = explode('.', $value["path"]);
              $count = count($pathExplode);
              $superKunik = $pathExplode[$count-1].$value["_id"];
              $blockKey = (string)$value["_id"];
              $cmsElement = $value["path"];       
              $params = [
                  "blockCms"  =>  $value,
                  "kunik"     =>  $superKunik,
                  "blockKey"  =>  $blockKey,
                  "content"   =>  array(),
                  "el"        =>  @$el,
                  "costum"    =>  $costum,
                  "page"      =>  $page
              ];
              preg_match("/url\('([^']+)'\)/", $value["css"]["backgroundImage"], $matches);
              if (isset($matches[1]))
                $inner_content = $matches[1];
              ?>
              <div class='swiper-slide' >
                  <div class="gallery-image-view" data-image-id="" data-toggle="modal" data-image="<?=$inner_content ?>" data-target="#image-gallery">
                    <?=$this->renderPartial("costum.views.".$cmsElement,$params); ?>
                </div>
              </div>

        <?php }
      }
    ?>
    </div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>

    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="image-gallery-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="image-gallery-image" class="img-responsive" src="" style="width: 100%; height: auto;">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="show-previous-image" style="float: left;"><i class="fa fa-arrow-left"></i>
                    </button>

                    <button type="button" id="show-next-image" class="btn btn-secondary" style="float: right;"><i class="fa fa-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var modalId = $('#image-gallery');
  jQuery(document).ready(function() { 
    var styleCss="";
    swiperObj.initSwiper('.content-<?= $kunik ?>', <?= json_encode($blockCms["swiper"]) ?>);
    styleCss += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(styleCss);
    if (costum.editMode) {
      var blockChildren = <?= json_encode($blockChildren) ?>;
      var optionsOfSlide = swiperObj.incrementeNumberOfOptions(Object.keys(blockChildren).length);
      var optionsOfSlideSlice = optionsOfSlide.slice(1);
      var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [] ) ?>;
      swiperObj.inputsOnEdit(paramsDataSwiper);
      cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
      var galleryInput = {
        configTabs : {
          general : {
            inputsConfig: [
              {
                type: 'inputFileImage',
                options: {
                  name : "images",
                  canSelectMultipleImage: true,
                  label : tradCms.uploadeImage,
                }
              },
              "width",
              "height"
            ]
          },
          swiper : {
            key: "swiper",
            keyPath : "swiper",
            icon: "image",
            label: "Swiper",
            content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"swiper"),
            inputsConfig : [
              {
                type: "selectGroup",
                options: {
                    label: tradCms.numberSlidetoShow,
                    name: "slidesPerView",
                    inputs:[
                        {
                            name: "md",
                            icon : "desktop",
                            options: notEmpty(optionsOfSlideSlice) ? optionsOfSlideSlice : [],
                            
                        },
                        {
                            name: "sm",
                            icon : "tablet",
                            options: notEmpty(optionsOfSlideSlice) ? optionsOfSlideSlice : []
                            
                        },
                        {
                            name: "xs",
                            icon : "mobile",
                            options: notEmpty(optionsOfSlideSlice) ? optionsOfSlideSlice : []
                            
                        }
                    ],
                    defaultValue: (notEmpty(paramsDataSwiper) && typeof paramsDataSwiper["breakpoints"] != "undefined" && typeof paramsDataSwiper["breakpoints"]["slidesPerView"] != "undefined") ? paramsDataSwiper["breakpoints"]["slidesPerView"] : []
                },
                payload: {
                  path: "swiper.breakpoints.slidesPerView"
                }
              },
              {
                  type: "select",
                  options: {
                      name: "initialSlide",
                      label: tradCms.firstSlide,
                      options: notEmpty(optionsOfSlide) ? optionsOfSlide : ["0"],
                  }
              },
              "addCommonConfig"
            ]
          },
          style: {
            inputsConfig: [
              "background",
              "addCommonConfig",
              "boxShadow"
            ]
          },
          hover: {
            inputsConfig : [
                "addCommonConfig",
            ]
          },
          advanced: {
              inputsConfig : [
                  "addCommonConfig",
              ]
          }
        },
        onChange: function (path,valueToSet,name,payload,value) {
          if (name == "images") {
            ajaxPost(
              null,
              baseUrl + "/co2/cms/insertgallery",
              {
                images: value,
                blockParent: "<?= $myCmsId ?>",
                page: cmsBuilder.config.page
              },
              function(response) {
                mylog.log("dataResponse", response);
              }
            )
          }
        },
        afterSave: function(path,valueToSet,name,payload,value) {
          cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
        }
      }
      cmsConstructor.blocks["<?= $kunik ?>"] =  galleryInput;
    } else {
        // Preview image gallery in preview mode
        loadGallery(true, '.gallery.content-<?= $kunik ?> .swiper-slide .gallery-image-view');

        function disableButtons(counter_max, counter_current) {
            $('#show-previous-image, #show-next-image')
                .show();
            if (counter_max === counter_current) {
                $('#show-next-image').hide();
            } else if (counter_current === 1) {
                $('#show-previous-image')
                    .hide();
            }
        }

        function loadGallery(setIDs, setClickAttr) {
            let current_image,
                selector,
                counter = 0;

            $('#show-next-image, #show-previous-image').click(function () {
                if ($(this)
                    .attr('id') === 'show-previous-image') {
                    current_image--;
                } else {
                    current_image++;
                }

                selector = $('[data-image-id="' + current_image + '"]');
                updateGallery(selector);
            });

            function updateGallery(selector) {
                var $sel = selector;
                current_image = $sel.data('image-id');
                $('#image-gallery-title').text($sel.data('title'));
                $('#image-gallery-image').attr('src', $sel.data('image'));
                disableButtons(counter, $sel.data('image-id'));
            }

            if (setIDs == true) {
                $('[data-image-id]').each(function () {
                    counter++;
                    $(this).attr('data-image-id', counter);
                });
            }
            $(setClickAttr).on('click', function () {
                updateGallery($(this));
            });
        }
    }
    
    $(".content-<?= $kunik ?> .sp-text").click(function() {
      $(this).attr('contenteditable', "true");
    })

  });
    $(document)
        .keydown(function (e) {
            switch (e.which) {
                case 37: // left
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                        $('#show-previous-image')
                            .click();
                    }
                    break;

                case 39: // right
                    if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                        $('#show-next-image')
                            .click();
                    }
                    break;

                default:
                    return;
            }
            e.preventDefault();
        });
</script>
