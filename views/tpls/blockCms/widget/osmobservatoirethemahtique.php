<?php
$keyTpl = "";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];

$cssAnsScriptFilesModule = array(
    '/js/osm/osmType.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule("co2")->assetsUrl);



?>

<style id="style-<?= $kunik ?>">
    .container-osm-observatoire-thematique .card-thematique {
        border-radius: .25rem;
        margin-bottom: 3rem;
        /*box-shadow: 0 2px 6px 0 rgba(225, 41, 41, 0.71), 0 2px 6px 0 rgba(206, 206, 238, 0.94);*/
        border-left: 5px solid  #e7352f !important;
        /*border-radius: 10px !important;*/
    }
    .container-osm-observatoire-thematique .card-thematique .card-body {
        min-height: 1px;
        padding: 0rem 1.25rem;
        text-align: left;
        background-color: white;
    }
    .container-osm-observatoire-thematique .card-thematique .card-body h1 {
        color: #e7352f;
        font-size: 45px;
    }

    span.thematic-name{
        font-size: 12pt;
    }

</style>

<div class="<?= $kunik ?>">
    <div class="container-osm-observatoire-thematique">
        <div class="container-osm-observatoire-thematique col-lg-12 col-md-12 col-xs-12 col-sm-12">
            <div class="text-center padding-30">
                <h3 class="title-osm-observatoire-thematique margin-bottom-30">CONTRIBUTION SUR LES THEMATIQUES</h3>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding-bottom-30">
                    <div class="row  card-items" id="container-thematique-name-and-count<?= $kunik ?>">
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var type = [];
    var locationValue = [];
    var allData;
    var dataElts = [];
    var madatlasContribution = [];
    var blockCms = {};

    function refreshMadatasContribution() {
        var tmp = [];
        let allcount = 0;
        let contributionCount = 0;
        if(allData != null && typeof allData.element != "undefined" && allData.element != null) {
            
            $.each(allData.element, function(key, data) {
                allcount++;
                if(data != null && typeof data.madatlasContribution != "undefined" && data.madatlasContribution) {
                    tmp.push(data);
                    contributionCount++;
                }
            });
        }

        if(allcount > 0) {
            let contributionPourcent = (contributionCount * 100) / allcount;
            contributionPourcent = contributionPourcent.toFixed(1);
            $.each($(".count-ville"), function(key, val) {
                $(val).html(contributionPourcent + " %");
            });
        }
        
        madatlasContribution = tmp;
    }

    function refreshListe() {
        refreshMadatasContribution();
        setOsmCountGraph();
    }

    function getChildType(child, res = { type: [], nameT : [] }) {
        $.each(child, function(key, type) { 
            if (type != null && typeof type.element != "undefined")
                getChildType(type.element, res);
            else if (type != null && typeof type.type != "undefined") {
                res.type.push(type.type);
                res.nameT.push(type.name);
            }
        });
    }

    function getTypeChildOfThematique(thematique) {
        var res = {
            name : "",
            type : [],
            nameT : [],
        };
        let i = 0;
        if (thematique != null && thematique != "" ) {
            $.each(osmAllTYpe, function(key, type) { 
                if(key == thematique && type != null && typeof type.name != "undefined") {
                    res["name"] = type.name;
                    getChildType(type.element, res);
                }
            });
        }

        return res;
    }   

    function setOsmCountGraph() {
        if(allData != null && typeof allData.count != "undefined" && allData.count != null) {
            var seperator = "";

            if(blockCms != null && typeof blockCms.selectedElement != "undefined" && blockCms.selectedElement != null) {
                var str = "";
                $("#container-thematique-name-and-count<?= $kunik ?>").html("");
                $.each(blockCms.selectedElement, function(key, elts) {
                    let thematique = getTypeChildOfThematique(elts);
                    let count = allData.count;
                    let countElts = 0;
                    let max = 100;

                    

                    $.each(madatlasContribution, function(kk, vv){
                        var tmpTrouve = false;

                        if (vv != null && typeof vv.tags != "undefined" && vv.tags != null && typeof vv.tags.length != "undefined") {
                            for(let i = 0; i < vv.tags.length; i++) {
                                if(thematique.type.indexOf(vv.tags[i]) != -1)
                                    tmpTrouve = true;
                            }
                        }
                        if(tmpTrouve) {
                            countElts++;
                            $.each(count, function(keyV, valueV){
                                if(valueV != null && typeof valueV.name != "undefined" && thematique.nameT.indexOf(valueV.name) != -1)
                                    max = valueV.count;
                            });
                        }
                    });

                    /* countElts = (countElts * max) / 100; 
                    countElts = countElts.toFixed(1); */

                    if(countElts > 0) {
                        str = `<div class=" col-lg-3 col-md-3 col-sm-4 col-xs-12"> <div class="card-thematique">
                                    <div class="card-body">
                                       <span class="mb-0 margin-top-25 thematic-name text-secondary">${thematique.name}</span>
                                       <h1>${countElts}</h1>
                                    </div>
                                </div></div>`;
                        
                        $("#container-thematique-name-and-count<?= $kunik ?>").append(str);
                    }
                    
                });
            }
            
        }
    }

    jQuery(document).ready(function() {
        blockCms = <?= json_encode($blockCms) ?>;
        if (blockCms != null) {
            if (typeof blockCms.location != "undefined")
                locationValue = blockCms.location;
            if (typeof blockCms.typeTag != "undefined")
                typeDataToGet = blockCms.typeTag;
            if (typeof blockCms.elts != "undefined") {
                allData = blockCms.elts;
                dataElts = blockCms.elts;
            }
                
        }
        refreshListe();

    });
</script>

<script type="text/javascript">

  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#style-<?= $kunik ?>").append(str);
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;

    osmobservatoire = {
      configTabs: {
        general: {
          inputsConfig: [
            {
				type: "section",
				options: {
                    name: "container-osm-observatoire-thematique",
                    label: "container",
                    showInDefault: true,
                    inputs: [
                        "width",
                        "height",
                        "background",
                    ]
                }
            },
            {
				type: "section",
				options: {
                name: "title-osm-observatoire-thematique",
                label: "Titre",
                showInDefault: true,
                inputs: [
                    "color",
					"background",
					"fontSize",
					"fontFamily",
					"fontStyle",
					"textTransform",
					"fontWeight",
					"textAlign",
                ]
              }
            },

            {
				type: "section",
				options: {
                    name: "statistique-osm-thematique",
                    label: "Statistique",
                    showInDefault: true,
                    inputs: [
                        "color",
                        "fontSize",
                        "fontFamily",
                        "fontStyle",
                        "textTransform",
                        "fontWeight",
                        "textAlign",
                    ]
                }
            },

            {
				type: "section",
				options: {
                    name: "separator-themathique",
                    label: "Separateur",
                    showInDefault: true,
                    inputs: [
                        "borderColor",
                    ]
                }
            }
          ]
        },
        style: {
          inputsConfig: [
            "addCommonConfig",
          ],
        },
        hover: {
          inputsConfig: [
            "addCommonConfig"
          ],
        },
        advanced: {
          inputsConfig: [
            "addCommonConfig"
          ],
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    };
    cmsConstructor.blocks.<?= $kunik ?> = osmobservatoire;
  }
</script>