<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];

$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>

<style id="elementStandard<?= $kunik ?>">
    .elem-stdr-content-<?= $kunik?> {
        min-height: 100px;
    }
    <?php
        if (isset(cacheHelper::getCostum()["css"]["color"]) && isset(cacheHelper::getCostum()["css"]["color"]["color1"]) && isset(cacheHelper::getCostum()["css"]["color"]["color2"])) {
            $colorCostum = cacheHelper::getCostum()["css"]["color"];
     ?>
    .material-card.color1 h2{ background-color:<?= $colorCostum["color1"]?> }
    .material-card.color1 h2:after{ border-top-color:<?= $colorCostum["color1"]?>;border-right-color:<?= $colorCostum["color1"]?>; }
    .material-card.color1 .mc-btn-action {background-color: <?= $colorCostum["color1"]?>;border-color:<?= $colorCostum["color2"]?>;}
    .material-card.color1 .mc-btn-action i{color:<?= $colorCostum["color2"]?>;}
    .material-card.color1 h2:before {border-right-color: <?= $colorCostum["color2"]?>;border-bottom-color: <?= $colorCostum["color2"]?>;}
    .material-card.color1 .mc-btn-action:hover {background-color: <?= $colorCostum["color2"]?>;color: <?= $colorCostum["color1"]?>;border-color: <?= $colorCostum["color1"]?>;}
    .material-card.color1 .mc-btn-action:hover i{color: <?= $colorCostum["color1"]?>;}
    .material-card.color1.mc-active .mc-btn-action {background-color: <?= $colorCostum["color2"]?>;border-color:<?= $colorCostum["color1"]?>;}
    .material-card.color1.mc-active .mc-btn-action i{color: <?= $colorCostum["color1"]?>;}
    .material-card.color1.mc-active .mc-footer {background-color: <?= $colorCostum["color1"]?>;}
    .material-card.color1.mc-active h2:before { border-right-color: <?= $colorCostum["color1"]?>;border-bottom-color: <?= $colorCostum["color1"]?>;}
    .material-card.color1.mc-active .mc-content {background-color: #eee}
    .material-card.color1 .mc-footer a {color:<?= $colorCostum["color2"]?>;background-color: <?= $colorCostum["color1"]?>; border-color: <?= $colorCostum["color2"]?>}

    /*element Panel Html*/
    .smartgrid-slide-element .img-back-card .text-wrap {
        background: <?= $colorCostum["color1"]?>;
        opacity: 0.8;
    }

    /*Rotating Card*/
    .rotatingCard .btn-simple {
        color: <?= $colorCostum["color1"]?>;
        border: 1px solid <?= $colorCostum["color1"]?>;
    }
    .rotatingCard .card .footer {
        border-top: 1px solid #202a5f;
    }
    .rotatingCard .card .user img {
        border: 4px solid #202a5f;
    }
    .elem-stdr-content-<?= $kunik?> .smartgrid-slide-element .slide-hover {
        background: <?= $colorCostum["color1"]?>;
        opacity: 0.8;
    }
    <?php
        }
    ?>

</style>
<!--  -->
<div class='headerSearchIncommunity no-padding col-xs-12'></div>
<div class="elem-stdr-content-<?= $kunik?> <?= $kunik?>-css">
    <div class='bodySearchContainer margin-top-10 swiper-container'>
        <div class='no-padding col-xs-12 swiper-wrapper dropdown-search-<?= $kunik ?>' id='dropdown_search'></div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <!-- <div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div> -->
    </div>
</div>


<script>

   str="";
   str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
   $("#elementStandard<?= $kunik ?>").append(str);

    <?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    mylog.log("ParamsData",<?php echo $kunik ?>ParamsData);

 
        var design = {
            "elementPanelHtml" : "<?= Yii::t("cms", "Multi-card of communecter") ?>" ,
            "rotatingCard" : "<?= Yii::t("cms", "Rotating card") ?>",
            "elementPanelHtmlFullWidth" : "<?= Yii::t("cms", "Panel full width") ?>",
            "materialDesign" : "Material Card",
        };
        var orderBy = {
            "name" : "<?= Yii::t("common", "Name") ?>",
            "created" : "<?= Yii::t("cms", "Date of creation") ?>",
            "updated" : "<?= Yii::t("cms", "Date of modification") ?>",
            "startDate" : "<?= Yii::t("cms", "Start date") ?>"
        }
        var orderType = {
            "1" : "<?= Yii::t("cms", "Ascending") ?>",
            "-1" : "<?= Yii::t("cms", "Descending") ?>",
        }
        var dataType = {
            /*"imageProfil" : "<?//= Yii::t("cms", "Image") ?>",*/
            "name" : "<?php echo Yii::t("common","Name") ?>",
            "description" : "<?= Yii::t("common", "Description") ?>",
            "address" : "<?= Yii::t("common", "Locality") ?>",
            "tags" : "<?= Yii::t("common", "Tags") ?>",
            "shortDescription" : "<?= Yii::t("cms", "Short description") ?>",
            "role" : "<?= Yii::t("form", "Roles") ?>",
            "collection" : "<?php echo Yii::t("common","collection") ?>",
            "type" : "<?php echo Yii::t("common","Type") ?>",
            "created" : "<?php echo Yii::t("common","Created") ?>",
            "startDate" : "<?php echo Yii::t("common","startDate") ?>",
            "endDate" : "<?php echo Yii::t("common","endDate") ?>",
        }



    if (costum.editMode){
        var paramsDataSwiper = <?= json_encode($blockCms["swiper"] ?? [])?>;
        swiperObj.inputsOnEdit(paramsDataSwiper);
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
            elementInput = {
                configTabs : {
                    general : {
                        inputsConfig : [
                            {
                                type: "section",
                                options: {
                                    name: "query",
                                    label: "<?= Yii::t("cms", "Query") ?>",
                                    showInDefault: true,
                                    inputs: [
                                        {
                                            type: "selectMultiple",
                                            options: {
                                                name: "types",
                                                label: "<?= Yii::t("cms", "Type of element") ?>",
                                                options: $.map(elTypeOptions, function (key, val) {
                                                    return {value: val, label: key}
                                                })
                                            },
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "indexStep",
                                                label: "<?= Yii::t("cms", "Number of results displayed") ?>",
                                            },
                                        },
                                        {
                                            type: "select",
                                            options: {
                                                name: "orderBy",
                                                label: "<?= Yii::t("cms", "Sort by") ?>",
                                                options: $.map(orderBy, function (key, val) {
                                                    return {value: val, label: key}
                                                })
                                            },
                                        },
                                        {
                                            type: "select",
                                            options: {
                                                name: "orderType",
                                                label: "<?= Yii::t("cms", "Order type") ?>",
                                                options: $.map(orderType, function (key, val) {
                                                    return {value: val, label: key}
                                                })
                                            },
                                        },
                                        {
                                            type: "selectMultiple",
                                            options: {
                                                name: "fieldShow",
                                                label: "<?= Yii::t("cms", "Data display") ?>",
                                                options: $.map(dataType, function (key, val) {
                                                    return {value: val, label: key}
                                                })
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    type : "checkbox",
                                    name : "carousel",
                                    label : "<?= Yii::t("cms", "Card scrolling") ?>",
                                    class : "switch"
                                },
                                payload: {
                                    path: "displayConfig.carousel"
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    type : "checkbox",
                                    name : "smartGrid",
                                    label : "<?= Yii::t("cms", "Smart grid") ?>",
                                    class : "switch"
                                },
                                payload: {
                                    path: "displayConfig.smartGrid"
                                }
                            },

                            {
                                type: "section",
                                options: {
                                    name: "displayConfig",
                                    label: "<?= Yii::t("cms", "Card display") ?>",
                                    class : "designConfig",
                                    showInDefault: true,
                                    inputs: [

                                        {
                                            type : "select",
                                            options : {
                                                name : "designType",
                                                label : "<?= Yii::t("cms", "Design type") ?>",
                                                options : $.map( design, function( key, val ) {
                                                    return {value: val,label: key}
                                                })
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    swiper : {
                        key: "swiper",
                        keyPath : "swiper",
                        icon: "image",
                        label: "swiper",
                        content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"swiper"),
                        inputsConfig : [
                            {
                                type: "selectGroup",
                                options: {
                                    label: tradCms.numberSlidetoShow,
                                    name: "slidesPerView",
                                    inputs:[
                                        {
                                            name: "md",
                                            icon : "desktop",
                                            options : ["1","2","3"],
                                            
                                        },
                                        {
                                            name: "sm",
                                            icon : "tablet",
                                            options : ["1","2","3"],
                                            
                                        },
                                        {
                                            name: "xs",
                                            icon : "mobile",
                                            options : ["1","2","3"],
                                            
                                        }
                                    ],
                                    defaultValue: (notEmpty(paramsDataSwiper) && typeof paramsDataSwiper["breakpoints"] != "undefined" && typeof paramsDataSwiper["breakpoints"]["slidesPerView"] != "undefined") ? paramsDataSwiper["breakpoints"]["slidesPerView"] : ""
                                },
                                payload: {
                                    path: "swiper.breakpoints.slidesPerView"
                                }
                            },
                            {
                                type: "select",
                                options: {
                                    name: "initialSlide",
                                    label: tradCms.firstSlide,
                                    options : ["0","1","2","3"],
                                }
                            },
                            "addCommonConfig"
                        ]
                    },
                    configCard : {
                        key: "configCard",
                        keyPath : "css.commonConfigCard",
                        icon: "list-alt",
                        label: "<?= Yii::t("cms", "Card") ?>",
                        content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"configCard"),
                        inputsConfig : [
                            "addCommonConfig",
                        ],
                    },
                    hover: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    },
                    advanced: {
                        inputsConfig : [
                            "addCommonConfig",
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
        };
        cmsConstructor.blocks[`elementStandard<?= $myCmsId?>`] =  elementInput;
    }

    function callQuery() {
        return new Promise(function(resolve, reject) {
            resolve(configSearchObj.query(<?php echo $kunik ?>ParamsData,null,"<?= $kunik ?>"));
        });
    }

    $(function(){
        callQuery().then(function (data) {
            setTimeout(function() {
                configSearchObj.bindEvents(<?php echo $kunik ?>ParamsData, "<?= $kunik ?>");
            },1500);

        });
    });



</script>
