
<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
    // $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
?>

<style type="text/css">
    .iframe-container {    
      padding-bottom: 60%;
      padding-top: 30px; height: 0; overflow: hidden;
  }
    .iframe-container iframe,
    .iframe-container object,
    .iframe-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    @media (min-width: 768px) {
      #myModalDoc .modal-header {
          min-height: 80px;
      }
    }
    .textView_<?=$kunik?> h2{
      margin-bottom: 10px;
      margin-top: 10px;
    }
    .button_<?=$kunik?> {
      background-color: <?= isset($blockCms['buttonProperty2']["buttoncolor"]) ? $blockCms['buttonProperty2']["buttoncolor"] : $blockCms["buttoncolor"]; ?>;
      border:1px solid <?= isset($blockCms['buttonProperty2']["buttoncolorBorder"]) ? $blockCms['buttonProperty2']["buttoncolorBorder"] : $blockCms["buttoncolorBorder"]; ?>;
      color: <?= isset($blockCms['buttonProperty2']["buttoncolorlabel"]) ? $blockCms['buttonProperty2']["buttoncolorlabel"] : $blockCms["buttoncolorlabel"]; ?>;
      padding: 8px 26px;
      margin-bottom: 4%;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      cursor: pointer;
      border-radius: 20px ;
    }
    .document_<?=$kunik?> {
      padding: 20px 0 20px 0!important;
      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
    }
    .document_<?=$kunik?> h1 {
      margin: 0 0 20px 0;
      font-size: 48px;
      font-weight: 700;
      line-height: 56px;
      color: #fff;
    }
    .document_<?=$kunik?> h2{
     margin: 0 0 10px 0;
     font-size: 35px;
     line-height: 35px;
     text-transform: none;
     /*margin-bottom: -180px;
     margin-top: 4%;*/
    }  
    .document_<?=$kunik?> h3{
    font-size: 25px;
    line-height: 32px;
    text-transform: none;
    margin-bottom: -30px;
    }
    .document_<?=$kunik?> .description{
      line-height: 26px;
      text-transform: none;
      /*padding: 10% 5% 10% 5%;
      margin-bottom: -90px;*/
    }
    @media (max-width: 414px) {
      .document_<?=$kunik?> h2{
        margin: 0 0 20px 0;
        font-size: 20px;
        line-height: 27px;
        text-transform: none;
        margin-bottom: -78px;
        margin-top: 17px;
      }
      .document_<?=$kunik?> .description{
        line-height: 15px;
        text-transform: none;
        padding: 82px 0px 19px 15px;
        margin-bottom: -40px;
        font-size: 13px;
      }
      .document_<?=$kunik?> {
        width: 100%;
        height: 100%;
        padding: 0 15px;
      }
      .button_<?=$kunik?> {
        padding: 8px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 11px;
        margin-top: 25px;
        /*margin: 22% 3% 4% 2%;*/
        cursor: pointer;
        border-radius: 20px;
      }
    }
    @media screen and (min-width: 1500px){
        .document_<?=$kunik?> h2{
          margin: 0 0 20px 0;
          font-size: 40px;
          line-height: 35px;
          text-transform: none;
          margin-top: 2%;
        }
        .document_<?=$kunik?> .description{
          line-height: 35px;
          font-size: 25px;
          text-transform: none;
          padding: 1% 0% 8% 0%;
          margin-bottom: -90px;
        }
        .button_<?=$kunik?> {
          padding: 10px 20px;
          margin-bottom: 2%;
          text-align: center;
          text-decoration: none;
          display: inline-block;
          font-size: 25px;
          cursor: pointer;
          border-radius: 35px;
        }
    }
</style>
<section  class="document_<?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>" >
    <div class="textView_<?=$kunik?> text-center" >
        <?php if ($blockCms["showType"] == "textbtn") { ?>
          <h2 class="sp-text img-text-bloc title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> <?=$blockCms["title"]?></h2>
          <div class="sp-text img-text-bloc description" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content" ><?=$blockCms["content"]?></div>
          <a href="" class="view-pdf<?=$kunik?> button_<?=$kunik?>" >
           <?= $blockCms["buttonlabel"]; ?>
         </a>
        <?php } else { ?>
            <a href="" class="view-pdf<?=$kunik?>" >
                <img class="logo<?=$kunik ?> img-responsive" src="<?php echo $blockCms['logo'] ?>" style="width: 100%;">
            </a>
        <?php } ?>
   </div>
</section>
<style id="showPdfInPopUp<?= $kunik ?>">
</style>
<script type="text/javascript">
(function(a){
  a.createModal=function(b){
    defaults={
      title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false
    };
    var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";
      html='<div class="portfolio-modal modal fade" id="myModalDoc" tabindex="-1" role="dialog" aria-hidden="true">';
      html+='<div class="modal-content padding-top-15">';
      html+='<div class="close-modal" data-dismiss="modal"><div class="lr"><div class="rl"></div></div></div>';
      html+='<div class="modal-header">';
      if(b.title.length>0){
        html+='<h4 class="modal-title">'+b.title+"</h4>"
      }
      html+="</div>";
      html+='<div class="modal-body" '+c+">";
      html+=b.message;
      html+="</div>";
      // html+='<div class="modal-footer">';
      // if(b.closeButton===true){
      //  html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'
      // }
      // html+="</div>";
      html+="</div>";
      html+="</div>";
      a("body").prepend(html);
      a("#myModalDoc").modal().on("hidden.bs.modal",function(){
        a(this).remove()
      })
  }
})(jQuery);
$(function(){    
    $('.view-pdf<?=$kunik?>').on('click',function(){
        var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        $.createModal({
        title:'<?=$blockCms["title"]?>',
        message: iframe,
        closeButton:true,
        scrollable:false
        });
        return false;        
    });    
})
var select = {
    "textbtn" : "<?php echo Yii::t('cms', 'Text and button')?>",
    "imgbtn" : "<?php echo Yii::t('cms', 'Image')?>"
};
str = "";
str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
$("#showPdfInPopUp<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var showingPdfInPopupModal = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "inputFile",
                            options: {
                                accept : ['pdf'],
                                type: "inputFile",
                                name: "pdf1",
                                label: "Pdf",
                            },
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "showType",
                                label : tradCms.displayType,
                                tabs: [
                                    {
                                        value:"textbtn",  
                                        label: tradCms.button,
                                        inputs : [
                                            {
                                                type: "inputSimple",
                                                options: {
                                                    name: "buttonlabel",
                                                    label: tradCms.label
                                                }
                                            },
                                        ]
                                    },
                                    {
                                        
                                        value:"imgbtn"  , 
                                        label: tradCms.image,   
                                        inputs :[
                                            {
                                                type: "inputFileImage",
                                                options: {
                                                    name: "logo",
                                                    label: tradCms.logo,
                                                }
                                            },
                                        ]
                                    }
                                ]
                            }
                        },
                    ],
                },
                style : { 
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "buttonProperty2",
                                label: tradCms.buttonProperty,
                                showInDefault: true,
                                inputs: [
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttoncolorlabel",
                                            label: tradCms.buttonLabelColor,
                                            defaultValue: "<?= isset($blockCms['buttonProperty2']["buttoncolorlabel"]) ? $blockCms['buttonProperty2']["buttoncolorlabel"] : $blockCms["buttoncolorlabel"]; ?>"
                                        }
                                    },
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttoncolorBorder",
                                            label: tradCms.buttonCorderColor,
                                            defaultValue: "<?= isset($blockCms['buttonProperty2']["buttoncolorBorder"]) ? $blockCms['buttonProperty2']["buttoncolorBorder"] : $blockCms["buttoncolorBorder"]; ?>"
                                        }
                                    },
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttoncolor",
                                            label: tradCms.buttonColor,
                                            defaultValue: "<?= isset($blockCms['buttonProperty2']["buttoncolor"]) ? $blockCms['buttonProperty2']["buttoncolor"] : $blockCms["buttoncolor"]; ?>"
                                        }
                                    },
                                ]
                            }
                        },
                    ]
                }
            },
            onChange: function(path, valueToSet, name, payload, value) {
                var fd = new FormData();
                var files = costumizer.design.fontUploadFiles;
                if (files != {}) {
                    fd.append('qquuid','fsdfsdf-yuiyiu-khfkjsdf-dfsd');
                    fd.append('qqfilename',value.name);
                    fd.append('qqtotalfilesize',value.size);
                    fd.append('qqfile',value);
                    fd.append('costumSlug', costum.contextSlug);
                    fd.append('costumEditMode', costum.editMode);
                    ajaxPost(
                        null,
                        baseUrl+'/co2/document/upload-save/dir/communecter/folder/'+costum.contextType+'/ownerId/'+costum.contextId+'/input/qqfile/docType/file/subKey/costumFont',
                        fd,
                        function(data){
                            if(data.result){
                                toastr.success(data.msg);
                            }
                        },
                        null,
                        null,
                        {
                            contentType: false,
                            processData: false
                        }
                    );
                } else {
                    toastr.error("Something went wrong !!");
                }
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        }
        cmsConstructor.blocks.showingPdfInPopupModal = showingPdfInPopupModal;
    }
    getPdf()
    function getPdf() {
        var urlToSend = baseUrl+"/"+moduleId+"/cms/getlistfile";
        var params = {
            costumId : costum.contextId,
            costumType : costum.contextType,
            "doctype" : "file",
            "subKey" : "costumFont"
        }
        ajaxPost(
            null,
            urlToSend,
            params,
            function(data){
                let dataResult = data.result;
                let maxCreatedObj = null;
                let maxCreatedValue = -Infinity;
                for (const key in dataResult) {
                    if (dataResult.hasOwnProperty(key)) {
                        const obj = dataResult[key];
                        const createdValue = obj.created;
                        if (createdValue > maxCreatedValue) {
                            maxCreatedValue = createdValue;
                            maxCreatedObj = obj;
                        }
                    }
                }
                // console.log("dasdasdasdsdasd", maxCreatedObj);
                $(".view-pdf<?=$kunik?>").prop("href", maxCreatedObj.docPath)
                
            }
        );
    }
</script>