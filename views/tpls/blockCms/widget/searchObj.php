<?php
  $keyTpl ="searchObj";
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [ "#".$kunik."-css" => $blockCms["css"] ?? [] ]; 
?>
<style id="searchObj<?= $kunik ?>">
    .elem-stdr-content-<?= $kunik?> {
        min-height: 100px;
    }
	.aac-design.aac-card {
		box-sizing: border-box;
		margin-bottom: 2rem;
	}
	<?php if(isset($blockCms["displayConfig"]["designType"]) && $blockCms["displayConfig"]["designType"] == "communDesign") {?>
		/* #dropdown_search {
			display: flex;
			flex-wrap: wrap;
			gap: 10px;
		} */
		.aac-card {
			/* flex: 1 1 calc(25% - 10px); */
			margin-bottom: 20px;
			box-sizing: border-box;
			margin-bottom: 20px;
		}
	
	<?php } ?>
	.initialisSearch{
		display: none;
	}
</style>
<div id="<?= $kunik ?>-css" class="searchObj-block" data-need-refreshblock="<?= $myCmsId ?>">
	<div id='listRoles' class='col-xs-12 no-padding'></div>
	<div class="col-sm-12 menu-filters-lg filter" id="filterContainerParent<?= $kunik ?>">
		<div id="filterSecondAlias<?= $kunik ?>" class="filterContainers<?= $kunik ?> filter-alias custom-filter searchBar-filters pull-left bs-mx-4 col-xs-12 no-padding hidden-xs hide">
			<input
					type="text"
					class="form-control pull-left text-center alias-main-search-bar search-bar"
					data-field="text"
					data-text-path="answers.aapStep1.titre"
					placeholder="<?php echo Yii::t('common', "Search by") ?> <?php echo ucfirst(Yii::t('common', "Search by proposal title")) ?>"
			>
			<span
					class="text-white input-group-addon pull-left main-search-bar-addon "
					data-field="text"
					data-icon="fa-search"
			>
				<i class="fa fa-search"></i>
			</span>
		</div>
		<h4 class="d-flex justify-content-between col-xs-12 bs-pt-2 bs-pb-32 border-bottom-default hide column-filter-title">
			<span class="title propsPanelTitle"><?php echo Yii::t("form", "Filters") ?></span>
			<i id="btnHideFilter" class="fa fa-angle-double-left cursor-pointer bs-pr-2 custom-font showHide-filters-xs hidden-xs"></i>
			<i class="fa fa-times cursor-pointer bs-pr-2 custom-font close-filter-xs visible-xs"></i>
		</h4>
		<div id='filterContainers<?= $kunik ?>' class='searchObjCSS configFilter'></div>
	</div>
	<div id='searchBodyContainer<?= $kunik ?>' class="col-sm-12">
		<div class="row header-grid-container bs-ml-0 bs-mr-0" id="header-filter-container<?= $kunik ?>">
			<div class="no-padding center-filter-header hide" id="filterAlias<?= $kunik ?>" data-connected-filter="filterContainers<?= $kunik ?>">
				<div class="filterContainers<?= $kunik ?> filterHidden filter-alias custom-filter searchBar-filters pull-left bs-mx-4">
					<input
							type="text"
							class="form-control pull-left text-center alias-main-search-bar search-bar"
							data-field="text"
							data-text-path="answers.aapStep1.titre"
							placeholder="<?= Yii::t('common', 'Search by proposal title') ?>"
					>
					<span
							class="text-white input-group-addon pull-left main-search-bar-addon "
							data-field="text"
							data-icon="fa-search"
					>
						<i class="fa fa-search"></i>
					</span>
				</div>
				<button type="button" class="btn btn-default showHide-filters-xs btn-aap-tertiary bs-mt-2 bs-py-3"
						style="height: fit-content; font-size: 1.5rem"
						id="show-filters-lg<?= $kunik ?>">
					<span class="filter-xs-count topbar-badge badge animated bounceIn badge-tranparent badge-success searchObj"></span>
					<i class="fa fa-sliders"></i>
					<small class="bs-pl-2 hidden-xs"><?php echo Yii::t("form", "Filters") ?></small>
				</button>
			</div>
			<div class='headerSearchIncommunity<?= $kunik ?> no-padding col-xs-12'></div>
		</div>
		<div class='bodySearchContainer margin-top-10 '>
			<div class='no-padding col-xs-12 elem-stdr-content-<?= $kunik?> dropdown-search-<?= $kunik ?>' id='dropdown_search'>
			</div>
			<div class='no-padding col-xs-12 text-left footerSearchContainer smartgrid-slide-element'></div> 
		</div>
	</div>
</div>
<script>

	str="";
   	str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
   	$("#searchObj<?= $kunik ?>").append(str);
	var coutryCode = {};
	var initInterval = null;
	ajaxPost(
		null, 
		baseUrl+'/co2/opendata/getcountries', 
		null, 
		function(data){
			$.each(data,function(k,v){
				if(typeof v.countryCode !=undefined)
					coutryCode[v.countryCode] = v.countryCode
			})
		},
		null,null,{async:false}
	);
	var design = {
		"elementPanelHtml" : "<?= Yii::t("cms", "Multi-card of communecter") ?>" ,
		"rotatingCard" : "<?= Yii::t("cms", "Rotating card") ?>",
		"elementPanelHtmlFullWidth" : "<?= Yii::t("cms", "Panel full width") ?>",
		"materialDesign" : "Material Card",
		"communDesign" : "Commun design",
	};
	if(typeof directory.cards == "function"){
		design["cards"] = "Carte simple"
	}
	var textField = {
		"name" : "Nom",
		"email" : "Email",
		"email" : "Email",
		"shortDescription" : "Description courte",
		"slug" : "Slug",
		"title" : "Titre",
		"description" : "Description"
	}
	var viewFilter = {
		"text" : "searchBar",
		"dropdownList" : "dropdownList",
		"buttonList" : "buttonList",
		"horizontalList" : "horizontalList",
		"accordionList" : "accordionList",
		"megaMenuAccordion" : "megaMenuAccordion",
	}
	var costumlist = {};
	if(typeof costum.lists != "undefined"){
		$.each(Object.keys(costum.lists),function(k,v){
			costumlist[v] = v
		})

	}
	var elTypesOptions = {
		"poi" : trad.poi,
		"events" : trad.events,
		"projects" : trad.project,
    	"citoyens" :trad.person,
		"organizations" : trad.organization
	};
	var typesEventFilter = {
		"text" : "text",
		"tags" : "tags",
		"filters" : "filters",
		"exists" : "exists",
		"selectList" : "selectList",
		"sortBy" : "sortBy"
	}
	paramsData = <?php echo json_encode( $blockCms ); ?>;
	filliaireCategories = <?php echo json_encode( $filliaireCategories ); ?>;
	var allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
	if (typeof dynamicContent == "undefined") {
		var dynamicContent = {};
	}
	if (paramsData?.query?.formId && allFormsContext?.[paramsData.query.formId]?.params) {
		dynamicContent[paramsData.query.formId] = allFormsContext[paramsData.query.formId].params
	}
	if (typeof paramsData?.filters?.listsData != "undefined" && notEmpty(paramsData?.filters?.listsData)) {
		$.each(paramsData.filters.listsData, function(k,v){
			dynamicContent[k] = v;
		})
	}
	// Add costum.costumizedDirectory list in your costum index js file.
	if(typeof costum.costumizedDirectory != "undefined"){
		design = {...design, ...costum.costumizedDirectory}
	}

	var keyHeaderGroup = {
		"count" : {
			label : tradCms.numberOfResults
		},
		"add" : {
			label : tradCms.buttonAdd
		},
		"map" : {
			label : tradCms.buttonMap
		},
		"calendar" : {
			label : trad.calendar
		}
	};
	if (costum.editMode){
		var thisCurrentQueryEditor = JSON.parse(JSON.stringify(cmsConstructor.commonEditor.query));
		if (thisCurrentQueryEditor.options && thisCurrentQueryEditor.options.inputs && thisCurrentQueryEditor.options.inputs[0]) {
			thisCurrentQueryEditor.options.inputs.splice(1, 0 , {
				type: "select",
				options: {
					name: "formId",
					label: "Formulaire concerné",
					class: "hide",
					isSelect2: true,
					options: $.map(allFormsContext, function (val, key) {
							return {
								value: key,
								label: (val.name ? ucfirst(val.name.toLocaleLowerCase()) : "Pas de nom") + (val.type ? ` (${val.type})` : "")
							};
						})
				}
			})
			thisCurrentQueryEditor.options.inputs.splice(2, 1 , {
				type : "inputSimple",
				options : {
					type : "checkbox",
					name : "community",
					label : trad.community,
					class : "switch"}
			})
		}
		mylog.log("",cmsConstructor.commonEditor.query)
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
		var searchObjInput = {
			configTabs : {
				general : {
                        inputsConfig : [
							thisCurrentQueryEditor,
							{
								type : "select",
								options : {
									name : "displayResults",
									label : tradCms.displayOptions,
									class : "displayResults",
									options : [
										{"label": trad.map ,"value" : "map"},
										{"label": tradCms.directory ,"value" : "directory"}
									]
								}
							},
                            {
                                type: "section",
                                options: {
                                    name: "displayConfig",
                                    label: "<?= Yii::t("cms", "Card display") ?>",
                                    class : "designConfig",
                                    showInDefault: false,
                                    inputs: [
                                        {
                                            type : "select",
                                            options : {
                                                name : "designType",
                                                label : "<?= Yii::t("cms", "Design type") ?>",
                                                options : $.map( design, function( key, val ) {
                                                    return {value: val,label: key}
                                                })
                                            }
                                        },
										{
											type: "groupButtons",
											options: {
												name: "disableScrollTop",
												label: "Desactiver scroll vers le haut en activant le filtre",
												options: [
													{
														label: "Oui",
														value: true,
														icon: ""
													},
													{
														label: "Non",
														value: false,
														icon: ""
													}
												]
											}
										},
										{
											type: "groupButtons",
											options: {
												name: "disableScrollTopPageClick",
												label: "Desactiver scroll vers le haut en cliquant sur la pagination",
												options: [
													{
														label: "Oui",
														value: true,
														icon: ""
													},
													{
														label: "Non",
														value: false,
														icon: ""
													}
												]
											}
										}
                                    ]
                                }
                            },
							{
                                type: "section",
                                options: {
                                    name: "moreQueryConfig",
                                    label: "Requete avancé",
                                    class : "moreQueryConfig",
                                    showInDefault: false,
                                    inputs: [
                                        {
                                            type : "select",
                                            options : {
                                                name : "url",
                                                label : "<?= Yii::t("cms", "Filter url") ?>",
												options : [
													{
														value : "default",
														label : "Par defaut"
													},
													{
														value : "aapProposalDir", 
														label : "Propositions AAP"
													},
													{
														value : "aapProjectDir",
														label : "Projets AAP"
													},
													{
														value : "aacDir",
														label : "Appel à commun"
													},
													{
														value : "communDir",
														label : "Les communs"
													}
												],
                                                defaultValue : null
                                            }
                                        },
										{
											type: "inputWithSelect",
											options: {
												label: "Page de redirection",
												name: "redirectPage",
												configForSelect2: {
													maximumSelectionSize: 1,
													tags: Object.keys(costum.app)
												}
											}
										},
										{
											type: "section",
											options: {
												name: "filtersMoreOptions",
												label: "Options de filtres",
												class: "filtersMoreOptions",
												showInDefault: false,
												inputs: [
													{
														type : "select",
														options : {
															name : "applyFor",
															label : "À appliquer pour",
															options : [
																{
																	value : "all",
																	label : "toutes les utilisateurs"
																},
																{
																	value : "forAdmin", 
																	label : "administrateur et super admin"
																},
																{
																	value : "forUser",
																	label : "simple utilisateur"
																}
															]
														}
													},
													{
														type: "textarea",
														options: {
															label: "Filters option",
															name: "filtersOption"
														}
													}
												]
											}
										}
                                    ]
                                }
                            }
                        ]
                    },  
				header : {
					key : "header",
					keyPath : "header", 
					icon : "header",
					label: tradCms.header,
					content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"header"),
					inputsConfig : [
						{
							type: "section",
							options: {
								name: "left",
								label: tradCms.left,
								showInDefault: true,
								inputs: []
							}
						},
						{
							type: "section",
							options: {
								name: "right",
								label: tradCms.right,
								showInDefault: true,
								inputs: []
							}
						},						
						
					]
				},
				filters : {
					key : "filters",
					icon : "filter",
					keyPath : "filters", 
					label: tradCms.filter,
					content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"filters"),
					inputsConfig : [						
						{
							type: "section",
							options: {
								name: "filterDesign",
								label: tradCms.designFilter,
								showInDefault: false,
								inputs: [
									{
										type : "select",
										options : {
											name : "displayType",
											label : tradCms.displayType,
											options : $.map( {"row" : tradCms.horizontal ,"column" : tradCms.vertical}, function( key, val ) {
												return {value: val,label: key}
											})
										},
									},
								]
							}
						},
						{
							type: "section",
							options: {
								name: "text",
								label: tradCms.searchBar,
								showInDefault: true,
								inputs: [
									{
										type : "inputSimple",
										options : {
											type : "checkbox",
											name : "active",
											label : trad.activated,
											class : "switch",
											//defaultValue : (typeof paramsData["header"] != "undefined" && typeof paramsData["header"]["left"] != "undefined" && typeof paramsData["header"]["left"][k] != "undefined") ? paramsData["header"]["left"][k]:false,
										
										}
									},
									{
										type: "inputSimple",
										options: {
											type: "inputSimple",
											name: "placeholder",
											label: tradCms.Placeholder,
											defaultValue : trad.whatlookingfor
										}
									},
									{										
										type : "selectMultiple",
										options : {
											name : "searchBy",
											label : tradCms.targetField,
											options : $.map( textField, function( key, val ) {
												return {value: val,label: key}
											})
										}
									},
								]
							}
						},					
						{
							type: "section",
							options: {
								name: "types",
								label: trad.type,
								showInDefault: true,
								inputs: [
									{
										type : "inputSimple",
										options : {
											type : "checkbox",
											name : "active",
											label : trad.activated,
											class : "switch",
										}
									},
									{
										type : "selectMultiple",
										options : {
											name : "lists",
                            				label: tradCms.typeOfElement,
											options : $.map( elTypesOptions, function( key, val ) {
												return {value: val,label: key}
											})
										},
									}
								]
							}
						},					
						{
							type: "section",
							options: {
								name: "scope",
								label: tradCms.scope,
								showInDefault: true,
								inputs: [
									{
										type : "inputSimple",
										options : {
											type : "checkbox",
											name : "active",
											label : trad.activated,
											class : "switch",
										}
									}
								]
							}
						},					
						{
							type: "section",
							options: {
								name: "scopeList",
								label: tradCms.scopeList,
								showInDefault: true,
								inputs: [
									{
										type : "inputSimple",
										options : {
											type : "checkbox",
											name : "active",
											label : trad.activated,
											class : "switch",
										}
									},
									{
										type : "inputSimple",
										options : {
											type : "inputSimple",
											name : "name",
											label : tradCms.label
										}
									},
									{
										type : "selectMultiple",
										options : {
											name : "countryCode",
											label : tradCms.countryCode,
											options : $.map( coutryCode, function( key, val ) {
												return {value: val,label: key}
											})
										},
									},
									{
										type : "select",
										options : {
											name : "level",
											label : tradCms.level,
											options : $.map( {"3" : "Région","4" : "Département"}, function( key, val ) {
												return {value: val,label: key}
											})
										},
									}
									
								]
							}
						},			
						{
							type: "section",
							options: {
								name: "themes",
								label: tradCms.thematic,
								showInDefault: true,
								inputs: [
									{
										type : "inputSimple",
										options : {
											type : "checkbox",
											name : "active",
											label : trad.activated,
											class : "switch",
										}
									}
								]
							}
						},	
						{
							type: "section",
							options: {
								name: "initLists",
								label: "Initialiser liste",
								showInDefault: false,
								inputs: [
									{
										type : "selectMultiple",
										options : {
											name : "lists",
                            				label: tradCms.type,
											options : [
												{
													value: "aac",
													label: "Appel à commun"
												},
												{
													value: "aacDefi",
													label: "Defi des appel à commun"
												}
											]
										},
									}
								]
							}
						},
						{
							type: "section",
							options: {
								name: "filtersPersonal",
								label: tradCms.customFilter,
								showInDefault: true,
								inputs: [
									{
										type: "inputMultiple",
										options: {
											name: "filterPersonal",
											label: "",
											class: "multiple-input-dark",
											inputs: [
												{
													type: "inputSimple",
													options: {
														label: tradCms.label,
														name: "label",
													},
												},
												{
													type: "inputSimple",
													options: {
														label : tradCms.targetField,
														name: "field",
													},
												},
												{
													type: "select",
													options: {
														label: tradCms.view,
														name: "view",
														options : $.map( viewFilter, function( key, val ) {
															return {value: val,label: key}
														})
													},
												},
												{
													type: "select",
													options: {
														label: trad.events,
														name: "events",
														options : $.map( typesEventFilter, function( key, val ) {
															return {value: val,label: key}
														})
													},
												},
												{
													type: "textarea",
													options: {
														name: "options",
														label: "options"
													}
												},
												{
													type: "select",
													options: {
														label: tradCms.enterDataOrUseExisting,
														name: "lists",
														class: "hide",
														options : [
															{
																value: "manuel",  
																label: tradCms.enterData,
															},
															{
																value: "exists",
																label: tradCms.selectTheExisting,
															},
															{
																value: "keyValue",
																label: "Clé valuer",
															},
															{
																value: "dynamic",
																label: "Dynamique",
															}
														]
													}
												},
												{
													type: "inputWithSelect",
													options: {
														name: "listTags",
														label: trad.List,
														class: "hide"
													}
												},
												{
													type: "select",
													options: {
														label: tradCms.existingList,
														name: "exist",
														class: "hide",
														options : $.map( costumlist, function( key, val ) {
															return {value: val,label: key}
														})
													},
												},
												{
													type: "inputMultiple",
													options: {
														name: "listObject",
														label: "Entrer des key valeur",
														class: "hide",
														inputs: [
															{
																type: "inputSimple",
																options: {
																	name: "objectkey",
																	label: "Clé"
																}
															},
															{
																type: "inputSimple",
																options: {
																	name: "objectvalue",
																	label: "valeur"
																}
															}
														],
													},
												},
												{
													type: "inputSimple",
													options: {
														label: "Valeur dynamic",
														name: "dynamic",
														class: "hide"
													},
												},
												// {
												// 	type : "inputSwitcher",
												// 	options : {
												// 		name : "lists",
												// 		label : tradCms.enterDataOrUseExisting,
												// 		tabs: [
												// 			{
												// 				value: "manuel",  
												// 				label: tradCms.enterData,
												// 				inputs : [
												// 					{
												// 						type: "inputWithSelect",
												// 						options: {
												// 							name: "listTags",
												// 							label: trad.List
												// 						}
												// 					},
												// 				]
												// 			}, 
												// 			{
												// 				value: "exists",
												// 				label: tradCms.selectTheExisting,
												// 				inputs: [
												// 					{
												// 						type: "select",
												// 						options: {
												// 							label: tradCms.existingList,
												// 							name: "exist",
												// 							options : $.map( costumlist, function( key, val ) {
												// 								return {value: val,label: key}
												// 							})
												// 						},
												// 					}
												// 				]
												// 			},
												// 			{
												// 				value: "keyValue",
												// 				label: "Clé valuer",
												// 				inputs: [
												// 					{
												// 						type: "inputMultiple",
												// 						options: {
												// 							name: "listObject",
												// 							label: "Entrer des key valeur",
												// 							inputs: [
												// 								{
												// 									type: "inputSimple",
												// 									options: {
												// 										name: "objectkey",
												// 										label: "Clé"
												// 									}
												// 								},
												// 								{
												// 									type: "inputSimple",
												// 									options: {
												// 										name: "objectvalue",
												// 										label: "valeur"
												// 									}
												// 								}
												// 							],
												// 						},
												// 					}
												// 				]
												// 			}
												// 		],
												// 		// defaultValue : (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal)?cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal:""
												// 	}
												// }
											],
											defaultValue: cmsConstructor.sp_params["<?= $myCmsId ?>"].filters && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal ? cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal : []
										}
									}
								]
								
							}
						},
					]
				},
				configCard : {
					key: "configCard",
					keyPath : "css.commonConfigCard",
					icon: "list-alt",
					label: tradCms.card,
					content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"configCard"),
					inputsConfig : [
						"addCommonConfig",
					],
				},
				configFilter : {
					key: "configFilter",
					keyPath : "css.configFilter",
					icon: "paint-brush",
					label: tradCms.designFilter,
					content: cmsConstructor.editor.views.tabConstructViews(cmsConstructor.kunik,cmsConstructor.spId,cmsConstructor.values,cmsConstructor.blockType,"configFilter"),
					inputsConfig : [
						{
							type: "section",
							options: {
								name : "searchBar-filters",
								label: tradCms.searchBar,
								showInDefault: true,
								inputs : [
									{
										type : "section",
										options: {
											name : "search-bar",
											label: tradCms.input,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									},
									{
										type : "section",
										options: {
											name : "main-search-bar-addon",
											label: tradCms.searchIcon,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									}
								]
							}
						},
						{
							type: "section",
							options: {
								name: "dropdown",
								label: tradCms.dropdownList,
								inputs: [
									{
										type : "section",
										options: {
											name : "btn-menu",
											label: tradCms.dropdownToggle,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									},
									{
										type : "section",
										options: {
											name : "list-filters",
											label: tradCms.dropdownMenu,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									},
									{
										type : "section",
										options: {
											name : "btn-filters-select",
											label: tradCms.listFilters,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									}
								]
							}
						},
						{
							type: "section",
							options: {
								name: "shadow-input-header",
								label: tradCms.scope,
								inputs: [
									{
										type : "section",
										options: {
											name : "input-global-search",
											label: tradCms.input,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									},
									{
										type : "section",
										options: {
											name : "input-group-addon",
											label: tradCms.addressIcon,
											showInDefault: true,
											inputs: [
												"color",
												"background",
												"border",
												"borderRadius",
												"boxShadow",
											]
										}

									}
								]
							}
						},
					],
				},
				hover: {
					inputsConfig : [
						"addCommonConfig",
					]
				},
			},
			beforeLoad: function(){
				// hide some input if not needed
				if (cmsConstructor.sp_params["<?= $myCmsId ?>"] && cmsConstructor.sp_params["<?= $myCmsId ?>"]) {
					if (cmsConstructor.sp_params["<?= $myCmsId ?>"].query) {
						if (cmsConstructor.sp_params["<?= $myCmsId ?>"].query.types && (cmsConstructor.sp_params["<?= $myCmsId ?>"].query.types.indexOf("answers") > -1 || cmsConstructor.sp_params["<?= $myCmsId ?>"].query.types.indexOf("projects") > -1)) {
							if (searchObjInput.configTabs.general.inputsConfig[0].options.inputs[1].options && searchObjInput.configTabs.general.inputsConfig[0].options.inputs[1].options.class) {
								var currentClass = searchObjInput.configTabs.general.inputsConfig[0].options.inputs[1].options.class;
								searchObjInput.configTabs.general.inputsConfig[0].options.inputs[1].options.class = currentClass.replace("hide", "")
							} 
						}
					}
				}
				if (typeof initInterval == "undefined" || initInterval == null) {
					initInterval = setInterval(() => {
						if ($('.section-filtersPersonal [name="events"]').length > 0) {
							if (cmsConstructor.sp_params["<?= $myCmsId ?>"] && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters) {
								if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal) {
									$($('.section-filtersPersonal [name="events"]')).each((index, elem) => {
										if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index] && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].events && cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].events != "text") {
											$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="lists"]`).closest(".form-group").removeClass("hide")
											if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists) {
												if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "manuel") {
													$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").removeClass("hide")
												}
												if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "exist") {
													$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="exist"]`).closest(".form-group").removeClass("hide")
												}
												if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "keyValue") {
													$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").removeClass("hide")
												}
												if (cmsConstructor.sp_params["<?= $myCmsId ?>"].filters.filtersPersonal.filterPersonal[index].lists == "dynamic") {
													$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).closest(".form-group").removeClass("hide")
													$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).removeClass("hide")
												}
											}
										}
									})
								}
							}
							clearInterval(initInterval);
							initInterval = null;
						}
					}, 300);
				}
            },
			onchange: function(path, valueToSet, name, payload, value) {
				if (path == "query" && name == "types" && valueToSet.indexOf("answers") < 0 && valueToSet.indexOf("projects") < 0) {
					$('.cmsbuilder-tabs-body [name="formId"]').closest(".form-group").addClass("hide");
				} else if (path == "query" && name == "types" && (valueToSet.indexOf("answers") > -1 || valueToSet.indexOf("projects") > -1)) {
					$('.cmsbuilder-tabs-body [name="formId"]').closest(".form-group").removeClass("hide")
				}
				if (path == "filtersPersonal" && name == "filterPersonal") {
					$($('.section-filtersPersonal [name="events"]')).each((index, elem) => {
						if (value[index].events && value[index].events != "text") {
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="lists"]`).closest(".form-group").removeClass("hide")
							if (value[index].lists) {
								if (value[index].lists == "manuel") {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").removeClass("hide")
								} else {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").addClass("hide")
								}
								if (value[index].lists == "exist") {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="exist"]`).closest(".form-group").removeClass("hide")
								} else {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="exist"]`).closest(".form-group").addClass("hide")
								}
								if (value[index].lists == "keyValue") {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").removeClass("hide")
								} else {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").addClass("hide")
								}
								if (value[index].lists == "dynamic") {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).closest(".form-group").removeClass("hide")
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).removeClass("hide")
								} else {
									$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).closest(".form-group").addClass("hide")
								}
							}
						} else if (value[index].events && value[index].events == "text") {
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="lists"]`).closest(".form-group").addClass("hide")
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="listTags"]`).closest(".form-group").addClass("hide")
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="exist"]`).closest(".form-group").addClass("hide")
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [class*="btn-add-input_"]`).closest(".form-group").addClass("hide")
							$(elem).closest(".form-group").closest(`[class*="inputs-row_"]`).find(`.form-group [name="dynamic"]`).closest(".form-group").addClass("hide")
						}
					})
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			}
		}
		cmsConstructor.blocks.searchObj =  searchObjInput;
		$.each(keyHeaderGroup,function(k,v){
			searchObjInput.configTabs.header.inputsConfig[0].options.inputs.push(
				{
					type : "inputSimple",
					options : {
						type : "checkbox",
						name : k,
						label : v.label,
						class : "switch",
						defaultValue : (typeof paramsData["header"] != "undefined" && typeof paramsData["header"]["left"] != "undefined" && typeof paramsData["header"]["left"][k] != "undefined") ? paramsData["header"]["left"][k]:false,
					
					}
				}
			)
			searchObjInput.configTabs.header.inputsConfig[1].options.inputs.push(
				{
					type : "inputSimple",
					options : {
						type : "checkbox",
						name : k,
						label : v.label,
						class : "switch",
						defaultValue : (typeof paramsData["header"] != "undefined" && typeof paramsData["header"]["right"] != "undefined" && typeof paramsData["header"]["right"][k] != "undefined") ? paramsData["header"]["right"][k]:false,
					}
				}
			)
		})
	}
	function searchObjPromise(paramsObjData, fillCategories, thisKunik) {
		return new Promise((resolve, reject) => {
			try {
				configSearchObj.query(paramsObjData, fillCategories, thisKunik);
				resolve();
			} catch (error) {
				reject(error);
			}
		});
	}

	searchObjPromise(paramsData, filliaireCategories, "<?= $kunik ?>")
		.then(() => {
			configSearchObj.bindEvents(paramsData, "<?= $kunik ?>")
		})
		.catch((error) => {
			
		});

	window.addEventListener("resize", () => {
		clearTimeout(window.resizeTimer);
		window.resizeTimer = setTimeout(() => {
			$(".item-text").css("height", "auto");
			var maxHeight = Math.max.apply(null, $(".item-text").map(function() {
				return $(this).outerHeight();
			}).get());
			$(".item-text").css("height", maxHeight);
		}, 200);
	});
	setTimeout(function() {
		var maxHeightiteminner = Math.max.apply(null, $(".item-text").map(function() {
			return $(this).outerHeight();
		}).get());
		$(".item-text").css("height", maxHeightiteminner);
	}, 1500)
	var primaryColor = "#ba1c24";
	var secondColor = "#F8B030";
	var color3 = "#358562";
	var color4 = "red";
	var color5 = "black";
	if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color1 !="undefined"){
		primaryColor = costum.css.color["color1"]; 
	}
	if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color2 !="undefined"){
		secondColor = costum.css.color["color2"];
	}
	if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color3 !="undefined"){
		color3 = costum.css.color["color3"];
	}
	if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color4 !="undefined"){
		color4 = costum.css.color["color4"];
	}
	if(typeof costum.css != "undefined" && typeof costum.css.color!="undefined" && typeof costum.css.color.color5 !="undefined"){
		color4 = costum.css.color["color5"];
	}
	stylecolor = ":root{ --primary-color: "+primaryColor+"; --secondary-color:"+secondColor+"; --color3:"+color3+"; --color4:"+color4+"; --color5:"+color5+" }"
	$("style").append(stylecolor);
	
</script>