<?php
    $keyTpl ="timeline";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
    $costumLanguage = isset($costum['langCostumActive']) ? $costum['langCostumActive'] : 'fr';
?>
<style type="text/css" id="css-<?= $kunik ?>">

    .timeline-block<?= $kunik ?> {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline-block<?= $kunik ?>:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: <?= $blockCms["iconColor"] ?>;
        left: 0%;
        margin-left: -1.5px;
        border-left: dashed;
    }

    .timeline-block<?= $kunik ?> > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline-block<?= $kunik ?> > li:before,
    .timeline-block<?= $kunik ?> > li:after {
        content: " ";
        display: table;
    }

    .timeline-block<?= $kunik ?> > li:after {
        clear: both;
    }

    .timeline-block<?= $kunik ?> > li:before,
    .timeline-block<?= $kunik ?> > li:after {
        content: " ";
        display: table;
    }

    .timeline-block<?= $kunik ?> > li:after {
        clear: both;
    }

    .timeline-block<?= $kunik ?> > li > .timeline-panel {
        width: 94%;
        float: left;
        border: 1px solid <?= $blockCms["panel"] ?>;
        background-color: <?= $blockCms["panel"] ?>;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline-block<?= $kunik ?> > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid <?= $blockCms["panel"] ?>;
        border-right: 0 solid <?= $blockCms["panel"] ?>;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline-block<?= $kunik ?> > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid <?= $blockCms["panel"] ?>;
        border-right: 0 solid <?= $blockCms["panel"] ?>;
        border-bottom: 14px solid transparent;
        content: " ";
    }
    .timeline-block<?= $kunik ?> > li.timeline-inverted, .timeline-container-offset {
        float: none;
    }



    .timeline-block<?= $kunik ?> > li > .timeline-badge {
        color: <?= $blockCms["iconColor"] ?>;
        width: 90px;
        height: 90px;
        line-height: 50px;
        font-size: 40px;
        padding: 20px;
        text-align: center;
        position: absolute;
        top: 0px;
        left: 0%;
        margin-left: -35px;
        background-color: <?= $blockCms["iconBgColor"] ?>!important;
        border: 2px solid <?= $blockCms["iconColor"] ?>;
        z-index: 100;
        border-radius: 50%;
    }

    .timeline-block<?= $kunik ?> > li.timeline-inverted > .timeline-panel {
        float: right;
    }
    .timeline-block<?= $kunik ?> > li.timeline-inverted > .timeline-panel:before {
        display: none;
    }


    .timeline-block<?= $kunik ?> > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }


    .timeline-block<?= $kunik ?> .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-block<?= $kunik ?> .timeline-body > p,
    .timeline-block<?= $kunik ?> .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-block<?= $kunik ?> .timeline-body {
        min-height: 70px;
    }


    .timeline-block<?= $kunik ?> .timeline-body > p  {
        font-size: 20px;
    }
    @media (min-width: 768px) and (max-width: 991px) {
        .timeline-block<?= $kunik ?> > li > .timeline-badge {
            color: #49cbe3;
            width: 70px;
            height: 70px;
            font-size: 32px;
            padding: 8px;
            margin-left: -24px;
        }
    }


    @media (max-width: 767px) {
        ul.timeline-block<?= $kunik ?>:before {
            left: 25px;
        }
        .timeline-block<?= $kunik ?> > li.timeline-inverted > .timeline-panel:before {
            display: block;
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }
        .timeline-block<?= $kunik ?> > li > .timeline-badge {
            width: 50px;
            height: 50px;
            display: block;
            line-height: 35px;
            font-size: 1.4em;
            margin-left: 12px!important;
            padding: 4px;
        }

        ul.timeline-block<?= $kunik ?> > li > .timeline-panel {
            width: calc(100% - 60px);
            width: -moz-calc(100% - 60px);
            width: -webkit-calc(100% - 60px);
        }

        ul.timeline-block<?= $kunik ?> > li > .timeline-badge {
            left: 0px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline-block<?= $kunik ?> > li > .timeline-panel {
            float: right;
        }

        ul.timeline-block<?= $kunik ?> > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline-block<?= $kunik ?> > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }

    .timeline-block<?= $kunik ?> .page-header h1 {
        color : <?= $blockCms["iconColor"] ?>;
    }

    .timeline-block<?= $kunik ?> .timeline-heading h4.timeline-title, .timeline-block .timeline-body p  {
        color: #ffffff;
    }
    /*#particle-js {
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: 50% 50%;
        position: absolute;
        top: 0px;
        z-index: 0;
    }*/
</style>

<div class="container<?php echo $kunik ?> col-xl-12 no-padding">
    <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 timeline-container-offset z-index-10 ">
        <?php 
            if (isset($blockCms["titleBlock"])) {
                $titleBlockToShow = "";
                $titleBlockLang = "fr";
                if (gettype($blockCms["titleBlock"]) == "array") {
                    if (array_key_exists($costumLanguage, $blockCms["titleBlock"])) {
                        $titleBlockToShow = $blockCms["titleBlock"][$costumLanguage];
                        $titleBlockLang = $costumLanguage;
                    } else {
                        $titleBlockToShow = reset($blockCms["titleBlock"]);
                    } 
                } else {
                    $titleBlockToShow = $blockCms["titleBlock"];
                } ?>
                <h3 class="title<?php echo $kunik ?> sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock" data-lang="<?= $titleBlockLang ?>"><?php echo $titleBlockToShow ?></h3>
        <?php    
            }
        ?>
    </div>
    <?php
        if (isset($blockCms["timelineData"])) { ?>
            <div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 timeline-container-offset">
                <ul class="timeline timeline-block<?= $kunik ?> z-index-10 ">
                    <?php
                    foreach ($blockCms["timelineData"] as $key => $value) {
                        $nameTimeline = "";
                        $nameTimelineLang = "fr";
                        if (gettype($value["nom"]) == "array") {
                            if (array_key_exists($costumLanguage, $value["nom"])) {
                                $nameTimeline = $value["nom"][$costumLanguage];
                                $nameTimelineLang = $costumLanguage;
                            } else {
                                $nameTimeline = reset($value["nom"]);
                            } 
                        } else {
                            $nameTimeline = $value["nom"];
                        }    
                    ?>
                        <li class="timeline-inverted">
                            <div class="timeline-badge warning"><i class="fa fa-<?= (isset($value["icon"]) ? $value['icon'] : "check" ) ?>"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-body">
                                    <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="timelineData.<?= $key ?>.nom" data-lang="<?= $nameTimelineLang ?>"><?php echo $nameTimeline ?></p>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    <!--<div id="particle-js"></div>-->
</div>
<script type="text/javascript">
    /*cmsEngine.callParticle("particle-js");*/
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);

    function mergeDataChangeAndDefaultData(target, source) {
        const result = {};
        $.each(target, function(key, valTarget) {
            if (source.hasOwnProperty(key)) {
                result[key] = dataHelper.arrayReplaceRecursive(target[key], source[key]);
            } else {
                result[key] = target[key];
                const language = (typeof costum.language != "undefined") ? costum.language : "fr";
                if (typeof target[key]["nom"] == "undefined")
                    jsonHelper.setValueByPath(result[key], "nom."+language, "<font face=\"NotoSans-Regular\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</font>");
            }
            result[key]["key"] = key;
        });
        return result;
    }

    jQuery(document).ready(function() {
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var formatDataValueTimeLine = [];
            var timeline = {
                configTabs: {
                    general:{
                        inputsConfig: [
                            {
                                type: "inputMultiple",
                                options: {
                                    name: "timelineData",
                                    inputs: [
                                        {
                                            type: "inputIcon",
                                            options: {
                                                label: tradCms.icon,
                                                name: "icon",
                                            }
                                        },
                                        {
                                            type: "textarea",
                                            options: {
                                                label: tradCms.description,
                                                name: "nom"
                                            }
                                        },
                                    ]
                                }
                            }
                        ],
                        processValue: {
                            timelineData: function(value) {
                                var valueChanged = {};
                                var valueToSave = {};
                                let count = 0;
                                for (let k = 0; k < value.length; k++) {
                                    if (typeof value[k]["nom"] != "undefined" && typeof value[k]["nom"] == "string") {
                                        value[k]["nom"] = {[costum.langCostumActive]: value[k]["nom"]};
                                    }
                                    valueChanged["line" + (k + 1)] = value[k];
                                    count++;
                                }
                                if (value.length === count) {
                                    valueToSave = mergeDataChangeAndDefaultData(valueChanged, cmsConstructor.sp_params[cmsConstructor.spId]["timelineData"]);
                                }
                                return valueToSave;
                            }
                        }
                    },
                    style: {
                        inputsConfig: [
                            {
                                type: "color",
                                options: {
                                    name: "panel",
                                    label: tradCms.bodyPanelColor,
                                    type : "color",
                                    defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.panel
                                }
                            },
                            {
                                type : "color",
                                options : {
                                    name : "iconColor",
                                    label : tradCms.iconColor,
                                    type : "color",
                                    defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.iconColor
                                }
                            },
                            {
                                type : "color",
                                options: {
                                    name: "iconBgColor",
                                    label: tradCms.iconBgColor,
                                    type: "color",
                                    defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.iconBgColor
                                }
                            },
                        ],
                    }
                },
                beforeLoad: function() {
                    if (notEmpty(formatDataValueTimeLine)) {
                        formatDataValueTimeLine = [];
                    }
                    Object.values(cmsConstructor.sp_params["<?= $myCmsId ?>"]["timelineData"]).forEach(function(values) {
                        let clonedValues = JSON.parse(JSON.stringify(values));

                        var formatName = "";
                        if (typeof clonedValues["nom"] == "object") {
                            formatName = (typeof clonedValues["nom"][costum.langCostumActive] != "undefined") 
                                ? clonedValues["nom"][costum.langCostumActive] 
                                : clonedValues["nom"][Object.keys(clonedValues["nom"])[0]];
                            delete clonedValues["nom"];
                            clonedValues["nom"] = formatName;
                        }
                        formatDataValueTimeLine.push(clonedValues);
                        timeline["configTabs"]["general"]["inputsConfig"][0]["options"]["defaultValue"] = formatDataValueTimeLine;
                    });
                },
                afterSave: function(path, valueToSet, name, payload, value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
            }
            $("p.sp-text[data-id='<?= $myCmsId ?>']").on("focusout", function(e) {
                e.stopImmediatePropagation();
                var thisSpText = $(this);
                if ($(".cmsbuilder-right-content").hasClass("active")) {
                    setTimeout(function() {
                        thisSpText.closest(".cmsbuilder-block[data-id='<?= $myCmsId ?>']").find("li[data-action='edit']").trigger("click");
                    }, 300);
                }
            });
            cmsConstructor.blocks.timeline = timeline;
        }
        
    });
</script>