<?php

$keyTpl = "parcoursGraph";

$paramsData = [];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style>
    #graph-container button#btn-edit {
        position: absolute;
        top: 10px;
        right: 10px;
    }

    .bg-gray {
        background-color: #f5f5f5 !important;
    }

    .bordered {
        border: 2px solid #eee;
    }

    .list-group-item {
        display: flex;
        gap: 20px;
    }
</style>

<?php
$graphAssets = [
    '/plugins/d3/d3.v6.min.js',
    '/js/graph.js',
    '/css/graph.css'
];
HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
);

function getChildrenTree($id, $collection, $badgeAssigned = null)
{
    $badge = PHDB::findOneById($id, $collection);
    if ($badge != null) {
        $badge["id"] = (string) $badge["_id"];
        $badge = recursiveChildrenTree($badge, $collection, $badgeAssigned);
        return $badge;
    } else {
        return array();
    }
}

function recursiveChildrenTree($badge, $collection, $badgeAssigned = null)
{
    if (!empty($badge)) {
        $children = PHDB::find($collection, ['$and' => [['parent.' . ((string) $badge["_id"]) => ["\$exists" => true]], ["_id" => ['$ne' =>  new MongoId((string) $badge["_id"])]]]]);
        if (!empty($children)) {
            $count = 0;
            $childrenLocked = 0;
            foreach ($children as $id => $child) {
                $child["id"] = (string) $id;
                $children[$id] = recursiveChildrenTree($child, $collection, $badgeAssigned);
                $count += $children[$id]["childrenCount"] + 1;
                $childrenLocked += $children[$id]["childrenLocked"];
                if (!empty($badgeAssigned)) {
                    if (!in_array((string) $id, $badgeAssigned)) {
                        $children[$id]["locked"] = true;
                        $childrenLocked++;
                    }
                }
            }
            $badge["children"] = array_values($children);
            $badge["childrenCount"] = $count;
            $badge["childrenLocked"] = $childrenLocked;
        } else {
            $badge["childrenCount"] = 0;
            $badge["childrenLocked"] = 0;
        }
    }
    return $badge;
}


$metiers = PHDB::find("metiers", ["onisepLinks" => ['$exists' => true]]); // Badge::getChildrenTree("62f0eef0a15b9870647942c4", "badges");
$formations = PHDB::find("formations");

$badges = []; //getChildrenTree("62f0eef0a15b9870647942c4", "badges");

$image = (isset($costum["logo"]) ? $costum["logo"] : Yii::app()->getModule("co2")->assetsUrl . '/images/thumbnail-default.jpg')
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 bg-gray" style="min-height: 100vh;">
            <div class="row margin-top-10 margin-bottom-10">
                <div class="col-md-4">
                    <h4>METIERS</h4>
                </div>
                <div class="col-md-8"><input type="text" class="form-control" placeholder="Rechercher un métier" id="search-metiers"></div>
            </div>
            <div id="metiers-container">
                <?php foreach ($metiers as $key => $value) { ?>
                    <div class="list-group margin-bottom-5" id="<?= $key ?>">
                        <a href="javascript:;" class="list-group-item" data-id="<?= $key ?>">
                            <div>
                                <img src="<?= $image ?>" height="50" alt="">
                            </div>
                            <div>
                                <h5 class="list-group-item-heading"><?= $value["job"]["title"] ?></h5>
                                <div class="list-group-item-text">
                                    <div>Projet de recrutement : <b><?= $value["employmentOffers"]["recruitmentProjects"] ?></b></div>
                                    <div>Difficultés de recrutement : <b><?= $value["employmentOffers"]["difficultRecruitments"] ?></b></div>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row margin-top-10 margin-bottom-10">
                <div class="col-md-9">
                    <h4 id="parcours-title">Parcours</h4>
                </div>
                <div class="col-md-3 text-right">
                    <button class="btn btn-default switcher" data-mode="list">Mode Liste</button>
                    <button class="btn btn-primary switcher" data-mode="mindmap">Mindmap</button>
                </div>
            </div>

            <div class="bordered">
                <div id="graph-container" style="min-height: 100vh;"></div>
            </div>
            <br><br>

            <!-- h4>Formations</h4 -->
            <?php /* foreach ($formations as $key => $value) { ?>
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <h5 class="list-group-item-heading"><?= $value["formation"]["name"] ?></h5>
                        <p class="list-group-item-text">
                            <?php foreach($value["formation"]["levels"] as $keyLevel => $valueLevel) { 
                                echo "<span class='badge ".(($keyLevel==0?"badge-primary":"badge-success"))."'>".$valueLevel."</span> ";
                            } ?>
                        </p>
                    </a>
                </div>
            <?php } */ ?>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade in" id="parcours-edit-modal" tabindex="-1" role="dialog" aria-hidden="false" style="padding-left: 0px; top: 56px;z-index: 20000">
    <div class="modal-content padding-top-15">
        <div id="parcours-editor-container" style="display: flex; justify-content:center; align-items: center; height: calc(100vh - 56px); width: 100vw; top: 56px; left: 0px; position: fixed; background-color: white; z-index: 8;"></div>
    </div>
</div>


<script>
    <?php if (false) { ?>
        const button = document.querySelector('#btn-edit');
        button.addEventListener('click', (e) => {
            e.stopPropagation();
            e.preventDefault();
            $('#parcours-edit-modal').modal("show");
            coInterface.showLoader("#parcours-editor-container");
            getAjax("#parcours-editor-container", baseUrl + '/co2/badges/parcoursedit/badge/' + contextId, null, 'html');
        })
    <?php } ?>

    $(document).ready(function() {
        var metiers = <?= json_encode($metiers) ?>;
        var formations = <?= json_encode($formations) ?>;
        var defaultImg = "<?= $image ?>";
        var selectedMetiersData = [];
        var mode = "mindmap";


        $(".list-group-item").off().on("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(".list-group-item").removeClass("active");
            $(this).addClass("active");
            var id = $(this).data("id");
            var data = metiers[id];
            data.name = data.job.title;
            data.profilMediumImageUrl = baseUrl + defaultImg;
            $("#parcours-title").text("Parcours de compétences sur : " + data.name);

            data.children = data.employmentOffers.jobTrends.map(d => {
                let linkCode = d.link.split("codeRome=")[1];
                var competences = formationsFromLevel(linkCode);
                var appellations = [];

                if (competences != null && typeof competences.appellations != "undefined") {
                    appellations = competences.appellations.map(c => {
                        let competencesKey = {
                            "name": c.libelle
                        };
                        if(c.link){
                            competencesKey.link = c.link;
                        }
                        if (c.competencesCles) {
                            competencesKey.children = c.competencesCles.map(c => {
                                let dt = {
                                    "name": c.competence.libelle
                                }
                                if(c.link){
                                    dt.link = c.competence.link;
                                }
                                return dt;
                            })
                        }
                        return competencesKey;
                    });
                }

                let child = {
                    "name": d.description,
                    "link": d.link,
                };

                if (competences != null) {
                    child.children = [{
                        "name": competences.libelle,
                        "children": appellations
                    }]
                }
                return child;
            });
            selectedMetiersData = data;

            if (mode == "list") {
                listParcours(data);
            } else {
                graphParcours(data);
            }
        });

        $(".list-group-item").eq(2).click();

        $("#search-metiers").on("keyup", function(e) {
            let results = [];
            $(".list-group.hidden").each((k, v) => {
                $(v).removeClass("hidden");
            })
            if ($(this).val().length > 0) {
                results = Object.values(metiers).filter(m => !m["job"]["title"].toLowerCase().includes($(this).val().toLowerCase()));
                $.each(results, (key, value) => {
                    $("#" + value._id.$id).addClass("hidden");
                });
            }
        })

        $(".switcher").on("click", function(e) {
            mode = $(this).data("mode");
            $(".switcher").removeClass("btn-primary");
            $(".switcher").removeClass("btn-default");
            $(this).addClass("btn-primary");

            if (mode == "list") {
                listParcours(selectedMetiersData);
            } else {
                graphParcours(selectedMetiersData);
            }
        })

        function listParcours(data) {
            $("#graph-container").empty();
            let string = `<div class="panel-group padding-top-20" id="accordion">`;
            $.each(data.children, (key, value) => {
                string += parcoursRecursive(data)
            });
            string += `</div>`;
            $("#graph-container").append(string);
        }

        function parcoursRecursive(obj) {
            let str = "";
            if (obj && obj.children && obj.children.length > 0) {
                $.each(obj.children, (key, value) => {
                    str += `<div class="panel panel-default margin-left-30">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="" href="#collapse${slugify(value.name,value.name)}">
                                        <span class="glyphicon glyphicon-file margin-right-10">
                                        </span>${value.name}</a>
                                        ${(value.link)?'<a href="'+value.link+'" target="_blank" class="btn btn-xs btn-default pull-right"><i class="fa fa-external-link"></i></a>':''}
                                    </h4>
                                </div>
                            <div id="collapse${slugify(value.name,value.name)}" class="panel-collapse collapse padding-5">
                        `;
                    str += parcoursRecursive(value)
                    str += `</div> </div>`;
                });
            } else if (obj && obj.name) {
                str += `
                    <div class="list-group padding-5">
                        <a href="${obj.link?obj.link:"javascript:;"}" target="_blank" class="list-group-item">
                            ${obj.name}
                        </a>
                    </div>`;
            }
            return str;
        }

        function graphParcours(data) {
            $("#graph-container").empty();
            let graph = new BadgeGraph(data, -1, {
                id: "root",
                label: "Parcours"
            }, false);
            graph._zoomInText = "";
            graph._zoomOutText = "";
            graph._zoomMagnifyGlassText = "";
            graph._zoomResetText = "";
            graph._zoomFsText = "";

            graph.setOnLabelClick((e, d) => {
                e.stopPropagation();
                e.preventDefault();
                //urlCtrl.loadByHash("#page.type.badges.id." + d.data.id);
            })
            graph.setLabelFunc((d, i, n) => {
                return d.data.name;
            })

            graph.draw("#graph-container");
            //graph.initZoom();
        }

        function formationsFromLevel(codeROM) {
            let competences = [];

            ajaxPost(null, 'https://qa.communecter.org/interop/francetravail/metier/method/read', {
                    code: codeROM
                },
                function(data) {
                    if (data.success) {
                        competences = data.content;
                    }
                },
                null,
                "json", {
                    async: false
                }
            );

            mylog.log("Eto competences", competences);

            return competences;
        }
    })
</script>