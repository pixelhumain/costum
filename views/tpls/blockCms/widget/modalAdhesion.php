<style>
    .modal-body<?= $kunik ?> div p {
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .content-step-0<?= $kunik ?> {
        width: 100%;
        float: left;
    }

    .final-content<?= $kunik ?> {
        width: 100%;
        float: right;
    }

    .close<?= $kunik ?> {
        background-color: transparent !important;
        color: #e54e5e;
        padding: 5px 10px !important;
        border-radius: 50% !important;
        border: 2px solid #999 !important;
        margin: 0px;
    }

    .rounded {
        border-radius: 10px;
    }

    #modalAdhesion<?= $kunik ?> {
        display: none;
        left: 0;
        top: 0 !important;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgb(50, 50, 50);
        background-color: rgba(50, 50, 50, 0.8);
        backdrop-filter: blur(1px);
        -webkit-backdrop-filter: blur(1px);
    }
</style>

<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "modalAdhesion";
?>

<style id="css-<?= $kunik ?>">
.btnModal<?= $kunik ?> {
    padding: 8px 25px;
    border: 8px solid #43C9B7;
    border-radius: 30px;
    font-size: 22px;
    color: #fff;
    margin: 30px 0 0 5px;
    text-decoration: none !important;
    display: inline-block;
    text-align: center;
    background-color: #43C9B7;
    transition: all .4s;
}
.btnModal<?= $kunik ?>:hover{
    border: 8px solid #43C9B7;
    color: #43C9B7;
    background-color: #fff;
    text-decoration: none;
}
.sp-elt-<?= $myCmsId ?> {
    width: auto !important;
}

@media (max-width : 500px) {
    div, p{
        font-size: 14px;       
    }
}
@media (min-width: 768px) {
    #dialogContentBody .section-nav-body {
        width: 11vw;
    }	
}
</style>

<button type="button" class="btn btn-info openModal<?= $kunik ?> btnModal<?= $kunik ?>" data-toggle="modal" data-target="#modalAdhesion<?= $kunik ?>"><?= $blockCms["btnLabel"] ?></button>

<!-- Modal -->
<div id="modalAdhesion<?= $kunik ?>" class="modal <?= $kunik ?> fade" role="document" style="z-index: 999999">
    <div class="modal-dialog modal-lg no-padding">
        <div class="modal-content <?= $kunik ?>">
            <div class="col-md-6">
                <div class="bg-white rounded padding-20">
                    <img src="<?= (isset($costum["logo"]) ? $costum["logo"] : Yii::app()->getModule("co2")->assetsUrl . '/images/thumbnail-default.jpg') ?>" alt="image" class="img-responsive">
                    <h4 class="margin-top-20"><?= (isset($costum["title"]) ? $costum["title"] : "") ?></h4>
                    <p class="margin-top-20">
                        <?= (isset($costum["description"]) ? $costum["description"] : "") ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="bg-white no-padding rounded">
                    <div class="modal-header">
                        <button type="button" class="close close<?= $kunik ?>" data-dismiss="modal">x</button>
                        <h4 class="modal-title padding-left-20">
                            <svg fill="none" height="25" viewBox="0 0 28 28" width="25" xmlns="http://www.w3.org/2000/svg" class="text-green" aria-hidden="true">
                                <g fill="currentColor">
                                    <path clip-rule="evenodd" d="m15.0393 2.44995c-.6707-.24772-1.4079-.24772-2.0786 0l-8.30715 3.06797c-.39274.14504-.65355.51939-.65355.93807v7.54411c0 3.6861 1.95965 6.6874 4.28911 8.8073 1.16017 1.0557 2.38789 1.8689 3.45309 2.4137 1.1081.5668 1.9145.779 2.2578.779s1.1497-.2122 2.2578-.779c1.0652-.5448 2.2929-1.358 3.4531-2.4137 2.3294-2.1199 4.2891-5.1212 4.2891-8.8073v-7.54411c0-.41868-.2608-.79303-.6536-.93807zm-2.7715-1.876139c1.1179-.412868 2.3465-.412868 3.4644 0l8.3071 3.067969c1.1783.43514 1.9607 1.55819 1.9607 2.81421v7.54411c0 4.4389-2.3618 7.9375-4.943 10.2865-1.2952 1.1786-2.6702 2.092-3.8885 2.7151-1.1754.6012-2.3332.9984-3.1685.9984s-1.9931-.3972-3.1685-.9984c-1.21831-.6231-2.59328-1.5365-3.88847-2.7151-2.58125-2.349-4.94303-5.8476-4.94303-10.2865v-7.54411c0-1.25602.78243-2.37907 1.96066-2.81421z" fill-rule="evenodd"></path>
                                    <path d="m18.2906 11.75h-.2535v-1.1855c0-2.19278-1.7415-4.02451-3.9182-4.06361-.0595-.00107-.1783-.00107-.2378 0-2.1767.0391-3.91819 1.87083-3.91819 4.06361v1.1855h-.25354c-.39069 0-.70937.4028-.70937.9003v5.9463c0 .4969.31868.9035.7094.9035h8.5812c.3907 0 .7094-.4066.7094-.9035v-5.9463c0-.4974-.3187-.9003-.7094-.9003zm-3.4867 3.8674v1.7967c0 .2058-.1723.3799-.3784.3799h-.8509c-.2061 0-.3785-.1741-.3785-.3799v-1.7967c-.1999-.1966-.3162-.4684-.3162-.7691 0-.5698.4408-1.0594 1.0013-1.082.0594-.0024.1783-.0024.2377 0 .5605.0226 1.0013.5122 1.0013 1.082 0 .3007-.1164.5725-.3163.7691zm1.5623-3.8674h-4.7323v-1.1855c0-1.30621 1.0623-2.38623 2.3661-2.38623s2.3662 1.08002 2.3662 2.38623z"></path>
                                </g>
                            </svg>
                            <?= $blockCms["modalLabel"] ?>
                        </h4>
                    </div>
                    <div class="modal-body modal-body<?= $kunik ?>">
                        <?php
                        $index = 0;
                        foreach ($subForms as $key => $sf) { ?>
                            <div class="<?= "content-step-$index" ?> content-step-<?= $index.$kunik ?>" >
                                <?php echo $this->renderPartial(
                                    "survey.views.tpls.forms.formSection",
                                    [
                                        "contextId" => $costum["contextId"],
                                        "contextType" => $costum["contextType"],
                                        "parentForm" => $form,
                                        "formId" => $blockCms["coformId"],
                                        "form" => $sf,
                                        "subFs" => $subForms,
                                        "wizard" => true,
                                        "answer" => $answer,
                                        "mode" => "w",
                                        "showForm" => true,
                                        "canEdit" => false,
                                        "canEditForm" => false,
                                        "canAdminAnswer" => false,
                                        "el" => $el
                                    ],
                                    true
                                );
                                $index++;
                                ?>
                                <!--button type="button" class="btn btn-info first-button">First Button</button-->
                            </div>
                        <?php } ?>
                        <div class="final-content final-content<?= $kunik ?>" style="display:none;">
                                <div class="text-green center" style="font-size:100px;">
                                    <i class="fa fa-check-circle"></i>
                                </div>
                                <p><?= (isset($blockCms["textAdhesionSuccess"]) ? $blockCms["textAdhesionSuccess"] : "") ?></p>
                                <!--div>
                                Nous sommes ravis de vous accueillir en tant que nouveau membre de notre communauté ! Votre adhésion a bien été enregistrée et le paiement a été reçu avec succès.
                                Voici quelques informations importantes pour commencer :
<ul>
    <li>Accès aux ressources : Vous pouvez désormais accéder à toutes les ressources réservées aux membres, telles que les événements, les ressources exclusives, etc.</li>
    <li>Accès aux événements : Vous pouvez désormais participer à des événements réservés aux membres, tels que les événements de formation, les événements de conférence, etc.</li>
    <li>Accès aux services : Vous pouvez désormais profiter de tous les services réservés aux membres, tels que les services de formation, les services de conseil, etc.</li>
    <li>Informations de connexion : Pour accéder à votre compte, connectez-vous avec vos identifiants sur notre site [lien du site].</li>
    <li>Assistance : Si vous avez des questions, n'hésitez pas à contacter notre équipe via [adresse e-mail de contact] ou à consulter la FAQ [lien de la FAQ].</li>
</ul>
Encore une fois, merci pour votre confiance. Nous sommes impatients de partager cette aventure avec vous !
                                </div-->
                            <!--button type="button" class="btn btn-info second-button">Second Button</button-->
                        </div>
                    </div>
                    <div class="modal-footer" style="border-top:none;padding:0px"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".openModal<?= $kunik ?>").click(function() {
            setTimeout(function() {
                var h = $(".modal-body .content-step-0").height();
                $(".modal-body").css('height', h + 'px');
                $(".modal-body .content-step-0").css('height', h + 'px');
            }, 250);
        });

        $('.first-button').on('click', function() {
            $('.content-step-0').animate({
                width: "toggle"
            }, function() {
                $('.final-content').animate({
                    width: "toggle"
                });
            });
        });

        $('.second-button').on('click', function() {
            $('.final-content').animate({
                width: "toggle"
            }, function() {
                $('.content-step-0').animate({
                    width: "toggle"
                });
                var h = $(".modal-body .content-step-0").height();
                $(".modal-body").css('height', h + 80 + 'px');
                $(".modal-body .content-step-0").css('height', h + 'px');
            });
        });
    });
</script>

<script>
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    const php = {
        kunik: '<?= $kunik ?>',
        my_cms_id: '<?= $myCmsId ?>',
    }
    $("#css-<?= $kunik ?>").append(str);

    if (costum.editMode) {
        const allFormsContext = <?php echo json_encode( $allFormsContext ); ?>;
        var customPages = $.map( costum.app, function( val, key ) {
            return {value: key,label: key}
        })
        $.each(allFormsContext, (formKey, formVal) => {
            const contextId = formVal.parent ? Object.keys(formVal.parent)[0] : "null";
            customPages.push({
                value : `#coformAap.context.${ costum.contextSlug }.formid.${ formKey }.step.aapStep1.answerId.new`,
                label : `#coformAap (${ formVal.name })`
            })
        });
        var pageToInModal = [];
        $.each(allFormsContext, (formKey, formVal) => {
            pageToInModal.push({
                value : formKey,
                label : `Nouvelle réponse au formulaire <b>${ formVal.name }</b>`
            });
        });
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var modalAdhesion = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "btnLabel",
                                label : "label du bouton",
                                collection : "cms"
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "coformId",
                                isSelect2 : true,
                                label : `Page`,
                                options : pageToInModal
                            }
                        },
                        {
                            "type": "groupButtons",
                            "options": {
                                "name": "getFirstStepOnly",
                                "label": "Récuperer l'etape un seulement",
                                "options": [
                                    {
                                        "label": "Oui",
                                        "value": true,
                                        "icon": ""
                                    },
                                    {
                                        "label": "Non",
                                        "value": false,
                                        "icon": ""
                                    }
                                ],
                                defaultValue : sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly ? sectionDyf?.[php.kunik + "BlockCms"]?.getFirstStepOnly : false
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "modalLabel",
                                label : `Titre du modal`
                            }
                        },
                        {
                            type : "textarea",
                            options : {
                                name : "textAdhesionSuccess",
                                label : `Texte de l'adhésion réussie`,
                                markdown: true,
                                class:"text-dark"
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig",
                        "width",
                        "color",
                        "backgroundColor"
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
                hover: {
                        inputsConfig: [
                            "addCommonConfig",
                            "color",
                            "backgroundColor"
                        ]
                },
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.modalAdhesion<?= $myCmsId ?> = modalAdhesion;
    }
</script>