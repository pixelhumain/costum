<?php

$styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
$otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";

$defaultStyle = [
	'event-type'    => [
		'border'        => '1px solid #52A8AC',
		'backgroundColor'    => '#52A8AC',
		'color'         => '#fff',
		'borderRadius'  => '24px',
		'fontSize'      => '20px',
		'fontWeight'    => '600',
		'paddingTop'    => '10px',
		'paddingRight'  => '10px',
		'paddingBottom' => '10px',
		'paddingLeft'   => '10px',
	],
	'filter-active' => [
		'color'      => '#01305E',
		'background' => '#fff',
		'border'     => '1px solid #52A8AC'
	]
];
$myCmsId = (string)$blockCms['_id'];
$blockCms["css"] = array_replace_recursive($defaultStyle, $blockCms["css"] ?? []);
$styleCss = (object)[$kunik => $blockCms["css"] ?? []];
?>

<style id="type_element<?= $kunik ?>" ></style>

<script>
	function eventTypology(W, $, php) {
		function get_session_data() {
			var session_data = sessionStorage.getItem('coevent-' + php.context.id + php.context.type);
			var filter = session_data ? JSON.parse(session_data) : {
				tags: [],
				regions: [],
				type: null
			};
			return (filter);
		}

		function set_session_data(data) {
			sessionStorage.setItem('coevent-' + php.context.id + php.context.type, JSON.stringify(data));
		}

		function parse_url_to_obj() {
			var output = {};
			var get_params = W.location.hash.indexOf('?');
			if (get_params > 1) {
				var get_string = W.location.hash.substr(get_params + 1);
				var values = get_string.split('&');
				$.each(values, function(i, value) {
					var index_cut = value.indexOf('=');
					var key = value.substr(0, index_cut);
					var val = value.substr(index_cut + 1);
					var is_array = key.search(/\[\]$/) > 1
					if (is_array)
						key = key.substr(0, index_cut - 2);
					if (typeof output[key] === 'undefined' && is_array)
						output[key] = [];
					if (is_array)
						output[key].push(val);
					else
						output[key] = val;
				});
			}
			return (output);
		}

		function parse_obj_to_url(type) {
			var params = parse_url_to_obj();
			params.type = type;
			var hash = location.hash.match(/\#[\w\-]+/);
			if (!hash)
				hash = '';
			else
				hash = hash[0];
			hash += '?'
			var key;
			for (key in params) {
				if (params[key] === null)
					continue;
				if (typeof params[key] === 'string')
					hash += key + '=' + params[key] + '&';
				else
					$.each(params[key], function(i, param) {
						hash += key + '[]=' + param + '&';
					});
			}
			hash = hash.replace(/.$/, '');
			hash = W.location.origin + W.location.pathname + hash;
			return (hash);
		}
		var C = $('[data-block=' + php.kunik + ']');
		var default_filter = get_session_data();

		C.find('.event-type').on('click', function() {
			var self = $(this);
			var parent = self.parents('[data-selected]');
			var session_data = get_session_data();

			if (session_data.type === self.attr('data-target'))
				session_data.type = null;
			else
				session_data.type = self.attr('data-target');
			set_session_data(session_data);
			W.history.pushState({}, '', parse_obj_to_url(session_data.type));
			$('.coevent-program').trigger('coevent-filter');
		});
		var get_params = parse_url_to_obj();
		if (Object.keys(get_params).includes('type')) {
			default_filter.type = get_params.type;
			set_session_data(default_filter);
		}
		if (default_filter.type)
			C.find('.event-type[data-target="' + default_filter + '"]').addClass('active filter-active');
		if (costum.editMode) {
			cmsConstructor.sp_params[php.myCmsId] = php.blockCms;
			var eventTypologyStyle = {
				configTabs: {
					style: {
						inputsConfig: [
							"background",
							{
								type: 'section',
								options: {
									name: 'event-type',
									label: 'Normal',
									showInDefault: true,
									inputs: [
										"background",
										"border",
										"padding",
										"fontSize",
										"fontWeight",
										"borderRadius"
									]
								}
							},
							{
								type: 'section',
								options: {
									name: 'filter-active',
									label: 'Active filter',
									showInDefault: true,
									inputs: [
										"background",
										"color",
										"border",
									]
								}
							}
						]
					}
				},
				afterSave: function(path, valueToSet, name, payload, value) {
					cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
				}
			};
			cmsConstructor.blocks[php.kunik] = eventTypologyStyle;
		}
	}
</script>
<style>
    .event-types {
        display: flex;
        gap: 10px;
        flex-wrap: wrap;
        justify-content: center;
        user-select: none;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .btn-in-icon {
        width:15px;
        transform: translateX(30px);
        margin-left: 10px;
    }

    .btn-with-icon-co{
        padding-left: 35px !important;
        /* padding-right: 20px !important; */
        display: flex;
        align-items: center;
    }

    .btn-with-icon-co.active .btn-in-icon,
    .btn-with-icon-co.active:hover .btn-in-icon,
    .btn-with-icon-co:hover .btn-in-icon{
        transform: translateX(0px);
        transition-duration: 0.3s;
    }
</style>
<div class="event-typology <?= $kunik ?>" data-block="<?= $kunik ?>">
    <div class="event-types co-content-typology" data-selected="" style="margin-top: 20px; margin-bottom: 20px;">
        <?php foreach ($eventTypes as $eventType) : ?>
            <button class="event-type btn-with-icon-co" style="margin: 0 5px;" data-target="<?= $eventType ?>">
                <?= ucfirst(Yii::t('event', $eventType)) ?>
                <svg class="btn-in-icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                </svg>
            </button>
        <?php endforeach; ?>
    </div>
</div>
<style id="eventTypologyStyle<?= $kunik ?>"></style>

<script>
    (function(W, $) {
        $(function() {
            var php = {
				context: JSON.parse(JSON.stringify(<?= json_encode($parent) ?>)),
				blockCms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
				myCmsId: '<?= $myCmsId ?>',
				kunik: '<?= $kunik ?>'
            };
            $('#eventTypologyStyle<?= $kunik ?>').append(cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>));
            eventTypology(W, $, php);
        });
    })(window, jQuery);
</script>