<?php
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $container = "allmetiers-".$kunik;
  $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
?>
<style class="css-<?= $kunik ?>">
  @import url("https://fonts.googleapis.com/css2?family=Fira+Sans:wght@300;400;600&display=swap");
  :root {
    --primary-color: #37517e;
    --hover-color: #95abd0;
    --text-color: #fff;
  }
  .metiers-<?= $kunik ?> {
    padding: 20px;
  }
  .metiers-<?= $kunik ?> .filters-content {
    flex-direction: column;
    padding: 20px;
  }

  .metiers-<?= $kunik ?> .filters-content,
  .metiers-<?= $kunik ?> .form-group-filters {
    width: 100%;
    position: relative;
    display: flex;
    align-items: flex-start;
  }

  .metiers-<?= $kunik ?> .filters-content label {
    margin-bottom: 10px;
    text-transform: uppercase;
    font-weight: 600;
  }

  .metiers-<?= $kunik ?> .form-group-filters input,
  .metiers-<?= $kunik ?> .form-group-filters select {
    margin-right: 15px;
    border: 1px solid var(--primary-color) !important;
    height: 50px;
    margin-top: 5px;
    border-radius: 50px;
    box-shadow: none;
    width: inherit;
    font-weight: 500;
    font-size: 16px;
  }
  .metiers-<?= $kunik ?> .metier-card-content {
    box-sizing: border-box;
    font-size: 100%;
    margin: 0;
  }

  .metiers-<?= $kunik ?> .metier-card-content img {
    max-width: 100%;
    height: auto;
  }

  .metiers-<?= $kunik ?> .metier-card-content {
    -webkit-text-size-adjust: 100%;
    font-variant-ligatures: none;
    text-rendering: optimizeLegibility;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    font-size: 100%;
    font-family: "Fira Sans", sans-serif;
    width: 100%;
  }

  .metiers-<?= $kunik ?> .metier-card-content h1,
  .metiers-<?= $kunik ?> .metier-card-content h2,
  .metiers-<?= $kunik ?> .metier-card-content h3,
  .metiers-<?= $kunik ?> .metier-card-content h4,
  .metiers-<?= $kunik ?> .metier-card-content h5 {
    font-weight: 800;
    margin-top: 0;
    margin-bottom: 0;
  }


  .metiers-<?= $kunik ?> .metier-data {
    margin-bottom: 20px;
  }
  .metiers-<?= $kunik ?> .card-metier {
    width: 100%;
    height: 500px;
    position: relative;
    overflow: hidden;
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    color: #1F1D42;
    border-radius: 20px;
  }
  
  .metiers-<?= $kunik ?> .card-metier:hover .card-metier__content {
    background-color: var(--primary-color);
    bottom: 100%;
    transform: translateY(100%);
    padding: 50px 60px;
    transition: all 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
  }
  .metiers-<?= $kunik ?> .card-metier:hover .card-metier__extra {
    transform: translateY(0);
    transition: transform 0.35s;
  }
  .metiers-<?= $kunik ?> .card-metier:hover .card-metier__link {
    opacity: 1;
    transform: translate(-50%, 0);
    transition: all 0.3s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
  }
  .metiers-<?= $kunik ?> .card-metier:hover img {
    transform: scale(1);
    transition: 0.35s 0.1s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
  }
  .metiers-<?= $kunik ?> .card-metier__content {
    width: 100%;
    text-align: center;
    background-color: var(--primary-color);
    padding: 0 60px 100px;
    position: absolute;
    bottom: 0;
    left: 0;
    transform: translateY(0);
    transition: all 0.35s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
    will-change: bottom, background-color, transform, padding;
    z-index: 1;
  }
  .metiers-<?= $kunik ?>  .card-metier__content::before, .metiers-<?= $kunik ?>  .card-metier__content::after {
    content: "";
    width: 100%;
    height: 90px;
    background-color: inherit;
    position: absolute;
    left: 0;
    z-index: -1;
  }
  .metiers-<?= $kunik ?>  .card-metier__content::before {
    top: -80px;
    -webkit-clip-path: ellipse(60% 80px at bottom center);
            clip-path: ellipse(60% 80px at bottom center);
  }
  .metiers-<?= $kunik ?>  .card-metier__content::after {
    bottom: -80px;
    -webkit-clip-path: ellipse(60% 80px at top center);
            clip-path: ellipse(60% 80px at top center);
  }
  .metiers-<?= $kunik ?>  .card-metier__title {
    font-size: 2rem !important;
    margin-bottom: 1em !important;
    text-transform: uppercase;
    font-weight: 600;
    text-align: center;
    color: var(--text-color) !important;
  }
  .metiers-<?= $kunik ?>  .card-metier__title span {
    color: #2d7f0b;
  }
  .metiers-<?= $kunik ?>  .card-metier__text {
    font-size: 15px !important;
    text-align: center;
  }
  .metiers-<?= $kunik ?>  .card-metier__link {
    position: absolute;
    bottom: 1rem;
    left: 50%;
    transform: translate(-50%, 10%);
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    text-decoration: none;
    color: var(--text-color);
    font-weight: bolder;
    opacity: 0;
    padding: 10px;
    transition: all 0.35s;
    border-radius: 50px;
    cursor: pointer;
  }
  .metiers-<?= $kunik ?>  .card-metier__link:hover svg {
    transform: translateX(4px);
  }
  .metiers-<?= $kunik ?>  .card-metier__link svg {
    width: 18px;
    margin-left: 4px;
    transition: transform 0.3s;
  }
  .metiers-<?= $kunik ?>  .card-metier__extra {
    height: 70%;
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    width: 100%;
    font-size: 1.2rem;
    text-align: left;
    background-color: var(--text-color);
    padding: 50px;
    bottom: 0;
    z-index: 0;
    color: #dee8c2;
    transform: translateY(100%);
    will-change: transform;
    transition: transform 0.35s;
  }
  .metiers-<?= $kunik ?>  .card-metier__extra span {
    color: #1F1D42;
    font-size: 13px;
    white-space: nowrap;      
    overflow: hidden;         
    text-overflow: ellipsis;
    width: 300px;             
    display: block;  

  }
  .metiers-<?= $kunik ?>  .card-metier img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    -o-object-fit: cover;
      object-fit: cover;
    -o-object-position: center;
      object-position: center;
    z-index: -1;
    transform: scale(1.2);
    transition: 0.35s 0.35s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
  }
  .metiers-<?= $kunik ?>  .metier-code {
    position: absolute;
    top: 0;
    left: 0;
    background-color: var(--primary-color);
    color: #fff;
    padding: 10px 20px;
    font-size: 1.5rem;
    border-radius: 20px 0 20px 0;
  }
  .metiers-<?= $kunik ?>  .metier-code span {
    color: #fff;
    font-weight: 600;
  }
  .metiers-<?= $kunik ?>  .extra-footer {
    font-size: 1.1rem;
    color: #1F1D42;
    position: absolute;
    bottom: 20px;
    left: 15px;
  }
  .aucun-data {
    text-align: center;
    color: var(--primary-color);
  }
</style>

<div class="metiers-<?= $kunik ?>">
  <?php 
    if (isset($blockCms["filters"])) { ?>
      <div class="filters-content">
        <div class="filters-title"><label>Veuillez saisir sur les champ ci-dessous pour filtrer</label></div>
        <div class="form-group-filters form-group">
            <?php foreach ($blockCms["filters"] as $key => $value) {
              if ($value["type"] === "text") { ?>
                <input type="text" class="form-control" id="<?= $value["value"] ?>" placeholder="<?= $value["description"] ?>">
            <?php
              } else if ($value["type"] === "select") { ?>
                <select class="form-control" id="<?= $value["value"] ?>">
                  <option selected><?= $value["description"] ?></option>
                  <?php foreach ($value["options"] as $keyOpt => $valueOpt) { ?>
                    <option value="<?= $valueOpt["value"] ?>"><?= $valueOpt["label"] ?></option>
                  <?php
                  } ?>
                </select>
            <?php
              }
            ?>
              
        <?php  
        } ?>
        </div>
      </div>
  <?php  
    }    
  ?>  
  <div class="metier-card-content row">
      
  </div>
</div>

<script>
  	$(function() {
      sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
      function affiche_data_metiers(data){
        var metiers= Object.values(data)
        $(".metier-card-content").html("");
        if(metiers.length == 0){
          $(".metier-card-content").append(`<div class="aucun-data"><h3>Aucun metier</h3></div>`);
          return;
        }
        $.each(metiers, function(index, metier) {
          $(".metier-card-content").append(`
            <div class="metier-data col-xs-12 col-md-4 col-lg-3">
              <div class="card-metier">
                <div class="metier-code">Code : <span>${metier.job.code}</span></div>
                <div class="card-metier__content">
                  <h3 class="card-metier__title">
                    ${metier.job.title}
                  </h3>
                  <a class="card-metier__text" href="${metier.url}"> ${metier.url} </a>
                  <div href="${metier.url}" class="card-metier__link">
                    <span>En savoir plus</span>
                    <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                    </svg>        
                  </div>
                </div>
                <div class="card-metier__extra">
                  <div class="extra-footer">
                      <h4>Formation associée : </h4>
                      <a href="${typeof metier.training != "undefined" ? metier.training : '#'}"><span>${typeof metier.training != "undefined" ? metier.training : "Pas d'information"}</span></a>
                  </div>
                </div>
                <img src="https://images.unsplash.com/photo-1586511925558-a4c6376fe65f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=60" alt="">
              </div>
            </div>
          `);
        });
      }
      affiche_data_metiers(sectionDyf.<?php echo $kunik ?>BlockCms.metiers);

      function filtre_metiers(titre, code, contrat, tendance) {
        var metiers = Object.values(sectionDyf.<?php echo $kunik ?>BlockCms.metiers);
        var metiers_filtres = metiers.filter(metier => {
          const matchesTitle = titre
            ? metier.job.title.toLowerCase().includes(titre.toLowerCase())
            : true;
      
          const matchesCode = code
            ? metier.job.code.toLowerCase().includes(code.toLowerCase())
            : true;

            const matchesContract = contrat && contrat != "Type de contrat.."
            ? metier.tags.some(tag =>
                tag.toLowerCase().includes(contrat.toLowerCase())
              )
            : true;

          const matchesTrend = tendance
            ? metier.employmentOffers.jobTrends.some(trend =>
                trend.description.toLowerCase().includes(tendance.toLowerCase())
              )
            : true;
    
          return matchesTitle && matchesCode && matchesContract && matchesTrend;
        });
        console.log('metiers_filtres', metiers_filtres);

        affiche_data_metiers(metiers_filtres);
      }

      $(".form-group-filters input").on("keyup", function() {
        if($(this).val() == ""){
          affiche_data_metiers(sectionDyf.<?php echo $kunik ?>BlockCms.metiers);
          return;
        }
        var titre = $("#title").val();
        var code = $("#code").val();
        var contrat = $("#contractTypes").val();
        var tendance = $("#jobTrends").val();
        filtre_metiers(titre, code, contrat, tendance);
      });
      $(".form-group-filters select").on("change", function() {
        if($(this).val() == "" || $(this).val() == "Type de contrat.."){
          affiche_data_metiers(sectionDyf.<?php echo $kunik ?>BlockCms.metiers);
          return;
        }
        console.log('select', $(this).val());
        var titre = $("#title").val();
        var code = $("#code").val();
        var contrat = $("#contractTypes").val();
        var tendance = $("#jobTrends").val();
        filtre_metiers(titre, code, contrat, tendance);
      });
    })
</script>