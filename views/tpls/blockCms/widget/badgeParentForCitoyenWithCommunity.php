<?php
$keyTpl = "badge_parent_block";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>

<style id="badgeparent<?= $kunik ?>">
  .container-block-citoyen-possess-badge<?= $kunik ?> {
    width: 100%;
  }

  #selectBadgeParent<?= $kunik ?> {
    margin: 2%;
    border-radius: 5px;
    max-width: 120px;
    text-overflow: wrap;
    display: block;
    padding: 20px;
  }

  .citoyen-possess-badge-item<?= $kunik ?> {
    height: 100%;
  }

  .citoyen-possess-badge-item<?= $kunik ?> .citoyen-item<?= $kunik ?> {
    width: 90%;
    position: relative;
    border: 1px initial black;
    box-shadow: 0px 0px 4px 0px black;
    padding: 10px;
    margin: 10px;
    border-radius: 10px 10px 10px 10px;
  }

  .citoyen-possess-badge-item<?= $kunik ?> .citoyen-item<?= $kunik ?> .child-<?= $kunik ?> {
    position: relative;
    overflow: hidden;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .img-citoyen-possess-badge<?= $kunik ?> {
    width: 100%;
    height: auto;
    
  }


  .container-info-badge-child<?= $kunik ?> {
    position: relative;
  }

  .name-badge-child-<?= $kunik ?> {
    cursor: pointer;
  }

 /*  .tags-list {
    background: transparent;
    border: 1px solid #00ff00;
    border-radius: 10px;
    margin:1px;
    padding: 1px;
  } */

  ::-webkit-scrollbar {
    width: 10px;
    height: 10px;
  }

  ::-webkit-scrollbar-track {
    background-color: white;
  }

  ::-webkit-scrollbar-thumb {
    background: #7c7c7c96;
    border-radius: 10px;
  }
</style>
<div class="badgeparent <?= $kunik ?> badgeparent<?= $kunik ?>">
  <div id="container-block-citoyen-possess-badge<?= $kunik ?>" class="row container-block-citoyen-possess-badge<?= $kunik ?>">
    <?php if (isset($blockCms) && isset($blockCms["badge"]) && isset($blockCms["badge"]["parent"])) { ?>

    <?php } else { ?>
      <a id="selectBadgeParent<?= $kunik ?>" class="bg-dark text-white" href="javascript:;">
        <i class="fa fa-2x fa-bookmark"></i> <br>
        Selectionner le(s) badge(s) que doit posseder le(s) citoyen(s)
      </a>
    <?php } ?>
  </div>
</div>

<script type="text/javascript">
  const classContainerIdBadgeInDynform = "element-finder-";
  var idBadgeParent = [];
  var mybadgeInBlockParent = null;
  


  jQuery(document).ready(function() {
    var parentBadgeInBlock = {};
    const classOfParentFinder = ".parentfinder .element-finder";
    const classOfTemplateFinder = ".templateChoiseForPossessBadgefinder .element-finder";
    const myBlockCmsInBadgeParent = <?= json_encode($blockCms) ?>;
    const kunikJs = "<?php echo $kunik ?>";
    const defaultProfil = modules.co2.url + "/images/avatar.jpg";
    var idTemplate = "";

    function bindClickSelectParentBadge() {
      $("#container-block-citoyen-possess-badge" + kunikJs + " #selectBadgeParent" + kunikJs).off().on("click", function() {
        showDynForm();
      });
    }

    function bindClickNameCitoyen() {
      $(".name-badge-child-"+kunikJs).off().on("click", function(){
        clickNameCitoyenAction($(this).data("key"), $(this).data("name")) ;
      });
    }

    function bindValidateDynForm() {
      $(".btn-send-dynform-" + kunikJs).off().on("click", function() {
        idBadgeParent = [];
        parentBadgeInBlock = {};
        $.each($(classOfParentFinder), function(key, val) {
          getIdAndNameParent($(val).attr("class"));
        });

        $.each($(classOfTemplateFinder), function(key, val) {
          getIdTemplate($(val).attr("class"));
        });

        hideDynForm();
        getChild();
        setParentBadgeInBlockCms(parentBadgeInBlock);
      });
    }

    function clickNameCitoyenAction(keyApp, subdomaineName) {
      if(costum != null && (typeof costum.app == "undefined" || (typeof costum.app != "undefined" && typeof costum.app[keyApp] == "undefined") ) ) {
        createNewPagePortfolio(keyApp, subdomaineName)
      }
      else{
        redirectToTemplate(keyApp)
      }
    }

    function useTemplate(keyApp) {
      if(idTemplate != ""){
		var params = {
			"id"		 : idTemplate,
			"parentId"   : costum.contextId,
			"page" 		 : keyApp,
			"parentSlug" : costum.contextSlug,
			"parentType" : costum.contextType
		};
		ajaxPost(
			null,
			baseUrl + "/co2/template/use",
			params,
			function (data) {
				redirectToTemplate(keyApp);
            	window.location.reload();
			},
			{ async: false }
		);
      }
    }

    function redirectToTemplate(keyApp) {
      urlCtrl.loadByHash(keyApp);
    }

    function createNewPagePortfolio(keyApp, subdomaineName) {
      var pageParams = {};
      pageParams["app."+keyApp] = {
        "name": {fr : subdomaineName},
        "metaDescription": "",
        "urlExtra": "/page/"+ keyApp.replace("#", ""),
        "hash": "#app.view"
      }

      ajaxPost(
        null,
        baseUrl+"/"+moduleId+"/cms/updatecostum",
        {
          params: pageParams
        },
        function(data){
          useTemplate(keyApp);
        }			
      );
      
    }

    function checkKeyApp(citoyen) {
      var keyApp = "#";
      if(citoyen != null){
        if(typeof citoyen.slug != "undefined")
          keyApp += citoyen.slug;
        else if(typeof citoyen.name != "undefined") {
          let name = citoyen.name.toLowerCase();
          alert(name);
          keyApp += name.replaceAll(" ", "-");
        }
      }
      return keyApp
    }

    function getIdAndNameParent(allClassBadge) {
      let classBadge = allClassBadge.split(" ");
      for (var i = 0; i < classBadge.length; i++) {
        let id = classBadge[i].split(classContainerIdBadgeInDynform);

        if (id.length > 1 && typeof id[1] != "undefined") {
          idBadgeParent.push(id[1]);
          parentBadgeInBlock[id[1]] = $("." + classContainerIdBadgeInDynform + id[1] + " .info-contact .name-contact").html();
        }

      }
    }

    function getIdTemplate(allClassTemplate) {
      let classTemplate = allClassTemplate.split(" ");
      for (var i = 0; i < classTemplate.length; i++) {
        let id = classTemplate[i].split(classContainerIdBadgeInDynform);

        if (id.length > 1 && typeof id[1] != "undefined") {
          idTemplate = id[1];
        }

      }
    }

    function getChild() {
      var params = {
        "parent": idBadgeParent,
        "contextId" : costum.contextId,
        "contextType" : costum.contextType
      };

      ajaxPost(
        null,
        baseUrl + "/co2/badges/getcitoyen-possess-badge",
        params,
        function(data) {
          refresh(data);
        },
        null
      );
    }

    function refresh(data = null) {
      var str = "";
      if (data != null && typeof data.data != "undefined" && notEmpty(data.data) && typeof data.res != "undefined" && data.res) {
        $.each(data.data, function(key, citoyen) {
          if (citoyen != null) {

            if(typeof citoyen.roles == "undefined" || (typeof citoyen.roles != "undefined" && typeof citoyen.roles.superAdmin == "undefined")){
              if( typeof citoyen.profilImageUrl == "undefined")
                citoyen.profilImageUrl = defaultProfil;

              var keyApp = checkKeyApp(citoyen);
              
              str += `
                      <div class="citoyen-possess-badge-item` + kunikJs + `" >
                          <div class="citoyen-item` + kunikJs + ` container-citoyen-possess-badge">
                              <div class="child-` + kunikJs + ` image-citoyen-possess-badge">`;

              str += `
                      <img class="img-citoyen-possess-badge` + kunikJs + ` " src="` + citoyen.profilImageUrl + `">
                  </div>
                  <div class="container-info-badge-child` + kunikJs + `">
                      <h3 data-key="`+keyApp+`" data-name="`+citoyen.name+`" class="name-badge-child-` + kunikJs + ` name-citoyen-possess-badge">` + citoyen.name + `</h3>`;


               if (typeof citoyen.roles != "undefined" && citoyen.roles != null && citoyen.roles.length > 0){
                  for(var i = 0; i < citoyen.roles.length; i++){
                    if(typeof citoyen.roles[i] != "undefined")
                      str += `<p class="post-citoyen-possess-badge">` + citoyen.roles[i] + `</p>`;
                  }
                  
               }

              if(typeof citoyen.badges != "undefined" && citoyen.badges != null) {
                str += "<div class='badge-item-style'>";
                $.each(citoyen.badges, function(kk, vv){
                  if(vv != null && typeof vv.name != "undefined" && !idBadgeParent.includes(kk) ){
                    str += "<span>"+vv.name+", </span> ";
                  }
                });
                str += "</div>";
              }
                

              if(typeof citoyen.tags != "undefined" && citoyen.tags != null) {
                str += "<div class='tags-item-style'>";
                for(let i = 0; i < citoyen.tags.length; i++ ){
                  str += "<span class='tags-list '>"+citoyen.tags[i]+"</span> ";
                  if(typeof citoyen.tags[i + 1] != "undefined")
                    str += "<span>, </span>"; 
                }
                str += "</div>";
              }

              str += `</div></div></div>`;
            }
          }
        });
      } else {
        str += `
                <a id="selectBadgeParent` + kunikJs + `" class="bg-dark text-white" href="javascript:;">
                    <i class="fa fa-2x fa-bookmark"></i> <br>
                    Selectionner le(s) badge(s) que doit posseder le(s) citoyen(s)
                </a>
            `;
      }

      $("#container-block-citoyen-possess-badge" + kunikJs).html(str);
      let width = $(".badgeparent"+ kunikJs).width();
      
      if(width > 1000)
        $(".citoyen-possess-badge-item" + kunikJs).removeClass("col-lg-6 col-md-6").removeClass("col-lg-5 col-md-5").addClass("col-lg-3 col-md-3");
      else if(width > 800)
        $(".citoyen-possess-badge-item" + kunikJs).removeClass("col-lg-3 col-md-3").removeClass("col-lg-5 col-md-5").addClass("col-lg-4 col-md-4")
      else 
        $(".citoyen-possess-badge-item" + kunikJs).removeClass("col-lg-4 col-md-4 ").removeClass("col-lg-3 col-md-3").addClass("col-lg-5 col-md-5");
      
      bindClickSelectParentBadge();
      bindClickNameCitoyen();
    }

    function hideDynForm() {
      $("#ajax-modal").modal("toggle");
    }

    function setParentBadgeInBlockCms(parentId) {
      var params = {
        "parent": parentId,
        "id": "<?= $myCmsId ?>",
        "template" : idTemplate,
      };

      ajaxPost(
        null,
        baseUrl + "/co2/badges/setbadgeparentinblockcms",
        params,
        function(data) {},
        null
      );
      // }
    }

    function showDynForm() {
      myDynForm = {
        jsonSchema: {
          title: "SELECTIONNER LE(S) BADGE(S) QUE DOIT POSSEDER LE(S) CITOYEN(s)(S)",
          icon: " bookmark-o",
          type: "object",
          onLoads: {
            onload: function(data) {

            }
          },
          beforeBuild: function() {},

          beforeSave: function(data, callB) {},
          afterSave: function(data, callB) {
            dyFObj.commonAfterSave(data, callB);
          },
          properties: (() => {

              return {
                parent: {
                  inputType: "finder",
                  label: "badge parent",
                  buttonLabel: "Changer le(s) badge(s) parent(s)",
                  //multiple : false,
                  initMe: false,
                  placeholder: tradBadge.findExistingBadge,
                  rules: {
                    required: true
                  },
                  initType: ["badges"],
                  initBySearch: true,
                  openSearch: true,
                },
                templateChoiseForPossessBadge: {
                  inputType: "finder",
                  label: "Template pour le Portfolio",
                  buttonLabel: "Choisir un template",
                  multiple : false,
                  initMe: false,
                  placeholder: "Nom de template existant",
                  rules: {
                    required: true
                  },
                  initType: ["templates"],
                  initBySearch: true,
                  openSearch: true,
                },
                
              }
            })
            ()
        }
      };

      let initDataParent = {}
      for (let i = 0; i < idBadgeParent.length; i++) {
        initDataParent[idBadgeParent[i]] = idBadgeParent[i];
      }

      dyFObj.openForm(myDynForm, null, initDataParent);
      $("#ajaxFormModal .form-actions").hide();

      $("#ajaxFormModal").append(`
            <div class=" form-group parentfinder">
                <div class="finder-parent">
                    <a href="javascript:;" class="form-control col-xs-6 btn btn-success btn-send-dynform-` + kunikJs + ` margin-bottom-10">Valider <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        `);

      bindValidateDynForm();

    }
    
    bindClickSelectParentBadge();
    if(typeof myBlockCmsInBadgeParent != "undefined" && myBlockCmsInBadgeParent != null && typeof myBlockCmsInBadgeParent.template != "undefined" && myBlockCmsInBadgeParent.template != "") {
      idTemplate = myBlockCmsInBadgeParent.template; 
    }

    if (typeof myBlockCmsInBadgeParent != "undefined" && myBlockCmsInBadgeParent != null && typeof myBlockCmsInBadgeParent.badge != "undefined" && typeof myBlockCmsInBadgeParent.badge.parent != "undefined" && myBlockCmsInBadgeParent.badge.parent != null) {
      mybadgeInBlockParent = myBlockCmsInBadgeParent.badge.parent;
      idBadgeParent = [];
      parentBadgeInBlock = {};
      $.each(mybadgeInBlockParent, function(key, val) {
        idBadgeParent.push(key);
        parentBadgeInBlock[key] = val;
      });
      getChild();
    }
  });
</script>

<script type="text/javascript">

  str = "";
  str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#badgeparent<?= $kunik ?>").append(str);
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;

    badgeparent = {
      configTabs: {
        general: {
          inputsConfig: [
            "width",
            "height",
            "background",
            {
              type: "section",
              options: {
                name: "container-citoyen-possess-badge",
                label: "Container",
                showInDefault: true,
                inputs: [
                  "background",
                  "width",
                  "height",
                ]
              }
            },
          ]
        },
        style: {
          inputsConfig: [
            "addCommonConfig",
            {
              type: "section",
              options: {
                name: "image-citoyen-possess-badge",
                label: "Image",
                showInDefault: true,
                inputs: [
                  "width",
                  "height",
                  "border",
                  "borderRadius",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "name-citoyen-possess-badge",
                label: "Nom",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "post-citoyen-possess-badge",
                label: "Poste",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "badge-item-style",
                label: "Badge",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                ]
              }
            },
            {
              type: "section",
              options: {
                name: "tags-item-style",
                label: "Specialisation",
                showInDefault: true,
                inputs: [
                  "color",
                  "background",
                  "fontSize",
                  "fontFamily",
                  "fontStyle",
                  "textTransform",
                  "fontWeight",
                  "textAlign",
                ]
              }
            },
          ],
        },
        hover: {
          inputsConfig: [
            "addCommonConfig"
          ],
        },
        advanced: {
          inputsConfig: [
            "addCommonConfig"
          ],
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    };
    cmsConstructor.blocks.<?= $kunik ?> = badgeparent;
  }
</script>