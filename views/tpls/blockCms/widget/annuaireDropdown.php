<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) [$kunik => $objectCss];
  $annuaireIcon =  $blockCms["icon"]["iconStyle"] ?? "";
  $size =  $blockCms["titre"]["fontTitre"] ?? "36px";
  $color =  $blockCms["titre"]["colorTitre"] ?? "black";
?>
<style id="css-<?= $kunik ?>">

    .carto-<?= $kunik?>{
        margin-top : -7%;
    }

    .carto-d-<?= $kunik?> {
        box-shadow: 0px 0px 20px -2px #777;
        width: 80%;
        left: 10%;
        background : white;
        font-size : 1.5vw;
        border-radius: 0.2em;
    }
    .texte-content-<?= $kunik?> {
        display: flex;
        justify-content : center;
    }
    .icones-content-<?= $kunik?> {
        width : 10% !important;
        text-align : right !important;
    }

    .title-content-<?= $kunik?> {
        text-align : center;
    }
    .title-content-<?= $kunik?> .titre {
        font-size : <?= $size; ?> !important;
        color : <?= $color; ?>  !important;
        text-transform : none !important;
    }
    .title-carto-<?= $kunik?>{
        font-size : 200%;
        text-shadow: 0px 0px 7px gray;
    }
    .c-description-<?= $kunik?>{
        width: 80%;
        left: 10%;
        font-size: 2vw;
        margin-top: 3%;
    }

    .carto-<?= $kunik?> .px-4{
        padding: 20px !important;
    }

</style>

<div class="carto-<?= $kunik?> row content-<?= $kunik?> <?= $kunik?>" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
    <div class="carto-h1-<?= $kunik?> col-xs-6 col-sm-6 no-padding">
        <div class="sp-text img-text-bloc title-carto-<?= $kunik?> hidden-sm hidden-xs title-2" style="margin-left: 19.5%;" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="textSlogan">
            <?php echo $blockCms["textSlogan"];?>
        </div>
    </div>
    <div class="carto-d-<?= $kunik?> col-xs-12 col-sm-12">
        <div class="row">
            <div class=" <?php echo (strlen($blockCms["image"]) > 45)?'col-sm-7 col-md-7 col-lg-7 text-left':'col-sm-12 col-md-12 col-lg-12 text-center' ?> px-4">
                    <div class="texte-content-<?= $kunik?>">
                        <a href="javascript:;" data-hash="<?= $blockCms["lien"]; ?>" class="lbh-menu-app" style="text-decoration : none;">
                            <span class="title-content-<?= $kunik?>">
                                <h1 class="titre">
                                    <?php if(strlen($annuaireIcon) > 6)
                                        echo '<i class="'.$kunik.'-icon icon '.$annuaireIcon.'"></i>';
                                    ?>
                                    <?= strlen($blockCms["titre"]["title"]) > 1 ? $blockCms["titre"]["title"] : 'CARTO/ANNUAIRE';?>
                                </h1>
                            </span>
                        </a>
                    </div>
                <div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content">
                    <?php echo $blockCms["content"]; ?>
                </div>
            </div>
            <?php if(strlen($blockCms["image"]) > 45) {?>
                <div class="col-md-5 col-sm-5 col-lg-5 text-right">
                    <a href="javascript:;" data-hash="#search" class="lbh-menu-app px-4" style="text-decoration : none;">
                        <img src="<?php echo $blockCms["image"]; ?>" class="img-responsive" style="max-height: 230px">
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- Carto NEWS -->
    <div class="row">
        <div class="c-description-<?= $kunik?> col-sm-12 col-xs-12">
            <div class="description col-md-6">
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function() { 
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var annuaireInput = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type : "section",
                                options : {
                                    name : "icon",
                                    label: tradCms.icon,
                                    inputs : [
                                        {
                                            type: "inputIcon",
                                            options: {
                                                name: "iconStyle",
                                                label: tradCms.selectIcon,
                                            },
                                        },
                                        {
                                            type: "fontSize",
                                            options: {
                                                name: "fontSize",
                                                label: tradCms.iconSize,
                                            },
                                        }
                                        
                                    ],
                                }
                            },

                            {
                                type : "section",
                                options : {
                                    name : "titre",
                                    label: tradCms.title,
                                    inputs : [
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "title",
                                                label: tradCms.directoryTitle,
                                            },
                                        },
                                        {
                                            type: "color",
                                            options: {
                                                name: "colorTitre",
                                                label: tradCms.titleColor,
                                            },
                                        },
                                        {
                                            type: "fontSize",
                                            options: {
                                                name: "fontTitre",
                                                label: tradCms.size,
                                            },
                                        }
                                    ],
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                name : "lien",
                                    label : tradCms.link,
                                    collection : "cms"
                                }
                            },
                            {
                                type : "inputFileImage",
                                options : {
                                    name : "image",
                                    label : tradCms.imageOnTheRight,
                                    collection : "cms"
                                }
                            }
                        ]
                    },
                    style: {
                        inputsConfig: [
                            "addCommonConfig",
                            "boxShadow"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },

                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                },

                onChange: function(path,valueToSet,name,payload,value){
                    if (name == "iconStyle"){
                        $('.' + cmsConstructor.kunik +"-icon").removeClass(function (index, css) {
                            return (css.match(/\bfa-\S+/g) || []).join(' '); // removes class that starts with "fa-"
                        });
                        
                        $('.' + cmsConstructor.kunik +"-icon").addClass("fa fa-" + value)
                    }
                }
            }
            cmsConstructor.blocks.annuaireDropdown = annuaireInput;
        }
    });

</script>