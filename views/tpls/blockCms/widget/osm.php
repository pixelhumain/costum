<?php
$keyTpl = "";
$myCmsId  = $blockCms["_id"]->{'$id'};
$styleCss = (object) [$kunik => $blockCms["css"] ?? []];

$cssAnsScriptFilesModule = array(
    '/js/osm/osmType.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule("co2")->assetsUrl);

HtmlHelper::registerCssAndScriptsFiles(array(
    "/plugins/Chart-2.8.0/Chart.min.js",
), Yii::app()->request->baseUrl);

?>

<style type="text/css">
    .border-green {
        border: 1px solid #9fbd38;
    }

    .table-osm-observatoire .tabling-osm thead tr {
        background-color: #086784;
        color: #ffffff;
        text-align: left;
    }


    .effectif .row {
        padding: 1em 0.2em;
        border: 2px solid #ddd
            /*#002861*/
        ;
        border-radius: 2px;
        margin: 2px;
        /*background: #ddd;*/
    }

    .dash-icon {
        font-size: 2em;
        border-radius: 50%;
        padding: 0.8em;
        margin-top: 0.3em;
        margin-bottom: auto;
        background: #eee;
    }

    .text-theme {
        color: #4E54C9;
    }

    .container-filter-search-osm-name {
        margin-bottom: 25px;
    }

    #modeMapOsm<?= $keyTpl ?> {
        color: #000 !important;
        text-decoration: none;
        text-align: right;
    }

    .dropdown-osm-filter-observatoire {
        margin-bottom: 5px;
        margin-top: 5px;
        margin-right: 10px;
        padding-top: 0px;
        padding-bottom: 0px;
        display: inline-block;
        float: left;
    }

    .container-osm-filter .container-filter-search-osm-name .input-group-addon {
        background-color: #9fbd38 !important;
    }

    .dropdown-osm-filter-observatoire .btn-menu,
    .container-osm-filter .search-bar {
        border: 1px solid #9fbd38 !important;
    }

    .dropdown-osm-filter-observatoire .btn-menu {
        padding: 10px 15px;
        font-size: 16px;
        border: 1px solid #6b6b6b;
        color: #6b6b6b;
        top: 0px;
        position: relative;
        border-radius: 20px;
        text-decoration: none;
        background: white;
        line-height: 20px;
        display: inline-block;
        margin-left: 5px;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu {
        position: absolute;
        overflow-y: visible !important;
        top: 50px;
        left: -55px;
        width: 280px;
        border-radius: 2px;
        border: 1px solid #6b6b6b;
        padding: 0px;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu .list-filters {
        max-height: 300px;
        overflow-y: scroll;
    }

    .dropdown-osm-filter-observatoire .dropdown-menu {
        font-size: 14px;
        text-align: left;
        list-style: none;
    }

    .container-osm-filter .dropdown-menu .list-filters button:hover {
        background-color: #e8e8e8;
    }

    .map-menu-left {
        background-color: #2B3136;
    }

    .map-menu-left .map-menu-left_title {
        color: #ffffff;
        background-color: #2B3136;
        font-size: 18px;
    }

    .osm-filter-switch-container .switch-text-label {
        min-width: 85% !important;
        max-width: 85%;
    }

    .wborder {
        border: 2px solid #ddd
            /*#002861*/
        ;
        border-radius: 0px;
    }

    /* .headerSearchContainer .toolbar-item {
        display: flex;
        flex: 0 1 auto;
        flex-flow: column wrap;
        justify-content: center;
    }

    .headerSearchContainer .toolbar-item .item-content {
        display: flex;
        flex: 0 1 auto;
        flex-flow: row nowrap;
        justify-content: center;
        height: 40px;
        width: auto;
        margin: 0 5px;
    }

    .headerSearchContainer button.bar-button {
        flex: 0 0 auto;
        flex-flow: row nowrap;
        align-items: center;
        padding: 0 10px;
        min-width: 30px;
        white-space: nowrap;
        display: flex;
        font-weight: bold;
    }

    .headerSearchContainer .joined>* {
        border-radius: 0;
        border-right: 1px solid rgba(0,0,0,.5);
    }

    .headerSearchContainer button {
        text-align: center;
        border: 0;
        background: #fff;
        color: #333;
        font-size: 12px;
        display: inline-block;
        border-radius: 4px;
    }

    .headerSearchContainer button {
        cursor: pointer;
    } */
     /* HTML: <div class="loader"></div> */
    /* HTML: <div class="loader"></div> */
    /* common */
    .loading {
        font-size: 50px;
        font-family: "Montserrat", sans-serif;
        font-weight: 800;
        text-align: center;
    }
    .loading span {
        display: inline-block;
        margin: 0 -0.05em;
    }
    .loading02 span {
        animation: loading02 1.2s infinite alternate;
    }
    .loading02 span:nth-child(2) {
    animation-delay: 0.2s;
    }
    .loading02 span:nth-child(3) {
        animation-delay: 0.4s;
    }
    .loading02 span:nth-child(4) {
        animation-delay: 0.6s;
    }
    .loading02 span:nth-child(5) {
        animation-delay: 0.8s;
    }
    .loading02 span:nth-child(6) {
        animation-delay: 1s;
    }
    .loading02 span:nth-child(7) {
        animation-delay: 1.2s;
    }
    .loading02 span:nth-child(8) {
        animation-delay: 1.4s;
    }
    .loading02 span:nth-child(9) {
        animation-delay: 1.6s;
    }
    .loading02 span:nth-child(10) {
        animation-delay: 1.8s;
    }
    .loading02 span:nth-child(11) {
        animation-delay: 2s;
    }
    .loading02 span:nth-child(12) {
        animation-delay: 2.2s;
    }
    .loading02 span:nth-child(13) {
        animation-delay: 2.4s;
    }
    .loading02 span:nth-child(14) {
        animation-delay: 2.6s;
    }
    .loading02 span:nth-child(15) {
        animation-delay: 2.8s;
    }
    .loading02 span:nth-child(16) {
        animation-delay: 3s;
    }
    .loading02 span:nth-child(17) {
        animation-delay: 3.2s;
    }
    .loading02 span:nth-child(18) {
        animation-delay: 3.4s;
    }
    .loading02 span:nth-child(19) {
        animation-delay: 3.6s;
    }

    @keyframes loading02 {
        0% {
            filter: blur(0);
            opacity: 1;
        }
        100% {
            filter: blur(2px);
            opacity: 0.2;
        }
    }
    #loader-osm-<?= $kunik ?> {
        position: absolute;
        width: 100%;
        height: calc(100vh - 90px);
        /* display: none; */
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 999999;
        background-color: #00000066;

    }
</style>


<div id="loader-osm-<?= $kunik ?>" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-center">
    <div class="loading loading02">
        <span>C</span>
        <span>H</span>
        <span>A</span>
        <span>R</span>
        <span>G</span>
        <span>E</span>
        <span>M</span>
        <span>E</span>
        <span>N</span>
        <span>T</span>
        <span></span>
        <span>E</span>
        <span>N</span>
        <span></span>
        <span>C</span>
        <span>O</span>
        <span>U</span>
        <span>R</span>
        <span>S</span>
    </div>
</div>
<div class="container-osm-observatoire col-md-12">

    <div id="main-container-osm-observatoire" class="main-container-osm-observatoire">
        <div class="panel-group container-osm-filter">
            <div id="activeFilters-osm-observatoire">
                <div id="activeFilters" class="col-xs-12 no-padding">

                </div>
            </div>

        </div>

        <div class=" no-padding col-xs-12" style="top: inherit; left: inherit; right: inherit;margin-bottom: 10px;">
            <div class="col-xs-12">
                <div class=" no-padding col-xs-12" style="top: inherit; left: inherit; right: inherit;">
                    <div class="col-xs-12">
                        <div class="headerSearchleft col-xs-8 elipsis no-padding">
                            <div id="modeMapOsm<?= $keyTpl ?>">
                                <h5 id="titleMode<?= $keyTpl ?>"></h5>
                            </div>
                            <!-- <button class="add-point pull-left" aria-disabled="false" aria-pressed="false">
                                Point
                            </button>
                            <button class="add-line pull-left" aria-disabled="false" aria-pressed="false">
                                Ligne
                            </button>
                            <button class="add-area pull-left" aria-disabled="false" aria-pressed="false">
                                Surface
                            </button> -->
                        </div>
                        <div class="col-xs-4 text-right no-padding">
                            <button onclick="onContributionMode()" type="button" class="btn-show-map btn-contribution-map hidden-xs" id="btn-contribution-osm" title="Changer de Mode" alt="Changer de Mode">
                                Mode Contributeur
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="map-container-osm-observatoire" style="height: 600px;">
            <div class="row">
                <div class="col-md-3 hidden-xs " id="container-menu-right-in-osm-map">
                    <div class="col-sm-12 no-padding menuRight_search">
                        <div class="map-menu-left col-sm-12 padding-10">
                            <div class="col-xs-6"><span class="map-menu-left_title"><span id="resultdivMap" class=""></span> Résultats </span></div>
                            <div class="col-xs-2 pull-right map-menu-left_title">
                                <button type="button" class="btn btnHeader dropdown-toggle map-menu-left_title" id="btn-filtersdivMap" data-toggle="dropdown"><i class="fa fa-filter"></i></button>
                                <ul id="map-menu-left-container-filter-lieu" class="dropdown-menu panel_filter panel_map map-menu-left_title" role="menu" aria-labelledby="panel_filter" style="overflow-y: auto; max-height: 400px;">
                                    <h3 class="title_panel_map"><i class="fa fa-angle-down"></i> Filtrer les résultats par Source de donnees</h3>
                                    <div class="source-list-osm-filter">
                                        <button class="item_panel_map btn-select-source-osm-filter" data-tag="isCoData" data-source="Communecter"><i class="fa fa-tag"></i>Communecter<span class="badge text-white bg-turq" id="count-data-in-co"></span></button>
                                        <button class="item_panel_map btn-select-source-osm-filter" data-tag="isOsmData" data-source="Open Street Map"><i class="fa fa-tag"></i>Open Street Map<span class="badge text-white bg-turq" id="count-data-in-osm"></span></button>
                                        <button class="item_panel_map btn-select-source-osm-filter" data-tag="madatlasContribution" data-source="MadAtlas"><i class="fa fa-tag"></i>MadAtlas<span class="badge text-white bg-turq" id="count-data-in-madatlas"></span></button>
                                    </div>
                                </ul>
                            </div>
                            <div class="col-xs-2 pull-right map-menu-left_title">
                                <button type="button" class="btn btnHeader dropdown-toggle map-menu-left_title" id="btn-paneldivMap" data-toggle="dropdown"><i class="fa fa-tags"></i></button>
                                <ul class="dropdown-menu panel_map panel_map2 pull-right map-menu-left_title" role="menu" aria-labelledby="panel_map" style="overflow-y: auto; max-height: 400px;">
                                    <h3 class="title_panel_map"><i class="fa fa-angle-down"></i>Filtrer les résultats par types</h3>
                                    <div class="list-type-filter">

                                    </div>

                                </ul>
                            </div>
                        </div>
                        <div id="menuRight_search_filtersdivMap" class="col-sm-12 no-padding menuRight_search_filters"><input class="form-control date-range active" id="filterByNameInOsmBlock" type="text" placeholder="Filtrer par nom"></div>
                        <div class="menuRight_body col-sm-12 no-padding liste_map_element" id="container-list-item-osm-map" style="height: 420px;">
                        </div>
                    </div>


                </div>
                <div style="height: 600px;" class="col-md-9 mapBackground no-padding divMap" id="divMap">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var mapAbout = {};
    var typeDataToGet = {};
    var countForTypeFilter = {};
    var locationValue = [];
    var allData;
    var blockCms;
    var typeFilter = [];
    var searchNameOsmData = "";
    var allMarker = [];
    var mymarkersCluster;
    var map<?= $kunik ?> = null;
    var contributionMode = false;
    var addContributionMode = false;
    var myMarkerAdd = null;
    const madatlas = [
        "MADATLAS", "Madatlas" , "madatlas" , "madAtlas", "MadAtlas", "MADAATLAS", "Madaatlas" , "madaatlas" , "madaAtlas", "MadaAtlas"
    ];
    var sourceFilter = [];
    var buttonAddInContribution = null;
    var buttomConfirmAddContribution = null;

    var myMarker, myCircle, myZoomed;

    function onContributionMode() {
        contributionMode = !contributionMode;
        if (contributionMode) {
            $("#container-menu-right-in-osm-map").fadeOut(1000);
            $("#divMap").removeClass("col-md-9").addClass("col-md-12");
            $("#btn-contribution-osm").html("Mode Visiteur");
            addButtonAddContribution();
            $("#titleMode<?= $keyTpl ?>").html("Mode Contributeur: Tous vos modifications serons envoyé directement vers OpenStreetMap");      
        } else {
            $("#divMap").removeClass("col-md-12").addClass("col-md-9");
            $("#container-menu-right-in-osm-map").fadeIn(1000);
            $("#btn-contribution-osm").html("Mode Contributeur");
            addContributionMode = false;
            removeButtonAddContribution();
            $("#titleMode<?= $keyTpl ?>").html("");
        }
    }

    function onAddContribution() {
        addContributionMode = !addContributionMode;
        if(addContributionMode) {
            mymarkersCluster.clearLayers();
            $.each(allMarker, function(key, val) {
                if (val != null) 
                    val.remove();
            });
            addDraggableMarker();
            addBouttonConfirmAddPoint();
        }
        else {
            refreshListe();
        }
    }

    function addDraggableMarker() {
        let bounds =  map<?= $kunik ?>.getMap().getBounds();
        let center = bounds.getCenter();

        let customIcon = L.icon({
            iconUrl: modules.map.assets + '/images/markers/osm/add.png',
            iconSize: [60, 60], 
            iconAnchor: [20, 40] 
        });
        
        myMarkerAdd = L.marker(center, { icon : customIcon, draggable: true }).addTo(map<?= $kunik ?>.getMap());

        if(buttonAddInContribution != null) {
            map<?= $kunik ?>.getMap().removeControl(buttonAddInContribution);
            buttonAddInContribution = null;
        }
    }

    function addBouttonConfirmAddPoint() {
        var buttomConfirmAddContribution = null;
        if(contributionMode && addContributionMode) {
            if(buttonAddInContribution != null) {
                map<?= $kunik ?>.getMap().removeControl(buttonAddInContribution);
                buttonAddInContribution = null;
            }

            buttomConfirmAddContribution = L.control({
                position: 'bottomleft'
            });

            buttomConfirmAddContribution.onAdd = function(map) {
                var div = L.DomUtil.create('div', 'info button btn-confirm-in-contribution');
                var str = `
                    <p class="row" style="margin-left:5px;">
                        <span>
                            <button href="#" onClick="openAddContribution()" class="btn btn-success" style="color:#ffffff;"><i class="fa fa-check"></i></button>
                        </span>
                        <span>
                            <button href="#" onClick="cancelAddContribution()" class="btn btn-danger" style="color:#ffffff;"><i class="fa fa-times"></i></button>
                        </span>
                    </p>
                `;
                div.innerHTML = str;
                return div;
            }
            buttomConfirmAddContribution.addTo(map<?= $kunik ?>.getMap());
        }

    }

    function cancelAddContribution() {
        if(buttomConfirmAddContribution != null) {
            map<?= $kunik ?>.getMap().removeControl(buttomConfirmAddContribution);
            buttomConfirmAddContribution = null;
        }

        $(".btn-confirm-in-contribution").remove();
        addContributionMode = true;
        onAddContribution();
    }

    function openAddContribution() {
        let position = myMarkerAdd.getLatLng(); 
        var params = {
            lat : position.lat,
            lon : position.lng,
        };
        
        urlCtrl.openPreview("/view/url/costum.views.custom.osm.add", params);
    }

    function addButtonAddContribution() {
        if(contributionMode) {
            buttonAddInContribution = L.control({
                position: 'bottomleft'
            });

            buttonAddInContribution.onAdd = function(map) {
                var div = L.DomUtil.create('div', 'info button btn-add-in-contribution');
                var str = `
                    <p class="row" style="margin-left:5px;">
                        <span>
                            <button href="#" onClick="onAddContribution()" class="btn btn-success" style="color:#ffffff;"><i class="fa fa-plus"></i></button>
                        </span>

                    </p>
                `;
                div.innerHTML = str;
                return div;
            }
            buttonAddInContribution.addTo(map<?= $kunik ?>.getMap());
        }
    }

    function removeButtonAddContribution() {
        if(!contributionMode) {
            if(buttonAddInContribution != null) {
                map<?= $kunik ?>.getMap().removeControl(buttonAddInContribution);
                buttonAddInContribution = null;
            }
        }

        if(myMarkerAdd != null && (!contributionMode || !addContributionMode))
            myMarkerAdd.remove();
    }

    function addFilter(type, name) {
        var indexOfvalue = typeFilter.indexOf(type);
        if (indexOfvalue != -1) {
            removeFilter(type, indexOfvalue);
        } else {
            typeFilter.push(type);

            $("#activeFilters-osm-observatoire #activeFilters").append(`
                <div class="filters-activate tooltips active-filter-osm` + type + `" data-position="bottom" data-title="Effacer" data-type="` + type + `">
                    <i class="fa fa-times-circle"></i>
                    <span class="activeFilters" data-type="` + type + `">` + name + `</span>
                </div>
            `);
            bindClickBtnDeleteFilter();
        }

        refreshListe();
    }

    function addFilterSource(tag, source) {
        var indexOfvalue = sourceFilter.indexOf(tag);
        if (indexOfvalue != -1) {
            removeFilterSource(tag);
        } else {
            sourceFilter.push(tag);

            $("#activeFilters-osm-observatoire #activeFilters").append(`
                <div class="filters-activate tooltips active-filter-osm` + tag + `" data-position="bottom" data-title="Effacer" data-tag="` + tag + `">
                    <i class="fa fa-times-circle"></i>
                    <span class="activeFilters" data-tag="` + tag + `">` + source + `</span>
                </div>
            `);
            bindClickBtnDeleteFilter();
        }

        refreshListe();
    }

    function bindClickBtnDeleteFilter() {
        $("#activeFilters-osm-observatoire #activeFilters .filters-activate").off().on("click", function() {
            if (typeof $(this).data("type") != "undefined") {
                var type = $(this).data("type");
                removeFilter(type);
                refreshListe();
            } else if (typeof $(this).data("tag") != "undefined") {
                var tag = $(this).data("tag");
                removeFilterSource(tag);
                refreshListe();
            }
        });
    }

    function removeFilter(type) {
        indexOfvalue = typeFilter.indexOf(type);
        if (indexOfvalue != -1)
            typeFilter.splice(indexOfvalue, 1);

        $(".active-filter-osm" + type).remove();
        if ($(".btn-select-type-osm-filter[data-type=" + type + "]").hasClass("active"))
            $(".btn-select-type-osm-filter[data-type=" + type + "]").removeClass("active");

    }

    function removeFilterSource(tag) {
        indexOfvalue = sourceFilter.indexOf(tag);
        if (indexOfvalue != -1)
            sourceFilter.splice(indexOfvalue, 1);

        $(".active-filter-osm" + tag).remove();
        if ($(".btn-select-source-osm-filter[data-tag=" + tag + "]").hasClass("active"))
            $(".btn-select-source-osm-filter[data-tag=" + tag + "]").removeClass("active");

    }

    function refreshListe() {
        if (typeof allData != "undefined" && allData != null && allData.element != "undefined")
            showMap(allData.element);
        else
            showMap();

        if (typeof allData != "undefined" && allData != null && typeof allData.count != "undefined"  && allData.element != "undefined")
            setTableObservatoire(allData.element, allData.count);
    }

    function getData(changeFiltreCity = false) {
       /*  $("#main-container-osm-observatoire").hide();
        $("#loader-osm-<?= $kunik ?>").show(); */

        lieuToSend = locationValue;

        var params = {
            idBlock: "<?= $myCmsId ?>",
            parentId : costum.contextId,
            parentType : blockCms.parent[costum.contextId].type,
            typeCocity : typeof costum.typeCocity != "undefined" ? costum.typeCocity : null,
            location: lieuToSend,
            types: typeDataToGet,
            selectedElement: <?= json_encode($blockCms["selectedElement"]) ?>,
            changeFiltreCity: changeFiltreCity,
            osmAllTYpe: osmAllTYpe,
        };

        // mylog.log("Executing beforeSend", map<?= $kunik ?>.showLoader());
        map<?= $kunik ?>.showLoader();
        ajaxPost(
            null,
            baseUrl + '/api/convert/osm',
            params,
            function(data) {
                // mylog.log("eeeeeeeeeeeeeeeeeeee data", data);
                // if(data != null && typeof data.element != "undefined" && data.element.length > 0) {
                //     allData = data;
                // }
                $("#loader-osm-<?= $kunik ?>").hide();
                $("#main-container-osm-observatoire").show();
                mylog.log("Ato tsika bain")
                if (data != null && typeof data.element != "undefined") {
                    if(data != null && typeof data.element != "undefined" && Object.keys(data.element).length > 0) {
                        allData = data;
                        refreshListe();
                    }
                }


            },null,null, {
                async: true,
                beforeSend: function() {
                    $("#loader-osm-<?= $kunik ?>").show();
                },
            }
        );
    }

    function getDataToShow(data, isCount = false) {
        var dataToShow = [];
        if (typeFilter.length == 0 && searchNameOsmData == "" && sourceFilter.length == 0)
            dataToShow = data;
        else {
            $.each(data, function(key, val) {
                if (searchNameOsmData == "") {
                    var tmpTrouve = false;
                    let inSource = false;

                    for(i = 0; i < sourceFilter.length; i++) {
                        if(typeof val[sourceFilter[i]] != "undefined" && val[sourceFilter[i]])
                            inSource = true;
                    }

                    if (val != null && typeof val.tags != "undefined" && val.tags != null) {
                        for(let i = 0; i < val.tags.length; i++) {
                            if(typeFilter.indexOf(val.tags[i]) != -1)
                                tmpTrouve = true;
                        }
                    }

                    if(inSource) 
                        dataToShow.push(val);
                    else if(tmpTrouve)
                        dataToShow.push(val);
                    else if (isCount && typeof val.osmType != "undefined" && typeFilter.indexOf(val.osmType) != -1)
                        dataToShow.push(val);
                } else if (val != null && typeof val.name != "undefined") {
                    let madatlasSearch = false;
                    let tagSearch = false;
                    

                    for(i = 0; i < madatlas.length; i++) {
                        if(madatlas[i].includes(searchNameOsmData))
                            madatlasSearch = true;
                    }

                    

                    if (val != null && typeof val.tags != "undefined" && val.tags != null) {
                        for(i = 0; i < val.tags.length; i++) {
                            if(val.tags[i].includes(searchNameOsmData))
                            tagSearch = true;
                        }
                    }

                    if(madatlasSearch) {
                        if(typeof val.madatlasContribution != "undefined" && val.madatlasContribution) {
                            if (typeFilter.length > 0) {
                                var tmpTrouve = false;

                                if (val != null && typeof val.tags != "undefined" && val.tags != null) {
                                    for(let i = 0; i < val.tags.length; i++) {
                                        if(typeFilter.indexOf(val.tags[i]) != -1)
                                            tmpTrouve = true;
                                    }
                                }
                                if(tmpTrouve)
                                    dataToShow.push(val);
                                else if (isCount && typeof val.osmType != "undefined" && typeFilter.indexOf(val.osmType) != -1)
                                    dataToShow.push(val);
                            } else
                                dataToShow.push(val);
                        }
                        
                    }
                    else if(tagSearch) {
                        if (typeFilter.length > 0) {
                            var tmpTrouve = false;

                            if (val != null && typeof val.tags != "undefined" && val.tags != null) {
                                for(let i = 0; i < val.tags.length; i++) {
                                    if(typeFilter.indexOf(val.tags[i]) != -1)
                                        tmpTrouve = true;
                                }
                            }
                            if(tmpTrouve)
                                dataToShow.push(val);
                            else if (isCount && typeof val.osmType != "undefined" && typeFilter.indexOf(val.osmType) != -1)
                                dataToShow.push(val);
                        } else
                            dataToShow.push(val);
                    }
                    else if(val.name.includes(searchNameOsmData)) {
                        if (typeFilter.length > 0) {
                            var tmpTrouve = false;

                            if (val != null && typeof val.tags != "undefined" && val.tags != null) {
                                for(let i = 0; i < val.tags.length; i++) {
                                    if(typeFilter.indexOf(val.tags[i]) != -1)
                                        tmpTrouve = true;
                                }
                            }
                            if(tmpTrouve)
                                dataToShow.push(val);
                            else if (isCount && typeof val.osmType != "undefined" && typeFilter.indexOf(val.osmType) != -1)
                                dataToShow.push(val);
                        } else
                            dataToShow.push(val);
                    }
                    
                }
            });
        }

        return dataToShow
    }

    function setTableObservatoire(data, getType) {
        str = "";
        let dataToShow = getDataToShow(data);
        var i = 0;
        $.each(dataToShow, function(key, val) {
            if (val != null && typeof val.geo != "undefined" && val.geo != null && typeof val.geo.latitude != "undefined" && typeof val.geo.longitude != "undefined") {
                let name = (typeof val.name != "undefined") ? val.name : "";
                let type = (typeof val.tags != "undefined" && typeof val.tags[0] != "undefined") ? val.tags[0] : "";
                str += `
                    <div class="element-right-list element-item-osm-map" id="element-right-list-` + i + `" data-lat="` + val.geo.latitude + `" data-long="` + val.geo.longitude + `" data-osmid="` + val.osmId + `">
                        <button class="item_map_list item_map_list_` + i + `">
                            <div class="left-col">
                                <div class="thumbnail-profil"><img class="lzy_img" height="50" width="50" src="<?= Yii::app()->getModule("co2")->assetsUrl ?>/images/thumbnail-default.jpg" data-src="/assets/39199a03/images/thumb/default_undefined.png"></div>
                            </div>
                            <div class="right-col">
                                <div class="info_item pseudo_item_map_list">` + name + `</div>
                                <div class="info_item items_map_list"><a href="javascript:" class="tag_item_map_list">#` + type + ` </a></div>
                            </div>
                            <div class="col-xs-12 separation"></div>
                        </button>
                    </div>
                `;
                i++;
            }
        });


        $("#container-list-item-osm-map").html(str);
        $("#resultdivMap").html(i);
        $(".element-item-osm-map").off().on("click", function() {
            const lat = $(this).data("lat");
            const long = $(this).data("long");
            $.each(allMarker, function(key, val) {
                if (val != null) {
                    var MarkerlatLong = val.getLatLng();
                    if (MarkerlatLong != null && typeof MarkerlatLong.lat != "undefined" && typeof MarkerlatLong.lng != "undefined") {
                        if (lat == MarkerlatLong.lat && long == MarkerlatLong.lng) {
                            mymarkersCluster.zoomToShowLayer(val, function() {
                                val.openPopup()
                                coInterface.bindLBHLinks();
                            })
                        }
                    }
                }
            });
        });
    }

    function getNameOfType(type, getType) {
        var str = "";
        $.each(getType, function(key, val) {
            if (val != null && typeof val.osmType != "undefined" && val.osmType == type && typeof val.name != "undefined")
                str = val.name;
        });

        return str;
    }

    function openPreviewOsm(id) {
        var params = {};
        description = (typeof $("#" + id).data("id") != "undefined") ? $("#" + id).data("id") : id;

        $.each(allData.element, function(key, data) {
            if (data != null && typeof data.osmId != "undefined" && data.osmId == description) {
                params = data;
            }
        });
        if(contributionMode)
            urlCtrl.openPreview("/view/url/costum.views.custom.osm.contribution", params);
        else 
            urlCtrl.openPreview("/view/url/costum.views.custom.osm.preview", params);
    }

    function openContributionOsmPanel() {
        var params = {};
        description = (typeof $("#" + id).data("id") != "undefined") ? $("#" + id).data("id") : id;

        $.each(allData.element, function(key, data) {
            if (data != null && typeof data.osmId != "undefined" && data.osmId == description) {
                params = data;
            }
        });
        urlCtrl.openPreview("/view/url/costum.views.custom.osm.contribution", params);
    }


    function showMap(dataMap = []) {

        // if (map<?= $kunik ?> != null) {
        //     teste = ;
        // }

        let dataToShow = getDataToShow(dataMap);
        map<?= $kunik ?>.clearMap()
        map<?= $kunik ?>.addElts(dataToShow);

        
        allMarker = map<?= $kunik ?>.getOptions().markerList;
        mymarkersCluster = map<?= $kunik ?>.getOptions().markersCluster;
        addButtonAddContribution();
        removeButtonAddContribution();
    };

    function showObservatoire() {
        $(".count-osm-observatoire-container").css("display", "block");
        $(".map-container-osm-observatoire").css("display", "none");
    }

    function getChildFormInFilter(parent, block = [], nameUpdate = null , pathUpdate = null,valueUpdate = null, keyParent= null ) {
        var res = [];
        let i = 0;
        if (parent != null) {
            $.each(parent, function(key, child) {
                elementChild = [];

                if (child != null && typeof child.name != "undefined" && typeof child.element != "undefined") {
                    let params = {};
                    var show = false;
                    if (block != null && typeof block[key] != "undefined") {

                        params = block[key];
                        show = true;
                    } else if (block != null && typeof block["typeTag"] != "undefined" && block["typeTag"] != null && typeof block["typeTag"][key] != "undefined") {
                        params = block["typeTag"][key];
                        show = true;
                    }

                    elementChild = getChildFormInFilter(child.element, params, nameUpdate, pathUpdate, valueUpdate, key);
                    res[i] = {
                        type: "section",
                        options: {
                            label: child.name,
                            name: key,
                            showInDefault: show,
                            inputs: elementChild
                        },
                    };
                } else if (child != null && typeof child.name != "undefined" && typeof child.type != "undefined" && typeof child.tag != "undefined") {
                    var defaultValue = false;

                    if(nameUpdate != null && pathUpdate != null && valueUpdate != null && nameUpdate == "allType" && pathUpdate.includes(keyParent))
                        defaultValue = valueUpdate;
                    else if (block != null && typeof block["allType"] != "undefined" && (block["allType"] == "true" || block["allType"] == true))
                        defaultValue = true;
                    else if (block != null && typeof block[key] != "undefined")
                        defaultValue = block[key];

                    if (i == 0) {
                        res[i] = {
                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: "allType",
                                label: "Selectionner tous",
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };

                    } else {
                        res[i] = {
                            type: "inputSimple",
                            options: {
                                type: "checkbox",
                                name: key,
                                label: child.name,
                                class: "switch",
                                classContainer: "osm-filter-switch-container",
                                defaultValue: defaultValue,
                            }
                        };
                    }

                }

                i++;
            });
        }

        return res;
    }

    function filters(name = null , pathUpdate = null, value = null) {
        var option = {};
        if(name != null && name == "selectedElement" && value != null) {
            blockCms["selectedElement"] = value;
            $.each(value, function(kk, vv) {
                if (typeof osmAllTYpe[vv] != "undefined") {
                    option[vv] = osmAllTYpe[vv];
                }
            });
        }
        else {
            $.each(blockCms["selectedElement"], function(kk, vv) {
                if (typeof osmAllTYpe[vv] != "undefined")
                    option[vv] = osmAllTYpe[vv];
            });
        }
        
        return getChildFormInFilter(option, <?= json_encode($blockCms) ?>, name, pathUpdate , value);
    }

    function getOptionTagElementTypeFilter(name = null , pathUpdate = null, value = null) {
        var option = [];
        $.each(osmAllTYpe, function(kk, vv) {
            if (vv != null && typeof vv.name != "undefined" && vv.element != "undefined")
                option.push({
                    value: kk,
                    label: vv.name
                });
        });
        return option;
    }

    function refreshFiltreType() {
        $(".list-type-filter").html("");
        setFiltreType(countForTypeFilter, osmAllTYpe);
        bindClickBtnFilterTypeChange();
    }

    function setFiltreType(typeF, osmAllTypeF) {
        if (typeF != null && osmAllTypeF != null) {
            $.each(typeF, function(key, val) {
                if(val != null && typeof val.type != "undefined" && typeof val.name != "undefined" ) {
                    $(".list-type-filter").append(`
                        <button class="item_panel_map btn-select-type-osm-filter"  data-type="` + val.type + `" data-name="` + val.name + `"><i class="fa fa-tag"></i> ` + val.name + ` <span class="badge text-white bg-turq count-list-type-` + val.type + `">`+val.count+`</span></button>
                    `);
                }
            });
        }
    }

    function bindClickBtnFilterTypeChange() {
        $(".btn-select-type-osm-filter").off().on("click", function() {
            var type = $(this).data("type");
            var name = $(this).data("name");

            ($(this).hasClass("active")) ? $(this).removeClass("active"): $(this).addClass("active");

            addFilter(type, name);
        });
    }

    function bindClickBtnFilterSourceChange() {
        $(".btn-select-source-osm-filter").off().on("click", function() {
            var tag = $(this).data("tag");
            var source = $(this).data("source");

            ($(this).hasClass("active")) ? $(this).removeClass("active"): $(this).addClass("active");

            addFilterSource(tag, source);
        });
    }

    function bindChangeValueFilterName() {
        $("#filterByNameInOsmBlock").off().on("input", function() {
            searchNameOsmData = $(this).val();
            refreshListe();
        });
    }
    

    map<?= $kunik ?> = new CoMap({
        container: "#divMap",
        activePopUp: true,
        activeCluster: true,
        mapOpt: {
            menuRight: false,
            btnHide: false,
            doubleClick: true,
            scrollWheelZoom: false,
            zoom: 2,
            activeCluster: true,
        },
        //mapCustom:customIcon,
        // elts: {}
    });

    jQuery(document).ready(function() {
        // if (map<?= $kunik ?> != null) {
        //     map<?= $kunik ?> = null;
        //     L.map('divMap').remove();
        // }


        blockCms = <?= json_encode($blockCms) ?>;
        if (blockCms != null) {
            if (typeof blockCms.location != "undefined")
                locationValue = blockCms.location;
            if (typeof blockCms.typeTag != "undefined")
                typeDataToGet = blockCms.typeTag;
            // if (typeof blockCms.elts != "undefined" && blockCms.elts != null) {
            //     allData = blockCms.elts;
            //     if (typeof blockCms.elts.count != "undefined" && blockCms.elts.count != null)
            //         countForTypeFilter = blockCms.elts.count;
            // }

        }
        
        refreshFiltreType();
        bindChangeValueFilterName();
        bindClickBtnFilterSourceChange();
        refreshListe();
        getData();
    });
</script>


<script type="text/javascript">
    function configInEditTrue(nameUpdate = null, pathUpdate = null, valueUpdate = null) {
        let filtreOsm = filters(nameUpdate, pathUpdate, valueUpdate);
        let optionTagElementTypeFilter = getOptionTagElementTypeFilter(nameUpdate, pathUpdate, valueUpdate);
        let defaultSelectedElementValue = <?= json_encode($blockCms["selectedElement"]) ?>;

        if(nameUpdate != null && nameUpdate == "selectedElement" && valueUpdate != null) 
            defaultSelectedElementValue = valueUpdate;
        
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        osmConfig = {
            configTabs: {
                general: {
                    inputsConfig: [{
                            type: "tags",
                            options: {
                                name: "location",
                                label: "Lieu",
                                options: [""],
                                defaultValue: locationValue,
                                icon: "chevron-down",
                            },
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "selectedElement",
                                label: "Element(s) a afficher",
                                options: optionTagElementTypeFilter,
                                defaultValue: defaultSelectedElementValue,
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "typeTag",
                                label: "Option pour les Elements a afficher",
                                showInDefault: true,
                                inputs: filtreOsm
                            },
                        },
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                },
                hover: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                }
            },
            onchange:function(path, valueToSet, name, payload, value){

                if(name == "location") {
                    var params = {
                        id: "<?= $myCmsId ?>",
                        collection : "cms",
                        value : value,
                        path : name,
                        costumSlug : costum.contextSlug,
                        costumEditMode : costum.editMode,
                    };

                    ajaxPost(
                        null,
                        baseUrl+"/co2/element/updatepathvalue",
                        params, 
                        function (response) {                    
                            toastr.success(tradCms.editionSucces);
                        });
                }
            },

            afterSave: function(path, valueToSet, name, payload, value) {
                configInEditTrue(name, path, value);
                $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                //cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']"); 
            }
        };
        cmsConstructor.blocks.<?= $kunik ?> = osmConfig;
    }

    str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#osmConfig<?= $kunik ?>").append(str);
    if (costum.editMode) {
        configInEditTrue();
    }
</script>