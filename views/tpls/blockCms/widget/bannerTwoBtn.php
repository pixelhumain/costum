<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
?>
<style type="text/css">
    .<?= $kunik ?> .borderLeftInput {
        border-top-left-radius: 20px !important;
        border-bottom-left-radius: 20px !important;
    }
    .<?= $kunik ?> .custom-border .select2-choices {
          border: 100px solid red;
    }
    .container<?= $kunik?>-css.cmsbuilder-block-container{
          margin-left: 30%;
    }
    .portfolio-modal .modal-content {
          margin-left: 0px;
    }
    #menuTopLeft a {
        text-decoration: none;
    }
    .header_<?= $kunik?> .content-text{
        margin-top: 10%;
        margin-left: 7%;
          /*background: linear-gradient(
                  70deg, rgb(6, 10, 25,.5), rgb(22, 32, 82,.5));
          border-radius: 10px;
          padding-top: 20px;*/
    }
    .header_<?= $kunik?> .content{
        margin-left: 7%;
    }
    .header_<?= $kunik?> .content .bouton:hover {
        background: transparent;
        color: white !important;
        border: 1px solid #F0FCFF;
    }
    .header_<?= $kunik?> .content p {
        margin-top: 33px;
    }
    .header_<?= $kunik?> .content .bouton{
        font-family: 'NotoSans-Regular' !important;
        background-color: #fff;
        padding: 5px 8px;
        border-radius: 30px;
        font-size: 15px;
        margin-top: 10px;
        margin-bottom: 20px;
        font-weight: bold;
        line-height: 28px;
    }
    .header_<?= $kunik?> .content button{
        margin-left : 10px;
    }
    .btn-<?= $kunik?> {
        margin-top : 50px;
    }
    .<?= $kunik ?> .link-btn {
        color: <?= isset($blockCms['linkProperty']["linkColor"]) ? $blockCms['linkProperty']["linkColor"] : $blockCms["linkColor"]; ?> ;
        text-transform: none;
        text-decoration: none;
        font-weight: 700;
    }
    .<?= $kunik ?> a.link-btn:hover, .<?= $kunik ?> a.link-btn:focus, .<?= $kunik ?> a.link-btn:active, .<?= $kunik ?> a.active {
        text-transform: none;
        text-decoration: none;
        color: #bfc2c4;
    }
    .<?= $kunik ?> a.btnTwo {
        white-space: initial;
        word-wrap: break-word;
        min-height: 40px;
        height: auto;
    }
    @media (max-width: 978px) {
        #menuTopLeft .menu-btn-top:before {
            font-size: 18px;
        }
        .header_<?= $kunik?> .content-text{
            margin-top: 20%;
        }
        .header_<?= $kunik?> .content .bouton{
            width: 39%;
            border-radius: 30px;
            height: auto;
            margin-top: 7%;
            font-weight: 600;
            padding: 1% 3% 1% 3%;
            margin-bottom: 10%;
            font-size: 18px !important;
        }
        .container<?= $blockCms["blockParent"]?>{
            background-image : none;
        }
    }
    .<?= $kunik ?> .header-video-bg {
        position: relative;
        /* background-color: black; */
        width: 100%;
        /* height: 95vh; */
        top: 0; right: 0; bottom: 0; left: 0;
        min-height: 25rem;
        overflow: hidden;
    }
    .<?= $kunik ?> .video-foreground,
    .<?= $kunik ?> .header-video-bg iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        pointer-events: none;
        object-fit: cover;
    }
    .header_<?= $kunik?> {
        background: linear-gradient(90deg, rgb(6, 10, 25, .9) 35%, rgba(22, 32, 82,0) 100%);
        height: 100%;
    }
    @media (min-width: 768px) {
        .header-video-bg {        
            height: 95vh;
        }
    }
    @media (max-width: 767px) {
        .sp-elt-<?= $blockKey?>{
        margin-bottom: 20%;
      }
          .header_<?= $kunik?> .content .bouton{
              width: 100%;
              height: auto;
              margin-top: 2%;
              margin-bottom: 0%;
          }
          .link-btn {
              font-size: 30px;
          }
          .header_<?= $kunik?> .content-text{
              margin-top: 5px;
          }
          .header-video-bg .video-container {
              margin-bottom: 5px;
          }
          .video-foreground,
          .header-video-bg iframe {
              pointer-events: all;
              object-fit: cover;
          }
          .header-video-bg {
              /* height: 100vh; */
          }
        .filter<?= $kunik?> #s2id_selectMultipleStatus .select2-choices{ 
            border-top-left-radius: 0 ;
            border-bottom-left-radius: 0 ;
        } 
        .filter<?= $kunik?>{
            margin-top: 10%;
        }
    }
    .<?= $kunik ?> .select2-container-multi .select2-choices{
        border : none;
    }
    @media (max-height: 700px) {
        .<?= $kunik ?> .header-video-bg {
            height: 720px;
        }
    }
    .<?= $kunik ?> .select2-container-multi .select2-choices .select2-search-field{
        width: 0.75em ;
        padding-bottom: 8px ;
    }
    .<?= $kunik ?> .header-video-bg .header_<?= $kunik?> {
        position: relative;
        z-index: 2;
    }
    @media (min-aspect-ratio: 16/9) {
        .video-foreground { height: 300%; top: -100%; }
    }
    @media (max-aspect-ratio: 16/9) {
        .video-foreground { width: 300%; left: -100%; }
    }
    @media all and (max-width: 600px) {
        .vid-info { width: 50%; padding: .5rem; }
        .vid-info h1 { margin-bottom: .2rem; }
    }
    @media all and (max-width: 500px) {
        .vid-info .acronym { display: none; }
    } 
    .filter<?= $kunik?> .selectMultiple label{
        color: white;
        font-size: 12px;
    }
    .filter<?= $kunik?>{
        margin-top: 3%;
        margin-left: 7%;
    }
    .filter<?= $kunik?> .select2-container-multi .select2-choices{
        border-radius: 0;
        border : 1px solid;
    }
    .filter<?= $kunik?> .search<?= $kunik?>{    
        padding: 10px;
        font-size: 21px;
        background-color: white;
        border: 1px solid;
        color: #1a2660; 
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }
    .filter<?= $kunik?> .select2-container-multi:not(.aapstatus-container) .select2-choices .select2-search-choice{   
        background: #1a2660!important;
        color: #fff!important;
        padding-left: 15px!important;
        padding-right: 15px!important;
        padding-top: 5px!important;
        padding-bottom: 5px!important;
        border: 1px solid #1a2660!important;
        border-radius: 5px!important;
        font-weight: 700!important;
    }
    .filter<?= $kunik?> .select2-search-choice-close {
        background: #fff!important; 
        margin: 0 !important;
        padding-left: 2px;
        border-radius: 10px;
    }
    .filter<?= $kunik?> .select2-search-choice-close::before {
        color: #1a2660 !important;
        font-size: 10px;
    }
    .btnSearch<?=$kunik?>{
        margin-top: 34px;
    }
    .filter<?= $kunik?> .select2-dropdown-open.select2-drop-above .select2-choices,
    .<?= $kunik ?> .select2-container-active .select2-choices{
        background-color : white !important;
    }

    /* .select2-container-multi .select2-choices  {
        background: none;
        border: none;
        padding: 20px 0 15px;
        width: 100%;
        height: 50px;
        border-radius: 10px;
        padding: 0 15px;
        box-shadow: none;
        border-right: 6px solid #9fbd38;
        font-size: 18px;
    } */

    .filter<?= $kunik?>  .select2-container-multi{
        width: 100%;
    }
    .filter<?= $kunik?> label {
        color: white;
    }
     .select2-result-label:hover{
        background-color:#1a2660;
    }
    .<?= $kunik ?> .select2-results .select2-highlighted{
        background-color:#1a2660;
    } 
    .<?=$kunik?> .container-filters-menu #activeFilters .filters-activate{
        background-color:#1a2660 !important;
    }
    .<?= $kunik ?> .select2-results .select2-result-label{
        border-bottom: 1px double #e2e2e2;
    }
    .<?= $kunik ?> .select2-container-multi .select2-choices .select2-search-field input:focus{
        background-color: white;
    }
</style>
<div class="col-xl-12 no-padding header-video-bg <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
    <div class="video-foreground hidden-xs">
        <iframe id="player" frameborder="0" allowfullscreen src="<?= isset($blockCms['fondVideo']["videoLink"]) ? $blockCms['fondVideo']["videoLink"] : ""; ?>"></iframe>
    </div>
    <div class="video-container visible-xs col-xs-12">
        <iframe id="player" frameborder="0" allowfullscreen src="<?= isset($blockCms['fondVideo']["videoLink"]) ? $blockCms['fondVideo']["videoLink"] : ""; ?>"></iframe>
    </div>
    <div class="header_<?= $kunik ?> search_<?= $kunik ?> col-xs-12">
        <div class="content-text col-lg-6 col-md-8 col-sm-10 col-xs-10">
            <p class="link-btn desactive lbh" href="<?= isset($blockCms['linkProperty']["linkLink"]) ? $blockCms['linkProperty']["linkLink"] : $blockCms["linkLink"]; ?>" data-link="<?= isset($blockCms['linkProperty']["linkLink"]) ? $blockCms['linkProperty']["linkLink"] : $blockCms["linkLink"];?>" style="font-size:<?= isset($blockCms['linkProperty']["linkSize"]) ? $blockCms['linkProperty']["linkSize"] : $blockCms["linkSize"]; ?>px;">
                <?= isset($blockCms['linkProperty']["linkLabel"]) ? $blockCms['linkProperty']["linkLabel"] : $blockCms["linkLabel"]; ?>
            </p>
            <!-- <div>
              <span style="font-family:'NotoSans-Regular';font-size: 31px;line-height: 50px;color: #ffffff;">
                <!--
              </span>
            </div> -->
            <div class="sp-text desc<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> </div>
        </div>
        <div class="filter<?= $kunik ?> col-xs-12">
            <div class='search-bar'>
                <div class="row">
                    <div id="aaaa1" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding" <?= @$blockCms['searchForm']['firstText']["show"] ?>>
                        <div>
                            <label>
                                <?= isset($blockCms['searchForm']['firstText']["premierLabel"]) ? @$blockCms['searchForm']['firstText']["premierLabel"] : $blockCms["premierLabel"]; ?>
                                <label>
                        </div>
                        <input id="selectMultipleStatus" data-params="statusListFilter" data-type="select2">
                    </div>
                    <div id="aaaa2" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding" <?= @$blockCms['searchForm']['secondoText']["show"] ?>>
                        <div>
                            <label>
                                <?= isset($blockCms['searchForm']['secondoText']["deuxiemeLabel"]) ? $blockCms['searchForm']['secondoText']["deuxiemeLabel"] : $blockCms["deuxiemeLabel"]; ?>
                                <label>
                        </div>
                        <input id="selectMultipleAccompaniment" data-params="accompanimentTypeFilter" data-type="select2">
                    </div>
                    <div id="aaaa3" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding" <?= @$blockCms['searchForm']['thirdText']["show"] ?>>
                        <div>
                            <label>
                                <?= isset($blockCms['searchForm']['thirdText']["troisiemeLabel"]) ? $blockCms['searchForm']['thirdText']["troisiemeLabel"] : $blockCms["troisiemeLabel"]; ?>
                                <label>
                        </div>
                        <input id="selectMultipleThemes" data-params="themes" data-type="select2">
                    </div>
                    <div id="aaaa4" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 no-padding" <?= @$blockCms['searchForm']['fourthText']["show"] ?>>
                        <div>
                            <label>
                                <?= isset($blockCms['searchForm']['fourthText']["quatriemeLabel"]) ? $blockCms['searchForm']['fourthText']["quatriemeLabel"] : $blockCms["quatriemeLabel"]; ?>
                                <label>
                        </div>
                        <input id="selectMultipleTypeFinancing" data-params="TypeFinancing" data-type="select2">
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 btnSearch<?= $kunik ?> no-padding">
                        <a class="search<?= $kunik ?>" href="javascript:;"> <i class="fa fa-search"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content col-lg-6 col-md-8 col-sm-10 col-xs-10">
            <div class="btn-<?= $kunik ?>">
                <a href="<?= isset($blockCms['proprieteBouton1']["button1Link"]) ? $blockCms['proprieteBouton1']["button1Link"] : $blockCms["button1Link"]; ?>" class="btn bouton btnTwo desactive <?= isset($blockCms['proprieteBouton1']["button1Class"]) ? $blockCms['proprieteBouton1']["button1Class"] : $blockCms["button1Class"]; ?>" <?= isset($blockCms['proprieteBouton1']["button1Class"]) ? $blockCms['proprieteBouton1']["button1Class"] : $blockCms["button1Class"]; ?>>
                    <?= isset($blockCms['proprieteBouton1']["buttonLabel1"]) ? $blockCms['proprieteBouton1']["buttonLabel1"] : $blockCms["buttonLabel1"]; ?>
                </a>
                <a href="javascript:;" id="joinBtn<?= $kunik ?>" class="btn bouton btnTwo" style="text-decoration : none;">
                    <?= isset($blockCms['proprieteBouton2']["buttonLabel2"]) ? $blockCms['proprieteBouton2']["buttonLabel2"] : $blockCms["buttonLabel2"]; ?>
                </a>
            </div>
        </div>
    </div>
</div>
<style id="bannerTwoBtn<?= $kunik ?>">
</style>
<script type="text/javascript">
    setTimeout(function() {
        var firstVisibleDiv = $('#aaaa1,#aaaa2,#aaaa3,#aaaa4').not(':hidden').first();
        var elementInsideDiv = firstVisibleDiv.find('.select2-choices');
        elementInsideDiv.addClass('borderLeftInput');
    }, 500);
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
    $(document).ready(function() {
        function ctrInputShow() {
            var countInput = 0;
            $('.col-lg-2.col-md-2.col-sm-2.col-xs-12').each(function() {
                if ($(this).is(":hidden")) {
                    countInput++;
                }
            });
            var colClass;
            switch (countInput) {
                case 1:
                    colClass = 'col-lg-3 col-md-3 col-sm-3 col-xs-12';
                    break;
                case 2:
                    colClass = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';
                    break;
                case 3:
                    colClass = 'col-lg-8 col-md-8 col-sm-8 col-xs-12';
                    break;
                default:
                    colClass = 'col-lg-2 col-md-2 col-sm-2 col-xs-12';
            }
            $('.col-lg-2.col-md-2.col-sm-2.col-xs-12').removeClass('col-lg-2 col-md-2 col-sm-2 col-xs-12').addClass(colClass);
            if (countInput === 0) {
                $('.col-lg-2.col-md-2.col-sm-2.col-xs-12').removeClass('col-lg-3 col-md-3 col-sm-3 col-xs-12 col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-8 col-md-2 col-sm-2 col-xs-12').addClass('col-lg-2 col-md-2 col-sm-2 col-xs-12');
            }
        }
        ctrInputShow();
        $('[hidden]').on('change', function() {
            ctrInputShow();
        });

    });
    $('#player').css('display', 'none');

    function onYouTubeIframeAPIReady() {
        var player = new YT.Player('player', {
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }

    function onPlayerReady(event) {
        event.target.playVideo();
    }
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;

    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING 
            && typeof sectionDyf.<?php echo $kunik ?>blockCms.fondVideo != "undefined"
            && typeof sectionDyf.<?php echo $kunik ?>blockCms.fondVideo.videoLink != "undefined" 
            && sectionDyf.<?php echo $kunik ?>blockCms.fondVideo.videoLink != "") {
            $('.container<?= $blockCms["blockParent"] ?>').css('background-image', 'none');
            $('#player').css('display', 'block');
        } else {
            $('#player').css('display', 'none');
        }
    }
    jQuery(document).ready(function() {
        var themes = [];
        var accompanimentTypeFilter = [];
        var statusListFilter = [];
        var typeFinancing = [];
        var toDefaultValueThemes = [];
        var toDefaultValueAccompanimentTypeFilter = [];
        var toDefaultValueStatusListFilter = [];
        var toDefaultValueTypeFinancing = [];
        if (typeof costum.lists != "undefined" && typeof costum.lists.themes !="undefined") {
            $.each(costum.lists.themes, function(kth, vth) {
                themes.push({
                    "id": kth,
                    "text": vth
                })
                toDefaultValueThemes.push(kth);
            })
        }
        let contenueTroisiemeInputVar = <?= @json_encode($blockCms['searchForm']['thirdText']["contenueTroisiemeInput"]) ?>;
        var array3 = (!notEmpty(contenueTroisiemeInputVar)) ? toDefaultValueThemes : contenueTroisiemeInputVar.split(",")
        contenueTroisiemeInputVar = (!notEmpty(contenueTroisiemeInputVar)) ? statusListFilter : contenueTroisiemeInputVar;
        const contenue3 = array3.map(valeur => {
            return {
                id: valeur,
                text: valeur
            };
        });
        if (typeof costum.lists != "undefined" && typeof costum.lists.accompanimentTypeFilter != "undefined") {
            $.each(costum.lists.accompanimentTypeFilter, function(ka, va) {
                accompanimentTypeFilter.push({
                    "id": ka,
                    "text": va
                })
                toDefaultValueAccompanimentTypeFilter.push(ka);
            })
        }

        let contenueDeuxiemeInputVar = <?= json_encode(@$blockCms['searchForm']['secondoText']["contenueDeuxiemeInput"]) ?>;
        var array2 = (!notEmpty(contenueDeuxiemeInputVar)) ? toDefaultValueAccompanimentTypeFilter : contenueDeuxiemeInputVar.split(",")
        contenueDeuxiemeInputVar = (!notEmpty(contenueDeuxiemeInputVar)) ? statusListFilter : contenueDeuxiemeInputVar;
        const contenue2 = array2.map(valeur => {
            return {
                id: valeur,
                text: valeur
            };
        });
        if (typeof costum.lists != "undefined" && typeof costum.lists.status != "undefined") {
            $.each(costum.lists.status, function(ks, vs) {
                statusListFilter.push({
                    "id": ks,
                    "text": vs
                })
                toDefaultValueStatusListFilter.push(ks)
            })
        }
        let contenuePremierInputVar = <?= json_encode(@$blockCms['searchForm']['firstText']["contenuePremierInput"]) ?>;
        var array1 = (!notEmpty(contenuePremierInputVar)) ? toDefaultValueStatusListFilter : contenuePremierInputVar.split(",")
        contenuePremierInputVar = (!notEmpty(contenuePremierInputVar)) ? statusListFilter : contenuePremierInputVar;
        const contenue1 = array1.map(valeur => {
            return {
                id: valeur,
                text: valeur
            };
        });
        if (typeof costum.lists != "undefined" && typeof costum.lists.typeFinancing != "undefined") {
            $.each(costum.lists.typeFinancing, function(kt, vt) {
                typeFinancing.push({
                    "id": vt,
                    "text": vt
                })
                toDefaultValueTypeFinancing.push(vt)
            })
        }
        let contenueQuatriemeInputVar = <?= json_encode(@$blockCms['searchForm']['fourthText']["contenueQuatriemeInput"]) ?>;
        var array4 = (!notEmpty(contenueQuatriemeInputVar)) ? toDefaultValueTypeFinancing : contenueQuatriemeInputVar.split(",")
        contenueQuatriemeInputVar = (!notEmpty(contenueQuatriemeInputVar)) ? statusListFilter : contenueQuatriemeInputVar;
        const contenue4 = array4.map(valeur => {
            return {
                id: valeur,
                text: valeur
            };
        });
        if (sectionDyf.<?php echo $kunik ?>blockCms.formShow == true) {
            dyFObj.openForm('organization', null, {
                type: "GovernmentOrganization",
                role: "admin"
            });
        }
        $("#btn-search-<?= $kunik ?>").click(function() {
            $("#search-form<?= $kunik ?>").submit();
            var str = $("#otherInput").val();
            location.hash = "#metiers?text=" + str;
            urlCtrl.loadByHash(location.hash);
        });
        $("#search-form<?= $kunik ?>").submit(function(event) {
            var str = $("#otherInput").val();
            location.hash = "#metiers?text=" + str;
            urlCtrl.loadByHash(location.hash);
            event.preventDefault();
        });
        str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
        $("#bannerTwoBtn<?= $kunik ?>").append(str);
        var paramSearch1 = <?= json_encode(@$blockCms['searchForm']['firstText']["paramCont1"]) ?>;
        var paramSearchCtrl1 = (!notEmpty(paramSearch1)) ? "tags" : paramSearch1;
        var paramSearch2 = <?= json_encode(@$blockCms['searchForm']['secondoText']["paramCont2"]) ?>;
        var paramSearchCtrl2 = (!notEmpty(paramSearch2)) ? "tags" : paramSearch2;
        var paramSearch3 = <?= json_encode(@$blockCms['searchForm']['thirdText']["paramCont3"]) ?>;
        var paramSearchCtrl3 = (!notEmpty(paramSearch3)) ? "tags" : paramSearch3;
        var paramSearch4 = <?= json_encode(@$blockCms['searchForm']['fourthText']["paramCont4"]) ?>;
        var paramSearchCtrl4 = (!notEmpty(paramSearch4)) ? "tags" : paramSearch4;
        var typeLink = {
            "target='_blank'": "Externe",
            "lbh-preview-element ": "page previsualisation",
            "lbh": "page entiere",
        };
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var bannerTwoBtn = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type: "section",
                                options: {
                                    name: "linkProperty",
                                    label: tradCms.linkProperty,
                                    showInDefault: true,
                                    inputs: [{
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "linkLabel",
                                                label: tradCms.label}
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "number",
                                                name: "linkSize",
                                                label: tradCms.size + "(px)"
                                            }
                                        },
                                        {
                                            type: "color",
                                            options: {
                                                type: "color",
                                                name: "linkColor",
                                                label: tradCms.color
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "linkLink",
                                                label: tradCms.link,
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "fondVideo",
                                    label: tradCms.videoBackground,
                                    showInDefault: false,
                                    inputs: [{
                                        type: "inputSimple",
                                        options: {
                                            type: "inputSimple",
                                            name: "videoLink",
                                            label: tradCms.link
                                        }
                                    }]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "proprieteBouton1",
                                    label: tradCms.buttonProperty + " 1",
                                    showInDefault: false,
                                    inputs: [{
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "buttonLabel1",
                                                label: tradCms.label
                                            }
                                        },
                                        {
                                            type: "select",
                                            options: {
                                                name: "button1Class",
                                                label: tradCms.internalOrExternalLink,
                                                options: $.map(typeLink, function(key, val) {
                                                    return {
                                                        value: val,
                                                        label: key
                                                    }
                                                }),
                                                selected: "lbh "
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "button1Link",
                                                label: tradCms.link
                                            }
                                        },
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "proprieteBouton2",
                                    label: tradCms.buttonProperty + " 2",
                                    showInDefault: false,
                                    inputs: [{
                                        type: "inputSimple",
                                        options: {
                                            type: "inputSimple",
                                            name: "buttonLabel2",
                                            label: tradCms.label
                                        }
                                    }]
                                }
                            },
                            {
                                type: "inputSimple",
                                options: {
                                    type: "checkbox",
                                    name: "formShow",
                                    label: tradCms.displayTheFormDirectly,
                                    class: "switch"
                                }
                            },
                        ]
                    },
                    searchForm : {
                        key : "searchForm",
                        keyPath : "searchForm",
                        icon: "gears",
                        label: tradCms.searchForm,
                        inputsConfig: [
                            {
                                type: "section",
                                options: {
                                    name: "firstText",
                                    label: tradCms.firstContent,
                                    showInDefault: true,
                                    inputs: [
                                        {
                                            type: "groupButtons",
                                            options: {
                                                name: "show",
                                                label: tradCms.show,
                                                options: [{
                                                    "label": trad.yes,
                                                    "value": ""
                                                }, {
                                                    "label": trad.no,
                                                    "value": "hidden"
                                                }],
                                                defaultValue: "<?= isset($blockCms['searchForm']['firstText']["show"]) ? $blockCms['searchForm']['firstText']["show"] : '' ; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "premierLabel",
                                                label: tradCms.label,
                                                defaultValue: "<?= isset($blockCms['searchForm']['firstText']["premierLabel"]) ? $blockCms['searchForm']['firstText']["premierLabel"] : "" ; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "paramCont1",
                                                label: tradCms.settings,
                                                defaultValue: paramSearchCtrl1
                                            }
                                        },
                                        {
                                            type: "tags",
                                            options: {
                                                name: "contenuePremierInput",
                                                label: tradCms.content,
                                                options: [],
                                                defaultValue: array1
                                            }
                                      },
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "secondoText",
                                    label: tradCms.secondContent,
                                    showInDefault: false,
                                    inputs: [{
                                            type: "groupButtons",
                                            options: {
                                                name: "show",
                                                label: tradCms.show,
                                                options: [{
                                                    "label": trad.yes,
                                                    "value": ""
                                                }, {
                                                    "label": trad.no,
                                                    "value": "hidden"
                                                }],
                                                defaultValue: "<?= isset($blockCms['searchForm']['secondoText']["show"]) ? $blockCms['searchForm']['secondoText']["show"] : '' ; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "deuxiemeLabel",
                                                label: tradCms.label,
                                                defaultValue: "<?= isset($blockCms['searchForm']['secondoText']["deuxiemeLabel"]) ? $blockCms['searchForm']['secondoText']["deuxiemeLabel"] : ""; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "paramCont2",
                                                label: tradCms.settings,
                                                defaultValue: paramSearchCtrl2
                                            }
                                        },
                                        {
                                            type: "tags",
                                            options: {
                                                name: "contenueDeuxiemeInput",
                                                label: tradCms.content,
                                                options: [],
                                                defaultValue: array2
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "thirdText",
                                    label: tradCms.thirdContent,
                                    showInDefault: false,
                                    inputs: [{
                                            type: "groupButtons",
                                            options: {
                                                name: "show",
                                                label: tradCms.show,
                                                options: [{
                                                    "label": trad.yes,
                                                    "value": ""
                                                }, {
                                                    "label": trad.no,
                                                    "value": "hidden"
                                                }],
                                                defaultValue: "<?= isset($blockCms['searchForm']['troisiemeLabel']["show"]) ? $blockCms['searchForm']['troisiemeLabel']["show"] : ''; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "troisiemeLabel",
                                                label: tradCms.label,
                                                defaultValue: "<?= isset($blockCms['searchForm']['thirdText']["troisiemeLabel"]) ? $blockCms['searchForm']['thirdText']["troisiemeLabel"] : ""; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "paramCont3",
                                                label: tradCms.settings,
                                                defaultValue: paramSearchCtrl3
                                            }
                                        },
                                        {
                                            type: "tags",
                                            options: {
                                                name: "contenueTroisiemeInput",
                                                label: tradCms.content,
                                                options: [],
                                                defaultValue: array3
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "fourthText",
                                    label: tradCms.fourthContent,
                                    showInDefault: false,
                                    inputs: [{
                                            type: "groupButtons",
                                            options: {
                                                name: "show",
                                                label: tradCms.show,
                                                options: [{
                                                    "label": trad.yes,
                                                    "value": ""
                                                }, {
                                                    "label": trad.no,
                                                    "value": "hidden"
                                                }],
                                                defaultValue: "<?= isset($blockCms['searchForm']['fourthText']["show"]) ? $blockCms['searchForm']['fourthText']["show"] : ''; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "quatriemeLabel",
                                                label: tradCms.label,
                                                defaultValue: "<?= isset($blockCms['searchForm']['fourthText']["quatriemeLabel"]) ? $blockCms['searchForm']['fourthText']["quatriemeLabel"] : ""; ?>"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                type: "inputSimple",
                                                name: "paramCont4",
                                                label: tradCms.settings,
                                                defaultValue: paramSearchCtrl4
                                            }
                                        },
                                        {
                                            type: "tags",
                                            options: {
                                                name: "contenueQuatriemeInput",
                                                label: tradCms.content,
                                                options: [],
                                                defaultValue: array4
                                            }
                                        }
                                    ]
                                }
                            },
                        ]
                    },
                },
                afterSave: function(path, valueToSet, name, payload, value) {
                    mylog.log(value)
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
            }
            cmsConstructor.blocks.bannerTwoBtn = bannerTwoBtn;
        }
        appendTextLangBased(".desc<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($blockCms["title"]) ?>, "<?= $blockKey ?>");
        $("#joinBtn<?= $kunik ?>").off().click(function() {
            dyFObj.openForm('organization');
            $("#ajax-modal").addClass(costum.slug+"Modal");
            $(".mainDynFormCloseBtn , .close-modal").off("click").on("click" , function(){
                $("#ajax-modal").removeClass(costum.slug+"Modal");
            })
        });
        // $("#addProject<?= $kunik ?>").off().click(function(){     
        //     dyFObj.openForm('project');
        // });
        // $("#scrrol<?php //echo $kunik ?>").off().click(function() {
        //     var link = $(this).data("link");
        //     $('html, body').animate({
        //             scrollTop: $("#" + link).offset().top - $("#mainNav").height()
        //         },
        //         800,
        //         () => {},
        //     );
        // });
        $(".search<?= $kunik ?>").click(function() {
            var status = $("#selectMultipleStatus").val();
            var accompaniment = $("#selectMultipleAccompaniment").val();
            var themes = $("#selectMultipleThemes").val();
            var typeFinancingtags = $("#selectMultipleTypeFinancing").val();

            var allTags = "";
            var obj = {};

            if (status != "") {
                allTags += status;
                obj[paramSearchCtrl1] = status;
            }

            if (accompaniment != "") {
                if (status != "") {
                    allTags += "," + accompaniment;
                } else {
                    allTags += accompaniment;
                }
                if (paramSearchCtrl2 === paramSearchCtrl1) {
                    if (!Array.isArray(obj[paramSearchCtrl2])) {
                        obj[paramSearchCtrl2] = [obj[paramSearchCtrl2]];
                    }
                    obj[paramSearchCtrl2].push(accompaniment);
                } else {
                    obj[paramSearchCtrl2] = accompaniment;
                }
            }

            if (themes != "") {
                if (accompaniment != "" || status != "") {
                    allTags += "," + themes;
                } else {
                    allTags += themes;
                }
                if (paramSearchCtrl3 === paramSearchCtrl1 || paramSearchCtrl3 === paramSearchCtrl2) {
                    if (!Array.isArray(obj[paramSearchCtrl3])) {
                        obj[paramSearchCtrl3] = [obj[paramSearchCtrl3]];
                    }
                    obj[paramSearchCtrl3].push(themes);
                } else {
                    obj[paramSearchCtrl3] = themes;
                }
            }

            if (typeFinancingtags != "") {
                if (themes != "" || accompaniment != "" || status != "") {
                    allTags += "," + typeFinancingtags;
                } else {
                    allTags += typeFinancingtags;
                }
                if (paramSearchCtrl4 === paramSearchCtrl1 || paramSearchCtrl4 === paramSearchCtrl2 || paramSearchCtrl4 === paramSearchCtrl3) {
                    if(typeof obj[paramSearchCtrl4] == "undefined"){
                        obj[paramSearchCtrl4] = [];
                    }
                    if (!Array.isArray(obj[paramSearchCtrl4])) {
                        obj[paramSearchCtrl4] = [obj[paramSearchCtrl4]];
                    }
                    obj[paramSearchCtrl4].push(typeFinancingtags);
                } else {
                    obj[paramSearchCtrl4] = typeFinancingtags;
                }
            }

            for (var prop in obj) {
                if (obj.hasOwnProperty(prop) && obj[prop] === undefined) {
                    delete obj[prop];
                }
            }
            var resultString = '';
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    var values = Array.isArray(obj[prop]) ? obj[prop].filter((element => element != undefined)).join(',') : obj[prop];
                    resultString += prop + '=' + values + '&';
                }
            }
            resultString = resultString.slice(0, -1);
            if (allTags == "") {
                urlCtrl.loadByHash("#search");
            }
            if (allTags != "") {
                allTags = allTags.split(",");
                urlCtrl.loadByHash("#search?"+resultString.trim(","));
            }
        })
        setTimeout(function() {
            $("#selectMultipleStatus").select2({
                data: contenue1,
                tags: true,
            });
            $("#selectMultipleAccompaniment").select2({
                data: contenue2,
                tags: true,
            });
            $("#selectMultipleThemes").select2({
                data: contenue3,
                tags: true,
            });
            $("#selectMultipleTypeFinancing").select2({
                data: contenue4,
                tags: true,
            });
        }, 100)

    });
</script>
<script src="https://www.youtube.com/iframe_api"></script>
