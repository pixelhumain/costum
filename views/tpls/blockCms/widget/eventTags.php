<?php
$defaultStyle = [];
$myCmsId = (string)$blockCms['_id'];
$blockCms["css"] = array_replace_recursive($defaultStyle, $blockCms["css"] ?? []);
$styleCss = (object)[$kunik => $blockCms["css"] ?? []];
?>
<style>
	.theme-selector ul {
		max-height: 300px;
		overflow-y: auto;
	}

	.theme-selector li {
		font-size: 14px;
	}

	.theme-selector .dropdown-toggle {
		font-size: 20px;
	}
</style>
<script>
	function event_tags(W, $, php) {
		var input_dom = $('.theme-selector[data-id=' + php.cms_id + ']');
		var default_filter = get_session_data();

		function get_session_data() {
			var session_data = sessionStorage.getItem('coevent-' + php.context.id + php.context.type);
			var filter = session_data ? JSON.parse(session_data) : {
				tags: [],
				regions: [],
				type: null
			};
			return (filter);
		}

		function set_session_data(data) {
			sessionStorage.setItem('coevent-' + php.context.id + php.context.type, JSON.stringify(data));
		}

		function parse_url_to_obj() {
			var output = {};
			var get_params = W.location.hash.indexOf('?');
			if (get_params > 1) {
				var get_string = W.location.hash.substr(get_params + 1);
				var values = get_string.split('&');
				$.each(values, function(i, value) {
					var index_cut = value.indexOf('=');
					
					var key = value.substr(0, index_cut);
					var val = decodeURI(value.substr(index_cut + 1));
					var is_array = key.search(/\[\]$/) > 1
					if (is_array)
						key = key.substr(0, index_cut - 2);
					if (typeof output[key] === 'undefined' && is_array)
						output[key] = [];
					if (is_array)
						output[key].push(val);
					else
						output[key] = val;
				});
			}
			return (output);
		}

		function parse_obj_to_url(tags) {
			var params = parse_url_to_obj();
			params.tags = tags;
			var hash = location.hash.match(/\#[\w\-]+/);
			if (!hash)
				hash = '';
			else
				hash = hash[0];
			hash += '?'
			var key;
			for (key in params) {
				if (params[key] === null)
					continue;
				if (typeof params[key] === 'string')
					hash += key + '=' + params[key] + '&';
				else
					$.each(params[key], function(i, param) {
						hash += key + '[]=' + param + '&';
					});
			}
			hash = hash.replace(/.$/, '');
			hash = W.location.origin + W.location.pathname + hash;
			return (hash);
		}

		input_dom.on('click', 'a[role=menuitem]', function(e) {
			var self = $(this);
			e.preventDefault();
			var value = self.attr('href');
			var filter = get_session_data();
			var index_of_value = filter.tags.indexOf(value);
			if (index_of_value >= 0)
				filter.tags.splice(index_of_value, 1);
			else
				filter.tags.push(value);
			set_session_data(filter);
			W.history.pushState({}, '', parse_obj_to_url(filter.tags));
			$('.coevent-program').trigger('coevent-filter');
		});
		$('.coevent-program').on('coevent-filter', function() {
			var filter = get_session_data();
			input_dom.find('li.active').removeClass('active');
			$.each(filter.tags, function(i, tag) {
				input_dom.find('a[role=menuitem][href="' + tag + '"]').parent().addClass('active');
			});
		});
		var get_params = parse_url_to_obj();
		if (Object.keys(get_params).includes('tags')) {
			default_filter.tags = get_params.tags;
			set_session_data(default_filter);
		}
		$.each(default_filter.tags, function(i, tag) {
			input_dom.find('a[role=menuitem][href="' + tag + '"]').parent().addClass('active');
		});
	}
</script>
<div class="dropdown theme-selector" data-id="<?= $myCmsId ?>">
	<button class="btn btn-default dropdown-toggle" type="button" id="drop<?= $myCmsId ?>" data-toggle="dropdown">
		<?= Yii::t('common', 'Thématiques') ?>
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu" role="menu" aria-labelledby="drop<?= $myCmsId ?>">
		<?php foreach ($tags as $tag) : ?>
			<li role="presentation"><a role="menuitem" tabindex="-1" href="<?= $tag ?>"><?= $tag ?></a></li>
		<?php endforeach; ?>
	</ul>
</div>
<script>
	(function(x, $) {
		var php = {
			cms_id: '<?= $myCmsId ?>',
			context: JSON.parse(JSON.stringify(<?= json_encode($parent) ?>)),
		};
		mylog.log("coevent php data tag", php);
		event_tags(x, $, php);
	})(window, jQuery);
</script>