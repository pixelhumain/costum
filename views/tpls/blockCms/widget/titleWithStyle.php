<?php 
	$myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
    // var_dump();
?>

<style type="text/css">
	.hex<?php echo $kunik ?> {
      position: relative;
      float: left;
      height: 60px;
      min-width: 75px;

      font-weight: bold;
      text-align: center;
      background: <?php echo $blockCms["bgcolor"]; ?>;
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
    }
    .hex<?php echo $kunik ?>.gradient-bg {
      left: 50%;
      transform: translate(-50%,-50%);
      min-width: 60%;
    }

    .hex<?php echo $kunik ?>:before {
      position: absolute;
      content: '';
      height: calc(100% - 14px);  /* 100% - 2 * border width */
      width: calc(100% - 14px);  /* 100% - 2 * border width */
      left: 7px; /* border width */
      top: 7px; /* border width */
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      z-index: -1;
    }
    .hex<?php echo $kunik ?>.gradient-bg:before {
      background: <?php echo $blockCms["bgcolor"]; ?>;
    }
    .hex<?php echo $kunik ?>.white-bg:before {
      background: #ffffff!important;
    }

    .hex<?php echo $kunik ?>.white-bg {
      background: #ffffff!important;
    }
    .hex<?php echo $kunik ?> span {
        /* display: inline-block; */
        margin-top: 30px;
        padding: 8px;
        transform: translateY(-50%);
        margin-left: 20px;
        margin-right: 20px;
        font-size: 28px;
    }
    .hex<?php echo $kunik ?> .bg-green {
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["colordecor"]; ?>!important;
      /*color: #fff;*/
    }

    .hex<?php echo $kunik ?> .bg-green span {
      font-size: 40px;
      margin-left: 10px;
      margin-right: 10px;
    }
    .hex<?php echo $kunik ?> .bg-green span.fa {
      font-size: 30px;
    }
    .contain-faq {
        display: flex;
    }
    @media (max-width: 991px ) {
        .hex<?php echo $kunik ?>.gradient-bg {
          width: 100%;
        }
        .hex<?php echo $kunik ?> span {
            display: block;
        }
    }
    @media (max-width: 767px ) {
        .hex<?php echo $kunik ?> span {
            font-size: 20px;
        }
        .prez-heading-text {
            font-size: 20px;
        }
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 no-padding prez-multi-col <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding contain-faq">
       <div class='hex<?php echo $kunik ?> gradient-bg mt-40'>
            <div class='hex<?php echo $kunik ?> bg-green'>
            	<?php if ($blockCms['typeInLeft'] == "Text") { ?>
              		<span style="color: <?=$blockCms["textLeftColor"] ?>;"> <?=$blockCms["textLeft"] ?>
              		</span>
            	<?php }else if ($blockCms['typeInLeft'] == "Icon") { ?>
            		<span class="fa fa-<?= $blockCms["iconLeft"] ?>" style="color: <?= $blockCms["iconLeftColor"] ?>;">
            		</span>
            	<?php } ?>
            </div>
          <span class="title-1 sp-text" data-id="<?= $blockKey ?>" data-field="title"><?php echo $blockCms["title"]; ?></span>
        </div> 
    </div>

</div>
<style id="titleWithStyle<?= $kunik ?>">
</style>
<script type="text/javascript">
	str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
     $("#titleWithStyle<?= $kunik ?>").append(str);
    if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var titleWithStyle = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeInLeft",
                                label : tradCms.displayOnTheLeftOfTheTitle,
                                tabs: [
                                    {
                                        value:"Text",  
                                        label: tradCms.textProperty,
                                        inputs : [
                                            {
                                                type : "inputSimple",
                                                name : "textLeft",
                                                label : tradCms.textOnTheLeft
                                            },
                                            {
                                            type: "colorPicker",
                                            name: "textLeftColor",
                                            label: tradCms.textColorOnTheLeft,
                                        },
                                        ]
                                    },
                                    {
                                        
                                        value:"Icon"  , 
                                        label: tradCms.iconProperty,   
                                        inputs :[
                                            {
                                                type: "inputIcon",
                                                name: "iconLeft",
                                                label: tradCms.iconOnTheLeft,
                                            },
                                            {
                                                type: "colorPicker",
                                                name: "iconLeftColor",
                                                label: tradCms.colorOfTheIcon,
                                            },
                                        ]
                                    }
                                ]
                            }
                        },
                    ],
                },
                style : {
                    inputsConfig : [
                        {
                           type: "color",
                           options: {
                               name: "bgcolor",
                               label: tradCms.backgroundColorOfTheTitle,
                               defaultValue :"<?= $blockCms["bgcolor"] ?>"
                           }
                        },
                        {
                           type: "color",
                           options: {
                               name: "colordecor",
                               label: tradCms.backgroundColorOfTheTitle + "(Decoration)",
                               defaultValue :"<?= $blockCms["colordecor"] ?>"
                           }
                        },
                    ]
                }
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        }
            cmsConstructor.blocks.titleWithStyle = titleWithStyle;
        }
</script>