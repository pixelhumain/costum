<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
?>
<style type="text/css">
.textBack_<?=$kunik?> h2{
  margin-bottom: 10px;
  margin-top: 10px;
}
.button_<?=$kunik?> {
  background-color: <?=isset($blockCms['imageLink']["buttonColor"]) ? $blockCms['imageLink']["buttonColor"] : $blockCms["buttonColor"] ?>;
  border:1px solid <?=isset($blockCms['imageLink']["buttonColorBorder"]) ? $blockCms['imageLink']["buttonColorBorder"] : $blockCms["buttonColorBorder"] ?>;
  color: <?=isset($blockCms['imageLink']["buttonColorlabel"]) ? $blockCms['imageLink']["buttonColorlabel"] : $blockCms["buttonColorlabel"] ?>;
  margin-bottom: 4%;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  padding: 10px;
  font-size: <?=isset($blockCms['imageLink']["buttonDsize"]) ? $blockCms['imageLink']["buttonDsize"] : $blockCms["buttonDsize"] ?>;
  cursor: pointer;
  border-radius: <?=isset($blockCms['imageLink']["buttonDBorderRadius"]) ? $blockCms['imageLink']["buttonDBorderRadius"] : $blockCms["buttonDBorderRadius"] ?>;
  padding: <?=isset($blockCms['imageLink']["buttonDPadding"]) ? $blockCms['imageLink']["buttonDPadding"] : $blockCms["buttonDPadding"] ?>;
}

/*.block-container-<?=$kunik?> {
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}*/

@media (max-width: 414px) {
  .textBack_<?=$kunik?> h2{
   font-size: 20px;
 }
 .textBack_<?=$kunik?> p{
  font-size: 14px
}

.button_<?=$kunik?> {
  padding: 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 11px;
  cursor: pointer;
  border-radius: 20px;
}
}

@media screen and (min-width: 1500px){
  .textBack_<?=$kunik?> h2{
    font-size: 40px;
  }
  .textBack_<?=$kunik?> p{
    line-height: 35px;
    font-size: 25px;
  }
  .button_<?=$kunik?> {
    margin-bottom: 2%;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    cursor: pointer;
  }
}
</style>

<section  class="parallax_<?=$kunik?> <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>" >
  <div class="textBack_<?=$kunik?> text-center container">
    <a href="javascript:;" class="btn button_<?=$kunik?>" data-toggle="modal" data-target="#modalRegister">
     <?= $blockCms["buttonLabel"]; ?>
   </a>
 </div>
</section>
<style id="createAccount<?= $kunik ?>">
</style>
<script type="text/javascript">
    str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
     $("#createAccount<?= $kunik ?>").append(str);
    if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var createAccount = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                           type: "inputSimple",
                           options: {
                               name: "blockUrlRedirect",
                               label: tradCms.urlOfTheRedirection,
                           }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                type: "inputSimple",
                                name: "buttonLabel",
                                label: tradCms.buttonLabel,
                            }
                        },
                    ],
                },
                style: { inputsConfig : [
                    {
                            type: "section",
                            options: {
                                name: "imageLink",
                                label: tradCms.settings,
                                showInDefault: true,
                                inputs: [
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttonColor",
                                            label: tradCms.buttonColor,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonColor"]) ? $blockCms['imageLink']["buttonColor"] : $blockCms["buttonColor"]; ?>"
                                        }
                                    },
                                    {
                                        type: "number",
                                        options: {
                                            units: [
                                              "px", "%"
                                            ],
                                            filterValue: cssHelpers.form.rules.checkLengthProperties,         
                                            name: "buttonDsize",
                                            label: tradCms.textsize,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonDsize"]) ? $blockCms['imageLink']["buttonDsize"] : $blockCms["buttonDsize"]; ?>"
                                        }
                                    },
                                    {
                                        type: "number",
                                        options: {
                                            units: [
                                              "px", "%"
                                            ],
                                            filterValue: cssHelpers.form.rules.checkLengthProperties,         
                                            name: "buttonDBorderRadius",
                                            label: tradCms.borderRadius,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonDBorderRadius"]) ? $blockCms['imageLink']["buttonDBorderRadius"] : $blockCms["buttonDBorderRadius"]; ?>"
                                        }
                                    },
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttonColorlabel",
                                            label: tradCms.colorOfTheButtonLabel,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonColorlabel"]) ? $blockCms['imageLink']["buttonColorlabel"] : $blockCms["buttonColorlabel"]; ?>"
                                        }
                                    },
                                    {
                                        type: "color",
                                        options: {
                                            type: "color",
                                            name: "buttonColorBorder",
                                            label: tradCms.buttonCorderColor,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonColorBorder"]) ? $blockCms['imageLink']["buttonColorBorder"] : $blockCms["buttonColorBorder"]; ?>"
                                        }
                                    },
                                    {
                                        type: "number",
                                        options: {
                                            units: [
                                              "px", "%"
                                            ],
                                            filterValue: cssHelpers.form.rules.checkLengthProperties,    
                                            name: "buttonDPadding",
                                            label: tradCms.buttonPadding,
                                            defaultValue :"<?= isset($blockCms['imageLink']["buttonDPadding"]) ? $blockCms['imageLink']["buttonDPadding"] : $blockCms["buttonDPadding"]; ?>"
                                        }
                                    },
                                ]
                            }
                        },
                    ]
                }
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        }
            cmsConstructor.blocks.createAccount = createAccount;
        }
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($blockCms); ?>;
  jQuery(document).ready(function() {

    Login.runRegisterValidator = function() {  
      mylog.log("runRegisterValidator!!!!");
      var form3 = $('.form-register');
      var errorHandler3 = $('.errorHandler', form3);
      var createBtn = null;
      if(Login.registerOptions.mode=="normal"){
        Login.addPostalCodeInRegister();
      }
      form3.validate({
        rules : Login.rulesRegister,
        submitHandler : function(form) { 
          mylog.log("runRegisterValidator submitHandler");
           
          errorHandler3.hide();
          //createBtn.start();
          $(".createBtn").prop('disabled', true);
          $(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
          params=Login.formatDataRegister();
          ajaxPost(
            null,
            baseUrl+"/"+moduleId+"/person/register",
            params,
            function(data){
              if(data.result) {
                $(".createBtn").prop('disabled', false);
                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
                $("#registerName").val("");
                $("#username").val("");
                $("#email3").val("");
                $("#password3").val("");
                $("#passwordAgain").val("");
                $(".form-register #addressLocality").val("");
                $(".form-register #postalCode").val("");
                $(".form-register #city").val("");
                $('#agree').prop('checked', true);
                mylog.log("authenticate",data);
                if((typeof data.isInvitation != "undefined" && data.isInvitation) || Login.registerOptions.loginAfter){
                  toastr.success(data.msg);
                  if(typeof themeParams.pages["#app.index"].redirect != "undefined" 
                    && typeof themeParams.pages["#app.index"].redirect.register != "undefined"){
                    history.pushState(null, "New Title","#"+themeParams.pages["#app.index"].redirect.register);
                    document.location.href =  "<?= isset($blockCms['imageLink']["blockUrlRedirect"]) ? $blockCms['imageLink']["blockUrlRedirect"] : $blockCms["blockUrlRedirect"]; ?>";
                    urlCtrl.loadByHash(location.hash);
                  }else{
                    history.pushState(null, "New Title",'#page.type.citoyens.id.'+params.pendingUserId);
                    document.location.href = "<?= isset($blockCms['imageLink']["blockUrlRedirect"]) ? $blockCms['imageLink']["blockUrlRedirect"] : $blockCms["blockUrlRedirect"]; ?>";
                    urlCtrl.loadByHash(location.hash);
                  }
                } else{
                  $('.modal').modal('hide');
                  $("#modalRegisterSuccessblockContent").html("<h3><i class='fa fa-smile-o fa-4x text-green'></i><br><br> "+data.msg+"</h3>");
                  $("#modalRegisterSuccess").modal({ show: 'true' }); 
                  $('#modalRegisterSuccess .btn-default').click(function() {
                    mylog.log("hide modale and reload");
                    $('.modal').modal('hide');
                    document.location.href =  "<?= isset($blockCms['imageLink']["blockUrlRedirect"]) ? $blockCms['imageLink']["blockUrlRedirect"] : $blockCms["blockUrlRedirect"]; ?>";
                    urlCtrl.loadByHash(location.hash);
                  });
                }
              }else {
                $('.registerResult').html(data.msg);
                $('.registerResult').show();
                $(".createBtn").prop('disabled', false);
                $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
              }
            },
            function(data) {
              toastr.error(trad["somethingwentwrong"]);
              $(".createBtn").prop('disabled', false);
              $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in"); 
            }
          );
          return false;
        },
        invalidHandler : function(event, validator) {//display error alert on form submit
          errorHandler3.show();
          $(".createBtn").prop('disabled', false);
          $(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
          //createBtn.stop();
        }
      });
    }
  Login.runRegisterValidator();

});
</script>