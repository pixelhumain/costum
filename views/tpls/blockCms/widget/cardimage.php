<?php
$myCmsId = $blockCms["_id"]->{'$id'};


$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;


$defaultData = [
    "image1" => "$assetsUrl/images/thumbnail-default.jpg",
    "image2" => "$assetsUrl/images/thumbnail-default.jpg"
];


if (isset($defaultData)) {
    $blockCms = array_replace_recursive($defaultData, $blockCms);
}


$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) ["$kunik-cardimage" => $objectCss];

$img1 = $assetsUrl;
$img1 .= $blockCms['image1'];


?>


    <div class="<?= $kunik ?>-cardimage" data-kunik="<?= $kunik ?>" data-name="cardimage" data-id="<?= $myCmsId ?>">
        <div class="containere">
            <!-- <img class="hand cardimage-image" src="<?php echo $blockCms['image1'] ?>" alt="" width="100%" height="100%">
            <img class="hand cardimage-image" src="<?php echo $blockCms['image2'] ?>" alt="" width="100%" height="100%">
            <?php echo $blockCms['image1'] ?> -->
        </div>
    </div>



<style id="cardimage<?= $kunik ?>">

    .<?= $kunik ?>-cardimage {
        width:100% !important;
        height:100% !important;
        position:relative;
    }

    .<?= $kunik ?>-cardimage:hover .containere {
        opacity: 0 !important;
    }

    .<?= $kunik ?>-cardimage .containere {
        width:100% !important;
        height:100% !important;
        background-repeat: no-repeat;
        background-size: cover;
        background-image : url(<?php echo $blockCms['image1'] ?>);
        transition: all 0.3s ease;
    }

    .<?= $kunik ?>-cardimage .containere:hover{
        background-image : url(<?php echo $blockCms['image2'] ?>) !important;
        transform: scale(1.03);
        opacity: 1 !important;
    }



</style>

<script>

    $(".<?= $kunik ?>-cardimage").on( "mouseover", function() {
        $(this).css("cursor", "pointer");
    })

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#cardimage<?= $kunik ?>").append(str);


    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var cardimage = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
							type : "inputFileImage",
							options : {
								name : "image1",
								label : tradCms.uploadeImage,
                                collection : "cms"
							}
						},
                        {
							type : "inputFileImage",
							options : {
								name : "image2",
								label : tradCms.uploadeImage,
                                collection : "cms"
							}
						},
                        "background"
                    ],
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },

            },
            onChange: function(path, valueToSet, name, payload, value) {

              if(typeof cssHelpers.json[name] != "undefined") {
                 cssHelpers.render.addStyleUI(cmsConstructor.kunik + "." + name,value);
              }
              if(name == 'image1') {
                $('.<?= $kunik ?>-cardimage .cardimage-image1').attr('src', value);
              }
              if(name == 'image2') {
                $('.<?= $kunik ?>-cardimage .cardimage-image2').attr('src', value);
              }

            },
        }
        cmsConstructor.blocks.cardimage = cardimage;
    }
</script>