<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];
?>
<style id="css-<?= $kunik ?>">
    .mx-2<?= $kunik ?>{
        margin-left: 1em;
        margin-right: 1em;
    }
    .truncate<?= $kunik ?>{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    li.indicator_<?=$kunik?>{
        background-color: #dddddd !important;
        width: 15px !important;
        height: 15px !important;
        margin-right: .3em !important;
        margin-left: .3em !important;
    }
    .projet_<?= $kunik ?> .color-h2{
        margin-top: 5%;
    }
    .projet_<?= $kunik ?> .a-caroussel{
        color: <?= $blockCms["iconActionColor"] ?>;
        text-decoration : none;
    }
    .projet_<?= $kunik ?> .modal-footer{
        border : none !important;
    }
    .projet_<?= $kunik ?> .carousel-indicators{
        bottom: 0px;
        position: relative;
    }
    .projet_<?= $kunik ?> .carousel-indicators li{
        border: 1px solid #8fcd52;
    }
    .projet_<?= $kunik ?> .arrow{
        margin-top: 2%; 
        font-size: 3rem; 
        color: #abb76b;
    }
    .projet_<?= $kunik?> .btn-edit-delete{
        display: none;
        z-index: 1000;
    }
    .projet_<?= $kunik?>:hover  .btn-edit-delete {
        display: block;
        -webkit-transition: all 0.9s ease-in-out 9s;
        -moz-transition: all 0.9s ease-in-out 9s;
        transition: all 0.9s ease-in-out 0.9s;
        position: absolute;
        left: 50%;
        transform: translate(-50%,-50%);
    }
    .projet_<?= $kunik ?> h1 i{
        color: <?= $blockCms["icon"]["iconColor"] ?>;
    }
    @media (max-width:768px){
        
        .projet_<?= $kunik ?>  .color-h2{
            margin-top: 5%;
        }
        .projet_<?= $kunik ?>  .a-caroussel{
            color:<?= $blockCms["iconActionColor"] ?>;
            text-decoration : none;
        }
        .projet_<?= $kunik ?>  .modal-footer{
            border : none !important;
        }
        .projet_<?= $kunik ?>  .carousel-indicators{
            bottom: 0px;
            position: relative;
        }
        .projet_<?= $kunik ?>  .carousel-indicators li{
            border: 1px solid #8fcd52;
        }
        .projet_<?= $kunik ?>  .carousel-indicators .active{
            background-color: #8fcd52;
        }
    }
</style>
<section class="projet_<?= $kunik ?> text-center">
    <h1 id="title<?= $kunik ?>" class="text-center title  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <i class="<?= $blockCms["icon"]["iconStyle"]?>"></i>
        <span><?= $blockCms["title"]; ?></span>
    </h1>
    <div class="text-center btn-edit-delete ">
        <?php if(Authorisation::isInterfaceAdmin()){ ?>
            <?php if($blockCms["elementType"]=="projects"){ ?>
                <button class="btn btn-primary btn-xs" >
                    <?php echo Yii::t('cms', "add element")?>
                </button>
            <?php } ?>

            <?php if($blockCms["elementType"]=="citoyens"){ ?>
                <a class="btn lbh btn-primary btn-xs" href="#element.invite.type.citoyens.id.<?= $_SESSION['userId'] ?>">Ajouter un élément</a>
            <?php } ?>
        <?php } ?>
    </div>
    <p class="description sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description">
        <?= $blockCms["description"]; ?>
    </p>
    <div id="elementsCarousel<?= $kunik ?>" class="carousel slide text-center" data-ride="carousel" data-type="multi">
        <div id="slider<?= $kunik ?>" class="text-center arrow hidden" style="">
            <a class="left a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="prev">  
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
            <a class="right a-caroussel" href="#elementsCarousel<?= $kunik ?>" data-slide="next">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
            </a>
        </div>
        <br>
        <div id="content-results-project<?= $kunik ?>" class="carousel-inner"></div>
        <div class="col-md-12" style="margin-top: 1%;">
            <ol class="carousel-indicators" id="indactors-elementsCarousel<?= $kunik ?>"></ol>
        </div>
    </div>
</section>

<script type="text/javascript">
    let nbItem<?= $kunik ?> = <?= $blockCms["limit"] ?>;
    let elementType<?= $kunik ?> = "<?= $blockCms["elementType"] ?>";
    tplCtx = {};
    sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    $(document).ready(function(){
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var projectCarousel = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type: "select",
                                options: {
                                        name: "elementType",
                                        label: tradCms.element,
                                        options: $.map({
                                            "projects":tradCms.projectListCommunity,
                                            "citoyens":tradCms.caummunautyMember
                                        }, function(key, val) {
                                            return {
                                                value: val,
                                                label: key
                                            }
                                    }),
                                }
                            },
                            {
                                type: "select",
                                options: {
                                    name: "imageForm",
                                    label: tradCms.imageShape,
                                    options: $.map({"circle" : tradCms.imageRound, "rounded": tradCms.imageSquare }, function(key, val) {
                                        return {
                                            value: val,
                                            label: key
                                        }
                                    }),
                                }
                            },
                            {
                                type : "section",
                                options : {
                                    name : "icon",
                                    label: tradCms.icon,
                                    inputs : [
                                        {
                                            type: "inputIcon",
                                            options: {
                                                name: "iconStyle",
                                                label: tradCms.iconType,
                                                defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.icon.iconStyle
                                            },
                                        },
                                        {
                                            type: "color",
                                            options: {
                                                name: "iconColor",
                                                label: tradCms.color,
                                                defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.icon.iconColor
                                            },
                                            
                                        },
                                    ],
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    type : "inputSimple",
                                    name : "category",
                                    label : tradCms.projectCategoryToShow,
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    type : "inputSimple",
                                    name : "limit",
                                    label : tradCms.NumberProjectToShow,
                                }
                            },
                            {
                                type : "inputSimple",
                                options : {
                                    type : "inputSimple",
                                    name : "textPreview",
                                    label : tradCms.seeMoreText,
                                }
                            }
                        ],
                    },
                    style: {
                        inputsConfig: [
                            {
                                type: "color",
                                options: {
                                    name: "iconActionColor",
                                    label: tradCms.seeMoreBouttonColor,
                                    type : "color",
                                    defaultValue :  sectionDyf.<?php echo $kunik ?>blockCms.iconActionColor
                                }
                            },
                        ],
                    },
                },
                afterSave: function(path, valueToSet, name, payload, value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                }
            }
            cmsConstructor.blocks.projectCarousel = projectCarousel;
        }
        // Affichage
        var params<?= $kunik ?> = {
            source : costum.contextSlug,
            element : elementType<?= $kunik ?>,
            id : costum.contextId
        };

        if (notNull(sectionDyf.<?php echo $kunik ?>blockCms.category)){
            params<?= $kunik ?>.category = sectionDyf.<?php echo $kunik ?>blockCms.category;
        }

        ajaxPost(
            null,
            baseUrl+"/costum/filiere/get-related-elements",
            params<?= $kunik ?>,
            function(data){
                mylog.log("success", data);
                var html = "";
                var li = "";
                var y= 0;

                if(data.result == true){
                    i = 0;
                    url1 = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
                    url2 = "<?= Yii::app()->getModule('co2')->assetsUrl; ?>/images/thumb/default_citoyens.png";
                    html += "<div class='item active'>";
                    li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='active indicator_<?=$kunik?>'></li>";

                    if(Object.entries(data.elt).length <= nbItem<?= $kunik ?>){
                        $("#slider<?= $kunik ?>").remove();
                        $("#title<?= $kunik ?>").css("margin-bottom","1em");
                    }

                    for (const [k, v] of Object.entries(data.elt)) {
                        
                        let imgThumb = url1;
                        
                        if(elementType<?= $kunik ?>=="projects"){
                            imgThumb = url1;
                        }else{
                            imgThumb = url2;
                        }
                        
                        if(typeof v.profilImageUrl != "undefined" && v.profilImageUrl != null && v.profilImageUrl != "none"){
                            imgThumb = v.profilImageUrl; 
                        }

                        let descript = "";

                        if(i >= nbItem<?= $kunik ?>){
                            html += "</div>";
                            html += "<div class='item'>";
                            y++;
                            li += "<li data-target='#elementsCarousel<?= $kunik ?>' data-slide-to='"+y+"' class='indicator_<?=$kunik?>'></li>";
                            i = 0;
                            $(".arrow").removeClass("hidden");
                        }

                        i++;
                                    
                        html += "<div class='card-info col-sm-12 col-md-"+(12/nbItem<?= $kunik ?>)+"'>";

                        html += "<center><img src='"+imgThumb+"' class='img-responsive img-rounded bg-light' style='<?=($blockCms["imageForm"]=="circle")?"border-radius : 50%; width: 150px; max-width: 150px;":"width: 200px; max-width: 200px;";?> height: 150px; max-height: 150px; object-fit: cover; background-color:#eeeeee'>";
                        
                        html += "<h2 class='color-h2 mx-2<?= $kunik ?> mb-1 title-2 text-center truncate<?= $kunik ?>'>"+v.name+"</h2></center>";
                                
                        if(v.shortDescription){
                            descript = v.shortDescription;
                        }else if(v.email){
                            descript = v.email
                        }

                        html += "<p class='description mx-2<?= $kunik ?> truncate<?= $kunik ?>'>"+descript.substring(0, 90)+"</p>";
                        
                        html += "<a href='#@"+v.slug+"' class='lbh-preview-element a-caroussel' data-hash='#page.type."+elementType<?= $kunik ?>+".id."+k+"'><?= $blockCms["textPreview"] ?></a>";
                        html += "</div>";
                    }
                }else{
                    html += "Pas d'Elements à Afficher";
                    li += "";
                }
                $("#content-results-project<?= $kunik ?>").html(html);
                $("#indactors-elementsCarousel<?= $kunik ?>").html(li);
            }
        );
        $('.carousel[data-type="multi"] .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
            for (var i = 0; i < nbItem<?= $kunik ?>; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));
            }
        });
    });
</script>



