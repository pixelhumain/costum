
<?php 
    $myCmsId  = $blockKey;
    if(isset($costum)){
        $myCmsId  = $blockCms["_id"]->{'$id'};
    }
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    if(!isset($costum)){
        $costum = CacheHelper::getCostum();
    }
?>
<style id="css-<?= $kunik ?>">
    .column-header-text {
        color: #000000 !important;
    }
    .kanban-content {
        display: flex;
    }
    .filtre {
        display : flex;
        padding : 0 20px;
    }
    .filtre .filtre-type {
        margin-right : 20px;
    }
    .active-mile {
        background-color : gray !important;
        color : #172b4d !important;
    }
    .kanban-move-card, .kanban-overlay {
        z-index: 9999999999999999 !important;
    }
    .kanbanAction-<?= $kunik ?> {
        height : 90vh !important;
    }
    .loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
        
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> .processingLoader {
        width : auto !important;
        height : auto !important;
    }
    .backdrop<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }
</style>

<div class="<?= $kunik ?> <?= $kunik ?>-css">
    <div id="action-preview<?= $blockKey ?>"></div>
    <div class="content<?=$kunik?>">
        <div class="kanban-content">
            <div class="filtre">
                <div class="filtre-name">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"> </i></span>
                        <input id="nameMember<?=$kunik?>" type="text" class="form-control" name="nameMember<?=$kunik?>" placeholder="Que recherchez-vous?">
                    </div>
                </div>
            </div>
            <div class="projet-content">
                <div class="button-projet">
                    <button type="button" id="action<?= $kunik ?>" class="btn btn-info hidden"><i class="fa fa-tasks"> </i> Tous les actions</button>
                </div>
            </div>
        </div>
        <div data-kunik="<?= $kunik ?>" class="kanbanAction-<?= $kunik ?>" data-name="kanbanAction" data-id="<?= $myCmsId ?>" style="margin-bottom:70px !important;">
            <div id="kanbanAction<?= $kunik ?>">
            </div>
        </div>
    </div>
    <div class="loader<?= $kunik ?> hidden" id="loader<?= $kunik ?>" role="dialog">
        <div class="content<?= $kunik ?>" id="content<?= $kunik ?>"></div>
    </div>
    <div class="backdrop<?= $kunik ?> hidden" id="backdrop<?= $kunik ?>"></div>
</div>
<script type="text/javascript">
    if(typeof costum != 'undefined' && costum !== null){
        sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
            var ocecoKanban = {
                configTabs: {
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks.oceco<?= $myCmsId ?> = ocecoKanban;
        }
    }
</script>

<script type="text/javascript">
    $(function () {
        var load<?=$kunik?> = true;
        var typeCostum = "";
        var idCostum = "";
        var slugCostum = "";
        var kanbanHeader = [
            { id: 'todo', label: 'A faire' , editable : false},
            { id: 'tracking', label: 'En cours' , editable : false },
            { id: 'totest', label: 'A tester' , editable : false },
            { id: 'done', label: 'Terminer' , editable : false }
        ]
        var milestoneData = [];
        if(contextData != null && typeof contextData != 'undefined'){
            idCostum = contextData.id;
            typeCostum = contextData.type;
            slugCostum = contextData.slug;
        } else if(typeof costum != 'undefined' && costum !== null){
            if(typeof costum.contextType !='undefined'){
                idCostum = costum.contextId;
                typeCostum = costum.contextType;
                slugCostum = costum.contextSlug;
            }
        }
        if(typeCostum != 'projects') {
            var txt = "<h1>L'element doit etre un projet utiliser ce kanban</h1>"
            $('.content<?=$kunik?>').empty().append(txt)
            return;
        }
        coInterface.showLoader('#content<?= $kunik ?>')
        function show_action_detail_modal(__id) {
            return new Promise(function (__resolve) {
                const url = baseUrl + '/costum/project/action/request/action_detail_html';
                const post = {
                    id : __id,
                    connectedUser: userConnected._id.$id
                };
                ajaxPost(null, url, post, __resolve, null, 'text');
            });
        }
        function filterKanban() {
            load<?=$kunik?> = false;
            let objet = {
                columns : ['milestone', 'listRole', 'todo', 'totest', 'tracking', 'done'],
                attributes : {
                    
                }
            }
            // if($('#typeMember<?=$kunik?>').val() != '') {
            //     objet.attributes.collection = $('#typeMember<?=$kunik?>').val()
            // } 
            if($('#nameMember<?=$kunik?>').val() != '') {
                objet.card = $('#nameMember<?=$kunik?>').val()
            } 
            $('#kanbanAction<?= $kunik ?>').kanban('filter', objet);
        }
        $('#nameMember<?=$kunik?>').on('input', filterKanban)
        $('#action<?= $kunik ?>').on('click', function(){
            $('#contributor-filter-container').html('')
            var my<?= $kunik ?> = ''
            var activeProjet<?= $kunik ?> = ''
            var activeMilestone<?= $kunik ?> = ''
            $('#loader<?= $kunik ?>').removeClass('hidden')
            $('#backdrop<?= $kunik ?>').removeClass('hidden')
            $('#projetBread').data('ide', '')
            
            var interv = setInterval(() => {
                $('#kanbanAction<?= $kunik ?>').kanban('destroy')
                loadKanbanAction('all', '')
                
                clearInterval(interv);
                interv = null;    
            }, 50); 
        })
        function loadContextData(){
            milestoneData = [];
            var dataProjet = {}
            var params = {     
                searchType : ["projects"],
                notSourceKey : true,
                fields : ["oceco"],
                'filters' : {
                    'slug' : slugCostum
                },
                indexStep : 0        
            };
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    dataProjet = data.results
                    mylog.log(data.results, "milestone")
                },null,null, {async:false}
            )
            let milestoneHeader = {
                id : 'milestone', label : "<i class='fa fa-flag'> </i> Les jalons de "+slugCostum, menus : [], editable : false
            }
            kanbanHeader.splice(0,0,milestoneHeader)
            $.map(dataProjet, function(valeur, keys) {
                if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined' && Array.isArray(valeur.oceco.milestones)) {
                    for(let i = 0 ; i < valeur.oceco.milestones.length ; i++) {
                        let milestoneCard = {
                            _id : valeur.oceco.milestones[i].milestoneId,
                            id : valeur.oceco.milestones[i].milestoneId,
                            title : "<b>"+valeur.oceco.milestones[i].name+"</b>",
                            header : "milestone",
                            canEditHeader: false,
                            canAddCard: false,
                            html : true,
                            name : valeur.oceco.milestones[i].name,
                            collection : valeur.collection,
                            slug : valeur.slug,
                            canMoveCard: false,
                            actions : [
                                {
                                    'icon': 'fa fa-trash',
                                    'bstooltip' : {
                                        'text' : 'Supprimer le milestone', 
                                        'position' : 'left'
                                    }, 
                                    'action': 'onRemoveMilestone'
                                }
                            ]
                        }
                        milestoneData.push(milestoneCard)
                    }
                }
            })
        }
        loadContextData()
        loadKanbanAction('all', '')
        function loadKanbanAction(linkAction, milesId) {
            let kanbanData = [];
            kanbanData.push(...milestoneData)
            mylog.log(kanbanData, 'kanbanData')
            var urlBase = 'https://www.communecter.org';
            // alert(linkAction)
            var dataAction = []
            let paramsKanbanAction = {     
                "connectedUser" : userConnected._id.$id,
                "filters[urls][$in][]" : location.href,
                "filters[parentId]" : idCostum
            };
            if(milesId != '') {
                paramsKanbanAction["filters"] = { 'milestone.milestoneId' : milesId }
            }
            ajaxPost(
                null,
                baseUrl+"/costum/project/action/request/actions/scope/"+linkAction,
                paramsKanbanAction,
                function(data){
                    dataAction = data;
                },null,null, {async:false}
            )
            mylog.log(dataAction, 'dataAction '+linkAction)
            
            for(let i = 0 ; i < dataAction.length ; i++) {
                if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "done") {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "done",
                        html : true,
                        canMoveCard: true,
                        actions : [
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    kanbanData.push(actionsItem)
                } 
                else if (typeof dataAction[i].tags != "undefined" && Array.isArray(dataAction[i].tags) && dataAction[i].tags.indexOf('totest') > -1) {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "totest",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    kanbanData.push(actionsItem)
                } 
                else if (typeof dataAction[i].tracking != "undefined" && dataAction[i].tracking == true) {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "tracking",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    kanbanData.push(actionsItem)
                } 
                
                else if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "todo") {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "todo",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    kanbanData.push(actionsItem)
                }
            }

            let kanbanDom = $('#kanbanAction<?= $kunik?>')
            .kanban({
                headers : kanbanHeader,
                data : kanbanData,
                editable: false,
                language : mainLanguage,
                canAddCard: true,
                editable : false,
                canAddColumn: false,
                canEditCard: false,
                canEditHeader: true,
                canMoveCard: true,
                canMoveColumn: true,
                readonlyHeaders: ['milestone'],
                copyWhenDragFrom: [],
                endpoint : `${baseUrl}/plugins/kanban/`,
                defaultColumnMenus: [''],
                onRenderDone(){
                    $('#action<?= $kunik ?>').addClass('hidden')
                    $('#backdrop<?= $kunik ?>').addClass('hidden')
                    $('#loader<?= $kunik ?>').addClass('hidden')
                    $('#kanban-wrapper-todo .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-tracking .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-totest .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-done .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-milestone .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-milestone .kanban-composer-container').html('')
                    $('#kanban-wrapper-milestone .kanban-list-content').css({
                        'background-color' : 'rgb(183, 183, 183)'
                    })
                    if(milesId != '') {
                        $('.kanban-list-card-detail[data-id = '+milesId+']').addClass('active-mile')
                        $('#action<?= $kunik ?>').removeClass('hidden')
                    }
                },
                onRemoveAction : function(action, cardDom){
                    $.confirm({
                        title : trad['Archive'], type : 'orange', content : trad['Please confirm archiving'], buttons : {
                            no : {
                                text : trad.no, btnClass : 'btn btn-default'
                            }, yes : {
                                text : trad.yes, btnClass : 'btn btn-warning', action : function(){
                                    var params = {
                                        id : action._id, collection : 'actions', path : 'status', value : 'disabled'
                                    };
                                    
                                    dataHelper.path2Value(params, function(response){
                                        if (response.result) {
                                            var index = kanbanDom.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(cardDom);
                                            kanbanDom.kanban('deleteData', {column : action.header, id : action.id});
                                            // ajaxPost(null, coWsConfig.pingActionManagement, {
                                            //     event : '.archive' + aapObj.common.getQuery().projectId, data : {
                                            //         action : action.id,
                                            //         emiter : '',
                                            //         column : action.header
                                            //     }
                                            // }, null, null, {contentType : 'application/json'});
                                        }
                                    });
                                }
                            }
                        }
                    });
                },
                onCardInsert : function(newAction){
                    const url = baseUrl + '/costum/project/project/request/room';
                    const post = {
                        project : idCostum,
                        connectedUser: userConnected._id.$id
                    };
                    let room = '';
                    ajaxPost(null, url, post, function (result) {
                        room = result;
                    },null,null, {async:false});
                    newAction = $.extend({}, newAction);
                    var actionId = newAction.id;
                    var path2Value = {
                        collection : 'actions',
                        path: 'allToRoot'
                    };
                    
                    if (['done', 'todo'].includes(newAction.header)) {
                        path2Value.value = {
                            name : newAction.title,
                            status : newAction.header
                        };
                    } else if (newAction.header === 'tracking') {
                        path2Value.value = {
                            name : newAction.title,
                            status : 'todo',
                            tracking: true
                        };
                        path2Value.setType = [{type : 'boolean', path : 'tracking'}];
                    } else {
                        var dictionnary = {totest : 'totest'};
                        path2Value.value = {
                            name : newAction.title,
                            status : 'todo',
                            tags : [dictionnary[newAction.header]]
                        };
                    }
                    path2Value.value.parentId = idCostum;
                    path2Value.value.creator = userConnected._id.$id;
                    path2Value.value.idUserAuthor = userConnected._id.$id;
                    if (room) {
                        path2Value.value.idParentRoom = room;
                    }
                    path2Value.value.parentType = 'projects';
                    
                    dataHelper.path2Value(path2Value, function(response){
                        if (response.result && response.saved && !response.saved.error) {
                            toastr.success(response.saved.msg);
                            newAction.id = response.saved.map._id.$id;
                            newAction._id = response.saved.map._id.$id;
                            switch (newAction.header) {
                                case 'tracking':
                                    newAction.tracking = true;
                                    break;
                                case 'totest':
                                    newAction.tags = ['totest'];
                                default:
                                    newAction.status = newAction.header;
                                    break;
                            }
                            
                            // send create notification
                            ajaxPost(null, baseUrl + '/survey/answer/rcnotification/action/newaction', {
                                actname : newAction.title,
                                projectId : idCostum
                            }, function(){});
                            
                            newAction.actions = [{
                                icon : 'fa fa-trash',
                                bstooltip : {text : trad.delete, position : 'top'},
                                action : 'onCardArchiveClick',
                                bstooltip : {text : trad['Archive'], position : 'top'}
                            }];
                            $('#kanbanAction<?= $kunik?>').kanban('setData', {column : newAction.header, id : actionId, data : newAction});
                            // ajaxPost(null, coWsConfig.pingActionManagement, {
                            //     event : '.kanban-insert-card' + aapObj.common.getQuery().projectId, data : {
                            //         id : newAction.id, action : newAction
                            //     }
                            // }, null, null, {contentType : 'application/json'});
                        }
                    });
                },
                onCardClick(clickedData, style) {
                    mylog.log('clickedData', clickedData)
                    if(clickedData.header == 'milestone') {
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        
                        var interv = setInterval(() => {
                            $('#kanbanAction<?= $kunik ?>').kanban('destroy')
                            loadKanbanAction('all', clickedData._id)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);
                    } else if(clickedData.header == 'todo' || clickedData.header == 'totest' || clickedData.header == 'tracking' || clickedData.header == 'done') {
                        var previewDom = $('#action-preview<?= $blockKey ?>');
                        previewDom.empty();
                        show_action_detail_modal(clickedData._id).then(function (__html) {
                            previewDom.empty().html(__html);
                        });
                    }
                },
                onCardDrop(dropped, updates){
                    if (dropped.origin != dropped.target) {
                        let path2ValueUrl = {};
                        path2ValueUrl.url = urlBase;
                        let actionObjet = {
                            id : dropped.data._id,
                            collection : 'actions',
                            costumSlug : slugCostum,
                            costumEditMode : 'false'
                        };
                        let resetObjet = {
                            id : dropped.data._id,
                            collection : 'actions',
                            costumSlug : slugCostum,
                            costumEditMode : 'false'
                        };
                        if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                            resetObjet['value'] = 'todo'
                            resetObjet['path'] = 'status'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'todo'
                            resetObjet['path'] = 'status'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                            
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                        } 
                        mylog.log(dropped, updates, 'data drop')
                    }
                }
            })
        }
    })
</script>