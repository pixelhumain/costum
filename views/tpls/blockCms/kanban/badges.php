
<?php 
    $myCmsId  = $blockKey;
    if(isset($costum)){
        $myCmsId  = $blockCms["_id"]->{'$id'};
    }
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    if(!isset($costum)){
        $costum = CacheHelper::getCostum();
    }
?>

<?php
    $kanbanAssets = [
        '/plugins/kanban/kanban.js',  '/plugins/kanban/kanban.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $kanbanAssets,
        Yii::app()->request->baseUrl
    );
?>


<style id="css-<?= $kunik ?>">
    .column-header-text {
        color: #000000 !important;
    }
    .kanban-<?= $kunik ?> {
        height : 82vh !important;
    }
    .filtre {
        display : flex;
        padding : 0 20px;
    }
    .filtre .filtre-type {
        margin-right : 20px;
    }
    .filtre .filtre-name {
        margin-right : 20px;
    }
    #ajax-modal.portfolio-modal.modal {
        z-index: 3000000 !important;
    }
    .bootbox {
        z-index: 99999999999999 !important;
    }
</style>
<div class="<?= $kunik ?> <?= $kunik ?>-css">
    <div class="filtre">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-search"> </i></span>
            <input id="nameBadge<?=$kunik?>" type="text" class="form-control" name="nameBadge<?=$kunik?>" placeholder="Que recherchez-vous?">
        </div>
    </div>
        
    <div data-kunik="<?= $kunik ?>" class="kanban-<?= $kunik ?>" data-name="kanban" data-id="<?= $myCmsId ?>" style="margin-bottom:70px !important;">
        <div id="kanban<?= $kunik ?>">
        </div>
    </div>
</div>

<script type="text/javascript">
    if(typeof costum != 'undefined' && costum !== null){
        sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum != null && costum.editMode == true) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
            var kanbanBadges = {
                configTabs: {
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks.badges<?= $myCmsId ?> = kanbanBadges;
        }
    }
</script>
<script type="text/javascript">
    
    $(function () {
        
        var data<?= $kunik ?> = [];
        var dataMember<?= $kunik ?> = {};
        var dataBadges<?= $kunik ?> = {};
        var dataPerBadges<?= $kunik ?> = {}
        var typeCostum = "";
        var idCostum = "";
        if(typeof contextData != 'undefined' && contextData != null && typeof contextData.id != 'undefined' && typeof contextData.type != 'undefined'){
            idCostum = contextData.id;
            typeCostum = contextData.type;
        } else if(typeof costum != 'undefined' && costum !== null && typeof costum.contextId != 'undefined' && typeof costum.contextType != 'undefined'){
            idCostum = costum.contextId;
            typeCostum = costum.contextType;
        }

        var defImg = modules.co2.url + '/images/thumbnail-default.jpg';

        var badgeParams<?= $kunik ?> = {     
            "searchType[]" : "badges",
            "indexMin" : 0,
            "initType" : "",
            "countType" : "",
            "indexStep" : 0,
            "notSourceKey" : true,
            "filters[preferences.private]" : false,
            "filters" : {
                "$or" : {}
            },
            "locality" : "",
            "fediverse" : false
        };

        badgeParams<?= $kunik ?>.filters.$or["issuer."+idCostum] = {"$exists" : true}

        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            badgeParams<?= $kunik ?>,
            function(data){
                dataBadges<?= $kunik ?> = data.results;
            },null,null, {async:false}
        )

        var memberParams<?= $kunik ?> = {     
            searchType : ["citoyens","organizations"],
            notSourceKey : true,
            fields : ["links"],
            filters : {},
            indexStep : 0        
        };

        if(typeCostum === "organizations"){
            memberParams<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
        }
        if(typeCostum === "projects"){
            memberParams<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
        }
        ajaxPost(
            null,
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            memberParams<?= $kunik ?>,
            function(data){
                dataMember<?= $kunik ?> = data.results;
            },null,null, {async:false}
        )

        var headers<?= $kunik ?> = [
            { id: 'memberof', label: 'Les membres' , editable : false,
                menus: [
                    { label: 'Envoyer une invitation', action: 'onInvite' }
                ]
            },
            { id: 'listBadges', label: 'Les badges' , editable : false,
                menus: [
                    { label: 'Ajouter un badge', action: 'onAddBadge' }
                ]
            }
        ]
        mylog.log(dataMember<?= $kunik ?>, "dataMember")

        // function(type, links) {
            $.map( 
                dataMember<?= $kunik ?> , function( valeur, keys ) {
                    let memberCard = {
                        _id : keys,
                        id : keys,
                        title : `<div><img src='${typeof valeur.profilImageUrl != "undefined" ? valeur.profilImageUrl : defImg}' style='width : 30px; height : 30px; border-radius: 50%' /> <b>${valeur.name}</b></div>`,
                        header : "memberof",
                        canEditHeader: false,
                        html : true,
                        name : valeur.name,
                        collection : valeur.collection,
                        profilThumbImageUrl : valeur.profilThumbImageUrl,
                        actions : [
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveMember'
                            },
                            {
                                'icon': valeur.collection == 'citoyens' ? 'fa fa-user' : 'fa fa-group',
                                'badge' : valeur.collection,
                                'action': ''
                            }
                        ]
                    }
                    data<?= $kunik ?>.push(memberCard) 

                }
            ); 
        // } 

            $.map( 
                dataBadges<?= $kunik ?> , function( valeur, keys ) {
                    let memberCard = {
                        _id : keys,
                        id : keys,
                        title : "<b>"+valeur.name+"</b>",
                        header : "listBadges",
                        canMoveCard: false,
                        canEditHeader: false,
                        html : true,
                        name : valeur.name,
                        collection : valeur.collection,
                        actions : [
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le badge', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveBadge'
                            },
                            {
                                'icon': 'fa fa-edit' ,
                                'bstooltip' : {
                                    'text' : 'Editer le badge', 
                                    'position' : 'left'
                                }, 
                                'action': 'onEdit'
                            },
                            {
                                'icon' : 'fa fa-bookmark',
                                'badge' : 'assigner',
                                'action': 'onAssigne'
                            }
                        ]
                    }
                    data<?= $kunik ?>.push(memberCard) 


                    let badgeCard = {
                        id : 'badge'+keys,
                        label : valeur.name,
                        editable : false
                    }
                    headers<?= $kunik ?>.push(badgeCard) 
                }
            );

            var keys<?=$kunik?> = [];
            keys<?=$kunik?> = Object.keys(dataBadges<?= $kunik ?>)
            var dataAdd = []
            var onLoadBadge = true;
            function memberInBagde(id) {
                dataAdd = []
                let dataPass = {}
                var badge<?= $kunik ?> = {     
                    "searchType" : ["citoyens","organizations"],
                    "indexMin" : 0,
                    "initType" : "",
                    "countType" : "",
                    "indexStep" : 0,
                    "notSourceKey" : true,
                    "filters" : {},
                    "fields[]": "badges",
                    "locality" : "",
                    "fediverse" : false
                };
                badge<?= $kunik ?>.filters["badges."+id] = {"$exists" : true}
                
                ajaxPost(
                    null,
                    baseUrl+"/" + moduleId + "/search/globalautocomplete",
                    badge<?= $kunik ?>,
                    function(data){
                        dataPass = data.results;
                    },null,null, {async:false}
                )

                $.map( dataPass , function (val, key) {
                    if(typeof val != 'undefined' && typeof val.badges != 'undefined') {
                        $.map( val.badges , function (v, k) {
                            if(k == id) {
                                let mbBadge = {
                                    _id : val._id.$id,
                                    id : val._id.$id,
                                    title : "<b>"+val.name+"</b>",
                                    header : "badge"+id,
                                    canEditHeader: false,
                                    html : true,
                                    name : val.name,
                                    collection : val.collection,
                                    canMoveCard: false,
                                    actions : [
                                        
                                    ]
                                }
                                if (typeof v.revoke != 'undefined' && v.revoke == "true") {
                                        let rev = {
                                            'icon': 'fa fa-reply-all' ,
                                            'bstooltip' : {
                                                'text' : 'Réinitialiser le badge', 
                                                'position' : 'left'
                                            }, 
                                            'badge' : 'Réinitialiser',
                                            'action': 'onReinitialise'
                                        }
                                        
                                    let act = {
                                        'icon': 'fa fa-window-close' ,
                                            'bstooltip' : {
                                                'text' : "Badge revoquer par l'admin", 
                                                'position' : 'left'
                                            },
                                        'badge' : 'Badge revoquer',
                                        'action': ''
                                    }
                                    mbBadge.actions.push(rev)
                                    mbBadge.actions.push(act)
                                
                                } else {
                                        let rev = {
                                            'icon': 'fa fa-times-circle' ,
                                            'bstooltip' : {
                                                'text' : 'Revoquer le badge', 
                                                'position' : 'left'
                                            }, 
                                            'badge' : 'Revoquer',
                                            'action': 'onRevoke'
                                        }   
                                        mbBadge.actions.push(rev)
                                    
                                    
                                    if (typeof v.attenteRecepteur != 'undefined' && v.attenteRecepteur == true) {
                                        let act = {
                                            'icon': 'fa fa-clock-o' ,
                                                'bstooltip' : {
                                                    'text' : 'En attente de confirmation du recepteur', 
                                                    'position' : 'left'
                                                },
                                            'badge' : 'En attente',
                                            'action': ''
                                        }
                                        mbBadge.actions.push(act)
                                    }
                                }
                                dataAdd.push(mbBadge) 
                                data<?= $kunik ?>.push(mbBadge) 
                            }
                        })
                    }
                })
            }

            function filterBadge() {
                onLoadBadge = false;
                let member = {
                    columns : ['memberof','listBadges'],
                    attributes : {
                        
                    }
                }

                if($('#nameBadge<?=$kunik?>').val() != '') {
                    member.card = $('#nameBadge<?=$kunik?>').val()
                } 
                $('#kanban<?= $kunik ?>').kanban('filter', member);
            }

            $('#nameBadge<?=$kunik?>').on('input', filterBadge)
            
        var badgesDom<?= $kunik ?> = $('#kanban<?= $kunik ?>').kanban({
            headers : headers<?= $kunik ?>,
            data : data<?= $kunik ?>,
            editable: false,
            language : mainLanguage,
            canAddCard: false,
            editable : true,
            canAddColumn: false,
            canEditCard: false,
            canEditHeader: false,
            canMoveCard: true,
            canMoveColumn: true,
            readonlyHeaders: ['memberof','listBadges'],
            copyWhenDragFrom: ['memberof'],
            endpoint : `${baseUrl}/plugins/kanban/`,
            defaultColumnMenus: [''],
            onRenderDone(){
                if (onLoadBadge == true) {
                    $.map( dataBadges<?= $kunik ?> , function( valeur, keys ) { 
                        $('#kanban-wrapper-badge'+keys).addClass('hidden')
                    })
                }
                $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                    'display' : 'none'
                })

            },
            onInvite(data){
                smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/'+'element/invite/type/'+typeCostum+'/id/'+idCostum);
            },
            onCardDrop(dropped, updates) { 
                onLoadBadge = false
                let onCancel = true
                let target = dropped.target;
                let badgeId = target.substr(5,target.length)
                let defaultBadge = {}
                defaultBadge[dropped.data.instanceIdentity] = {
                    'type' : dropped.data.collection,
                    'name' : dropped.data.name,
                    'profilThumbImageUrl' : dropped.data.profilThumbImageUrl
                }
                dyFObj.openForm("assignbadge", null, {badgeId : badgeId, award:defaultBadge}, null, {
                    afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                        dyFObj.closeForm()
                        onCancel = false
                    })
                });
                var copy = $.extend({}, dropped.data);
                copy.canMoveCard = false;
                copy.actions = [
                    {
                        'icon': 'fa fa-times-circle' ,
                        'bstooltip' : {
                            'text' : 'Revoquer le badge', 
                            'position' : 'left'
                        }, 
                        'action': 'onRevoke',
                        'badge' : 'Revoquer'
                    },
                    {
                        'icon': 'fa fa-clock-o' ,
                            'bstooltip' : {
                                'text' : 'En attente de confirmation du recepteur', 
                                'position' : 'left'
                            },
                        'badge' : 'En attente',
                        'action': ''
                    }
                ]
                badgesDom<?= $kunik ?>.kanban('setData', { column: copy.header, id: copy.id, data: copy});
            },
            onRevoke(data,dom) {
                let target = data.header;
                let badgeId = target.substr(5,target.length)
                let awardId = data.instanceIdentity
                let awardType = data.collection

                let dataRevoke = {
                    badgeId : badgeId,
                    awardId : awardId,
                    awardType : awardType
                }
                dyFObj.openForm("revokebadge",null, dataRevoke,null, {
                    afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                        dyFObj.closeForm()
                    })
                });
            },
            onReinitialise(data, dom) {
                let memberCard = {
                    _id : data.instanceIdentity,
                    id : data.instanceIdentity,
                    title : "<b>"+data.name+"</b>",
                    header : data.header,
                    canEditHeader: false,
                    canMoveCard : false,
                    html : true,
                    name : data.name,
                    collection : data.collection,
                    profilThumbImageUrl : data.profilThumbImageUrl,
                    actions : [
                        {
                            'icon': 'fa fa-times-circle' ,
                            'bstooltip' : {
                                'text' : 'Revoquer le badge', 
                                'position' : 'left'
                            }, 
                            'action': 'onRevoke',
                            'badge' : 'Revoquer'
                        },
                        {
                            'icon': 'fa fa-clock-o' ,
                                'bstooltip' : {
                                    'text' : 'En attente de confirmation du recepteur', 
                                    'position' : 'left'
                                },
                            'badge' : 'En attente',
                            'action': ''
                        }
                    ]
                }
                let index<?= $kunik ?> = badgesDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                badgesDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.instanceIdentity,  index: index<?= $kunik ?> });
                let target = data.header;
                let badgeId = target.substr(5,target.length)
                let awardId = data.instanceIdentity
                let awardType = data.collection
                let url = baseUrl + '/co2/badges/cancel-revoke/badge/' + badgeId + '/award/' + awardId + '/type/' + awardType;
                $.ajax({
                    type:"GET",
                    url: url,
                    success:function(data) {
                        badgesDom<?= $kunik ?>.kanban('addData', memberCard);
                        if(data.result){
                            toastr.success("Badges reinitialisé");
                        }else{
                            toastr.error("Erreur lors de la reinitialisation du badges");
                        }
                    },
                    error:function (xhr, ajaxOptions, thrownError){
                        toastr.error("Erreur lors de l'assignation des badges");
                    }
                });
            },
            onCardClick(data) {
                onLoadBadge = false
                if($('#kanban-wrapper-badge'+data.id).hasClass('hidden')) {
                    if($('#kanban-wrapper-badge'+data.id).hasClass('load')) {
                        $('#kanban-wrapper-badge'+data.id).removeClass('hidden');
                    } else {
                        memberInBagde(data.id)
                        badgesDom<?= $kunik ?>.kanban('addData', dataAdd);
                        $('#kanban-wrapper-badge'+data.id).addClass('load');
                    }

                    $('#kanban-wrapper-badge'+data.id).removeClass('hidden');
                    $('#kanban-wrapper-badge'+data.id).addClass('visible');

                    $('.active-card').css({
                        "background-color" : "gray",
                        "color" : "#172b4d"
                    });
                    $('.active-card .kanban-footer-card').css({
                        "color" : "#172b4d"
                    });
                    $('.active-card').addClass('activer')

                } else if ($('#kanban-wrapper-badge'+data.id).hasClass('visible')){
                    $('#kanban-wrapper-badge'+data.id).addClass('hidden');
                    $('#kanban-wrapper-badge'+data.id).removeClass('visible');
                    $('.active-card').css({
                        "background-color" : "#fff", 
                        "color" : "#172b4d"
                    });
                    $('.active-card .kanban-footer-card').css({
                        "color" : "#172b4d"
                    });
                    $('.active-card').removeClass('activer')
                }
            },
            onAssigne(data) {
                var badgeId = data.id
                let memberCard = {
                    _id : data.instanceIdentity,
                    id : data.instanceIdentity,
                    title : "<b>"+data.name+"</b>",
                    header : data.header,
                    canEditHeader: false,
                    canMoveCard : false,
                    html : true,
                    name : data.name,
                    collection : data.collection,
                    profilThumbImageUrl : data.profilThumbImageUrl,
                    actions : [
                        {
                            'icon': 'fa fa-times-circle' ,
                            'bstooltip' : {
                                'text' : 'Revoquer le badge', 
                                'position' : 'left'
                            }, 
                            'action': 'onRevoke',
                            'badge' : 'Revoquer'
                        },
                        {
                            'icon': 'fa fa-clock-o' ,
                                'bstooltip' : {
                                    'text' : 'En attente de confirmation du recepteur', 
                                    'position' : 'left'
                                },
                            'badge' : 'En attente',
                            'action': ''
                        }
                    ]
                }
                dyFObj.openForm("assignbadge",null, {badgeId : badgeId},null, {
                    afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                        dyFObj.closeForm()
                        badgesDom<?= $kunik ?>.kanban('addData', memberCard);
                    })
                });
            },
            onEdit(data) {
                var badgeId = data.id
                dyFObj.editElement('badges', badgeId);
            },
            onRemoveMember(data, dom){
                var memb<?= $kunik ?> = {
                    childId : data.instanceIdentity,
                    childType : data.collection,
                    parentType : typeCostum,
                    parentId : idCostum,
                    connectType : typeCostum == 'organizations' ? 'members' : 'contributors'
                }
                bootbox.confirm(trad.areyousuretodelete, function(result) {
                    if (!result) {
                        return;
                    } else {
                        ajaxPost(
                            null,
                            baseUrl+"/" + moduleId + "/link/disconnect",
                            memb<?= $kunik ?>,
                            function(donne){
                                let index<?= $kunik ?> = badgesDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                                badgesDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.id,  index: index<?= $kunik ?> });
                                urlCtrl.loadByHash(location.hash)
                            },null,null, {async:false}
                        )
                    }
                })
            },
            onAddBadge(data, dom) {
                dyFObj.openForm('badge',null, {
                    afterSave: (data) => dyFObj.commonAfterSave(data, () => {
                        dyFObj.closeForm()
                    })
                });
            },
            onRemoveBadge(data, dom) {
                onLoadBadge = false
                let id = $('.active-card').data('id')
                var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/badges/id/"+data.instanceIdentity;
                
                bootbox.confirm(trad.areyousuretodelete, function(result) {
                    if (!result) {
                        return;
                    } else { 
                        let index<?= $kunik ?> = badgesDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                        badgesDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.id,  index: index<?= $kunik ?> });
                        $('#kanban-wrapper-badge'+id).addClass('hidden')
                        $('#kanban-wrapper-badge'+id).removeClass('visible')
                        ajaxPost(
                            null,
                            urlToSend,
                            {},
                            function(data){ 
                                toastr.success("élément effacé");
                            }
                        );
                    }
                })
                
            }
        });
        
    });
</script>
