
<?php 
    $myCmsId  = $blockKey;
    if(isset($costum)){
        $myCmsId  = $blockCms["_id"]->{'$id'};
    }
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    if(!isset($costum)){
        $costum = CacheHelper::getCostum();
    }

    HtmlHelper::registerCssAndScriptsFiles(
		[
			"/js/aap/top_contributor_filter.js",
			'/css/aap/detail.css',
            '/css/aap/aap.css'
		],
		Yii::app()->getModule('co2')->assetsUrl
	);

    $graphAssets = [
        '/js/hexagon.js',
        '/plugins/d3/d3.v6.min.js',
        '/js/graph.js',
        '/js/venn.js',
        '/css/graph.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
    );
?>



<style id="css-<?= $kunik ?>">
    .column-header-text {
        color: #000000 !important;
    }
    .btn-is-admin {
        text-decoration: line-through;
    }
    .kanban-<?= $kunik ?> {
        height : 80vh !important;
    }
    .kanban-list-card-action {
        right : 7px !important;
    } 
    .kanban-content {
        display: flex;
    }
    .projet-content {
        display: flex;
        align-items: end;
    }
    .projet-content .button-projet{
        margin-right : 20px;
    }
    .filtre {
        display : flex;
        padding : 0 20px;
    }
    .filtre .filtre-type {
        margin-right : 20px;
    }
    
    .bar-content {
        display: flex;
        justify-content : space-between;
        padding: 0px 20px;
        background-color: #eee;
        border : 1px solid #eee;
        border-radius : 5px;
        margin : 5px 20px 0px;
        min-height: 50px;
    }
    .bar-avatar {
        margin-right : 5px;
    }
    .bar-avatar .tab-contributor.expander {
        background-color : #9BC125 !important;
    }
    .bar-avatar .contributor-project-filter{
       width : 200px !important;
    }
    
    .active-role {
        background-color : gray !important;
        color : #172b4d !important;
    }
    .active-mile {
        background-color : gray !important;
        color : #172b4d !important;
    }
    .active-member {
        background-color : rgb(183, 183, 183) !important;
    }
    .color-red {
        color : #a94442 !important;
    }
    .action-menu {
        text-align: center;
        list-style: none;
        padding: 0;
    }
    .action-menu > li > a {
        display: block;
        padding: 5px 10px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        color: #333 !important;
        white-space: nowrap;
        font-size: 16px !important;
    }
    .action-menu > li > a:focus, .action-menu > li > a:hover {
        color: #262626;
        text-decoration: none;
        background-color: #f5f5f5;
    }
    #ajax-modal {
        z-index: 9999999999999999 !important;
    }
    .kanban-overlay {
        z-index: 9999999999999999 !important;
    }
    ul.breadcrumb {
        padding: 5px 16px;
        list-style: none;
        background-color: #eee;
        margin : 7px 0 0 0; 
    }
    ul.breadcrumb li {
        display: inline;
        font-size: 12px;
    }
    ul.breadcrumb li+li:before {
        padding: 8px;
        color: black;
        content: none !important;
    }
    @media screen and (max-width: 576px) {
        .bar-breadcrumb {
            display : none;
        }
    }

    .active-bread {
        color: #0275d8;
        text-decoration: none;
    }
    .active-bread:hover {
        color: #01447e;
        text-decoration: underline;
    }
    .hide-bread {
        color: black;
        text-decoration: none;
        cursor: default;
    }
    .hide-bread:hover {
        color: black;
        text-decoration: none;
    }
    .separateur-bread {
        color : black !important;
        margin: 0 5px !important;
    }
    .loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 40%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
        
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> .processingLoader {
        width : auto !important;
        height : auto !important;
    }
    .backdrop<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }
    .kanban-list-wrapper .kanban-list-header .column-header-text {
        color: #000 !important;
    }
    #hexa-container-<?= $kunik ?>{
        display: flex;
        justify-content: space-between;
        align-items: stretch;
    }

    #hexa-container-<?= $kunik ?> #filter-hexagone-container{
		display: flex;
		flex-direction: column;
		width: 300px;
		background-color: white;
        height: 700px;
        overflow-y: auto;
    }

    #hexa-container-<?= $kunik ?> #filter-hexagone-container .btn-hexagone-zoomer[data-type="group"]{
        width: 90%;
    }

    #hexa-container-<?= $kunik ?> #filter-hexagone-container .btn-expand[data-toggle="collapse"]{
        width: 10%;
    }

    #hexa-container-<?= $kunik ?> .hexagone-child-container{
        padding-left: 15%;
    }

    #hexa-container-<?= $kunik ?> .btn-hexagone-zoomer[data-type="children"]{
        width: 95%;
    }

    .bar-utility{
        align-content: center;
    }

    .bar-view{
        display: flex;
    }

    .btn-view{
        padding-left: 5px;
    }

    .btn-view-change.active{
        background-color: #68ca91 !important;
    }

</style>
<div class="<?= $kunik ?> <?= $kunik ?>-css">
    <div class="kanban-content">
        <div class="filtre">
            <div class="filtre-name">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"> </i></span>
                    <input id="nameMember<?=$kunik?>" type="text" class="form-control" name="nameMember<?=$kunik?>" placeholder="Que recherchez-vous?">
                </div>
            </div>
        </div>
    </div>
    <div class="bar-content">
        <div class="bar-breadcrumb">
            <ul class="breadcrumb">
                <li><a href="javascript:;" class="" id="roleBread">Roles </a> </li>
            </ul>
        </div>
        <div class="bar-filtre">
            <div class="bar-avatar">
                <div class="contributor-project-filter dropdown" id="contributor-filter-container">
                </div>   
            </div>
        </div>
        <div class="bar-utility">
            <div class="bar-view">
                <div class="btn-view" id="contributor-filter-container">
                    <button type="button" class="switchViewEdition btn-view-change btn btn-primary hidden-xs active" style="" title="afficher sur un graph" alt="afficher sur un graph">
                        <svg style="height: 12px; width: 15px;fill:white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" xml:space="preserve">
                        <path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"></path>
                        <path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"></path>
                        <path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"></path>
                        </svg> Edit
                    </button>
                </div> 
                <div class="btn-view" id="contributor-filter-container">
                    <button type="button" class="switchViewGraph btn btn-view-change btn-primary hidden-xs" style="" title="afficher sur un graph" alt="afficher sur un graph">
                        <svg style="height: 12px; width: 15px;fill:white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="svg-graph-icon" x="0px" y="0px" viewBox="0 0 349.899 349.898" xml:space="preserve">
                        <path d="M175.522,12.235c-42.6,0-77.256,34.649-77.256,77.25c0,42.6,34.656,77.255,77.256,77.255    c42.591,0,77.257-34.656,77.257-77.255C252.779,46.895,218.113,12.235,175.522,12.235z"></path>
                        <path d="M77.255,337.663c42.599,0,77.255-34.641,77.255-77.251c0-42.594-34.656-77.25-77.255-77.25    C34.653,183.162,0,217.818,0,260.412C0,303.012,34.653,337.663,77.255,337.663z"></path>
                        <path d="M272.648,183.151c-42.603,0-77.256,34.65-77.256,77.256c0,42.604,34.653,77.25,77.256,77.25    c42.6,0,77.251-34.646,77.251-77.25C349.909,217.818,315.248,183.151,272.648,183.151z"></path>
                        </svg> Graph
                    </button>
                </div>   
                <div class="btn-view" id="contributor-filter-container">
                    <button type="button" class="ssmla switchViewHexa btn btn-view-change btn-primary hidden-xs" data-view="cohexagone" style="margin-right: 5px; display: inline-flex;align-items: center;" title="Graph de Communauté" alt="Graph de Communauté">
							<svg xmlns="http://www.w3.org/2000/svg" style="width: 20px;fill:white" viewBox="0 0 256 256"><path d="M232,80.18v95.64a16,16,0,0,1-8.32,14l-88,48.17a15.88,15.88,0,0,1-15.36,0l-88-48.17a16,16,0,0,1-8.32-14V80.18a16,16,0,0,1,8.32-14l88-48.17a15.88,15.88,0,0,1,15.36,0l88,48.17A16,16,0,0,1,232,80.18Z"></path></svg> Hexagone
					</button>
                </div> 
            </div>
        </div>
    </div>
    <div data-kunik="<?= $kunik ?>" class="kanban-<?= $kunik ?>" data-name="kanban" data-id="<?= $myCmsId ?>" style="margin-bottom:70px !important;">
        <div id="kanban<?= $kunik ?>">
        </div>
        <div id="circle-container-<?= $kunik ?>" style="display: none;">
        </div>
        <div id="hexa-container-<?= $kunik ?>" style="display: none;">
            <canvas id="hexagoneCommunity" height="700px"></canvas>
            <div id="filter-hexagone-container">
            </div>
        </div>
    </div>

    <div class="loader<?= $kunik ?> hidden" id="loader<?= $kunik ?>" role="dialog">
        <div class="content<?= $kunik ?>" id="content<?= $kunik ?>"></div>
    </div>
    <div class="backdrop<?= $kunik ?> hidden" id="backdrop<?= $kunik ?>"></div>
</div>

<script type="text/javascript">
    
    if(typeof costum != 'undefined' && costum !== null){
        var sectionDyf = sectionDyf || {};
        sectionDyf["<?php echo $kunik ?>BlockCms"] = <?php echo json_encode( $blockCms ); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum != null && costum.editMode == true) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf["<?php echo $kunik ?>blockCms"];
            var kanbanRoles = {
                configTabs: {
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks["kanban<?= $myCmsId ?>"] = kanbanRoles;
        }
    }
</script>
<script type="text/javascript">
    
    $(function () {
        var newCol = {
            id : '',
            label : '' 
        }
        var memberPerRole<?= $kunik ?> = [];
        var params<?= $kunik ?> = {     
            searchType : ["citoyens","organizations"],
            notSourceKey : true,
            fields : ["links"],
            filters : {},
            indexStep : 0        
        };
        var member<?= $kunik ?> = [];
        var initMember<?= $kunik ?> = [];
        var allProjet<?= $kunik ?> = {};
        var my<?= $kunik ?> = ''
        var activeProjet<?= $kunik ?> = ''
        var activeMilestone<?= $kunik ?> = ''
        var typeCostum = "";
        var activeCircle<?= $kunik ?> = "";
        var circle<?= $kunik ?> = null;
        var idCostum = "";
        var slugCostum = "";
        coInterface.showLoader('#content<?= $kunik ?>')
        if(contextData != null && typeof contextData != 'undefined'){
            idCostum = contextData.id;
            typeCostum = contextData.type;
            slugCostum = contextData.slug;
        } else if(typeof costum != 'undefined' && costum !== null){
            if(typeof costum.contextType !='undefined'){
                idCostum = costum.contextId;
                typeCostum = costum.contextType;
                slugCostum = costum.contextSlug;
            }
        }

        

        if(typeCostum === "organizations"){
            params<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
        }
        else if(typeCostum === "projects"){
            params<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
        }
        else {
            params<?= $kunik ?>.filters["links.events."+idCostum] = {'$exists':true};
        }

        function filterKanban() {
            load<?=$kunik?> = false;
            let objet = {
                columns : ['memberof', 'listRole'],
                attributes : {
                    
                }
            }
            // if($('#typeMember<?=$kunik?>').val() != '') {
            //     objet.attributes.collection = $('#typeMember<?=$kunik?>').val()
            // } 
            if($('#nameMember<?=$kunik?>').val() != '') {
                objet.card = $('#nameMember<?=$kunik?>').val()
            } 
            $('#kanban<?= $kunik ?>').kanban('filter', objet);
        }

        // $('#typeMember<?=$kunik?>').on('change', filterKanban)
        $('#nameMember<?=$kunik?>').on('input', filterKanban)

        loadKanban()
        function loadKanban(){
            var defImg = modules.co2.url + '/images/thumbnail-default.jpg';
            load<?=$kunik?> = true;
            let titleHead = 'Gestion des roles de : '+slugCostum;
            $('#preview-kanban .titre-role').text(titleHead);
            dataMember<?= $kunik ?> = {};
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    dataMember<?= $kunik ?> = data.results;
                },null,null, {async:false}
            )

            var headers<?= $kunik ?> = [
                { id: 'memberof', label: "<i class='fa fa-user'> </i> Les membres" , editable : false,
                    menus: [
                        { label: 'Envoyer une invitation', action: 'onInvite' }
                    ]
                },
                { id: 'listRole', label: "<i class='fa fa-user-circle'> </i> Les rôles" , editable : false ,
                    menus: [
                        { label: 'Afficher/Cacher tous les roles', action: 'onAffiche' }
                    ]
                }
            ]

            var data<?= $kunik ?> = []
            var uniqueRole<?= $kunik ?> = []

            if(typeCostum === "organizations"){
                const dataRole = <?= json_encode($roles ?? []) ?>;
                $.map( 
                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                        let memberCard = {
                            _id : keys,
                            id : keys,
                            title : `<div><img src='${typeof valeur.profilImageUrl != "undefined" ? valeur.profilImageUrl : defImg}' style='width : 30px; height : 30px; border-radius: 50%' /> <b>${valeur.name}</b></div>`,
                            header : "memberof",
                            canEditHeader: false,
                            html : true,
                            name : valeur.name,
                            collection : valeur.collection,
                            slug : valeur.slug,
                            actions : [
                                {
                                    'icon': 'fa fa-trash' ,
                                    'bstooltip' : {
                                        'text' : 'Supprimer le membre de la communauter', 
                                        'position' : 'left'
                                    }, 
                                    'action': 'onRemoveMember'
                                }
                            ]
                        }
                        if(exists(valeur.links) && exists(valeur.links.memberOf)){
                            $.map(valeur.links.memberOf , function( val, key ) {
                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                    dataRole.push(...val.roles)
                                }
                                if(valeur.collection == 'citoyens') {
                                    if(typeof val.isAdmin != 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '' , 
                                            'bstooltip' : { 
                                                'text' : 'Enlever le droit admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="removeAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>' , 
                                            'action': 'onRemoveAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    } else if(typeof val.isAdmin == 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '', 
                                            'bstooltip' : { 
                                                'text' : 'Ajouter comme admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="btn-is-admin addAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>', 
                                            'action': 'onAddAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    }
                                } else if (valeur.collection == 'organizations' && key == idCostum) {
                                    let iconBadge = {
                                        'icon': 'fa fa-group' , 
                                        'bstooltip' : { 
                                            'text' : 'Une organisation',
                                            'position' : 'left'
                                        }, 
                                        'badge' : 'Orga' , 
                                        'action': '' 
                                    }
                                    memberCard.actions.push(iconBadge)
                                }
                            })
                        }
                        data<?= $kunik ?>.push(memberCard) 
                        member<?= $kunik ?>.push(memberCard)
                        initMember<?= $kunik ?>.push(memberCard)
                    }
                );
                uniqueRole<?= $kunik ?> = [...new Set(dataRole)]
                

                for(let i = 0; i < uniqueRole<?= $kunik ?>.length ; i++) {
                    
                    let nbR = 0;
                    let hd = {
                        id : "<?= $kunik ?>"+i,
                        label : uniqueRole<?= $kunik ?>[i],
                        editable : false,
                        closable : true
                    }
                    headers<?= $kunik ?>.push(hd)
                    $.map( 
                        dataMember<?= $kunik ?> , function( valeur, keys ) {
                            if(typeof valeur.links != 'undefined' && typeof valeur.links.memberOf != 'undefined'){
                                $.map(valeur.links.memberOf , function( val, key ) {
                                    if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)) {
                                        if(val.roles.indexOf(uniqueRole<?= $kunik ?>[i]) > -1) {
                                            let mb = {
                                                _id : keys,
                                                id : keys+"<?= $kunik ?>"+i,
                                                instanceIdentity : keys,
                                                title : '<b>'+valeur.name+'</b>',
                                                name : valeur.name,
                                                collection : valeur.collection,
                                                header : "<?= $kunik ?>"+i,
                                                html : true,
                                                canMoveCard: false,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-trash', 
                                                        'bstooltip' : { 
                                                            'text' : 'Enlever cette role du membre',
                                                            'position' : 'left'
                                                        }, 
                                                        'action': 'onDelete'
                                                    }
                                                ] 
                                            }
                                            data<?= $kunik ?>.push(mb)
                                            nbR += 1;
                                        }                                    
                                    }
                                })
                            }
                        }
                    );
                    memberPerRole<?= $kunik ?>.push(nbR)
                    var listRole = {
                        _id : i,
                        id : "<?= $kunik ?>"+i,
                        instanceIdentity : i,
                        title : "<i>"+uniqueRole<?= $kunik ?>[i]+"</i>",
                        name : uniqueRole<?= $kunik ?>[i],
                        header : "listRole",
                        canEditHeader: false,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {icon: 'fa fa-trash', action: 'onRemoveRole'},
                            {icon: '',badge: '<span class="nbpers">'+memberPerRole<?= $kunik ?>[i]+' <i class="fa fa-user"> </i></span>', action: '' }
                        ]
                    }
                    data<?= $kunik ?>.splice(1, 0, listRole)
                }   
            }

            if(typeCostum === "events"){
                const dataRole = <?= json_encode($roles ?? []) ?>;
                $.map( 
                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                        let memberCard = {
                            _id : keys,
                            id : keys,
                            title : `<div><img src='${typeof valeur.profilImageUrl != "undefined" ? valeur.profilImageUrl : defImg}' style='width : 30px; height : 30px; border-radius: 50%' /> <b>${valeur.name}</b></div>`,
                            header : "memberof",
                            canEditHeader: false,
                            html : true,
                            name : valeur.name,
                            collection : valeur.collection,
                            slug : valeur.slug,
                            actions : [
                                {
                                    'icon': 'fa fa-trash' ,
                                    'bstooltip' : {
                                        'text' : 'Supprimer le membre de la communauter', 
                                        'position' : 'left'
                                    }, 
                                    'action': 'onRemoveMember'
                                }
                            ]
                        }
                        if(typeof valeur.links != 'undefined' && typeof valeur.links.events != 'undefined'){
                            $.map(valeur.links.events , function( val, key ) {
                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                    dataRole.push(...val.roles)
                                }
                                if(valeur.collection == 'citoyens') {
                                    if(typeof val.isAdmin != 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '' , 
                                            'bstooltip' : { 
                                                'text' : 'Enlever le droit admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="removeAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>' , 
                                            'action': 'onRemoveAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    } else if(typeof val.isAdmin == 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '', 
                                            'bstooltip' : { 
                                                'text' : 'Ajouter comme admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="btn-is-admin addAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>', 
                                            'action': 'onAddAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    }
                                } else if (valeur.collection == 'organizations' && key == idCostum) {
                                    let iconBadge = {
                                        'icon': 'fa fa-group' , 
                                        'bstooltip' : { 
                                            'text' : 'Une organisation',
                                            'position' : 'left'
                                        }, 
                                        'badge' : 'Orga' , 
                                        'action': '' 
                                    }
                                    memberCard.actions.push(iconBadge)
                                }
                            })
                        }
                        data<?= $kunik ?>.push(memberCard) 
                        member<?= $kunik ?>.push(memberCard)
                        initMember<?= $kunik ?>.push(memberCard)
                    }
                );
                uniqueRole<?= $kunik ?> = [...new Set(dataRole)]
                

                for(let i = 0; i < uniqueRole<?= $kunik ?>.length ; i++) {
                    
                    let nbR = 0;
                    let hd = {
                        id : "<?= $kunik ?>"+i,
                        label : uniqueRole<?= $kunik ?>[i],
                        editable : false,
                        closable : true
                    }
                    headers<?= $kunik ?>.push(hd)
                    $.map( 
                        dataMember<?= $kunik ?> , function( valeur, keys ) {
                            if(typeof valeur.links != 'undefined' && typeof valeur.links.events != 'undefined'){
                                $.map(valeur.links.events , function( val, key ) {
                                    if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)) {
                                        if(val.roles.indexOf(uniqueRole<?= $kunik ?>[i]) > -1) {
                                            let mb = {
                                                _id : keys,
                                                id : keys+"<?= $kunik ?>"+i,
                                                instanceIdentity : keys,
                                                title : '<b>'+valeur.name+'</b>',
                                                name : valeur.name,
                                                collection : valeur.collection,
                                                header : "<?= $kunik ?>"+i,
                                                html : true,
                                                canMoveCard: false,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-trash', 
                                                        'bstooltip' : { 
                                                            'text' : 'Enlever cette role du membre',
                                                            'position' : 'left'
                                                        }, 
                                                        'action': 'onDelete'
                                                    }
                                                ] 
                                            }
                                            data<?= $kunik ?>.push(mb)
                                            nbR += 1;
                                        }                                    
                                    }
                                })
                            }
                        }
                    );
                    memberPerRole<?= $kunik ?>.push(nbR)
                    var listRole = {
                        _id : i,
                        id : "<?= $kunik ?>"+i,
                        instanceIdentity : i,
                        title : "<i>"+uniqueRole<?= $kunik ?>[i]+"</i>",
                        name : uniqueRole<?= $kunik ?>[i],
                        header : "listRole",
                        canEditHeader: false,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {icon: 'fa fa-trash', action: 'onRemoveRole'},
                            {icon: '',badge: '<span class="nbpers">'+memberPerRole<?= $kunik ?>[i]+' <i class="fa fa-user"> </i></span>', action: '' }
                        ]
                    }
                    data<?= $kunik ?>.splice(1, 0, listRole)
                }   
            }

            if(typeCostum === "projects"){
                const dataRole = <?= json_encode($roles ?? []) ?>;
                $.map(  
                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                        let memberCard = {
                            _id : keys,
                            id : keys,
                            title : `<div><img src='${typeof valeur.profilImageUrl != "undefined" ? valeur.profilImageUrl : defImg}' style='width : 30px; height : 30px; border-radius: 50%' /> <b>${valeur.name}</b></div>`,
                            header : "memberof",
                            name : valeur.name,
                            slug : valeur.slug,
                            collection : valeur.collection,
                            html : true,
                            actions : [
                                {
                                    'icon': 'fa fa-trash' , 
                                    'bstooltip' : {
                                        'text' : 'Supprimer le membre de la communauter' , 
                                        'position' : 'left'
                                    }, 
                                    'action': 'onRemoveMember'
                                }
                            ]
                        }
                        if(exists(valeur.links) && exists(valeur.links.projects)){
                            $.map(valeur.links.projects , function( val, key ) {
                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                    dataRole.push(...val.roles)
                                }
                                if(valeur.collection == 'citoyens') {
                                    if(typeof val.isAdmin != 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '' , 
                                            'bstooltip' : { 
                                                'text' : 'Enlever le droit admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="removeAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>' , 
                                            'action': 'onRemoveAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    } else if(typeof val.isAdmin == 'undefined' && key == idCostum){
                                        let iconBadge = {
                                            'icon': '', 
                                            'bstooltip' : { 
                                                'text' : 'Ajouter comme admin',
                                                'position' : 'left'
                                            }, 
                                            'badge' : '<span class="btn-is-admin addAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>', 
                                            'action': 'onAddAdmin' 
                                        }
                                        memberCard.actions.push(iconBadge)
                                    }
                                } else if (valeur.collection == 'organizations' && key == idCostum) {
                                    let iconBadge = {
                                        'icon': 'fa fa-group' , 
                                        'bstooltip' : { 
                                            'text' : 'Une organisation',
                                            'position' : 'left'
                                        }, 
                                        'badge' : 'Orga' , 
                                        'action': '' 
                                    }
                                    memberCard.actions.push(iconBadge)
                                }
                            })
                        }
                        data<?= $kunik ?>.push(memberCard) 
                        member<?= $kunik ?>.push(memberCard)
                        initMember<?= $kunik ?>.push(memberCard)
                    }
                );
                uniqueRole<?= $kunik ?> = [...new Set(dataRole)]

                for(let i = 0; i < uniqueRole<?= $kunik ?>.length ; i++) {
                    let nbR = 0;
                    let hd = {
                        id : "<?= $kunik ?>"+i,
                        label : uniqueRole<?= $kunik ?>[i],
                        editable : false,
                        closable : true
                    }
                    headers<?= $kunik ?>.push(hd)
                    $.map( 
                        dataMember<?= $kunik ?> , function( valeur, keys ) {
                            if(typeof valeur.links != 'undefined' && typeof valeur.links.projects != 'undefined'){
                                $.map(valeur.links.projects , function( val, key ) {
                                    if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                        if(val.roles.indexOf(uniqueRole<?= $kunik ?>[i]) > -1) {
                                            let mb = {
                                                _id : keys,
                                                id : keys+"<?= $kunik ?>"+i,
                                                instanceIdentity : keys,
                                                title : '<b>'+valeur.name+'</b>',
                                                name : valeur.name,
                                                header : "<?= $kunik ?>"+i,
                                                collection : valeur.collection,
                                                html : true,
                                                canMoveCard: false,
                                                actions : [
                                                    {
                                                        'icon': 'fa fa-trash', 
                                                        'bstooltip' : { 
                                                            'text' : 'Enlever cette role du membre',
                                                            'position' : 'left'
                                                        }, 
                                                        'action': 'onDelete'
                                                    }
                                                ]
                                            }
                                            data<?= $kunik ?>.push(mb)
                                            nbR += 1;
                                        }
                                    }
                                })
                            }
                        }
                    );
                    memberPerRole<?= $kunik ?>.push(nbR)
                    var listRole = {
                        _id : i,
                        id : "<?= $kunik ?>"+i,
                        instanceIdentity : i,
                        title : "<i>"+uniqueRole<?= $kunik ?>[i]+"</i>",
                        name : uniqueRole<?= $kunik ?>[i],
                        header : "listRole",
                        canEditHeader: false,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {icon: 'fa fa-trash', action: 'onRemoveRole'},
                            {icon: '',badge: '<span class="nbpers">'+memberPerRole<?= $kunik ?>[i]+' <i class="fa fa-user"> </i></span>', action: '' }
                        ]
                    }
                    data<?= $kunik ?>.splice(1, 0, listRole)
                }
            }


            kanbanDom<?= $kunik ?> = $('#kanban<?= $kunik ?>').kanban({
                headers : headers<?= $kunik ?>,
                data : data<?= $kunik ?>,
                editable: false,
                language : mainLanguage,
                canAddCard: false,
                editable : true,
                canAddColumn: true,
                canEditCard: false,
                canEditHeader: true,
                canMoveCard: true,
                canMoveColumn: true,
                readonlyHeaders: ['memberof','listRole'],
                copyWhenDragFrom: ['memberof'],
                endpoint : `${baseUrl}/plugins/kanban/`,
                defaultColumnMenus: [''],
                showContributors : false,
                onRenderDone(){
                    $('#backdrop<?= $kunik ?>').addClass('hidden')
                    $('#loader<?= $kunik ?>').addClass('hidden')
                    $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                        'display' : 'none'
                    })
                    $('#roleBread').addClass('hide-bread')
                    $('#roleBread').removeClass('active-bread')
                    $('#projetBread').addClass('hidden')
                    $('#actionBread').addClass('hidden')
                    if(load<?=$kunik?> == true) {
                        $('#kanban-wrapper-listRole .kanban-list-header').css({
                            'cursor' : 'pointer'
                        })
                        let htm = `<i class="fa fa-plus-circle"> </i> Ajouter un nouveau rôle`
                        $('#kanban-wrapper-null .kanban-new-column').html(htm)
                        for(let i = 0; i < uniqueRole<?= $kunik ?>.length ; i++) {
                            $('#kanban-wrapper-<?= $kunik ?>'+i).addClass('hidden')
                            $('#kanban-wrapper-<?= $kunik ?>'+i).addClass('roles-list')
                            $('#kanban-wrapper-<?= $kunik ?>'+i+' .kanban-action-dropdown').css({
                                'display' : 'none'
                            })
                            $('.kanban-list-card-detail').css("background-color", "#fff !important");
                        }
                        $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                            'display' : 'none'
                        })
                        $('#kanban-wrapper-memberof .kanban-list-content').css({
                            'background-color' : 'gray'
                        })
                        $('#kanban-wrapper-memberof .column-header-text').css({
                            'color' : '#fff'
                        })
                        $('#kanban-wrapper-listRole .kanban-list-content').css({
                            'background-color' : 'rgb(183, 183, 183)'
                        })
                    }
                },
                onAffiche(data) {
                    if($('.roles-list').hasClass('hidden')){
                        $('.roles-list').removeClass('hidden')
                        $('#kanban-wrapper-listRole .kanban-list-card-detail').addClass('active-role')
                    } else {
                        $('.roles-list').addClass('hidden')
                        $('#kanban-wrapper-listRole .kanban-list-card-detail').removeClass('active-role')
                    }
                    $('#kanban-wrapper-memberof .kanban-list-card-detail').attr('class', 'kanban-list-card-detail')
                },
                onColumnInsert(header) {
                    mylog.log(header, 'header')
                },
                onRemoveAdmin(data){
                    links.updateadminlink(
                        typeCostum,
                        idCostum,
                        data.instanceIdentity,
                        'citoyens',
                        (typeCostum == 'organizations' ? 'members' : (typeCostum == 'projects' ? 'contributors' : 'attendees')),
                        false,
                        data.name,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .removeAdmin').addClass('btn-is-admin')
                        }
                    )
                   
                },
                onAddAdmin(data){
                    links.connect(
                        typeCostum, 
                        idCostum, 
                        data.instanceIdentity, 
                        'citoyens', 
                        'admin', 
                        '', 
                        true,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .addAdmin').removeClass('btn-is-admin')
                        }
                    )
                },
                onInvite(data){
                    smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/'+'element/invite/type/'+typeCostum+'/id/'+idCostum);
                },
                onRemoveMember(data, dom){
                    var memb<?= $kunik ?> = {
                        childId : data.instanceIdentity,
                        childType : data.collection,
                        parentType : typeCostum,
                        parentId : idCostum,
                        connectType : typeCostum == 'organizations' ? 'members' : (typeCostum == 'projects' ? 'contributors' : 'attendees')
                    }
                    bootbox.confirm(trad.areyousuretodelete, function(result) {
                        if (!result) {
                            return;
                        } else { 
                            ajaxPost(
                                null,
                                baseUrl+"/" + moduleId + "/link/disconnect",
                                memb<?= $kunik ?>,
                                function(donne){

                                    let index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                                    kanbanDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.id,  index: index<?= $kunik ?> });
                                    toastr.success("Membre supprimer");
                                    for(let i = 0; i < uniqueRole<?= $kunik ?>.length ; i++) {
                                        // if($('.kanban-list-card-detail[data-id ='+data.id+'<?=$kunik?>'+i+']').parent().children().length  == 0) {
                                        //     $('.kanban-list-card-detail[data-id ='+data.id+'<?=$kunik?>'+i+']').parent().parent().parent().addClass('hidden')
                                        // }
                                        $('.kanban-list-card-detail[data-id ='+data.id+'<?=$kunik?>'+i+']').addClass('hidden')
                                    }
                                },null,null, {}
                            )
                        }
                    })
                    
                },
                onRemoveRole(clickedData, dom){
                    let success = false;
                    load<?=$kunik?> = false 
                    $.confirm({
                        title : trad.confirmdelete,
                        content : trad.actionirreversible,
                        buttons : {
                            no : {
                                text : 'Non',
                                btnClass : 'btn btn-default'
                            },
                            yes : {
                                text : 'Oui',
                                btnClass : 'btn btn-danger',
                                action : function(){
                                    if(typeCostum === "projects") {
                                        let ide = $('.active-card').data('id')                        
                                        $('#kanban-wrapper-'+ide).addClass('hidden')
                                        $('#kanban-wrapper-'+ide).removeClass('visible')
                                        var index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${clickedData.header}] .kanban-list-card-detail`).index(dom)
                                        kanbanDom<?= $kunik ?>.kanban('deleteData', { column: clickedData.header, id:clickedData.id,  index: index<?= $kunik ?> });
                                        params<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
                                        ajaxPost(
                                            null,
                                            baseUrl+"/" + moduleId + "/search/globalautocomplete",
                                            params<?= $kunik ?>,
                                            function(data){
                                                dataMember<?= $kunik ?> = data.results;
                                            },null,null, {async:false}
                                        )
                                        $.map( 
                                            dataMember<?= $kunik ?> , function( valeur, keys ) {
                                                if(typeof valeur.links != 'undefined' && typeof valeur.links.projects != 'undefined'){
                                                    $.map(valeur.links.projects , function( val, key ) {
                                                        if(typeof val.roles == 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                            let newRole = [];
                                                            var par<?= $kunik ?> = {     
                                                                contextId : idCostum,
                                                                contextType : typeCostum,
                                                                roles : "",
                                                                childId : keys,
                                                                childType : valeur.collection,
                                                                connectType : "members",
                                                                collection : "undefinied",
                                                                scope : ""
                                                            };
                                                            let indice = val.roles.indexOf(clickedData.name);
                                                            if(indice != -1) {
                                                                newRole = val.roles;
                                                                newRole.splice(indice,1)
                                                                par<?= $kunik ?>.roles = newRole.toString();
                                                                let tplCtxToSend = {
                                                                    collection: 'citoyens',
                                                                    id: keys,
                                                                    path: 'links.projects.'+idCostum+'.roles',
                                                                    value: newRole
                                                                }
                                                                dataHelper.path2Value(tplCtxToSend, function (data) {
                                                                    $('#kanban-wrapper-'+clickedData.id).addClass('hidden');
                                                                    success = true
                                                                    toastr.success("Rôle supprimer du membre reussi");
                                                                    $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class', 'kanban-list-card-detail')
                                                                    if(typeof contextData.links != "undefined" && 
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined"){
                                                                            var oldRoles=$.extend({}, contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles);
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                        }
                                                                        else
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                                                    if(typeof contextData.rolesLists != "undefined"){
                                                                        $.each(data.roles, function(e, v){
                                                                            if(rolesList.indexOf(v) == -1)
                                                                                rolesList.push(v);
                                                                            if(typeof contextData.rolesLists[v] != "undefined")
                                                                                contextData.rolesLists[v].count++;
                                                                            else
                                                                                contextData.rolesLists[v]={count:1, label: v};
                                                                            if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
                                                                                oldRoles.splice(oldRoles.indexOf(v) , 1);
                                                                        });
                                                                        if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
                                                                            $.each(oldRoles, function(e, v){
                                                                                if(typeof contextData.rolesLists[v] != "undefined"){
                                                                                    contextData.rolesLists[v].count--;
                                                                                    if(contextData.rolesLists[v].count==0)
                                                                                        delete contextData.rolesLists[v];
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    if(typeof filterGroup != "undefined"){
                                                                        if(notNull(filterGroup.results.community)){
                                                                            if(typeof filterGroup.results.community.links != "undefined" && 
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined")
                                                                                    filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                            else
                                                                                filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.elt.roles};
                                                                        }
                                                                        //filterGroup.search.init(filterGroup);
                                                                    }//else
                                                                    pageProfil.views.directory();
                                                                }) 
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        );
                                        let dataToSend = {
                                            "connectType": "projects",
                                            "column": clickedData
                                        };
                                        if(typeof directorySocket != "undefined"){
                                            directorySocket.emitEvent(wsCO, 'remove_role', dataToSend);
                                        }
                                    }
                                    if(typeCostum === "organizations") {
                                        let ide = $('.active-card').data('id')
                                        $('#kanban-wrapper-'+ide).addClass('hidden')
                                        $('#kanban-wrapper-'+ide).removeClass('visible')
                                        params<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
                                        var index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${clickedData.header}] .kanban-list-card-detail`).index(dom)
                                        kanbanDom<?= $kunik ?>.kanban('deleteData', { column: clickedData.header, id:clickedData.id,  index: index<?= $kunik ?> });
                                        ajaxPost(
                                            null,
                                            baseUrl+"/" + moduleId + "/search/globalautocomplete",
                                            params<?= $kunik ?>,
                                            function(data){
                                                dataMember<?= $kunik ?> = data.results;
                                            },null,null, {async:false}
                                        )
                                        $.map( 
                                            dataMember<?= $kunik ?> , function( valeur, keys ) {
                                                if(typeof valeur.links != 'undefined' && typeof valeur.links.memberOf != 'undefined'){
                                                    $.map(valeur.links.memberOf , function( val, key ) {
                                                        if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                            let newRole = [];
                                                            var par<?= $kunik ?> = {     
                                                                contextId : idCostum,
                                                                contextType : typeCostum,
                                                                roles : "",
                                                                childId : keys,
                                                                childType : valeur.collection,
                                                                connectType : "members",
                                                                collection : "undefinied",
                                                                scope : ""
                                                            };
                                                            let indice = val.roles.indexOf(clickedData.name);
                                                            if(indice !== -1) {
                                                                newRole = val.roles;
                                                                newRole.splice(indice,1)
                                                                par<?= $kunik ?>.roles = newRole.toString()
                                                                let tplCtxToSend = {
                                                                    collection: 'citoyens',
                                                                    id: keys,
                                                                    path: 'links.memberOf.'+idCostum+'.roles',
                                                                    value: newRole
                                                                }
                                                                dataHelper.path2Value(tplCtxToSend, function (data) {
                                                                    $('#kanban-wrapper-'+clickedData.id).addClass('hidden');
                                                                    success = true
                                                                    toastr.success("Rôle supprimer du membre reussi");
                                                                    $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class', 'kanban-list-card-detail')
                                                                    if(typeof contextData.links != "undefined" && 
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined"){
                                                                            var oldRoles=$.extend({}, contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles);
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                        }
                                                                        else
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                                                    if(typeof contextData.rolesLists != "undefined"){
                                                                        $.each(data.roles, function(e, v){
                                                                            if(rolesList.indexOf(v) == -1)
                                                                                rolesList.push(v);
                                                                            if(typeof contextData.rolesLists[v] != "undefined")
                                                                                contextData.rolesLists[v].count++;
                                                                            else
                                                                                contextData.rolesLists[v]={count:1, label: v};
                                                                            if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
                                                                                oldRoles.splice(oldRoles.indexOf(v) , 1);
                                                                        });
                                                                        if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
                                                                            $.each(oldRoles, function(e, v){
                                                                                if(typeof contextData.rolesLists[v] != "undefined"){
                                                                                    contextData.rolesLists[v].count--;
                                                                                    if(contextData.rolesLists[v].count==0)
                                                                                        delete contextData.rolesLists[v];
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    if(typeof filterGroup != "undefined"){
                                                                        if(notNull(filterGroup.results.community)){
                                                                            if(typeof filterGroup.results.community.links != "undefined" && 
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined")
                                                                                    filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                            else
                                                                                filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.elt.roles};
                                                                        }
                                                                        //filterGroup.search.init(filterGroup);
                                                                    }//else
                                                                    pageProfil.views.directory();
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        );

                                        let dataToSend = {
                                            "connectType": "members",
                                            "column": clickedData
                                        };
                                        if(typeof directorySocket != "undefined"){
                                            directorySocket.emitEvent(wsCO, 'remove_role', dataToSend);
                                        }
                                    }
                                    if(typeCostum === "events") {
                                        let ide = $('.active-card').data('id')
                                        $('#kanban-wrapper-'+ide).addClass('hidden')
                                        $('#kanban-wrapper-'+ide).removeClass('visible')
                                        params<?= $kunik ?>.filters["links.events."+idCostum] = {'$exists':true};
                                        var index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${clickedData.header}] .kanban-list-card-detail`).index(dom)
                                        kanbanDom<?= $kunik ?>.kanban('deleteData', { column: clickedData.header, id:clickedData.id,  index: index<?= $kunik ?> });
                                        ajaxPost(
                                            null,
                                            baseUrl+"/" + moduleId + "/search/globalautocomplete",
                                            params<?= $kunik ?>,
                                            function(data){
                                                dataMember<?= $kunik ?> = data.results;
                                            },null,null, {async:false}
                                        )
                                        $.map( 
                                            dataMember<?= $kunik ?> , function( valeur, keys ) {
                                                if(typeof valeur.links != 'undefined' && typeof valeur.links.events != 'undefined'){
                                                    $.map(valeur.links.events , function( val, key ) {
                                                        if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                            let newRole = [];
                                                            var par<?= $kunik ?> = {     
                                                                contextId : idCostum,
                                                                contextType : typeCostum,
                                                                roles : "",
                                                                childId : keys,
                                                                childType : valeur.collection,
                                                                connectType : "attendees",
                                                                collection : "undefinied",
                                                                scope : ""
                                                            };
                                                            let indice = val.roles.indexOf(clickedData.name);
                                                            if(indice !== -1) {
                                                                newRole = val.roles;
                                                                newRole.splice(indice,1)
                                                                par<?= $kunik ?>.roles = newRole.toString()
                                                                let tplCtxToSend = {
                                                                    collection: 'citoyens',
                                                                    id: keys,
                                                                    path: 'links.events.'+idCostum+'.roles',
                                                                    value: newRole
                                                                }
                                                                dataHelper.path2Value(tplCtxToSend, function (data) {
                                                                    $('#kanban-wrapper-'+clickedData.id).addClass('hidden');
                                                                    success = true
                                                                    toastr.success("Rôle supprimer du membre reussi");
                                                                    $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class', 'kanban-list-card-detail')
                                                                    if(typeof contextData.links != "undefined" && 
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                        typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined"){
                                                                            var oldRoles=$.extend({}, contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles);
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                        }
                                                                        else
                                                                            contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                                                    if(typeof contextData.rolesLists != "undefined"){
                                                                        $.each(data.roles, function(e, v){
                                                                            if(rolesList.indexOf(v) == -1)
                                                                                rolesList.push(v);
                                                                            if(typeof contextData.rolesLists[v] != "undefined")
                                                                                contextData.rolesLists[v].count++;
                                                                            else
                                                                                contextData.rolesLists[v]={count:1, label: v};
                                                                            if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
                                                                                oldRoles.splice(oldRoles.indexOf(v) , 1);
                                                                        });
                                                                        if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
                                                                            $.each(oldRoles, function(e, v){
                                                                                if(typeof contextData.rolesLists[v] != "undefined"){
                                                                                    contextData.rolesLists[v].count--;
                                                                                    if(contextData.rolesLists[v].count==0)
                                                                                        delete contextData.rolesLists[v];
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    if(typeof filterGroup != "undefined"){
                                                                        if(notNull(filterGroup.results.community)){
                                                                            if(typeof filterGroup.results.community.links != "undefined" && 
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                                                typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined")
                                                                                    filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                                            else
                                                                                filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.elt.roles};
                                                                        }
                                                                        //filterGroup.search.init(filterGroup);
                                                                    }//else
                                                                    pageProfil.views.directory();
                                                                })
                                                            }
                                                        }
                                                    })
                                                }
                                            }
                                        );

                                        let dataToSend = {
                                            "connectType": "attendees",
                                            "column": clickedData
                                        };
                                        if(typeof directorySocket != "undefined"){
                                            directorySocket.emitEvent(wsCO, 'remove_role', dataToSend);
                                        }
                                    }
                                }
                            }
                        }
                    });
                },
                onCardClick(clickedData, style) {
                    if(clickedData.header == 'listRole') {
                        // ajaxPost(
                        //     null,
                        //     baseUrl+"/" + moduleId + "/search/globalautocomplete",
                        //     params<?= $kunik ?>,
                        //     function(data){
                        //         dataMember<?= $kunik ?> = data.results;
                        //     },null,null, {async:false}
                        // )

                        if($('#kanban-wrapper-'+clickedData.id).hasClass('hidden')) {
                            $('#kanban-wrapper-'+clickedData.id).removeClass('hidden');
                            $('#kanban-wrapper-'+clickedData.id).addClass('visible');
                            $('.active-card').addClass('active-role')
                            $('.active-card .kanban-footer-card').css({
                                "color" : "#172b4d"
                            });
                            $('.active-card').addClass('activer')
                            if(typeCostum === "organizations") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.memberOf != 'undefined'){
                                            $.map(valeur.links.memberOf , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass('active-member')
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass(val.roles[indice])
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                            if(typeCostum === "projects") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.projects != 'undefined'){
                                            $.map(valeur.links.projects , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass('active-member')
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass(val.roles[indice])
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                            if(typeCostum === "events") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.events != 'undefined'){
                                            $.map(valeur.links.events , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass('active-member')
                                                        $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').addClass(val.roles[indice])
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                            

                        } else {
                            $('#kanban-wrapper-'+clickedData.id).addClass('hidden');
                            $('#kanban-wrapper-'+clickedData.id).removeClass('visible');
                            $('.active-card').removeClass('active-role');
                            $('.active-card .kanban-footer-card').css({
                                "color" : "#172b4d"
                            });
                            $('.active-card').removeClass('activer')
                            if(typeCostum === "organizations") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.memberOf != 'undefined'){
                                            $.map(valeur.links.memberOf , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        if($('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class') == 'kanban-list-card-detail active-member '+val.roles[indice]) {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass('active-member')
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        } else {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        }
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                            if(typeCostum === "projects") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.projects != 'undefined'){
                                            $.map(valeur.links.projects , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        if($('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class') == 'kanban-list-card-detail active-member '+val.roles[indice]) {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass('active-member')
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        } else {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        }
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                            if(typeCostum === "events") {
                                $.map( 
                                    dataMember<?= $kunik ?> , function( valeur, keys ) {
                                        if(typeof valeur.links != 'undefined' && typeof valeur.links.events != 'undefined'){
                                            $.map(valeur.links.events , function( val, key ) {
                                                if(typeof val.roles === 'object' && Array.isArray(val.roles) && key == idCostum) {
                                                    let newRole = [];
                                                    let indice = val.roles.indexOf(clickedData.name);
                                                    if(indice !== -1) {
                                                        if($('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').attr('class') == 'kanban-list-card-detail active-member '+val.roles[indice]) {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass('active-member')
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        } else {
                                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+keys+']').removeClass(val.roles[indice])
                                                        }
                                                    }
                                                }
                                            })
                                        }
                                    }
                                );
                            }
                        }
                    }
                },
                onEditCardOpen(editData) {
                },
                onCreateCardOpen(column) {
                    mylog.log("Ato lets zao", column);
                },
                onCardUpdate(updated) {
                },
                onCardInsert(inserted) {
                    var copy = $.extend({}, inserted);
                    copy.actions = [
                        { icon: 'fa fa-commenting',badge: '0', action: 'onCardCommentClick' },
                        { icon: 'fa fa-picture-o', badge: '0', action: 'onCardPictureClick' },
                        { icon: 'fa fa-thumb-tack', badge: '0/0', action: 'onCardTaskClick', condition: { badge: '0/0' } },
                    ];
                    var id = copy.id;
                    copy.id = 'newid';
                    console.log('id is', id);
                    kanbanDom<?= $kunik ?>.kanban('setData', { column: inserted.header, id: id, data: copy });
                },
                onCardDrop(dropped, updates) {
                    let nb = $('.activer[data-id = '+dropped.target+'] .card-action').text();
                    load<?=$kunik?> = false
                    var copy = $.extend({}, dropped.data);
                    copy.canMoveCard = false;
                    copy.actions = [
                        { icon: 'fa fa-trash', action: 'onDelete'}
                    ]
                    kanbanDom<?= $kunik ?>.kanban('setData', { column: copy.header, id: copy.id, data: copy});
                    
                    var assignRole = "";
                    
                    var par<?= $kunik ?> = {     
                        contextId : idCostum,
                        contextType : typeCostum,
                        roles : "",
                        childId : dropped.data.instanceIdentity,
                        childType : dropped.data.collection,
                        connectType : "members",
                        collection : "undefinied",
                        scope : ""
                    };

                    for(let i = 0 ; i < dropped.columns.length ; i++) {
                        if(dropped.columns[i].id == dropped.target ){
                            assignRole = dropped.columns[i].label;
                        }
                    }
                    var assignMember = ""
                    $.map( 
                        dataMember<?= $kunik ?> , function( valeur, keys ) {
                            if(keys == dropped.data.instanceIdentity) {
                                if(typeCostum == "projects" && exists(valeur.links) && exists(valeur.links.projects)){
                                    par<?= $kunik ?>.connectType = "contributors";
                                    $.map(valeur.links.projects, function( val, key){
                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                            assignMember = val.roles.toString()
                                            assignMember = assignMember+","+assignRole
                                        }
                                    })
                                }
                                if(typeCostum == "organizations" && exists(valeur.links) && exists(valeur.links.memberOf)){
                                    par<?= $kunik ?>.connectType = "members";
                                    $.map(valeur.links.memberOf, function( val, key){
                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                            assignMember = val.roles.toString()
                                            assignMember = assignMember+","+assignRole
                                        }
                                    })
                                    
                                }
                                if(typeCostum == "events" && exists(valeur.links) && exists(valeur.links.events)){
                                    par<?= $kunik ?>.connectType = "attendees";
                                    $.map(valeur.links.events, function( val, key){
                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                            assignMember = val.roles.toString()
                                            assignMember = assignMember+","+assignRole
                                        }
                                    })
                                    
                                }

                            }
                        }
                    );
                    if(assignMember == ""){
                        par<?= $kunik ?>.roles = assignRole

                    } else {
                        par<?= $kunik ?>.roles = assignMember
                    }
                    
                    ajaxPost(
                        null,
                        baseUrl+"/" + moduleId + "/link/removerole/",
                        par<?= $kunik ?>,
                        function(data){
                            let htm = `${nb*1+1*1} <span class="fa fa-user"> </span>`
                            $('.activer[data-id = '+dropped.target+'] .kanban-footer-card .nbpers').html(htm)
                            $('.activer[data-id = '+dropped.target+']').addClass('active-role')
                            toastr.success("Ajout de rôle dans le membre reussi");
                            if(typeof contextData.links != "undefined" && 
                                typeof contextData.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined"){
                                    var oldRoles=$.extend({}, contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles);
                                    contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                    dataMember<?= $kunik ?>[par<?= $kunik ?>.childId].links[par<?= $kunik ?>.connectType == "members" ? "memberOf" : par<?= $kunik ?>.connectType][contextData.id].roles = data.roles;
                                }
                                else{
                                    contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                    dataMember<?= $kunik ?>[par<?= $kunik ?>.childId].links[par<?= $kunik ?>.connectType == "members" ? "memberOf" : par<?= $kunik ?>.connectType][contextData.id] = {"roles": data.roles};
                                }
                            if(typeof contextData.rolesLists != "undefined"){
                                $.each(data.roles, function(e, v){
                                    if(rolesList.indexOf(v) == -1)
                                        rolesList.push(v);
                                    if(typeof contextData.rolesLists[v] != "undefined")
                                        contextData.rolesLists[v].count++;
                                    else
                                        contextData.rolesLists[v]={count:1, label: v};
                                    if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
                                        oldRoles.splice(oldRoles.indexOf(v) , 1);
                                });
                                if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
                                    $.each(oldRoles, function(e, v){
                                        if(typeof contextData.rolesLists[v] != "undefined"){
                                            contextData.rolesLists[v].count--;
                                            if(contextData.rolesLists[v].count==0)
                                                delete contextData.rolesLists[v];
                                        }
                                    });
                                }
                            }
                            if(typeof filterGroup != "undefined"){
                                if(notNull(filterGroup.results.community)){
                                    if(typeof filterGroup.results.community.links != "undefined" && 
                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined")
                                            filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                    else
                                        filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                }
                                //filterGroup.search.init(filterGroup);
                            }//else
                            pageProfil.views.directory();
                            let dataToSend = {
                                "connectType": par<?= $kunik ?>.connectType,
                                "column": {
                                    "id": dropped.target,
                                    "label": assignRole,
                                    "count": updates.target.length
                                },
                                "data": dropped.data,
                            };
                            if(typeof directorySocket != "undefined"){
                                directorySocket.emitEvent(wsCO, 'add_members', dataToSend);
                            }
                        },null,null, {}
                    )
                    let newRole = {
                        _id : newCol.id,
                        id : newCol.id,
                        instanceIdentity : newCol.id,
                        title : "<i>"+newCol.label+"</i>",
                        name : newCol.label,
                        header : "listRole",
                        canEditHeader: false,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {icon: 'fa fa-trash', action: 'onRemoveRole'},
                            {icon: 'fa fa-user',badge: 1, action: '' }
                        ]
                    }
                    let ii = '';
                    for(let i = 0 ; i < headers<?= $kunik ?>.length ; i++) {
                        if(headers<?= $kunik ?>[i].id == dropped.target ){
                            ii = 1;
                        }
                    }
                    if(ii === '') {
                        kanbanDom<?= $kunik ?>.kanban('addData', newRole);
                        $('.kanban-list-card-detail[data-id ='+newCol.id+']').css({
                            "background-color" : "gray",
                            "color" : "#172b4d"
                        });
                        let newHead = {
                            id : newCol.id,
                            label : newCol.label
                        }
                        $('.kanban-list-card-detail[data-id ='+newCol.id+']').addClass('activer')
                        headers<?= $kunik ?>.push(newHead)
                    }
                },
                onHeaderChange(changed) {
                    console.log(changed, 'changed');
                    newCol.id = changed.column
                    newCol.label = changed.values.new
                },
                onCardCommentClick(datum) {
                    console.log('comment', datum);
                },
                onDelete(action, dom) {
                    let nb = $('.activer[data-id = '+action.header+'] .card-action').text();
                    load<?=$kunik?> = false;
                    var assignRole = "";
                    $.confirm({
                        title : trad.confirmdelete,
                        content : trad.actionirreversible,
                        buttons : {
                            no : {
                                text : 'Non',
                                btnClass : 'btn btn-default'
                            },
                            yes : {
                                text : 'Oui',
                                btnClass : 'btn btn-danger',
                                action : function(){
                                    var par<?= $kunik ?> = {     
                                        contextId : idCostum,
                                        contextType : typeCostum,
                                        roles : "",
                                        childId : action.instanceIdentity,
                                        childType : action.collection,
                                        connectType : "members",
                                        collection : "undefinied",
                                        scope : ""
                                    };

                                    for(let i = 0 ; i < headers<?= $kunik ?>.length ; i++) {
                                        if(headers<?= $kunik ?>[i].id == action.header ){
                                            assignRole = headers<?= $kunik ?>[i].label;
                                        }
                                    }
                                    var assignMember = []
                                    $.map( 
                                        dataMember<?= $kunik ?> , function( valeur, keys ) {
                                            if(keys == action.instanceIdentity) {
                                                if(typeCostum == "projects" && exists(valeur.links) && exists(valeur.links.projects)){
                                                    par<?= $kunik ?>.connectType = "contributors";
                                                    $.map(valeur.links.projects, function( val, key){
                                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                                            let indice = val.roles.indexOf(assignRole);
                                                            if(indice !== -1) {
                                                                assignMember = val.roles;
                                                                assignMember.splice(indice,1)
                                                                par<?= $kunik ?>.roles = assignMember.toString()
                                                            }
                                                        }
                                                    })
                                                }
                                                if(typeCostum == "organizations" && exists(valeur.links) && exists(valeur.links.memberOf)){
                                                    par<?= $kunik ?>.connectType = "members";
                                                    $.map(valeur.links.memberOf, function( val, key){
                                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                                            let indice = val.roles.indexOf(assignRole);
                                                            if(indice != -1) {
                                                                assignMember = val.roles;
                                                                assignMember.splice(indice,1)
                                                                par<?= $kunik ?>.roles = assignMember.toString()
                                                            }
                                                        }
                                                    })
                                                    
                                                }
                                                if(typeCostum == "events" && exists(valeur.links) && exists(valeur.links.events)){
                                                    par<?= $kunik ?>.connectType = "attendees";
                                                    $.map(valeur.links.events, function( val, key){
                                                        if(key == idCostum && typeof val.roles === 'object' && Array.isArray(val.roles)){
                                                            let indice = val.roles.indexOf(assignRole);
                                                            if(indice != -1) {
                                                                assignMember = val.roles;
                                                                assignMember.splice(indice,1)
                                                                par<?= $kunik ?>.roles = assignMember.toString()
                                                            }
                                                        }
                                                    })
                                                    
                                                }

                                            }
                                        }
                                    );
                                    ajaxPost(
                                        null,
                                        baseUrl+"/" + moduleId + "/link/removerole/",
                                        par<?= $kunik ?>,
                                        function(data){
                                            toastr.success("Rôle supprimer du membre reussi");
                                            let htm = `${nb-1} <span class="fa fa-user"> </span>`
                                            $('.activer[data-id = '+action.header+'] .kanban-footer-card .nbpers').html(htm)
                                            if(nb == 1) {
                                                $('.activer[data-id = '+action.header+']').addClass('hidden')
                                                $('#kanban-wrapper-'+action.header).addClass('hidden')
                                            }
                                            $('#kanban-wrapper-memberof .kanban-list-card-detail[data-id ='+action.instanceIdentity+']').attr('class', 'kanban-list-card-detail')
                                            if(typeof contextData.links != "undefined" && 
                                                typeof contextData.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                typeof contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined"){
                                                    var oldRoles=$.extend({}, contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles);
                                                    if(typeof data.roles == "object" && Array.isArray(data.roles)){
                                                        contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                    }else{
                                                        contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = [];
                                                    }
                                                }
                                                else{
                                                    if(typeof data.roles == "object" && Array.isArray(data.roles)){
                                                        contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] = {"roles": data.roles} ;
                                                    }else{
                                                        contextData.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] = {"roles": []};
                                                    }
                                                }
                                            if(typeof contextData.rolesLists != "undefined"){
                                                $.each(data.roles, function(e, v){
                                                    if(rolesList.indexOf(v) == -1)
                                                        rolesList.push(v);
                                                    if(typeof contextData.rolesLists[v] != "undefined")
                                                        contextData.rolesLists[v].count++;
                                                    else
                                                        contextData.rolesLists[v]={count:1, label: v};
                                                    if(typeof oldRoles != "undefined" && $.inArray(v, oldRoles) >= 1)
                                                        oldRoles.splice(oldRoles.indexOf(v) , 1);
                                                });
                                                if(typeof oldRoles != "undefined" && notEmpty(oldRoles)){
                                                    $.each(oldRoles, function(e, v){
                                                        if(typeof contextData.rolesLists[v] != "undefined"){
                                                            contextData.rolesLists[v].count--;
                                                            if(contextData.rolesLists[v].count==0)
                                                                delete contextData.rolesLists[v];
                                                        }
                                                    });
                                                }
                                            }
                                            if(typeof filterGroup != "undefined"){
                                                if(notNull(filterGroup.results.community)){
                                                    if(typeof filterGroup.results.community.links != "undefined" && 
                                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType] != "undefined" &&
                                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId] != "undefined" &&
                                                        typeof filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles != "undefined")
                                                            filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId].roles = data.roles ;
                                                    else
                                                        filterGroup.results.community.links[par<?= $kunik ?>.connectType][par<?= $kunik ?>.childId]={"roles": data.roles};
                                                }
                                                //filterGroup.search.init(filterGroup);
                                            }//else
                                            let dataToSend = {
                                                "connectType": par<?= $kunik ?>.connectType,
                                                "column": {
                                                    "id": action.header,
                                                    "label": assignRole,
                                                    "count": nb - 1
                                                },
                                                "data": action,
                                            };
                                            if(typeof directorySocket != "undefined"){
                                                directorySocket.emitEvent(wsCO, 'remove_members', dataToSend);
                                            }
                                            pageProfil.views.directory();
                                        },null,null, {async:false}
                                    )
                                    var index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(dom)
                                    kanbanDom<?= $kunik ?>.kanban('deleteData', { column: action.header, id:action.id,  index: index<?= $kunik ?> });
                                }
                            }
                        }
                    });
                },
                onCardPictureClick() {
                    alert('clicked');
                }
            });
        }

        const getValueByPath = (object, path) => {
            if (path === undefined || path === null) {
                return object;
            }
            const parts = path.split('.');
            for (let i = 0; i < parts.length; ++i) {
                if (object === undefined || object === null) {
                    return undefined;
                }
                const key = parts[i];
                object = object[key];
            }

            return object;
        }

        function addFocuserFilter(tags){
            var authTagsData = (Array.isArray(tags))?tags:Object.keys(tags);
            authTagsData = authTagsData.map(v=> v.trim());
            authTagsData = (new Set(authTagsData));
            d3.select("div#focus-container<?= $kunik ?>")
            .selectAll("button")
            .data(authTagsData)
            .join((enter) => {
                enter.append("xhtml:button")
                    .text(d => d)
                    .classed("btn btn-circle-zoomer", true)
                    .on("click", (e,d) => {
                    var thisElement = $(e.target);
                    if(activeCircle<?= $kunik ?> != d){
                        circle<?= $kunik ?>.focus(d);
                        activeCircle<?= $kunik ?> = d;
                        $(".btn-circle-zoomer").removeClass("btn-primary");
                        thisElement.addClass("btn-primary");
                    }else{
                        activeCircle<?= $kunik ?> = "";
                        thisElement.removeClass("btn-primary");
                        circle<?= $kunik ?>.unfocus().then(() => {
                            mylog.log("UNFOCUSED");
                        });
                    }
                    
                    $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                    $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $("#"+GraphUtils.slugify(d)).addClass("in");
                })
            });
        }

        $(".switchViewGraph").on("click", function () {
            $(".btn-view-change").removeClass("active");
            $(this).addClass("active");
            $("#hexa-container-<?= $kunik ?>,#kanban<?= $kunik ?>").css("display", "none");
            $("#circle-container-<?= $kunik ?>").slideDown(500);
            var authorizedTag = [];
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    var structuredData = [];
                    var lists = {};
                    let fieldPath =  `links.memberOf.${idCostum}.roles`;
                    var dataCircle = {};
                    if(typeof typeCostum != "undefined" && typeCostum == "organizations"){
                        fieldPath = `links.memberOf.${idCostum}.roles`;
                    }else if(typeof typeCostum != "undefined" && typeCostum == "projects"){
                        fieldPath = `links.projects.${idCostum}.roles`;
                    }else if(typeof typeCostum != "undefined" && typeCostum == "events"){
                        fieldPath = `badges`;
                    }else if (typeof typeCostum != "undefined" && !["organizations", "projects", "events"].includes(typeCostum)){
                        fieldPath = `tags`;
                    }
                    $.each(data.results, (key, index) => {
                        var val = getValueByPath(data.results[key], fieldPath);
                        if(val && val!=""){
                            if(Array.isArray(val)){
                                for(var i in val){
                                    if(typeof val[i]=="object"){
                                        lists[val[i].name] = val[i]
                                    }else{
                                        lists[val[i]] = val[i];
                                    }
                                }
                            }else if(typeof val=="object" && val!=null){
                                if(fieldPath=="badges"){
                                    $.each(val, function(i, v){
                                        lists[v.name] = v.name
                                    });
                                }else{
                                    $.each(val, function(k, v){
                                        lists[k] = v.name;
                                    });
                                }
                            }else if(typeof val=="string"){
                                lists[val] = val;
                            }
                            
                        }
                    });
                    dataCircle = lists;

                    if(authorizedTag.length == 0){
                        authorizedTag = lists;
                    }
                    if(authorizedTag != null){
                        $.each(authorizedTag, function(i, authTag){
                            var child = {
                                id:"element"+i,
                                label:(typeof authTag == "string")?authTag:i,
                                children: []
                            }
                            Object.keys(data.results).forEach((key, index) => {
                                var element = data.results[key];
                                let itIsIn = false;
                                if(typeof authTag == "object"){
                                    for(var k in authTag){
                                        if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                            itIsIn = true;
                                        }
                                    }
                                }
                                if(
                                    child.label==getValueByPath(element, fieldPath)
                                    || (Array.isArray(getValueByPath(element, fieldPath)) && getValueByPath(element, fieldPath).includes(child.label)) 
                                    || itIsIn
                                    ){
                                        structuredData.push({
                                            "id":key,
                                            "label":element.name,
                                            "collection": element.collection,
                                            "type": child.label,
                                            "group": child.label,
                                            "img":element.profilThumbImageUrl,
                                            "profileImage": element.profilMediumImageUrl,
                                            "userWallet": element.userWallet,
                                            "socialNetwork": element.socialNetwork
                                        });
                                }else if(fieldPath=="badges"){
                                    if(typeof element.badges != "undefined" && element.badges != null){
                                        $.each(element.badges, function(ieb, eb){
                                            if(eb.name==child.label){
                                                structuredData.push({
                                                    "id":key,
                                                    "label":element.name,
                                                    "collection": element.collection,
                                                    "type": eb.name,
                                                    "group": eb.name,
                                                    "img":element.profilThumbImageUrl,
                                                    "profileImage": element.profilMediumImageUrl,
                                                    "userWallet": element.userWallet,
                                                    "socialNetwork": element.socialNetwork
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }

                    var intervalGraph = setInterval(() => {
                        if($("#circle-container-<?= $kunik ?>").is(':visible')){
                            clearInterval(intervalGraph);
                            circle<?= $kunik ?> = new CircleGraph([], d => d.group, authorizedTag);
                            circle<?= $kunik ?>.draw("#circle-container-<?= $kunik ?>");
                            circle<?= $kunik ?>.updateData(structuredData);
                            circle<?= $kunik ?>.initZoom();
                            addFocuserFilter(authorizedTag);

                            circle<?= $kunik ?>.setOnClickNode((e,d,n) => {
                                params = {};
                                params["elementId"] = idCostum;
                                params["userId"] = d.data.id;
                                params["name"] = d.data.label;
                                params["type"] = d.data.collection;
                                params["socialNetwork"] = typeof d.data.socialNetwork != 'undefined' ? d.data.socialNetwork : [];
                                params["profileImg"] = typeof d.data.profileImage != 'undefined' ? d.data.profileImage : "";
                                params["userCredits"] = typeof d.data.credits != 'undefined' ? d.data.credits : 0;
                                ajaxPost(
                                    null,
                                    baseUrl + "/co2/person/getuserbadgeandrecentcontribution",
                                    params, 
                                    function (response) {  
                                        params["badgesDetails"] = response.badges;
                                        params["projects"] = response.projects;
                                        params["actionsCreated"] = response.actionsCreated;
                                        params["actionsParticipated"] = response.actionsParticipated;
                                        params["actionsLast12Month"] = response.actionsLast12Month;
                                        params["countAll"] = response.countAll;

                                        urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                                    }
                                );
                            })
                        }
                    }, 100);

                },null,null, {async:false}
            )
        })

        $(".switchViewHexa").on("click", function(){
            $(".btn-view-change").removeClass("active");
            $(this).addClass("active");
            var canvas = $("#hexagoneCommunity")[0];
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            // Supprime le contenu précédent pour éviter l'accumulation
            $(".container-preview #filter-hexagone-container").empty();

            // Réinitialise l'objet hexagonCommunity en recréant une instance propre
            var hexagonCommunity<?= $kunik ?> = new HexagonGrid("hexagoneCommunity", 50, window.innerWidth - 700);

            // Réinitialiser l'affichage des panels
            $("#circle-container-<?= $kunik ?>,#kanban<?= $kunik ?>").css("display", "none");
            $("#hexa-container-<?= $kunik ?>").slideDown(1000);

            HexagonGrid.prototype.clickEvent = function(e){
                let hexa = this;
                var mouseX = e.pageX;
                var mouseY = e.pageY;

                var localX = mouseX - this.canvasOriginX;
                var localY = mouseY - this.canvasOriginY;

                var tile = this.getSelectedTile(localX, localY);
                if(this.hasHexagon(tile.column, tile.row)){
                    let drawedHex = this.drawedHex[`${tile.column},${tile.row}`];
                    let data = this.data[drawedHex.group][`${drawedHex.column},${drawedHex.row}`];
                    let params = {};
                    params["elementId"] = idCostum;
                    params["userId"] = data.id;
                    params["name"] = data.text;
                    params["type"] = data.type;
                    params["socialNetwork"] = typeof data.socialNetwork != 'undefined' ? data.socialNetwork : [];
                    params["profileImg"] = data.image != null ? data.image : "";
                    params["userCredits"] = typeof data.credits != 'undefined' ? data.credits : 0;
                    ajaxPost(
                        null,
                        baseUrl + "/co2/person/getuserbadgeandrecentcontribution",
                        params, 
                        function (response) {  
                            params["badgesDetails"] = response.badges;
                            params["projects"] = response.projects;
                            params["actionsCreated"] = response.actionsCreated;
                            params["actionsParticipated"] = response.actionsParticipated;
                            params["actionsLast12Month"] = response.actionsLast12Month;
                            params["countAll"] = response.countAll;

                            urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                        }
                    );
                }
            }

            var dataMemberHexa<?= $kunik ?> = {
                "roles": [],
                "data" : {

                }
            };
            let center = hexagonCommunity<?= $kunik ?>.getCenterTile();
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    $.map(data.results, function(valeur, key){
                        if(typeCostum == "organizations"){
                            if(exists(valeur.links) && exists(valeur.links.memberOf)){
                                $.map(valeur.links.memberOf, function(value, idData){
                                    if(idCostum == idData){
                                        if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                            dataMemberHexa<?= $kunik ?>.roles.push(...value.roles);
                                            $.map(value.roles, function(role, roleKey){
                                                if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                    if(roleToShow.indexOf(role) >= 0){
                                                        if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMemberHexa<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+idCostum+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                    }
                                                }else{
                                                    if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMemberHexa<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: [],
                                                            users: []
                                                        };
                                                    }
                                                    dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                    dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+idCostum+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        if(typeCostum == "events"){
                            if(exists(valeur.links) && exists(valeur.links.events)){
                                $.map(valeur.links.events, function(value, idData){
                                    if(idCostum == idData){
                                        if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                            dataMemberHexa<?= $kunik ?>.roles.push(...value.roles);
                                            
                                            $.map(value.roles, function(role, key){
                                                if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                    if(roleToShow.indexOf(role) >= 0){
                                                        if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMemberHexa<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+idCostum+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                    }
                                                }else{
                                                    if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMemberHexa<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: [],
                                                            users: []
                                                        };
                                                    }
                                                    dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                    dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                }    
                                            });
                                        }
                                    }
                                });
                            }
                        }
                        if(typeCostum == "projects"){
                            if(exists(valeur.links) && exists(valeur.links.projects)){
                                $.map(valeur.links.projects, function(value, idData){
                                    if(idCostum == idData){
                                        if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                            dataMemberHexa<?= $kunik ?>.roles.push(...value.roles);
                                            
                                            $.map(value.roles, function(role, key){
                                                if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                    if(roleToShow.indexOf(role) >= 0){
                                                        if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMemberHexa<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                    }
                                                }else{
                                                    if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMemberHexa<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: [],
                                                            users: []
                                                        };
                                                    }
                                                    dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                }if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                    if(roleToShow.indexOf(role) >= 0){
                                                        if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMemberHexa<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                    }
                                                }else{
                                                    if(typeof dataMemberHexa<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMemberHexa<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: [],
                                                            users: []
                                                        };
                                                    }
                                                    dataMemberHexa<?= $kunik ?>.data[role].users.push(valeur);
                                                    dataMemberHexa<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                }    
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                },null,null, {async:false}
            )
            dataMemberHexa<?= $kunik ?>.roles = [...new Set(dataMemberHexa<?= $kunik ?>.roles)];
            $.each(dataMemberHexa<?= $kunik ?>.data, function(key, value){
                let btn = `<div class="btn btn-hexa-container">
                    <button class="btn-expand" data-toggle="collapse" href="#${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <button class="btn-hexagone-zoomer" data-type="group">
                        ${key}
                        <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${dataMemberHexa<?= $kunik ?>.data[key].childs.length}</span>
                    </button>
                </div>
                <div class="collapse hexagone-child-container" id="${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                `;
                dataMemberHexa<?= $kunik ?>.data[key].childs.forEach((child) => {
                    btn += `<button class="btn btn-hexagone-zoomer" data-type="children" data-value='${hexagonCommunity<?= $kunik ?>.slugify(child.text)}'>
                        ${child.text}
                    </button>`;
                })
                btn += `</div>`
                $(".container-preview #filter-hexagone-container").append(btn);
            });

            hexagonCommunity<?= $kunik ?>.placeGroupsWithOffsets(Object.values(dataMemberHexa<?= $kunik ?>.data), center);
            hexagonCommunity<?= $kunik ?>.fitBounds();
            $(".btn-hexagone-zoomer").off().on('click', function(){
                if($(this).data("type") == "group"){
                    if($(this).parents(".btn-hexa-container").hasClass("btn-primary")){
                        $(".btn-hexa-container").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let group = $(this).find("span").data("countvalue");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).parents(".btn-hexa-container").addClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomToGroup(group);
                    }
                }else{
                    if($(this).hasClass("btn-primary")){
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let texte = $(this).data("value");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).addClass("btn-primary");
                        let element = Object.values(hexagonCommunity<?= $kunik ?>.drawedHex).find(hex => {
                            return hexagonCommunity<?= $kunik ?>.slugify(hex.text) == texte;
                        });
                        if(typeof element != "undefined"){
                            hexagonCommunity<?= $kunik ?>.zoomToElement(element.column, element.row);
                        }
                    }
                }
            })
        });

        $(".switchViewEdition").on("click", function(){
            $(".btn-view-change").removeClass("active");
            $(this).addClass("active");
            $("#circle-container-<?= $kunik ?>,#hexa-container-<?= $kunik ?>").css("display", "none");
            $("#kanban<?= $kunik ?>").slideDown(1000)
        });
    });


</script>
