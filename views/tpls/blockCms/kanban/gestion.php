
<?php 
    $myCmsId  = $blockKey;
    if(isset($costum)){
        $myCmsId  = $blockCms["_id"]->{'$id'};
    }
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    if(!isset($costum)){
        $costum = CacheHelper::getCostum();
    }

    HtmlHelper::registerCssAndScriptsFiles(
		[
			"/js/aap/top_contributor_filter.js",
			'/css/aap/detail.css',
            '/css/aap/aap.css'
		],
		Yii::app()->getModule('co2')->assetsUrl
	);
?>



<style id="css-<?= $kunik ?>">
    .column-header-text {
        color: #000000 !important;
    }
    .btn-is-admin {
        text-decoration: line-through;
    }
    .kanban-<?= $kunik ?> {
        height : 80vh !important;
    }
    .kanban-list-card-action {
        right : 7px !important;
    } 
    .kanban-content {
        display: flex;
    }
    .projet-content {
        display: flex;
        align-items: end;
    }
    .projet-content .button-projet{
        margin-right : 20px;
    }
    .filtre {
        display : flex;
        padding : 0 20px;
    }
    .filtre .filtre-type {
        margin-right : 20px;
    }
    .bar-content {
        display: flex;
        justify-content : space-between;
        padding: 0px 20px;
        background-color: #eee;
        border : 1px solid #eee;
        border-radius : 5px;
        margin : 5px 20px 0px;
        min-height: 50px;
    }
    .bar-avatar {
        margin-right : 5px;
    }
    .bar-avatar .tab-contributor.expander {
        background-color : #9BC125 !important;
    }
    .bar-avatar .contributor-project-filter{
       width : 400px !important;
    }
    
    .active-role {
        background-color : gray !important;
        color : #172b4d !important;
    }
    .active-mile {
        background-color : gray !important;
        color : #172b4d !important;
    }
    .active-member {
        background-color : rgb(183, 183, 183) !important;
    }
    .color-red {
        color : #a94442 !important;
    }
    .action-menu {
        text-align: center;
        list-style: none;
        padding: 0;
    }
    .action-menu > li > a {
        display: block;
        padding: 5px 10px;
        clear: both;
        font-weight: 400;
        line-height: 1.42857143;
        color: #333 !important;
        white-space: nowrap;
        font-size: 16px !important;
    }
    .action-menu > li > a:focus, .action-menu > li > a:hover {
        color: #262626;
        text-decoration: none;
        background-color: #f5f5f5;
    }
    #ajax-modal {
        z-index: 9999999999999999 !important;
    }
    .kanban-overlay {
        z-index: 9999999999999999 !important;
    }
    ul.breadcrumb {
        padding: 5px 16px;
        list-style: none;
        background-color: #eee;
        margin : 7px 0 0 0; 
    }
    ul.breadcrumb li {
        display: inline;
        font-size: 12px;
    }
    ul.breadcrumb li+li:before {
        padding: 8px;
        color: black;
        content: none !important;
    }

    @media screen and (max-width: 576px) {
        .bar-breadcrumb {
            display : none;
        }
        .bar-avatar .contributor-project-filter{
            width : 200px !important;
        }
    }

    .active-bread {
        color: #0275d8;
        text-decoration: none;
    }
    .active-bread:hover {
        color: #01447e;
        text-decoration: underline;
    }
    .hide-bread {
        color: black;
        text-decoration: none;
        cursor: default;
    }
    .hide-bread:hover {
        color: black;
        text-decoration: none;
    }
    .separateur-bread {
        color : black !important;
        margin: 0 5px !important;
    }
    .separateur-bread:hover {
        color : black !important;
        text-decoration: none !important;
    }
    .loader<?= $kunik ?> {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 42%;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
        z-index: 9999999999999999 !important;
        padding-top: 180px !important;
        
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> {
        width: auto;
        height: auto;
    }
    .loader<?= $kunik ?> .content<?= $kunik ?> .processingLoader {
        width : auto !important;
        height : auto !important;
    }
    .backdrop<?= $kunik ?> {
        position: fixed;
        opacity: .8;
        background-color : #000;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        width: 100%;
        height : 100vh;
        z-index: 999999999 !important;
    }
    .kanban-new-card-button {
        color: #172b4d !important;
    }
</style>
<div class="<?= $kunik ?> <?= $kunik ?>-css">
    <div class="content<?= $kunik ?>">
        <div class="kanban-content">
            <div class="filtre">
                <div class="filtre-name">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-search"> </i></span>
                        <input id="nameMember<?=$kunik?>" type="text" class="form-control" name="nameMember<?=$kunik?>" placeholder="Que recherchez-vous?">
                    </div>
                </div>
            </div>
            <div class="projet-content">
                <div class="button-projet">
                    <button type="button" id="project-<?= $kunik ?>" class="btn btn-info"><i class="fa fa-lightbulb-o"> </i> Tous les projets</button>
                </div>
            </div>
        </div>
        <div class="bar-content">
            <div class="bar-breadcrumb">
                <ul class="breadcrumb">
                    <li><a href="javascript:;" class="" id="projetBread" data-ide=""><span class="separateur-bread"></span> Projet</a></li>
                    <li><a href="javascript:;" class="hidden" id="actionBread"><span class="separateur-bread"> / </span> Action</a></li>
                </ul>
            </div>
            <div class="bar-filtre">
                <div class="bar-avatar">
                    <div class="contributor-project-filter dropdown" id="contributor-filter-container">
                        
                    </div>   
                </div>
            </div>
        </div>
        <div id="action-preview<?= $blockKey ?>"></div>
        <div data-kunik="<?= $kunik ?>" class="kanban-<?= $kunik ?>" data-name="kanban" data-id="<?= $myCmsId ?>" style="margin-bottom:70px !important;">
            <div id="kanban<?= $kunik ?>">
            </div>
        </div>

        <div class="loader<?= $kunik ?> hidden" id="loader<?= $kunik ?>" role="dialog">
            <div class="content<?= $kunik ?>" id="content<?= $kunik ?>"></div>
        </div>
        <div class="backdrop<?= $kunik ?> hidden" id="backdrop<?= $kunik ?>"></div>
    </div>
</div>

<script type="text/javascript">
    if(typeof costum != 'undefined' && costum !== null){
        sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        if (costum != null && costum.editMode == true) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
            var kanbanGestion = {
                configTabs: {
                    style: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    },
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            }
            cmsConstructor.blocks.gestion<?= $myCmsId ?> = kanbanGestion;
        }
    }
    
    $(function () {
        var load<?=$kunik?> = true;
        var newCol = {
            id : '',
            label : '' 
        }
        var memberPerRole<?= $kunik ?> = [];
        var params<?= $kunik ?> = {     
            searchType : ["citoyens","organizations"],
            notSourceKey : true,
            fields : ["links"],
            filters : {},
            indexStep : 0        
        };
        var dataMember<?= $kunik ?> = {};
        var member<?= $kunik ?> = [];
        var initMember<?= $kunik ?> = [];
        var allProjet<?= $kunik ?> = {};
        var my<?= $kunik ?> = ''
        var activeProjet<?= $kunik ?> = ''
        var activeMilestone<?= $kunik ?> = ''
        var typeCostum = "";
        var idCostum = "";
        var slugCostum = "";
        coInterface.showLoader('#content<?= $kunik ?>')
        if(contextData != null && typeof contextData != 'undefined'){
            idCostum = contextData.id;
            typeCostum = contextData.type;
            slugCostum = contextData.slug;
        } else if(typeof costum != 'undefined' && costum !== null){
            if(typeof costum.contextType !='undefined'){
                idCostum = costum.contextId;
                typeCostum = costum.contextType;
                slugCostum = costum.contextSlug;
            }
        }
        if(typeCostum != 'organizations') {
            var txt = "<h1>L'element doit etre un organizations utiliser ce kanban</h1>"
            $('.content<?=$kunik?>').empty().append(txt)
            return;
        }
        mylog.log(typeof contextType, "contextType")

        if(typeCostum === "organizations"){
            params<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
        }
        if(typeCostum === "projects"){
            params<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
        }

        loadMembers()
        function loadMembers() {
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    dataMember<?= $kunik ?> = data.results;
                },null,null, {async:false}
            )
            mylog.log(dataMember<?= $kunik ?>, "dataMember")
            var data<?= $kunik ?> = []
            $.map( 
                dataMember<?= $kunik ?> , function( valeur, keys ) {
                    let memberCard = {
                        _id : keys,
                        id : keys,
                        title : "<b>"+valeur.name+"</b>",
                        header : "memberof",
                        canEditHeader: false,
                        html : true,
                        name : valeur.name,
                        collection : valeur.collection,
                        slug : valeur.slug,
                        actions : [
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveMember'
                            }
                        ]
                    }
                    if(exists(valeur.links) && exists(valeur.links.memberOf)){
                        $.map(valeur.links.memberOf , function( val, key ) {
                            if(valeur.collection == 'citoyens') {
                                if(typeof val.isAdmin != 'undefined' && key == idCostum){
                                    let iconBadge = {
                                        'icon': '' , 
                                        'bstooltip' : { 
                                            'text' : 'Enlever le droit admin',
                                            'position' : 'left'
                                        }, 
                                        'badge' : '<span class="removeAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>' , 
                                        'action': 'onRemoveAdmin' 
                                    }
                                    memberCard.actions.push(iconBadge)
                                } else if(typeof val.isAdmin == 'undefined' && key == idCostum){
                                    let iconBadge = {
                                        'icon': '', 
                                        'bstooltip' : { 
                                            'text' : 'Ajouter comme admin',
                                            'position' : 'left'
                                        }, 
                                        'badge' : '<span class="btn-is-admin addAdmin color-red"><i class="fa fa-user-secret"> </i> Admin</span>', 
                                        'action': 'onAddAdmin' 
                                    }
                                    memberCard.actions.push(iconBadge)
                                }
                            } else if (valeur.collection == 'organizations' && key == idCostum) {
                                let iconBadge = {
                                    'icon': 'fa fa-group' , 
                                    'bstooltip' : { 
                                        'text' : 'Une organisation',
                                        'position' : 'left'
                                    }, 
                                    'badge' : 'Orga' , 
                                    'action': '' 
                                }
                                memberCard.actions.push(iconBadge)
                            }
                        })
                    }
                    data<?= $kunik ?>.push(memberCard) 
                    member<?= $kunik ?>.push(memberCard)
                    initMember<?= $kunik ?>.push(memberCard)
                }
            ); 
            
        }

        function filterKanban() {
            load<?=$kunik?> = false;
            let objet = {
                columns : ['memberof', 'listProjet', 'milestone', 'listRole', 'todo', 'totest', 'tracking', 'done'],
                attributes : {

                }
            }
            if($('#nameMember<?=$kunik?>').val() != '') {
                objet.card = $('#nameMember<?=$kunik?>').val()
            } 
            $('#kanban<?= $kunik ?>').kanban('filter', objet);
        }

        function show_action_detail_modal(__id) {
            return new Promise(function (__resolve) {
                const url = baseUrl + '/costum/project/action/request/action_detail_html';
                const post = {
                    id : __id,
                    connectedUser: userConnected._id.$id
                };
                ajaxPost(null, url, post, __resolve, null, 'text');
            });
        }
        // $('#typeMember<?=$kunik?>').on('change', filterKanban)
        $('#nameMember<?=$kunik?>').on('input', filterKanban)

        $('#project-<?= $kunik ?>').on('click', function(){
            $('#contributor-filter-container').html('')
            var my<?= $kunik ?> = ''
            var activeProjet<?= $kunik ?> = ''
            var activeMilestone<?= $kunik ?> = ''
            $('#loader<?= $kunik ?>').removeClass('hidden')
            $('#backdrop<?= $kunik ?>').removeClass('hidden')
            $('#projetBread').data('ide', '')
            
            var interv = setInterval(() => {
                $('#kanban<?= $kunik ?>').kanban('destroy')
                loadKanbanAllProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, '')
                
                clearInterval(interv);
                interv = null;    
            }, 50); 
        })
        $('#projetBread').on('click', function(){
            if($(this).hasClass('active-bread')) {
                $('#contributor-filter-container').html('')
                let id = $(this).data('ide');
                $('#loader<?= $kunik ?>').removeClass('hidden')
                $('#backdrop<?= $kunik ?>').removeClass('hidden')
                $('#projetBread').data('ide', '')
                
                var interv = setInterval(() => {
                    $('#kanban<?= $kunik ?>').kanban('destroy')
                    loadKanbanAllProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, id)
                    
                    clearInterval(interv);
                    interv = null;    
                }, 50);  
            }
        })
        function loadAllProjet() {
            if(typeof aapObj != 'undefined' && typeof aapObj.form != 'undefined' && aapObj.form != null && typeof aapObj.context != 'undefined') {
                const url = baseUrl + '/co2/aap/directoryproject/form/'+aapObj.form._id.$id+'/context/'+aapObj.context._id.$id;
                const post = {
                    'textPath' : 'answers.aapStep1.tags',
                    'searchType[]' : 'projects',
                    'tagsPath' : 'answers.aapStep1.tags',
                    'count' : true,
                    'countType[]' : 'projects',
                    'notSourceKey' : true,
                    'filters' : {
                        'form' : aapObj.form._id.$id,
                        'answers.aapStep1.titre' : { '$exists' : true },
                        'project.id' : { '$exists' : true }
                    },
                    'fediverse' : false
                }
                ajaxPost(
                    null,
                    url,
                    post,
                    function(data){
                        allProjet<?= $kunik ?> = data.results
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            loadKanbanAllProjet(data.results, member<?= $kunik ?>, '')
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);  
                    },null,null, {}
                )
            } else {
                const url = baseUrl + '/co2/search/globalautocompleteadmin/type/'+typeCostum+'/id/'+idCostum+'/canSee/true';
                const post = {
                    'searchType[]' : 'projects',
                    'count' : true,
                    'notSourceKey' : true,
                    'countType[]' : 'projects',
                    filters : {
                        '$or' : {

                        }
                    },
                    'fediverse' : false
                };
                post.filters['preferences.toBeValidated.'+slugCostum] = { '$exists' : false }
                post.filters['$or']['links.contributors.'+idCostum] = { '$exists' : true }
                post.filters['$or']['parent.'+idCostum] = { '$exists' : true }
                post.filters['links.contributors.'+idCostum] = { '$exists' : true }
                ajaxPost(
                    null,
                    url,
                    post,
                    function(data){
                        allProjet<?= $kunik ?> = data.results
                        console.log(allProjet<?= $kunik ?>, 'allProjetReq')
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            var ide = (typeof userConnected != 'undefined' ? userConnected._id.$id : '')
                            loadKanbanAllProjet(data.results, member<?= $kunik ?>, ide)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 5);  
                    },null,null, {}
                )
            }
        }
        loadAllProjet()

        function findMemberOnProject(idProject) {
            let contribId = []
            let dataContrib = []
            if (idProject != '') {
                $.map(allProjet<?= $kunik ?> , function( valeur, keys ) {
                    if (idProject == keys && typeof valeur.links != 'undefined' && typeof valeur.links.contributors != 'undefined') {
                        contribId = Object.keys(valeur.links.contributors)
                    }
                })
                if (contribId.length > 0) {
                    for(let i = 0; i < contribId.length; i++) {
                        $.map(dataMember<?= $kunik ?>, function(val, key) {
                            if(contribId[i] == key) {
                                let oneContrib = {
                                    id : key,
                                    name : val.name,
                                    image : typeof val.profilThumbImageUrl != 'undefined' ? baseUrl+val.profilThumbImageUrl : (typeof val.profilMediumImageUrl != 'undefined' ? baseUrl+val.profilMediumImageUrl : '<?php Yii::app()->getModule( "co2" )->assetsUrl."/images/thumbnail-default.jpg"?>')
                                }
                                dataContrib.push(oneContrib)
                            }
                        })
                    }
                }
            }

            return dataContrib;
        }

        function findMineActionOnProject(idMember){
            console.log(allProjet<?= $kunik ?>, 'allProjetFOn')
            $('#loader<?= $kunik ?>').removeClass('hidden')
            $('#backdrop<?= $kunik ?>').removeClass('hidden')
            $('#projetBread').data('ide', '')
            var dataContributors = findMemberOnProject(activeProjet<?= $kunik ?>);
            mylog.log(dataContributors, 'avatar')
            var contributorFilter = topContributorFilter('#contributor-filter-container');
            contributorFilter.build(dataContributors)
            contributorFilter.onFilter = function(){
                var value = this.selected === null ? '' : this.selected;
                findMineActionOnProject(value)
            }
            var interv = setInterval(() => {
                $('#kanban<?= $kunik ?>').kanban('destroy')
                findActionOnProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, idMember, activeProjet<?= $kunik ?>, 'all', activeMilestone<?= $kunik ?>)
                
                clearInterval(interv);
                interv = null;    
            }, 50);
        }

        function loadKanbanAllProjet(object, dataMemb, ide) {
            console.log(object, 'objectPro')
            load<?=$kunik?> = true;
            let titleHead = 'Gestion de projet de : '+slugCostum
            mylog.log($('#preview-kanban .titre-role').text() , 'title')
            $('#preview-kanban .titre-role').text(titleHead)
            let head = [
                { id: 'memberof', label: "<i class='fa fa-user'> </i> Les membres" , editable : false,
                    menus: [
                        { label: 'Envoyer une invitation', action: 'onInvite' }
                    ]
                },
                { id: 'listProjet', label: "<i class='fa fa-lightbulb-o'> </i> Les projets" , editable : false ,
                    menus: [
                        { label: 'Ajouter un nouveau projet', action: 'onAddProjet' }
                    ]
                }
            ]
            let data = []
            mylog.log(dataMemb, 'dataMemb')
            data.push(...dataMemb)
            mylog.log(data, 'data af push')
            let listProjet = []
            let listMiles = []
            $.map(object , function(valeur , keys) {
                if (ide != '' && typeof valeur.links != 'undefined' && typeof valeur.links.contributors != 'undefined') {
                    $.map(valeur.links.contributors, function(v,k) {
                        if(k == ide) {
                            if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                                let mileProjet = {
                                    id: "mile"+valeur.slug , label: "milestone - "+valeur.slug , editable : false ,
                                    menus: []
                                }
                                head.push(mileProjet); 
                                listMiles.push(mileProjet); 
                                for(let i = 0; i < valeur.oceco.milestones.length; i++) { 
                                    let miles = {
                                        _id : valeur.oceco.milestones[i].milestoneId,
                                        id : valeur.oceco.milestones[i].milestoneId+"<?= $kunik ?>",
                                        instanceIdentity : valeur.oceco.milestones[i].milestoneId,
                                        title : '<b>'+valeur.oceco.milestones[i].name+'</b>',
                                        name : valeur.oceco.milestones[i].name,
                                        header : "mile"+valeur.slug,
                                        collection : valeur.collection,
                                        slug : valeur.slug,
                                        html : true,
                                        parentId : keys,
                                        canMoveCard: false,
                                        actions : [
                                            {
                                                'icon': 'fa fa-trash', 
                                                'bstooltip' : { 
                                                    'text' : 'Supprimer le milestone',
                                                    'position' : 'left'
                                                }, 
                                                // 'action': 'onRemoveMilestone'
                                            }
                                        ]
                                    }
                                    data.push(miles)
                                }
                            }

                            let colProjet = {
                                id: keys , label: valeur.name , editable : false , _id : keys, name: valeur.name, slug: valeur.slug,
                                menus: [
                                    { label: 'Ajouter une action', action: 'onAddAction' }
                                ]
                            }
                            head.push(colProjet);
                            listProjet.push(colProjet);

                            let dataMember = {}
                            let params = {
                                searchType : ["citoyens","organizations"],
                                count : true,
                                countType : ["citoyens","organizations"],
                                notSourceKey : true,
                                filters : {},
                                indexStep : 0
                            }
                            params.filters["links.projects."+keys] = {'$exists':true};

                            ajaxPost(
                                null,
                                baseUrl+"/" + moduleId + "/search/globalautocompleteadmin/type/projects/id/"+keys+"/canSee/true",
                                params,
                                function(data){
                                    dataMember = data.results;
                                },null,null, {async:false}
                            )

                            $.map(dataMember , function(val , key) {
                                let member = {
                                    _id : key,
                                    id : key+"<?= $kunik ?>",
                                    instanceIdentity : key,
                                    title : '<b>'+val.name+'</b>',
                                    name : val.name,
                                    header : keys,
                                    collection : val.collection,
                                    slug : val.slug,
                                    html : true,
                                    canMoveCard: false,
                                    actions : [
                                        {
                                            'icon': typeof val.role != 'undefined' && val.role == 'admin' ? 'fa fa-industry' : 'fa fa-trash', 
                                            'bstooltip' : { 
                                                'text' : typeof val.role != 'undefined' && val.role == 'admin' ? 'Administrateur' : 'Enlever cet-te contributeur du projet',
                                                'position' : 'left'
                                            }, 
                                            'badge' : typeof val.role != 'undefined' && val.role == 'admin' ? 'Cooperative' : '',
                                            'action': typeof val.role != 'undefined' && val.role == 'admin' ? '' : 'onDelete'
                                        }
                                    ]
                                }
                                data.push(member)
                            })

                            
                            
                            let projet = {
                                _id : keys,
                                id : keys+"<?= $kunik ?>",
                                instanceIdentity : keys,
                                title : '<b>'+valeur.name+'</b>',
                                name : valeur.name,
                                header : "listProjet",
                                collection : valeur.collection,
                                slug : valeur.slug,
                                html : true,
                                canMoveCard: false,
                                actions : [
                                    {
                                        'icon': 'fa fa-trash', 
                                        'bstooltip' : { 
                                            'text' : 'Supprimer le projet',
                                            'position' : 'left'
                                        }, 
                                        'action': 'onRemoveProjet'
                                    },
                                    {
                                        'icon': 'fa fa-tasks', 
                                        'bstooltip' : { 
                                            'text' : 'Afficher les actions',
                                            'position' : 'left'
                                        }, 
                                        'badge' : 'Actions',
                                        'action': 'onAfficheAction'
                                    }
                                ]
                            }
                            if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                                let act = {
                                    'icon': 'fa fa-flag', 
                                    'bstooltip' : { 
                                        'text' : 'Les milestones',
                                        'position' : 'left'
                                    },
                                    'badge' : 'Jalons', 
                                    'action': 'onMilestone'
                                }
                                projet.actions.splice(1, 0, act)
                            }
                            data.push(projet)
                        }
                    })
                    $('#projetBread').data('ide', ide)
                    $('#project-<?= $kunik ?>').removeClass('hidden')
                } else if (ide == ''){
                    $('#projetBread').data('ide', '')
                    $('#project-<?= $kunik ?>').addClass('hidden')
                    if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                        let mileProjet = {
                            id: "mile"+valeur.slug , label: "milestone - "+valeur.slug , editable : false ,
                            menus: []
                        }
                        head.push(mileProjet); 
                        listMiles.push(mileProjet); 
                        for(let i = 0; i < valeur.oceco.milestones.length; i++) {
                            let miles = {
                                _id : valeur.oceco.milestones[i].milestoneId,
                                id : valeur.oceco.milestones[i].milestoneId+"<?= $kunik ?>",
                                instanceIdentity : valeur.oceco.milestones[i].milestoneId,
                                title : '<b>'+valeur.oceco.milestones[i].name+'</b>',
                                name : valeur.oceco.milestones[i].name,
                                header : "mile"+valeur.slug,
                                collection : valeur.collection,
                                slug : valeur.slug,
                                html : true,
                                parentId : keys,
                                canMoveCard: false,
                                actions : [
                                    {
                                        'icon': 'fa fa-trash', 
                                        'bstooltip' : { 
                                            'text' : 'Supprimer le milestone',
                                            'position' : 'left'
                                        }, 
                                        // 'action': 'onRemoveMilestone'
                                    }
                                ]
                            }
                            data.push(miles)
                        }
                    }
                    let colProjet = {
                        id: keys , label: valeur.name , editable : false , _id : keys, name: valeur.name, slug: valeur.slug,
                        menus: [
                            { label: 'Ajouter une action', action: 'onAddAction' }
                        ]
                    }
                    head.push(colProjet);
                    listProjet.push(colProjet);

                    let dataMember = {}
                    let params = {
                        searchType : ["citoyens","organizations"],
                        count : true,
                        countType : ["citoyens","organizations"],
                        notSourceKey : true,
                        filters : {},
                        indexStep : 0
                    }
                    params.filters["links.projects."+keys] = {'$exists':true};

                    ajaxPost(
                        null,
                        baseUrl+"/" + moduleId + "/search/globalautocompleteadmin/type/projects/id/"+keys+"/canSee/true",
                        params,
                        function(data){
                            dataMember = data.results;
                            mylog.log(dataMember, 'membre')
                        },null,null, {async:false}
                    )

                    $.map(dataMember , function(val , key) {
                        let member = {
                            _id : key,
                            id : key+"<?= $kunik ?>",
                            instanceIdentity : key,
                            title : '<b>'+val.name+'</b>',
                            name : val.name,
                            header : keys,
                            collection : val.collection,
                            slug : val.slug,
                            html : true,
                            canMoveCard: false,
                            actions : [
                                {
                                    'icon': typeof val.role != 'undefined' && val.role == 'admin' ? 'fa fa-industry' : 'fa fa-trash', 
                                    'bstooltip' : { 
                                        'text' : typeof val.role != 'undefined' && val.role == 'admin' ? 'Administrateur' : 'Enlever cet-te contributeur du projet',
                                        'position' : 'left'
                                    }, 
                                    'badge' : typeof val.role != 'undefined' && val.role == 'admin' ? 'Cooperative' : '',
                                    'action': typeof val.role != 'undefined' && val.role == 'admin' ? '' : 'onDelete'
                                }
                            ]
                        }
                        data.push(member)
                    })

                    let projet = {
                        _id : keys,
                        id : keys+"<?= $kunik ?>",
                        instanceIdentity : keys,
                        title : '<b>'+valeur.name+'</b>',
                        name : valeur.name,
                        header : "listProjet",
                        collection : valeur.collection,
                        slug : valeur.slug,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {
                                'icon': 'fa fa-trash', 
                                'bstooltip' : { 
                                    'text' : 'Supprimer le projet',
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveProjet'
                            },
                            {
                                'icon': 'fa fa-tasks', 
                                'bstooltip' : { 
                                    'text' : 'Afficher les actions',
                                    'position' : 'left'
                                }, 
                                'badge' : 'Actions',
                                'action': 'onAfficheAction'
                            }
                        ]
                    }
                    if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                        let act = {
                            'icon': 'fa fa-flag', 
                            'bstooltip' : { 
                                'text' : 'Les milestones',
                                'position' : 'left'
                            },
                            'badge' : 'Jalons', 
                            'action': 'onMilestone'
                        }
                        projet.actions.splice(1, 0, act)
                    }
                    data.push(projet)
                }
            })

            var kanban<?= $kunik ?> = $('#kanban<?= $kunik ?>').kanban({
                headers : head,
                data : data,
                editable: false,
                language : mainLanguage,
                canAddCard: false,
                editable : false,
                canAddColumn: false,
                canEditCard: false,
                canEditHeader: false,
                canMoveCard: true,
                canMoveColumn: true,
                readonlyHeaders: ['memberof','listProjet'],
                copyWhenDragFrom: ['memberof'],
                endpoint : `${baseUrl}/plugins/kanban/`,
                defaultColumnMenus: [''],
                showContributors : false,
                onRenderDone(){ 
                    $('#backdrop<?= $kunik ?>').addClass('hidden')
                    $('#loader<?= $kunik ?>').addClass('hidden')
                    mylog.log(listProjet, load<?=$kunik?>, 'projetct')
                    $('#projetBread').addClass('hide-bread')
                    $('#projetBread').removeClass('active-bread')
                    $('#projetBread').removeClass('hidden')
                    $('#actionBread').addClass('hidden')
                    if(ide != '') {
                        $('.kanban-list-card-detail[data-id = '+ide+']').addClass('active-member')
                    }
                    $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                        'display' : 'none'
                    })
                    $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                        'display' : 'none'
                    })
                    $('#kanban-wrapper-memberof .kanban-list-content').css({
                        'background-color' : 'gray'
                    })
                    $('#kanban-wrapper-memberof .column-header-text').css({
                        'color' : '#fff'
                    })
                    $('#kanban-wrapper-listProjet .kanban-list-content').css({
                        'background-color' : 'rgb(183, 183, 183)'
                    })
                    
                    if(load<?=$kunik?> == true) {
                        $('#kanban-wrapper-listProjet .kanban-list-header').css({
                            'cursor' : 'pointer'
                        })
                        for (let i = 0; i < listProjet.length; i++) {
                            $('#kanban-wrapper-'+listProjet[i].id).addClass('hidden')
                            $('#kanban-wrapper-'+listProjet[i].id).addClass('roles-list')
                            // $('#kanban-wrapper-'+listProjet[i].id+' .kanban-action-dropdown').css({
                            //     'display' : 'none'
                            // })
                            $('.kanban-list-card-detail').css("background-color", "#fff !important");
                        }
                        for (let i = 0; i < listMiles.length; i++) {
                            $('#kanban-wrapper-'+listMiles[i].id).addClass('hidden')
                            $('#kanban-wrapper-'+listMiles[i].id).addClass('roles-list')
                            $('#kanban-wrapper-'+listMiles[i].id+' .kanban-action-dropdown').css({
                                'display' : 'none'
                            })
                            $('.kanban-list-card-detail').css("background-color", "#fff !important");
                        }
                    }
                },
                onAfficheAction(clickedData){ 
                    activeProjet<?= $kunik ?> = clickedData._id
                    activeMilestone<?= $kunik ?> = ''
                    $('#loader<?= $kunik ?>').removeClass('hidden')
                    $('#backdrop<?= $kunik ?>').removeClass('hidden')
                    $('#projetBread').data('ide', '')
                    var dataContributors = findMemberOnProject(clickedData._id)
                    mylog.log(dataContributors, 'avatar')
                    var contributorFilter = topContributorFilter('#contributor-filter-container');
                    contributorFilter.build(dataContributors)
                    contributorFilter.onFilter = function(){
                        var value = this.selected === null ? '' : this.selected;
                        findMineActionOnProject(value)
                    }
                    var interv = setInterval(() => {
                        $('#kanban<?= $kunik ?>').kanban('destroy')
                        findActionOnProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, ide, clickedData._id,'all', '')
                        
                        clearInterval(interv);
                        interv = null;    
                    }, 50);
                },
                onAddAction(clickedData){
                    let path2ValueOption = {}
                    path2ValueOption.url = baseUrl
                    const url = baseUrl + '/costum/project/project/request/room';
                    const post = {
                        project : clickedData._id,
                        connectedUser: userConnected._id.$id
                    };
                    let room = '';
                    ajaxPost(null, url, post, function (result) {
                        room = result;
                        mylog.log(clickedData,room, 'milestone')
                    },null,null, {async:false});
                    const default_finder = {};

                    default_finder[userConnected['_id']['$id']] = {
                        type : 'citoyens',
                        name : userConnected['name']
                    };

                    dyFObj.openForm({
                        jsonSchema : {
                            title : 'Ajouter une action',
                            properties : {
                                action_name : dyFInputs.name('action'),
                                action_parentType : dyFInputs.inputHidden("projects"),
                                action_parentId : dyFInputs.inputHidden(clickedData._id),
                                action_creator : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                action_credits : dyFInputs.inputHidden(1),
                                action_idUserAuthor : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                action_idParentRoom : dyFInputs.inputHidden(room),
                                action_tags : {
                                    inputType : 'tags',
                                    label : trad.tags
                                },
                                action_contributors : {
                                    inputType : 'finder',
                                    label : 'Assigné à qui ?',
                                    multiple : true,
                                    rules : {
                                        lengthMin : [1, 'contributors'],
                                        required : true
                                    },
                                    initType : ['citoyens'],
                                    values : default_finder,
                                    initBySearch : true,
                                    initMe : true,
                                    initContext : false,
                                    initContacts : false,
                                    openSearch : true
                                },
                                action_urls : {
                                    label : 'Urls',
                                    inputType : 'array',
                                    value : [
                                        window.location.toString()
                                    ]
                                },
                                action_status : dyFInputs.inputHidden('todo')
                            },
                            save : function (__form) {
                                var value = {
                                    urls : getArray('.action_urlsarray')
                                };
                                var contributors = {};
                                $.each(Object.keys(__form['action_contributors']), function (__, __key) {
                                    contributors[__key] = {
                                        type : 'citoyens'
                                    };
                                });
                                $.each(['scope', 'collection', 'action_contributors', 'action_urls[]'], function (__, __key) {
                                    delete __form[__key];
                                });

                                $.each(Object.keys(__form), function (__, __name) {
                                    var name = __name.substring(7);
                                    if (name === 'tags') value[name] = __form[__name].split(',');
                                    else value[name] = __form[__name];
                                });
                                var params = {
                                    collection : 'actions',
                                    value
                                };

                                dataHelper.path2Value(params, function (__data) {
                                    if (__data['result'] && __data['result'] === true) {
                                        var action_id = __data['saved']['id'];

                                        // Envoyer une notification rocket chat pour la nouvelle tâche
                                        var url = baseUrl + '/survey/answer/rcnotification/action/newaction';
                                        var post = {
                                            projectname : clickedData.name,
                                            actname : value['name'],
                                            channelChat : clickedData.slug,
                                        };
                                        ajaxPost(null, url, post);

                                        // Enregistrer les contributeurs
                                        dataHelper.path2Value({
                                            id : action_id,
                                            collection : 'actions',
                                            path : 'links.contributors',
                                            value : contributors
                                        }, function (__data) {
                                            if (__data['result'] && __data['result'] === true) {
                                                toastr.success(__data.msg);
                                                dyFObj.closeForm();
                                                rocket_chat_contribution(action_id, Object.keys(contributors));
                                            }
                                        }, path2ValueOption);
                                    }
                                }, path2ValueOption);
                            }
                        }
                    }, null, null, null, null, {
                        type : 'bootbox'
                    });
                    
                },
                onMilestone(clickedData){
                    mylog.log(clickedData, 'milestone')
                    if($('#kanban-wrapper-mile'+clickedData.slug).hasClass('hidden')) {
                        $('#kanban-wrapper-mile'+clickedData.slug).removeClass('hidden');
                        $('#kanban-wrapper-mile'+clickedData.slug).addClass('visible');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('active-mile')
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                            "color" : "#172b4d"
                        });
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('activer')
                    } else {
                        $('#kanban-wrapper-mile'+clickedData.slug).addClass('hidden');
                        $('#kanban-wrapper-mile'+clickedData.slug).removeClass('visible');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('active-mile');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                            "color" : "#172b4d"
                        });
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('activer')
                    }
                },
                onCardClick(clickedData, style) {
                    load<?=$kunik?> == false
                    if(clickedData.header == 'memberof') {
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        $('#projetBread').data('ide', '')
                        
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            loadKanbanAllProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, clickedData._id)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);
                    } else if(clickedData.header == 'listProjet') {
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').popover('hide')
                        if($('#kanban-wrapper-'+clickedData._id).hasClass('hidden')) {
                            $('#kanban-wrapper-'+clickedData._id).removeClass('hidden');
                            $('#kanban-wrapper-'+clickedData._id).addClass('visible');
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('active-role')
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                                "color" : "#172b4d"
                            });
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('activer')
                        } else {
                            $('#kanban-wrapper-'+clickedData._id).addClass('hidden');
                            $('#kanban-wrapper-'+clickedData._id).removeClass('visible');
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('active-role');
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                                "color" : "#172b4d"
                            });
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('activer')
                        } 
                    } else {
                        activeMilestone<?= $kunik ?> = clickedData._id
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        $('#projetBread').data('ide', '')
                        var dataContributors = findMemberOnProject(clickedData.parentId)
                        mylog.log(dataContributors, 'avatar')
                        var contributorFilter = topContributorFilter('#contributor-filter-container');
                        contributorFilter.build(dataContributors)
                        contributorFilter.onFilter = function(){
                            var value = this.selected === null ? '' : this.selected;
                            findMineActionOnProject(value)
                        }
                        
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            findActionOnProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, ide, clickedData.parentId,'all', clickedData._id)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);
                    }
                },
                onCardDrop(dropped, dom) {
                    load<?=$kunik?> = false
                    mylog.log(dropped, load<?=$kunik?>)
                    
                    var copy = $.extend({}, dropped.data);
                    copy.canMoveCard = false;
                    copy.actions = [
                        { icon: 'fa fa-trash', action: 'onDelete'}
                    ]
                    kanban<?= $kunik ?>.kanban('setData', { column: copy.header, id: copy.id, data: copy});
                    if(dropped.origin != dropped.target) {
                        let params = {
                            'parentId' : dropped.target,
                            'parentType' : 'projects',
                            'listInvite' : {
                                'citoyens' : {
                                    
                                }
                            } 
                        }
                        params['listInvite']['citoyens'][dropped.data._id] = {
                            'name' : dropped.data.name
                        }
                        ajaxPost(
                            null,
                            baseUrl+"/co2/link/multiconnect",
                            params,
                            function(result){
                                toastr.success(dropped.data.slug+' est ajouter dans le projet', {
                                    closeButton : true,
                                    preventDuplicates : true,
                                    timeOut : 3000
                                });
                            },null,null, {}
                        )
                    }
                },
                onAddProjet(action){
                    mylog.log('hita')
                    var typeForm = 'project'
                    var commonElObj<?= $kunik?> = {
                        "beforeBuild":{
                            "properties" : { 
                            }
                        },
                        "onload" : {
                            "actions" : {
                                "src" : { 
                                    "infocustom" : "Remplir le champ"
                                }, 
                            }
                        }
                    };   
                    commonElObj<?= $kunik?>.afterSave = function(data){
                        dyFObj.commonAfterSave(data, function(){
                            $("#ajax-modal").modal('hide');
                            dyFObj.closeForm();
                            //urlCtrl.loadByHash(location.hash);
                        });
                    }
                    dyFObj.openForm(typeForm, null, null, null, commonElObj<?= $kunik?>);
                    // if(contextData && contextData.type && contextData.id ){
                    //     dyFObj.openForm(typeForm,"sub");
                    // }
                    // else{
                    //     dyFObj.openForm(typeForm);
                    // }
                },
                onDelete(action, dom){
                    mylog.log(action, 'delete');
                    load<?=$kunik?> = false
                    links.disconnect(
                        'projects',
                        action.header,
                        action._id,
                        action.collection,
                        'contributors',
                        function () {
                            load<?=$kunik?> = false
                            var index<?= $kunik ?> = kanban<?= $kunik ?>.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(dom)
                            kanban<?= $kunik ?>.kanban('deleteData', { column: action.header, id:action.id,  index: index<?= $kunik ?> });
                        }
                    );
                },
                onRemoveProjet(action, dom) {
                    mylog.log(action, 'delete');
                    load<?=$kunik?> = false
                    links.disconnect(
                        typeCostum,
                        idCostum,
                        action._id,
                        action.collection,
                        'projects',
                        function () {
                            load<?=$kunik?> = false
                            var index<?= $kunik ?> = kanban<?= $kunik ?>.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(dom)
                            kanban<?= $kunik ?>.kanban('deleteData', { column: action.header, id:action.id,  index: index<?= $kunik ?> });
                        }
                    );
                },
                onRemoveAdmin(data){
                    links.updateadminlink(
                        typeCostum,
                        idCostum,
                        data.instanceIdentity,
                        'citoyens',
                        (typeCostum == 'organizations' ? 'members' : 'contributors'),
                        false,
                        data.name,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .removeAdmin').addClass('btn-is-admin')
                        }
                    )
                   
                },
                onAddAdmin(data){
                    links.connect(
                        typeCostum, 
                        idCostum, 
                        data.instanceIdentity, 
                        'citoyens', 
                        'admin', 
                        '', 
                        true,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .addAdmin').removeClass('btn-is-admin')
                        }
                    )
                },
                onInvite(data){
                    smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/'+'element/invite/type/'+typeCostum+'/id/'+idCostum);
                },
                onRemoveMember(data, dom){
                    mylog.log(data.collection, 'collection')
                    var memb<?= $kunik ?> = {
                        childId : data.instanceIdentity,
                        childType : data.collection,
                        parentType : typeCostum,
                        parentId : idCostum,
                        connectType : typeCostum == 'organizations' ? 'members' : 'contributors'
                    }
                    bootbox.confirm(trad.areyousuretodelete, function(result) {
                        if (!result) {
                            return;
                        } else { 
                            ajaxPost(
                                null,
                                baseUrl+"/" + moduleId + "/link/disconnect",
                                memb<?= $kunik ?>,
                                function(donne){
                                    let index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                                    kanbanDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.id,  index: index<?= $kunik ?> });
                                    toastr.success("Membre supprimer");
                                    
                                },null,null, {}
                            )
                        }
                    })
                    
                },
            });
        }


        function findActionOnProjet(object, dataMemb, ide, projetId, linkAction, milesId) {
            load<?=$kunik?> = true;
            mylog.log(ide ,'ide')
            my<?= $kunik ?> = (ide == '' ? my<?= $kunik ?> : ide)
            let data = []
            mylog.log(dataMemb, 'dataMemb')
            data.push(...dataMemb)
            mylog.log(data, 'data af push')
            for(let i = 0 ; i < data.length ; i++) {
                data[i].canMoveCard = false
            }
            var dataAction = []
            var listMiles = []
            let head = [
                { id: 'memberof', label: "<i class='fa fa-user'> </i> Les membres" , editable : false,
                    menus: [
                        { label: 'Envoyer une invitation', action: 'onInvite' }
                    ]
                },
                { id: 'listProjet', label: "<i class='fa fa-lightbulb-o'> </i> Les projets" , editable : false ,
                    menus: [
                        { label: 'Ajouter un nouveau projet', action: 'onAddProjet' }
                    ]
                },
                { id: 'todo', label: 'A faire' , editable : false},
                { id: 'tracking', label: 'En cours' , editable : false },
                { id: 'totest', label: 'A tester' , editable : false },
                { id: 'done', label: 'Terminer' , editable : false }
            ]
            let link = ''
            $.map(object , function(valeur , keys) {
                if (ide != '' && typeof valeur.links != 'undefined' && typeof valeur.links.contributors != 'undefined') {
                    link = 'mine'
                    $.map(valeur.links.contributors, function(v,k) {
                        if(k == ide) {
                            if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                                let mileProjet = {
                                    id: "mile"+valeur.slug , label: "milestone - "+valeur.slug , editable : false ,
                                    menus: []
                                }
                                // head.push(mileProjet);
                                head.splice(2, 0, mileProjet)
                                listMiles.push(mileProjet); 
                                for(let i = 0; i < valeur.oceco.milestones.length; i++) { 
                                    let miles = {
                                        _id : valeur.oceco.milestones[i].milestoneId,
                                        id : valeur.oceco.milestones[i].milestoneId+"<?= $kunik ?>",
                                        instanceIdentity : valeur.oceco.milestones[i].milestoneId,
                                        title : '<b>'+valeur.oceco.milestones[i].name+'</b>',
                                        name : valeur.oceco.milestones[i].name,
                                        header : "mile"+valeur.slug,
                                        collection : valeur.collection,
                                        slug : valeur.slug,
                                        html : true,
                                        parentId : keys,
                                        canMoveCard: false,
                                        actions : [
                                            {
                                                'icon': 'fa fa-trash', 
                                                'bstooltip' : { 
                                                    'text' : 'Supprimer le milestone',
                                                    'position' : 'left'
                                                }, 
                                                // 'action': 'onRemoveMilestone'
                                            }
                                        ]
                                    }
                                    data.push(miles)
                                }
                            }
                            let projet = {
                                 _id : keys,
                                id : keys+"<?= $kunik ?>",
                                instanceIdentity : keys,
                                title : '<b>'+valeur.name+'</b>',
                                name : valeur.name,
                                header : "listProjet",
                                collection : valeur.collection,
                                slug : valeur.slug,
                                html : true,
                                canMoveCard: false,
                                actions : [
                                    {
                                        'icon': 'fa fa-trash', 
                                        'bstooltip' : { 
                                            'text' : 'Supprimer le projet',
                                            'position' : 'left'
                                        }, 
                                        'action': 'onRemoveProjet'
                                    },
                                    {
                                        'icon': 'fa fa-plus-circle', 
                                        'bstooltip' : { 
                                            'text' : 'Ajouter une action',
                                            'position' : 'left'
                                        }, 
                                        'badge' : 'Action',
                                        'action': 'onAddAction'
                                    }
                                ]
                            }
                            data.push(projet)
                            if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                                let act = {
                                    'icon': 'fa fa-flag', 
                                    'bstooltip' : { 
                                        'text' : 'Les milestones',
                                        'position' : 'left'
                                    },
                                    'badge' : 'Jalons', 
                                    'action': 'onMilestone'
                                }
                                projet.actions.splice(1, 0, act)
                            }
                            data.push(projet)
                        }
                    })
                    $('#projetBread').data('ide', ide)
                } else if(ide == '') {
                    $('#projetBread').data('ide', '')
                    link = 'all'
                    if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                        let mileProjet = {
                            id: "mile"+valeur.slug , label: "milestone - "+valeur.slug , editable : false ,
                            menus: []
                        }
                        head.splice(2, 0, mileProjet)
                        // head.push(mileProjet); 
                        listMiles.push(mileProjet); 
                        for(let i = 0; i < valeur.oceco.milestones.length; i++) { 
                            let miles = {
                                _id : valeur.oceco.milestones[i].milestoneId,
                                id : valeur.oceco.milestones[i].milestoneId+"<?= $kunik ?>",
                                instanceIdentity : valeur.oceco.milestones[i].milestoneId,
                                title : '<b>'+valeur.oceco.milestones[i].name+'</b>',
                                name : valeur.oceco.milestones[i].name,
                                header : "mile"+valeur.slug,
                                collection : valeur.collection,
                                slug : valeur.slug,
                                html : true,
                                parentId : keys,
                                canMoveCard: false,
                                actions : [
                                    {
                                        'icon': 'fa fa-trash', 
                                        'bstooltip' : { 
                                            'text' : 'Supprimer le milestone',
                                            'position' : 'left'
                                        }, 
                                        // 'action': 'onRemoveMilestone'
                                    }
                                ]
                            }
                            data.push(miles)
                        }
                    }
                    let projet = {
                        _id : keys,
                        id : keys+"<?= $kunik ?>",
                        instanceIdentity : keys,
                        title : '<b>'+valeur.name+'</b>',
                        name : valeur.name,
                        header : "listProjet",
                        collection : valeur.collection,
                        slug : valeur.slug,
                        html : true,
                        canMoveCard: false,
                        actions : [
                            {
                                'icon': 'fa fa-trash', 
                                'bstooltip' : { 
                                    'text' : 'Supprimer le projet',
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveProjet'
                            },
                            {
                                'icon': 'fa fa-plus-circle', 
                                'bstooltip' : { 
                                    'text' : 'Ajouter une action',
                                    'position' : 'left'
                                }, 
                                'badge' : 'Action',
                                'action': 'onAddAction'
                            }
                        ]
                    }
                    data.push(projet)
                    if(typeof valeur.oceco != 'undefined' && typeof valeur.oceco.milestones != 'undefined') {
                        let act = {
                            'icon': 'fa fa-flag', 
                            'bstooltip' : { 
                                'text' : 'Les milestones',
                                'position' : 'left'
                            },
                            'badge' : 'Jalons', 
                            'action': 'onMilestone'
                        }
                        projet.actions.splice(1, 0, act)
                    }
                    data.push(projet)
                }
            })
            let paramsKanbanAction = {     
                "connectedUser" : ide,
                "filters[urls][$in][]" : location.href,
                "filters[parentId]" : projetId
            };
            if(milesId != '') {
                paramsKanbanAction["filters"] = { 'milestone.milestoneId' : milesId }
            }
            ajaxPost(
                null,
                baseUrl+"/costum/project/action/request/actions/scope/"+link,
                paramsKanbanAction,
                function(result){
                    mylog.log(result, 'result')
                    dataAction = result;
                },null,null, {async:false}
            )
            for(let i = 0 ; i < dataAction.length ; i++) {
                if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "done") {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "done",
                        html : true,
                        canMoveCard: true,
                        actions : [
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    data.push(actionsItem)
                } 
                else if (typeof dataAction[i].tags != "undefined" && dataAction[i].tags.indexOf('totest') > -1) {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "totest",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    data.push(actionsItem)
                } 
                else if (typeof dataAction[i].tracking != "undefined" && dataAction[i].tracking == true) {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "tracking",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    data.push(actionsItem)
                } 
                
                else if (typeof dataAction[i].status != "undefined" && dataAction[i].status == "todo") {
                    let actionsItem = {
                        _id : dataAction[i].id,
                        id : dataAction[i].id+i,
                        instanceIdentity : dataAction[i].id,
                        title : '<b>'+dataAction[i].name+'</b>',
                        name : dataAction[i].name,
                        tags : (typeof dataAction[i].tags != 'undefined' ? dataAction[i].tags : []),
                        header : "todo",
                        html : true,
                        canMoveCard: true,
                        actions : [ 
                            {
                                'icon': 'fa fa-trash' ,
                                'bstooltip' : {
                                    'text' : 'Supprimer le membre de la communauter', 
                                    'position' : 'left'
                                }, 
                                'action': 'onRemoveAction'
                            }
                        ]
                    }
                    data.push(actionsItem)
                }
            }
            var kanban<?= $kunik ?> = $('#kanban<?= $kunik ?>').kanban({
                headers : head,
                data : data,
                editable: false,
                language : mainLanguage,
                canAddCard: true,
                editable : false,
                canAddColumn: false,
                canEditCard: false,
                canEditHeader: false,
                canMoveCard: true,
                canMoveColumn: true,
                readonlyHeaders: ['memberof','listProjet'],
                endpoint : `${baseUrl}/plugins/kanban/`,
                defaultColumnMenus: [''],
                onRenderDone(){ 
                    $('#backdrop<?= $kunik ?>').addClass('hidden')
                    $('#loader<?= $kunik ?>').addClass('hidden')
                    $('#projetBread').removeClass('hide-bread')
                    $('#projetBread').addClass('active-bread')
                    $('#projetBread').removeClass('hidden')
                    $('#actionBread').removeClass('hidden')
                    $('#actionBread').addClass('hide-bread')
                    $('#kanban-wrapper-memberof').addClass('hidden')
                    $('#kanban-wrapper-todo .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-tracking .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-totest .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-done .kanban-action-dropdown').addClass('hidden')
                    $('#kanban-wrapper-listProjet .kanban-composer-container').html('')
                    $('#kanban-wrapper-milestone .kanban-composer-container').html('')
                    if(ide != '') {
                        $('.kanban-list-card-detail[data-id = '+ide+']').addClass('active-member')
                        $('.contributor-project-filter ul.dropdown-menu li[data-id = '+ide+'] .avatar-container').addClass('active')
                    } 
                    $('.kanban-list-card-detail[data-id = '+milesId+'<?= $kunik ?>]').addClass('active-mile')
                    $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                        'display' : 'none'
                    })
                    $('#kanban-wrapper-memberof .kanban-list-card-action').css({
                            'display' : 'none'
                    })
                    $('#kanban-wrapper-memberof .kanban-list-content').css({
                        'background-color' : 'gray'
                    })
                    $('#kanban-wrapper-memberof .column-header-text').css({
                        'color' : '#fff'
                    })
                    $('#kanban-wrapper-listProjet .kanban-list-content').css({
                        'background-color' : 'rgb(183, 183, 183)'
                    })
                    if(load<?=$kunik?> == true) {
                        $('.kanban-list-card-detail[data-id = '+projetId+'<?= $kunik ?>]').addClass('active-role')
                        $('#kanban-wrapper-listProjet .kanban-list-header').css({
                            'cursor' : 'pointer'
                        })
                        $.map(data, function(val, key){
                            $('#kanban-wrapper-'+key).addClass('hidden')
                            $('#kanban-wrapper-'+key).addClass('roles-list')
                            $('#kanban-wrapper-'+key+' .kanban-action-dropdown').css({
                                'display' : 'none'
                            })
                            $('.kanban-list-card-detail').css("background-color", "#fff !important");
                        }) 
                        for (let i = 0; i < listMiles.length; i++) {
                            $('#kanban-wrapper-'+listMiles[i].id).addClass('hidden')
                            $('#kanban-wrapper-'+listMiles[i].id).addClass('roles-list')
                            $('#kanban-wrapper-'+listMiles[i].id+' .kanban-action-dropdown').css({
                                'display' : 'none'
                            })
                            $('.kanban-list-card-detail').css("background-color", "#fff !important");
                        }                           
                    }
                },
                onCardClick(clickedData, style) {
                    if(clickedData.header == 'memberof') {
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        $('#projetBread').data('ide', '')
                        
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            loadKanbanAllProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, clickedData._id)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);
                    } else if(clickedData.header == 'listProjet') {
                        load<?=$kunik?> = true
                        if($('.kanban-list-card-detail[data-id = '+clickedData.id+']').hasClass('active-role')) { 
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('active-role')
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('load')
                            $('#kanban-wrapper-todo').addClass('hidden')
                            $('#kanban-wrapper-tracking').addClass('hidden')
                            $('#kanban-wrapper-totest').addClass('hidden')
                            $('#kanban-wrapper-done').addClass('hidden')
                        } else if($('.kanban-list-card-detail[data-id = '+clickedData.id+']').hasClass('load')){
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('active-role')
                            $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('load')
                            $('#kanban-wrapper-todo').removeClass('hidden')
                            $('#kanban-wrapper-tracking').removeClass('hidden')
                            $('#kanban-wrapper-totest').removeClass('hidden')
                            $('#kanban-wrapper-done').removeClass('hidden')
                        } else {
                            activeProjet<?= $kunik ?> = clickedData._id
                            activeMilestone<?= $kunik ?> = ''
                            $('#loader<?= $kunik ?>').removeClass('hidden')
                            $('#backdrop<?= $kunik ?>').removeClass('hidden')
                            $('#projetBread').data('ide', '')
                            var dataContributors = findMemberOnProject(clickedData._id)
                            mylog.log(dataContributors, 'avatar')
                            var contributorFilter = topContributorFilter('#contributor-filter-container');
                            contributorFilter.build(dataContributors)
                            contributorFilter.onFilter = function(){
                                var value = this.selected === null ? '' : this.selected;
                                findMineActionOnProject(value)
                            }
                            
                            var interv = setInterval(() => {
                                $('#kanban<?= $kunik ?>').kanban('destroy')
                                findActionOnProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, my<?= $kunik ?>, clickedData._id,'all', '')
                                
                                clearInterval(interv);
                                interv = null;    
                            }, 50);
                        }
                    } else if(clickedData.header != 'listProjet' && clickedData.header != 'memberof' && clickedData.header != 'todo' && clickedData.header != 'totest' && clickedData.header != 'tracking' && clickedData.header != 'done') {
                        mylog.log(clickedData, 'clickedData')
                        activeMilestone<?= $kunik ?> = clickedData._id
                        $('#loader<?= $kunik ?>').removeClass('hidden')
                        $('#backdrop<?= $kunik ?>').removeClass('hidden')
                        $('#projetBread').data('ide', '')
                        
                        var interv = setInterval(() => {
                            $('#kanban<?= $kunik ?>').kanban('destroy')
                            findActionOnProjet(allProjet<?= $kunik ?>, member<?= $kunik ?>, ide, clickedData.parentId,'all', clickedData._id)
                            
                            clearInterval(interv);
                            interv = null;    
                        }, 50);
                    } else if(clickedData.header == 'todo' || clickedData.header == 'totest' || clickedData.header == 'tracking' || clickedData.header == 'done') {
                        var previewDom = $('#action-preview<?= $blockKey ?>');
                        previewDom.empty();
                        show_action_detail_modal(clickedData._id).then(function (__html) {
                            previewDom.empty().html(__html);
                        });
                    }
                },
                onMilestone(clickedData){
                    mylog.log(clickedData, 'milestone')
                    if($('#kanban-wrapper-mile'+clickedData.slug).hasClass('hidden')) {
                        $('#kanban-wrapper-mile'+clickedData.slug).removeClass('hidden');
                        $('#kanban-wrapper-mile'+clickedData.slug).addClass('visible');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('active-mile')
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                            "color" : "#172b4d"
                        });
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').addClass('activer')
                    } else {
                        $('#kanban-wrapper-mile'+clickedData.slug).addClass('hidden');
                        $('#kanban-wrapper-mile'+clickedData.slug).removeClass('visible');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('active-mile');
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+'] .kanban-footer-card').css({
                            "color" : "#172b4d"
                        });
                        $('.kanban-list-card-detail[data-id = '+clickedData.id+']').removeClass('activer')
                    }
                },
                onAddAction(clickedData){
                    let path2ValueOption = {}
                    path2ValueOption.url = baseUrl
                    const url = baseUrl + '/costum/project/project/request/room';
                    const post = {
                        project : clickedData._id,
                        connectedUser: userConnected._id.$id
                    };
                    let room = '';
                    ajaxPost(null, url, post, function (result) {
                        room = result;
                        mylog.log(clickedData,room, 'milestone')
                    },null,null, {async:false});
                    const default_finder = {};

                    default_finder[userConnected['_id']['$id']] = {
                        type : 'citoyens',
                        name : userConnected['name']
                    };

                    dyFObj.openForm({
                        jsonSchema : {
                            title : 'Ajouter une action',
                            properties : {
                                action_name : dyFInputs.name('action'),
                                action_parentType : dyFInputs.inputHidden("projects"),
                                action_parentId : dyFInputs.inputHidden(clickedData._id),
                                action_creator : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                action_credits : dyFInputs.inputHidden(1),
                                action_idUserAuthor : dyFInputs.inputHidden(userConnected['_id']['$id']),
                                action_idParentRoom : dyFInputs.inputHidden(room),
                                action_tags : {
                                    inputType : 'tags',
                                    label : trad.tags
                                },
                                action_contributors : {
                                    inputType : 'finder',
                                    label : 'Assigné à qui ?',
                                    multiple : true,
                                    rules : {
                                        lengthMin : [1, 'contributors'],
                                        required : true
                                    },
                                    initType : ['citoyens'],
                                    values : default_finder,
                                    initBySearch : true,
                                    initMe : true,
                                    initContext : false,
                                    initContacts : false,
                                    openSearch : true
                                },
                                action_urls : {
                                    label : 'Urls',
                                    inputType : 'array',
                                    value : [
                                        window.location.toString()
                                    ]
                                },
                                action_status : dyFInputs.inputHidden('todo')
                            },
                            save : function (__form) {
                                var value = {
                                    urls : getArray('.action_urlsarray')
                                };
                                var contributors = {};
                                $.each(Object.keys(__form['action_contributors']), function (__, __key) {
                                    contributors[__key] = {
                                        type : 'citoyens'
                                    };
                                });
                                $.each(['scope', 'collection', 'action_contributors', 'action_urls[]'], function (__, __key) {
                                    delete __form[__key];
                                });

                                $.each(Object.keys(__form), function (__, __name) {
                                    var name = __name.substring(7);
                                    if (name === 'tags') value[name] = __form[__name].split(',');
                                    else value[name] = __form[__name];
                                });
                                var params = {
                                    collection : 'actions',
                                    value
                                };

                                dataHelper.path2Value(params, function (__data) {
                                    if (__data['result'] && __data['result'] === true) {
                                        var action_id = __data['saved']['id'];

                                        // Envoyer une notification rocket chat pour la nouvelle tâche
                                        var url = baseUrl + '/survey/answer/rcnotification/action/newaction';
                                        var post = {
                                            projectname : clickedData.name,
                                            actname : value['name'],
                                            channelChat : clickedData.slug,
                                        };
                                        ajaxPost(null, url, post);

                                        // Enregistrer les contributeurs
                                        dataHelper.path2Value({
                                            id : action_id,
                                            collection : 'actions',
                                            path : 'links.contributors',
                                            value : contributors
                                        }, function (__data) {
                                            var newAction = {
                                                    _id : __data.elt._id.$id,
                                                id :  __data.elt._id.$id,
                                                instanceIdentity :  __data.elt._id.$id,
                                                title : '<b>'+ __data.elt.name+'</b>',
                                                name : __data.elt.name,
                                                tags : (typeof __data.elt.tags != 'undefined' ?  __data.elt.tags : []),
                                                header : "todo",
                                                html : true,
                                                canMoveCard: true,
                                                actions : [ 
                                                    {
                                                        'icon': 'fa fa-trash' ,
                                                        'bstooltip' : {
                                                            'text' : 'Supprimer le membre de la communauter', 
                                                            'position' : 'left'
                                                        }, 
                                                        'action': 'onRemoveAction'
                                                    }
                                                ] 
                                            }
                                            mylog.log('new action', newAction)
                                            
                                            $('#kanban<?= $kunik?>').kanban('setData', {column : 'todo', id : newAction._id, data : newAction});
                                            if (__data['result'] && __data['result'] === true) {
                                                toastr.success(__data.msg);
                                                dyFObj.closeForm();
                                                rocket_chat_contribution(action_id, Object.keys(contributors));
                                            }
                                        }, path2ValueOption);
                                    }
                                }, path2ValueOption);
                            }
                        }
                    }, null, null, null, null, {
                        type : 'bootbox'
                    });
                },
                onRemoveAction : function(action, cardDom){
                    $.confirm({
                        title : trad['Archive'], type : 'orange', content : trad['Please confirm archiving'], buttons : {
                            no : {
                                text : trad.no, btnClass : 'btn btn-default'
                            }, yes : {
                                text : trad.yes, btnClass : 'btn btn-warning', action : function(){
                                    var params = {
                                        id : action._id, collection : 'actions', path : 'status', value : 'disabled'
                                    };
                                    
                                    dataHelper.path2Value(params, function(response){
                                        if (response.result) {
                                            var index = kanban<?= $kunik ?>.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(cardDom);
                                            kanban<?= $kunik ?>.kanban('deleteData', {column : action.header, id : action.id});
                                            // ajaxPost(null, coWsConfig.pingActionManagement, {
                                            //     event : '.archive' + aapObj.common.getQuery().projectId, data : {
                                            //         action : action.id,
                                            //         emiter : '',
                                            //         column : action.header
                                            //     }
                                            // }, null, null, {contentType : 'application/json'});
                                        }
                                    });
                                }
                            }
                        }
                    });
                },
                onCardInsert : function(newAction){
                    const url = baseUrl + '/costum/project/project/request/room';
                    const post = {
                        project : projetId,
                        connectedUser: userConnected._id.$id
                    };
                    let room = '';
                    ajaxPost(null, url, post, function (result) {
                        room = result;
                    },null,null, {async:false});
                    newAction = $.extend({}, newAction);
                    var actionId = newAction.id;
                    var path2Value = {
                        collection : 'actions',
                        path: 'allToRoot'
                    };
                    
                    if (['done', 'todo'].includes(newAction.header)) {
                        path2Value.value = {
                            name : newAction.title,
                            status : newAction.header
                        };
                    } else if (newAction.header === 'tracking') {
                        path2Value.value = {
                            name : newAction.title,
                            status : 'todo',
                            tracking: true
                        };
                        path2Value.setType = [{type : 'boolean', path : 'tracking'}];
                    } else {
                        var dictionnary = {totest : 'totest'};
                        path2Value.value = {
                            name : newAction.title,
                            status : 'todo',
                            tags : [dictionnary[newAction.header]]
                        };
                    }
                    path2Value.value.parentId = projetId;
                    path2Value.value.creator = userConnected._id.$id;
                    path2Value.value.idUserAuthor = userConnected._id.$id;
                    if (room) {
                        path2Value.value.idParentRoom = room;
                    }
                    path2Value.value.parentType = 'projects';
                    
                    dataHelper.path2Value(path2Value, function(response){
                        if (response.result && response.saved && !response.saved.error) {
                            toastr.success(response.saved.msg);
                            newAction.id = response.saved.map._id.$id;
                            newAction._id = response.saved.map._id.$id;
                            switch (newAction.header) {
                                case 'tracking':
                                    newAction.tracking = true;
                                    break;
                                case 'totest':
                                    newAction.tags = ['totest'];
                                default:
                                    newAction.status = newAction.header;
                                    break;
                            }
                            
                            // send create notification
                            ajaxPost(null, baseUrl + '/survey/answer/rcnotification/action/newaction', {
                                actname : newAction.title,
                                projectId : projetId
                            }, function(){});
                            
                            newAction.actions = [{
                                icon : 'fa fa-trash',
                                bstooltip : {text : trad.delete, position : 'top'},
                                action : 'onRemoveAction',
                                bstooltip : {text : trad['Archive'], position : 'top'}
                            }];
                            $('#kanban<?= $kunik?>').kanban('setData', {column : newAction.header, id : actionId, data : newAction});
                            mylog.log('new action', newAction)
                            // ajaxPost(null, coWsConfig.pingActionManagement, {
                            //     event : '.kanban-insert-card' + aapObj.common.getQuery().projectId, data : {
                            //         id : newAction.id, action : newAction
                            //     }
                            // }, null, null, {contentType : 'application/json'});
                        }
                    });
                },
                onAddProjet(action){
                    mylog.log('hita')
                    var typeForm = 'project'
                    var commonElObj<?= $kunik?> = {
                        "beforeBuild":{
                            "properties" : { 
                            }
                        },
                        "onload" : {
                            "actions" : {
                                "src" : { 
                                    "infocustom" : "Remplir le champ"
                                }, 
                            }
                        }
                    };   
                    commonElObj<?= $kunik?>.afterSave = function(data){
                        dyFObj.commonAfterSave(data, function(){
                            $("#ajax-modal").modal('hide');
                            dyFObj.closeForm();
                            //urlCtrl.loadByHash(location.hash);
                        });
                    }
                    dyFObj.openForm(typeForm, null, null, null, commonElObj<?= $kunik?>);
                    // if(contextData && contextData.type && contextData.id ){
                    //     dyFObj.openForm(typeForm,"sub", null, null, commonElObj<?= $kunik?>);
                    // }
                    // else{
                    //     dyFObj.openForm(typeForm, null, null, null, commonElObj<?= $kunik?>);
                    // }
                },
                onRemoveProjet(action, dom) {
                    mylog.log(action, 'delete');
                    load<?=$kunik?> = false
                    links.disconnect(
                        typeCostum,
                        idCostum,
                        action._id,
                        action.collection,
                        'projects',
                        function () {
                            load<?=$kunik?> = false
                            var index<?= $kunik ?> = kanban<?= $kunik ?>.find(`[data-column=${action.header}] .kanban-list-card-detail`).index(dom)
                            kanban<?= $kunik ?>.kanban('deleteData', { column: action.header, id:action.id,  index: index<?= $kunik ?> });
                        }
                    );
                },
                onCardDrop(dropped, updates){
                    var urlBase = 'https://www.communecter.org';
                    mylog.log(dropped)
                    if(dropped.origin != dropped.target) {
                        let path2ValueUrl = {};
                        path2ValueUrl.url = urlBase;
                        let actionObjet = {
                            id : dropped.data._id,
                            collection : 'actions',
                            costumSlug : slugCostum,
                            costumEditMode : 'false'
                        };
                        let resetObjet = {
                            id : dropped.data._id,
                            collection : 'actions',
                            costumSlug : slugCostum,
                            costumEditMode : 'false'
                        };
                        if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                            resetObjet['value'] = 'todo'
                            resetObjet['path'] = 'status'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'tracking') {
                            actionObjet['value'] = 'true'
                            actionObjet['path'] = 'tracking'
                            actionObjet['setType'] = 'boolean'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'done') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'todo'
                            resetObjet['path'] = 'status'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'totest') {
                            dropped.data.tags.push('totest')
                            actionObjet['value'] = dropped.data.tags
                            actionObjet['path'] = 'tags'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes');
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'done') {
                            actionObjet['value'] = 'done'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                            
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'totest') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            let index = dropped.data.tags.indexOf('totest')
                            dropped.data.tags.splice(index,1)
                            resetObjet['value'] = dropped.data.tags
                            resetObjet['path'] = 'tags'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo' && typeof dropped.origin != 'undefined' && dropped.origin == 'tracking') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);

                            resetObjet['value'] = 'false'
                            resetObjet['path'] = 'tracking'
                            resetObjet['setType'] = 'boolean'
                            dataHelper.path2Value(resetObjet, function (__data) {
                                if (__data['result']) {
                                    
                                }
                            }, path2ValueUrl);
                        } else if (typeof dropped.target != 'undefined' && dropped.target == 'todo') {
                            actionObjet['value'] = 'todo'
                            actionObjet['path'] = 'status'
                            dataHelper.path2Value(actionObjet, function (__data) {
                                if (__data['result']) {
                                    toastr.success('Action deplacer avec succes', {
                                        closeButton : true,
                                        preventDuplicates : true,
                                        timeOut : 3000
                                    });
                                }
                            }, path2ValueUrl);
                        } 
                        mylog.log(dropped, updates, 'data drop')
                    }
                },
                onRemoveAdmin(data){
                    links.updateadminlink(
                        typeCostum,
                        idCostum,
                        data.instanceIdentity,
                        'citoyens',
                        (typeCostum == 'organizations' ? 'members' : 'contributors'),
                        false,
                        data.name,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .removeAdmin').addClass('btn-is-admin')
                        }
                    )
                   
                },
                onAddAdmin(data){
                    links.connect(
                        typeCostum, 
                        idCostum, 
                        data.instanceIdentity, 
                        'citoyens', 
                        'admin', 
                        '', 
                        true,
                        function() {
                            $('.kanban-list-card-detail[data-id='+data._id+'] .addAdmin').removeClass('btn-is-admin')
                        }
                    )
                },
                onInvite(data){
                    smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/'+'element/invite/type/'+typeCostum+'/id/'+idCostum);
                },
                onRemoveMember(data, dom){
                    mylog.log(data.collection, 'collection')
                    var memb<?= $kunik ?> = {
                        childId : data.instanceIdentity,
                        childType : data.collection,
                        parentType : typeCostum,
                        parentId : idCostum,
                        connectType : typeCostum == 'organizations' ? 'members' : 'contributors'
                    }
                    bootbox.confirm(trad.areyousuretodelete, function(result) {
                        if (!result) {
                            return;
                        } else { 
                            ajaxPost(
                                null,
                                baseUrl+"/" + moduleId + "/link/disconnect",
                                memb<?= $kunik ?>,
                                function(donne){
                                    let index<?= $kunik ?> = kanbanDom<?= $kunik ?>.find(`[data-column=${data.header}] .kanban-list-card-detail`).index(dom)
                                    kanbanDom<?= $kunik ?>.kanban('deleteData', { column: data.header, id:data.id,  index: index<?= $kunik ?> });
                                    toastr.success("Membre supprimer");
                                    
                                },null,null, {}
                            )
                        }
                    })
                    
                },
            });
        }
    });
</script>
