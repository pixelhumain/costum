<?php 

    $kunik = "communityWithViewSwitcher";

    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";

    $graphAssets = [
        '/js/hexagon.js',
        '/plugins/d3/d3.v6.min.js',
        '/js/graph.js',
        '/js/venn.js',
        '/css/graph.css'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
    );

    $cssAnsScriptFilesModuleMap = array( 
        '/leaflet/leaflet.css',
        '/leaflet/leaflet.js',
        '/css/map.css',
        '/markercluster/MarkerCluster.css',
        '/markercluster/MarkerCluster.Default.css',
        '/markercluster/leaflet.markercluster.js',
        '/js/map.js',
    );
    
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

?>
<style type="text/css">

    #menuRightmapCommunity{
        position: absolute !important;
    }

    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
    .container-preview{
        display: flex;
        justify-content: center;
        align-items: stretch;
    }

    .container-preview #filter-hexagone-container{
		display: flex;
		flex-direction: column;
		width: 300px;
		background-color: white;
        height: 700px;
        overflow-y: auto;
    }
    .hexagone-child-container.in{
		display: flex;
		flex-direction: column;
		text-align: left;
	}
	.hexagone-child-container{
		padding: 0px 15px;
	}

	.hexagone-child-container .btn-hexagone-zoomer{
		text-align: left;
	}
    .btn-hexa-container{
		background: white;
    	border: 1px solid;
		padding: 0;
		text-align: left;
	}
	.btn-hexa-container.btn-primary{
		color: white;
		background-color: #2C3E50;
		border-color: #2C3E50;
		font-weight: 700;
	}

	.btn-hexa-container button{
		background: none;
    	border: none;
		padding: 6px 12px;
	}

    /* .communityView{
        box-shadow:0 0 3px rgba(0,0,0,.3);
    } */

    :root {
        --duration: 0.5s;
    }

    .tab-pane.active{
        border-width: 5px 5px 5px 5px;
        border-style: solid;
        border-color: #e54e5e;
    }

    #pills-tabContent{
        padding-left: 6px ;
    }

    .image-grid<?= $kunik?> {
        /* display: grid;
        grid-template-columns: repeat(auto-fit, minmax(200px, 1fr)); */
        display: flex;
        justify-content: center;
        align-items: center; 
        flex-wrap: wrap;       
        background-color: white;
        border-radius: 10px;
        box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
        transition: transform 0.3s ease;
        margin: 5px;
        padding: 15px;
    }

  .join<?= $kunik?> {
    display: grid;
    justify-content: center;
    align-items: center; 
    background-color: white;
    border-radius: 10px;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
    transition: transform 0.3s ease;
    margin: 5px;
    padding: 15px;
  }

  .grid-item<?= $blockKey ?> {
      flex: 1 1 200px;
      width: 100%;
      height: 200px;
      display: block;
      margin: 5px;
  }


  .box<?= $blockKey ?>{
        height: 100% ;
        width: 100%;
        background-size: auto;
        background-repeat: no-repeat;
        background-position: center;
        filter: grayscale(100%);
        transition: filter 0.3s ease;
        border-radius: 10px;
        margin: 5px;
  }

  .box<?= $blockKey ?>:hover {
    filter: grayscale(0%);
    transform : scale(1.10093);
    transition : all .5s cubic-bezier(.7,0,.3,1);
    z-index: 2;
    border: 1px solid white;
  }

  .box<?= $blockKey ?> {position:relative;text-align:center;box-shadow:0 0 3px rgba(0,0,0,.3);overflow:hidden}
  .box<?= $blockKey ?> .post{text-transform:capitalize}
  .box<?= $blockKey ?>:before{content:"";width:100%;background:rgba(0,0,0,.5);height:100%;position:absolute;top:0;left:0;opacity:0;transition:all .35s ease 0s}
  .box<?= $blockKey ?>:hover:before{opacity:1}
  .box<?= $blockKey ?> img{width:100%;height:auto}
  .box<?= $blockKey ?> .box-content{width:90%;height:90%;position:absolute;top:5%;left:5%}
  .box<?= $blockKey ?> .box-content:after,.box<?= $blockKey ?> .box-content:before{content:"";position:absolute;top:0;left:0;bottom:0;right:0;opacity:0;transition:all .7s ease 0s}
  .box<?= $blockKey ?> .box-content:before{border-bottom:1px solid rgba(255,255,255,.5);border-top:1px solid rgba(255,255,255,.5);transform:scale(0,1);transform-origin:0 0 0}
  .box<?= $blockKey ?> .box-content:after{border-left:1px solid rgba(255,255,255,.5);border-right:1px solid rgba(255,255,255,.5);transform:scale(1,0);transform-origin:100% 0 0}
  .box<?= $blockKey ?>:hover .box-content:after,.box<?= $blockKey ?>:hover .box-content:before{opacity:1;transform:scale(1);transition-delay:.15s}
  .box<?= $blockKey ?> .title{font-size:15px;font-weight:700;color:#fff;margin:15px 0;opacity:0;transform:translate3d(0,-50px,0);transition:transform .5s ease 0s}
  .box<?= $blockKey ?>:hover .title{opacity:1;transform:translate3d(0,0,0)}
  .box<?= $blockKey ?> .post{font-size:14px;color:#fff;padding:10px;background:#d79719;opacity:0;border-radius:0 19px;transform:translate3d(0,-50px,0);transition:all .7s ease 0s}
  .box<?= $blockKey ?> .icon,.box15 .icon{padding:0;list-style:none}
  .box<?= $blockKey ?>:hover .post{opacity:1;transform:translate3d(0,0,0);transition-delay:.15s}
  .box<?= $blockKey ?> .icon{width:100%;margin:0;position:absolute;bottom:-10px;left:0;opacity:0;z-index:1;transition:all .7s ease 0s}
  .box<?= $blockKey ?>:hover .icon{bottom:20px;opacity:1;transition-delay:.15s}
  .box<?= $blockKey ?> .icon li button{display:block;width:40px;height:40px;line-height:40px;border:1px solid #fff;border-radius:0 16px;font-size:14px;color:#fff;margin-right:5px;transition:all .4s ease 0s}
  .box<?= $blockKey ?> .icon li button:hover{background:#e54e5e;border-color:#e54e5e}
  .box<?= $blockKey ?> .icon li,.box<?= $blockKey ?> .post{display:inline-block}
  .box<?= $blockKey ?>:hover .generic-avatar{opacity:0}
  @media only screen and (max-width:990px){.box<?= $blockKey ?>{margin-bottom:30px}
  }

  .mobile-select-filter<?= $blockKey ?>{
    display: inline-block;
	  position: relative;
    margin-right: 10px;
    padding: 10px 20px;
    border-radius: 5px;
    background-color: #fff;
    color: #000;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
  }

  .filter<?= $kunik ?>SMXS{
    display: flex;
    justify-content: center;
    align-items: center; 
    flex-wrap: wrap;  
    box-shadow:0 0 3px rgba(0,0,0,.3);
    margin: 5px;
    padding: 15px;
    border-radius: 10px;
    background-color: white;
  }

  .float-shadow<?= $blockKey ?> {
	display: inline-block;
	position: relative;
	transition-duration: var(--duration);
	transition-property: transform;
    margin-right: 10px;
    padding: 10px 20px;
    border-radius: 5px;
    background-color: #fff;
    color: #000;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
    transition: background-color 0.3s ease;

	@include hideTapHighlightColor();
	@include hardwareAccel();
	@include improveAntiAlias();

	&:before {
		pointer-events: none;
	  position: absolute;
	  z-index: -1;
	  content: '';
	  top: 100%;
	  left: 5%;
	  height: 10px;
	  width: 90%;
	  opacity: 0;
	  background: radial-gradient(ellipse at center, rgba(0,0,0,.35) 0%,rgba(0,0,0,0) 80%); /* W3C */
		transition-duration: var(--duration);
		transition-property: transform opacity;
	}

	&:hover {
		transform: translateY(-5px); /* move the element up by 5px */

		&:before {
			opacity: 1;
			transform: translateY(5px); /* move the element down by 5px (it will stay in place because it's attached to the element that also moves up 5px) */
		}
	}
 }


  .filter<?= $kunik ?>LG{
    display: flex;
    justify-content: center;
    align-items: center; 
    flex-wrap: wrap;  
    box-shadow:0 0 3px rgba(0,0,0,.3);
    margin: 5px;
    padding: 15px;
    border-radius: 10px;
    background-color: white;
  }

  svg {
      display: block;
      margin: auto;
        }

  .filter<?= $kunik ?>LG button.filterSelected {
    background-color: black;
    color: white;
  }

  .generic-avatar {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .generic-avatar i {
    font-size: 48px; /* Taille de l'icône utilisateur */
    color: #999;
  }

  .joinUs{
    font-size: 2em !important;
  }

  .btn-download-link-qr-node-container<?= $kunik ?> {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .3);
        display: none;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: .3s;
    }

    .btn-download-link-qr-node-container<?= $kunik ?>:hover {
        opacity: 1;
    }

    .btn-download-link-qr-node-container<?= $kunik ?> button {
        border: none;
        width: 50px;
        height: 50px;
        background-color: black;
        color: white;
        text-align: center;
        line-height: 50px;
        font-size: 20px;
        border-radius: 100%;
    }

    .invitation-link-qr-container{
      margin-top: 10px;
      border-radius: 4px;
      position: relative;
    }
    
    .wrapper{
        text-align:center;
        margin:50px auto;
    }

    .tabs<?= $kunik ?>{
        margin-top:50px;
        font-size:15px;
        padding:0px;
        list-style:none;    
        box-shadow:0px 5px 20px rgba(0,0,0,0.1);
        display:inline-block;
        border-radius: 50px;
        position:relative;
    }

    .tabs<?= $kunik ?> a{
        text-decoration:none;
        color: #777;
        text-transform:uppercase;
        padding:10px 20px;
        display:inline-block;
        position:relative;
        z-index:1;
        transition-duration:0.6s;
        color: black;
        transition: transform 1s ease, color var(--duration);
    }

    .tabs<?= $kunik ?> a.active{
        color:#fff;
    }

    .tabs<?= $kunik ?> a i{
        margin-right:5px;
    }

    .tabs<?= $kunik ?> .selector{
        height:100%;
        display:inline-block;
        position:absolute;
        left:0px;
        top:0px;
        z-index:1;
        border-radius: 50px;
        transition-duration:0.6s;
        transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
        background: #e54e5e;
        background: -moz-linear-gradient(45deg, #e54e5e 0%, #8200f4 100%);
        background: -webkit-linear-gradient(45deg, #e54e5e 0%,#8200f4 100%);
        background: linear-gradient(45deg, #e54e5e 0%,#8200f4 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e54e5e', endColorstr='#8200f4',GradientType=1 );
    }

    #graph-container-<?= $kunik ?>{
        margin-bottom: 0px !important;
        background-color: transparent !important;
        overflow: hidden !important;
        width: 100% !important
    }

    #graph-container-<?= $kunik ?> .leaf-group img{
        border-radius: 50% ;
    }
</style>

<style id="community<?= $kunik ?>"></style>

<div id="preview-graph" class="col-xs-12 <?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
        <div class="col-lg-12" style="z-index: 2;padding-bottom: 5px;">
            <div class="col-lg-12" style="display: flex;justify-content: center;align-items: center;">
                <nav class="tabs<?= $kunik ?>">
                    <div class="selector"></div>
                    <a class="nav-link active" id="pills-card-tab" data-toggle="pill" href="#pills-card" role="tab" aria-controls="pills-card" aria-selected="true" style="color: black;border-radius: 20px 20px 0px 0px; ">Annuaire</a></a>
                    <a class="nav-link" id="pills-hexagon-tab" data-toggle="pill" href="#pills-hexagon" role="tab" aria-controls="pills-hexagon" aria-selected="false" style="color: black;border-radius: 20px 20px 0px 0px;">Hexagone</a></a>
                    <a class="nav-link" id="pills-graph-tab" data-toggle="pill" href="#pills-graph" role="tab" aria-controls="pills-graph" aria-selected="false" style="color: black;border-radius: 20px 20px 0px 0px;">Circle</a></a>
                    <a class="nav-link" id="pills-map-tab" data-toggle="pill" href="#pills-map" role="tab" aria-controls="pills-map" aria-selected="false" style="color: black;border-radius: 20px 20px 0px 0px;">Map</a></a>
                </nav>
            </div>
        </div>
        <div class="col-lg-12 sm-padding communityView" style="z-index: 1; top: 0px">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane co-scroll fade menu-tab-filter card active in" id="pills-card" role="tabpanel" aria-labelledby="pills-card-tab">
                        <div class="filter<?= $kunik ?>LG hidden-sm hidden-xs"></div>
                        <div class="filter<?= $kunik ?>SMXS hidden-lg">
                            <select class="mobile-select-filter<?= $blockKey ?>" onchange="filterSelection(this.value)"></select>
                        </div>
                        <div class="image-grid<?= $kunik?>"></div>
                    </div>

                    <div class="tab-pane co-scroll fade menu-tab-filter" id="pills-hexagon" role="tabpanel" aria-labelledby="pills-hexagon-tab">
                        <div class="col-xs-12 padding-10">
                            <span class="pull-left">
                                <h4>Hexagone visualisation du communauté</h4>
                            </span>
                        </div>
                        <div class="container-preview padding-10" style="overflow-y: auto;">
                            <canvas id="hexagoneCommunity" height="700px"></canvas>
                            <div id="filter-hexagone-container">

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane co-scroll circleTab fade menu-tab-filter" id="pills-graph" role="tabpanel" aria-labelledby="pills-graph-tab">
                        <div class="col-xs-12 padding-10" id="ciclePacking" >
                            <div id="focus-container<?= $kunik ?>"></div>
                            <div id="search-container-<?= $kunik ?>" class="searchObjCSS" style='background-color:transparent!important'></div>
                            <div id="graph-container-<?= $kunik ?>"></div>
                        </div>
                    </div>

                    <div class="tab-pane map-tab mapTab fade menu-tab-filter" id="pills-map" role="tabpanel" aria-labelledby="pills-map-tab"> 
                            <div class="switcherFilter filter<?= $kunik ?>LG hidden-sm hidden-xs"></div>
                            <div class="switcherFilter filter<?= $kunik ?>SMXS hidden-lg">
                                <select class="filterSelectMap mobile-select-filter<?= $blockKey ?>"></select>
                            </div>
                            <div style="z-index: 1;height: 60vh;" class="col-md-12 no-padding" id="mapCommunity<?= $kunik?>"></div>
                    </div>

                    

                </div>
        </div>
</div>

<script>
    // script tab nav

    var tabs = $('.tabs<?= $kunik ?>');
    var selector = $('.tabs<?= $kunik ?>').find('a').length;
    //var selector = $(".tabs").find(".selector");
    var activeItem = tabs.find('.active');
    var activeWidth = activeItem.innerWidth();
    $(".selector").css({
    "left": activeItem.position.left + "px", 
    "width": activeWidth + "px"
    });

    $(".tabs<?= $kunik ?>").on("click","a",function(e){
    e.preventDefault();
    $('.tabs<?= $kunik ?> a').removeClass("active");
    $(this).addClass('active');
    var activeWidth = $(this).innerWidth();
    var itemPos = $(this).position();
    $(".selector").css({
        "left":itemPos.left + "px", 
        "width": activeWidth + "px"
    });
    });


    //nav
    var activeCircle<?= $kunik ?> = "";
    var circle<?= $kunik ?> = null;
    HexagonGrid.prototype.clickEvent = function(e){
        let hexa = this;
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        var localX = mouseX - this.canvasOriginX;
        var localY = mouseY - this.canvasOriginY;

        var tile = this.getSelectedTile(localX, localY);
        if(this.hasHexagon(tile.column, tile.row)){
            let drawedHex = this.drawedHex[`${tile.column},${tile.row}`];
            let data = this.data[drawedHex.group][`${drawedHex.column},${drawedHex.row}`];
            let params = {};
            params["elementId"] = costum.contextId;
            params["userId"] = data.id;
            params["name"] = data.text;
            params["type"] = data.type;
            params["socialNetwork"] = typeof data.socialNetwork != 'undefined' ? data.socialNetwork : [];
            params["profileImg"] = data.image != null ? data.image : "";
            params["userCredits"] = typeof data.credits != 'undefined' ? data.credits : 0;
            ajaxPost(
                null,
                baseUrl + "/co2/person/getuserbadgeandrecentcontribution",
                params, 
                function (response) {  
                    params["badgesDetails"] = response.badges;
                    params["projects"] = response.projects;
                    params["actionsCreated"] = response.actionsCreated;
                    params["actionsParticipated"] = response.actionsParticipated;
                    params["actionsLast12Month"] = response.actionsLast12Month;
                    params["countAll"] = response.countAll;

                    urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                }
            );
        }
    }

    var hexagonCommunity<?= $kunik ?> = new HexagonGrid("hexagoneCommunity", 50, window.innerWidth - 700);
    $(function(){
        str="";
        str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#community<?= $kunik ?>").append(str);

        if (costum.editMode){   
            cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
            communityWithViewSwitcher = {
                configTabs : {
                    general : {
                        inputsConfig : [
                            "background",
                            {
                            type : "selectMultiple",
                            options : {
                                name : "roleToShow",
                                label : tradDynForm.roles,
                                canDragAndDropChoise : true,
                            }
                            },
                        ]
                    },
                    style: {
                        inputsConfig : [
                            {
                                type: "section",
                                options: {
                                    name: "carouselCard",
                                        label: "Card" ,
                                        showInDefault: false,
                                        inputs: [
                                        "border",
                                        "boxShadow",
                                        "margin",
                                        "padding",
                                        "borderRadius"
                                    ]
                                }
                            },
                            "addCommonConfig"
                        ]
                    },
                    advanced : {
                        inputsConfig : [
                            "addCommonConfig"
                        ]
                    },
                    },
                onChange : function(path,valueToSet,name,payload,value){
                    
                },
                afterSave: function(path,valueToSet,name,payload,value,id){
                    cmsConstructor.helpers.refreshBlock(id, ".cmsbuilder-block[data-id='"+id+"']");
                    costumSocket.emitEvent(wsCO, "update_component", {
                                    functionName: "refreshBlock", 
                                    data:{ id: id}
                    })
                }
                
            }
            cmsConstructor.blocks.communityWithViewSwitcher<?= $blockKey ?> = communityWithViewSwitcher;
            
        } 

        var typeCostum = "";
        var idCostum = "";
        var slugCostum = "";
        var roleToShow = <?= json_encode($blockCms["roleToShow"]) ?>;
        var map<?= $kunik ?>;
        var allData = {};
        var dataFiltered = [];
        var dataMember<?= $kunik ?> = {
                "roles": [],
                "data" : {

                }
            };


        if(contextData != null && typeof contextData != 'undefined'){
            idCostum = typeof contextData.id != 'undefined' ? contextData.id : contextData._id.$id;
            typeCostum = typeof contextData.collection != "undefined" ? contextData.collection : contextData.type;
            slugCostum = contextData.slug;
        }

        if(typeof costum != 'undefined' && costum !== null){
            if(typeof costum.contextType !='undefined'){
                idCostum = costum.contextId;
                typeCostum = costum.contextType;
                slugCostum = costum.contextSlug;
            }
        }
        var params<?= $kunik ?> = {     
            searchType : ["citoyens"],
            notSourceKey : true,
            fields : ["links","badges","userWallet","adress","geo","geoPosition","socialNetwork"],
            filters : {},
            indexStep : 0        
        };
        if(typeCostum === "organizations"){
            params<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
        }
        else if(typeCostum === "projects"){
            params<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
        }
        else {
            params<?= $kunik ?>.filters["links.events."+idCostum] = {'$exists':true};
        }

        let center = hexagonCommunity<?= $kunik ?>.getCenterTile();
        function getData(){
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    if(typeof data.results != "undefined"){
                        allData = data.results;
                        var item = `
                            <div class="grid-item<?= $blockKey ?> inviteLink<?= $kunik ?> <?= @$linksBtn["isMember"] == true ? "hidden" : "" ?>">
                                <div class="box<?= $blockKey ?> carouselCard inviteLink<?= $blockKey ?>" >
                                <div class="box-content">
                                    <h3 class="title joinUs" ><?php echo Yii::t("cms", "Join Us")?></h3>
                                    <ul class="icon">
                                    <li>
                                        <button id="btn-download-link-qr-node<?= $kunik ?>"><i class="fa fa-download" aria-hidden="true"></i></button>
                                    </li>
                                    </ul>
                                </div>
                                </div>
                            </div>`;
                        $.each(data.results,function(k,v){

                            if ( roleToShow.length > 0 ){
                                if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").indexOf(roleToShow[0]) >= 0 ){
                                    item += createGridItem(k, v, "<?= $blockKey ?>");
                                }
                            } 
                            else {
                                item += createGridItem(k, v, "<?= $blockKey ?>");
                            }
                            
                            
                        })

                        $(".image-grid<?= $kunik?>").append(item);
                        $(".showDetails").on("click", function(e){
                            e.stopPropagation();
                            id = $(this).data("id");
                            params = {};
                            params["elementId"] = costum.contextId;
                            params["userId"] = id;
                            params["name"] = data.results[id]["name"];
                            params["socialNetwork"] = typeof data.results[id]["socialNetwork"] !== "undefined" ? data.results[id]["socialNetwork"] : [];
                            params["profileImg"] = typeof data.results[id]["profilMediumImageUrl"] !== "undefined" ? data.results[id]["profilMediumImageUrl"] : "";
                            params["badges"] = typeof data.results[id]["badges"] !== "undefined" ? data.results[id]["badges"] : [];
                            params["userCredits"] = typeof data.results[id]["userWallet"] !== "undefined" && typeof data.results[id]["userWallet"][costum.contextId] !== "undefined" && typeof data.results[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? data.results[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
                            ajaxPost(
                            null,
                            baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                            params, 
                            function (response) {  
                                params["badgesDetails"] = response.badges;
                                params["projects"] = response.projects;
                                params["actionsCreated"] = response.actionsCreated;
                                params["actionsParticipated"] = response.actionsParticipated;
                                params["actionsLast12Month"] = response.actionsLast12Month;
                                params["countAll"] = response.countAll;

                                urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                            }
                            );
                        });
                        if(costum.editMode) $(".inviteLink<?= $kunik ?>").removeClass("hidden")
                        createQR();

                        $.map(data.results, function(valeur, key){
                            if(typeCostum == "organizations"){
                                if(exists(valeur.links) && exists(valeur.links.memberOf)){
                                    $.map(valeur.links.memberOf, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                $.map(value.roles, function(role, roleKey){
                                                    if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                        if(roleToShow.indexOf(role) >= 0){
                                                            if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                                dataMember<?= $kunik ?>.data[role] = {
                                                                    "group": role,
                                                                    childs: [],
                                                                    users: []
                                                                };
                                                            }
                                                            dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                            dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+costum.contextId+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                        }
                                                    }else{
                                                        if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMember<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+costum.contextId+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                            if(typeCostum == "events"){
                                if(exists(valeur.links) && exists(valeur.links.events)){
                                    $.map(valeur.links.events, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                
                                                $.map(value.roles, function(role, key){
                                                    if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                        if(roleToShow.indexOf(role) >= 0){
                                                            if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                                dataMember<?= $kunik ?>.data[role] = {
                                                                    "group": role,
                                                                    childs: [],
                                                                    users: []
                                                                };
                                                            }
                                                            dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                            dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection , credits: jsonHelper.getValueByPath(valeur,"userWallet."+costum.contextId+".userCredits") , socialNetwork: jsonHelper.getValueByPath(valeur,"socialNetwork") });
                                                        }
                                                    }else{
                                                        if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMember<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                    }    
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                            if(typeCostum == "projects"){
                                if(exists(valeur.links) && exists(valeur.links.projects)){
                                    $.map(valeur.links.projects, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                
                                                $.map(value.roles, function(role, key){
                                                    if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                        if(roleToShow.indexOf(role) >= 0){
                                                            if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                                dataMember<?= $kunik ?>.data[role] = {
                                                                    "group": role,
                                                                    childs: [],
                                                                    users: []
                                                                };
                                                            }
                                                            dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                            dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                        }
                                                    }else{
                                                        if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMember<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                    }if(typeof roleToShow != "undefined" && roleToShow.length > 0){
                                                        if(roleToShow.indexOf(role) >= 0){
                                                            if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                                dataMember<?= $kunik ?>.data[role] = {
                                                                    "group": role,
                                                                    childs: [],
                                                                    users: []
                                                                };
                                                            }
                                                            dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                            dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                        }
                                                    }else{
                                                        if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                            dataMember<?= $kunik ?>.data[role] = {
                                                                "group": role,
                                                                childs: [],
                                                                users: []
                                                            };
                                                        }
                                                        dataMember<?= $kunik ?>.data[role].users.push(valeur);
                                                        dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                    }    
                                                });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },null,null, {async:false}
            )
            dataMember<?= $kunik ?>.roles = [...new Set(dataMember<?= $kunik ?>.roles)];
            $.each(dataMember<?= $kunik ?>.data, function(key, value){
                let btn = `<div class="btn margin-5 btn-hexa-container">
                    <button class="btn-expand" data-toggle="collapse" href="#${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <button class="btn-hexagone-zoomer" data-type="group">
                        ${key}
                        <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${dataMember<?= $kunik ?>.data[key].childs.length}</span>
                    </button>
                </div>
                <div class="collapse hexagone-child-container" id="${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                `;
                dataMember<?= $kunik ?>.data[key].childs.forEach((child) => {
                    btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonCommunity<?= $kunik ?>.slugify(child.text)}'>
                        ${child.text}
                    </button>`;
                })
                btn += `</div>`
                $(".container-preview #filter-hexagone-container").append(btn);
            });

            hexagonCommunity<?= $kunik ?>.placeGroupsWithOffsets(Object.values(dataMember<?= $kunik ?>.data), center);
            hexagonCommunity<?= $kunik ?>.fitBounds();
            $(".btn-hexagone-zoomer").off().on('click', function(){
                if($(this).data("type") == "group"){
                    if($(this).parents(".btn-hexa-container").hasClass("btn-primary")){
                        $(".btn-hexa-container").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let group = $(this).find("span").data("countvalue");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).parents(".btn-hexa-container").addClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomToGroup(group);
                    }
                }else{
                    if($(this).hasClass("btn-primary")){
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let texte = $(this).data("value");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).addClass("btn-primary");
                        let element = Object.values(hexagonCommunity<?= $kunik ?>.drawedHex).find(hex => {
                            return hexagonCommunity<?= $kunik ?>.slugify(hex.text) == texte;
                        });
                        if(typeof element != "undefined"){
                            hexagonCommunity<?= $kunik ?>.zoomToElement(element.column, element.row);
                        }
                    }
                }
            })


            var filter = ``;
            var filterXS = "";
            var tags = dataMember<?= $kunik ?>.roles;
            if ( roleToShow.length > 0 ) {
                roleToShow.forEach((value, index) => {
                    if (index === 0) {
                        filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?> filterSelected">${value}</button> `;
                        filterXS += `<option value="${value}" selected>${value}</option>`;
                    } else {
                        filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?> ">${value}</button> `;
                        filterXS += `<option value="${value}">${value}</option>`;
                    }
                });
            } else {
                tags.forEach((value, key) => {
                    filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?>">${value}</button> `;
                    filterXS += `<option value="${value}">${value}</option>`;
                });
            }
            
            if (costum.editMode) {
                cmsConstructor.blocks.communityWithViewSwitcher<?= $blockKey ?>.configTabs.general.inputsConfig[1].options.options = 
                $.map(tags, function(value, key) {
                    return { value: value, label: value };
                });
            }
            

            $(".filter<?= $kunik ?>LG").append(filter);
            $(".mobile-select-filter<?= $blockKey ?>").append(filterXS);


        }
        getData();

        function createGridItem(key, value, blockKey) {
        const hasImage = value.profilMediumImageUrl && value.profilMediumImageUrl !== '' || value.image && value.image !== '' ;
        const avatar = hasImage
            ? ''
            : `<div class="generic-avatar">
                <i class="fa fa-user"></i> <!-- Icon for user -->
                <h3 class="user-name">${value.name || value.text}</h3>
            </div>`;

        return `
            <div class="grid-item${blockKey}">
            <div class="box${blockKey} carouselCard" style="${hasImage ? `background-image:url(${value.profilMediumImageUrl ?? value.image});` : 'display: flex; justify-content: center; align-items: center; background: none;'}">
                ${avatar}
                <div class="box-content">
                <h3 class="title">${value.name || value.text}</h3>
                <ul class="icon">
                    <li><button class="subtitle showDetails" data-id="${key}">
                    <i class="fa fa-eye"></i></button>
                    </li>
                </ul>
                </div>
            </div>
            </div>
        `;
        }


        $(document).on('click', `.filter<?= $kunik ?>LG button`, function() {
            const key = $(this).data("key");
            const data =  dataMember<?= $kunik ?>.data[key]["users"];
            if ($("#pills-map-tab").hasClass("active")) {
                $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
                $(this).addClass("filterSelected");
                map<?= $kunik ?>.clearMap();
                map<?= $kunik ?>.addElts(data)
            } else {
                var item = "";
                const firstElement = $(".image-grid<?= $kunik?>").children().first().detach(); 
                $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
                $(this).addClass("filterSelected");
                $(".image-grid<?= $kunik?>").html("");
                $.each(data,function(k,v){
                    item += createGridItem(v._id["$id"], v, "<?= $blockKey ?>");
                });
                $(".image-grid<?= $kunik?>").append(firstElement);
                $(".image-grid<?= $kunik?>").append(item);

                $(".showDetails").on("click", function(e){
                    e.stopPropagation();
                    id = $(this).data("id");
                    params = {};
                    params["elementId"] = costum.contextId;
                    params["userId"] = id;
                    params["name"] = allData[id]["name"];
                    params["socialNetwork"] = typeof allData[id]["socialNetwork"] !== "undefined" ? allData[id]["socialNetwork"] : [];
                    params["profileImg"] = typeof allData[id]["profilMediumImageUrl"] !== "undefined" ? allData[id]["profilMediumImageUrl"] : "";
                    params["badges"] = typeof allData[id]["badges"] !== "undefined" ? allData[id]["badges"] : [];
                    params["userCredits"] = typeof allData[id]["userWallet"] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? allData[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
                    ajaxPost(
                        null,
                        baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                        params, 
                        function (response) {  
                            params["badgesDetails"] = response.badges;
                            params["projects"] = response.projects;
                            params["actionsCreated"] = response.actionsCreated;
                            params["actionsParticipated"] = response.actionsParticipated;
                            params["actionsLast12Month"] = response.actionsLast12Month;
                            params["countAll"] = response.countAll;

                            urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                        }
                    );
                });
            }
        });


        function filterSelection(key){
            var item = "";
            var firstElement = $(".image-grid<?= $kunik?>").children().first().detach();
            const data =  dataMember<?= $kunik ?>.data[key]["childs"]
            $.each(data,function(k,v){
                item += createGridItem(v._id["$id"], v, "<?= $blockKey ?>");
            });
            $(".image-grid<?= $kunik?>").html("");
            $(".image-grid<?= $kunik?>").append(firstElement);
            $(".image-grid<?= $kunik?>").append(item);
            $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
            $(`.filter<?= $kunik ?>LG button[data-key="${key}"]`).addClass("filterSelected");
            $(".showDetails").on("click", function(e){
                e.stopPropagation();
                id = $(this).data("id");
                params = {};
                params["elementId"] = costum.contextId;
                params["userId"] = id;
                params["name"] = allData[id]["name"];
                params["socialNetwork"] = typeof allData[id]["socialNetwork"] !== "undefined" ? allData[id]["socialNetwork"] : [];
                params["profileImg"] = typeof allData[id]["profilMediumImageUrl"] !== "undefined" ? allData[id]["profilMediumImageUrl"] : "";
                params["badges"] = typeof allData[id]["badges"] !== "undefined" ? allData[id]["badges"] : [];
                params["userCredits"] = typeof allData[id]["userWallet"] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? allData[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
                ajaxPost(
                null,
                baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                params, 
                function (response) {  
                    params["badgesDetails"] = response.badges;
                    params["projects"] = response.projects;
                    params["actionsCreated"] = response.actionsCreated;
                    params["actionsParticipated"] = response.actionsParticipated;
                    params["actionsLast12Month"] = response.actionsLast12Month;
                    params["countAll"] = response.countAll;

                    urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                }
                );
            });
        }

    
        

        function createQR() {
            new AwesomeQR.AwesomeQR({
                text: "<?= Yii::app()->getBaseUrl(true) . "/co2/link/connect/ref/" . $invitelink["ref"] ?>",
                size: 200,
                logoImage: typeof costum.logoMin !== "undefined" ? costum.logoMin : "",
                logoScale: 0.25,
                dotScale: 0.6
            }).draw().then(function(dataUrl) {
                $('.inviteLink<?= $blockKey ?>').css('background-image', `url(${dataUrl})`);
                $(".btn-download-link-qr-node-container<?= $kunik ?>").css({
                    display: "flex"
                })

                $("#btn-download-link-qr-node<?= $kunik ?>").click(function() {
                    var link = document.createElement('a');
                    link.href = dataUrl;
                    link.download = `qrlink-${costum.slug}-${moment().format("DD-MM-YYYY")}.png`;
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                })
            })
        }


        //map 

        $("#pills-map-tab").on("click", function(){
            $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
            var interval = setInterval(function(){
                if($("#mapCommunity<?= $kunik?>").is(':visible')){
                    clearInterval(interval);
                    var tabContent = $('.mapTab');
                    if (tabContent.length) {
                        tabContent.css('height', `calc(60vh + 20px + ${$('.switcherFilter').css("height")})`);
                    }

                    map<?= $kunik ?> =  new CoMap({
                        container : "#mapCommunity<?= $kunik?>",
                        activePopUp : false,
                        mapOpt:{
                            menuRight : false,
                            btnHide : false,
                            doubleClick : false,
                            zoom : 3,
                            scrollWheelZoom: false,
                            onclickMarker: function(data) {
                                map<?= $kunik ?>.centerOne(data.elt.geo.latitude, data.elt.geo.longitude)
                                params = {};
                                params["elementId"] = costum.contextId;
                                params["userId"] = data.elt._id.$id;
                                params["name"] = data.elt.name;
                                params["socialNetwork"] = typeof data.elt["socialNetwork"] !== "undefined" ? data.elt["socialNetwork"] : [];
                                params["profileImg"] = typeof data.elt["profilMediumImageUrl"] !== "undefined" ? data.elt["profilMediumImageUrl"] : "";
                                params["badges"] = typeof data.elt["badges"] !== "undefined" ? data.elt["badges"] : [];
                                params["userCredits"] = typeof data.elt["userWallet"] !== "undefined" && typeof data.elt["userWallet"][costum.contextId] !== "undefined" && typeof data.elt["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? data.elt["userWallet"][costum.contextId]["userCredits"] : 0 ;
                                ajaxPost(
                                    null,
                                    baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
                                    params, 
                                    function (response) {  
                                        params["badgesDetails"] = response.badges;
                                        params["projects"] = response.projects;
                                        params["actionsCreated"] = response.actionsCreated;
                                        params["actionsParticipated"] = response.actionsParticipated;
                                        params["actionsLast12Month"] = response.actionsLast12Month;
                                        params["countAll"] = response.countAll;

                                        urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                                    }
                                );
                            }
                        },
                        elts : allData
                    });

                    $(".filterSelectMap").off("change").on("change", function(){
                        var key = $(this).val();
                        $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
                        $(`.filter<?= $kunik ?>LG button[data-key="${key}"]`).addClass("filterSelected");
                        var data =  dataMember<?= $kunik ?>.data[key]["users"];
                        map<?= $kunik ?>.clearMap();
                        map<?= $kunik ?>.addElts(data)
                    })
                }
            }, 100);
        });
    
        $("#pills-graph-tab").on("click", function(){
            var structuredData = [];
            var lists = {};
            let fieldPath =  `links.memberOf.${costum.contextId}.roles`;
            var authorizedTag = typeof roleToShow != "undefined" && Array.isArray(roleToShow) ? roleToShow : [];
            var dataCircle = {};
            if(typeof costum.contextType != "undefined" && costum.contextType == "organizations"){
                fieldPath = `links.memberOf.${costum.contextId}.roles`;
            }else if(typeof costum.contextType != "undefined" && costum.contextType == "projects"){
                fieldPath = `links.projects.${costum.contextId}.roles`;
            }else if(typeof costum.contextType != "undefined" && costum.contextType == "events"){
                fieldPath = `badges`;
            }else if (typeof costum.contextType != "undefined" && !["organizations", "projects", "events"].includes(costum.contextType)){
                fieldPath = `tags`;
            }
            $.each(allData, (key, index) => {
                var val = getValueByPath(allData[key], fieldPath);
                if(val && val!=""){
                    if(Array.isArray(val)){
                        for(var i in val){
                            if(typeof val[i]=="object"){
                                lists[val[i].name] = val[i]
                            }else{
                                lists[val[i]] = val[i];
                            }
                        }
                    }else if(typeof val=="object" && val!=null){
                        if(fieldPath=="badges"){
                            $.each(val, function(i, v){
                                lists[v.name] = v.name
                            });
                        }else{
                            $.each(val, function(k, v){
                                lists[k] = v.name;
                            });
                        }
                    }else if(typeof val=="string"){
                        lists[val] = val;
                    }
                    
                }
            });
            dataCircle = lists;

            if(authorizedTag.length == 0){
                authorizedTag = lists;
            }
            if(authorizedTag != null){
                $.each(authorizedTag, function(i, authTag){
                    var child = {
                        id:"element"+i,
                        label:(typeof authTag == "string")?authTag:i,
                        children: []
                    }
                    Object.keys(allData).forEach((key, index) => {
                        var element = allData[key];
                        let itIsIn = false;
                        if(typeof authTag == "object"){
                            for(var k in authTag){
                                if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                    itIsIn = true;
                                }
                            }
                        }
                        if(
                            child.label==getValueByPath(element, fieldPath)
                            || (Array.isArray(getValueByPath(element, fieldPath)) && getValueByPath(element, fieldPath).includes(child.label)) 
                            || itIsIn
                            ){
                                structuredData.push({
                                    "id":key,
                                    "label":element.name,
                                    "collection": element.collection,
                                    "type": child.label,
                                    "group": child.label,
                                    "img":element.profilThumbImageUrl,
                                    "profileImage": element.profilMediumImageUrl,
                                    "userWallet": element.userWallet,
                                    "socialNetwork": element.socialNetwork
                                });
                        }else if(fieldPath=="badges"){
                            if(typeof element.badges != "undefined" && element.badges != null){
                                $.each(element.badges, function(ieb, eb){
                                    if(eb.name==child.label){
                                        structuredData.push({
                                            "id":key,
                                            "label":element.name,
                                            "collection": element.collection,
                                            "type": eb.name,
                                            "group": eb.name,
                                            "img":element.profilThumbImageUrl,
                                            "profileImage": element.profilMediumImageUrl,
                                            "userWallet": element.userWallet,
                                            "socialNetwork": element.socialNetwork
                                        });
                                    }
                                });
                            }
                        }
                    });
                });
            }

            var intervalGraph = setInterval(() => {
                if($("#graph-container-<?= $kunik ?>").is(':visible')){
                    clearInterval(intervalGraph);
                    circle<?= $kunik ?> = new CircleGraph([], d => d.group, authorizedTag);
                    circle<?= $kunik ?>.draw("#graph-container-<?= $kunik ?>");
                    circle<?= $kunik ?>.updateData(structuredData);
                    circle<?= $kunik ?>.initZoom();
                    addFocuserFilter(authorizedTag);
                    var tabContent = $('.circleTab');
                    if (tabContent.length) {
                        tabContent.css('height', `calc( 20px + ${$('.switcherFilter').css("height")} + ${$('#graph-container-<?= $kunik ?>').css("height")})`);
                    }

                    circle<?= $kunik ?>.setOnClickNode((e,d,n) => {
                            params = {};
                            params["elementId"] = costum.contextId;
                            params["userId"] = d.data.id;
                            params["name"] = d.data.label;
                            params["type"] = d.data.collection;
                            params["socialNetwork"] = typeof d.data.socialNetwork != 'undefined' ? d.data.socialNetwork : [];
                            params["profileImg"] = typeof d.data.profileImage != 'undefined' ? d.data.profileImage : "";
                            params["userCredits"] = typeof d.data.credits != 'undefined' ? d.data.credits : 0;
                            ajaxPost(
                                null,
                                baseUrl + "/co2/person/getuserbadgeandrecentcontribution",
                                params, 
                                function (response) {  
                                    params["badgesDetails"] = response.badges;
                                    params["projects"] = response.projects;
                                    params["actionsCreated"] = response.actionsCreated;
                                    params["actionsParticipated"] = response.actionsParticipated;
                                    params["actionsLast12Month"] = response.actionsLast12Month;
                                    params["countAll"] = response.countAll;

                                    urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                                }
                            );
                        })
                }
            }, 100);
        });
    });

    const getValueByPath = (object, path) => {
        if (path === undefined || path === null) {
            return object;
        }
        const parts = path.split('.');
        for (let i = 0; i < parts.length; ++i) {
            if (object === undefined || object === null) {
                return undefined;
            }
            const key = parts[i];
            object = object[key];
        }
        return object;
    }
    function addFocuserFilter(tags){
        var authTagsData = (Array.isArray(tags))?tags:Object.keys(tags);
        authTagsData = authTagsData.map(v=> v.trim());
        authTagsData = (new Set(authTagsData));
        d3.select("div#focus-container<?= $kunik ?>")
        .selectAll("button")
        .data(authTagsData)
        .join((enter) => {
            enter.append("xhtml:button")
                .text(d => d)
                .classed("btn margin-5 btn-circle-zoomer", true)
                .on("click", (e,d) => {
                var thisElement = $(e.target);
                if(activeCircle<?= $kunik ?> != d){
                    circle<?= $kunik ?>.focus(d);
                    activeCircle<?= $kunik ?> = d;
                    $(".btn-circle-zoomer").removeClass("btn-primary");
                    thisElement.addClass("btn-primary");
                }else{
                    activeCircle<?= $kunik ?> = "";
                    thisElement.removeClass("btn-primary");
                    circle<?= $kunik ?>.unfocus().then(() => {
                        mylog.log("UNFOCUSED");
                    });
                }
                
                $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                $("#"+GraphUtils.slugify(d)).addClass("in");
            })
        });
    }
</script>
