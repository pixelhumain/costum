<?php

/**
 * @deprecated
 */
$css_and_scripts = [
    "/plugins/carousel/css/carousel.css",
    "/plugins/carousel/js/carousel.js"
];
HtmlHelper::registerCssAndScriptsFiles($css_and_scripts, Yii::app()->request->baseUrl);

$keyTpl = "coevent_acteur_participant";
$paramsData = [
    "blockCmsBgColor" => "transparent",
    "textColorTheme" => "black",
    "primaryColor" => "#EE302C"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

$subEventCostums = isset($this->costum["communityLinks"]["subEvents"]) ? $this->costum["communityLinks"]["subEvents"] : [];
$citoyen_keys = [];
$acteurs = [];

foreach ($subEventCostums as $key => $value) {
    $temporary_contributor = [];
    $temporary_contributor = PHDB::findOne(Event::COLLECTION, array("_id" => new MongoId($key)), ["links"]);
    if (isset($temporary_contributor["links"]["attendees"])) {
        foreach ($temporary_contributor["links"]["attendees"] as $contributor_key => $contributor) {
            if (!array_key_exists($contributor_key, $citoyen_keys)) $citoyen_keys[$contributor_key] = $contributor;
        }
    }
}

foreach ($citoyen_keys as $citoyen_key => $citoyen_value) {
    $temporary = PHDB::findOne(Citoyen::COLLECTION, array("_id" => new MongoId($citoyen_key)), ["name", "profilImageUrl"]);
    $citoyen = [
        "name" => $temporary["name"],
        "profile" => isset($temporary["profilImageUrl"]) && !empty($temporary["profilImageUrl"]) ? $temporary["profilImageUrl"] : "/upload/communecter/events/thumbnail-default.jpg",
    ];
    $acteurs[] = $citoyen;
}
?>
<style>
    .slider {
        height: 100%;
        width: 100%;
        margin: 0;
        overflow: hidden;
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }

    .slides {
        list-style: none;
    }

    .class<?= $blockKey ?>card-content {
        background-color: white;
    }

    .class<?= $blockKey ?>footer-section {
        padding-top: 5%;
        background-color: <?= $paramsData["blockCmsBgColor"] ?>;
        background-image: url('<?= Yii::app()->getModule("costum")->getAssetsUrl(); ?>/images/coevent/footer.png');
        background-repeat: no-repeat;
        background-size: contain;
        background-position: 0 100%;
        height: 800px;
    }

    .class<?= $blockKey ?>card-acteurs {
        width: 1024px;
        margin-left: auto;
        margin-right: auto;
        padding: 1em 1em 4em;
        display: flex;
        justify-content: space-between;
    }

    .class<?= $blockKey ?>card-acteur {
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, .5);
        overflow: hidden;
    }

    .class<?= $blockKey ?>card-acteur .class<?= $blockKey ?>card-header {
        height: 200px;
        width: 300px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

    .class<?= $blockKey ?>card-acteur .class<?= $blockKey ?>card-content {
        position: relative;
        padding: 2em;
        font-weight: bold;
    }

    .class<?= $blockKey ?>card-acteur .class<?= $blockKey ?>card-content .class<?= $blockKey ?>badget {
        font-size: 3em;
        display: block;
        color: white;
        background-color: <?= $paramsData["primaryColor"] ?>;
        border-radius: 5px;
        position: absolute;
        left: 15px;
        top: 0;
        transform: translateY(-50%);
        padding: .1em .2em;
    }

    .class<?= $blockKey ?>card-acteur .class<?= $blockKey ?>card-content .class<?= $blockKey ?>nom {
        color: <?= $paramsData["primaryColor"] ?>;
        display: block;
    }

    .class<?= $blockKey ?>slide-container {
        position: relative;
        width: 100%;
        overflow: hidden;
    }
</style>
<div class="row class<?= $blockKey ?>footer-section">
    <div>
        <div class="col-md-12">
            <h1 class="dinalternate text-center" style="color: <?= $paramsData["textColorTheme"] ?>;">
                Acteurs et participants
            </h1>
        </div>
        <div class="col-xs-12">
            <div class="container" style="position: relative;">
                <img src="<?= Yii::app()->getModule("costum")->getAssetsUrl(); ?>/images/coevent/points_block_membre.png" alt="block membre" style="width: 100px; position: absolute; top: 20px; left: 0;">
                <p id="id<?= $blockKey ?>coevent-acteur-message" class="dinalternate text-center"></p>
                <div class="row" id="id<?= $blockKey ?>coevent-acteur-container">
                    <div class="slider">
                        <ul class="slides" id="id<?= $blockKey ?>coevent-acteur">
                            <!-- acteurs -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var contributors = <?= json_encode($acteurs) ?>;
    // retrieve contributor keys from the context

    sectionDyf.<?= $kunik ?>ParamsData = <?= json_encode($paramsData) ?>;
    sectionDyf.<?= $kunik ?>Params = {
        jsonSchema: {
            title: `<?= Yii::t('cms', 'Set up your section') ?>`,
            description: `<?= Yii::t('cms', 'Customize your section') ?>`,
            icon: 'fa-cog',
            properties: {
                textColorTheme: {
                    label: tradCms.textcolor,
                    inputType: 'colorpicker',
                    values: sectionDyf.<?= $kunik ?>ParamsData.textColorTheme
                },
                primaryColor: {
                    label: 'Couleur primaire',
                    inputType: 'colorpicker',
                    values: sectionDyf.<?= $kunik ?>ParamsData.primaryColor
                }
            },
            beforeBuild: function() {
                uploadObj.set('cms', '<?= $blockKey ?>');
            },
            save: function() {
                tplCtx.value = {}
                for (var property in sectionDyf.<?= $kunik ?>Params.jsonSchema.properties) {
                    tplCtx.value[property] = $(`#${property}`).val()
                }
                mylog.log("save tplCtx", tplCtx)
                if (tplCtx.value === undefined) toastr.error('value cannot be empty!')
                else {
                    dataHelper.path2Value(tplCtx, function(params) {
                        dyFObj.commonAfterSave(params, function() {
                            toastr.success('<?= Yii::t('cms', 'Element well added') ?>')
                            $('#ajax-modal').modal('hide')
                            urlCtrl.loadByHash(location.hash)
                        })
                    })
                }
            }
        }
    };

    function coevent_acteur_display_contributors() {
        let coevent_acteur_display_html = '<li class="slide"><div class="class<?= $blockKey ?>card-acteurs">'
        if (contributors.length > 0) {
            for (var i = 0; i < contributors.length; i++) {
                if (i != 0 && i % 3 == 0) coevent_acteur_display_html += '</div></li><li class="slide"><div class="class<?= $blockKey ?>card-acteurs">'
                coevent_acteur_display_html += `
            <div class="class<?= $blockKey ?>card-acteur">
                <div class="class<?= $blockKey ?>card-header" style="background-color: rgba(0, 0, 0, .5); background-image: url(${contributors[i].profile});">
                </div>
                <div class="class<?= $blockKey ?>card-content">
                    <span class="fa fa-user class<?= $blockKey ?>badget"></span>
                    <span class="class<?= $blockKey ?>nom">${contributors[i].name}</span>
                    <span class="class<?= $blockKey ?>description">
                        
                    </span>
                </div>
            </div>
            `
            }
            coevent_acteur_display_html += '</div></li>'
            coevent_acteur_display_html += '</div></li>'
            $('#id<?= $blockKey ?>coevent-acteur-message').text('');
            $('#id<?= $blockKey ?>coevent-acteur-container').css({
                display: 'block'
            })
            $('#id<?= $blockKey ?>coevent-acteur').html(coevent_acteur_display_html)
        } else {
            $('#id<?= $blockKey ?>coevent-acteur-message').text('Aucun acteur ne participe à cet évènement');
        }
    }

    function coevent_acteur_reload_carousel() {
        var coevent_acteur_carousel = $carousel({
            container: $('#id<?= $blockKey ?>coevent-acteur'),
            autoplay: false,
            navigation: {
                indicator: true,
                button: {
                    state: true,
                    next: '<span class="fa fa-chevron-right"></span>',
                    prev: '<span class="fa fa-chevron-left"></span>'
                },
            },
            animationDuration: 200,
            delay: 800
        })
        coevent_acteur_carousel.init()
        coevent_acteur_carousel.start()
    }

    coevent_acteur_display_contributors();
    if (contributors.length > 0) coevent_acteur_reload_carousel();
</script>