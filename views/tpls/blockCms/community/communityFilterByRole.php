<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
?>


<style id="carousel<?= $kunik ?>">
  
  .image-grid<?= $kunik?> {
    /* display: grid;
    grid-template-columns: repeat(auto-fit, minmax(200px, 1fr)); */
    display: flex;
    justify-content: center;
    align-items: center; 
    flex-wrap: wrap;       
    background-color: white;
    border-radius: 10px;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
    transition: transform 0.3s ease;
    margin: 5px;
    padding: 15px;
  }

  .join<?= $kunik?> {
    display: grid;
    justify-content: center;
    align-items: center; 
    background-color: white;
    border-radius: 10px;
    box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);
    transition: transform 0.3s ease;
    margin: 5px;
    padding: 15px;
  }

  .grid-item<?= $blockKey ?> {
      flex: 1 1 200px;
      width: 100%;
      height: 200px;
      display: block;
      margin: 5px;
  }


  .box<?= $blockKey ?>{
        height: 100% ;
        width: 100%;
        background-size: auto;
        background-repeat: no-repeat;
        background-position: center;
        filter: grayscale(100%);
        transition: filter 0.3s ease;
        border-radius: 10px;
        margin: 5px;
  }

  .box<?= $blockKey ?>:hover {
    filter: grayscale(0%);
    transform : scale(1.10093);
    transition : all .5s cubic-bezier(.7,0,.3,1);
    z-index: 2;
    border: 1px solid white;
  }

  .box<?= $blockKey ?> {position:relative;text-align:center;box-shadow:0 0 3px rgba(0,0,0,.3);overflow:hidden}
  .box<?= $blockKey ?> .post{text-transform:capitalize}
  .box<?= $blockKey ?>:before{content:"";width:100%;background:rgba(0,0,0,.5);height:100%;position:absolute;top:0;left:0;opacity:0;transition:all .35s ease 0s}
  .box<?= $blockKey ?>:hover:before{opacity:1}
  .box<?= $blockKey ?> img{width:100%;height:auto}
  .box<?= $blockKey ?> .box-content{width:90%;height:90%;position:absolute;top:5%;left:5%}
  .box<?= $blockKey ?> .box-content:after,.box<?= $blockKey ?> .box-content:before{content:"";position:absolute;top:0;left:0;bottom:0;right:0;opacity:0;transition:all .7s ease 0s}
  .box<?= $blockKey ?> .box-content:before{border-bottom:1px solid rgba(255,255,255,.5);border-top:1px solid rgba(255,255,255,.5);transform:scale(0,1);transform-origin:0 0 0}
  .box<?= $blockKey ?> .box-content:after{border-left:1px solid rgba(255,255,255,.5);border-right:1px solid rgba(255,255,255,.5);transform:scale(1,0);transform-origin:100% 0 0}
  .box<?= $blockKey ?>:hover .box-content:after,.box<?= $blockKey ?>:hover .box-content:before{opacity:1;transform:scale(1);transition-delay:.15s}
  .box<?= $blockKey ?> .title{font-size:15px;font-weight:700;color:#fff;margin:15px 0;opacity:0;transform:translate3d(0,-50px,0);transition:transform .5s ease 0s}
  .box<?= $blockKey ?>:hover .title{opacity:1;transform:translate3d(0,0,0)}
  .box<?= $blockKey ?> .post{font-size:14px;color:#fff;padding:10px;background:#d79719;opacity:0;border-radius:0 19px;transform:translate3d(0,-50px,0);transition:all .7s ease 0s}
  .box<?= $blockKey ?> .icon,.box15 .icon{padding:0;list-style:none}
  .box<?= $blockKey ?>:hover .post{opacity:1;transform:translate3d(0,0,0);transition-delay:.15s}
  .box<?= $blockKey ?> .icon{width:100%;margin:0;position:absolute;bottom:-10px;left:0;opacity:0;z-index:1;transition:all .7s ease 0s}
  .box<?= $blockKey ?>:hover .icon{bottom:20px;opacity:1;transition-delay:.15s}
  .box<?= $blockKey ?> .icon li button{display:block;width:40px;height:40px;line-height:40px;border:1px solid #fff;border-radius:0 16px;font-size:14px;color:#fff;margin-right:5px;transition:all .4s ease 0s}
  .box<?= $blockKey ?> .icon li button:hover{background:#e54e5e;border-color:#e54e5e}
  .box<?= $blockKey ?> .icon li,.box<?= $blockKey ?> .post{display:inline-block}
  .box<?= $blockKey ?>:hover .generic-avatar{opacity:0}
  @media only screen and (max-width:990px){.box<?= $blockKey ?>{margin-bottom:30px}
  }

  .mobile-select-filter<?= $blockKey ?>{
    display: inline-block;
	  position: relative;
    margin-right: 10px;
    padding: 10px 20px;
    border-radius: 5px;
    background-color: #fff;
    color: #000;
    box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
  }

  .filter<?= $kunik ?>SMXS{
    display: flex;
    justify-content: center;
    align-items: center; 
    flex-wrap: wrap;  
    box-shadow:0 0 3px rgba(0,0,0,.3);
    margin: 5px;
    padding: 15px;
    border-radius: 10px;
    background-color: white;
  }

  .float-shadow<?= $blockKey ?> {
	display: inline-block;
	position: relative;
	transition-duration: $defaultDuration;
	transition-property: transform;
  margin-right: 10px;
  padding: 10px 20px;
  border-radius: 5px;
  background-color: #fff;
  color: #000;
  box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.2);
  transition: background-color 0.3s ease;

	@include hideTapHighlightColor();
	@include hardwareAccel();
	@include improveAntiAlias();

	&:before {
		pointer-events: none;
	  position: absolute;
	  z-index: -1;
	  content: '';
	  top: 100%;
	  left: 5%;
	  height: 10px;
	  width: 90%;
	  opacity: 0;
	  background: radial-gradient(ellipse at center, rgba(0,0,0,.35) 0%,rgba(0,0,0,0) 80%); /* W3C */
		transition-duration: $defaultDuration;
		transition-property: transform opacity;
	}

	&:hover {
		transform: translateY(-5px); /* move the element up by 5px */

		&:before {
			opacity: 1;
			transform: translateY(5px); /* move the element down by 5px (it will stay in place because it's attached to the element that also moves up 5px) */
		}
	}
}


  .filter<?= $kunik ?>LG{
    display: flex;
    justify-content: center;
    align-items: center; 
    flex-wrap: wrap;  
    box-shadow:0 0 3px rgba(0,0,0,.3);
    margin: 5px;
    padding: 15px;
    border-radius: 10px;
    background-color: white;
  }

  .filter<?= $kunik ?>LG button.filterSelected {
    background-color: black;
    color: white;
  }

  .generic-avatar {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .generic-avatar i {
    font-size: 48px; /* Taille de l'icône utilisateur */
    color: #999;
  }

  .joinUs{
    font-size: 2em !important;
  }

  .btn-download-link-qr-node-container<?= $kunik ?> {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .3);
        display: none;
        justify-content: center;
        align-items: center;
        opacity: 0;
        transition: .3s;
    }

    .btn-download-link-qr-node-container<?= $kunik ?>:hover {
        opacity: 1;
    }

    .btn-download-link-qr-node-container<?= $kunik ?> button {
        border: none;
        width: 50px;
        height: 50px;
        background-color: black;
        color: white;
        text-align: center;
        line-height: 50px;
        font-size: 20px;
        border-radius: 100%;
    }

    .invitation-link-qr-container{
      margin-top: 10px;
      border-radius: 4px;
      position: relative;
    }


</style>
<div class="<?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
  <span class="title sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></span>
    <div class="filter<?= $kunik ?>LG hidden-sm hidden-xs"></div>
    <div class="filter<?= $kunik ?>SMXS hidden-lg">
        <select class="mobile-select-filter<?= $blockKey ?>" onchange="filterSelection(this.value)"></select>
    </div>
    <div class="image-grid<?= $kunik?>"></div>
</div>



<script type="text/javascript">

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#carousel<?= $kunik ?>").append(str);
    if (costum.editMode){   
        cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
        communityFilter = {
            configTabs : {
                general : {
                    inputsConfig : [
                        "background",
                        {
                          type : "selectMultiple",
                          options : {
                            name : "roleToShow",
                            label : tradDynForm.roles,
                            canDragAndDropChoise : true,
                          }
                        },
                    ]
                },
                style: {
                    inputsConfig : [
                        {
                            type: "section",
                            options: {
                                name: "carouselCard",
                                    label: "Card" ,
                                    showInDefault: false,
                                    inputs: [
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
                        "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                },
            onChange : function(path,valueToSet,name,payload,value){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                cmsConstructor.helpers.refreshBlock(id, ".cmsbuilder-block[data-id='"+id+"']");
                costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "refreshBlock", 
                                data:{ id: id}
                })
            }
            
        }
        cmsConstructor.blocks.communityFilterByRole<?= $blockKey ?> = communityFilter;
        
    } 
    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["title"]) ?>,"<?= $blockKey ?>");

    //populate the list 

    params<?= $kunik ?> = {};

    params<?= $kunik ?>["searchType"] = ["citoyens"];
    params<?= $kunik ?>["indexMin"] = 0;
    params<?= $kunik ?>["initType"] = "";
    params<?= $kunik ?>["count"] = true;
    params<?= $kunik ?>["countType"] = ["citoyens"];
    params<?= $kunik ?>["indexStep"] = 6000;
    params<?= $kunik ?>["notSourceKey"] = true;
    params<?= $kunik ?>["fediverse"] = false;

    filters<?= $kunik ?> = {'$or':{},'toBeValidated' : {'$exists':false}};
    filters<?= $kunik ?>['$or']["parent."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["source.keys"] = (costum.contextId)?costum.slug:contextData.slug;
    filters<?= $kunik ?>['$or']["links.projects."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["links.events."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    filters<?= $kunik ?>['$or']["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};

    params<?= $kunik ?>['filters'] = filters<?= $kunik ?>

    fields<?= $kunik ?> = {};
    fields<?= $kunik ?>["tags"] = "tags";
    fields<?= $kunik ?>["collection"] = "collection"
    fields<?= $kunik ?>["name"] = "name"
    fields<?= $kunik ?>["email"] = "email"
    fields<?= $kunik ?>["socialNetwork"] = "socialNetwork"
    fields<?= $kunik ?>["userWallet"] = "userWallet"
    fields<?= $kunik ?>["links"] = "links"
    fields<?= $kunik ?>["profilMediumImageUrl"] = "profilMediumImageUrl"
    fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
    fields<?= $kunik ?>["type"] = trad.type;
    fields<?= $kunik ?>["address.level1Name"] = "Pays";
    fields<?= $kunik ?>["address.level3Name"] = "Région";
    fields<?= $kunik ?>["badges"] = "badges";
    fields<?= $kunik ?>["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)+".roles"] = "Rôles";
    if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
        fields<?= $kunik ?>["tags"] = trad.tags;
    }

    params<?= $kunik ?>['fields'] = fields<?= $kunik ?>;


    function mergeArrays(arr1, arr2) {
      arr2.forEach(element => {
        // Check if the element already exists in arr1
        if (!arr1.includes(element)) {
          arr1.push(element); // Add it if it's not already there
        }
      });
      return arr1;
    }

    function createGridItem(value, key, blockKey) {
      const hasImage = value.profilMediumImageUrl && value.profilMediumImageUrl !== '';
      const avatar = hasImage
        ? ''
        : `<div class="generic-avatar">
            <i class="fa fa-user"></i> <!-- Icon for user -->
            <h3 class="user-name">${value.name}</h3>
          </div>`;

      return `
        <div class="grid-item${blockKey}">
          <div class="box${blockKey} carouselCard" style="${hasImage ? `background-image:url(${value.profilMediumImageUrl});` : 'display: flex; justify-content: center; align-items: center; background: none;'}">
            ${avatar}
            <div class="box-content">
              <h3 class="title">${value.name}</h3>
              <ul class="icon">
                <li><button class="subtitle" data-id="${key}" onclick="showDetails(this.getAttribute('data-id'))">
                  <i class="fa fa-eye"></i></button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      `;
    }
    
    var allData = "";
    
    ajaxPost(
        null,
        baseUrl+"/co2/search/globalautocomplete",
        params<?= $kunik ?>,
        function(response){
          var tags = [];
          var item = `
              <div class="grid-item<?= $blockKey ?> inviteLink<?= $kunik ?> <?= @$linksBtn["isMember"] == true ? "hidden" : "" ?>">
                <div class="box<?= $blockKey ?> carouselCard inviteLink<?= $blockKey ?>" >
                  <div class="box-content">
                    <h3 class="title joinUs" ><?php echo Yii::t("cms", "Join Us")?></h3>
                    <ul class="icon">
                      <li>
                        <button id="btn-download-link-qr-node<?= $kunik ?>"><i class="fa fa-download" aria-hidden="true"></i></button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>`;
          var filter = ``;
          var filterXS = "";
          var roleToShow = <?= json_encode($blockCms["roleToShow"]) ?>;
          allData = response.results;
          $.each(response.results,function(k,v){
            if ( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object"){
              tags = mergeArrays(tags, jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles"));
            }

            if ( roleToShow.length > 0 ){
                if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").indexOf(roleToShow[0]) >= 0 ){
                  item += createGridItem(v, k, "<?= $blockKey ?>");
                }
            } else {
              item += createGridItem(v, k, "<?= $blockKey ?>");
            }
            
            
          })

          if ( roleToShow.length > 0 ) {
            roleToShow.forEach((value, index) => {
              if (index === 0) {
                  filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?> filterSelected">${value}</button> `;
                  filterXS += `<option value="${value}" selected>${value}</option>`;
              } else {
                  filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?> ">${value}</button> `;
                  filterXS += `<option value="${value}">${value}</option>`;
              }
            });
          } else {
            tags.forEach((value, index) => {
                filter += `<button data-key="${value}" class="btn margin-5 btn-circle-zoomer float-shadow<?= $blockKey ?>">${value}</button> `;
                filterXS += `<option value="${value}">${value}</option>`;
            });
          }
          
          if (costum.editMode) {
            cmsConstructor.blocks.communityFilterByRole<?= $blockKey ?>.configTabs.general.inputsConfig[1].options.options = 
            $.map(tags, function(value, index) {
                return { value: value, label: value };
            });
          }
          

          $(".filter<?= $kunik ?>LG").append(filter);
          $(".mobile-select-filter<?= $blockKey ?>").append(filterXS);
          $(".image-grid<?= $kunik?>").append(item);
          if(costum.editMode) $(".inviteLink<?= $kunik ?>").removeClass("hidden")
          createQR();
        }
    );

    $(document).on('click', `.filter<?= $kunik ?>LG button`, function() {
      var item = "";
      var firstElement = $(".image-grid<?= $kunik?>").children().first().detach(); 
      $(`.filter<?= $kunik ?>LG button`).removeClass("filterSelected");
      $(this).addClass("filterSelected");
      $(".image-grid<?= $kunik?>").html("");
      var key = $(this).data("key");
      $.each(allData,function(k,v){
        if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").includes(key)){
          item += createGridItem(v, k, "<?= $blockKey ?>");
        }
            
      });
      $(".image-grid<?= $kunik?>").append(firstElement);
      $(".image-grid<?= $kunik?>").append(item);
    });


    function filterSelection(key){
        var item = "";
        var firstElement = $(".image-grid<?= $kunik?>").children().first().detach(); 
        $(".image-grid<?= $kunik?>").html("");
        $.each(allData,function(k,v){
          if( typeof jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles") === "object" && jsonHelper.getValueByPath(v,"links.memberOf."+costum.contextId+".roles").includes(key)){
            item += createGridItem(v, k, "<?= $blockKey ?>");
          }
              
        });
        $(".image-grid<?= $kunik?>").append(firstElement);
        $(".image-grid<?= $kunik?>").append(item);
    }

    function showDetails(id){
      params = {};
      params["userId"] = id;
      params["name"] = allData[id]["name"];
      params["socialNetwork"] = typeof allData[id]["socialNetwork"] !== "undefined" ? allData[id]["socialNetwork"] : [];
      params["profileImg"] = typeof allData[id]["profilMediumImageUrl"] !== "undefined" ? allData[id]["profilMediumImageUrl"] : "";
      params["badges"] = typeof allData[id]["badges"] !== "undefined" ? allData[id]["badges"] : [];
      params["userCredits"] = typeof allData[id]["userWallet"] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId] !== "undefined" && typeof allData[id]["userWallet"][costum.contextId]["userCredits"] !== "undefined"  ? allData[id]["userWallet"][costum.contextId]["userCredits"] : 0 ;
      ajaxPost(
        null,
        baseUrl + "/co2/person/getuserbadgeandrecentcontribution/",
        params, 
        function (response) {  
          params["badgesDetails"] = response.badges;
          params["projects"] = response.projects;
          params["actionsCreated"] = response.actionsCreated;
          params["actionsParticipated"] = response.actionsParticipated;
          params["actionsLast12Month"] = response.actionsLast12Month;
          params["countAll"] = response.countAll;

          urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
        }
      );
    }
    

    function createQR() {
        new AwesomeQR.AwesomeQR({
            text: "<?= Yii::app()->getBaseUrl(true) . "/co2/link/connect/ref/" . $invitelink["ref"] ?>",
            size: 200,
            logoImage: typeof costum.logoMin !== "undefined" ? costum.logoMin : "",
            logoScale: 0.25,
            dotScale: 0.6
        }).draw().then(function(dataUrl) {
            $('.inviteLink<?= $blockKey ?>').css('background-image', `url(${dataUrl})`);
            $(".btn-download-link-qr-node-container<?= $kunik ?>").css({
                display: "flex"
            })

            $("#btn-download-link-qr-node<?= $kunik ?>").click(function() {
                var link = document.createElement('a');
                link.href = dataUrl;
                link.download = `qrlink-${costum.slug}-${moment().format("DD-MM-YYYY")}.png`;
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            })
        })
    }

</script>