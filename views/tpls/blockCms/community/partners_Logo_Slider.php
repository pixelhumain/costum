<?php 
$keyTpl = "partners_Logo_Slider";
$paramsData=[
	"role" => [],
	"slide" => true

];


if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/js/blockcms/slick.js"], $assetsUrl);
$rolesLists = [];
if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
	if($costum["contextType"] == Organization::COLLECTION ){
		if (isset($el["links"]["members"])) {
			  foreach ($el["links"]["members"] as $e => $v) {
				if (isset($v["roles"]) && isset($v["type"]) && $v["type"] == Organization::COLLECTION) {
					  foreach ($v["roles"] as $role) {
						  if (!empty($role)) {
							if (!isset($rolesLists[$role])) {
								$rolesLists[$role] = $role;
							} 
						  }
					}
				}
			}
		}

	}else if($costum["contextType"] == Project::COLLECTION ){
		if (isset($el["links"]["contributors"])) {
			foreach ($el["links"]["contributors"] as $e => $v) {
			  if (isset($v["roles"]) && isset($v["type"]) && $v["type"] == Organization::COLLECTION) {
					foreach ($v["roles"] as $role) {
						if (!empty($role)) {
							if (!isset($rolesLists[$role])) {
								$rolesLists[$role] = $role;
							} 
						}
				  	}
			  	}
		  	}
	  	}
	}
}
?>
<style type="text/css">
	.block_<?= $kunik?> h2{
	  padding: 20px;
	}
	.block_<?= $kunik?> section {
	    padding: 20px 0;
	}
	.block_<?= $kunik?> .customer-logos .slide h5{
		text-transform: none;
	    box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
	    min-height: 60px;
	    vertical-align: baseline;
	    display: flex;
	    justify-content: center;
	    align-items: center;
	    text-align: center;
	}
	/* Slider */

	.block_<?= $kunik?> .slick-slide {
	    margin: 0px 20px;
	}

	.block_<?= $kunik?> .slick-slide img {
	    width: 100%;
	}

	.block_<?= $kunik?> .slick-slider
	{
	    position: relative;
	    display: block;
	    box-sizing: border-box;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	            user-select: none;
	    -webkit-touch-callout: none;
	    -khtml-user-select: none;
	    -ms-touch-action: pan-y;
	        touch-action: pan-y;
	    -webkit-tap-highlight-color: transparent;
	}

	.block_<?= $kunik?> .slick-list
	{
	    position: relative;
	    display: block;
	    overflow: hidden;
	    margin: 0;
	    padding: 0;
	}
	.block_<?= $kunik?> .slick-list:focus
	{
	    outline: none;
	}
	.block_<?= $kunik?> .slick-list.dragging
	{
	    cursor: pointer;
	    cursor: hand;
	}

	.block_<?= $kunik?> .slick-slider .slick-track,
	.block_<?= $kunik?> .slick-slider .slick-list
	{
	    -webkit-transform: translate3d(0, 0, 0);
	       -moz-transform: translate3d(0, 0, 0);
	        -ms-transform: translate3d(0, 0, 0);
	         -o-transform: translate3d(0, 0, 0);
	            transform: translate3d(0, 0, 0);
	}

	.block_<?= $kunik?> .slick-track
	{
	    position: relative;
	    top: 0;
	    left: 0;
	    display: block;
	}
	.block_<?= $kunik?> .slick-track:before,
	.block_<?= $kunik?> .slick-track:after
	{
	    display: table;
	    content: '';
	}
	.block_<?= $kunik?> .slick-track:after
	{
	    clear: both;
	}
	.block_<?= $kunik?> .slick-loading .slick-track
	{
	    visibility: hidden;
	}

	.block_<?= $kunik?> .slick-slide
	{
	    display: none;
	    float: left;
	    height: 100%;
	    min-height: 1px;
	}
	.block_<?= $kunik?> [dir='rtl'] .slick-slide
	{
	    float: right;
	}
	.block_<?= $kunik?> .slick-slide img
	{
	    display: block;
	}
	.block_<?= $kunik?> .slick-slide.slick-loading img
	{
	    display: none;
	}
	.block_<?= $kunik?> .slick-slide.dragging img
	{
	    pointer-events: none;
	}
	.block_<?= $kunik?> .slick-initialized .slick-slide
	{
	    display: block;
	}
	.block_<?= $kunik?> .slick-loading .slick-slide
	{
	    visibility: hidden;
	}
	.block_<?= $kunik?> .slick-vertical .slick-slide
	{
	    display: block;
	    height: auto;
	    border: 1px solid transparent;
	}
	.block_<?= $kunik?> .slick-arrow.slick-hidden {
	    display: none;
	}
	<?php if($paramsData["slide"] != true){ ?>	
		.block_<?= $kunik?> section {
			display: flex;
			flex-wrap: wrap;
			place-content: space-between;
		}
		.block_<?= $kunik?> .customer-logos .slide {
			width: 15%;
			flex-basis: auto;
			margin-bottom : 1%;
		}
	<?php } ?>
</style>

<div class="block_<?= $kunik?>">
	<div class="container">
	  	<?php
		if(!empty($paramsData["role"])){
			$partners =Element::getCommunityByTypeAndId($costum["contextType"], $costum["contextId"], Organization::COLLECTION,null,$paramsData["role"]);
		}else{
			$partners =Element::getCommunityByTypeAndId($costum["contextType"], $costum["contextId"], Organization::COLLECTION);
		}

		$partner = array();
		foreach ($partners as $keyElt => $valueElt) {
			array_push($partner, Element::getElementSimpleById($keyElt,Organization::COLLECTION,null,["name", "profilMediumImageUrl","collection"]));
		}
		if (!empty($partner)) {
			?>
		   <section class="customer-logos customer-logos-<?= $kunik?> slider">
		   		<?php
				foreach ($partner as $key => $value) { ?>
					<div class="slide">
						<?php if (empty($value["profilMediumImageUrl"]) || $value["profilMediumImageUrl"]== null ) {?>
							<h5><?=  $value["name"];?></h5>
						<?php
						}else{
							$img = Yii::app()->request->baseUrl.$value["profilMediumImageUrl"];
						?>
                        <a href="#page.type.<?= $value["collection"]?>.id.<?= (String) $value["_id"] ?>" class="lbh-preview-element">
						    <img src="<?=  $img ?>" class="part img-responsive">
                        </a>
						<?php
					}
					?>
					</div>
				<?php } ?>
		   </section>
		   <?php
		 }else { ?>
			<div class="text-center padding-30">
                <?php echo Yii::t('cms', 'No partner')?>
			</div>
		<?php }	?>
	   
	</div>
</div>


<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
   	var rolesLists =  <?= json_encode($rolesLists) ?>;
	jQuery(document).ready(function() {
		if(sectionDyf.<?php echo $kunik ?>ParamsData.slide){
			$('.block_<?= $kunik?> .customer-logos').slick({
			    slidesToShow: 6,
			    slidesToScroll: 1,
			    autoplay: true,
			    autoplaySpeed: 1500,
			    arrows: false,
			    dots: false,
			    pauseOnHover: true,
			    responsive: [{
			        breakpoint: 768,
			        settings: {
			            slidesToShow: 4
			        }
			    }, {
			        breakpoint: 520,
			        settings: {
			            slidesToShow: 3
			        }
			    }]
			});
		}
		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					"colorTitle":{
						label : "<?php echo Yii::t('cms', 'Title color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.colorTitle
					},
					"role": {
                        "label": "Rôles",
                        "inputType": "selectMultiple",
                        "placeholder": "",
                        "class": "multi-select",
                        "isSelect2": true,
                    	"options" : rolesLists
                    },
					"slide" : { 
						"label": "Slide",
						"inputType" : "checkboxSimple",
						"params" : checkboxSimpleParams,
					},
				},
	            beforeBuild : function(){
	                uploadObj.set("cms","<?php echo $blockKey ?>");
	            },
				save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
	                }
                }
			}

		};
		mylog.log("paramsData",sectionDyf);
		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>
