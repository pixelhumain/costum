<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
?>

<style id="carousel<?= $kunik ?>">
  .caroussel-container<?= $kunik?> .swiper-container {
    width: 100%;
    height: auto;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 10px;
    box-shadow: 0 15px 30px rgb(0 0 0 / 20%);
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;
    padding: 45px 0px;
    overflow: visible;
  }
  .caroussel-container<?= $kunik?> .hand{
      position: absolute;
      right: -131px;
      bottom: -137px;
      width: 50%;
  }

  .caroussel-container<?= $kunik?> .swiper-slide{
    display: flex;
    justify-content: center;
  }

  .caroussel-container<?= $kunik?> .swiper-slide .carouselCard{
    position: relative;
    height: 200px;
    width: 200px;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
  }

  .caroussel-container<?= $kunik?> .title{
    margin-bottom: 15px;
  }
  .caroussel-container<?= $kunik?> .description{
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3; /* number of lines to show */
    -webkit-box-orient: vertical;
    font-size: 13px;
  } 

  .box<?= $blockKey ?> {position:relative;text-align:center;box-shadow:0 0 3px rgba(0,0,0,.3);overflow:hidden}
  .box<?= $blockKey ?> .post{text-transform:capitalize}
  .box<?= $blockKey ?>:before{content:"";width:100%;height:100%;background:rgba(0,0,0,.5);position:absolute;top:0;left:0;opacity:0;transition:all .35s ease 0s}
  .box<?= $blockKey ?>:hover:before{opacity:1}
  .box<?= $blockKey ?> img{width:100%;height:auto}
  .box<?= $blockKey ?> .box-content{width:90%;height:90%;position:absolute;top:5%;left:5%}
  .box<?= $blockKey ?> .box-content:after,.box<?= $blockKey ?> .box-content:before{content:"";position:absolute;top:0;left:0;bottom:0;right:0;opacity:0;transition:all .7s ease 0s}
  .box<?= $blockKey ?> .box-content:before{border-bottom:1px solid rgba(255,255,255,.5);border-top:1px solid rgba(255,255,255,.5);transform:scale(0,1);transform-origin:0 0 0}
  .box<?= $blockKey ?> .box-content:after{border-left:1px solid rgba(255,255,255,.5);border-right:1px solid rgba(255,255,255,.5);transform:scale(1,0);transform-origin:100% 0 0}
  .box<?= $blockKey ?>:hover .box-content:after,.box<?= $blockKey ?>:hover .box-content:before{opacity:1;transform:scale(1);transition-delay:.15s}
  .box<?= $blockKey ?> .title{font-size:21px;font-weight:700;color:#fff;margin:15px 0;opacity:0;transform:translate3d(0,-50px,0);transition:transform .5s ease 0s}
  .box<?= $blockKey ?>:hover .title{opacity:1;transform:translate3d(0,0,0)}
  .box<?= $blockKey ?> .post{font-size:14px;color:#fff;padding:10px;background:#d79719;opacity:0;border-radius:0 19px;transform:translate3d(0,-50px,0);transition:all .7s ease 0s}
  .box<?= $blockKey ?> .icon,.box15 .icon{padding:0;list-style:none}
  .box<?= $blockKey ?>:hover .post{opacity:1;transform:translate3d(0,0,0);transition-delay:.15s}
  .box<?= $blockKey ?> .icon{width:100%;margin:0;position:absolute;bottom:-10px;left:0;opacity:0;z-index:1;transition:all .7s ease 0s}
  .box<?= $blockKey ?>:hover .icon{bottom:20px;opacity:1;transition-delay:.15s}
  .box<?= $blockKey ?> .icon li a{display:block;width:40px;height:40px;line-height:40px;border:1px solid #fff;border-radius:0 16px;font-size:14px;color:#fff;margin-right:5px;transition:all .4s ease 0s}
  .box<?= $blockKey ?> .icon li a:hover{background:#d79719;border-color:#d79719}
  .box<?= $blockKey ?> .icon li,.box<?= $blockKey ?> .post{display:inline-block}
  @media only screen and (max-width:990px){.box<?= $blockKey ?>{margin-bottom:30px}
  }

  .box<?= $blockKey ?> .img-responsive{
        height: 350px;
        float: inherit;
        width: 100%;
        object-fit: cover;
  }

  .caroussel-container<?= $kunik?> .caroussel-container{
    position: relative;
    height: auto;
    width: auto;
  }
</style>
<div class="<?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
  <span class="title sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></span>
  <div class="caroussel-container caroussel-container<?= $kunik?>">
    <div class="swiper-container">
      <div class="swiper-wrapper"></div>
      <div class="swiper-pagination"></div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
  </div>
</div>


<script type="text/javascript">

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#carousel<?= $kunik ?>").append(str);

    if (costum.editMode){     
        cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
        editCarouselV2 = {
            configTabs : {
                general : {
                    inputsConfig : [
                        "background", 
                        "width",
                        "height",
                    ]
                },
                style: {
                    inputsConfig : [
                        {
                            type: "section",
                            options: {
                                name: "carouselCard",
                                    label: "Card" ,
                                    showInDefault: false,
                                    inputs: [
                                    "border",
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "borderRadius"
                                ]
                            }
                        },
                        "addCommonConfig"
                    ]
                },
                advanced : {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
                },
            onChange : function(path,valueToSet,name,payload,value){
                if(name=="text1") {
                    $(".text1<?= $blockKey ?>").text(value["text"]);
                }
                if(name=="text2") {
                    $(".text2<?= $blockKey ?>").text(value["text"]);
                }
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                costumSocket.emitEvent(wsCO, "update_component", {
                                functionName: "refreshBlock", 
                                data:{ id: id}
                })
            }
            
        }
        cmsConstructor.blocks.communityCarouselV2<?= $blockKey ?> = editCarouselV2;
        
    } 

    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($blockCms["title"]) ?>,"<?= $blockKey ?>");


        /***********CAROUSSEL**************************************/
      
      var linkContextType = links.connectType[costum.contextType];
      getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/"+linkContextType,
        function(data){
            var html = "";
              $.each(data,function(k,v){
                html += 
                `
                <div class="swiper-slide">
                  <div class="box<?= $blockKey ?> carouselCard" style="background-image:url(${v.profilMediumImageUrl})">
                        <div class="box-content">
                            <h3 class="title">${v.name}</h3>
                            <ul class="icon">
                                <li><a class="subtitle lbh" href="#page.type.${v.collection}.id.${k}"><i class="fa fa-eye"></i></a></li>
                            </ul>
                        </div>
                  </div>
                </div>
                `;
              })

              $('.caroussel-container<?= $kunik?> .swiper-wrapper').html(html);
              var swiper = new Swiper(".caroussel-container<?= $kunik?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                // init: false,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
                keyboard: {
                  enabled: true,
                },
                breakpoints: {
                  640: {
                    slidesPerView: 2,
                    spaceBetween: 5,
                  },
                  768: {
                    slidesPerView: 4,
                    spaceBetween: 10,
                  },
                  1024: {
                    slidesPerView: 6,
                    spaceBetween: 20,
                  },
                }
              });
              coInterface.bindLBHLinks();
        },
      );
        //***************END CAROUSSEL***********************************/

</script>