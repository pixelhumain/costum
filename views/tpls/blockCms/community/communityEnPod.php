<?php
$cssAndScriptFilesModule = array(
  '/js/default/profilSocial.js'
);
 HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());
?>
<?php 
  $keyTpl ="communityEnPOd";
  $paramsData = [ 
    "listIconColor" =>"#008037",
    "descriptionColor" =>"#565656",
  ];


  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
  $rolesLists = [];
  if(isset($costum["contextType"]) && isset($costum["contextId"])){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
    if($costum["contextType"] == Organization::COLLECTION ){
      if (isset($el["links"]["members"])) {  
        foreach ($el["links"]["members"] as $e => $v) {
          if (isset($v["roles"]) && isset($v["type"]) && $v["type"] == Organization::COLLECTION) {
            foreach ($v["roles"] as $role) {
              if (!empty($role)) {
                if (!isset($rolesLists[$role])) {
                  $rolesLists[$role] = array("count" => 1, "label" => $role);
                }else{
                  $rolesLists[$role]["count"]++;
                } 
              }
            }
          }
        }
      }
    }else if($costum["contextType"] == Project::COLLECTION ){
      if (isset($el["links"]["contributors"])) {
        foreach ($el["links"]["contributors"] as $e => $v) {
          if (isset($v["roles"]) && isset($v["type"]) && $v["type"] == Organization::COLLECTION) {
            foreach ($v["roles"] as $role) {
                if (!empty($role)) {
                  if (!isset($rolesLists[$role])) {
                    $rolesLists[$role] = array("count" => 1, "label" => $role);
                  }else{
                    $rolesLists[$role]["count"]++;
                  } 
                }
              }
            }
          }
        }
    }
  }

  $defaultTexts = [
    "title" => "Ma liste", 
    "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting"
  ];
  
  foreach ($defaultTexts as $key => $defaultValue) {
    ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
    ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

    $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
  };

 ?>

<style>
  .container<?php echo $kunik ?> .title-color{
    position: relative;
  }
  .container<?php echo $kunik ?> .text-color{

  }
  .container<?php echo $kunik ?> h2{
    padding-top:15px;
  }
  .description<?php echo $kunik ?> {
      list-style: none;
      margin: 0;
      padding: 10px 0;
      z-index: 99;
  }
  .description<?php echo $kunik ?> p {
      position: relative;
      padding: 5px 10px;
      border: 0px solid;
      /*background: RGBA(255, 255, 255, 0.72);*/
      word-wrap: break-word;
      /*border-radius: 50%;*/
      margin: 0 15px 0px 10px;
      /*box-shadow: -5px 5px 10px -5px rgba(0, 0, 0, 0.4);*/
  }
  .description<?php echo $kunik ?> li {
    margin-bottom: 20px
  }
  /*.list<?php //echo $kunik ?> p:before {
      content: "";
      display: block;
      width: 15px;
      height: 15px;
      border: 2px solid;
      border-radius: 50%;
  }*/
  .div-icon-right{
    height: 400px
  }
  #filters-nav {
    display:none !important;
  }
</style>
<style>
  .container<?php echo $kunik ?> .block-edit-delete{
    display: none;
    position: absolute;
    bottom:0%;
    left: 50%;
    transform: translate(-50%,0);
    z-index: 999;

  }
  .container<?php echo $kunik ?>:hover .block-edit-delete{
    display: block;
    cursor: pointer;
  }
  /*.container<?php echo $kunik ?> #central-container{
    height: auto !important;
  }*/
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class=" title-color sp-text img-text-bloc title title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h2>

  <div class=" description<?php echo $kunik ?> description<?= $blockKey ?> col-md-12 text-explain sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="description"></div>
</div>
<div id="community" class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
    <div id="central-container" style="min-height: auto !important;"></div> 
</div>

<script type="text/javascript">

  if (costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
  } 

	appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".description<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($description) ?>,"<?= $blockKey ?>");


    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var contextData = <?= json_encode($el) ?>;
    var elementData = <?= json_encode($el) ?>;
    if(typeof renderView == "string")
      pageProfil.renderView = renderView;
    contextData.id = costum.contextId;
    contextData.type = costum.contextType;
    contextType = costum.contextType;
    contextId = costum.contextId;
    contextData.rolesLists =  <?= json_encode($rolesLists) ?>;
    var canEdit = true;
    var canParticipate = false;
    var hashUrlPage = "#<?= $page ?>";
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
            
            "title" : {
                "inputType" : "text",
                "label" : "<?php echo Yii::t('cms', 'Title')?>",
                
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
            },
            "description" :{
                  "inputType" : "textarea",
                  "label" : "<?php echo Yii::t('cms', 'Description')?>",
                  "markdown" : true,
                  value : sectionDyf.<?php echo $kunik ?>ParamsData.description 
              },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                  });
                } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          $(this).html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>')
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

      setTimeout(() => {
        mylog.log("mitia",contextData);
        pageProfil.params.dir=(!notEmpty(pageProfil.params.dir)) ? links.connectType[costum.contextType] : pageProfil.params.dir;
        pageProfil.directory.init();
        }, 600);
  })
</script>