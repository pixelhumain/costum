<?php 
$keyTpl = "images_to_left_and_right_with_text_and_heading";
$paramsData = [
    "title1" => "Lorem Ipsum",
    "title2" => "Lorem Ipsum",
    "sizeTitle"=>"30",  
    "colorTitle"=>"#fff",    
    "sizeContent"=>"20",
    "content1"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "content2"=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    "bgcolorTitle"=>"#9fbd38",
    "bgcolor"=>"#ffffff",
    "widthImg"=>"50",
    "photo1"=>"",
    "logo1"=>"",
    "photo2"=>"",
    "logo2"=>""
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}  
?>
<?php 
  $photo1 = [];
  $logo1 = [];
  $photo2 = [];
  $logo2 = [];

  $latestPhoto1 = [];
  $latestLogo1 = [];
  $latestPhoto2 = [];
  $latestLogo2 = [];

  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
        ),"image"
    );

    foreach ($initImage as $key => $value) {
      if ($value["subKey"] == "photo1") {
        $photo1[] = $value; 
        $latestPhoto1[]= $value["imagePath"];
      }
      if ($value["subKey"] == "logo1") {
        $logo1[] = $value;  
        if (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
            $latestLogo1[]= $value["imageThumbPath"];
        else 
            $latestLogo1[]= $value["imagePath"];
      }
      if ($value["subKey"] == "photo2") {
        $photo2[] = $value; 
        $latestPhoto2[]= $value["imagePath"];
      }
      if ($value["subKey"] == "logo2") {
        $logo2[] = $value; 
        if (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
            $latestLogo2[]= $value["imageThumbPath"];
        else 
            $latestLogo2[]= $value["imagePath"];
      }
    }
?>
<style type="text/css">
    h4.title-start-left {
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["bgcolorTitle"]; ?>;
        padding: 10px 0px 10px 20%;
        min-height: 90px;
        margin-right: 25px;  
    }
     h4.title-start-right {
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["bgcolorTitle"]; ?>;
        padding: 10px 20% 10px 0px;
        min-height: 90px;
        margin-left: 25px;
    }
    .text-desc-left{
        margin-left: 20%;
        font-size: <?= $paramsData["sizeContent"]?>px;
    }
    .text-desc-right{
        margin-right: 20%;
        font-size: <?= $paramsData["sizeContent"]?>px;
        text-align: right;
    }
    .icon-before-title {
        border-radius: 50%;
        width: 120px;
        height: 120px;
        text-align: center;
        display: inline-block;
        border: 8px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["bgcolorTitle"]; ?>;
        background-color: #fff;
        transition: all .3s ease-out;
        margin-top: -30px;
        box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
    }
    .mr-25 {
        margin-right: -25px;
    }
    .ml-25 {
        margin-left: -25px;
    }

    .icon-before-title img {
        width: 100%;
        height: 100%;
    }
    .how-section1 h4{
        color: <?= $paramsData["colorTitle"]?>;
        font-weight: 500;
        font-size: <?= $paramsData["sizeTitle"]?>px;
        text-transform: none;
    }
    .how-section1 .subheading{
        padding-right: 100px;
    }
    .how-section1 .row
    {
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .how-img 
    {
        text-align: center;
    }
    .how-img img{
        width: <?= $paramsData["widthImg"]?>%;
    }
    @media (max-width: 768px) {
        .icon-before-title {
            width: 60px;
            height: 60px;
            border: 4px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["bgcolorTitle"]; ?>;
        }
        h4.title-start-left {
            padding: 10px 0px 10px 10%;
        }
        h4.title-start-right {
            padding: 10px 10px 10px 0px;
        }
        .text-desc-left{
            margin-left: 10%;
            text-align: justify;
        }
        .text-desc-right {
            margin-left: 10%;
            text-align: left;
            text-align: justify;
            margin-right: 0px;
        }
        .how-section1 h4 {
            font-size: 20px;
        }
    }
</style>

<div class="how-section1">
    
    <div class="row">
        <div class="col-md-8">
            <h4 class="title-start-left bg1" data-sptarget="background">
                <span class="icon-before-title mr-25 pull-right sp-bg" data-id="<?= $blockKey ?>" data-field="bgcolorTitle" data-value="<?= $paramsData['bgcolorTitle']; ?>" data-team="bg1" data-sptarget="border" data-kunik="<?= $kunik?>">
                    <?php if ($logo1 != "") { ?>
                    <img class="img-responsive text-center  img-circle" src="<?php echo !empty($latestLogo1) ? $latestLogo1[0] : ""; ?>">
                   <?php  } ?>
                </span>
                <span class="subheading sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title1"><?= $paramsData["title1"]?> </span>
                
            </h4>
            <div class="text-desc-left sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="content1"><?= $paramsData["content1"]?></div>
        </div>
        <div class="col-md-4 how-img">
            <img src="<?php echo !empty($latestPhoto1) ? $latestPhoto1[0] : ""; ?>"  alt=""/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 how-img">
            <img src="<?php echo !empty($latestPhoto2) ? $latestPhoto2[0] : ""; ?>"  alt=""/>
        </div>
        <div class="col-md-8">
            <h4  class="title-start-right bg1" data-sptarget="background">
                <span class="icon-before-title ml-25 pull-left bg1" data-sptarget="border">
                      <?php if ($logo2 != "") { ?>
                    <img class="img-responsive text-center  img-circle" src="<?php echo !empty($latestLogo2) ? $latestLogo2[0] : ""; ?>">
                   <?php  } ?>
                </span>
                <span class="subheading padding-left-15 sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title2"><?= $paramsData["title2"]?></span>
            </h4>
           <div class="text-desc-right sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="content2"><?= $paramsData["content2"]?></div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {    
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {  
                    "photo1" : {
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'First Image')?>",
                        "showUploadBtn" : false,
                        "docType" : "image",
                        "itemLimit" : 1,
                        "contentKey" : "slider",
                        "order" : 8,
                        "domElement" : "photo1",
                        "placeholder" : "Image",
                        "afterUploadComplete" : null,
                        "endPoint" : "/subKey/photo1",
                        "filetypes" : [
                            "png","jpg","jpeg","gif"
                        ],
                        initList : <?php echo json_encode($photo1) ?>
                    },
                    "logo1" : {
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'First logo')?>",
                        "showUploadBtn" : false,
                        "docType" : "image",
                        "itemLimit" : 1,
                        "contentKey" : "slider",
                        "order" : 9,
                        "domElement" : "logo1",
                        "placeholder" : "image logo",
                        "afterUploadComplete" : null,
                        "endPoint" : "/subKey/logo1",
                        "filetypes" : [
                            "png","jpg","jpeg","gif"
                        ],
                        initList : <?php echo json_encode($logo1) ?>
                    },
                    "photo2" : {
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'Second Image')?>",
                        "showUploadBtn" : false,
                        "docType" : "image",
                        "itemLimit" : 1,
                        "contentKey" : "slider",
                        "order" : 8,
                        "domElement" : "photo2",
                        "placeholder" : "Image",
                        "afterUploadComplete" : null,
                        "endPoint" : "/subKey/photo2",
                        "filetypes" : [
                            "png","jpg","jpeg","gif"
                        ],
                        initList : <?php echo json_encode($photo2) ?>
                    },
                    "logo2" : {
                        "inputType" : "uploader",
                        "label" : "<?php echo Yii::t('cms', 'Second logo')?>",
                        "showUploadBtn" : false,
                        "docType" : "image",
                        "itemLimit" : 1,
                        "contentKey" : "slider",
                        "order" : 9,
                        "domElement" : "logo2",
                        "placeholder" : "image logo",
                        "afterUploadComplete" : null,
                        "endPoint" : "/subKey/logo2",
                        "filetypes" : [
                        "png","jpg","jpeg","gif"
                        ],
                        initList : <?php echo json_encode($logo2) ?>
                    },
                    "bgcolorTitle":{
                        label : "<?php echo Yii::t('cms', 'Content color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.bgcolorTitle
                    },
                    "widthImg":{
                        label : "<?php echo Yii::t('cms', 'Image size from 1 to 100')?>",
                        inputType : "number",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.widthImg
                    }
                    
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                          dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                             toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                              $("#ajax-modal").modal('hide');
                              var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                              var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                              var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                              cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                              //urlCtrl.loadByHash(location.hash);
                            });
                          } );
                    }
                }
            }
        };
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
            event.stopImmediatePropagation()
        });

    });
    </script>