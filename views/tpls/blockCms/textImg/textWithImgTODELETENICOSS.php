<style type="text/css">
  .seen-plus{
    background-color:white;
    color:#e6344d;
    padding:12px;
    border-radius:24px;
    top:5%;
    position:relative;
    font-size: 17px;
  }

  @font-face{
    font-family: "DINAlternate-Bold";
    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/coevent/DINAlternate-Bold.ttf")
  }

  .dinalternate{
    font-family: 'DINAlternate-Bold';
  }

  .title{
    color: <?php echo @$color ?>;
    margin-top: 3vw;
  }

  .containInfo{
    text-align:left;
    position:absolute;
    height:550px;
    background-color: rgba(238, 48, 44, 0.5);
  }

</style>

<?php 
$structField = "structags";

if( count(Cms::getCmsByStruct($cmsList,$tag,$structField))!=0 )
{   
    $p = Cms::getCmsByStruct($cmsList,$tag,$structField)[0];
    $img = PHDB::findOne(Document::COLLECTION, array("id" => (String) $p["_id"]));

    echo '<div class="col-md-6 containInfo">';
      echo '<h2 class="title dinalternate">'.@$p["name"].'</h2>';
      echo "<div style='color:white;' class='markdown dinalternate'>".@$p["description"]."</div>";
      echo '<a class="seen-plus dinalternate add2fav  lbh-preview-element" href="#page.type.poi.id.'.(String) $p["_id"].'">Voir plus</a>';
    echo '</div>';

    if(isset($img)){ ?>
      <img style="height: 550px;font-size: 18px;width:100%;" src="<?php echo Yii::app()->baseUrl.'/upload/'.$img["moduleId"].'/'.$img["folder"].'/'.$img["name"]; ?>" class="img-responsive">
    <?php }

    if(isset($p["documents"])){
          echo "<br/><h4>Documents</h4>";
          foreach ($p["documents"] as $key => $value) {
            $dicon = "fa-file";
            $fileType = explode(".", $p["name"])[1];
            if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
              $dicon = "fa-file-image-o";
            else if( $fileType == "pdf" )
              $dicon = "fa-file-pdf-o";
            else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
              $dicon = "fa-file-excel-o";
            else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
              $dicon = "fa-file-text-o";
            else if( $fileType == "ppt" || $fileType == "pptx" )
              $dicon = "fa-file-text-o";
            echo "<a href='".$p["path"]."' target='_blanck'><i class='text-red fa "+$dicon+"'></i> ".$p["name"]."</a><br/>";
          }
    }
              
    $edit ="update";
} 
else 
    $edit ="create";

echo "<div class='col-md-12'>";
echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => $edit,
                                    'tag' => $tag,
                                    'id' => (isset($p["_id"])) ? (string)$p["_id"] : null
                                 ),true);

echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
echo "</div>";
?>

<script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.text",'tag : <?php echo $tag ?>');
    //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    // /alert("costum.views.tpls.text");
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('cms',null,{structags:$(this).data("tag")},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
});
</script>