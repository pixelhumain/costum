<?php
$keyTpl = "e_three_menu_img";
$paramsData = [
  "title" => "lorem Ipsum",
  "subTitle" => "lorem Ipsum",
  "imgBorderColor" => "#008037",
  "textPosition" => "center",
  "menu1ImgText" => "CULTURE",
  "menu2ImgText" => "EDUCATION FORMATION",
  "menu3ImgText" => "ART",
  "menu1Url" => "",
  "menu2Url" => "",
  "menu3Url" => "",
  "menu1UrlExtern" => "",
  "menu2UrlExtern" => "",
  "menu3UrlExtern" => ""
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (isset($blockCms[$e])) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}

$menu1Img = [];
$menu2Img = [];
$menu3Img = [];

$latestMenu1Img = [];
$latestMenu2Img = [];
$latestMenu3Img = [];

$assetsUrl = Yii::app()->getModule('costum')->assetsUrl . "/images/blockCmsImg/defaultImg";

$initImage = Document::getListDocumentsWhere(
  array(
    "id" => $blockKey,
    "type" => 'cms',
  ),
  "image"
);

foreach ($initImage as $key => $value) {
  if ($value["subKey"] == "menu1Img") {
    $menu1Img[] = $value;
    $latestMenu1Img[] = !empty($value["imageMediumPath"]) ? $value["imageMediumPath"] : $value["imagePath"];
  }
  if ($value["subKey"] == "menu2Img") {
    $menu2Img[] = $value;
    $latestMenu2Img[] = !empty($value["imageMediumPath"]) ? $value["imageMediumPath"] : $value["imagePath"];
  }
  if ($value["subKey"] == "menu3Img") {
    $menu3Img[] = $value;
    $latestMenu3Img[] = !empty($value["imageMediumPath"]) ? $value["imageMediumPath"] : $value["imagePath"];
  }
}
?>

<style>
  <?php if($paramsData["textPosition"] == "center"){?>
  .block-container2-<?= $kunik ?> {
    box-shadow: 0 15px 30px rgb(0 0 0 / 20%);
    border-bottom-right-radius: 15px;
    border-bottom-left-radius: 15px;
  }

  .block-container2-<?= $kunik ?> .img-container-1,
  .block-container2-<?= $kunik ?> .img-container-2,
  .block-container2-<?= $kunik ?> .img-container-3 {
    position: relative;
    width: 218px;
    height: 218px;
  }

  .block-container2-<?= $kunik ?> .img-container-1 img,
  .block-container2-<?= $kunik ?> .img-container-2 img,
  .block-container2-<?= $kunik ?> .img-container-3 img {
    width: 218px;
    height: 218px;
    object-fit: contain;
    position: absolute;
    top: 0;
    left: 0;
  }

  .block-container2-<?= $kunik ?> .img-container-1 a,
  .block-container2-<?= $kunik ?> .img-container-2 a,
  .block-container2-<?= $kunik ?> .img-container-3 a {
    position: absolute;
    z-index: 111;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .block-container2-<?= $kunik ?> .img-container {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
  }

  @media (max-width: 992px) {
    .block-container2-<?= $kunik ?> .img-container {
      margin: 0 auto;
    } 
  }
  <?php }else if($paramsData["textPosition"] == "top"){?>
    .block-container2-<?= $kunik ?> .img-container { 
      text-align: center; 
    }
    .block-container2-<?= $kunik ?> .img-container-1 img,
    .block-container2-<?= $kunik ?> .img-container-2 img,
    .block-container2-<?= $kunik ?> .img-container-3 img {
      width: 218px;
      height: 218px;
      object-fit: contain;
    }
  <?php }else if($paramsData["textPosition"] == "bottom"){?>
    .block-container2-<?= $kunik ?> .img-container { 
      text-align: center; 
    }
    .block-container2-<?= $kunik ?> .img-container h3{ 
      color: black !important;
    }
    .block-container2-<?= $kunik ?> .img-container-1 img,
    .block-container2-<?= $kunik ?> .img-container-2 img,
    .block-container2-<?= $kunik ?> .img-container-3 img {
      width: 218px;
      height: 218px;
      object-fit: contain;
    }
  <?php }?>
  
</style>
<div class="col-md-12 col-xs-12 block-container2-<?= $kunik ?> container-<?= $kunik ?>">
  <h2 class="title sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"] ?></h2>
  <h3 class="subtitle sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="subTitle"><?php echo $paramsData["subTitle"] ?></h3>

  <div class="col-md-4 col-xs-12">
      <div class="img-container ">
        <div class="img-container-1">
          <?php if($paramsData["textPosition"] != "bottom"){?>
            <a href="<?= !empty($paramsData["menu1UrlExtern"]) ? $paramsData["menu1UrlExtern"] : $paramsData["menu1Url"] ?>" 
              class="<?= !empty($paramsData["menu1UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu1UrlExtern"]) ? "target='_blank'" : "" ?>
              >
              <h3 class="title-3 "><?= $paramsData["menu1ImgText"] ?></h3>
            </a>
          <?php }?>
          <img src="<?php echo !empty($latestMenu1Img[0]) ? $latestMenu1Img[0] : ""; ?>">
          <?php if($paramsData["textPosition"] == "bottom"){?>
            <a href="<?= !empty($paramsData["menu1UrlExtern"]) ? $paramsData["menu1UrlExtern"] : $paramsData["menu1Url"] ?>" 
                class="<?= !empty($paramsData["menu1UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu1UrlExtern"]) ? "target='_blank'" : "" ?>
                >
              <h3 class="title-3"><?= $paramsData["menu1ImgText"] ?></h3>          
            </a> 
          <?php }?>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-xs-12">
    <div class="img-container">
      <div class="img-container-2">
          <?php if($paramsData["textPosition"] != "bottom"){?>
            <a href="<?= !empty($paramsData["menu2UrlExtern"]) ? $paramsData["menu2UrlExtern"] : $paramsData["menu2Url"] ?>" 
              class="<?= !empty($paramsData["menu2UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu2UrlExtern"]) ? "target='_blank'" : "" ?>
              >
              <h3 class="title-3"><?= $paramsData["menu2ImgText"] ?></h3>
            </a>
          <?php }?>
          <img src="<?php echo !empty($latestMenu2Img[0]) ? $latestMenu2Img[0] : ""; ?>">
          <?php if($paramsData["textPosition"] == "bottom"){?>
            <a href="<?= !empty($paramsData["menu2UrlExtern"]) ? $paramsData["menu2UrlExtern"] : $paramsData["menu2Url"] ?>" 
                class="<?= !empty($paramsData["menu2UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu2UrlExtern"]) ? "target='_blank'" : "" ?>
                >
              <h3 class="title-3"><?= $paramsData["menu2ImgText"] ?></h3>          
            </a>
          <?php }?>
      </div>
    </div>
  </div>

  <div class="col-md-4 col-xs-12">
    <div class="img-container">
      <div class="img-container-3">
          <?php if($paramsData["textPosition"] != "bottom"){?>
            <a href="<?= !empty($paramsData["menu3UrlExtern"]) ? $paramsData["menu3UrlExtern"] : $paramsData["menu3Url"] ?>" 
              class="<?= !empty($paramsData["menu3UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu3UrlExtern"]) ? "target='_blank'" : "" ?>
              >
              <h3 class="title-3"><?= $paramsData["menu3ImgText"] ?></h3>
            </a>
          <?php }?>
          <img src="<?php echo !empty($latestMenu2Img[0]) ? $latestMenu2Img[0] : ""; ?>">
          <?php if($paramsData["textPosition"] == "bottom"){?>
            <a href="<?= !empty($paramsData["menu3UrlExtern"]) ? $paramsData["menu3UrlExtern"] : $paramsData["menu3Url"] ?>" 
                class="<?= !empty($paramsData["menu3UrlExtern"]) ? "" : "lbh" ?>"
                <?= !empty($paramsData["menu3UrlExtern"]) ? "target='_blank'" : "" ?>
                >
              <h3 class="title-3"><?= $paramsData["menu3ImgText"] ?></h3>          
            </a>
          <?php }?>
      </div>
    </div>
  </div>

</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    var urlOptions = {}
    if (notNull(costum) && exists(costum.app))
      $.each(costum.app, function(k, v) {
        urlOptions[k] = exists(v.subdomainName) ? v.subdomainName : "";
      })
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema": {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon": "fa-cog",

        "properties": {
          // "imgBorderColor": {
          //   "inputType": "colorpicker",
          //   "label": "<?php //echo Yii::t('cms', 'Border of the photo')?>",
          //   values: sectionDyf.<?php //echo $kunik ?>ParamsData.imgBorderColor
          // },
          "textPosition": {
            "inputType": "select",
            "label": "Position de l'image",
            "options": {
              "center" : "centre de l'image",
              "bottom" : "En bas de l'image",
              "top" : "En haut de l'image",
            },
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu1ImgText
          },
          "menu1ImgText": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'Text on image')?> 1",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu1ImgText
          },
          "menu2ImgText": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'Text on image')?> 2",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu2ImgText
          },
          "menu3ImgText": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'Text on image')?> 3",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu3ImgText
          },
          "menu1Url": {
            "inputType": "select",
            "label": "<?php echo Yii::t('cms', 'Url APP')?>",
            options: urlOptions,
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu1Url
          },
          "menu2Url": {
            "inputType": "select",
            "label": "<?php echo Yii::t('cms', 'Url APP')?>",
            options: urlOptions,
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu2Url
          },
          "menu3Url": {
            "inputType": "select",
            "label": "<?php echo Yii::t('cms', 'Url APP')?>",
            options: urlOptions,
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu3Url
          },
          "menu1UrlExtern": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'External url')?>",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu1UrlExtern
          },
          "menu2UrlExtern": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'External url')?>",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu2UrlExtern
          },
          "menu3UrlExtern": {
            "inputType": "text",
            "label": "<?php echo Yii::t('cms', 'External url')?>",
            values: sectionDyf.<?php echo $kunik ?>ParamsData.menu3UrlExtern
          },
          "menu1Img": {
            "inputType": "uploader",
            "label": "<?php echo Yii::t('cms', 'Image')?> 1",
            "domElement": "menu1Img",
            "docType": "image",
            "contentKey": "slider",
            "itemLimit": 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            "endPoint": "/subKey/menu1Img",
            initList: <?php echo json_encode($menu1Img) ?>
          },
          "menu2Img": {
            "inputType": "uploader",
            "label": "<?php echo Yii::t('cms', 'Image')?> 2",
            "domElement": "menu2Img",
            "docType": "image",
            "contentKey": "slider",
            "itemLimit": 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            "endPoint": "/subKey/menu2Img",
            initList: <?php echo json_encode($menu2Img) ?>
          },
          "menu3Img": {
            "inputType": "uploader",
            "label": "<?php echo Yii::t('cms', 'Image')?> 3",
            "domElement": "menu3Img",
            "docType": "image",
            "contentKey": "slider",
            "itemLimit": 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            "endPoint": "/subKey/menu3Img",
            initList: <?php echo json_encode($menu3Img) ?>
          }
        },
        beforeBuild: function() {
          uploadObj.set("cms", "<?php echo $blockKey ?>");
        },
        save: function(data) {
          tplCtx.value = {};
          $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, function(k, val) {
            tplCtx.value[k] = $("#" + k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;

            if (k == "description")
              tplCtx.value[k] = data.description;
          });
          console.log("save tplCtx", tplCtx);

          if (typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value(tplCtx, function(params) {
              dyFObj.commonAfterSave(params, function() {
               toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                //urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, "menu1", 4, 6, null, null, "Menu image 1", "blue");
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, "menu2", 4, 6, null, null, "Menu image 2", "blue");
      alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, "menu3", 4, 6, null, null, "Menu image 3", "blue");
    });
  });
</script>