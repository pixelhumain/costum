<?php 
  $keyTpl ="kaf_pdf_and_capture";
  $paramsData = [ 
    "title" => "TÉLÉCHARGER LE LIVRET DE PAROLES",
    "btnLabel" => "TÉLÉCHARGER",
    "btnBgColor" => "#f0ad16",
    "btnLabelColor" => "#ffffff",
    "btnBgColorHover" => "#000000",
    "btnLabelColorHover" => "#ffffff",
    "btnBorderColor" => "#f0ad16",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

  $initImage = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'image',
    ), "image"
  );

  //var_dump($initImage);
    $initPdf = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'pdf',
    ), "file"
  );
   //var_dump($initPdf);
?>

<style>
	.paroles-container-<?= $kunik ?>{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		width: 100%;
		height: auto;
		margin-top: 80px;
		align-items: center;
		justify-content: center;
		padding: 0 100px;
		margin-bottom: 68px;
	}
	.paroles-container-<?= $kunik ?> img{
		width: 100%;
	    height: 100%;
	    object-fit: contain;
	    object-position: center;
	}
	.btn-<?= $kunik ?>{
		padding: 8px 31px;
	    background-color: <?= $paramsData["btnBgColor"] ?>;
	    font-family: 'Montserrat-Bold';
	    color: <?= $paramsData["btnLabelColor"] ?>;
	    font-size: 24px ;
	    border:2px solid <?= $paramsData["btnBorderColor"] ?>;
	}
	.btn-<?= $kunik ?>:hover{
	    background-color: <?= $paramsData["btnBgColorHover"] ?>;
	    font-family: 'Montserrat-Bold';
	    color: <?= $paramsData["btnLabelColorHover"] ?>;
	}
	.btn-<?= $kunik ?>:focus{
	    background-color: <?= $paramsData["btnBgColorHover"] ?>;
	    font-family: 'Montserrat-Bold';
	    color: <?= $paramsData["btnLabelColorHover"] ?>;
	}
	@media (max-width: 765px){
		.paroles-container-<?= $kunik ?> div{
			flex:none !important;
		}
		.paroles-container-<?= $kunik ?>{
			flex-direction: column;
			padding: 0;
		}
	}
</style>
<br>
<h3 class="title-1  sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"] ?></h3>
<div class="paroles-container-<?= $kunik ?>">
	<?php foreach ($initImage as $key => $value) {  ?>
		<div style="flex:40%">
		   <img src="<?= $value["imagePath"] ?>" alt="">
	    </div>
	<?php } ?>
</div>
<?php foreach ($initPdf as $ki => $vi) {  ?>
	<p class="text-center">
		<a href="<?= $vi["docPath"] ?>" target="_blank" class="btn-<?= $kunik ?> btn"><?= $paramsData["btnLabel"] ?></a>
	</p>
<?php } ?>
<script>
	  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	  $(function(){
	      sectionDyf.<?php echo $kunik ?>Params = {
	        "jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
	          	"icon" : "fa-cog",
	          
	          	"properties" : {
					"image" :{
						"inputType" : "uploader",
						"label" : "<?php echo Yii::t('cms', 'Speech Capture')?>",
						"domElement" : "image",
						"docType": "image",
						"contentKey" : "slider",
						"itemLimit" : 2,
						"filetypes": ["jpeg", "jpg", "gif", "png"],
						"showUploadBtn": false,
						"endPoint" :"/subKey/image",
						initList : <?php echo json_encode($initImage) ?>
					},
					"pdf" :{
						"inputType" : "uploader",
						"label" : "<?php echo Yii::t('cms', 'Speech PDF')?>",
						"domElement" : "pdf",
						"docType": "file",
						"contentKey" : "file",
						"itemLimit" : 1,
						"filetypes": ["pdf"],
						"showUploadBtn": false,
						"endPoint" :"/subKey/pdf",
						initList : <?php echo json_encode($initPdf) ?>
					},
	              	btnLabel:{
						inputType:"text",
						label:"<?php echo Yii::t('cms', 'Button label')?>",
						value: "<?= $paramsData["btnLabel"] ?>"
					},
					btnBgColor:{
						inputType:"colorpicker",
						label:"<?php echo Yii::t('cms', 'Button color')?>",
						value: "<?= $paramsData["btnBgColor"] ?>"
					},
					btnLabelColor:{
						inputType:"colorpicker",
						label:"<?php echo Yii::t('cms', 'Color of the button label')?>",
						value: "<?= $paramsData["btnLabelColor"] ?>"
					},
					btnBgColorHover:{
						inputType:"colorpicker",
						label:"<?php echo Yii::t('cms', 'Button color on hover')?>",
						value: "<?= $paramsData["btnBgColorHover"] ?>"
					},
					btnLabelColorHover:{
						inputType:"colorpicker",
						label:"<?php echo Yii::t('cms', 'Label color on hover')?>",
						value: "<?= $paramsData["btnLabelColorHover"] ?>"
					},
					btnBorderColor:{
						inputType:"colorpicker",
						label:"<?php echo Yii::t('cms', 'Border color')?>",
						value: "<?= $paramsData["btnBorderColor"] ?>"
					}	   											           
	          	},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
				},
				save : function (data) {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
					tplCtx.value[k] = $("#"+k).val();
					if (k == "parent")
						tplCtx.value[k] = formData.parent;
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
					toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
						dyFObj.commonAfterSave(params,function(){
							toastr.success("Élément bien ajouté");
							$("#ajax-modal").modal('hide');
							var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
							var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
							var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
							cmsBuilder.block.loadIntoPage(id, page, path, kunik);
							// urlCtrl.loadByHash(location.hash);
						});
						} );
					}

				}
	        }
	    };


		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"btn",2,6,null,null,"<?php echo Yii::t('cms', 'Button')?>","blue","");
		});
	});
</script>