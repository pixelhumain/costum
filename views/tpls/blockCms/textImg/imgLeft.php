<?php
  $keyTpl ="imgLeft";
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) [$kunik => $objectCss];
 ?>
 
<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $initFiles = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"notBackground"
    ), "image"
  );
//$arrayImg = [];
$imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
if(isset($initFiles[0])){
  $images = $initFiles[0];
  if(!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
      $imgUrl = $baseUrl.$images["imagePath"];
  elseif (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
      $imgUrl = $baseUrl.$images["imageMediumPath"];
}
if ($imgUrl != Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg' && $blockCms["image"] == "") {
  $blockCms["image"] = $imgUrl;
}
//var_dump($baseUrl.$images["imageMediumPath"]);
//var_dump($initFiles);
 ?>
<!-- ****************end get image uploaded************** -->
<style id="#css-<?= $kunik ?>">
  .container<?php echo $kunik ?>{
    /*box-shadow: 0 0px 20px rgba(0, 0, 0, 0.2);*/
    /*background-image: url("<?php  //echo Yii::app()->getModule('costum')->assetsUrl?>/images/eywa/background.png");
    text-align: left; 
    /*margin-top: 10px;*/
    /*margin-bottom: 40px;*/ 
    padding-bottom: 38px;
  }
.container<?php echo $kunik ?> .main-title{
  color: #2d2d2d;
  text-align: center;
  text-transform: capitalize;
  padding: 0.7em 0;
}



.container<?php echo $kunik ?> .content {
  position: relative;
  width: 90%;
  max-width: 700px;
  margin: auto;
  overflow: hidden;
  border-radius: 10px;
  margin-top: 38px;
}

.container<?php echo $kunik ?> .content .content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 99%;
  width: 100%;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}

.container<?php echo $kunik ?>  .content:hover .content-overlay{
  opacity: 1;
}

.container<?php echo $kunik ?> .content-image{
  width: 100%;
}

.container<?php echo $kunik ?> .content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
}

.container<?php echo $kunik ?> .content:hover .content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}

.container<?php echo $kunik ?> .content-details h3{
  color: #fff;
  font-weight: 500;
  letter-spacing: 0.15em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}

.container<?php echo $kunik ?> .content-details p{
  color: #fff;
  font-size: 0.8em;
}

.container<?php echo $kunik ?> .fadeIn-bottom{
  top: 80%;
}

.container<?php echo $kunik ?> .fadeIn-top{
  top: 20%;
}

.container<?php echo $kunik ?> .fadeIn-left{
  left: 20%;
}

.container<?php echo $kunik ?> .fadeIn-right{
  left: 80%;
}
.container<?php echo $kunik ?> .description{
  text-align: justify;
}
.container<?php echo $kunik ?> .description::first-letter{
  font-size: 30px;
  color: <?php echo $blockCms["css"]["btnCss"]["borderColor"] ?>;
  text-transform: capitalize !important; 

}
.container<?php echo $kunik ?> div[class^="illustration"]{
  position: absolute;
  background-color: <?php echo $blockCms["css"]["btnCss"]["borderColor"] ?>;
    -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
}

.container<?php echo $kunik ?> .illustration-left-1 {
    width: 5px;
    height: 80px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-left-2 {
    width: 50%;
    height: 5px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .illustration-right-1 {
    width: 5px;
    height: 80px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-right-2 {
    width: 50%;
    height: 5px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .btn-edit-delete{
  display: none;
}
.container<?php echo $kunik ?>:hover .btn-edit-delete{
  display: block;
  -webkit-transition: all 0.9s ease-in-out 9s;
  -moz-transition: all 0.9s ease-in-out 9s;
  transition: all 0.9s ease-in-out 0.9s;= $blockCms["image"] ?>
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);
}

.container<?php echo $kunik ?> .button-link{
  display: block;
  width: fit-content;
  text-decoration: none;
  padding: 10px 20px;
  background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["btnCss"]["backgroundColor"]; ?>;
  font-size: <?= $blockCms["css"]["btnCss"]["fontSize"]?>px;
  margin-top: 20px;
  color: white;
  font-weight: bold;
  border-radius: 4px;
  transition: .3s;
}

.container<?php echo $kunik ?> .button-link:hover{
  text-decoration: none;
  color: white;
}

#filters-nav {
  display:none !important;
}
@media (max-width: 567px){
  .container<?php echo $kunik ?>,.content<?php echo $kunik ?>{
    padding-right: 0;
    padding-left: 0;
  }
}
@media (max-width: 992px){
  .container<?php echo $kunik ?> .title-xs{
    display: block
  }
  .container<?php echo $kunik ?> .title{
    display: none
  }
}

@media (min-width: 993px){
  .container<?php echo $kunik ?> .title-xs{
    display: none !important;
  }
  .container<?php echo $kunik ?> .title{
    display: block
  }
} 
</style>
<div class="container<?php echo $kunik ?> col-md-12 col-xs-12">
  <div class="illustration-left-1"></div>
  <div class="illustration-left-2"></div>
<h3 class="title-xs title title-color ChangoRegular sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title"><?php echo $blockCms["title"] ?></h3>
    <?php if($blockCms["imagePosition"]=="right"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class=" title-color title ChangoRegular text-center sp-text img-text-bloc  text-center hidden-xs" data-id="<?= $blockKey ?>" data-field="title"><?php echo $blockCms["title"] ?></h3>
    <div class="description text-color more<?php echo $kunik ?> sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="description"><?php echo $blockCms["description"] ?></div>
    <?php if($blockCms["buttonShow"]){ ?>
      <a href="<?= $blockCms["buttonUrl"] ?>" class="button-link lbh"><?= $blockCms["buttonLabel"] ?></a>
    <?php } ?>
  </div>
  <?php } ?>

  <div class="content<?php echo $kunik ?> col-md-6 col-xs-12">
    <div class="content">
      <a href="javascript:;">
            <img class="content-image lzy_img" src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"]?> " data-src="<?php echo $blockCms["image"] == '' ? $imgUrl : $blockCms["image"]?> " alt="">
            <!-- <img class="content-image" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" data-src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$imgUrl ?>" alt="">  -->
      </a>
    </div>
  </div>

  <?php if($blockCms["imagePosition"]=="left"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class=" title title-color ChangoRegular sp-text img-text-bloc  text-center hidden-xs" data-id="<?= $blockKey ?>" data-field="title"><?php echo $blockCms["title"] ?></h3>
    <div class="description text-color more<?php echo $kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="description"><?php echo $blockCms["description"] ?></div>
    <?php if($blockCms["buttonShow"]){ ?>
      <a href="<?= $blockCms["buttonUrl"] ?> "class="button-link lbh"><?= $blockCms["buttonLabel"] ?></a>
    <?php } ?>
  </div>
  <?php } ?>

  <div class="illustration-right-1"></div>
  <div class="illustration-right-2"></div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var imgLeftInput = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputFileImage",
                            options : {
                                name : "image",
                                label : tradCms.image,
                                collection : "cms"
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "imagePosition",
                                label : tradCms.alignImage,
                                collection : "cms",
                                options : [
                                    {
                                        value : "left",
                                        label : tradCms.left
                                    },
                                    {
                                        value : "right",
                                        label : tradCms.right
                                    }
                                ]
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "buttonLabel",
                                label : tradCms.label,
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "buttonUrl",
                                label : tradCms.link,
                                collection : "cms"
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "buttonShow",
                                label : tradCms.showBtn,
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].buttonShow
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "btnReadMore",
                                label : tradCms.seeMore,
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].btnReadMore
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "btnCss",
                                label: tradCms.buttonColor,
                                inputs : [
                                    "backgroundColor",
                                    "borderColor",
                                    "fontSize"
                                ]
                            }
                        }
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.imgLeft<?= $myCmsId ?> = imgLeftInput;
    }
    jQuery(document).ready(function() {
      <?php if($blockCms["btnReadMore"]==true){ ?>
        setTimeout(() => {
          $(".more<?php echo $kunik ?>").myOwnLineShowMoreLess({
          showLessLine: 10,
          showLessText:'Lire Moins',
          showMoreText:'Lire plus',
          //lessAtInitial:false,
          //showLessAfterMore:false
          });
        }, 900);
      <?php } ?>
    });
</script>
