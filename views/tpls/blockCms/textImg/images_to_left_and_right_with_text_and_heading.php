<?php 
    $keyTpl = "images_to_left_and_right_with_text_and_heading";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss]; 
?>
<?php 
  $photo1 = [];
  $logo1 = [];
  $photo2 = [];
  $logo2 = [];

  $latestPhoto1 = [];
  $latestLogo1 = [];
  $latestPhoto2 = [];
  $latestLogo2 = [];

  $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
        ),"image"
    );

    foreach ($initImage as $key => $value) {
      if ($value["subKey"] == "photo1") {
        $photo1[] = $value; 
        $latestPhoto1[]= $value["imagePath"];
      }
      if ($value["subKey"] == "logo1") {
        $logo1[] = $value;  
        if (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
            $latestLogo1[]= $value["imageThumbPath"];
        else 
            $latestLogo1[]= $value["imagePath"];
      }
      if ($value["subKey"] == "photo2") {
        $photo2[] = $value; 
        $latestPhoto2[]= $value["imagePath"];
      }
      if ($value["subKey"] == "logo2") {
        $logo2[] = $value; 
        if (!empty($images["imageMediumPath"]) && Document::urlExists($baseUrl.$images["imageMediumPath"]))
            $latestLogo2[]= $value["imageThumbPath"];
        else 
            $latestLogo2[]= $value["imagePath"];
      }
    }
    if (!empty($latestPhoto1) && $blockCms["photo1"] == "") {
        $p1 = $latestPhoto1[0];
        $blockCms["photo1"] = $p1;
    }
    if (!empty($latestPhoto2) && $blockCms["photo2"] == "") {
        $p2 = $latestPhoto2[0];
        $blockCms["photo2"] = $p2;
    }
    if (!empty($value) && !empty($latestLogo1) && $blockCms["logo1"] == "") {
        $l1 = $latestLogo1[0];
        $blockCms["logo1"] = $l1;
    }
    if (!empty($value) && !empty($latestLogo2) && $blockCms["logo2"] == "") {
        $l2 = $latestLogo2[0];
        $blockCms["logo2"] = $l2;
    }
?>
<style type="text/css">
    h4.title-start-left {
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["imgCss"]["backgroundColor"]; ?>;
        padding: 10px 0px 10px 20%;
        min-height: 90px;
        margin-right: 25px;  
    }
     h4.title-start-right {
        background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["imgCss"]["backgroundColor"]; ?>;
        padding: 10px 20% 10px 0px;
        min-height: 90px;
        margin-left: 25px;
    }
    .text-desc-left{
        margin-left: 20%;
        font-size: <?= $blockCms["sizeContent"]?>px;
    }
    .text-desc-right{
        margin-right: 20%;
        font-size: <?= $blockCms["sizeContent"]?>px;
        text-align: right;
    }
    .icon-before-title {
        border-radius: 50%;
        width: 120px;
        height: 120px;
        text-align: center;
        display: inline-block;
        border: 8px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["imgCss"]["backgroundColor"]; ?>;
        background-color: #fff;
        transition: all .3s ease-out;
        margin-top: -30px;
        box-shadow: 0 2px 5px 0 rgb(63, 78, 88), 0 2px 10px 0 rgb(63, 78, 88);
    }
    .mr-25 {
        margin-right: -25px;
    }
    .ml-25 {
        margin-left: -25px;
    }

    .icon-before-title img {
        width: 100%;
        height: 100%;
    }
    .how-section1 h4{
        color: <?= $blockCms["colorTitle"]?>;
        font-weight: 500;
        font-size: <?= $blockCms["sizeTitle"]?>px;
        text-transform: none;
    }
    .how-section1 .subheading{
        padding-right: 100px;
    }
    .how-section1 .row
    {
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .how-img 
    {
        text-align: center;
    }
    .how-img img{
        width: <?= $blockCms["css"]["imgCss"]["width"]?>;
    }
    @media (max-width: 768px) {
        .icon-before-title {
            width: 60px;
            height: 60px;
            border: 4px solid <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["imgCss"]["backgroundColor"]; ?>;
        }
        h4.title-start-left {
            padding: 10px 0px 10px 10%;
        }
        h4.title-start-right {
            padding: 10px 10px 10px 0px;
        }
        .text-desc-left{
            margin-left: 10%;
            text-align: justify;
        }
        .text-desc-right {
            margin-left: 10%;
            text-align: left;
            text-align: justify;
            margin-right: 0px;
        }
        .how-section1 h4 {
            font-size: 20px;
        }
    }
</style>

<div class="how-section1">
    
    <div class="row">
        <div class="col-md-8">
            <h4 class="title-start-left bg1" data-sptarget="background">
                <span class="icon-before-title mr-25 pull-right sp-bg" data-id="<?= $blockKey ?>" data-field="bgcolorTitle" data-value="<?= $blockCms['bgcolorTitle']; ?>" data-team="bg1" data-sptarget="border" data-kunik="<?= $kunik?>">
                    <img class="img-responsive text-center  img-circle" src="<?php echo $blockCms["logo1"]; ?>">
                </span>
                <span class="subheading sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title1"><?= $blockCms["title1"]?> </span>
                
            </h4>
            <div class="text-desc-left sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="content1"><?= $blockCms["content1"]?></div>
        </div>
        <div class="col-md-4 how-img">
            <img src="<?php echo $blockCms["photo1"]; ?>"  alt=""/>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 how-img">
            <img src="<?php echo $blockCms["photo2"]; ?>"  alt=""/>
        </div>
        <div class="col-md-8">
            <h4  class="title-start-right bg1" data-sptarget="background">
                <span class="icon-before-title ml-25 pull-left bg1" data-sptarget="border">
                    <img class="img-responsive text-center  img-circle" src="<?php echo $blockCms["logo2"]; ?>">
                </span>
                <span class="subheading padding-left-15 sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="title2"><?= $blockCms["title2"]?></span>
            </h4>
           <div class="text-desc-right sp-text img-text-bloc  text-center" data-id="<?= $blockKey ?>" data-field="content2"><?= $blockCms["content2"]?></div>
        </div>
    </div>
    
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var images_to_left_and_right_with_text_and_headingInput = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputFileImage",
                            options : {
                                name : "photo1",
                                label : tradCms.image+" 1",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputFileImage",
                            options : {
                                name : "logo1",
                                label : tradCms.logo+" 1",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputFileImage",
                            options : {
                                name : "photo2",
                                label : tradCms.image+" 2",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputFileImage",
                            options : {
                                name : "logo2",
                                label : tradCms.logo+" 2",
                                collection : "cms"
                            }
                        },
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "imgCss",
                                label: tradCms.image,
                                inputs : [
                                    "backgroundColor",
                                    "width"
                                ]
                            }
                        }
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.images_to_left_and_right_with_text_and_heading<?= $myCmsId ?> = images_to_left_and_right_with_text_and_headingInput;
    }
</script>