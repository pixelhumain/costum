<?php 
    $keyTpl = "linkDocs";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss]; 

    $kunik = $keyTpl.(string)$blockCms["_id"];
    $blockKey = (string)$blockCms["_id"];
    $page = isset($page)?$page:"";
?>
<style type="text/css">  
    section.file<?php echo $kunik ?>  {
        padding: 25px 0;
    }
    .file<?php echo $kunik ?>  {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }
    .file<?php echo $kunik ?>   {
        min-height: 85px;
    }

    .file<?php echo $kunik ?>  .card_<?=$kunik?> .card-title {
        font-weight: bold;

    }
    .file<?php echo $kunik ?>  img {
        height: 200px;
        object-fit: cover;
        object-position: center;
    }
    .file<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none
    }
    .file<?=$kunik?> .icon-<?=$kunik?> li a{
        display:block;width:35px;
        height:35px;
        line-height:35px;
        border-radius:50%;
        color: <?php echo (isset($costum["css"]["color"]["label-color"])) ? $costum["css"]["color"]["label-color"] : $blockCms["css"]["btnCss"]["color"]; ?> ;
        font-size:18px;
        background:<?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["btnCss"]["backgroundColor"]; ?>;
        border:1px solid <?php echo (isset($costum["css"]["color"]["border-color"])) ? $costum["css"]["color"]["border-color"] : $blockCms["css"]["btnCss"]["borderColor"]; ?>;
        margin-right:10px;transition:all .5s ease 0s

    }
    .file<?=$kunik?> .icon-<?=$kunik?> li a:hover{
        transform:rotate(360deg)
    }
    .file<?=$kunik?>{
        box-shadow:0 0 3px rgba(0,0,0,.3)
    }
    .file<?=$kunik?> .icon-<?=$kunik?>{
        padding:0;
        list-style:none;
        margin:0;
        text-align:center;
    }
    
    .file<?=$kunik?> .icon-<?=$kunik?> li {
        display: none;
    }

    .file<?=$kunik?>:hover .icon-<?=$kunik?> li{
        display:inline-block;
    }
    @media (max-width: 767px) {
        .file<?php echo $kunik ?>  img {
            max-width: 300px;
        }
    }
    .addDoc<?= $blockCms['_id'] ?> button{
      margin-left: 12px;
      font-size: 16px;
      line-height: 24px;
      min-height: 40px;
      padding: 8px 24px;
      background-color: #000091!important;
      color: #fff!important;
      border-radius: 2px;
    }


</style>
<section class="file<?php echo $kunik ?>" class="pb-5">
    <div class=" editSectionBtns<?php echo $kunik ?>">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
                <div class=" addDoc<?= $blockCms['_id'] ?>">
                    <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo Yii::t('cms', 'Add a resource block')?></button>
                </div>  
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class = "filtersActive<?= $kunik?>"> </div>
        <div class="card_<?=$kunik?>">
            
        </div>
    </div>
</section>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    var dyfPoiDoc={
        "beforeBuild":{
            "properties" : {
                "name": {
                    "label" : "<?php echo Yii::t('cms', 'File title')?>",
                    "placeholder" : "<?php echo Yii::t('cms', 'File title')?>",
                    "order" : 1,
                    rules :{
                        "required" :true
                    }
                },
                "subtype" : {
                    "label" : "<?php echo Yii::t('cms', 'Name of the page')?>",
                    value : ["<?= $page?>"]
                },
                "category" : {
                    "label" : "<?php echo Yii::t('cms', 'Display type')?>",
                    inputType :"select",
                    "placeholder" : "<?php echo Yii::t('cms', 'Display type')?>",
                    "class":"form-control",
                    options : {
                        "link" : "<?php echo Yii::t('cms', 'External link')?>",
                        "content" : "<?php echo Yii::t('cms', 'Preview')?>",
                        "fullScreen" : "<?php echo Yii::t('cms', 'Full screen')?>",
                    },
                    order : 2,
                    rules :{
                        "required" :true
                    }
                },
                "urls": {
                    "label" : "<?php echo Yii::t('cms', 'File link')?>",
                    "order" : 3
                },
                "description" : {
                    "label" : "<?php echo Yii::t('cms', 'Content')?> ",
                    "order" : 4
                }
            }
        },
        "onload" : {
            "actions" : {
                "setTitle" : "<?php echo Yii::t('cms', 'Add a file')?>",
                "html" : {
                    "infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
                },
                "presetValue" : {
                    "type" : "affiche",
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "formLocalityformLocality": 1,
                    "urlsarrayBtn" : 1,
                    "parentfinder" : 1
                }
            }
        }
    };
    dyfPoiDoc.afterSave = function(data){
        dyFObj.commonAfterSave(data,function(){
	    	dyFObj.closeForm();
            urlCtrl.loadByHash(location.hash);
        });
    };
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
        var linkDocsInput = {
            configTabs: {
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "btnCss",
                                label: tradCms.buttonColor,
                                inputs : [
                                    "color",
                                    "backgroundColor",
                                    "borderColor"
                                ]
                            }
                        }
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.linkDocs<?= $myCmsId ?> = linkDocsInput;
    }
    jQuery(document).ready(function() {
        var tagsFilter<?= $kunik?> = JSON.parse(localStorage.getItem("fitersTagspoi"+page));
        if(notEmpty(tagsFilter<?= $kunik?>)){
            var str = "<i class ='fa fa-filter'></i>Filtre actif :";
            $.each(tagsFilter<?= $kunik?>,function(k,v){
                str += "<a href='javascript:;' class='deleteFilter tooltips ' data-title='Effacer' data-value= \""+v+"\"><i class='fa fa-times-circle'></i>"+v+"</a>";
            })
            $(".filtersActive<?= $kunik?> ").html(str);
            $(".filtersActive<?= $kunik?> ").css("margin-bottom","1%")
            $(".filtersActive<?= $kunik?> ").css("box-shadow"," 0 2px 5px 0 rgb(23 47 145), 0 2px 10px 0 rgb(23 47 145)")
            $(".filtersActive<?= $kunik?> ").css("padding","10px");
        }
        $(".file<?php echo $kunik ?> .deleteFilter").click(function(){
            var value = $(this).data("value");
            delete tagsFilter<?= $kunik?>[value];
            localStorage.setItem("fitersTagspoi"+page,JSON.stringify(tagsFilter<?= $kunik?>));
            urlCtrl.loadByHash(location.hash);
        })
        $(".addDoc<?= $blockCms['_id'] ?>").click(function(){
            dyFObj.openForm('poi',null, null,null,dyfPoiDoc);
        })
        getDoc<?= $kunik?>();
        function getDoc<?= $kunik?>(){  
            var params = {
                searchType : ["poi"],
                filters : {
                    type : "affiche",
                    $or : {
                        tags : ["<?= $page?>"], 
                        subtype : "<?= $page?>"
                    }
                },
                fields : ["urls","parent"],
                sortBy :["name"]

            };
            if(notEmpty(tagsFilter<?= $kunik?>)){
                params.filters.tags = [];
                $.each(tagsFilter<?= $kunik?>,function(k,v){
                    params.filters.tags.push(k)
                })
            }
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    var html = "";
                        $.each(data.results, function( index, value ) {
                            var lbh = "lbh-preview-element";
                            // if(typeof value.slug != "undefined")
                            //     lbh = "lbh";
                            category = exists(value.category)?value.category:"";
                            html += 
                                '<div class="col-md-4 col-sm-6 both-box mb-30 text-center" style="height:350px">'+
                                    '<div class="">'+       
                                        '<div class=" ">'+
                                            '<h4 class="card-title title-2">'+value.name+'</h4>'+
                                        '</div>';
                                        <?php if($costum["editMode"] == "true"){?>
                                            html +='<ul class="icon-<?=$kunik?> hiddenPreview">'+
                                                '<li>'+
                                                    '<a href="javascript:;" class="edit" data-type="'+value.type+'" data-id="'+value._id.$id+'" ><i class="fa fa-edit"></i></a>'+
                                                '</li>'+
                                                '<li>'+
                                                    '<a href="javascript:;" class="delete" data-type="'+value.type+'" data-id="'+value._id.$id+'" ><i class="fa fa-trash"></i></a>'+
                                                '</li>'+
                                            '</ul>';
                                        <?php } ?>
                                        if ((value.category  == "link" && typeof value.urls!= "undefined" && typeof value.urls[0]!= "undefined")) 
                                            html +=  
                                                '<a href="'+value.urls[0]+'" target="_blank">';
                                        else 
                                            html +=         
                                            '<a href="#page.type.poi.id.'+value._id.$id+'" class="'+lbh+ '" "'+category+' ">';
                                        if (typeof value.profilMediumImageUrl && value.profilMediumImageUrl != null) 
                                            html +=  '<img class="" src="'+value.profilMediumImageUrl+'" alt="card image">';
                                        else
                                            html +=         
                                                '<img class="" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/ctenat/mars2019.png"> ';

                                    html += '</a>';
                            
                                html += '</div>'+
                            '</div>';
                        });
                        $(".card_<?=$kunik?>").html(html);
                        $(".card_<?=$kunik?> .edit").off().on('click',function(){
                            var id = $(this).data("id");
                            var type = $(this).data("type");
                            dyFObj.editElement('poi',id,type,dyfPoiDoc);
                        });
                        $(".card_<?=$kunik?> .delete").off().on("click",function () {
                            $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                            var btnClick = $(this);
                            var id = $(this).data("id");
                            var type = "poi";
                            var urlToSend = baseUrl+"/"+moduleId+"/element/delete/type/"+type+"/id/"+id;
                            
                            bootbox.confirm("voulez vous vraiment supprimer cette actualité !!",
                            function(result) {
                                if (!result) {
                                    btnClick.empty().html('<i class="fa fa-trash"></i>');
                                    return;
                                } else {
                                    ajaxPost(
                                        null,
                                        urlToSend,
                                        null,
                                        function(data){ 
                                            if ( data && data.result ) {
                                            toastr.success("élément effacé");
                                            $("#"+type+id).remove();
                                            getDoc<?= $kunik?>();
                                            } else {
                                            toastr.error("something went wrong!! please try again.");
                                            }
                                        }
                                    );
                                }
                            });
                        });                 
                    coInterface.bindLBHLinks();   
               }
            );
        }
    });
</script>