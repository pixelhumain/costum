<?php

/**
 * @deprecated
 */

use PixelHumain\PixelHumain\modules\citizenToolKit\components\UtilsHelper;
use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;

$blockKey = $blockKey ?? sha1(date('c'));
$connectedUser = Yii::app()->session['userId'] ?? '';
$sub_events = [];
$excludeIdsList = [];

// $isAdmin = !empty(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"])) || Form::canAdmin((string)$elform["_id"]);
if (!function_exists('get_actor')) {
    function get_actor($context, $id, $element = []) {
        $db_actor = PHDB::findOneById($element['type'], $id, ['name', 'email', 'profilImageUrl']);
        $actor = [
            'id'    => $id,
            'image' => '',
            'name'  => $db_actor['name']
        ];
        return $actor;
    }
}

if (!function_exists('getAllEvents')) {
    function getAllEvents($id, $options = ['subevents' => false]) {
        if (empty($id)) return [];
        $dbCurrentEventsList = PHDB::findByIds(Event::COLLECTION, $id, [
            'creator',
            'name',
            'description',
            'shortDescription',
            'startDate',
            'endDate',
            'slug',
            'timeZone',
            'type',
            'links.subEvents',
            'links.subEvent',
            'profilImageUrl'
        ]);
        $events = [];
        $subEventsIdsList = [];
        foreach ($dbCurrentEventsList as $currentEventId => $dbCurrentEvent) {
            $start = new DateTime();
            $end = new DateTime();
            $timezone = $dbCurrentEvent['timeZone'] ?? date_default_timezone_get();
            $start->setTimestamp(UtilsHelper::get_as_timestamp(['startDate'], $dbCurrentEvent));
            $start->setTimezone(new DateTimeZone($timezone));
            $end->setTimestamp(UtilsHelper::get_as_timestamp(['endDate'], $dbCurrentEvent));
            $end->setTimezone(new DateTimeZone($timezone));
            $events[] = [
                'id'               => $currentEventId,
                'slug'             => $dbCurrentEvent['slug'] ?? '',
                'type'             => $dbCurrentEvent['type'],
                'name'             => $dbCurrentEvent['name'],
                'shortDescription' => $dbCurrentEvent['shortDescription'] ?? '',
                'description'      => $dbCurrentEvent['description'] ?? '',
                'profilImageUrl'   => $dbCurrentEvent['profilImageUrl'] ?? '',
                'startDate'        => $start->format('c'),
                'endDate'          => $end->format('c'),
            ];
            $excludeIdsList[] = $currentEventId;
            if (!empty($dbCurrentEvent['links']['subEvents'])) $subEventsIdsList = array_merge($subEventsIdsList, array_keys($dbCurrentEvent['links']['subEvents']));
            if (!empty($dbCurrentEvent['links']['subEvent'])) $subEventsIdsList = array_merge($subEventsIdsList, array_keys($dbCurrentEvent['links']['subEvent']));
        }
        foreach ($excludeIdsList as $exclude) {
            if (in_array($exclude, $subEventsIdsList)) {
                $index = array_search($exclude, $subEventsIdsList);
                array_splice($subEventsIdsList, $index, 1);
            }
        }
        if ($options['subevents'] && !empty($subEventsIdsList)) {
            $events = array_merge($events, getAllEvents($subEventsIdsList, $options));
        }
        return $events;
    }
}

if (isset($this->costum['contextType']) && $this->costum['contextType'] === 'events') {
    $ownerEvent = PHDB::findOneById(Event::COLLECTION, $this->costum['contextId']);
    $subEventsDatabase = PHDB::findAndSort(Event::COLLECTION, ['parent.' . ((string)$ownerEvent['_id']) => ['$exists' => true]], ['startDate' => 1], 0, ['description', 'shortDescription', 'organizerName', 'endDate', 'name', 'startDate', 'timeZone', 'type', 'links.organizer', 'links.attendees', 'links.creator', 'profilRealBannerUrl', 'slug', 'collection', 'address.streetAddress', 'address.addressLocality', 'address.postalCode']);
    $subEvents = [];
    $categories = [];
    $dates = [];
    $temporary_actors = [];
    $actors = [];
    $lastDateIndex = '';
    $lastDateValue = [];
    $participants = [];
    // attendees
    // if (!empty($ownerEvent['links']['attendees'])) {
    //     foreach ($ownerEvent['links']['attendees'] as $key => $value) {
    //         if (!in_array($key, $tempActors)) {
    //             $actor = get_actor($this->costum, $key, $value);
    //             // if (!$isAdmin) $isAdmin = $connectedUser === (string)$actor['_id'];
    //             $tempActors[] = $key;
    //             $actors[] = $actor;
    //         }
    //     }
    // }

    // organizer
    if (!empty($owner_event['links']['organizer'])) {
        foreach ($owner_event['links']['organizer'] as $key => $value) {
            if (!in_array($key, $temporary_actors)) {
                $actor = get_actor($this->costum, $key, $value);
                // if (!$isAdmin) $isAdmin = $connectedUser === (string)$actor['_id'];
                $tempActors[] = $key;
                $actors[] = $actor;
            }
        }
    }

    foreach ($subEventsDatabase as $subEvent) {
        $subEvent['communities'] = [];
        $address = '';
        if (!empty($subEvent['address'])) {
            $address_grouped = [];
            if (!empty($subEvent['address']['streetAddress'])) $address_grouped[] = $subEvent['address']['streetAddress'];
            if (!empty($subEvent['address']['postalCode'])) $address_grouped[] = $subEvent['address']['postalCode'];
            if (!empty($subEvent['address']['addressLocality'])) $address_grouped[] = $subEvent['address']['addressLocality'];
            $address = implode(', ', $address_grouped);
        }
        $subEvent['address'] = $address;
        // attendees
        // if (!empty($subEvent['links']['attendees'])) {
        //     foreach ($subEvent['links']['attendees'] as $key => $value) {
        //         $subEvent['communities'][] = $key;
        //         if (!in_array($key, $tempActors)) {
        //             $actor = get_actor($this->costum, $key, $value);
        //             // if (!$isAdmin) $isAdmin = $connectedUser === (string)$actor['_id'];
        //             $tempActors[] = $key;
        //             $actors[] = $actor;
        //         }
        //     }
        // }

        // organizer
        if (!empty($subEvent['links']['organizer'])) {
            foreach ($subEvent['links']['organizer'] as $key => $value) {
                $subEvent['communities'][] = $key;
                if (!in_array($key, $temporary_actors)) {
                    $actor = get_actor($this->costum, $key, $value);
                    // if (!$isAdmin) $isAdmin = $connectedUser === (string)$actor['_id'];
                    $tempActors[] = $key;
                    $actors[] = $actor;
                }
            }
        }

        if (!empty($subEvent['organizerName'])) {
            $new = [
                'id' => $subEvent['organizerName'],
                'type' => '',
                'name' => $subEvent['organizerName']
            ];
            $subEvent['communities'][] = $subEvent['organizerName'];
            $actors[] = $new;
        }

        $subEvent['detailed'] = false;

        $dateTime = $subEvent['startDate']->toDateTime();
        $dateTime->setTimezone(new DateTimeZone($subEvent['timeZone']));
        $dateFormat = $dateTime->format('Y-m-d');
        $subEvent['formattedDate'] = $dateTime->format('d.m.Y');
        $subEvent['isodate'] = $dateTime->format('c');
        $subEvent['normalizedDate'] = $dateTime->format('Y-m-d');
        $subEvent['hour'] = $dateTime->format('H');

        if ($lastDateIndex != $dateFormat) {
            $lastDateIndex = $dateFormat;
            $dates[] = [$dateFormat => []];
        }
        $dates[count($dates) - 1][$dateFormat][] = [
            'min'   => $dateTime->format('i'),
            'hour'  => $dateTime->format('H'),
            'day'   => $dateTime->format('d'),
            'month' => $dateTime->format('M'),
            'year'  => $dateTime->format('Y'),
            'event' => (string)$subEvent['_id'],
            'type'  => $subEvent['type']
        ];

        if (!array_key_exists($subEvent['type'], $categories)) {
            $categories[$subEvent['type']] = $subEvent['type'];
        }

        $subEvent['profilRealBannerUrl'] = $subEvent['profilRealBannerUrl'] ?? $this->costum['assetsUrl'] . '/images/coevent/thumbnail-default.jpg';

        $sub_events[] = $subEvent;
    }
    $paramsData = [
        'logo'             => $owner_event['profilImageUrl'] ?? '/assets/6b85ffc2/images/coevent/thumbnail-default.jpg',
        'banniere'         => $owner_event['profilRealBannerUrl'] ?? '/assets/6b85ffc2/images/coevent/thumbnail-default.jpg',
        'aboutBanniere1'   => $this->costum['assetsUrl'] . '/images/coevent/points_block_membre.png',
        'aboutBanniere2'   => $this->costum['assetsUrl'] . '/images/coevent/img4.jpg',
        'aboutBanniere3'   => $this->costum['assetsUrl'] . '/images/coevent/img3.png',
        'sectionSeparator' => $this->costum['assetsUrl'] . '/images/coevent/trait.png',
        'primary_color'    => '#EE302C',
        'programContent'   => 'Contenu du programme',
        'show_map'         => 'true'
    ];

    $defaultTexts = [
        'mainTitle'        => 'Qui sommes-nous ?',
        'aboutTitle'       => 'titre',
        'aboutContent'     => 'Contenu'
    ];

    foreach ($defaultTexts as $key => $defaultValue) {
        ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
        ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

        $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
    };



    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e])) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
    $scripts_and_css = [];
    HtmlHelper::registerCssAndScriptsFiles($scripts_and_css, Yii::app()->request->baseUrl);
?>
    <link title="timeline-styles" rel="stylesheet" href="https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css">
    <script src="https://cdn.knightlab.com/libs/timeline3/latest/js/timeline.js"></script>
    <style>
        .mt-3 {
            margin-top: 1.5rem;
        }

        div {
            -moz-user-select: none;
            -webkit-user-select: none;
            user-select: none;
        }

        .text-white {
            color: white;
        }

        #dropdown {
            background-color: whitesmoke;
            width: 55% !important;
            z-index: 1000;
        }

        .containjourney {
            border-radius: 13px;
            cursor: pointer;
        }

        .containjourney .date {
            color: white;
            background-color: <?= $paramsData['primary_color'] ?>;
            padding: 10px;
            border-radius: 10px;
            margin-left: 2%;
            margin-right: 2%;
        }

        .containjourney .journey {
            color: black;
        }

        .shortdescsub_event {
            font-family: 'DIN-Regular';
        }

        .containjourney .dateactive {
            background-color: black;
            color: white;
            padding: 10px;
            border-radius: 10px;
        }

        .containjourney .journeyactive {
            color: <?= $paramsData['primary_color'] ?>;
        }

        .datelongactive {
            display: initial;
        }

        .datelong {
            display: none;
        }

        .imgProfil {
            max-height: 150px;
        }

        .header {
            position: relative;
        }

        .class<?= $kunik ?>about-link-container {
            position: absolute;
            top: 0;
            left: 90px;
            padding: 2rem 5rem 2rem 2rem;
            border-radius: 20px;
            background: <?= $paramsData['primary_color'] ?>;
            color: white;
            font-size: 1.5rem;
            text-align: left;
            width: auto;
        }

        .class<?= $kunik ?>about-link-container a {
            display: block;
            cursor: pointer;
            text-decoration: none;
            color: inherit;
            padding: 0 2rem 0 0;
        }

        .class<?= $kunik ?>about-link-container a.active {
            padding: 0 0 0 2rem;
        }

        .class<?= $kunik ?>about-link-container a:hover {
            text-decoration: underline;
        }

        .class<?= $kunik ?>about-link-container a.active:hover {
            text-decoration: none;
        }

        .coevent-button {
            border: none;
            border-radius: 20px;
            padding: 1rem 2rem;
            background-color: <?= $paramsData['primary_color'] ?>;
            color: white;
        }

        .coevent-button:hover {
            text-decoration: none;
        }

        .coevent-button.inverted {
            color: <?= $paramsData['primary_color'] ?>;
            background-color: white;
        }

        #journee-caroussel {
            position: relative;
        }

        #items {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        #items .item {
            cursor: pointer;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        #items .item .date {
            border: <?= $paramsData['primary_color'] ?> 1px solid;
            background: <?= $paramsData['primary_color'] ?>;
            color: white;
            padding: 1.2rem 1.5rem .8rem;
            border-radius: 10px;
            display: block;
            font-weight: bold;
            font-size: 1.9rem;
            width: fit-content;
            width: -moz-fit-content;
        }

        #items .item.active .date {
            border-color: black;
            background-color: white;
            color: black;
        }

        #items .item .jour,
        .month {
            text-align: center;
            font-weight: bold;
            display: block;
            margin: auto 10px;
        }

        .journee-control {
            position: absolute;
            top: 5px;
            padding: 1.9rem 1rem;
            border-radius: 10px;
            border: transparent 1px solid;
            background-color: transparent;
            color: <?= $paramsData['primary_color'] ?>;
        }

        .journee-control:hover {
            border-color: <?= $paramsData['primary_color'] ?>;
            background-color: white;
            cursor: pointer;
        }

        .journee-control.right {
            right: 40px;
        }

        .journee-control.left {
            left: 40px;
        }

        .caroussel-button {
            width: 20px;
        }

        #event-content {
            border-left: <?= $paramsData['primary_color'] ?> solid 5px;
            padding: 2rem;
            margin-left: 5rem;
        }

        #event-content .date {
            font-size: 2rem;
            font-weight: bold;
            display: block;
        }

        #event-content ul {
            margin: none;
        }

        #event-content .event-detail {
            position: relative;
            font-weight: bold;
        }

        #event-content .event-detail li {
            margin-top: 2rem;
        }

        #event-content .event-detail li::before {
            content: "\2022";
            color: <?= $paramsData['primary_color'] ?>;
            font-weight: bold;
            font-size: 1.5em;
            display: inline-block;
            width: .8em;
            margin-left: -1em;
        }

        #event-content .event-detail-detail li {
            margin-top: 0;
            line-height: 1;
        }

        #event-content .event-detail-detail li::before {
            color: black;
        }

        #event-content .event-hour {
            font-weight: bold;
            position: absolute;
            left: -7rem;
            top: .7rem;
            color: <?= $paramsData['primary_color'] ?>;
        }

        .slider {
            height: 100%;
            width: 100%;
            margin: 0;
        }

        .coevent-slider {
            background-color: <?= $paramsData['primary_color'] ?>;
            padding: 5em;
        }

        .navigation-caroussel {
            cursor: pointer;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            color: white;
            font-size: 2em;
            background-color: <?= $paramsData['primary_color'] ?>;
            height: 100%;
            padding: 50% .5em;
        }

        .navigation-caroussel.prev {
            left: 0;
        }

        .navigation-caroussel.next {
            right: 0;
        }

        .event-slide-title {
            font-weight: bold;
            font-size: 2em;
        }

        .event-slide-specification {
            color: white;
            font-size: 1.4rem;
        }

        .event-slide-description {
            text-align: justify;
            color: white;
            font-size: 1.4rem;
        }

        .card-content {
            background-color: white;
        }

        .footer-section {
            margin-top: 3%;
            height: 530px;
        }

        .card-acteurs {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            padding: 1em 1em 4em;
            display: flex;
            justify-content: space-evenly;
        }

        .card-acteur {
            border-radius: 10px;
            max-width: 300px;
            flex: 1;
            background-color: white;
            box-shadow: 0 0 10px rgba(0, 0, 0, .5);
            overflow: hidden;
        }

        .card-acteur .card-header {
            height: 200px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }

        .card-acteur .card-content {
            position: relative;
            padding: 1em;
            font-weight: bold;
        }

        .card-acteur .card-content .badget {
            font-size: 1.5em;
            display: block;
            color: white;
            background-color: <?= $paramsData['primary_color'] ?>;
            border-radius: 5px;
            position: absolute;
            left: 15px;
            top: -25px;
            padding: .1em .2em;
        }

        .card-acteur .card-content .acteur-nom {
            color: <?= $paramsData['primary_color'] ?>;
            display: block;
        }

        .slide-container {
            position: relative;
            width: 100%;
            overflow: hidden;
        }

        #block-img3 {
            width: 320px;
            position: absolute;
            top: 170px;
            left: 150px;
        }

        .program-container {
            text-align: left;
        }

        @media only screen and (max-width: 425px) {
            .about-image {
                display: none;
            }

            .journee-control.left {
                left: 0;
            }

            .journee-control.right {
                right: 0;
            }
        }

        #active_filter_container {
            background-color: <?= $paramsData['primary_color'] ?>;
            color: white;
            position: fixed;
            left: 50px;
            padding: 10px;
            border-radius: 0 0 10px 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, .3);
            z-index: 9;
            border-left: 1px solid rgba(0, 0, 0, .1);
            border-bottom: 1px solid rgba(0, 0, 0, .1);
            border-right: 1px solid rgba(0, 0, 0, .1);
            cursor: pointer;
        }

        @media only screen and (max-width: 992px) {
            #block-img3 {
                display: none;
            }

            .program-container {
                text-align: left;
                max-width: 100%;
            }
        }
    </style>
    <style>
        .slider {
            width: 100%;
            overflow: hidden;
            margin: 0 auto;
            position: relative;
        }

        .slides {
            display: flex;
            flex-direction: row;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
        }

        .slide {
            list-style: none;
            flex: 0 0 auto;
            width: 100%;
        }

        .event-detail>li {
            list-style: none;
        }

        .slide-element {
            width: 100%;
            height: 100%;
        }

        .slide-indicators {
            margin: 0;
            padding: 5px;
            position: absolute;
            display: flex;
            flex-direction: row;
            bottom: 12px;
            left: 50%;
            transform: translateX(-50%);
            background-color: rgba(0, 0, 0, .3);
            border-radius: 20px;
            line-height: 0;
        }

        .slide-navigator {
            list-style: none;
            display: block;
            padding: 15px 10px;
            background-color: transparent;
            margin-top: -5px;
            margin-bottom: -5px;
            color: white;
            cursor: pointer;
            align-self: center;
        }

        .slide-navigator:hover {
            background-color: white;
            color: black;
        }

        .slide-navigator.prev {
            margin-left: -6px;
            border-right: 1px solid rgba(255, 255, 255, .5);
            border-radius: 20px 0 0 20px;
        }

        .slide-navigator.next {
            margin-right: -6px;
            border-left: 1px solid rgba(255, 255, 255, .5);
            border-radius: 0 20px 20px 0;
        }

        .slide-indicator {
            display: block;
            list-style: none;
            margin-left: 5px;
            margin-right: 5px;
            align-self: center;
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: transparent;
            border: white 1px solid;
        }

        .slide-indicator {
            cursor: pointer;
        }

        .slide-indicator.active {
            background-color: white;
        }

        #second-search-bar {
            padding-left: 45px;
            width: calc(100% - 50px);
            text-align: left;
        }

        #search-button {
            background: <?= $paramsData['primary_color'] ?>;
            width: 50px;
            border-radius: 0 30px 30px 0;
        }

        #search-dropdown {
            margin-left: 60px;
        }

        .tl-headline {
            padding: 2px;
        }

        .tl-slidenav-title {
            opacity: 1 !important;
        }
    </style>

    <script>
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>
        } else 

        appendTextLangBased(".mainTitle<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($mainTitle) ?>, "<?= $blockKey ?>");
        appendTextLangBased(".aboutTitle<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($aboutTitle) ?>, "<?= $blockKey ?>");
        appendTextLangBased(".aboutContent<?= $blockKey ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($aboutContent) ?>, "<?= $blockKey ?>");
    </script>
    <!-- Header -->
    <div id="active_filter_container" class="tooltips" data-toggle="tooltip" data-placement="bottom" data-original-title="Cliquez ici pour désactiver le filtre" area-describedby="tooltipeventfilter" style="display: none;">
        Evénements filtrés sur la catégorie : <span id="active_filter_text" data-type=""></span>
        <div id="tooltipeventfilter" class="tooltip">Cliquez ici pour désactiver le filtre</div>
    </div>
    <div class="container-fluid">
        <div id="quisommesnous" style="margin-bottom: 3%;" class="row infos">
            <div class="col-md-12">
                <h1 class="dinalternate text-center sp-text mainTitle<?= $blockKey ?>" style="color: black;" data-id="<?= $blockKey ?>" data-field="mainTitle"></h1>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6" style="position: relative;">
                        <img src="<?= $paramsData['aboutBanniere1'] ?>" alt="block membre" style="width: 100px;">
                        <img src="<?= $paramsData['aboutBanniere2'] ?>" alt="block img4" class="about-image" style="width: 250px; margin-left: 75px;">
                        <img src="<?= $paramsData['aboutBanniere3'] ?>" alt="block img3" id="block-img3" class="about-image">
                        <div class="class<?= $kunik ?>about-link-container">
                            <span style="display: block; margin-bottom: 10px;">A PROPOS</span>
                        </div>
                    </div>
                    <div class="col-md-6" style="text-align: left;">
                        <h2 class="sp-text aboutTitle<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="aboutTitle"></h2>
                        <div class="sp-text aboutContent<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="aboutContent"></div>
                        <!--                        <button class="coevent-button">VOIR PLUS</button>-->
                    </div>
                </div>
            </div>
            <img style="width:100%;" class="margin-top-20" src="<?= $paramsData['sectionSeparator'] ?>">
        </div>
        <div class="container" style="margin-top: 3%;">
            <div class="dropdown">
                <div class="hidden-xs container" id="search-bar">
                    <a data-type="filters" href="javascript:;">
                        <span style="margin-left: 0%;position: absolute;width: 1%;background: transparent;border: transparent;" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                            <i style="color: <?= $paramsData['primary_color'] ?>;font-size: 29px;" id="fa-search" class="fa fa-search"></i>
                        </span>
                    </a>
                    <input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Rechercher un événement">
                    <a href="javascript:;">
                        <span class="text-white input-group-addon pull-left main-search-bar-addon-mednum" id="search-button">
                            <i style="font-size: 26px;" id="fa-search" class="fa fa-search"></i>
                        </span>
                    </a>
                </div>
                <ul class="dropdown-menu" role="menu" aria-labelledby="search-dropdown" id="search-dropdown"></ul>
            </div>
            <br>

            <div id="leprogramme" class="row">
                <div class="col-sm-12 program-container">
                    <h1 class="dinalternate" style="color: black;">
                        Le programme
                    </h1>
                    <div style="border: solid <?= $paramsData['primary_color'] ?> 4px;border-radius: 11px;width: 17%; margin-bottom: 15px;"></div>
                    <div class="btn-group journee_views" style="margin-bottom: 1em;">
                        <button type="button" class="btn btn-default" onclick="change_views(this, 0)">Calendrier</button>
                        <button type="button" class="btn btn-default" onclick="change_views(this, 1)">Filtre par journée</button>
                        <button type="button" class="btn btn-default" onclick="change_views(this, 2)">Chronologie</button>
                    </div>

                    <div id="journee-caroussel">
                        <div class="slider">
                            <div class="slides" id="journee-carousel">
                            </div>
                        </div>
                        <div id="journee-navigation">
                            <a class="journee-control left" data-slide="prev">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="journee-control right" data-slide="next">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div id="event-content"></div>
                </div>
            </div>

            <!-- vue calendrier des events -->
            <div id="event_calendar"></div>
            <!-- vue calendrier des events -->
            <div id="chronology_container">
                <div id='timeline-embed' style="width: 100%; height: 55vh; margin-top: 50px;"></div>
            </div>

            <br>
            <div class="text-center">
                <button class="coevent-button" type="button" onclick="return coevent_create_event()">
                    <i class="fa fa-calendar"></i> Proposer un évènement
                </button>
                <button class="coevent-button" type="button" onclick="return downloadPdf('<?= $this->costum['contextId'] ?>')">
                    <i class="fa fa-file-pdf-o"></i> Télécharger le programme
                </button>
            </div>
        </div>
        <br><br>
        <!-- <h1 id="chronologie" class="tooltips dinalternate text-center margin-top-20" data-toggle="tooltip" data-placement="bottom" data-original-title="Naviguez dans la frise chronologique" area-describedby="tooltipchronologie">Chronologie de <?= $this->costum['title'] ?>
            <div id="tooltipchronologie" class="tooltip">Naviguez dans la frise chronologique</div>
        </h1> -->
        <div class="row footer-section">
            <div id="acteursetparticipants">
                <div class="col-md-12">
                    <h1 class="dinalternate text-center" style="color: black;">
                        Acteurs et participants
                    </h1>
                </div>
                <div class="col-xs-12">
                    <div class="container" style="position: relative;">
                        <img src="<?= $this->costum['assetsUrl'] ?>/images/coevent/points_block_membre.png" alt="block membre" style="width: 100px; position: absolute; top: 20px; left: 0;">
                        <div class="row">
                            <div class="slider">
                                <ul class="slides" id="acteur">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="text-align: center; padding: 1em;">
        <button id="show_hide_map_container" class="btn btn-primary" onclick="show_hide_map(this)">
            <?php if ($paramsData['show_map']) { ?>
                <i class="fa fa-eye-slash"></i> Cacher la carte des évènements
            <?php } else { ?>
                <i class="fa fa-eye"></i> Afficher la carte des évènements
            <?php } ?>
        </button>
    </div>
    <div id="map_container" style="display: <?= $paramsData['show_map'] ? 'block' : 'none' ?>;">
        <?= //$this->renderPartial('costum.views.tpls.blockCms.map.basicmaps', ['kunik' => $kunik, 'el' => [], 'blockKey' => $blockKey, "blockCms" => array("activeFilter" => "false", "btnAnnuaireText" => "Voir le calendrier", "btnBackground" => $paramsData['primary_color'], "btnLink" => 'leprogramme', "isBtnLbh" => "false")])
        BlockCmsWidget::widget([
            "path" => "tpls.blockCms.map.basicmaps",
            //cette config (notFoundViewPath) peut être enlever quand tout les blocks cms sont transformés en Widget
            "notFoundViewPath" => "costum.views.tpls.blockCms.map.basicmaps",
            "config" => [
                'costum' => $costum,
                'kunik' => $kunik,
                'el' => [],
                'blockKey' => $blockKey,
                "blockCms" => array(
                    "activeFilter" => false,
                    "btnAnnuaire" => array(
                        'text' => "Voir le calendrier",
                        "link" => "leprogramme",
                        "isLbh" => false
                    ),
                    "css" => array(
                        "styleAnnuaire" => array(
                            'backgroundColor' => $paramsData['primary_color']
                        )
                    )
                )
            ]
        ]);
        ?>
    </div>
    <script type="text/javascript" src="<?= $this->costum['assetsUrl'] ?>/js/coevent/carousel.js"></script>
    <script type="text/javascript">
        const sub_events = JSON.parse(JSON.stringify(<?= json_encode($sub_events) ?>));
        const dates = JSON.parse(`<?= json_encode($dates) ?>`);
        const actors = JSON.parse(`<?= json_encode($actors) ?>`);
        const day_names = ['DIM', 'LUN', 'MAR', 'MER', 'JEU', 'VEN', 'SAM'];
        let selected_date = '';
        let subevents_view = sub_events.slice();
        let dates_view = [];
        dates.slice().forEach(date => {
            for (let grouped in date) {
                date[grouped].forEach(event => {
                    event.composite = grouped;
                    dates_view.push(event);
                });
            }
        });

        function change_views($element, $view) {
            $('.journee_views button').removeClass('active');
            $($element).addClass('active');
            switch ($view) {
                case 0:
                    $('#event_calendar').css('display', '');
                    $('#journee-caroussel').css('display', 'none');
                    $('#chronology_container').css('display', 'none');
                    $('.program-container').next('div').css('display', 'none');
                    load_fullcalendar($(document.getElementById('active_filter_text')).data('type'));
                    break;
                case 1:
                    $('#event_calendar').css('display', 'none');
                    $('#journee-caroussel').css('display', '');
                    $('#chronology_container').css('display', 'none');
                    $('.program-container').next('div').css('display', '');
                    break;
                case 2:
                    $('#event_calendar').css('display', 'none');
                    $('#journee-caroussel').css('display', 'none');
                    $('#chronology_container').css('display', '');
                    $('.program-container').next('div').css('display', 'none');
                    break;
            }
        }

        function show_hide_map(element) {
            var content = {
                hide: '<i class="fa fa-eye-slash"></i> Cacher la carte des adresses',
                show: '<i class="fa fa-eye"></i> Afficher la carte des adresses'
            }
            var shown = $('#map_container').is(':visible');
            const params = {
                id: `<?= $blockKey ?>`,
                collection: 'cms',
                path: 'allToRoot',
                value: {
                    'show_map': !shown
                },
                setType: [{
                    path: 'show_map',
                    type: 'boolean'
                }]
            };
            dataHelper.path2Value(params, function(callback) {
                toastr.success('Modification avec succès');
            });

            $('#map_container').css({
                display: shown ? 'none' : 'block'
            });
            $(element).html(content[shown ? 'show' : 'hide']);
        }

        function resetJourneeCarousel() {
            return carousel({
                container: $('#journee-carousel'),
                autoplay: false,
                navigation: {
                    indicator: false,
                    button: {
                        show: false,
                        next: '<span class="fa fa-chevron-right"></span>',
                        prev: '<span class="fa fa-chevron-left"></span>'
                    }
                },
                delay: 2000
            });
        }

        function resetCarouselProgramme() {
            const output = carousel({
                container: $('#carousel-programme'),
                autoplay: false,
                navigation: {
                    indicator: false
                },
                animationDuration: 300,
                delay: 100
            });
            output.init();
            output.onSlide = element => {
                const event = sub_events.find(sub_event => sub_event['_id']['$id'] === element.attr('id'));
                const image_loader = new Image();
                image_loader.onload = function() {
                    $('#carousel-programme-container').css({
                        backgroundImage: `url('${this['src']}')`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                    });
                };
                image_loader['src'] = event['profilRealBannerUrl'];
            };
            return output;
        }

        function filterEventPerType(element) {
            // partie affichage
            const eventTypes = [...document.querySelectorAll('.event-type-filter')];
            eventTypes.forEach(other => {
                if (other !== element)
                    other.classList.remove('active');
            });
            element.classList.toggle('active');
            var type = '';
            if (element.classList.contains('active')) {
                type = element.getAttribute('href');
                $('#active_filter_text').text(tradCategory[type]);
                $('#active_filter_text').data('type', type);
                $('#active_filter_container').fadeIn(800, function() {
                    $(this).css({
                        'display': '',
                        top: `${$('#mainNav').outerHeight()}px`
                    });
                    $(this).off('click').on('click', function() {
                        var active = document.querySelector('.event-type-filter.active');
                        filterEventPerType(active);
                    });
                });
                // smooth scroll
                $('html, body').animate({
                    scrollTop: $('.program-container').offset().top - $('#mainNav').outerHeight()
                }, 500);
            } else {
                $('#active_filter_container').fadeOut(800);
                $('#active_filter_text').data('type', '');
            }

            // partie filtre
            if (element.classList.contains('active')) {
                subevents_view = sub_events.slice().filter(subEvent => subEvent['type'] === type);
                dates_view = [];
                dates.slice().forEach(date => {
                    for (let grouped in date) {
                        const filtered = date[grouped].filter(event => event.type === type);
                        filtered.forEach(event => {
                            event.composite = grouped;
                            dates_view.push(event);
                        });
                    }
                });
            } else {
                subevents_view = sub_events.slice();
                dates_view = [];
                dates.slice().forEach(date => {
                    for (let grouped in date) {
                        date[grouped].forEach(event => {
                            event.composite = grouped;
                            dates_view.push(event);
                        });
                    }
                });
            }
            load_fullcalendar(type);
            load_timeline(type);
            loadViews();
        }

        function downloadPdf(event) {
            document.location.href = `${baseUrl}/costum/coevent/getpdfaction?event=<?= $this->costum['contextId'] ?>`;
            return false;
        }

        function loadViews() {
            load_journees();
            loadProgrammes();
            if (sub_events.length > 0) loadProgrammeSlide();
            coInterface.bindLBHLinks();
        }

        function load_journees() {
            const container = $('#journee-carousel');
            var date_view_limit = 3;
            var type = $('#active_filter_container').is(':hidden') ? '' : $('#active_filter_text').data('type');
            get_subevent_dates(type).then(function(dbDates) {
                var change = 0;

                function buildDateCarousel(dates, change) {
                    var html = '<div class="slide"><div id="items">';
                    var last_group = '';
                    dates.forEach(function(aDate, index) {
                        if (change % date_view_limit === 0 && change !== 0) {
                            html += '</div></div><div class="slide"><div id="items">';
                        }
                        if (last_group !== aDate['date_group']) {
                            change++;
                            last_group = aDate['date_group'];
                            html += `<div class="item" data-target="${last_group}" onclick="loadProgrammes('${last_group}', this)">
                                    <span class="month">${trad[aDate.month.toLowerCase()].toUpperCase()}</span>
                                    <span class="date">${aDate.date}</span>
                                    <span class="jour">${trad[aDate.day.toLowerCase()].toUpperCase()}</span>
                                </div>`;
                        }
                    });
                    html += '</div></div>';
                    return html;
                }
                var dateHtml = '';
                if (dbDates.fromNow.length) {
                    dateHtml = buildDateCarousel(dbDates.fromNow, change);
                }
                if (dbDates.before.length) {
                    dateHtml += buildDateCarousel(dbDates.before, change);
                }
                container.html(dateHtml);
                if (change > date_view_limit) {
                    journeeCarousel = resetJourneeCarousel();
                    journeeCarousel.init();
                    $('.journee-control').css({
                        display: 'block'
                    });
                } else $('.journee-control').css({
                    display: 'none'
                });
            });
        }

        async function loadProgrammes(date, element = null) {
            var communities = await get_actors(['links.organizer', 'organizer', 'organizerName']);
            const container = $('#event-content');
            let html = '';
            let lastHour = '';
            var selected_date = typeof date === 'undefined' ? '' : date;
            const loop = selected_date === '' ? (subevents_view.length > 0 ? subevents_view.slice().filter(subEvent => subEvent['formattedDate'] === subevents_view[0]['formattedDate']) : []) : subevents_view.slice().filter(subEvent => subEvent['formattedDate'] === date);
            loop.forEach((view, index) => {
                var organizers = communities.filter(function(filter) {
                    return typeof filter['roles'] != 'undefined' && typeof filter['roles'][view['_id']['$id']] != 'undefined';
                }).map(function({
                    name,
                    image
                }) {
                    return {
                        name,
                        image
                    }
                });

                if (index === 0) html += `<span class="dinalternate date">${view['formattedDate']}</span>`;
                if (view['hour'] !== lastHour) {
                    lastHour = view['hour'];
                    if (index !== 0) html += '</ul>';
                    html += `<ul class="event-detail"><span class="event-hour">${moment(view['isodate']).format('H')}H</span>`;
                }
                var actors_with_id = [];
                if (userConnected) {
                    actors_with_id = view['communities'].filter(community => community === userConnected['_id']['$id']);
                }

                var isFollowed = false;
                if (userId != "" && typeof view.links !== 'undefined' && typeof view.links.attendees != 'undefined' && typeof view.links.attendees[userId] != "undefined") isFollowed = true;
                var isShared = false;
                var tip = trad['interested'];
                var actionConnect = 'follow';
                var icon = 'chain';
                var classBtn = '';
                if (isFollowed) {
                    actionConnect = 'unfollow';
                    icon = 'unlink';
                    classBtn = 'text-green';
                    tip = "Je ne veux plus participer";
                }

                var btnLabel = (actionConnect == "follow") ? "PARTICIPER" : "DÉJÀ PARTICIPANT(E)";

                html += `<li>
                    <a href="#page.type.${view.collection}.id.${view._id.$id}" class="lbh-preview-element" style="color: <?= $paramsData['primary_color'] ?>">${view['name']}</a> 
                    <button class="btn btn-default btn-sm tooltips" onclick="event_participation(this)"
                       data-toggle="tooltip" data-placement="left" data-original-title="${tip}"
                       data-ownerlink="${actionConnect}" data-id="${view._id.$id}" data-type="${view.collection}" data-name="${view.name}"
                       data-isFollowed="${isFollowed}"><i class="fa fa-${icon}"></i> ${btnLabel}</button><br>
                       ${organizers.length == 0 ? `Aucun organisateur<br>` : `Organisateur${organizers.length > 1 ? 's' : ''}:<br>${organizers.map(function(map){
                            return `<span style="padding-left: 20px;"> ${map['image'] ? ` * <img src="${baseUrl}/${map['image']}" height="30" width="30" class="" style="display: inline; vertical-align: middle; border-radius:100%;">` : ' * '} ${map['name']}</span><br>`;
                       }).join('')}`}
                       ${view['address'] ? `Lieu : ${view['address']}<br>` : ''}
                       ${typeof view['shortDescription'] !== 'undefined' ? view['shortDescription'] : ''}<br>
                    </li>`
            });
            var items = Array.from(document.querySelectorAll('#journee-caroussel #items .item'));
            items.map(function(item) {
                item.classList.remove('active');
            });

            if (items.length) {
                selected_date = subevents_view[0]['formattedDate'];
                if (element) {
                    element.classList.add('active');
                } else items.find(function(find) {
                    return find['dataset']['target'] === selected_date;
                }).classList.add('active');
            }
            html += '';
            container.html(html);
            coInterface.bindLBHLinks();
        }

        function loadProgrammeSlide() {
            const container = $('#carousel-programme');
            let html = '';
            subevents_view.slice().forEach((sub_event, index) => {
                if (index === 0) {
                    $('#carousel-programme-container').css({
                        backgroundImage: `url('${sub_event['profilRealBannerUrl']}')`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                    });
                }
                html += `<div class="slide" id="${sub_event['_id']['$id']}">
                            <div class="slide-element coevent-slider">
                        <p class="event-slide-title">${sub_event['name']}${typeof sub_event['shortDescription'] !== 'undefined' ? '<br><span class="event-slide-specification">' + sub_event['shortDescription'] + '</span>' : ''}</p>`
                if (typeof sub_event['description'] !== 'undefined') {
                    html += `<div class="event-slide-description" style="font-size: 13px;" id="description${sub_event['_id']['$id']}">`
                    if (sub_event['description'].length > 400) {
                        html += `${dataHelper.markdownToHtml(sub_event['description'].substring(0, 400) + ' <b>. . .</b>')}`;
                        html += `<a href="javascript:;" onclick="showMore('${sub_event['_id']['$id']}')" class="lbh-preview-element coevent-button inverted">VOIR PLUS</a>`;
                    } else html += dataHelper.markdownToHtml(sub_event['description']);
                    html += '</div>';
                }
                html += `
                        </div>
                    </div>
                    `;
            })
            container.html(html);
            // carouselProgramme = resetCarouselProgramme();
        }

        function showMore(id) {
            const event = sub_events.find(sub_event => sub_event['_id']['$id'] === id);
            const html = `
                ${dataHelper.markdownToHtml(event['description'])}
                <a href="javascript:;" onclick="showLess('${id}')" class="lbh-preview-element coevent-button inverted">VOIR MOINS</a>
            `;
            $(`#description${id}`).html(html);
        }

        function showLess(id) {
            const event = sub_events.find(sub_event => sub_event['_id']['$id'] === id);
            const html = `
                ${dataHelper.markdownToHtml(event['description'].substring(0, 400) + ' <b>. . .</b>')}
                <a href="javascript:;" onclick="showMore('${id}')" class="lbh-preview-element coevent-button inverted">VOIR PLUS</a>
            `;
            $(`#description${id}`).html(html);
        }

        function participate(user, event) {
            dataHelper.path2Value({
                id: event,
                collection: 'events',
                path: 'links.attendees.' + user,
                value: {
                    type: `<?= Person::COLLECTION ?>`
                }
            }, () => {
                toastr.success('Vous êtes désormais participant de cet évènement');
                const temp_event = sub_events.find(e => e['_id']['$id'] === event);
                temp_event['communities'].push(user);
                loadProgrammes(selected_date);
                build_actor_carousel();
            });
        }

        // let carouselProgramme = resetCarouselProgramme();
        // $('.navigation-caroussel.next').on('click', () => carouselProgramme.gotoNext());
        // $('.navigation-caroussel.prev').on('click', () => carouselProgramme.gotoPrevious());

        let journeeCarousel = resetJourneeCarousel();
        journeeCarousel.init();
        $('.journee-control.right').on('click', () => journeeCarousel.gotoNext());
        $('.journee-control.left').on('click', () => journeeCarousel.gotoPrevious());

        $('#second-search-bar').on('input', e => {
            const dropdown = $('#search-dropdown');
            const {
                value
            } = e.target;
            if (value.length > 0) dropdown.parents('.dropdown').addClass('open');
            else dropdown.parents('.dropdown').removeClass('open');
            let html = '<li role="presentation" class="dropdown-header">Résultat de la recherche</li>';
            const regex = new RegExp(value, 'ig');
            const filters = sub_events.filter(sub_event => sub_event['slug'].match(regex) || sub_event['name'].match(regex))
            filters.forEach(filter => {
                html += `<li><a href="#page.type.${filter.collection}.id.${filter._id.$id}" class="lbh-preview-element">${filter['name']}</a></li>`;
            });
            dropdown.html(filters.length === 0 ? '<li><a>Aucun élément ne corréspond à le recherche</a></li>' : html);
            coInterface.bindLBHLinks();
        });

        $('#second-search-bar').on('focus', e => {
            const dropdown = $('#search-dropdown');
            const {
                value
            } = e.target;
            if (value.length > 0) dropdown.parents('.dropdown').addClass('open');
            else dropdown.parents('.dropdown').removeClass('open');
            let html = '<li role="presentation" class="dropdown-header">Résultat de la recherche</li>';
            const regex = new RegExp(value, 'ig');
            const filters = sub_events.filter(sub_event => sub_event['slug'].match(regex) || sub_event['name'].match(regex))
            filters.forEach(filter => {
                html += `<li><a href="#page.type.${filter.collection}.id.${filter._id.$id}" class="lbh-preview-element">${filter['name']}</a></li>`;
            });
            dropdown.html(filters.length === 0 ? '<li><a>Aucun élément ne corréspond à le recherche</a></li>' : html);
            coInterface.bindLBHLinks();
        });

        $('#second-search-bar').on('blur', e => {
            const dropdown = $('#search-dropdown');
            $(document).off('click').on('click', e => {
                const parent = dropdown.parents('.dropdown');
                if (parent.has(e.target).length <= 0) parent.removeClass('open');
            });
        });

        function get_actors(types = ['organizer', 'links.attendees', 'creator', 'links.creator', 'links.organizer', 'organizerName']) {
            var url = `${baseUrl}/costum/coevent/get_events/request/actors/event/${costum['contextId']}`;
            var parameter = {
                types
            }
            return new Promise(function(resolve) {
                ajaxPost(null, url, parameter, function(events) {
                    resolve(events)
                }, null, 'json')
            });
        }

        function get_subevent_dates(type) {
            var url = type ? `${baseUrl}/costum/coevent/get_events/request/dates/event/${costum['contextId']}/filter/${type}` : `${baseUrl}/costum/coevent/get_events/request/dates/event/${costum['contextId']}`;
            return new Promise(function(resolve) {
                ajaxPost(null, url, null, function(events) {
                    resolve(events)
                }, null, 'json')
            });
        }

        function get_subevents(type) {
            var url = type ? `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}/filter/${type}` : `${baseUrl}/costum/coevent/get_events/request/subevents/event/${costum['contextId']}`;
            return new Promise(function(resolve) {
                ajaxPost(null, url, null, function(events) {
                    resolve(events)
                }, null, 'json')
            });
        }

        function get_categories() {
            var url = `${baseUrl}/costum/coevent/get_events/request/categories/event/${costum['contextId']}`;
            return new Promise(function(resolve) {
                ajaxPost(null, url, null, function(events) {
                    resolve(events)
                }, null, 'json')
            })
        }

        function construct_actor_html(index, db_actors) {
            var role_trad = {
                creator: 'Organisateur',
                attendee: 'Participant',
                speaker: 'Intervenant',
                organizer: `Organisateur d'événement`
            }

            var html = '<li class="slide"><div class="card-acteurs">';
            var key = 0;
            for (var actor_key in db_actors) {
                var actor = db_actors[actor_key];
                var {
                    roles
                } = actor;

                var description_li = [];
                var description_html = '';

                var current_role = '';
                var role_array = [];
                for (var role_key in roles) {
                    var role = roles[role_key];
                    if (!role_array.includes(role_trad[role['role']])) role_array.push(role_trad[role['role']]);
                }
                description_html += `<small><b>Statut : </b> ${role_array.map(function(map){return map;}).join(', ')}</small>`;

                var genre = trad[actor['type']];
                if (key % index === 0 && key !== 0) {
                    html += '</div></li><li class="slide"><div class="card-acteurs">';
                }
                var other_defaults = {
                    laSemaineDesTierslieux: `${baseUrl}/upload/communecter/organizations/5d85cd7240bb4eec7d496a75/Logo-RTL.gif`
                }

                var default_image = other_defaults[costum['slug']] || `${costum['assetsUrl']}/images/thumbnail-default.jpg`
                html += `
                    <div class="card-acteur">
                        <div class="card-header" style="background-image: url('${actor['image'] || default_image}');"></div>
                        <div class="card-content">
                            <!-- <span class="badget">${genre}</span> -->
                            <span class="acteur-nom">${actor['name']}</span>
                            <span class="acteur-description">${description_html}</span>
                        </div>
                    </div>
                `;
                key++;
            }
            html += '</div></li>';
            return html;
        }

        function build_actor_carousel() {
            var container = $('#acteur');
            get_actors(['organizer', 'links.orgnaizer', 'organizerName']).then(function(db_actors) {
                const windowSize = $(window);
                if (windowSize.width() <= 700) container.html(construct_actor_html(1, db_actors));
                else if (windowSize.width() > 700 && windowSize.width() <= 992) container.html(construct_actor_html(2, db_actors));
                else if (windowSize.width() > 992) container.html(construct_actor_html(3, db_actors));
                const carouselActeur = carousel({
                    container: container,
                    autoplay: false,
                    navigation: {
                        indicator: true,
                        button: {
                            show: true,
                            next: '<span class="fa fa-chevron-right"></span>',
                            prev: '<span class="fa fa-chevron-left"></span>'
                        }
                    },
                    delay: 2000
                });
                carouselActeur.init();
                carouselActeur.start();
                carouselActeur.onResize = function() {
                    if (windowSize.width() <= 700) container.html(construct_actor_html(1, db_actors));
                    else if (windowSize.width() > 700 && windowSize.width() <= 992) container.html(construct_actor_html(2, db_actors));
                    else if (windowSize.width() > 992) container.html(construct_actor_html(3, db_actors));
                    this.recreate(container);
                };
            });
        }

        function build_event_filter() {
            get_categories().then(function(db_categories) {
                [...document.querySelectorAll('.event-type-filter')].forEach(function(element) {
                    element.remove();
                });
                var container = document.querySelector('.class<?= $kunik ?>about-link-container');
                for (var category of db_categories) {
                    var filter = document.createElement('a');
                    filter.classList.add('event-type-filter');
                    filter.setAttribute('href', category);
                    filter.textContent = tradCategory[category];
                    filter.onclick = function(e) {
                        e.preventDefault();
                        filterEventPerType(this)
                    };
                    container.appendChild(filter);
                }
            });
        }

        async function load_fullcalendar(type = '') {
            var parent_event = await get_event_context();
            var start_moment_parent = moment(parent_event['start_date']);
            var start_moment_parent = moment(parent_event['start_date']);
            var end_moment_parent = moment(parent_event['end_date']);
            var default_view = '';
            switch (true) {
                case start_moment_parent.format('YYYY-MM-DD') == end_moment_parent.format('YYYY-MM-DD'):
                    default_view = 'agendaDay';
                    break;
                case end_moment_parent.diff(start_moment_parent, 'days') <= 6 && start_moment_parent.day() == 1:
                    default_view = 'agendaWeek';
                    break;
                default:
                    default_view = 'month';
                    break
            }

            get_subevents(type).then(function(db_events) {
                $('#event_calendar').fullCalendar('destroy');
                $('#event_calendar').fullCalendar({
                    lang: 'fr',
                    selectable: true,
                    timeFormat: 'H(:mm)',
                    defaultView: default_view,
                    header: {
                        left: 'title',
                        center: '',
                        right: 'prev,next'
                    },
                    events: db_events.map(function(event) {
                        return {
                            id: event['id'],
                            title: event['name'],
                            start: moment(event['start_date']).format('YYYY-MM-DD HH:mm'),
                            end: moment(event['end_date']).format('YYYY-MM-DD HH:mm'),
                            className: ['lbh-preview-element'],
                            url: `#page.type.events.id.${event['id']}`,
                            backgroundColor: `<?= $paramsData['primary_color'] ?> !important`
                        }
                    }),
                    select: function(start, end) {
                        // alert('selected');
                    },
                    eventMouseover: function() {
                        coInterface.bindLBHLinks();
                    }
                });
                $('#event_calendar').fullCalendar('gotoDate', start_moment_parent.format('YYYY-MM-DD HH:mm'));
            });
        }

        loadViews();
        build_actor_carousel();

        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
        jQuery(document).ready(function() {
            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema": {
                    "title": "<?php echo Yii::t('cms', 'Set up your section') ?>",
                    "description": "<?php echo Yii::t('cms', 'Customize your section') ?>",
                    "icon": "fa-cog",
                    "properties": {
                        primary_color: {
                            inputType: 'colorpicker',
                            label: 'Couleur primaire',
                            values: '<?= $paramsData['primary_color'] ?>'
                        }
                    },
                    beforeBuild: function() {
                        uploadObj.set("cms", "<?php echo $blockKey ?>");
                    },
                    save: function(data) {
                        tplCtx.value = {};
                        $.each(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties, function(k, val) {
                            tplCtx.value[k] = $("#" + k).val();
                            if (k == "parent")
                                tplCtx.value[k] = formData.parent;

                            if (k == "items")
                                tplCtx.value[k] = data.items;
                        });

                        if (typeof tplCtx.value == "undefined") toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value(tplCtx, function(params) {
                                dyFObj.commonAfterSave(params, function() {
                                    toastr.success("<?php echo Yii::t('cms', 'Element well added') ?>");
                                    $("#ajax-modal").modal('hide');
                                    urlCtrl.loadByHash(location.hash);
                                });
                            });
                        }

                    }
                }
            };
            load_timeline();
        });

        function get_event_context() {
            return new Promise(function(resolve) {
                ajaxPost(null, `${baseUrl}/costum/coevent/get_events/request/event/event/${costum['contextId']}`, null, function(event) {
                    resolve(event)
                }, null, 'json')
            })
        }

        function load_timeline(type = '') {
            var timeline_knightclub = null;
            var tl_interval = setInterval(function() {
                if (typeof TL !== 'undefined') {
                    clearInterval(tl_interval);
                    get_subevents(type).then(function(db_sub_events) {
                        var data = {
                            events: db_sub_events.map(function(event) {
                                var moment_start = moment(event['start_date']);
                                var moment_end = moment(event['end_date']);
                                var map = {
                                    start_date: {
                                        year: moment_start.year(),
                                        month: moment_start.month() + 1,
                                        day: moment_start.date(),
                                        hour: moment_start.hour(),
                                        minute: moment_start.minute()
                                    },
                                    end_date: {
                                        year: moment_end.year(),
                                        month: moment_end.month() + 1,
                                        day: moment_end.date(),
                                        hour: moment_end.hour(),
                                        minute: moment_end.minute()
                                    },
                                    text: {
                                        headline: event['name'],
                                        text: `<blockquote>${event['short_description']}</blockquote> ${dataHelper.markdownToHtml(event['full_description'])}`
                                    },
                                    media: {
                                        url: event['profile'],
                                        thumbnail: event['profile']
                                    },
                                    background: {}
                                };
                                if (event['banner'].length) map['background']['url'] = event['banner'];
                                else map['background']['color'] = `<?= $paramsData['primary_color'] ?>`;

                                return map;
                            })
                        };
                        var options = {
                            script_path: `${baseUrl}/plugins/knightlab_timeline3/js/`,
                            language: 'fr',
                            font: 'bevan-pontanosans',
                            theme: 'contrast',
                            scale_factor: 0.5,
                            timenav_position: 'top'
                        }

                        timeline_knightclub = new TL.Timeline('timeline-embed', data, options);
                    });
                }
            });
        }

        async function coevent_create_event($start = null, $end = null) {
            dyFObj.unloggedMode = true;

            var current_event = await get_event_context();

            var today = moment();
            var moment_start = $start != null ? (today.format('YYYY-MM-DD') === start_date.format('YYYY-MM-DD') ? today : $start) : moment(current_event['start_date']);
            var moment_end = $end != null ? $end.clone().subtract(60, 'minutes') : moment(current_event['end_date']);

            // post_save['links'] = {};
            // post_save['links']['attendees'] = {};
            // post_save['links']['attendees'][userConnected['_id']['$id']] = {
            //     type: 'citoyens',
            //     isAdmin: true
            // };
            var eventDynformDefaultParams = {
                beforeBuild: {
                    "properties": {
                        "type": {
                            "options": eventTypes
                        }
                    }
                },
                afterBuild: function() {
                    $(".parentfinder").hide();
                    $(".publiccheckboxSimple").hide();
                    //$("#recurrency").val(true);
                    $(".recurrencycheckbox").hide();
                    $(".organizerNametext").hide();
                    $(".finder-organizer").find("a").html("Rechercher et sélectionner ma structure");


                },
                afterSave: function(data) {
                    // var typeToSet = [];
                    // //alert(JSON.stringify(data))
                    // if(typeof post_save.links != "undefined" && typeof post_save.links.attendees[data.map.creator] != "undefined" ){
                    //     typeToSet.push({
                    //         path: `links.attendees.${data.map.creator}.isAdmin`,
                    //         type: 'boolean'
                    //     })
                    // }
                    var callbackParams = {};
                    callbackParams['links'] = {};
                    callbackParams['links']['attendees'] = {};
                    callbackParams['links']['attendees'][data['map']['creator']] = {
                        type: 'citoyens',
                        isAdmin: true
                    };

                    callbackParams['parent'] = {};
                    callbackParams['parent'][costum.contextId] = {
                        "type": costum.contextType,
                        "name": costum.title
                    };

                    const params = {
                        id: data['id'],
                        collection: data['map']['collection'],
                        path: 'allToRoot',
                        value: callbackParams
                        // setType: [{
                        //     path: `links.attendees.${userConnected['_id']['$id']}.isAdmin`,
                        //     type: 'boolean'
                        // }]
                    };


                    dataHelper.path2Value(params, (callback) => {
                        dyFObj.commonAfterSave(params, function() {
                            document.location.reload();
                        });
                    });
                }
            };



            var eventDynformParams = (costum != null && typeof costum.typeObj != "undefined" && typeof costum.typeObj.events != "undefined" && typeof costum.typeObj.events.dynFormCostum != "undefined") ? null : eventDynformDefaultParams;



            var presentValuesEvent = {
                startDate: moment_start.format('DD/MM/YYYY HH:mm'),
                endDate: moment_end.format('DD/MM/YYYY HH:mm')
            };

            // presentValuesEvent['parent'] = {};
            // presentValuesEvent['parent'][costum.contextId] = {
            //     "type": costum.contextType,
            //     "name": costum.title
            // };   



            dyFObj.openForm('event', null, presentValuesEvent, null, eventDynformParams);

            return false;
        }

        function event_participation(dom) {
            var that = dom;
            let eventData = JSON.parse(JSON.stringify(<?= json_encode($subEvents) ?>)).find(function(find) {
                return find['id'] == $(that).data("id")
            });
            thisElement = $(that);
            let isExpAttFull = (typeof eventData != "undefined" && typeof eventData.links.attendees != "undefined" && typeof eventData.expectedAttendees != "undefined" && Object.keys(eventData.links.attendees).length == eventData.expectedAttendees);

            if (isExpAttFull == false) {
                if (userId) {
                    subscribeToEvent(thisElement);
                } else {
                    $("#modalLogin").on('show.bs.modal', function(e) {
                        if ($("#infoBL").length == 0) {
                            $("#modalLogin .modal-content .container div.row").append(`<div class="col-xs-12" id="infoBL">
                                Vous devez être connecté(e) pour vous inscrire à l’événement. Vous n’avez pas encore de compte ? 
                                <a href="javascript:;" class="bold" data-toggle="modal" data-target="#modalRegister"> Créez-en un </a>
                            </div>`);
                        }
                    });
                    $("#modalLogin").on('hide.bs.modal', function(e) {
                        $("#infoBL").remove();
                    });
                    toastr.info("Vous devez être connecté(e) pour vous inscrire à l’événement");
                    Login.openLogin();
                }
            } else {
                if (userId) {
                    subscribeToEvent(thisElement);
                }
            }

            if (typeof eventData != "undefined" && isExpAttFull && thisElement.data("ownerlink") == "follow") {
                bootbox.dialog({
                    message: `<div class="alert-white text-center"><br>
                                <strong>Désolé ! Il n'y a plus de place. Le nombre maximal de participants a été atteint</strong>
                                <br><br>
                                <button class="btn btn-default bootbox-close-button">OK</button>
                            </div>`
                });
            }
        }

        function subscribeToEvent(eventSource, theUserId = null) {
            var labelLink = "";
            var parentId = eventSource.data("id");
            var parentType = eventSource.data("type");
            var childId = (theUserId) ? theUserId : userId;
            var childType = "citoyens";
            var name = eventSource.data("name");
            var id = eventSource.data("id");
            //traduction du type pour le floopDrawer
            eventSource.html("<i class='fa fa-spin fa-circle-o-notch text-azure'></i>");
            var connectType = (parentType == "events") ? "connect" : "follow";

            if (eventSource.data("ownerlink") == "follow") {
                callback = function() {
                    labelLink = (parentType == "events") ? "DÉJÀ PARTICIPANT(E)" : trad.alreadyFollow;
                    if (eventSource.hasClass("btn-add-to-directory"))
                        labelLink = "";
                    $(`[data-id=${id}]`).each(function() {
                        $(this).html("<small><i class='fa fa-unlink'></i> " + labelLink.toUpperCase() + "</small>");
                        $(this).data("ownerlink", "unfollow");
                        $(this).data("original-title", labelLink);
                    });
                }
                if (parentType == "events")
                    links.connectAjax(parentType, parentId, childId, childType, connectType, null, callback);
                else
                    links.follow(parentType, parentId, childId, childType, callback);
            } else {
                //eventSource.data("ownerlink")=="unfollow"
                connectType = (parentType == "events") ? "attendees" : "followers";
                callback = function() {
                    labelLink = (parentType == "events") ? "PARTICIPER" : "DÉJÀ PARTICIPANT(E)";
                    if (eventSource.hasClass("btn-add-to-directory"))
                        labelLink = "";
                    $(`[data-id=${id}]`).each(function() {
                        $(this).html("<small><i class='fa fa-chain'></i> " + labelLink.toUpperCase() + "</small>");
                        $(this).data("ownerlink", "follow");
                        $(this).data("original-title", labelLink);
                        $(this).removeClass("text-white");
                    });
                };
                links.disconnectAjax(parentType, parentId, childId, childType, connectType, null, callback);
            }
        }

        $(document).on("mouseup", "#annuaireButton<?= $kunik ?>", function() {
            $('.journee_views button')[0].click();
        });

        $(function() {
            build_event_filter();
            load_fullcalendar();
            get_event_context().then(function(event) {
                var start_moment_parent = moment(event['start_date']);
                var end_moment_parent = moment(event['end_date']);
                if (end_moment_parent.diff(start_moment_parent, 'days') > 6) change_views(document.querySelector('.journee_views button:nth-child(2)'), 1);
                else change_views(document.querySelector('.journee_views button:first-child'), 0);
            });
            $(document.body).on('co.delete.event', function() {
                var deleteId = document.location.hash.match(/preview=events\.([a-fA-F0-9]+)/)[1];
                var index = subevents_view.map(function(subevent) {
                    return subevent._id.$id;
                }).indexOf(deleteId);
                subevents_view.splice(index, 1);
                loadViews();
            });
        });
    </script>
<?php } else { ?>
    <div class="jumbotron mt-3">
        <h1>Oops, halte là!</h1>
        <p>Ce block est conçu uniquement pour les évènements</p>
    </div>
<?php } ?>