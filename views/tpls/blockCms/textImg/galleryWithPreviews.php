<?php
    $styleCss      = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];
    $otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $blockKey = $blockCms["_id"];

?>


<style type="text/css" id="galleryPrevious<?= $kunik ?>">

    .other-css-<?= $kunik ?> {  
        <?php echo $otherCss;?> 
    }
  
    @media (min-width : 768px) and (max-width: 991px){
        .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
    }

    @media screen and (max-width: 767px) {
        .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
    }

    .co-images {
        position: relative;
        overflow: hidden;
        height: 200px;
    }

    .co-images img {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    .co-images img:hover {
        opacity: 0.7;
    }


    .co-description {
        display: none;
    }

    .co-images img:hover~.co-description{
        position: absolute;
        bottom: 1px;
        color: #fff;
        background-color: rgba(0, 0, 0, 0.6);
        padding: 5px 10px;
        border-radius: 5px;
        font-size: 14px;
        display: block;
        width: 100%;
    }

   
    .co-modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
    }
    .co-d-none {
        display: none;
    }
    .co-modal-content { 
        animation-name: zoom;
        animation-duration: 0.6s;
    }
    .co-width-50 {
        width: 48%;
    }
    @keyframes zoom {
        from {transform:scale(0)} 
        to {transform:scale(1)}
    }
    

    .co-pos-relative {
        position: relative;
    }

    .co-pos-absolute {
        position: absolute;
    }

    @media only screen and (max-width: 700px){
        .co-modal-content {
            width: 100%;
        }
    }

    .co-d-block .inputs-container>div>div>div {
        display: block !important;
    }
  
</style>

<div class="<?= $kunik ?> <?= $kunik ?>-css  <?= $otherClass ?> other-css-<?= $kunik ?>" data-kunik="<?= $kunik ?>">
    
    <div class="container">
        <div class="row">            
            <?php foreach ($blockCms["content"] as $item): ?>
                <div class="col-md-6" style="margin-bottom: 10px">
                    <div class="co-pos-relative co-images">
                        <img class="costum-img-preview" src="<?= isset($item['keyImg']) ? $item['keyImg'] : "" ?>" alt="">
                        <span class="co-description">
                            <?= isset($item['keyDescr']) ? $item['keyDescr'] : "" ; ?>
                        </span>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>

    


<script>

    $(function(){
        if (costum.editMode){
            cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
            coGalleryWithPrevious = {
            configTabs : {
                general : {
                    inputsConfig : [
                        {
                            type: "inputMultiple",
                            options: {
                                label: "content",
                                class: "co-d-block",
                                name: "content",
                                inputs: [
                                    [
                                        {
                                            type: "inputSimple",
                                            options: {
                                                label: "keyDescr",
                                                name: "keyDescr",
                                            }
                                        },
                                        {
                                            type : "inputFileImage",
                                            options : {
                                                name : "keyImg",
                                                label : "keyImg",
                                                collection : "cms",
                                            }
                                        },
                                    ]
                                ]
                            }
                        },
                        
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
            },
            onChange : function(path,valueToSet,name,payload,value){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                if ( name === "content" ) { 
                    costumSocket.emitEvent(wsCO, "update_component", {
                                    functionName: "refreshBlock", 
                                    data:{ id: id}
                    })
                    cmsConstructor.helpers.refreshBlock(id, ".cmsbuilder-block[data-id='"+id+"']");
                }
            }
            
        }
        cmsConstructor.blocks.<?= $kunik ?> = coGalleryWithPrevious;
        }

        cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')

        str="";
        str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#galleryPrevious<?= $kunik ?>").append(str);
    });
  
</script>