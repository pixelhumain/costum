<!-- ****************get image uploaded************** -->
<?php 
    $initFiles = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'block',
        ), "image"
    );   
    $initLogoFiles = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'logo',
        ), "image"
    );
    $logoPicture = [];
    foreach ($initLogoFiles as $key => $value) {
        array_push($logoPicture, $value["imagePath"]);
    }
    $backgroundImages = [];
    foreach ($initFiles as $key => $value) {
        array_push($backgroundImages, $value["imagePath"]);
    }
?>

<?php
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

    $keyTpl ="carouselWithLogo";
    
    $paramsData = [ 
        "title"=>"Lorem Ipsum Dolor",
        "titleColor"=>"#ffffff",
        "imgHeight"=>"800",
        "logo" => null,
        "logoPosition" => "centre",
        "backgroundOpacity" => 0.3,
        "backgroundImages"=> null
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    } 

    $paramsData["logo"] = $logoPicture;
    $paramsData["backgroundImages"] = $backgroundImages;

    

  if($paramsData["logoPosition"]=="bottom-left"){
    $left = "10%";
    $top = "auto";
    $bottom = "10%";
  }else if($paramsData["logoPosition"]=="top-left"){
    $left = "10%";
    $top = "15%";
    $bottom="auto";
  }else{
    $left = "35%";
    $top = "30%";
    $bottom = "auto";
  }

?>

<style>
  .containerCarousel-<?= $kunik?>{
    position:relative;
    height: 70vh;
    overflow: hidden;
    padding: 0px;
  }
  .containerCarousel-<?= $kunik?>:before{
    content: '';
    position:absolute;
    background: #111111 !important;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    opacity: <?= $paramsData["backgroundOpacity"] ?>;

  }
  .carousel-control{
    z-index: 9
  }
  .containerCarousel-<?= $kunik?> img{
    width:100%;
    height: 100%;
  }
  .containerCarousel-<?= $kunik?> .content<?=$kunik ?>{
    position: absolute; 
    left:0;
    right: 0;
    top:45%;
    text-align:center; 
    transform:translateY(-50%);
    z-index: 2;
  }

  .logo<?=$kunik ?>{
      width: 32%;
      position: absolute;
      z-index: 9;
      left: <?=$left ?>;
      top: <?=$top ?>;
      bottom: <?=$bottom ?>;
      background: transparent;
  }

  .content<?=$kunik ?>{
      background: transparent;
  }

  #head<?=$kunik ?>{
    vertical-align: middle;
    text-align:center;
    font-weight: bold;
    color: <?= $paramsData["titleColor"] ?>;
    font-size: large;
    text-shadow: black 0.1em 0.1em 0.2em;
  }
   
</style>

<div class="containerCarousel containerCarousel-<?= $kunik?>">
    <div id="docCarousel<?=$kunik ?>" class="carousel slide" data-ride="carousel">
      <p id="head<?=$kunik ?>" class="col-xs-12  content<?=$kunik ?> sp-text" data-id="<?= $blockKey ?>" data-field="title">
           <?= "\n".$paramsData["title"] ?>
        </p>
        <div class="content<?=$kunik ?>">
            <?php if(!empty($paramsData["logo"])){ ?>
                <img class="logo<?=$kunik ?>" src="<?= $paramsData['logo']['0'] ?>">
            <?php } ?>
        </div>
        
        <div class="carousel-inner">
            <?php if(count($paramsData["backgroundImages"])==0){ ?>
                <div class="item active">
                    <img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'> 
                </div>
            <?php }else{ ?>
                <?php foreach ($paramsData["backgroundImages"] as $key => $value){ ?>
                    <div class='item <?= ($key==0)?"active":"" ?>'  style="text-align: center;">
                        <img src='<?php echo Yii::app()->createUrl("/").$value; ?>' style="width: 100%;object-fit: cover; height: <?= $paramsData['imgHeight']?>px">
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if(count($paramsData["backgroundImages"])>1){ ?>
            <a class="left carousel-control" href="#docCarousel<?=$kunik ?>" data-slide="prev">
                <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#docCarousel<?=$kunik ?>" data-slide="next">
                <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
   
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            "properties" : {
                "logo" :{                    
                   "inputType" : "uploader",
                   "docType": "image",
                   "contentKey":"slider",
                   "endPoint": "/subKey/logo",
                   "domElement" : "logo",
                   "filetypes": ["jpeg", "jpg", "gif", "png"],
                   "label": "<?php echo Yii::t('cms', 'Logo')?> :",
                   "itemLimit" : 1,
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initLogoFiles) ?> 
                },
                "logoPosition" : {
                    "class":"form-control",
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Position of the logo')?>",
                    "options":{
                        "top-left":"<?php echo Yii::t('cms', 'Top left')?>",
                        "center":"<?php echo Yii::t('cms', 'Center')?>",
                        "bottom-left":"<?php echo Yii::t('cms', 'Bottom left')?>"
                    },
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.logoPosition
                },
                "backgroundImages" :{
                   "inputType" : "uploader",
                   "label" : "<?php echo Yii::t('cms', 'Background image')?>",
                   "docType": "image",
                   "contentKey" : "slider",
                   "endPoint": "/subKey/block",
                   "domElement" : "image",
                   "filetypes": ["jpeg", "jpg", "gif", "png", "svg"],
                   "label": "Image de fond :",
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initFiles) ?> 
                },
                "backgroundOpacity":{
                  "inputType" : "range",
                  "min": 0,
                  "max": 1,
                  "label" : "<?php echo Yii::t('cms', 'Background opacity')?>",
                  "value" : sectionDyf.<?php echo $kunik ?>ParamsData.backgroundOpacity
                },
                "imgHeight" : {
                    "inputType" : "number",
                    "label" : "Hauteur de l'image (px)",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgHeight
                },
                "logo" :{                    
                   "inputType" : "uploader",
                   "label" : "<?php echo Yii::t('cms', 'Image')?>",
                   "docType": "image",
                   "contentKey" : "slider",
                   "endPoint": "/subKey/logo",
                   "domElement" : "logo",
                   "filetypes": ["jpeg", "jpg", "gif", "png"],
                   "label": "Logo :",
                   "itemLimit" : 1,
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initLogoFiles) ?>
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
          },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });

</script>