<?php
$keyTpl = "blockwithimg";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = $blockCms["_id"];

  $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
  $paramsData = [ 
    "title"=>"Art",
    "description" =>$loremIpsum,
    "imagePosition" => "left",
    "borderColor" => "#008037"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if ( !empty($blockCms[$e]) && isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<!-- ****************get image uploaded************** -->
<?php 
  $blockKey = (string)$blockCms["_id"];
  $initFiles = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>"notBackground"
    ), "image"
  );
$arrayImg = [];
foreach ($initFiles as $key => $value) {
  $arrayImg[]= $value["imagePath"];
}
 ?>
<!-- ****************end get image uploaded************** -->

<style>
  .container<?php echo $kunik ?>{
    /*box-shadow: 0 0px 20px rgba(0, 0, 0, 0.2);*/
    /*background-image: url("<?php  //echo Yii::app()->getModule('costum')->assetsUrl?>/images/eywa/background.png");
    text-align: left; 
    /*margin-top: 10px;*/
    /*margin-bottom: 40px;*/ 
    padding-bottom: 38px;
  }
</style>

<style>
.container<?php echo $kunik ?> .main-title{
  color: #2d2d2d;
  text-align: center;
  text-transform: capitalize;
  padding: 0.7em 0;
}



.container<?php echo $kunik ?> .content {
  position: relative;
  width: 90%;
  max-width: 700px;
  margin: auto;
  overflow: hidden;
  border-radius: 10px;
  margin-top: 38px;
}

.container<?php echo $kunik ?> .content .content-overlay {
  background: rgba(0,0,0,0.7);
  position: absolute;
  height: 99%;
  width: 100%;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  opacity: 0;
  -webkit-transition: all 0.4s ease-in-out 0s;
  -moz-transition: all 0.4s ease-in-out 0s;
  transition: all 0.4s ease-in-out 0s;
}

.container<?php echo $kunik ?>  .content:hover .content-overlay{
  opacity: 1;
}

.container<?php echo $kunik ?> .content-image{
  width: 100%;
}

.container<?php echo $kunik ?> .content-details {
  position: absolute;
  text-align: center;
  padding-left: 1em;
  padding-right: 1em;
  width: 100%;
  top: 50%;
  left: 50%;
  opacity: 0;
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
}

.container<?php echo $kunik ?> .content:hover .content-details{
  top: 50%;
  left: 50%;
  opacity: 1;
}

.container<?php echo $kunik ?> .content-details h3{
  color: #fff;
  font-weight: 500;
  letter-spacing: 0.15em;
  margin-bottom: 0.5em;
  text-transform: uppercase;
}

.container<?php echo $kunik ?> .content-details p{
  color: #fff;
  font-size: 0.8em;
}

.container<?php echo $kunik ?> .fadeIn-bottom{
  top: 80%;
}

.container<?php echo $kunik ?> .fadeIn-top{
  top: 20%;
}

.container<?php echo $kunik ?> .fadeIn-left{
  left: 20%;
}

.container<?php echo $kunik ?> .fadeIn-right{
  left: 80%;
}
.container<?php echo $kunik ?> .description{
  text-align: justify;
}
.container<?php echo $kunik ?> .description::first-letter{
  font-size: 30px;
  color: <?php echo $paramsData["borderColor"] ?>;
  text-transform: capitalize !important; 

}
.container<?php echo $kunik ?> div[class^="illustration"]{
  position: absolute;
  background-color: <?php echo $paramsData["borderColor"] ?>;
    -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
}

.container<?php echo $kunik ?> .illustration-left-1 {
    width: 5px;
    height: 80px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-left-2 {
    width: 50%;
    height: 5px;
    top: 0;
    left: 0;
}
.container<?php echo $kunik ?>:hover .illustration-left-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .illustration-right-1 {
    width: 5px;
    height: 80px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-1{
  -webkit-transition: all 1s ease 0s;
  -moz-transition: all 1s ease 0s;
  transition: all 1s ease 0s;
  height: 100%;
}

.container<?php echo $kunik ?> .illustration-right-2 {
    width: 50%;
    height: 5px;
    bottom: 0;
    right: 0;
}
.container<?php echo $kunik ?>:hover .illustration-right-2{
  -webkit-transition: all 1s ease-in-out 0s;
  -moz-transition: all 1s ease-in-out 0s;
  transition: all 1s ease-in-out 0s;
  width: 100%;
}


.container<?php echo $kunik ?> .btn-edit-delete{
  display: none;
}
.container<?php echo $kunik ?>:hover .btn-edit-delete{
  display: block;
  -webkit-transition: all 0.9s ease-in-out 9s;
  -moz-transition: all 0.9s ease-in-out 9s;
  transition: all 0.9s ease-in-out 0.9s;
  position: absolute;
  top:50%;
  left: 50%;
  transform: translate(-50%,-50%);
}
#filters-nav {
  display:none !important;
}
@media (max-width: 567px){
  .container<?php echo $kunik ?>,.content<?php echo $kunik ?>{
    padding-right: 0;
    padding-left: 0;
  }
}
@media (max-width: 992px){
  .container<?php echo $kunik ?> .title-xs{
    display: block
  }
  .container<?php echo $kunik ?> .title{
    display: none
  }
}

@media (min-width: 993px){
  .container<?php echo $kunik ?> .title-xs{
    display: none
  }
  .container<?php echo $kunik ?> .title{
    display: block
  }
}
</style>
<div class="container<?php echo $kunik ?> col-md-12 col-xs-12">
  <div class="illustration-left-1"></div>
  <div class="illustration-left-2"></div>
<h3 class="title-xs title-color ChangoRegular text-center"><?php echo $paramsData["title"] ?></h3>
    <?php if($paramsData["imagePosition"]=="right"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class="title title-color ChangoRegular text-center"><?php echo $paramsData["title"] ?></h3>
    <p class="description text-color SharpSansNo1Medium">
      <?php echo $paramsData["description"] ?>
    </p>
  </div>
  <?php } ?>

  <div class="content<?php echo $kunik ?> col-md-6 col-xs-12">
    <div class="content">
      <a href="javascript:;" target="_blank">
        <!-- <div class="content-overlay"></div> -->
        <?php if (count($arrayImg)!=0) {?>
            <img class="content-image" src="<?php echo $arrayImg[0] ?>" alt=""> 
        <?php }else{
          echo '<img class="content-image" src="'.Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg">';
        } ?>
        
        <!-- <div class="content-details fadeIn-top">
          <h3>This is a title</h3>
          <p>This is a short description</p>
        </div> -->
      </a>
    </div>
  </div>

  <?php if($paramsData["imagePosition"]=="left"){ ?>
  <div class="col-md-6 col-xs-12">
    <h3 class="title title-color ChangoRegular text-center"><?php echo $paramsData["title"] ?></h3>
    <p class="description text-color SharpSansNo1Medium">
      <?php echo $paramsData["description"] ?>
    </p>
  </div>
  <?php } ?>

  <div class="illustration-right-1"></div>
  <div class="illustration-right-2"></div>

</div>
 <br>
  <?php 
//          echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "kunik"=>$kunik, "page" => @$page,"id" => (string)$blockCms["_id"]]);  
    ?>
   <br>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre block",
            "description" : "Personnaliser votre block",
            "icon" : "fa-cog",
            
            "properties" : {
            "title" : {
                "inputType" : "text",
                "label" : "Titre",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
            },
            "description" : {
                "inputType" : "textarea",
                "markdown" : true,
                "label" : "Description",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.description
            },
            "image" :{
              "inputType" : "uploader",
                "label" : "image",
                "docType" : "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "endPoint" :"/subKey/notBackground",
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                initList : <?php echo json_encode($initFiles) ?>
            },
            "imagePosition" :{
              "inputType" : "select",
                "label" : "Aligner l'image",
                "options":{
                  "left":"à gauche",
                  "right":"à droite"
                }
            },
            "borderColor" : {
                "inputType" : "colorpicker",
                "label" : "Couleur de la bordure",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.borderColor
            }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                     toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
                  location.reload();
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>