<?php

    $assetsUrl = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl();
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $otherClass     = $blockCms["advanced"]["otherClass"] ??  "";
    $otherCss       = $blockCms["advanced"]["otherCss"] ??  "";
    $otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
    $otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
    $costumLanguage = isset($costum['langCostumActive']) ? $costum['langCostumActive'] : '';
    $link = isset($blockCms["link"]) ? $blockCms["link"] : [];
    $linkToShow = "";
    if (array_key_exists($costumLanguage, $link)) {
        $linkToShow = $link[$costumLanguage];
    } else {
        $linkToShow = reset($link);
    }
?>

<style id="imac<?= $kunik ?>" type="text/css">

.other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
@media (min-width : 768px) and (max-width: 991px){
.other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
}

@media screen and (max-width: 767px) {
.other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
}

.imac-wrapper<?= $blockKey ?> {
    max-width: 92%;
    margin: 0 auto;
    padding-bottom: 3%;
}

.imac-wrapper<?= $blockKey ?> .imac {
    padding: 3%;
    margin: 0 auto;
    max-width: 90%;
    margin-bottom: 8%;
    position: relative;
    border-radius: 10px;
    background: #303030;
    background: linear-gradient(to bottom left, #303030, #303030 35%, #1d1d1d 35.1%, #1d1d1d);
}

.imac-wrapper<?= $blockKey ?> .imac .imac-inner {
    position: relative;
    padding-bottom: 60%;
}

.imac-wrapper<?= $blockKey ?> .imac .imac-inner:before {
    content: ' ';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 30, 60, 0.15);
}

.imac-wrapper<?= $blockKey ?> .imac .imac-inner iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
    border-radius: 5px;
}

.imac-wrapper<?= $blockKey ?> .imac .imac-inner .imac-stand {
    position: absolute;
    top: 105%;
    width: 22%;
    left: 39%;
    height: 13%;
    background: #ecf2f4;
    background: linear-gradient(to bottom, #97a0ae, #c9cfda 45%, #ecf2f4 70%, #97a0ae);
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    -webkit-transform: perspective(300px) rotateX(20deg);
    transform: perspective(300px) rotateX(20deg);
    box-shadow: 0 25px 30px rgba(0, 0, 0, 0.15);
}

.imac-wrapper<?= $blockKey ?> .imac .imac-inner .imac-stand:after {
    content: ' ';
    display: block;
    position: absolute;
    top: 49%;
    width: 100%;
    height: 100%;
    background: #ecf2f4;
    background: linear-gradient(to bottom, #97a0ae, #97a0ae 75%, #808894);
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    border-radius: 0 0 10% 10%;
    -webkit-transform: perspective(150px) rotateX(50deg);
    transform: perspective(150px) rotateX(50deg);
    box-shadow: inset 0 0 3px #76797a, 0 2px #ecf2f4;
}

</style>

<div class="imac-wrapper<?= $blockKey ?> textTypping<?= $blockKey ?> <?= $kunik ?> other-css-<?= $kunik ?> <?= $otherClass ?>">
        <div class="imac">
            <div class="imac-inner">
                <!-- <img class="this-content-<?= $kunik ?>" src=""> -->
                <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="<?= $linkToShow ?>?title=0&warningTitle=0&controls=0&peertubeLink=0&loop=1&autoplay=1&muted=1" frameborder="0" allowfullscreen></iframe>
                <div class="imac-stand"></div>
            </div>
        </div>
</div>

<script type="text/javascript">

    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#imac<?= $kunik ?>").append(str);

    if (costum.editMode){     
        cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>;
        imac = {
            configTabs : {
                general : {
                    inputsConfig : [
                        // {
                        //     type : "inputFileImage",
                        //     options : {
                        //         name : "image",
                        //         label : tradCms.uploadeImage,
                        //         collection : "cms",
                        //         translate : {auto: false}
                        //     }
                        // },
                        {
                            type : "inputSimple",
                            options : {
                                type : "text",
                                name : "link",
                                label : tradCms.link,
                                translate : {auto: false},
                            },
                        },
                    ]
                },
                style: {
                    inputsConfig : [
                        "addCommonConfig"
                    ]
                },
            },
            onChange : function(path,valueToSet,name,payload,value){
                
            },
            afterSave: function(path,valueToSet,name,payload,value,id){
                if ( name === "link" ) { 
                    costumSocket.emitEvent(wsCO, "update_component", {
                                    functionName: "refreshBlock", 
                                    data:{ id: id}
                    })
                    cmsConstructor.helpers.refreshBlock(id, ".cmsbuilder-block[data-id='"+id+"']");
                }
            }
            
        }
        cmsConstructor.blocks.imac<?= $blockKey ?> = imac;
        
    } 
</script>