<?php 
	$keyTpl = "textCollapseonHover";
	$objectCss = $blockCms["css"] ?? [];
	$styleCss  = (object) [$kunik => $objectCss]; 

	$defaultTexts = [
		"titleWithColor1" => "Titre", 
		"content" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
		];
	
	$paramsData = $blockCms;

	foreach ($defaultTexts as $key => $defaultValue) {
		${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
		${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];

		$paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
	};

?>
<style type="text/css">
.<?= $kunik?> {
	background: transparent !important;
}
.<?= $kunik?> .panel-body{
}
.title<?= $kunik?> {
	padding-top: 60px;
}
.content-<?= $kunik?> {
	margin-top: -10px
}
.<?= $kunik?> .scroll {
	animation: down 1.5s infinite;
	-webkit-animation: down 1.5s infinite;
}
.<?= $kunik?> .scroll i {
	font-size: <?= $blockCms["css"]["iconCss"]["fontSize"]; ?>;
	color: <?= $blockCms["css"]["iconCss"]["color"];?>;
}

@keyframes down {
	0% {
		transform: translate(0);
	}
	20% {
		transform: translateY(15px);
	}
	40% {
		transform: translate(0);
	}
}
@media (max-width: 978px) {
	.<?= $kunik?> .title<?= $kunik?> h2 .title-1 {
		font-size: 40px !important;
	}

	.<?= $kunik?> .scroll i {
		font-size: 100px;
	}
	.<?= $kunik?> #collapseOne .content-<?= $kunik?> .title-4 p{
		font-size: 17px !important;
	}
}


/*.block-container-<?=$kunik?> {
	background-attachment: fixed !important;
}*/
.<?= $kunik?> #collapseOne .content-<?= $kunik?> .panel-body p{
	margin: -3px !important;
}

.collapse-<?= $kunik?> {
    min-height: 245px;
}
</style>
<div class="panel <?= $kunik?> ">
	<div class="wholeHead-<?= $kunik?> ">
		<div class="title<?= $kunik?>">
			<div id="titleWithColor1<?= $blockKey ?>" class="text-center fadeInLeft animated sp-text titleWithColor1<?= $blockKey ?>" data-id="<?= $blockKey ?>"  data-field="titleWithColor1">
			</div>
		</div>
		<div class="panel-heading no-padding" role="tab" id="headingOne">
			<div class="scroll iconCss">
				<h4 class="panel-title text-center">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor:pointer;">
						<i class="fa fa-angle-down "></i> 
					</a>
				</h4>
			</div>
		</div>
	</div>
	<div id="collapseOne" class="panel-collapse collapse-<?= $kunik?>" role="tabpanel" aria-labelledby="headingOne">
		<div class="content-<?= $kunik?> container">
			<div  id="content<?= $blockKey ?>" class="panel-body sp-text content<?= $blockKey ?>" data-id="<?= $blockKey ?>"  data-field="content" style="line-height: 1.2;"></div>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>blockCms = <?php echo json_encode( $paramsData ); ?>;

	var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?php echo json_encode( $blockCms ); ?>;
        var textCollapseonHoverInput = {
            configTabs : {
                style : {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "iconCss",
                                label : tradCms.iconStyle,
                                inputs : [
                                    "color",
									"fontSize"
                                ]
                            }
                        }
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.textCollapseonHover<?= $blockKey ?> = textCollapseonHoverInput;
    }

	appendTextLangBased(".titleWithColor1<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleWithColor1) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".content<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($content) ?>,"<?= $blockKey ?>");


	$(".collapse-<?= $kunik?>").hide();
	$(".scroll").mouseenter(function() {
		$(".collapse-<?= $kunik?>").show();
		$(".wholeHead-<?= $kunik?> ").hide();
	}
	);

	$(".panel-body ").mouseleave(function() {
		$(".wholeHead-<?= $kunik?> ").show();
		$(".collapse-<?= $kunik?>").hide();
	}
	);

	</script>