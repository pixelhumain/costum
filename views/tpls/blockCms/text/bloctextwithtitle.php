<?php 
$keyTpl = "bloctextwithtitle";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = $blockCms["_id"];
$paramsData = [
	"title"			=> "Cet accompagnement vous permettra de :",
	"titleAlign"	=> "left",
	"titleSize"    	=> "22",
	"titleColor"   	=> "#ffffff",
	"textAlign"		=> "left",
	"textSize"	  	=> "18",
	"textColor"	  	=> "#ffffff",
	"bg_color"		=> "#44536a",
	"text"			=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut"
];

if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?> .btn-edit-delete .btn{
    box-shadow: 0px 0px 20px 3px #ffffff;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:20%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .text<?php echo $kunik ?>{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>; 
  	text-align: <?= $paramsData['textAlign'] ?>;
  }
  .text<?php echo $kunik ?> p{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
  .text<?php echo $kunik ?> li{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
  .text<?php echo $kunik ?> p, .text<?php echo $kunik ?> li{
  	font-size: <?php echo $paramsData["textSize"]; ?>px;
  	color: <?php echo $paramsData["textColor"]; ?>;
  	text-align: <?= $paramsData['textAlign'] ?> 
  }
</style>
<div class="container<?php echo $kunik ?>" style=" background-color: <?= $paramsData['bg_color'] ?>;"> 
	<div class="container">
		<div style="text-align: <?= $paramsData['titleAlign'] ?>;padding-top: 20px;">
		<div class="bold title sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title" style="font-size: <?= $paramsData['titleSize'] ?>px;color: <?= $paramsData['titleColor'] ?>;padding-left: 55px"><div><?= $paramsData['title'] ?></div></div>
		</div>
		<div style="padding-bottom: 50px;padding-top: 50px;padding-left: 10% ;padding-right: 10%;">
			<div class="description text<?php echo $kunik ?> sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="text"><?= $paramsData['text'] ?></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

	jQuery(document).ready(function() {

		sectionDyf.<?php echo $kunik ?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {
					bg_color : {
						label : "<?php echo Yii::t('cms', 'Background color')?>",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.bg_color
					}
				},
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
	                  dataHelper.path2Value( tplCtx, function(params) {
	                    dyFObj.commonAfterSave(params,function(){
	                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
	                      $("#ajax-modal").modal('hide');
						  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
						  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
						  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
						  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
	                    //   urlCtrl.loadByHash(location.hash);
	                    });
	                  } );
					}
				}
			}
		};

		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";

			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
		});
	});
</script>