<?php 
$cssJS = array(
	'/plugins/jQuery-Knob/js/jquery.knob.js',
	'/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
	'/plugins/showdown/showdown.min.js',
	'/plugins/to-markdown/to-markdown.js',
	"/plugins/jquery-counterUp/waypoints.min.js",
	"/plugins/jquery-counterUp/jquery.counterup.min.js",
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
?>
<?php 
$keyTpl = "blockWithCounter";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
	"texte1" => "CTE Lauréats",
	"texte2" =>"CTE signés",
	"texte3" => "millions €",
	"texte4" => "EPCI",
	"texte5" => "Francais concernés",
	"texte6" => "Actions"

];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style type="text/css">
	.<?= $kunik?> .counterBlock{
		border: 2px solid white; 
		background-color: white; 
		color:#24284D; 
		border-radius: 20px;
		height:200px;
	}
	.<?= $kunik?> .counterBlock i{
		font-size:18em;
		color:#134292
	}
	.editPhoto<?= $kunik?> i{
		font-size: 15px !important;
		color :white !important ;
		width: 35px;
		height: 35px;
		line-height: 35px;
		border-radius: 50%;
		color: white;
		font-size: 18px;
		background: #000091;
		border: 1px solid #000091;
		margin-right: 10px;
		transition: all .5s ease 0s;
	}
	.editPhoto<?= $kunik?> :hover{
        transform:rotate(360deg)
    }
	@media (max-width: 767px) {
		.<?= $kunik?> img{
			height: 100px;
			display: initial;
		}
		.<?= $kunik?> .counterBlock i{
			font-size: 8em !important;
		}
	}
</style>
<?php 
$financeTotal=Yii::$app->cache->get('ctenatfinanceTotal');
$actionCount=Yii::$app->cache->get('ctenatactionCount');
$epci=Yii::$app->cache->get('ctenatepci');
$population=Yii::$app->cache->get('ctenatpopulation');
$cters=Yii::$app->cache->get('ctenatcters');
$ctelaureat=Yii::$app->cache->get('ctenatctelaureat');
if($financeTotal===false || $actionCount===false || $population===false || $epci===false || $cters===false || $ctelaureat===false)
{

	$ctelaureat=PHDB::count(Project::COLLECTION, array("category" => Ctenat::CATEGORY_CTER,"source.status.ctenat"=>Ctenat::STATUT_CTER_LAUREAT));
	$cters = PHDB::find(Project::COLLECTION, ["category" => Ctenat::CATEGORY_CTER,
		"source.status.ctenat"=>Ctenat::STATUT_CTER_SIGNE ],["nbHabitant","name", "slug", "scope"]);
	$cters=count($cters);

	$answers = PHDB::find( Form::ANSWER_COLLECTION, array("source.key"=>"ctenat",
		"priorisation" => ['$in'=> Ctenat::$validActionStates ]), ["answers.caracter.actionPrincipal","answers.action.project","answers.murir.planFinancement","cterSlug"] );
	$ssBadgeFamily = [];
	foreach ($costum["lists"]["domainAction"] as $pb => $bChild) {
		foreach ($bChild as $key => $bName) {
			$ssBadgeFamily[$bName] = $pb;
		}
	}
	$financeTotal = 0;
	$actionCount = 0;


	$finance = [];
	$financeLbl = [];
	$res = ["data"=>[],"lbls"=>[],"total"=>0];
	$parentForm = PHDB::findOne(Form::COLLECTION,["id"=>Ctenat::PARENT_FORM ], ["_id", "id","params.period"] );


	foreach ( $answers as $id => $ans ) {

		$daPrinci = @$ans["answers"]["caracter"]["actionPrincipal"];
		if( isset( $ssBadgeFamily[$daPrinci] )
			&& isset($ans["answers"]["action"]["project"][0]["id"]) ) 
			$actionCount++;
	} 

	foreach ($answers  as $i => $a) {
		if(isset($a["answers"]["murir"]["planFinancement"]))
		{
			$fin = $a["answers"]["murir"]["planFinancement"];

			foreach ($fin as $ix => $f) {

				$cumul = 0;
				if(isset($parentForm["params"]["period"])){

					$from = intval($parentForm["params"]["period"]["from"])+1;
					$to = intval($parentForm["params"]["period"]["to"]);
					while ( $from <= $to) {
						if(!empty($f["amount".$from]))
							$cumul += intval( $f["amount".$from] );
						$from++;
					}
				}	
				if(isset($f["financerType"])){
					if(!isset($finance[$f["financerType"]])){
						$finance[ $f["financerType"] ] = $cumul;
						$financeLbl[ $f["financerType"] ] = (isset(Ctenat::$financerTypeList[$f["financerType"]])) ? Ctenat::$financerTypeList[$f["financerType"]] : $f["financerType"] ;
					}
					else 
						$finance[$f["financerType"]] += $cumul;
				}
			}
		}

		foreach (array_keys($finance) as $k => $v) {
			$financeTotal += intval($finance[$v]);
		}
		$finance = [];
		$financeLbl = [];
	}

	$financeTotal= round($financeTotal/1000000,1);

	$idScope =  array();
	$allcter = PHDB::find(Project::COLLECTION, 
		[ 
			"category" => Ctenat::CATEGORY_CTER,
			"source.status.ctenat"=> ['$in'=>Ctenat::$validCter ] 
		],
		["nbHabitant","name", "slug", "scope"]);
	foreach ($allcter as $keyCTER => $cter) {
		if(!empty($cter["scope"])){
			foreach ($cter["scope"] as $key => $val) {
				if(!empty($val["id"]) && !in_array($val["id"], $idScope))
					$idScope[] = new MongoId($val["id"]) ;;
			}
		}
	}

       $epci = 178;//PHDB::count(Zone::COLLECTION, array(
                                            // "_id" => array('$in' => $idScope), 
                                            // "epci" => array('$exists' => 1)), array("name"));
            //population
       $population = 0;
       foreach ($allcter as $id => $cter) {
       	if(isset($cter["nbHabitant"]))
       		$population += intval($cter["nbHabitant"]);
       }
       $population=number_format($population, 0, ',', '');

       Yii::$app->cache->set('ctenatctelaureat',$ctelaureat, 720);
       Yii::$app->cache->set('ctenatcters',$cters, 720);
       Yii::$app->cache->set('ctenatepci',$epci, 720);
       Yii::$app->cache->set('ctenatpopulation',$population, 720);
       Yii::$app->cache->set('ctenatfinanceTotal',$financeTotal, 720);
       Yii::$app->cache->set('ctenatactionCount',$actionCount, 720);
}

?>

<?php 
   $baseUrl = Yii::app()->getRequest()->getBaseUrl(true);
   if (isset($blockCms["photo"])) {
   	foreach ($blockCms["photo"] as $key => $value) {
   		//var_dump($key);
   		${$key} = Document::getListDocumentsWhere(
   			array(
   				"id"=> $blockKey,
   				"type"=>'cms',
   				"subKey"=>(String)$key
   			), "image"
   		);
   		$images = ${$key}[0] ;
         if(!empty($images["imageThumbPath"]) && Document::urlExists($baseUrl.$images["imageThumbPath"]))
            ${'array' . $key} = $baseUrl.$images["imageThumbPath"];
         elseif (!empty($images["imagePath"]) && Document::urlExists($baseUrl.$images["imagePath"]))
            ${'array' . $key} = $baseUrl.$images["imagePath"];
         else 
            ${'array' . $key} = $baseUrl.$images["imageMediumPath"];
      }
   }
?>

   <div class="<?= $kunik?>">       
   	<div class="col-xs-12 text-explain  bg-white" style="padding-bottom: 0px;" >
   		<div class="col-md-4  col-sm-4 col-xs-12  text-center text-white border- padding-10"  >
   			<div class="counterBlock"  >
   				<h1 >
   					<span class="counter"><?php echo $ctelaureat ?></span>
   				</h1>
   				<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte1"> <?= $paramsData["texte1"]?></h3>
   				<div class="col-md-4 col-md-offset-4">

   					<img  class="logo<?= $kunik ?> img-responsive" src="<?php echo !empty($arrayphoto1) ? $arrayphoto1 :  Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/PictoCTE.png"; ?> ">



   					<a href="javascript:;" class="editPhoto<?= $kunik?>" data-path="photo1" data-photo = '<?= isset($photo1)?json_encode($photo1):""?>' title="Modifier la photo"> <i class="fa fa-edit" ></i> </a>
   				</div>
   			</div>
   		</div> 

   		<div class="col-md-4  col-sm-4 col-xs-12   text-center  padding-10"  >
   			<div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
   				<h1 class="title-1"><span class="counter"><?php echo $cters ?></span></h1>
   				<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte2"><?= $paramsData["texte2"]?> </h3>
   				<div class="col-md-4 col-md-offset-4">
   					<img  class="logo<?= $kunik ?> img-responsive" src="<?php echo !empty($arrayphoto2) ? $arrayphoto2 :  Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/PictoCTE.png"; ?> ">
   					<a href="javascript:;" class="editPhoto<?= $kunik?>" data-path="photo2" data-photo = '<?= isset($photo2)?json_encode($photo2):""?>' title="Modifier la photo" ><i class="fa fa-edit" ></i> </a>
   				</div>
   			</div>
   		</div>


   		<div class="col-md-4  col-sm-4 col-xs-12  text-center  padding-10"  >
   			<h1 class="title-1">
   				<span class="counter"><?php echo $financeTotal; ?></span>
   			</h1>
   			<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte3"><?= $paramsData["texte3"]?> </h3>
   			<div class="counterBlock" id="millionPie" >

   			</div>
   		</div>


   	</div>

   	<div class="col-xs-12 " style="background-color:#000091;height:40px;"> </div>

   	<div class="col-xs-12 text-explain bg-white" style="margin-bottom:40px; padding-bottom: 20px;" >
   		<div class="col-md-4  col-sm-4 col-xs-12  text-center  padding-10"  >
   			<div class="counterBlock"  >
   				<h1 class="title-1">
   					<span class="counter"><?php echo $epci; ?></span>
   				</h1>
   				<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte4"> <?= $paramsData["texte4"]?></h3>
   				<div class="col-md-6 col-md-offset-3">
   					<img  class="logo<?= $kunik ?> img-responsive" src="<?php echo !empty($arrayphoto3) ? $arrayphoto3 :  Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/PictoEPCI.jpg"; ?> ">
   					<a href="javascript:;" class="editPhoto<?= $kunik?>" data-path="photo3" data-photo = '<?= isset($photo3)?json_encode($photo3):""?>' title="Modifier la photo" ><i class="fa fa-edit" ></i> </a>
   				</div>
   			</div>
   		</div> 

   		<div class="col-md-4  col-sm-4 col-xs-12  text-center  padding-10"  >
   			<div class="counterBlock" style="border-left:1px dashed #6CC3AC;border-right:1px dashed #6CC3AC;" >
   				<h1 class="title-1">
   					<span class="counter"><?php echo $population ?>
   					</span>
   				</h1>
   				<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte5"> <?= $paramsData["texte5"]?> </h3>
   				<div class="col-md-6 col-md-offset-3">
   					<img  class="logo<?= $kunik ?> img-responsive" src="<?php echo !empty($arrayphoto4) ? $arrayphoto4 :  Yii::app()->getModule('costum')->assetsUrl."/images/ctenat/Francais-Acteurs.jpg"; ?> ">
   					<a href="javascript:;" class="editPhoto<?= $kunik?>" data-path="photo4" data-photo = '<?= isset($photo4)?json_encode($photo4):""?>' title="Modifier la photo"><i class="fa fa-edit" ></i> </a>
   					
   				</div>
   			</div>
   		</div>

   		<div class="col-md-4  col-sm-4 col-xs-12  text-center  padding-10"  >
   			<div class="counterBlock"  >
   				<h1 class="title-1">
   					<span class="counter"><?php echo $actionCount ?>
   					</span>
   				</h1>
   				<h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="texte6"> <?= $paramsData["texte6"]?> </h3>
   				<div class="col-md-6 col-md-offset-3">
   					<i class="fa fa-lightbulb-o" style=""></i>
   				</div>
   			</div>
   		</div>
   	</div>
   </div> 

   <script type="text/javascript">
   	sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
   	jQuery(document).ready(function() {
   		$(".editPhoto<?= $kunik?>").off().on("click",function(){
   			tplCtx.id = "<?= $blockKey ?>";
   			tplCtx.collection = "cms";
   			tplCtx.path = "photo."+$(this).data("path");
   			var activeForm = {
   				"jsonSchema" : {
   					"title" : "<?php echo Yii::t('cms', 'Edit photo')?>",
   					"type" : "object",
   					onLoads : {
   						onload : function(data){
   							$(".parentfinder").css("display","none");
   						}
   					},
   					"properties" : {
   						"image" :{
   							"inputType" : "uploader",
   							"label" : "<?php echo Yii::t('cms', 'Image')?>",
   							"docType": "image",
   							"contentKey" : "slider",
   							"itemLimit" : 1,
   							"endPoint": "/subKey/"+$(this).data("path"),
   							"domElement" : "image",
   							"filetypes": ["jpeg", "jpg", "gif", "png"],
   							"label": "Image :",
   							"showUploadBtn": false,
   							initList : $(this).data("photo")
   						},
   					},
   					beforeBuild : function(){
   						uploadObj.set("cms","<?= $blockCms['_id'] ?>");
   					},
   					save : function (data) {  
   						tplCtx.value = {};
   						$.each( activeForm.jsonSchema.properties , function(k,val) { 
   							tplCtx.value[k] = $("#"+k).val();
   						});
   						mylog.log("save tplCtx",tplCtx);

   						if(typeof tplCtx.value == "undefined")
   							toastr.error('value cannot be empty!');
   						else {
   							dataHelper.path2Value( tplCtx, function(params) {
   								dyFObj.commonAfterSave(params,function(){
   									toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
   									$("#ajax-modal").modal('hide');
									var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
									var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
									var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
									cmsBuilder.block.loadIntoPage(id, page, path, kunik);
   									// urlCtrl.loadByHash(location.hash);
   								});
   							} );
   						}

   					}
   				}
   			}; 
   			dyFObj.openForm( activeForm );
   		});
   		$(".<?= $kunik?> .editable").blur(function(){
   			var textData = {
   				collection : "cms",
   				id : "<?= $blockKey?>",
   				path : $(this).data("path"),
   				value : $(this).text()
   			};
   			dataHelper.path2Value( textData , function(params) {		    

   			} );
   		});
   		sectionDyf.<?php echo $kunik ?>Params = {
   			"jsonSchema" : {
          		"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		     	"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
   				"icon" : "fa-cog",
   				"properties" : {  
   				},
   				beforeBuild : function(){
   					uploadObj.set("cms","<?php echo $blockKey ?>");
   				},
   				save : function (data) {  
   					tplCtx.value = {};
   					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
   						tplCtx.value[k] = $("#"+k).val();
   					});
   					mylog.log("save tplCtx",tplCtx);
   					if(typeof tplCtx.value == "undefined")
   						toastr.error('value cannot be empty!');
   					else {
   						dataHelper.path2Value( tplCtx, function(params) {
   							dyFObj.commonAfterSave(params,function(){
   								toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
   								$("#ajax-modal").modal('hide');
								var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
								var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
								var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
								cmsBuilder.block.loadIntoPage(id, page, path, kunik);
   								// urlCtrl.loadByHash(location.hash);
   							});
   						} );
   					}
   				}
   			}
   		};
   		$(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
   			tplCtx.id = $(this).data("id");
   			tplCtx.collection = $(this).data("collection");
   			tplCtx.path = "allToRoot";
   			dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
   		});
   		$('.counter').counterUp({
   			delay: 10,
   			time: 2000
   		});
   		$('.counter').addClass('animated fadeInDownBig');
   		$('h3').addClass('animated fadeIn');

   		ajaxPost('#millionPie', baseUrl+'/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMillion/size/S', 
   			null,
   			function(){  },"html");
   	})

   </script>