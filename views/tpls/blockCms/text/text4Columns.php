<?php 
$keyTpl = "text4Columns";
$paramsData = [
  "title" => "\n        \n        <span style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\" style=\"\"><u>\n        Le Processus      </u></font></span><div><u style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\"><br></font></u></div><div><u style=\"\n    width: 20px !important;\n\"><font size=\"6\" color=\"#63aabc\"><br></font></u></div>            ",
    
  "title1"=>"<font color=\"#63aabc\" size=\"5\">Connecter</font>            ",
  "content1" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.</font>",
  "content2" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula.&nbsp;<span style=\"background-color: transparent;\">Ut et purus lorem.</span></font>",
  "content3" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.  </font>",
  "content4" => "<font face=\"SharpSans-No1-Medium\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin mattis est sed auctor vehicula. Ut et purus lorem.  </font>",
  "title2" => "<font color=\"#63aabc\" size=\"5\">Observer</font>            ",
  "title3" => "<font color=\"#63aabc\" size=\"5\">Cultiver</font>            ", 
  "title4" => "<font color=\"#63aabc\" size=\"5\">Service</font>            "
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style type="text/css">
  .mainCocity_<?= $kunik?>{
    padding: 3%;
    background-size: 100% 100%;    
    background-position: bottom;
    background-repeat: no-repeat;    
    padding-bottom: 2%;
  }

  .bloc-img_<?= $kunik?>{
    padding-top: 10%;
    padding-bottom: 10%;  
    height: 300px;
    width: 40%;
    background-size: cover;
    font-size: 19px;
    box-shadow: 0px 0px 6px silver;
    
  }


  .bloc-img_<?= $kunik?> a{
    position: absolute;
    top: 35%;
    left: 0;
    right: 0;
    text-align: center;
    z-index: 1;
    font-size: 20px;
  }
  .mainCocity_<?= $kunik?> .menu1:after, .mainCocity_<?= $kunik?> .menu2:after {
    content: " ";
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: rgba(0,0,0,.7);
  }

  .icon<?= $kunik?> {
    height: 60px;
    width: 60px;
    border-radius: 50%;
    padding: 12%;
    background-color: white;
    box-shadow: 1px 1px 2px 2px #e7f3f7;
  }

  .col<?= $kunik?> .col{
    padding-right: 25%;
    padding-left: 40%;
    margin-bottom: 7%;
  }
  .col<?= $kunik?> .col i{
    font-size: 30px;
    color: #63aabc;
  } 
  .icone<?= $kunik?> {
    font-size: 25px;
    color: #63aabc;
  }
  .mainCocity_<?= $kunik?>  h4{
      text-transform: none;
    }
  
  @media (max-width: 978px) {
    .col<?= $kunik?> .icon<?= $kunik?> i{
      font-size: 20px !important;
    }
    .col<?= $kunik?> .icone<?= $kunik?>{
      font-size: 18px !important;
    }
    .mainCocity_<?= $kunik?> .col<?= $kunik?> h4{
      font-size: 20px !important;
    }
    .mainCocity_<?= $kunik?> .col<?= $kunik?> p{
      font-size: 16px !important;
      margin-top: 5%;
      margin-left: 5%;
      margin-right: 5%;
    }
    .bloc-img_<?= $kunik?>{
      margin-top: 3%;
      padding-top: 10%;
      height: 200px;
      padding-bottom: 10%;
      width: 100%;
      margin-left: 0;
      background-size: cover;
      font-size: 17px;
    }
    .icon<?= $kunik?> {
     padding: 5%;
     height: 40px;
     width: 40px;
    }
    .col<?= $kunik?> .col{
      padding-left: 45%;
    }
    .col<?= $kunik?> {
      margin-bottom: 2%;
    }
  }
</style>
<div class="mainCocity_<?= $kunik?> row">
  <div class="col-xs-12 col-md-12 text-center  ">
    <h4  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <?= $paramsData["title"]?>
      </h4>
    <div class=" col-md-3  col-xs-12  col<?= $kunik?>" >
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-link"></i>
        </div>
      </div>        
      <h4  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title1">
        <?= $paramsData["title1"]?>
      </h4>
      <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content1" ><?= $paramsData["content1"]?></p>
    </div>
    <div class=" col-md-3  col-xs-12  col<?= $kunik?>" >  
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-eye"></i>
        </div>
      </div>  
       <h4  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title2">
        <?= $paramsData["title2"]?>
      </h4>
      <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content2" ><?= $paramsData["content2"]?></p>
    </div>
    <div class=" col-md-3  col-xs-12  col<?= $kunik?>" >  
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-book"></i>
        </div>
      </div>  
       <h4  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title3">
        <?= $paramsData["title3"]?>
      </h4>
      <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content3" ><?= $paramsData["content3"]?></p>
    </div>
    <div class=" col-md-3  col-xs-12 col<?= $kunik?>" >
      <div class="col">
        <div class="icon<?= $kunik?>">
          <i class="fa fa-heart-o"></i>
        </div>
      </div>  
       <h4  class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title4">
        <?= $paramsData["title4"]?>
      </h4>
      <p class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content4" ><?= $paramsData["content4"]?></p>
    </div>
  </div>
</div>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
                dyFObj.closeForm();
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    mylog.log("paramsData",sectionDyf);
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });
  })
</script>