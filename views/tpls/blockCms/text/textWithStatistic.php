<?php 
$keyTpl = "textWithStatistic";
$paramsData=[
	"text1"=>"Participants: 2-10",
	"text2" => "Prep Time: 30min",
	"text3"=>"Time to Run : 1-3 hours"
];
if (isset($blockCms)) {
	foreach ($paramsData as $e => $v) {
		if (  isset($blockCms[$e]) ) {
			$paramsData[$e] = $blockCms[$e];
		}
	}
}
?>
<style>
    .col<?= $kunik ?>{
        background-color: #656565 ;
        color: white;
        text-align: center;
        padding: 5px;
        border-radius: 10px;

    }
</style>
<div class="col-xs-12">
    <div class="col-xs-4">
        <div class="col<?= $kunik ?>" >
            <i class="fa fa-users" ></i>
            <span class="sp-text" data-id="<?= $blockKey ?>" data-field="text1"><?= $paramsData["text1"]?></span>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="col<?= $kunik ?>">
            <i class="fa fa-file" ></i>
            <span class="sp-text" data-id="<?= $blockKey ?>" data-field="text2"><?= $paramsData["text2"]?></span>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="col<?= $kunik ?>">
            <i class="fa fa-clock-o" ></i>
            <span class="sp-text" data-id="<?= $blockKey ?>" data-field="text3"><?= $paramsData["text3"]?></span>
        </div>
    </div>

</div> 