
<?php
$keyTpl     = "btnFollow";
$paramsData = [
  "buttonColorlabel"  => "#000000",
  "buttonColor" => "#e2e2e2",
	"buttonPadding" => "8px 8px",
	"buttonBorderRadius" => "2",
	"buttonsize"=> "20"
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if ( isset($blockCms[$e])) {
      $paramsData[$e] = $blockCms[$e];
    }
  } 
}
?>
<style>
  .parallax_<?=$kunik?> a{
		color: <?= $paramsData["buttonColorlabel"]?>;
		background-color: <?= $paramsData["buttonColor"]?>;
		padding: <?= $paramsData["buttonPadding"]?>;
		border-radius: <?= $paramsData["buttonBorderRadius"]?>px;
		font-size :<?= $paramsData["buttonsize"]?>px;
		height:inherit;
	}
</style>
<?php if(@Yii::app()->session["userId"]){?>
  <section  class="parallax_<?=$kunik?>" >
    <div class="textBack_<?=$kunik?> text-center container"  >
      <?php  if (@$el["links"]["followers"][Yii::app()->session["userId"]]){ ?>
        <a href="javascript:links.disconnect('<?php echo $el["collection"] ?>','<?php echo (string)$el["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>','followers')" >
          <?php echo Yii::t("common", "Don't follow this page"); ?>
        </a>
      <?php }else { ?>
        <a href="javascript:links.follow('<?php echo $el["collection"] ?>','<?php echo (string)$el["_id"] ?>','<?php echo Yii::app()->session["userId"] ?>','<?php echo Person::COLLECTION ?>')"><?php echo Yii::t("common","Follow") ?> </a>		
      <?php } ?>
    </div>
  </section>
<?php }?>
<script type="text/javascript">
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$(".create<?= $kunik?>").click(function(){
			<?= $paramsData["btnFctOnClick"] ?>
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "Configurer votre section",
				"description" : "Personnaliser votre section",
				"icon" : "fa-cog",
				"properties" : {	
					"buttonColorlabel" : {
						label : "Couleur du label du bouton",
						inputType:"colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.buttonColorlabel
					},
					"buttonColor":{
						label : "Couleur du fond du bouton",
						inputType : "colorpicker",
						values :  sectionDyf.<?php echo $kunik?>ParamsData.buttonColor
					},
					"buttonsize":{
						label:"Taille de la texte( en px)",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.buttonsize,
						rules:{
							number :true
						}
					},
					"buttonPadding":{
						label:"Rembourage du bouton",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.buttonPadding
						
					},
					"buttonBorderRadius":{
						label:"Rayon de la bordure( en px)",
						inputType :"text",
						values : sectionDyf.<?php echo $kunik?>ParamsData.buttonBorderRadius,
						rules:{
							number :true
						}
					}

				},
				beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
				save : function () {  
					tplCtx.value = {};
					$.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
						if (k == "parent") {
							tplCtx.value[k] = formData.parent;
						}
					});
					console.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
				var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
				var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
				var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
				cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"button",4,6,null,null,"Propriété du bouton","#000091","");
		});

	})
</script> 