<?php 
$keyTpl = "textWithBg";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
  "height"=> "200",
  "contentIcone" =>"map-marker",
  "content"=>"Où en est-on ?",
  "contentMarginTop" => "20",
  "contentMarginBottom" => ""

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
  .<?= $kunik?> {
    min-height: <?= $paramsData["height"]?>px !important;
  }
  .<?= $kunik?> h1{
    margin-top: <?= $paramsData["contentMarginTop"]?>px !important ;
    margin-bottom: <?= $paramsData["contentMarginBottom"]?>px !important 
  }
  .<?= $kunik?> h1 i{
    font-size: 60px;
  }
  @media (max-width: 767px) {
    .<?= $kunik?> h1{
      font-size: 25px !important;
    }
    
  }

</style>
<div class="col-xs-12 no-padding support-section text-center <?= $kunik?>">
  <h1>
    <i class="fa fa-<?= $paramsData["contentIcone"]?>"></i><br/>
    <font class=" sp-text" data-id="<?= $blockKey ?>" data-field="content"><?= $paramsData["content"]?></font>
    
  </h1>
</div>
<script type="text/javascript">
  var fontAwesome = (typeof fontAwesome != "undefined") ? fontAwesome : "";
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
				"description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {  
          "height" : {
            "inputType" : "text",
            label : "<?php echo Yii::t('cms', 'Height of the block')?>",
            rules: {
              number: true,
            },
            values :  sectionDyf.<?php echo $kunik?>ParamsData.height
          },
          "contentIcone" : {
            "inputType" : "select",
            label : "<?php echo Yii::t('cms', 'Icon ')?> ",
            options :fontAwesome,
            values :  sectionDyf.<?php echo $kunik?>ParamsData.contentIcone
          },
          contentMarginTop :{
            "inputType" : "text",
            label : "<?php echo Yii::t('cms', 'Margin top of the content')?> ",
            rules: {
              number: true,
            },
            values :  sectionDyf.<?php echo $kunik?>ParamsData.contentMarginTop
          },
          contentMarginBottom :{
            "inputType" : "text",
            label : "<?php echo Yii::t('cms', 'Margin bottom of the content')?> ",
            rules: {
              number: true,
            },
            values :  sectionDyf.<?php echo $kunik?>ParamsData.contentMarginBottom
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });
          mylog.log("save tplCtx",tplCtx);
          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                $("#ajax-modal").modal('hide');
                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }
        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  })

</script>