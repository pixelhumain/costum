<?php 
    $keyTpl = "countdown";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss]; 

    $kunik = $keyTpl.(string)$blockCms["_id"];
    $blockKey = (string)$blockCms["_id"];
    $page = isset($page)?$page:"";
    $dateCible = strtotime($blockCms["datetime"] ?? "2024-06-13"); 
    $dateActuelle = time(); 
    $diff = $dateCible - $dateActuelle;

    $joursRestants = floor($diff / (60 * 60 * 24))+1;
    $label = "J-";
    if($joursRestants < 0){
        $label = "J+";
        $joursRestants = -$joursRestants;
    }

?>
<style id="css-<?= $kunik ?>">

</style>
<p class=" <?= $kunik ?>"><?php echo $label.$joursRestants; ?></p>
<script>
    sectionDyf.<?php echo $kunik?>blockCms = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode){
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var  countdownInput = {
			configTabs : {
                general: {
                    inputsConfig: [
                        {
                            type: "dateTimePicker",
                            options: {
                                name: "datetime",
                                label: "date",
                                placeholder: "date",
                                classInput: "classInputDateTime",
                                configForDateTime: {
                                    autoclose: true,
                                    lang: 'fr',
                                    format: 'y-m-d'
                                }
                            }
                        }
                    ],
                },
				style : {
					inputsConfig : [
						"color",
						"fontSize",
						"textAlign",
						"fontFamily",
                        "fontWeight",
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			}
		}
		cmsConstructor.blocks["<?= $kunik ?>"] =   countdownInput;
		
	}
</script>