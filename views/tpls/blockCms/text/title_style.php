<?php 
	$myCmsId  = $blockCms["_id"]->{'$id'};
    $container = "content-".$kunik." .swiper-container";
    $styleCss = (object) [$container => $blockCms["css"] ?? [] ];

    $title  = ($blockCms["title"] ?? [ "fr" => "Questions Fréquentes" ]);
    $title = is_array($title) ? $title : [ "fr" => $title ];
?>

<style type="text/css">
	.hex<?php echo $kunik ?> {
      position: relative;
      float: left;
      height: 60px;
      min-width: 75px;

      font-weight: bold;
      text-align: center;
      background: <?php echo $blockCms["css"]["bgcolor"]["backgroundColor"]; ?>;
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
    }
    .hex<?php echo $kunik ?>.gradient-bg {
      left: 50%;
      transform: translate(-50%,-50%);
      min-width: 60%;
    }

    .hex<?php echo $kunik ?>:before {
      position: absolute;
      content: '';
      height: calc(100% - 14px);  /* 100% - 2 * border width */
      width: calc(100% - 14px);  /* 100% - 2 * border width */
      left: 7px; /* border width */
      top: 7px; /* border width */
      -webkit-clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      clip-path: polygon(20px 0px, calc(100% - 20px) 0px, 100% 50%, calc(100% - 20px) 100%, 20px 100%, 0px 50%);
      z-index: -1;
    }
    .hex<?php echo $kunik ?>.gradient-bg:before {
      background: <?php echo $blockCms["css"]["bgcolor"]["backgroundColor"]; ?>;
    }
    .hex<?php echo $kunik ?>.white-bg:before {
      background: #ffffff!important;
    }

    .hex<?php echo $kunik ?>.white-bg {
      background: #ffffff!important;
    }
    .hex<?php echo $kunik ?> span {
        /* display: inline-block; */
        margin-top: 30px;
        padding: 8px;
        transform: translateY(-50%);
        margin-left: 20px;
        margin-right: 20px;
        font-size: 28px;
    }
    .hex<?php echo $kunik ?> .bg-green {
      background: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $blockCms["css"]["colordecor"]["backgroundColor"]; ?>!important;
      /*color: #fff;*/
    }

    .hex<?php echo $kunik ?> .bg-green span {
      font-size: 40px;
      margin-left: 10px;
      margin-right: 10px;
    }
    .hex<?php echo $kunik ?> .bg-green span.fa {
      font-size: 30px;
    }
    .contain-faq {
        display: flex;
    }
    @media (max-width: 991px ) {
        .hex<?php echo $kunik ?>.gradient-bg {
          width: 100%;
        }
        .hex<?php echo $kunik ?> span {
            display: block;
        }
    }
    @media (max-width: 767px ) {
        .hex<?php echo $kunik ?> span {
            font-size: 20px;
        }
        .prez-heading-text {
            font-size: 20px;
        }
    }
</style>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 no-padding prez-multi-col <?= $kunik ?> <?= $kunik ?>-css" data-kunik="<?= $kunik ?>" data-id="<?= $myCmsId ?>">
    <div class="col-xs-12 col-sm-12 col-md-12 no-padding contain-faq">
       <div class='hex<?php echo $kunik ?> gradient-bg mt-40'>
            <div class='hex<?php echo $kunik ?> bg-green'>
            	<?php if ($blockCms['typeInLeft'] == "text") { ?>
              		<span style="color: <?=$blockCms["css"]["textLeftColor"]["color"] ?>;"> <?=$blockCms["textLeft"] ?>
              		</span>
            	<?php }else if ($blockCms['typeInLeft'] == "icon") { ?>
            		<span class="fa fa-<?= $blockCms["iconLeft"] ?>" style="color: <?= $blockCms["css"]["iconLeftColor"]["color"] ?>;">
            		</span>
            	<?php } ?>
            </div>
          <span class="title-1 sp-text title<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></span>
        </div> 
    </div>

</div>
<style id="titleWithStyle<?= $kunik ?>">
</style>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
	str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>, `<?= $kunik ?>`);
    $("#titleWithStyle<?= $kunik ?>").append(str);
    if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
            var titleWithStyle = {
            configTabs: {
                general: {
                    inputsConfig : [
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "typeInLeft",
                                label : tradCms.displayOnTheLeftOfTheTitle,
                                tabs: [
                                    {
                                        value:"text",  
                                        label: tradCms.textProperty,
                                        inputs : [
                                            {
                                                type : "inputSimple",
                                                options : {
                                                    name : "textLeft",
                                                    label : tradCms.textOnTheLeft
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        
                                        value:"icon", 
                                        label: tradCms.iconProperty,   
                                        inputs :[
                                            {
                                                type: "inputIcon",
                                                options : {
                                                    name: "iconLeft",
                                                    label: tradCms.iconOnTheLeft
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ]
			    },
                style : {
                    inputsConfig : [
						{
							type : "section",
                            options : {
                                name : "iconLeftColor",
                                label : tradCms.colorOfTheIcon,
                                inputs : [
                                    "color"
                                ]
                            }
						},
						{
							type : "section",
                            options : {
                                name : "textLeftColor",
                                label : tradCms.textColorOnTheLeft,
                                inputs : [
                                    "color"
                                ]
                            }
						},
						{
							type : "section",
                            options : {
                                name : "bgcolor",
                                label : tradCms.backgroundColorOfTheTitle,
                                inputs : [
                                    "backgroundColor"
                                ]
                            }
						},
						{
							type : "section",
                            options : {
                                name : "colordecor",
                                label : tradCms.backgroundColorOfTheTitle + "(Decoration)",
                                inputs : [
                                    "backgroundColor"
                                ]
                            }
						}
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.title_style<?= $myCmsId ?> = titleWithStyle;
    }

    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");
</script>