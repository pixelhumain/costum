<?php 
$myCmsId  = $blockCms["_id"]->{'$id'};
$container = "content-".$kunik." .swiper-container";
$styleCss = (object) [$container => $blockCms["css"] ?? [] ];
$keyTpl = "number_text";
$paramsData = [
    "title" => "Titre block",
    "design" => "1",
    "colorBloc" => "#202A5F",
    "colorTitle" => "#49CBE3",
    "colorText" => "white",
    "previewxs" => 1,
    "previewsm" => 2,
    "previewmd" => 3,
    "previewlg" => 6,
    "content"=>" "

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);

?>

<style type="text/css">
    .box<?= $kunik?> .box-title h1 {
        font-family: 'NotoSans-Bold';
        color:<?= $paramsData["colorTitle"]?> ;
    }
    .box<?= $kunik?> .swiper-container {
        overflow: unset;
    }

    .box<?= $kunik?> .box-part{
        background:<?= $paramsData["colorBloc"]?>;
        border-radius:15px;
        min-height: 250px;
        padding:20px 10px;
        margin:30px 0px;
        box-shadow: 0px 0px 6px silver;
    }

    
    .box<?= $kunik?> .box-part span{
        font-family: 'NotoSans-Regular';
        font-size: 18px;
        color:<?= $paramsData["colorText"]?> ;
    }
    .box<?= $kunik?> .text{
        margin:20px 0px;
        min-height:100px;
    }
    .card<?= $kunik?>{
        margin-left : 3px;
    }
    @media (min-width : 1200px){
        .box<?= $kunik?> .box-part{
            min-height: 290px;
        }
    }

    @media (min-width: 487px) and (max-width: 992px ){
        .box<?= $kunik?> .box-part{
            min-height: 290px;
        }
    }
    .box<?= $kunik?> .btn-editSection {
        width: 35px;
        height: 35px;
        line-height: 35px;
        border-radius: 50%;
        font-size: 18px;
        border: 1px solid #ffffff;
        margin-right: 5px;
        padding-left: 5px;
        transition: all .5s ease 0s;
        padding: 0px;
    }
    .box<?= $kunik?> .swiper-container-horizontal>.swiper-pagination-bullets{
        top: 100%;
    }
    .box<?= $kunik?> .swiper-pagination-bullet.swiper-pagination-bullet-active{
        background-color: #49CBE3;
        border: none;
    }
    .box<?= $kunik?> .swiper-pagination-bullet{
        border: 1px solid white;
    }
    .box<?= $kunik?> .swiper-button-next, .box<?= $kunik?> .swiper-button-prev{
        color : <?= $paramsData["colorTitle"]?> ;
        bottom: -10%;
        top : auto;
    }
    .box<?= $kunik?> .swiper-button-next{
        right: 40%;
    }
    .box<?= $kunik?> .swiper-button-prev{
        left : 40%;
    }
    @media  (max-width: 767px)   {
        .box<?= $kunik?> .card<?= $kunik?> .box-part{
            margin-left: 10px !important;
            min-height: 357px !important;
        }
        .swiper-button-next, .swiper-button-prev{
            display: none;
        }
        .box<?= $kunik?>{
            overflow: hidden;
        }

    }

    .card<?= $kunik?> img{
        height: 50px;
        width: auto;

    }



    .card-service-<?= $kunik?> .single-service{
        padding:30px 40px;
        text-align:center;
        border:1px solid #ddd;
        box-shadow:0 5px 30px -5px #eee;
        position:relative;
        min-height: 280px;
    }
    .card-service-<?= $kunik?> .service-icon{
        position:relative;
        margin-bottom:40px;
    }
    .card-service-<?= $kunik?> .service-icon img{
        color: #92bf34;
        padding: 10px;
        border: 1px solid #92bf34;
        border-radius: 50%;
        transition: .3s;
        width: 90px;
        height: 90px;
        object-fit: contain;
    }
    .card-service-<?= $kunik?> .service-icon:before{
        position:absolute;
        content:"";
        width:2px;
        height:20px;
        background-color:#92bf34;
        left:50%;
        top:100%;
        transform:translateX(-50%)
    }
    .card-service-<?= $kunik?> .service-icon:after{
        position:absolute;
        content:"";
        width:40px;
        height:2px;
        background-color:#92bf34;
        bottom:-20px;
        left:50%;
        transform:translateX(-50%)
    }
    .card-service-<?= $kunik?> .single-service h4{
        text-transform:uppercase;
        font-size:22px;
        margin-bottom:10px;
        font-weight: 700;
    }
    .card-service-<?= $kunik?> .single-service p{
        margin-bottom:10px;
    }
    .card-service-<?= $kunik?> .single-service a.moreInfo{
        position:absolute;
        padding:10px 30px;
        color:#fff;
        text-decoration:none;
        background-color:#92bf34;
        left:50%;
        transform:translateX(-50%);
        margin-top:10px;
        transition:.3s;
    }
    .card-service-<?= $kunik?> .single-service a.moreInfo:hover{
        background-color:#333;
    }
    .card-service-<?= $kunik?> .single-service:before{
        position:absolute;
        content:"";
        height:0;
        left:0;
        top:0;
        border-left:2px solid #92bf34;
        transition:.3s;
    }
    .card-service-<?= $kunik?> .single-service:after{
        position:absolute;
        content:"";
        height:0;
        right:0;
        bottom:0;
        border-right:2px solid #92bf34;
        transition:.3s;
    }
    .card-service-<?= $kunik?> .single-service:hover:before, .card-service-<?= $kunik?> .single-service:hover:after{
        height:50%;
    }
    .card-service-<?= $kunik?> .single-service:hover img{
        background-color:#92bf34;
        color:#fff;
    }
    .modal-header{
        background-color: #94c138;
        color: white;
    }
    body.modal-open {
        overflow: hidden !important;
    }
</style>
<?php 
    $initFilesIcone= Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms'
        ), "image"
    );
    $allIcone = [];
    foreach($initFilesIcone as $ke => $va){
        $allIcone[$va["subKey"]] = $va;
    }
?>
<div class="col-xs-12 no-padding">
    <div class="z-index-10 col-xs-12">
        <h2 class="title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
            <?php //echo $paramsData["title"] ?>
        </h2>
    </div>
    <div class="box<?= $kunik?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
        
         <?php if($costum["editMode"] == "true"){?>
            <div class="text-center edit<?= $kunik?> editSectionBtns hiddenPreview z-index-10 col-xs-12">
                <div class="" style="width: 100%; display: inline-table; padding: 10px;">
                    <div class="text-center ">
                        <button class="btn btn-primary addElement<?= $kunik ?>"><?php echo Yii::t('cms', 'Add content')?></button>
                    </div>
                </div>
            </div> 
        <?php } ?>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                if ($paramsData["design"] == "1") {
                foreach ($paramsData["content"] as $key => $value) {
                    ${'initFilesIcone' . $key}= Document::getListDocumentsWhere(
                        array(
                          "id"=> $blockKey,
                          "type"=>'cms',
                          "subKey"=> (string)$key
                        ), "image"
                      );
                      ${'arrFileIcone' . $key}= [];
                      foreach (${'initFilesIcone' . $key} as $k => $v) {
                        ${'arrFileIcone' . $key}[] =$v['imagePath'];
                      }
                    ?>
                    <div class="swiper-slide card<?= $kunik?>">
                        <div class="box-part text-center">
                                <?php if (count(${'arrFileIcone' . $key})!=0) {?>
                        <img src="<?php echo ${'arrFileIcone' . $key}[0] ?>" alt="">
                        <?php } ?>
                            <div class="box-title">
                                <h1 class="single "><?php echo $value["number"]?></h1>
                            </div>
                            <div class="text">
                                <span ><?php echo $value["title"]?></span>
                            </div>
                            <?php if($costum["editMode"] == "true"){ ?>
                                <div class="text-center editSectionBtns hiddenPreview" >
                                    <a  href="javascript:;"
                                    class="btn  btn-primary btn-editSection editElement<?= $kunik ?>"
                                    data-key="<?= $key ?>"
                                    data-number='<?= @$value["number"] ?>'
                                    data-icon='<?php echo json_encode(${"initFilesIcone" . $key}) ?>' 
                                    data-position='<?php echo $key ?>' 
                                    data-title='<?= @$value["title"] ?>' >
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a  href="javascript:;"
                                    class="btn  bg-red text-center btn-editSection deleteElement<?= $kunik?> "
                                    data-id ="<?= $blockKey ?>"
                                    data-path="content.<?= $key ?>"
                                    data-collection = "cms"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php }
                } else {
                    foreach ($paramsData["content"] as $key => $value) {
                        ${'initFilesIcone' . $key}= Document::getListDocumentsWhere(
                            array(
                                "id"=> $blockKey,
                                "type"=>'cms',
                                "subKey"=> (string)$key
                            ), "image"
                        );
                        ${'arrFileIcone' . $key}= [];
                        foreach (${'initFilesIcone' . $key} as $k => $v) {
                            ${'arrFileIcone' . $key}[] =$v['imagePath'];
                        }
                        ?>
                        <div class="swiper-slide card-service-<?= $kunik?>">
                            <div class="single-service">
                                <?php if (count(${'arrFileIcone' . $key})!=0) {?>
                                <div class="service-icon">
                                    <img src="<?php echo ${'arrFileIcone' . $key}[0] ?>" alt="">
                                </div>
                                <?php } ?>
                                <h4 class="single "><?php echo $value["number"]?></h4>
                                <div class="text">
                                    <p ><?php echo $value["title"]?></p>
                                </div>
                                <?php if($costum["editMode"] == "true"){ ?>
                                    <div class="text-center editSectionBtns hiddenPreview" >
                                        <a  href="javascript:;"
                                            class="btn  btn-primary btn-editSection editElement<?= $kunik ?>"
                                            data-key="<?= $key ?>"
                                            data-number='<?= @$value["number"] ?>'
                                            data-icon='<?php echo json_encode(${"initFilesIcone" . $key}) ?>'
                                            data-position='<?php echo $key ?>'
                                            data-title='<?= @$value["title"] ?>' 
                                            data-description='<?= @$value["description"] ?>' >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a  href="javascript:;"
                                            class="btn  bg-red text-center btn-editSection deleteElement<?= $kunik?> "
                                            data-id ="<?= $blockKey ?>"
                                            data-path="content.<?= $key ?>"
                                            data-collection = "cms"
                                        >
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                                <a class="moreInfo read-more"    data-description='<?= @$value["description"] ?>'><?php echo Yii::t('cms', 'More information')?></a>
                            </div>
                        </div>

                <?php }
                }
                ?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
    <div class="modal fade" id="descriptionTarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background-color: rgba(0, 0, 0, 0.5);">
        <div class="modal-dialog modal-lg" role="document" style="margin-top: 8%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Les tarifs</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -4%">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
    <!--<div id="particles-js"></div>-->
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    
    jQuery(document).ready(function() {
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($paramsData); ?>;
        }
        appendTextLangBased(".title<?= $kunik ?>", <?= json_encode($costum["langCostumActive"]) ?>, <?= json_encode($paramsData["title"]) ?>, "<?= $blockKey ?>");
        var allContent = <?= json_encode($paramsData["content"])?>;
        var allIcone = <?= json_encode($allIcone)?>;
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "design" : {
                        "label" : "Design",
                        inputType : "select",
                        options : {
                            "1" : "Design 1",
                            "2" : "Design 2",
                        },
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.design
                    },
                    "colorBloc" : {
                        "label" : "Couleur du fond de la carte",
                        inputType : "colorpicker", 
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorBloc
                    },
                    "colorTitle" : {
                        "label" : "Couleur du titre",
                        inputType : "colorpicker", 
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorTitle
                    },
                    "colorText" : {
                        "label" : "Couleur du texte",
                        inputType : "colorpicker", 
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorText
                    },
                    "previewxs" : {
                        label : "<?php echo Yii::t('cms', 'Preview')?> (xs)",
                        "inputType" : "text",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.previewxs
                    },
                    "previewsm" : {
                        label : "<?php echo Yii::t('cms', 'Preview')?> (sm)",
                        "inputType" : "text",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.previewsm
                    },
                    "previewmd" : {
                        label : "<?php echo Yii::t('cms', 'Preview')?>  (md)",
                        "inputType" : "text",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.previewmd
                    },
                    "previewlg" : {
                        label : "<?php echo Yii::t('cms', 'Preview')?> (lg)",
                        "inputType" : "text",
                        "rules" : {
                            "required": true,
                            "number" : true
                        },
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.previewlg
                    },
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();

                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"color",4,6,null,null,"Propriété de la carte","green","");
            alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"preview",3,6,null,null,"Propriété de l'affichage","green","");
        });
        $(".addElement<?= $kunik ?>").click(function() {        
            var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($paramsData["content"]); ?>);
            var	lastContentK = 0; 
            if (keys<?= $blockCms['_id'] ?>.length!=0) 
                lastContentK = parseInt((keys<?= $blockCms['_id'] ?>[(keys<?= $blockCms['_id'] ?>.length)-1]), 10);
            var tplCtx = {};
            tplCtx.id = "<?= $blockKey ?>";
            tplCtx.collection = "cms";
            var content ={};

            var obj = {
                subKey : (lastContentK+1),
                number :     $(this).data("number"),
                icon :     $(this).data("icon"),
                position :     lastContentK+1,
                title :     $(this).data("title"),
                
            };
            <?php if ($paramsData["design"] == "2") { ?>
                obj.description = $(this).data("description")
            <?php } ?>
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add content')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                            $(".parentfinder").css("display","none");
                            
                        }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) { 
                        tplCtx.value = {};
                        position = "";
                        value = {}; 
                        if(typeof sectionDyf.<?php echo $kunik?>ParamsData.content == "string"){
                            $.each( activeForm.jsonSchema.properties , function(k,val) {
                                tplCtx.value[k] = $("#"+k).val();
                            })
                            tplCtx.path = "content."+(lastContentK+1);
                        }else{
                            $.each( activeForm.jsonSchema.properties , function(k,val) { 
                                if(k == "position") {
                                    position = $("#"+k).val();
                                    if(typeof allContent[position] != "undefined" && position != lastContentK+1){                                    
                                        $.each( allContent , function(k1,val1) { 
                                            if(parseInt(k1) >= parseInt(position)){
                                                content[parseInt(k1)+1] = val1; 
                                                content[parseInt(k1)+1]["position"] = parseInt(k1)+1;
                                                if(typeof allIcone[k1] != "undefined" && typeof allIcone[k1]._id != "undefined"){
                                                    id = allIcone[k1]._id.$id ;
                                                    changeDocument(id,parseInt(k1)+1);
                                                }
                                            } 
                                            else {
                                                content[parseInt(k1)] = val1;
                                            }
                                        });
                                    }else{
                                        content = allContent; 
                                    }
                                }
                                value[k] =  $("#"+k).val();
                            });
                            content[position] = value;
                            tplCtx.path = "content";
                            tplCtx.value = content;
                        }
                        
                        mylog.log("save tplCtx",tplCtx);

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }

                }
            };
            dyFObj.openForm( activeForm );
        });
        $(".read-more").click(function() { 
            var src = "";
            var description = $(this).data("description");
            src += `<p> La participation mensuelle permet d’améliorer le commun et donne droit à l’assistance mail et chat. </p>
                    <p>
                        Le prix fixe comprend : <br />
                    <ul>
                        <li>Personnalisation d’une de nos maquettes : ajout du logo, couleurs et textes</li>
                        <li>Choix des fonctionnalités (<a href="https://doc.co.tools/books/2---utiliser-loutil/page/liste" target="_blank">voir la liste</a>)</li>
                        <li>500 Mo d’espace de stockage</li>
                        <li><b>Formation de 3 heures à la prise en main de l’outil</b></li>
                    </ul>
                    </p>
                    <p>
                        Il est bien sûr possible d’aller plus loin (devis sur simple demande) : <br />
                    <ul>
                        <li>Importer des données</li>
                        <li>Organiser un événement convivial de référencement collectif (cartopartie)</li>
                        <li>Créer une maquette personnalisée</li>
                        <li>Intégrer votre maquette réalisée chez un autre prestataire</li>
                        <li>Créer une nouvelle fonctionnalité</li>
                        <li>Organiser un concours</li>
                    </ul>
                    </p>`;
            $("#descriptionTarif .modal-body").html(description);
            $("#descriptionTarif").modal("show")
        })       
        $(".editElement<?= $kunik ?>").click(function() {        
            var key = $(this).data("key");
            var tplCtx = {};
            tplCtx.id = "<?= $blockKey ?>";
            tplCtx.collection = "cms";

            var content ={};

            tplCtx.path = "content."+(key);
            var obj = {
                subKey : key,
                number :     $(this).data("number"),
                icon :     $(this).data("icon"),
                position :     key,
                title :     $(this).data("title"),
                
            };
            <?php if ($paramsData["design"] == "2") { ?>
                obj.description = $(this).data("description")
            <?php } ?>
            var activeForm = {
                "jsonSchema" : {
                    "title" : "<?php echo Yii::t('cms', 'Add content')?>",
                    "type" : "object",
                    onLoads : {
                        onload : function(data){
                        $(".parentfinder").css("display","none");
                    }
                    },
                    "properties" : getProperties(obj),
                    beforeBuild : function(){
                        uploadObj.set("cms","<?= $blockCms['_id'] ?>");
                    },
                    save : function (data) {  
                        tplCtx.value = {};
                        position = "";
                        value = {};
                        $.each( activeForm.jsonSchema.properties , function(k,val) { 
                            if(k == "position") {
                                position = $("#"+k).val();

                                if(typeof allContent[position] != "undefined" && position != obj.position){                                   
                                    $.each( allContent , function(k1,val1) { 
                                        if(parseInt(k1) >= parseInt(position)){
                                            content[parseInt(k1)+1] = val1;
                                            content[parseInt(k1)+1]["position"] = parseInt(k1)+1;
                                            if(typeof allIcone[k1] != "undefined" && typeof allIcone[k1]._id != "undefined"){
                                                id = allIcone[k1]._id.$id ;
                                                changeDocument(id,parseInt(k1)+1);
                                            }
                                        } 
                                        else {
                                            content[parseInt(k1)] = val1;
                                        }
                                    });
                                    if(position > obj.position){
                                        content[obj.position] = undefined;
                                    }else{
                                        content[obj.position+1] = undefined;
                                    }
                                }else{
                                    content = allContent;
                                    if(position != obj.position){
                                        content[obj.position] = undefined;
                                        if(typeof obj.icon[0] != "undefined" && typeof obj.icon[0]._id != "undefined"){
                                            id = obj.icon[0]._id.$id ;
                                            changeDocument(id,position);
                                        }
                                    }
                                }
                            }
                            value[k] =  $("#"+k).val();
                        });

                        if(typeof obj.icon[0] != "undefined" && typeof obj.icon[0]._id != "undefined"){
                            id = obj.icon[0]._id.$id ;
                            changeDocument(id,position);
                        }
                        content[position] = value;
                        tplCtx.path = "content";
                        tplCtx.value = content;

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                dyFObj.commonAfterSave(params,function(){
                                    toastr.success("Élément bien ajouté");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                                });
                            } );
                        }
                    }
                }
            };
            dyFObj.openForm( activeForm );
        });
        
        function changeDocument(id,newPosition){
            tplCtx.id = id;
            tplCtx.collection = "documents"; 
            tplCtx.path = "subKey";
            tplCtx.value = newPosition;
            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                    });
                } );
            }
        }
        function getProperties(obj={}){
            var props = {
                title : {
                    label : "<?php echo Yii::t('cms', 'Title')?>",   
                    inputType : "text",
                    value : obj["title"]
                },
                number : {
                    label : "<?php echo Yii::t('cms', 'Number')?>",
                    inputType : "text",
                    value : obj["number"]
                },
                position : {
                    //label : "<?php //echo Yii::t('cms', 'Position')?>",
                    inputType : "hidden",
                    value : obj["position"],
                    rules:{
                        number :true
                    }
                },
				logo : {
					inputType : "uploader",	
					label : "<?php echo Yii::t('cms', 'Logo')?>",
                     docType: "image",
					contentKey : "slider",
					domElement : obj["subKey"],		
                    filetype : ["jpeg", "jpg", "gif", "png"],
                    showUploadBtn: false,
                    endPoint :"/subKey/"+obj["subKey"],
                    initList : obj["icon"]
				},
            };

            
            <?php if ($paramsData["design"] == "2") { ?>  
                props.description = {
                    label : "<?php echo Yii::t('cms', 'Description')?>",   
                    inputType : "textarea",
                    markdown : true,
                    value : obj["description"]                
                }  
            <?php } ?>
            return props;
        }
        $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",

				function(result){
					if (!result) {
						return;
					}else {
						dataHelper.path2Value( deleteObj, function(params) {
							mylog.log("deleteObj",params);
							toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
							// urlCtrl.loadByHash(location.hash);
						});
					}
				}); 
			
		});
        var swiper = new Swiper(".box<?= $kunik?> .swiper-container", {
            slidesPerView: 2,
            spaceBetween: 1,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 1500,
            },
            keyboard: {
                enabled: true,
            },
            breakpoints: {
                486: {
                    slidesPerView: <?= $paramsData["previewxs"] ?>,
                },
                768: {
                    slidesPerView:<?= $paramsData["previewsm"] ?>,
                    spaceBetween: 10,
                },
                992: {
                    slidesPerView: <?= $paramsData["previewsm"] ?>,
                    spaceBetween: 10,
                },
                1024: {
                    slidesPerView: <?= $paramsData["previewmd"] ?>,
                    spaceBetween:10,
                },
                1200: {
                    slidesPerView: <?= $paramsData["previewlg"] ?>,
                    spaceBetween:10,
                },
            }
        });
    });

<?php
if ($paramsData["design"] == "1") { ?>
    /*cmsEngine.callParticle("particles-js");*/
 <?php } ?>



</script>