<?php 
$keyTpl = "bloctextwithtitle";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = $blockCms["_id"];

  $paramsData = [ 
    "title" => "Lorem ipsum",
    "subTitle" =>" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "titleColor" => "#333333",
    "subTitleColor" => "#333333"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if ( !empty($blockCms[$e]) && isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
 ?>

<style>
  .container<?= $kunik ?>{
    min-height: 100px !important;
  }
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
    color: <?php echo $paramsData["titleColor"] ?>;

  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
    font-weight: 500;
    color: <?php echo $paramsData["subTitleColor"] ?>;
  }
  .item<?php echo $kunik ?>{
    text-transform: none !important;
    font-size:23px;
  }
  @media (max-width: 414px) {
    
    .container<?php echo $kunik ?> h2{
      font-size: 18px !important;
    }
    .container<?php echo $kunik ?> h3{
      font-size: 15px !important;
    }    
    .subTitle<?php echo $kunik ?>{
      margin-bottom: 10px;
    }
    .container<?php echo $kunik ?> p{
      font-size: 14px !important;
    }
  }
</style>

<div class="container<?php echo $kunik ?> col-md-12">
  <h2 class="title title<?php echo $kunik ?> Oswald">
    <?php echo $paramsData["title"] ?>
  </h2>
  <h3 class="subtitle subTitle<?php echo $kunik ?> ReenieBeanie">
    <?php echo $paramsData["subTitle"] ?>
  </h3>
  <p class="description item<?php echo $kunik ?> markdown"><?php echo $paramsData["description"]?></p>
  <br>
  <?php 
//          echo $this->renderPartial("costum.views.tpls.editTplBtns", ["canEdit" => $canEdit, "kunik"=>$kunik, "page" => @$page,"id" => (string)$blockCms["_id"]]);  
    ?>
   <br>
</div>


 <script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                "title" : {
                    "inputType" : "text",
                    "label" : "Titre",
                    
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                },
                "titleColor"  : {
                    "label" : "Couleur du titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleColor
                },
                "subTitle" : {
                    "inputType" : "text",
                    "label" : "Couleur du sous titre",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitle
                },
                "subTitleColor"  : {
                    "label" : "Sous titre",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.subTitleColor
                },
                "description" :{
                      "inputType" : "textarea",
                      "label" : "Description",
                      "markdown" :true,
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.description
                }       
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                      // urlCtrl.loadByHash(location.hash);
                    });
                  } );
                  location.reload();
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>

