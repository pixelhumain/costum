  <?php
/* 
Super container:
Created by Ifaliana Arimanana
Edited by Sitraka Philippe
ifaomega@gmail.com
26 Apr 2021
*/

/***************** Required *****************/

use PixelHumain\PixelHumain\modules\costum\components\blockCms\BlockCmsWidget;
$keyTpl      = "container";
$myCmsId     = $blockCms["_id"]->{'$id'};
$blockParent = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";

/* Get settings */
$anchorTarget     = $blockCms["advanced"]["anchorTarget"] ?? "";
$name             = $blockCms["name"] ?? "";
$widthClass       = $blockCms["class"]["width"] ?? "std";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$balise         = $blockCms["advanced"]["balise"] ??  "div";
$order            = $blockCms["order"] ??  "0";
$backgroundImage = $blockCms["css"]["backgroundImage"] ?? "";
$backgroundType = $blockCms["css"]["backgroundType"] ?? "";
$backgroundColor = $blockCms["css"]["backgroundColor"] ?? "";
$typeUrl =  $blockCms["advanced"]["typeUrl"] ??  "internalLink";
$link =  $blockCms["advanced"]["link"] ??  "";
$targetBlank = $blockCms["advanced"]["targetBlank"] ?? false ;
$downloadFile = $blockCms["advanced"]["downloadFile"] ?? false ;


//fix responsive default value 

$mode = ["sm","xs"];
$defaultTexts = [
  "width" => "100%",
  "backgroundSize" => "contain",
  "paddingLeft" => "auto",
  "paddingRight" => "auto",
  "marginLeft" => "auto",
  "marginRight" => "auto",
];

foreach ($mode as $keyMode) {
  foreach ($defaultTexts as $key => $value) {
    $blockCms["css"][$keyMode][$key] =  $blockCms["css"][$keyMode][$key] ?? $value;
  };
};

$blockChildren  = isset($blockCms["blockChildren"]) ? $blockCms["blockChildren"] : [];

if ( $backgroundType == "" && $backgroundColor != "" ){
    $backgroundType = "backgroundColor";
}

// if( !isset($blockCms["css"]["height"]) || $blockCms["css"]["height"] == "" ){
//   $blockCms["css"]["height"] = "250px";
// }

if ( count($blockChildren) == 0 && $backgroundType != "" ) {
  if( !isset($blockCms["css"]["height"]) || $blockCms["css"]["height"] == "" ){
    $blockCms["css"]["height"] = "250px";
  }
}

if (isset($blockCms['blockParent'])) {
  $blockCms["css"]["width"] = $blockCms["css"]["width"] ?? "50%";
} else {
  $blockCms["css"]["width"] = $blockCms["css"]["width"] ?? "100%";
}

$objectCss        = $blockCms["css"] ?? [];
$styleCss = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];

$isDefaultSection = $blockCms["defaultSection"] ?? false;

// Alias cms
$aliasState = "";
if (isset($blockCms['alias'])) {
  foreach ($blockCms['alias'] as $keyParent => $valueParent) { 
    foreach ($valueParent["container"] as $key => $value) {   
      if (isset($blockCms["blockChildren"]) && isset($blockCms["blockChildren"][$value])) {
        $aliasState = "super-alias-".$value; 
      }  
    }
  } 
}

?>

<style type="text/css" id="sectionCss<?= $kunik ?>">
  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  
  <?php 
if ($backgroundImage == "" && count($blockChildren) > 0) { ?>
  @media (max-width: 800px) {
    .whole-<?= $kunik ?> {
      height: auto ;
    }
  }
<?php } ?>
  <?php if(isset($blockCms["animation"]["activeAnimate"]) && isset($blockCms["animation"]["animateType"]) && $blockCms["animation"]["animateType"] != "") {?>
    .<?= $kunik ?>-css {
      opacity: 0;
    }
    @keyframes cmsAnimate<?= $kunik ?> {
      <?= $blockCms["animation"]["animateType"] ?>
    }
    .visible-<?= $kunik ?>-css {
      opacity: 1;
      position: relative;
      animation-name: cmsAnimate<?= $kunik ?>;
      animation-duration: <?= isset($blockCms["animation"]["tempsAnimate"]) ? $blockCms["animation"]["tempsAnimate"] : 1.5 ?>s;  
      animation-fill-mode: forwards;
    }
  <?php } ?>
</style>

<script>
    $(function(){
        if (costum.editMode){
          cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        } else {
            if ( "<?= $link ?>" !== "" ){
              $(".<?= $kunik ?>").css("cursor", "pointer");
              $(".<?= $kunik ?>").find("*").css("cursor", "inherit");
              $(".container<?= $myCmsId ?>").off("click").on("click", function(e){ 
                  e.stopPropagation();
                  e.stopImmediatePropagation();
                  onClickOpenLink("<?= $typeUrl ?>","<?= $link ?>","<?= $targetBlank ?>","<?= $downloadFile ?>");
              });
            }
        }
        cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')
        str="";
        str+=cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#sectionCss<?= $kunik ?>").append(str);

        cssHelpers.render.otherClassControl(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')

    })
</script>

<<?= $balise ?> 
    class="colorDroppable <?= $aliasState ?>  <?= (isset($blockCms['blockParent']))?"cmsbuilder-block cmsbuilder-block-droppable":"" ?>  <?= $kunik ?>-css cmsbuilder-block-container whole-<?= $kunik ?> spCustomBtn sp-cms-<?= $widthClass ?>  <?= $widthClass ?> <?= $kunik ?> super-cms <?= $otherClass ?> other-css-<?= $kunik ?> text-left <?= $myCmsId ?>" 
    style="order: <?= $order ?>;" 
    data-kunik="<?= $kunik ?>" 
    data-id="<?= $myCmsId ?>"
    data-noteditable ="<?= $blockCms['notEditable'] ?? false ?>" 
    data-blockType="<?= (isset($blockCms['blockParent']))?"column":"" ?>" 
    data-name="<?= (!isset($blockCms['blockParent']))?"": ($name != "" ? $name : "Colonne") ?>" 
    data-bloc-name="Container-<?= $myCmsId ?>"
    data-path="tpls.blockCms.superCms.container"
    data-anchor-target="<?= substr($anchorTarget , 1) ?>"
>

<?php

        if (!empty($blockChildren)) {
          // Short by position
         usort($blockChildren, function($a, $b) {
           if (isset($a['position'], $b['position'])) {
            return $a['position'] - $b['position'];
          }
          return 0;
        });
        foreach ($blockChildren as $key => $value) {
            $pathExplode      = explode('.',@$value["path"]);
          //  $count            = count($pathExplode);
            $superKunik       = $pathExplode[count($pathExplode) - 1] . $value["_id"];
            $blockKey         = (string)$value["_id"];
            $params     = [
                "blockCms"  =>  $value,
                "kunik"     =>  $superKunik,
                "blockKey"  =>  $blockKey,
                "content"   =>  array(),
                "costum"    =>  $costum,
                "page"      =>  @$value["page"],
                "canEdit"   =>   $costum["editMode"],
                "clienturi" =>  @$clienturi,
                "el"        =>  @$el,
                "extraParams" => @$extraParams
            ];
            //SPECIFIQUE FOR BLOCKCMS => ADD DIV CONTAINER TO GET ACTIONS FOR ALL TOOLBARS ON HOVER
            if (strpos(@$value["path"], "superCms.elements.image") == true) { ?>
                <div class="cmsbuilder-block super-cms image-wrapper image-wrapper<?= $superKunik ?>" data-blockType="element" data-kunik="<?= $superKunik ?>" data-id="<?= $blockKey ?>" data-name="image">
                <?php 
            } else if (strpos(@$value["path"], "superCms") == false) { ?>
                <div class="cmsbuilder-block stop-propagation col-md-12 col-xs-12 no-padding sp-elt-<?= $blockKey ?> super-cms" data-blockType="custom" data-kunik="<?= $superKunik ?>" data-id="<?= $blockKey ?>" data-name="<?= isset($value["name"]) ? $value["name"] : (isset($value["blockName"]) ? $value["blockName"] : 'Block cms') ?>">
                <?php 
                // BOUBOULE JUST CLEAN : DONT ASK HIM WHY THAT 
                if(isset($value["blockCmsColorTitle1"]))
                    echo $this->renderPartial("costum.views.tpls.OldGarbageToKeepUntilTheEndCSS", array("value" => $value, "blockKey"=>$blockKey)); 
            }
             
            //GENERATE VIEW OF BLOCK CHILDREN
         
              if (isset($value["path"]) && is_file($this->getViewFile("costum.views." . $value["path"]))) { 
                echo BlockCmsWidget::widget([
                  "path" => $value["path"],
                  "notFoundViewPath" => "costum.views." . $value["path"], 
                  "config" => $params
                ]); 
              } else {
                echo $this->renderPartial("costum.views.tpls.blockNotFound", $params);
              }
            //
            // END OF SPECIFIQUE add Container div FOR BLOCKCMS CHILDREN 
            if (strpos(@$value["path"], "superCms.elements.image") == true)  echo "</div>"; 
            if (strpos(@$value["path"], "superCms") == false) echo "</div>"; ?>

        <!--  Required for old block inside a container -->
        <script>
          if (window.cmsConstructor && typeof cmsConstructor.sp_params["<?= $blockKey ?>"] === "undefined"){
            cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($value) ?>
          }
        </script>
        <a href="javascript:;" style="display:none;" class="edit<?= $superKunik ?>Params" , data-path="<?= @$value["path"] ?>" data-id="<?= $blockKey ?>" data-collection="cms">
        </a>
        <?php }
        ?>
        <?php
    } 
    if ($costum["editMode"] == "true") { ?>
      <div class="empty-section-blocks empty-sp-element<?= $kunik ?> <?= ( (count($blockChildren) > 0))  ? 'd-none' : '' ?> padding-30">
        <?php
        $assetsUrl = Yii::app()->getModule(Costum::MODULE)->getAssetsUrl(); 
        $structuredContainer = "";
        if (!isset($blockCms['blockParent'])) {
          $structuredContainer = "<h4>". Yii::t("cms", "Select your structure")." </h4>";
          // Structure of column (%)
          $arrayStructuredContainer = [
            [100],
            [50, 50],
            [35, 65],
            [65, 35],
            [25, 25, 50],
            [50, 25, 25],
            [25, 50, 25],
            [16.66, 66.68, 16.66],
            [33, 33, 33],
            [25, 25, 25, 25],
            [20, 20, 20, 20, 20],
            [16.66, 16.66, 16.66, 16.66, 16.66, 16.66]
          ];
          $structuredContainer .= '<div class="sp-cms-std" style="width:100%;justify-content: center;">';
          $structuredContainer .= '<div class="sp-cms-std" style="width:80%;">';
          foreach ($arrayStructuredContainer as $keyStrc => $valueStrc) {
            $spStructure = json_encode($valueStrc);
            $structuredContainer .= '<div class="sp-cms-std padding-10" style="width:16%;">';
            $structuredContainer .= '<div data-structure="' . $spStructure . '" data-id="'.$myCmsId.'" class="cursor-hand choose-structure sp-cms-std" style="width:100%;">';
            foreach ($valueStrc as $key => $valueWidth) {
              $structuredContainer .= '<div class="sp-cms-std container-structure" data-width="' . $valueWidth . '" style="width:' . $valueWidth . '%;"></div>';
            }
            $structuredContainer .= '</div>';
            $structuredContainer .= '</div>';
          }
          $structuredContainer .= '</div>';
          $structuredContainer .= '</div>';
        }

        ?>
        <div style="width:100%;justify-content: center;" class="sp-cms-std"> 
          <span class="padding-left-10"><button class="btn btn-empty-column text-white choose-model-cms" style="background-color: #e6344d;" href="#" data-id="<?= $myCmsId ?>"><?php echo Yii::t("cms", "Add a block"); ?></button></span>
          <?php if (!isset($blockCms['blockParent']) && $isDefaultSection) { ?>
            <span class="padding-left-10"><button class="btn-empty-column text-white choose-template" href="#" style="background-color: white;color: #6d7a86 !important; border: solid #6d7a86 1px;" onclick="$(`.lbh-costumizer[data-link='page']`).trigger('click');"><i style="vertical-align: middle;"class="fa fa-star"></i></button></span>
          <?php } ?>
          <p class="padding-top-10 italic" style="width: 100%;justify-content: center;text-align: center;"><?= Yii::t("cms", "Drag your block CMS here") ?>
          (<a class="show-tutorial" style="vertical-align: middle;" href="javascript:;" data-imgsrc="<?= $assetsUrl ?>/cmsBuilder/img/tutorial/insertFromEmptyElt.gif"><i class="fa fa-info-circle"></i> info</a>)</p>
          <?= $structuredContainer ?>
        </div>
      </div>

      <script>
        if ( $(".<?= $kunik ?>").css( "background") != "rgba(0, 0, 0, 0) none repeat scroll 0% 0% / auto padding-box border-box" && $(".<?= $kunik ?>").css( "background") != "none" ){
          $(".empty-sp-element<?= $kunik ?>").addClass("d-none")
        }
      </script>

  <?php } ?>
  <script>
    <?php 
    if(isset($blockCms["animation"]["animateType"]) && $blockCms["animation"]["animateType"] != "") { ?>
      var ratio = .3;
      var options = {
        root: null,
        rootMargin: '0px',
        threshold: ratio
      }
      var cmsHandleIntersect = function(entries, observer) {
        entries.forEach(function (entry){
          if(entry.intersectionRatio > ratio){
              <?php if(isset($blockCms["animation"]["repeatAnimate"]) && $blockCms["animation"]["repeatAnimate"]) { ?>
                  entry.target.classList.remove("visible-<?= $kunik ?>-css");
                  void entry.target.offsetWidth;
                  entry.target.classList.add("visible-<?= $kunik ?>-css");
              <?php } else { ?>
                entry.target.classList.add("visible-<?= $kunik ?>-css");
                observer.unobserve(entry.target);
              <?php } ?>
          }
        });
      }
      var cmsObserve = new IntersectionObserver(cmsHandleIntersect, options);
      // window.addEventListener("load", function() {
        cmsObserve.observe(document.querySelector(".<?= $kunik ?>-css"));
      // });
    <?php } ?>

    <?php if ($aliasState != "" && $costum["editMode"] == "true") { ?>
      if (!$(".<?= $aliasState ?> > .cmsbuilder-block-container").hasClass("cmsbuilder-block")) {
        $(".<?= $aliasState ?> > .cmsbuilder-block-container").addClass("cmsbuilder-block")
        $(".<?= $aliasState ?> > .cmsbuilder-block-container").removeClass("cmsbuilder-block-droppable")
      }
      $(".<?= $aliasState ?> > .cmsbuilder-block").find(".cmsbuilder-block").removeClass("cmsbuilder-block").removeClass("cmsbuilder-block-droppable")
      // $(".<?= $aliasState ?> > .cmsbuilder-block").attr("data-name","Alias depuis "+alias.parent[Object.keys(alias.parent)[0]].name+", "+ alias.page)
    <?php } ?>

  </script>
</<?= $balise ?>>


