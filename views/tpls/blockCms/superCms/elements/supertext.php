<?php 
/* 
Super text:
Created by Ifaliana Arimanana, Gerson Elvestino
ifaomega@gmail.com, mgelvestino@gmail.com
26 Apr 2021
*/
$keyTpl           = "text";
$paramsData       = [
  "text" => array($costum["langCostumActive"] => '<div style="text-align: justify;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>'),
  "fontSize" => "20",
  "markdownText" => array($costum["langCostumActive"] => ""),
  "isMarkdown" => (isset($blockCms["isMarkdown"]) ? $blockCms["isMarkdown"] : false)
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (!isset($blockCms[$e]) ) {
      $blockCms[$e] = $paramsData[$e];
    }
  }
}

$kunik            = preg_replace('/super/i', "", $kunik);
$html             = $blockCms["text"] ??  "<div><span>".Yii::t("cms", "Write a text")."....<span></div>";
$myCmsId          = $blockCms["_id"]->{'$id'};
$subtype          = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$name             = $blockCms["name"] ?? "nom du block";
$array_position   = $blockCms['position'] ?? $blockCms['position'] ?? [];
$blockParent      = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";
$params           = array();

$styleCss         = (object) [ "cmsbuilder-block .".$kunik."-css" => $blockCms["css"] ?? [] ];

$order            = $blockCms["order"] ??  "0";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm       = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs       = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$markdownAlias    = [];
$name             = $blockCms["name"] ?? "";

if (isset($blockCms["markdownAlias"])) {
  $markdownAlias = $blockCms["markdownAlias"];
}else if(isset($costum["app"]["#".$page]["note"])){
  $markdownAlias = ["noteId" => $costum["app"]["#".$page]["note"]["noteId"]];
  $blockCms["markdownAlias"] = ["noteId" => $costum["app"]["#".$page]["note"]["noteId"]];
  $blockCms["isMarkdown"] = true;
}

?>

<style type="text/css" id="textCss-<?= $myCmsId ?>">
  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
</style>
<script type="text/javascript" id="textCss<?= $kunik ?>">
$(function(){
  if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
  }
  
  cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')

  str="";
  str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#textCss-<?= $myCmsId ?>").append(str);
  
  var costumLanguage = <?= json_encode($costum["langCostumActive"]) ?>,
      textLang = costumLanguage,
      text = <?= json_encode($blockCms["text"]) ?>,
      markdownText = <?= json_encode($blockCms["markdownText"]) ?>,
      isMarkdown = <?= json_encode($blockCms["isMarkdown"]) ?>,
      markdownAlias = <?= json_encode($markdownAlias) ?>; 


  if (notEmpty(costum.app["#"+page]) && typeof costum.app["#"+page].note != "undefined" && (!notEmpty(markdownAlias) || text == <?= json_encode($paramsData["text"][array_keys($paramsData["text"])[0]]) ?>)) {
    markdownAlias = { noteId: costum["app"]["#"+page]["note"]["noteId"]};
    isMarkdown = true;
  }

  if (markdownText.hasOwnProperty(costumLanguage)) {
    markdownTextToShow = markdownText[costumLanguage];
  } else {
    markdownTextToShow = markdownText[Object.keys(markdownText)[0]];
  }
  
  if (notEmpty(markdownAlias)) {  
    mylog.log("markdownAlias",markdownAlias);
    $(".markdown-container[data-id='<?= $myCmsId ?>'").html(strLoading)
    ajaxPost(
      null,
      baseUrl+"/"+moduleId+"/cms/notemarkdown",
      {
        id  : markdownAlias.noteId
      },
      function(data){
        mylog.log("notemarkdown <?= $myCmsId ?>",data)
        $(".markdown-container[data-id='<?= $myCmsId ?>'").html(dataHelper.markdownToHtml(data.data.doc))
      },
      null

      );
  }

  if (isMarkdown) {
    if (!costum.editMode) {
      // $(".sp-markdown-<?= $myCmsId ?>").html(markdownTextToShow);
      $(".sp-markdown-<?= $myCmsId ?>").html(dataHelper.markdownToHtml(markdownTextToShow))
    }else{
      $(".sp-markdown-<?= $myCmsId ?>").html(dataHelper.markdownToHtml(markdownTextToShow))
      dataHelper.activateMarkdown('.sp-markdown-<?= $myCmsId ?>')
    }
    $(".markdown-container[data-id='<?= $myCmsId ?>']").removeClass("hidden")
    $("#sp-<?= $myCmsId ?>").addClass("hidden")
  }else{    
    appendTextLangBased(".sp-text-<?= $myCmsId ?>",costumLanguage,text,"<?= $myCmsId ?>");
  }

});

</script>

<div class="cmsbuilder-block super-cms default-text-params colorDroppable  <?= $kunik ?> other-css-<?= $kunik ?> <?= $kunik ?>-css <?= $otherClass ?> " data-blockType="text" data-kunik="<?= $kunik ?>" data-name="text" data-id="<?= $myCmsId ?>">
<div lang="de" id="sp-<?= $myCmsId ?>" style="cursor: auto;hyphens: auto;" class="editable sp-text colorDroppable sp-text-<?= $myCmsId ?>" data-id="<?= $myCmsId ?>" data-field="text" data-name="text" data-kunik="<?= $kunik ?>"></div>
  <div class="markdown-container hidden" data-id="<?= $myCmsId ?>">    
    <div class="sp-markdown-<?= $myCmsId ?> markdown"></div>
  </div>
</div>
<script type="text/javascript">
  
</script>