<?php 
/* 
Super container:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/


/***************** Required *****************/
$keyTpl ="container";
$myCmsId = $blockCms["_id"]->{'$id'};
$params = array();
/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

$latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
if($latestImg == "empty"){
  $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
}
/*****************end get image uploaded***************/

/* Get settings */

$array_position   = $blockCms['position'] ?? $blockCms['position'] ?? [];
$widthClass       = $blockCms["class"]["width"] ?? $blockCms["class"]["width"] ?? "col-md-4";
$borderColor      = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$borderRadius     = $blockCms["css"]["border"]["radius"] ?? $blockCms["css"]["border"]["radius"] ?? "";
$paddingGeneral   = $blockCms["css"]["padding"]["general"] ?? $blockCms["css"]["padding"]["general"] ?? "20";
$backgroundColor  = $blockCms["css"]["background"]["color"] ?? $blockCms["css"]["background"]["color"] ?? "transparent";

/* End get settings */

?>
<style type="text/css">
  .content-<?= $myCmsId ?> {  
    border: dashed 2px gray;
    background-color: <?= $backgroundColor ?>;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px;
    <?php } ?>
  }

  .view-mode-<?= $myCmsId ?> {
    border: solid <?= $borderColor ?> !important;
    background-color: <?= $backgroundColor ?> !important;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px !important;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px !important;
    <?php } ?>
  }

  .selected-mode-<?= $myCmsId ?> {
    border: solid 1px lightblue;
    box-shadow: 0 6px 12px lightblue;
    background-color: <?= $backgroundColor ?>;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px;
    <?php } ?>
  }

  #toolsBar {
    left: 40%;
    top: 40%;
  }


</style>
  <div class="whole-content-<?= $myCmsId ?> <?= $widthClass ?> <?= $myCmsId ?>">
    <div class="container-fluid content-<?= $myCmsId ?> this-content-<?= $myCmsId ?> super-cms" style="min-height: 50px;position: relative; <?php foreach ($array_position as $key => $value) {
      echo $key." : ".$value." !important; "; } ?>">
      <div class="stop-propagation">
        <div class="row">
          <?php
          $idElem = $childForm = PHDB::find("cms", array("type" => "blockChild" , "tplParent" => $myCmsId ));
          if (!empty($idElem)) {
            foreach ($idElem as $key => $value) {
              $pathExplode = explode('.', $value["path"]);
              $count = count($pathExplode);
              $superKunik = $pathExplode[$count-1].$value["_id"];
              $blockKey = (string)$value["_id"];
              $cmsElement = $value["path"];       
              $params = [
                "blockCms"  =>  $value,
                "kunik"     =>  $superKunik
              ];
            echo $this->renderPartial("costum.views.".$cmsElement,$params); 
            if(Authorisation::isInterfaceAdmin()){?>
              <div class="hiddenPreview" style="position: absolute;right: 10px">
                <?php  if ($value["subtype"] != "supercms") { ?>
                    <div class="text-center btn-edit-delete-<?= @$superKunik?> edit-inside-sp-block hiddenPreview col-md-12" >
                      <a class="tooltips edit<?php echo @$superKunik?>Params padding-5" data-path="<?= @$path ?>" data-id="<?= @$blockKey; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Modifier ce <?= @$name?>">
                        <i class="fa fa-edit" aria-hidden="true"></i>
                      </a>

                      <a style="color: #f44336;" class="deleteLine tooltips padding-5" data-path="<?= @$path ?>" data-id="<?= @$blockKey; ?>" data-collection="cms" type="button" data-parent="<?= @$parentId ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer ce <?= @$name?>">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                      </a>
                    </div>
                    <script type="text/javascript">
                      cmsBuilder.manage.delete()
                      $(document).ready(function() {
                        let isEmpty = false;
                        if (localStorage.getItem("previewMode") == "w") {  
                         previewMode = false;
                       }else{    
                        $(hideOnPreview).hide();
                      }
                        if (typeof sectionDyf.<?php echo @$superKunik?>Params !== "undefined"){
                                 isEmpty = Object.keys(sectionDyf.<?php echo @$superKunik?>Params.jsonSchema.properties).length === 0;
                              }

                        if (isEmpty) {
                          $(".edit<?php echo @$superKunik?>Params").off().on('mousedown', function(e){
                            e.stopImmediatePropagationt()
                            $("#toolsBar").hide();  
                            $("#toolsBar").html("");
                            toastr.info("Aucun paramètre à modifier!")
                          });
                        }else{
                          $(".edit<?php echo $superKunik?>Params").mousedown(function(e) {  
                            e.stopImmediatePropagationt()
                            $("#toolsBar").hide();  
                            $("#toolsBar").html("");
                            let path = '<?= @$path ?>';
                            let blockName = ""
                            path = path.replace(/./g, '');    
                            if(typeof callByName[path.replace(/./g, '')] !== 'undefined')
                             blockName = callByName[path.replace(/./g, '')] 
                           sectionDyf.<?php echo $superKunik?>Params.jsonSchema.title = "<i class='fa fa-info-circle cursor-pointer' title='<?= @$path ?>' style='color: #00bcd4;'></i> <?php echo Yii::t('cms', 'Set up your section')?> "+ blockName;
                         });
                        }
                      });
                    </script>
            <?php } ?>
              </div>
            
          <?php }

        }else{ ?>  
          <div class="hiddenPreview" style="
          height: 25vh;
          top: 50%;
          margin: 2px 0 0;
          font-size: 35px;
          color: slategrey;
          text-align: center;
          ">
          <h3 style="font-weight: normal;">Aucun element</h3>
          </div><?php } ?>  
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">

</script>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque faucibus dolor sit amet eros elementum rutrum. Sed eget vulputate libero. Donec eleifend, ante in varius pharetra, eros nunc blandit massa, in sodales massa neque non massa. Pellentesque vehicula tincidunt mauris a mattis. Suspendisse tincidunt vehicula lorem ut mattis. Suspendisse rhoncus congue tincidunt. Sed feugiat venenatis dolor, sed ultrices nisi imperdiet sit amet. Vivamus lacinia massa odio, quis sollicitudin augue ultrices quis. Quisque non finibus nisi, at efficitur lorem. Duis ut velit sit amet ligula commodo egestas nec eget tellus.
