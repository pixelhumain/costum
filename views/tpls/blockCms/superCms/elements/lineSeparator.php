<?php
/* 
Super line separator:
Created by Ifaliana Arimanana
Edited by Sitraka Philippe
ifaomega@gmail.com
10 juin 2022
*/

/***************** Required *****************/

// if ($blockCms["type"] !== "blockCopy" && empty($blockCms["cmsParent"])) {
//   $blockCms["cmsParent"] = @$blockCms["tplParent"];
// } elseif ($blockCms["type"] !== "blockCopy" && empty($blockCms["tplParent"])) {
//   $blockCms["tplParent"] = $blockCms["cmsParent"];
// }

$keyTpl     = "separator";
$myCmsId    = $blockCms["_id"]->{'$id'};
//$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = array();
$blockParent = $blockCms["cmsParent"] ?? $blockCms["cmsParent"] ?? "";



if (isset($blockCms)) {

}
/*************** End required ***************/

/* Get settings */
$name             = $blockCms["name"] ?? "";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$blockCms["icon"]["display"] = true ; 

if (isset($blockCms["css"]["icon"]["display"]) && $blockCms["css"]["icon"]["display"] == "none"){
  $blockCms["icon"]["display"] = false ;
}else {
  $blockCms["icon"]["display"] = true ;
}
//////////iciiiiiiii/////////
$styleCss = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];


/********Line separator********/
  $LineSeparatorTop =  $blockCms["positionTop"] ?? "false";
  $LineSeparatorIcon =  $blockCms["icon"]["iconStyle"] ?? "fa fa-angle-down";
/* End get settings */

?>

<style type="text/css" id="textCss<?= $kunik ?>">

  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  

  @media (max-width: 800px) {
    .<?= $kunik ?> {
      width: 100% !important;
      background-size: contain;
      padding-left: 0px;
      padding-right: 0px;
      margin-left: 0px;
      margin-right: 0px;
    }
  }

  .bottom-lineSeparator<?= $kunik ?> {
    bottom: 0 !important;
  }

  .top-lineSeparator<?= $kunik ?> {
    top: 0 !important;
  }

  .icon-visibility<?= $kunik ?> {
    display: none !important;
  }
</style>

<script>
  $(function(){

    if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
    } 

    cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')
    
    str="";
    str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#textCss<?= $kunik ?>").append(str);
  });
</script>
  
  <div class="bottom-lineSeparator<?= $kunik ?>" id="wrapperforLineSeparator<?= $kunik ?>" style="width:100%; position:absolute ;">
       <div class="superLineSeparator cmsbuilder-block super-cms <?= $kunik ?> <?= $kunik ?>-css other-css-<?= $kunik ?> <?= $otherClass ?> " data-blockType="element"data-kunik="<?= $kunik ?>"data-id="<?= $myCmsId ?>" data-name="separator"> 
              <i class=" <?= $LineSeparatorIcon ?>  <?= $kunik ?>-icon icon" ></i>
       </div>
</div>

<script>
   /****************Position*/
   if( "<?= $LineSeparatorTop ?>" == true ){
    $("#wrapperforLineSeparator<?= $kunik ?>").removeClass("bottom-lineSeparator<?= $kunik ?>");  
    $("#wrapperforLineSeparator<?= $kunik ?>").addClass("top-lineSeparator<?= $kunik ?>");  
  }
</script>