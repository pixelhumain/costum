<?php
/* 
Super image:
Created by Ifaliana Arimanana
Edited by Sitraka Philippe, Gerson Elvestino
ifaomega@gmail.com
26 Apr 2021
*/

/***************** Required *****************/
$keyTpl           = "image";
$myCmsId          = $blockCms["_id"]->{'$id'};
$kunik            = $keyTpl . $myCmsId;
$subtype          = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$blockParent      = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCssString   = "";

if (is_array($otherCss)) {
  foreach ($otherCss as $csskey => $cssvalue) {
    $otherCssString .= $csskey.":".$cssvalue.";\r\n";
  }
  $otherCss = $otherCssString;
}

$name             = $blockCms["name"] ?? "";
$styleCss         = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];
if( !isset($blockCms["image"])){
  $blockCms["image"] = array($costum["langCostumActive"] => isset($costum["logo"]) ? $costum["logo"] : Yii::app()->getModule("co2")->assetsUrl . '/images/thumbnail-default.jpg');
}
$image = $blockCms['image']; 
$typeUrl =  $blockCms["advanced"]["typeUrl"] ??  "internalLink";
$link =  $blockCms["advanced"]["link"] ??  "";
$targetBlank = $blockCms["advanced"]["targetBlank"] ?? false ;
$downloadFile = $blockCms["advanced"]["downloadFile"] ?? false ;

/* End get settings */
?>

<style type="text/css" id="imageCss<?= $kunik ?>">
  @media (max-width: 800px) {
    .<?= $kunik ?> {
      padding: 0px;
      margin: 0px;
      left: 50% !important;
    }
  }

  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  
</style>

<?php 
  if ($costum["editMode"] == "true" && isset($costum["useOpenAI"])) { ?>
    <div class="content-loader-on-generateImage <?= $kunik ?>-loader"></div>
<?php  
  }
?>
<img class="<?= $kunik ?> <?= $kunik ?>-css  <?= $otherClass ?> other-css-<?= $kunik ?> this-content-<?= $kunik ?> unselectable" data-kunik="<?= $kunik ?>"  src="" style="order: <?= $order ?> ;"/>

<script>

$(function(){
  if (costum.editMode){
    cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
  } else {
      if ( "<?= $link ?>" !== "" ){
        $(".this-content-<?= $kunik ?>").css("cursor", "pointer");
        $(".this-content-<?= $kunik ?>").off("click").on("click", function(e){ 
          e.stopImmediatePropagation();
          onClickOpenLink("<?= $typeUrl ?>","<?= $link ?>","<?= $targetBlank ?>","<?= $downloadFile ?>");
        });
      }
  }

  cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? [ ]) ?>,'<?= $kunik ?>')

  str="";
  str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#imageCss<?= $kunik ?>").append(str);

  var imageToShow = "",
      costumLangActive = <?= json_encode($costum["langCostumActive"] ?? "fr") ?>,
      image = <?= json_encode($image) ?>;

  if (image.hasOwnProperty(costumLangActive)) {
    imageToShow = image[costumLangActive];
  } else {
    imageToShow = image[Object.keys(image)[0]];
  }
  $(".this-content-<?= $kunik ?>").attr("src", imageToShow);
});
  
</script>