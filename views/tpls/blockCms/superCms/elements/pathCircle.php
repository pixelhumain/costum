<?php
$keyTpl     = "pathcircle";
$paramsData = [
    "width" => "700",
    "height" => "200",
    "circleSize" => "30",
    "handleHeadSize" => "10",
    "circleColor" => "#cdeae3",
    "textColor" => "rgb(55, 116, 121)",
    "data" => "",
    "pathColor" => "rgb(55, 116, 121)"
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<svg id="draw">

</svg>

<script src="./d3.v6.min.js"></script>

<script>
    var line = d3.line().curve(d3.curveCardinal.tension(-0.8));
    var width = <?= $paramsData['width'] ?>;
    var height = <?= $paramsData['height'] ?>;
    var circleSize = <?= $paramsData['circleSize'] ?>;
    var handleSize = circleSize;
    var handleHeadSize = <?= $paramsData['handleHeadSize'] ?>;
    var canDrag = false;

    var circleColor = "<?= $paramsData['circleColor'] ?>";
    var pathColor = "<?= $paramsData['pathColor'] ?>";
    var textColor = "<?= $paramsData['textColor'] ?>";

    var data = [
    {
        "x": 186,
        "y": 0,
        "r": 0.1
    },
    {
        "x": 232,
        "y": 48.769584192780485,
        "r": 35,
        "text": "UNE FORMATION AU SERVICE DE VOTRE PROJET MANAGÉRIALE .."
    },
    {
        "x": 374.5,
        "y": 36.92537324801134,
        "r": 35,
        "text": "ADAPTÉE À VOTRE CONTEXTE ..."
    },
    {
        "x": 463,
        "y": 86.58347743106381,
        "r": 35,
        "text": "UN SUIVI PERSONNALISÉ"
    },
    {
        "x": 386.5,
        "y": 152.08347743106378,
        "r": 35,
        "text": "DES ATELIERS COLLECTIFS ET DU COACHING INDIVIDUEL "
    },
    {
        "x": 273.5,
        "y": 140.08347743106378,
        "r": 35,
        "text": "80% TRAINING 20% THÉORIE  "
    },
    {
        "x": 252,
        "y": 212.06762636626468,
        "r": 0.1
    }
];

    var selected = data[0];
    const svg = d3.select("svg#draw")
    .attr("viewBox", [-14, 0, width + 28, height])
    .attr("tabindex", 1)
    .attr("pointer-events", "all")
    .call(d3.drag()
        .subject(dragsubject)
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", d => {
            console.log("VITA")
        }),
        );

    svg.append("style").text(`
        svg[tabindex] {
            display: block;
            margin: 0 -14px;
            border: solid 2px transparent;
            box-sizing: border-box;
        }
        svg[tabindex]:focus {
            outline: none;
            border: solid 2px lightblue;
        }
        `);

    svg.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", pathColor)
    .attr("stroke-dasharray","6,4")
    .attr("stroke-width", 2)
    .call(update);

  // d3.select(window).on("keydown", keydown);

  function update() {
    svg.select("path").attr("d", d => line(d.map(x => [x.x,x.y])));
    const circle = svg.selectAll("g")
    .data(data, d => d)

    circle.enter().append("g")
    .call(g => { g.append("circle")
        .attr("r", (d,i) => i != 0 && i != data.length - 1 ? handleSize : handleHeadSize)
        .attr("fill", "none")
        .style("box-shadow", "rgb(0 0 0 / 20%) 10px 8px 20px 0px")
    })
    .call(g => {
        g.append("circle")
        .attr("fill", circleColor)
        .attr("r", (d,i) => d.r = i != 0 && i != data.length - 1 ? circleSize : 0.1)
        g
        .filter((d,i) => i != 0 && i != data.length - 1)
        .append("foreignObject")
        .attr("transform", `translate(-${(Math.cos(Math.PI / 4) * circleSize)},-${(Math.cos(Math.PI / 4) * circleSize)})`)
        .attr("width", (Math.cos(Math.PI / 4) * circleSize) * 2)
        .attr("height", (Math.cos(Math.PI / 4) * circleSize) * 2)
        .append("xhtml:div")
        .style("width", "100%")
        .style("height", "100%")
        .style("display", "flex")
        .style("justify-content", "center")
        .style("align-items", "center")
        .style("font-size", "5px")
        .style("color", textColor)
        .text(d => d.text)
    })
    .merge(circle)
    .attr("transform", d => `translate(${d.x}, ${d.y})`)
    .select("circle:last-child")
    .attr("fill", d => d === selected ? "lightblue" : "yellow");

    circle.exit().remove();
}

function dragsubject(event) {
    let subject = event.sourceEvent.target.__data__;
//    if (!subject) {
//      data.push(subject = {x : event.x, y: event.y});
//      data = data;
//      update();
//    }
return subject;
}

function dragstarted({subject}) {
    if(canDrag){
        selected = subject;
        update();
    }
}

function dragged(event) {
    if(canDrag){
        var i = data.indexOf(event.subject);
        event.subject.x = Math.max(-14, Math.min(width + 14, i != 0 && i != data.length - 1 ? event.x - circleSize / 2 : event.x));
        event.subject.y = Math.max(0, Math.min(height, i != 0 && i != data.length - 1 ? event.y - circleSize / 2 : event.y));
        data = data;
        update();
    }
}

//  function keydown(event) {
//    if (!selected) return;
//    switch (event.key) {
//      case "Backspace":
//      case "Delete": {
//        event.preventDefault();
//        const i = data.indexOf(selected);
//        data.splice(i, 1);
//        data = data;
//        selected = data.length ? data[i > 0 ? i - 1 : 0] : null;
//        update();
//        break;
//      }
//    }
//  }
<?php if(Authorisation::isInterfaceAdmin()){?>
    canDrag = true;
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
  /*  
    "width" => "700",
    "height" => "200",
    "circleSize" => "30",
    "handleHeadSize" => "10",
    "circleColor" => "#cdeae3",
    "textColor" => "rgb(55, 116, 121)",
    "data" => "",
    "pathColor" => "rgb(55, 116, 121)"
*/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            "properties" : {            
            "width" : {
                "label" : "Largeur",
                "inputType": "quantity",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.width
            },
            "height" : {
                "label" : "Hauteur",
                "inputType": "quantity",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.height
            },           
            "circleSize" : {
                "label" : "Taille du cercle",
                "inputType": "quantity",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.circleSize
            },                  
            "handleHeadSize" : {
                "label" : "handle Head Size",
                "inputType": "quantity",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.handleHeadSize
            },               
            "circleColor" : {
                "label" : "Couleur des cercles",
                "inputType": "colorpicker",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.circleColor
            },   

            "textColor" : {
                "label" : "Couleur des textes",
                "inputType": "colorpicker",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.textColor
            },
            "pathColor" : {
                "label" : "Couleur du ligne",
                "inputType": "colorpicker",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.pathColor
            },            
            "data" : {
                "label" : "Data",
                "inputType": "textarea",
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.data
            }
},
beforeBuild : function(){
  uploadObj.set("cms","<?php echo $blockKey ?>");
},
save : function () {
  tplCtx.value = {};
  $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
    tplCtx.value[k] = $("#"+k).val();
    if (k == "parent") {
      tplCtx.value[k] = formData.parent;
  }
  if (k == "dataSource") {
      tplCtx.value[k] = formData.dataSource;
  }
});
  mylog.log("save tplCtx",tplCtx);

  if(typeof tplCtx.value == "undefined")
    toastr.error('value cannot be empty!');
else {
  dataHelper.path2Value( tplCtx, function(params) {
    dyFObj.commonAfterSave(params,function(){
      toastr.success("Élément bien ajouté");
      $("#ajax-modal").modal('hide');
      var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
      var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
      var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
      cmsBuilder.block.loadIntoPage(id, page, path, kunik);
    //   urlCtrl.loadByHash(location.hash);
  });
} );
}

}
}
};
mylog.log("sectiondyfff",sectionDyf);
$(".edit<?php echo $kunik ?>Params").off().on("click",function() {
  tplCtx.id = $(this).data("id");
  tplCtx.collection = $(this).data("collection");
  tplCtx.path = "allToRoot";
  dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
  setTimeout(() => {
    onload();
    $("select[name='graphType']").change(onload);
},1200)
});

});
<?php }?>
</script>