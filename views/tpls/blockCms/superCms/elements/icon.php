<?php 
/***************** Required *****************/

// if ($blockCms["type"] !== "blockCopy" && empty($blockCms["cmsParent"])) {
//   $blockCms["cmsParent"] = @$blockCms["tplParent"];
// }elseif ($blockCms["type"] !== "blockCopy" && empty($blockCms["tplParent"])) {
//   $blockCms["tplParent"] = $blockCms["cmsParent"];
// }

$keyTpl     ="icon";
$myCmsId    = $blockCms["_id"]->{'$id'};
$params     = array();
$paramsData = array();

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

/* Get settings */
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$Icon             =  $blockCms["iconStyle"] ?? "fa fa-angle-down";
$objectCss        = $blockCms["css"] ?? [];
$styleCss = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];
$typeUrl =  $blockCms["advanced"]["typeUrl"] ??  "internalLink";
$link =  $blockCms["advanced"]["link"] ??  "";
$targetBlank = $blockCms["advanced"]["targetBlank"] ?? false ;
$downloadFile = $blockCms["advanced"]["downloadFile"] ?? false ;

/********Icon********/

?>
<style type="text/css" id="icon<?= $kunik ?>">

  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  
</style>

<script type="text/javascript">
    $(function(){

      if (costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
      } else {

        if ( "<?= $link ?>" !== "" ){
          $(".<?= $kunik ?>").css("cursor", "pointer");
          $(".<?= $kunik ?>").off("click").on("click", function(e){ 
            e.stopImmediatePropagation();
            onClickOpenLink("<?= $typeUrl ?>","<?= $link ?>","<?= $targetBlank ?>","<?= $downloadFile ?>");
          });
        }
        
      }

      cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["css"] ?? []) ?>,'<?= $kunik ?>')

      str="";
      str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
      $("#icon<?= $kunik ?>").append(str);

    })
</script>

<i id="icon-<?= $kunik ?>" class="colorDroppable superIcon <?= $kunik ?> cmsbuilder-block super-cms <?= $Icon ?> icon-<?= $kunik ?> <?= $kunik ?>-css" 
    data-blockType="element"data-kunik="<?= $kunik ?>"data-id="<?= $myCmsId ?>"data-name="icon">
</i>