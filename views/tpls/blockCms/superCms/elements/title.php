<?php

/***************** Required *****************/
$myCmsId          = (string)$blockCms["_id"];
$kunik            = "title".$myCmsId;
$balise           = $blockCms["balise"] ??  "h1";
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$styleCss         = (object) ["cmsbuilder-block .".$kunik."-css" => $blockCms["css"] ?? [] ];

if( !isset($blockCms["title"])){
  $blockCms["title"] = array($costum["langCostumActive"] => "Votre nouveau titre");
}

$title = $blockCms["title"];
/* End get settings */
?>

<style type="text/css" id="textCss<?= $kunik ?>">
  .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
  @media (min-width : 768px) and (max-width: 991px){
    .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
  }

  @media screen and (max-width: 767px) {
    .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
  }
  
</style>

<?php echo 
  "<".$balise." class='".$kunik." ".$kunik."-css title-".$myCmsId." spTitleBlock cmsbuilder-block super-cms other-css-".$kunik." ".$otherClass."' data-kunik='".$kunik."' data-id='".$myCmsId."' data-name='".Yii::t("common","title")."'>
    <span class='sp-text text-center colorDroppable sp-text-".$blockKey."' data-id='".$myCmsId."' data-field='title' style='display: block!important;' ></span>
  </".$balise.">"; 
?>

<script>
  $(function(){
      if (costum.editMode){
          cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>;
      } 
      cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')
      str="";
      str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
      $("#textCss<?= $kunik ?>").append(str);
      appendTextLangBased(".sp-text-<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");

  });
</script>
