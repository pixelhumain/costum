<?php

$myCmsId          = (string)$blockCms["_id"];
$kunik            = "video".$myCmsId;
$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherCssSm      = $blockCms["advanced"]["sm"]["otherCss"] ??  "";
$otherCssXs         = $blockCms["advanced"]["xs"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["classes"] ??  "";
$styleCss         = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];
$autoplay         = $blockCms["autoplay"] ?? "false";
$repeat           = $blockCms["repeat"] ?? "false";

if( !isset($blockCms["link"])){
    $blockCms["link"] = array($costum["langCostumActive"] => "https://player.vimeo.com/video/133636468");
    // var_dump($blockCms);die();
}
$link = $blockCms["link"]


?>
<style type="text/css" id="video<?= $kunik ?>">
   .other-css-<?= $kunik ?> {  <?php echo $otherCss;?> }
  
    @media (min-width : 768px) and (max-width: 991px){
        .other-css-<?= $kunik ?> {  <?php echo $otherCssSm;?> }
    }

    @media screen and (max-width: 767px) {
        .other-css-<?= $kunik ?> {  <?php echo $otherCssXs;?> }
    }
  
    @media (max-width: 978px) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 350px;
        }
    }
    @media (min-width: 767px) and (max-width: 992px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 235px;
        }
    }
    @media (min-width: 993px) and (max-width: 1200px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 280px;
        }
    }

    @media (min-width: 1201px) and (max-width: 1664px ) {
        .<?= $kunik?> iframe{
            width: 100%;
            height: 350px;
        }
    }
</style>

<div class="cmsbuilder-block super-cms" data-blockType="element" data-kunik="<?= $kunik ?>" data-name="video" data-id="<?= $myCmsId ?>" style="width:100%; height:max-content">
    <iframe class="<?= $kunik?> <?= $kunik ?>-css video-<?= $kunik ?> bs other-css-<?= $kunik ?> <?= $otherClass ?>"  src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<script type="text/javascript">
    $(function(){
        if (costum.editMode){
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?> ;
        }

        cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')
        str="";
        str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#video<?= $kunik ?>").append(str);

        var linkToShow = "",
            costumLanguage = <?= json_encode($costum["langCostumActive"]) ?>,
            link = <?= json_encode($link) ?>;

        if (link.hasOwnProperty(costumLanguage)) {
            linkToShow = link[costumLanguage];
        } else {
            linkToShow = link[Object.keys(link)[0]];
        }
        // traitement du lien
        var autoplay = <?= json_encode($autoplay) ?>,
            repeat = <?= json_encode($repeat) ?>,
            srcVideo = "",
            videoId = "",
            autoplayVideo = "",
            repeatVideo = "";

        if (linkToShow.includes("=") == true) {
            videoId = linkToShow.split("=").pop();
        } else {
            videoId = linkToShow.split("/").pop();
        }

        if (repeat == "true")
            repeatVideo = "playlist="+videoId+"&loop=1&";
        if (autoplay == "true")
            autoplayVideo = "autoplay=1&mute=1&";

        if (linkToShow.includes("vimeo") == true) {
            srcVideo = "https://player.vimeo.com/video/"+videoId;
        } else if (linkToShow.includes("youtu") == true) {
            srcVideo = "https://www.youtube.com/embed/"+ videoId + "?" + autoplayVideo + repeatVideo;
        } else if (linkToShow.includes("dailymotion") == true) {
            srcVideo = "https://www.dailymotion.com/embed/video/"+videoId;
        } else if (linkToShow.includes("indymotion") == true) {
            srcVideo = "https://indymotion.fr/videos/embed/w/"+videoId;
        }

        $(".video-<?= $kunik ?>").attr("src",srcVideo);
    })

</script>