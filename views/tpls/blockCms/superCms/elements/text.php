<?php 
/* 
Super text:
Created by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/
$keyTpl ="text";
$paramsData = [
  "title"=>"",
  "text" => array("default" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."),
  "fontSize" => "20"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}
$kunik          = preg_replace('/super/i', "", $kunik);
$html           = $blockCms["text"] ??  "<div><span>".Yii::t("cms", "Write a text")."....<span></div>";
$myCmsId        = $blockCms["_id"]->{'$id'};
$subtype        = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$name           = $blockCms["name"] ?? "nom du block";
$array_position = $blockCms['position'] ?? $blockCms['position'] ?? [];
$blockParent    = $blockCms["blockParent"] ?? $blockCms["blockParent"] ?? "";
$params         = array();

  /* Get settings */
$otherClass       = $blockCms["advanced"]["otherClass"] ?? $blockCms["class"]["other"] ?? "";
$otherCss         = $blockCms["advanced"]["otherCss"] ?? $blockCms["css"]["other"] ?? [];
$order            = $blockCms["order"] ?? $blockCms["order"] ?? "0";

$styleCss = (object) [ $kunik."-css" => $blockCms["css"] ?? [] ];


$order            = $blockCms["order"] ??  "0";
$otherCss         = $blockCms["advanced"]["otherCss"] ??  "";
$otherClass       = $blockCms["advanced"]["otherClass"] ??  "";
$otherCssString = "";
  if (is_array($otherCss)) {
    foreach ($otherCss as $csskey => $cssvalue) {
      $otherCssString .= $csskey.":".$cssvalue.";\r\n";
    }
    $otherCss = $otherCssString;
  }


$name             = $blockCms["name"] ?? "";
$styleCss         = (object) ['css' => $blockCms["css"] ?? [] ];
?>

<style type="text/css" id="textCss-<?= $myCmsId ?>">

.other-css-<?= $kunik ?> { 
  <?= $otherCss ?>
}  
</style>
<script type="text/javascript" id="textCss<?= $kunik ?>">
$(function(){
  if (costum.editMode){
      cmsConstructor.sp_params["<?= $myCmsId ?>"] =  <?= json_encode($blockCms) ?>
  } 

  cssHelpers.render.addClassDomByPath(<?= json_encode($blockCms["advanced"] ?? []) ?>,'<?= $kunik ?>')

  str="";
  str+= cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
  $("#textCss-<?= $myCmsId ?>").append(str);
  textWithLang = "";
  spLang = ""
  text = <?= json_encode($paramsData["text"]) ?>;

  if ( isUserConnected == "logged" && typeof userConnected.language != "undefined" && text.hasOwnProperty(userConnected.language)) {
    textWithLang = text[userConnected.language]
    spLang = userConnected.language

  }else if(text.hasOwnProperty(getCookie("lang"))){
    textWithLang = text[getCookie("lang")]    
    spLang = getCookie("lang")
  }else{    
    textWithLang = text[Object.keys(text)[0]];
    spLang = Object.keys(text)[0] 
  }
  // console.log("Mer if <?= $myCmsId ?>", text[Object.keys(text)[0]])
  $("#sp-<?= $myCmsId ?>").html(textWithLang)
  $("#sp-<?= $myCmsId ?>").attr("data-lang", spLang);

});

</script>

<div class="cmsbuilder-block super-cms default-text-params colorDroppable <?= $kunik ?> other-css-<?= $kunik ?> <?= $kunik ?>-css" data-blockType="text" data-kunik="<?= $kunik ?>" data-name="text" data-id="<?= $myCmsId ?>">
<div lang="de" id="sp-<?= $myCmsId ?>" style="cursor: auto;hyphens: auto;" class="editable colorDroppable sp-text sp-text-<?= $myCmsId ?>" data-id="<?= $myCmsId ?>" data-field="text" data-name="text" data-kunik="<?= $kunik ?>"></div>
</div>
