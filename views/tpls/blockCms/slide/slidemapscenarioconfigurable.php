<?php

/***************** Required *****************/
$keyTpl     ="slidemapscenarioconfigurable";
$myCmsId    = @$blockCms["_id"]->{'$id'};
?>

<style type="text/css">
    html, body {
        overflow-x: clip;
        overflow-y: visible!important;
    }
    #graphD3{
        position: relative;
    }
    #graphformap<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container {
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
    .titleControlsContainer{
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .titleControlsContainer h3{
        margin-top: 10px;
        margin-bottom: 0;
    }
    .controls<?= $kunik?>{
        display: flex;
        justify-content: center;
        align-items: center;
        margin-left: 15px;
    }
    .controls<?= $kunik?> button.control{
        border: none;
        background: none;
        font-size: 50px;
        opacity: 0;
        transition: all ease .5s;
        color: <?= $blockCms["progressColor"] ?>;
    }
    .controls<?= $kunik?> button.enabled{
        opacity: 1;
    }
    .button-menu-costumizer .labelStatusMenuButton {
        font-size: 14px;
        line-height: normal;
    }
    #right-sub-panel-container .right-sub-panel-header-top,
    #right-sub-panel-container .right-sub-panel-header button{
        height: 50px;
    }
    .right-sub-panel-body.col-md-12.sp-cms-options.dark{
        padding: 15px 5px !important;
    }
    .slideConfig.active {
        border-color: #74d700;
    }
</style>

<div class="<?= $kunik?> no-padding col-xs-12">
    <div class="text-center titleControlsContainer">
        <h3 class="text-center"><?= $blockCms["title"] ?></h3>
        <div class="controls<?= $kunik?>">
            <button class="left-controls control">
                <i class="fa fa-angle-left"></i>
            </button>
            <button class="btn btn-info btn-addScenario text-center"><?= Yii::t("graph", "Add a scenario") ?></button>
            <button class="right-controls control">
                <i class="fa fa-angle-right"></i>
            </button>
        </div>
    </div>
    <div class="text-center">
        <h6><?= Yii::t("graph", "Zone") ?> : <span class="zones"></span></h6>
        <h6><?= Yii::t('graph', "Display") ?> : <span class="affichage"></span></h6>
    </div>
    <div id="graphD3">
        <div id="graphformap<?= $kunik ?>">
        
        </div>
    </div>
</div>

<script type="text/javascript">
    var mapD3<?= $kunik?>= null;
    var zonesValues = {},pays = {}, index = 0, zone = null;
    var blockConfig = <?= json_encode($blockCms["config"]) ?>;
    var typeChart = {
        "default": "<?= Yii::t("graph", "Default") ?>",
        "pie": "<?= Yii::t("graph", "Pie Chart") ?>",
        "bar": "<?= Yii::t("graph", "Bar Chart") ?>",
        "donut": "<?= Yii::t('graph', "Donut Chart") ?>",
        "geoshape": "<?= Yii::t("graph", "Geoshape") ?>",
        "heatmap": "<?= Yii::t("graph", "Heatmap") ?>"
    };
    var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
    var comapOptions = {
        container : "#graphformap<?= $kunik ?>",
        activePopUp : true,
        showLegende: true,
        dynamicLegende: false,
        groupBy: 'tags',
        legendeVar: 'tags',
        mapOpt:{
            zoomControl:false
        },
        elts: {

        },
        mapCustom:customMap
    };
    $(function(){
        if(blockConfig.length > 0){
            comapOptions.markerType = (blockConfig[index].marker == "heatmap") ? blockConfig[index].marker : "default";
            comapOptions.clusterType = (blockConfig[index].marker != "heatmap" && blockConfig[index].marker != "default") ? blockConfig[index].marker : "pie";
            comapOptions.legende = costum.lists[blockConfig[index].legende.cible];
            comapOptions.legendeLabel= blockConfig[index].legende.legendeLabel;
        }
        (new Promise(function(resolve){
            mapD3<?= $kunik?> = new MapD3(comapOptions)
            resolve();
        })).then(function(){
            if(blockConfig.length > 0){
                getData(blockConfig[index].zone.zones, blockConfig[index].zone.zones[Object.keys(blockConfig[index].zone.zones)[0]].name, blockConfig[index])
            }
        })

        if(index < Object.keys(blockConfig).length - 1){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
        }
        $(".<?= $kunik?> .left-controls").off().on("click", function(){
            $(".<?= $kunik?> .right-controls").addClass("enabled");
            $(".<?= $kunik?> .right-controls").prop("disabled", false);
            if(index > 0){
                index--;
                zone = {};
                zone = blockConfig[index].zone.zones;
                comapOptions.markerType = (blockConfig[index].marker == "heatmap") ? blockConfig[index].marker : "default";
                comapOptions.clusterType = (blockConfig[index].marker != "heatmap" && blockConfig[index].marker != "default") ? blockConfig[index].marker : "pie";
                comapOptions.legende = costum.lists[blockConfig[index].legende.cible];
                comapOptions.legendeLabel= blockConfig[index].legende.label;
                mapD3<?= $kunik?>= new MapD3(comapOptions);
                if(Object.keys(zone).length > 0){
                    getData(zone, blockConfig[index].zone.zones[Object.keys(blockConfig[index].zone.zones)[0]]["name"],blockConfig[index]);
                }
                if(index == 0){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $(".<?= $kunik?> .right-controls").off().on("click", function(){
            $(".<?= $kunik?> .left-controls").addClass("enabled");
            $(".<?= $kunik?> .left-controls").prop("disabled", false);
            if(index <= Object.keys(blockConfig).length - 2){
                index++;
                zone = {};
                zone = blockConfig[index].zone.zones;
                comapOptions.markerType = (blockConfig[index].marker == "heatmap") ? blockConfig[index].marker : "default";
                comapOptions.clusterType = (blockConfig[index].marker != "heatmap" && blockConfig[index].marker != "default") ? blockConfig[index].marker : "pie";
                comapOptions.legende = costum.lists[blockConfig[index].legende.cible];
                comapOptions.legendeLabel= blockConfig[index].legende.label;
                mapD3<?= $kunik?>= new MapD3(comapOptions);
                if(Object.keys(zone).length > 0){
                    getData(zone, blockConfig[index].zone.zones[Object.keys(blockConfig[index].zone.zones)[0]]["name"],blockConfig[index]);
                }
                if(index == Object.keys(blockConfig).length - 1){
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
    });
    function getData(zones, name, dataConfig) {
        var defaultFilters<?= $kunik ?> = {};
        // defaultFilters<?= $kunik ?>['$or'] = {}; 
        // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        // /*if(costum.contextType!="projects"){
        //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        // }*/
        // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
        var params = {
            // notSourceKey: false,
            searchType : <?= json_encode(['organizations']) ?>,
            fields : mapSearchFields,
            filters: defaultFilters<?= $kunik ?>,
            indexStep: 0,
            activeContour: true,
            locality: zones
        };
        params["options"] = {
            'tags' : {
                'verb': '$all'
            }
        };
        ajaxPost(
            null,
            baseUrl+'/co2/search/globalautocomplete',
            params,
            function(data){
                var dataToSend = data.results ? data.results : data;
                if(typeof data.zones != "undefined"){
                    Object.assign(dataToSend, data.zones);
                }
                mapD3<?= $kunik?>.clearMap();
                mapD3<?= $kunik?>.addElts(dataToSend);
                // mapD3<?= $kunik?>.getMap().invalidateSize()
                // mapD3<?= $kunik?>.fitBounds();
                $(".<?= $kunik ?> .zones").html(name);
                $(".<?= $kunik ?> .affichage").html(dataConfig.marker == "heatmap" ? typeChart[dataConfig.marker] : typeChart[dataConfig.marker] + " <?= Yii::t("graph", "of") ?> " + dataConfig.legende.label);
            }, function(data){
                    mylog.log('Get zones errors', data);
            },
            null, {
                async: false,
                beforeSend: function(){
                    mapD3<?= $kunik?>.showLoader();
                }
            }
        )
    }
</script>

<script>
    if(costum.editMode) {
        var data = <?= json_encode($blockCms) ?>;
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = data;
        $(".btn-addScenario").off().on('click', function(){
            tplCtx.id = "<?= $myCmsId ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "config";
            blockConfig.push({
                "marker" : "pie",
                "legende":  {
                    "cible" : "",
                    "label": ""
                },
                "zone": {
                    "zones" : '',
                    "country" : "",
                    "level": ""
                }
            });
            tplCtx.value = blockConfig;
            dataHelper.path2Value(tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                    initViewMode();
                    // $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                });
            });
        })
        function initViewMode(){
            var str=`<div class='padding-10 manageMenusCostum col-xs-12'>
                <div class="sortable-list col-xs-12 no-padding">`;
                if(typeof data.config != "undefined") {
                    $.each(data.config, function(k, v){
                        str += `
                            <div class="button-menu-costumizer slideConfig ${k == index ? "active" : ""} col-xs-12" data-key="${k}">
                                <div class="labelStatusMenuButton">
                                    <span class="titleButtonMenu">
                                        ${(v.legende.label != '') ? typeChart[v.marker]+" de "+v.legende.label : "Nouveau slide"} ${Object.keys(v.zone.zones).length > 0 ? v.zone.zones[Object.keys(v.zone.zones)[0]]["name"] : ""}
                                    </span>
                                </div>
                                <div class="col-xs-12 toolsButtonEdit">
                                    <a href="javascript:;" data-title="${(v.legende.label != '') ? typeChart[v.marker]+" de "+v.legende.label : "Nouveau slide"} ${Object.keys(v.zone.zones).length > 0 ? v.zone.zones[Object.keys(v.zone.zones)[0]]["name"] : ""}" data-key="${k}" class="editConfig badge bg-blue">
                                        <i class="fa fa-pencil"></i>
                                        Éditer
                                    </a>
                                    <a href="javascript:;" data-title="${(v.legende.label != '') ? typeChart[v.marker]+" de "+v.legende.label : "Nouveau slide"} ${Object.keys(v.zone.zones).length > 0 ? "(Zone : "+v.zone.zones[Object.keys(v.zone.zones)[0]]["name"]+")" : ""}" data-key="${k}" class="removeConfig badge bg-red margin-left-5">
                                        <i class="fa fa-trash"></i>
                                        Effacer
                                    </a>
                                </div>
                            </div>
                        `;
                    });
                }
                str += `
                </div>
            </div>`;
            $(".cmsbuilder-right-content").addClass("active")
            setTimeout(() => {
                costumizer.actions.tabs.init(
                    "#right-panel",
                    [
                        {
                            key: "scenario",
                            label: "Liste des scénarios",
                            icon: "list-ul",
                            containerClassname:"contains-view-admin"
                        }
                    ],
                    {
                        getContent: function(){
                            return str;
                        },
                        onInitialized: function(){
                            $(".editConfig").off().on('click', function(){
                                var title = $(this).data('title');
                                var indexConfig = $(this).data("key");
                                var dataToSend = $.extend(true, data);
                                var allzone = data.zoneData[index];
                                var zone = data.config[index].zone.zones;
                                costumizer.actions.closeRightSubPanel();
                                var $header = $(`
                                    <div class="right-sub-panel-header right-sub-panel-header-top">
                                        <span>Edition - ${title}</span>
                                        <button>
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                `)
			                    $("#right-sub-panel-container").append($header)
                                
                                var configRight = {
                                    width:300,
                                    header:{
                                        title:`Edition - `+title,
                                        position:"top"
                                    },
                                    className: "col-md-12 sp-cms-options dark",
                                    idName: "general"
                                };
                                
			                    // coInterface.initHtmlPosition();
                                costumizer.actions.openRightSubPanel("", configRight)
                                
                                cmsConstructor.kunik = '<?= $kunik ?>'+index;
                                cmsConstructor.spId = '<?= $myCmsId ?>';
                                cmsConstructor.blocks['<?= $kunik ?>'+index] = getMapInput(indexConfig, allzone, zone);
                                cmsConstructor.editor.actions.generateCoInputs('<?= $kunik ?>'+index, '<?= $myCmsId ?>', null , '<?= $kunik ?>'+index , "general");
					            // cmsConstructor.editor.init('#right-sub-panel-container');
                            })
                            $(".removeConfig").off().on('click', function(){
                                var title = $(this).data('title');
                                var indexConfig = $(this).data("key");
                                bootbox.confirm({
                                    message: `Vous voulez supprimer le slide "${title}"`,
                                    buttons: {
                                        confirm: {
                                            label: tradCms.yesDelete,
                                            className: 'btn-danger'
                                        },
                                        cancel: {
                                            label: trad.cancel,
                                            className: 'btn-default'
                                        }
                                    },
                                    callback: function (response) {
                                        if (response) {
                                            data.config.splice(indexConfig, 1);
                                            var dataToSendPathToValue = {
                                                id: '<?= $myCmsId ?>',
                                                collection: "cms",
                                                path: "config",
                                                value: data.config
                                            };
                                            dataHelper.path2Value( dataToSendPathToValue, function(params) {
                                                dyFObj.commonAfterSave(params,function(){
                                                    cmsConstructor.helpers.refreshBlock('<?= $myCmsId ?>', ".cmsbuilder-block[data-id='<?= $myCmsId ?>']");
                                                    initViewMode();
                                                });
                                            });
                                            // $('.selectPages:checked').each(function () {
                                            //     pages.push($(this).val());
                                            // });
                                            // costumizer.pages.actions.deletePage(pages);
                                        }
                                    }
                                });
                            });
                            $(".slideConfig").off().on("click", function(){
                                var indexConfig = $(this).data("key");
                                index = indexConfig;
                                $(".slideConfig").removeClass("active");
                                $(this).addClass("active");
                                var zoneConfig = data.config[indexConfig].zone.zones;
                                comapOptions.markerType = (data.config[indexConfig].marker == "heatmap") ? data.config[indexConfig].marker : "default";
                                comapOptions.clusterType = (data.config[indexConfig].marker != "heatmap" && data.config[indexConfig].marker != "default") ? data.config[indexConfig].marker : "pie";
                                comapOptions.legende = costum.lists[data.config[indexConfig].legende.cible];
                                comapOptions.legendeLabel= data.config[indexConfig].legende.label;
                                mapD3<?= $kunik?>= new MapD3(comapOptions);
                                if(Object.keys(zoneConfig).length > 0){
                                    getData(zoneConfig, data.config[indexConfig].zone.zones[Object.keys(data.config[indexConfig].zone.zones)[0]]["name"],data.config[indexConfig]);
                                }
                            });
                        }
                    }
                );
            }, 100);
        }
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            initViewMode();
        });

        var dataToSend = $.extend(true, data);
        // var data = <?= json_encode($blockCms) ?>;

        // cmsConstructor.blocks['<?= $kunik ?>'] = getMapInput(index);

        function getMapInput(index, allzone, zone) {
            var mapD3Input = {
                configTabs: {
                    general : {
                        inputsConfig: [
                            {
                                type: "select",
                                options: {
                                    name : "marker",
                                    label: "<?= Yii::t("graph", "Type of marker") ?>",
                                    options: [
                                        {value: "pie", label: "<?= Yii::t("graph", "Pie Chart") ?>"},
                                        {value: "bar", label: "<?= Yii::t("graph", "Bar Chart") ?>"},
                                        {value: "donut", label: "<?= Yii::t('graph', "Donut Chart") ?>"},
                                        {value: "geoshape", label: "<?= Yii::t("graph", "Geoshape") ?>"},
                                        {value: "heatmap", label: "<?= Yii::t("graph", "Heatmap") ?>"}
                                    ],
                                    defaultValue: typeof data.config[index] != "undefined" ? data.config[index].marker : ''
                                },
                                payload: {
                                    path: "config"
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "legende",
                                    label: tradCms.legend,
                                    showInDefault: true,
                                    inputs: [
                                        {
                                            type: "select",
                                            options: {
                                                name: "cible",
                                                class: `legendecible-${cmsConstructor.kunik}`,
                                                label: "<?= Yii::t("graph", 'Legend on') ?>",
                                                options: Object.keys((costum.lists||{})).map(function(element){
                                                    return {label: element, value: element}
                                                }),
                                                defaultValue: typeof data.config[index] != "undefined" ? data.config[index].legende.cible : '',
                                            },
                                            payload: {
                                                path: "config"
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                name: "label",
                                                label: "<?= Yii::t("graph", "Name of the legend")?>",
                                                defaultValue: typeof data.config[index] != "undefined" ? data.config[index].legende.label : '',
                                            },
                                            payload: {
                                                path: "config"
                                            }
                                        },
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "zone",
                                    label: "Zone",
                                    showInDefault: true,
                                    inputs: [
                                        {
                                            type: "select",
                                            options: {
                                                "name": "level",
                                                "label" : "<?= Yii::t("graph","Zone level") ?>",
                                                options: [
                                                    {
                                                        value : "2",
                                                        label : "Niveau 2"
                                                    },
                                                    {
                                                        value : "3",
                                                        label : "Niveau 3"
                                                    },
                                                    {
                                                        value : "4",
                                                        label : "Niveau 4"
                                                    },
                                                    {
                                                        value : "5",
                                                        label : "Niveau 5"
                                                    },
                                                    /* {
                                                        value : "6",
                                                        label : "Niveau 6"
                                                    }, */
                                                ],
                                                defaultValue: typeof data.config[index] != "undefined" ? data.config[index].zone.level : '',
                                            },
                                            payload: {
                                                path: "config"
                                            }
                                        },
                                        {
                                            type: "select",
                                            options: {
                                                isSelect2: true,
                                                name: "country",
                                                label: "<?= Yii::t("graph", "Country") ?>",
                                                options: Object.values(<?= json_encode($country) ?>).map(function(element){
                                                    return {label: element.name, value: element.countryCode};
                                                }),
                                                defaultValue: typeof data.config[index] != "undefined" ? data.config[index].zone.country : '',
                                            },
                                            payload: {
                                                path: "config"
                                            }
                                        },
                                        {
                                            type: "select",
                                            options: {
                                                isSelect2: true,
                                                name: "zones",
                                                label: "<?= Yii::t("graph", "Zone") ?>",
                                                options: Object.keys(allzone).map(function(element){
                                                    return {label: allzone[element].name, value: element};
                                                }),
                                                defaultValue: Object.values(zone).map(function(element){
                                                    return element.id
                                                }).join(","),
                                            },
                                            payload: {
                                                path: "config"
                                            }
                                        }
                                    ]
                                }
                            },
                        ],
                        processValue: {
                            marker: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                dataToSend.config[index].marker = value;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            },
                            cible: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                dataToSend.config[index].legende.cible = value;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            },
                            label: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                dataToSend.config[index].legende.label = value;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            },
                            level: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                dataToSend.config[index].zone.level = value;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            },
                            country: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                dataToSend.config[index].zone.country = value;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            },
                            zones: function(value){
                                delete dataToSend.config[index].zone.zoneData;
                                var dataZones = {};
                                dataZones[value+"level"+allzone[value].level[0]] = {
                                    name: allzone[value].name,
                                    active: true,
                                    id: value,
                                    countryCode: allzone[value].countryCode,
                                    level: allzone[value].level[0],
                                    type: "level"+allzone[value].level[0],
                                    key: value+'level'+allzone[value].level[0]
                                };
                                dataToSend.config[index].zone.zones = dataZones;
                                cmsConstructor.sp_params[cmsConstructor.spId].config = dataToSend.config;
                                return dataToSend.config;
                            }
                        }
                    }
                },
                onChange: function(path, valueToSet, name, payload, value){
                   
                },
                afterSave: function(path, valueToSet, name, payload, value){
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                    if(name == "country"){
                        // cmsConstructor.blocks['<?= $kunik ?>'+index].configTabs.general.inputsConfig[2].options.showInDefault = true;
                        $(".editConfig[data-key="+index+"]").trigger("click");
                    }
                    if(name == "level"){
                        // cmsConstructor.blocks['<?= $kunik ?>'+index].configTabs.general.inputsConfig[2].options.showInDefault = true;
                        $(".editConfig[data-key="+index+"]").trigger("click");
                    }
                }
            }
            return mapD3Input;
        }
    }
</script>