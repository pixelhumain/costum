<?php

/***************** Required *****************/
$keyTpl     = "slide_map_scenario_for_zone";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$typeChart = array(
    "default" => Yii::t("graph", "Default"),
    "pie" => Yii::t("graph", "Pie Chart"),
    "bar" => Yii::t("graph", "Bar Chart"),
    "donut" => Yii::t('graph', "Donut Chart"),
    "geoshape" => Yii::t("graph", "Geoshape"),
    "heatmap" => Yii::t("graph", "Heatmap")
);
$typeGraph = array(
    "circlerelation" => Yii::t("graph", "Circle Relation"),
    "mindmap" => Yii::t("graph", "Mindmap"),
    "circularbarplot" => Yii::t("graph", "Circular Barplot")
);
$iconsmapPng = array(
    "pie" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/pie.png",
    "bar" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/bar.png",
    "geoshape" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/geoshape.png",
    "heatmap" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/heatmap.png",
    "donut" => Yii::app()->getModule(Map::MODULE)->getAssetsUrl() . "/images/icons/donut.png",
);
$iconsGraphPng = array(
    "mindmap" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/mindmap.png",
    "circlerelation" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/circlepacking.png",
    "circularbarplot" => Yii::app()->getModule("graph")->getAssetsUrl() . "/images/icons/circularbarplot.png",
);
$graphAssets = [
    '/js/venn.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
//var_dump($blockChildren);
/*************** End required ***************/
?>

<style type="text/css">
    html,
    body {
        overflow-x: clip;
        overflow-y: visible !important;
    }

    #graphD3 {
        position: relative;
    }

    #graphformap<?= $kunik ?> {
        height: 700px;
        position: relative;
    }

    .chart-legende-container {
        /* position: absolute;
        right: 0;
        top: 0; */
        z-index: 9999999;
        background: white;
        /* display: none; */
    }

    .titleControlsContainer {
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .titleControlsContainer h3 {
        margin-top: 10px;
        margin-bottom: 0;
    }

    .controls<?= $kunik ?> {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-left: 15px;
    }

    .controls<?= $kunik ?> button.control {
        border: none;
        background: none;
        font-size: 50px;
        opacity: 0;
        transition: all ease .5s;
        color: <?= $blockCms["progressColor"] ?>;
    }

    .controls<?= $kunik ?> button.enabled {
        opacity: 1;
    }

    .img-icon {
        width: 25px;
    }

    .btn-type {
        display: flex;
        /* flex-direction: column; */
        align-items: center;
        justify-content: center;
        order: -1
    }
    .graph-icons-container {
        display: flex;
        /* flex-direction: column; */
        align-items: center;
        justify-content: center;
    }

    .btn-type button {
        margin: 5px;
        background-color: white;
        box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
        box-sizing: border-box;
        color: #1A202C;
        border-radius: 8px;
        cursor: pointer;
    }

    .btn-type button.type-active {
        background-color: #8f8f8f;
        box-shadow: rgba(0, 0, 0, .15) 0 3px 9px 0;
        transform: translateY(-2px);
    }
    .graph-icons-container button {
        margin: 5px;
        background-color: white;
        box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
        box-sizing: border-box;
        color: #1A202C;
        border-radius: 8px;
        cursor: pointer;
    }

    .graph-icons-container button.type-active {
        background-color: #8f8f8f;
        box-shadow: rgba(0, 0, 0, .15) 0 3px 9px 0;
        transform: translateY(-2px);
    }

    .leaflet-top.leaflet-right {
        display: flex;
        flex-direction: column;
        background-color: white;
    }
    #graphformap<?= $kunik ?> g.divide>circle {
        fill: transparent !important;
    }
    #graphformap<?= $kunik ?> #mobile-section {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        width: 100%;
        z-index: 999;
        padding: 4%;
        overflow-y: scroll;
    }
    .switches-container<?= $kunik ?> {
        width: 20%;
        position: relative;
        display: flex;
        padding: 0;
        position: relative;
        background: #a5c145;
        line-height: 3rem;
        border-radius: 3rem;
        margin-left: auto;
    }

    /* input (radio) for toggling. hidden - use labels for clicking on */
    .switches-container<?= $kunik ?> input {
        visibility: hidden;
        position: absolute;
        top: 0;
    }

    /* labels for the input (radio) boxes - something to click on */
    .switches-container<?= $kunik ?> label {
        width: 50%;
        padding: 10px;
        margin: 0;
        text-align: center;
        cursor: pointer;
        color: white;
    }

    /* switch highlighters wrapper (sliding left / right) 
        - need wrapper to enable the even margins around the highlight box
    */
    .switch-wrapper<?= $kunik ?> {
        position: absolute;
        top: 0;
        bottom: 0;
        width: 50%;
        padding: 0.15rem;
        z-index: 3;
        transition: transform .5s cubic-bezier(.77, 0, .175, 1);
        /* transition: transform 1s; */
    }

    /* switch box highlighter */
    .switch<?= $kunik ?> {
        border-radius: 3rem;
        background: white;
        height: 100%;
    }

    /* switch box labels
        - default setup
        - toggle afterwards based on radio:checked status 
    */
    .switch<?= $kunik ?> div {
        width: 100%;
        text-align: center;
        opacity: 0;
        padding: 10px;
        display: block;
        color: #a5c145 ;
        transition: opacity .2s cubic-bezier(.77, 0, .175, 1) .125s;
        will-change: opacity;
        position: absolute;
        top: 0;
        left: 0;
    }

    /* slide the switch box from right to left */
    .switches-container<?= $kunik ?> input:nth-of-type(1):checked~.switch-wrapper<?= $kunik ?> {
        transform: translateX(0%);
    }

    /* slide the switch box from left to right */
    .switches-container<?= $kunik ?> input:nth-of-type(2):checked~.switch-wrapper<?= $kunik ?> {
        transform: translateX(100%);
    }

    /* toggle the switch box labels - first checkbox:checked - show first switch div */
    .switches-container<?= $kunik ?> input:nth-of-type(1):checked~.switch-wrapper<?= $kunik ?> .switch<?= $kunik ?> div:nth-of-type(1) {
        opacity: 1;
    }

    /* toggle the switch box labels - second checkbox:checked - show second switch div */
    .switches-container<?= $kunik ?> input:nth-of-type(2):checked~.switch-wrapper<?= $kunik ?> .switch<?= $kunik ?> div:nth-of-type(2) {
        opacity: 1;
    }
</style>
<div class="<?= $kunik ?> no-padding col-xs-12">
    <div class="text-center titleControlsContainer">
        <h3 class="text-center"><?= $blockCms["title"] ?></h3>
        <div class="controls<?= $kunik ?>">
            <button class="left-controls control" disabled="true">
                <i class="fa fa-angle-left"></i>
            </button>
            <button class="right-controls control" disabled="true">
                <i class="fa fa-angle-right"></i>
            </button>
        </div>
    </div>
    <div class="text-center">
        <h6><?= Yii::t("graph", "Zone") ?> : <span class="zones"></span></h6>
        <h6><?= Yii::t('graph', "Display") ?> : <span class="affichage"></span></h6>
    </div>
    <div id="graphD3">
        <?php if(count($blockCms["element"]["map"]) > 0 && count($blockCms["element"]["graph"]) > 0){ ?>
            <div class="graph-button">
                <div class="switches-container<?= $kunik ?>">
                    <input type="radio" id="switchMap" name="switchType" value="map" checked="checked" />
                    <input type="radio" id="switchGraph" name="switchType" value="graph" />
                    <label for="switchMap">Carte</label>
                    <label for="switchGraph">Graphique</label>
                    <div class="switch-wrapper<?= $kunik ?>">
                        <div class="switch<?= $kunik ?>">
                            <div>Carte</div>
                            <div>Graphique</div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(count($blockCms["element"]["graph"]) > 0) { ?>
            <div class="graph-icons-container">

            </div>
        <?php } ?>
        <div id="graphformap<?= $kunik ?>">

        </div>
    </div>
</div>

<script type="text/javascript">
    var mapD3<?= $kunik ?> = null;
    var iconmapPng = <?= json_encode($iconsmapPng) ?>;
    var iconGraphPng = <?= json_encode($iconsGraphPng) ?>;
    var dataConfig = <?= json_encode($blockCms) ?>;
    var zonesValues = {},
        pays = {},
        index = 0,
        zone = null;
    var typeChart = <?= json_encode($typeChart) ?>;
    var typeGraph = <?= json_encode($typeGraph) ?>;
    var customMap = (typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {
        tile: "maptiler"
    };
    var mapSelected = 'pie';
    var graphSelected = 'circlerelation';
    var getDataActive = "map" ;
    var comapOptions = {
        container: "#graphformap<?= $kunik ?>",
        activePopUp: true,
        showLegende: true,
        dynamicLegende: false,
        groupBy: 'tags',
        legendeVar: 'tags',
        mapOpt: {
            zoomControl: false
        },
        mapCustom: customMap,
        elts: {

        }
    };
    var graph<?= $kunik ?> = null;

    function changeelement(type) {
        window[getDataActive+'Selected'] = type;
        getData(dataConfig.zone.zones, dataConfig.zone.zones[Object.keys(dataConfig.zone.zones)[0]]["name"])
        
    }

    function addIcon() {
        var button = L.control({
            position: 'topright'
        });
        button.onAdd = function(map) {
            var div = L.DomUtil.create('div', 'info button btn-type');
            var str = "";
            dataConfig.element.map.forEach((element) => {
                str += `<button class='btn btn-chart-type ${element == mapSelected ? "type-active" : ''}' title='${typeChart[element]}' onclick="changeelement('${element}')">
                    <img class='img-icon' src="${baseUrl+iconmapPng[element]}" ?>
                </button>`;
            });
            div.innerHTML = str;
            return div;
        }
        button.addTo(mapD3<?= $kunik ?>.getMap());
    }
    function addIconGraph() {
        var str = "";
        if($(".graph-icons-container").length == 0){
            $("#").append('<div class="graph-icons-container"></div>');
        }
        var $div = $(".graph-icons-container");
        dataConfig.element.graph.forEach((element) => {
            str += `<button class='btn btn-chart-type ${element == graphSelected ? "type-active" : ''}' title='${typeGraph[element]}' onclick="changeelement('${element}')">
                <img class='img-icon' src="${baseUrl+iconGraphPng[element]}" ?>
            </button>`;
        });
        $div.html(str);
    }

    function initMap(legende, data) {
        if (Object.keys(legende).length > 0) {
            comapOptions.legende = costum.lists[legende.cible];
            comapOptions.legendeLabel = legende.label;
        } else {
            comapOptions.dynamicLegende = true;
            comapOptions.groupBy = 'type';
            comapOptions.legendeVar = 'type';
        }
        comapOptions.markerType = (mapSelected == "heatmap" || mapSelected == "geoshape") ? mapSelected : "default";
        comapOptions.clusterType = (mapSelected != "heatmap" && mapSelected != "default" && mapSelected != "geoshape") ? mapSelected : "pie";

        var dataToSend = data.results ? data.results : data;
        if(typeof data.zones != "undefined"){
            Object.assign(dataToSend, data.zones);
        }
        (new Promise(function(resolve) {
            mapD3<?= $kunik ?> = new MapD3(comapOptions)
            resolve();
        })).then(function() {
            mapD3<?= $kunik?>.clearMap();
            mapD3<?= $kunik?>.addElts(dataToSend);
            addIcon();
            $(".<?= $kunik ?> .affichage").html((mapSelected == "heatmap" || mapSelected == "geoshape") ? typeChart[mapSelected] : typeChart[mapSelected] + " de " + (dataConfig.legende.length > 0 ? dataConfig.legende[index].label : "Type"));
            var intervalMap = setInterval(() => {
                if($("#graphformap<?= $kunik ?>").is(":visible")){
                    mapD3<?= $kunik?>.getMap().invalidateSize();
                    mapD3<?= $kunik?>.fitBounds();
                    clearInterval(intervalMap);
                }
            }, 500);
        })
    }
    function countTags(tags, results){
        var counts = {};
        for (const [key, value] of Object.entries(results)) {
            for (const [index, tag] of Object.entries(costum.lists[tags])) {
                if(value.tags.includes(tag)){
                    counts[tag] = counts[tag] || 0;
                    counts[tag] += 1;
                }
            }
        }
        return counts;
    }
    var activeCircle<?= $kunik ?> = "";

    function processMindmap(tags, data) {
        let structuredData = {
            id:"root<?= $kunik ?>",
            label:'Tiers-lieux',
            children:[]
        }
        for (var i in costum.lists[tags]) {
            let child = {
                id:"element"+i,
                label:costum.lists[tags][i],
                children: []
            }
            Object.keys(data).forEach((key, index) => {
                let element = data[key];
                let itIsIn = false;
               if(element.tags.includes(costum.lists[tags][i])){
                    child.children.push({
                        id:key,
                        label:element.name,
                        collection: element.collection
                    });
                }
            });
            structuredData["children"].push(child)
        }
        return structuredData;
    }

    function initmindmap(tags, data) {
        var dataToSend = data.results ? data.results : data;
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new MindmapGraph([], 1, {id: "root", label: "Tiers-lieux"})
            resolve();
        })).then(function(){
            var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            tooltip.hide();
            window.onresize = (e) => {
                tooltip.goToNode();
            }
            // graph<?= $kunik ?>.setTheme(tags);
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.setOnClickNode((e,d,n) => {
				if(d.data.collection){
					urlCtrl.openPreview("#page.type."+d.data.collection+".id."+d.data.id);
				}
			})
            graph<?= $kunik ?>.updateData(processMindmap(tags, dataToSend), true)
        })
    }

    function initcircularbarplot(tags, data) {
        var dataToSend = data.results ? data.results : data;
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new CircularBarplotGraph()
            resolve();
        })).then(function(){
            // var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            // tooltip.hide();
            // window.onresize = (e) => {
            //     tooltip.goToNode();
            // }
            $("#graphformap<?= $kunik ?>").empty();
            graph<?= $kunik ?>.setAuthorizedTags(costum.lists[tags]);
            graph<?= $kunik ?>.setMax(40);
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.updateData(graph<?= $kunik ?>.preprocessResults(dataToSend), true)
        })
    }

    function initcirclerelation(tags, data) {
        if(!window.initedGraph){
            window.initedGraph = [];
        }
        if($('div#circle-filter-container').length == 0){
            $("#graphformap<?= $kunik ?>").before('<div id="circle-filter-container" class="margin-top-10 text-center"></div>');
        }
        $("div#circle-filter-container").empty();
        var dataToSend = data.results ? data.results : data;
        var authorizedTags = costum.lists[tags];
        var color = d3.scaleOrdinal()
                .domain(authorizedTags)
                .range(d3.schemeTableau10);
        // authorizedTags.push("Autre");
        (new Promise(function(resolve){
            graph<?= $kunik ?> = new CircleRelationGraph([], d => d.group, authorizedTags, [])
            resolve();
        })).then(function(){
            var tooltip = new GraphTooltip("#graphformap<?= $kunik ?>");
            tooltip.hide();
            window.onresize = (e) => {
                tooltip.goToNode();
            }
            graph<?= $kunik ?>.draw("#graphformap<?= $kunik ?>");
            graph<?= $kunik ?>.setOnClickNode((e,d,n) => {
                tooltip.node = d3.select(e.currentTarget)
                tooltip.setContent(d)
                tooltip.show()
            })
            graph<?= $kunik ?>.setBeforeDrag(() => {
                graph<?= $kunik ?>.setDraggable(costum.editMode);
            });
            graph<?= $kunik ?>.setOnZoom((e,d,n) => {
                tooltip.goToNode();
            })
            var counts = countTags(tags,dataToSend);
            d3.select("div#circle-filter-container")
            .selectAll("button")
            .data(authorizedTags)
            .join((enter) => {
                enter.append("xhtml:button")
                    .text(d => d)
                    .classed("btn margin-right-5 btn-circle-zoomer", true)
                    .on("click", (e,d) => {
                    var thisElement = $(e.target);
                    if(activeCircle<?= $kunik ?> != d){
                        graph<?= $kunik ?>.focus(d);
                        activeCircle<?= $kunik ?> = d;
                        $(".btn-circle-zoomer").removeClass("btn-primary");
                        thisElement.addClass("btn-primary");
                        //$("#"+GraphUtils.slugify(d)+" .list-group-item").show();
                    }else{
                        activeCircle<?= $kunik ?> = "";
                        thisElement.removeClass("btn-primary");
                        graph<?= $kunik ?>.unfocus().then(() => {
                        mylog.log("UNFOCUSED");
                        });
                    }
                    
                    $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                    $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $("#"+GraphUtils.slugify(d)).addClass("in");
                })
                .append("xhtml:span")
                .classed("badge badge-theme-count margin-left-5", true)
                .attr("data-countvalue", d => d)
                .attr("data-countkey", d => d)
                .attr("data-countlock", "false")
                .html(d => counts[d]);
            });
            
                // p<?= $kunik ?>.filters.actions.themes.setThemesCounts(p<?= $kunik ?>);
            graph<?= $kunik ?>._circlePadding = 20;
            graph<?= $kunik ?>._externalCircleMargin = 30;
            graph<?= $kunik ?>._color = (da, index) => {
                /*p<?= $kunik ?>.graph.graph.rootG.selectAll(".divide circle:last-child")
                .filter(function (d,i) { return i === index;})
                .style("fill-opacity", 0.8)*/

                //   $(".nodes-container div").css({"color": "white"});
                return color(index);
            };
            graph<?= $kunik ?>.switchMode = (mode) => {
                if (mode == "graph"){
                    graph<?= $kunik ?>._buttonGraph.classed("active", true);
                    graph<?= $kunik ?>._buttonList.classed("active", false);
                    $("#mobile-section").hide();
                    graph<?= $kunik ?>._currentMode = mode;
                    graph<?= $kunik ?>._transition = 750;
                }else if(mode == "list"){
                    $("#mobile-section").show();
                    graph<?= $kunik ?>._buttonGraph.classed("active", false);
                    graph<?= $kunik ?>._buttonList.classed("active", true);
                    graph<?= $kunik ?>._currentMode = mode;
                    graph<?= $kunik ?>._transition = 0
                }else{
                    console.error("MODE UNKNOWN");
                }
            }
            graph<?= $kunik ?>.updateData(graph<?= $kunik ?>.preprocessResults(dataToSend), true)
        })
        if($("#graphformap<?= $kunik ?>").is(":visible")){

            window.initedGraph.push('<?= $kunik ?>');
            // initGraph<?= $kunik ?>();
        }
    }
    function initGraph(tags, data) {
        window["init"+graphSelected](tags, data);
        addIconGraph();
        $(".<?= $kunik ?> .affichage").html( typeGraph[graphSelected] + " de " + (dataConfig.legende.length > 0 ? dataConfig.legende[index].label : "Type"));
    }
    $(function() {
        if(Object.keys(dataConfig.zone.zones).length > 0){
            getData(dataConfig.zone.zones, dataConfig.zone.zones[Object.keys(dataConfig.zone.zones)[0]]["name"])
        }else{
            if(typeof costum.address != "undefined" && typeof costum.address.level3 != "undefined"){
                var zones = {};
                zones[costum.address.level3+"level3"] = {
                    name: costum.address.level3Name,
                    active: true,
                    id: costum.address.level3,
                    countryCode: costum.address.addressCountry,
                    level: 3,
                    type: "level3",
                    key: costum.address.level3+'level3'
                }
                dataConfig.zone.zones = zones;
                getData(zones, costum.address.level3Name);
            }
        }
        if (index < dataConfig.legende.length - 1) {
            $(".<?= $kunik ?> .right-controls").addClass("enabled");
            $(".<?= $kunik ?> .right-controls").prop("disabled", false);
        }
        $(".<?= $kunik ?> .left-controls").off().on("click", function() {
            $(".<?= $kunik ?> .right-controls").addClass("enabled");
            $(".<?= $kunik ?> .right-controls").prop("disabled", false);
            if (index > 0) {
                index--;
                getData(dataConfig.zone.zones, dataConfig.zone.zones[Object.keys(dataConfig.zone.zones)[0]]["name"])
                // initMap(dataConfig.legende[index])
                if (index == 0) {
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $(".<?= $kunik ?> .right-controls").off().on("click", function() {
            $(".<?= $kunik ?> .left-controls").addClass("enabled");
            $(".<?= $kunik ?> .left-controls").prop("disabled", false);
            if (index <= dataConfig.legende.length - 2) {
                index++;
                getData(dataConfig.zone.zones, dataConfig.zone.zones[Object.keys(dataConfig.zone.zones)[0]]["name"]);
                if (index == dataConfig.legende.length - 1) {
                    $(this).removeClass("enabled");
                    $(this).prop("disabled", true);
                }
            }
        });
        $("input[name=switchType]").off("change").on('change', function () {
            getDataActive = $(this).val();
            getData(dataConfig.zone.zones, dataConfig.zone.zones[Object.keys(dataConfig.zone.zones)[0]]["name"]);
        })
    })

    function getData(zones, name) {
        var defaultFilters<?= $kunik ?> = {};
        // defaultFilters<?= $kunik ?>['$or'] = {}; 
        // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
        // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
        // /*if(costum.contextType!="projects"){
        //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
        // }*/
        // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
        // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
        var mapSearchFields = (costum != null && typeof costum.map != "undefined" && typeof costum.map.searchFields != "undefined") ? costum.map.searchFields : ["urls", "address", "geo", "geoPosition", "tags", "type", "zone"];
        var params = {
            // notSourceKey: false,
            searchType: <?= json_encode(['organizations']) ?>,
            fields: mapSearchFields,
            filters: defaultFilters<?= $kunik ?>,
            indexStep: 0,
            activeContour: getDataActive === "map",
            locality: zones
        };
        params["options"] = {
            'tags': {
                'verb': '$all'
            }
        };
        ajaxPost(
            null,
            baseUrl + '/co2/search/globalautocomplete',
            params,
            function(data) {
                $("div#circle-filter-container").empty();
                $("div.graph-icons-container").empty();
                if(getDataActive == "map") {
                    initMap(dataConfig.legende[index], data)
                }else{
                    initGraph(dataConfig.legende[index].cible, data.results);
                }
                // mapD3<?= $kunik ?>.getMap().invalidateSize()
                // mapD3<?= $kunik ?>.fitBounds();
                $(".<?= $kunik ?> .zones").html(name);
            },
            function(data) {
                mylog.log('Get zones errors', data);
            },
            null, {
                async: false,
            }
        )
    }
</script>
<script>
    if(costum.editMode){
        var allzone = <?= json_encode($zones) ?>;
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var zone = <?= json_encode($blockCms["zone"]["zones"]) ?>;
        var region = <?= json_encode($region) ?>;
        var slideScenarioforZoneInputs = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: 'inputSimple',
                            options: {
                                label: tradCms.title,
                                name: "title"
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "element",
                                label: "Type d'élément à afficher",
                                inputs: [
                                    {
                                        type: "selectMultiple",
                                        options: {
                                            name: "map",
                                            label: "Carte",
                                            options: [
                                                {label: "<?= Yii::t("graph", "Default")?>", value: "default"},
                                                {label: "<?= Yii::t("graph", "Pie Chart")?>", value: "pie"},
                                                {label: "<?= Yii::t("graph", "Bar Chart")?>", value: "bar"},
                                                {label: "<?= Yii::t("graph", "Donut Chart")?>", value: "donut"},
                                                {label: "<?= Yii::t("graph", "Geoshape")?>", value: "geoshape"},
                                                {label: "<?= Yii::t("graph", "Heatmap")?>", value: "heatmap"},
                                            ]
                                        },
                                    },
                                    {
                                        type: "selectMultiple",
                                        options: {
                                            name: "graph",
                                            label: "Graph",
                                            options: [
                                                {label: "<?= Yii::t("graph", "Circle Relation")?>", value: "circlerelation"},
                                                {label: "<?= Yii::t("graph", "Mindmap")?>", value: "mindmap"},
                                                {label: "<?= Yii::t("graph", "Circular Barplot")?>", value: "circularbarplot"},
                                            ]
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "inputMultiple",
                            options: {
                                label: tradCms.preferencelegende,
                                name: "legende",
                                class: `legendeconfig-${cmsConstructor.kunik}`,
                                inputs: [
                                    [
                                        {
                                            type: "select",
                                            options: {
                                                name: "cible",
                                                class: `legendecible-${cmsConstructor.kunik}`,
                                                label: "Legende Cible",
                                                options: Object.keys((costum.lists||{})).map(function(element){
                                                    return {label: element, value: element}
                                                })
                                            }
                                        },
                                        {
                                            type: "inputSimple",
                                            options: {
                                                label: tradCms.label,
                                                name: "label",
                                            }
                                        }
                                    ]
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "zone",
                                label: "Zone",
                                inputs: [
                                    {
                                        type: "select",
                                        options: {
                                            "name": "level",
                                            "label" : "<?= Yii::t("graph","Zone level") ?>",
                                            options: [
                                                {
                                                    value : "2",
                                                    label : "Niveau 2"
                                                },
                                                {
                                                    value : "3",
                                                    label : "Niveau 3"
                                                },
                                                {
                                                    value : "4",
                                                    label : "Niveau 4"
                                                },
                                                {
                                                    value : "5",
                                                    label : "Niveau 5"
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            isSelect2: true,
                                            name: "country",
                                            label: "<?= Yii::t("graph", "Country") ?>",
                                            options: Object.values(<?= json_encode($country) ?>).map(function(element){
                                                return {label: element.name, value: element.countryCode};
                                            })
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            isSelect2: true,
                                            class: `region-<?=$kunik ?> ${Object.keys(region).length > 0 ? '' : 'hidden'}`,
                                            name: "region",
                                            label: "<?= Yii::t("graph", "Région") ?>",
                                            options: Object.keys(region).map(function(element){
                                                return {label: region[element].name, value: element};
                                            })
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name: "zones",
                                            isSelect2: true,
                                            label: "<?= Yii::t("graph", "Zone") ?>",
                                            options: Object.keys(allzone).map(function(element){
                                                return {label: allzone[element].name, value: element};
                                            }),
                                            defaultValue: Object.values(zone).map(function(element){
                                                return element.id
                                            }).join(",")
                                        }
                                    }
                                ]
                            }
                        },
                    ],
                    processValue: {
                        zones: function(value){
                            var data = {};
                            data[value+"level"+allzone[value].level[0]] = {
                                name: allzone[value].name,
                                active: true,
                                id: value,
                                countryCode: allzone[value].countryCode,
                                level: allzone[value].level[0],
                                type: "level"+allzone[value].level[0],
                                key: value+'level'+allzone[value].level[0]
                            }
                            return data;
                        }
                    }
                },
            },
            beforeLoad: function(){
                
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value){
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                if(name == "country"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "level"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "region"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
            }
        }
        cmsConstructor.blocks['<?= $kunik ?>'] = slideScenarioforZoneInputs;
    }
</script>