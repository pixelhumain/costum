<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$initAnswerFiles = Document::getListDocumentsWhere(array(
	"id" => (string)$blockCms["answer"]["_id"],
	"type" => 'answers'
), "image");

$depimg = [];
foreach ($initAnswerFiles as $key4 => $d4) {
	if (isset($d4["imageKey"])) {
		$depimg[$d4["imageKey"]] = $d4;
	}
}

$porteurName = $blockCms["campPorteur"]["text"] ?? "France Tiers Lieux";
$porteurImg = $blockCms["porteurImg"];
$porteurId = $blockCms["campPorteur"]["id"] ?? "";
$imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';

//$isftl = filter_var($blockCms["isftl"], FILTER_VALIDATE_BOOLEAN);

?>
<style id="css-<?= $kunik ?>" type="text/css">
	@font-face {
		font-family: "montserrat";
		src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
			url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
	}

	.mst {
		font-family: 'montserrat' !important;
	}

	@font-face {
		font-family: "CoveredByYourGrace";
		src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
	}

	.cbyg {
		font-family: 'CoveredByYourGrace' !important;
	}


	:root {
		--pfpb-primary-green-color: <?php echo @$blockCms["originalColor"] ?>;
		--pfpb-primary-grey-color: #808180;
		--pfpb-secondary-grey-color: #e6e1e1;
		--pfpb-primary-blue-color: #7193c2;
		--pfpb-primary-red-color: rgb(238, 122, 122);
		;
		--pfpb-secondary-red-color: #f1e2e2;
		--pfpb-primary-purple-color: <?php echo @$blockCms["campColor"] ?>;
	}

	.pasl-container .financementprogress-card .pasl-actionname,
	.pasl-container .financementprogress-card .pasl-action-info .pasl-actioninfo,
	.pfpb-primary-green-color {
		color: var(--pfpb-primary-grey-color);
	}

	.pasl-container {
		justify-content: center;
		align-items: center;
		font-family: "montserrat";
		padding: 20px;
	}

	.pasl-container .swiper {
		margin: 30px;
		overflow: hidden;
	}

	.pasl-container .financementprogress-card {
		border-radius: 10px;
		box-shadow: 0 10px 10px 2px rgba(0, 0, 0, 0.2);
	}


	.pasl-container .financementprogress-card .pasl-img {
		transition: all 0.3s;
		overflow: hidden;
	}

	.pasl-container .multiselect-active .financementprogress-card .pasl-img,
	.pasl-container .financementprogress-card:hover .pasl-img {
		height: 0;
		padding: 0;
	}

	.pasl-container .progress-bar {
		border-radius: 5px;
		height: 20px;
		cursor: pointer;
	}

	.pasl-container .progress-bar.progress-bar-success {
		background: var(--pfpb-primary-green-color);
	}

	.pasl-container .progress {
		border-radius: 30px;
		background: #eceff1;
		margin-top: 20px;
		box-shadow: 20px 20px 20px #e6eaed, -20px -20px 20px #f2f4f5;
		margin: 10px;
	}

	.pasl-container .amountpercentTotal {
		position: absolute;
		color: var(--pfpb-primary-green-color);
		transform: translateY(10px);
		font-weight: bold;
	}

	.pasl-container .financementprogress-card .pasl-lbh {
		height: 50px;
		width: 50px;
	}

	.pasl-container .financementprogress-card .pasl-lbh img {
		display: inline-block;
		overflow: hidden;
		width: 35px;
		height: 35px;
		border-radius: 10px;
		vertical-align: middle;
		border: 3px solid #fff;
		margin-left: -10px;
	}

	.pasl-container .financementprogress-card .pasl-action-info {
		margin-top: 20px;
		transition: all 0.3s;
		width: 100%;
		height: 0;
		overflow: hidden;
		white-space: nowrap;
		padding: 0px;
		background: var(--pfpb-secondary-grey-color);
		border-bottom-left-radius: 10px;
		border-bottom-right-radius: 10px;
	}

	.pasl-container .financementprogress-card:hover .pasl-action-info,
	.pasl-container .multiselect-active .financementprogress-card .pasl-action-info {
		height: 100%;
		padding: 20px;
		overflow: visible;
	}

	.financementprogress-card .statbutton-cofinance {
		display: none;
		text-align: center;
		width: 120px !important;
	}

	.pasl-action-btn {
		width: 100%;
	}

	.pasl-container .multiselect-active .financementprogress-card .statbutton-cofinance {
		display: block;
	}

	.pasl-container .financementprogress-card .plustext {
		background-color: #a6b1bc;
		text-align: center;
		color: #fff;
		font-size: 18px;
		padding: 3.5px;
		border-radius: 10px;
		vertical-align: middle;
		border: 3px solid #fff;
		margin-left: -10px;
	}

	.pasl-container .financementprogress-card .pasl-info-action {
		display: inline-flex;
		flex-direction: vertical;
		width: 100%;
	}


	.pasl-container .financementprogress-card .pasl-actionname {
		margin: 10px;
		padding-top: 20px;
		-webkit-line-clamp: 2;
		line-clamp: 2;
		overflow: hidden;
		text-overflow: ellipsis;
		display: -webkit-box;
		-webkit-box-orient: vertical;
		min-height: 50px;
	}

	.pasl-container .financementprogress-card:hover .pasl-actionname {
		margin: 10px;
		padding-top: 10px;
		-webkit-line-clamp: unset;
		line-clamp: unset;
		overflow: visible;
		text-overflow: unset;
		display: block;
		height: min-content;
		border-radius: 10px;
	}

	.pasl-container button {
		margin: 5px;
		background-color: var(--pfpb-primary-blue-color);
		border: 0;
		border-radius: 7px;
		padding: 5px 10px;
		color: white;
		font-weight: bold;
		width: 220px;
		padding: auto;
	}

	.pasl-container button i {
		display: contents;
	}

	.pasl-container .financementprogress-card button {
		background-color: white;
		color: var(--pfpb-primary-blue-color);
	}

	.pasl-container .statbutton-multiselect-container {
		/* float: right; */
		/* text-align: center; */
		transition: all 0.3s;
		width: 0%;
		overflow: hidden;
	}

	.pasl-container .statbutton-multiselect-container.statbutton-multiselect-active {
		width: 100%;
	}

	.pasl-container .statbutton-group {
		display: block;
		flex-direction: horizontal;
		width: 100%;
		min-height: 35px;
	}

	.pasl-container .statbutton-group.flex {
		display: flex;
	}

	.pasl-container .multiselect-group-button,
	.pasl-container .multiselect-group-button-2 {
		float: right;
		text-align: center;
		/*justify-content: center;*/
		transition: all 0.3s;
		width: 0%;
		overflow: hidden;
	}

	.multiselect-group-button.multiselect-group-button-active {
		width: 80%;
		flex-wrap: wrap;
	}

	.multiselect-group-button-2.multiselect-group-button-active {
		width: min-content;
		margin-left: 20px;
	}

	.pasl-container .statbutton-cofinance {
		background: var(--pfpb-primary-green-color);
	}

	.pasl-container .statbutton-cofinance:disabled {
		background: var(--pfpb-primary-grey-color);
	}

	.pasl-container .financementprogress-card {
		transition: all 0.3s
	}

	.pasl-action-btn {
		display: inline-flex;
		text-align: center;
		justify-items: center;
		justify-content: center;
		align-items: center;
		width: 100%;
	}

	.pasl-container .financementprogress-card .pasl-action-info .pasl-actioninfo {
		background: rgb(241, 237, 237);
		;
		display: inline-block;
		width: 80px;
		padding: 10px;
		border-radius: 10px;
		height: 80px;
		text-align: center;
		font-weight: bold;

	}

	.pasl-container .financementprogress-card .pasl-action-info .pasl-actioninfo span {
		background: rgb(241, 237, 237);
		display: block;

	}

	.pasl-container .financementprogress-card .pasl-action-info .pasl-actionecot {
		background: rgb(241, 237, 237);
		color: var(--pfpb-primary-blue-color);
	}

	.pasl-container .financementprogress-card .pasl-action-info .pasl-actioninfo:not(:first-child) {
		margin-left: 20px;
	}

	.pasl-actioninfo-container {
		text-align: center;
	}

	.financementprogress-card-chkb {
		display: none;
	}

	.financementprogress-card-chkb+.financementprogress-card:after {
		transition: all 0.3s;
		/*content: '\f058';*/
		content: '\f14a';
		font-family: FontAwesome;
		position: absolute;
		top: 0;
		right: 10px;
		color: var(--pfpb-primary-blue-color);
		font-size: 20px;
		opacity: 0;
	}

	.financementprogress-card-chkb:checked+.financementprogress-card:after {
		opacity: 100;
	}

	.financementprogress-card-chkb:checked+.financementprogress-card button {
		background: #f1e2e2;
		color: rgb(238, 122, 122);
	}

	.pasl-container .financementprogress-card .pasl-img {
		height: 120px;
		padding: 20px;
		text-align: center
	}

	.pasl-container .financementprogress-card .pasl-img img {
		height: 120px;
	}

	/*
    .pasl-container .financementprogress-card .pasl-actioninfo:last-child {
        width: 0;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background: var(--pfpb-primary-blue-color);
        margin-left: 0 !important;
        transition: all 0.3s;
    }

    .financementprogress-card-chkb:checked + .financementprogress-card .pasl-actioninfo {
        width: 74px;
        height: 74px;
    }

    .financementprogress-card-chkb:checked + .financementprogress-card .pasl-actioninfo:last-child {
        width: 74px;
        margin-left: 5px !important;
        padding: 10px;
        overflow: visible;
    }

    .financementprogress-card-chkb + .financementprogress-card .pasl-actioninfo:last-child span {
        transition: all 0.3s;
        background: var(--pfpb-primary-blue-color);
        color: #fff;
    }

    .financementprogress-card-chkb:checked + .financementprogress-card .pasl-action-info .pasl-actioninfo:not(:first-child) {
        margin-left: 5px
    }

    .financementprogress-card-chkb:checked + .financementprogress-card .pasl-action-info {
        padding-left: 5px;
    }*/

	.pasl-container .input-group {
		padding: 5px;
	}

	.pasl-container input {
		padding: 5px;
		height: 30px;
		border-top-left-radius: 20px;
		border-bottom-left-radius: 20px;
		border: 2px solid var(--pfpb-primary-green-color);
	}

	.pasl-container .input-group-addon {
		padding: 5px;
		height: 30px;
		padding: 5px;
		border-top-right-radius: 20px;
		border-bottom-right-radius: 20px;
		background: var(--pfpb-primary-green-color);
	}

	.pasl-container .statbutton-cancel-multiselect span,
	.pasl-container .statbutton-sellectall span {
		transition: all 0.3s;
		width: 0% !important;
		overflow: hidden;
		display: inline-flex;
	}

	.pasl-container .statbutton-cancel-multiselect:hover span,
	.pasl-container .statbutton-sellectall:hover span {
		transition: all 0.3s;
		width: min-content !important;
		overflow: hidden;
		display: inline-flex;
	}

	.pasl-container .statbutton-cancel-multiselect:hover,
	.pasl-container .statbutton-sellectall:hover {
		transition: all 0.3s;
		width: min-content !important;
	}

	.pasl-container .statbutton-cancel-multiselect,
	.pasl-container .statbutton-sellectall {
		transition: all 0.3s;
		width: 50px;
		float: left;
	}

	.pasl-container .statbutton-cancel-multiselect {
		color: var(--pfpb-primary-red-color);
		background: var(--pfpb-secondary-grey-color);
	}

	.pasl-container .statbutton-sellectall {
		color: var(--pfpb-primary-blue-color);
		background: var(--pfpb-secondary-grey-color);
	}

	.pasl-container .multiselect-group-button {
		height: auto
	}

	.pasl-container .pasl-actionecot {
		justify-items: center;
		text-align: center;
		background: #fff;
		margin: 10px;
		border-radius: 10px;
		height: 0;
		overflow: hidden;
	}

	.financementprogress-card-chkb:checked+.financementprogress-card .pasl-info-action .pasl-action-info .pasl-actionecot {
		height: 100%;
	}

	@media (min-width: 720px) {
		.pasl-container .financementprogress-card {
			width: 250px;
			height: 250px;
			margin: 20px;
		}

		.pasl-container .multiselect-active .financementprogress-card {
			width: 250px;
			height: 370px;
			margin: 20px;
		}

		.pasl-container .financementprogress-card:hover,
		.pasl-container .multiselect-active .financementprogress-card {
			position: relative;
			width: 250px;
			min-height: 250px;
			margin: 20px;
			height: min-content;
		}

		.pasl-container .financementprogress-card .pasl-actionname {
			height: 60px;
			overflow: hidden;
		}

		.pasl-container .swiper-slide {
			width: fit-content;
		}

		.multiselect-group-button,
		.multiselect-group-button-2 {
			display: inline-flex;
			flex-direction: horizontal;
			white-space: nowrap;
		}

	}

	.aap-cover-spin {
		position: fixed;
		width: 100%;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		background-color: rgba(255, 255, 255, 0.7);
		z-index: 99999999;
		display: none;
	}

	@-webkit-keyframes spin {
		from {
			-webkit-transform: rotate(0deg);
		}

		to {
			-webkit-transform: rotate(360deg);
		}
	}

	@keyframes spin {
		from {
			transform: rotate(0deg);
		}

		to {
			transform: rotate(360deg);
		}
	}

	.aap-cover-spin {
		content: '';
		display: block;
		position: absolute;
		left: 48%;
		top: 40%;
		width: 40px;
		height: 40px;
		border: 4px solid black;
		border-top-color: transparent;
		border-radius: 50%;
		-webkit-animation: spin .8s linear infinite;
		animation: spin .8s linear infinite;
	}

	.pasl-bigtitle {
		width: 100%;
		display: flex;
		flex-wrap: wrap;
		align-items: center;
		justify-content: space-between;
	}

	.pasl-bigtitle span {
		color: #43c9b7;
		font-weight: bold;
		font-size: 30px;
	}

	.pasl-container {
		padding: 4% 20% 8%;
	}

	@media (max-width : 500px) {
		.pasl-container {
			padding: 4% 10% 8%;
		}

		.camp-detail-content .action-cont {
			align-items: center;
		}
	}

	.pasl-bigtitle .btn-group button {
		width: 50px !important;
		background-color: var(--pfpb-primary-purple-color);
	}

	.pasl-bigtitle .btn-group {
		float: right !important;
		text-align: right;
		align-items: right;
	}

	.financ-line-content {
		display: block;
		position: relative;
		/*
        padding: 20px 100px;*/
		width: 100%;
	}

	.financ-line-title span {
		font-size: 40px;
		font-weight: 600;
		color: #43C9B7;
	}

	.financ-line-content .financ-line-desc {
		display: block;
		position: relative;
		border: 1px #d9d9d9 solid;
		/* width: 80%; */
		border-radius: 8px;
		margin-bottom: 5px;
	}

	.financ-line-content .financ-line-desc .desc-title {
		position: relative;
		width: 100%;
		display: flex;
		align-items: center;
		font-weight: 800;
		font-size: 2.2rem;
		padding: 10px;
	}

	.financ-line-content .financ-line-desc .desc-title:not(.collapsed) {
		border-bottom: 1px #d9d9d9 solid;
	}

	.financ-line-content .financ-line-desc .desc-motif {
		position: relative;
		width: 100%;
		display: flex;
		align-items: center;
		font-weight: bold;
		font-size: 22px;
		padding: 0 0 5px;
	}

	.financ-line-content .financ-line-desc .desc-line {
		flex-grow: 1;
		border-bottom: 2px dashed #000;
		margin: 0 10px;
	}

	.financ-line-content .montant-financ {
		max-width: 25%;
		min-width: 10%;
		text-align: right;
		white-space: nowrap;
		font-family: 'lunchType';
	}

	.financ-line-content .fin-motif-desc-text {
		max-width: 70%;
		font-family: 'lunchType';
	}

	.pasl-sw {
		overflow-y: clip;
		overflow-x: scroll;
	}

	.tglv:not(.active) {
		background-color: #fff !important;
		border: 1px solid var(--pfpb-primary-purple-color) !important;
		color: var(--pfpb-primary-purple-color) !important;
	}

	.financ-line-content .financ-line-desc .desc-motif .motif-text {
		display: flex;
		align-items: center;
	}

	.financ-line-content .financ-line-desc .desc-motif .motif-text span {
		font-family: 'lunchType';
		margin-top: 8px;
	}

	.financ-line-content .financ-line-desc .desc-motif .motif-text img {
		width: 25px;
		height: 25px;
		border-radius: 50%;
		margin-right: 3px;
	}

	.financ-line-content .financ-line-desc .desc-title {
		cursor: pointer;
	}

	.financ-line-content .financ-line-desc:hover {
		border: 1px #43C9B7 solid;
	}

	.financ-line-content .financ-line-desc .desc-title:hover {
		color: #43C9B7;
	}

	.financ-line-content .financ-line-desc .desc-title:hover .desc-line {
		border-bottom: 2px dashed #43C9B7;
	}

	.financ-line-content .financ-line-desc .desc-motif.pending {
		color: #F78C6B;
	}

	.financ-line-content .financ-line-desc .desc-motif.pending .desc-line {
		border-bottom: 2px dashed #F78C6B;
	}

	.financ-line-content .financ-line-desc .desc-motif.paid {
		color: #073B4C;
	}

	.financ-line-content .financ-line-desc .desc-motif.paid .desc-line {
		border-bottom: 2px dashed #073B4C;
	}

	.financ-line-content .financ-line-desc .desc-motif.virement {
		color: #118AB2;
	}

	.financ-line-content .financ-line-desc .desc-motif.virement .desc-line {
		border-bottom: 2px dashed #118AB2;
	}

	.statbutton-group .statbutton-multiselect-container button {
		margin: 5px 0 !important;
	}

	.multiselect-group-button-active .input-group {
		display: flex !important;
		padding: 5px 0 !important;
		margin-right: 2px !important;
	}

	.multiselect-group-button-active .input-group .input-group-addon {
		height: auto !important;
		width: 25px !important;
		padding: auto !important;
		display: flex;
		align-items: center;
		justify-content: center;
	}

	.multiselect-group-button .cofinanc-btn {
		text-align: start;
	}

	.multiselect-group-button .cofinanc-btn button {
		padding: 5px !important;
		width: auto !important;
		margin: 0 !important;
	}

	.statbutton-group .multiselect-group-button-2.multiselect-group-button-active {
		width: 20% !important;
		margin-left: 0 !important;
	}

	.statbutton-group .multiselect-group-button-2.multiselect-group-button-active button {
		padding: 5px 10px !important;
		width: auto !important;
		display: block;
		margin: 0 5px 5px 0 !important;
		height: 30px !important;
	}

	@media (max-width : 550px) {
		.financ-line-content .financ-line-desc .desc-title {
			font-size: 14px !important;
		}

		.financ-line-content .financ-line-desc .desc-motif {
			font-size: 14px !important;
		}

		.financ-line-title span {
			font-size: 22px;
		}

		.financ-line-content .fin-motif-desc-text {
			max-width: 65%;
		}

		.financ-line-content .desc-line {
			visibility: hidden;
		}

		.financ-line-content .montant-financ {
			max-width: 30%
		}
	}

	.cancel-promess {
		color: #F78C6B;
		position: relative;
		cursor: pointer;
		display: none;
		margin-left: 8px;
		margin-bottom: 6px;
	}

	.pending .cancel-promess {
		display: inline-block !important;
	}


	.addDepensecms {
		border: 2px solid var(--pfpb-primary-purple-color) !important;
		background-color: var(--pfpb-primary-purple-color) !important;
	}

	.addDepensecms:hover {
		border-color: var(--pfpb-primary-purple-color) !important;
		color: var(--pfpb-primary-purple-color);
		background-color: transparent !important;
	}

	.caretDown {
		margin-left: 10px;
		font-size: 24px;
		margin-bottom: 5px;
	}

	.financ-ftl {
		color: <?= $blockCms["campColor"] ?> !important;
	}

	.financeur-lista {
		padding: 15px;
	}

	@media (max-width : 400px) {
		.financ-line-content .montant-financ {
			max-width: 50%;
		}
		.financ-line-content .fin-motif-desc-text {
			max-width: 50%;
		}
	}

	.dep-img {
		float: left;
		width: 140px;
		height: 105;
		margin-right: 10px;
		margin-bottom: 10px;
	}

	.dep-img img {
		width: 100%;
		height: 100%;
		object-fit: cover;
	}

	.dep-desc {
		text-align: justify;
		font-size: 1.4rem;
		font-weight: 600;
	}
	.editLineDep {
		color: #fff;
		position: absolute;
		cursor: pointer;
		display: none !important;
		top: -13px;
		right: 60px;
		width: 20px;
		height: 20px;
		align-items: center;
		justify-content: center;
		background-color: #337ab7;
		border-radius: 5px;
		font-size: 12px;
	}
	.supLineDep {
		color: #fff;
		position: absolute;
		cursor: pointer;
		display: none !important;
		top: -13px;
		right: 35px;
		width: 20px;
		height: 20px;
		align-items: center;
		justify-content: center;
		background-color: red;
		border-radius: 5px;
		font-size: 12px;
	}
    .hideLineDep , .showLineDep {
        color: #fff;
        position: absolute;
        cursor: pointer;
        display: none !important;
        top: -13px;
        right: 10px;
        width: 20px;
        height: 20px;
        align-items: center;
        justify-content: center;
        background-color: red;
        border-radius: 5px;
        font-size: 12px;
    }
    .hideLineDep {
        background-color: red;
    }
    .showLineDep {
        background-color: var(--pfpb-primary-grey-color);
    }
	#imageDepenseuploader .qq-upload-button-selector {
		margin: 0 30%;
	} 
	<?php if ($blockCms["canEdit"] || isset($costum["admins"][Yii::app()->session['userId']])) { ?>
	.financ-line-content .financ-line-desc:hover .editLineDep, .financ-line-content .financ-line-desc:hover .supLineDep , .financ-line-content .financ-line-desc:hover .hideLineDep , .financ-line-content .financ-line-desc:hover .showLineDep {
		display: flex !important;
	}
	<?php } ?>

    .archived_answer_toogle {
        background-color: #ff276c !important;
        width: fit-content !important;
    }

    #archivedAnswer span {
        color: #bbb8b8;
    }

    #archivedAnswer span i {
        color: #fff;
    }
</style>

<div class="pasl-container <?= $kunik ?> <?= $kunik ?>-css" data-need-refreshblock="<?= (string) $blockCms["_id"] ?>" data-refreshblock-suppr="<?= $myCmsId ?>">
	<!-- Financer -->
    <?php
    if(!empty($blockCms["financerData"])){
    ?>

    <div class="pasl-bigtitle financ-line-title">
        <span><?= Yii::t("cms", "Cofinancer")?>(s)</span>
    </div>
    <div class="margin-bottom-50">
        <div class="financ-line-content">
            <?php
            $aacParentName = "";
            if (isset($blockCms["parent"])) {
                $parentKeys = array_keys($blockCms["parentForm"]["parent"]);
                if (!empty($parentKeys)) {
                    $aacParentName = $blockCms["parent"]["name"];
                }
            }
			// Trouver les index des éléments dans le tableau
			$index_main = null;
			$index_to_remove = null;
			
			if(isset($costum) && isset($costum["contextId"]) && $blockCms["financerData"]){
				foreach ($blockCms["financerData"] as $key => $item) {
					if ($item['id'] === $costum["contextId"]) {
						$index_main = $key;
					}
					if ($item['name'] === $porteurName) {
						$index_to_remove = $key;
					}
				}
			}
			
			// Vérifier si les deux éléments existent
			if ($index_main !== null && $index_to_remove !== null) {
				// Ajouter le montant
				$blockCms["financerData"][$index_main]['amount'] += floatval($blockCms["financerData"][$index_to_remove]['amount']);
			
				// Supprimer l'élément
				unset($blockCms["financerData"][$index_to_remove]);
			
				// Réindexer le tableau après suppression
				$blockCms["financerData"] = array_values($blockCms["financerData"]);
			}

			
            foreach ($blockCms["financerData"] as $key => $value) {
                if($aacParentName != $value['name']){
            ?>
                <div class="financ-line-desc">
                    <div class="desc-title" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;">
                        <span class="fin-motif-desc-text">
                            <a style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;" class="lbh-preview-element lbh contact<?=@$value['id']?>" href="#page.type.organizations.id.<?= $value['name'] == $aacParentName ? $porteurId : @$value['id']?>">
								<img height="20px" data-id="<?=@$value['id']?>" src="<?= $value['name'] == $aacParentName ? $porteurImg : (isset($value['profilImg']) && $value['profilImg'] != '' ? $value['profilImg'] : $imgUrl )?>"> <?= $aacParentName == $value['name'] ? $porteurName : $value['name']?>
                            </a>
                        </span>
                        <span class="desc-line" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;"></span>
                        <span class="montant-financ" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;">
                            <span><?= @$value['amount'] ?><i class="fa fa-eur"></i>
                        </span>
                    </div>
                </div>
            <?php
                }
            }
            foreach ($blockCms["financerData"] as $key => $value) {
                if($aacParentName == $value['name']){
            ?>
                    <div class="financ-line-desc">
                        <div class="desc-title" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;">
                        <span class="fin-motif-desc-text">
                            <a style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;" class="lbh-preview-element lbh contact<?=@$value['id']?>" href="#page.type.organizations.id.<?= $value['name'] == $aacParentName ? $porteurId : @$value['id']?>">
								<img height="20px" data-id="<?=@$value['id']?>" src="<?= $value['name'] == $aacParentName ? $porteurImg : (isset($value['profilImg']) && $value['profilImg'] != '' ? $value['profilImg'] : $imgUrl )?> "> <?= $aacParentName == $value['name'] ? $porteurName : $value['name']?>
                            </a>
                        </span>
                            <span class="desc-line" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;"></span>
                            <span class="montant-financ" style="color: <?= $value['name'] == $aacParentName ? $blockCms["campColor"] : (isset($value["type"]) && $value["type"] == "tl" ? "#ff276c" : "")?>;">
                            <span><?= @$value['amount'] ?><i class="fa fa-eur"></i>
                        </span>
                        </div>
                    </div>
            <?php
                }
            }?>
        </div>
    </div>

    <?php
    }
    ?>
    <!--  LDD -->
    <div class="pasl-bigtitle financ-line-title">
		<span><?= Yii::t("cms", "Objectives and ﬁnancing requirements")?></span>
		<?php
		if ($blockCms["canEdit"] || isset($costum["admins"][Yii::app()->session['userId']])) {
		?>
			<button class="btn addDepensecms pull-right" type="button">
				<i class="fa fa-plus-circle "></i> <?= Yii::t("cms", "Add")?>
			</button>
		<?php
		}
		?>
		<!--<div class="btn-group pull-right" role="group" aria-label="Basic example">
            <button type="button" class="btn tglv active depensetableview"><i class="fa fa-list-ul"></i></button>
            <button type="button" class="btn tglv depensecardview"><i class="fa fa-trello"></i></button>
            <?php /*if(isset($costum["admins"][Yii::app()->session['userId']])){ */ ?>
            <button type="button" class="btn tglv getinputdepense"><i class="fa fa-cogs"></i></button>
            <?php /*} */ ?>
        </div>-->
	</div>
	<div class="statbutton-group">
		<?php //if(){ 
		?>
		<!--<div class="statbutton-multiselect-container statbutton-multiselect-active">
            <button class="statbutton-multiselect" > Sélectionner et cofinancer <i class="fa fa-arrow-right pull-right"></i> </button>
        </div>-->
		<?php //} 
		?>
		<div class="multiselect-group-button">
			<div class="input-group">
				<input class="input-name-multiselect" type="text" placeholder="Nom">
				<span class="input-group-addon"> <i class="fa fa-user-o"></i> </span>
			</div>
			<div class="input-group">
				<input class="input-email-multiselect" type="text" placeholder="Email">
				<span class="input-group-addon"> <i class="fa fa-envelope-o"></i> </span>
			</div>
			<div class="input-group">
				<input class="input-amount-multiselect" type="number" placeholder="Montant">
				<span class="input-group-addon"> <i class="fa fa-euro"></i> </span>
			</div>
			<div class="cofinanc-btn">
				<button class="statbutton-cofinance" disabled> Cofinancer (<span class="action-select-count">0</span>) action(s) <i class="fa fa-arrow-right pull-right"></i> </button>
			</div>
		</div>
		<div class="multiselect-group-button-2">
			<button class="statbutton-cancel-multiselect"> <i class="fa fa-times pull-right"></i> </button>
			<button class="statbutton-sellectall"> <i class="fa fa-check-square pull-right"></i> </button>
		</div>
	</div>
	<div>
		<div class="financeftl-list financ-line-content">

		</div>
		<div style="display: none" class="swiper" id="pasl-swiper-<?= $kunik ?>">
			<div class="swiper-wrapper pasl-sw">
				<?php
				foreach ($blockCms["lines"] as $lineId => $line) {
					$price = isset($line["price"]) ? (int)$line["price"] : 0;
					$totalfinancement = is_array($line["totalfinancement"]) ? 0 : (int)$line["totalfinancement"];
					$percentage = 0;
					if ($price != 0 && $totalfinancement != 0) {
						$percentage = ($totalfinancement * 100) / $price;
					}
				?>
					<div class="swiper-slide">
						<input type="checkbox" class="financementprogress-card-chkb" data-id="<?= $lineId ?>" id="action-<?= $lineId ?>" name="action-<?= $lineId ?>">
						<div class="financementprogress-card" data-id="<?= $lineId ?>">
							<div style="width: 100%;" class='pasl-img'>
								<?php
								if (!empty($line["imageKey"]) && !empty($depimg[$line["imageKey"]])) {
								?>
									<div style="width: 100%;" class=''>
										<img class="answer-depense-img" height="140" src="<?php echo $depimg[$line["imageKey"]]["imagePath"] ?>">
									</div>
								<?php
								} else {
								?>
									<div style="width: 100%;" class=''>
										<img height="140" src="<?php echo Yii::app()->getModule(Yii::app()->params["module"]["parent"])->getAssetsUrl() . "/images/thumbnail-default.jpg"; ?>" class="answer-depense-img">
									</div>
								<?php
								}
								?>
								<!--<img src="<?php /*echo Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumbnail-default.jpg"; */ ?>" class="">-->
							</div>
							<div class="pasl-actionname">
								<?= $line["poste"] ?? '(Vide)' ?>
							</div>
							<div class="progress" style="height: 10px">
								<div class="progress-bar progress-bar-success" role="progressbar" style="width:<?= $percentage ?>%">
									<span class="amountpercentTotal"><?= round($percentage, 0) ?>%</span>
								</div>
								<?php if ($percentage != 0 && $percentage != 100) { ?>
									<div class="progress-bar" role="progressbar" style="width:<?= 100 - $percentage ?>%; background: grey">

									</div>
								<?php } ?>
							</div>
							<div class="pasl-info-action">
								<div class="pasl-action-info">
									<div class="pasl-actioninfo-container">
										<div class="pasl-actioninfo">
											<span class="pasl-actioninfo-i"> <i class="fa fa-money"></i> </span>
											<span class="pasl-actioninfo-sp">Total</span>
											<span class="pasl-actioninfo-sp"><?= trim(strrev(chunk_split(strrev($price), 3, ' '))) ?>€</span>
										</div>
										<div class="pasl-actioninfo">
											<span class="pasl-actioninfo-i"> <i class="fa fa-calculator"></i> </span>
											<span class="pasl-actioninfo-sp">Reste</span>
											<span class="pasl-actioninfo-sp resteact" data-reste="<?= $price - $totalfinancement ?>" data-id="<?= $lineId ?>"><?= trim(strrev(chunk_split(strrev($price - $totalfinancement), 3, ' '))) ?>€</span>
										</div>
									</div>
									<div class="pasl-actionecot">
										<span class="pasl-actionecot-i"> <i class="fa fa-user"></i> </span>
										<span class="pasl-actionecot-sp">Distribution</span>
										<span class="pasl-actionecot-sp ecotact" data-id="<?= $lineId ?>" data-ecotact="0" data-changed="false"><span data-before="0" data-id="<?= $lineId ?>" class="ecotenteditable" contenteditable="true" data-id="<?= $lineId ?>">0</span>€</span>
									</div>
									<div class="pasl-action-btn">
										<button data-id="<?= $lineId ?>" class="statbutton-cofinance statbutton-cofinance-index">Séléctionner</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php
				}
				?>
			</div>
			<!--<div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div>-->
		</div>
		<div class="financeftl-input">

		</div>
	</div>

</div>

<script>
    function reloadpasl(callback = function() {}) {
        $(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}'] .pfpb-container`).html('<div class="co-popup-cofinance-loader-section"></div>');
        coInterface.showCostumLoader(".co-popup-cofinance-loader-section")
        ajaxPost(
            null,
            baseUrl + "/co2/cms/refreshblock", {
                idBlock: sectionDyf.<?php echo $kunik ?>blockCms._id.$id,
                clienturi: window.location.href
            },
            function(data) {
                if (data.result) {
                    $(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}`).html(data["view"]);
                    // cmsBuilder.block.initEvent();
                    callback();
                } else {
                    toastr.error("something went wrong!! please try again.");
                }
            },
            null,
            "json", {
                async: false
            }
        )
    }

    (function(X, $) {
		var depense_images,
			php,
			aap;

		depense_images = [];
		aap = new aapv2();
		php = {
			kunik: "<?= $kunik ?>",
			myCmsId: "<?= $myCmsId ?>",
			blockCms: JSON.parse(JSON.stringify(<?= json_encode($blockCms) ?>)),
			styleCss: JSON.parse(JSON.stringify(<?= json_encode($styleCss) ?>)),
			initAnswerFiles : <?php echo (!empty($initAnswerFiles) ? json_encode($initAnswerFiles) : json_encode([])); ?>
		};
		php.blockCms = $.extend({}, {
			answer: null,
			parentCommunity: null,
			canEdit: false
		}, php.blockCms);
		var projectAnswer = php.blockCms.answer;
        var archivedProjectAnswer = php.blockCms.archivedAnswer;
		sectionDyf[php.kunik + "blockCms"] = php.blockCms;
        var reloadpaslcontainer = sectionDyf[php.kunik + "blockCms"]._id.$id;
		var communityParent = php.blockCms.parentCommunity;
		var userCanEdit = php.blockCms.canEdit;
		var str = "";
		str += cssHelpers.render.generalCssStyle(php.styleCss);
		$("#css-" + php.kunik).append(str);
		if (costum.editMode) {
			cmsConstructor.sp_params[php.myCmsId] = php.blockCms;
			var projectActionSlide = {
				configTabs: {
					style: {
						inputsConfig: [
							"addCommonConfig"
						]
					},
					advanced: {
						inputsConfig: [
							"addCommonConfig"
						]
					},
				},
				afterSave: function(path, valueToSet, name, payload, value) {
					cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
				}
			}
			cmsConstructor.blocks["projectActionSlide" + php.myCmsId] = projectActionSlide;
		}

		function validateCoFinancement() {
			var validate = $('.input-amount-multiselect').val() != "" &&
				$('.input-amount-multiselect').val() > 0 &&
				($('.input-name-multiselect').val() != "" || $('.input-email-multiselect').val() != "") &&
				$('.financementprogress-card-chkb:checked').length > 0;
			$('.multiselect-group-button .statbutton-cofinance').prop('disabled', !validate);
		}

		function addSpace(number) {
			if (notEmpty(number)) {
				number = number.toString();
				var pattern = /(-?\d+)(\d{3})/;
				while (pattern.test(number))
					number = number.replace(pattern, "$1 $2");
			} else
				number = 0;
			return number;
		}

		function disturbAmount() {
			$('.financementprogress-card').each(function() {
				var thisid = $(this).data('id');
				$('.ecotact[data-id="' + thisid + '"]').html('<span data-before="0" data-id="' + thisid + '" class="ecotenteditable" contenteditable="true">0</span>€');
				$('.ecotact[data-id="' + thisid + '"]').data('ecotact', '0');
			});

			var totalReste = 0;

			var actionckd1 = [];
			$('.financementprogress-card-chkb:checked + .financementprogress-card .resteact').each(function() {
				if (!actionckd1.includes($(this).data('id'))) {
					totalReste += parseInt($(this).data('reste'));
					actionckd1.push($(this).data('id'));
				}
			});

			var actionckd2 = [];
			$('.financementprogress-card-chkb:checked + .financementprogress-card').each(function() {
				var thisid = $(this).data('id');
				if (!actionckd2.includes(thisid)) {
					var reste = parseInt($('.resteact', this).data('reste'));

					if (!isNaN(Math.round(((reste / totalReste) * $('.input-amount-multiselect').val())))) {
						$('.ecotact[data-id="' + thisid + '"]').html('<span data-before="' + Math.round((reste / totalReste) * $('.input-amount-multiselect').val()) + '" data-id="' + thisid + '" class="ecotenteditable" contenteditable="true">' + Math.round((reste / totalReste) * $('.input-amount-multiselect').val()) + '</span>€');
						$('.ecotact[data-id="' + thisid + '"]').data('ecotact', Math.round((reste / totalReste) * $('.input-amount-multiselect').val()));
					} else {
						$('.ecotact[data-id="' + thisid + '"]').html('<span data-before="0" data-id="' + thisid + '" class="ecotenteditable" contenteditable="true">0</span>€');
						$('.ecotact[data-id="' + thisid + '"]').data('ecotact', '0');
					}

					actionckd2.push(thisid);
				}
			});
		}

		function getFinanceView(uid, index, finance, financegroup, answerId) {
			var html = '';
			var output = ''
			if (notEmpty(finance) && finance.id !== Object.keys(projectAnswer.context)[0]) {
				if (communityParent[finance.id] != 'undefined') {
					output = communityParent[finance.id];
				}
				/*html += `<div class="desc-motif ${typeof finance != 'undefined' ? ( typeof finance['bill'] != "undefined" && finance['bill'] != '' ? 'virement' : (typeof finance.status != 'undefined' && finance.status == 'paid' ? 'paid' : 'pending')) : ''}">
                            <span class="motif-text"><img src="${typeof output != "undefined" && output && typeof output['profilImageUrl'] != 'undefined' && output['profilImageUrl'] != '' ? output['profilImageUrl'] : modules.co2.url +'/images/thumbnail-default.jpg'}"/> <span>${finance.name}</span></span>
                            <span class="desc-line"></span>
                            <span class="montant-financ">${finance.amount}<i class="fa fa-eur"></i> </span>${(typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || userCanEdit == true ? `<span class="cancel-promess" data-depenseindex=${uid} data-financerindex=${index} data-answerid=${answerId}><i class="fa fa-close"></i></span>` : ''}
                         </div>`;*/
				$.each(financegroup, function(i, value) {
					if (value.id == Object.keys(projectAnswer.context)[0] && finance.finkey == value.finkey) {
						isvalidfinance = true;
						/*html += `<div class="desc-motif" style='color: <?= $blockCms["campColor"] ?>'>
                                    <span class="motif-text"><img src="<?= $porteurImg ?>"/> <span><?= $porteurName ?> ${notEmpty(finance.tlname) ? "pour "+finance.tlname : ""}</span></span>
                                    <span class="desc-line"></span>
                                    <span class="montant-financ" data-id="${value.id}">${value.amount}<i class="fa fa-eur"></i> </span> ${(typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || userCanEdit == true ? `<span class="cancel-promess" data-depenseindex=${uid} data-financerindex=${i} data-answerid=${answerId}><i class="fa fa-close"></i></span>` : '' }
                                </div>`;*/
					}
				});
			}
			return html;
		};

		function financeList(index, depense, answerId) {
			var html = '';
			if (notEmpty(depense.financer)) {
				$.each(depense.financer, function(i, fin) {
					if (!notEmpty(depense.status) || depense.status != 'financementFTL')
						html += getFinanceView(index, i, fin, depense.financer, answerId);
				});
			}
			return html;
		};

		function depenseList(answer, loading_images , archivedAnswer = {}) {
			mylog.log(answer, 'anss')
			var html = {
				start: "",
				end: "",
                archived: ""
			};
			var answerId = answer["_id"]["$id"];
			var financer_template_html;
            var archived_financer_template_html;

			financer_template_html = '' +
				'<div class="financ-line-desc">' +
				'    <div class="desc-title collapsed" data-toggle="collapse" data-target="#financeur{{index}}" data-index="{{index}}" data-place="{{place}}"> ' +
				'        <span class="editLineDep"><i class="fa fa-pencil"></i></span>'+
				'        <span class="supLineDep"><i class="fa fa-trash"></i></span>'+
                '        <span class="hideLineDep"><i class="fa fa-eye-slash"></i></span>'+
				'        <span class="fin-motif-desc-text">{{poste}}</span>' +
				'        <span class="desc-line"></span>' +
				'        <span class="montant-financ">{{progress_amount}}</span>' +
				'        <span class="fa fa-caret-down caretDown"> </span>' +
				'    </div>' +
				'    <div class="financeur-lista collapse" id="financeur{{index}}" data-index={{index}}>' +
				'    {{img}}' +
				'    {{desc}}' +
				'    {{financer_list}}' +
				'    </div>' +
				'</div>';

            archived_financer_template_html = '' +
                '<div class="financ-line-desc">' +
                '    <div class="desc-title collapsed" data-toggle="collapse" data-target="#financeur{{index}}" data-index="{{index}}" data-place="{{place}}" data-mode="archive">' +
                '        <span class=""><i class="fa fa-archive"></i></span>'+
                '        <span class="editLineDep"><i class="fa fa-pencil"></i></span>'+
                '        <span class="supLineDep"><i class="fa fa-trash"></i></span>'+
                '        <span class="showLineDep"><i class="fa fa-eye"></i></span>'+
                '        <span class="fin-motif-desc-text">{{poste}}</span>' +
                '        <span class="desc-line"></span>' +
                '        <span class="montant-financ">{{progress_amount}}</span>' +
                '        <span class="fa fa-caret-down caretDown"> </span>' +
                '    </div>' +
                '    <div class="financeur-lista collapse" id="financeur{{index}}" data-index={{index}}>' +
                '    {{img}}' +
                '    {{desc}}' +
                '    {{financer_list}}' +
                '    </div>' +
                '</div>';

			// nettoyer la liste des images
			while (loading_images.length)
				loading_images.pop();
			if (notEmpty(answer.answers) && notEmpty(answer.answers.aapStep1) && notEmpty(answer.answers.aapStep1.depense)) {
				$.each(answer.answers.aapStep1.depense, function(index, dep) {
					var total,
						formated_financer,
						subs;

					if (typeof dep.poste !== "string")
						return (true);
					total = 0;
					if (typeof dep.financer !== 'undefined' && Array.isArray(dep.financer))
						total = dep.financer.reduce((old, curr) => old + (isNaN(curr.amount) ? 0 : parseFloat(curr.amount)), 0);
					formated_financer = financeList(index, dep, answerId);
					subs = {
						index: dep.depenseindex,
						place : index,
						poste: "",
						progress_amount: "",
						img: "",
						desc: "",
						//financer_list: "<p style='clear: both;'><?= Yii::t("cms", "No financer")?></p>"
                        financer_list: "",
					};
					if (notEmpty(dep.poste))
						subs.poste = dep.poste;
					if (notEmpty(dep.price))
						subs.progress_amount = dataHelper.printf('<span>{{total}} <i class="fa fa-eur"></i></span> / <span>{{price}} <i class="fa fa-eur"></i></span>', {
							total: total,
							price: dep.price
						});
					if (formated_financer)
						subs.financer_list = formated_financer;
					if (typeof dep.description === "string")
						subs.desc = "<p class='dep-desc'>" + dep.description + "</p>";
					if (typeof dep.imageKey === "string" && dep.imageKey.length != 'null') {
						subs.img = "<div class='dep-img' style='display : none;'><img src='' data-key='" + dep.imageKey + "' /></div>";
						loading_images.push(dep.imageKey);
					}
					if (typeof dep.price !== "undefined" && total >= parseInt(dep.price))
						html.end += dataHelper.printf(financer_template_html, subs);
					else
						html.start += dataHelper.printf(financer_template_html, subs);
				});
			} else
				return ("<div class='center'>Il n'y a rien ici</div>");

            if (notEmpty(archivedAnswer.answers) && notEmpty(archivedAnswer.answers.aapStep1) && notEmpty(archivedAnswer.answers.aapStep1.depense)) {
                html.archived += `<button class='btn btn-sm archived_answer_toogle' type="button" data-toggle="collapse" data-target="#archivedAnswer" aria-expanded="false" aria-controls="archivedAnswer"> <?= Yii::t("cms", "Show archived expense lines")?>  </button>`;
                html.archived += `<div id='archivedAnswer' class="collapse multi-collapse" >`;
                $.each(archivedAnswer.answers.aapStep1.depense, function(index, dep) {
                    var total,
                        formated_financer,
                        subs;

                    if (typeof dep.poste !== "string")
                        return (true);
                    total = 0;
                    if (typeof dep.financer !== 'undefined' && Array.isArray(dep.financer))
                        total = dep.financer.reduce((old, curr) => old + (isNaN(curr.amount) ? 0 : parseFloat(curr.amount)), 0);
                    formated_financer = financeList(index, dep, answerId);
                    subs = {
                        index: dep.depenseindex,
                        poste: "",
						place: index,
                        progress_amount: "",
                        img: "",
                        desc: "",
                        financer_list: "<p style='clear: both;'><?= Yii::t("cms", "No financer")?></p>"
                    };
                    if (notEmpty(dep.poste))
                        subs.poste = dep.poste;
                    if (notEmpty(dep.price))
                        subs.progress_amount = dataHelper.printf('<span>{{total}} <i class="fa fa-eur"></i></span> / <span>{{price}} <i class="fa fa-eur"></i></span>', {
                            total: total,
                            price: dep.price
                        });
                    if (formated_financer)
                        subs.financer_list = formated_financer;
                    if (typeof dep.description === "string")
                        subs.desc = "<p class='dep-desc'>" + dep.description + "</p>";
                    if (typeof dep.imageKey === "string" && dep.imageKey.length != 'null') {
                        subs.img = "<div class='dep-img' style='display : none;'><img src='' data-key='" + dep.imageKey + "' /></div>";
                        loading_images.push(dep.imageKey);
                    }
                    html.archived += dataHelper.printf(archived_financer_template_html, subs);

                });
                html.archived += `</div>`;
            }
			return (html.start + html.end + html.archived);
		}

		function getinputdepense(callback = function() {}) {
			$(`.financeftl-input`).html('<div class="co-popup-cofinance-loader-section"></div>');
			coInterface.showCostumLoader(".co-popup-cofinance-loader-section")
			ajaxPost(
				`.financeftl-input`,
				baseUrl + '/survey/answer/answer/id/' + projectAnswer._id.$id + '/form/' + projectAnswer.form + '/step/aapStep3/input/financer', {

				},
				function(data) {

				},
				null,
				"json", {
					async: true
				}
			)
		}

		sectionDyf.depenseDyfcms = {
			jsonSchema: {
				title: "<?= Yii::t("cms", "Estimated budget")?>",
				icon: "fa-money",
				text: "<?= Yii::t("cms", "Describe the main expense items here: what do the costs correspond to?")?> <br/><?= Yii::t("cms", "Specify what is an operating expense and what is a capital expenditure")?>.<br/><?= Yii::t("cms", "Costs must be exclusive of tax")?>.",
				properties: {
					"poste": {
						"inputType": "text",
						"label": "<?= Yii::t("cms", "Expense item")?>",
						"placeholder": "<?= Yii::t("cms", "Expense item")?>",
						"rules": {
							"required": true
						}
					},
					"description": {
						"inputType": "textarea",
						"label": "Description",
						"placeholder": "Description",
						"rules": {
							"required": false
						}
					},
					"image": {
						"label": "image",
						"placeholder": "",
						"inputType": "uploader",
						"docType": "image",
						"itemLimit": 1,
						"filetypes": ["jpeg", "jpg", "gif", "png"]
					},
					"price": {
						"inputType": "text",
						"label": "<?= Yii::t("cms", "Amount")?>",
						"placeholder": "<?= Yii::t("cms", "Amount")?>",
						"rules": {
							"required": true
						}
					}
				},
				afterBuild: function() {
					setTimeout(function() {
						//uploadObj.set("answers",projectAnswer._id.$id);
						$('.qq-upload-button-selector.btn.btn-primary').css({
							position: 'absolute',
							left: '50%',
							transform: 'translateX(-50%)'
						});
					}, 200);
					var bootboxClose = setInterval(() => {
						if ($("#modal-form-bootbox .bootbox-close-button").length > 0) {
							$("#modal-form-bootbox .bootbox-close-button")[0].addEventListener("click", function() {
								dyFObj.closeForm();
							})
							clearInterval(bootboxClose);
						}
					}, 700);
                    $('#price').attr('type','number');
				},
				save: function(data) {
					dyFObj.commonAfterSave(data, function() {

						var depensename = "";

						tplCtx.value = {
							date: "now"
						};

						$.each(sectionDyf.depenseDyfcms.jsonSchema.properties, function(k, val) {
							tplCtx.value[k] = $("#" + k).val();

							if (k == "poste")
								depensename = tplCtx.value[k];
							if (k == "description")
								tplCtx.value[k] = data.description;

						});


						if (notEmpty(tplCtx.keygen))
							tplCtx.value["imageKey"] = tplCtx.keygen;
						tplCtx.lastKeygen ? delete tplCtx.lastKeygen : "";

						if (typeof tplCtx.value != "undefined") {
							dataHelper.path2Value(tplCtx, function(params) {
								delete tplCtx.keygen;

								var tplCtxproject = {};
								$('#btn-submit-form').html('Envoi').prop('disabled', 'disabled');
								setTimeout(function() {
									reloadpasl(function() {
									});
                                    reloadpfpb(function() {
                                    });
									dyFObj.closeForm();
								}, "200");

							}, function(p) {

							});
						}
                        return false;
					});

					// nesorina fa mampisy bug ajout ligne de dépense avec image
					// delete tplCtx.setType;
				},
				beforeBuild: function(data) {
					uploadObj.set("answers", projectAnswer._id.$id);

					const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
					var rand = (min = 0, max = 1000) => Math.floor(Math.random() * (max - min) + min);
					randChar = (length = 6) => {
						var randchars = [];
						for (let i = 0; i < length; i++) {
							randchars.push(chars[rand(0, chars.length)]);
						}

						return randchars.join('');
					}
					keygenerator = (prefix = 'a-', sufix = '') => `${prefix}${randChar()}${sufix}`;
					var keygen = keygenerator();
					uploadObj.path += "/subKey/answers.aapStep1.depense.image/imageKey/" + (tplCtx.lastKeygen ? tplCtx.lastKeygen : keygen);
					tplCtx.keygen = tplCtx.lastKeygen ? tplCtx.lastKeygen : keygen;
				}
			}
		};

		jQuery(document).ready(function() {

			function content_reload() {
				ajaxPost(
					null,
					baseUrl + "/co2/cms/refreshblock", {
						idBlock: sectionDyf[php.kunik + "blockCms"]._id.$id,
						clienturi: window.location.href
					},
					function(data) {
						if (data.result) {
							$(`.cmsbuilder-block[data-id='${sectionDyf[php.kunik + "blockCms"]._id.$id}`).html(data["view"]);
							cmsBuilder.block.initEvent();
						} else {
							toastr.error("something went wrong!! please try again.");
						}
					},
					null
				)
			}

			function prepFileForFormsUploader(data){
				arrayList=[];
				$i=0;
				$.each(data, function(e, v){
					item=new Object;
					item.size=v.size,
					item.uuid=e,
					item.name=v.name,
					item.deleteFileEndpoint=baseUrl+"/"+moduleId+"/document/deletedocumentbyid/id";
					item.deleteFileParams={ids:e};
					if(typeof v.imageThumbPath != "undefined"){
						item.thumbnailUrl=v.imageThumbPath;
						item.pathUrl=v.docPath;
					}if(typeof v.docPath != "undefined"){
						item.thumbnailUrl=v.docPath;
						item.pathUrl=v.docPath;
					}
					//initListUploader[e].uuid=$i;
					$i++;
					arrayList.push(item);
				} );
				return arrayList;
			}
			/*let swiper ;
			swiper = new Swiper('#pasl-swiper-<?= $kunik ?>', {
			    loop: true,
			    slidesPerView: "auto",
			    autoplay : {
			        delay : 5000,
			        disableOnInteraction : true,
			        pauseOnHover : true,
			        pauseOnMouseEnter : true
			    },
			    slidesOffsetBefore : 0,
			    slidesOffsetAfter : 0,
			    direction : 'horizontal',
			    pagination : {
			        el : '.swiper-pagination',
			    },
			    navigation : {
			        nextEl : '.swiper-button-next',
			        prevEl : '.swiper-button-prev',
			    },
			    scrollbar : {
			        el : '.swiper-scrollbar'
			    },
			    allowTouchMove : false
			});

			$('#swiper-<?= $kunik ?>').hover(function (){
			    $(this).context.swiper.autoplay.stop();
			}, function (){
			    $(this).context.swiper.autoplay.start();
			});*/

			$('.statbutton-multiselect').off().on("click", function() {
				$('.pasl-container .swiper-wrapper').addClass('multiselect-active');
				$('.multiselect-group-button').addClass('multiselect-group-button-active');
				$('.multiselect-group-button-2').addClass('multiselect-group-button-active');
				$('.statbutton-multiselect-container').removeClass('statbutton-multiselect-active');
				$('.statbutton-group').addClass('flex')
				if ($('.depensecardview').length > 0) {
					$('.depensecardview').trigger('click');
				}
				validateCoFinancement();
				disturbAmount();
				if (notEmpty(userConnected)) {
					$('.input-name-multiselect').val(userConnected.name);
					$('.input-email-multiselect').val(userConnected.email);
				}
			});

			$('.statbutton-cancel-multiselect').off().on("click", function() {
				$('.pasl-container .swiper-wrapper').removeClass('multiselect-active');
				$('.multiselect-group-button').removeClass('multiselect-group-button-active');
				$('.multiselect-group-button-2').removeClass('multiselect-group-button-active');
				$('.statbutton-multiselect-container').addClass('statbutton-multiselect-active');
				$('.financementprogress-card-chkb').prop('checked', false);
				$('.statbutton-group').removeClass('flex')
			});

			$('.statbutton-cofinance-index').off().on("click", function() {
				var thisbtn = $(this);
				if ($('input#action-' + thisbtn.data('id')).prop('checked')) {
					$('input#action-' + thisbtn.data('id')).prop('checked', false);
					thisbtn.html('Sélectionner');
					thisbtn.removeClass('active');
				} else {
					$('input#action-' + thisbtn.data('id')).prop('checked', true);
					thisbtn.html('Desélectionner');
					thisbtn.addClass('active');
				}
				if ($('.financementprogress-card-chkb:checked').length == $('.financementprogress-card').length) {
					$('.statbutton-sellectall').html('<i class="fa fa-times-square"></i>');
				} else {
					$('.statbutton-sellectall').html('<i class="fa fa-check-square"></i>');
				}
				var actionckd = [];
				$('.financementprogress-card-chkb:checked').each(function() {
					var thischbk = $(this);
					if (!actionckd.includes(thischbk.data('id'))) {
						actionckd.push(thischbk.data('id'));
					}
				});
				$('.action-select-count').html(actionckd.length);
				validateCoFinancement();
				disturbAmount();
			});

			$('.financementprogress-card-chkb').on('change', function() {
				if ($('.financementprogress-card-chkb:checked').length == $('.financementprogress-card').length) {
					$('.statbutton-sellectall').html('<i class="fa fa-times-square"></i>');
				} else {
					$('.statbutton-sellectall').html('<i class="fa fa-check-square"></i>');
				}
				var actionckd = [];
				$('.financementprogress-card-chkb:checked').each(function() {
					var thischbk = $(this);
					if (!actionckd.includes(thischbk.data('id'))) {
						actionckd.push(thischbk.data('id'));
					}
				});
				$('.action-select-count').html(actionckd.length);
				validateCoFinancement();
				disturbAmount();
			});

			$('.statbutton-sellectall').off().on("click", function() {
				var thisbtn = $(this);
				if ($('.financementprogress-card-chkb:checked').length == $('.financementprogress-card').length) {
					thisbtn.html('<i class="fa fa-check"></i>');
					$('.statbutton-cofinance-index').html('Sélectionner');
					$('.financementprogress-card-chkb').prop('checked', false);
				} else {
					thisbtn.html('<i class="fa fa-times"></i>');
					$('.statbutton-cofinance-index').html('Desélectionner');
					$('.financementprogress-card-chkb').prop('checked', true);
				}
				var actionckd = [];
				$('.financementprogress-card-chkb:checked').each(function() {
					var thischbk = $(this);
					if (!actionckd.includes(thischbk.data('id'))) {
						actionckd.push(thischbk.data('id'));
					}
				});
				$('.action-select-count').html(actionckd.length);
				validateCoFinancement();
				disturbAmount();
			});

			$('.multiselect-group-button .statbutton-cofinance').off().on("click", function() {
				var actionckd2 = [];
				var lineToPay = [];
				$('.financementprogress-card-chkb:checked + .financementprogress-card').each(function() {
					var thisid = $(this).data('id');
					if (!actionckd2.includes(thisid)) {
						lineToPay.push({
							id: thisid,
							amount: $('.ecotact[data-id="' + thisid + '"]').data('ecotact')
						});
						actionckd2.push(thisid);
					}
				});

				var tplCtx = {
					id: projectAnswer._id.$id,
					collection: "answers",
					arrayForm: true,
					form: "aapStep1",
					setType: [{
							"path": "amount",
							"type": "int"

						},
						{
							"path": "date",
							"type": "isoDate"
						}
					]
				}

				$.each(lineToPay, function(index, value) {

					tplCtx.path = "answers.aapStep1.depense." + value.id + ".financer";
					var today = new Date();
					today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
					tplCtx.value = {
						line: '',
						amount: value.amount,
						user: userId,
						date: today,
						name: $('.input-name-multiselect').val(),
						email: $('.input-email-multiselect').val(),
						id: userConnected._id.$id
					}

					if (value.amount > 0) {
						dataHelper.path2Value(tplCtx, function(params) {

						});
					}
				});
				//urlCtrl.loadByHash(location.hash);
				if ($(`.cmsbuilder-block[data-id='${sectionDyf[php.kunik + "blockCms"]._id.$id}`).append('<div class="aap-cover-spin"></div>')) {
					content_reload();
				}

				validateCoFinancement();
				disturbAmount();
			});

			$('.input-amount-multiselect').on('keyup', function() {
				validateCoFinancement();
				disturbAmount();
			});

			$('body').off().on('focus', '.pasl-container [contenteditable]', function() {
				const $thisContEdble = $(this);
				$thisContEdble.prop('data-before', $thisContEdble.html());
			}).off().on('keyup', '.pasl-container [contenteditable]', function() {
				const $thisContEdble = $(this);
				if ($thisContEdble.data('before') !== $thisContEdble.html()) {
					$thisContEdble.data('before', $thisContEdble.html());
					$thisContEdble.trigger('change');
				}
			})

			$('body').on('change', '.pasl-container [contenteditable]', function() {
				var thiseot = $(this);
				var thiseotElm = this;
				var amountInserted = parseInt($('.input-amount-multiselect').val());
				if (isNaN(parseInt(thiseot.html())) || parseInt(thiseot.html()) < 0) {
					thiseot.html('0');
				}

				/*if($('.ecotact').length == $('.ecotact[data-changed]').length){
				    var totalpart = 0;

				    var actionckd1bis = [];
				    $('.financementprogress-card-chkb:checked + .financementprogress-card .resteact').each(function (){
				        var thisctxt = $(this);
				        if (!actionckd1bis.includes(thisctxt.data('id'))) {
				            totalpart += parseInt($('.ecotact[data-id="'+thiseot.data("id")+'"]').data('ecotact'));
				            actionckd1bis.push(thisctxt.data('id'));
				        }
				    });

				    if(amountInserted > totalpart + parseInt(thiseot.html())){
				        thiseot.html(amountInserted - totalpart);
				    }
				}else{*/
				if (amountInserted < parseInt(thiseot.html())) {
					thiseot.html(amountInserted);
				}
				//}


				if (thiseot.html() > $('.resteact[data-id="' + thiseot.data("id") + '"]').data('reste')) {
					thiseot.html($('.resteact[data-id="' + thiseot.data("id") + '"]').data('reste'));
				}

				$('.ecotact[data-id="' + thiseot.data("id") + '"]').data('changed', 'true');
				$('.ecotact[data-id="' + thiseot.data("id") + '"]').data('ecotact', thiseot.html());

				var totalResteAuto = 0;
				var totalResteEdited = 0;
				var totalChanged = 0;

				var actionckd1 = [];
				$('.financementprogress-card-chkb:checked + .financementprogress-card .resteact').each(function() {
					var thisctxt = $(this);
					if (!actionckd1.includes(thisctxt.data('id'))) {
						if ($('.ecotact[data-id="' + thisctxt.data("id") + '"]').data('changed') != 'true') {
							totalResteAuto += parseInt(thisctxt.data('reste'));
						} else {
							totalChanged += parseInt($('.ecotact[data-id="' + thiseot.data("id") + '"]').data('ecotact'));
							totalResteEdited += parseInt(thisctxt.data('reste'));
						}
						actionckd1.push(thisctxt.data('id'));
					}
				});

				var amountAccepted = amountInserted - totalChanged;

				var totalReste = totalResteAuto + totalResteEdited;

				var actionckd2 = [];
				$('.financementprogress-card-chkb:checked + .financementprogress-card').each(function() {
					var thisid = $(this).data('id');
					if (
						!actionckd2.includes(thisid) &&
						$('.ecotact[data-id="' + thisid + '"]').data('changed') != 'true'
					) {
						var reste = parseInt($('.resteact[data-id="' + thisid + '"]').data('reste'));

						if (!isNaN(Math.round(((reste / totalReste) * amountAccepted)))) {
							$('.ecotact[data-id="' + thisid + '"]').html('<span data-id="' + thisid + '" class="ecotenteditable" contenteditable="true">' + Math.round((reste / totalResteAuto) * amountAccepted) + '</span>€');
							$('.ecotact[data-id="' + thisid + '"]').data('ecotact', Math.round((reste / totalResteAuto) * amountAccepted));
						} else {
							$('.ecotact[data-id="' + thisid + '"]').html('<span data-id="' + thisid + '" class="ecotenteditable" contenteditable="true">0</span>€');
							$('.ecotact[data-id="' + thisid + '"]').data('ecotact', '0');
						}

						actionckd2.push(thisid);
					}
				});
			});

			$(".financeftl-list").html(depenseList(projectAnswer, depense_images , archivedProjectAnswer));
			$('.supLineDep').click(function(){
				if((typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || (userCanEdit == true)) {
					var depenseIndex = $(this).parent().data('index');
					var placeIndex = $(this).parent().data('place');
					var depense = projectAnswer.answers.aapStep1.depense[placeIndex];
					if (typeof depense != 'undefined' && typeof depense['financer'] != 'undefined' && depense['financer'].length > 0) {
						toastr.error("Vous ne pouvez pas supprimer cette ligne de dépense car elle a déjà été financée!");
						return;
					}
					var params = {
						id: projectAnswer._id.$id,
						collection: 'answers',
						path: 'answers.aapStep1.depense.' + depenseIndex ,
						pull: 'answers.aapStep1.depense',
						value: null
					}
					$.confirm({
						title: "<?= Yii::t("cms", "Delete this financing")?>",
						content: "<?= Yii::t("cms", "This action will remove this funding and it will be irreversible")?>.",
						buttons: {
							yes: {
								text: '<?= Yii::t("cms", "Delete")?>',
								btnClass: 'btn btn-danger',
								action: function() {
									mylog.log(params, '_response')
									dataHelper.path2Value(params, function(__response) {
										if (__response.result && __response.result === true) {
											mylog.log(__response.result, '_response')
											if ($(`[data-refreshblock-suppr]`).length > 0) {
												$(`[data-refreshblock-suppr]`).each((index, elem) => {
													const blockId = $(elem).attr("data-refreshblock-suppr")
													if (typeof refreshBlock == "function") {
														refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
													}
												})
											}
											toastr.success("<?= Yii::t("cms", "Delete expense line")?>!");
										}
									});
								}
							},
							no: {
								text: coTranslate('cancel'),
								btnClass: 'btn btn-default'
							}
						}
					});
				}
			})
			$('.editLineDep').click(function(){
				if((typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || (userCanEdit == true)) {
					var keygen;
					var initListHmtl;
					var depenseIndex = $(this).parent().data('index');
					var placeIndex = $(this).parent().data('place');
					var mode = $(this).parent().data('mode');
					var depense = projectAnswer.answers.aapStep1.depense[placeIndex];
					if(mode == 'archive'){
						depense = archivedProjectAnswer.answers.aapStep1.depense[placeIndex];
					}
					console.log(depense, 'depense')
					// var depenseId = depense.actionId;
					var depenseData = {
						"poste": depense.poste,
						"description": depense.description ?? null,
						"price": depense.price,
						"image": depense.imageKey ?? null
					}
					mylog.log(depenseData, 'depenseData')
					mylog.log($("img[data-key='" + depenseData.image + "']").attr("src"), 'imageUrl')

					var dynObj = {
						jsonSchema: {
							title: "<?= Yii::t("cms", "Estimated budget")?>",
							icon: "fa-money",
							text: "<?= Yii::t("cms", "Describe the main expense items here: what do the costs correspond to?")?> <br/><?= Yii::t("cms", "Specify what is an operating expense and what is a capital expenditure")?>.<br/><?= Yii::t("cms", "Costs must be exclusive of tax")?>.",
							properties: {
								"poste": {
									"inputType": "text",
									"label": "<?= Yii::t("cms", "Expense item")?>",
									"placeholder": "<?= Yii::t("cms", "Expense item")?>",
									"rules": {
										"required": true
									}
								},
								"description": {
									"inputType": "textarea",
									"label": "Description",
									"placeholder": "Description",
									"rules": {
										"required": false
									}
								},
								"image": {
									"label": "image",
									"placeholder": "",
									"inputType": "uploader",
									"docType": "image",
									"itemLimit": 1,
									"filetypes": ["jpeg", "jpg", "gif", "png"]
								},
								"price": {
									"inputType": "text",
									"label": "<?= Yii::t("cms", "Amount")?>",
									"placeholder": "<?= Yii::t("cms", "Amount")?>",
									"rules": {
										"required": true
									}
								}
							},
							afterBuild: function() {
								setTimeout(function() {
									$('.qq-upload-button-selector.btn.btn-primary').css({
										position: 'absolute',
										left: '50%',
										transform: 'translateX(-50%)'
									});
								}, 200);
								var bootboxClose = setInterval(() => {
									if ($("#modal-form-bootbox .bootbox-close-button").length > 0) {
										$("#modal-form-bootbox .bootbox-close-button")[0].addEventListener("click", function() {
											dyFObj.closeForm();
										})
										clearInterval(bootboxClose);
									}
								}, 700);

							},
							save: function(data) {
								dyFObj.commonAfterSave(data, function() {
									if($('.qq-upload-list').is(':empty')) {
										depense["imageKey"] = null;
									} else if(!$('.qq-upload-cancel-selector').hasClass('qq-hide')) {
										depense["imageKey"] = keygen;
									}
									depense["poste"] = $('#poste').val();
									depense["description"] = $('#description').val();
									depense["price"] = $('#price').val();
									var params = {
										id: projectAnswer._id.$id,
										collection: 'answers',
										path: 'answers.aapStep1.depense.' + depenseIndex,
										value: depense,
									}

									dataHelper.path2Value(params, function(__response) {
										if (__response.result && __response.result === true) {
											mylog.log(__response.result, '_response')
											if ($(`[data-refreshblock-suppr]`).length > 0) {
												$(`[data-refreshblock-suppr]`).each((index, elem) => {
													const blockId = $(elem).attr("data-refreshblock-suppr")
													if (typeof refreshBlock == "function") {
														refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
													}
												})
											}
											toastr.success("<?= Yii::t("cms", "Expense line modify")?>!");
											dyFObj.closeForm();
										}
									});
								});
							},
							beforeBuild: function(data) {
								uploadObj.set("answers", projectAnswer._id.$id);

								const chars = 'azertyuiopqsdfghjklmwxcvbn123456789';
								var rand = (min = 0, max = 1000) => Math.floor(Math.random() * (max - min) + min);
								randChar = (length = 6) => {
									var randchars = [];
									for (let i = 0; i < length; i++) {
										randchars.push(chars[rand(0, chars.length)]);
									}
									return randchars.join('');
								}
								keygenerator = (prefix = 'a-', sufix = '') => `${prefix}${randChar()}${sufix}`;
								keygen = keygenerator();
								uploadObj.path += "/subKey/answers.aapStep1.depense.image/imageKey/" + keygen;
								
							},
							onLoads : {
								onload : function() {
									$("#poste").val(depenseData.poste);
									$("#description").val(depenseData.description);
									$("#price").val(depenseData.price);
									var keyIndex = php.initAnswerFiles.filter(fichier => fichier.imageKey === depenseData.image);

									if(keyIndex.length > 0){
										$("#imageUploader").fineUploader("addInitialFiles", prepFileForFormsUploader(keyIndex));
									}
								},
								afterLoad : function(data){

								}
							}
						}
					}
					dyFObj.openForm(dynObj, null, null, null, null, {
						type: "bootbox",
						notCloseOpenModal: true,
					});
				} else {
					toastr.info("Vous n'avez pas les droits pour modifier cette ligne de dépense");
				}
			})
            $('.hideLineDep').click(function(){
                if((typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || (userCanEdit == true)) {
                    var depenseIndex = $(this).parent().data('index');
                    var params = {
                        id: projectAnswer._id.$id,
                        collection: 'answers',
                        path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                        value: 'false',
                        setType : 'bool'
                    }
                    $.confirm({
                        title: "Cacher momentanément ce ligne de depense et ses financements",
                        content: `Cette action va cacher momentanément ce ligne de depense et ses financements dans les données pris en compte par la plateforme."`,
                        buttons: {
                            yes: {
                                text: 'Cacher',
                                btnClass: 'btn btn-danger',
                                action: function() {
                                    mylog.log(params, '_response')
                                    dataHelper.path2Value(params, function(__response) {
                                        if (__response.result && __response.result === true) {
                                            mylog.log(__response.result, '_response')
                                            if ($(`[data-refreshblock-suppr]`).length > 0) {
                                                $(`[data-refreshblock-suppr]`).each((index, elem) => {
                                                    const blockId = $(elem).attr("data-refreshblock-suppr")
                                                    if (typeof refreshBlock == "function") {
                                                        refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                                    }
                                                })
                                            }
                                        }
                                    });
                                }
                            },
                            no: {
                                text: coTranslate('cancel'),
                                btnClass: 'btn btn-default'
                            }
                        }
                    });
                }
            })
            $('.showLineDep').click(function(){
                if((typeof costum != "undefined" && costum != null && costum.isCostumAdmin) || (userCanEdit == true)) {
                    var depenseIndex = $(this).parent().data('index');
                    var params = {
                        id: projectAnswer._id.$id,
                        collection: 'answers',
                        path: 'answers.aapStep1.depense.' + depenseIndex + '.include',
                        value: 'true',
                        setType : 'bool'
                    }
                    $.confirm({
                        title: "Désarchiver ce ligne de depense et ses financements",
                        content: `Cette action va désarchiver ce ligne de depense et ses financements et l'ajouter dans les données pris en compte par la plateforme."`,
                        buttons: {
                            yes: {
                                text: 'Désarchiver',
                                btnClass: 'btn btn-danger',
                                action: function() {
                                    mylog.log(params, '_response')
                                    dataHelper.path2Value(params, function(__response) {
                                        if (__response.result && __response.result === true) {
                                            mylog.log(__response.result, '_response')
                                            if ($(`[data-refreshblock-suppr]`).length > 0) {
                                                $(`[data-refreshblock-suppr]`).each((index, elem) => {
                                                    const blockId = $(elem).attr("data-refreshblock-suppr")
                                                    if (typeof refreshBlock == "function") {
                                                        refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
                                                    }
                                                })
                                            }
                                        }
                                    });
                                }
                            },
                            no: {
                                text: coTranslate('cancel'),
                                btnClass: 'btn btn-default'
                            }
                        }
                    });
                }
            })
			$('.qq-upload-delete').click(function(){
				mylog.log('delete')
			})
			$('.cancel-promess').click(function() {
				var self = $(this)
				$.confirm({
					title: "Supprimer ce financement",
					content: "Cette action supprimera ce financement et ce sera irreversible.",
					buttons: {
						yes: {
							text: 'Supprimer',
							btnClass: 'btn btn-danger',
							action: function() {
								var answerId = self.data('answerid')
								var indexDepenese = self.parent().parent().data('index')
								var indexFinance = self.data('financerindex')
								var params = {
									id: answerId,
									collection: 'answers',
									path: 'answers.aapStep1.depense.' + indexDepenese + '.financer.' + indexFinance,
									pull: 'answers.aapStep1.depense.' + indexDepenese + '.financer',
									value: null
								}
								mylog.log(params, '_response')
								dataHelper.path2Value(params, function(__response) {
									if (__response.result && __response.result === true) {
										mylog.log(__response.result, '_response')
										// self.parent().remove()
										if ($(`[data-refreshblock-suppr]`).length > 0) {
											$(`[data-refreshblock-suppr]`).each((index, elem) => {
												const blockId = $(elem).attr("data-refreshblock-suppr")
												if (typeof refreshBlock == "function") {
													refreshBlock(blockId, `.cmsbuilder-block[data-id="${blockId}"]`)
												}
											})
										}
										toastr.success("Financement supprimer!");
									}
								});
							}
						},
						no: {
							text: coTranslate('cancel'),
							btnClass: 'btn btn-default'
						}
					}
				});
			})

			$('.tglv').off().on("click", function() {
				var thisbtn = $(this);
				if (!thisbtn.hasClass('active')) {
					$('.tglv').removeClass('active');
					thisbtn.addClass('active');
					if (thisbtn.hasClass('depensetableview')) {
						$('.financeftl-list').show();
						$('.pasl-container .swiper').hide();
						$('.financeftl-input').hide();
					} else if (thisbtn.hasClass('depensecardview')) {
						$('.financeftl-list').hide();
						$('.pasl-container .swiper').show();
						$('.financeftl-input').hide();
					} else {
						$('.financeftl-list').hide();
						$('.pasl-container .swiper').hide();
						$('.financeftl-input').show();
					}
				}

			});

			$('.addDepensecms').off().on("click", function() {
				tplCtx = {
					id: projectAnswer._id.$id,
					collection: "answers",
					arrayForm: true,
					form: "aapStep1",
					setType: [{
							"path": "price",
							"type": "int"
						},
						{
							"path": "date",
							"type": "isoDate"
						}
					],

				}

				tplCtx.path = "answers.aapStep1.depense";

				dyFObj.openForm(sectionDyf.depenseDyfcms, null, null, null, null, {
					type: "bootbox",
					notCloseOpenModal: true,
				});
			});
			if (depense_images.length)
				setTimeout(function() {
					aap.xhr()
						.depense_images(depense_images)
						.then(function(images) {
							$.each(images, function(key, path) {
								$("img[data-key='" + key + "']").attr("src", path);
								$("img[data-key='" + key + "']").parent().attr("style", "display : inline-block;");
							})
						});
				}, 2000);
		});
	})(window, jQuery);
</script>