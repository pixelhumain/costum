<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $objectCss = $blockCms["css"] ?? [];
  $styleCss  = (object) [$kunik => $objectCss];
  $keyTpl = "formsList";
  $blockCms["coformSlug"] = $blockCms["coformSlug"] == "" ? $costum["contextSlug"] :  $blockCms["coformSlug"];
?>

<style id="css-<?= $kunik ?>">
  .card<?= $kunik ?>{
    margin-top: 1.5em;
    margin-bottom: 1.5em;
    padding:.6em 1.2em;
    border-radius: 5px;
    background-color: #eee;
    font-size: 12pt !important;
  }

  .card<?= $kunik ?> p{
    font-size: 15pt !important;
    font-weight: bold;
    color: #777;
    margin-left: 1.3em;
  }

  .title<?= $kunik ?>{
    color: <?= $blockCms["css"]["formListCss"]["color"] ?>;
    text-transform: uppercase;
  }

  .calendar-icon<?= $kunik ?>{
    border-radius: 50%;
    padding: 0.5em 0.6em;
    border-left: 4px solid <?= $blockCms["css"]["formListCss"]["color"] ?>;
    border-bottom: 4px solid <?= $blockCms["css"]["formListCss"]["color"] ?>;
    border-top: 4px solid <?= $blockCms["css"]["formListCss"]["color"] ?>;
  }

  .btn-answer<?= $kunik ?>{
    background-color: <?= $blockCms["css"]["formListCss"]["color"] ?>;
    border-radius: 40px;
    padding: .7em 1.5em;
    color:white;
  }

  .btn-answers<?= $kunik ?>{
    background-color: white;
    border-radius: 40px;
    padding: .7em 1.5em;
    color: <?= $blockCms["css"]["formListCss"]["color"] ?>;
    margin-left: 1em;
  }

  .btn-config<?= $kunik ?>{
    background-color: white;
    border-radius: 40px;
    padding: .7em 1.5em;
    margin-left: 1em
  }

  .btn-<?= $kunik ?>{
    font-weight: bold;
    text-transform: uppercase;
    transition-property: all;
    transition-duration: .5s;
  }

  .btn-createForm<?= $kunik ?>{
    background-color: #eee;
    border-radius: 0px;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    padding: .6em 1.4em;
    text-transform: uppercase;
  }

  .btn-templateList<?= $kunik ?>{
    background-color: <?= $blockCms["css"]["formListCss"]["color"] ?>;
    border-radius: 0px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    padding: .6em 1.4em;
    text-transform: uppercase;
    color: white;
  }

  .btn-container<?= $kunik ?>{
    margin: 1.5em 0em .5em 0em;
    justify-content: space-between !important;
  }

  .context-icon<?= $kunik ?>{
    font-weight: bolder;
  }

  #central-container{
    background-color:white;
    min-height: auto !important;
  }

  .calendar<?= $kunik ?>{
    margin-right: 1em;
  }

  a .fa{
    margin-right: .6em;
  }

  h1, .h1, h2, .h2, h3, .h3 {
      margin-top: 20px !important;
      margin-bottom: 10px !important;
  }
</style>
  <div class="<?= $kunik ?>">
    <div class="container margin-top-10 col-xs-12">
      <a id="backtoinitial" class="btn btn-default btn-<?= $kunik ?>" style="border-radius: 40px" href="javascrit:;"><i class="fa fa-arrow-left"></i>Retour</a>
    </div>
    <div id="central-container"></div>
  </div>
  <?php
    if (isset($costum["contextType"]) && isset($costum["contextId"])) {
      $formAssets = [
        '/js/form.js', '/css/form.css'
      ];
      HtmlHelper::registerCssAndScriptsFiles(
        $formAssets,
        Yii::app()->request->baseUrl . Yii::app()->getModule("survey")->getAssetsUrl()
      );
    }

  ?>

  <?php 
    $adminForm = [];
    if(!empty($blockCms["coformDisplay"])){
      foreach($blockCms["coformDisplay"] as $kf => $vf){
        if(Form::isFormAdmin($vf)){
          $adminForm[$vf] = true;
        }else{
          $adminForm[$vf] = false;
        }
      }
    } 
  ?> 
  
<script type="text/javascript">
  var allCoform = {}
  var slugParent = {}
  var strOptions = "";
  function getAllParent(){
    slugParent = {}
    ajaxPost(
      null, 
      baseUrl+'/survey/form/getallparent/userId/'+userId, 
      null,
      function(data){
        slugParent = data;
      },null,null,{async:false}
    );
    return slugParent;
      
  }
  function getAllCoform(slug,html=false){
    strOptions = "";
    ajaxPost(
      null, 
      baseUrl+'/survey/form/get/slug/'+slug+'/tpl/json', 
      null,
      function(data){
        var str = "";
        if(typeof data != "undefined" && data != null &&
          data.forms != "undefined" && data.forms != null){
          $.each( data.forms , function(kf,valf) {
            if(html){
              strOptions += `<option value="${kf}" data-value="">${valf.name}</option>`
            }else{
              allCoform[kf] = valf.name;
            }
            
          }); 
        }        
      },null,null,{async:false}
    );
    if(!html)
      return allCoform;
    else 
      return strOptions
  }
  sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
  adminForm = <?php echo json_encode($adminForm); ?>;

    slugParent = getAllParent() ;
    allCoform = getAllCoform(sectionDyf.<?php echo $kunik ?>blockCms.coformSlug);
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    if (costum.editMode) {
      cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
      var formsListInput = {
        configTabs: {
          general: {
            inputsConfig: [
              {
                type : "inputSimple",
                options : {
                  name : "title",
                  label : tradCms.title,
                  collection : "cms"
                }
              },
              {
                type : "inputSimple",
                options : {
                  name : "btnNewFormText",
                  label : tradCms.textForAddNewFormButton,
                  collection : "cms"
                }
              },
              {
                type : "inputSimple",
                options : {
                  name : "btnTemplateText",
                  label : tradCms.textForModelSelectionButton,
                  collection : "cms"
                }
              },
              {
                type : "inputSimple",
                options : {
                  name : "btnAnswerText",
                  label : tradCms.textForTheReplyFormButton,
                  collection : "cms"
                }
              },
              {
                type : "inputSimple",
                options : {
                  name : "btnListAnswersText",
                  label : tradCms.textForButtonDisplayListAnswers,
                  collection : "cms"
                }
              },
              {
                type : "inputSimple",
                options : {
                  name : "btnConfigText",
                  label : tradCms.textForButtonSetForm,
                  collection : "cms"
                }
              },
              {
                type : "select",
                options : {
                  name : "coformSlug",
                  label : tradCms.sourcesDisplay,
                  class : "<?= $kunik ?>-coformSlug",
                  options : $.map( slugParent, function( key, val ) {
                    return {
                      value: val,
                      label: key
                    }
                  })
                }
              },
              {
                type : "selectMultiple",
                options : {
                  name : "coformDisplay",
                  label : tradCms.coformsDisplay,
                  class : "<?= $kunik ?>-coformDisplay",
                  options : $.map( allCoform, function( key, val ) {
                    return {
                      value: val,
                      label: key
                    }
                  })
                }
              },
              {
                type : "groupButtons",
                options : {
                  name : "activeOnly",
                  label : tradCms.displayOnlyActiveForms,
                  options : [
                    {
                      value : false,
                      label : trad.no
                    },
                    {
                      value : true,
                      label : trad.yes
                    }
                  ],
                  defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].activeOnly
                }
              }
            ]
          },
          style: {
            inputsConfig: [
              {
                type : "section",
                options : {
                    name : "formListCss",
                    label : tradCms.chooseThemeColor,
                    inputs : [
                        "color"
                    ]
                }
              }
            ]
          },
          advanced: {
            inputsConfig: [
              "addCommonConfig"
            ]
          },
        },
        afterSave: function(path,valueToSet,name,payload,value) {
          cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
        },
        onChange: function(path,valueToSet,name,payload,value){
          if (name == "coformSlug"){
            allCoform = getAllCoform( $(".<?= $kunik ?>-coformSlug select").val(),true );
            $(".select2-search-choice").hide()
            $(".<?= $kunik ?>-coformDisplay select").val("")  
            
            var tplCtx = {};
            tplCtx = {
              id : "<?= $myCmsId ?>",
              value : null,
              path : "coformDisplay",
              collection: "cms"
            }
            dataHelper.path2Value( tplCtx, function(params) {})
            $(".<?= $kunik ?>-coformDisplay select").html(allCoform).select2();
          }
        }
      }
      cmsConstructor.blocks.formsList<?= $myCmsId ?> = formsListInput;
    }

  contextData = costum;
  if(window.location.hash==""){
    window.location.hash="#welcome";
  } else {
    window.location.hash=window.location.hash.split(".")[0];
  }
  var coFormObj = formObj;
  coFormObj.views.forms = function(fObj, forms){
    var str = '';
    str += '<div id="formList">'+
        '<div class="col-xs-12 col-lg-10 col-lg-offset-1">'+
          '<div class="page__title">'+
            '<h2 class="title<?=$kunik?> sp-text pull-left"  id="sp-<?= $blockKey ?>"  data-id="<?= $blockKey ?>" data-field="title"> <?= $blockCms["title"]; ?> </h2>';
    str +=		' </div>';
    str += 		'<div class="col-xs-12 no-padding" role="row">';
    if(notEmpty(sectionDyf.<?php echo $kunik ?>blockCms.coformDisplay)){
      $.each(sectionDyf.<?php echo $kunik ?>blockCms.coformDisplay,function(kcf,vcf){
        if(forms[vcf]){
          str += fObj.views.newform(vcf, forms[vcf], fObj);
        }
      })
    } else {
      $.each(forms, function(idF, valForm){
            str += fObj.views.newform(idF, valForm, fObj);
      });
    }
    str += ' 	</div>'
        '</div>'+
    '</div>';
    $(fObj.container).html(str);
    fObj.events.form(fObj);
    fObj.events.add(fObj);
    fObj.events.share(fObj);
    fObj.events.answers(fObj);
  }
  coFormObj.urls.forms = function(fObj){
    ajaxPost('#central-container', baseUrl+'/survey/form/get/slug/'+sectionDyf.<?php echo $kunik ?>blockCms.coformSlug+'/tpl/json', 
    null,
    function(data){
      var str = "";
      if(typeof data != "undefined" && data != null &&
        data.forms != "undefined" && data.forms != null){
        fObj.el = data.el;
        fObj.openFormList = fObj.openForm.list();
        fObj.views.forms(fObj, data.forms);
      }
    },"html");
  }
  coFormObj.views.newform = function(id, form, fObj){
    var panelColor = "panel-primary";
    var formCostum = false;
    var urlCostum = "";
    var datatypeform = "";
    var elconfigslug;
		var today = new Date();

    if (typeof form.type != "undefined" && (form.type === "aap" || form.type === "templatechild") && typeof form.config != "undefined"){
      ajaxPost(
        null,
        baseUrl+"/survey/form/getaapconfig/formid/"+form.config,
        null,
        function(data){
          elconfigslug = data;
        },
        null,
        "",
        {async: false}
      );
    }
    if( typeof form.source != "undefined" &&
      typeof form.source.insertOrign != "undefined" &&
      form.source.insertOrign == "costum"){
      panelColor = "panel-success";
      formCostum = true;
      urlCostum = baseUrl+'/costum/co/index/id/'+form.source.key;
    }
    form.active = (form.active === true || form.active === "true");
    form.hasObservatory = true;
    if( typeof form.openForm != "undefined" && form.openForm == true){
      datatypeform = 'data-type="openform"';
    }
    if( typeof form.description == "undefined" || form.description == null ){
      form.description = "<b> ("+trad.nodescription+") </b>";
    }

    if( typeof form.startDate == "undefined" || form.startDate == null ){
      form.startDate = " ("+trad.notSpecified+") ";
    }else{
			form.startDate = (typeof form.startDate === 'object' && typeof form.startDate.sec === 'number' ? moment.unix(form.startDate.sec).format('DD/MM/YYYY') : form.startDate);
    }
    if( typeof form.endDate == "undefined" || form.endDate == null ){
      form.endDate = " ("+trad.notSpecified+") ";
    }else{
			form.endDate = (typeof form.endDate === 'object' && typeof form.endDate.sec === 'number' ? moment.unix(form.endDate.sec).format('DD/MM/YYYY') : form.endDate);
    }
    var str = "";
    if(typeof form.type != "undefined" && (form.type == "aapConfig"))
    {}
    else
    {
      var sbsl = [];
      if(typeof form != "undefined" && typeof form.subForms != "undefined") {
        $.each(form.subForms, function (ind, val) {
          if (typeof form[val] != "undefined" && form[val] != null && typeof form[val].name != "undefined") {
            sbsl.push(form[val].name);
          }
        });
      }
      <?php if($blockCms["activeOnly"]=="true" ){  echo "if(form.active){";  } ?>
        str += `
          <div class="card card<?= $kunik ?>">
            <h3><i class="fa fa-wpforms context-icon<?= $kunik ?>"></i> ${form.name}</h3>
            <p>${form.description}</p>
            <span class="calendar<?= $kunik ?>">
              <i class="fa fa-calendar calendar-icon<?= $kunik ?>"></i> <b>${trad.start}</b> : ${form.startDate}
            </span>
            <span class="calendar<?= $kunik ?>">
              <i class="fa fa-calendar calendar-icon<?= $kunik ?>"></i> <b>${trad.end}</b> : ${form.endDate}
            </span>
            <div class="btn-container<?= $kunik ?>">`;
              if(form.type != "aap" || typeof form.type == "undefined"){
                str += (form.active && isUserConnected == "logged") ?	'<a href="javascript:;" data-id="'+id+'" data-parentformid="'+form._id.$id+'" '+datatypeform+' class="addAnswer btn btn-answer<?= $kunik ?> btn-<?= $kunik ?> margin-left-10" data-toggle="tooltip" data-placement="bottom" data-original-title="Répondre au formulaire"><i class="fa fa-plus"></i> <?= $blockCms["btnAnswerText"] ?></a>':'';
              }
              if(adminForm[id]) {  
                str += `<a href="javascript:;" data-id="${id}" data-form="${escapeHtml(JSON.stringify(form))}" data-subselect="${escapeHtml(JSON.stringify(sbsl))}" class="btnConfigForm btn btn-config<?= $kunik ?> btn-<?= $kunik ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Modifier les details et la description du formulaire"><i class="fa fa-cogs"></i> <?= $blockCms["btnConfigText"] ?></a>`;
              }  
        str += `</div> </div>`;
      <?php if($blockCms["activeOnly"]=="true"){ echo "}"; } ?>
    }
    return str;
  }
  function init(){ 
    if(typeof hashUrlPage == "undefined"){
      hashUrlPage = "#@"+costum.slug;
    }
    $("#central-container").empty();
    $("#central-container").append(formObj.urls.forms(formObj));
    
    $("#backtoinitial").hide();
  }
  init();
  $("#backtoinitial").on("click", function(){
    init();
  })
  $(document).on("click",".btn-answer<?= $kunik ?>", function(e){
    $("#backtoinitial").show();
  })
</script>