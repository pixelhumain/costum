<?php
$myCmsId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];
$canCofinance = $blockCms["checkDate"] == "include" ? true : false;
$imgUrl = Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg';
$porteurName = $blockCms["campPorteur"]["text"] ?? "France Tiers Lieux";
$porteurImg = $blockCms["porteurImg"];
$porteurId = $blockCms["campPorteur"]["id"] ?? "";
// Trouver les index des éléments dans le tableau
$index_main = null;
$index_to_remove = null;

if(isset($costum) && isset($costum["contextId"]) && $blockCms["financerData"]){
	foreach ($blockCms["financerData"] as $key => $item) {
		if ($item['id'] === $costum["contextId"]) {
			$index_main = $key;
		}
		if ($item['name'] === $porteurName) {
			$index_to_remove = $key;
		}
	}
}

// Vérifier si les deux éléments existent
if ($index_main !== null && $index_to_remove !== null) {
	// Ajouter le montant
	$blockCms["financerData"][$index_main]['amount'] += floatval($blockCms["financerData"][$index_to_remove]['amount']);

	// Supprimer l'élément
	unset($blockCms["financerData"][$index_to_remove]);

	// Réindexer le tableau après suppression
	$blockCms["financerData"] = array_values($blockCms["financerData"]);
}				
?>
<style  id="css-<?= $kunik ?>" type="text/css">
	@font-face {
		font-family: "montserrat";
		src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.woff") format("woff"),
			url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/montserrat-regular.ttf") format("ttf")
	}

	.mst {
		font-family: 'montserrat' !important;
	}

	@font-face {
		font-family: "CoveredByYourGrace";
		src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/co/CoveredByYourGrace.ttf")
	}

	.cbyg {
		font-family: 'CoveredByYourGrace' !important;
	}

	:root {
		--pfpb-primary-green-color: <?php echo $blockCms["originalColor"] ?>;
		--pfpb-primary-grey-color: #808180;
		--pfpb-blue-color: #4623C9;
        --pfpb-primary-purple-color : <?php echo @$blockCms["campColor"] ?>;
    }

	.pfpb-container .co-popup-custom-header-h2 .fa-star,
	.pfpb-container .earn-title-header,
	.pfpb-container .co-popup-custom-header-h2.titre,
	.pfpb-primary-green-color {
		color: var(--pfpb-primary-green-color);
	}

	.pfpb-container {
		justify-content: center;
		align-items: center;
		font-family: "montserrat";
	}

	.pfpb-container .statboard {
		display: inline-flex;
		width: 100%;
		-text-align: -webkit-center;
		justify-content: center;
		align-items: center;
		margin: 20px 10px;
		color: var(--pfpb-primary-grey-color);
	}

	.pfpb-container .statcol {
		width: 33.33333%;
		text-align: right;
	}

	.pfpb-container .statcol:not(:last-child)::after {
		content: '';
		background: white;
		border: 2.5px solid var(--pfpb-primary-grey-color);
		border-radius: 20px;
		height: 50px;
		position: absolute;
		transform: translateY(-100%);
	}

	.pfpb-container .statcol h3,
	.pfpb-container .statcol div,
	.pfpb-container .co-popup-custom-header-h2.titre,
	.pfpb-container .co-popup-custom-subtitle {
		text-align: center;
	}

	.pfpb-container .progress-bar .financerprogressinfo {
		display: none;
		position: absolute;
		color: var(--pfpb-primary-grey-color);
		border: 2px solid var(--pfpb-primary-green-color);
		/* height: 100px; */
		overflow: visible;
		transform: translateY(50px);
		border-radius: 20px;
		padding: 10px;
		background: whitesmoke;
		z-index: 99999;
		box-shadow: rgba(0, 0, 0, 0.12) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
		z-index: 10;
	}

	.pfpb-container .progress-bar:hover .financerprogressinfo {
		display: inline-grid;
	}

	.pfpb-container .progress-bar {
		border-radius: 10px;
		height: 40px;
		cursor: pointer;
	}

	.pfpb-container .progress {
		display: flex;
		border-radius: 10px;
		background: #eceff1;
		margin-top: 20px;
		box-shadow: 20px 20px 20px #e6eaed, -20px -20px 20px #f2f4f5;
	}

	.pfpb-container .amountpercentTotal {
		position: absolute;
		color: black;
		transform: translateY(20px);
		font-weight: bold;
	}

	.pfpb-container .cofinancestat-container .statbutton {
		margin: auto;
		bottom: 0px;
		border-radius: 30px;
		text-align: center;
		padding: 20px 10px;
	}

	.pfpb-container .cofinancestat-container .statbutton button {
		margin: 5px;
		background-color: #7193c2;
		border: 0;
		border-radius: 20px;
		padding: 5px 10px;
		color: white;
		font-weight: bold;
		width: 220px;
		padding: auto;
	}

	.pfpb-container .cofinancestat-container .statbutton button i {
		display: contents;
	}

	.pfpb-container .cofinancestat-container .statbutton button:hover {
		background-color: rgba(113, 147, 194, 0.75);
	}

	@media (min-width: 720px) {

		/* .pfpb-container {
			padding: 40px 200px 80px;
		} */

		.pfpb-container .statboard {
			display: inline-flex;
			justify-content: center;
			align-items: center
		}

		/* .pfpb-container .cofinancestat-container {
            margin-left: 80px;
            margin-right: 80px;
        } */

		.co-popup-cofinance-container {
			width: 100%;
			height: 100vh;
			position: fixed;
			top: 0;
			left: 0;
			background-color: rgba(0, 0, 0, .5);
			display: flex;
			justify-content: center;
			align-items: center;
			color: #2c3e50;
			z-index: 199999;
		}

		.co-popup-cofinance {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			min-width: 800px;
			min-height: 400px;
			background-color: white;
			border-radius: 10px;
			position: relative;
			font-family: "montserrat" !important;
		}

		.checkbox-cofinance-start-container:checked+.co-popup-cofinance {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			min-width: 1000px;
			min-height: 600px;
			border-radius: 20px;
		}

		.co-popup-cofinance-content,
		.co-popup-cofinance-finish {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			width: 700px;
			height: 387px;
		}

		.co-popup-custom-header-h2.co-popup-custom-title.titre {
			height: 70px;
		}

	}

	.co-popup-cofinance-loader {
		width: 80px;
		height: 60px;
		position: relative;
		display: inline-block;
		--base-color: #263238;
		/*use your base color*/
	}

	.co-popup-cofinance-loader::before {
		content: '';
		left: 0;
		top: 0;
		position: absolute;
		width: 36px;
		height: 36px;
		border-radius: 50%;
		background-color: var(--pfpb-primary-green-color);
		background-image: radial-gradient(circle 8px at 18px 18px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 18px 0px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 0px 18px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 36px 18px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 18px 36px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 30px 5px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 30px 5px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 30px 30px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 5px 30px, #FFF 100%, transparent 0), radial-gradient(circle 4px at 5px 5px, #FFF 100%, transparent 0);
		background-repeat: no-repeat;
		box-sizing: border-box;
		animation: rotationBack 3s linear infinite;
	}

	.co-popup-cofinance-loader::after {
		content: '';
		left: 35px;
		top: 15px;
		position: absolute;
		width: 24px;
		height: 24px;
		border-radius: 50%;
		background-color: var(--pfpb-blue-color);
		background-image: radial-gradient(circle 5px at 12px 12px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 12px 0px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 0px 12px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 24px 12px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 12px 24px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 20px 3px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 20px 3px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 20px 20px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 3px 20px, #FFF 100%, transparent 0), radial-gradient(circle 2.5px at 3px 3px, #FFF 100%, transparent 0);
		background-repeat: no-repeat;
		box-sizing: border-box;
		animation: rotationBack 4s linear infinite reverse;
	}

	@keyframes rotationBack {
		0% {
			transform: rotate(0deg);
		}

		100% {
			transform: rotate(-360deg);
		}
	}

	.co-popup-cofinance-loader-section {
		width: 800px;
		height: 500px;
		padding: 10px;
		position: relative;
		display: flex;
		align-items: center;
		justify-content: center;
	}

	.ap-starter-btn-close2 {
		float: right;
		margin: 0;
		top: -20px;
		background-color: transparent;
		border-color: #2C393EFF;
		color: #2C393EFF;
	}

	/* .pfpb-container {
		padding: 0 8% !important;
	} */

	@media (max-width : 550px) {
		.financ-line-content .financ-line-desc .desc-title {
			font-size: 14px;
		}

		.financ-line-content .financ-line-title span {
			font-size: 22px;
		}

		.pfpb-container {
			padding: 0 8%;
		}

		.pfpb-container .titre {
			font-size: 22px !important;
			text-transform: none;
		}

		.financement-detail .finance-content .title,
		.financement-detail .finance-content .chiffre,
		.financement-detail .finance-content .financeur {
			font-size: 14px !important;
		}

		.btn-cofinance {
			width: auto !important;
			font-size: 14px !important;
			padding: 10px !important;
		}
	}

	.financ-line-content {
		display: block;
		position: relative;
		/* padding: 20px 10%; */
		width: 100%;
	}

	.financ-line-content .financ-line-title span {
		font-size: 40px;
		font-weight: 600;
		color: #43C9B7;
	}

	.financ-line-content .financ-line-desc {
		display: block;
		position: relative;
		/*width: 80%;*/
	}

	.financ-line-content .financ-line-desc .desc-title {
		position: relative;
		width: 100%;
		display: flex;
		align-items: center;
		font-weight: 600;
		font-size: 20px;
		/* padding: 25px 0 15px; */
	}

	.financ-line-content .financ-line-desc .desc-motif {
		position: relative;
		width: 100%;
		display: flex;
		align-items: center;
		font-weight: 400;
		font-size: 20px;
		padding: 0 0 5px;
	}

	.financ-line-content .financ-line-desc .desc-line {
		flex-grow: 1;
		border-bottom: 2px dashed #000;
		margin: 0 10px;
	}

	.mm-dropdown-container {
		display: flex;
		padding: 20px;
		width: 100%;

	}

	/*.mm-dropdown-container .textfirst{
        height: 45px;
        border: 2px solid var(--aap-primary-ftl-color);
        width: fit-content;
        margin: 10px;
        border-radius: 10px;
        padding: 5px;
    }*/

	.mm-dropdown-container .textfirst {
		width: fit-content;
		padding: 5px;
	}

	.mm-dropdown-container .inputfirstsearch , .mm-dropdown-container .inputfirstsearchorga {
		height: 40px;
		border: 1px solid rgb(196, 189, 189);
		width: 200px;
		/*margin-left: 10px;*/
		border-radius: 10px;
		padding: 5px;
	}

	.mm-dropdown-container ul {
		list-style: none;
		height: 150px;
		width: 600px;
		margin: 10px;
		padding: 10px;
        margin-left: 0 !important;
		overflow-y: scroll;
		border: 1px solid rgb(196, 189, 189);
		border-radius: 5px;
		background-color: #eeeeee;
	}

	.mm-dropdown-container ul li {
		list-style: none;
		height: 40px;
		background-color: #fff;
		margin: 5px;
	}

	.mm-dropdown-container ul li:hover,
	.mm-dropdown-container ul li.active {
		background-color: var(--aap-primary-ftl-color);
		cursor: pointer;
	}

	.mm-dropdown-container ul li img {
		display: inline-block;
		overflow: hidden;
		width: 40px;
		height: 40px;
		border-radius: 15px;
		vertical-align: middle;
		border: 3px solid #fff;
	}

	.mm-dropdown-container .textfirst img {
		display: inline-block;
		overflow: hidden;
		width: 40px;
		height: 40px;
		border-radius: 15px;
		vertical-align: middle;
		border: 3px solid #fff;
		margin-top: -5px;
	}

	.mm-dropdown-container ul::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
		background-color: #F5F5F5;
		border-radius: 10px;
	}

	.mm-dropdown-container ul::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	.mm-dropdown-container ul::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background-color: #FFF;
		background-image: -webkit-gradient(linear,
				40% 0%,
				75% 84%,
				from(#bceab5),
				to(var(--aap-primary-ftl-color)),
				color-stop(.6, #9eeaa5))
	}

	.financement-detail .finance-content {
		display: flex;
		align-items: center;
		justify-content: space-between;
		padding-top: 2%;
		/* padding: 35px 5px 15px 7px; */
	}

	.financement-detail .finance-content .title {
		font-size: 20px;
		font-weight: 800;
		color: #494646;
		display: block;
	}

	.financement-detail .finance-content .chiffre {
		font-size: 19px;
		font-weight: 700;
		color: grey;
		display: block;
	}

	.financement-detail .finance-content .financeur {
		font-size: 22px;
		font-weight: 900;
		color: #000;
		display: block;
	}

	.financement-detail .progress-content {
		padding: 0 7px 10px 9px;
	}

	.progress-content .progress {
		height: 40px;
		border-radius: 15px;
	}

	.financement-detail .progress-content .progress .progress-bar {
		background-color: #4623C9;
		display: flex;
		justify-content: center;
		align-items: center;
		font-size: 18px;
	}

	.btn-cofinance-content {
		display: block;
		position: relative;
		padding: 10px 9px;
	}

	.btn-cofinance {
		padding: 10px 18px;
		border-radius: 10px;
		border: 2px solid #4623C9;
		background-color: #4623C9;
		color: #fff;
		transition: all .4s;
		font-size: 22px;
		font-weight: 600;
		width: 220px;
		float: right;
	}
	.btn-cofinance:hover {
		border: 2px solid #4623C9;
		background-color: transparent;
		color: #4623C9;
	}

	:root {
		--aap-primary-ftl-color: #46c6b5;
		--aap-secondary-ftl-color: #4623C9;
	}

	.co-popup-custom-question [type="radio"],
	.co-popup-custom-question [type="checkbox"] {
		border: 0;
		clip: rect(0 0 0 0);
		height: 1px;
		margin: -1px;
		overflow: hidden;
		padding: 0;
		position: absolute;
		width: 1px;
	}

	.co-popup-custom-question label {
		/*display: block;*/
		cursor: pointer;
	}

	.co-popup-custom-question [type="radio"]+span:before {
		content: '';
		display: inline-block;
		width: 1.5em;
		height: 1.5em;
		vertical-align: -0.25em;
		border-radius: 20%;
		border: 0.125em solid #fff;
		box-shadow: 0 0 0 0.15em #555;
		/*transition: 0.5s ease all;*/
	}

	.co-popup-custom-question [type="checkbox"]+span:before {
		content: '';
		display: inline-block;
		width: 1.5em;
		height: 1.5em;
		vertical-align: -0.25em;
		border-radius: .25em;
		border: 0.125em solid #fff;
		box-shadow: 0 0 0 0.15em #555;
		transition: 0.5s ease all;
	}

	.co-popup-custom-question [type="checkbox"]+span:before,
	.co-popup-custom-question [type="radio"]+span:before {
		margin-right: 0.75em;
	}

	.co-popup-custom-question [type="radio"]:checked+span:before,
	.co-popup-custom-question [type="checkbox"]:checked+span:before {
		background: var(--aap-primary-color);
		box-shadow: 0 0 0 0.25em #666;
	}

	.co-popup-custom-question [type="radio"]:focus span:after {
		content: '\0020\2190';
		font-size: 1.2em;
		line-height: 1;
		vertical-align: -0.125em;
	}

	.co-popup-custom-question .wrapper {
		display: flex;
		flex-direction: column;
	}

	.co-popup-custom-question .grid-row {
		margin-bottom: 1em
	}

	.co-popup-custom-question .grid-row,
	.grid-header {
		display: flex;
		/*   flex: 1 0 auto; */
		/*   height: auto; */
	}

	.co-popup-custom-question .grid-header {
		align-items: flex-end;
	}

	.co-popup-custom-question .header-item {
		width: 100px;
		text-align: center;
		/*   border:1px solid transparent; */
	}

	.co-popup-custom-question .header-item:nth-child(1) {
		width: 180px;
	}

	.co-popup-custom-question .subtitle {
		font-size: 0.7em;
	}

	.co-popup-custom-question .flex-item:before {
		content: '';
		padding-top: 26%;
	}

	.co-popup-custom-question .flex-item {
		display: flex;
		/*   flex-basis:25%; */
		width: 100px;
		border-bottom: 1px solid #ccc;
		justify-content: center;
		align-items: center;
		/*   text-align:left; */
		font-size: 0.8em;
		font-weight: normal;
		color: #999;
	}

	.co-popup-custom-question .flex-item:nth-child(1) {
		border: none;
		font-size: 1em;
		color: #000;
		width: 180px;
		justify-content: left;
	}



	.co-popup-custom-question fieldset {
		font-size: 0.8em;
		border: 2px solid #000;
		padding: 2em;
		border-radius: 0.5em;
		margin-bottom: 20px;
	}

	.co-popup-custom-question .swiper-slide-:not(:last-child) {
		visibility: hidden;
	}

	.co-popup-custom-question .swiper-slide-:not(:last-child) div {
		display: none;
	}

	.swiper-slide .wrapper {
		height: 50%;
		/* height: 320px; */
		overflow-x: auto;
		margin-bottom: 5px;
	}

	.swiper-slide .wrapper::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
		background-color: #F5F5F5;
		border-radius: 10px;
	}

	.swiper-slide .wrapper::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	.swiper-slide .wrapper::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background-color: #FFF;
		background-image: -webkit-gradient(linear,
				40% 0%,
				75% 84%,
				from(#bceab5),
				to(#7ee281),
				color-stop(.6, #9eeaa5))
	}

	.amountaffect-list {
		padding: 5px;
		background-color: #f0f7ed;
		border-radius: 10px;
		margin: 10px;
		display: flex;
        flex-direction: column;
	}

	.amountaffect-list.recaplist {
		padding: 10px 10px;
	}

	.inputamount {
		-webkit-tap-highlight-color: transparent;
		line-height: calc(2.125rem);
		appearance: none;
		resize: none;
		box-sizing: border-box;
		width: 100%;
		color: rgb(0, 0, 0);
		display: block;
		text-align: left;
		border: 0.0625rem solid rgb(206, 212, 218);
		background-color: rgb(255, 255, 255);
		transition: border-color 100ms ease 0s;
		min-height: 2.25rem;
		padding: 10px;
		border-radius: 0.25rem;
	}

	.totalToPay {
		font-size: 18px;
	}

	.close-co-popup-chooseline-header {
		right: 0;
		position: absolute;
		top: 0;
	}

	legend .droplef {
		margin-top: -10px;
	}

	.border-radius-10 {
		border-radius: 10px;
	}

	.border-radius-20 {
		border-radius: 10px;
	}

	.border-radius-perc-50 {
		border-radius: 50%;
	}

	.radio-item [type="checkbox"] {
		display: none;
	}

	.radio-item+.radio-item {
		margin-top: 15px;
	}

	.radio-item label {
		display: block;
		padding: 5px 20px;
		background: #e2eaf2;
		border-radius: 10px;
		cursor: pointer;
		font-size: 12px;
		font-weight: 400;
		min-width: 220px;
		white-space: nowrap;
		position: relative;
		/*transition: 0.4s ease-in-out 0s;*/
	}

	.radio-item label:after {
		height: 19px;
		width: 19px;
		border: 2px solid #524eee;
		left: 19px;
		top: calc(50% - 12px);
	}

	.radio-item label+.radio-item label [type="checkbox"]:checked {
		background-color: #e3fced;
		background-image: radial-gradient(at 47% 33%, hsl(0, 0%, 100%) 0, transparent 59%), radial-gradient(at 82% 65%, hsl(41.63, 62%, 85%) 0, transparent 55%);
	}

	.radio-item label [type="checkbox"]:checked+.radio-item label {
		background-color: #e3fced;
		background-image: radial-gradient(at 47% 33%, hsl(0, 0%, 100%) 0, transparent 59%), radial-gradient(at 82% 65%, hsl(41.63, 62%, 85%) 0, transparent 55%);
	}

	.checkbox-budget:checked+label,
	.checkbox-budget:not(:checked)+label {
		position: relative;
		display: inline-block;
		padding: 0;
		padding-top: 20px;
		padding-bottom: 20px;
		width: 150px;
		font-size: 32px;
		line-height: 52px;
		font-weight: 700;
		letter-spacing: 1px;
		margin: 0 auto;
		margin-left: 5px;
		margin-right: 5px;
		margin-bottom: 10px;
		text-align: center;
		border-radius: 4px;
		overflow: hidden;
		cursor: pointer;
		text-transform: uppercase;
		-webkit-transition: all 300ms linear;
		transition: all 300ms linear;
		-webkit-text-stroke: 1px var(--aap-primary-color);
		text-stroke: 1px var(--aap-primary-color);
		-webkit-text-fill-color: var(--aap-primary-color);
		text-fill-color: var(--aap-primary-color);
		color: var(--aap-primary-color);
	}

	.checkbox-budget:not(:checked)+label {
		background-color: var(--dark-light);
		box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
	}

	.checkbox-budget:checked+label {
		background-color: transparent;
		box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
	}

	.checkbox-budget:not(:checked)+label:hover {
		box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
	}

	.checkbox-budget:checked+label::before,
	.checkbox-budget:not(:checked)+label::before {
		position: absolute;
		content: '';
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		border-radius: 4px;
		background-color: #e8f9f1;
		background-image: radial-gradient(at 47% 33%, hsl(61.48deg 67.78% 84.36%) 0, transparent 59%), radial-gradient(at 82% 65%, hsl(65.91deg 49.58% 84.38%) 0, transparent 55%);
		z-index: -1;
	}

	.checkbox-budget:checked+label span,
	.checkbox-budget:not(:checked)+label span {
		position: relative;
		display: block;
	}

	.checkbox-budget:checked+label span::before,
	.checkbox-budget:not(:checked)+label span::before {
		position: absolute;
		content: attr(data-hover);
		top: 0;
		left: 0;
		width: 100%;
		overflow: hidden;
		-webkit-text-stroke: transparent;
		text-stroke: transparent;
		-webkit-text-fill-color: var(--aap-primary-color);
		text-fill-color: var(--aap-primary-color);
		color: var(--aap-primary-color);
		-webkit-transition: max-height 0.3s;
		-moz-transition: max-height 0.3s;
		transition: max-height 0.3s;
	}

	.checkbox-budget:not(:checked)+label span::before {
		max-height: 0;
	}

	.checkbox-budget:checked+label span::before {
		max-height: 100%;
	}

	.checkbox:checked~.section .container .row .col-xl-10 .checkbox-budget:not(:checked)+label {
		background-color: var(--light);
		-webkit-text-stroke: 1px var(--light);
		text-stroke: 1px var(--light);
		box-shadow: 0 1x 4px 0 rgba(0, 0, 0, 0.05);
	}

	.checkbox-budget:checked+label,
	.checkbox-budget:not(:checked)+label {
		position: relative;
		display: inline-block;
		padding: 0;
		width: 150px;
		font-size: 20px;
		line-height: 52px;
		font-weight: 700;
		letter-spacing: 1px;
		margin: 0 auto;
		margin-left: 5px;
		margin-right: 5px;
		margin-bottom: 10px;
		text-align: center;
		border-radius: 4px;
		overflow: hidden;
		cursor: pointer;
		text-transform: uppercase;
		-webkit-transition: all 300ms linear;
		transition: all 300ms linear;
		-webkit-text-stroke: 1px var(--aap-tertiary-color);
		text-stroke: 1px var(--aap-tertiary-color);
		-webkit-text-fill-color: var(--aap-tertiary-color);
		text-fill-color: var(--aap-tertiary-color);
		color: var(--aap-tertiary-color);
	}

	.checkbox-budget:not(:checked)+label {
		background-color: #e2eaf2;
		box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
	}

	.checkbox-budget:checked+label {
		background-color: transparent;
		box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
	}

	.checkbox-budget:not(:checked)+label:hover {
		box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
	}

	.checkbox-budget:checked+label::before,
	.checkbox-budget:not(:checked)+label::before {
		position: absolute;
		content: '';
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		border-radius: 4px;
		background-color: #e8f9f1;
		background-image: radial-gradient(at 47% 33%, hsl(61.48deg 67.78% 84.36%) 0, transparent 59%), radial-gradient(at 82% 65%, hsl(65.91deg 49.58% 84.38%) 0, transparent 55%);
		z-index: -1;
	}

	.checkbox-budget:checked+label span,
	.checkbox-budget:not(:checked)+label span {
		position: relative;
		display: block;
	}

	.checkbox-budget:checked+label span::before,
	.checkbox-budget:not(:checked)+label span::before {
		position: absolute;
		content: attr(data-hover);
		top: 0;
		left: 0;
		width: 100%;
		overflow: hidden;
		-webkit-text-stroke: var(--aap-primary-color);
		text-stroke: var(--aap-primary-color);
		-webkit-text-fill-color: var(--aap-primary-color);
		text-fill-color: var(--aap-primary-color);
		color: var(--aap-primary-color);
		-webkit-transition: max-height 0.3s;
		-moz-transition: max-height 0.3s;
		transition: max-height 0.3s;
	}

	.checkbox-budget:not(:checked)+label span::before {
		max-height: 0;
	}

	.checkbox-budget:checked+label span::before {
		max-height: 100%;
	}

	.checkbox:checked~.checkbox-budget:not(:checked)+label {
		background-color: var(--light);
		-webkit-text-stroke: 1px var(--white);
		text-stroke: 1px var(--white);
		box-shadow: 0 1x 4px 0 rgba(0, 0, 0, 0.05);
	}

	.custom-fin-img {
		display: inline-block;
		overflow: hidden;
		width: 35px;
		height: 35px;
		border-radius: 15px;
		vertical-align: middle;
	}

	.custom-fin-mult-img {
		display: inline-block;
		overflow: hidden;
		width: 40px;
		height: 40px;
		border-radius: 15px;
		vertical-align: middle;
		border: 3px solid #fff;
		;
	}

	.custom-fin-mult-img.plustext {
		background-color: #a6b1bc;
		text-align: center;
		color: #fff;
		font-size: 18px;
		/* font-weight: bold; */
		padding-top: 4px;
	}

	.fin-container .custom-fin-mult-img+.custom-fin-mult-img {
		margin-left: -10px;
	}

	.porter-container .custom-fin-mult-img+.custom-fin-mult-img {
		margin-left: -10px;
	}

	/* --------------ftl---------------- */

	.cofinanceftl .co-popup-custom-question [type="radio"]:checked+span:before,
	.cofinanceftl .co-popup-custom-question [type="checkbox"]:checked+span:before {
		background: var(--aap-primary-ftl-color);
		box-shadow: 0 0 0 0.25em #666;
	}

	.cofinanceftl .checkbox-budget:checked+label,
	.cofinanceftl .checkbox-budget:not(:checked)+label {
		position: relative;
		display: inline-block;
		padding: 0;
		width: 150px;
		font-size: 32px;
		line-height: 52px;
		font-weight: 700;
		letter-spacing: 1px;
		margin: 0 auto;
		margin-left: 5px;
		margin-right: 5px;
		text-align: center;
		border-radius: 4px;
		overflow: hidden;
		cursor: pointer;
		text-transform: uppercase;
		-webkit-transition: all 300ms linear;
		transition: all 300ms linear;
		-webkit-text-stroke: 1px var(--aap-primary-ftl-color);
		text-stroke: 1px var(--aap-primary-ftl-color);
		-webkit-text-fill-color: var(--aap-primary-ftl-color);
		text-fill-color: var(--aap-primary-ftl-color);
		color: var(--aap-primary-ftl-color);
	}

	.cofinanceftl .checkbox-budget:checked+label span::before,
	.cofinanceftl .checkbox-budget:not(:checked)+label span::before {
		position: absolute;
		content: attr(data-hover);
		top: 0;
		left: 0;
		width: 100%;
		overflow: hidden;
		-webkit-text-stroke: transparent;
		text-stroke: transparent;
		-webkit-text-fill-color: var(--aap-primary-ftl-color);
		text-fill-color: var(--aap-primary-ftl-color);
		color: var(--aap-primary-ftl-color);
		-webkit-transition: max-height 0.3s;
		-moz-transition: max-height 0.3s;
		transition: max-height 0.3s;
	}

	.cofinanceftl .checkbox-budget:checked+label span::before,
	.cofinanceftl .checkbox-budget:not(:checked)+label span::before {
		position: absolute;
		content: attr(data-hover);
		top: 0;
		left: 0;
		width: 100%;
		overflow: hidden;
		-webkit-text-stroke: var(--aap-primary-ftl-color);
		text-stroke: var(--aap-primary-ftl-color);
		-webkit-text-fill-color: var(--aap-primary-ftl-color);
		text-fill-color: var(--aap-primary-ftl-color);
		color: var(--aap-primary-ftl-color);
		-webkit-transition: max-height 0.3s;
		-moz-transition: max-height 0.3s;
		transition: max-height 0.3s;
	}

	.cofinanceftl .wrapper::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background-color: #FFF;
		background-image: -webkit-gradient(linear,
				40% 0%,
				75% 84%,
				from(#bceab5),
				to(var(--aap-primary-ftl-color)),
				color-stop(.6, #9eeaa5))
	}

	@media (min-width: 720px) {
		.custom-financer-mobile-bg {
			display: none;
		}

		.custom-financer-bg {
			display: block;
		}

		.ap-starter-btn-sheet {
			/*margin: 0 10px;*/
			border: 1.7px solid var(--aap-secondary-color);
			width: 100%;
			background-color: #fff;
			color: var(--aap-secondary-color);
			border-radius: 4px 0px 0px 4px;
		}

		.ap-starter-btn-start-custom {
			border: 1.7px solid var(--aap-primary-color);
		}

		.ap-starter-btn-close {
			border: 1.7px solid var(--aap-secondary-color);
			border-radius: 0px 4px 4px 0px;
		}

		.ap-starter-btn-close:hover {
			border-radius: 4px;
		}

		.ap-starter-btn-sheet:hover {
			border-radius: 4px;
		}

		/*.co-popup-custom-finish:before {
            position: absolute;
            content: "";
            top: 0;
            right: -35px;
            width: 88px;
            height: 88px;
            background: var(--aap-primary-color);;
            opacity: 0.2;
            border-radius: 8px;
            transform: rotate(45deg);
        }

        .co-popup-custom-finish:after {
            position: absolute;
            content: "";
            top: 30px;
            right: -35px;
            width: 88px;
            height: 88px;
            background: var(--aap-primary-color);;
            opacity: 0.2;
            border-radius: 8px;
            transform: rotate(45deg);
        }*/

		.co-popup-custom-container:not(.cofinancemodal) {
			width: 100%;
			height: 90vh;
			top: 0;
			left: 0;
			background-color: #e3fced;
			background-image: radial-gradient(at 47% 33%, hsl(0, 0%, 100%) 0, transparent 59%), radial-gradient(at 82% 65%, hsl(41.63, 62%, 85%) 0, transparent 55%);
			background-repeat: no-repeat;
			display: flex;
			justify-content: center;
			align-items: center;
			color: #2c3e50;
			z-index: 9999999;
		}

		.co-popup-custom {
			min-width: 800px;
			/*min-height: 500px;*/
			background-color: white;
			border-radius: 10px;
			position: relative;
			font-family: "montserrat" !important;

		}

		input.checkbox-custom-start2:checked+.co-popup-custom {
			border-radius: 20px;
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
		}

		.co-popup-custom-content {
			/*transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            */
			width: 800px;
			height: 500px;
			position: relative;
			padding: 20px;
		}

		.co-popup-custom-finish,
		.co-popup-custom-cart {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			width: 800px;
			height: 500px;
			border-radius: 10px;
			padding: 20px;
		}

		.co-popup-custom-cart .co-popup-custom-header {
			height: 350px;
		}

		.co-popup-custom-header {
			padding-top: 20px;
			text-align: center;
		}

		.co-popup-custom-header h2 {
			/* font-weight: bold; */
		}

		.co-popup-custom-header p {
			font-size: 12px;
			line-height: 1.8;
		}

		.co-popup-custom-header p a {
			text-decoration: none;
			/* font-weight: bold; */
			color: #9fbd38;
			text-decoration: underline;
		}

		.co-popup-custom-actions {
			display: flex;
			flex-direction: row;
			padding-top: 50px;
			align-items: center;
			justify-content: center;
			margin: 0;
			padding: 0;
			margin-top: 20px;
		}

		.co-popup-custom-actions li {
			display: flex;
			margin-bottom: 5px;
			width: 300px;
			flex-wrap: no-wrap;
			align-items: stretch;
			justify-content: center;
			overflow: hidden;
		}

		.co-popup-custom-actions li a {
			text-decoration: none;
			padding: 10px 20px;
			color: #2c3e50;
			font-size: 16px;
			align-items: stretch;
			justify-content: center;
		}

		.co-popup-custom-actions li.li-abord-custom a {
			display: flex;
			justify-content: center;
			align-items: center;
			/*transform: skewX(150deg);*/
			transition: all 0.5s;
			cursor: pointer;
			white-space: nowrap;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-close .labelspan {
			display: none;
			transition: all ease-in-out 0.5s;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-close:hover .labelspan {
			display: flex;
			transition: all ease-in-out 0.5s;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-sheet {
			width: 270px;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-sheet:hover {
			width: 299px;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-close {
			width: 30px;
		}

		.co-popup-custom-actions li.li-abord-custom a.ap-starter-btn-close:hover {
			width: 299px;
			border-radius: 4px;
		}

		a.ap-starter-btn-sheet:hover+a.ap-starter-btn-close {
			width: 0px;
			padding: 0;
			margin: 0;
			border: 0px
		}

		li:has(> a.ap-starter-btn-close:hover) a.ap-starter-btn-sheet {
			width: 0px;
			padding: 0;
			margin: 0;
			border: 0px
		}

		.co-popup-custom-actions li a span {
			padding-right: 10px;
		}

		.co-popup-custom-actions li:first-child a {
			background-color: #9fbd38;
			color: white;
			/* font-weight: bold; */
		}

		.co-popup-custom-actions button {
			width: fit-content;
			margin-top: 10px;
			background-color: transparent;
			font-size: 16px;
			color: #2c3e50;
		}

		.co-popup-custom-background {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}

		/*.co-popup-custom-content , .co-popup-custom-finish {
            transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
            position: relative;
            padding: 20px;
        }*/

		.co-popup-custom-particle {
			width: 100%;
			height: 100%;
		}

		.ap-starter-btn-email {
			background-color: #2c3e50;
			color: white !important;
			/* margin-left: 5px;*/
			width: 100%;
			border-radius: 5px;
			font-weight: 100;
		}

		.co-popup-custom-header {
			background-color: #fff !important;
		}

		@media (max-width: 720px) {
			.co-popup-custom-actions {
				flex-direction: column;
			}
		}



		.swiper-slide-next {
			display: none !important;
		}

		.co-popup-custom-menu {
			background-color: transparent;
			height: 100%;
			border-top-left-radius: 20px;
			border-bottom-left-radius: 20px;
			padding: 0px;
		}

		.co-popup-custom-projectresume {
			background-color: var(--aap-primary-color);
			height: 76%;
			border-top-left-radius: 20px;
			border-bottom-right-radius: 20px;
			/*text-align: center;*/
			position: relative;
		}

		.co-popup-custom-question-container {
			height: 600px;
			width: 1000px;
		}

		/*preconfig Timeline */
		ul.timeline.aap-customListStepSwipping {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			list-style-type: none;
			position: relative;
			padding: 0px;
		}

		ul.timeline.aap-customListStepSwipping:before {
			content: ' ';
			background: #fff;
			display: inline-block;
			position: absolute;
			left: 10px;
			width: 2px;
			height: 100%;
			z-index: 400;
		}

		ul.timeline.aap-customListStepSwipping>li {
			margin: 20px 0;
			padding-left: 30px;
			float: none;
		}

		ul.timeline.aap-customListStepSwipping>li:before {
			content: ' ';
			background: #fff;
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid var(--aap-primary-color);
			left: 1px;
			width: 18px;
			height: 18px;
			z-index: 400;
		}

		ul.timeline.aap-customListStepSwipping>li.active:before {
			content: ' ';
			background: var(--aap-primary-color);
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid var(--aap-secondary-dark);
			left: 1px;
			width: 18px;
			height: 18px;
			z-index: 400;
		}


		.timeline.aap-customListStepSwipping>li .name-step {
			display: block;
			max-width: 100%;
			margin-bottom: 5px;
			font-weight: 700;
			font-size: 14px;
			color: #fff;
			text-align: left;
		}

		.timeline.aap-customListStepSwipping>li>p {
			font-size: 12px;
			color: #eee;
			grid-template-rows: min-content 1fr;
		}

		.timeline.aap-customListStepSwipping>li>hr {
			width: 100%;
			margin-bottom: 0px;
		}

		.aap-customcontain-info-costum {
			margin-top: 30px;
			margin-bottom: 30px;
		}

		.aap-customcontain-info-costum img.logo-info {
			width: 75px;
			height: 75px;
			border: solid 2px rgba(255, 255, 255);
			border-radius: 50%;
		}

		.aap-customcontain-info-costum .title-info,
		.co-popup-custom-projectresume .title-info {
			font-size: 16px;
			color: #fff;
			border-bottom: 1px solid #fff;
			text-align: left;
			padding: 10px 0px 10px 20px;
		}

		.li-start-custom,
		.li-abord-custom {
			background-color: var(--aap-primary-color);
			border-radius: 4px;
		}

		input.checkbox-custom-start:checked+div div+div ul .li-start-custom {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			width: 100%;
			left: 0;
			height: 100%;
			margin-top: 0px;
		}

		.li-start-custom {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			width: 300px;
			left: 0;
			height: 100%;
			justify-content: center;
			border: 1.7px;
		}

		input.checkbox-custom-start:checked+div div+div ul .li-abord-custom,
		input.checkbox-custom-start:checked+div div+div ul .li-sheet-custom {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			width: 0%;
			opacity: 0;
		}

		.li-abord-custom {
			width: 300px !important;
			margin: 0 0 0 20px;
		}

		input.checkbox-custom-start:checked+div.co-popup-custom-content {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			width: 1000px;
			height: 600px;
			padding: 0;
		}

		input.checkbox-custom-start:checked+div div+div ul .li-start-custom a,
		input.checkbox-custom-start:checked+div div+div ul .li-abord-cofinance,
		input.checkbox-custom-start:checked+div div+div ul .li-abord-custom,
		input.checkbox-custom-start:checked+div div+div ul .li-sheet-custom,
		input.checkbox-custom-start:checked+div div .co-popup-custom-header-h2,
		input.checkbox-custom-start:checked+div div .co-popup-custom-header-h4,
		input.checkbox-custom-start:checked+div div .co-popup-custom-header-p,
		input.checkbox-custom-start:checked+div div .progress,
		input.checkbox-custom-start:checked+div div .statboard {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			opacity: 0;
		}

		input.checkbox-custom-start:checked+div div .co-popup-custom-header-logo {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			float: left;
			margin-left: 130px;
		}

		.li-start-custom,
		.li-abord-custom,
		.li-sheet-custom,
		.co-popup-custom-header-h2,
		.co-popup-custom-header-h4,
		.co-popup-custom-header-p {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			opacity: 1;
			/*margin-top: 25px;*/
			/* font-weight: bold; */
		}

		input.checkbox-custom-start:checked+div div+div .co-popup-custom-actions {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			width: 33.33333333%;
			left: 0;
			height: 76%;
			border-bottom-right-radius: 20px;
			border-top-left-radius: 20px;
			margin: 0;
			background-color: var(--aap-primary-color);
		}

		input.checkbox-custom-start:checked+div div+div.co-popup-custom-header-action {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			height: 100%;
		}

		co-popup-custom-header-action {
			height: 45px;
		}

		input.checkbox-custom-start:checked+div div.co-popup-custom-header {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			background-color: transparent !important;
			position: absolute;
		}

		.co-popup-custom-actions {
			transition: all 0.7s 0.3s cubic-bezier(0.5, 0.005, 0.075, 0.985), transform 0.1s 0.7s, -webkit-transform 0.1s;
			width: 100%;
			left: 0;
			height: 100%;
		}

		.co-popup-custom-header-logo img.logo-info {
			width: 75px;
			height: 75px;
			border: solid 2px var(--aap-primary-color);
			border-radius: 50%;
		}

		.co-popup-custom-menu img.logo-info {
			width: 50px;
			height: 50px;
			border: solid 2px var(--aap-primary-color);
			border-radius: 50%;
		}

		.co-popup-custom-menu legend {
			font-size: 22px;
		}

		.co-popup-custom-center {
			margin: 0;
			top: 50%;
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
		}



		.co-popup-custom-title {
			font-size: 22px;
			/* font-weight: bold; */
			color: var(--aap-secondary-dark)
		}

		.co-popup-custom-subtitle {
			font-size: 12px;
			/*/* font-weight: bold; */
			*/ color: #000
		}

		.preconfig-info-has-tooltip:hover .preconfig-info-tooltip,
		.preconfig-info-has-tooltip:focus .preconfig-info-tooltip,
		.preconfig-info-has-tooltip.hover .preconfig-info-tooltip {
			opacity: 1;
			rotate(0deg);
			pointer-events: inherit;
		}

		.preconfig-info-tooltip {
			display: block;
			position: absolute;
			padding: 10px 30px;
			border-radius: 5px;
			background: var(--aap-primary-color);
			text-align: center;
			color: white;
			opacity: 0;
			pointer-events: none;
			z-index: 5;
			bottom: 30%;
			transform: translate(-50%, 0px);
		}

		.preconfig-info-tooltip:hover {
			opacity: 1;
			pointer-events: inherit;
		}

		.preconfig-info-tooltip img {
			max-height: 200px;
		}

		.preconfig-info-tooltip:after {
			content: '';
			display: block;
			margin: 0 auto;
			widtH: 0;
			height: 0;
			border: 2px solid transparent;
			border-top: 5px solid rgba(0, 0, 0, 0.75);
			position: absolute;
			left: 50%;
		}

		.aap-primary-color {
			color: #9BC125;
		}

		.aap-secondary-color {
			color: #3F4E58;
		}

		h3.co-popup-custom-title.aap-primary-color {
			text-transform: none;
			font-size: 28px;
			margin-top: 10px;
		}

		.co-popup-custom-header-h4.co-popup-custom-subtitle.aap-secondary-color {
			font-size: 16px;
			/* font-weight: bold; */
			margin-top: 10px;
			max-height: 60px;
			overflow: hidden;
			display: -webkit-box;
			-webkit-line-clamp: 2;
			-webkit-line-clamp: 2;
			-webkit-box-orient: vertical;
		}

		.co-popup-custom-header .progress,
		.co-popup-custom-header .progress-bar {
			border-radius: 15px;
			height: 15px;
		}

		.co-popup-custom-header .row .col-md-4.col-sm-12:not(:last-child) {
			border-right: 3px solid #91a8b2;
		}

		.custom-fin-percent {
			font-size: 18px;
		}

		.co-popup-custom-projectresume .ap-starter-btn-close {
			bottom: -10px;
			width: 150px;
			border-radius: 0px 4px 4px 0px;
			margin-left: -75px;
			left: 50%;
			position: absolute;
		}

		legend {
			margin: 0px 0px 10px 0px;
			padding: 10px;
			padding-left: 40px;
		}

		.co-popup-custom-projectresume .carddesc {
			height: inherit;
		}

		.co-popup-custom-projectresume .bottomdesc {
			padding-bottom: 40px;
			position: absolute;
			bottom: 0;
			width: 100%;
		}

		.co-popup-custom-projectresume .title-projectname {
			padding-left: 20px;
			padding-right: 20px;
			font-size: 18px;
			display: -webkit-box;
			-webkit-line-clamp: 2;
			-webkit-box-orient: vertical;
			overflow: hidden;
		}

		.co-popup-custom-projectresume .title-projectdesc {
			padding-left: 20px;
			padding-right: 20px;
			font-size: 12px;
			display: -webkit-box;
			-webkit-line-clamp: 3;
			-webkit-box-orient: vertical;
			overflow: hidden;
		}

		.co-popup-custom-projectresume .title-percent {
			font-size: 75px;
			/* font-weight: bold; */
			margin-left: 20px;
		}

		.co-popup-custom-projectresume .title-cardpercent {
			position: absolute;
			top: 30%;
		}

		.co-popup-custom-projectresume .title-percent-icon {
			padding-top: 25px;
			padding-left: 5px;
			position: absolute;
			top: 0;
			font-size: 18px;
		}

		.co-popup-custom-projectresume .title-reste {
			margin-left: 20px;
			padding-left: 20px;
			padding-right: 20px;
			border-radius: 10px;
			background-color: #fff;
		}

		.custom-footer-container {
			display: inline;
		}

		.custom-fin-creator {
			display: inline-flex;
		}

		.co-popup-custom-projectswiper {
			height: 100%;
		}

		.co-popup-custom-projectswiper .swiper {
			height: 76%;
		}



		:root {
			--white: #ffffff;
			--light: #f0eff3;
			--black: #000000;
			--dark-blue: #1f2029;
			--dark-light: #353746;
			--red: #da2c4d;
			--yellow: #f8ab37;
			--grey: #ecedf3;
		}

		.co-popup-chooseline-container {
			width: 100%;
			height: 100vh;
			position: fixed;
			top: 0;
			left: 0;
			background-color: rgba(0, 0, 0, .5);
			display: flex;
			justify-content: center;
			align-items: center;
			color: #2c3e50;
			z-index: 9999999;
		}

		.co-popup-chooseline {
			width: 1000px;
			max-height: 500px;
			background-color: white;
			position: relative;
			font-family: "montserrat" !important;
			border-radius: 10px;
		}

		.co-popup-chooseline-list {
			height: 300px;
			overflow-y: auto;
		}

		.co-popup-chooseline-list::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
			background-color: #F5F5F5;
			border-radius: 10px;
		}

		.co-popup-chooseline-list::-webkit-scrollbar {
			width: 10px;
			background-color: #F5F5F5;
		}

		.co-popup-chooseline-list::-webkit-scrollbar-thumb {
			border-radius: 10px;
			background-color: #FFF;
			background-image: -webkit-gradient(linear,
					40% 0%,
					75% 84%,
					from(#bceab5),
					to(#7ee281),
					color-stop(.6, #9eeaa5))
		}

		.amountaffect {
			font-family: Montserrat, sans-serif;
			-webkit-tap-highlight-color: transparent;
			box-sizing: border-box;
			background-color: rgb(255, 255, 255);
			box-sizing: border-box;
			border-radius: 0.25rem;
			box-shadow: rgba(0, 0, 0, 0.18) 0px 1px 3px, rgba(0, 0, 0, 0.24) 0px 1px 2px;
			background-color: rgb(255, 255, 255);
			border-radius: 0.25rem;
			position: relative;
			height: 120px;
			overflow: auto;
			margin-bottom: 10px;
			border-radius: 10px;
		}

		.amountaffect::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
			background-color: #F5F5F5;
			border-radius: 10px;
		}

		.amountaffect::-webkit-scrollbar {
			width: 10px;
			background-color: #F5F5F5;
		}

		.amountaffect::-webkit-scrollbar-thumb {
			border-radius: 10px;
			background-color: #FFF;
			background-image: -webkit-gradient(linear,
					40% 0%,
					75% 84%,
					from(#bceab5),
					to(#7ee281),
					color-stop(.6, #9eeaa5))
		}

		.co-popup-chooseline-title {
			text-transform: none;
			font-size: 20px;
		}

		.close-co-popup-chooseline-header {
			border: 1px solid var(--aap-secondary-color);
			border-radius: 50%;
			margin: 20px;
			color: var(--aap-secondary-dark);
			background-color: #fff;
			transition: 0.4s ease-in-out 0s;
			height: 30px;
			width: 30px;
		}

		.co-popup-chooseline-header .close-co-popup-chooseline-header:hover {
			transform: rotate(90deg);
		}

		.close-co-popup-chooseline-header:hover {
			border-color: #f9576d;
			color: #f9576d;
		}

		.co-popup-chooseline-header {
			border-bottom: 1px solid;
		}

		.co-popup-chooseline-footer {
			border-top: 1px solid;
		}

		.co-popup-chooseline-footer .close-co-popup-chooseline-header {
			position: relative;
			border: 0px;
			margin: 10px;
			margin-right: 20px;
		}

		.custom-financer-bg #custom-search-input {
			margin: 0;
			margin-top: 10px;
			padding: 0;
		}

		.custom-financer-bg #custom-search-input .search-query {
			padding-right: 3px;
			padding-right: 4px \9;
			padding-left: 3px;
			padding-left: 4px \9;
			margin-bottom: 0;
			-webkit-border-radius: 10px;
			-moz-border-radius: 10px;
			border-radius: 10px;
		}

		.custom-financer-bg #custom-search-input button {
			border: 0;
			background: none;
			padding: 2px 5px;
			margin-top: 2px;
			position: relative;
			left: -28px;
			margin-bottom: 0;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			color: var(--aap-primary-color);
		}

		.custom-financer-bg .search-query:focus+button {
			z-index: 3;
		}

		.custom-financer-bg .search-query {
			border-radius: 10px;
		}

		.custom-financer-bg .search-query:focus {
			border-color: var(--aap-primary-color);
			-webkit-box-shadow: none;
			-moz-box-shadow: none;
			box-shadow: none;
		}

		.inputemail {
			padding: 0px !important;
			width: 0px !important;
			border: var(--aap-primary-color) solid 2px !important;
			height: 44px !important;
			color: var(--aap-secondary-color) !important;
			transition: 0.4s ease-in-out 0s !important;
			font-size: 15px !important;
		}

		.inputemail.active {
			transition: 0.4s ease-in-out 0s !important;
			width: 270px !important;
			box-shadow: 0 0 0 transparent;
		}

		.ap-starter-btn-connect.email {
			transition: 0.4s ease-in-out 0s !important;
			width: 30px !important;
			border-top-right-radius: 0px !important;
			border-bottom-right-radius: 0px !important;
			padding: 10px !important;
		}

		.ap-starter-btn-connect.email .connect-label {
			display: none !important;
		}

		.ap-starter-btn-connect.email:hover {
			transition: 0.4s ease-in-out 0s !important;
			width: 300px !important;
		}

		.ap-starter-btn-connect.email:hover+.inputemail.active {
			width: 0px !important;
		}

		.ap-starter-btn-connect.email:hover .connect-label {
			display: inline !important;
		}

		.ap-starter-btn-sheet. {
			height: 43px;
			padding: 10px 20px !important;
		}

		.ap-starter-btn-close {
			border-radius: 0px 4px 4px 0px;
			background-color: #2c3e50;
			color: white !important;
			width: 100%;
			font-weight: 100;
		}

		.h5action {
			display: inline-flex;
			width: 330px;
			white-space: initial;
		}

		.swiper-slide .wrapper {
			padding: 20px 50px;
			text-align: left;
		}

		/* ----------------ftl--------------- */
		.cofinanceftl .ap-starter-btn-start-custom {
			border: 1.7px solid var(--aap-primary-ftl-color);
		}

		/*.cofinanceftl .co-popup-custom-finish:before {
            position: absolute;
            content: "";
            top: 0;
            right: -35px;
            width: 88px;
            height: 88px;
            background: var(--aap-primary-ftl-color);
            opacity: 0.2;
            border-radius: 8px;
            transform: rotate(45deg);
        }

        .cofinanceftl .co-popup-custom-finish:after {
            position: absolute;
            content: "";
            top: 30px;
            right: -35px;
            width: 88px;
            height: 88px;
            background: var(--aap-primary-ftl-color);
            opacity: 0.2;
            border-radius: 8px;
            transform: rotate(45deg);
        }*/

		.cofinanceftl .co-popup-custom-header p a {
			text-decoration: none;
			/* font-weight: bold; */
			color: var(--aap-primary-ftl-color);
			text-decoration: underline;
		}

		.cofinanceftl .co-popup-custom-actions li:first-child a {
			background-color: var(--aap-primary-ftl-color);
			color: white;
			/* font-weight: bold; */
		}

		.cofinanceftl .co-popup-custom-projectresume {
			background-color: var(--aap-primary-ftl-color);
			height: 76%;
			border-top-left-radius: 20px;
			border-bottom-right-radius: 20px;
			/*text-align: center;*/
			position: relative;
		}

		.cofinanceftl ul.timeline.aap-customListStepSwipping>li:before {
			content: ' ';
			background: #fff;
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid var(--aap-primary-ftl-color);
			left: 1px;
			width: 18px;
			height: 18px;
			z-index: 400;
		}

		.cofinanceftl ul.timeline.aap-customListStepSwipping>li:before {
			content: ' ';
			background: #fff;
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid var(--aap-primary-ftl-color);
			left: 1px;
			width: 18px;
			height: 18px;
			z-index: 400;
		}

		.cofinanceftl ul.timeline.aap-customListStepSwipping>li.active:before {
			content: ' ';
			background: var(--aap-primary-ftl-color);
			display: inline-block;
			position: absolute;
			border-radius: 50%;
			border: 3px solid var(--aap-secondary-dark);
			left: 1px;
			width: 18px;
			height: 18px;
			z-index: 400;
		}

		.cofinanceftl .li-start-custom,
		.cofinanceftl .li-abord-custom {
			background-color: var(--aap-primary-ftl-color);
			border-radius: 4px;
		}

		.cofinanceftl input.checkbox-custom-start:checked+div div+div .co-popup-custom-actions {
			transition: all 0.7s cubic-bezier(0.5, 0.005, 0.075, 0.985);
			width: 33.33333333%;
			left: 0;
			height: 70%;
			margin: 0;
			background-color: var(--aap-primary-ftl-color);
		}

		.cofinanceftl .co-popup-custom-header-logo img.logo-info {
			width: 75px;
			height: 75px;
			border: solid 2px var(--aap-primary-ftl-color);
			border-radius: 50%;
		}

		.cofinanceftl .co-popup-custom-menu img.logo-info {
			width: 50px;
			height: 50px;
			border: solid 2px var(--aap-primary-ftl-color);
			border-radius: 50%;
		}

		.cofinanceftl .preconfig-info-tooltip {
			display: block;
			position: absolute;
			padding: 10px 30px;
			border-radius: 5px;
			background: var(--aap-primary-ftl-color);
			text-align: center;
			color: white;
			opacity: 0;
			pointer-events: none;
			z-index: 5;
			bottom: 30%;
			transform: translate(-50%, 0px);
		}

        .wrapperlist {
            height: 270px !important;
            padding: 10px;
        }

        .wrapperlist {

        }
	}

	@media (max-width: 720px) {
		.custom-financer-bg {
			display: none;
		}

		.titre-desc-mobile {
			/* background-color: #8fbea9;*/
			background-color: #fff;
			padding: 20px;
			border-radius: 30px;
			text-align: -webkit-center;
			margin: 10px 0;
		}

		.titre-desc-mobile .titre {
			color: #0f6a42;
		}

		.titre-desc-mobile .description {
			color: #72997b;
			font-weight: bold;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            -webkit-line-clamp: 2;
            overflow: hidden;
            text-overflow: ellipsis;
		}

		.text1-mobile {
			text-align: -webkit-center;
			font-weight: bold;
			font-size: 20px;
			font-family: 'Montserrat';
		}

		.text2-mobile {
			text-align: -webkit-center;
			font-family: 'Montserrat';
		}

		/*.custom-financer-mobile-bg {
            display: block;
            background-image: -webkit-gradient(linear, 40% 0%, 75% 84%, from(#bceab5), to(#bdd7c3), color-stop(.6, #b3e3ce));
            background-repeat: no-repeat;
            padding: 20px;
        }*/

		.co-popup-custom-content,
		.co-popup-custom-finish {
			display: block;
			background-image: -webkit-gradient(linear, 40% 0%, 75% 84%, from(#bceab5), to(#bdd7c3), color-stop(.6, #b3e3ce));
			background-repeat: no-repeat;
			padding: 20px;
            width: 100%;
		}

		.custom-financer-header-mobile {
			position: relative;
			display: flex;
			flex-direction: column;
			align-items: center;
			height: 200px;
			background-image: -webkit-gradient(linear, 40% 0%, 75% 84%, from(#bceab5), to(#7ee281), color-stop(.6, #9eeaa5));
			background-repeat: no-repeat;
		}

		.chfm-slide-row {
			display: flex;
			transition: 0.5s;
			width: 300%;
		}

		.chfm-slide-col {
			position: relative;
			width: 100%;
			text-align: -webkit-center;
			padding: 20px;
		}

		.chfm-indicator {
			display: flex;
			justify-content: center;
			margin-top: 20px;
		}

		.chfm-indicator .cf-header-btn {
			display: inline-block;
			height: 15px;
			width: 15px;
			margin: 4px;
			border-radius: 15px;
			background: #fff;
			cursor: pointer;
			transition: all 0.5s ease-in-out;
		}

		.cf-header-btn.active {
			width: 30px;
		}

		.chfm-slider {
			width: 100%;
			overflow: hidden;
		}

		.statboard {
			display: inline-flex;
			width: 100%;
			margin-left: 5px;
			margin-right: 5px;
			text-align: -webkit-center;
			color: #808180;
		}

		.progress {
			height: 4px;
			background: #3b8d4e3d;
			border-radius: 0px;
		}

		.progress-bar {
			background-color: #fff;
		}

		.co-popup-custom-header-p {
			padding: 20px;
			color: #808180;
		}

		.statboard .statcol {
			width: 33.333%;
		}

		.statboard .statcolfinish {
			width: 50%;
		}

		.statcol:not(:last-child)::after {
			content: '';
			background: white;
			position: absolute;
			bottom: 0;
			left: 0;
			transform: translateY(-30%);
			width: 1.5px;
		}

		.co-popup-custom-actions li {
			list-style: none;
			padding: 20px;
			background-color: #8fbea9;
			border: 0;
			border-radius: 30px;
			margin-top: 10px;
			display: inline-flex;
			/*flex-direction: column;*/
			align-items: center;
			width: 100%;
		}

		.co-popup-custom-actions {
			margin: 0 !important;
			padding: 0;
		}

		.co-popup-custom-actions li a i {
			color: #f5e068;
			padding: 0 10px;
		}

		.co-popup-custom-actions li a {
			text-align: center;
			align-items: center;
			width: 100%;
			color: white;
			font-weight: bold;
		}

		.header-mobile {
			background-image: -webkit-gradient(linear, 40% 0%, 75% 84%, from(#bceab5), to(#bdd7c3), color-stop(.6, #b3e3ce));
			border-radius: 0 0 30px 30px;
			padding: 20px;
		}

		.back-to-dashboard-btn {
			background: transparent;
			border: none;
			width: 100%;
			font-weight: bold;
			color: white;
			text-transform: uppercase;
			letter-spacing: 15px;
		}

		.label-toogle-mobile-p-i {
			width: 50px;
			height: 30px;
			border-radius: 20px;
			background-color: white;
			text-align: center;
			float: right;
			margin: 10px;
		}

		.label-toogle-mobile-p-i:before {
			font-family: FontAwesome;
			content: '\f107';
			color: #000;
			font-size: 20px;
		}

		.toogle-mobile-p-i:checked+.label-toogle-mobile-p-i:before {
			font-family: FontAwesome;
			content: '\f106';
			color: #000;
			font-size: 20px;
		}

		.title-projectname {
			font-size: 20px;
			font-weight: bold;
			padding: 10px;
		}

		.title-projectdesc {
			padding: 10px;
			display: none;
			transition: all 1s ease-in-out;
		}

		.toogle-mobile-p-i:checked~.title-projectdesc {
			display: block;
			transition: all 1s ease-in-out;
		}

		.title-reste {
			display: none;
			transition: all 1s ease-out;
			background-color: white;
			padding: 10px;
			border-radius: 20px;
			width: fit-content;
			transition: all 1s ease-in-out;
		}

		.title-reste .actreste {
			font-size: 20px;
		}

		.mobile-presentation-info {
			transition: all 1s ease-out;
		}

		.toogle-mobile-p-i:checked~.title-reste {
			display: block;
			transition: all 1s ease-in-out;
		}

		/* .lnlist h5 {
            width: 50%;
        } */

		.swiper-container-initialized {
			padding: 20px;
		}

		.retour {
			display: inline-flex;
			width: 100%;
		}

		.profilPicDropdown img {
			border-radius: 50%;
		}

		.custom-financer-mobile-bg .dropleft img:not(.custom-fin-img-mobile) {
			display: none;
		}

		.h5action {
			display: inline-flex;
			width: 50%;
			white-space: initial;
		}

		.swiper-slide .wrapper {
			text-align: left;
		}

		.swiper-slide .row div {
			float: left;
		}

		/* .custom-fin-mult-img {

        } */

		.custom-financer-header-mobile-finish {
			position: relative;
			display: flex;
			flex-direction: column;
			align-items: center;
			background-repeat: no-repeat;
		}

		.custom-financer-header-mobile-finish .co-popup-custom-subtitle,
		.custom-financer-header-mobile-finish .co-popup-custom-header-p {
			text-align: -webkit-center;
		}

		.inputemail {
			padding: 0px !important;
			width: 0px !important;
			color: var(--aap-secondary-color) !important;
			transition: 0.4s ease-in-out 0s !important;
			font-size: 15px !important;
		}

		.inputemail.active {
			transition: 0.4s ease-in-out 0s !important;
			width: 270px !important;
			box-shadow: 0 0 0 transparent;
		}

		.ap-starter-btn-connect.email {
			transition: 0.4s ease-in-out 0s !important;
			width: 30px !important;
			border-top-right-radius: 0px !important;
			border-bottom-right-radius: 0px !important;
			padding: 10px !important;
		}

		.ap-starter-btn-connect.email .connect-label {
			display: none !important;
		}

		.ap-starter-btn-connect.email:hover {
			transition: 0.4s ease-in-out 0s !important;
			width: 300px !important;
		}

		.ap-starter-btn-connect.email:hover+.inputemail.active {
			width: 0px !important;
		}

		.ap-starter-btn-connect.email:hover .connect-label {
			display: inline !important;
		}

	}

	.cofinanceftl .li-start-custom {
		background-color: var(--aap-primary-ftl-color) !important;
	}

	.cofinanceftl .ap-starter-btn-start-custom {
		background-color: var(--aap-primary-ftl-color) !important;
		border: 1.7px solid var(--aap-primary-ftl-color);
		font-weight: bold;
	}

	.cofinanceftl .li-abord-cofinance {
		border: 1px solid var(--aap-secondary-ftl-color) !important;
		font-weight: bold;
		border-radius: 5px;
		margin-left: 20px;
	}

	.ap-starter-btn-close {
		border-radius: 0px 4px 4px 0px;
		border: 0;
		color: var(--aap-secondary-ftl-color) !important;
		width: auto;
		font-weight: 100;
		background-color: transparent;
	}

	.financerprogressinfo-img {
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.financerprogressinfo-img img {
		width: 40px;
		height: 40px;
		border: #43C9B7 1px solid;
		border-radius: 50%;
		object-fit: contain;
		background-color: white;
	}

	@media (max-width: 720px) {
		.co-popup-cofinance-container .custom-financer-mobile-bg {
			width: 100%;
			height: 100vh;
			position: fixed;
			top: 0;
			left: 0;
			background-color: rgba(0, 0, 0, .5);
			display: flex;
			justify-content: center;
			align-items: center;
			color: #2c3e50;
			z-index: 9999999999999999999999999999999;
		}

		.co-popup-cofinance-container .custom-financer-mobile-bg .co-popup-cofinance-content {
			background-color: #d8f4f0;
		}

		.co-popup-cofinance-container .custom-financer-mobile-bg .co-popup-custom-actions li {
			background-color: #43c9b7;
		}


		.co-popup-cofinance-container {
			top: 25%;
			position: fixed;
			background-color: #d8f4f0;
			width: 100% !important;
			height: 50% !important;
			z-index: 9999999999999999999999999999999;
		}

		.co-popup-cofinance-loader-section {
			top: 25%;
			position: fixed;
			background-color: #d8f4f0;
			width: 100% !important;
			height: 50% !important;
			z-index: 9999999999999999999999999999999;
		}

		.co-popup-cofinance-loader:after {
			background-image: radial-gradient(circle 5px at 12px 12px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 12px 0px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 0px 12px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 24px 12px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 12px 24px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 20px 3px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 20px 3px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 20px 20px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 3px 20px, #d8f4f0 100%, transparent 0), radial-gradient(circle 2.5px at 3px 3px, #d8f4f0 100%, transparent 0);
		}

		.co-popup-cofinance-loader:before {
			background-image: radial-gradient(circle 8px at 18px 18px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 18px 0px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 0px 18px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 36px 18px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 18px 36px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 30px 5px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 30px 5px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 30px 30px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 5px 30px, #d8f4f0 100%, transparent 0), radial-gradient(circle 4px at 5px 5px, #d8f4f0 100%, transparent 0);
		}

		.cofinancemobileftl .co-popup-custom-question {
			padding: 5px;
			width: 100%;
			height: 100%;
			overflow-y: scroll;
			background-color: white;

		}

		.cofinancemobileftl .swiper-wrapper .ap-starter-btn-close2 {
			display: none;
		}

		.cofinancemobileftl div:not(.swiper-wrapper) .ap-starter-btn-close2 {
			position: fixed;
			top: 0;
			right: 0;
		}

		.cofinancemobileftl .depensefinancerdropdown ul {
			width: 100%;
		}

		.cofinancemobileftl .depensefinancerdropdown li {
			width: 100%;
			height: auto;
		}

		.cofinancemobileftl .custom-financer-header-mobile {
			bottom: 0;
		}

		.cofinancemobileftl .input-amount-ftl .tab {
			margin-bottom: 150px;
		}

	}

    .recapcheck , .doublefinancecheck {
        height: 15px !important;
        position: relative !important;
        width: 15px !important;
    }

    .sfacc-container.col-md-12 {
        padding: 0 !important;
    }

    h5.grey{
        color : grey;
    }

    .radio-item.disabled , label.disabled {
        cursor: not-allowed;
    }

    .co-popup-custom-question [type="checkbox"]+span.disabled:before {

    }

    .co-popup-custom-question [type="checkbox"]+span.disabled:before {
        content: '\f00c';
        font-family: FontAwesome;
        color: var(--aap-primary-ftl-color);
        display: inline-block;
        width: 1.5em;
        height: 1.5em;
        vertical-align: -0.25em;
        border-radius: 0.25em;
        border: 0.125em solid #fff;
        box-shadow: 0 0 0 0.15em var(--aap-primary-ftl-color);
        transition: 0.5s ease all;
    }
	.progress {
		height: 40px;
		margin-bottom: 10px;
	}

    .mininputamount {
        color: #ee7b74;
        font-size: 12px;
        margin-bottom: 20px;
    }

    .title-projectname.titre , .title-projectdesc.description , .title-percent.actpercent , .title-percent-icon  {
        color: white !important;
    }

    .labelcofinance , .recapcheckcont {
        display: flex;
        justify-content: space-between;
    }

    .labelcofinance .doublefinancecheck  {
        margin: 10px;
        margin-left: 0px;
    }

    .recapcheckcont .recapcheck {
        margin: 10px;
        margin-left: 0px;
        width: 25px !important;
        height: 25px !important;
    }

    .slideinnnerheight {
        height: 350px !important;
    }

    .financerchoiceheader {
        padding-left: 20px;
        font-size: 16px;
    }

    .financerchoicedesc {
        padding: 5px;
        padding-left: 20px;
        font-size: 10px;
    }

    .financerlistbtn {
        padding-left: 20px;
        padding-right: 20px;
        width: max-content;
    }

    .depensefinancerdropdown .btn-group {
        width: 100%;
    }

    .btn-group-fc {
        margin-right: 20px;
        margin-left: 20px;
    }

    .userinfoco {
        margin: 20px;
        height: 220px;
    }

	.financeur-list {
		padding: 8px;
		max-height: 200px;
		overflow-y: auto;
		position: absolute;
		z-index: 10;
		display: none;
		background-color: #fff;
		box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
		border-radius: 10px;
	}
	.financeur-list.show {
		display: flex !important;
		flex-direction: column;
		justify-content: start;
		align-items: baseline;
	}

	.financ-list {
		display: flex;
		flex-direction: column;
		justify-content: start;
		align-items: baseline;
	}

	.financ-list img {
		width: 25px;
		height: 25px;
		border-radius: 50%;
		cursor: pointer;
		margin-right: 2px;
	}
	.financeur-list .close-i {
		margin-bottom: 5px;
		display: flex;
		justify-content: end;
		width: 100%;
		cursor: pointer;
	}
	#financeurCount {
		cursor: pointer;
	}

	.financeur-count-content {
		display: flex;
	}
	.financeur-count-content .nb-financeur {
		margin-right: 10px;
	}
</style>
<div class="pfpb-container pfpb-container-<?= $kunik ?>  <?= $kunik ?> <?= $kunik ?>-css" data-need-refreshblock="<?= (string) $blockCms["_id"] ?>" data-refreshblock-suppr="<?= $myCmsId ?>">
	<?php

	if (isset($blockCms["answer"]["answers"]["aapStep1"]["titre"]) && filter_var($blockCms["showtitle"], FILTER_VALIDATE_BOOLEAN)) {
	?>
		<h3 class="co-popup-custom-header-h2 c-opopup-custom-title pfpb-primary-green-color titre"><?= $blockCms["answer"]["answers"]["aapStep1"]["titre"] ?></h3>
	<?php
	}
	if (isset($blockCms["answer"]["answers"]["aapStep1"]["description"]) && filter_var($blockCms["showdesc"], FILTER_VALIDATE_BOOLEAN)) {
	?>
		<!--<div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color description"><?php /*= $blockCms["answer"]["answers"]["aapStep1"]["description"] */ ?></div>-->
	<?php
	}
	?>

	<div class="cofinancestat-container">
		<!--<div class=" margin-top-20 statboard">
            <div class="statcol">
                <h3 class="co-popup-custom-header-h2 co-popup-custom-title "><i class="fa fa-star pfpb-primary-green-color" ></i><span id="nbFin" class="financementnbr"><?php /*= count($blockCms["financementData"]["lines"]) */ ?></h3>
                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Cofinancement</div>
            </div>
            <div class="statcol">
                <h3 class="co-popup-custom-header-h2 co-popup-custom-title earn-title-header"><i class="fa fa-euro pfpb-primary-green-color"></i><span id="totalEarn" class="aap-primary-color amountearn"><?php /*= trim(strrev(chunk_split(strrev($blockCms["financementData"]["amountearn"]),3, ' '))) */ ?></span></h3>
                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Obtenu(s)</div>
            </div>
            <div class="statcol">
                <h3 class="co-popup-custom-header-h2 co-popup-custom-title"><i class="fa fa-euro"></i><span id="total" class="total"><?php /*= trim(strrev(chunk_split(strrev($blockCms["financementData"]["total"]),3, ' '))) */ ?></span></h3>
                <div class="co-popup-custom-header-h4 co-popup-custom-subtitle aap-secondary-color">Total</div>
            </div>
        </div>-->
        <div class="financement-detail">
            <div class="finance-content">
                <div class="recolte">
                    <span class="title"><?= Yii::t("cms", "Collected")?> : </span>
                    <span class="chiffr"><?= trim(strrev(chunk_split(strrev($blockCms["financementData"]["amountearn"]),3, ' '))) ?> <i class="fa fa-eur"></i></span>
                </div>
                <div class="financeur" id="financeurCount">
					<div class="financeur-count-content">
						<div class="nb-financeur"><?= count($blockCms["financerData"])?> </div>
						<div class=""> <?= Yii::t("cms", "Cofinancer")?>(s) </div>
					</div>
					<div class="financeur-list">
						<span class="close-i"><i class="fa fa-close"></i></span>
						<div class="financ-list">
							<?php foreach ($blockCms["financerData"] as $key => $value) {?>
								<a class="lbh-preview-element lbh contact<?=@$value['id']?>" href="#page.type.organizations.id.<?=$costum["contextId"] == $value["id"] ? $porteurId : @$value['id']?>">
								<img data-id="<?=@$value['id']?>" src="<?=$costum["contextId"] == $value["id"] ? $porteurImg : (isset($value['profilImg']) && $value['profilImg'] != '' ? $value['profilImg'] : $imgUrl) ?>"> <?= $costum["contextId"] == $value["id"] ? $porteurName : @$value['name'] ?> </a>
							<?php } ?>
						</div>
					</div>
                </div>
                <div class="objectif">
                    <span class="title"><?= Yii::t("cms", "Objective")?> : </span>
                    <span class="chiffre"><?= trim(strrev(chunk_split(strrev($blockCms["financementData"]["total"]),3, ' '))) ?> <i class="fa fa-eur"></i></span>
                </div>
            </div>
            <!--<div class="progress-content">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        70%
                    </div>
                </div>
            </div>
            <div class="btn-cofinance-content">
                <button class="btn-cofinance">Je cofinance</button>
            </div>-->
		</div>
		
		<div class="progress">
			<?php
			$amountearnTotal = 0;
			$amountpercentTotal = 0;
			$financerDetail = '';
			$imgDefault = Yii::app()->controller->module->assetsUrl . '/images/thumbnail-default.jpg';
			$aacParentName = $costum["title"];
			if (isset($blockCms["parentForm"]["parent"])) {
				$parentKeys = array_keys($blockCms["parentForm"]["parent"]);
				if (!empty($parentKeys)) {
					$aacParentName = $blockCms["parentForm"]["parent"][$parentKeys[0]]["name"];
				}
			}
			foreach ($blockCms["financerData"] as $financer => $data) {
				$amount = (float)$data['amount'];
				$percentage = 0;
				if ($amount != 0) {
					$percentage = ($amount * 100) / $blockCms["financementData"]["total"];
				}
				$amountpercentTotal += $percentage;
			}
			foreach ($blockCms["financerData"] as $financer => $data) {
				$amount = (float)$data['amount'];
				$percentage = 0;
				if (isset($data['id'])) {
					// $financerDetail = PHDB::findOneById(Organization::COLLECTION, $data['id']);
				}
				if ($amount != 0) {
					$percentage = ($amount * 100) / $blockCms["financementData"]["total"];
				}
			?>
				
				<div data-amount="<?= $amount ?>" data-financer="<?= $data['name'] == $aacParentName ? $porteurName : $data['name'] ?>" data-percentage="<?= $percentage ?>" class="progress-bar" role="progressbar" style="background: <?= $data['name'] == $aacParentName ? $blockCms["campColor"] : $data['color'] ?> ; width:<?= $percentage ?>%">

					<?php
					if ($financer + 1 == round(count($blockCms["financerData"]) / 2)) {
					?>
						<!--<span class="amountpercentTotal" >Completé : <?php /*= round($amountpercentTotal,2) */ ?>% </span>-->
					<?php
					}
					?>
					<div class="financerprogressinfo">
						<div class="financerprogressinfo-img"><img src="<?= $data['name'] == $aacParentName ? $porteurImg : (isset($data["profilImg"]) && $data["profilImg"] != '' ? $data["profilImg"] : $imgDefault) ?>" alt=""> </div>
						<div class="financerprogressinfo-name"><?= $data['name'] == $aacParentName ? $porteurName : $data['name'] ?> </div>
						<div class="financerprogressinfo-amount"><?= trim(strrev(chunk_split(strrev($amount), 3, ' '))) ?>€ </div>
						<div class="financerprogressinfo-percentage"><?= round($percentage, 2) ?>% </div>
					</div>
				</div>


			<?php
			}
			if ($blockCms["financementData"]["amountearn"] < $blockCms["financementData"]["total"]) {
			?>
				<div class="progress-bar" role="progressbar" style="background: grey ; width:<?= 100 - $amountpercentTotal ?>%">
					<!--<span class="amountpercentTotal" > Reste : <?php /*= round(100 - $amountpercentTotal , 2) */ ?>%</span>-->
				</div>
			<?php
			}
			?>
		</div>
		<?php if ($blockCms["showbtn"] && $canCofinance): ?>
			<div class="">
				<button class="btn-cofinance pull-right" data-id="<?= !empty($blockCms["answer"]["_id"]) ? (string)$blockCms["answer"]["_id"] : "" ?>"> <?= Yii::t("cms", "I cofinance")?> </button>
			</div>
		<?php endif; ?>
	</div>


</div>

<script>
	sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
    var reloadpfpbcontainer = sectionDyf.<?php echo $kunik ?>blockCms._id.$id;
	mylog.log('financeur data', <?php echo json_encode($blockCms); ?>)
	var projectAnswer = <?php echo (!empty($blockCms["answer"]) ? json_encode($blockCms["answer"]) : "null") ?>;
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#css-<?= $kunik ?>").append(str);
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms); ?>;
		var projectFinancingProgressBar = {
			configTabs: {
				style: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				},
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			}
		}
		cmsConstructor.blocks.projectFinancingProgressBar<?= $myCmsId ?> = projectFinancingProgressBar;
	}
	function reloadpfpb(callback = function() {}) {
		$(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}'] .pfpb-container`).html('<div class="co-popup-cofinance-loader-section"></div>');
		coInterface.showCostumLoader(".co-popup-cofinance-loader-section")
		ajaxPost(
			null,
			baseUrl + "/co2/cms/refreshblock", {
				idBlock: sectionDyf.<?php echo $kunik ?>blockCms._id.$id,
				clienturi: window.location.href
			},
			function(data) {
				if (data.result) {
					$(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}`).html(data["view"]);
					// cmsBuilder.block.initEvent();
					callback();
				} else {
					toastr.error("something went wrong!! please try again.");
				}
			},
			null,
			"json", {
				async: false
			}
		)
	}
	jQuery(document).ready(function() {
		$(".financeur-list .close-i i").click(function(){
			var self = $(this)
			self.parent().removeClass("show")
		})
		$('#financeurCount').click(function(){
			if($('.nb-financeur').text() == 0)
				return
			if($('.financeur-list').hasClass('show')){
				$('.financeur-list').removeClass('show')
			} else {
				$('.financeur-list').addClass('show')
			}
		})
		$(".btn-cofinance").off('click').on("click", function() {
			var thisbtn = $(this);
			var url_vars = {
				id: thisbtn.data('id'),
				form: projectAnswer.form,
				step: 'aapStep3',
				input: 'financer',
				view: 'custom',
				isinsideform: false
			};
			var url = baseUrl + '/survey/answer/answer';
			$.each(url_vars, function(key, val) {
				url += '/' + key + '/' + val;
			});
			var post = {
				url: window.location.href
			};
			var cofinance_container_dom = $('<div>', {
				class: 'co-popup-cofinance-container',
				html: '' +
					'<input type="checkbox" class="checkbox-cofinance-start-container hide">' +
					'<div class="co-popup-cofinance">' +
					'	<input type="checkbox" class="checkbox-preconfig-start hide">' +
					'	<div class="co-popup-cofinance-content" style="width: 800px;height: 500px;" >' +
					'		<div class="co-popup-cofinance-loader-section"></div>' +
					'	</div>' +
					'</div>'
			});
			$('.co-popup-cofinance-container').remove();
			$('body').append(cofinance_container_dom).ready(function() {
				coInterface.showCostumLoader(".co-popup-cofinance-loader-section")
				ajaxPost('.co-popup-cofinance-content', url, post,
					function(html) {
						$(".ap-starter-btn-close, .ap-starter-btn-close2").off('click').on("click", function() {
							cofinance_container_dom.remove();
						});
					}, null, 'html');
			});


		});
	});
</script>