<style>
	:root {
		--pfpb-primary-green-color: <?= $blockCms["primaryColor"] ?>;
		--pfpb-primary-grey-color: #808180;
		--pfpb-secondary-grey-color: #e6e1e1;
		--pfpb-primary-blue-color: <?= $blockCms["secondaryColor"] ?>;
		--pfpb-primary-red-color: rgb(238, 122, 122);
		--pfpb-secondary-red-color: #f1e2e2;
	}

	.d3actionview<?= $kunik ?> {
		margin: 20px;
		overflow: hidden;
	}

	.d3actionview<?= $kunik ?>.co-popup-custom-header-h2 {
		color: var(--pfpb-primary-green-color);
	}

	.d3actionview<?= $kunik ?>.co-popup-custom-header-h2 {
		text-align: center;
		padding: 20px;
	}

	.d3actionview<?= $kunik ?>.statbutton-group {
		display: inline-flex;
		flex-direction: horizontal;
		width: 100%;
	}

	.d3actionview<?= $kunik ?>.statbutton-multiselect-container {
		display: inline-flex;
		float: right;
		text-align: center;
		justify-content: center;
		transition: all 0.3s;
		width: 0;
		overflow: hidden;
	}

	.d3actionview<?= $kunik ?>.statbutton-multiselect-container.statbutton-multiselect-active {
		width: 100%;
	}

	.d3actionview<?= $kunik ?>.multiselect-group-button,
	.d3actionview<?= $kunik ?>.multiselect-group-button-2 {
		float: right;
		text-align: center;
		transition: all 0.3s;
		width: 0;
		/*overflow: hidden;*/
	}

	.d3actionview<?= $kunik ?>.multiselect-group-button.multiselect-group-button-active {
		width: 80%;
	}

	.d3actionview<?= $kunik ?>.multiselect-group-button-2.multiselect-group-button-active {
		width: min-content;
		margin-left: 20px;
	}

	.d3actionview<?= $kunik ?>.statbutton-cofinance {
		background: var(--pfpb-primary-green-color);
	}

	.d3actionview<?= $kunik ?>.statbutton-cofinance:disabled {
		background: var(--pfpb-primary-grey-color);
	}

	.d3actionview<?= $kunik ?>.input-group {
		padding: 5px;
		height: 40px;
	}

	.d3actionview<?= $kunik ?>input {
		padding: 5px;
		height: 30px;
		border-top-left-radius: 20px;
		border-bottom-left-radius: 20px;
		border: 2px solid var(--pfpb-primary-green-color);
	}

	.d3actionview<?= $kunik ?>select {
		padding: 5px;
		height: 30px;
		border-radius: 20px;
		border: 2px solid var(--pfpb-primary-green-color);
		padding: 5px;
		margin: 5px;
	}

	.d3actionview<?= $kunik ?>.jumpto-aos-container,
	.d3actionview<?= $kunik ?>.sct-aos-container {
		margin-left: 20px;
		margin-right: 20px;
	}

	.d3actionview<?= $kunik ?>.input-group-addon {
		padding: 5px;
		height: 30px;
		padding: 5px;
		border-top-right-radius: 20px;
		border-bottom-right-radius: 20px;
		background: var(--pfpb-primary-green-color);
	}

	.d3actionview<?= $kunik ?>.statbutton-cancel-multiselect span,
	.d3actionview<?= $kunik ?>.statbutton-sellectall span {
		transition: all 0.3s;
		width: 0 !important;
		overflow: hidden;
		display: inline-flex;
	}

	.d3actionview<?= $kunik ?>.statbutton-cancel-multiselect:hover span,
	.d3actionview<?= $kunik ?>.statbutton-sellectall:hover span {
		transition: all 0.3s;
		width: min-content !important;
		overflow: hidden;
		display: inline-flex;
	}

	.d3actionview<?= $kunik ?>.pasl-container .statbutton-cancel-multiselect:hover,
	.d3actionview<?= $kunik ?>.pasl-container .statbutton-sellectall:hover {
		transition: all 0.3s;
		width: min-content !important;
	}

	.d3actionview<?= $kunik ?>.statbutton-cancel-multiselect,
	.d3actionview<?= $kunik ?>.statbutton-sellectall {
		transition: all 0.3s;
		width: 50px;
		float: left;
	}

	.d3actionview<?= $kunik ?>.statbutton-cancel-multiselect {
		color: var(--pfpb-primary-red-color);
		background: var(--pfpb-secondary-grey-color);
	}

	.d3actionview<?= $kunik ?>.statbutton-sellectall {
		color: var(--pfpb-primary-blue-color);
		background: var(--pfpb-secondary-grey-color);
	}

	.d3actionview<?= $kunik ?>.multiselect-group-button {
		/*height: 40px*/
	}

	.d3actionview<?= $kunik ?>.pasl-actionecot {
		justify-items: center;
		text-align: center;
		background: #fff;
		margin: 10px;
		border-radius: 10px;
		height: 0;
		/*overflow: hidden;*/
	}

	.d3actionview<?= $kunik ?>.statbutton-cofinance {
		display: none;
		text-align: center;
		justify-content: center;
		width: 200px !important;
	}

	.d3actionview<?= $kunik ?>.statbutton-cofinance i::before {
		margin: auto;
	}

	.d3actionview<?= $kunik ?>.multiselect-group-button-active .statbutton-cofinance {
		z-index: 99999;
		display: inline-flex;
	}

	.d3actionview<?= $kunik ?>button {
		margin: 5px;
		background-color: var(--pfpb-primary-blue-color);
		border: 0;
		border-radius: 20px;
		padding: 5px 10px;
		color: white;
		font-weight: bold;
		width: 220px;
		padding: auto;
		height: 30px;
	}

	.d3actionview<?= $kunik ?>button i {
		display: contents;
	}

	.d3actionview<?= $kunik ?>.financementprogress-card button {
		background-color: white;
		color: var(--pfpb-primary-blue-color);
	}

	.d3actionview<?= $kunik ?>.statbutton-multiselect-container {
		float: right;
		text-align: center;
		transition: all 0.3s;
		width: 0;
		/*overflow: hidden;*/
	}

	.d3actionview<?= $kunik ?>.statbutton-multiselect-container.statbutton-multiselect-active {
		width: 100%;
	}

	.d3actionview<?= $kunik ?>.statbutton-group {
		display: inline-flex;
		flex-direction: horizontal;
		width: 100%;
	}

	.d3actionview<?= $kunik ?>.statbutton-cancel-multiselect:hover,
	.d3actionview<?= $kunik ?>.statbutton-sellectall:hover {
		transition: all 0.3s;
		width: min-content !important;
	}

	.d3actionview<?= $kunik ?>.statbutton-group {
		/*height: 40px;*/
	}

	.tspaninfo,
	.tspanselect,
	.tspanselect tspan,
	g.rectgaction.active text.tspaninfo {
		display: none;
	}

	g.rectg:hover text.tspaninfo,
	g.rectgaction:hover:not(.active) .tspaninfo,
	g.rectgaction.active .tspanselect tspan {
		display: block !important;
	}

	g.rectg:hover rect.prg-bar {
		stroke: var(--pfpb-primary-grey-color);
		stroke-width: 2;
	}

	g.rectg:hover rect.per {
		stroke-width: 0;
	}

	.swiper-wrapper.multiselect-active g.rectg rect.prg-bar {
		stroke: var(--pfpb-secondary-grey-color);
		stroke-width: 2;
	}

	.swiper-wrapper.multiselect-active g.rectg rect.per {
		stroke-width: 0;
	}

	.swiper-wrapper.multiselect-active g.rectgaction .tspanselect {
		display: block;
	}

	g.rectg.active .titleactive,
	g.rectgaction.active .titleactive {
		stroke: var(--pfpb-primary-green-color);
		fill: var(--pfpb-primary-green-color);
	}

	.swiper-wrapper.multiselect-active g.rectg.active rect.prg-bar,
	.swiper-wrapper.multiselect-active g.rectgaction.active rect.prg-bar {
		stroke: var(--pfpb-primary-green-color);
		stroke-width: 2;
	}

	.swiper-wrapper.multiselect-active g.rectgaction.active rect.per {
		stroke-width: 0;
	}

	.orgaaction-btngroup {
		position: relative;
		display: flex;
		flex-direction: row;
		border-collapse: separate;
		flex-wrap: nowrap;
	}

	.orgbtn {
		margin: 5px 10px 5px -21px !important;
		background-color: var(--pfpb-primary-blue-color);
		border: 0;
		border-radius: 0 20px 20px 0 !important;
		padding: 5px 10px;
		color: white;
		font-weight: bold;
		width: 80px !important;
		height: 30px;
	}

	.dropdown-menu-orgaaction {
		position: absolute;
		z-index: 9999999999999 !important;
		opacity: 1;
	}

	.d3actionview<?= $kunik ?>.dropdown .dropdown-menu-orgaaction {
		display: none;
		background: #e6e4e4;
		padding: 10px;
		border-radius: 20px;
		border: 2px solid var(--pfpb-secondary-grey-color);
	}

	.d3actionview<?= $kunik ?>.dropdown .dropdown-menu-orgaaction h3 {
		color: var(--pfpb-primary-grey-color);
		font-size: 15px;
	}

	.d3actionview<?= $kunik ?>.dropdown .dropdown-menu-orgaaction li {
		color: var(--pfpb-primary-grey-color);
		font-size: 12px;
		list-style: none;
	}

	.d3actionview<?= $kunik ?>.dropdown.open .dropdown-menu-orgaaction {
		display: block;
	}

	.d3actionview<?= $kunik ?>.dropdown-menu-orgaaction {
		right: 0;
		text-align: left;
		padding: 30px;
	}

	.d3actionview<?= $kunik ?>.dropdown-menu-orgaaction h3,
	.d3actionview<?= $kunik ?>.dropdown-menu-orgaaction .span-name {
		width: 100%;
		white-space: initial;
	}

	.d3actionview<?= $kunik ?>.dropdown-menu-orgaaction li .span-part {
		right: 20px;
		position: absolute;
		text-align: left;
		color: var(--pfpb-primary-blue-color);
		font-weight: bold;
	}

	.d3actionview<?= $kunik ?>.dropdown-menu-orgaaction .span-info {
		white-space: initial;
		font-size: 12px;
	}


	@media (min-width: 720px) {

		.multiselect-group-button,
		.multiselect-group-button-2 {
			display: inline-flex;
			flex-direction: horizontal;
			white-space: nowrap;
		}

		.dropdown-menu-orgaaction {
			width: 400px !important;
		}
	}

	@media (max-width: 720px) {
		.dropdown-menu-orgaaction {
			width: 100% !important;
		}
	}

	.aap-cover-spin {
		position: fixed;
		width: 100%;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		background-color: rgba(255, 255, 255, 0.7);
		z-index: 99999999;
		display: none;
	}

	@-webkit-keyframes spin {
		from {
			-webkit-transform: rotate(0deg);
		}

		to {
			-webkit-transform: rotate(360deg);
		}
	}

	@keyframes spin {
		from {
			transform: rotate(0deg);
		}

		to {
			transform: rotate(360deg);
		}
	}

	.aap-cover-spin {
		content: '';
		display: block;
		position: absolute;
		left: 48%;
		top: 40%;
		width: 40px;
		height: 40px;
		border: 4px solid black;
		border-top-color: transparent;
		border-radius: 50%;
		-webkit-animation: spin .8s linear infinite;
		animation: spin .8s linear infinite;
	}
</style>
<div class="d3actionview<?= $kunik ?>">
	<?php
	$myCmsId  = $blockCms["_id"]->{'$id'};
	if (empty($blockCms["form"])) {
	?>
		<h5 class=""> <i class="fa fa-cog"></i> Configuration requise </h5>
	<?php
	} else {
	?>
		<h3 class="co-popup-custom-header-h2 co-popup-custom-title aap-primary-color titre"><?= $blockCms["title"] ?> </h3>
		<div class="statbutton-group">
			<div class="statbutton-multiselect-container statbutton-multiselect-active">
				<div class="sct-aos-container pull-right">
					<select class="statorgaselect">
						<option value="financer">Vue des financeurs</option>
						<option value="type" selected>Vue des actions</option>
					</select>
				</div>
				<div>
					<button class="statbutton-multiselect"> Sélectionner et cofinancer <i class="fa fa-arrow-right pull-right"></i> </button>
				</div>
				<div class="jumpto-aos-container pull-right">
					Sauter sur
					<select class="jumpto-aos">
					</select>
				</div>
			</div>
			<div class="multiselect-group-button">
				<div class="input-group">
					<input class="input-name-multiselect-tree" type="text" placeholder="Nom">
					<span class="input-group-addon"> <i class="fa fa-user-o"></i> </span>
				</div>
				<div class="input-group">
					<input class="input-email-multiselect-tree" type="text" placeholder="Email">
					<span class="input-group-addon"> <i class="fa fa-envelope-o"></i> </span>
				</div>
				<div class="input-group">
					<input class="input-amount-multiselect-tree" type="number" placeholder="Montant">
					<span class="input-group-addon"> <i class="fa fa-euro"></i> </span>
				</div>
				<div class="orgaaction-btngroup">
					<button class="statbutton-cofinance" disabled> Cofinancer (<span class="action-select-count">0</span>) action(s) <i class="fa fa-arrow-right pull-right"></i> </button>
					<div class="dropdown">
						<button class="dropdown-toggle orgbtn" type="button" data-toggle="dropdown" id="dropdown-orgaaction" aria-haspopup="true" aria-expanded="false">
							Liste <span class="caret"></span>
						</button>
						<div class="dropdown-menu-orgaaction" aria-labelledby="dropdown-orgaaction">
							<li>
								Rien n'a encore été séléctionné
							</li>
						</div>
					</div>
				</div>
			</div>
			<div class="multiselect-group-button-2">
				<button class="statbutton-cancel-multiselect"> <span> Annuler </span> <i class="fa fa-times pull-right"></i> </button>
				<button class="statbutton-sellectall"> <span> Tout désélectionner </span> <i class="fa fa-square pull-right"></i> </button>
			</div>
		</div>
		<div style='width: 100%' class="swiper" id="oas-swiper-<?= $kunik ?>">
			<div class="swiper-wrapper">

			</div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-pagination"></div>
			<!--<div  class="feature" id="charttt"></div>-->
		</div>
	<?php
	}
	?>
</div>
<script>
	sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
	var data_formID = sectionDyf.<?php echo $kunik ?>blockCms.data;
	if (costum.editMode) {
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
		var orgaActionSlide = {
			configTabs: {
				general: {
					inputsConfig: [{
							type: "inputSimple",
							options: {
								name: "title",
								label: tradCms.title,
								collection: "cms"
							}
						},
						{
							type: "select",
							options: {
								name: "form",
								label: tradCms.selectaform,
								options: $.map(data_formID, function(index, val) {
									return {
										label: index,
										value: val
									}
								})
							}
						},
						{
							type: "section",
							options: {
								name: "primaryColor",
								label: tradCms.chooseThemeColor,
								inputs: [
									"color"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "secondaryColor",
								label: tradCms.SecondaryColor,
								inputs: [
									"color"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "fondNiv1",
								label: "Couleur de fond carte niveau 1",
								inputs: [
									"color"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "fondNiv2",
								label: "Couleur de fond carte niveau 2",
								inputs: [
									"color"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "progressNiv1",
								label: "Couleur de la progression carte niveau 1",
								inputs: [
									"color"
								]
							}
						},
						{
							type: "section",
							options: {
								name: "progressNiv2",
								label: "Couleur de la progression niveau 2",
								inputs: [
									"color"
								]
							}
						},
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path, valueToSet, name, payload, value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
			},
		}
		cmsConstructor.blocks.orgaActionSlide<?= $myCmsId ?> = orgaActionSlide;
	}

	if (notEmpty(sectionDyf.<?php echo $kunik ?>blockCms.form)) {
		var orgaActionObj = {
			parentDiv: document.getElementById("oas-swiper-<?= $kunik ?>"),
			width: 500,
			height: 500,
			filtertype: "type",
			isActive: false,
			activeList: {},
			index: 0
		}

		orgaActionObj.width = orgaActionObj.parentDiv.clientWidth;

		orgaActionObj.x = d3.scaleLinear().rangeRound([0, orgaActionObj.width]);
		orgaActionObj.y = d3.scaleLinear().rangeRound([0, orgaActionObj.height]);

		var totalAnswers = <?= json_encode($blockCms["answers"]) ?>;

		var totalfinancement = <?= json_encode($blockCms["totalfinancement"]) ?>;

		var financerTreeMap = {
			init: function() {

			},
			action: {}
		}

		var filteredAnswers = Object.values(totalAnswers).filter((answers => notEmpty(answers) && notEmpty(answers.answers) && notEmpty(answers.answers.aapStep1) && notEmpty(answers.answers.aapStep1.depense)));
	}
	jQuery(document).ready(function() {
		if (notEmpty(sectionDyf.<?php echo $kunik ?>blockCms.form)) {
			switchFilterTreemap();
			let swiper;
			swiper = new Swiper('#oas-swiper-<?= $kunik ?>', {
					loop: false,
					slidesPerView: 1,
					observer: true,
					observeParents: true,
					observeSlideChildren: true,
					slidesOffsetBefore: 0,
					slidesOffsetAfter: 0,
					direction: 'horizontal',
					pagination: {
						el: '.swiper-pagination',
					},
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',
					},
					scrollbar: {
						el: '.swiper-scrollbar'
					},
					allowTouchMove: false
				})
				.on('slideChange', function(e) {
					$('.d3actionview<?= $kunik ?> .jumpto-aos option').attr('selected', false);
					const swiperEl = document.getElementById('oas-swiper-<?= $kunik ?>');
					$('.d3actionview<?= $kunik ?> .jumpto-aos option[value="' + swiperEl.swiper.realIndex + '"]').attr('selected', 'selected');
				});
		}

		let resizeObserver = new ResizeObserver(() => {
			//reload<?= $kunik ?>();
		});

		resizeObserver.observe($('#oas-swiper-<?= $kunik ?>')[0]);

		$('.d3actionview<?= $kunik ?> .statorgaselect').on('change', function() {
			orgaActionObj.filtertype = $(this).children("option:selected").val();
			switchFilterTreemap($(this).children("option:selected").val());
			const swiperEl = document.getElementById('oas-swiper-<?= $kunik ?>');
			swiperEl.swiper.update();
			if (orgaActionObj.filtertype != "type") {
				$(".d3actionview<?= $kunik ?> .statbutton-multiselect").hide();
				if ($('.d3actionview<?= $kunik ?> .swiper-wrapper').hasClass('multiselect-active')) {
					$('.d3actionview<?= $kunik ?> .swiper-wrapper').removeClass('multiselect-active');
					orgaActionObj.isActive = false;
					$('g.rectgaction').attr('class', 'rectgaction');
				}
			} else {
				$(".d3actionview<?= $kunik ?> .statbutton-multiselect").show();
			}

			if (orgaActionObj.filtertype != "type") {
				$(".d3actionview<?= $kunik ?> .statbutton-multiselect").hide();
			} else {
				$(".d3actionview<?= $kunik ?> .statbutton-multiselect").show();
			}
		});

		$('.d3actionview<?= $kunik ?> .jumpto-aos').off().on('change', function() {
			var jumptoaosValue = $(this).children("option:selected").val();
			const swiperEl = document.getElementById('oas-swiper-<?= $kunik ?>');
			swiperEl.swiper.slideTo(jumptoaosValue);
			swiperEl.swiper.update();
			return false;
		});

		$('.d3actionview<?= $kunik ?> .statbutton-multiselect').off().on("click", function() {
			$('.d3actionview<?= $kunik ?> .swiper-wrapper').addClass('multiselect-active');
			$('.d3actionview<?= $kunik ?> .multiselect-group-button').addClass('multiselect-group-button-active');
			$('.d3actionview<?= $kunik ?> .multiselect-group-button-2').addClass('multiselect-group-button-active');
			$('.d3actionview<?= $kunik ?> .statbutton-multiselect-container').removeClass('statbutton-multiselect-active');
			orgaActionObj.isActive = true;
			/*validateCoFinancement();
			disturbAmount();*/
			if (notEmpty(userConnected)) {
				$('.d3actionview<?= $kunik ?> .input-name-multiselect-tree').val(userConnected.name);
				$('.d3actionview<?= $kunik ?> .input-email-multiselect-tree').val(userConnected.email);
			}

			/*d3.selectAll("rect.per")
			    .attr("transform", d => {
			        if(notEmpty(d.attr('class'))){
			            return `translate(2,${((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) - (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10)*Math.round((d.data.financement / d.data.price) * 100))/100) ) + 2})`;
			        }
			        return "";
			    })*/
			//.attr("transform", d => d === root ? `translate(0,0)` : `translate(2,${((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) - (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10)*Math.round((d.data.financement / d.data.price) * 100))/100) ) + 2})`)

		});

		$('.d3actionview<?= $kunik ?> .statbutton-cancel-multiselect').off().on("click", function() {
			$('.d3actionview<?= $kunik ?> .swiper-wrapper').removeClass('multiselect-active');
			$('.d3actionview<?= $kunik ?> .multiselect-group-button').removeClass('multiselect-group-button-active');
			$('.d3actionview<?= $kunik ?> .multiselect-group-button-2').removeClass('multiselect-group-button-active');
			$('.d3actionview<?= $kunik ?> .statbutton-multiselect-container').addClass('statbutton-multiselect-active');
			$('.d3actionview<?= $kunik ?> .financementprogress-card-chkb').prop('checked', false);
			orgaActionObj.isActive = false;
			$('g.rectgaction').attr('class', 'rectgaction');
		});

		$('.d3actionview<?= $kunik ?> .statbutton-sellectall').off().on("click", function() {
			orgaActionObj.activeList = {};
			updatePart();
			updateDropdownList();
			updateTilePart();
			$('.d3actionview<?= $kunik ?> g.rectgaction').each(function() {
				d3.select(this).classed('active', false);
			});
		});

		$('.d3actionview<?= $kunik ?> .multiselect-group-button .statbutton-cofinance').off().on("click", function() {
			var lineToPay = [];
			$.each(orgaActionObj.activeList, function(index, value) {
				lineToPay.push({
					uid: value.uid,
					id: value.id,
					amount: value.part
				});
			});

			var tplCtx = {
				collection: "answers",
				arrayForm: true,
				form: "aapStep1",
				setType: [{
						"path": "amount",
						"type": "int"

					},
					{
						"path": "date",
						"type": "isoDate"
					}
				]
			}

			$.each(lineToPay, function(index, value) {
				tplCtx.id = value.id
				tplCtx.path = "answers.aapStep1.depense." + value.uid + ".financer";
				var today = new Date();
				today = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
				tplCtx.value = {
					line: '',
					amount: value.amount,
					user: userId,
					date: today,
					name: $('.d3actionview<?= $kunik ?> .input-name-multiselect-tree').val(),
					email: $('.d3actionview<?= $kunik ?> .input-email-multiselect-tree').val(),
					id: userConnected._id.$id
				}

				if (value.amount > 0) {
					dataHelper.path2Value(tplCtx, function(params) {

					});
				}
			});
			//urlCtrl.loadByHash(location.hash);
			//cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			reload<?= $kunik ?>();
		});

		$('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').on('keyup', function() {
			updatePart();
			validateCoFinancement();
			updateTilePart();
		});

		$('body').off().on('focus', '.d3actionview<?= $kunik ?> [contenteditable]', function() {
			const $thisContEdble = $(this);
			$thisContEdble.prop('data-before', $thisContEdble.html());
		}).off().on('keyup', '.d3actionview<?= $kunik ?> [contenteditable]', function() {
			const $thisContEdble = $(this);
			if ($thisContEdble.data('before') !== $thisContEdble.html()) {
				$thisContEdble.data('before', $thisContEdble.html());
				$thisContEdble.trigger('change');
			}
		})

		$('body').on('change', '.d3actionview<?= $kunik ?> [contenteditable]', function() {
			var thiseot = $(this);
			var thiseotElm = this;
			var amountInserted = parseInt($('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val());
			if (isNaN(parseInt(thiseot.html())) || parseInt(thiseot.html()) < 0) {
				thiseot.html('0');
			}

			/*var totalpart = 0;
			$.each(orgaActionObj.activeList, function (index,value){
			    totalpart += value.part;
			});*/

			if (amountInserted < parseInt(thiseot.html())) {
				thiseot.html(amountInserted);
			}

			if (parseInt(thiseot.html()) > parseInt(thiseot.data('reste'))) {
				thiseot.html(thiseot.data('reste'));
			}

			orgaActionObj.activeList[thiseot.data('iduid')].changed = true;
			orgaActionObj.activeList[thiseot.data('iduid')].part = thiseot.html();

			updatePart();
			validateCoFinancement();
			updateTilePart();
		});
	});

	function render(group, root, svg) {
		const node = group
			.selectAll("g")
			.data(root.children.concat(root))
			.join("g");

		node.filter(d => d === root ? d.parent : d.children)
			.attr("cursor", "pointer")
			.on("click", (event, d) => d === root ? zoomout(root, group, svg) : zoomin(d, group, svg));

		node.filter(d => d.depth === 1)
			.classed("rectg", true)

		node.filter(d => d.depth === 2)
			.attr("cursor", "pointer")
			.classed("rectgaction", true)
			.on("click", function() {
				if (orgaActionObj.isActive) {
					var activeClass = "active";
					var alrActive = d3.select(this).classed(activeClass);
					d3.select(this).classed(activeClass, !alrActive);
					if (!alrActive) {
						orgaActionObj.activeList[d3.select(this).attr('data-id') + d3.select(this).attr('data-uid')] = {
							id: d3.select(this).attr('data-id'),
							uid: d3.select(this).attr('data-uid'),
							part: d3.select(this).attr('data-part'),
							reste: d3.select(this).attr('data-reste'),
							name: d3.select(this).attr('data-name'),
							parentname: d3.select(this).attr('data-parentname'),
						}
					} else {
						delete orgaActionObj.activeList[d3.select(this).attr('data-id') + d3.select(this).attr('data-uid')];
					}

					updatePart();
					validateCoFinancement();
					updateTilePart();
				}
			});

		node.filter(d => notEmpty(d.parent) && (notEmpty(d.data.uid) || d.data.uid == 0) && notEmpty(orgaActionObj.activeList[d.parent.data.id + d.data.uid]))
			.classed("active", true);

		node.filter(d => d !== root && notEmpty(d.children) && notEmpty(d.data.id) && Object.keys(orgaActionObj.activeList).some(function(k) {
				return ~k.indexOf(d.data.id)
			}))
			.classed("active", true);

		node.filter(d => d.depth == 2)
			.attr("data-id", d => d.parent.data.id)
			.attr("data-parentname", d => d.parent.data.name)
			.attr("data-name", d => d.data.name)
			.attr("data-uid", d => d.data.uid)
			.attr("data-reste", d => d.data.reste)
			.attr("data-part", d => d.data.part)

		node.filter(d => d.depth == 2)
			.attr("data-id", d => d.parent.data.id)

		node.append("title")
			.text(d => `${treemapName(d)}\n${frformatNum(d.value)}`);

		/*node.append("defs")
		    .append("pattern")
		    .attr("id", d => (d === root ? "bg"+treemapName(d).replace(/[^A-Z0-9]/ig, '') : "bg"+d.data.name.replace(/[^A-Z0-9]/ig, '')))
		    .append("image")
		    .attr("x", 2)
		    .attr("width", 76)
		    .attr("height", 120)
		    .attr("xlink:href", baseUrl+defaultImage);*/

		node.append("rect")
			.attr("fill", d => d === root ? sectionDyf.<?php echo $kunik ?>blockCms.fondNiv1 : d.children ? sectionDyf.<?php echo $kunik ?>blockCms.fondNiv1 : sectionDyf.<?php echo $kunik ?>blockCms.fondNiv2)
			.attr("rx", 20)
			.attr('class', 'prg-bar')
			.attr("stroke", "#fff");

		node.append("rect")
			.attr("fill", d => d === root ? sectionDyf.<?php echo $kunik ?>blockCms.primaryColor : d.children ? sectionDyf.<?php echo $kunik ?>blockCms.progressNiv1 : sectionDyf.<?php echo $kunik ?>blockCms.progressNiv2)
			.attr("rx", 20)
			.attr('class', 'per')
			.attr("stroke", "#fff");

		node.append("clipPath")
			.append("use")

		node.append("text")
			.attr("fill", (d => (d === root ? "#808180" : "#7193c2")))
			.attr("stroke", (d => (d === root ? "#808180" : "#7193c2")))
			.attr("stroke-width", 0.5)
			.selectAll("tspan")
			//.data(d => (d === root ? [treemapName(d)] : d.data.name.split(/(?=[A-Z][^A-Z])/g)))
			.data(d => (d === root ? [treemapName(d)] : treemapName2(d)))
			.join("tspan")
			.attr("x", 20)
			//.attr("y", (d, i, nodes) => `${(i === nodes.length - 1) * 0.3 + 1.1 + i * 0.9}em`)
			.attr("y", (d, i, nodes) => (d === root ? 20 : (20 * i) + 20))
			.attr("font-weight", (d => (d === root ? "bold" : "bold")))
			.text(d => d)
			.attr('class', "titleactive")
			.style("font", "16px montserrat");

		node.append("text")
			.attr('class', (d => (d === root ? "" : "tspaninfo")))
			.each(function(d) {
				var text = d3.select(this);
				$.each(treemapName3(d, root), function(index, value) {
					text.append("tspan")
						.attr("x", 20)
						.attr("y", (d, i, nodes) => {
							if (d === root)
								return 40;
							return 20 + (treemapName2(d).length * 20) + index * 20;
						})
						.style("font", "15px montserrat")
						.attr("fill", '#33363D')
						.attr("stroke", "#33363D")
						.attr("stroke-width", 0.5)
						.text(value);
				});
			});

		node.append("text")
			.attr('class', (d => (d === root ? "" : "tspanselect")))
			.each(function(d) {
				var text = d3.select(this);
				if (d !== root) {
					$.each(treemapName4(d), function(index, value) {
						text.append("tspan")
							.attr("x", 20)
							.attr("y", (d, i, nodes) => {
								if (d === root)
									return 40;
								return 20 + (treemapName2(d).length * 20) + index * 20;
							})
							.style("font", "14px montserrat")
							.attr("fill", '#33363D')
							.attr("stroke", "#33363D")
							.attr("stroke-width", 0.5)
							.text(value);
					});
				}
			});

		node.append("text")
			.attr("x", (d => (d === root ? "" : (orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - treemapsize(d, (orgaActionObj.y(d.x1) - orgaActionObj.y(d.x0))) * 3))))
			.attr("y", (d => (d === root ? "" : (orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - treemapsize(d, (orgaActionObj.y(d.x1) - orgaActionObj.y(d.x0)))))))
			.style("font", (d => (d === root || isNaN(Math.round((d.data.financement / d.data.price) * 100)) ? "" : treemapsize(d, (orgaActionObj.y(d.x1) - orgaActionObj.y(d.x0))) + "px montserrat")))
			.attr("opacity", 0.2)
			.attr("font-weight", (d => (d === root ? "normal" : "bold")))
			.text(d => (d === root || isNaN(Math.round((d.data.financement / d.data.price) * 100)) ? "" : Math.round((d.data.financement / d.data.price) * 100) + "%"));

		var nodev1 = node.append("text")
			.attr("x", (d => (d !== root ? "" : (orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 150))))
			.attr("y", (d => (d !== root ? "" : 10)))
			.style("font", "15px montserrat")
			.attr("fill", "#33363D")
			.attr("font-weight", (d => (d === root ? "normal" : "bold")))

		nodev1.append("tspan")
			.attr("x", (d => (d !== root ? "" : (orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 150))))
			.text(d => (d !== root ? "" : "Niveau " + (parseInt(d.depth) + 1)))
			.attr("y", 20);

		nodev1.append("tspan")
			.attr("x", (d => (d !== root ? "" : (orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 150))))
			.attr("y", 40)
			.text(d => (d === root && d.depth == 1 ? "Retour au niv.1" : ""));


		group.call(position, root);
	}

	function position(group, root) {
		/*group.selectAll("g")
		    .attr("data-reste", d => d.data.reste)
		    .attr("data-part", d => d.data.part)*/

		group.selectAll("g")
			.attr("transform", d => d === root ? `translate(10,-50)` : `translate(${orgaActionObj.x(d.x0) +10},${orgaActionObj.y(d.y0) +10})`)
			.select("rect.prg-bar")
			.attr("width", d => d === root ? orgaActionObj.width : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 10)
			.attr("height", d => d === root ? 50 : orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10);

		group.selectAll("g")
			.attr("transform", d => d === root ? `translate(10,-50)` : `translate(${orgaActionObj.x(d.x0) +10},${orgaActionObj.y(d.y0)+10})`)
			.select("rect.per")
			.attr("width", d => d === root ? (d.depth == 0 ? (d.data.financement / d.data.total) * orgaActionObj.width : (d.data.financement / d.value) * orgaActionObj.width) : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 10)
			.attr("height", d => d === root ? 50 : ((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) * Math.round((d.data.financement / d.data.price) * 100)) / 100)
			.attr("data-width1", d => d === root ? (d.depth == 0 ? (d.data.financement / d.data.total) * orgaActionObj.width : (d.data.financement / d.value) * orgaActionObj.width) - 1 : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 11)
			.attr("data-height1", d => d === root ? 50 : (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) * Math.round((d.data.financement / d.data.price) * 100)) / 100) - 1)
			.attr("data-width2", d => d === root ? (d.depth == 0 ? (d.data.financement / d.data.total) * orgaActionObj.width : (d.data.financement / d.value) * orgaActionObj.width) : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 10)
			.attr("data-height2", d => d === root ? 50 : ((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) * Math.round((d.data.financement / d.data.price) * 100)) / 100);

		group.selectAll("rect.per")
			.attr("transform", d => d === root ? `translate(0,0)` : `translate(0,${((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) - (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10)*Math.round((d.data.financement / d.data.price) * 100))/100)) })`)
	}

	function zomminprogress(thiss, d, root) {
		d3.select(thiss)
			.attr("transform", d => d === root ? `translate(10,-50)` : `translate(${orgaActionObj.x(d.x0) +10},${orgaActionObj.y(d.y0)+10})`)
			.select("rect.per")
			.attr("width", d => d === root ? (d.depth == 0 ? (d.data.financement / d.data.total) * orgaActionObj.width : (d.data.financement / d.value) * orgaActionObj.width) - 1 : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 14)
			.attr("height", d => d === root ? 50 : (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) * Math.round((d.data.financement / d.data.price) * 100)) / 100) - 4)
			.select("rect.per")
			.attr("transform", d => d === root ? `translate(0,0)` : `translate(2,${((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) - (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10)*Math.round((d.data.financement / d.data.price) * 100))/100) ) + 2})`);
	}

	function zommoutprogress(thiss, d, root) {
		d3.select(thiss)
			.attr("transform", d => d === root ? `translate(10,-50)` : `translate(${orgaActionObj.x(d.x0) +10},${orgaActionObj.y(d.y0)+10})`)
			.select("rect.per")
			.attr("width", d => d === root ? (d.depth == 0 ? (d.data.financement / d.data.total) * orgaActionObj.width : (d.data.financement / d.value) * orgaActionObj.width) : orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0) - 10)
			.attr("height", d => d === root ? 50 : ((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) * Math.round((d.data.financement / d.data.price) * 100)) / 100)
			.select("rect.per")
			.attr("transform", d => d === root ? `translate(0,0)` : `translate(0,${((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10) - (((orgaActionObj.y(d.y1) - orgaActionObj.y(d.y0) - 10)*Math.round((d.data.financement / d.data.price) * 100))/100)) })`);
	}

	// When zooming in, draw the new nodes on top, and fade them in.
	function zoomin(d, group, svg) {
		console.log('dddddd', d);
		orgaActionObj.x.domain([d.x0, d.x1]);
		orgaActionObj.y.domain([d.y0, d.y1]);

		const group0 = group.attr("pointer-events", "none");
		const group1 = group = svg.append("g").call(render, d, svg);

		svg.transition()
			.duration(750)
			.call(t => group0.transition(t).remove()
				.call(position, d.parent))
			.call(t => group1.transition(t)
				.attrTween("opacity", () => d3.interpolate(0, 1))
				.call(position, d));
	}

	// When zooming out, draw the old nodes on top, and fade them out.
	function zoomout(d, group, svg) {
		console.log('dddddd', d);
		orgaActionObj.x.domain([d.parent.x0, d.parent.x1]);
		orgaActionObj.y.domain([d.parent.y0, d.parent.y1]);

		const group0 = group.attr("pointer-events", "none");
		const group1 = group = svg.insert("g", "*").call(render, d.parent, svg);

		svg.transition()
			.duration(750)
			.call(t => group0.transition(t).remove()
				.attrTween("opacity", () => d3.interpolate(1, 0))
				.call(position, d))
			.call(t => group1.transition(t)
				.call(position, d.parent));
	}

	function frformatNum(number) {
		if (notEmpty(number)) {
			number = number.toString();
			var pattern = /(-?\d+)(\d{3})/;
			while (pattern.test(number))
				number = number.replace(pattern, "$1 $2") + "€";
		} else {
			number = "0";
		}
		return number;
	}

	function filterNiv1type(answers) {
		return {
			id: answers._id.$id,
			name: answers.answers.aapStep1.titre,
			image: answers.answers.aapStep1.image,
			financers: answers.answers.aapStep1.depense.reduce(getfinancerName, []),
			financement: answers.answers.aapStep1.depense.reduce(getfinancement, 0),
			price: answers.answers.aapStep1.depense.reduce((a, v) => v.price ? a + parseInt(v.price) : a, 0),
			financementCount: answers.answers.aapStep1.depense.reduce((a, v) => v.financer ? a + v.financer.length : a, 0),
			type: typeof answers.project != "undefined" && typeof answers.project.id != "undefined" ? "project" : "proposition",
			children: answers.answers.aapStep1.depense.map(filterNiv2type)
		}
	}

	function filterNiv2type(depense, index) {
		return {
			uid: index,
			name: depense.poste,
			value: depense.price,
			image: depense.image,
			financers: depense.financer ? getfinNameAmount(depense.financer) : [],
			price: depense.price,
			financementCount: depense.financer ? depense.financer.length : 0,
			financement: depense.financer ? depense.financer.reduce((n, {
				amount
			}) => n + parseInt(amount), 0) : 0,
			reste: depense.price - (depense.financer ? depense.financer.reduce((n, {
				amount
			}) => n + parseInt(amount), 0) : 0)
		}
	}

	function filterNiv1fin(accumulator, answers) {
		if (notEmpty(answers.answers)) {
			var financers = answers.answers.aapStep1.depense.reduce(getfinancerName, []);
			$.each(Object.values(financers), function(index, value) {
				var topush = {
					id: answers._id.$id,
					name: answers.answers.aapStep1.titre,
					image: answers.answers.aapStep1.image,
					financers: value,
					price: answers.answers.aapStep1.depense.reduce((a, v) => v.price ? a + parseInt(v.price) : a, 0),
					financement: answers.answers.aapStep1.depense.reduce(getfinancement, 0),
					financementCount: answers.answers.aapStep1.depense.reduce((a, v) => v.financer ? a + v.financer.length : a, 0),
					type: typeof answers.project != "undefined" && typeof answers.project.id != "undefined" ? "project" : "proposition",
					children: answers.answers.aapStep1.depense.map(filterNiv2fin(value))
				}
				accumulator.push(topush);
			})
		}
		return accumulator;
	}

	function filterNiv2fin(finName) {
		return function(depense) {
			if (notEmpty(depense)) {
				var financemenforX = 0;
				if (notEmpty(depense.financer)) {
					$.each(depense.financer, function(id, fin) {
						if (fin.name == finName) {
							financemenforX += parseInt(fin.amount);
						}
					});
				}

				return {
					name: depense.poste,
					price: depense.price,
					image: depense.image,
					financers: finName,
					financement: financemenforX,
					value: financemenforX,
					reste: depense.price - financemenforX
				}
			}
		}
	}

	function lvl2reduceNiv1financer(accumulator, value) {
		if (notEmpty(value.financers)) {
			const notchange = value;
			$.each(Object.keys(notchange.financers), function(index, value2) {
				var topush = {};
				topush = {
					id: notchange.id,
					name: notchange.name,
					image: notchange.image,
					financementCount: notchange.financementCount,
					type: notchange.type,
				}
				topush.financers = value2;
				topush.children = notchange.children.filter(lvl2filterNiv2financer(value2));
				accumulator.push(topush);
			});
		}
		return accumulator;
	}

	function lvl2filterNiv2financer(index) {
		return function(x) {
			if (notEmpty(x)) {
				return {
					name: x.name,
					price: x.value,
					image: x.image,
					financers: index,
					financementCount: x.financementCount,
					value: x.financers[index] ? x.financers[index] : 0
				}
			}
		}
	}

	function getfinancerName(accumulator, value) {
		if (typeof value.financer != "undefined") {
			$.each(value.financer, function(id, fin) {
				if (typeof fin.name != "undefined") {
					if (accumulator[fin.name]) {
						accumulator[fin.name] += parseInt(fin.amount)
					} else {
						accumulator[fin.name] = parseInt(fin.amount)
					}
				}
			});
		}
		return accumulator;
	}

	function getfinancement(accumulator, value) {
		if (typeof value.financer != "undefined") {
			$.each(value.financer, function(id, fin) {
				if (typeof fin.amount != "undefined") {
					accumulator += parseInt(fin.amount);
				}
			});
		}
		return accumulator;
	}

	function getfinNameAmount(vf) {
		var fn = {};
		$.each(vf, function(id, fin) {
			if (fn[fin.name]) {
				fn[fin.name] += parseInt(fin.amount)
			} else {
				fn[fin.name] = parseInt(fin.amount)
			}
		});
		return fn;
	}

	function tile(node, x0, y0, x1, y1) {
		myd3binary(node, 0, 0, orgaActionObj.width, orgaActionObj.height);
		for (const child of node.children) {
			child.x0 = x0 + child.x0 / orgaActionObj.width * (x1 - x0);
			child.x1 = x0 + child.x1 / orgaActionObj.width * (x1 - x0);
			child.y0 = y0 + child.y0 / orgaActionObj.height * (y1 - y0);
			child.y1 = y0 + child.y1 / orgaActionObj.height * (y1 - y0);
		}
	}

	function treemapName(d) {
		var arr = d.ancestors().map(d => d.data.name).reverse();
		var res = "";
		for (i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				res += " > " + arr[i];
			} else if (i -= 0) {
				res += arr[i];
			} else {
				res += " > " + arr[i];
			}
		}
		return res;
	}

	function treemapName2(d) {
		var dataname = "";
		if (notEmpty(d.data.name)) {
			dataname = d.data.name;
		}
		var res = [];
		var rp = ((orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0)) / orgaActionObj.width) * 100;

		var arr1 = dataname.split(" ");
		var tt = 0;
		if (rp < 20) {
			tt = 0;
		} else if (rp < 30) {
			tt = 1;
		} else if (rp < 40) {
			tt = 2;
		} else if (rp < 50) {
			tt = 3;
		} else if (rp < 60) {
			tt = 4;
		} else if (rp < 90) {
			tt = 5;
		} else {
			tt = 20;
		}
		if (arr1.length < tt) {
			return [dataname];
		}
		var i = 0;
		var i2 = 0;
		var ii = [];
		$.each(arr1, function(index, value) {
			if (i2 == 4) {
				return;
			}
			if (i < tt) {
				ii.push(value);
				if ((i2 * (tt + 1)) + i + 1 == arr1.length) {
					res.push(ii.join(' '));
				}
				i++;
			} else if (i == tt) {
				ii.push(value);
				res.push(ii.join(' '));
				i = 0;
				ii = [];
				i2++;
			}
		});
		return res;
	}

	function treemapName3(d, root) {
		var textvalue = "";
		if (d === root && orgaActionObj.filtertype == "type" && d.depth == 0) {
			textvalue += "Total: " + frformatNum(d.data.total).replace(" ", "-") + "~Reste à financer des " + d.data.keyname + ": " + frformatNum(d.data.total - d.data.financement).replace(" ", "-");
		} else if (d === root && orgaActionObj.filtertype == "type" && d.depth == 1) {
			textvalue += "Total: " + frformatNum(d.value).replace(" ", "-") + " ~Reste à financer pour " + d.data.name + " : " + frformatNum(d.value - d.data.financement).replace(" ", "-");
		} else if (d !== root && orgaActionObj.filtertype == "type") {
			textvalue += "Total: " + frformatNum(d.value).replace(" ", "-") + " ~" + d.data.financementCount + 'Cofinancement: ' + frformatNum(d.data.financement).replace(" ", "-").replace("_", "-");
		} else if (d !== root && orgaActionObj.filtertype != "type") {
			textvalue += "Montant: " + frformatNum(d.data.price).replace(" ", "-") + " Total financé: " + frformatNum(d.value).replace(" ", "-");
		}

		var res = [];
		var rp = ((orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0)) / orgaActionObj.width) * 100;

		var arr1 = textvalue.split(" ");

		var tt = 0;

		if (rp < 20) {
			tt = 0;
		} else if (rp < 30) {
			tt = 1;
		} else if (rp < 40) {
			tt = 2;
		} else if (rp < 50) {
			tt = 3;
		} else if (rp < 60) {
			tt = 4;
		} else if (rp < 90) {
			tt = 5;
		} else {
			tt = 20;
		}
		if (arr1.length < tt || d === root) {
			return [textvalue.replace("-", " ")];
		}
		var i = 0;
		var i2 = 0;
		var ii = [];

		$.each(arr1, function(index, value) {
			if (i < tt) {
				ii.push(value.replace("-", " "));
				if ((i2 * (tt + 1)) + i + 1 == arr1.length) {
					res.push(ii.join(' '));
				}
				i++;
			} else if (i == tt) {
				ii.push(value.replace("-", " "));
				res.push(ii.join(' '));
				i = 0;
				ii = [];
				i2++;
			}
		});

		return res;
	}

	function treemapName4(d) {
		var part = 0;
		if (notEmpty(d.parent) && (notEmpty(d.data.uid) || d.data.uid == 0) && notEmpty(orgaActionObj.activeList[d.parent.data.id + d.data.uid])) {
			part = parseInt(orgaActionObj.activeList[d.parent.data.id + d.data.uid].part);
		}
		var textvalue = "";
		if (orgaActionObj.filtertype == "type") {
			textvalue += "Reste: " + frformatNum(d.data.reste).replace(" ", "-") + " Part: " + frformatNum(part);
		}

		var res = [];
		var rp = ((orgaActionObj.x(d.x1) - orgaActionObj.x(d.x0)) / orgaActionObj.width) * 100;

		var arr1 = textvalue.split(" ");
		var tt = 0;
		if (rp < 20) {
			tt = 0;
		} else if (rp < 30) {
			tt = 1;
		} else if (rp < 40) {
			tt = 2;
		} else if (rp < 50) {
			tt = 3;
		} else if (rp < 60) {
			tt = 4;
		} else if (rp < 90) {
			tt = 5;
		} else {
			tt = 20;
		}
		if (arr1.length < tt) {
			return [textvalue.replace("-", " ")];
		}
		var i = 0;
		var i2 = 0;
		var ii = [];
		$.each(arr1, function(index, value) {
			if (i < tt) {
				ii.push(value.replace("-", " "));
				if ((i2 * (tt + 1)) + i + 1 == arr1.length) {
					res.push(ii.join(' '));
				}
				i++;
			} else if (i == tt) {
				ii.push(value.replace("-", " "));
				res.push(ii.join(' '));
				i = 0;
				ii = [];
				i2++;
			}
		});
		return res;
	}

	function treemapsize(d, ee) {
		var rp = (ee / orgaActionObj.width) * 100;
		var tt = 0;
		if (rp < 20) {
			tt = 40;
		} else if (rp < 35) {
			tt = 75;
		} else if (rp > 35) {
			tt = 200;
		}
		return tt;
	}

	function createBranchTreemap(groupId, groupData) {
		// Compute the layout.

		const hierarchy = d3.hierarchy(groupData)
			.sum(d => d.value)
			.sort((a, b) => b.value - a.value);
		const root = d3.treemap().tile(tile)(hierarchy);

		const svg = d3.select('#oas-chart-' + groupId.replace(/[^A-Z0-9]/ig, '')).append('svg')
			.attr("viewBox", [0.5, -50.5, orgaActionObj.width, orgaActionObj.height + 50])
			.attr("preserveAspectRatio", "xMinYMin meet")
			.attr('width', orgaActionObj.width - 60)
			.attr('height', orgaActionObj.height + 70)
			.style("font", "10px sans-serif")

		// Display the root.
		let group;
		group = svg.append("g")
			.call(render, root, svg);

		svg.node();
	}

	function createGroupTreemap(group) {
		$.each(group, function(index, value) {
			$('#oas-swiper-<?= $kunik ?> .swiper-wrapper').append(`
                <div class="swiper-slide">

                    <div id="oas-chart-${index.replace(/[^A-Z0-9]/ig, '')}"></div>
                </div>
            `);
			createBranchTreemap(index, value);
		});
	}

	function switchFilterTreemap(filtertype = "type") {
		var finalValue = {};
		var groupValue = {};
		var groupValue2 = {};
		if (filtertype == "type") {
			groupValue = Object.groupBy(filteredAnswers.map(filterNiv1type), ({
				type
			}) => type)
		} else {
			groupValue = Object.groupBy(filteredAnswers.reduce(function(accumulator, answers) {
				if (notEmpty(answers.answers)) {
					var financers = answers.answers.aapStep1.depense.reduce(getfinancerName, []);
					$.each(Object.keys(financers), function(index, value) {
						var topush = {
							id: answers._id.$id,
							name: answers.answers.aapStep1.titre,
							image: answers.answers.aapStep1.image,
							financers: value,
							price: answers.answers.aapStep1.depense.reduce((a, v) => v.price ? a + parseInt(v.price) : a, 0),
							financement: answers.answers.aapStep1.depense.reduce(getfinancement, 0),
							financementCount: answers.answers.aapStep1.depense.reduce((a, v) => v.financer ? a + v.financer.length : a, 0),
							type: typeof answers.project != "undefined" && typeof answers.project.id != "undefined" ? "project" : "proposition",
							children: answers.answers.aapStep1.depense.map(filterNiv2fin(value))
						}
						accumulator.push(topush);
					})
				}
				return accumulator;
			}, []), ({
				financers
			}) => financers);

		}

		$.each(groupValue, function(index, value) {
			if (value.length > 10 && filtertype == "type") {
				var c20 = chunk20(value, 8);
				$.each(c20, function(index2, value2) {
					groupValue2[index + '-' + (index2 + 1)] = value2;
				});
			} else {
				groupValue2[index] = value;
			}
		});

		$.each(groupValue2, function(index, value) {
			var key = index.split('-')[0];
			var name = "";
			var keyname = "";
			switch (key) {
				case 'project':
					keyname = name = 'projets'
					break;
				case 'proposition':
					keyname = name = 'propositions'
					break;
				default:
					keyname = name = key;
			}
			if (notEmpty(index.split('-')[1])) {
				name += ' page ' + index.split('-')[1]
			}

			finalValue[name] = {
				"name": name,
				"keyname": keyname,
				"children": value
			}

			if (filtertype == "type") {
				var tempH = d3.hierarchy({
						"name": keyname,
						"children": groupValue[key]
					})
					.sum(d => d.value)
					.sort((a, b) => b.value - a.value);

				finalValue[name].financement = tempH.children.reduce(function(a, v) {
					return a + v.data.financement;
				}, 0);
				finalValue[name].total = tempH.value;
			}
		});
		$('#oas-swiper-<?= $kunik ?> .swiper-wrapper').html('');
		createGroupTreemap(finalValue);
		var jumptooption = "";
		var p = 0;
		$.each(Object.keys(groupValue2), function(index, value) {
			jumptooption += `<option class="jumpto-aos-option" value="${p}">${value}</option>`;
			p++;
		});
		$('.d3actionview<?= $kunik ?> .jumpto-aos').html(jumptooption);

	}

	function onlyUnique(value, index, array) {
		array.indexOf(value) === index;
	}

	function chunk20(arr, size) {
		return Array.from({
			length: Math.ceil(arr.length / size)
		}, (v, i) => arr.slice(i * size, i * size + size));
	}

	function getColorValue(d) {

	}

	function myd3binary(parent, x0, y0, x1, y1) {
		var nodes = parent.children,
			i, n = nodes.length,
			sum, sums = new Array(n + 1);
		var mysums = 0;
		var minrp = 10;
		if (parent.children.length == 2) {
			minrp = 20;
		}

		var mymin = (parent.value * minrp) / 100;

		for (sums[0] = sum = i = 0; i < n; ++i) {
			if (nodes[i].value > mymin) {
				mysums = sums[i + 1] = sum += nodes[i].value;
			} else {
				mysums = sums[i + 1] = sum += mymin;
			}
		}

		partition(0, n, mysums, x0, y0, x1, y1);

		function partition(i, j, value, x0, y0, x1, y1) {
			if (i >= j - 1) {
				var node = nodes[i];
				node.x0 = x0, node.y0 = y0;
				node.x1 = x1, node.y1 = y1;
				return;
			}

			var valueOffset = sums[i],
				valueTarget = (value / 2) + valueOffset,
				k = i + 1,
				hi = j - 1;

			while (k < hi) {
				var mid = k + hi >>> 1;
				if (sums[mid] < valueTarget) k = mid + 1;
				else hi = mid;
			}

			if ((valueTarget - sums[k - 1]) < (sums[k] - valueTarget) && i + 1 < k) --k;

			var valueLeft = sums[k] - valueOffset,
				valueRight = value - valueLeft;

			if ((x1 - x0) > (y1 - y0)) {
				var xk = value ? (x0 * valueRight + x1 * valueLeft) / value : x1;
				partition(i, k, valueLeft, x0, y0, xk, y1);
				partition(k, j, valueRight, xk, y0, x1, y1);
			} else {
				var yk = value ? (y0 * valueRight + y1 * valueLeft) / value : y1;
				partition(i, k, valueLeft, x0, y0, x1, yk);
				partition(k, j, valueRight, x0, yk, x1, y1);
			}
		}
	}

	function validateCoFinancement() {
		if (
			notEmpty(orgaActionObj.activeList) &&
			$('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val() > 0 &&
			($('.d3actionview<?= $kunik ?> .input-name-multiselect-tree').val() != "" || $('.d3actionview<?= $kunik ?> .input-email-multiselect-tree').val() != "")
		) {
			$('.d3actionview<?= $kunik ?> .multiselect-group-button .statbutton-cofinance').prop('disabled', false);
		} else {
			$('.d3actionview<?= $kunik ?> .multiselect-group-button .statbutton-cofinance').prop('disabled', 'disabled');
		}
	}

	function updateDropdownList() {
		var count = 0;
		var html = `<span class="span-info"> <i class="fa fa-info"></i> Vous pouvez éditer le part de votre participation à une action ici en appuyant deux(02) fois sur la valeur`;
		var ref = {};
		if (!notEmpty(orgaActionObj.activeList));
		{
			$('.d3actionview<?= $kunik ?> .action-select-count').html('0');
			$('.d3actionview<?= $kunik ?> .dropdown-menu-orgaaction').html(`<li>Rien n'a encore été séléctionné</li>`);
		}
		$.each(orgaActionObj.activeList, function(index, value) {
			if (notEmpty(ref[value.parentname])) {
				ref[value.parentname].push([value.name, value.part, value.reste, value.id + value.uid]);
			} else {
				ref[value.parentname] = [
					[value.name, value.part, value.reste, value.id + value.uid]
				];
			}
			count++;
		});

		$.each(ref, function(index, value) {
			html += `<h3>${index}</h3>`;
			$.each(value, function(index2, value2) {
				html += `<li><span class="span-name">${value2[0]}</span><span class="span-part " contenteditable="true" data-reste="${value2[2]}" data-iduid="${value2[3]}">${value2[1]}</span></li>`;
			});
		});
		$('.d3actionview<?= $kunik ?> .action-select-count').html(count);
		$('.d3actionview<?= $kunik ?> .dropdown-menu-orgaaction').html(html);
	}

	function updateTilePart() {
		if ($('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val() != "" && $('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val() != 0) {
			$('.d3actionview<?= $kunik ?> .tspanselect').remove();

			d3.selectAll('.rectgaction')
				.append("text")
				.attr('class', "tspanselect")
				.each(function(d) {
					var text = d3.select(this);
					$.each(treemapName4(d), function(index, value) {
						text.append("tspan")
							.attr("x", 20)
							.attr("y", (d, i, nodes) => {
								return 20 + (treemapName2(d).length * 20) + index * 20;
							})
							.style("font", "14px montserrat")
							.attr("fill", '#33363D')
							.attr("stroke", "#33363D")
							.attr("stroke-width", 0.5)
							.text(value);
					});
				});
		}
	}

	function updatePart() {
		var totalResteAuto = 0;
		var totalResteEdited = 0;
		var totalChanged = 0;

		$.each(orgaActionObj.activeList, function(index, value) {
			if (notEmpty(orgaActionObj.activeList[index].changed) && orgaActionObj.activeList[index].changed) {
				totalResteEdited += parseInt(value.reste);
				totalChanged += value.part;
			} else {
				totalResteAuto += parseInt(value.reste);
			}
		});
		var amountInserted = parseInt($('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val() == '' ? 0 : $('.d3actionview<?= $kunik ?> .input-amount-multiselect-tree').val());
		var amountAccepted = amountInserted - totalChanged;

		$.each(orgaActionObj.activeList, function(index, value) {
			if (!notEmpty(orgaActionObj.activeList[index].changed) || !orgaActionObj.activeList[index].changed) {
				orgaActionObj.activeList[index].part = isNaN(Math.round(((value.reste / totalResteAuto) * amountAccepted))) ? 0 : Math.round(((value.reste / totalResteAuto) * amountAccepted));
			}
		});
		updateDropdownList();
	}

	function reload<?= $kunik ?>() {
		$(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}`).append('<div class="aap-cover-spin"></div>')
		ajaxPost(
			null,
			baseUrl + "/co2/cms/refreshblock", {
				idBlock: sectionDyf.<?php echo $kunik ?>blockCms._id.$id,
				clienturi: window.location.href
			},
			function(data) {
				if (data.result) {
					$(`.cmsbuilder-block[data-id='${sectionDyf.<?php echo $kunik ?>blockCms._id.$id}`).html(data["view"]);
					cmsBuilder.block.initEvent();
				} else {
					toastr.error("something went wrong!! please try again.");
				}
			},
			null,
			"json", {
				async: false
			}
		)
	}
</script>