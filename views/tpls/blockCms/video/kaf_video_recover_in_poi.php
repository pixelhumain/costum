<?php 
$keyTpl = "kaf_video_recover_in_poi";
$content = [];
if(isset($blockCms["content"])) {
	$content = $blockCms["content"];
}
$paramsData = [
    "title" => "Video",
    "blockdescription" => "Loren upsum dolor",
    "bgBoxColor" => "#eeeeee"   
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php 
  $blockKey = (string)$blockCms["_id"];
 ?>
<style type="text/css">
    .box-video {
        background: <?php echo $paramsData["bgBoxColor"]; ?>;
        padding: 30px;
        margin: 0 0 24px 0;
    }
    @media (max-width: 767px) {
    	.box-video {
	        padding: 10px;
	        margin: 0 0 15px 0;
	    }
    }
    /* width */
    #videolist<?= $kunik ?>::-webkit-scrollbar {
      width: 2px;
    }

    /* Track */
    #videolist<?= $kunik ?>::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px grey; 
      border-radius: 10px;
    }
     
    /* Handle */
    #videolist<?= $kunik ?>::-webkit-scrollbar-thumb {
      background: #f0ad16; 
      border-radius: 10px;
    }

    /* Handle on hover */
    #videolist<?= $kunik ?>::-webkit-scrollbar-thumb:hover {
      background: #b30000; 
    }
    #videolist<?= $kunik ?> {
        max-height: 760px;
        overflow-y: auto;
    }
	.morelink {
	    font-size: 16px;
	    font-weight: bold;
	    margin-left: 2px;
	    color: #ff9800;
	    cursor: pointer;
	}

    .rte .boxHeadline {
        font-weight: 400;
        margin: 0 0 15px 0;
    }
    .rte .boxHeadline+.boxHeadlineSub {
        font-weight: 400;
        margin: 0px 0 20px 0;
    }
    .section-title { margin-bottom: 40px; }
    .morecontent {
	    display: none;
	}
	.morecontent {
  		display: none;
	}


</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
	<div class="section-title">
        <h2 class="title-1 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></h2>
        <div class="title-2 tsp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="blockdescription"><?php echo $paramsData["blockdescription"]; ?></div>
    </div>
    
    <div class="col-xs-12" id="videolist<?= $kunik ?>"></div>

    <div class="text-center editSectionBtns">
        <div class="" style="width: 100%; display: inline-table; padding: 10px;">
            <?php if(Authorisation::isInterfaceAdmin()){?>
                <div class="text-center addElement<?= $blockCms['_id'] ?>">
                    <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add a video')?></button>
                </div>  
            <?php } ?>
        </div>
    </div>

</div>

<script type="text/javascript">
	function addVideo(){
    var dyfPoi={
      "beforeBuild":{
        "properties" : {
          "properties" : {
            "name": {
              "label" : "<?php echo Yii::t('cms', 'Name of the video')?>",
              "placeholder" : "<?php echo Yii::t('cms', 'Name of the video')?>",
              "order" : 1

            },
            "shortDescription" : {
              "label" : "<?php echo Yii::t('cms', 'Short description')?>",
              "placeholder" : "<?php echo Yii::t('cms', 'Short description')?>",
              "order" : 3
            },
            "urls": {
              "label" : "<?php echo Yii::t('cms', 'Url of the video')?>",
              "order" : 2,
              "rules":{
                "required":true
              }
            }          
          }
        },
        "onload" : {
          "actions" : {
            "setTitle" : "<?php echo Yii::t('cms', 'Add a video')?>",
            "src" : { 
						  "infocustom" : "<?php echo Yii::t('cms', 'Fill in the field')?>"
            },
            "presetValue" : {
                "type" : "video"
            },
            "hide" : {
              "breadcrumbcustom" : 1,
              "imageuploader" : 1,
              "urlsarrayBtn" : 1,
              "parentfinder" : 1,
              "removePropLineBtn" : 1,
              "tagstags" :1,
              "formLocalityformLocality" :1,
              "locationlocation" :1
            }
          }
        }

      }    
    }
      dyfPoi.afterSave = function(data){
        dyFObj.commonAfterSave(data, function(){
          mylog.log("dataaaa", data);
          urlCtrl.loadByHash(location.hash);

        });
      }
      dyFObj.openForm('poi',null, null,null,dyfPoi);
  }

  function videoLink<?= $kunik?> (){
    var params = {
      "type" : "video",        
      "searchType" : ["poi"],
      fields : ["urls","parent"]          
    };
    var strVideo = '';
    ajaxPost(
      null,
      baseUrl+"/" + moduleId + "/search/globalautocomplete",
      params,
      function(data){
        mylog.log("videotest",data.results);   
        var params = data.results;         
     		var str = '';
     		$.each( data.results, function( key, params ) {
          var url = params.urls[0];
          var youtubeId = "";
          if (url.indexOf("=") != -1) {
            youtubeId = (((url).split("=")).reverse())[0];
          } else {
            youtubeId = (((url).split("/")).reverse())[0];
          }
          var vidName = (typeof params.name != 'undefined') ? params.name : "";
          var vidShortDescription = (typeof params.shortDescription != 'undefined') ? params.shortDescription : "";
          var vidDescription = (typeof params.description != 'undefined') ? params.description : "";
          str += '<div class="box-video rte col-xs-12">'+
              '<div class="col-xs-12 col-sm-6 col-md-5 col-lg-4">'+
                  '<iframe width="100%" height="281" src="https://www.youtube.com/embed/'+youtubeId+'" frameborder="0" allowfullscreen=""></iframe>'+
              '</div>'+

              '<div class="col-xs-12 col-sm-6 col-md-7 col-lg-8">'+
                  '<h2 class="boxHeadline title-3">'+vidName+'</h2>'+
                  '<h3 class="boxHeadlineSub title-4">'+vidShortDescription+'</h3>'+
                  '<div class="title-5 more markdown">'+vidDescription+'</div>'+
              '</div>';
          <?php if(Authorisation::isInterfaceAdmin()){ ?>
            str +='<div class = "col-xs-12 col-sm-6 col-md-7 col-lg-8 text-center edit<?= $kunik?>">'+
                  '<a href="javascript:;" data-value=\''+JSON.stringify(params)+'\' data-id="'+params._id.$id+'" class="btn btn-primary btn-sm btn-edit-vid margin-5"> <i class="fa fa-edit"></i>'+
                  '</a>'+
                              '<a href="javascript:;" data-id="'+params._id.$id+'"  class="btn btn-danger btn-sm btn-delete-vid margin-5"> <i class="fa fa-trash"></i>'+
                              '</a>'+
                '</div>';
          <?php }?>
                  
          str += '</div>'
			  });
			 
			  $("#videolist<?= $kunik ?>").html(str);
        $.each($(".markdown"), function(k,v){
          descHtml = dataHelper.markdownToHtml($(v).html());
          $(v).html(descHtml);
        });
        AddReadMoreLink();
        $('.btn-delete-vid').on('click',function(){
          var idPoi = $(this).data('id');
            bootbox.confirm(trad.areyousuretodelete, function(result){ 
                if(result){
                  var url = baseUrl+"/"+moduleId+"/element/delete/id/"+idPoi+"/type/poi";
                  var param = new Object;
                  ajaxPost(null,url,param,function(data){ 
                          if(data.result){
                            toastr.success(data.msg);
                            urlCtrl.loadByHash(location.hash);
                          }else{
                            toastr.error(data.msg); 
                        }
                  })
                }
            }) 
        });

        $('.btn-edit-vid').on('click',function(){
          var params = $(this).data("value");
          var id = $(this).data("id");
          var dyfPoi={
            "beforeBuild":{
              "properties" : {
                "name": {
                  "label" : "<?php echo Yii::t('cms', 'Name of the video')?>",
                  "placeholder" : "<?php echo Yii::t('cms', 'Name of the video')?>",
                  "order" : 1

                },
                "shortDescription" : {
                  "label" : "<?php echo Yii::t('cms', 'Short description')?>",
                  "placeholder" : "<?php echo Yii::t('cms', 'Short description')?>",
                  "order" : 3
                },
                "urls": {
                  "label" : "<?php echo Yii::t('cms', 'Url of the video')?>",
                  "order" : 2,
                  "rules":{
                    "required":true
                  }
                }                  
              }
            },
            "onload" : {
              "actions" : {
                "setTitle" : "<?php echo Yii::t('cms', 'Edit video')?>",
                "src" : { 
                    "infocustom" : "<?php echo Yii::t('cms', 'Fill in the field')?>"
                },
                "presetValue" : {
                    "type" : "video"
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "imageuploader" : 1,
                    "urlsarrayBtn" : 1,
                    "parentfinder" : 1,
                    "removePropLineBtn" : 1,
                    "tagstags" :1,
                    "formLocalityformLocality" :1,
                    "locationlocation" :1
                }
              }
            }

          }
          dyfPoi.afterSave = function(data){
            dyFObj.commonAfterSave(data, function(){
              mylog.log("dataaaa", data);
              urlCtrl.loadByHash(location.hash);

            });
          }
          dyFObj.editElement('poi',id,'video',dyfPoi);
        });
      }
    )
  };
  
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    videoLink<?= $kunik?>();
    $(".addElement<?= $blockCms['_id'] ?>").on('click', function () {
      addVideo();
    });

    sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            "properties" : { 
                "title" : {
                  label : "<?php echo Yii::t('cms', 'Title')?>",
                  values :  sectionDyf.<?php echo $kunik?>ParamsData.title
                },    
                "blockdescription" : {
                    label : "<?php echo Yii::t('cms', 'Description')?>",
                    "inputType" : "textarea",
                "markdown" : true,
                  values :  sectionDyf.<?php echo $kunik?>ParamsData.blockdescription
                },            
                bgBoxColor : {
                  label : "<?php echo Yii::t('cms', 'Background color of the box')?>",
                  inputType : "colorpicker",
                  values :  sectionDyf.<?php echo $kunik ?>ParamsData.bgBoxColor
                }
            },
            save : function () {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                });
                mylog.log("save tplCtx",tplCtx);
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) { 
                            toastr.success("<?php echo Yii::t('cms', 'updated item')?>");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        } );
                    }
            }
        }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";

        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });

  });

  function AddReadMoreLink() {
    var showChar = 160;
    var ellipsestext = "...";
    var moretext = "<?php echo Yii::t('cms', 'Read more')?>";
    var lesstext = "<?php echo Yii::t('cms', 'Read less')?>";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);
        //var h = content.substr(showChar-1, content.length - showChar);

        var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

        $(this).html(html);
        $(this).after('<a href="" class="morelink btn btn-default">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
        });
        
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
        });
      }
      
      return false;
    });
  }


</script>