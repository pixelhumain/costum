<?php
/**
 * TPLS QUI PERMET AFFICHAGE DES NEWS
 */
$keyTpl = "basicnews";

$paramsData = [
    "title"      => "News",
    "icon"       => "",
    "color"      => "#000000",
    "background" => "#FFFFFF",
    "nbPost"     => 2,
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>
<style type="text/css">
    .news_<?=$kunik?> h1 {
        color:<?=$paramsData["color"]?>;
        background-color: <?=$paramsData["background"]?>;
    }  
    .news_<?=$kunik?> ul #noMoreNews{
        color: black !important;
        background-color: #dddddd69;
        backdrop-filter: blur(5px);
    }
    
</style>
<div class="news_<?=$kunik?>">
    <h1 class="text-center title  col-xs-12 col-sm-12 col-lg-12  padding-20 sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <?=$paramsData["title"]?>
    </h1>
   
    <div id="newsstream">

    </div>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
    jQuery(document).ready(function() {
        urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/<?=$paramsData["nbPost"];?>/scroll/false";
        ajaxPost(
            "#newsstream",
            baseUrl+"/"+urlNews,
                {
                    search:true,
                    formCreate:false,
                    scroll:false
                },
            function(news){},
            "html"
        );
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Configurer la section actualité",
                "description" : "Personnaliser votre section sur l'actualité",
                "icon" : "fa-cog",
                "properties" : {                   
                    icon : {
                        label : "Icone",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        values : (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.icon !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.icon:""
                    },
                    color : {
                        label : "Couleur du titre",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.color !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.color:""
                    },
                    background : {
                        label : "Couleur du fond du titre",
                        inputType : "colorpicker",
                        values : (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.background !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.background:""
                    },
                    nbPost : {
                        label : "Nombre de post",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.nbPost !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.nbPost:""
                    }
                },
                beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                        } );
                    }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
        });

    });


    function getActu(){
        var urlNews = "/news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false/";

        ajaxPost("#newsstream",baseUrl+"/"+urlNews,{
            typeNews : [ "news" ],
            indexStep : 4,
            search:true,
            formCreate:false,
            scroll:false,
        }, function(news){

        }, "html");
    }
</script>