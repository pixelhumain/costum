<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<?php if(isset($el) && isset($el['address'])) { ?>
    <style id="<?= $kunik ?>annuaireCss">
        .annuaireData<?= $kunik ?> ul {
        padding: 0;
        display: flex;
        flex-wrap: wrap;
        gap: 20px; /* Espace entre les éléments */
        }

        .annuaireData<?= $kunik ?> li {
        margin-bottom: 10px;
        list-style: none;
        flex-basis: calc(50% - 20px); /* 50% de la largeur avec un espace de 20px entre les colonnes */
        }

        .annuaireData<?= $kunik ?> .count_list_data {
            border-radius: 5px;
            margin: 2px;
            height: 100%;
        }

        .annuaireData<?= $kunik ?> h4, .annuaireData<?= $kunik ?> h6  {
            text-align: center;
        }
        /*  */
        .counter{
            background: linear-gradient(to right bottom,#fff 50%, #f9f9f9 51%);
            font-family: 'Comfortaa', cursive;
            text-align: center;
            width: 100%;
            min-width: 100px;
            padding: 10px 0 0;
            margin: 0 auto;
            border-radius: 50px 0;
            box-shadow: 0 0 15px -5px rgba(0, 0, 0, 0.3);
            overflow: hidden;
            height: 110px;
        }
        .counter .counter-icon{
            margin: 0 0 10px;
            height: 14px;
        }
        .counter h3{
            font-weight: 400;
            text-transform: capitalize;
            margin: 0 0 10px;
            text-decoration: none;
        }
        .counter .counter-value{
            color: #fff;
            font-size: 25px;
            font-weight: 400;
            line-height: 40px;
            padding: 7px 0 3px;
            display: block;
        }
        /*  */

    </style>
    <div class="annuaireData<?= $kunik ?> <?= $kunik ?>" id="<?= $kunik ?>annuaireData">
        <ul id="annuaireDatalist<?= $kunik ?>" style="margin-top:10%">

        </ul>
    </div>
    <script>
        var str = "";
        var elCostum = <?= json_encode($el) ?>;
        var citiesArray = <?php echo json_encode($cities); ?>;
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#<?= $kunik ?>annuaireCss").append(str);
        // 
        if(costum.editMode)
        {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
            var annuaireData = {
                configTabs : {
                    style : {
                        inputsConfig : [
                            {
                                type: "section",
                                options: {
                                    name: "fondContainer",
                                    label: "<?php echo Yii::t('cms', 'Background color')?>",
                                    inputs: [
                                        "backgroundColor"
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "text",
                                    label: "Text",
                                    inputs: [
                                        "fontSize",
                                        "color",
                                    ]
                                }
                            },
                            {
                                type: "section",
                                options: {
                                    name: "icone",
                                    label: "Icon",
                                    inputs: [
                                        "fontSize",
                                        "color",
                                    ]
                                }
                            },
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                }
            }
            cmsConstructor.blocks.annuaireData<?= $myCmsId ?> = annuaireData;
        }
        var elCostum<?= $kunik ?> = <?php echo json_encode( $el ) ?>;
        var icons = {
        Cooperative: 'fa-building',
        GovernmentOrganization: 'fa-university',
        Group: 'fa-users',
        LocalBusiness: 'fa-industry',
        NGO: 'fa-circle-o',
        citoyens: 'fa-user',
        classifieds: 'fa-bullhorn',
        poi: 'fa-map-marker',
        projects: 'fa-cogs',
        ressources: 'fa-database',
        spam: 'fa-ban',
        events: 'fa-database'
        };
        var blockData<?= $kunik ?> = {
            listCount:function(){
                var dataList = document.getElementById("annuaireDatalist<?= $kunik ?>");

                if(typeof costum.annuaireData != "undefined") {
                    var data = costum.annuaireData;
                    var datas = data.count;
                    for (var key in datas) {
                        if (datas.hasOwnProperty(key) && key != "spam" && datas[key] > 0) {

                            var li = document.createElement("li");
                            li.innerHTML = `<div class="col-md-8 col-sm-8" id="dataks-${key}" style="cursor:pointer;" data-type="${key}">

                                                    <div class="counter">
                                                        <div class="counter-icon">
                                                            <i class="fa ${icons[key]} icone"></i>
                                                        </div>
                                                        <h3 class="text">${trad[key]}</h3>
                                                        <span class="counter-value fondContainer">${datas[key]}</span>
                                                    </div>
                                                
                                            </div>`;
                            if(key != "GovernmentOrganization") {
                                dataList.appendChild(li);
                            } else if(key == "GovernmentOrganization" && (datas[key] - 1) > 0 ) {
                                dataList.appendChild(li);
                            }
                            
                            $("#dataks-"+key).on('click', function(){
                                var types = $(this).attr("data-type");
                                location.hash = "#search?types="+types;
                                urlCtrl.loadByHash(location.hash);
                            });
                        }
                    }
                } else {
                    var params = {
                        searchType: ["organizations", "poi", "projects", "classifieds", "citoyens", "ressources", "events"],
                        notSourceKey: true,
                        filters: {
                            $or: {
                                "source.keys": ["<?=$el['slug']?>"]
                            }
                        },
                        count: true,
                        countType: ["NGO", "LocalBusiness", "Group", "GovernmentOrganization", "Cooperative", "ressources", "poi", "projects", "classifieds", "citoyens", "events"],
                        indexStep : 0,
                        onlyCount : true
                    };

                    <?php if(isset($el["address"])){ ?>
                        $("#nullAdresse<?= $kunik ?>").html("");
                        var idAddress = {};
                        if (typeof costum.typeCocity !== "undefined") {
                            switch (costum.typeCocity) {
                                case "region":
                                    idAddress = { "address.level3": elCostum.address.level3 };
                                    break;
                                case "ville":
                                    idAddress = { "address.localityId": elCostum.address.localityId };
                                    break;
                                case "departement":
                                case "district":
                                    idAddress = { "address.level4": elCostum.address.level4 };
                                    break;
                                case "epci":
                                    idAddress = { "address.localityId": { $in: costum.citiesArray } };
                                    break;
                            }
                        } else if (elCostum.address.level3) {
                            idAddress = { "address.level3": elCostum.address.level3 };
                        }
                        var addressFilters = {
                            "source.keys": "<?= $el['slug'] ?>",
                            ...idAddress,
                            "links.memberOf.<?= (string)$el['_id'] ?>": {'$exists': true}
                        };

                        params.filters["$or"] = addressFilters;
                    <?php } ?>

                    ajaxPost(
                        null,
                        baseUrl + "/" + moduleId + "/search/globalautocomplete",
                        params,
                        function(data) {
                            if(data.results != [] && data.results != null) costum.annuaireData = data;
                            if(data.count) {
                                var datas = data.count;
                                for (var key in datas) {
                                    if (datas.hasOwnProperty(key) && key != "spam" && datas[key] > 0) {

                                        var li = document.createElement("li");
                                        li.innerHTML = `<div class="col-md-8 col-sm-8" id="dataks-${key}" style="cursor:pointer;" data-type="${key}">

                                                                <div class="counter">
                                                                    <div class="counter-icon">
                                                                        <i class="fa ${icons[key]} icone"></i>
                                                                    </div>
                                                                    <h3 class="text">${trad[key]}</h3>
                                                                    <span class="counter-value fondContainer">${datas[key]}</span>
                                                                </div>
                                                            
                                                        </div>`;
                                        if(key != "GovernmentOrganization") {
                                            dataList.appendChild(li);
                                        } else if(key == "GovernmentOrganization" && (datas[key] - 1) > 0 ) {
                                            dataList.appendChild(li);
                                        }
                                        
                                        $("#dataks-"+key).on('click', function(){
                                            var types = $(this).attr("data-type");
                                            location.hash = "#search?types="+types;
                                            urlCtrl.loadByHash(location.hash);
                                        });
                                    }
                                }
                            }
                        }
                    );
                }
            }
        }

        jQuery(document).ready(function() {
            blockData<?= $kunik ?>.listCount();
        });

    </script>
<?php } else { ?>
     <script>
        toastr.info("Il n'y a pas d'adresse associée à votre élément. Veuillez le renseigner dans l'information générale de votre organisation.");
     </script>
<?php } ?>