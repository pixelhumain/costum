<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];

	$parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["name", "description","descriptionVille","shortDescription","slug","profilMediumImageUrl","email","socialNetwork","profilBannerUrl"]);
?>

<style type="text/css" id="<?= $kunik ?>bannerWithSearchGlobal">
	#menuTopLeft a {
		text-decoration: none;
	}
	.headerCocity_<?= $kunik?> {
		background-color: #052434;
	}
	.headerCocity_<?= $kunik?> .contentCocity{
		margin-top: 10%;
		margin-left: 5%;
	}
	.headerCocity_<?= $kunik?> .contentCocity a:hover {
		background: transparent;
		color: white !important;
		border: 3px solid #F0FCFF;
	}
	.headerCocity_<?= $kunik?> .contentCocity p {
		margin-top: 5%;
	}
	.headerCocity_<?= $kunik?> .contentCocity .bouton{
		background-color: #fff;
		text-transform: uppercase;
		padding: 3%;
		border-radius: 10px;
		font-size: 20px;
		margin-top: 5%;
		margin-bottom: 15%;
		font-weight: bold;
	}

	.headerCocity_<?= $kunik?> .contentCocity h1{
		font-size: 100px !important; 
		margin-top: 5%;
		color: white !important;
		text-transform: none !important;
	}
	
	@media (max-width: 978px) {
		#menuTopLeft .menu-btn-top:before {
			font-size: 18px;

		}
		.headerCocity_<?= $kunik?> .contentCocity{
			margin-top: 30%;
		}
		.headerCocity_<?= $kunik?> .contentCocity h1{
			font-size: 55px !important; 
			margin-top: 10%;
			color: white !important;
		}

		.headerCocity_<?= $kunik?> .contentCocity p {
			font-size: 18px !important;
		}
		
		.headerCocity_<?= $kunik?> .contentCocity .bouton{
			width: 39%;
			border-radius: 10px;
			height: 40px;
			margin-top: 7%;
			font-weight: 600;
			padding: 7px;
			margin-bottom: 10%;
			font-size: 18px !important;
		}
	}
</style>
<div class="headerCocity_<?= $kunik?> searchCocity_<?= $kunik?> <?= $kunik?>" >
	<div class="contentCocity col-md-5" >
		<h1 class="title"><?= $parent["name"]?></h1>
		<div class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="descriptionVille"  > <?= $blockCms["descriptionVille"]?> </div>
		<a href="#@<?= $parent["slug"]?>" class="btn lbh bouton title-5" >En savoir plus </a>
	</div>
</div>
<script>
	var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#<?= $kunik ?>bannerWithSearchGlobal").append(str);

	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var bannerWithSearchGlobal = {
			configTabs : {
				general : {
					inputsConfig: [
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
		}
		cmsConstructor.blocks.bannerWithSearchGlobal<?= $myCmsId ?> = bannerWithSearchGlobal;
	}
</script>