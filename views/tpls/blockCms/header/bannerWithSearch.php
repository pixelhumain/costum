<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];

  $templateCocity = PHDB::findOne(Template::COLLECTION,array(
		"name" => "Template cocity",
		"source.key" => "templateCocity"
	));

	$templateFiliere = PHDB::findOne(Template::COLLECTION,array(
		"name" => "Template Filiere",
		"source.key" => "templateFiliere"
	));

	$_idTemplatecocity = isset($templateCocity["_id"]) ? (string)$templateCocity["_id"] : "";
	$_idTemplatefiliere = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : "";

$initBg= Document::getListDocumentsWhere(
  array(
    "id"=> $blockKey,
    "type"=>'cms',
    "subKey"=> "background"
  ), "image"
);

$arrayBackground= [];
foreach ($initBg as $k => $v) {
  $arrayBackground[] =$v['imagePath'];
}
  //$allVille = PHDB::find(Organization::COLLECTION);
?>
<style type="text/css">

  #homeCocityPrez<?= $kunik?>  {
    overflow: hidden;
    position: relative;
  }
  #homeCocityPrez<?= $kunik?> form {
    width: 100%;
    max-width: 700px;
    padding-top: 3%;
    margin-left: 23%;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
    box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    height: 50px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field {
    height: 68px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input {
    height: 100%;
    border: 0;
    display: block;
    width: 100%;
    padding: 10px 0;
    font-size: 16px;
    color: #000;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input.placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:-moz-placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input::-webkit-input-placeholder {
    color: #222;
    font-size: 14px;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:hover, #homeCocityPrez<?= $kunik?> form .inner-form .input-field input:focus {
    box-shadow: none;
    outline: 0;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap {
    -ms-flex-positive: 1;
    flex-grow: 1;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    background: #fff;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap input {
    -ms-flex-positive: 1;
    flex-grow: 1;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap .svg-wrapper {
    min-width: 60px;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 23px;
    color: #0A96B5;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.first-wrap svg {
    width: 36px;
    height: 36px;
    fill: #222;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap {
    min-width: 50px;
  }
  #costumActif<?= $kunik?> .smartgrid-slide-element .img-back-card {
    width: 100%;
    height: 300px;
    object-position: initial;
    float: left;
    object-fit: contain;
    border-radius: 5px;
    -webkit-border-radius: 5px;
    overflow: hidden;
    border: 1px solid #ccc;
    background: #bdbdbd;
  }
  #costumActif<?= $kunik?> .smartgrid-slide-element .img-back-card .container-img-city  .fa {
    position: absolute;
    top: 36%;
    left: 50%;
    transform: translate(-50%,-50%);
    /* opacity: 0.2; */
    font-size: 110px;
    color: #fff;
  }
  #costumActif<?= $kunik?> .smartgrid-slide-element .img-back-card .container-img-city {
    width: 100%;
    float: left;
    min-height: 230px;
    line-height: 230px;
    background-color: white;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search {
    height: 100%;
    width: 100%;
    white-space: nowrap;
    font-size: 21px;
    color: #fff;
    border: 0;
    cursor: pointer;
    position: relative;
    z-index: 0;
    background: #0A96B5;
    transition: all .2s ease-out, color .2s ease-out;
    font-weight: 300;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:hover {
    background: #0A96B5;
  }

  #homeCocityPrez<?= $kunik?> form .inner-form .input-field.second-wrap .btn-search:focus {
    outline: 0;
    box-shadow: none;
  }
  .initialiseOrga {
    background-color: #0A96B5;
  }
  .initialiseCocity{
    background-color: #0A96B5;
    padding: 15px 15px 15px 15px;
    font-weight: bolder !important;
    border-radius: 40px !important;
    width: auto;
    font-size: 16px;
    color: white;
  }
  #homeCocityPrez<?= $kunik?> .caption{
    margin: 5% 5%;
  }
  #homeCocityPrez<?= $kunik?>{
    background-size: cover;
    <?php  if (count($arrayBackground)!=0) { ?>
      background-image: url(<?= $arrayBackground[0] ?>); 
    <?php }else { ?>
      background-image: url(<?= Yii::app()->getModule("costum")->assetsUrl ; ?>/images/cocity/etang4-2.jpg); 
    <?php } ?>
  }
  @media (max-width: 978px) {
    #homeCocityPrez<?= $kunik?> form {
      width: 100%;
      max-width: 500px;
      height: 40px;
    }
    #homeCocityPrez<?= $kunik?> .caption h1{
      font-size: 45px;
      margin-top: 10%;
    }

    .#homeCocityPrez<?= $kunik?> .caption p {
      font-size: 18px;
      margin-top: 13%;
    }
    #homeCocityPrez<?= $kunik?> form {
      margin-left: 0%;
    }
  }
/* //css modal and loader */
  .loader-loader<?= $kunik ?> {
      position: fixed;
      top: 0;
      right: 0;
      bottom: 0;
      left: 42%;
      overflow: hidden;
      -webkit-overflow-scrolling: touch;
      outline: 0;
      z-index: 9999999999999999 !important;
      padding-top: 180px !important;
      
  }
  .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
      width: auto;
      height: auto;
  }
  .loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
      width : auto !important;
      height : auto !important;
  }
  .backdrop-loader<?= $kunik ?> {
      position: fixed;
      opacity: .8;
      background-color : #000;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      width: 100%;
      height : 100vh;
      z-index: 999999999 !important;
  }
</style>
<!--  -->
<div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
    <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
</div>
<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>
<!--  -->
<header id="homeCocityPrez<?= $kunik?>">
  <div class="caption"> 
  <form id="formCocity">
      <div class="inner-form">
        <div class="input-field first-wrap">
          <div class="svg-wrapper">
            <i class="fa fa-search"> </i>              
          </div>
          <input type="text" id="nameCity" name="nameCity" placeholder="Taper votre ville ..." value="" />
        </div>
        <div class="input-field second-wrap">
          <button onclick="cocityObj<?= $kunik?>.searchCity() " class="btn-search" type="button">
            <i class="fa fa-arrow-right"></i>
          </button>
        </div>
      </div>
    </form>
  </div>
</header>
<div id="costumActif<?= $kunik?>">

</div>
<script type="text/javascript">

  if (costum.editMode){
      cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
  } 
  
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($blockCms); ?>; 
  var  cocityObj<?= $kunik?> ={
    useTemplate:function(tpl_params, loadpage) {
        var params = {
            "id"		 : tpl_params.idTplSelected,
            "page"		 : "",
            "newCostum"  : true,
            "action"	 : tpl_params.action,
            "collection" : tpl_params.collection,
            "parentId"   : tpl_params.contextId,
            "parentSlug" : tpl_params.contextSlug,
            "parentType" : tpl_params.contextType
        };
        ajaxPost(
            null,
            baseUrl + "/co2/template/use",
            params,
            function (data) {
                if (loadpage) {
                    // window.open(""+baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug+"", "_blank");
                    window.location = baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug;
                }			
            },
            { async: false }
        );
    },
		createOrgaFiliere:function(params,paramscocity) {
			ajaxPost(null, baseUrl+"/"+moduleId+'/element/save', params,
				function(result){
                    var data = {
                        collection : 'organizations',
                        id: result.id,
                        path: 'allToRoot',
                        value : {
                            "costum.slug" : "costumize",
                            "cocity" : paramscocity.cocity, 
                            "thematic" : paramscocity.thematic, 
                            "ville" : paramscocity.ville,
                            "source.key" : paramscocity.keys,
                            "source.keys" : [paramscocity.keys],
                            "costum.typeCocity" : "ville"
                        }
                    }
                    dataHelper.path2Value(data, function(){
                      let loadpage = false;
                      let themat = (paramscocity.thematic).toLowerCase();
                      var params = {"thematic" : themat , "contextId" : result.id, "contextSlug" : result.afterSave.organization.slug, "contextType" : result.map.collection};
                      co.importCostumContent(params, loadpage);
                    });
				},
				function(){
					toastr.error( "Une erreur s'est produite, veuillez réessayer et si le problème persiste, contecter l'administrateur");
				},
				"json"
			);
		},
    // ********************** search a city**********************
    searchCity:function(){
      var nameCity =document.getElementById("nameCity").value;
      var data = {      
				"searchType" : ["organizations"],
				"notSourceKey": true,
        "fields" : ["costum","ville"],
				filters : {
					"costum.cocity" : {'$exists': true},
				},
        "name" :nameCity
      }; 
      coInterface.showLoader("#costumActif<?= $kunik?>", "Recherche en cours");
      ajaxPost(
        null,
        baseUrl+"/" + moduleId + "/search/globalautocomplete",
        data,
        function(data){
          var hsrc = '';
          if(nameCity == "") {
            hsrc += '<div class=" text-center" style="margin-bottom: 10px;">';
            hsrc += '<button class="initialiseCocity btn btn-default"> Initialiser une COcity pour une ville';
            hsrc += '</button>';
            hsrc += '</div>';
          } else if ( data.results.length == 0){
            hsrc += "<p class='text-center' style='color: white'>Il n'éxiste aucune cocity </p>";
            hsrc += '<div class=" text-center" style="margin-bottom: 10px;">';
            hsrc += '<button  class="initialiseCocity btn btn-default"> Initialiser une COcity pour la ville de '+nameCity;
            hsrc += '</button>';
            hsrc += '</div>';
          } else {
            hsrc += '<div class=" text-center" style="margin-bottom: 10px;">';
            hsrc += '<button  class="initialiseCocity btn btn-default"> Initialiser une COcity pour la ville de '+nameCity;
            hsrc += '</button>';
            hsrc += '</div>';
            $.each(data.results, function( index, value ) {
              var image = typeof value.costum.logo!= "undefined" ? '<img class="img-responsive img-profil-entity"  src="'+ value.costum.logo +'">' : '<img class="img-responsive img-profil-entity"  src="'+costum.logo+'">';
              if (typeof value.costum == "undefined") {
                hsrc += "";
              } 
              if (typeof value.costum != "undefined" && value.costum.cocity != true) {
                hsrc += "";
              }
              if (typeof value.costum != "undefined" && value.costum.cocity == true) {
                if(typeof value.ville == "undefined"){
                  hsrc += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds classifieds">';
                  hsrc += '<div class="item-slide">';
                  hsrc += '<div class="entityCenter">';
                  hsrc += ' <a href="" class="pull-right"> ';
                  hsrc += '<i class="fa fa-group bg-azure"></i> </a>';
                  hsrc += '</div>';
                  hsrc += '<div class="img-back-card">';
                  hsrc += '<div class="container-img-city">';
                  hsrc += image;
                  hsrc += ' </div>';
                  hsrc += '<div class="text-wrap searchEntity">';
                  hsrc += '<div class="entityRight profil no-padding" style="margin-top: 10px">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" target="_blank" class="entityName letter-turq">';
                  // hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit; color: white">';
                  hsrc += '<i class="fa fa-group"></i>  '+ value.name +'<br>';
                  // hsrc += '</font>';
                  hsrc += '<i class="fa fa-map-marker" style="color: deepskyblue; margin-top: 5px;"></i>&nbsp;&nbsp;<t style="font-size: 10pt; color: deepskyblue;">'+value.address.addressLocality+'</t>';
                  hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';

                  hsrc += '<div class="slide-hover co-scroll">';
                  hsrc += '<div class="text-wrap">';
                  hsrc += '<h4 class="entityName">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" target="_blank" class=" letter-turq" style="text-decoration: none;" >';
                  // hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit; color: white">';
                  hsrc += '<i class="fa fa-group"> </i> ' + value.name ;
                  hsrc += '</font>';
                  // hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</h4>';
                  hsrc += '<div class="entityType col-xs-12 no-padding">';
                  hsrc += '<span class="type">';
                  // hsrc += '<font style="vertical-align: inherit;">';
                  // hsrc += '<font style="vertical-align: inherit;">';
                  // hsrc += '</font>';
                  // hsrc += '</font>';
                  hsrc += '</span>';
                  hsrc += '</div>';
                  hsrc += '<div class="desc">';
                  hsrc += '<div class="socialEntityBtnActions btn-link-content">';
                  hsrc += '<a href="<?php  echo Yii::app()->createUrl("/costum")?>/co/index/slug/'+ value.slug +'" target="_blank"   class="btn btn-info btn-link btn-share-panel" style="text-decoration: none;" >';
                  // hsrc += '<font style="vertical-align: inherit;">';
                  hsrc += '<font style="vertical-align: inherit;"> Voir le costum de cette ville'; 
                  hsrc += '</font>';
                  // hsrc += '</font>';
                  hsrc += '</a>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                  hsrc += '</div>';
                }
              }
            });
          }
          $("#costumActif<?= $kunik?>").html(hsrc);
          $(".initialiseCocity").off().on("click",function(){
            var dyfOrga={
              "beforeBuild":{
                "properties" : {
                  "name" : {
                    "label" : "Nom de votre ville", 
                    "inputType" : "text",
                    "placeholder" :"Nom de votre ville",
                    "value" :nameCity,
                    "order" : 1
                  },
                  "formLocality" : {
                    "label" : "Adresse de votre ville",
                    "rules" :{
                      "required" : true
                    }
                  },
                  "thematic":{
                    "inputType" : "tags",
                    "label" : "Sélectionner les filières que vous voulez dés la création de la page",
                    "values" : allSugThematic,
                    "value" : [],
                    "order" : 3
                  }
                }
              },
              "onload" : {
                "actions" : {
                  "setTitle" : "Creer votre cocity",
                  "src" : {
                    "infocustom" : "<br/>Remplir le champ"
                  }          
                } 
              }   
            };
            dyfOrga.afterSave = function(orga){
              dyFObj.commonAfterSave(orga, function(){
                $('#loader-loader<?= $kunik ?>').removeClass('hidden');
								$('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
                coInterface.showLoader('#content-loader<?= $kunik ?>');
                var thematicCocity = [];
                var allTags = [];
                if(notNull(orga.map.thematic)) thematicCocity = (orga.map.thematic).split(",");

                if(notNull(orga.map.tags)) allTags = orga.map.tags;

                var themaWithCheck = thematicCocity != [] ? allSugThematic.filter(thema => !thematicCocity.includes(thema)) : allSugThematic;

                var paramsCheck = {
                  "thematic" : themaWithCheck,
                  "address" : orga.map.address,
                  "typeCity" : "ville"
                }

                ajaxPost(
                  null,
                  baseUrl+"/costum/cocity/checkdatafiliere",
						      paramsCheck,
                  function(results){

                    if(results != "") Array.prototype.push.apply(thematicCocity, results);

                    if (thematicCocity != []) {
                      var filiers = {};
                      var coordonateTemp = {
                        "address" : orga.map.address,
                        "geo" : orga.map.geo,
                        "geoPosition" : orga.map.geoPosition
                      };

                      thematicCocity.forEach(function(val){

                        var defaultTags = [];
                        var valkey = val.toLowerCase();
                        var valname = (val.toLowerCase()).charAt(0).toUpperCase() + (val.toLowerCase()).slice(1);

                        if(!allTags.includes(valkey)) allTags.push(valkey);
                        if (typeof  allThem[valkey] != "undefined" && notNull(allThem[valkey]["tags"])) {
                          var tagsToAdd = allThem[valkey]["tags"];
                          var newTags = tagsToAdd.filter(tag => !allTags.includes(tag));
                          Array.prototype.push.apply(allTags, newTags);
                        } 
                        
                        if (typeof  allThem[valkey] != "undefined" && notNull(allThem[valkey]["tags"])) {
                          defaultTags = allThem[valkey]["tags"]; 
                          filiers[val] = allThem[valkey];
                        } else {
                          var oneThem = {
                            "default" : {
                              "name" : valname,
                              "icon" : "fa-link",
                              "tags" : [valname.toLowerCase()]
                            }
                          };
                          filiers[valkey] = oneThem["default"];
                        }
                        var defaultName = orga.map.name +" "+ val;
                        var paramscocity = {
                          cocity : orga.id,
                          ville : orga.map.name,
                          thematic : valname,
                          keys: orga.map.slug
                        };
                        var params = {
                          name: defaultName,
                          collection : "organizations",
                          type: "Group",
                          role: "admin",
                          image: "",
                          tags: defaultTags,
                          ...coordonateTemp,
                          addresses : undefined,
                          shortDescription: "Filière "+val+" "+orga.map.name
                        }
                        cocityObj<?= $kunik?>.createOrgaFiliere(params, paramscocity);  
                      });
                      if (filiers !== null && Object.keys(filiers).length == thematicCocity.length){
                        var tplCtx = {
                          collection: "organizations",
                          value: filiers,
                          id: orga.id,
                          path: "filiere",
                        };
                        dataHelper.path2Value(tplCtx, function (data){});
                        var data = {
                          collection : 'organizations',
                          id: orga.id,
                          path: 'allToRoot',
                          value : {
                            "costum.slug" : "costumize",
                            "costum.cocity" : true,
                            "thematic" : thematicCocity,
                            "tags" : allTags,
                            "costum.typeCocity" : "ville"
                          },
                        }

                        dataHelper.path2Value(data, function(){
                          <?php if($_idTemplatecocity != "") { ?>
                            let tpl_params = {"idTplSelected" : "<?= $_idTemplatecocity ?>", "collection" : "templates", "action" : "", "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection};
                            let loadpage = true;
                            cocityObj<?= $kunik?>.useTemplate(tpl_params, loadpage);
                          <?php } ?>
                        });
                      }
                    }
                  }
                )
              });
            }
            dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"},null,dyfOrga);
          })
        }
      );
    }
  }
  jQuery(document).ready(function() {
    var timeoutId;
    $("#nameCity").on("keyup", function(event) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
          cocityObj<?= $kunik?>.searchCity();
        }, 1000);
    });

    $("#formCocity").on('submit', function(event){ 
      event.preventDefault();
      var data = $(this).serialize();
      cocityObj<?= $kunik?>.searchCity();
      return false;      
    });
  });
  </script>