<!-- ****************get image uploaded************** -->
<?php 
    $initFiles = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>'block',
        ), "image"
    );    
    $initLogoFiles = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );
    $logoPicture = [];
    foreach ($initLogoFiles as $key => $value) {
        $logoPicture[]= $value["imagePath"];
    }
    $backgroundImages = [];
    foreach ($initFiles as $key => $value) {
        array_push($backgroundImages, $value["imagePath"]);
    }

?>

<?php
    $loremIpsum = "lorem Ipsum is simply dummy text of the printing and typesetting industry.";
    $keyTpl ="header";
    $kunik = $keyTpl.(string)$blockCms["_id"];
    $blockKey = (string)$blockCms["_id"];
    $paramsData = [ 
        "titleBlock"=>"Lorem Ipsum Dolor",
        "subtitleBlock"=> $loremIpsum,
        "other" => "Lorem Ipsum Dolo",
        "imgHeight"=>"600",
        "imgHeightMd"=>"550",
        "imgHeightSm"=>"460",
        "imgHeightXs"=>"300",
        "imgSize" => "cover",
        "imgPosition" => "center",
        "textMTlg" => "50",
        "textMTmd" => "50",
        "textMTsm" => "50",
        "textMTxs" => "50",
        "textPR" => "10",
        "textPL" => "10",
        "logo" => "",
        "logosPosition" => "centre",
        "logosWidth" => "5",
        "backgroundImages"=> null,

        "btnctaShow" => false,
        "btnctaAdminOnly" => false,
        "btnctaBackground" => "btn-success",
        "btnctaText" => "Call To Action", 
        "btnctaLink" => ""
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    } 

    if(count($logoPicture)!=0){
      $paramsData["logo"] = $logoPicture;
    }
    
    $paramsData["backgroundImages"] = $backgroundImages;

    $spText = ["titleBlock","subtitleBlock","other"];

    foreach ($spText as $key) {
        ${$key} = ($blockCms[$key] ?? ["fr" => $paramsData[$key]]);
        ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];
        
        $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
    };
    
    
  $transform = "inherit";
  if($paramsData["logosPosition"]=="bottom-left"){
    $left = "10%";
    $top = "auto";
    $bottom = "10%";
  }else if($paramsData["logosPosition"]=="top-left"){
    $left = "10%";
    $top = "15%";
    $bottom="auto";

  }else if($paramsData["logosPosition"]=="top"){
    $left = "50%";
    $top = "20%";
    $bottom = "auto";
    $transform = "translate(-50%,-50%)";
    
  }else{
    $left = "50%";
    $top = "60%";
    $bottom = "auto";
    $transform = "translate(-50%,-50%)";
  }
    $parent = Element::getElementSimpleById($costum["contextId"],$costum["contextType"],null,["cocity","name"]);
 ?>

<style>
    <?php if (@$parent["cocity"] != "") {?>
        #menuTopLeft a {
            text-decoration: none;
        }
        #menuTopLeft .menu-btn-top:before {
            content: "<?= $parent["name"]?>";
            line-height: 3;

        }
    <?php } ?>
    .containerCarousel-<?= $kunik?>{
        margin-top:  -12px;
    }

    .logo<?=$kunik ?>{
        width: <?= $paramsData["logosWidth"] ?>%;
        position: absolute;
        z-index: 9;
        left: <?=$left ?>;
        top: <?=$top ?>;
        bottom: <?=$bottom ?>;
        background: transparent;
        -webkit-transform: <?=$transform ?>;
        transform: <?=$transform ?>;
    }

    .content<?=$kunik ?>{
        z-index: 9998;
        background: transparent;
    }

    #head<?=$kunik ?>{
        position:absolute;
        z-index:1;
        /*text-align:center;*/
        margin-top: <?= $paramsData['textMTlg']?>px;
        /*color: <?//= $paramsData["titleColor"] ?>;
        text-shadow: black 0.1em 0.1em 0.2em;*/
    }
    .title-1, .title-2, .title-3 {
      font-weight: 900;
    }
    .imgBgHeight {
        height: <?= $paramsData['imgHeight']?>px!important;
        width: 100%;
        object-fit:<?= $paramsData["imgSize"]?>;
        object-position: <?= $paramsData["imgPosition"]?>;
    }
    @media (max-width: 1199px) and (min-width: 992px) {
      .imgBgHeight {
          height: <?= $paramsData['imgHeightMd']?>px!important;
      }
        #head<?=$kunik ?>{
            margin-top: <?= $paramsData['textMTmd']?>px;
        }
    }
   @media (max-width: 991px) and (min-width: 768px) {
     .imgBgHeight {
          height: <?= $paramsData['imgHeightSm']?>px!important;
      }
       #head<?=$kunik ?>{
           margin-top: <?= $paramsData['textMTsm']?>px;
       }
    }
     @media (max-width: 767px){
        .imgBgHeight {
            height: <?= $paramsData['imgHeightXs']?>px!important;
        }
        .containerCarousel-<?= $kunik?> .carousel .title-1 {
            font-size: 22px!important;
        }
        .containerCarousel-<?= $kunik?> .carousel .title-2 {
            font-size: 18px!important;
        }
        .containerCarousel-<?= $kunik?> .carousel .title-3 {
            font-size: 18px!important;
        }
        #head<?=$kunik ?>{
            margin-top: <?= $paramsData['textMTxs']?>px;
        }                                                                   
    }
</style>

<div class="containerCarousel containerCarousel-<?= $kunik?>">
    <div id="docCarousel<?=$kunik ?>" class="carousel slide text-center" data-ride="carousel">
        <div id="head<?=$kunik ?>" class="col-xs-12">
          <h1 class="sp-text img-text-bloc text-white titleBlock<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"></h1>
          <h3 class="sp-text img-text-bloc text-white subtitleBlock<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitleBlock"></h3>
          <h5 class="sp-text img-text-bloc text-white other<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="other"></h5>
          <br>
          <?php if($paramsData["btnctaShow"]=="true"){ ?>
            <div class="center">
                <?php if($paramsData["btnctaAdminOnly"]=="true"){ ?>
                    <?php if(Authorisation::isInterfaceAdmin()){ ?>
                        <a id="callToAction<?= $kunik?>" href="javascript:;" data-link="<?= $paramsData["btnctaLink"] ?>" class="btn btn-success btn-lg"><?= $paramsData["btnctaText"] ?></a>;
                    <?php } ?>
                <?php }else{ ?>
                    <a id="callToAction<?= $kunik?>" href="javascript:;" data-link="<?= $paramsData["btnctaLink"] ?>" class="btn btn-success btn-lg"><?= $paramsData["btnctaText"] ?></a>;
                <?php } ?>
            </div>
          <?php } ?>
          <br>
        </div>
        
        <div class="content<?=$kunik ?>">
            <?php if(is_string($paramsData["logo"]) && $paramsData["logo"]!=""){
                ?>
                <img class="logo<?=$kunik ?>" src="<?= $paramsData['logo']; ?>" style="z-index: 1;">
            <?php }else if(isset($paramsData['logo']['0'])){ ?>                
                <img class="logo<?=$kunik ?>" src="<?= $paramsData['logo']['0'] ?>" style="z-index: 1;">
            <?php } ?>
        </div>
        
        <div class="carousel-inner">
            <?php if(count($paramsData["backgroundImages"])==0){ ?>
                <div class="item active">
                    <img class="imgBgHeight" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/smarterre/cercle1.jpg'>
                </div>
            <?php }else{ ?>
                <?php foreach ($paramsData["backgroundImages"] as $key => $value){ ?>
                    <div class='item <?= ($key==0)?"active":"" ?>'  style="text-align: center;">
                        <img class="imgBgHeight" src='<?php echo Yii::app()->createUrl("/").$value; ?>' >
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php if(count($paramsData["backgroundImages"])>1){ ?>
            <a class="left carousel-control" href="#docCarousel<?=$kunik ?>" data-slide="prev">
                <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#docCarousel<?=$kunik ?>" data-slide="next">
                <span style="font-family: 'Glyphicons Halflings' !important;" class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
    //sp-params and text append 

    if(costum.editMode){
            cmsConstructor.sp_params["<?= $blockKey ?>"] = <?php echo json_encode( $blockCms ); ?>;
    };

    appendTextLangBased(".titleBlock<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleBlock) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".subtitleBlock<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($subtitleBlock) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".other<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($other) ?>,"<?= $blockKey ?>");

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                
                "textMTlg" : {
                    "inputType" : "text",
                    "label" : "marge en haut du text (Large)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textMTlg
                },
                "textMTmd" : {
                    "inputType" : "text",
                    "label" : "marge en haut du text (Moyenne)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textMTmd
                },
                "textMTsm" : {
                    "inputType" : "text",
                    "label" : "marge en haut du text (Petit)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textMTsm
                },
                "textMTxs" : {
                    "inputType" : "text",
                    "label" : "marge en haut du text (Petit)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textMTxs
                },
                "textPL" : {
                    "inputType" : "text",
                    "label" : "marge a gauche du text (en %)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textPL
                },
                "textPR" : {
                    "inputType" : "text",
                    "label" : "marge a droite du text (en %)",
                    "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.textPR
                },
                "logosWidth" : {
                    "inputType" : "text",
                    "label" : "taille du logo",
                    values :  sectionDyf.<?php echo $kunik ?>ParamsData.logosWidth
                },
                "logo" :{                    
                   "inputType" : "uploader",
                   "docType": "image",
                   "contentKey":"slider",
                   "endPoint": "/subKey/logo",
                   "domElement" : "logo",
                   "filetypes": ["jpeg", "jpg", "gif", "png"],
                   "label": "Logo :",
                   "itemLimit" : 1,
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initLogoFiles) ?> 
                },
                "logosPosition" : {
                    "class":"form-control",
                    "inputType" : "select",
                    "label" : "Position du logo",
                    "options":{
                        "top":"Haut",
                        "top-left":"Haut gauche",
                        "center":"Centre",
                        "bottom-left":"Bas gauche"
                    },
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.logosPosition
                },
                "imgHeight" : {
                    "inputType" : "number",
                    "label" : "Larges (px)",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgHeight
                },
                "imgHeightMd" : {
                    "inputType" : "number",
                    "label" : "Desktops (px)",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgHeightMd
                },
                "imgHeightSm" : {
                    "inputType" : "number",
                    "label" : "Tablettes (px)",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgHeightSm
                },
                "imgHeightXs" : {
                    "inputType" : "number",
                    "label" : "Phones (px)",
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgHeightXs
                },
                "imgSize" : {
                    "inputType" : "select",
                    "label" : tradCms.size,
                    options: {
                        " ": "",
                        "cover": "<?php echo Yii::t('cms', 'Cover')?>",
                        "contain": "<?php echo Yii::t('cms', 'Contain')?>",
                    },
                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgSize
                },
                "imgPosition" : {
                    "inputType" : "select",
                    "label" : tradCms.position,
                    options: {
                        "left": "<?php echo Yii::t('cms', 'Left')?>",
                        "left top": "<?php echo Yii::t('cms', 'Left top')?>",
                        "left bottom": "<?php echo Yii::t('cms', 'Left bottom')?>",
                        "right": "<?php echo Yii::t('cms', 'Right')?>",
                        "right top": "<?php echo Yii::t('cms', 'Right')?>",
                        "right bottom": "<?php echo Yii::t('cms', 'Right top')?>",
                        "center": "<?php echo Yii::t('cms', 'Center')?>",
                        "center top": "<?php echo Yii::t('cms', 'Top center')?>",
                        "center bottom": "<?php echo Yii::t('cms', 'Bottom center')?>",
                    },

                    value : sectionDyf.<?php echo $kunik ?>ParamsData.imgPosition
                },
                "backgroundImages" :{
                   "inputType" : "uploader",
                   "label" : "Image de fond",
                   "docType": "image",
                    "contentKey" : "slider",
                   "endPoint": "/subKey/block",
                   "domElement" : "image",
                   "filetypes": ["jpeg", "jpg", "gif", "png", "svg"],
                   "label": "Image de fond :",
                   "showUploadBtn": false,
                    initList : <?php echo json_encode($initFiles) ?> 
                },
                "btnctaShow" : {
                    "inputType" : "checkboxSimple",
                    "label" : "Afficher le bouton call to action",
                    "rules" : {
                        "required" : true
                    },
                    "params" : {
                        "onText" : trad.yes,
                        "offText" : trad.no,
                        "onLabel" : trad.yes+" <a href='#privacy' target='_blank'></a>",
                        "offLabel" : trad.no+" <a href='#privacy' target='_blank'></a>"
                    },
                    "checked" : sectionDyf.<?php echo $kunik?>ParamsData.btnctaShow
                },
                "btnctaAdminOnly" : {
                    "inputType" : "checkboxSimple",
                    "label" : "Afficher le bouton call to action pour admin seulement",
                    "rules" : {
                        "required" : true
                    },
                    "params" : {
                        "onText" : trad.yes,
                        "offText" : trad.no,
                        "onLabel" : trad.yes+" <a href='#privacy' target='_blank'></a>",
                        "offLabel" : trad.no+" <a href='#privacy' target='_blank'></a>"
                    },
                    "checked" : sectionDyf.<?php echo $kunik?>ParamsData.btnctaAdminOnly
                },
                "btnctaBackground" : {
                    "label" : "Classe du bouton call to action",
                    "placeholder" : "btn-success",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnctaBackground
                },
                "btnctaText" : {
                    "label" : "Texte du bouton call to action",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnctaText
                },
                "btnctaLink" : {
                    "label" : "Lien cible du bouton call to action",
                    "placeholder":"Laisser vide si action spécifique",
                    inputType : "text",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnctaLink
                },
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
          },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
                  mylog.log("andrana",data.items)
              });
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    //   urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $("#callToAction<?= $kunik?>").off().on("click", function(){
            if($(this).data("link")!=""){
                urlCtrl.loadByHash($(this).data("link"));
            }
            // to do costum action
        });

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"img",3,6,null,null,"Taille de l'image de Fond (hauteur)","green","");
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"logos",6,6,null,null,"Propriéré du logo","green","");
          alignInput2(sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties ,"text",4,4,null,null,"Position du texte","green","");
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",3,6,null,null,"Bouton Call To Action","green","");

        });
    });

</script>