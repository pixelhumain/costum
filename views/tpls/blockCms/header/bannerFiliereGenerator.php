<?php 
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
	$templateFiliere = PHDB::findOne(Template::COLLECTION,array(
		"name" => "Template Filiere",
		"source.key" => "templateFiliere"
	));

	$_idTemplatefiliere = isset($templateFiliere["_id"]) ? (string)$templateFiliere["_id"] : ""; 
?>

<style type="text/css" id="<?= $kunik ?>bannerFiliereGenerator">
	.banner{
		position:relative;
		height: 15vh;
		overflow: hidden;
	}
	.banner .content{
		position: absolute; 
		left:0;
		right: 0;
		top:45%;
		text-align:center; 
		transform:translateY(-50%);
		z-index: 2;
	}
	#addFiliere, .btn-get-started{
		padding: 10px 15px 15px 15px;
		font-weight: bolder !important;
		border-radius: 40px !important;
		margin-top: 10px;
	}

	.bg-green {
		background-color: #134f5c !important;
		color: white !important;
	}
	 h3 {
		color: white !important;
	}

/* //css modal and loader */
	.loader-loader<?= $kunik ?> {
		position: fixed;
		top: 0;
		right: 0;
		bottom: 0;
		left: 42%;
		overflow: hidden;
		-webkit-overflow-scrolling: touch;
		outline: 0;
		z-index: 9999999999999999 !important;
		padding-top: 180px !important;
      
 	}
	.loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> {
		width: auto;
		height: auto;
	}
	.loader-loader<?= $kunik ?> .content-loader<?= $kunik ?> .processingLoader {
		width : auto !important;
		height : auto !important;
	}
  	.backdrop-loader<?= $kunik ?> {
		position: fixed;
		opacity: .8;
		background-color : #000;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		width: 100%;
		height : 100vh;
		z-index: 999999999 !important;
  	}
</style>
<!--  -->
<div class="loader-loader<?= $kunik ?> hidden" id="loader-loader<?= $kunik ?>" role="dialog">
    <div class="content-loader<?= $kunik ?>" id="content-loader<?= $kunik ?>"></div>
</div>
<div class="backdrop-loader<?= $kunik ?> hidden" id="backdrop-loader<?= $kunik ?>"></div>
<!--  -->
<div  class="banner <?= $kunik ?>"> 
	<div class="content sp-cms-container">
		<div class=" col-md-8  description col-md-offset-2 ">
			<button id="addFiliere" class='btn btn-get-started bouton'> 
				Générer Filière pour une ville
				<!-- < ?php echo $blockCms["btnGenerateText"] ?>&nbsp; <i class="fa fa-arrow-right"></i> -->
			</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	var str = "";
	str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
	$("#<?= $kunik ?>bannerFiliereGenerator").append(str);

	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var bannerFiliereGenerator = {
			configTabs : {
				style: {
					inputsConfig: [
						{
							type: "section",
							options: {
								name: "bouton",
								label: "Bouton",
								inputs: [
									"fontSize",
									"color",
									"backgroundColor"
								]
							}
						}
					]
				},
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
				cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
			}
		}
		cmsConstructor.blocks.bannerFiliereGenerator<?= $myCmsId ?> = bannerFiliereGenerator;
	};

	var cocityParam = localStorage.getItem("paramsCocity")
	var cocityParamArray = "";
	if(cocityParam){
		cocityParamArray = cocityParam.split(".")
	}
	
	var cocity = {};
	var cocityId = ville = thematic = translatedThematic = "";
	var typeCocity = 'ville';

	if(typeof cocityParam != "undefined" && typeof cocityParamArray != "undefined" && cocityParamArray.length <= 5){
		if(cocityParamArray[0]){
			cocityId = cocityParamArray[0]
		}
		if(cocityParamArray[1]){
			ville = decodeURI(cocityParamArray[1]);
		}
		if(cocityParamArray[2]){
			translatedThematic = typeof tradTags[cocityParamArray[2].toLowerCase()] != "undefined" ? decodeURI(tradTags[cocityParamArray[2].toLowerCase()].charAt(0).toUpperCase()+tradTags[cocityParamArray[2].toLowerCase()].slice(1)) : decodeURI((cocityParamArray[2]).charAt(0).toUpperCase() + (cocityParamArray[2]).slice(1));
			thematic = cocityParamArray[2].toLowerCase();
			ajaxPost(
				null, 
				"/co2/element/get?type=organizations&&id="+cocityParamArray[0], 
				{},
				function(data){
					cocity = data["map"];
				},
				function(xhr,textStatus,errorThrown,data){
					toastr.error("Une erreur s'est produite, veuillez signaler ce problème à notre administrateur");
				}
			);
			$("#addFiliere").text("Générer Filière "+translatedThematic+" "+ville);
		}
		if(cocityParamArray[4]) {
			typeCocity = cocityParamArray[4];
		}
	}

	jQuery(document).ready(function() {
		$("#addFiliere").off().on('mousedown', 
			function() {
				let defaultName = "";
				let defaultTags = [];
				let defaultLocation = {};

				defaultName = ville+" "+translatedThematic;

				if(cocity.filiere && cocity.filiere[thematic.toLowerCase()] && cocity.filiere[thematic.toLowerCase()]["tags"]){
					defaultTags = cocity.filiere[thematic.toLowerCase()]["tags"];
				}

				let cocityCoordonate = {};
							
				if(cocity.address && cocity.geo && cocity.geoPosition){
					cocityCoordonate["address"] = cocity.address;
					cocityCoordonate["geo"] = cocity.geo;
					cocityCoordonate["geoPosition"] = cocity.geoPosition;
				}

				if(cocity.email){
					cocityCoordonate["email"] = cocity.email;
				}

				var data = {
					name: defaultName,
					type: "Group",
					role: "admin",
					image: "",
					tags: defaultTags,
					// ...cocityCoordonate,
					center:true,
					addresses : undefined,
					shortDescription: "Page Filière "+translatedThematic+" "+ville
				}

				if(typeof cocity.address != 'undefined' && typeof cocity.geo != 'undefined' && typeof cocity.geoPosition != 'undefined'){
					data["address"] = cocity.address;
					data["geo"] = cocity.geo;
					data["geoPosition"] = cocity.geoPosition;
				}

				var dyfOrga = {
					"beforeBuild":{
						"properties" : {
							"thematic":{
								"inputType" : "text",
								"rules" : {
									"required" : true
								},
								"label" : "Le théme de votre filière (thématique)",
								"value" : translatedThematic,
								"order" : 3
							}
						}
					}
				};

				dyfOrga.afterSave = function(orga){
					dyFObj.commonAfterSave(orga, function(){
						$('#loader-loader<?= $kunik ?>').removeClass('hidden');
						$('#backdrop-loader<?= $kunik ?>').removeClass('hidden');
						var data = {
							collection : 'organizations',
							id: orga.id,
							path: 'allToRoot',
							value : {
								"costum.slug" : "costumize",
								"cocity" : cocityId,
								"costum.typeCocity" : typeCocity,
								"ville" : ville,
							}
						}

						if(cocityParamArray != "" && cocityParamArray.length > 0 && typeof cocityParamArray[3] != "undefined" && cocityParamArray[3] != '') {
							data.value["source.key"] = cocityParamArray[3];
							data.value["source.key"] = [cocityParamArray[3]];
						}
						dataHelper.path2Value(data, function(){
							<?php if( $_idTemplatefiliere != "") { ?>
								let loadpage = true;
								let them = (orga.map.thematic).toLowerCase();
								let params = {"thematic" : them, "contextId" : orga.id, "contextSlug" : orga.map.slug,"contextType" : orga.map.collection}
								// let tpl_params = {"idTplSelected" : "< ?= $_idTemplatefiliere ?>", "collection" : "templates", "action" : "", "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection};
								// useTemplate(tpl_params);
								co.importCostumContent(params, loadpage);

								// resolve();
							<?php } ?>
						})
					})
				}
				dyFObj.openForm('organization',null,Object.assign(data, {}),null,dyfOrga);
				
				// dyFObj.openForm(
				// 	'organization', 
				// 	null, 
				// 	{
				// 		name: defaultName,
				// 		type: "Group",
				// 		role: "admin",
				// 		image: "",
				// 		tags: defaultTags,
				// 		...cocityCoordonate,
				// 		center:true,
				// 		addresses : undefined,
				// 		shortDescription: "Filière "+translatedThematic+" "+ville
				// 	}, 
				// 	null, 
				// 	{
				// 		afterSave : function(orga){
				// 			dyFObj.commonAfterSave(orga, function(){
				// 				$('#loader-loader< ?= $kunik ?>').removeClass('hidden');
				// 				$('#backdrop-loader< ?= $kunik ?>').removeClass('hidden');
				// 				var data = {
				// 					collection : 'organizations',
				// 					id: orga.id,
				// 					path: 'allToRoot',
				// 					value : {
				// 						"costum.slug" : "costumize",
				// 						"cocity" : cocityId,
				// 						"typeCocity" : 'ville',
				// 						"thematic" : translatedThematic, 
				// 						"ville" : ville,
				// 					}
				// 				}

				// 				if(cocityParamArray != "" && cocityParamArray.length > 0 && typeof cocityParamArray[3] != "undefined" && cocityParamArray[3] != '') {
				// 					data.value["source.key"] = cocityParamArray[3];
				// 					data.value["source.key"] = [cocityParamArray[3]];
				// 				}
				// 				dataHelper.path2Value(data, function(){
				// 					< ?php if( $_idTemplatefiliere != "") { ?> 
				// 						let tpl_params = {"idTplSelected" : "< ?= $_idTemplatefiliere ?>", "collection" : "templates", "action" : "", "contextId" : orga.id, "contextSlug" : orga.map.slug, "contextType" : orga.map.collection}
				// 						useTemplate(tpl_params);
				// 					< ?php } ?>
				// 				})
				// 			})
				// 		}
				// 	}
				// );
			}
		);
	});

	function useTemplate(tpl_params) {
		var params = {
			"id"		 : tpl_params.idTplSelected,
			"page"		 : "",
			"newCostum"  : true,
			"action"	 : tpl_params.action,
			"collection" : tpl_params.collection,
			"parentId"   : tpl_params.contextId,
			"parentSlug" : tpl_params.contextSlug,
			"parentType" : tpl_params.contextType
		}
		ajaxPost(
			null,
			baseUrl + "/co2/template/use",
			params,
			function (data) {				
				window.location = baseUrl+"/costum/co/index/slug/"+tpl_params.contextSlug;
			},
			{ async: false }
		);
	}
</script>
