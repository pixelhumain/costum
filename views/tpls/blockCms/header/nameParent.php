<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
    $language =  "FR";
?>
<style type="text/css" id="<?= $kunik ?>nameParent">
    .nameParent<?= $kunik?> .nameOrga .sp-text {
        text-transform: none;
        font-weight: bolder;
        font-family: 'Sharp_sans';
    }
    .nameParent<?= $kunik?> .nameOrga {
        font-weight: bolder;
        font-size: 45px !important;
        width: fit-content;
    }
    .nameParent<?= $kunik?> .nameOrga span{
        font-size: 60px;
    }
    .fade {
        opacity: 1 !important;
    }

    #popup {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 999999;
    }

    .popup-content {
        background-color: #fff;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        text-align: right;
    }

    .hidden {
        display: none;
    }

    .popup-content button {
        margin: 10px;
    }

    #noBtn {
        height: 36px;
        font-size: 16px;
        width: 100px;
        font-weight: bold;
        border-radius: 10px;
        background: #ea4335;
        border-color: #ea4335;
    }

    #yesBtn {
        height: 36px;
        font-size: 16px;
        width: 100px;
        font-weight: bold;
        border-radius: 10px;
        color: white;
        background: #092434;
    }

    .<?= $kunik?> p.laville {
        color: black !important;
    }
    .<?= $kunik?> .nameOrga a {
        color: black !important;
    }

    .name<?= $kunik?>container {
        position: relative;
        width: 100%;
        background-size: cover;
        background-position: center;
        display: flex;
        align-items: center;
        justify-content: <?= $blockCms["css"]["position"] ?>;
    }
    .name<?= $kunik?>container .text-box {
        background-color: <?= $blockCms['css']['styleText']['backgroundColor'] ?>;
        padding-top: <?= $blockCms['css']['styleText']['paddingTop'] ?>;
        padding-bottom: <?= $blockCms['css']['styleText']['paddingBottom'] ?>;
        padding-left: <?= $blockCms['css']['styleText']['paddingLeft'] ?>;
        padding-right: <?= $blockCms['css']['styleText']['paddingRight'] ?>;
        border-radius: 8px;
        box-shadow: <?= $blockCms['css']['styleText']['boxShadow'] ?>;
        backdrop-filter: blur(<?= $blockCms['css']['styleText']['inputNumberRange'] ?>px);
        margin-top: <?= $blockCms['css']['styleText']['marginTop'] ?>;
        margin-bottom: <?= $blockCms['css']['styleText']['marginBottom'] ?>;
        margin-left: <?= $blockCms['css']['styleText']['marginLeft'] ?>;
        margin-right: <?= $blockCms['css']['styleText']['marginRight'] ?>;
    }

    .name<?= $kunik?>container .text-box h1 {
        margin: 0;
        color: #333;
        font-size: 32px;
        font-weight: bold;
    }
    .name<?= $kunik?>container .text-box p {
        margin: 0;
        color: #333;
        font-size: 18px;
    }
	
</style>
<div class="name<?= $kunik?>container nameParent<?= $kunik?> <?= $kunik?>" >
	<div class="nameOrga text-box" >
		<div class="sp-text titre<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titre"></div>
		<?php if(isset($blockCms["showVille"]) && $blockCms["showVille"] == true) { ?>
			<a id="links-parent" href="#" target=""><p class="laville sp-text laville<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="laville" style="font-size: 24px; margin-left: 5px;"></p></a>
		<?php }?>
	</div>
</div>
<div id="popup" class="hidden">
  <div class="popup-content">
      <p class="txt-confirm-add" id="txt-confirm-add"></p>
      <button id="noBtn">Annuler</button>
      <button id="yesBtn">Ajouter</button>
  </div>
</div>
<script>
	var str = "";
    var elCostum = <?= json_encode($parent) ?>;
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#<?= $kunik ?>nameParent").append(str);
    var blockCms = <?php echo json_encode( $blockCms ); ?>;
    let links = "";
    if(typeof blockCms.showVille != "undefined" && blockCms.showVille && costum.editMode == false && typeof elCostum.cocity != "undefined" && typeof elCostum.cocity != "" && typeof elCostum.source != "undefined" && typeof elCostum.source.key != "undefined" && elCostum.source.key != "") {
        links = baseUrl+"/costum/co/index/slug/"+elCostum.source.key;
        document.getElementById('links-parent').href = links;
        document.getElementById('links-parent').target = "_blank";
    }

	if(costum.editMode)
	{
		cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ); ?>;
		var nameParent = {
			configTabs : {
				general : {
					inputsConfig: [
						{
							type:"groupButtons",
                            options: {
                                name:"showVille",
                                label: tradCms.showAddress,
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ]
                            }
						}
					]
				},
                style : {
                    inputsConfig:[
                        {
                            type : "select",
                            options : {
                                name : "position",
                                label : tradCms.textPosition,
                                options : [
                                    {
                                        value : "center",
                                        label : tradCms.center
                                    },
                                    {
                                        value : "left",
                                        label : tradCms.left
                                    },
                                    {
                                        value : "right",
                                        label : tradCms.right
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "styleText",
                                label: tradCms.textStyle,
                                inputs: [
                                    "backgroundColor",
                                    {
                                        type : "inputNumberRange",
                                        options : {
                                            label : "Flou du fond",
                                            minRange: 0,
                                            maxRange: 20,
                                            filterValue: cssHelpers.form.rules.checkLengthProperties
                                        }
                                    },
                                    "boxShadow",
                                    "margin",
                                    "padding",
                                    "border",
                                    "borderRadius"
                                ]
                            }
                        }
                    ]
                },
				advanced: {
					inputsConfig: [
						"addCommonConfig"
					]
				}
			},
			afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
		}
		cmsConstructor.blocks.nameParent<?= $myCmsId ?> = nameParent;
	}
    
    appendTextLangBased(".titre<?= $blockKey ?>",<?= isset($costum["costumLangActive"]) ?  json_encode($costum["costumLangActive"]) : json_encode($language) ?>,<?= json_encode($blockCms["titre"]) ?>,"<?= $blockKey ?>");
    appendTextLangBased(".laville<?= $blockKey ?>",<?= isset($costum["costumLangActive"]) ?  json_encode($costum["costumLangActive"]) : json_encode($language) ?>,<?= json_encode($blockCms["laville"]) ?>,"<?= $blockKey ?>");
</script>