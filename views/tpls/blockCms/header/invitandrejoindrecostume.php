<?php 
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [$kunik => $blockCms["css"] ?? []];
?>
<style id="invitandrejoindrecostume">
     /* .< ?= $kunik ?> .btn {
        display: flex;
        gap: 5px;
        justify-content: center;
     } */
      .form_btn {
        text-align: <?= $blockCms['css']["position"] ?>;
      }
</style>
<div class="<?= $kunik ?>">
    <div class="form_btn">
        <?php if(isset($infoData) && !$infoData['linksBtn']['isMember']) {?>
            <button class="botton btn" id="rejoinneElements"><i class="fa fa-link"></i>  <?php echo Yii::t("common", "Join") ?></button>
        <?php } ?>
        <?php if(Authorisation::isInterfaceAdmin()) { ?>
            <a class="botton btn lbhp icone<?= $kunik?>" data-placement="bottom" data-original-title="Inviter des personnes " href="#element.invite.type.organizations.id.<?= $costum["contextId"]?>" onClick="invite()"><i class="fa fa-plus"></i>  Inviter</a>
        <?php } ?>
    </div>
</div>
<script>
    var infoData = <?= json_encode($infoData) ?>;

    <?= $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#invitandrejoindrecostume").append(str);

    if(costum.editMode)
	{
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= $kunik?>ParamsData;
        var invitandrejoindrecostume = {
            configTabs : {
                style : {
                    inputsConfig:[
                        {
                            type : "select",
                            options : {
                                name : "position",
                                label : tradCms.textPosition,
                                options : [
                                    {
                                        value : "center",
                                        label : tradCms.center
                                    },
                                    {
                                        value : "left",
                                        label : tradCms.left
                                    },
                                    {
                                        value : "right",
                                        label : tradCms.right
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "botton",
                                label: tradCms.button,
                                inputs: [
                                    "color",
                                    "backgroundColor",
                                    "fontSize",
                                    "borderRadius",
                                    "width",
                                    "borderColor",
                                    "padding",
                                    "margin"
                                ]
                            }
                        }
                    ]
                },
                hover: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "botton",
                                label: tradCms.button,
                                inputs: [
                                    "color",
                                    "backgroundColor",
                                    "borderColor"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.invitandrejoindrecostume<?= $myCmsId ?> = invitandrejoindrecostume;
    }
    
    function invite(){
        if(!userId){
            toastr.error("Vous devez être connecté");
            $(".close-modal").hide();      
            $('#modalLogin').modal("show");
        }
    }

    function rejoindre() {
        if(!userId){
            toastr.error("Vous devez être connecté");
            $(".close-modal").hide();      
            $('#modalLogin').modal("show");
        } else {
            links.connectAjax(costum.contextType, costum.contextId, userId, userConnected.collection, "member", null, null);
        }
    }

    if(!costum.editMode)
	{
        $("#rejoinneElements").on('click', rejoindre);
    }

</script>