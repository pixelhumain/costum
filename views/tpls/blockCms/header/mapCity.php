<?php 
  $myCmsId  = $blockCms["_id"]->{'$id'};
  $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style>

</style>
<div class="mapCity<?= $kunik ?>-container">
    <div id="mapCity<?= $kunik?>" class="no-padding col-md-12 col-xs-12 mapCity<?= $kunik?>"  style="height: 500px; position: relative;" >

    </div>
</div>
<script>
    var paramsCity = localStorage.getItem("paramsCocity");
    var cityParams = [];
	if(paramsCity){
		cityParams = paramsCity.split(".");
	}
    var map<?= $kunik ?> = new CoMap({
		container : "#mapCity<?= $kunik?>",
		activePopUp : true,
		mapOpt:{
			menuRight : false,
			btnHide : false,
			doubleClick : true,
			zoom : 2,
		},
		mapCustom:{
			tile : 'maptiler',
			getPopup: function(data){
				var id = data._id ? data._id.$id:data.id;
				var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
				if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
					imgProfil = baseUrl + data.profilThumbImageUrl;
				else
					imgProfil = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";

				var eltName = data.title ? data.title:data.name;
				var popup = "";
				popup += "<div class='padding-5' id='popup" + id + "'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

				if(data.tags && data.tags.length > 0){
					popup += "<div style='margin-top : 5px;'>";
					var totalTags = 0;
					$.each(data.tags, function(index, value){
						totalTags++;
						if (totalTags < 3) {
							popup += "<div class='popup-tags'>#" + value + " </div>";
						}
					})
					popup += "</div>";
				}
				if(data.address){
					var addressStr="";
					if(data.address.streetAddress) 
						addressStr += data.address.streetAddress;
					if(data.address.postalCode)
						addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
					if(data.address.addressLocality)
						addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
					popup += "<div class='popup-address text-dark'>";
					popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
					popup += "</div>";
				}
				if(data.shortDescription && data.shortDescription != ""){
					popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
					popup += "</div>";
				}
				if((data.url && typeof data.url == "string") || data.email || data.telephone){
					popup += "<div id='pop-contacts' class='popup-section'>";
					popup += "<div class='popup-subtitle'>Contacts</div>";

					if(data.url && typeof data.url === "string"){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa fa-desktop fa_url'></i> ";
						popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
						popup += "</div>";
					}

					if(data.email){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
						popup += "</div>";
					}

					if(data.telephone){
						popup += "<div class='popup-info-profil'>";
						popup += "<i class='fa fa-phone fa_phone'></i> ";
						var tel = ["fixe", "mobile"];
						var iT = 0;
						$.each(tel, function(keyT, valT){
							if(data.telephone[valT]){
								$.each(data.telephone[valT], function(keyN, valN){
									if(iT > 0)
										popup += ", ";
									popup += valN;
									iT++; 
								})
							}
						})
						popup += "</div>";
					}

					popup += "</div>";
					popup += "</div>";
				}
				var url = baseUrl+'/costum/co/index/slug/'+data.slug;
				popup += "<div class='popup-section'>";
				popup += "<a href='" + url + "' target='_blank' class=' item_map_list popup-marker' id='popup" + id + "'>";
				popup += '<div class="btn btn-sm btn-more col-md-12">';
				popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
				popup += '</div></a>';
				popup += '</div>';
				popup += '</div>';

				return popup;
			},
			markers: {
				getMarker : function(data){
					var imgM = "<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/cocity/smarterritoire.png";	
					return imgM;
				}
			}
		},
		elts : []
	});

    function setCityMap(){
        var params = {
            "searchType" : ["organizations"],
            "notSourceKey": true,
            filters : {
                "slug" : `${cityParams[3]}`,
            }
        }
        ajaxPost(
            null,
            // baseUrl+"/costum/cocity/getlistcocityaction",
            baseUrl+"/" + moduleId + "/search/globalautocomplete",
            params,
            function(data){
                map<?= $kunik ?>.clearMap();
                var src = '';
                map<?= $kunik ?>.addElts(data.results);

            }
        );
    }
    jQuery(document).ready(function() {
        setCityMap();
    });
</script>