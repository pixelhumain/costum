<?php 
$keyTpl = "banner_with_logo";
$paramsData = [
  "title" => "Lorem Ipsum",
  "sizeTitle"=>"35"  
];

$paramsData = array_replace_recursive($paramsData,$blockCms);

if( !(is_array($paramsData["title"]))){
	$title = ["fr" => $paramsData["title"]];
} else {
	$title = $paramsData["title"];
}

$paramsData["title"] = $title[$costum['langCostumActive']] ?? reset($title);

?>
<?php 
  $latestLogo = [];

  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl."/images/blockCmsImg/defaultImg";

    $initImage = Document::getListDocumentsWhere(
        array(
          "id"=> $blockKey,
          "type"=>'cms',
          "subKey"=>"logo"
        ),"image"
    );
    foreach ($initImage as $key => $value) {
         $latestLogo[]= $value["imagePath"];
    }

?>
<style >
    #bg-homepage{
        width: 100%;
        border-top: 1px solid #ccc;
    }
    #header-wrapper {
      position: relative;
      padding: 0em 0em 0em 0em;
      width: 100%;
      height: 550px;
      /*top: -50px;*/
      text-align: center;
  }
  

  .header-logo{
      text-align: center;
      margin-top: 12%;
  }
  .header-logo img {
    height: 300px;
    width: auto;
  }

  .well {
      min-height: 20px;
      padding: 19px;
      box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
      border: none;
      border-radius: 0px;

  }
  #header-about {
      background-color: transparent;
      /*text-transform: none;
      opacity: 0.8;
      padding: 30px 70px;*/
      position: absolute;
      top: 65%;
      left: 50%;
      transform: translate(-50%, -50%);
  }

  @media (max-width: 1399px ) {

    .header-logo{
      text-align: center;
    }
    .header-logo img {
      height: 250px;
      width: auto;
    }
  }
  @media (min-width: 1400px) {
    #header-wrapper {
        height: 600px;
    }
  }
  
  @media (max-width: 767px){
    
   #header-about h1 {
       font-size: 22px!important;
    }
    #header-wrapper {
        height: 320px;
    }
    .header-logo{
      margin-top: 15%;
      padding-left: inherit;
        padding-top: 20px;
        padding-bottom: 20px;
        text-align: center;
    }

    .header-logo{
      padding-left: inherit;
        padding-top: 20px;
        padding-bottom: 20px;
        text-align: center;
    }
    .header-logo img {
      height: 110px;
      width: auto;
    }
  }
  
</style>

<div id="header-wrapper" >

    <div class="header-logo">
      <a class="lbh-menu-app" href="#welcome">
        <img src="<?php echo !empty($latestLogo) ? $latestLogo[0] : ""; ?>">
      </a>
    </div>

  <div id="header-about" class="well col-md-6 col-sm-8 col-xs-12">
    <h1 class="title sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h1>
  </div>

<script type="text/javascript">
  if(costum.editMode){
		cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
	}
	//sp-params and text append 
	appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");

  sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer votre bloc",
        "description" : "Personnaliser votre bloc",
        "icon" : "fa-cog",
        "properties" : {          
        
          "logo" : {
            "inputType" : "uploader",
            "label" : "logo",
            "showUploadBtn" : false,
            "docType" : "image",
            "itemLimit" : 1,
            "contentKey" : "slider",
            "order" : 9,
            "domElement" : "logo",
            "placeholder" : "image logo",
            "afterUploadComplete" : null,
            "endPoint" : "/subKey/logo",
            "filetypes" : [
            "png","jpg","jpeg","gif"
            ],
            initList : <?php echo json_encode($initImage) ?>
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};

          $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
          });

          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouté");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                          // urlCtrl.loadByHash(location.hash);
                        });
                      } );
          }
        }
      }
    };
    $(".edit<?php echo $kunik?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
    });

  });
</script>