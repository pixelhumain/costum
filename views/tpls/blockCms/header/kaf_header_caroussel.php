
<?php 
  $keyTpl ="kaf_header_caroussel";
  $paramsData = [ 
    "item" => array()
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }

 
  $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
  HtmlHelper::registerCssAndScriptsFiles(["/js/blockcms/jquery.touchSwipe.min.js"], $assetsUrl);

  $lastKey = 0;
  $itemHeaderSize = array();
  if(isset($paramsData["item"]) && is_array($paramsData["item"])){
    $itemHeaderSize = array_keys($paramsData["item"]);
    $lastKey = end($itemHeaderSize);
  }
  $initbackgroundPhoto= Document::getListDocumentsWhere(
    array(
      "id"=> (string)$blockCms["_id"],
      "type"=>'cms',
    ), "image"
  );
  //var_dump($initbackgroundPhoto);
    krsort($paramsData["item"]);
  ?> 
<style>

  .<?= $kunik ?> .carousel-inner > .item > .item-container {
    position: relative;
    height:70vh; 
    width: 100%;
  }
  .<?= $kunik ?> .title-1{
    font-size: 27px;
  }
  .<?= $kunik ?> .title-2{
    font-size: 90px;
    margin-top: 10px;
    margin-bottom: 0px;
  }
  .<?= $kunik ?>.title-3{
    font-size: 43px;
    color: #F0AD16;
    margin-top: 3px;
  }

  .<?= $kunik ?> .item-container{
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: 10%;
  }
  .<?= $kunik ?> .glyphicon-chevron-left,.<?= $kunik ?> .glyphicon-chevron-right{
    display: none;
  }
  .<?= $kunik ?>:hover .glyphicon-chevron-left,.<?= $kunik ?>:hover .glyphicon-chevron-right{
    display: initial;
  }
  @media (max-width: 672px){
    .<?= $kunik ?> .item-container{
        align-items: center;
        padding-left: initial;
    }
  }
  @media (max-width: 552px ){
   .<?= $kunik ?> .title-2{
      font-size: 41px;
      margin-top: 10px;
      margin-bottom: 0px;
    }
    .<?= $kunik ?> .carousel-inner > .item > .item-container {
      height:300px; 
    }
    .<?= $kunik ?> .title-1,.<?= $kunik ?> .title-2,.<?= $kunik ?> #myCarousel .title-3{
      font-size: 16px !important;
    }
  }
</style>
<div id="myCarousel" class="carousel slide <?= $kunik ?>" data-ride="carousel">
  <!-- data-interval="false" -->
  <ol class="carousel-indicators">
  <?php $i=0; foreach ($paramsData["item"] as $kk => $vv) { ?>
    <li data-target="#myCarousel" data-slide-to="<?= $i ?>" class="<?php echo ($i == 0) ? "active" : "" ?>"></li>
  <?php $i ++; } ?>
  </ol>

  <div class="carousel-inner">
<?php if(empty($paramsData["item"])){ ?>
  <div class="item active">
      <div class="item-container" style="background:grey url('<?= @$defaultImg ?>') no-repeat center /cover;">
          <div class="item-action">
            <button class="btn btn-sm btn-success add-item-<?= $kunik ?>">ajouter affiche</button>
          </div>
      </div>
  </div>
<?php } ?>

<?php 
 $itemBg = "";
 $imgId="";
 $j = 0;
  foreach ($paramsData["item"] as $key => $value) {
    foreach ($initbackgroundPhoto as $kb => $vb) {
      if($vb["subKey"] == $key ){
        $itemBg = $vb["imagePath"];
        $imgId =  (string)$vb["_id"];
      }
    }
   ?>
    <div class="item <?php echo ($j == 0) ? "active" : "" ?>">
      <div class="item-container" style="background:grey url('<?= $itemBg ?>') no-repeat center /cover;">
          <h4 class="title-1 sp-text" data-id="<?= $blockKey ?>" data-field="<?= $value["titleone"] ?>"><?= $value["titleone"] ?> </h4>
          <h1 class="title-2 sp-text" data-id="<?= $blockKey ?>" data-field="<?= $value["titletwo"] ?>"><?= $value["titletwo"] ?></h1>
          <h3 class="title-3 sp-text" data-id="<?= $blockKey ?>" data-field="<?= $value["titlethree"] ?>"><?= $value["titlethree"] ?></h3>
          <?php if(Authorisation::isInterfaceAdmin()){ ?>
          <div class="item-action hiddenPreview">
            <button class="btn btn-sm btn-success hiddenPreview add-item-<?= $kunik ?>">Nouveau</button>
            <button class="btn btn-sm btn-info hiddenPreview edit-item-<?= $kunik ?>" data-id="<?= $key ?>">Modifier</button>
            <button class="btn btn-sm btn-danger hiddenPreview delete-item" data-idimg="<?= $imgId ?>" data-id="<?= $key ?>">supprimer</button>
          </div>
          <?php } ?>
      </div>
    </div>
<?php $j++; } ?>
  </div>


  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  $(function(){
      var headerItem = <?php echo json_encode($paramsData["item"]) ?>;
      mylog.log('headeritem',headerItem["item"]);
      var initbackgroundPhoto = <?php echo json_encode($initbackgroundPhoto) ?>;
            mylog.log('initbackgroundPhoto',initbackgroundPhoto);
      sectionDyf.<?php echo $kunik ?>Params = {
        "jsonSchema" : {    
          "title" : "Configurer votre section",
          "description" : "Personnaliser votre gallerie",
          "icon" : "fa-cog",
          
          "properties" : {
              
          },
          beforeBuild : function(){
              uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
              if (k == "parent")
                tplCtx.value[k] = formData.parent;
            });
            console.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
              else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouté");
                    $("#ajax-modal").modal('hide');
                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                  });
                } );
              }

          }
        }
      };


      $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      });

      $(".carousel").swipe({
        swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
          if (direction == 'left') $(this).carousel('next');
          if (direction == 'right') $(this).carousel('prev');
        },
        allowPageScroll:"vertical"
      });

      $('.add-item-<?= $kunik ?>').off().on('click',function(){
         <?= $kunik ?>afficheDynForm();
      })
      $('.edit-item-<?= $kunik ?>').off().on('click',function(){
        var id = $(this).data('id');
        var copyBg = [];
        $.each(initbackgroundPhoto,function(k,v){
          if(v.subKey == id)
            copyBg.push(v);
        });
         <?= $kunik ?>afficheDynForm(id,copyBg);
      })
      $('.delete-item').off().on('click',function(){
          tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
          tplCtx.collection = "cms";
          tplCtx.path =  "item."+$(this).data('id') ;
          tplCtx.value = null;
          bootbox.confirm(trad["areyousuretodelete"], function(result){ 
              if(result)
                dataHelper.path2Value( tplCtx, function(params) {
                    toastr.success("Suppression réussie");

                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                    // urlCtrl.loadByHash(location.hash);
                });
          });
      });
      function <?= $kunik ?>afficheDynForm(id=null,img=null){
        var headerItem<?= $kunik ?> = {
            "jsonSchema" : {    
              "title" : notNull(id) ? "Modifier un affiche" : "Ajouter un affiche",
              "description" : "Ajouter 3 texts et une image de fond dans votre en-tête",
              "icon" : "fa-cog",
              "properties" : {
                  "titleone" : {
                    "inputType" : "text",
                    "label" : "Titre 1",  
                    value : (notNull(id) && exists(headerItem[id]["titleone"])) ? headerItem[id]["titleone"] : ""       
                  },
                  "titletwo" : {
                    "inputType" : "text",
                    "label" : "Titre 2",
                     value : (notNull(id) && exists(headerItem[id]["titletwo"])) ? headerItem[id]["titletwo"] : ""         
                  },
                  "titlethree" : {
                    "inputType" : "text",
                    "label" : "Titre 3",
                     value : (notNull(id) && exists(headerItem[id]["titlethree"])) ? headerItem[id]["titlethree"] : ""      
                  },
                  "bgImage" : {
                    inputType : "uploader",
                    "label" : "image",
                    "domElement" : "bgImage",
                    "docType": "image",
                    "contentKey" : "slider",
                    "itemLimit" : 1,
                    "filetypes": ["jpeg", "jpg", "gif", "png"],
                    "showUploadBtn": false,
                    "endPoint" : notNull(id) ? "/subKey/"+id : "/subKey/<?= $lastKey+1 ?>"
                  }
              },
              beforeBuild : function(){
                  uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
              },
              save : function (data) {  
                tplCtx.id = "<?= (string)$blockCms["_id"] ?>";
                tplCtx.collection = "cms";
                tplCtx.path =  notNull(id) ? "item."+id : "item.<?= $lastKey+1 ?>";
                tplCtx.value = {};
                $.each(headerItem<?= $kunik ?>.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                  if (k == "parent")
                    tplCtx.value[k] = formData.parent;
                });
                console.log("save tplCtx",tplCtx);

                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                  else {
                    dataHelper.path2Value( tplCtx, function(params) {
                      dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');

                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                      });
                    } );
                  }

              }
            }
          };
          if(notNull(img)){
              headerItem<?= $kunik ?>.jsonSchema.properties.bgImage["initList"] = img;
          }
        dyFObj.openForm(headerItem<?= $kunik ?>);
      }
  });
</script>