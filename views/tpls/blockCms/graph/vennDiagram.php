<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $graphAssets = [
     '/plugins/venn/venn.js'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>

<script type="text/javascript">
    var sets = [
            {sets : [0], label : 'Tiers Lieux', size : 50,}, 
            {sets : [1], label : 'Formation', size: 65},
            {sets : [2], label : 'Mise en Réseau', size : 168}, 
            {sets : [3], label : 'Accompagnement - support', size:186},
            {sets : [4], label : 'Financement', size:156},

            
            {sets : [1,3], size:51, images:[{name:"MSLDK"}]},
            {sets : [2,3], size:1},
            {sets : [0,3], size:40},
            {sets : [0,1], size:45},
            {sets : [2,0], size:57},
            {sets : [2,3], size:109},
            {sets : [2,1], size:32},
            {sets : [4,3], size:86}
        ];
</script>
    <div id="vennDiagram" class="text-center">
        <!--link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'-->
        <script>
            var chart = venn.VennDiagram()
                             .width(800)
                             .height(600);

            d3.select("#vennDiagram").datum(sets).call(chart);

            var colours = d3.schemeCategory10;
            var areas = d3.selectAll("#vennDiagram g")
            areas.select("path")
                .filter(function(d) { return d.sets.length == 1; })
                .style("fill-opacity", .2)
                .style("stroke-width", 5)
                .style("stroke-opacity", .8)
                .style("fill", function(d,i) { return colours[i]; })
                .style("stroke", function(d,i) { return colours[i]; });

                d3.selectAll(".venn-circle").append("image")
                  .attr("xlink:href", "http://communecter74-dev/upload/communecter/organizations/5718adb340bb4eeb271d6558/medium/logopnrun.png")
                  .attr("width", 40)
                  .attr("height", 40)
                
            areas.select("text").style("fill", function(d,i) { return colours[i]; })
                .style("font-size", "20px");

          var defs = d3.select("#vennDiagram svg").append("defs");

          // from http://stackoverflow.com/questions/12277776/how-to-add-drop-shadow-to-d3-js-pie-or-donut-chart
          var filter = defs.append("filter")
              .attr("id", "vennDiagramfilter")

          filter.append("feGaussianBlur")
              .attr("in", "SourceAlpha")
              .attr("stdDeviation", 4)
              .attr("result", "blur");
          filter.append("feOffset")
              .attr("in", "blur")
              .attr("dx", 5)
              .attr("dy", 5)
              .attr("result", "offsetBlur");

          var feMerge = filter.append("feMerge");

          feMerge.append("feMergeNode")
              .attr("in", "offsetBlur")
          feMerge.append("feMergeNode")
              .attr("in", "SourceGraphic");

          areas.attr("filter", "url(#vennDiagramfilter)");
        </script>
    </div>