<?php
    $keyTpl = "chartGraph";
    $myCmsId  = $blockCms["_id"]->{'$id'};
?>
<style>
    #g<?= $kunik ?>{
        transform: translate(50%, 50%);
    }
</style>

<div class="<?= $kunik ?>">
    <div id="graph<?= $kunik ?>">  
    </div>
</div>

<script>
    $(function(){
        const data = <?= json_encode($blockCms['dataAnswers']) ?>;
        if(data && data.length > 0){
            const width = 450,
            height = 450,
            margin = 40;

        // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
        const radius = Math.min(width, height) / 2 - margin;
        const color = d3.scaleOrdinal()
            .range(d3.schemeSet3)
            // .interpolate(d3.interpolateRainbow);
        const svg = d3.select("#graph<?= $kunik ?>")
            .append("svg")
                .attr("width", "100%")
                .attr("height", height)
            .append("g")
                .attr('id', "g<?= $kunik ?>");
                // .attr("transform", `translate(${width/2},${height/2})`);
        
        const pie = d3.pie()
            .sort(function (a, b) {
                return b[1] > a[1];
            }) // Do not sort group by size
            .value(d => {console.log("LOG LOG", d); return d[1]})
        
        const arc = d3.arc()
            .innerRadius(radius * 0.5)         // This is the size of the donut hole
            .outerRadius(radius)

            // Another arc that won't be drawn. Just for labels positioning
        const outerArc = d3.arc()
            .innerRadius(radius * 0.9)
            .outerRadius(radius * 0.9)

        var text = svg.select(".labels").selectAll("text")
		    .data(pie(data), key);

        text.enter()
            .append("text")
            .attr("dy", ".35em")
            .text(function(d) {
                return d[0];
            });

        
            var polyline = svg.select(".lines").selectAll("polyline")
		.data(pie(data), key);
	
	polyline.enter()
		.append("polyline");

	polyline.transition().duration(1000)
		.attrTween("points", function(d){
			this._current = this._current || d;
			var interpolate = d3.interpolate(this._current, d);
			this._current = interpolate(0);
			return function(t) {
				var d2 = interpolate(t);
				var pos = outerArc.centroid(d2);
				pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
				return [arc.centroid(d2), outerArc.centroid(d2), pos];
			};			
		});
	
	polyline.exit()
            
        if(data && typeof data.percentageArray != "undefined"){
            const dataEntries = {};
            $.map(data.percentageArray, function(value, key){
                dataEntries[value.label] = value.value;
            });
            const data_ready = pie(Object.entries(dataEntries));
            svg.selectAll('allSlices')
                .data(data_ready)
                .join('path')
                .attr('d', arc)
                .attr('fill', d => color(d.data[1]))
                .attr("stroke", "white")
                .style("stroke-width", "2px")
                .style("opacity", 0.7)
        }
        }
    });

    if (costum.editMode) {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
        }
        chartGraphInput = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "select",
                            options: {
                                name: "typeChart",
                                label: "Type de graph que vous voulez utiliser",
                                options: [
                                    {"value" : "donut", label: "Donut Chart"},
                                    {"value" : "pie", label: "Pie Chart"},
                                    {"value" : "bar", label: "Bar Chart"},
                                ]
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "coform",
                                label: "Choisir un Formulaire :",
                                options: coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                label: " À Quelle Question corresponds la graph ?",
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                label: " À Quelle Question corresponds la graph ?",
                                options: answerValueData
                            }
                        },
                        "color"
                    ]
                }/* ,
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ],
                } */
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
                    cmsConstructor.blocks.chartGraph<?= $myCmsId ?>.configTabs.general.inputsConfig[2].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){

                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
                    cmsConstructor.blocks.chartGraph<?= $myCmsId ?>.configTabs.general.inputsConfig[3].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        };
        cmsConstructor.blocks.chartGraph<?= $myCmsId ?> = chartGraphInput;
        function getAnswerPath(value) {
            var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            var children = [];
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    var input = childForm[value][stepKey][inputKey];
                    //var isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                        // $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".radiocplx")){
                        children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".checkboxNew")){
                        children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".radioNew")){
                        children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                    }
                    if(input["type"]=="text"){
                        children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                    }
                }
            }
            return children;
        }

        function getAnswerValue(coformId, value) {
            var coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[coformId] != "undefined" ){
                coform = coform[coformId];
            }
            mylog.log("COform for answerValue", coform);
            var input = value.split(".")[1];
            var answerValue = [];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        answerValue.push({value: paramValue, label: paramValue})
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
            return answerValue;
        }
    }
</script>