<?php 
    $kunik = "communityHexagone";

    $graphAssets = [
        '/js/hexagon.js'
    ];
    HtmlHelper::registerCssAndScriptsFiles(
        $graphAssets,
        Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
    );
?>
<style type="text/css">
    #modal-preview-coop{
        left: 0;
        box-shadow: none !important;
    }

    #close-graph-modal{
        border-radius: 50px;
        border: 4px solid #f5f5f5;
        font-size: 16pt;
    }
    .container-preview{
        display: flex;
        justify-content: center;
        align-items: stretch;
    }

    .container-preview #filter-hexagone-container{
		display: flex;
		flex-direction: column;
		width: 300px;
		background-color: white;
        height: 700px;
        overflow-y: auto;
    }
    .hexagone-child-container.in{
		display: flex;
		flex-direction: column;
		text-align: left;
	}
	.hexagone-child-container{
		padding: 0px 15px;
	}

	.hexagone-child-container .btn-hexagone-zoomer{
		text-align: left;
	}
    .btn-hexa-container{
		background: white;
    	border: 1px solid;
		padding: 0;
		text-align: left;
	}
	.btn-hexa-container.btn-primary{
		color: white;
		background-color: #2C3E50;
		border-color: #2C3E50;
		font-weight: 700;
	}

	.btn-hexa-container button{
		background: none;
    	border: none;
		padding: 6px 12px;
	}
</style>
<div id="preview-graph" class="col-xs-12 no-padding">
	<div class="col-xs-12 padding-10">
        <span class="pull-left">
            <h4>Hexagone visualisation du communauté</h4>
        </span>
		<!-- <button id="close-graph-modal" class="btn btn-default pull-right btn-close-preview">
			<i class="fa fa-times"></i>
		</button> -->
	</div>
	<div class="container-preview padding-10" style="overflow-y: auto;">
        <canvas id="hexagoneCommunity" height="700px"></canvas>
        <div id="filter-hexagone-container">

        </div>
	</div>
</div>

<script>
    HexagonGrid.prototype.clickEvent = function(e){
        let hexa = this;
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        var localX = mouseX - this.canvasOriginX;
        var localY = mouseY - this.canvasOriginY;

        var tile = this.getSelectedTile(localX, localY);
        console.log("Tile", tile);
        if(this.hasHexagon(tile.column, tile.row)){
            let drawedHex = this.drawedHex[`${tile.column},${tile.row}`];
            let data = this.data[drawedHex.group][`${drawedHex.column},${drawedHex.row}`];
            let params = {};
            params["userId"] = data.id;
            params["name"] = data.text;
            params["type"] = data.type;
            params["socialNetwork"] = [];
            params["profileImg"] = data.image != null ? data.image : "";
            params["userCredits"] = 0 ;
            ajaxPost(
                null,
                baseUrl + "/co2/person/getuserbadgeandrecentcontribution",
                params, 
                function (response) {  
                    params["badgesDetails"] = response.badges;
                    params["projects"] = response.projects;
                    params["actionsCreated"] = response.actionsCreated;
                    params["actionsParticipated"] = response.actionsParticipated;
                    params["actionsLast12Month"] = response.actionsLast12Month;
                    params["countAll"] = response.countAll;

                    urlCtrl.openPreview("/view/url/costum.views.custom.community.previewWithProjectAndBadge", { "params" : params}, false);
                }
            );
        }
    }

    var hexagonCommunity<?= $kunik ?> = new HexagonGrid("hexagoneCommunity", 50, window.innerWidth - 700);
    $(function(){
        var typeCostum = "";
        var idCostum = "";
        var slugCostum = "";
        if(contextData != null && typeof contextData != 'undefined'){
            idCostum = typeof contextData.id != 'undefined' ? contextData.id : contextData._id.$id;
            typeCostum = typeof contextData.collection != "undefined" ? contextData.collection : contextData.type;
            slugCostum = contextData.slug;
        } else if(typeof costum != 'undefined' && costum !== null){
            if(typeof costum.contextType !='undefined'){
                idCostum = costum.contextId;
                typeCostum = costum.contextType;
                slugCostum = costum.contextSlug;
            }
        }
        var params<?= $kunik ?> = {     
            searchType : ["citoyens","organizations"],
            notSourceKey : true,
            fields : ["links"],
            filters : {},
            indexStep : 0        
        };
        if(typeCostum === "organizations"){
            params<?= $kunik ?>.filters["links.memberOf."+idCostum] = {'$exists':true};
        }
        else if(typeCostum === "projects"){
            params<?= $kunik ?>.filters["links.projects."+idCostum] = {'$exists':true};
        }
        else {
            params<?= $kunik ?>.filters["links.events."+idCostum] = {'$exists':true};
        }

        let center = hexagonCommunity<?= $kunik ?>.getCenterTile();
        function getData(){
            dataMember<?= $kunik ?> = {
                "roles": [],
                "data" : {

                }
            };
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params<?= $kunik ?>,
                function(data){
                    if(typeof data.results != "undefined"){
                        $.map(data.results, function(valeur, key){
                            if(typeCostum == "organizations"){
                                if(exists(valeur.links) && exists(valeur.links.memberOf)){
                                    $.map(valeur.links.memberOf, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                $.map(value.roles, function(role, roleKey){
                                                    if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMember<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: []
                                                        };
                                                    }
                                                    dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                                });
                                            }else {
                                                if(typeof dataMember<?= $kunik ?>.data['Pas de role'] === 'undefined'){
                                                    dataMember<?= $kunik ?>.data['Pas de role'] = {
                                                        "group": 'Pas de role',
                                                        childs: []
                                                    };
                                                }

                                                dataMember<?= $kunik ?>.data['Pas de role'].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                            }
                                        }
                                    });
                                }
                            }
                            if(typeCostum == "events"){
                                if(exists(valeur.links) && exists(valeur.links.events)){
                                    $.map(valeur.links.events, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                
                                                $.map(value.roles, function(role, key){
                                                    if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMember<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: []
                                                        };
                                                    }
                                                    dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null });
                                                });
                                            }else {
                                                if(typeof dataMember<?= $kunik ?>.data['Pas de role'] === 'undefined'){
                                                    dataMember<?= $kunik ?>.data['Pas de role'] = {
                                                        "group": 'Pas de role',
                                                        childs: []
                                                    };
                                                }

                                                dataMember<?= $kunik ?>.data['Pas de role'].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                            }
                                        }
                                    });
                                }
                            }
                            if(typeCostum == "projects"){
                                console.log("Projects data", valeur);
                                if(exists(valeur.links) && exists(valeur.links.projects)){
                                    $.map(valeur.links.projects, function(value, idData){
                                        if(idCostum == idData){
                                            if(typeof value.roles === 'object' && Array.isArray(value.roles)) {
                                                dataMember<?= $kunik ?>.roles.push(...value.roles);
                                                
                                                $.map(value.roles, function(role, key){
                                                    if(typeof dataMember<?= $kunik ?>.data[role] === 'undefined'){
                                                        dataMember<?= $kunik ?>.data[role] = {
                                                            "group": role,
                                                            childs: []
                                                        };
                                                    }
                                                    dataMember<?= $kunik ?>.data[role].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null });
                                                });
                                            }else {
                                                if(typeof dataMember<?= $kunik ?>.data['Pas de role'] === 'undefined'){
                                                    dataMember<?= $kunik ?>.data['Pas de role'] = {
                                                        "group": 'Pas de role',
                                                        childs: []
                                                    };
                                                }

                                                dataMember<?= $kunik ?>.data['Pas de role'].childs.push({text: valeur.name, color: "green", image: typeof valeur.profilMediumImageUrl != "undefined" ? valeur.profilMediumImageUrl : null, id: key, type: valeur.collection });
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },null,null, {async:false}
            )
            dataMember<?= $kunik ?>.roles = [...new Set(dataMember<?= $kunik ?>.roles)];
            $.each(dataMember<?= $kunik ?>.data, function(key, value){
                let btn = `<div class="btn margin-5 btn-hexa-container">
                    <button class="btn-expand" data-toggle="collapse" href="#${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                        <i class="fa fa-chevron-down"></i>
                    </button>
                    <button class="btn-hexagone-zoomer" data-type="group">
                        ${key}
                        <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${dataMember<?= $kunik ?>.data[key].childs.length}</span>
                    </button>
                </div>
                <div class="collapse hexagone-child-container" id="${hexagonCommunity<?= $kunik ?>.slugify(key)}">
                `;
                dataMember<?= $kunik ?>.data[key].childs.forEach((child) => {
                    btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonCommunity<?= $kunik ?>.slugify(child.text)}'>
                        ${child.text}
                    </button>`;
                })
                btn += `</div>`
                $(".container-preview #filter-hexagone-container").append(btn);
            });
            hexagonCommunity<?= $kunik ?>.placeGroupsWithOffsets(Object.values(dataMember<?= $kunik ?>.data), center);
            hexagonCommunity<?= $kunik ?>.fitBounds();
            $(".btn-hexagone-zoomer").off().on('click', function(){
                if($(this).data("type") == "group"){
                    if($(this).parents(".btn-hexa-container").hasClass("btn-primary")){
                        $(".btn-hexa-container").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let group = $(this).find("span").data("countvalue");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).parents(".btn-hexa-container").addClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomToGroup(group);
                    }
                }else{
                    if($(this).hasClass("btn-primary")){
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        hexagonCommunity<?= $kunik ?>.zoomTo = [];
                        hexagonCommunity<?= $kunik ?>.fitBounds();
                    }else{
                        let texte = $(this).data("value");
                        $(".btn-hexa-container").removeClass("btn-primary");
                        $(".btn-hexagone-zoomer").removeClass("btn-primary");
                        $(this).addClass("btn-primary");
                        let element = Object.values(hexagonCommunity<?= $kunik ?>.drawedHex).find(hex => {
                            return hexagonCommunity<?= $kunik ?>.slugify(hex.text) == texte;
                        });
                        if(typeof element != "undefined"){
                            hexagonCommunity<?= $kunik ?>.zoomToElement(element.column, element.row);
                        }
                    }
                }
            })
        }
        getData();
    });
</script>