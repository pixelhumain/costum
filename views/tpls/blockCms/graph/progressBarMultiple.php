<?php
    $keyTpl = "progressBarMultiple";
    $blockId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
?>
<style id="css-<?= $kunik ?>">
    .progress-contain<?= $blockId ?>{
        margin-top: <?= -5 + (($blockCms['labelSize']-16)*2+35-$blockCms["progressBarHeight"]) ?>px;
    }
    .progress-contain<?= $blockId ?> .progress-bar {
        text-align: left;
        white-space: nowrap;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        cursor: pointer;
        height: <?= $blockCms["progressBarHeight"] ?>px;
        z-index: 0;
    }
    .progress-contain<?= $blockId ?> .progress-type {
        font-size: 16pt;
        padding-left: <?= (($blockCms["progressBarHeight"]<30)?0: 1) ?>em;
        padding-bottom: 1em;
        z-index: 4;
        position: absolute;
        margin-top: <?= - (($blockCms['labelSize']-16)*2 + 35-$blockCms["progressBarHeight"]) ?>px;
    }
    .progress-contain<?= $blockId ?> .progress {
        height: <?= $blockCms["progressBarHeight"] ?>px;
        z-index: 0;
        /*margin-top: <?= 35-$blockCms["progressBarHeight"] ?>px;*/
        border-radius: 2px;
        box-shadow: none !important;
    }

    .flex-container {
        display: flex;
        flex-direction: row;
    }

    .flex-left<?= $blockId ?> {
        position: relative;
        font-weight: bolder;
        font-size: <?= $blockCms['labelSize']; ?>pt;
    }
</style>

<div class="<?= $kunik ?>">
    <div id="progressbars<?= $blockId ?>"></div>
</div>

<script>

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    $(function(){
        var data = <?= json_encode($blockCms['dataAnswers']) ?>;
        var progressbarHtml = "";
        if(data && typeof data.percentageArray != "undefined" && data.percentageArray.length>0){
            data = data.percentageArray.sort((a, b) => {
                return b.value - a.value;
            });
        }else{
            data = [{label:"Placeholder 01", value: 20}, {label:"Placeholder 02", value:50}]
        }
        $.each(data, function(index, line){
            progressbarHtml += `
            <div class="progress-contain<?= $blockId ?> margin-top-20">
                <div class="progress-type flex-container">
                    <span class="flex-left<?= $blockId ?> padding-right-10 percentColor"><?= ($blockCms["showValue"] == false ? '${line.value} %'  : "") ?> <?= ($blockCms["withStaticTextBottom"] == true)?'<span class="labelColor">${line.label}</span>':'' ?></span>
                    <span class="flex-right">
                        <span class="sp-text" data-id="<?= $blockId ?>" data-field="textOnProgressBar"><?= $blockCms["textOnProgressBar"] ?></span>
                    </span>
                </div>
                <div class="mp-progress padding-0">
                    <div class="progress emptyColor progress<?= $blockId ?>">
                        <div class="progress-bar completeColor" role="progressbar" aria-valuenow="${line.value}" aria-valuemin="0" aria-valuemax="100" style="width: ${line.value}% !important;" data-toggle="tooltip" data-placement="top" title="${line.value||0}%">
                            <span class="sr-only">
                                ${line.value}% Complete
                            </span>
                        </div>
                    </div>
                    <?php if($blockCms["withStaticTextBottom"] == false){ ?>
                        <div class="sp-text margin-top-10 labelColor" style="font-size:14pt" data-id="<?= $blockId ?>" data-field="label">${line.label}</div>
                    <?php } ?>
                </div>
            </div>`;
        });

        $("#progressbars<?= $blockId ?>").append(progressbarHtml)
    });

    if (costum.editMode) {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $blockId ?>"] = <?= json_encode($blockCms) ?>;
        if(cmsConstructor.sp_params["<?= $blockId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $blockId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockId ?>"].coform, cmsConstructor.sp_params["<?= $blockId ?>"].answerPath);
        }
        progressBarMultipleInput = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "select",
                            options : {
                                name : "coform",
                                isSelect2 : true,
                                label : tradCms.selectaform ?? "Selectionner un formulaire",
                                options : coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                label: tradCms.whichQuestionCorrespondstotheGraph ?? "À Quelle Question corresponds la graph ?",
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                label: tradCms.answervalue ?? "À Quelle Question corresponds la graph ?",
                                options: answerValueData
                            }
                        }
                    ]
                } ,
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "percentColor",
                                label: tradCms.colorofpercentagetext ?? "Couleur de pourcentage",
                                inputs : ["color"]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "labelColor",
                                label: tradCms.labelcolor ?? "Couleur du label",
                                inputs : ["color"]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "emptyColor",
                                label: "Couleur du bar vide",
                                inputs : ["backgroundColor"]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "completeColor",
                                label: "Couleur du bar rempli",
                                inputs : ["backgroundColor"]
                            }
                        },
                        {
                            type: "groupButtons",
                            options:{
                                name: "withStaticTextBottom",
                                label: "Texte en bas ou sur le bar",
                                options:[
                                    {label:"Sur bar", value:true},
                                    {label:"En bas", value:false}
                                ],
                                defaultValue: cmsConstructor.sp_params["<?= $blockId ?>"].withStaticTextBottom
                            }
                        },
                        {
                            type: "groupButtons",
                            options:{
                                name: "showValue",
                                label: "Afficher le pourcentage",
                                options:[
                                    {label: trad.yes, value:false},
                                    {label: trad.no, value:true}
                                ],
                                defaultValue: cmsConstructor.sp_params["<?= $blockId ?>"].showValue
                            }
                        }
                    ]
                }
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockId ?>"].coform);
                    cmsConstructor.blocks.progressBarMultiple<?= $blockId ?>.configTabs.general.inputsConfig[1].options.options = answerPathData;
                    $(".cmsbuilder-block[data-blockId=<?= $blockId ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockId ?>"].coform, cmsConstructor.sp_params["<?= $blockId ?>"].answerPath);
                    cmsConstructor.blocks.progressBarMultiple<?= $blockId ?>.configTabs.general.inputsConfig[2].options.options = answerValueData;
                    $(".cmsbuilder-block[data-blockId=<?= $blockId ?>]").trigger("click");
                }
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
            }
        };
        cmsConstructor.blocks.progressBarMultiple<?= $blockId ?> = progressBarMultipleInput;
        function getAnswerPath(value) {
            var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            var children = [];
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    var input = childForm[value][stepKey][inputKey];
                    //var isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                        // $("#answerPath.<?php echo $blockId ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".radiocplx")){
                        children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".checkboxNew")){
                        children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                    }
                    if(input["type"].includes(".radioNew")){
                        children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                    }
                    if(input["type"]=="text"){
                        children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                    }
                }
            }
            return children;
        }

        function getAnswerValue(coformId, value) {
            var coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[coformId] != "undefined" ){
                coform = coform[coformId];
            }
            mylog.log("COform for answerValue", coform);
            var input = value.split(".")[1];
            var answerValue = [];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        answerValue.push({value: paramValue, label: paramValue})
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
            return answerValue;
        }
    }
</script>