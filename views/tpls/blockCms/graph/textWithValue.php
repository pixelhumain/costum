<?php
    $keyTpl = "textWithValue";
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $spText = ["textLeft","textRight"];

    foreach ($spText as $key) {
        if( !(is_array($blockCms[$key]))){
            ${$key} = ["fr" => $blockCms[$key]];
        } else {
            ${$key} = $blockCms[$key];
        }
    };
?>

<style type="text/css" id="css-<?= $kunik ?>">
    .textWithValue<?= $kunik; ?>{
        font-weight: bolder;
    }

    #ttp<?= $kunik; ?>{
        border:5px solid #ddd;
        border-radius: 50%
    }

    .profil-icon{
        background-image: url("<?=Yii::app()->getModule('costum')->assetsUrl; ?>/images/franceTierslieux/profil_pro.png");
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
    }
</style>

<div class="<?= $kunik ?>">
    <div class="chartviz">
        <div class="text-<?= $blockCms["textAlign"] ?>">
            <span class="padding-10 padding-top-40" style="background:<?= $blockCms["css"]["textValue"]["backgroundColor"]; ?>">
                <?php
                    if($blockCms["withStaticTextLeft"]){ ?>
                    <span class="sp-text margin-top-10 textLeft<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="textLeft"></span>
                <?php  }
                ?>
                <span class="textWithValue<?= $kunik; ?> textValue margin-top-10" ><?= @$blockCms["countage"] ? number_format(@$blockCms["dataAnswers"]["count"], 0, ",", " ") : @$blockCms["dataAnswers"]["value"]  ?></span>
                <?php
                    if($blockCms["withStaticTextRight"]){ ?>
                    <span class="sp-text margin-top-10 textRight<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="textRight"></span>
                <?php  }
                ?>
            </span>
        </div>
    </div>
    <table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10">
        <tr><td>Valeur :</td><td class="textWithValue<?= $kunik; ?>"></td></tr>
    </table>
</div>

<script type="text/javascript">
    $(function(){
        sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
       
        if(typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers != "undefined" && sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers){
            var pada = sectionDyf.<?php echo $kunik ?>BlockCms;
            if( 
                pada.valBloc01 != "" 
                && pada.valBloc02 != "" 
                && typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada != "undefined"
                && typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc01 != "undefined" 
                && typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc02 != "undefined" 
                && sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc01.value.$numberDecimal 
                && sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc02.value.$numberDecimal){

                var v1 = parseInt(sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc01.value.$numberDecimal);
                var v2 = parseInt(sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.pada.valBloc02.value.$numberDecimal);
                var sum = v1+v2;
                
                $(".textWithValue<?= $kunik; ?>").text((sum));

            }
            if(typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.respondentCount != "undefined"){
                $(function(){
                    $("#ttp<?= $kunik; ?>").tooltip({
                        placement: "left",
                        title: "Ce chiffre est par rapport au nombre des répondant sur ce question ( "+sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.respondentCount+" )",
                    });
                });
            } else {
                $("#ttp<?=$kunik;?>").hide();
            }
        }
        $("#tableData<?=$kunik?>").hide();
    });
</script>
<script>

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);

    if (costum.editMode) {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];

        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
        if(cmsConstructor.sp_params["<?= $blockKey ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockKey ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $blockKey ?>"].coform != "" && cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockKey ?>"].coform, cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath);
        }
        
        var textWithValue = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "select",
                            options : {
                                name : "coform",
                                isSelect2 : true,
                                label : tradCms.selectaform ?? "Selectionner un formulaire",
                                options : coformOptions.length > 0 ? coformOptions : []
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "answerPath",
                                isSelect2 : true,
                                label : tradCms.whichQuestionCorrespondstotheGraph ?? " À Quelle Question corresponds la graph ?",
                                options : answerPathData.length > 0 ? answerPathData : []
                            }
                        },
                        {
                            type : "selectMultiple",
                            options : {
                                name : "answerValue",
                                label : tradCms.answervalue ??  "Valeur répondu",
                                options : answerValueData.length > 0 ? answerValueData : []
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "textAlign",
                                class : "<?php echo $kunik ?>",
                                label : tradCms.textalignment,
                                options : [
                                    {value : "center", label: tradCms.center},
                                    {value : "left", label: tradCms.left},
                                    {value : "right", label: tradCms.right}
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].textAlign
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "withStaticTextLeft",
                                label : tradCms.textOnLeft ?? "Texte à gauche",
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].withStaticTextLeft
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "withStaticTextRight",
                                label : tradCms.textOnRight ?? "Texte à droite",
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value :true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].withStaticTextRight
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "roundMillionValue",
                                label : tradCms.roundTheValueMillion ?? "Arrondir la valeur en million par M",
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].roundMillionValue
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "isAverage",
                                label : tradCms.displayValueAverage ?? "Afficher la valeur en moyen",
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].isAverage
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "countage",
                                label : tradCms.display ?? "Affichage",
                                options : [
                                    {
                                        value : false,
                                        label : "Pourcentage"
                                    },
                                    {
                                        value : true,
                                        label : "Nombre"
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].isAverage
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "applyFormForAll",
                                label : tradCms.applyTheFormAllThePageGraphicCharts ?? "Appliquer le formulaire à tous les chartes graphiques de la page",
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $blockKey ?>"].applyFormForAll
                            }
                        },
                        
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "textValue",
                                label : tradCms.textStyle ?? "Style du texte",
                                inputs : [
                                    "fontSize",
                                    "color",
                                    "backgroundColor"
                                ]
                            }
                        }
                        
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockKey ?>"].coform);
                    cmsConstructor.blocks.textWithValue<?= $blockKey ?>.configTabs.general.inputsConfig[1].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockKey ?>"].coform, cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath);
                    cmsConstructor.blocks.textWithValue<?= $blockKey ?>.configTabs.general.inputsConfig[2].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
            }
        }
        cmsConstructor.blocks.textWithValue<?= $blockKey ?> = textWithValue;
    }

    //sp-params and text append 

	appendTextLangBased(".textLeft<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($textLeft) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".textRight<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($textRight) ?>,"<?= $blockKey ?>");


    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];

                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }else if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }else if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }else if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }else if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }else if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }else {
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }

    function getAnswerValue(coformId, value) {
        var coforme = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coforme = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coforme[coformId] != "undefined" ){
            coforme = coforme[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(typeof input!="undefined" && ( input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx") ) ){
            if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["global"]){
                for(const paramValue of coforme["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }

        if( typeof input!="undefined" && ( input.includes("checkboxNew") || input.includes("radioNew") ) ){
            if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["list"]){
                for(const paramValue of coforme["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        return answerValue;
    }
    
</script>

