<?php 
    $keyTpl     = "pourcentageLayers";
    $paramsData = [
        "label" => "Ajouter label ici",
        "coform" => "",
        "answerPath" => "",
        "answerValue" => "",
        "textOnProgressBar" => "",
        "progressBarHeight" => 35,
        "labelSize" => 16,
        "percentColor" => "white",
        "emptyColor" => "#FF286B",
        "completeColor" => "#9B6FAC",
        "withStaticTextBottom" => true,
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<?php ?>
<svg id="Calque_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 842.7 556.19"><defs>
    <style>
        .clsl-1{font-family:Poppins-Bold, Poppins;font-weight:700;}
        .clsl-1,
        .clsl-2{fill:#a5c145;}
        .clsl-1,
        .clsl-3{font-size:21px;}
        .clsl-4{fill:#adadad;}
        .clsl-5,
        .clsl-3{fill:#4d4d4d;}
        .clsl-3{font-family:Poppins-SemiBold, Poppins;font-weight:600;}
    </style>
</defs>
<polygon class="clsl-5" points="119.94 540.13 9.23 464.5 119.94 391.14 230.65 464.5 119.94 540.13"/>
<polygon class="clsl-2" points="119.94 505.05 9.23 429.42 119.94 356.06 230.65 429.42 119.94 505.05"/>
<polygon class="clsl-5" points="119.94 469.97 9.23 394.35 119.94 320.98 230.65 394.35 119.94 469.97"/>
<polygon class="clsl-2" points="119.94 434.89 9.23 359.27 119.94 285.91 230.65 359.27 119.94 434.89"/>
<polygon class="clsl-5" points="119.94 404.16 9.23 328.53 119.94 255.17 230.65 328.53 119.94 404.16"/>
<polygon class="clsl-2" points="119.94 369.08 9.23 293.45 119.94 220.09 230.65 293.45 119.94 369.08"/>
<polygon class="clsl-5" points="119.94 334 9.23 258.37 119.94 185.01 230.65 258.37 119.94 334"/>
<polygon class="clsl-2" points="119.94 298.92 9.23 223.29 119.94 149.93 230.65 223.29 119.94 298.92"/>
<polygon class="clsl-4" points="448.09 368.44 404.52 328.08 242.91 328.08 242.91 326.72 405.06 326.72 449.01 367.45 448.09 368.44"/>
<polygon class="clsl-4" points="476.88 335.08 433.31 294.72 242.91 294.72 242.91 293.36 433.84 293.36 477.8 334.09 476.88 335.08"/>
<polygon class="clsl-4" points="500.84 299.97 457.27 259.6 242.91 259.6 242.91 258.24 457.81 258.24 501.76 298.97 500.84 299.97"/>
<polygon class="clsl-4" points="523.67 264.34 480.11 223.98 242.91 223.98 242.91 222.61 480.64 222.61 524.6 263.34 523.67 264.34"/>
<polygon class="clsl-4" points="325.67 503.47 282.1 463.1 242.91 463.1 242.91 461.74 282.63 461.74 326.59 502.47 325.67 503.47"/>
<polygon class="clsl-4" points="354.45 470.11 310.89 429.74 242.91 429.74 242.91 428.38 311.42 428.38 355.38 469.11 354.45 470.11"/>
<polygon class="clsl-4" points="378.42 434.99 334.85 394.63 242.91 394.63 242.91 393.27 335.38 393.27 379.34 434 378.42 434.99"/>
<polygon class="clsl-4" points="401.25 399.37 357.68 359 242.91 359 242.91 357.64 358.22 357.64 402.17 398.37 401.25 399.37"/>
<polygon class="clsl-5" points="119.94 264.85 9.23 189.23 119.94 115.86 230.65 189.23 119.94 264.85"/>
<polygon class="clsl-2" points="119.94 229.77 9.23 154.15 119.94 80.79 230.65 154.15 119.94 229.77"/>
<polygon class="clsl-5" points="119.94 194.7 9.23 119.07 119.94 45.71 230.65 119.07 119.94 194.7"/>
<polygon class="clsl-2" points="119.94 159.62 9.23 83.99 119.94 10.63 230.65 83.99 119.94 159.62"/>
<polygon class="clsl-4" points="543.95 229.14 500.38 188.77 242.91 188.77 242.91 187.41 500.92 187.41 544.87 228.14 543.95 229.14"/>
<polygon class="clsl-4" points="572.74 195.78 529.17 155.41 242.91 155.41 242.91 154.05 529.7 154.05 573.66 194.78 572.74 195.78"/>
<polygon class="clsl-4" points="596.7 160.67 553.13 120.3 242.91 120.3 242.91 118.94 553.67 118.94 597.62 159.67 596.7 160.67"/>
<polygon class="clsl-4" points="619.53 125.04 575.97 84.67 242.91 84.67 242.91 83.31 576.5 83.31 620.46 124.04 619.53 125.04"/>
<text class="clsl-3" transform="translate(639.48 127.76)"><tspan x="0" y="0">Aménageur</tspan></text>
<text class="clsl-1" transform="translate(781.8 127.76)"><tspan id="Aménageur" x="0" y="0">01%</tspan></text>
<text class="clsl-1" transform="translate(781.8 168.72)"><tspan id="Bailleursocial" x="0" y="0">02%</tspan></text>
<text class="clsl-1" transform="translate(781.8 200.55)"><tspan id="Commune" x="0" y="0">03%</tspan></text>
<text class="clsl-1" transform="translate(781.8 236.94)"><tspan id="Intercommunalité" x="0" y="0">04%</tspan></text>
<text class="clsl-1" transform="translate(698.86 275.09)"><tspan id="Département" x="0" y="0">05%</tspan></text>
<text class="clsl-1" transform="translate(603.04 310.7)"><tspan id="Région" x="0" y="0">06%</tspan></text>
<text class="clsl-1" transform="translate(541.93 343.27)"><tspan id="État" x="0" y="0">07%</tspan></text>
<text class="clsl-1" transform="translate(649.71 377.85)"><tspan id="Opérateurpublic" x="0" y="0">08%</tspan></text>
<text class="clsl-1" transform="translate(663.19 409.71)"><tspan id="Promoteurimmobilier" x="0" y="0">09%</tspan></text>
<text class="clsl-1" transform="translate(701.59 445.01)"><tspan id="Propriétaireprivéparticulier" x="0" y="0">10%</tspan></text>
<text class="clsl-1" transform="translate(716.7 475.84)"><tspan id="Propriétaireprivéinstitutionnel" x="0" y="0">11%</tspan></text>
<text class="clsl-1" transform="translate(389.5 512.42)"><tspan id="SEM" x="0" y="0">12%</tspan></text>
<text class="clsl-3" transform="translate(609.48 166.97)"><tspan x="0" y="0">Bailleur social</tspan></text>
<text class="clsl-3" transform="translate(589.48 200.55)"><tspan x="0" y="0">Commune</tspan></text>
<text class="clsl-3" transform="translate(560.48 235.63)"><tspan x="0" y="0">Intercommunalité</tspan></text>
<text class="clsl-3" transform="translate(540.48 273.34)"><tspan x="0" y="0">Département</tspan></text>
<text class="clsl-3" transform="translate(517.48 308.95)"><tspan x="0" y="0">Région</tspan></text>
<text class="clsl-3" transform="translate(490.48 341.52)"><tspan x="0" y="0">État</tspan></text>
<text class="clsl-3" transform="translate(459.48 376.1)"><tspan x="0" y="0">Opérateur public</tspan></text>
<text class="clsl-3" transform="translate(415.27 409.71)"><tspan x="0" y="0">Promoteur immobilier</tspan></text>
<text class="clsl-3" transform="translate(391.27 443.26)"><tspan x="0" y="0">Propriétaire privé particulier</tspan></text>
<text class="clsl-3" transform="translate(369.27 475.84)"><tspan x="0" y="0">Propriétaire privé institutionnel</tspan></text>
<text class="clsl-3" transform="translate(338.27 512.42)"><tspan x="0" y="0">SEM</tspan></text></svg>

<script>
    var data = [];
    if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"]){
        data = costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"];
    }
    if(data.length>0){
        $.each(data, function(index, d){
            try{
                $("#"+d["label"].replace(/[()/]|\s/g, "")).text(d["value"]+" %")
            }catch(e){

            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": ((typeof costum["dashboardGlobalConfig"] !="undefined")? costum["dashboardGlobalConfig"]["coformList"]:{}) || {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "answerPath" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.answerPath
                    },
                    "answerValue" : {
                        "inputType" : "selectMultiple",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Valeur répondu",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {},
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.answerValue
                    },
                    "chartType": {
                        "inputType" : "select",
                        "class":"select2 form-control",
                        "label" : "Type du chart",
                        "options":{"bar":"Bar", "doughnut":"Circulaire", "line":"Line", "pie":"Pie"}
                    },
                    "percentColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur du text de pourcentage"
                    },
                    "completeColor": {
                        "inputType" : "colorpicker",
                        "label" : "Couleur de la chart"
                    },
                    "showLegend" : {
                        "label" : "Afficher la légende",
                        "inputType" : "checkboxSimple",
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : true,
                        "values" :  sectionDyf.<?php echo $kunik?>ParamsData.showLegend
                    },
                    "legendPosition" :{
                        "inputType" : "select",
                        "label" : "Position des légendes",
                        "values" : sectionDyf.<?=$kunik ?>ParamsData.legendPosition,
                        "class" : "form-control",
                        "options" : {
                            "top" : "En Haut",
                            "bottom" : "En Bas",
                            "right" : "Droite",
                            "left" : "Gauche"
                        }
                    }
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform, function(){
                            if($("#answerPath.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.answerPath+"']").length > 0){
                                $("#answerPath.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerPath);
                                $("#answerPath.<?php echo $kunik ?>").change();
                                $("#answerValue.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.answerValue)
                            }
                        });
                    }
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k=="titleBottom"){
                            tplCtx.value[k] = $("#"+answerValue).val().toString();
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        
        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        $(document).on("change", "#answerPath.<?php echo $kunik ?>", function(){
            $("#answerValue.<?php echo $kunik ?>").empty();
            let coform = [];
            if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
                coform = costum["dashboardGlobalConfig"]["formTL"];
            }
            if(typeof coform[$("#coform.<?php echo $kunik ?>").val()] != "undefined" ){
                coform = coform[$("#coform.<?php echo $kunik ?>").val()];
            }
            let input = $(this).val().split(".")[1];
            if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
                if(typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                    for(const paramValue of coform["params"][input]["global"]["list"]){
                        $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                    }
                }
            }

            if(input.includes("checkboxNew") || input.includes("radioNew")){
                for(const paramValue of coform["params"][input]["list"]){
                    $("#answerValue.<?php echo $kunik ?>").append('<option value="'+paramValue+'" >'+paramValue+'</option>');
                }
            }
        });

        let updateInputList = function(value, callback=null){
            let childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
            $("#answerPath.<?php echo $kunik ?>").empty();
            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    //let isSelected = ()?"":""

                    if(input["type"].includes(".multiCheckboxPlus")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".multiRadio")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radiocplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radiocplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxcplx")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxcplx'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".checkboxNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.checkboxNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes(".radioNew")){
                        $("#answerPath.<?php echo $kunik ?>").append('<option value="'+stepKey+'.radioNew'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }

            if(callback!=null && typeof callback=="function"){
                callback();
            }
        }
    });
</script>
