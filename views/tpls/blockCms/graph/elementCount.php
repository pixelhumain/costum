<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
?>

<style type="text/css" id="css-<?= $kunik ?>">
    .text-green-theme<?= $kunik ?>{
        color: <?= $blockCms["css"]["elementCss"]["color"] ?>;
    }

    .effectif{
        padding: 0px ;
    }

    a{
        text-decoration: none !important;
    }
     <?php if(($blockCms["elementIcon"]=="")){ ?>
        h2.cedarville-number{
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            margin-top: 0px  !important;
            margin-bottom: 0px  !important;
        }
     <?php } ?>

</style>

<?php if($blockCms["design"] == "horizontalDesign"){ ?>
<style type="text/css" id="css-<?= $kunik ?>">   
    .dbox-icon<?= $kunik ?> {
        margin: auto;
        width: 60%;
        padding: 1.3em;
    }
    .dbox-icon<?= $kunik ?>:before {
        width: 95px;
        height: 95px;
        position: absolute;
        background: rgba(255,255,255, 1);
        content: '';
        border-radius: 50%;
        left: -17px;
        top: -17px;
        z-index: -2;
        clear: both;
        display: table;
    }
    .dbox-icon<?= $kunik ?>:after {
        width: 80px;
        height: 80px;
        position: absolute;
        background: <?= $blockCms["css"]["elementCss"]["backgroundColor"] ?>;
        content: '';
        border-radius: 50%;
        left: -10px;
        top: -10px;
        z-index: -1;
        clear: both;
        display: table;
    }
    .dbox-icon<?= $kunik ?> > i {
        background: <?= $blockCms["css"]["elementCss"]["backgroundColor"] ?>;
        border-radius: 50%;
        line-height: 40px;
        width: 60px;
        height: 60px;
        font-size:2em;
        padding: 15%;
        text-align: center;
        clear: both;
        display: table;
        margin: auto;
    }
</style>
<?php }else{ ?>
<style type="text/css" id="css-<?= $kunik ?>">
    .dbox<?= $kunik ?> {
        position: relative;
        background: <?= $blockCms["css"]["elementCss"]["backgroundColor"] ?>;
        border-radius: 4px;
        text-align: center;
    }
    .p-1{
        padding: .3em;
    }
    .dbox-body-<?= $kunik ?> {
        padding: <?= ($blockCms["elementIcon"]!="")?"10px 0px 40px 0px":"0px";?> !important;
    }
    .dbox-title-<?= $kunik ?> {
        font-size: 14px;
        color: #888;
    }
    .dbox-icon<?= $kunik ?> {
        position: absolute;
        transform: translateY(-50%) translateX(-50%);
        left: 50%;
    }
    .dbox-icon<?= $kunik ?>:before {
        width: 75px;
        height: 75px;
        position: absolute;
        background: rgba(255,255,255, 1);
        content: '';
        border-radius: 50%;
        left: -17px;
        top: -17px;
        z-index: -2;
    }
    .dbox-icon<?= $kunik ?>:after {
        width: 60px;
        height: 60px;
        position: absolute;
        background: <?= $blockCms["css"]["elementCss"]["backgroundColor"] ?>;
        content: '';
        border-radius: 50%;
        left: -10px;
        top: -10px;
        z-index: -1;
    }
    .dbox-icon<?= $kunik ?> > i {
        background: #FFF;
        border-radius: 50%;
        line-height: 40px;
        width: 40px;
        height: 40px;
        font-size:22px;
    }
    .dbox-action<?= $kunik ?> {
        transform: translateY(-50%) translateX(-50%);
        position: absolute;
        left: 50%;
    }
</style>
<?php } ?>

<div class="col-xs-12 effectif">
    <a class="lbh" href="#search?searchType=organization">
   		<div class="container-fluid">
   			<div class="row">
                <?php if($blockCms["design"] == "horizontalDesign"){ ?>
                    <?php if($blockCms["elementIcon"]!=""){ ?>
           				<div class="col-md-4">
                            <span class="dbox-icon<?= $kunik ?>">
                                <i class="fa elementCss text-green-theme<?= $kunik ?> fa-<?= $blockCms['elementIcon'] ?> dash-icon<?= $kunik ?>"></i>
                            </span>
           				</div>
                        <div class="col-md-1"></div>
                    <?php } ?>
   				<div class="col-md-<?= ($blockCms["elementIcon"]!="")?"7":"12";?> text-center dbox-body-<?= $kunik ?>">
                    <h1 data-value="<?= $blockCms["count"] ?>" class="elementCss text-green-theme<?= $kunik ?> cedarville-number"><?= $blockCms["count"] ?></h1>
                    <?php if($blockCms["countTitle"]!=""){ ?>
   					    <h6><?=Yii::t("common", $blockCms["countTitle"])?></h6>
                    <?php } ?>
   				</div>

                <?php }else{ ?>

                <div class="dbox<?= $kunik ?>">
                    <div class="dbox-body-<?= $kunik ?>">
                        <h2 data-value="<?= $blockCms["count"] ?>" class="elementCss text-green-theme<?= $kunik ?> cedarville-number"><?= $blockCms["count"] ?></h2>
                        <?php if($blockCms["countTitle"]!=""){ ?>
                        <h6 class="dbox-title-<?= $kunik ?>">
                            <?=Yii::t("common", $blockCms["countTitle"])?>
                        </h6>
                        <?php } ?>
                    </div>
                    <?php if($blockCms["elementIcon"]!=""){ ?>
                        <div class="dbox-action<?= $kunik ?>">
                            <a href="javascript:;" class="dbox-icon<?= $kunik ?>">
                                <i class="fa fa-<?= $blockCms['elementIcon'] ?> elementCss text-green-theme<?= $kunik ?>"></i>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
   			</div>
   		</div>
    </a>
</div>

<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>blockCms = <?php echo json_encode($blockCms); ?>;
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        sectionDyf.<?php echo $kunik ?>blockCms.elementIcon = "fa fa-"+sectionDyf.<?php echo $kunik ?>blockCms.elementIcon;
        $("#css-<?= $kunik ?>").append(str);
        if (costum.editMode) {
            cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
            var elementCountInput = {
                configTabs: {
                    general: {
                        inputsConfig: [
                            {
                                type : "inputSimple",
                                options : {
                                    name : "countTitle",
                                    label : tradCms.title,
                                    collection : "cms"
                                }
                            },
                            {
                                type: "inputIcon",
                                options: {
                                    name: "elementIcon",
                                    label: tradCms.selectIcon
                                }
                            },
                            {
                                type : "select",
                                options : {
                                    name : "elementType",
                                    label : tradCms.elementType,
                                    options : [
                                        {
                                            value : "<?= Organization::COLLECTION ?>", 
                                            label : trad.organizations
                                        },
                                        {
                                            value : "<?= Person::COLLECTION ?>", 
                                            label : tradCategory.citizen
                                        },
                                        {
                                            value : "<?= Event::COLLECTION ?>",
                                            label : trad.events
                                        },
                                        {
                                            value : "<?= Answer::COLLECTION ?>",
                                            label : trad.answers
                                        }
                                    ]
                                }
                            },
                            
                            {
                                type : "select",
                                options : {
                                    name : "design",
                                    label : tradCms.choosingDesign,
                                    options : [
                                        {
                                            value : "horizontalDesign", 
                                            label : tradCms.horizontalDesign
                                        },
                                        {
                                            value : "verticalDesign", 
                                            label : tradCms.verticalDesign
                                        }
                                    ]
                                }
                            },
                        ]
                    },
                    style: {
                        inputsConfig: [
                            {
                                type : "section",
                                options : {
                                    name : "elementCss",
                                    label : "style du texte",
                                    inputs : [
                                        "color",
                                        "backgroundColor"
                                    ]
                                }
                            }
                        ]
                    },
                    advanced: {
                        inputsConfig: [
                            "addCommonConfig"
                        ]
                    }
                },
                afterSave: function(path,valueToSet,name,payload,value) {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            } 
            cmsConstructor.blocks.elementCount<?= $myCmsId ?> = elementCountInput;
        }
</script>
