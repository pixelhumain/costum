<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style type="text/css" id="simplePie<?= $kunik ?>">
   .percentageText<?= $kunik ?>{
    fill: <?= $blockCms["css"]['percentColor']["color"] ?> !important;
   }

   .labelText{
    color: black;
    font-weight: bolder;
   }

   .percentageNext{
    line-height: 1;
   }
</style>

<div id="pie<?= $kunik; ?>" class="<?= (($blockCms['nextTo']=='') ? 'text-center' : '') ?> <?= $kunik ?>">
</div>

<script>
    jQuery(document).ready(function(){
        var data = [];
        var dataAnswers = <?php echo json_encode( $blockCms["dataAnswers"] ) ?>;
        if(dataAnswers && dataAnswers != null && dataAnswers != "undefined")
        {
            data = dataAnswers.nameCountArray;      
        }
        var coef = ((data && data.length >= 3)? 2 : 4);
        var width = document.getElementById("pie<?= $kunik; ?>").clientWidth,
        height = (width/2)+20,
        radius = 90,
        innerRadius = 30;

        if(data && data.length < 3){
            radius = 60;
            innerRadius = 0;
        }
        try{
            var arc = d3.arc()
            .outerRadius(radius - 10)
            .innerRadius(innerRadius);

            var pie = d3.pie()
                .sort(null)
                .value(function(d) {
                    return d.count;
                });

            var svg = d3.select('#pie<?= $kunik; ?>')
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate("+width/coef+","+height/2+")");

            var g = svg.selectAll(".arc")
            .data(pie(data))
            .enter().append("g");    

            g.append("path")
                .attr("d", arc)
            .style("fill", function(d,i) {
                return d.data.color;
            });

            
            if(data.length>2){
                g.append("text")
                .attr("transform", function(d) {
                var _d = arc.centroid(d);
                _d[0] *= 1; //multiply by a constant factor
                _d[1] *= 1; //multiply by a constant factor
                return "translate(" + _d + ")";
                })
                .attr("dy", ".50em")
                .attr('font-size', '1.8em')
                .attr("class", "percentageText<?= $kunik ?>")
                .style("text-anchor", "middle")
                .style("font-weight", "bolder")
                .text(function(d) {
                if(d.data.percentage < 8) {
                    return '';
                }
                return d.data.percentage + '%';
                });

            g.append("foreignObject")
                .attr("x", function(d){
                if(arc.centroid(d)[0]<0){
                    return arc.centroid(d)[0]*3.2;
                }else{
                    return arc.centroid(d)[0]*1.6;
                }
                })
                .attr("y", function(d){
                return arc.centroid(d)[1];
                })
                .attr("width", 100)
                .attr("height", 100)
                .attr("dy", ".50em")
                .style("text-transform", "uppercase")
                .style("text-anchor", "middle")
                .append("xhtml:div")
                .attr("class", "labelText")
                .html(function(d) {
                    return d.data.name + '';
                });

            
            }else{
            svg.append("foreignObject")
                .attr("x", ((width/5)-20))
                .attr("y", -height/3)
                .attr("width", (width - width/3))
                .attr("height", height)
                .attr("class", "percentColor")
                .append("xhtml:div")
                    //.style("color", "< ?= $blockCms["percentColor"] ?>ter")
                    .style("line-height", 1)
                    .style("margin-bottom", "3px")
                    .style("font-weight", "bolder")
                    .html("<span style='font-size:90pt'>"+(costum["dashboardData"]["<?= $blockKey ?>"]["nextTo"]||0)+"</span><sup style='font-size:50pt'>%</sup>");
            }
        }catch(ex){}
    });
</script>
<script type="text/javascript">

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#simplePie<?= $kunik ?>").append(str);

    if(costum.editMode)
    {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?php echo json_encode( $blockCms ) ?>;
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
        }
        var simplePie = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type: "select",
                            options: {
                                name: "coform",
                                label: tradCms.selectaform,
                                options: coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                label: tradCms.whichQuestionCorrespondstotheGraph,
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                label: tradCms.answervalue,
                                options: answerValueData
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "percentColor",
                                label: tradCms.colorofpercentagetext,
                                inputs: [
                                    "color"
                                ]
                            }
                        }
                    ]
                },
                advanced : {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
                    cmsConstructor.blocks.simplePie<?= $myCmsId ?>.configTabs.general.inputsConfig[1].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
                    cmsConstructor.blocks.simplePie<?= $myCmsId ?>.configTabs.general.inputsConfig[2].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.simplePie<?= $myCmsId ?> = simplePie;
    }

    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];
                
                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }
                if(input["type"]=="text"){
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }
    function getAnswerValue(coformId, value) {
        var coform = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coform = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coform[coformId] != "undefined" ){
            coform = coform[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
            if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        if(input.includes("checkboxNew") || input.includes("radioNew")){
            if(typeof conform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["list"]){
                for(const paramValue of coform["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        return answerValue;
    }
</script>