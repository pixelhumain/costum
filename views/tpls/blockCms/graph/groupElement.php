<?php
    $keyTpl = "groupElement";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
?>

<style id="<?= $kunik ?>">
    .flex-center{
        /*justify-content: center;*/
        display: flex;
        flex-wrap: wrap;
    }
    @media screen and (min-width: 992px) {
        .circle-content<?=$kunik?> {
            width: <?= 100/$blockCms["nombre"] ?>%;
        }
    }
    .d3jsTooltip<?= $kunik ?> {
        position: absolute;
        display: none;
        width: auto;
        height: auto;
        background: none repeat scroll 0 0 white;
        border: 0 none;
        border-radius: 8px 8px 8px 8px;
        box-shadow: -3px 3px 15px #888888;
        color: black;
        font: 12px sans-serif;
        padding: 5px;
        text-align: center;
    }
    .d3legende<?= $kunik ?>{
        width: 250px;
    }
</style>

<div class="<?= $kunik ?>">
    <?php 
        if(isset($blockCms["dataCount"]["percentageArray"]) && $blockCms["design"] == "pourcentageMultiple"){ ?>
            <div id="arc<?= $kunik ?>" class="row">
        <?php foreach ($blockCms["dataCount"]["percentageArray"] as $key => $value) { ?>
            <div id="<?= 'circle'.$kunik.$key ?>" data-label="<?= $value["label"] ??'' ?> <?=$kunik?>" class="col-md-3 col-sm-4 col-xs-12 text-center padding-bottom-10 <?= 'circle-content'.$kunik ?>"></div>
    <?php   }
        echo '</div>';
        }else{ ?>

        <div id="chart<?= $kunik ?>" style="
    display: flex;
    align-items: center;
    justify-content: center;
"></div>

    <?php    }
    ?>
</div>

<script>


    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#<?= $kunik ?>").append(str);
    var data = [];
    var dataAnswers = <?= json_encode( $blockCms["dataCount"] ) ?>;
   
    if(dataAnswers != null)
    {
        data = dataAnswers.percentageArray;
    }
    jQuery(document).ready(function() {
        var progressCircle<?= $kunik ?> = {
            width: 120,
            height: 120,
            pi : 2 * Math.PI,
            data: {},
            arc : null,
            parent:null,
            init: function(procir, DOMselector){
                procir.arc = d3.arc()
                    .innerRadius(45)
                    .outerRadius(55) // arc width
                    .startAngle(0);
                d3.select(DOMselector).empty();
                procir.parent = d3.select(DOMselector).append("svg")
                    .attr("width", procir.width)
                    .attr("height", procir.height);

                procir.draw(procir, procir.data);
            },
            initData:function(procir, d){
                procir.data.id = d.label+"<?=$kunik ?>";
                procir.data.percent = d.value||0;
            },
            draw: function(procir, data){
                var d3Ring = procir.parent  
                    .append("g")
                    .attr("transform", "translate("+ procir.width/2 +"," +60+")")
                    .attr("id", data.id);
            
                // Background
                d3Ring
                    .append("path")
                    .datum({endAngle: procir.pi})
                    .attr("class", "empty")
                    .attr("d", procir.arc); 
                
                // Foreground
                var foreground = d3Ring
                    .append("path")
                    .datum({endAngle: 0})
                        .style("stroke", "none")
                        .style("stroke-width", "0px")
                        .style("opacity", 1)
                    .attr("class", "percent")
                    .attr("d", procir.arc);
                
                // Text
                d3Ring.empty();
                d3Ring
                    .append("text")
                    // .attr("x", "-20%")
                    // .attr("y", 8) 
                    .attr("class", "text")
                    .style("font-size", "25px")
                    .style("font-family", "Georgia, Arial, sans-serif !important")
                    .style("font-weight", "bolder")
                    .style("dominant-baseline", "middle")
                    .style("text-anchor", "middle")
                    .text(data.percent + "%");
                //alert(data.percent);
                var angle = procir.helpers.convertPercentToAngle(procir);
                
                // Animation
                foreground
                .transition()
                .duration(1500)
                .delay(500)
                .call(procir.arcTween, procir, angle);
            },
            arcTween: function(transition, procir, newAngle){
                transition.attrTween("d", function(d) {  
                    var interpolate = d3.interpolate(d.endAngle, newAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return procir.arc(d);
                    };
                });
            },
            helpers:{
                convertPercentToAngle: function(procir){
                    return ( procir.data.percent / 100 ) * procir.pi
                }
            }
        }

        if(typeof data != "undefined" && data.length>0){
            $("#tableData<?=$kunik?>").empty();
            if("<?= $blockCms["design"] ?>" == "pourcentageMultiple"){
                $.each(data, function(index, value){
                    var procircle = '*[data-label="'+value.label+' <?=$kunik?>"';
                    progressCircle<?= $kunik ?>.initData(progressCircle<?= $kunik ?>, value);
                    progressCircle<?= $kunik ?>.init(progressCircle<?= $kunik ?>, procircle);
                    $(procircle).append('<div class="text-center label-text-color textColor" title="'+value.label+' ( '+value.value+' )" style="height:55px; overflow:hidden">'+value.label+'</div>');
                    $("#tableData<?=$kunik?>").append(`<tr>
                        <td>${value.label}</td>
                        <td class="tableValue">${value.value}</td>
                    </tr>`)
                })
            }else if("<?= $blockCms["design"] ?>" == "pie"){
               var tooltip = d3.select("#chart<?= $kunik ?>").append("div").attr("class", "d3jsTooltip<?= $kunik ?>");
               //Width and height
                var width = 400;
                var height = 400;
                var outerRadius = width / 2;
                var innerRadius = 0;
                // Easy colors accessible via a 10-step ordinal scale
                var color = <?= json_encode($blockCms["config"]) ?>;
                color = color.map(function(element){
                    return [element.label, element.color];
                })
                if(color.length > 0){
                    color = Object.fromEntries(color);
                }else{
                    color = d3.scaleOrdinal().range(d3.schemeSet3);
                }
                var svg = d3.select("#chart<?= $kunik ?>")
                    .append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .append("g")
                    .attr("transform", `translate(${width/2}, ${height/2})`);
                    const pie = d3.pie()
                        .value(function(d) {return d.value; })

                    const dataReady = pie(data);
                    svg.selectAll("path")
                        .data(dataReady)
                        .join('path')
                        .on("mousemove", function(d, i){
                            d3.select(this).attr('fill-opacity', 1).attr("stroke", "white")
                                .style("stroke-width", "3px");
                            tooltip.style("left", d.layerX+10+"px");
                            tooltip.style("top", d.layerY-25+"px");
                            tooltip.style("display", "inline-block");
                            tooltip.html(`<span>${i.data.label}</span><br><div style='margin-top: 10px; display: flex; align-items: center; justify-content: center;'><label style="width: 15px; height: 15px; margin-bottom: 0; margin-right: 5px; background-color: ${typeof color == "function" ? color(i.data.label) : color[i.data.label]}"></label><span>${typeof dataAnswers[i.data.label] != "undefined" ? dataAnswers[i.data.label] : i.data.value}</span></div>`)
                        })
                        .on("mouseout", function(d){
                            d3.select(this).attr('fill-opacity', "0.8").attr("stroke", "none").style("stroke-width", "0px");
                            tooltip.style("display", "none");
                        })
                        .transition()
                        .duration(1000)
                        .attr('d', d3.arc()
                            .innerRadius(0)
                            .outerRadius(outerRadius)
                        )
                        .attr('fill', function(d){ return (typeof color == "function" ? color(d.data.label) : color[d.data.label]) })
                        .attr('fill-opacity', "0.8")
                        // .attr("stroke", "white")
                        .style("stroke-width", "2px")
                        .style("opacity", 1);

                var legende = d3.select("#chart<?= $kunik ?>").append("svg").attr("width", "20%").style("margin-left", "20px").attr("height", 20+(25*dataReady.length)).attr("class", "d3legende<?= $kunik ?>");
                legende.selectAll("legende")
                    .data(dataReady)
                    .join((enter) => {
                        let g = enter;
                        g = g.append("g").attr("height", "20px").attr("width", "100%");
                        g.append("circle")
                            .attr("cx", 20)
                            .attr("cy", function(d,i){ return 20 + i*25}) // 20 is where the first dot appears. 25 is the distance between dots
                            .attr("r", 10)
                            .style("fill", function(d){ return typeof color == "function" ? color(d.data.label) : color[d.data.label];})
                            .style("stroke", function(d){ return typeof color == "function" ? color(d.data.label) : color[d.data.label];})
                        g.append("text")
                            .attr("x", 40)
                            .attr("y", function(d,i){ return 20 + i*25}) // 20 is where the first dot appears. 25 is the distance between dots
                            .style("fill", "black")
                            .text(function(d){ return d.data.value+' %'})
                            .attr("text-anchor", "left")
                            .style("alignment-baseline", "middle")
                        g.append("text")
                            .attr("x", 90)
                            .attr("y", function(d,i){ return 20 + i*25}) // 20 is where the first dot appears. 25 is the distance between dots
                            .style("fill", "black")
                            .text(function(d){ return d.data.label})
                            .attr("text-anchor", "left")
                            .style("alignment-baseline", "middle")
                        return g;
                    })
            }
        }
        $("#tableData<?=$kunik?>").hide();
    });
    if(costum.editMode){
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
        }
        var groupElement = {
            configTabs: {
                general : {
                    inputsConfig : [
                        {
                            type: "select",
                            options: {
                                name: "choice",
                                label: "Type d'éléments",
                                options: [
                                    {
                                        value: "coform", label: "Formulaire"
                                    },
                                    {
                                        value: "elements", label: 'Eléments'
                                    }
                                ]
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "design",
                                label: "Type d'affichage",
                                options: [
                                    {
                                        value: "pourcentageMultiple", label: "Pourcentage multiple"
                                    },
                                    {
                                        value: "pie", label: '<?= Yii::t("graph", "Pie Chart") ?>'
                                    }
                                ]
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "coform",
                                class: "hidden",
                                label: tradCms.selectaform ?? "Selectionner un formulaire",
                                options: coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                class: "hidden",
                                label: tradCms.whichQuestionCorrespondstotheGraph ?? " À Quelle Question corresponds la graph ?",
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                class: "hidden",
                                label: tradCms.answervalue ??  "Valeur répondu",
                                options: answerValueData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "type",
                                class: "",
                                label: "Elements",
                                options: [
                                    {value : "organizations", label: trad.organizations},
                                    {value : "citoyens" , label: tradCategory.citizen},
                                    {value : "events" , label: trad.events},
                                    {value : "projects" , label: trad.projects},
                                    {value : "poi" , label: trad.poi}
                                ]
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "cible",
                                class: "",
                                label: "Cible pour le groupage",
                                options: Object.keys((costum.lists||{})).map(function(element){
                                    return {label: element, value: element}
                                })
                            }
                        },
                        {
                            type: "inputMultiple",
                            options: {
                                label: tradCms.preferencelegende,
                                name: "config",
                                class: `hidden`,
                                inputs: [
                                    [
                                        "color",
                                        {
                                            type: "inputSimple",
                                            options: {
                                                label: tradCms.label,
                                                name: "label",
                                            }
                                        }
                                    ]
                                ]
                            }
                        }
                        /* {
                            type: "groupButtons",
                            options: {
                                "name": "activeContour",
                                "label" : tradCms.showzoneoutline,
                                options: [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ]
                            }
                        } */
                    ]
                },
                style: {
                    inputsConfig: [
                        "padding",
                        {
                            type: "section",
                            options: {
                                name: "text",
                                label: tradCms.colorofpercentagetext ?? "Couleur de la pourcentage",
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "empty",
                                label: tradCms.colorofemptyarc ?? "Couleur de l'arc vide",
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "percent",
                                label: tradCms.filledarccolor ?? "Couleur de l'arc rempli",
                                inputs:[
                                    "fill"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "target",
                                label: "Target"
                            }
                        }
                    ]
                }
            },
            beforeLoad: function(){
                if(cmsConstructor.sp_params[cmsConstructor.spId].choice == "elements"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[2].options.class = "hidden";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.class = "hidden";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.class = "hidden";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[5].options.class = "";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[6].options.class = "";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[7].options.class = "";
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[2].options.class = "";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.class = "";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[4].options.class = "";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[5].options.class = "hidden";
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[6].options.class = "hidden";
                    if(cmsConstructor.sp_params[cmsConstructor.spId].answerValue.length > 1){
                        cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[7].options.class = "";
                    }else{
                        cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[7].options.class = "hidden";
                    }
                }

                if(cmsConstructor.sp_params[cmsConstructor.spId].design != "pourcentageMultiple"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[7].options.class = "";
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[7].options.class = "hidden";
                }

            },
            onChange: function(path, valueToSet, name, payload, value){
            },
            afterSave: function(path, valueToSet, name, payload, value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                if(name == "design" || name == "choice"){
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
                    cmsConstructor.blocks["<?= $kunik ?>"].configTabs.general.inputsConfig[3].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
                    cmsConstructor.blocks["<?= $kunik ?>"].configTabs.general.inputsConfig[4].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerValue"){
                    var legendeConfig = value.map(function(element){
                        return {label: element, color: `#${Math.floor(Math.random()*16777215).toString(16)}`}
                    });
                    var data = {
                        id: cmsConstructor.spId,
                        collection: "cms",
                        path: "config",
                        value: legendeConfig
                    };
                    dataHelper.path2Value( data, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            cmsConstructor.sp_params[cmsConstructor.spId].config = legendeConfig;
                            $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                        });
                    });
                }
                if(name == "cible"){
                    if(typeof costum.lists[value] != "undefined"){
                        var legendeConfig = Object.values((costum.lists[value]||{})).map(function(element){
                            return {label: element, color: `#${Math.floor(Math.random()*16777215).toString(16)}`}
                        });
                        var data = {
                            id: cmsConstructor.spId,
                            collection: "cms",
                            path: "config",
                            value: legendeConfig
                        };
                    }
                }
            }
        }
        cmsConstructor.blocks['<?= $kunik ?>'] = groupElement;
    }
    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];
                
                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }
                if(input["type"]=="text"){
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }
    function getAnswerValue(coformId, value) {
        var coform = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coform = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coform[coformId] != "undefined" ){
            coform = coform[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
            if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        if(input.includes("checkboxNew") || input.includes("radioNew")){
            // if(coform != [] && coform != null)
            // {
                if(typeof conform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["list"]){
                    for(const paramValue of coform["params"][input]["list"]){
                        answerValue.push({value: paramValue, label: paramValue})
                    }
                // }
            }
        }
        return answerValue;
    }
</script>