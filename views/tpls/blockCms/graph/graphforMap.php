<?php
$keyTpl = "graphforMap";
$myCmsId  = $blockCms["_id"]->{'$id'};
?>

<style>
    #graphD3{
        position: relative;
    }
    #graphformap<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container {
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
</style>
<div id="app-map-filters" class="searchObjCSS"></div>
<div id="graphD3">
    <div id="graphformap<?= $kunik ?>">
    
    </div>
</div>
<script>
    var appMap = null;
    var legende = <?= gettype($blockCms["legende"]["config"]) == "array" ? json_encode($blockCms["legende"]["config"]) : $blockCms["legende"]["config"]?>;
    $(function(){
        var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
        var mapOptions = {
            container : "#graphformap<?= $kunik ?>",
            activePopUp : true,
            clusterType : '<?= $blockCms["marker"]["cluster"] ?>',
            markerType : '<?= $blockCms["marker"]["marker"] ?>',
            showLegende: '<?= $blockCms["legende"]["show"] ?>',
            dynamicLegende: '<?= $blockCms["legende"]["dynamic"] ?>',
            mapOpt:{
                zoomControl:false
            },
            mapCustom:customMap,
            elts : {}
        };
        if(<?= json_encode($blockCms["legende"]["dynamic"]) ?>){
            mapOptions.groupBy = '<?= $blockCms["legende"]["var"] ?>';
            mapOptions.legendeLabel = '<?= $blockCms["legende"]["label"] ?>';
        }else{
            if("<?= $blockCms["legende"]["cible"] ?>" != "autre" && "<?= $blockCms["legende"]["cible"] ?>" != ""){
                mapOptions.legende = costum.lists["<?= $blockCms["legende"]["cible"] ?>"];
            }else{
                mapOptions.legende = legende.map(function(d){
                    return d.label;
                });
            }
            mapOptions.groupBy = '<?= $blockCms["legende"]["var"] ?>';
            mapOptions.legendeVar = '<?= $blockCms["legende"]["var"] ?>';
            mapOptions.legendeLabel = '<?= $blockCms["legende"]["label"] ?>';
        }
        appMap = new MapD3(mapOptions);
        if(<?= json_encode($blockCms["filters"]["activate"]) ?>){
            var filterUser = JSON.parse(JSON.stringify(<?= str_replace("\"", "'", $blockCms["filters"]["config"])?>));
            if(typeof filterUser.filters != "undefined"){
                filterUser.mapCo = appMap;
                filterUser.container = "#app-map-filters";
                filterUser.layoutDirection = "vertical";
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }else{
                filterUser = {
                    "options" : {
                        "tags" : {
                            "verb" : "$and"
                        }
                    },
                    "results" : {
                        "renderView" : "directory.elementPanelHtml",
                        "smartGrid" : true,
                        "map" : {
                            "active" : true
                        }
                    },
                    "defaults" : {
                        "indexStep" : 0,
                        "types" : [ 
                            "organizations"
                        ]
                    },
                    "filters" : filterUser,
                    container:"#app-map-filters",
                    layoutDirection:"vertical",
                    mapCo:appMap,
                    results:{
                        multiCols:false,
                        map:{
                            active:true
                        }
                    },
                    mapContent:{
                        hideViews: ["btnHideMap"]
                    }
                };
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }
        }else{
            var url = '<?= $blockCms['linkData'] ?>';
            var params = {};
            if(url.indexOf("globalautocomplete") > -1){
                var zones = <?php echo json_encode( $blockCms["zone"]['zones'] ); ?>;
                var defaultFilters<?= $kunik ?> = {};
                var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
                params = {
                    // notSourceKey: false,
                    searchType : <?= json_encode(['organizations']) ?>,
                    fields : mapSearchFields,
                    filters: defaultFilters<?= $kunik ?>,
                    indexStep: 0
                };
                if(zones){
                    params["locality"] = zones;
                    params["activeContour"] = true
                }
                if('<?= $blockCms['legende']['var'] ?>' == 'tags'){
                    params["options"] = {
                        'tags' : {
                            'verb': '$all'
                        }
                    };
                }
            }
            ajaxPost(
                null,
                (url.indexOf("http") > -1 ? url : baseUrl + "/" + url),
                params,
                function(data){
                    var dataToSend = data.results ? data.results : data;
                    if(typeof data.zones != "undefined"){
                        Object.assign(dataToSend, data.zones);
                    }
                    appMap.clearMap();
                    appMap.addElts(dataToSend);
                    appMap.getMap().invalidateSize()
                    appMap.fitBounds();
                }
            )
        }
    }); 
</script>
<script>
    if(costum.editMode) {
        var allzone = <?= json_encode($zones) ?>;
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        var zone = <?= json_encode($blockCms["zone"]["zones"]) ?>;
        var mapD3Input = {
            configTabs: {
                general : {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "marker",
                                label: "Marquer",
                                inputs: [
                                    {
                                        type: "select",
                                        options: {
                                            name : "marker",
                                            label: "<?= Yii::t("graph", "Type of marker") ?>",
                                            options: [
                                                {value: "default", label: "<?= Yii::t("graph", "Default") ?>"},
                                                {value: "pie", label: "<?= Yii::t("graph", "Pie Chart") ?>"},
                                                {value: "bar", label: "<?= Yii::t("graph", "Bar Chart") ?>"},
                                                {value: "donut", label: "<?= Yii::t('graph', "Donut Chart") ?>"},
                                                {value: "geoshape", label: "<?= Yii::t("graph", "Geoshape") ?>"},
                                                {value: "heatmap", label: "<?= Yii::t("graph", "Heatmap") ?>"}
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name : "cluster",
                                            label: "<?= Yii::t("graph", "Cluster type") ?>",
                                            options: [
                                                {value: "pie", label: "<?= Yii::t("graph", "Pie Chart") ?>"},
                                                {value: "bar", label: "<?= Yii::t("graph", "Bar Chart") ?>"},
                                                {value: "donut",label: "<?= Yii::t('graph', "Donut Chart") ?>"}
                                            ]
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "legende",
                                label: tradCms.legend,
                                inputs: [
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "show",
                                            "label" : tradCms.showlegende,
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "dynamic",
                                            "label" : "<?= Yii::t('graph', 'Automatic legend') ?>",
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "var",
                                            label: "<?= Yii::t('graph', 'Legend variable on the data') ?>"
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            name: "cible",
                                            class: `legendecible-${cmsConstructor.kunik}`,
                                            label: "<?= Yii::t("graph", 'Legend on') ?>",
                                            options: Object.keys((costum.lists||{})).map(function(element){
                                                return {label: element, value: element}
                                            })
                                        }
                                    },
                                    // {
                                    //     type: "inputMultiple",
                                    //     options: {
                                    //         label: tradCms.preferencelegende,
                                    //         name: "config",
                                    //         class: `legendeconfig-${cmsConstructor.kunik}`,
                                    //         inputs: [
                                    //             [
                                    //                 {
                                    //                     type: "inputSimple",
                                    //                     options: {
                                    //                         label: tradCms.label,
                                    //                         name: "label",
                                    //                     }
                                    //                 }
                                    //             ]
                                    //         ]
                                    //     }
                                    // },
                                    {
                                        type: "inputSimple",
                                        options: {
                                            name: "label",
                                            label: "<?= Yii::t("graph", "Name of the legend")?>"
                                        }
                                    },
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "filters",
                                label: tradCms.filtr,
                                inputs: [
                                    {
                                        type: "groupButtons",
                                        options: {
                                            "name": "activate",
                                            "label" : "<?= Yii::t("graph", "Use the filter") ?>",
                                            options: [
                                                {
                                                    value : false,
                                                    label : trad.no
                                                },
                                                {
                                                    value : true,
                                                    label : trad.yes
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        type: "textarea",
                                        options: {
                                            name: "config",
                                            label: "<?= Yii::t("graph", "Filter configuration") ?>"
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "zone",
                                label: "Zone",
                                inputs: [
                                    {
                                        type: "select",
                                        options: {
                                            "name": "level",
                                            "label" : "<?= Yii::t("graph","Zone level") ?>",
                                            options: [
                                                {
                                                    value : "2",
                                                    label : "Niveau 2"
                                                },
                                                {
                                                    value : "3",
                                                    label : "Niveau 3"
                                                },
                                                {
                                                    value : "4",
                                                    label : "Niveau 4"
                                                },
                                                {
                                                    value : "5",
                                                    label : "Niveau 5"
                                                },
                                                /* {
                                                    value : "6",
                                                    label : "Niveau 6"
                                                }, */
                                            ]
                                        }
                                    },
                                    {
                                        type: "select",
                                        options: {
                                            isSelect2: true,
                                            name: "country",
                                            label: "<?= Yii::t("graph", "Country") ?>",
                                            options: Object.values(<?= json_encode($country) ?>).map(function(element){
                                                return {label: element.name, value: element.countryCode};
                                            })
                                        }
                                    },
                                    {
                                        type: "selectMultiple",
                                        options: {
                                            name: "zones",
                                            label: "<?= Yii::t("graph", "Zone") ?>",
                                            options: Object.keys(allzone).map(function(element){
                                                return {label: allzone[element].name, value: element};
                                            }),
                                            defaultValue: Object.values(zone).map(function(element){
                                                return element.id
                                            })
                                        }
                                    }
                                ]
                            }
                        },
                    ],
                    processValue: {
                        zones: function(value){
                            var data = {};
                            value.forEach(element => {
                                data[element+"level"+allzone[element].level[0]] = {
                                    name: allzone[element].name,
                                    active: true,
                                    id: element,
                                    countryCode: allzone[element].countryCode,
                                    level: allzone[element].level[0],
                                    type: "level"+allzone[element].level[0],
                                    key: element+'level'+allzone[element].level[0]
                                }
                            });
                            return data;
                        }
                    }
                }
            },
            beforeLoad: function(){
                if(cmsConstructor.sp_params[cmsConstructor.spId].legende.dynamic){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[1].options.inputs[4].options.class = `legendeconfig-${cmsConstructor.kunik} hidden`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[1].options.inputs[3].options.class = `legendecible-${cmsConstructor.kunik} hidden`
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[1].options.inputs[3].options.class = `legendecible-${cmsConstructor.kunik}`
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[1].options.inputs[4].options.class = `legendeconfig-${cmsConstructor.kunik}`
                }
                if(cmsConstructor.sp_params[cmsConstructor.spId].filters.activate){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[2].options.inputs[1].options.class = `filtersconfig-${cmsConstructor.kunik}`
                }else{
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[2].options.inputs[1].options.class = `filtersconfig-${cmsConstructor.kunik} hidden`
                }
            },
            onChange: function(path, valueToSet, name, payload, value){
                
            },
            afterSave: function(path, valueToSet, name, payload, value){
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                if(name == "country"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "level"){
                    cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                // if(name == "zones"){
                //     var data = {};
                //     value.forEach(element => {
                //         data[element+"level"+allzone[element].level[0]] = {
                //             name: allzone[element].name,
                //             active: true,
                //             id: element,
                //             countryCode: allzone[element].countryCode,
                //             level: allzone[element].level[0],
                //             type: "level"+allzone[element].level[0],
                //             key: element+'level'+allzone[element].level[0]
                //         }
                //     });
                //     var tplCtx = {
                //         id: cmsConstructor.spId,
                //         path: "zone.zones",
                //         collection: "cms",
                //         value: data
                //     }
                //     dataHelper.path2Value(tplCtx, function(){
                //         cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
                //         cmsConstructor.blocks['<?= $kunik ?>'].configTabs.general.inputsConfig[3].options.showInDefault = true;
                //         $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                //     })
                // }
            }
        }

        cmsConstructor.blocks['<?= $kunik ?>'] = mapD3Input;
    }
</script>