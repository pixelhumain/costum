<?php

    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
    $keyTpl = "chartForE";

    // Get project owner's logo
    $csvData = Document::getListDocumentsWhere(
        array(
        "id"=> $blockKey,
        "type"=>'cms',
        ), "csv"
    );

    $csvChartData = [];

    foreach ($csvData as $key => $value){
        array_push($csvChartData, array('folder' => $value["folder"], 'moduleId' => $value["moduleId"],'name' => $value["name"]));
    }

if(count($csvChartData)>0){
    foreach ($csvChartData as $key => $f) {
        $file = fopen("upload/".$f["moduleId"]."/".$f["folder"]."/".$f["name"], 'r');
        if($file){
            $blockCms["chartDataManual"] = [];
        }
        while($line=fgetcsv($file)){
            if($line[0]!="" && $line[1]!=""){
                array_push($blockCms["chartDataManual"], 
                array('label'=>$line[0], 'value' =>$line[1], 'color' =>$line[2]));
            }
        }
        fclose($file);
    }
}
?>

<style media="screen">
  #chartContainer<?=$kunik?>{
    position: relative;
    width:<?= $blockCms['css']['chartBlockCss']['width'] ?>;
    padding: 3%;
    height: <?= $blockCms['css']['chartBlockCss']['height'] ?>;
    background: <?= $blockCms['css']['chartBlockCss']['backgroundColor'] ?>;
    border-bottom-left-radius :	<?=$blockCms["css"]["chartBlockCss"]["borderBottomLeftRadius"]?>;
    border-bottom-right-radius : <?=$blockCms["css"]["chartBlockCss"]["borderBottomRightRadius"]?>;
    border-top-left-radius : <?=$blockCms["css"]["chartBlockCss"]["borderTopLeftRadius"]?>;
    border-top-right-radius : <?=$blockCms["css"]["chartBlockCss"]["borderTopRightRadius"]?>;
    justify-content: center !important;
  }
</style>
<center>
  <div id="chartContainer<?=$kunik?>" class="chartviz">
      <canvas id="chart<?=$kunik?>" width="900"></canvas>
  </div>
  <table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz"></table>

</center>

<script src="/plugins/Chart-2.8.0/Chart.min.js"></script>
<script type="text/javascript">
    var ctx<?= $kunik ?> = document.getElementById("chart<?=$kunik?>").getContext("2d"); // Canvas context
    var data<?= $kunik ?> = {}; // Data for chart
    var chartData<?= $kunik ?> = {}; // Data for chart
    var dataToShow<?= $kunik ?> = <?php echo json_encode( $blockCms["chartDataToShow"] ); ?>; // Data for chart
    var fields<?= $kunik ?> = {}; // Generic fiels to fetch
    var selectedField<?= $kunik ?> = "<?= (is_array($blockCms["chartDataFields"]))?$blockCms["chartDataFields"][0]:$blockCms["chartDataFields"] ?>"; // field to make statistic

    var chartBgColor<?= $kunik ?> = []; // Generic background color
    var chartBrColor<?= $kunik ?> = []; // Generic border color

    sectionDyf.<?=$kunik ?>blockCms = <?php echo json_encode( $blockCms ); ?>;


    /**
     * liste des fields
     * */
    let extraFields = [];

    if(typeof jsonHelper.getValueByPath(costum, "typeObj.organizations.dynFormCostum.beforeBuild.properties")!="undefined"){
        extraFields = costum.typeObj.organizations.dynFormCostum.beforeBuild.properties;
    }

    Object.keys(extraFields).forEach(function(keyValue, k) {
        let keyV = keyValue;
        let keyL = "";

        if(keyValue.indexOf("-")!=-1){
            keyV = keyValue.replace("-", ".");

        }else if(keyValue.indexOf("[")!=-1){
            keyV = keyValue.replace("[", ".").replace("]", "");
        }

        if(typeof extraFields[keyValue] != "undefined" && typeof extraFields[keyValue].label != "undefined"){
            keyL = extraFields[keyValue].label
        }else{
            keyL = keyV;
        }

        fields<?= $kunik ?>[keyV] = keyL;
    });

    fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
    fields<?= $kunik ?>["type"] = trad.type;
    fields<?= $kunik ?>["address.level1Name"] = "Par Pays";
    fields<?= $kunik ?>["address.level3Name"] = "Par Région";

    if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
        fields<?= $kunik ?>["tags"] = trad.tags;
    }

    var elementTypes<?= $kunik ?> = <?php echo json_encode($blockCms['chartDataElementTypes'])?>;


    if(typeof elementTypes<?= $kunik ?> == "string"){
        elementTypes<?= $kunik ?> = [elementTypes<?= $kunik ?>];
    }

    fields<?= $kunik ?>["address.level1Name"] = "Par Pays";
    fields<?= $kunik ?>["address.level3Name"] = "Par Région";

    if(elementTypes<?= $kunik ?>.includes("organizations")){
        fields<?= $kunik ?>["type"] = trad.type;
    }

    if(elementTypes<?= $kunik ?>.includes("organizations")){
        fields<?= $kunik ?>["type"] = trad.type;
    }

    /*** DEFAULT SEARCH PARAMS *****/

    let defaultFilters<?= $kunik ?> = {'$or':{}, "toBeValidated":{'$exists':false}};
    defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
    defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.slug;
    defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    if(costum.slug=="meir"){
        defaultFilters<?= $kunik ?>["category"] = "acteurMeir";
    }

    var searchParams<?= $kunik ?> = {
        loadEvent: {
            default:"dashboard"
        },
        defaults: {
            notSourceKey:true,
            types : elementTypes<?= $kunik ?>,
            fields:{...fields<?= $kunik ?>, "tags":"tags", "name":"name"},
            indexStep : 90000,
            filters:defaultFilters<?= $kunik ?>
        },
        results:{
          dom:"#chart<?=$kunik?>"
        }
    }

    coInterface.showLoader("#chart<?=$kunik?>");
    
    var search<?= $kunik ?> = {};
    var chart<?= $kunik ?>;

    jQuery(document).ready(function() {
        search<?= $kunik ?> = searchObj.init(searchParams<?= $kunik ?>);

        /*** override successComplete graph funct***/
        search<?= $kunik ?>.dashboard.successComplete = function(fObj, response){
          var chartData<?= $kunik ?> = []; // Data for chart
          data<?= $kunik ?> = [];
          
          var manualData = sectionDyf.<?php echo $kunik?>blockCms.chartDataManual;
          if((!Array.isArray(manualData) || (Array.isArray(manualData) && manualData.length!=0)) && sectionDyf.<?= $kunik ?>blockCms.chartDataUse=="true"){
            
            $.each(sectionDyf.<?php echo $kunik?>blockCms.chartDataManual, function(index, manData){
                chartData<?= $kunik ?>[manData.label] = parseInt(manData.value)
                chartBgColor<?= $kunik ?>.push(manData.color)
                chartBrColor<?= $kunik ?>.push(manData.color)
            })
        }else{
            for (var [key, value] of Object.entries(response.results)) {

                let valueFromPath = jsonHelper.getValueByPath(value, selectedField<?= $kunik ?>);

                if(typeof valueFromPath=="undefined"){
                    if(typeof costum.lists != "undefined" && costum.lists[selectedField<?= $kunik ?>]){
                        if(Array.isArray(costum.lists[selectedField<?= $kunik ?>])){
                            valueFromPath = costum.lists[selectedField<?= $kunik ?>].filter(function (x) {
                                if(value.tags){
                                    return value.tags.includes(x)
                                }
                                return false
                            })
                        }else if(typeof costum.lists[selectedField<?= $kunik ?>] == "object" && notNull(costum.lists[selectedField<?= $kunik ?>])){
                            valueFromPath = Object.keys(costum.lists[selectedField<?= $kunik ?>]).filter(function (x) {
                            return value.tags.includes(x) })
                        }
                    }
                }

                if(typeof valueFromPath !="undefined"){
                    let translatedValue = valueFromPath;

                    if(typeof translatedValue!="undefined" && notNull(translatedValue) && translatedValue!=""){
                        if(typeof(translatedValue)=="string"){
                            translatedValue = [translatedValue]
                        }

                        let labely = "";

                        translatedValue.forEach(function(label, index){
                            labely = label.toLowerCase();
                            if(typeof trad[label]!="undefined"){
                                labely = trad[label].toLowerCase();
                            }
                            if(typeof tradCategory[label]!="undefined"){
                                labely = tradCategory[label].toLowerCase();
                            }
                            if(typeof tradDynForm[label]!="undefined"){
                                labely = tradDynForm[label].toLowerCase();
                            }

                            //labels<?= $kunik ?>[translatedValue];
                            if(typeof data<?= $kunik ?>[labely] != "undefined"){
                                data<?= $kunik ?>[labely] += 1;
                                chartData<?= $kunik ?>[labely] += 1;
                            }else{
                                data<?= $kunik ?>[labely] = 1;
                                chartData<?= $kunik ?>[labely] = 1;
                            }

                            if(dataToShow<?= $kunik ?>.length!=0 && !dataToShow<?= $kunik ?>.includes(labely)){
                                delete chartData<?= $kunik ?>[labely];
                            }
                        });
                    }
                }
            }

            if(costum.lists && costum.lists["<?= $blockCms['chartBlockType']; ?>Color"]){
                chartBgColor<?= $kunik ?> = costum.lists["<?= $blockCms['chartBlockType']; ?>Color"];
                chartBrColor<?= $kunik ?> = costum.lists["<?= $blockCms['chartBlockType']; ?>Color"];
            }else{
                // Générate chart background and border color
                Object.keys(data<?= $kunik ?>).forEach(function(val, index){
                    let one, two, three = 0
                    if(index%2==0){
                        one = 255 - index*23;
                        two = 25 + index*23;
                        three = 130-index*23;
                    }else{
                        one = 75 + index*23;
                        two = 160-index*23;
                        three = 180-index*23;
                    }

                    chartBgColor<?= $kunik ?>.push('rgba('+(one - index*26)+', '+(two+index*9)+', '+(three+index*39)+', 1)');
                    chartBrColor<?= $kunik ?>.push('rgba('+(one - index*26)+', '+(two+index*9)+', '+(three+index*39)+', 1)');
                });
            }
            
            if("<?= $blockCms['chartDataColorGradient']; ?>"=="true"){
                let bgColors = (typeof chartBgColor<?= $kunik ?>=="string")?[chartBgColor<?= $kunik ?>]:chartBgColor<?= $kunik ?>;
                let brColors = (typeof chartBrColor<?= $kunik ?>=="string")?[chartBrColor<?= $kunik ?>]:chartBrColor<?= $kunik ?>;
                chartBgColor<?= $kunik ?> = [];
                chartBrColor<?= $kunik ?> = [];
                for(var index in bgColors){
                    let gradient = ctx<?= $kunik ?>.createLinearGradient(0, 0, 0, 260);
                    try {
                      gradient.addColorStop(1, bgColors[index]);
                      gradient.addColorStop(0, "white");
                    } catch (e) {
                      gradient = bgColors[index];
                    }

                    // border color
                    let brGradient = ctx<?= $kunik ?>.createLinearGradient(0, 0, 0, 260);

                    try {
                      brGradient.addColorStop(1, brColors[index]);
                      brGradient.addColorStop(0, "white");
                    } catch (e) {
                      brGradient = bgColors[index];
                    }

                    chartBgColor<?= $kunik ?>.push(brGradient);
                    chartBrColor<?= $kunik ?>.push(brGradient);
                }

                if(chartBgColor<?= $kunik ?>.length ==1){
                    chartBgColor<?= $kunik ?>=chartBgColor<?= $kunik ?>[0];
                    chartBrColor<?= $kunik ?>=chartBgColor<?= $kunik ?>[0];
                }
            }
            
        }
        //alert(JSON.strinfify(chartData<?= $kunik ?>));
        console.log("datadata", chartData<?= $kunik ?>);
            if(Object.keys(chartData<?= $kunik ?>).length>0){
                $("#tableData<?=$kunik?>").empty();
                for (const label in chartData<?= $kunik ?>) {
                    $("#tableData<?=$kunik?>").append(`<tr>
                        <td>${label}</td>
                        <td class="tableValue">${chartData<?= $kunik ?>[label]}</td>
                    </tr>`)
                }
            }
            
            $("#tableData<?=$kunik?>").hide();


            if(typeof chart<?= $kunik ?> != "undefined"){
                window.chart<?= $kunik ?>.destroy();
            }

            const addCountOnLabel = function(data){
                fullLabels = [];
                $.each(Object.keys(data), function(index,label){
                    fullLabels.push("( "+data[label]+" ) "+label);
                })
                return fullLabels;
            }

            var chartLegendCssSize = "<?= $blockCms['css']['chartLegendCss']['fontSize'] ?>";
            var fontSizeLegend = chartLegendCssSize.slice(0, -2)
            var chartTitleCssSize = "<?= $blockCms['css']['chartTitleCss']['fontSize'] ?>";
            var fontSizeTitle = chartTitleCssSize.slice(0, -2)
            /** INIT AND DRAW CHART **/
            window.chart<?= $kunik ?> = new Chart(window.ctx<?= $kunik ?>, {
                type: "<?= $blockCms['chartBlockType'] ?>",
                data: {
                    labels: addCountOnLabel(chartData<?= $kunik ?>),
                    datasets: [
                        {
                            label: "<?= $blockCms["chartDataLabel"] ?>",
                            data: Object.values(chartData<?= $kunik ?>),
                            backgroundColor: chartBgColor<?= $kunik ?>,
                            borderColor: chartBrColor<?= $kunik ?>,
                            borderWidth: 2
                        }
                    ]
                },
                <?php if($blockCms['chartDesignImage']==true){ ?>
                plugins: [{
                    afterDraw: chart => {
                        var ctx = chart.chart.ctx;
                        var xAxis = chart.scales['x-axis-0'];
                        var yAxis = chart.scales['y-axis-0'];
                        if(typeof xAxis!="undefined"){
                            xAxis.ticks.forEach((value, index) => {
                                if(typeof costum.lists!="undefined" && typeof costum.lists.imgThematic!="undefined" && typeof costum.lists.imgThematic[value]!="undefined"){

                                    var x = xAxis.getPixelForTick(index);
                                    var height = chart.getDatasetMeta(0).data[index]._model.y;

                                    if(costum.lists.imgThematic[value].includes("/")){

                                        var image = new Image(10, "auto");

                                        image.src = "<?= Yii::app()->getModule('costum')->assetsUrl ?>"+costum.lists.imgThematic[value];

                                        ctx.drawImage(image, x-15, height-40, 30, 30);
                                    }else if(costum.lists.imgThematic[value].includes("\\")){

                                        ctx.font='14px FontAwesome';
                                        ctx.fillText(costum.lists.imgThematic[value], x-12, height-12);
                                    }

                                }
                            })
                        }
                    }
                }],
                <?php } ?>

                
                // CHART OPTIONS
                options: {
                    responsive: true,
                    maintainAspectRatio:false,
                    legend: {
                        display: <?= (isset($blockCms['chartLegendDisplay']) && $blockCms['chartLegendDisplay']!="")?$blockCms['chartLegendDisplay']:true ?>,
                        position: "<?= $blockCms['chartLegendPosition'] ?>",
                        align: "middle",
                        labels:{
                            fontColor: "<?= $blockCms['css']['chartLegendCss']['color'] ?>",
                            fontSize: parseInt(fontSizeLegend),
                            fontStyle:"bold",
                            padding : 20,
                            boxWidth:50,
                            usePointStyle:false
                        }
                    },
                    legendCallback:function(chart){
                        mylog.log("legend call back", chart);
                        return "legend";
                    },
                    title: {
                        display: true,
                        text: "<?= $blockCms['chartTitleText'] ?>",
                        fontColor:"<?= $blockCms['css']['chartTitleCss']['color'] ?>",
                        fontSize:parseInt(fontSizeTitle)
                    },
                    //cutoutPercentage: <?= ($blockCms['chartBlockType']=='pie')?"0":"70" ?>,
                    scales: {
                        <?php if(!in_array($blockCms['chartBlockType'],["pie","doughnut", "polarArea", "radar"])){ ?>
                        yAxes: [{
                            gridLines:{
                                display:<?= $blockCms['chartAxeY'] ?>,
                                color:"<?= $blockCms['css']['chartAxeCss']['color'] ?>",
                            },
                            ticks:{
                                //fontFamily: "Font Awesome 5 Free",
                                fontColor:"<?= $blockCms['css']['chartAxeCss']['color'] ?>",
                                display:<?= $blockCms['chartAxeY'] ?>,
                                beginAtZero: true,
                                max:Math.max(...Object.values(chartData<?= $kunik ?>))+1
                            }
                        }],
                        <?php } ?>
                    <?php if(!in_array($blockCms['chartBlockType'],["pie","doughnut", "polarArea", "radar"])){ ?>
                        xAxes: [{
                            categories: "<i class='fa fa-home'></i>",
                            <?php if(!in_array($blockCms['chartBlockType'],["bar"])){ ?>
                            gridLines:{
                                display:<?= $blockCms['chartAxeX'] ?>,
                                color:"<?= $blockCms['css']['chartAxeCss']['color'] ?>",
                            },
                            <?php } ?>
                            ticks:{
                                fontColor:"<?= $blockCms['css']['chartAxeCss']['color'] ?>",
                                display:<?= $blockCms['chartAxeX'] ?>,
                                //fontFamily: "Font Awesome 5 Free"
                            }
                        }]
                    <?php } ?>
                    },
                    plugins: {
                      datalabels: {
                         display: true,
                         align: 'center',
                         anchor: 'center'
                      }
                   }
                }
            });
        }

    search<?= $kunik ?>.search.init(search<?= $kunik ?>);

    /**** ADD EVENT ON CHART ****/
    $("#chart<?=$kunik?>").on("click", function(evt) {
      var activePoints = chart<?= $kunik  ?>.getElementsAtEvent(evt);
      if (activePoints[0]) {
        var chartData = activePoints[0]['_chart'].config.data;
        var idx = activePoints[0]['_index'];

        var label = chartData.labels[idx];
        var value = chartData.datasets[0].data[idx];

        var cleanLabel = "";
        cleanLabel = label.substr(label.indexOf(") ")); 
        label = cleanLabel.replace(") ", "");

        if(selectedField<?= $kunik ?>=="type"){
            const tradLowercase = Object.fromEntries(Object.entries(trad).map(([key, value]) => [key, (typeof value == 'string' ? value.toLowerCase() : value)]));
            label = Object.keys(trad)[Object.values(tradLowercase).indexOf(label)];
        }
        urlCtrl.loadByHash("#search?"+selectedField<?= $kunik ?>+"="+label)
      }
    });

    costum.checkboxSimpleEvent = {
        true : function(id){
            if(id=="chartDataUse"){
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple").show();
                costum.checkboxSimpleEvent[sectionDyf.<?= $kunik ?>blockCms.chartDataImportOrType]("chartDataImportOrType")
            }
            if(id=="chartDataImportOrType"){
                $("#ajax-modal .chartDataManuallists").show();
                $("#ajax-modal .chartDataFromFileuploader").hide();
            }
        },
        false : function(id){
            if(id=="chartDataUse"){
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple .btn-dyn-checkbox").trigger("click");
                $("#ajax-modal .chartDataImportOrTypecheckboxSimple").hide();
                $("#ajax-modal .chartDataFromFileuploader").hide();


            }
            if(id=="chartDataImportOrType"){
                $("#ajax-modal .chartDataFromFileuploader").show();
                $("#ajax-modal .chartDataManuallists").hide();
            }
        }
    }
})
</script>
<script>
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#css-<?= $kunik ?>").append(str);
    var dataShow = [];
    if (costum.editMode) {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik ?>blockCms;
        var chartForEInput = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "inputSimple",
                            options : {
                                name : "chartTitleText",
                                label : "Titre du chart",
                                collection : "cms"
                            }
                        },
                        {
                            type : "inputSimple",
                            options : {
                                name : "chartDataLabel",
                                label : "Libellé du graph",
                                collection : "cms"
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "chartBlockType",
                                label : "Type de graph",
                                options : [
                                    {
                                        value : "bar",
                                        label : "Bar"
                                    },
                                    {
                                        value : "horizontalBar",
                                        label : "Bar Horizontal"
                                    },
                                    {
                                        value : "pie",
                                        label : "Pie"
                                    },
                                    {
                                        value : "doughnut",
                                        label : "Doughnut"
                                    },
                                    {
                                        value : "line",
                                        label : "Line"
                                    },
                                    {
                                        value : "radar",
                                        label : "Radar"
                                    },
                                    {
                                        value : "polarArea",
                                        label : "PolarArea"
                                    }
                                ]
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "chartDataElementTypes",
                                label : "Type d'élément",
                                options : [
                                    {
                                        value : "organizations",
                                        label : trad.organizations
                                    },
                                    {
                                        value : "events",
                                        label : trad.events
                                    },
                                    {
                                        value : "projects",
                                        label : trad.projects
                                    }
                                ]
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "chartLegendPosition",
                                label : "Position des légendes",
                                options : [
                                    {
                                        value : "top",
                                        label : "En Haut"
                                    },
                                    {
                                        value : "bottom",
                                        label : "En Bas"
                                    },
                                    {
                                        value : "right",
                                        label : "Droite"
                                    },
                                    {
                                        value : "left",
                                        label : "Gauche"
                                    }
                                ]
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "chartDataFields",
                                label : "Champs cible",
                                options : $.map( fields<?= $kunik ?>, function( val, key ) {
                                    return {
                                        value: key,
                                        label: val
                                    }
                                })
                            }
                        },
                        {
                            type : "selectMultiple",
                            options : {
                                name : "chartDataToShow",
                                label : "Données à Afficher",
                                class : "<?= $kunik ?>-chartDataToShow",
                                options : []
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartDataColorGradient",
                                label : "Ajouter une effet gradient",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartDataColorGradient
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartLegendDisplay",
                                label : "Afficher la légende",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartLegendDisplay
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartAxeY",
                                label : "Afficher l'axe Y",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartAxeY
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartAxeX",
                                label : "Afficher l'axe X",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartAxeX
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartDesignImage",
                                label : "Afficher pour Image ou Icon des données",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartDesignImage
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "chartDataUse",
                                label : "Utiliser données à part",
                                options : [
                                    {
                                        value : false,
                                        label : trad.no
                                    },
                                    {
                                        value : true,
                                        label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartDataUse
                            }
                        },
                        {
                            type : "inputSwitcher",
                            options : {
                                name : "chartDataImportOrType",
                                label : "Saisir les données ou Importer un Fichier CSV",
                                class : "<?= $kunik ?>-chartDataImportOrType",
                                tabs: [
                                    {
                                        value: "manuel",  
                                        label: "Saisir les données",
                                        inputs : [
                                            {
                                                type: "inputMultiple",
                                                options: {
                                                    name: "chartDataManual",
                                                    label : "Inserer ici vos Données manuellement",
                                                    inputs: [
                                                        {
                                                            type: "inputSimple",
                                                            options: {
                                                                name: "label",
                                                                label: "Libellé"
                                                            }
                                                        },
                                                        {
                                                            type: "number",
                                                            options: {        
                                                                name: "value",
                                                                label: "Valeur",
                                                                filterValue: cssHelpers.form.rules.checkLengthProperties,     
                                                            }
                                                        },
                                                        {
                                                            type: "color",
                                                            options: {
                                                                name: "color",
                                                                label: "Couleur"
                                                            }
                                                        }
                                                    ],
                                                    defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartDataManual
                                                }
                                            }
                                        ]
                                    }, 
                                    {
                                        value: "csv",
                                        label: "Importer Fichier CSV",
                                        inputs: [
                                            {
                                                type: "inputFile",
                                                options: {
                                                    name: "chartDataFromFile",
                                                    label: "Importer un Fichier csv avec le format (libellé, valeur, couleur)",
                                                    accept: ["csv"]
                                                }
                                            }
                                        ]
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].chartDataImportOrType
                            }
                        }
                        
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "chartTitleCss",
                                label : "style du titre",
                                inputs : [
                                    "color",
                                    "fontSize"
                                ]
                            }
                        },
                        {
                            type : "section",
                            options : {
                                name : "chartLegendCss",
                                label : "style du legende",
                                inputs : [
                                    "color",
                                    "fontSize"
                                ]
                            }
                        },
                        {
                            type : "section",
                            options : {
                                name : "chartBlockCss",
                                label : "style du chart",
                                inputs : [
                                    "width",
                                    "height",
                                    "backgroundColor",
                                    "borderRadius"
                                ]
                            }
                        },
                        {
                            type : "section",
                            options : {
                                name : "chartAxeCss",
                                label : "style de l'axe",
                                inputs : [
                                    "color"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                if(name == "chartDataUse"){
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "chartDataFields"){
                    var tplCtx = {};
                    tplCtx = {
                        id : "<?= $myCmsId ?>",
                        value : [],
                        path : "chartDataToShow",
                        collection: "cms"
                    }
                    dataHelper.path2Value( tplCtx, function(params) {
                        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                    })
                    search<?= $kunik ?>.search.init(search<?= $kunik ?>)
                    setTimeout(() => {
                        $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click")
                    }, 3000);
                } else {
                    cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                }
            },
            beforeLoad : function(path,valueToSet,name,payload,value) {
                uploadObj.set("cms","<?php echo $blockKey ?>");
                if(sectionDyf.<?php echo $kunik ?>blockCms.chartDataUse == false) {
                    $('.co-input-switcher').hide();
                } else {
                    $('.co-input-switcher').show();
                }
                dataShow = Object.keys(data<?= $kunik ?>).reduce((a, v) => ({ ...a, [v]: v}), {});
                
                mylog.log(dataShow, "donneko")

                let dataPasse = $.map( 
                    dataShow , function( val, key ) {
                        return {
                            value: key,
                            label: val
                        }
                    }
                );
                cmsConstructor.blocks.chartForE<?= $myCmsId ?>.configTabs.general.inputsConfig[6].options.options = dataPasse;
            }
        }
        cmsConstructor.blocks.chartForE<?= $myCmsId ?> = chartForEInput;
    }

</script>
