<?php
$keyTpl = "hexagoneMapping";
$blockId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];

$graphAssets = [
  '/js/hexagon.js'
];
HtmlHelper::registerCssAndScriptsFiles(
  $graphAssets,
  Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
);
?>

<style>
  .d-none {
    display: none !important;
  }

  .modal.hexagon-choose-image {
    text-align: center;
    background: #0000009c;
  }

  @media screen and (min-width: 768px) {
    .modal.hexagon-choose-image:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .hexagon-choose-image .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }

  .<?= $kunik ?> {
    position: relative;
  }

  .d-none {
    display: none !important;
  }


  .btn-hexa-container {
    background: white;
    border: 1px solid;
    padding: 0;
    text-align: left;
  }

  .btn-hexa-container.btn-primary {
    color: white;
    background-color: #2C3E50;
    border-color: #2C3E50;
    font-weight: 700;
  }

  .btn-hexa-container button {
    background: none;
    border: none;
    padding: 6px 12px;
  }

  .tab-pane #hexagone {
    position: relative;
  }

  .hexagone-child-container.in {
    display: flex;
    flex-direction: column;
    text-align: left;
  }

  .hexagone-child-container {
    padding: 0px 15px;
  }

  .hexagone-child-container .btn-hexagone-zoomer {
    text-align: left;
  }

  .hexagone-badges-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: stretch;
    align-content: stretch;
  }

  .filters-container-circle-<?= $kunik ?> {
    width: 300px;
  }

  .filters-container-circle-<?= $kunik ?> #hexagone-filter-container-<?= $kunik ?> {
    display: flex;
    flex-direction: column;
    width: 300px;
    background-color: white;
    overflow-y: auto;
    height: 700px;
  }


  #hexagone-detail-preview {
    padding-top: 0%;
    padding-right: 9%;
    padding-left: 9%;
    position: fixed;
    background: rgba(255, 255, 255, 0.89);
    width: 100%;
    z-index: 8;
    padding-bottom: 10% !important;
  }

  #hexagone-detail-preview .page-title {
    font-weight: 700;
    font-size: 24px;
    margin: 1% 0%;
    color: #244A58;
  }

  .hexagone-prev-header {
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
  }

  .hexagone-content-preview {
    height: 65vh !important;
    box-shadow: -10px -5px 34px 0px #0000001C;
    border-radius: 14px;
    overflow-y: auto;
    background: white;
    padding: 20px 20px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> ul {
    list-style: none;
    display: inline-block;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> .icon {
    font-size: 14px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li {
    float: left;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a {
    color: #FFF;
    display: block;
    background: #3498db;
    text-decoration: none;
    position: relative;
    height: 40px;
    line-height: 40px;
    padding: 0 10px 0 5px;
    text-align: center;
    margin-right: 23px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a {
    background-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:before {
    border-color: #2980b9;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:after {
    border-left-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a {
    padding-left: 15px;
    -moz-border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px 0 0 4px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a:before {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a {
    padding-right: 15px;
    -moz-border-radius: 0 4px 4px 0;
    -webkit-border-radius: 0;
    border-radius: 0 4px 4px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a:after {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before,
  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    content: "";
    position: absolute;
    top: 0;
    border: 0 solid #3498db;
    border-width: 20px 10px;
    width: 0;
    height: 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before {
    left: -20px;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    left: 100%;
    border-color: transparent;
    border-left-color: #3498db;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover {
    background-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:before {
    border-color: #1abc9c;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:after {
    border-left-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active {
    background-color: #16a085;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:before {
    border-color: #16a085;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:after {
    border-left-color: #16a085;
  }


  .btn-close-detail,
  .btn-open-url {
    margin-top: 20px;
    margin-left: 20px;
    box-shadow: 0px 4px 18px 0px #0000001C;
    padding: 13px;
    font-weight: bold;
    border-radius: 15px;
    color: #000 !important;
  }
</style>
<div class="<?= $kunik ?>">
  <div id="hexagone-breadcrumbs-<?= $kunik ?>" style="margin: 20px auto;"></div>
  <div class="hexagone-badges-wrapper">
    <canvas id="hexBadgesCanvas<?= $kunik ?>" class="hexContainerCanvas" height="700"></canvas>
    <div class="filters-container-circle-<?= $kunik ?> hidden-sm hidden-xs">
      <div id="hexagone-filter-container-<?= $kunik ?>" class="margin-top-10 text-center"></div>
    </div>
  </div>
</div>

<script>
  var hexagonBadges<?= $kunik ?> = null;
  var hexagoneDataBadges<?= $kunik ?> = <?= json_encode($blockCms["data"]) ?>;
  var hexagoneDataMembers<?= $kunik ?> = <?= json_encode($blockCms["members"]) ?>;
  HexagonGrid.prototype.clickEvent = function(e) {
    var mouseX = e.pageX;
    var mouseY = e.pageY;

    var localX = mouseX - this.canvasOriginX;
    var localY = mouseY - this.canvasOriginY;

    var tile = this.getSelectedTile(localX, localY);
    if (this.hasHexagon(tile.column, tile.row)) {
      let hexaData = this.drawedHex[`${tile.column},${tile.row}`];
      if ((<?= json_encode($blockCms["navigate"]) ?> == "depth")) {
        if (typeof this.data[hexaData.text] != "undefined" && this.data[hexaData.text].length > 0) {
          this.navigationStack.push({
            scale: this.scale,
            offsetX: this.offsetX,
            offsetY: this.offsetY,
            zoomTo: this.navigation
          });
          let group = Object.values(this.data[hexaData.text]);
          this.drawedHex = {};
          this.drawedTitle = {};
          this.navigation = hexaData.text;
          this.clearCanvas();
          this.placeGroupsWithOffsets(
            group, tile, false, false, 3);
          this.fitBounds();
          this.addBreadCrumb();
        } else {
            var url = `#page.type.${hexaData.extra.collection}.id.${hexaData.extra.id}`;
            hashT = location.hash.split("?");
            getStatus = urlCtrl.getUrlSearchParams();

            hashNav = (hashT[0].indexOf("#") < 0) ? "#" + hashT[0] : hashT[0];
            urlHistoric = hashNav + "?preview=" + hexaData.extra.collection + "." + hexaData.extra.id;
            if (getStatus != "") urlHistoric += "&" + getStatus;
            history.replaceState({}, null, urlHistoric);
				    urlCtrl.openPreview(url);
        }
      }
    }
  }

  function bindContextMenu<?= $kunik ?>() {
    if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
      $("#hexBadgesCanvas<?= $kunik ?>").on('contextmenu', function(e) {
        e.preventDefault();
        if (hexagonBadges<?= $kunik ?>.navigationStack.length > 0) {
          const previousState = hexagonBadges<?= $kunik ?>.navigationStack.pop();
          hexagonBadges<?= $kunik ?>.scale = previousState.scale;
          hexagonBadges<?= $kunik ?>.offsetX = previousState.offsetX;
          hexagonBadges<?= $kunik ?>.offsetY = previousState.offsetY;
          hexagonBadges<?= $kunik ?>.drawedHex = {};
          hexagonBadges<?= $kunik ?>.drawedTitle = {};
          hexagonBadges<?= $kunik ?>.navigation = previousState.zoomTo;
          hexagonBadges<?= $kunik ?>.clearCanvas();
          // Redessiner la vue
          if (hexagonBadges<?= $kunik ?>.preview === false) {
            hexagonBadges<?= $kunik ?>.drawHexEditMode();
          }
          let group = Object.values(hexagonBadges<?= $kunik ?>.data[previousState.zoomTo]);
          hexagonBadges<?= $kunik ?>.popBreadCrumb()
          if (previousState.zoomTo == "root") {
            hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 2);
          } else {
            hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 3);
          }

        }
      })
    }
  }

  function buildBtnView<?= $kunik ?>(data, addChildData){
      let addChild = typeof addChildData != "undefined" ? addChildData : true;
      if(addChild){
        $.each(data, function(key, child) {
          let btn = `<div class="btn margin-5 btn-hexa-container">
            <button class="btn-expand" data-toggle="collapse" href="#${hexagonBadges<?= $kunik ?>.slugify(key)}">
              <i class="fa fa-chevron-down"></i>
            </button>
            <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  data-type="group">
              ${key}
              <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
            </button>
          </div>
          <div class="collapse hexagone-child-container" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  id="${hexagonBadges<?= $kunik ?>.slugify(key)}">
          `;
          data[key].childs.forEach((child) => {
            btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  data-type="children" data-value='${hexagonBadges<?= $kunik ?>.slugify(child.text)}'>
              ${child.text}
            </button>`;
          })
          btn += `</div>`;
          $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
        })
      }else{
        $.each(data, function(key, child) {
          if(typeof data[key].childs != "undefined"){
            let btn = `<div class="btn margin-5 btn-hexa-container">
              <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?> data-type="group">
                ${key}
                <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
              </button>
            </div>`;
            $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
          }else{
            let btn = `<div class="btn margin-5 btn-hexa-container">
              <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?> data-type="children">
                ${key}
                <span class="badge badge-theme-count margin-left-5 label-primary hidden" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
              </button>
            </div>`;
            $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
          }

        });
      }
    }

    function drawHexagoneBadges(data){
      buildBtnView<?= $kunik ?>(data, <?= json_encode($blockCms["navigate"]) ?> != "depth");
      if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
        hexagonBadges<?= $kunik ?>.setBreadCrumbContainer("#hexagone-breadcrumbs-<?= $kunik ?>");
        hexagonBadges<?= $kunik ?>.setType(<?= json_encode($blockCms["navigate"]) ?>);
        
        hexagonBadges<?= $kunik ?>.data["root"] = Object.values(data);
        hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(Object.values(data), {
          column: 0,
          row: 0
        }, false, false, 2);
        hexagonBadges<?= $kunik ?>.fitBounds();
        hexagonBadges<?= $kunik ?>.addBreadCrumb()
      } else {
        
        hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(Object.values(data), hexagonBadges<?= $kunik ?>.getCenterTile());
        hexagonBadges<?= $kunik ?>.fitBounds();
      }
    }

  function bindBtnClick<?= $kunik ?>() {
      $(".btn-hexagone-zoomer").off().on('click', function() {
        if($(this).data("navigate") ==  "depth"){
          if ($(this).data("type") == "group") {
            if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
              $(".btn-hexa-container").removeClass("btn-primary");
              if (hexagonBadges<?= $kunik ?>.navigationStack.length > 0) {
                const previousState = hexagonBadges<?= $kunik ?>.navigationStack.pop();
                hexagonBadges<?= $kunik ?>.scale = previousState.scale;
                hexagonBadges<?= $kunik ?>.offsetX = previousState.offsetX;
                hexagonBadges<?= $kunik ?>.offsetY = previousState.offsetY;
                hexagonBadges<?= $kunik ?>.drawedHex = {};
                hexagonBadges<?= $kunik ?>.drawedTitle = {};
                hexagonBadges<?= $kunik ?>.navigation = previousState.zoomTo;
                hexagonBadges<?= $kunik ?>.clearCanvas();
                // Redessiner la vue
                if (hexagonBadges<?= $kunik ?>.preview === false) {
                  hexagonBadges<?= $kunik ?>.drawHexEditMode();
                }
                let group = Object.values(hexagonBadges<?= $kunik ?>.data[previousState.zoomTo]);
                hexagonBadges<?= $kunik ?>.popBreadCrumb()
                if (previousState.zoomTo == "root") {
                  hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(
                    group, {
                      column: 0,
                      row: 0
                    }, false, false, 2);
                } else {
                  hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(
                    group, {
                      column: 0,
                      row: 0
                    }, false, false, 3);
                }

              }
            } else {
              let groupText = $(this).find("span").data("countvalue");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).parents(".btn-hexa-container").addClass("btn-primary");
              hexagonBadges<?= $kunik ?>.navigationStack.push({
                scale: hexagonBadges<?= $kunik ?>.scale,
                offsetX: hexagonBadges<?= $kunik ?>.offsetX,
                offsetY: hexagonBadges<?= $kunik ?>.offsetY,
                zoomTo: hexagonBadges<?= $kunik ?>.navigation
              });
              let group = Object.values(hexagonBadges<?= $kunik ?>.data[groupText]);
              hexagonBadges<?= $kunik ?>.drawedHex = {};
              hexagonBadges<?= $kunik ?>.drawedTitle = {};
              hexagonBadges<?= $kunik ?>.navigation = groupText;
              hexagonBadges<?= $kunik ?>.clearCanvas();
              hexagonBadges<?= $kunik ?>.placeGroupsWithOffsets(
                group, {column:0, row:0}, false, false, 3);
              hexagonBadges<?= $kunik ?>.fitBounds();
              hexagonBadges<?= $kunik ?>.addBreadCrumb();
            }
          } else {
            if ($(this).hasClass("btn-primary")) {
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              hexagonBadges<?= $kunik ?>.zoomTo = [];
              hexagonBadges<?= $kunik ?>.fitBounds();
            } else {
              let texte = $(this).data("value");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).addClass("btn-primary");
              let element = Object.values(hexagonBadges<?= $kunik ?>.drawedHex).find(hex => {
                return hexagonBadges<?= $kunik ?>.slugify(hex.text) == texte;
              });
              if (typeof element != "undefined") {
                hexagonBadges<?= $kunik ?>.zoomToElement(element.column, element.row);
              }
            }
          }
        }else{
          if ($(this).data("type") == "group") {
            if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
              $(".btn-hexa-container").removeClass("btn-primary");
              hexagonBadges<?= $kunik ?>.zoomTo = [];
              hexagonBadges<?= $kunik ?>.fitBounds();
            } else {
              let group = $(this).find("span").data("countvalue");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).parents(".btn-hexa-container").addClass("btn-primary");
              hexagonBadges<?= $kunik ?>.zoomToGroup(group);
            }
          } else {
            if ($(this).hasClass("btn-primary")) {
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              hexagonBadges<?= $kunik ?>.zoomTo = [];
              hexagonBadges<?= $kunik ?>.fitBounds();
            } else {
              let texte = $(this).data("value");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).addClass("btn-primary");
              let element = Object.values(hexagonBadges<?= $kunik ?>.drawedHex).find(hex => {
                return hexagonBadges<?= $kunik ?>.slugify(hex.text) == texte;
              });
              if (typeof element != "undefined") {
                hexagonBadges<?= $kunik ?>.zoomToElement(element.column, element.row);
              }
            }
          }
        }
      })
    }
  $(function(){
    hexagonBadges<?= $kunik ?> = new HexagonGrid("hexBadgesCanvas<?= $kunik ?>", <?= json_encode($blockCms["circle"]) ?>, window.innerWidth - 300);
    function groupBadgesData(data){
      var dataGroupped = {};

      $.each(data, function(key, value){
        dataGroupped[value.name] = {
          group: value.name,
          text: value.name,
          id: value._id.$id,
          collection: value.collection,
          image : typeof value.profilMediumImageUrl != "undefined" ? value.profilMediumImageUrl : null,
          childs: Object.values(hexagoneDataMembers<?= $kunik ?>).filter(function(member){
            return typeof member.badges[key] != 'undefined';
          }).map(function(member){
            return {
              text: member.name,
              image: typeof member.profilMediumImageUrl != "undefined" ? member.profilMediumImageUrl : null,
              slug: member.slug,
              description: member.description,
              collection: member.collection,
              id: member._id.$id,
              color: `#eeeeee`
            };
          })
        };
      });

      return dataGroupped;
    }
    drawHexagoneBadges(groupBadgesData(hexagoneDataBadges<?= $kunik ?>));
    bindBtnClick<?= $kunik ?>();
    bindContextMenu<?= $kunik ?>();
  });
  
</script>