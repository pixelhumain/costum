<?php
$keyTpl = "hexagoneGraph";
$blockId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];

$graphAssets = [
  '/js/hexagon.js'
];
HtmlHelper::registerCssAndScriptsFiles(
  $graphAssets,
  Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
);
HtmlHelper::registerCssAndScriptsFiles(array(
  '/plugins/spectrum-colorpicker2/spectrum.min.js',
  '/plugins/spectrum-colorpicker2/spectrum.min.css'
), null);
?>
<style>
  .d-none {
    display: none !important;
  }

  .modal.hexagon-choose-image {
    text-align: center;
    background: #0000009c;
  }

  @media screen and (min-width: 768px) {
    .modal.hexagon-choose-image:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .hexagon-choose-image .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }

  .<?= $kunik ?> {
    position: relative;
  }

  .d-none {
    display: none !important;
  }

  /* CSS */
  .button-switch {
    align-items: center;
    appearance: none;
    background-color: #fff;
    border-radius: 24px;
    border-style: none;
    box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px, rgba(0, 0, 0, .14) 0 6px 10px 0, rgba(0, 0, 0, .12) 0 1px 18px 0;
    box-sizing: border-box;
    color: #3c4043;
    cursor: pointer;
    display: inline-flex;
    fill: currentcolor;
    font-family: "Google Sans", Roboto, Arial, sans-serif;
    font-size: 14px;
    font-weight: 500;
    height: 48px;
    justify-content: center;
    letter-spacing: .25px;
    line-height: normal;
    max-width: 100%;
    overflow: visible;
    padding: 2px 24px;
    position: absolute;
    top: 10px;
    right: 10px;
    text-align: center;
    text-transform: none;
    transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1), opacity 15ms linear 30ms, transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;
    user-select: none;
    -webkit-user-select: none;
    touch-action: manipulation;
    width: auto;
    will-change: transform, opacity;
    z-index: 5;
  }

  .button-switch:hover {
    background: #F6F9FE;
    color: #174ea6;
  }

  .button-switch:active {
    box-shadow: 0 4px 4px 0 rgb(60 64 67 / 30%), 0 8px 12px 6px rgb(60 64 67 / 15%);
    outline: none;
  }

  .button-switch:not(:disabled) {
    box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
  }

  .button-switch:not(:disabled):hover {
    box-shadow: rgba(60, 64, 67, .3) 0 2px 3px 0, rgba(60, 64, 67, .15) 0 6px 10px 4px;
  }

  .button-switch:not(:disabled):focus {
    box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
  }

  .button-switch:not(:disabled):active {
    box-shadow: rgba(60, 64, 67, .3) 0 4px 4px 0, rgba(60, 64, 67, .15) 0 8px 12px 6px;
  }

  .button-switch:disabled {
    box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
  }

  .btn-hexa-container {
    background: white;
    border: 1px solid;
    padding: 0;
    text-align: left;
  }

  .btn-hexa-container.btn-primary {
    color: white;
    background-color: #2C3E50;
    border-color: #2C3E50;
    font-weight: 700;
  }

  .btn-hexa-container button {
    background: none;
    border: none;
    padding: 6px 12px;
  }

  .tab-pane #hexagone {
    position: relative;
  }

  .hexagone-child-container.in {
    display: flex;
    flex-direction: column;
    text-align: left;
  }

  .hexagone-child-container {
    padding: 0px 15px;
  }

  .hexagone-child-container .btn-hexagone-zoomer {
    text-align: left;
  }

  .hexagone-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: stretch;
    align-content: stretch;
  }

  .filters-container-circle {
    width: 300px;
  }

  .filters-container-circle #hexagone-filter-container {
    display: flex;
    flex-direction: column;
    width: 300px;
    background-color: white;
    overflow-y: auto;
    height: 700px;
  }

  #hexagon-co-inputs {
    display: flex;
    flex-wrap: wrap;
  }

  #hexagone-detail-preview {
    padding-top: 0%;
    padding-right: 9%;
    padding-left: 9%;
    position: fixed;
    background: rgba(255, 255, 255, 0.89);
    width: 100%;
    z-index: 8;
    padding-bottom: 10% !important;
  }

  #hexagone-detail-preview .page-title {
    font-weight: 700;
    font-size: 24px;
    margin: 1% 0%;
    color: #244A58;
  }

  .hexagone-prev-header {
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
  }

  .hexagone-content-preview {
    height: 65vh !important;
    box-shadow: -10px -5px 34px 0px #0000001C;
    border-radius: 14px;
    overflow-y: auto;
    background: white;
    padding: 20px 20px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> ul {
    list-style: none;
    display: inline-block;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> .icon {
    font-size: 14px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li {
    float: left;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a {
    color: #FFF;
    display: block;
    background: #3498db;
    text-decoration: none;
    position: relative;
    height: 40px;
    line-height: 40px;
    padding: 0 10px 0 5px;
    text-align: center;
    margin-right: 23px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a {
    background-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:before {
    border-color: #2980b9;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:after {
    border-left-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a {
    padding-left: 15px;
    -moz-border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px 0 0 4px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a:before {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a {
    padding-right: 15px;
    -moz-border-radius: 0 4px 4px 0;
    -webkit-border-radius: 0;
    border-radius: 0 4px 4px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a:after {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before,
  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    content: "";
    position: absolute;
    top: 0;
    border: 0 solid #3498db;
    border-width: 20px 10px;
    width: 0;
    height: 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before {
    left: -20px;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    left: 100%;
    border-color: transparent;
    border-left-color: #3498db;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover {
    background-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:before {
    border-color: #1abc9c;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:after {
    border-left-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active {
    background-color: #16a085;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:before {
    border-color: #16a085;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:after {
    border-left-color: #16a085;
  }


  .btn-close-detail,
  .btn-open-url {
    margin-top: 20px;
    margin-left: 20px;
    box-shadow: 0px 4px 18px 0px #0000001C;
    padding: 13px;
    font-weight: bold;
    border-radius: 15px;
    color: #000 !important;
  }
</style>
<div class="<?= $kunik ?>">
  <div id="hexagone-breadcrumbs-<?= $kunik ?>" style="margin: 20px auto;"></div>
  <div class="hexagone-wrapper">
    <canvas id="HexCanvas<?= $kunik ?>" class="hexContainerCanvas" height="700"></canvas>
    <div class="filters-container-circle hidden-sm hidden-xs">
      <div id="hexagone-filter-container" class="margin-top-10 text-center"></div>
    </div>
  </div>
  <div class="modal fade hexagon-choose-image">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h5 class="modal-title">Choisir une image de fond</h5>
        </div>
        <div class="modal-body">
          <div id="hexagon-co-inputs" class="row">

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-change-position hidden" style="float: left;">Deplacer</button>
          <button type="button" class="btn btn-primary btn-update-description" style="float: left;">Configurer la description</button>
          <button type="button" class="btn btn-secondary btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary btn-validate-background">OK</button>
        </div>
      </div>
    </div>
  </div>
  <button class="button-switch d-none" role="button">Changer en mode édition</button>
  <div id="hexagone-detail-preview" style="display: none;">
    <div class="hexagone-prev-content">
      <div class="hexagone-prev-header">
        <p class="page-title"></p>
        <div class="hexagone-preview-content">
          <a class="btn btn-open-url hidden" target="_blank" href="#">
            <i class="fa fa-link" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Ouvrir le lien
          </a>
          <button class="btn btn-close-detail">
            <i class="fa fa-times-circle" style="font-size: 19px; color: #8DBC21; padding: 2px 5px;"></i> Fermer
          </button>
        </div>
      </div>
      <div class="col-xs-12 hexagone-content-preview">

      </div>
    </div>
  </div>
</div>


<script>
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $blockKey ?>"] = <?= json_encode($blockCms) ?>;
    hexagonGraphInput = {
      configTabs: {
        general: {
          inputsConfig: [{
              type: "groupButtons",
              options: {
                name: "groupped",
                label: "Grouper les hexagones",
                options: [{
                    value: false,
                    label: trad.no
                  },
                  {
                    value: true,
                    label: trad.yes
                  }
                ],
                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].groupped
              }
            },
            {
              type: "number",
              options: {
                name: "circle",
                class: "col-sm-12",
                label: "Taille de l'hexagone",
                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].circle,
                filterValue: cssHelpers.form.rules.checkLengthProperties
              }
            },
            {
              type: "select",
              options: {
                name: "navigate",
                class: "col-sm-12",
                label: "Type de navigation",
                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].navigate,
                options: [{
                    "label": "Disposition normale",
                    "value": "normal"
                  },
                  {
                    "label": "Navigation en profondeur",
                    "value": "depth"
                  }
                ]
              }
            },
          ]
        },
        style : {
          inputsConfig: [
            {
              type: "colorPicker",
              options: {
                name: "fillColor",
                label: tradCms.borderColor
              }
            },
            {
              type: "number",
              options: {
                name: "fillWidth",
                label: tradCms.borderSize,
                filterValue: cssHelpers.form.rules.checkLengthProperties
              }
            }
          ]
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
      }
    }

    cmsConstructor.blocks.hexagoneGraph<?= $blockKey ?> = hexagonGraphInput;
  }
  if (costum.isCostumAdmin) {
    $(".button-switch").removeClass("d-none");
    $("#hexagone-filter-container").css({
      "padding-top": $(".button-switch").height() + 30
    });
  }
  var hexagonData = <?= json_encode($blockCms["data"]) ?>;
  var hexaMode = true;
  var hexagoneDescription = null;

  function dragHexagone(hexa, tile) {
    var data = {};
    $.each(hexa.drawedHex, function(k, v) {
      data[v.column + v.row] = $.extend(true, {}, v);
    });
    data[`${tile.column}${tile.row}`] = hexa.drawedHex[hexa.dragData.column + "," + hexa.dragData.row];
    data[`${tile.column}${tile.row}`].column = tile.column;
    data[`${tile.column}${tile.row}`].row = tile.row;
    delete data[hexa.dragData.column + "" + hexa.dragData.row];
    var saveHexagone = {
      id: "<?= $blockId ?>",
      collection: 'cms',
      path: `data`,
      value: data
    }
    dataHelper.path2Value(saveHexagone, function() {
      delete hexa.drawedHex[hexa.dragData.column + "," + hexa.dragData.row];
      hexa.switchDrag(false, null);
      hexa.context.save();
      hexa.context.translate(hexa.offsetX, hexa.offsetY);
      hexa.context.scale(hexa.scale, hexa.scale);
      hexa.drawHexAtColRow(tile.column, tile.row, data[`${tile.column}${tile.row}`].color, data[`${tile.column}${tile.row}`].text, data[`${tile.column}${tile.row}`].image);
      hexa.context.restore();
      toastr.success("Hexagone déplacé avec succès");
    });
  }

  HexagonGrid.prototype.clickEvent = function(e) {
    if (this.isDoubleClick) {
      // Si un double clic a été détecté, annuler le simple clic
      this.isDoubleClick = false;
      return;
    }

    this.clickTimeout = setTimeout(() => {
      let hexa = this;
      var mouseX = e.pageX;
      var mouseY = e.pageY;

      var localX = mouseX - this.canvasOriginX;
      var localY = mouseY - this.canvasOriginY;

      var tile = this.getSelectedTile(localX, localY);
      if (this.preview == false) {
        if (hexa.drag && hexa.dragData != null) {
          if (this.hasHexagon(tile.column, tile.row)) {
            bootbox.confirm({
              message: "Etes vous sur de remplacer cette hexagone ?",
              buttons: {
                confirm: {
                  label: trad["yes"],
                  className: 'btn-success'
                },
                cancel: {
                  label: trad["no"],
                  className: 'btn-danger'
                }
              },
              callback: function(result) {
                if (!result) {
                  hexa.switchDrag(false, null);
                  return;
                } else {
                  dragHexagone(hexa, tile);
                }
              }
            });
          } else {
            dragHexagone(hexa, tile);
          }
        } else {

          $(".btn-change-position").addClass("hidden");
          let imageData = "",
            color = "",
            text = "",
            radius = hexa.radius,
            group = "",
            url = "",
            description = "";
          if (this.hasHexagon(tile.column, tile.row)) {
            imageData = this.drawedHex[`${tile.column},${tile.row}`].image;
            color = this.drawedHex[`${tile.column},${tile.row}`].color;
            text = this.drawedHex[`${tile.column},${tile.row}`].text;
            radius = this.drawedHex[`${tile.column},${tile.row}`].radius || hexa.radius;
            url = this.drawedHex[`${tile.column},${tile.row}`].extra.url;
            group = this.drawedHex[`${tile.column},${tile.row}`].extra.group;
            hexagoneDescription = this.drawedHex[`${tile.column},${tile.row}`].extra.description;
            $(".btn-change-position").removeClass("hidden");
          }
          var myInputs = {
            container: "#hexagon-co-inputs",
            inputs: [{
                type: "colorPicker",
                options: {
                  name: "hexaColor",
                  class: "col-sm-4",
                  label: "Couleur du fond",
                  defaultValue: color,
                }
              },
              {
                type: "inputSimple",
                options: {
                  name: "text",
                  class: "col-sm-4",
                  label: "Texte",
                  defaultValue: text,
                }
              },
              {
                type: "number",
                options: {
                  name: "radius",
                  class: "col-sm-4",
                  label: "Taille de l'hexagone",
                  defaultValue: radius,
                  filterValue: cssHelpers.form.rules.checkLengthProperties
                }
              },
              {
                type: "inputSimple",
                options: {
                  name: "url",
                  class: "col-sm-4",
                  label: "Lien vers l'élément",
                  defaultValue: url
                }
              },
              {
                type: "inputFileImage",
                options: {
                  name: "image",
                  label: "Image du fond",
                  collection: "documents",
                  class: "imageUploader col-sm-12",
                  endPoint: "/subKey/hexagon",
                  domElement: "image",
                  filetype: ["jpeg", "jpg", "gif", "png"],
                }
              }
            ],
            onchange: function(name, value, payload) {
              if (name == "image") {
                imageData = value;
                if (value.includes("images/thumbnail-default.jpg")) {
                  imageData = null;
                }
              }
              if (name == "hexaColor") {
                color = value;
              }
              if (name == "text") {
                text = value;
              }
              if (name == "radius") {
                radius = value;
              }
              if (name == "group") {
                group = value;
              }
              if (name == "url") {
                url = value;
              }
            }
          }
          if ((<?= json_encode($blockCms["groupped"]) ?> == true || <?= json_encode($blockCms["groupped"]) ?> == "true") || (<?= json_encode($blockCms["navigate"]) ?> == "depth")) {
            group = this.navigation != "root" ? this.navigation : group;
            myInputs.inputs.splice(3, 0, {
              type: "inputSimple",
              options: {
                name: "group",
                class: "col-sm-4",
                label: "Nom du groupe",
                defaultValue: group
              }
            });
            if (imageData != "" && imageData != null) {
              myInputs.inputs[5].options.defaultValue = imageData;
            }
            $(".btn-change-position").addClass("hidden");
          } else {
            if (imageData != "" && imageData != null) {
              myInputs.inputs[4].options.defaultValue = imageData;
            }
          }
          new CoInput(myInputs);
          $(".hexagon-choose-image").modal("show");
          $(".btn-validate-background").off("click").on("click", function() {
            let key = hexa.hasHexagon(tile.column, tile.row) ? `${hexa.drawedHex[`${tile.column},${tile.row}`].extra.column}${hexa.drawedHex[`${tile.column},${tile.row}`].extra.row}` : `${tile.column}${tile.row}`;
            var saveHexagone = {
              id: "<?= $blockId ?>",
              collection: 'cms',
              path: `data.${key}`,
              value: {
                column: hexa.hasHexagon(tile.column, tile.row) ? hexa.drawedHex[`${tile.column},${tile.row}`].extra.column : tile.column,
                row: hexa.hasHexagon(tile.column, tile.row) ? hexa.drawedHex[`${tile.column},${tile.row}`].extra.row : tile.row,
                color: color != "" ? color : "#ddd",
                text: text,
                url: url,
                // radius: radius,
                image: imageData != "" ? imageData : null
              }
            }
            if (group != "") {
              saveHexagone.value.group = group;
            }
            if (hexagoneDescription != "" && hexagoneDescription != null) {
              saveHexagone.value.description = hexagoneDescription;
            }
            dataHelper.path2Value(saveHexagone, function() {
              new Promise((resolve, reject) => {
                hexagonData[key] = saveHexagone.value;
                hexagonGrid<?= $kunik ?>.drawedHex = {};
                hexagonGrid<?= $kunik ?>.drawedTitle = {};
                hexagonGrid<?= $kunik ?>.clearCanvas();
                resolve();
              }).then((value) => {
                if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
                  const index = hexagonGrid<?= $kunik ?>.data[hexa.navigation].findIndex(obj => obj.column === saveHexagone.value.column && obj.row === saveHexagone.value.row);
                  if(index !== -1) {
                    hexagonGrid<?= $kunik ?>.data[hexa.navigation][index] = saveHexagone.value;
                  } else {
                    hexagonGrid<?= $kunik ?>.data[hexa.navigation].push(saveHexagone.value);
                  }
                  
                  let group = Object.values(hexagonGrid<?= $kunik ?>.data[hexa.navigation]);
                  
                  if (hexa.navigation == "root") {
                    hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(
                      group, {
                        column: 0,
                        row: 0
                      }, false, false, 2);
                  } else {
                    hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(
                      group, {
                        column: 0,
                        row: 0
                      }, false, false, 3);
                  }
                } else {
                  hexagonGrid<?= $kunik ?>.data = {};
                  dragHexagoneData();
                }
                bindBtnClick();
              })
              hexagoneDescription = null;
              $(".hexagon-choose-image").modal("hide");
            })
          });
          $(".btn-change-position").off("click").on("click", function() {
            $(".hexagon-choose-image").modal("hide");
            hexa.switchDrag(true, {
              column: tile.column,
              row: tile.row
            });
          });
          $(".btn-update-description").off("click").on("click", function() {
            urlCtrl.openPreview("/view/url/costum.views.custom.graph.hexagoneDetailEditor", {
              "preview": true,
              "title": text != "" ? text : "l'hexagone",
              "data": description,
              "type": "blockHexagone",
              "callback": "updateHexagoneData",
              "paramsCallback": "htmlGenerate"
            });
          });
        }
      } else {
        if (this.hasHexagon(tile.column, tile.row)) {
          if ((<?= json_encode($blockCms["navigate"]) ?> == "depth")) {
            if (this.hasHexagon(tile.column, tile.row)) {
              let hexaData = this.drawedHex[`${tile.column},${tile.row}`];
              if (typeof hexagonGrid<?= $kunik ?>.data[hexaData.text] != "undefined" && hexagonGrid<?= $kunik ?>.data[hexaData.text].length > 0) {
                hexagonGrid<?= $kunik ?>.navigationStack.push({
                  scale: hexagonGrid<?= $kunik ?>.scale,
                  offsetX: hexagonGrid<?= $kunik ?>.offsetX,
                  offsetY: hexagonGrid<?= $kunik ?>.offsetY,
                  zoomTo: hexagonGrid<?= $kunik ?>.navigation
                });
                let group = Object.values(hexagonGrid<?= $kunik ?>.data[hexaData.text]);
                hexagonGrid<?= $kunik ?>.drawedHex = {};
                hexagonGrid<?= $kunik ?>.drawedTitle = {};
                hexagonGrid<?= $kunik ?>.navigation = hexaData.text;
                hexagonGrid<?= $kunik ?>.clearCanvas();
                hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(
                  group, tile, false, false, 3);
                hexagonGrid<?= $kunik ?>.fitBounds();
                hexagonGrid<?= $kunik ?>.addBreadCrumb();
              }else{
                var clickedHex = this.drawedHex[`${tile.column},${tile.row}`];
                if (typeof clickedHex.extra != "undefined" && typeof clickedHex.extra.description != "undefined" && clickedHex.extra.description != "") {
                  $("#hexagone-detail-preview .hexagone-content-preview").html(clickedHex.extra.description);
                  $("#hexagone-detail-preview .page-title").text(clickedHex.text);
                  $("#hexagone-detail-preview").css({
                    "top": $("#mainNav").height() + "px",
                  });
                  let url = typeof clickedHex.extra.url != "undefined" ? clickedHex.extra.url : "";
                  if (url != "") {
                    $(".btn-open-url").attr("href", url).removeClass("hidden");
                  } else {
                    $(".btn-open-url").addClass("hidden");
                  }

                  $("#hexagone-detail-preview").show();
                } else if (typeof clickedHex.extra != "undefined" && typeof clickedHex.extra.url != "undefined" && clickedHex.extra.url != "") {
                  window.open(clickedHex.extra.url, "_blank");
                }
              }
            }
          } else {
            var clickedHex = this.drawedHex[`${tile.column},${tile.row}`];
            if (typeof clickedHex.extra != "undefined" && typeof clickedHex.extra.description != "undefined" && clickedHex.extra.description != "") {
              $("#hexagone-detail-preview .hexagone-content-preview").html(clickedHex.extra.description);
              $("#hexagone-detail-preview .page-title").text(clickedHex.text);
              $("#hexagone-detail-preview").css({
                "top": $("#mainNav").height() + "px",
              });
              let url = typeof clickedHex.extra.url != "undefined" ? clickedHex.extra.url : "";
              if (url != "") {
                $(".btn-open-url").attr("href", url).removeClass("hidden");
              } else {
                $(".btn-open-url").addClass("hidden");
              }

              $("#hexagone-detail-preview").show();
            } else if (typeof clickedHex.extra != "undefined" && typeof clickedHex.extra.url != "undefined" && clickedHex.extra.url != "") {
              window.open(clickedHex.extra.url, "_blank");
            }
          }
        }
      }
    }, 200);

  };

  function updateHexagoneData(data) {
    hexagoneDescription = data;
    $("#modal-preview-coop").empty();
    $("#modal-preview-coop").hide();
  }

  function reorganizeParents(parents){
    let parentToDelete = [];
    for (const parentName in parents) {
      const parentData = parents[parentName];
      for (const otherParentName in parents) {
        if (otherParentName === parentName) continue;

        const otherParentData = parents[otherParentName];
        if (otherParentData.childs.some(child => child.text === parentName)) {
          const childIndex = otherParentData.childs.findIndex(child => child.text === parentName);
          otherParentData.childs[childIndex].childs = parentData.childs;
          parentToDelete.push(parentName);
          break;
          
        }
      }
    }
    parentToDelete.forEach((v) => delete parents[v]);
    return parents;
  }

  
  // function reorganizeParents(parents) {
  //   function isChildRecursive(parents, parentName, targetParentName) {
  //     const parentData = parents[targetParentName];
  //     if (!parentData) return false;

  //     const isDirectChild = parentData.childs.some(child => child.text === parentName);
  //     if (isDirectChild) return true;

  //     for (const child of parentData.childs) {
  //       if (isChildRecursive(parents, parentName, child.text)) {
  //         return true;
  //       }
  //     }

  //     return false;
  //   }
  //   for (const parentName in parents) {
  //     const parentData = parents[parentName];
  //     for (const otherParentName in parents) {
  //       if (otherParentName === parentName) continue;

  //       const otherParentData = parents[otherParentName];
  //       /* const childIndex = otherParentData.childs.findIndex(child => child.text === parentName);
  //       if (childIndex !== -1) {
  //         otherParentData.childs[childIndex].childs = parentData.childs;
  //         delete parents[parentName];

  //         break;
  //       } */
  //         if (isChildRecursive(parents, parentName, otherParentName)) {
  //           const childIndex = otherParentData.childs.findIndex(child => child.text === parentName);
  //           if (childIndex !== -1) {
  //               otherParentData.childs[childIndex].childs = parentData.childs;
  //               delete parents[parentName];
  //               break; 
  //           }
  //       }
  //     }
  //   }
  //   return parents;
  // }

  function organizeData(data) {
    const parents = {};
    for (const key in data) {
      const hexagon = data[key];
      const groupName = hexagon.group;

      if (groupName) {
        if (!parents[groupName]) {
          parents[groupName] = {
            parent: null,
            childs: []
          };
        }

        if (hexagon.text === groupName) {
          parents[groupName].parent = hexagon;
        } else {
          parents[groupName].childs.push(hexagon);
        }
        if (typeof parents[hexagon.text] != "undefined") {
          parents[hexagon.text].parent = hexagon;
        }
      } else {
        if (Object.keys(parents).includes(hexagon.text)) {
          parents[hexagon.text].parent = hexagon;
        } else {
          parents[hexagon.text] = {
            parent: hexagon,
            childs: []
          };
        }
      }
    }

    $.each(parents, function(key, value) {
      if (parents[key].parent == null) {
        parents[key].parent = {
          "group": key,
          "text": key,
          "color": "white",
        }
      }
      parents[key] = {
        childs: parents[key].childs,
        ...parents[key].parent
      }
    });
    return reorganizeParents(parents);
  }

  function dragHexagoneData() {
    $("#hexagone-filter-container").empty();
    if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
      let hexagoneToDrag<?= $kunik ?> = organizeData(hexagonData);
      hexagonGrid<?= $kunik ?>.data["root"] = Object.values(hexagoneToDrag<?= $kunik ?>);
      hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(Object.values(hexagoneToDrag<?= $kunik ?>), {
        column: 0,
        row: 0
      }, false, false, 2);
      hexagonGrid<?= $kunik ?>.fitBounds();
      hexagonGrid<?= $kunik ?>.addBreadCrumb()
      // bindContextMenu();
    } else if ((<?= json_encode($blockCms["groupped"]) ?> == true || <?= json_encode($blockCms["groupped"]) ?> == "true")) {
      let hexagoneToDrag<?= $kunik ?> = {};
      $.each(hexagonData, function(key, value) {
        if (typeof value.group != "undefined" && value.group != "") {
          if (typeof hexagoneToDrag<?= $kunik ?>[value.group] == "undefined") {
            hexagoneToDrag<?= $kunik ?>[value.group] = {
              "group": value.group,
              "childs": []
            };
          }
          hexagoneToDrag<?= $kunik ?>[value.group].childs.push(value);
        } else {
          if (typeof hexagoneToDrag<?= $kunik ?>["Sans groupe"] == "undefined") {
            hexagoneToDrag<?= $kunik ?>["Sans groupe"] = {
              "group": "Sans groupe",
              "childs": []
            };
          }
          hexagoneToDrag<?= $kunik ?>["Sans groupe"].childs.push(value);
        }
      });

      $.each(hexagoneToDrag<?= $kunik ?>, function(key, child) {
        let btn = `<div class="btn margin-5 btn-hexa-container">
          <button class="btn-expand" data-toggle="collapse" href="#${hexagonGrid<?= $kunik ?>.slugify(key)}">
            <i class="fa fa-chevron-down"></i>
          </button>
          <button class="btn-hexagone-zoomer" data-type="group">
            ${key}
            <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${hexagoneToDrag<?= $kunik ?>[key].childs.length}</span>
          </button>
        </div>
        <div class="collapse hexagone-child-container" id="${hexagonGrid<?= $kunik ?>.slugify(key)}">
        `;
        hexagoneToDrag<?= $kunik ?>[key].childs.forEach((child) => {
          btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonGrid<?= $kunik ?>.slugify(child.text)}'>
            ${child.text}
          </button>`;
        })
        btn += `</div>`;
        $(`#hexagone-filter-container`).append(btn);
      })
      hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(Object.values(hexagoneToDrag<?= $kunik ?>), hexagonGrid<?= $kunik ?>.getCenterTile(), true);
      hexagonGrid<?= $kunik ?>.fitBounds();
    } else {
      $.each(hexagonData, function(key, value) {
        hexagonGrid<?= $kunik ?>.drawHexAtColRow(value.column, value.row, value.color, value.text, value.image, value.radius, true, {}, null, value);
      });
      $.each(hexagonData, function(key, value) {
        let btn = `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonGrid<?= $kunik ?>.slugify(value.text)}'>
          ${value.text}
        </button>`;
        $(`#hexagone-filter-container`).append(btn);
      });
    }
  }

  var hexagonGrid<?= $kunik ?> = null;
  $(function() {
    hexagonGrid<?= $kunik ?> = new HexagonGrid("HexCanvas<?= $kunik ?>", <?= json_encode($blockCms["circle"]) ?>, (<?= json_encode($blockCms["navigate"]) ?> == "depth" ? window.innerWidth : window.innerWidth - 300), hexaMode);
    hexagonGrid<?= $kunik ?>.setBreadCrumbContainer("#hexagone-breadcrumbs-<?= $kunik ?>");
    hexagonGrid<?= $kunik ?>.setType(<?= json_encode($blockCms["navigate"]) ?>);
    hexagonGrid<?= $kunik ?>.setDBClickOption(false);
    hexagonGrid<?= $kunik ?>.setLineStroke(<?= $blockCms["css"]["fillWidth"] ?>);
    hexagonGrid<?= $kunik ?>.setColorStroke("<?= $blockCms["css"]["fillColor"] ?>");
    // var center = hexagonGrid<?= $kunik ?>.getCenterTile();
    // hexagonGrid<?= $kunik ?>.drawHexAtColRow(center.column, center.row, "transparent", "", costum.logo);
    (new Promise((resolve, reject) => {
      if (hexagonData != null) {
        dragHexagoneData();
        resolve();
      } else {
        resolve();
      }
    })).then(function() {
      hexagonGrid<?= $kunik ?>.fitBounds();
      bindBtnClick();
    });
    $(".button-switch").off("click").on("click", function() {
      hexaMode = !hexaMode;
      hexagonGrid<?= $kunik ?>.switchMode(hexaMode);
      if (hexaMode) {
        $(".button-switch").text("Changer en mode édition");
      } else {
        $(".button-switch").text("Changer en mode preview");
      }
    });
    bindContextMenu();
  })

  function bindBtnClick() {
    $(".btn-hexagone-zoomer").off().on('click', function() {
      if ($(this).data("type") == "group") {
        if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
          $(".btn-hexa-container").removeClass("btn-primary");
          hexagonGrid<?= $kunik ?>.zoomTo = [];
          hexagonGrid<?= $kunik ?>.fitBounds();
        } else {
          let group = $(this).find("span").data("countvalue");
          $(".btn-hexa-container").removeClass("btn-primary");
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          $(this).parents(".btn-hexa-container").addClass("btn-primary");
          hexagonGrid<?= $kunik ?>.zoomToGroup(group);
        }
      } else {
        if ($(this).hasClass("btn-primary")) {
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          hexagonGrid<?= $kunik ?>.zoomTo = [];
          hexagonGrid<?= $kunik ?>.fitBounds();
        } else {
          let texte = $(this).data("value");
          $(".btn-hexa-container").removeClass("btn-primary");
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          $(this).addClass("btn-primary");
          let element = Object.values(hexagonGrid<?= $kunik ?>.drawedHex).find(hex => {
            return hexagonGrid<?= $kunik ?>.slugify(hex.text) == texte;
          });
          console.log("element", element);
          if (typeof element != "undefined") {
            hexagonGrid<?= $kunik ?>.zoomToElement(element.column, element.row);
          }
        }
      }
    })
    $(".btn-close-detail").off("click").on("click", function() {
      $("#hexagone-detail-preview").hide();
    });
  }

  function bindContextMenu() {
    if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
      $("#HexCanvas<?= $kunik ?>").on('contextmenu', function(e) {
        e.preventDefault();
        if (hexagonGrid<?= $kunik ?>.navigationStack.length > 0) {
          const previousState = hexagonGrid<?= $kunik ?>.navigationStack.pop();
          hexagonGrid<?= $kunik ?>.scale = previousState.scale;
          hexagonGrid<?= $kunik ?>.offsetX = previousState.offsetX;
          hexagonGrid<?= $kunik ?>.offsetY = previousState.offsetY;
          hexagonGrid<?= $kunik ?>.drawedHex = {};
          hexagonGrid<?= $kunik ?>.drawedTitle = {};
          hexagonGrid<?= $kunik ?>.navigation = previousState.zoomTo;
          hexagonGrid<?= $kunik ?>.clearCanvas();
          // Redessiner la vue
          if (hexagonGrid<?= $kunik ?>.preview === false) {
            hexagonGrid<?= $kunik ?>.drawHexEditMode();
          }
          let group = Object.values(hexagonGrid<?= $kunik ?>.data[previousState.zoomTo]);
          hexagonGrid<?= $kunik ?>.popBreadCrumb()
          if (previousState.zoomTo == "root") {
            hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 2);
          } else {
            hexagonGrid<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 3);
          }

        }
      })
    }
  }

  // mylog.log("hexagonGrid<?= $kunik ?>", );
</script>