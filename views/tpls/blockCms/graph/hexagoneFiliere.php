<?php
$keyTpl = "hexagoneFiliere";
$blockId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];

$graphAssets = [
  '/js/hexagon.js'
];
HtmlHelper::registerCssAndScriptsFiles(
  $graphAssets,
  Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
);
?>

<style>
  .d-none {
    display: none !important;
  }

  .modal.hexagon-choose-image {
    text-align: center;
    background: #0000009c;
  }

  @media screen and (min-width: 768px) {
    .modal.hexagon-choose-image:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .hexagon-choose-image .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }

  .<?= $kunik ?> {
    position: relative;
  }

  .d-none {
    display: none !important;
  }


  .btn-hexa-container {
    background: white;
    border: 1px solid;
    padding: 0;
    text-align: left;
  }

  .btn-hexa-container.btn-primary {
    color: white;
    background-color: #2C3E50;
    border-color: #2C3E50;
    font-weight: 700;
  }

  .btn-hexa-container button {
    background: none;
    border: none;
    padding: 6px 12px;
  }

  .tab-pane #hexagone {
    position: relative;
  }

  .hexagone-child-container.in {
    display: flex;
    flex-direction: column;
    text-align: left;
  }

  .hexagone-child-container {
    padding: 0px 15px;
  }

  .hexagone-child-container .btn-hexagone-zoomer {
    text-align: left;
  }

  .hexagone-filiere-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: stretch;
    align-content: stretch;
  }

  .filters-container-circle-<?= $kunik ?> {
    width: 300px;
  }

  .filters-container-circle-<?= $kunik ?> #hexagone-filter-container-<?= $kunik ?> {
    display: flex;
    flex-direction: column;
    width: 300px;
    background-color: white;
    overflow-y: auto;
    height: 700px;
  }


  #hexagone-detail-preview {
    padding-top: 0%;
    padding-right: 9%;
    padding-left: 9%;
    position: fixed;
    background: rgba(255, 255, 255, 0.89);
    width: 100%;
    z-index: 8;
    padding-bottom: 10% !important;
  }

  #hexagone-detail-preview .page-title {
    font-weight: 700;
    font-size: 24px;
    margin: 1% 0%;
    color: #244A58;
  }

  .hexagone-prev-header {
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
  }

  .hexagone-content-preview {
    height: 65vh !important;
    box-shadow: -10px -5px 34px 0px #0000001C;
    border-radius: 14px;
    overflow-y: auto;
    background: white;
    padding: 20px 20px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> ul {
    list-style: none;
    display: inline-block;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> .icon {
    font-size: 14px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li {
    float: left;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a {
    color: #FFF;
    display: block;
    background: #3498db;
    text-decoration: none;
    position: relative;
    height: 40px;
    line-height: 40px;
    padding: 0 10px 0 5px;
    text-align: center;
    margin-right: 23px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a {
    background-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:before {
    border-color: #2980b9;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:after {
    border-left-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a {
    padding-left: 15px;
    -moz-border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px 0 0 4px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a:before {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a {
    padding-right: 15px;
    -moz-border-radius: 0 4px 4px 0;
    -webkit-border-radius: 0;
    border-radius: 0 4px 4px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a:after {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before,
  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    content: "";
    position: absolute;
    top: 0;
    border: 0 solid #3498db;
    border-width: 20px 10px;
    width: 0;
    height: 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before {
    left: -20px;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    left: 100%;
    border-color: transparent;
    border-left-color: #3498db;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover {
    background-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:before {
    border-color: #1abc9c;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:after {
    border-left-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active {
    background-color: #16a085;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:before {
    border-color: #16a085;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:after {
    border-left-color: #16a085;
  }


  .btn-close-detail,
  .btn-open-url {
    margin-top: 20px;
    margin-left: 20px;
    box-shadow: 0px 4px 18px 0px #0000001C;
    padding: 13px;
    font-weight: bold;
    border-radius: 15px;
    color: #000 !important;
  }
</style>
<div class="<?= $kunik ?>">
  <div id="hexagone-breadcrumbs-<?= $kunik ?>" style="margin: 20px auto;"></div>
  <div class="hexagone-filiere-wrapper">
    <canvas id="hexFiliereCanvas<?= $kunik ?>" class="hexContainerCanvas" height="700"></canvas>
    <div class="filters-container-circle-<?= $kunik ?> hidden-sm hidden-xs">
      <div id="hexagone-filter-container-<?= $kunik ?>" class="margin-top-10 text-center"></div>
    </div>
  </div>
</div>
<script>
    var allThem<?= $kunik ?> = {
        "alimentation" : {
            "name" : "Food",
            "icon" : "fa-cutlery",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_alimentation.png`,
            "tags" : [ 
                "agriculture", 
                "alimentation", 
                "nourriture", 
                "AMAP"
            ]
        },
        "santé" : {
            "name" : "Health",
            "icon" : "fa-heart-o",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_sante.png`,
            "tags" : [ 
                "santé"
            ]
        },
        "déchets" : {
            "name" : "Waste",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_dechets.png`,
            "icon" : "fa-trash-o ",
            "tags" : [ 
                "déchets"
            ]
        },
        "transport" : {
            "name" : "Transport",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_transport.png`,
            "icon" : "fa-bus",
            "tags" : [ 
                "Urbanisme", 
                "transport", 
                "construction"
            ]
        },
        "éducation" : {
            "name" : "Education",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_education.png`,
            "icon" : "fa-book",
            "tags" : [ 
                "éducation", 
                "petite enfance"
            ]
        },
        "citoyenneté" : {
            "name" : "Citizenship",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_citoyennete.png`,
            "icon" : "fa-user-circle-o",
            "tags" : [ 
                "citoyen", 
                "society"
            ]
        },
        "Économie" : {
            "name" : "Economy",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_economie.png`,
            "icon" : "fa-money",
            "tags" : [ 
                "ess", 
                "économie social solidaire"
            ]
        },
        "énergie" : {
            "name" : "Energy",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_energie.png`,
            "icon" : "fa-sun-o",
            "tags" : [ 
                "énergie", 
                "climat"
            ]
        },
        "culture" : {
            "name" : "Culture",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_culture.png`,
            "icon" : "fa-universal-access",
            "tags" : [ 
                "culture", 
                "animation"
            ]
        },
        "environnement" : {
            "name" : "Environnement",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_environnement.png`,
            "icon" : "fa-tree",
            "tags" : [ 
                "environnement", 
                "biodiversité", 
                "écologie"
            ]
        },
        "numérique" : {
            "name" : "Numeric",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_numerique.png`,
            "icon" : "fa-laptop",
            "tags" : [ 
                "informatique", 
                "tic", 
                "internet", 
                "web",
                "numérique"
            ]
        },
        "sport" : {
            "name" : "Sport",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_sport.png`,
            "icon" : "fa-futbol-o",
            "tags" : [ 
                "sport"
            ]
        },
        "tiers lieux" : {
            "name" : "Third places",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_alimentation.png`,
            "icon" : "fa-globe",
            "tags" : [
                "TiersLieux"
            ]
        },
        "pacte" : {
            "name" : "Pact",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_pacte transition.png`,
            "icon" : "fa-hand-o-up",
            "tags" : [
                "pacte"
            ]
        },
        "associations" : {
            "name" : "Associations",
            "image" : `${modules.costum.url}/images/cocity/icones_filieres/i_associations.png`,
            "icon" : "fa-chain",
            "tags" : [
                "associations"
            ]
        }
    }

    // var allSugThematic = [
    //     tradTags["food"],
    //     tradTags["health"] ,
    //     tradTags["associations"],
    //     tradTags["pact"],
    //     tradTags["third places"],
    //     tradTags["transport"] ,
    //     tradTags["education"],
    //     tradTags["citizenship"],
    //     tradTags["economy"],
    //     tradTags["energy"],
    //     tradTags["culture"],
    //     tradTags["environment"],
    //     tradTags["numeric"],
    //     tradTags["sport"],
    //     tradTags["waste"] 
    // ];
    var hexagoneFiliere<?= $kunik ?> = null;
    var hexagoneDataFiliere<?= $kunik ?> = <?= json_encode($blockCms["data"]) ?>;
    HexagonGrid.prototype.clickEvent = function(e) {
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        var localX = mouseX - this.canvasOriginX;
        var localY = mouseY - this.canvasOriginY;

        var tile = this.getSelectedTile(localX, localY);
        if (this.hasHexagon(tile.column, tile.row)) {
            let hexaData = this.drawedHex[`${tile.column},${tile.row}`];
            if ((<?= json_encode($blockCms["navigate"]) ?> == "depth")) {
                if (typeof this.data[hexaData.text] != "undefined" && this.data[hexaData.text].length > 0) {
                  this.navigationStack.push({
                      scale: this.scale,
                      offsetX: this.offsetX,
                      offsetY: this.offsetY,
                      zoomTo: this.navigation
                  });
                  let group = Object.values(this.data[hexaData.text]);
                  this.drawedHex = {};
                  this.drawedTitle = {};
                  this.navigation = hexaData.text;
                  this.clearCanvas();
                  if(group.length > 100){
                    this.drawHexGridFromList(group, 100, 100);
                  }else{
                    this.placeGroupsWithOffsets(
                      group, tile, false, false, 3);
                  }
                  this.fitBounds();
                  this.addBreadCrumb();
                } else {
                    var url = `#page.type.${hexaData.extra.collection}.id.${hexaData.extra.id}`;
                    hashT = location.hash.split("?");
                    getStatus = urlCtrl.getUrlSearchParams();

                    hashNav = (hashT[0].indexOf("#") < 0) ? "#" + hashT[0] : hashT[0];
                    urlHistoric = hashNav + "?preview=" + hexaData.extra.collection + "." + hexaData.extra.id;
                    if (getStatus != "") urlHistoric += "&" + getStatus;
                    history.replaceState({}, null, urlHistoric);
                    urlCtrl.openPreview(url);
                }
            }
        }
    }
    function findKeyByTag(obj, tag) {
        const normalizedTag = normalizeString(tag); // Normalise le tag pour la recherche insensible à la casse et aux accents
        
        // Parcourt chaque clé et vérifie les tags
        for (const key in obj) {
            if (obj[key].tags.some(t => normalizeString(t) === normalizedTag)) {
                return key; // Retourne la clé correspondante
            }
        }
        return null; // Retourne null si aucune correspondance trouvée
    }
    function groupFiliereData(data) {
        let group = {};
        let thematic = <?= json_encode($blockCms["thematic"]) ?>;
        let allThematicData = [];
        thematic.forEach((thematic) => {
            group[thematic] = {
                group: thematic,
                text: thematic,
                image: allThem<?= $kunik ?>[thematic].image,
                childs : []
            };
            allThematicData = allThematicData.concat(allThem<?= $kunik ?>[thematic].tags);
        });

        let thematicSensivity = allThematicData.map((theme) => normalizeString(theme))
        mylog.log("Convert sensivity", thematicSensivity);
        Object.values(data).forEach((element) => {
            if (typeof element.tags != "undefined") {
                element.tags.forEach((tag) => {
                    if(thematicSensivity.includes(normalizeString(tag))){
                        let key = findKeyByTag(allThem<?= $kunik ?>, tag);
                        group[key].childs.push({
                            text: element.name,
                            image: typeof element.profilMediumImageUrl != "undefined" ? element.profilMediumImageUrl : null,
                            slug: element.slug,
                            description: element.description,
                            collection: element.collection,
                            id: element._id.$id,
                            color: `#eeeeee`
                        })
                    }
                });
            }
        });
        for (const key in group) {
            let setData = new Set(group[key].childs.map(JSON.stringify));
            group[key].childs = Array.from(setData).map(JSON.parse);
        }
        return group;
    }
    function normalizeString(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    }

    function bindContextMenu<?= $kunik ?>() {
        if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
            $("#hexFiliereCanvas<?= $kunik ?>").on('contextmenu', function(e) {
                e.preventDefault();
                if (hexagoneFiliere<?= $kunik ?>.navigationStack.length > 0) {
                const previousState = hexagoneFiliere<?= $kunik ?>.navigationStack.pop();
                hexagoneFiliere<?= $kunik ?>.scale = previousState.scale;
                hexagoneFiliere<?= $kunik ?>.offsetX = previousState.offsetX;
                hexagoneFiliere<?= $kunik ?>.offsetY = previousState.offsetY;
                hexagoneFiliere<?= $kunik ?>.drawedHex = {};
                hexagoneFiliere<?= $kunik ?>.drawedTitle = {};
                hexagoneFiliere<?= $kunik ?>.navigation = previousState.zoomTo;
                hexagoneFiliere<?= $kunik ?>.clearCanvas();
                // Redessiner la vue
                if (hexagoneFiliere<?= $kunik ?>.preview === false) {
                    hexagoneFiliere<?= $kunik ?>.drawHexEditMode();
                }
                let group = Object.values(hexagoneFiliere<?= $kunik ?>.data[previousState.zoomTo]);
                hexagoneFiliere<?= $kunik ?>.popBreadCrumb()
                if (previousState.zoomTo == "root") {
                    hexagoneFiliere<?= $kunik ?>.placeGroupsWithOffsets(
                    group, {
                        column: 0,
                        row: 0
                    }, false, false, 2);
                } else {
                    hexagoneFiliere<?= $kunik ?>.placeGroupsWithOffsets(
                    group, {
                        column: 0,
                        row: 0
                    }, false, false, 3);
                }

                }
            })
        }
    }
    function buildBtnView<?= $kunik ?>(data, addChildData){
      let addChild = typeof addChildData != "undefined" ? addChildData : true;
      if(addChild){
        $.each(data, function(key, child) {
          let btn = `<div class="btn margin-5 btn-hexa-container">
            <button class="btn-expand" data-toggle="collapse" href="#${hexagonFiliere<?= $kunik ?>.slugify(key)}">
              <i class="fa fa-chevron-down"></i>
            </button>
            <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  data-type="group">
              ${key}
              <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
            </button>
          </div>
          <div class="collapse hexagone-child-container" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  id="${hexagonFiliere<?= $kunik ?>.slugify(key)}">
          `;
          data[key].childs.forEach((child) => {
            btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?>  data-type="children" data-value='${hexagonFiliere<?= $kunik ?>.slugify(child.text)}'>
              ${child.text}
            </button>`;
          })
          btn += `</div>`;
          $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
        })
      }else{
        $.each(data, function(key, child) {
          if(typeof data[key].childs != "undefined"){
            let btn = `<div class="btn margin-5 btn-hexa-container">
              <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?> data-type="group">
                ${key}
                <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
              </button>
            </div>`;
            $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
          }else{
            let btn = `<div class="btn margin-5 btn-hexa-container">
              <button class="btn-hexagone-zoomer" data-navigate=<?= json_encode($blockCms["navigate"]) ?> data-type="children">
                ${key}
                <span class="badge badge-theme-count margin-left-5 label-primary hidden" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${data[key].childs.length}</span>
              </button>
            </div>`;
            $(`#hexagone-filter-container-<?= $kunik ?>`).append(btn);
          }

        });
      }
    }

    function drawHexagoneFiliere(data){
      buildBtnView<?= $kunik ?>(data, <?= json_encode($blockCms["navigate"]) ?> != "depth");
      if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
        hexagonFiliere<?= $kunik ?>.setBreadCrumbContainer("#hexagone-breadcrumbs-<?= $kunik ?>");
        hexagonFiliere<?= $kunik ?>.setType(<?= json_encode($blockCms["navigate"]) ?>);
        
        hexagonFiliere<?= $kunik ?>.data["root"] = Object.values(data);
        hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(Object.values(data), {
          column: 0,
          row: 0
        }, false, false, 2);
        hexagonFiliere<?= $kunik ?>.fitBounds();
        hexagonFiliere<?= $kunik ?>.addBreadCrumb()
      } else {
        
        hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(Object.values(data), hexagonFiliere<?= $kunik ?>.getCenterTile());
        hexagonFiliere<?= $kunik ?>.fitBounds();
      }
    }

  function bindBtnClick<?= $kunik ?>() {
      $(".btn-hexagone-zoomer").off().on('click', function() {
        if($(this).data("navigate") ==  "depth"){
          if ($(this).data("type") == "group") {
            if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
              $(".btn-hexa-container").removeClass("btn-primary");
              if (hexagonFiliere<?= $kunik ?>.navigationStack.length > 0) {
                const previousState = hexagonFiliere<?= $kunik ?>.navigationStack.pop();
                hexagonFiliere<?= $kunik ?>.scale = previousState.scale;
                hexagonFiliere<?= $kunik ?>.offsetX = previousState.offsetX;
                hexagonFiliere<?= $kunik ?>.offsetY = previousState.offsetY;
                hexagonFiliere<?= $kunik ?>.drawedHex = {};
                hexagonFiliere<?= $kunik ?>.drawedTitle = {};
                hexagonFiliere<?= $kunik ?>.navigation = previousState.zoomTo;
                hexagonFiliere<?= $kunik ?>.clearCanvas();
                // Redessiner la vue
                if (hexagonFiliere<?= $kunik ?>.preview === false) {
                  hexagonFiliere<?= $kunik ?>.drawHexEditMode();
                }
                let group = Object.values(hexagonFiliere<?= $kunik ?>.data[previousState.zoomTo]);
                hexagonFiliere<?= $kunik ?>.popBreadCrumb()
                if (previousState.zoomTo == "root") {
                  hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(
                    group, {
                      column: 0,
                      row: 0
                    }, false, false, 2);
                } else {
                  if(group.length > 100){
                    hexagonFiliere<?= $kunik ?>.drawHexGridFromList(group, 100, 100);
                  }else{
                    hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(
                      group, {
                        column: 0,
                        row: 0
                      }, false, false, 3);
                  }
                }

              }
            } else {
              let groupText = $(this).find("span").data("countvalue");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).parents(".btn-hexa-container").addClass("btn-primary");
              hexagonFiliere<?= $kunik ?>.navigationStack.push({
                scale: hexagonFiliere<?= $kunik ?>.scale,
                offsetX: hexagonFiliere<?= $kunik ?>.offsetX,
                offsetY: hexagonFiliere<?= $kunik ?>.offsetY,
                zoomTo: hexagonFiliere<?= $kunik ?>.navigation
              });
              let group = Object.values(hexagonFiliere<?= $kunik ?>.data[groupText]);
              hexagonFiliere<?= $kunik ?>.drawedHex = {};
              hexagonFiliere<?= $kunik ?>.drawedTitle = {};
              hexagonFiliere<?= $kunik ?>.navigation = groupText;
              hexagonFiliere<?= $kunik ?>.clearCanvas();
              // hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(
              //   group, {column:0, row:0}, false, false, 3);
              if(group.length > 100){
                hexagonFiliere<?= $kunik ?>.drawHexGridFromList(group, 100, 100);
              }else{
                hexagonFiliere<?= $kunik ?>.placeGroupsWithOffsets(
                  group, {
                    column: 0,
                    row: 0
                  }, false, false, 3);
              }
              hexagonFiliere<?= $kunik ?>.fitBounds();
              hexagonFiliere<?= $kunik ?>.addBreadCrumb();
            }
          } else {
            if ($(this).hasClass("btn-primary")) {
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              hexagonFiliere<?= $kunik ?>.zoomTo = [];
              hexagonFiliere<?= $kunik ?>.fitBounds();
            } else {
              let texte = $(this).data("value");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).addClass("btn-primary");
              let element = Object.values(hexagonFiliere<?= $kunik ?>.drawedHex).find(hex => {
                return hexagonFiliere<?= $kunik ?>.slugify(hex.text) == texte;
              });
              if (typeof element != "undefined") {
                hexagonFiliere<?= $kunik ?>.zoomToElement(element.column, element.row);
              }
            }
          }
        }else{
          if ($(this).data("type") == "group") {
            if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
              $(".btn-hexa-container").removeClass("btn-primary");
              hexagonFiliere<?= $kunik ?>.zoomTo = [];
              hexagonFiliere<?= $kunik ?>.fitBounds();
            } else {
              let group = $(this).find("span").data("countvalue");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).parents(".btn-hexa-container").addClass("btn-primary");
              hexagonFiliere<?= $kunik ?>.zoomToGroup(group);
            }
          } else {
            if ($(this).hasClass("btn-primary")) {
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              hexagonFiliere<?= $kunik ?>.zoomTo = [];
              hexagonFiliere<?= $kunik ?>.fitBounds();
            } else {
              let texte = $(this).data("value");
              $(".btn-hexa-container").removeClass("btn-primary");
              $(".btn-hexagone-zoomer").removeClass("btn-primary");
              $(this).addClass("btn-primary");
              let element = Object.values(hexagonFiliere<?= $kunik ?>.drawedHex).find(hex => {
                return hexagonFiliere<?= $kunik ?>.slugify(hex.text) == texte;
              });
              if (typeof element != "undefined") {
                hexagonFiliere<?= $kunik ?>.zoomToElement(element.column, element.row);
              }
            }
          }
        }
      })
    }
  
    $(function(){
        hexagonFiliere<?= $kunik ?> = new HexagonGrid("hexFiliereCanvas<?= $kunik ?>", <?= json_encode($blockCms["circle"]) ?>, window.innerWidth - 300);
        mylog.log('Groupped filiere data', groupFiliereData(hexagoneDataFiliere<?= $kunik ?>));
        drawHexagoneFiliere(groupFiliereData(hexagoneDataFiliere<?= $kunik ?>));
        bindBtnClick<?= $kunik ?>();
        bindContextMenu<?= $kunik ?>();
    })
</script>