<?php
$keyTpl = "hexagoneMapping";
$blockId  = $blockCms["_id"]->{'$id'};
$objectCss = $blockCms["css"] ?? [];
$styleCss  = (object) [$kunik => $objectCss];

$graphAssets = [
  '/js/hexagon.js'
];
HtmlHelper::registerCssAndScriptsFiles(
  $graphAssets,
  Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
);
?>
<style>
  .d-none {
    display: none !important;
  }

  .modal.hexagon-choose-image {
    text-align: center;
    background: #0000009c;
  }

  @media screen and (min-width: 768px) {
    .modal.hexagon-choose-image:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .hexagon-choose-image .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }

  .<?= $kunik ?> {
    position: relative;
  }

  .d-none {
    display: none !important;
  }


  .btn-hexa-container {
    background: white;
    border: 1px solid;
    padding: 0;
    text-align: left;
  }

  .btn-hexa-container.btn-primary {
    color: white;
    background-color: #2C3E50;
    border-color: #2C3E50;
    font-weight: 700;
  }

  .btn-hexa-container button {
    background: none;
    border: none;
    padding: 6px 12px;
  }

  .tab-pane #hexagone {
    position: relative;
  }

  .hexagone-child-container.in {
    display: flex;
    flex-direction: column;
    text-align: left;
  }

  .hexagone-child-container {
    padding: 0px 15px;
  }

  .hexagone-child-container .btn-hexagone-zoomer {
    text-align: left;
  }

  .hexagone-wrapper {
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: stretch;
    align-content: stretch;
  }

  .filters-container-circle {
    width: 300px;
  }

  .filters-container-circle #hexagone-filter-container {
    display: flex;
    flex-direction: column;
    width: 300px;
    background-color: white;
    overflow-y: auto;
    height: 700px;
  }


  #hexagone-detail-preview {
    padding-top: 0%;
    padding-right: 9%;
    padding-left: 9%;
    position: fixed;
    background: rgba(255, 255, 255, 0.89);
    width: 100%;
    z-index: 8;
    padding-bottom: 10% !important;
  }

  #hexagone-detail-preview .page-title {
    font-weight: 700;
    font-size: 24px;
    margin: 1% 0%;
    color: #244A58;
  }

  .hexagone-prev-header {
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
  }

  .hexagone-content-preview {
    height: 65vh !important;
    box-shadow: -10px -5px 34px 0px #0000001C;
    border-radius: 14px;
    overflow-y: auto;
    background: white;
    padding: 20px 20px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> ul {
    list-style: none;
    display: inline-block;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> .icon {
    font-size: 14px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li {
    float: left;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a {
    color: #FFF;
    display: block;
    background: #3498db;
    text-decoration: none;
    position: relative;
    height: 40px;
    line-height: 40px;
    padding: 0 10px 0 5px;
    text-align: center;
    margin-right: 23px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a {
    background-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:before {
    border-color: #2980b9;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:nth-child(even) a:after {
    border-left-color: #2980b9;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a {
    padding-left: 15px;
    -moz-border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px 0 0 4px;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:first-child a:before {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a {
    padding-right: 15px;
    -moz-border-radius: 0 4px 4px 0;
    -webkit-border-radius: 0;
    border-radius: 0 4px 4px 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li:last-child a:after {
    border: none;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before,
  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    content: "";
    position: absolute;
    top: 0;
    border: 0 solid #3498db;
    border-width: 20px 10px;
    width: 0;
    height: 0;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:before {
    left: -20px;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:after {
    left: 100%;
    border-color: transparent;
    border-left-color: #3498db;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover {
    background-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:before {
    border-color: #1abc9c;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:hover:after {
    border-left-color: #1abc9c;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active {
    background-color: #16a085;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:before {
    border-color: #16a085;
    border-left-color: transparent;
  }

  #hexagone-breadcrumbs-<?= $kunik ?> li a:active:after {
    border-left-color: #16a085;
  }


  .btn-close-detail,
  .btn-open-url {
    margin-top: 20px;
    margin-left: 20px;
    box-shadow: 0px 4px 18px 0px #0000001C;
    padding: 13px;
    font-weight: bold;
    border-radius: 15px;
    color: #000 !important;
  }
</style>
<div class="<?= $kunik ?>">
  <div id="hexagone-breadcrumbs-<?= $kunik ?>" style="margin: 20px auto;"></div>
  <div class="hexagone-wrapper">
    <canvas id="HexCanvas<?= $kunik ?>" class="hexContainerCanvas" height="700"></canvas>
    <div class="filters-container-circle hidden-sm hidden-xs">
      <div id="hexagone-filter-container" class="margin-top-10 text-center"></div>
    </div>
  </div>
</div>


<script>
  var optionsLists = [];
  if (costum.editMode) {
    cmsConstructor.sp_params["<?= $blockId ?>"] = <?= json_encode($blockCms) ?>;
    hexagonMappingInput = {
      configTabs: {
        general: {
          inputsConfig: [{
              type: "selectMultiple",
              options: {
                name: "elementsType",
                label: tradCms.mapElement,
                options: [{
                    value: "organizations",
                    label: trad.organizations
                  },
                  {
                    value: "citoyens",
                    label: tradCategory.citizen
                  },
                  {
                    value: "events",
                    label: trad.events
                  },
                  {
                    value: "projects",
                    label: trad.projects
                  },
                  {
                    value: "poi",
                    label: trad.poi
                  }
                ]
              }
            },
            {
              type: "number",
              options: {
                name: "circle",
                class: "col-sm-12",
                label: "Taille de l'hexagone",
                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].circle,
                filterValue: cssHelpers.form.rules.checkLengthProperties
              }
            },
            {
              type: "select",
              options: {
                name: "navigate",
                class: "col-sm-12",
                label: "Type de navigation",
                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].navigate,
                options: [{
                    "label": "Disposition grouppé",
                    "value": "normal"
                  },
                  {
                    "label": "Navigation en profondeur",
                    "value": "depth"
                  }
                ]
              }
            },
          ]
        },
        advanced: {
          inputsConfig: [{
              type: "inputSimple",
              options: {
                name: "groupVar",
                label: 'Variable pour le groupage'
              }
            },
            {
              type: "inputMultiple",
              options: {
                label: "Les éléments à afficher",
                name: "groupList",
                class: `groupList-${cmsConstructor.kunik}`,
                inputs: [
                  [{
                    type: "select",
                    options: {
                      label: tradCms.label,
                      name: "label",
                      isSelect2: true,
                      options: optionsLists.map((value) => {
                        return {
                          value: value,
                          label: value
                        }
                      })
                    }
                  }]
                ]
              }
            },
          ]
        }
      },
      afterSave: function(path, valueToSet, name, payload, value) {
        cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='" + cmsConstructor.spId + "']");
        if(name == "elementsType"){
          $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
        }
      }
    }

    cmsConstructor.blocks.hexagoneMapping<?= $blockKey ?> = hexagonMappingInput;
  }

  function groupDataMapping<?= $kunik ?>(data) {
    let groupVar = <?= json_encode($blockCms["advanced"]["groupVar"]) ?>;
    let groupList = <?= json_encode($blockCms["advanced"]["groupList"]) ?>;
    if(groupList.length > 0){
      groupList = groupList.map((value) => value.label);
    }
    let groupData = {};
    optionsLists = [];
    $.each(data, function(key, value) {
      if (typeof value[groupVar] != "undefined") {
        value[groupVar].forEach(element => {
          optionsLists.push(element);
          if(groupList.length > 0 && groupList.indexOf(element) == -1){
            return;
          }
          if (groupData[element] == undefined) {
            groupData[element] = {
              group: element,
              childs: []
            };
          }
          groupData[element].childs.push({
            text: value.name,
            image: typeof value.profilImageUrl != "undefined" ? value.profilImageUrl : null,
            slug: value.slug,
            description: value.description,
            collection: value.collection,
            id: value._id.$id,
            color: `#eeeeee`
          });
        });
      }
    });
    if(costum.editMode){
      hexagonMappingInput.configTabs.advanced.inputsConfig[1].options.inputs[0][0].options.options = optionsLists.map((value) => {
        return {
          value: value,
          label: value
        }
      });
    }
    return groupData;
  }

  HexagonGrid.prototype.clickEvent = function(e) {
    var mouseX = e.pageX;
    var mouseY = e.pageY;

    var localX = mouseX - this.canvasOriginX;
    var localY = mouseY - this.canvasOriginY;

    var tile = this.getSelectedTile(localX, localY);
    if (this.hasHexagon(tile.column, tile.row)) {
      let hexaData = this.drawedHex[`${tile.column},${tile.row}`];
      if ((<?= json_encode($blockCms["navigate"]) ?> == "depth")) {
        if (typeof this.data[hexaData.text] != "undefined" && this.data[hexaData.text].length > 0) {
          this.navigationStack.push({
            scale: this.scale,
            offsetX: this.offsetX,
            offsetY: this.offsetY,
            zoomTo: this.navigation
          });
          let group = Object.values(this.data[hexaData.text]);
          this.drawedHex = {};
          this.drawedTitle = {};
          this.navigation = hexaData.text;
          this.clearCanvas();
          this.placeGroupsWithOffsets(
            group, tile, false, false, 3);
          this.fitBounds();
          this.addBreadCrumb();
        } else {
            var url = `#page.type.${hexaData.extra.collection}.id.${hexaData.extra.id}`;
            hashT = location.hash.split("?");
            getStatus = urlCtrl.getUrlSearchParams();

            hashNav = (hashT[0].indexOf("#") < 0) ? "#" + hashT[0] : hashT[0];
            urlHistoric = hashNav + "?preview=" + hexaData.extra.collection + "." + hexaData.extra.id;
            if (getStatus != "") urlHistoric += "&" + getStatus;
            history.replaceState({}, null, urlHistoric);
				    urlCtrl.openPreview(url);
        }
      }
    }
  }

  function bindContextMenu<?= $kunik ?>() {
    if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
      $("#HexCanvas<?= $kunik ?>").on('contextmenu', function(e) {
        e.preventDefault();
        if (hexagonMapping<?= $kunik ?>.navigationStack.length > 0) {
          const previousState = hexagonMapping<?= $kunik ?>.navigationStack.pop();
          hexagonMapping<?= $kunik ?>.scale = previousState.scale;
          hexagonMapping<?= $kunik ?>.offsetX = previousState.offsetX;
          hexagonMapping<?= $kunik ?>.offsetY = previousState.offsetY;
          hexagonMapping<?= $kunik ?>.drawedHex = {};
          hexagonMapping<?= $kunik ?>.drawedTitle = {};
          hexagonMapping<?= $kunik ?>.navigation = previousState.zoomTo;
          hexagonMapping<?= $kunik ?>.clearCanvas();
          // Redessiner la vue
          if (hexagonMapping<?= $kunik ?>.preview === false) {
            hexagonMapping<?= $kunik ?>.drawHexEditMode();
          }
          let group = Object.values(hexagonMapping<?= $kunik ?>.data[previousState.zoomTo]);
          hexagonMapping<?= $kunik ?>.popBreadCrumb()
          if (previousState.zoomTo == "root") {
            hexagonMapping<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 2);
          } else {
            hexagonMapping<?= $kunik ?>.placeGroupsWithOffsets(
              group, {
                column: 0,
                row: 0
              }, false, false, 3);
          }

        }
      })
    }
  }

  function dragMappingData<?= $kunik ?>(data) {
    let dataToDrag = groupDataMapping<?= $kunik ?>(data);
    if (<?= json_encode($blockCms["navigate"]) ?> == "depth") {
      hexagonMapping<?= $kunik ?>.setBreadCrumbContainer("#hexagone-breadcrumbs-<?= $kunik ?>");
      hexagonMapping<?= $kunik ?>.setType(<?= json_encode($blockCms["navigate"]) ?>);
      for (const key in dataToDrag) {
        dataToDrag[key].text = key;
        dataToDrag[key].image = null;
        dataToDrag[key].color = `#${(Math.floor(Math.random()*0xFFFFFF)).toString(16)}`;
        delete dataToDrag[key].group;
      }
      hexagonMapping<?= $kunik ?>.data["root"] = Object.values(dataToDrag);
      hexagonMapping<?= $kunik ?>.placeGroupsWithOffsets(Object.values(dataToDrag), {
        column: 0,
        row: 0
      }, false, false, 2);

      hexagonMapping<?= $kunik ?>.fitBounds();
      hexagonMapping<?= $kunik ?>.addBreadCrumb()
    } else {
      $.each(dataToDrag, function(key, child) {
        let btn = `<div class="btn margin-5 btn-hexa-container">
          <button class="btn-expand" data-toggle="collapse" href="#${hexagonMapping<?= $kunik ?>.slugify(key)}">
            <i class="fa fa-chevron-down"></i>
          </button>
          <button class="btn-hexagone-zoomer" data-type="group">
            ${key}
            <span class="badge badge-theme-count margin-left-5 label-primary" data-countvalue="${key}" data-countkey="${key}" data-countlock="false">${dataToDrag[key].childs.length}</span>
          </button>
        </div>
        <div class="collapse hexagone-child-container" id="${hexagonMapping<?= $kunik ?>.slugify(key)}">
        `;
        dataToDrag[key].childs.forEach((child) => {
          btn += `<button class="btn margin-5 btn-hexagone-zoomer" data-type="children" data-value='${hexagonMapping<?= $kunik ?>.slugify(child.text)}'>
            ${child.text}
          </button>`;
        })
        btn += `</div>`;
        $(`#hexagone-filter-container`).append(btn);
      })
      hexagonMapping<?= $kunik ?>.placeGroupsWithOffsets(Object.values(dataToDrag), hexagonMapping<?= $kunik ?>.getCenterTile(), true);
      hexagonMapping<?= $kunik ?>.fitBounds();
    }
    bindBtnClick<?= $kunik ?>();
  }

  function fetchData() {
    let params = {
      searchType: <?= json_encode($blockCms["elementsType"]) ?>,
      notSourceKey: true,
      indexStep: 0,
      filters: {
        "$or": {
          "source.keys": costum.slug,
          "reference.costum": costum.slug
        },
        "toBeValidated": {
          "$exists": false
        }
      }
    }
    params.filters.$or[`parent.${costum.contextId}`] = {
      "$exists": true
    }
    if ($.inArray("organizations", <?= json_encode($blockCms["elementsType"]) ?>) >= 0) {
      params.filters.$or[`links.memberOf.${costum.contextId}`] = {
        "$exists": true
      }
    }
    if ($.inArray("citoyens", <?= json_encode($blockCms["elementsType"]) ?>) >= 0) {
      params.filters.$or[`links.memberOf.${costum.contextId}`] = {
        "$exists": true
      }
    }
    if ($.inArray("projects", <?= json_encode($blockCms["elementsType"]) ?>) >= 0) {
      params.filters.$or[`links.projects.${costum.contextId}`] = {
        "$exists": true
      }
    }
    ajaxPost(
      null,
      baseUrl + "/co2/search/globalautocomplete",
      params,
      function(data) {
        dragMappingData<?= $kunik ?>(data.results);
      }
    );
  }

  var hexagonMapping<?= $kunik ?> = null;
  $(function() {
    hexagonMapping<?= $kunik ?> = new HexagonGrid("HexCanvas<?= $kunik ?>", <?= json_encode($blockCms["circle"]) ?>, (<?= json_encode($blockCms["navigate"]) ?> == "depth" ? window.innerWidth : window.innerWidth - 300));
    fetchData();
    bindContextMenu<?= $kunik ?>();
  });

  function bindBtnClick<?= $kunik ?>() {
    $(".btn-hexagone-zoomer").off().on('click', function() {
      if ($(this).data("type") == "group") {
        if ($(this).parents(".btn-hexa-container").hasClass("btn-primary")) {
          $(".btn-hexa-container").removeClass("btn-primary");
          hexagonMapping<?= $kunik ?>.zoomTo = [];
          hexagonMapping<?= $kunik ?>.fitBounds();
        } else {
          let group = $(this).find("span").data("countvalue");
          $(".btn-hexa-container").removeClass("btn-primary");
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          $(this).parents(".btn-hexa-container").addClass("btn-primary");
          hexagonMapping<?= $kunik ?>.zoomToGroup(group);
        }
      } else {
        if ($(this).hasClass("btn-primary")) {
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          hexagonMapping<?= $kunik ?>.zoomTo = [];
          hexagonMapping<?= $kunik ?>.fitBounds();
        } else {
          let texte = $(this).data("value");
          $(".btn-hexa-container").removeClass("btn-primary");
          $(".btn-hexagone-zoomer").removeClass("btn-primary");
          $(this).addClass("btn-primary");
          let element = Object.values(hexagonMapping<?= $kunik ?>.drawedHex).find(hex => {
            return hexagonMapping<?= $kunik ?>.slugify(hex.text) == texte;
          });
          console.log("element", element);
          if (typeof element != "undefined") {
            hexagonMapping<?= $kunik ?>.zoomToElement(element.column, element.row);
          }
        }
      }
    })
  }
</script>