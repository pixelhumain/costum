<?php
$keyTpl = "graphforMap";
$paramsData = [
    "cluster" => "pie",
    "marker" => "default",
    "showLegende" => 'true',
    "dynamicLegende" => 'true',
    "legendConfig" => [],
    "legendeVar" => "type",
    "legendeCible" => "",
    "legendeLabel" => "Type",
    "useFilters" => 'false',
    "filtersConfig" => "",
    "zones" => [],
    "linkData" => "co2/search/globalautocomplete"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (isset($blockCms["graph"][$e]) ) {
            $paramsData[$e] = gettype($blockCms["graph"][$e]) == "boolean" ? json_encode($blockCms["graph"][$e]) : $blockCms["graph"][$e];
        }
    }
    if($paramsData["linkData"] == ""){
        $paramsData["linkData"] = "co2/search/globalautocomplete";
    }
}
?>

<style>
    #graphD3{
        position: relative;
    }
    #graphformap<?= $kunik ?>{
        height: 700px;
        position: relative;
    }
    .chart-legende-container {
        position: absolute;
        right: 0;
        top: 0;
        z-index: 9999999;
        background: white;
    }
</style>
<div id="app-map-filters" class="searchObjCSS"></div>
<div id="graphD3">
    <div id="graphformap<?= $kunik ?>">
    
    </div>
</div>
<script>
    var appMap = null;
    var filterSearch;
    var useFilter = <?= $paramsData["useFilters"]; ?>;
    var legende = <?= gettype($paramsData["legendConfig"]) == "array" ? json_encode($paramsData["legendConfig"]) : $paramsData["legendConfig"]?>;
    var dynamicLegende = <?= $paramsData["dynamicLegende"]; ?>;
    $(function(){
        var customMap=(typeof paramsMapD3CO.mapCustom != "undefined") ? paramsMapD3CO.mapCustom : {tile : "maptiler"};
        var mapOptions = {
            container : "#graphformap<?= $kunik ?>",
            activePopUp : true,
            clusterType : '<?= $paramsData["cluster"] ?>',
            markerType : '<?= $paramsData["marker"] ?>',
            showLegende: '<?= $paramsData["showLegende"] ?>',
            dynamicLegende: '<?= $paramsData["dynamicLegende"] ?>',
            mapOpt:{
                zoomControl:false
            },
            mapCustom:customMap,
            elts : {}
        };
        if(dynamicLegende == true){
            mapOptions.groupBy = '<?= $paramsData["legendeVar"] ?>';
            mapOptions.legendeLabel = '<?= $paramsData["legendeLabel"] ?>';
        }else{
            if("<?= $paramsData["legendeCible"] ?>" != "autre" && "<?= $paramsData["legendeCible"] ?>" != ""){
                mapOptions.legende = costum.lists["<?= $paramsData["legendeCible"] ?>"];
            }else{
                mapOptions.legende = legende.map(function(d){
                    return d.label;
                });
            }
            mapOptions.groupBy = '<?= $paramsData["legendeVar"] ?>';
            mapOptions.legendeVar = '<?= $paramsData["legendeVar"] ?>';
            mapOptions.legendeLabel = '<?= $paramsData["legendeLabel"] ?>';
        }
        appMap = new MapD3(mapOptions);
        if(useFilter){
            var filterUser = JSON.parse(JSON.stringify(<?= str_replace("\"", "'", $paramsData["filtersConfig"])?>));
            if(typeof filterUser.filters != "undefined"){
                filterUser.mapCo = appMap;
                filterUser.container = "#app-map-filters";
                filterUser.layoutDirection = "vertical";
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }else{
                filterUser = {
                    "options" : {
                        "tags" : {
                            "verb" : "$and"
                        }
                    },
                    "results" : {
                        "renderView" : "directory.elementPanelHtml",
                        "smartGrid" : true,
                        "map" : {
                            "active" : true
                        }
                    },
                    "defaults" : {
                        "indexStep" : 0,
                        "types" : [ 
                            "organizations"
                        ]
                    },
                    "filters" : filterUser,
                    container:"#app-map-filters",
                    layoutDirection:"vertical",
                    mapCo:appMap,
                    results:{
                        multiCols:false,
                        map:{
                            active:true
                        }
                    },
                    mapContent:{
                        hideViews: ["btnHideMap"]
                    }
                };
                filterSearch = searchObj.init(filterUser);
                filterSearch.search.init(filterSearch);
            }
        }else{
            var url = '<?= $paramsData['linkData'] ?>';
            var params = {};
            if(url.indexOf("globalautocomplete") > -1){
                var zones = <?php echo json_encode( $paramsData['zones'] ); ?>;
                var defaultFilters<?= $kunik ?> = {};
                // defaultFilters<?= $kunik ?>['$or'] = {}; 
                // defaultFilters<?= $kunik ?>['$or']["parent."+costum.contextId] = {'$exists':true};
                // defaultFilters<?= $kunik ?>['$or']["source.keys"] = costum.slug;
                // /*if(costum.contextType!="projects"){
                //     defaultFilters<?= $kunik ?>['$or']["links.projects."+costum.contextId] = {'$exists':true};
                // }*/
                // defaultFilters<?= $kunik ?>['$or']["reference.costum"] = costum.contextSlug;
                // defaultFilters<?= $kunik ?>['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
                var mapSearchFields=(costum!=null && typeof costum.map!="undefined" && typeof costum.map.searchFields!="undefined") ? costum.map.searchFields : ["urls","address","geo","geoPosition", "tags", "type", "zone"];
                params = {
                    // notSourceKey: false,
                    searchType : <?= json_encode(['organizations']) ?>,
                    fields : mapSearchFields,
                    filters: defaultFilters<?= $kunik ?>,
                    indexStep: 0
                };
                if(zones){
                    params["locality"] = zones;
                    params["activeContour"] = true
                }
                if('<?= $paramsData['legendeVar'] ?>' == 'tags'){
                    params["options"] = {
                        'tags' : {
                            'verb': '$all'
                        }
                    };
                }
            }
            ajaxPost(
                null,
                (url.indexOf("http") > -1 ? url : baseUrl + "/" + url),
                params,
                function(data){
                    var dataToSend = data.results ? data.results : data;
                    if(typeof data.zones != "undefined"){
                        Object.assign(dataToSend, data.zones);
                    }
                    appMap.clearMap();
                    appMap.addElts(dataToSend);
                    appMap.getMap().invalidateSize()
                    appMap.fitBounds();
                }
            )
        }
    });
</script>

<script type="text/javascript">
    $(function(){
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        if(sectionDyf.<?php echo $kunik ?>ParamsData.zones){
            sectionDyf.<?php echo $kunik ?>ParamsData.zones = Object.entries(sectionDyf.<?php echo $kunik ?>ParamsData.zones).map(([key, val]) => {
                return val.id;
            })
        }
        var cible = Object.keys((costum.lists||{})).reduce((a,b)=> {
                            return (a[b]=b, a);
                        },{});
        cible['autre'] = "Autre";
        var zones = {},pays = {};
        var allZones = {};
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "<?= Yii::t('graph', "Graph configuration") ?>",
                "description" : "<?= Yii::t('graph', "Customize your graph") ?>",
                "icon" : "fa-cog",
                "properties" : {
                    "marker": {
                        "label" : "<?= Yii::t("graph", "Type of marker") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"default": "<?= Yii::t("graph", "Default") ?>","pie": "<?= Yii::t("graph", "Pie Chart") ?>", "bar": "<?= Yii::t("graph", "Bar Chart") ?>", "donut": "<?= Yii::t('graph', "Donut Chart") ?>", "geoshape": "<?= Yii::t("graph", "Geoshape") ?>", "heatmap": "<?= Yii::t("graph", "Heatmap") ?>"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "cluster": {
                        "label" : "<?= Yii::t("graph", "Cluster type") ?>",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {"pie": "<?= Yii::t("graph", "Pie Chart") ?>", "bar": "<?= Yii::t("graph", "Bar Chart") ?>", "donut": "<?= Yii::t('graph', "Donut Chart") ?>"},
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.type
                    },
                    "showLegende" : {
                        "inputType" : "checkboxSimple",
                        "label" : "<?= Yii::t("graph", "Show legend") ?>",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.showLegende
                    },
                    "dynamicLegende" : {
                        "inputType" : "checkboxSimple",
                        "label" : "<?= Yii::t('graph', 'Automatic legend') ?>",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.dynamicLegende
                    },
                    "legendeVar":{
                        "label" : "<?= Yii::t('graph', 'Legend variable on the data') ?>",
                        "inputType" : "text",
                        "placeholder":"ex: type, tags",
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendeVar
                    },
                    "legendeCible":{
                        "inputType" : "select",
                        "label" : "<?= Yii::t("graph", 'Legend on') ?>",
                        "options" : cible
                    },
                    "legendConfig":{
                        "label" : "<?= Yii::t("graph", 'Preference on the legend') ?>",
                        "inputType" : "lists",
                        "entries" : {
                            "label" : {
                                "type" : "text",
                                "label": "label",
                                "class": "col-sm-10",
                                "placeholder":"Libellé"
                            }
                        },
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendConfig
                    },
                    "legendeLabel":{
                        "label" : "<?= Yii::t("graph", "Name of the legend")?>",
                        "inputType" : "text",
                        "placeholder":"ex: Taille",
                        "values": sectionDyf.<?php echo $kunik ?>ParamsData.legendeLabel
                    },
                    "useFilters" : {
                        "inputType" : "checkboxSimple",
                        "label" : "<?= Yii::t("graph", "Use the filter") ?>",
                        "rules" : {
                            "required" : true
                        },
                        "params" : {
                            "onText" : trad.yes,
                            "offText" : trad.no,
                            "onLabel" : trad.yes,
                            "offLabel" : trad.no
                        },
                        "checked" : sectionDyf.<?php echo $kunik ?>ParamsData.useFilters
                    },
                    "filtersConfig":{
                        "label" : "<?= Yii::t("graph", "Filter configuration") ?>",
                        "inputType" : "textarea",
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersConfig
                    },
                    "linkData":{
                        "label" : "<?= Yii::t("graph", "Data URL") ?>",
                        "inputType" : "text",
                        "placeholder":"Defaut : co2/search/globalautocomplete",
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.linkData
                    },
                    "level": {
                        "inputType" : "select",
                        "label" : "<?= Yii::t("graph","Zone level") ?>",
                        "options" : {
                            // "1": "Niveau 1",
                            "2": "Niveau 2",
                            "3": "Niveau 3",
                            "4": "Niveau 4",
                            "5": "Niveau 5",
                            "6": "Niveau 6"
                        }
                    },
                    "countryCode":{
                        "inputType" : "selectMultiple",
                        "label" : "<?= Yii::t("graph", "Country") ?>",
                        "options" : pays
                    },
                    "zones":{
                        "inputType" : "selectMultiple",
                        "label" : "<?= Yii::t("graph", "Zone") ?>",
                        "options" : zones
                    }
                },
                beforeBuild: function(){
                    ajaxPost(null,
                        baseUrl + '/co2/search/getzone/',
                        {
                            level: [1]
                        },function(data){
                            $.each(data, function(kV, vV){
                                if(!pays[vV.countryCode]){
                                    pays[vV.countryCode] = vV.name;
                                }
                            })
                        }, function(data){
                            mylog.log('Get zones errors', data);
                    },
                    null, {
                        async: false
                    });
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik?>ParamsData.useFilters!="true" && sectionDyf.<?php echo $kunik?>ParamsData.useFilters!=true){
                        $(".filtersConfigtextarea").hide();
                        $(".linkDatatext").show();
                        if(sectionDyf.<?php echo $kunik?>ParamsData.linkData.indexOf('globalautocomplete') > -1){
                            $(".zonesselectMultiple").show();
                            $(".countryCodeselectMultiple").show();
                            $(".levelselect").show();
                        }else{
                            $(".zonesselectMultiple").hide();
                            $(".countryCodeselectMultiple").hide();
                            $(".levelselect").hide();
                        }
                    }else{
                        $(".filtersConfigtextarea").show();
                        $(".linkDatatext").hide();
                    }
                    if(sectionDyf.<?php echo $kunik?>ParamsData.dynamicLegende!="true" && sectionDyf.<?php echo $kunik?>ParamsData.dynamicLegende!=true){
                        $(".legendeCibleselect").show();
                        if(sectionDyf.<?php echo $kunik?>ParamsData.legendeCible !="autre" && sectionDyf.<?php echo $kunik?>ParamsData.legendeCible != ""){
                            $(".legendConfiglists").hide();
                        }else{
                            $(".legendConfiglists").show();
                        }
                    }else{
                        $(".legendConfiglists").hide();
                        $(".legendeCibleselect").hide();
                    }
                    $("#legendeCible").on("change", function(){
                        var legendeCible = $(this).val();
                        if(legendeCible != "autre" && legendeCible != ""){
                            $(".legendConfiglists").hide();
                        }else{
                            $(".legendConfiglists").show();
                        }
                    })
                    $("#countryCode").on("change", function(){
                        var country = $(this).val();
                        country = country.filter(function (el) {
                            return el != null && el != '';
                        });
                        var params = {};
                        if($("#level").val()){
                            params['level'] = [];
                            params['level'].push($("#level").val());
                        }
                        if(country.length > 0){
                            params['countryCode'] = country;
                        }
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            params,
                            function(data){
                                allZones = data;
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#zones").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    });
                    $("#level").on("change", function(){
                        var country = $("#countryCode").val();
                        var params = {};
                        if($(this).val()){
                            params['level'] = [];
                            params['level'].push($(this).val());
                        }
                        if(country && country.length > 0){
                            params['countryCode'] = country;
                        }
                        ajaxPost(null,
                            baseUrl + '/co2/search/getzone/',
                            params,
                            function(data){
                                allZones = data;
                                var text = "<option></option>";
                                $.each(data, function(kV, vV){
                                    text += `<option value='${kV}'>${vV.name}</option>`;
                                })
                                $("select#zones").html(text)
                            }, function(data){
                                mylog.log('Get zones errors', data);
                        },
                        null, {
                            async: false
                        });
                    });
                    $("#linkData").on("blur", function(){
                        if($(this).val().indexOf('globalautocomplete') > -1){
                            $(".zonesselectMultiple").show();
                            $(".countryCodeselectMultiple").show();
                            $(".levelselect").show();
                        }else{
                            $(".zonesselectMultiple").hide();
                            $(".countryCodeselectMultiple").hide();
                            $(".levelselect").hide();
                        }
                    })
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value["graph"][k] = $("#"+k).val();
                        if (k == "legendConfig"){
                            tplCtx.value["graph"][k] = [];
                            $.each(data.legendConfig, function(index, va){
                                tplCtx.value["graph"][k].push(va);
                            })
                        }
                        if (k == "zones"){
                            tplCtx.value["graph"][k] = {};
                            if(typeof data.zones == "string"){
                                tplCtx.value["graph"][k][data.zones+'level'+allZones[data.zones].level[0]] = {
                                        name: allZones[data.zones].name,
                                        active: true,
                                        id: data.zones,
                                        countryCode: allZones[data.zones].countryCode,
                                        level: allZones[data.zones].level[0],
                                        type: "level"+allZones[data.zones].level[0],
                                        key: data.zones+'level'+allZones[data.zones].level[0]
                                    }
                            }else if(typeof data.zones == "object"){
                                $.each(data.zones, function (kV, vV) {
                                    mylog.log("ZOnes key", kV, vV);
                                    tplCtx.value["graph"][k][vV+'level'+allZones[vV].level[0]] = {
                                        name: allZones[vV].name,
                                        active: true,
                                        id: vV,
                                        countryCode: allZones[vV].countryCode,
                                        level: allZones[vV].level[0],
                                        type: "level"+allZones[vV].level[0],
                                        key: vV+'level'+allZones[vV].level[0]
                                    }
                                })
                            }else{
                                tplCtx.value["graph"][k] = "";
                            }
                            // tplCtx.value["graph"][k].push(va);
                        }
                    });
                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        });
                    }
                }
            }
        }
        costum.checkboxSimpleEvent = {
            true : function(id){
                if(id=="dynamicLegende"){
                    // $("#ajaxFormModal .legendConfiglists").hide();
                    $("#ajaxFormModal .legendeCibleselect").hide();
                }
                if(id=="useFilters"){
                    $("#ajaxFormModal .filtersConfigtextarea").show();
                    $("#ajaxFormModal .linkDatatext").hide();
                }
            },
            false : function(id){
                if(id=="dynamicLegende"){
                    // $("#ajaxFormModal .legendConfiglists").show();
                    $("#ajaxFormModal .legendeCibleselect").show();
                }
                if(id=="useFilters"){
                    $("#ajaxFormModal .filtersConfigtextarea").hide();
                    $("#ajaxFormModal .linkDatatext").show();
                }
            }
        }
        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx["id"] = $(this).data("id");
            tplCtx["collection"] = $(this).data("collection");
            tplCtx["path"] = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>