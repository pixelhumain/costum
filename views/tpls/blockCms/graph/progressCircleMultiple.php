<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];    
?>
<style id="progressCircleMultiple<?= $kunik ?>">
    .flex-center{
            /*justify-content: center;*/
        display: flex;
        flex-wrap: wrap;
    } 
    #chartContainer<?=$kunik?> .label-text-color {
        color: <?= $blockCms["css"]["textColor"]["color"] ?>;
    }
    #ttp<?= $kunik; ?>{
        border:5px solid #ddd;
        border-radius: 50%
    }
    .onoffswitch
    {
        position:relative;
        width:40%;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }

    .onoffswitch-checkbox {
        display: none;
        <?php if($blockCms["textRight"]){ ?>
            #chartContainer<?=$kunik?> .flex-center .circle-content<?=$kunik?>{
                flex-direction: row;
                align-items: center;
                display: flex;
            }
            #chartContainer<?=$kunik?> .flex-center .circle-content<?=$kunik?> .container-circle-svg{
                display: flex;
                justify-content: center;
                width: 140px;
            }
            #chartContainer<?=$kunik?> .flex-center .circle-content<?=$kunik?> .label-text-color{
                text-align: left !important;
                height: auto !important;
                width: calc(100% - 140px);
            }
        <?php } ?>
    }

    .circle-content<?=$kunik?> {
        width: <?= 100/$blockCms["nombre"] ?>%;
    }

    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 0px solid #999999; border-radius: 0px;
    }

    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        -moz-transition: margin 0.3s ease-in 0s; -webkit-transition: margin 0.3s ease-in 0s;
        -o-transition: margin 0.3s ease-in 0s; transition: margin 0.3s ease-in 0s;
    }

    .onoffswitch-inner > span {
        display: block; float: left; position: relative; width: 50%; height: 30px; padding: 0; line-height: 30px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
    }

    .onoffswitch-inner .onoffswitch-active {
        padding-left: 10px;
        background-color: #EEEEEE; color: #FFFFFF;
    }

    .onoffswitch-inner .onoffswitch-inactive {
        padding-right: 10px;
        background-color: #EEEEEE; color: #FFFFFF;
        text-align: right;
    }

    .onoffswitch-switch {
        display: block; width: 50%; margin: 0px; text-align: center; 
        border: 0px solid #999999;border-radius: 0px; 
        position: absolute; top: 0; bottom: 0;
    }
    .onoffswitch-active .onoffswitch-switch {
        background: #27A1CA; left: 0;
    }
    .onoffswitch-inactive .onoffswitch-switch {
        background: #A1A1A1; right: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }

</style>
<div class="<?= $kunik ?>">
    <?php if(!isset($blockCms["answerPath"]) || $blockCms["answerPath"]==""){ ?>
        <div class="text-center">
            <div class="alert alert-danger">Paramètre manquant :  answerPath</div>
        </div>
    <?php } ?>
    <?php if(!isset($blockCms["answerValue"]) || $blockCms["answerValue"]==""){ ?>
        <div class="text-center">
            <div class="alert alert-danger">Paramètre manquant :  answerValue</div>
        </div>
    <?php } ?>
    <div id="chartContainer<?=$kunik?>" class="chartviz margin-top-10">
            <?php if(isset($blockCms["answerValue"]) && count($blockCms["answerValue"])>0 ){ 
                echo '<div id="arc'.$kunik.'" class="row no-margin flex-center">';
                
                foreach (@$blockCms["dataAnswers"]["percentageArray"] as $key => $value) {?>
                    <div id="<?= 'circle'.$kunik.$key ?>" data-label="<?= $value["label"] ?? "" ?> <?=$kunik?>" class="circle-content<?=$kunik?> text-center padding-bottom-10"></div>
            <?php } 
                echo "</div>";
            }else{
                echo "<h4>Votre données sera affiché ici</h4>";
            } ?>
    </div>
    <table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10"></table>
</div>

<script type="text/javascript">
    var data = [];
    var dataAnswers = <?= json_encode( $blockCms["dataAnswers"] ) ?>;
   
    if(dataAnswers != null)
    {
        data = dataAnswers.percentageArray;
    }

    // if(typeof costum["dashboardData"] !="undefined" && typeof costum["dashboardData"]["<?= $blockKey ?>"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"]){
    //     let d = costum["dashboardData"]["<?= $blockKey ?>"]["percentageArray"];
    //     let c = costum["dashboardData"]["<?= $blockKey ?>"]["respondentCount"];

    //     $.each(d, function(index, arr){
    //         dataByQuestion<?= $kunik; ?>.push({"label" : arr.label, "value": Math.round((arr.value/c)*100)});
    //     });
    // }

    // if(typeof costum["dashboardData"] !="undefined" && costum["dashboardData"]["<?= $blockKey ?>"] && typeof costum["dashboardData"]["<?= $blockKey ?>"]["respondentCount"]!="undefined"){
    //     $(function(){
    //         $("#ttp<?=$kunik;?>").tooltip({
    //             placement: "bottom",
    //             title: "Ce chiffre est par rapport au nombre des répondant sur ce question ( "+costum["dashboardData"]["<?= $blockKey ?>"]["respondentCount"]+" )",
    //         });
    //     });
    // }else{
    //     $("#ttp<?=$kunik;?>").hide();
    // }
    
    jQuery(document).ready(function() {
        var progressCircle<?= $kunik ?> = {
            width: 120,
            height: 120,
            pi : 2 * Math.PI,
            data: {},
            arc : null,
            parent:null,
            init: function(procir, DOMselector){
                procir.arc = d3.arc()
                    .innerRadius(46)
                    .outerRadius(58) // arc width
                    .startAngle(0);
                d3.select(DOMselector).empty();
                procir.parent = d3.select(DOMselector).append("svg")
                    .attr("width", procir.width)
                    .attr("height", procir.height);

                procir.draw(procir, procir.data);
            },
            initData:function(procir, d){
                procir.data.id = d.label+"<?=$kunik ?>";
                procir.data.percent = d.value||0;
            },
            draw: function(procir, data){
                var d3Ring = procir.parent  
                .append("g")
                .attr("transform", "translate("+ procir.width/2 +"," +60+")")
                .attr("id", data.id);
            
                // Background
                d3Ring
                .append("path")
                .datum({endAngle: procir.pi})
                .attr("class", "emptyColor")
                .attr("d", procir.arc); 
                
                // Foreground
                var foreground = d3Ring
                .append("path")
                .datum({endAngle: 0})
                    .style("stroke", "none")
                    .style("stroke-width", "0px")
                    .style("opacity", 1)
                .attr("class", "completeColor")
                .attr("d", procir.arc);
                
                // Text
                d3Ring.empty();
                d3Ring
                .append("text")
                .attr("x", -20)
                .attr("y", 8)
                .attr("class", "percentColor")
                .style("font-size", 25)
                .style("font-family", "Georgia, Arial, sans-serif")
                .style("font-weight", "bolder")
                .text(data.percent + "%");
                var angle = procir.helpers.convertPercentToAngle(procir);
                
                // Animation
                foreground
                .transition()
                .duration(1500)
                .delay(500)
                .call(procir.arcTween, procir, angle);
            },
            arcTween: function(transition, procir, newAngle){
                transition.attrTween("d", function(d) {  
                    var interpolate = d3.interpolate(d.endAngle, newAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return procir.arc(d);
                    };
                });
            },
            helpers:{
                convertPercentToAngle: function(procir){
                    return ( procir.data.percent / 100 ) * procir.pi
                }
            }
        }

        if(data.length>0){
            $("#tableData<?=$kunik?>").empty();
            $.each(data, function(index, value){
                var procircle = '*[data-label="'+value.label+' <?=$kunik?>"';
                progressCircle<?= $kunik ?>.initData(progressCircle<?= $kunik ?>, value);
                progressCircle<?= $kunik ?>.init(progressCircle<?= $kunik ?>, procircle);
                $(procircle).append('<div class="text-center label-text-color textColor" title="'+value.label+' ( '+value.value+' )" style="height:55px; overflow:hidden">'+value.label+'</div>');
                $("#tableData<?=$kunik?>").append(`<tr>
                    <td>${value.label}</td>
                    <td class="tableValue">${value.value}</td>
                </tr>`)
            })
        }
        $("#tableData<?=$kunik?>").hide();
    });
</script>
<script type="text/javascript">

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#progressCircleMultiple<?= $kunik ?>").append(str);

    if(costum.editMode)
    {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode( $blockCms ) ?>;
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
        }

        var progressCircleMultiple = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type:"groupButtons",
                            options: {
                                name:"textRight",
                                label: tradCms.textrightofcircle ??  "Texte à gauche du cercle",
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ],
                                defaultValue: cmsConstructor.sp_params["<?= $myCmsId ?>"].textRight
                            }
                        },
                        {
                            type: "inputSimple",
                            options: {
                                name:"nombre",
                                label: tradCms.numberofelementperline ?? "Nombre d'élément par ligne"
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "coform",
                                label: tradCms.selectaform ?? "Selectionner un formulaire",
                                options: coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                label: tradCms.whichQuestionCorrespondstotheGraph ?? " À Quelle Question corresponds la graph ?",
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                label: tradCms.answervalue ??  "Valeur répondu",
                                options: answerValueData
                            }
                        },
                        {
                            type:"groupButtons",
                            options: {
                                name:"order",
                                label: "Trier par ordre décroissante",
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ]
                            }
                        },
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "percentColor",
                                label: tradCms.colorofpercentagetext ?? "Couleur du texte de pourcentage",
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "emptyColor",
                                label: tradCms.colorofemptyarc ?? "Couleur de l'arc vide",
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "completeColor",
                                label: tradCms.filledarccolor ?? "Couleur de l'arc rempli",
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "textColor",
                                label: tradCms.labelcolor ?? "Couleur du label",
                                inputs:[
                                    "color"
                                ]
                            }
                        }
                    ]
                },
                advanced : {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value){

                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
                    cmsConstructor.blocks.progressCircleMultiple<?= $myCmsId ?>.configTabs.general.inputsConfig[3].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
                    cmsConstructor.blocks.progressCircleMultiple<?= $myCmsId ?>.configTabs.general.inputsConfig[4].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
            }
        };
        cmsConstructor.blocks.progressCircleMultiple<?= $myCmsId ?> = progressCircleMultiple;
    }

    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];
                
                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }
                if(input["type"]=="text"){
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }
    function getAnswerValue(coformId, value) {
        var coform = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coform = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coform[coformId] != "undefined" ){
            coform = coform[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
            if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        if(input.includes("checkboxNew") || input.includes("radioNew")){
            // if(coform != [] && coform != null)
            // {
                if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["list"]){
                    for(const paramValue of coform["params"][input]["list"]){
                        answerValue.push({value: paramValue, label: paramValue})
                    }
                // }
            }
        }
        return answerValue;
    }
</script>