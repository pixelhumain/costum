<?php 
    $keyTpl = "textWithValueAndIcon";
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $objectCss = $blockCms["css"] ?? [];
    $styleCss  = (object) [$kunik => $objectCss];
?>

<?php ?>

<style> 
    .custom-card {
        background-color: white;
        border-radius: 10px;
        text-align: center;
        width: 320px;
        height: 230px;
        position: relative;
        padding: 30px;
        margin: 50px auto
        
    }
    .custom-card.data-top{
        border: 1px dotted #b3b3b3;
        background: none;
        padding-top: 60px;
        height: 290px;
        display: flex;
        align-items: center;
    }
    .custom-card .number{
        font-size: 40px;
    }
    .data-top .number{
        color: white;
        position: absolute;
        top: -25px;
        margin: 0 auto;
        left: 0;
        width: 100%;
    }
    .data-top .number span{
        padding: 10px 15px;
        background: #a5c145;
        border-radius: 10px;
    }
    .data-top .number span::after{
        content:""; 
        border-left: 15px solid transparent;
        border-right: 15px solid transparent;
        border-top: 10px solid #a4c147;
        position: absolute;
        top: 55px;
        left: 145px;
    }
    .custom-card .title{
        font-size: 24px;
    }
    .custom-card .title .accentuated{
        color: #a9c34b;
    }
    .custom-card .img-absolute{
        position: absolute;
        border-radius: 100%;
        background: white;
        padding: 30px;
        top: -20%;
        right: -20%;
    }
    .custom-card .img-absolute .img-icon{
        width: 50px;
        height: 50px;
        object-fit: contain;
    }
</style>

<div class="<?= $kunik ?>">
    <div class="row">
        <div class="col-md-4">
            <div class="custom-card <?= $blockCms['design'] ? "data-top" : "" ?> ">
                <div>
                    <h4 class="number">
                        <span>2 537</span>
                    </h4>
                    <p class="title"><?= $blockCms["text"] ?></p>
                    <?php if($blockCms["profilImageUrl"] != "" && $blockCms["profilImageUrl"] != "/assets/bf33044a/images/thumbnail-default.jpg"){ ?>
                        <div class="img-absolute">
                            <img src="<?= $blockCms["profilImageUrl"] ?>" alt="" class="img-icon">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    sectionDyf.<?php echo $kunik ?>BlockCms = <?php echo json_encode( $blockCms ); ?>;
    if(typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers !="undefined" && typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.value && sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.value.$numberDecimal){
        $(".<?= $kunik ?> .number>span").html((new Intl.NumberFormat('fr')).format(sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.value.$numberDecimal));
    }else if(typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers !="undefined" && typeof sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.value !="undefined"){
        $(".<?= $kunik ?> .number>span").html(sectionDyf.<?php echo $kunik ?>BlockCms.dataAnswers.value);
    }

</script>

<script>

    if (costum.editMode) {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];

        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = <?= json_encode($blockCms) ?>;
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform != "" && cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
        }
        
        var str = "";
        str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
        $("#css-<?= $kunik ?>").append(str);
        var textWithValueAndIcon = {
            configTabs: {
                general: {
                    inputsConfig: [
                        {
                            type : "select",
                            options : {
                                name : "coform",
                                isSelect2 : true,
                                label : tradCms.selectaform,
                                options : coformOptions.length > 0 ? coformOptions : []
                            }
                        },
                        {
                            type : "select",
                            options : {
                                name : "answerPath",
                                isSelect2 : true,
                                label : tradCms.whichQuestionCorrespondstotheGraph,
                                options : answerPathData.length > 0 ? answerPathData : []
                            }
                        },
                        {
                            type : "selectMultiple",
                            options : {
                                name : "answerValue",
                                label : tradCms.answervalue,
                                options : answerValueData.length > 0 ? answerValueData : []
                            }
                        },
                        {
                            type : "textarea",
                            options : {
                                name : "text",
                                label : tradCms.textDisplay,
                            }
                        },
                        {
                            type : "inputFileImage",
                            options : {
                                name : "profilImageUrl",
                                label : tradCms.uploadeImage,
                            }
                        },
                        {
                            type : "groupButtons",
                            options : {
                                name : "design",
                                label : tradCms.showDataTheTop,
                                options : [
                                    {
                                    value : false,
                                    label : trad.no
                                    },
                                    {
                                    value : true,
                                    label : trad.yes
                                    }
                                ],
                                defaultValue : cmsConstructor.sp_params["<?= $myCmsId ?>"].design
                            }
                        },
                        
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type : "section",
                            options : {
                                name : "percentColor",
                                label : tradCms.textStyle,
                                inputs : [
                                    "color"
                                ]
                            }
                        }
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform);
                    cmsConstructor.blocks.textWithValueAndIcon<?= $myCmsId ?>.configTabs.general.inputsConfig[1].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $myCmsId ?>"].coform, cmsConstructor.sp_params["<?= $myCmsId ?>"].answerPath);
                    cmsConstructor.blocks.textWithValueAndIcon<?= $myCmsId ?>.configTabs.general.inputsConfig[2].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
            }
        }
        cmsConstructor.blocks.textWithValueAndIcon<?= $myCmsId ?> = textWithValueAndIcon;
    }

    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];

                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }
                if(input["type"]=="text"){
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }

    function getAnswerValue(coformId, value) {
        var coforme = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coforme = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coforme[coformId] != "undefined" ){
            coforme = coforme[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
            if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["global"]){
                for(const paramValue of coforme["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }

        if(input.includes("checkboxNew") || input.includes("radioNew")){
            if(typeof coforme["params"] != "undefined" && typeof coforme["params"][input] != "undefined" && coforme["params"][input]["list"]){
                for(const paramValue of coforme["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        return answerValue;
    }
    
</script>
