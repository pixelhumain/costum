<?php
if (!isset($costum)) {
  $costum = CacheHelper::getCostum();
}

$keyTpl     = "radial";
$paramsData = [
  "dataSource" => [$costum["contextId"] => ["name" => $costum["contextSlug"], "type" => $costum["contextType"]]],
  "levels" => 4,
  "category1" => [
    ["name" => "value1", "value" => 2],
    ["name" => "value2", "value" => 5],
    ["name" => "value3", "value" => 0],
  ],
  "category2" => [
    ["name" => "value1", "value" => 3],
    ["name" => "value2", "value" => 2],
    ["name" => "value3", "value" => 4],
  ]
];
if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (isset($blockCms[$e])) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}

?>


<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $graphAssets = [
    '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>


<style>
  #graph-container-<?= $kunik ?> {
    height: 100%;
    width: 100%;
    overflow: hidden;
    min-height: 100px;
  }

  .super-cms .graph-panel {
    background-color: transparent !important;
    width: 100%;
    height: 100%;
  }
</style>
<!-- <button class="edit<?php echo $kunik ?>Params" data-collection="cms" data-id="<?= (string) $blockCms["_id"] ?>">Dynform</button> -->
<div id="graph-container-<?= $kunik ?>" class="graph-panel">

</div>

<script>
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  //Avoid change DATA IN sectionDyf.<?php echo $kunik ?>ParamsData variable
  var radialGraph = new RadialGraph([]);
  radialGraph.setLevels(sectionDyf.<?php echo $kunik ?>ParamsData.levels)
  radialGraph.draw("#graph-container-<?= $kunik ?>")
  const {category1, category2} = sectionDyf.<?php echo $kunik ?>ParamsData;
  radialGraph.updateData(radialGraph.preprocessResults({category1, category2}))
</script>
<script type="text/javascript">
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema": {
        "title": "Configurer votre section",
        "description": "Personnaliser votre section",
        "icon": "fa-cog",
        "properties": {
          "levels": {
              inputType : "numeric",
              rules:{
                  required:true,
                  number:true
              },
              label : "Nombre de niveau des cercles"
          },
          "category1": {
            inputType: "lists",
            label: "Category 1",
            entries: {
              name: {
                label: "Nom {num}",
                class: "col-xs-12",
                type: "text"
              },
              value: {
                label: "Valeur",
                placeholder: "1",
                type: "text",
                class: "col-xs-12 col-md-4"
              }
            }
          },
          "category2": {
            inputType: "lists",
            label: "Category 2",
            entries: {
              name: {
                label: "Nom {num}",
                class: "col-xs-12",
                type: "text"
              },
              value: {
                label: "Valeur",
                placeholder: "1",
                type: "text",
                class: "col-xs-12 col-md-4"
              }
            }
          },
        },
        save: function(data) {
          data["collection"] = "cms"
          tplCtx.value = data;
          mylog.log("save tplCtx", tplCtx);
          if (typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value(tplCtx, function(params) {
              dyFObj.commonAfterSave(params, function() {
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    mylog.log("sectiondyfff", sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click", function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      console.log(tplCtx, sectionDyf.<?php echo $kunik ?>ParamsData)
      dyFObj.openForm(sectionDyf.<?php echo $kunik ?>Params, null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>