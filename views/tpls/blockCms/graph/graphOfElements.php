<?php
if(!isset($costum)){
    $costum = CacheHelper::getCostum();
}


$graphAssets = [
    '/plugins/d3/d3.v6.min.js','/js/graph.js', '/js/venn.js', '/css/graph.css'
];

HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl.Yii::app()->getModule("graph")->getAssetsUrl()
);

?>


<?php

    $keyTpl     = "graphOfElements";
    $paramsData = [
        "coform" => "",
        "path" => "",
        "name" => "",
        "elementTypes" => Organization::COLLECTION,
        "fields" => "type",
        "graphRoot" => "",
        "graphTypes" => ["circle"=>"Cercle D3", "mindmap"=>"Carte mentale D3", "network"=>"Réseau D3", "relation"=>"Relation D3"],
        "width" => ["4"=>4, "6"=>6, "8"=>8, "9"=>9, "10"=>10, "12"=>12],
        "graphType" => "mindmap",
        "widthValue" => "100",
        "height" => 400,
        "graph" => null,
        "graphDataToShow" => [],
        "graphColor" => "",
        "focus" => true
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) && $blockCms[$e]!="" ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }


    $title  = ($blockCms["title"] ?? [ "fr" => "Titre du graph" ]);
    $title = is_array($title) ? $title : [ "fr" => $title ];
    if(!isset($costum['langCostumActive'])){
        $costum['langCostumActive'] = "fr";
    }
    $paramsData["title"] = $title[$costum['langCostumActive']] ?? reset($title);
?>

<style>
    #graph-container-<?= $kunik ?>{
        margin-bottom: 0px !important;
        background-color: transparent !important;
        overflow: hidden !important;
        width: <?= $paramsData["widthValue"] ?>% !important
    }
</style>

<div>
    <h4 class="padding-top-5 sp-text padding-left-10 title<?= $blockKey ?>" data-field="title" data-id="<?= $blockKey ?>" ></h4>
    <div id="focus-container<?= $kunik ?>"></div>
    <div id="search-container-<?= $kunik ?>" class="searchObjCSS" style='background-color:transparent!important'></div>
    <div id="graph-container-<?= $kunik ?>"></div>
</div>

<script>

    var authorizedTags<?= $kunik ?> = <?= json_encode($paramsData["graphDataToShow"]) ?>;
    var activeFocus<?= $kunik ?> = <?= json_encode($paramsData["focus"]) ?>;
    var data<?= $kunik ?> = {};
    var fields<?= $kunik ?> = {};// Generic fiels to fetch
    var path<?= $kunik ?> = "<?= (is_array($paramsData["fields"]))?$paramsData["fields"][0]:$paramsData["fields"] ?>";// field to make statistic
    var activeCircle<?= $kunik ?> = "";
    
    if(typeof sectionDyf == "undefined"){
      var sectionDyf = {};
    }

    if(typeof tplCtx == "undefined"){
      var tplCtx = {};
    }
    
    if(contextData && typeof contextData.slug != "undefined" && typeof contextData.costum ){
        costum = (contextData)?{contextId: contextData.id, contextSlug:contextData.slug, ...contextData.costum, slug:contextData.slug}:{}
    }

    if (costum.editMode){
        cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
    }
if(typeof appendTextLangBased == "function" ){
    appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");
}

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    const getValueByPath = (object, path) => {
        if (path === undefined || path === null) {
            return object;
        }
        const parts = path.split('.');
        for (let i = 0; i < parts.length; ++i) {
            if (object === undefined || object === null) {
                return undefined;
            }
            const key = parts[i];
            object = object[key];
        }
        return object;
    }

    /**
     * liste des fields
     **/

    let extraFields = [];

    if(typeof getValueByPath(costum, "typeObj.<?php echo $paramsData['elementTypes'] ?>.dynFormCostum.beforeBuild.properties")!="undefined"){
        extraFields = costum.typeObj.<?php echo $paramsData['elementTypes'] ?>.dynFormCostum.beforeBuild.properties;
    }

    Object.keys(extraFields).forEach(function(keyValue, k) {
        let keyV = keyValue;
        let keyL = "";

        if(keyValue.indexOf("-")!=-1){
            keyV = keyValue.replace("-", ".");

        }else if(keyValue.indexOf("[")!=-1){
            keyV = keyValue.replace("[", ".").replace("]", "");
        }

        if(typeof extraFields[keyValue].inputType != "undefined" && extraFields[keyValue].inputType != "text" && extraFields[keyValue].inputType != "textarea"){

            if(typeof extraFields[keyValue] != "undefined" && typeof extraFields[keyValue].label != "undefined"){
                keyL = extraFields[keyValue].label
            }else{
                keyL = keyV;
            }

            fields<?= $kunik ?>[keyV] = (trad[keyL])?trad[keyL]:keyL;
        }
    });

    fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
    fields<?= $kunik ?>["type"] = trad.type;
    fields<?= $kunik ?>["address.level1Name"] = "Pays";
    fields<?= $kunik ?>["address.level3Name"] = "Région";
    fields<?= $kunik ?>["badges"] = "badges";
    fields<?= $kunik ?>["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)+".roles"] = "Rôles";
    
    if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
        fields<?= $kunik ?>["tags"] = trad.tags;
    }

    let defaultFilters<?= $kunik ?> = {'$or':{}, 'toBeValidated':{'$exists':false}};
    defaultFilters<?= $kunik ?>['$or']["parent."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["source.keys"] = (costum.contextId)?costum.slug:contextData.slug;
    defaultFilters<?= $kunik ?>['$or']["links.projects."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["links.events."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};
    defaultFilters<?= $kunik ?>['$or']["links.memberOf."+((costum.contextId)?costum.contextId:contextData.id)] = {'$exists':true};

    var l<?= $kunik ?> = {
        //container: "#search-container-<?= $kunik ?>",
        loadEvent: {
            default: "graph"
        },
        defaults: {
            notSourceKey:true,
            types: ["<?php echo $paramsData['elementTypes'] ?>"],
            fields: {...fields<?= $kunik ?>, "tags":"tags", "collection":"collection", "type":"type", "name":"name", "links":"links", "profilThumbImageUrl":"profilThumbImageUrl"},
            filters:defaultFilters<?= $kunik ?>,
            indexStep : 60000
        },
        graph: {
            dom: "#graph-container-<?= $kunik ?>",
            mindmapDepth: 1,
            authorizedTags: authorizedTags<?= $kunik ?>,
            defaultGraph: "<?php echo $paramsData['graphType'] ?>",
        },
        header: {
            options : {}
        },
        results:{
          dom:"#loader-container-<?= $kunik ?>"
        }
    };

    coInterface.showLoader("#loader-container-<?= $kunik ?>");

    var p<?= $kunik ?> = {};

    var root<?= $kunik ?> = "<?php echo $paramsData['graphRoot'] ?>";

    setTimeout(() => {
        jQuery.extend(p<?= $kunik ?>, searchObj)//jQuery.extend(true, {}, searchObj)
        p<?= $kunik ?>.mapContent.initEvents = function(fObj){};
        p<?= $kunik ?>.mapContent.initViews = function(fObj){};
        p<?= $kunik ?> = p<?= $kunik ?>.init(l<?= $kunik ?>);
        p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
            p<?= $kunik ?>.graph.lastResult = rawData;
            /*if(rawData.results && rawData.results.length==0){
                $("#graph-container-<?= $kunik ?>").append("<h3 style='color: #aaa; margin-top: 20%'>aucun résultat</h3>")
            }*/
            
            let valueFromPath = path<?= $kunik ?>.split(".");

            <?php if(isset($paramsData["graphType"]) && $paramsData["graphType"]=="mindmap"){ ?>

            var structuredData = {
                id:"root<?= $kunik ?>",
                label:(root<?= $kunik ?>!="")?root<?= $kunik ?>:costum.title,
                children:[]
            }

            var lists = null;
            var isCostumList = false;

            if(costum.lists && exists(costum.lists[path<?= $kunik ?>])){
                lists = costum.lists[path<?= $kunik ?>];
                data<?= $kunik ?> = {};
                $.each(lists, function(icl, vcl){
                  if(typeof vcl == "object"){
                    Object.assign(data<?= $kunik ?>, vcl);
                  }else{
                    data<?= $kunik ?>[icl]=vcl;
                  }
                });
                isCostumList == true;
            }else{
                lists = {}
                Object.keys(rawData.results).forEach((key, index) => {
                    var val = getValueByPath(rawData.results[key], path<?= $kunik ?>);
                    if(val && val!=""){
                        if(Array.isArray(val)){
                            for(var i in val){
                                if(typeof val[i]=="object"){
                                    lists[val[i]["name"]] = val[i]
                                }else{
                                    lists[val[i]] = val[i];
                                }
                            }
                        }else if(typeof val=="object" && typeof val!=null){
                            if(path<?= $kunik ?>=="badges"){
                                $.each(val, function(i, v){
                                    lists[v.name] = v.name
                                });
                            }
                        }else if(typeof val=="string"){
                            lists[val] = val;
                        }
                    }
                });
                data<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>.length==0){
                authorizedTags<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>!=null){
                for (var i in authorizedTags<?= $kunik ?>) {
                    var child = {
                        id:"element"+i,
                        label:(typeof authorizedTags<?= $kunik ?>[i] == "string")?authorizedTags<?= $kunik ?>[i]:i,
                        children: []
                    }
                    Object.keys(rawData.results).forEach((key, index) => {
                        var element = rawData.results[key];
                        var itIsIn = false;
                        if(typeof authorizedTags<?= $kunik ?>[i] == "object"){
                            for(var k in authorizedTags<?= $kunik ?>[i]){
                                if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                    itIsIn = true;
                                }
                            }
                        }

                        if(child.label == getValueByPath(element, path<?= $kunik ?>)||(Array.isArray(getValueByPath(element, path<?= $kunik ?>)) && getValueByPath(element, path<?= $kunik ?>).includes(child.label)) || (isCostumList && typeof(element.tags)!="undefined" && element.tags.includes(child.label)) || itIsIn){
                            child.children.push({
                                id:key,
                                label:element.name,
                                collection: element.collection
                            });
                        }else if(path<?= $kunik ?>=="badges"){
                            if(typeof element.badges != "undefined" && element.badges != null){
                                $.each(element.badges, function(ieb, eb){
                                    if(eb.name==child.label){
                                        child.children.push({
                                            id:key,
                                            label:element.name,
                                            collection:element.collection
                                        });
                                    }
                                });
                            }
                        }
                    });
                    structuredData["children"].push(child)
                }
            }
        <?php } ?>


        <?php if(isset($paramsData["graphType"]) && in_array($paramsData["graphType"], array("network","circle", "relation"))){ ?>

            var structuredData = [];

            <?php if($paramsData["graphType"]=="network"){ ?>
            structuredData.push({
                label: (root<?= $kunik ?>!="")?root<?= $kunik ?>:costum.title,
                type: "root",
                group: "root"
            });
            <?php } ?>

            var lists = null;
            var isCostumList = false;

            if(costum.lists && exists(costum.lists[path<?= $kunik ?>])){
                lists = costum.lists[path<?= $kunik ?>];
                data<?= $kunik ?> = {};
                $.each(lists, function(icl, vcl){
                  if(typeof vcl == "object"){
                    Object.assign(data<?= $kunik ?>, vcl);
                  }else{
                    data<?= $kunik ?>[icl]=vcl;
                  }
                });
                isCostumList = true;
            }else{
                lists = {};
                $.each(rawData.results, (key, index) => {
                    var val = getValueByPath(rawData.results[key], path<?= $kunik ?>);
                    if(val && val!=""){
                        if(Array.isArray(val)){
                            for(var i in val){
                                if(typeof val[i]=="object"){
                                    lists[val[i].name] = val[i]
                                }else{
                                    lists[val[i]] = val[i];
                                }
                            }
                        }else if(typeof val=="object" && val!=null){
                            if(path<?= $kunik ?>=="badges"){
                                $.each(val, function(i, v){
                                    lists[v.name] = v.name
                                });
                            }else{
                                $.each(val, function(k, v){
                                    lists[k] = v.name;
                                });
                            }
                        }else if(typeof val=="string"){
                            lists[val] = val;
                        }
                        
                    }
                });
                data<?= $kunik ?> = lists;
            }

            if(authorizedTags<?= $kunik ?>.length==0){
                authorizedTags<?= $kunik ?> = lists;
            }
            
            if(authorizedTags<?= $kunik ?>!=null){
                $.each(authorizedTags<?= $kunik ?>, function(i, authTag){
                    var child = {
                        id:"element"+i,
                        label:(typeof authTag == "string")?authTag:i,
                        children: []
                    }
                    Object.keys(rawData.results).forEach((key, index) => {
                        var element = rawData.results[key];
                        let itIsIn = false;
                        if(typeof authTag == "object"){
                            for(var k in authTag){
                                if(typeof element.tags!="undefined" && element.tags.includes(k)){
                                    itIsIn = true;
                                }
                            }
                        }
                        if(
                            child.label==getValueByPath(element, path<?= $kunik ?>)
                            || (Array.isArray(getValueByPath(element, path<?= $kunik ?>)) && getValueByPath(element, path<?= $kunik ?>).includes(child.label)) 
                            || ((isCostumList||path<?= $kunik ?>=="tags") && typeof element.tags !="undefined" && element.tags.includes(child.label)) 
                            || itIsIn
                            ){
                                structuredData.push({
                                    "id":key,
                                    "label":element.name,
                                    "collection": element.collection,
                                    "type": child.label,
                                    "group": child.label,
                                    "img":element.profilThumbImageUrl
                                });
                        }else if(path<?= $kunik ?>=="badges"){
                            if(typeof element.badges != "undefined" && element.badges != null){
                                $.each(element.badges, function(ieb, eb){
                                    if(eb.name==child.label){
                                        structuredData.push({
                                            "id":key,
                                            "label":element.name,
                                            "collection": element.collection,
                                            "type": eb.name,
                                            "group": eb.name,
                                            "img":element.profilThumbImageUrl
                                        });
                                    }
                                });
                            }
                        }
                    });
                });
            }
        <?php } ?>
        p<?= $kunik ?>.graph.graph.setOnZoom((e,d,n) => {
            this.tooltip.goToNode();
        });
        p<?= $kunik ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
        p<?= $kunik ?>.graph.graph.initZoom();
        if(activeFocus<?= $kunik ?>){
            addFocuserFilter(authorizedTags<?= $kunik ?>);
        }
        coInterface.bindLBHLinks();

        function addFocuserFilter(tags){
            var authTagsData = (Array.isArray(tags))?tags:Object.keys(tags);
            authTagsData = authTagsData.map(v=> v.trim());
            authTagsData = (new Set(authTagsData));
            d3.select("div#focus-container<?= $kunik ?>")
            .selectAll("button")
            .data(authTagsData)
            .join((enter) => {
                enter.append("xhtml:button")
                  .text(d => d)
                  .classed("btn margin-5 btn-circle-zoomer", true)
                  .on("click", (e,d) => {
                    var thisElement = $(e.target);
                    if(activeCircle<?= $kunik ?> != d){
                      p<?= $kunik ?>.graph.graph.focus(d);
                      activeCircle<?= $kunik ?> = d;
                      $(".btn-circle-zoomer").removeClass("btn-primary");
                      thisElement.addClass("btn-primary");
                    }else{
                      activeCircle<?= $kunik ?> = "";
                      thisElement.removeClass("btn-primary");
                      p<?= $kunik ?>.graph.graph.unfocus().then(() => {
                        mylog.log("UNFOCUSED");
                      });
                    }
                    
                    $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                    $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $("#"+GraphUtils.slugify(d)).addClass("in");
                })
                /*.append("xhtml:span")
                .classed("badge badge-theme-count margin-left-5", true)
                .attr("data-countvalue", d => d.trim())
                .attr("data-countkey", d => d.trim())
                .attr("data-countlock", "false");*/
            });
        }

        // Dynform after load
        if(costum.editMode){
            sectionDyf.<?php echo $kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Configuration de graph",
                    "description" : "Personnaliser votre graphe",
                    "icon" : "fa-cog",
                    "properties" : {
                        "graphRoot" : {
                            "inputType" : "text",
                            "label" : "Racine du graph",
                            "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.graphRoot
                        },
                        "graphType" : {
                            "inputType" : "select",
                            "label" : "Quelle type de graph",
                            "class" : "form-control <?php echo $kunik ?>",
                            "rules" : {
                                "required" : true
                            },
                            "options" :  sectionDyf.<?php echo $kunik ?>ParamsData.graphTypes,
                            "value":sectionDyf.<?php echo $kunik ?>ParamsData.graphType
                        },
                        "graphColor" : {
                            "label" : "Couleur de cercle de graph",
                            "inputType" : "colorpicker",
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.graphColor
                        },
                        "elementTypes" : {
                            "inputType" : "select",
                            "label" : "Type d'élément",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.elementTypes,
                            "class" : "form-control",
                            "options" : {
                                "organizations" : trad.organizations,
                                "events" : trad.events,
                                "projects" : trad.projects,
                                "citoyens" : trad.person
                            }
                        },
                        "fields" :{
                            "inputType" : "select",
                            "label" : "Champs cible",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.fields,
                            "class" : "form-control",
                            "options" : fields<?= $kunik ?>
                        },
                        "graphDataToShow" :{
                            "inputType" : "selectMultiple",
                            "isSelect2" : true,
                            "label" : "Données à Afficher",
                            "class" : "form-control",
                            "noOrder" : true,
                            "placeholder" : "Données à Afficher",
                            "options" : data<?= $kunik ?>,
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.graphDataToShow
                        },
                        "height" : {
                            "inputType" : "text",
                            "label" : "Hauteur du graph",
                            "rules":{
                                "number":true
                            },
                            "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.height
                        },
                    },
                    afterBuild : function(){
                        // Initialize something here
                    },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                            tplCtx.value[k] = $("#"+k).val();
                        });
                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                toastr.success("La configuration de graph a été mis à jour");
                                    $("#ajax-modal").modal('hide');
                                    var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                    var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                    var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                    cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                    // urlCtrl.loadByHash(location.hash);
                            });
                        }
                    }
                }
            }

            $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
                tplCtx.id = $(this).data("id");
                tplCtx.collection = $(this).data("collection");
                tplCtx.path = "allToRoot";
                dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            });
        }

        

        }

        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);

        setTimeout(() => {
            $("#loader-container-<?= $kunik ?>").remove();
            <?php if($paramsData["graphColor"]!=""){ ?>
                $("#graph-container-<?= $kunik ?> circle").css({
                    "fill":"<?= $paramsData["graphColor"] ?>"
                })
            <?php } ?>
        }, 1000);

    }, 200);
</script>