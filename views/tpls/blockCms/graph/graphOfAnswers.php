<?php 

if($costum["contextType"] && $costum["contextId"]){
    $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
    if(isset($costum["dashboard"])){   
        $el["costum"]["dashboard"] = $costum["dashboard"];
    }
}

?>

<style>
    #graph-container-<?= $kunik ?>{
        margin-bottom: 0px !important;
        width: 100% !important;
        overflow: hidden !important;
    }

    #graph-container-<?= $kunik ?> svg:not(:root) {
        min-height: 340px !important;
    }
</style>

<?php
    if (isset($costum["contextType"]) && isset($costum["contextId"])) {
        $graphAssets = [
            '/plugins/d3/d3.v6.min.js', '/js/venn.js', '/js/graph.js', '/css/graph.css'
        ];
        
        HtmlHelper::registerCssAndScriptsFiles(
            $graphAssets,
            Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
        );
    }
?>

<?php 

    // formulaire
    $paramsData = [
        "title" => "",
        "coform" => "",
        "path" => "",
        "name" => "",
        "type" => ["circle"=>"Cercle D3", "mindmap"=>"Carte mentale D3", "network"=>"Réseau D3", "relation"=>"Relation D3"],
        "width" => ["4"=>4, "6"=>6, "8"=>8, "9"=>9, "10"=>10, "12"=>12],
        "typeValue" => "circle",
        "widthValue" => "",
        "graph" => null
    ];
    if(!isset($childForm) && !isset($formInputs)){
        // Get all Forms
        $childForm = PHDB::find("forms", array("parent.".$costum['contextId']=>['$exists'=>true]));
        $formInputs = [];
        foreach ($childForm as $formKey => $formValue) {
            $subForms = PHDB::find(Form::COLLECTION, array(
                'id' => array('$in' => $formValue["subForms"])));
            foreach ($subForms as $subFormKey => $subFormValue) {
                if(isset($subFormValue["inputs"])){
                    $formInputs[$formKey][$subFormValue["id"]]=$subFormValue["inputs"];
                }
            }
        }
    }

    if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
      }
    }
  }

    // Default Form
    if(isset($formId)){
        $formTL = PHDB::findOneById(Form::COLLECTION, $formId);
    }else{
        $formTL = PHDB::findOne(Form::COLLECTION, array("parent.".$costum["contextId"] => array('$exists' => true)));
    }
    
    $formConfig = [];
    
    if(isset($path) && isset($formTL) && isset($formTL["params"][explode(".", $path)[1]])){
        $formConfig = $formTL["params"][explode(".", $path)[1]];
    }
 ?>

<div>
    <div id="search-container-<?= $kunik ?>" class="searchObjCSS" style='background-color:white!important'></div>
    <div id="loader-container-<?= $kunik ?>"></div>
    <div id="graph-container-<?= $kunik ?>"></div>
</div>

<script>
    jQuery(document).ready(function() {
        contextData = <?php echo json_encode($el); ?>;
        var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
        var contextType = <?php echo json_encode($costum["contextType"]); ?>;
        var contextName = <?php echo json_encode($el["name"]); ?>;
        contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
    });
</script>

<script>
    var rawTags = "";
    var authorizedTags = []
    if(rawTags.trim() != ""){
        authorizedTags = rawTags.split(',');
    }

    <?php if(isset($formTL['_id']) || isset($formId)){ ?>
    var l<?= $kunik ?> = {
        //container: "#search-container-<?= $kunik ?>",
        loadEvent: {
            default: "graph"
        },
        defaults: {
            types: ["answers"],
            fields: ["collection", "user", "answers"],
            indexStep: 100,
            filters: {
                "form": "<?php echo isset($formId)?$formId:(string)$formTL['_id'] ?>",
                "draft": {'$exists':false},
                "answers": {'$exists':true}
            }
        },
        graph: {
            dom: "#graph-container-<?= $kunik ?>",
            authorizedTags: [],
            defaultGraph: "<?php echo $paramsData['typeValue'] ?>", //  mindmap, relation , network
        },
        header: {
            options : {}
        }
    };
    coInterface.showLoader("#loader-container-<?= $kunik ?>");
    let namePath = "<?php echo $paramsData["name"] ?>";
    if(namePath!=""){
        namePath = namePath.split(".");
    }
    var path<?= $kunik ?> = "";
    var root<?= $kunik ?> = "";
    var formConfig<?= $kunik ?> = <?php echo json_encode($formConfig) ?>;
    
    var p<?= $kunik ?> = {};

    <?php if(isset($path)){ ?>
        path<?= $kunik ?> = "<?php echo $path ?>";
    <?php } ?>

    <?php if(isset($title)){ ?>
        root<?= $kunik ?> = "<?php echo $title ?>";
    <?php } ?>


    extractLabelsFromAnswers = function(inputData){
        let extractedData;
        if(Array.isArray(inputData)){
            extractedData = [];
            for (let i = inputData.length - 1; i >= 0; i--) {
                if(typeof inputData[i] === 'string' && inputData[i]!==null){
                    extractedData.push(inputData[i]);
                }else{
                    extractedData.push(Object.keys(inputData[i])[0]);
                }
            }
        }else{
            if(inputData && inputData["value"]){
                extractedData = inputData["value"];
            }else{
                extractedData = "<?php echo $paramsData['title'] ?>";
            }
        }
        return extractedData;
    }

    
    
    setTimeout(() => {
        p<?= $kunik ?> = searchObj.init(l<?= $kunik ?>);

        <?php if($formConfig != []){ ?>

        <?php if(isset($paramsData["typeValue"]) && $paramsData["typeValue"]=="mindmap"){ ?>
        p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $kunik ?>.split(".");

            let structuredData = {
                id:"root<?= $kunik ?>",
                label:(root<?= $kunik ?>!="")?root<?= $kunik ?>:"<?php echo $paramsData['title'] ?>",
                children:[]
            };
            for (var i = formConfig<?= $kunik ?>["global"]["list"].length - 1; i >= 0; i--) {
                let child = {
                    id:"element"+i,
                    label:formConfig<?= $kunik ?>["global"]["list"][i],
                    children: []
                }
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    let identity = {};
                    if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length==2){
                        identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                    }else{
                        identity["label"] = "Non définit";
                    }
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }

                    if(extracted && extracted.includes(formConfig<?= $kunik ?>["global"]["list"][i])){
                        child.children.push({
                            id:key,
                            ...identity,
                            collection: rawData.results[key].collection
                        })
                    }
                });
                structuredData["children"].push(child)
            }

            p<?= $kunik ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $kunik ?>.graph.graph.initZoom();
        }
        <?php } ?>


        <?php if(isset($paramsData["typeValue"]) && $paramsData["typeValue"]=="network" ){ ?>
        p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $kunik ?>.split(".");
            
            let structuredData = [
                {
                    label: (root<?= $kunik ?>!="")?root<?= $kunik ?>:"Tiers Lieux",
                    type: "root",
                    group: "root"
                }
            ];

            for (var i = formConfig<?= $kunik ?>["global"]["list"].length - 1; i >= 0; i--) {
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    let identity = {};
                    if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length == 2){
                        identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                    }else{
                        identity["label"] = "Non définit";
                    }
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }
                    if(extracted && extracted.includes(formConfig<?= $kunik ?>["global"]["list"][i])){
                        structuredData.push({
                            id:key,
                            ...identity,
                            collection: rawData.results[key].collection,
                            type: formConfig<?= $kunik ?>["global"]["list"][i],
                            group: formConfig<?= $kunik ?>["global"]["list"][i],
                            img:""
                        })
                    }
                });
            }

            p<?= $kunik ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $kunik ?>.graph.graph.initZoom();
        }
        <?php } ?>

        <?php if(isset($paramsData["typeValue"]) && $paramsData["typeValue"]=="circle"){ ?>
        p<?= $kunik ?>.graph.successComplete = function(fObj, rawData){
            const tags = fObj.search.obj.tags ? fObj.search.obj.tags : [];
            fObj.graph.graph.setAuthorizedTags(tags)
            this.lastResult = rawData;
            let answerPath = path<?= $kunik ?>.split(".");
            let structuredData = [];

            for (var i = formConfig<?= $kunik ?>["global"]["list"].length - 1; i >= 0; i--) {
                Object.keys(rawData.results).forEach((key, index) => {
                    let extracted = null;
                    if(rawData.results[key].answers[answerPath[0]] && rawData.results[key].answers[answerPath[0]][answerPath[1]]){
                        extracted = extractLabelsFromAnswers(rawData.results[key].answers[answerPath[0]][answerPath[1]]);
                    }
                    if(extracted && extracted.includes(formConfig<?= $kunik ?>["global"]["list"][i])){
                        let identity = {};
                        if(rawData.results[key].answers && Array.isArray(namePath) && namePath.length==2){
                            identity["label"] = rawData.results[key].answers[namePath[0]][namePath[1]]
                        }else{
                            identity["label"] = "Non définit";
                        }

                        structuredData.push({
                            id: key,
                            ...identity,
                            group: (formConfig<?= $kunik ?>["global"]["list"][i])?formConfig<?= $kunik ?>["global"]["list"][i]:"",
                            img: "",
                            collection:rawData.results[key].collection
                        })
                    }
                });
            }
            p<?= $kunik ?>.graph.graph.updateData(structuredData);//this.graph.preprocessResults(rawData.results)
            p<?= $kunik ?>.graph.graph.initZoom();
        }
        <?php } ?>
        <?php } ?>
        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);
        
        setTimeout(() => {
            p<?= $kunik ?>.graph.graph.initZoom();
            $("#loader-container-<?= $kunik ?>").remove();
        },500);
    
    }, 200)
<?php } ?>
</script>

<script type="text/javascript">
    let sectionDyf = {};
    let tplCtx = {};


    jQuery(document).ready(function() {
        if(localStorage.getItem("previewMode") && localStorage.getItem("previewMode")=="v"){
            $(".dashboarConfigBtn").hide();
            $(".removeConfigBtn").hide();
        }else{
            $(".removeConfigBtn").show();
            $(".dashboarConfigBtn").show();
        }
        
        sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
                "title" : "Configuration de graph",
                "description" : "Personnaliser votre graphe",
                "icon" : "fa-cog",
                "properties" : {
                    "title" : {
                        "inputType" : "text",
                        "label" : "Titre du graph",
                        "value" :  sectionDyf.<?php echo $kunik ?>ParamsData.title
                    },
                    "url" : {
                        "inputType" : "select",
                        "label" : "Quelle type de graph",
                        "class" : "form-control <?php echo $kunik ?>",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $kunik ?>ParamsData.type,
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.typeValue
                    },
                    "coform": {
                        "label" : "Choisir un Formulaire :",
                        "class" : "form-control <?php echo $kunik ?>",
                        "inputType" : "select",
                        "rules" : {
                            "required" : true
                        },
                        "options": {
                            <?php 
                            foreach($childForm as $key => $value) { 
                                echo  '"'.$key.'" : "'.$value["name"].'",';
                            }    
                            ?>
                        },
                        "value": sectionDyf.<?php echo $kunik ?>ParamsData.coform
                    },
                    "path" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "À Quelle Question corresponds la graph",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Chercher la question",
                        "options": {}
                    },
                    "name" : {
                        "inputType" : "select",
                        "class" : "<?php echo $kunik ?>",
                        "label" : "Selectionner une champ dans pour représenter la réponse",
                        "rules" : {
                            "required" : true
                        },
                        "isSelect2":true,
                        "placeholder":"Nom de l'entité dans la réponse",
                        "options": {}
                    },
                    "width" : {
                        "inputType" : "select",
                        "class" : "form-control <?php echo $kunik ?>",
                        "label" : "Largeur d'espace à occuper",
                        "rules" : {
                            "required" : true
                        },
                        "options" :  sectionDyf.<?php echo $kunik ?>ParamsData.width,
                        "value":sectionDyf.<?php echo $kunik ?>ParamsData.widthValue
                    },
                },
                afterBuild : function(){
                    if(sectionDyf.<?php echo $kunik ?>ParamsData.coform!=""){
                        updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.coform);
                    }
                    if($("#path.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.path+"']").length > 0){
                        $("#path.new-graph").val(sectionDyf.<?php echo $kunik ?>ParamsData.path);
                    }
                    if($("#name.<?php echo $kunik ?> option[value='"+sectionDyf.<?php echo $kunik ?>ParamsData.name+"']").length > 0){
                        $("#name.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.name);
                    }
                    
                },
                save : function (data) {  
                    tplCtx.value = {};
                    tplCtx.value["graph"] = {"key":"<?php echo $kunik ?>", "data":{}};
            
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        if(k != "title" && k != "width"){
                            if(k == "url"){
                                tplCtx.value["graph"][k] = "/graph/co/dash/g/costum.views.custom.franceTierslieux.graph.d3Common/type/"+$("#"+k).val();
                                tplCtx.value["type"] = $("#"+k).val();
                            }else{
                                tplCtx.value["graph"]["data"][k] = $("#"+k).val();
                            }
                        }else{
                            tplCtx.value[k] = $("#"+k).val();
                        }
                    });

                    tplCtx.value["counter"] = "";

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            toastr.success("La configuration de graph a été mis à jour");
                                $("#ajax-modal").modal('hide');
                                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                                // urlCtrl.loadByHash(location.hash);
                        });
                    }
                }
            }
        }

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        $(".remove<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx["value"] = {};

            bootbox.dialog({message:`<div class="alert-white text-center"><br>
                <strong>Vous voulez vraiement supprimer cette section de graph</strong>
                <br><br>
                <button id="deleteGraphBtn" class="btn btn-danger bootbox-close-button">JE CONFIRME</button>
                <button type="button" class="btn btn-default bootbox-close-button" aria-hidden="true">NON, ANNULER</button></div>`});

                $("#deleteGraphBtn").on("click", function(){
                    dataHelper.path2Value( tplCtx, function(params) {
                        toastr.info("La suppression de graph <?php $paramsData['title'] ?>");
                        $("#ajax-modal").modal('hide');
                        var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                        var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                        var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                        cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        // urlCtrl.loadByHash(location.hash);
                    });
                });
        });

        $(document).on("change", "#coform.<?php echo $kunik ?>", function(){
            updateInputList($(this).val());
        });

        let updateInputList = function(value){
            let childForm = <?php echo json_encode($formInputs) ?>;
            $("#path.<?php echo $kunik ?>").empty();

            for(const stepKey in childForm[value] ){
                for(const inputKey in childForm[value][stepKey]){
                    let input = childForm[value][stepKey][inputKey];
                    if(input["type"].includes("multiCheckboxPlus")){
                        $("#path.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiCheckboxPlus'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"].includes("multiRadio")){
                        $("#path.<?php echo $kunik ?>").append('<option value="'+stepKey+'.multiRadio'+inputKey+'" >'+input["label"]+'</option>');
                    }
                    if(input["type"]=="text"){
                        $("#name.<?php echo $kunik ?>").append('<option value="'+stepKey+'.'+inputKey+'" >'+input["label"]+'</option>');
                    }
                }
            }
        }
    });
    
</script>