<?php 
    $keyTpl = "worldNavigateCocity";
    $myCmsId  = isset($blockCms["_id"]) ? $blockCms["_id"]->{'$id'} : null;
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];
?>
<style>

.card-counter {
    background-color: #2E86C1;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
    padding: 20px;
    margin: 10px 0;
    color: #fff;
    text-align: center;
    position: relative;
    transition: transform 0.3s ease, box-shadow 0.3s ease;
    cursor: pointer;
}
.card-counter:hover {
    transform: translateY(-10px);
    box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
}

.card-counter.primary {
    background-color: #4CAF50; 
}

.card-counter.success {
    background-color: #FF5733;
}

.card-counter.info {
    background-color: #3498DB;
}

.card-counter.pruple {
    background-color: #9B59B6;
}

.card-counter.green {
    background-color: #E67E22;
}

.card-counter.warning {
    background-color: #F39C12; 
}

.card-counter.danger {
    background-color: #E74C3C; 
}

.card-counter.secondary {
    background-color: #95A5A6; 
}

.card-counter.light {
    background-color: #F1C40F; 
}

.card-counter.dark {
    background-color: #2C3E50; 
}

.card-counter i {
    font-size: 40px;
    margin-bottom: 10px;
}
.card-counter .count-numbers {
    font-size: 24px;
    font-weight: bold;
    display: block;
}
.card-counter .count-name {
    font-size: 18px;
    font-weight: 500;
    margin-top: 5px;
}
@media (max-width: 768px) {
    .card-counter {
        padding: 15px;
        margin: 10px 5px;
    }
}

.div-containte, .div-containte2 {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
}
.section-title {
    font-size: 22px;          
    font-weight: bold;       
    color: #2C3E50;          
    text-align: center;       
    margin-bottom: 10px;      
    text-transform: uppercase;
    margin-top: 4%; 
}

.section-description {
    font-size: 16px;          
    color: #7F8C8D;           
    text-align: center;       
    margin-bottom: 20px;      
    line-height: 1.5;         
    font-style: italic;       
}

.card-number {
    position: absolute;
    top: 10px;                  
    left: 10px;                 
    background-color: #fff;    
    color: #333;                
    border-radius: 50%;         
    width: 30px;                
    height: 30px;               
    display: flex;
    align-items: center;        
    justify-content: center;    
    font-size: 14px;            
    font-weight: bold;          
    z-index: 1;                 
}

.text-filters {
    text-align: left;
    box-shadow: rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;
    width: 100%;
    z-index: 1111;
    background-color: white;
    height: 55px;
    position: fixed;
}

.text-filters .searchBar-filters .main-search-bar-addon {
    background-color: #00b5c0;
}

.card-counter.active-filter {
    transform: scale(0.7);
}

.card-counter.active-filter .fa, .card-counter.clicked .fa {
    display: none;
}

.card-counter.clicked {
    transform: scale(0.8);
    box-shadow: 0 4px 15px #84d70f;
    border: 2px solid #16a085; 
}

.div-containte .card-counter.clicked {
    margin-top: -1%;
}

.container-filters {
    height: 90px !important;
    margin-top: -10px;
}

.results-filters {
    margin-top: 2%;
}

</style>

<div class="text-filters hidden">

</div>
<div class="thematique<?= $kunik ?>">
    <h2 class="section-title">Top 10 des Thématiques les Plus Actives</h2>
    <p class="section-description">Découvrez les thématiques les plus dynamiques, classées par leur niveau d'activité. Ces sujets se distinguent par leur engagement et leur impact.</p>
    <div class="container-section div-containte row">

    </div>
    <div class="container-section div-containte2 row">

    </div>

    <div class="results-filters">

    </div>
</div>
<script>
    let _allData = <?php echo json_encode($allDatas) ?>;
    let _thematic = <?php echo json_encode($thematic) ?>;

    let colorClass = [
        'primary',   
        'purple',    
        'green',     
        'info',      
        'success',   
        'warning',   
        'danger',     
        'secondary',  
        'light',      
        'dark' 
    ];
    let htmlTop = "";
    let htmlBottom = "";

    function list() {

        let top5 = Object.entries(_allData).slice(0, 5);
        let bottom5 = Object.entries(_allData).slice(5, 10);
        
        $.each(top5, function(index, [key, value]) {
            if (value > 0) {
                let color = colorClass[index % colorClass.length];
                htmlTop += `<div class="col-md-2 col-xs-6 datacitysection datacitysection_country">
                                <div class="card-counter ${color}" onclick="filtersThema('${key}')">
                                    <span class="card-number">${index + 1}</span>
                                    <i class="fa ${allThem[key]['icon']}"></i>
                                    <span class="count-numbers">${value}</span>
                                    <span class="count-name">${key}</span>
                                </div>
                            </div>`;
            }
        });

        $.each(bottom5, function(index, [key, value]) {
            if (value > 0) {
                let color = colorClass[(index +5) % colorClass.length];
                htmlBottom += `<div class="col-md-2 col-xs-6 datacitysection datacitysection_country">
                                <div class="card-counter ${color}" onclick="filtersThema('${key}')">
                                    <span class="card-number">${index + 6 }</span>
                                    <i class="fa ${allThem[key]['icon']}"></i>
                                    <span class="count-numbers">${value}</span>
                                    <span class="count-name">${key}</span>
                                </div>
                            </div>`;
            }
        });

        $(".div-containte").html(htmlTop );
        $(".div-containte2").html( htmlBottom);

        // $(".card-counter")
    }

    function filtersThema(thematic) {
        $('.card-counter').removeClass('clicked');
        $('.card-counter').addClass('active-filter');
        $('.container-section').addClass('container-filters');
        $(`.card-counter:contains(${thematic})`).removeClass('active-filter');
        $(`.card-counter:contains(${thematic})`).addClass('clicked');

        let paramsFilter = {
	 	    container : ".text-filters",
            defaults : {
                notSourceKey:true,
                indexStep : 0,
                filters : {
                    "thematic" : _thematic[thematic]['thematique']			
                },
                types : ["events",  "organizations", "poi", "projects","ressources"]
            },
            results : {
                dom : ".results-filters",
                smartGrid : true,
                renderView : "directory.elementPanelHtmlSmallCard"
            },
            loadEvent:{
                default:"scroll"
            },
            filters : {
                text : true
            }
        }

        filterSearch = searchObj.init(paramsFilter);
        filterSearch.search.init(filterSearch);
        $(".text-filters").removeClass('hidden');
    }

    $(document).ready(function(){
        list();
    })
</script>