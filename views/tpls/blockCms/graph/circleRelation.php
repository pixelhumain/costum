<?php
$keyTpl     = "circleRelation";
$elementsCostum = "";
if(isset($costum["contextType"]) && isset($costum["contextId"])){
	$elementsCostum = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
}
$paramsData = [
    "dataSource" => [$costum["contextId"] => ["name" => $costum["contextSlug"], "type" => $costum["contextType"]]],
    "tags" => "",
    "depth" => 1,
    "isSearch" => false,
    "isClickAndScrollToZoom" => true,
    "isTagFilter" => false,
    "tagDirect" => true,
    "circlerelations" => [],
    "positions" => [],
    "height" => "100%",
    "noPreview" => false,
    "relationColor" => "#1A2660",
    "circleColor" => "white",
    "textZoomIn" => "Zoom Avant",
    "textZoomOut" => "Zoom Arrière",
    "textMagnify" => "Loupe",
    "textZoomReset" => "Recadrer",
    "textZoomFullScreen" => "Plein Ecran",
    "circlePadding" => 5,
    "externalCircleMargin" => 0
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
foreach($paramsData["circlerelations"] as $index => $value){
  $paramsData["circlerelations"][$index] = ["source" => strtolower($paramsData["circlerelations"][$index]["source"]), "target" => strtolower($paramsData["circlerelations"][$index]["target"])];
}
?>


<?php
if (isset($costum["contextType"]) && isset($costum["contextId"])) {
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>
<script>
  jQuery(document).ready(function() {
    contextData = <?php echo json_encode($el); ?>;
    var contextId = <?php echo json_encode((string) @$el["_id"]); ?>;
    var contextType = <?php echo json_encode($costum["contextType"]); ?>;
    var contextName = <?php echo json_encode(@$el["name"]); ?>;
    contextData.id = <?php echo json_encode((string) @$el["_id"]); ?>;
  });
</script>

<style>
  #graph-container-<?= $kunik ?>{
    height: <?= $paramsData["height"] ?>;
    width: 100%;
    overflow: hidden;
    background-color: white !important;
  }

  svg#graph{
    height: <?= $paramsData["height"] ?>;
  }

  .super-cms .graph-panel{
    background-color: white !important;
    width: 100%;
    height: 100%;
  }

  .listing-item {
    height: 100% !important;
    border: 1px solid #ddd !important;
    min-height: 80px;
  }

  #graph-cover-<?= $kunik ?>{
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    z-index: 0;
    cursor: no-drop;
  }

  @media only screen and (max-width: 600px) {
    #graph{
      height: 500px;
      width: 100%!important;
    }
  }
  .main-container .portfolio-modal.modal, .portfolio-modal.modal {
      background: rgba(32, 42, 95, 0.7)!important;
  }
  @media (min-width: 768px) {
      #openModal .modal-content {
          width: 70%;
          margin-left: 15%;
      }
      .portfolio-modal.modal .modal-content .container {
          width: 90%;
          margin-right: 5%;
          margin-left: 5%;
      }
  }

  .portfolio-modal .close-modal {
    z-index: 10 !important;
      width: auto;
}

.portfolio-modal .close-modal .lr, .portfolio-modal .close-modal .lr .rl {
    background-color: #2C3E50;
    width:3px !important;
}

  <?php if($paramsData["circleColor"]!=""){ ?>  
    g.divide>circle{
      fill: <?= $paramsData["circleColor"] ?> !important;
    }
  <?php } ?>

  <?php if($paramsData["relationColor"]!=""){ ?>
    line.links-line {
      stroke: <?= $paramsData["relationColor"] ?> !important;
    }
  <?php } ?>
</style>
<div style="justify-content:center">
<div class="filters-container-circle hidden-sm hidden-xs">
  <div id="circle-filter-container<?= $kunik ?>" class="margin-top-10 text-center"></div>
  <?php if($paramsData["isSearch"] == "true"){ ?>
      <div id="search-container-<?= $kunik ?>" class="col-xs-12 searchObjCSS"></div>
  <?php } ?>
</div>


<div id="graph-container-<?= $kunik ?>" class="graph-panel">
  <?php if($paramsData["isClickAndScrollToZoom"]=="true"){ ?>
    <div id="graph-cover-<?= $kunik ?>" title="Double-cliquer pour activer/desactiver le scroll"></div>
  <?php } ?>
</div>
</div>

<script>
    var epsilon = 1;
    var rawTags = "<?= $paramsData["tags"] ?>";
    var authorizedTags = [];
    var crs = <?= json_encode($paramsData["circlerelations"])?>;
    var circleLinks = [];

    let defaultFilters = {"$or":{}};
    defaultFilters['$or']["parent."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["source.keys"] = costum.slug;
    defaultFilters['$or']["reference.costum"] = costum.slug;
    defaultFilters['$or']["links.projects."+costum.contextId] = {'$exists':true};
    defaultFilters['$or']["links.memberOf."+costum.contextId] = {'$exists':true};
    defaultFilters["toBeValidated"]={'$exists':false};

    if(costum && costum.slug=="meir"){
      defaultFilters["category"]="acteurMeir";
    }
    // 
    var dataCostum = <?php echo json_encode( $elementsCostum ) ?>;
    if(typeof dataCostum.costum.typeCocity != "undefined" && typeof dataCostum.thematic != "undefined" && dataCostum.thematic != ""){
      let themTemp = dataCostum.thematic;
      if(typeof themTemp === 'string' ) {
        if(typeof costum.tagsToShow != "undefined" && costum.tagsToShow != "" && costum.tagsToShow != []) {
          themTemp = costum.tagsToShow; 
        } else {
          themTemp = [themTemp];
        }
      } 
      defaultFilters['$or']["$and"] = setparamsAndThematic(themTemp);
      let them = themTemp.map(item => {
        var normalizedItem = item.replace(/\s+/g, '').toLowerCase();
        return normalizedItem === 'tierslieux' ? 'TiersLieux' : item;
      });
      
      authorizedTags = them;
      crs = generateTagPermutations(authorizedTags);
    } else if(rawTags.trim() != ""){
      const decodedAnd = rawTags.replace("&amp;",'&');
      authorizedTags = decodedAnd.split(',');
    }

    $.each(crs, function(i, el){
      if(el.source.includes("&amp;") || el.target.includes("&amp;")){
        el.source = el.source.replace("&amp;", '&');
        el.target = el.target.replace("&amp;", '&');
      }
      circleLinks.push({"source": el.source, "target": el.target});
    });

    function setparamsAndThematic(thematic) {
      var dataAnd = [];

      var idAddress = {};
      if(typeof dataCostum.address != "undefined") {
        var mapping = {
          region: "address.level3",
          departement: "address.level4",
          district: "address.level4",
          epci: "address.level4",
          ville: "address.localityId"
        };

        var key = mapping[costum.typeCocity];

        if (key) {
            idAddress = { [key]: dataCostum.address[key.split('.').pop()] };
        }
      }

      let thems = thematic.map(item => {
        var normalizedItem = item.replace(/\s+/g, '').toLowerCase();
        return normalizedItem === 'tierslieux' ? 'TiersLieux' : item;
      });

      if(typeof dataCostum.address != "undefined") {
        dataAnd = [
          idAddress,
          {
            "tags" : {
              '$in' : thems
            } 
          }
        ]
      } else {
        dataAnd = [
          {
            "tags" : {
              '$in' : thems
            } 
          }
        ]
      }
      
      return dataAnd;
    }

    function generateTagPermutations(tags) {
      let permutations = [];

      for (let i = 0; i < tags.length; i++) {
          for (let j = 0; j < tags.length; j++) {
              if (i !== j) {
                  // Vérifier si l'inverse existe déjà
                  let inverseExists = permutations.some(permutation => 
                      permutation.source === tags[j] && permutation.target === tags[i]
                  );
                  
                  if (!inverseExists) {
                      permutations.push({
                          source: tags[i],
                          target: tags[j]
                      });
                  }
              }
          }
      }

      return permutations;
    }

    var l<?= $kunik ?> = {
      container: "#search-container-<?= $kunik ?>",
      loadEvent: {
        default: "graph"
      },
      defaults: {
        notSourceKey:true,
        types: ["organizations"],
        indexStep: 500,
        filters: defaultFilters
      },
      results: {
        dom: "#loader-container"
      },
      graph: {
        dom: "#graph-container-<?= $kunik ?>",
        authorizedTags: authorizedTags,
        defaultGraph: "circlerelation",
        circlerelations: circleLinks,
        initPosition: <?= json_encode($paramsData["positions"]) ?>
      },
      filters: {
        text: true,
      },
      header: {
        options : {
        }
      },
      obj:{}
    }
  var costumColors = ["#1a2660", "#671d67", "#c679b4", "#006400", "#003059", "#401e5b", "#501e5f","#a6deee"];
  // var costumColors = ["#daf3c7", "#fcbf9d", "#c9effc", "#fcebc4", "#7f88c1", "#f8c0c7"];
// "#e80849", "#00a6f2", "#f4b231", "#86d248", "#7f88c1"
  // l<?= $kunik ?>.defaults.forced = {
  //   filters: {
  //       "$or" : {}
  //   }
  //   }
  // var dataSource<?= $kunik ?> = <?= json_encode($paramsData["dataSource"]) ?>;
  // if(Object.keys(dataSource<?= $kunik ?>).length > 0){
  //   l<?= $kunik ?>.defaults["notSourceKey"] = true;
  //   for (const [id, value] of Object.entries(dataSource<?= $kunik ?>)) {
  //     l<?= $kunik ?>.defaults.forced.filters["$or"]["links.memberOf."+id] = {"$exists" : true};
  //   }
  // }
    if(!window.initedGraph){
      window.initedGraph = [];
    }
    var p<?= $kunik ?> = {};
    var activeCircle<?= $kunik ?> = "";
    function initGraph<?= $kunik ?>() {
        if(Object.keys(p<?= $kunik ?>).length==0){
          p<?= $kunik ?> = searchObj;
          p<?= $kunik ?>.graph.beforeDraw = function(fObj){
            fObj.graph.graph._zoomInText = "<?= $paramsData["textZoomIn"] ?>";
            fObj.graph.graph._zoomOutText = "<?= $paramsData["textZoomOut"] ?>";
            fObj.graph.graph._zoomMagnifyGlassText = "<?= $paramsData["textMagnify"] ?>";
            fObj.graph.graph._zoomResetText = "<?= $paramsData["textZoomReset"] ?>";
            fObj.graph.graph._zoomFsText = "<?= $paramsData["textZoomFullScreen"] ?>";
          }
          p<?= $kunik ?> = p<?= $kunik ?>.init(l<?= $kunik ?>)
        }else{
          p<?= $kunik ?> = p<?= $kunik ?>.init(p<?= $kunik ?>.pInit);
        }
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);
        p<?= $kunik ?>.graph.graph._circlePadding = <?= $paramsData["circlePadding"] ?>;
        p<?= $kunik ?>.graph.graph._externalCircleMargin = <?= $paramsData["externalCircleMargin"] ?>;
  
        p<?= $kunik ?>.graph.graph._color = (da, index) => {
          /*p<?= $kunik ?>.graph.graph.rootG.selectAll(".divide circle:last-child")
          .filter(function (d,i) { return i === index;})
          .style("fill-opacity", 0.8)*/

          $(".nodes-container div").css({"color": "white"});
          return costumColors[index]
        };

        <?php // if($paramsData["noPreview"]=="true"){ ?>
          p<?= $kunik ?>.graph.graph.setOnClickNode((e,d,n) => {
            e.preventDefault();
            e.stopPropagation();
            if(costum.slug =="institutBleu"){
              smallMenu.openAjaxHTML(baseUrl+"/costum/institutbleu/elementhome/type/organizations/id/"+d.data.id);

            }else{
              smallMenu.openAjaxHTML(baseUrl+"/costum/meir/elementhome/type/organizations/id/"+d.data.id);
            }
            $(".portfolio-modal.modal .close-modal").html("<h4><i class='fa fa-arrow-left'></i>Retour</h4>");
          })
        <?php // } ?>

        p<?= $kunik ?>.graph.graph._onClickNodeMobile = function(event, data){
          alert("here")
          p<?= $kunik ?>.graph.graph._onClickNode(event, data);
        };

        p<?= $kunik ?>.graph.graph.setBeforeDrag(() => {
          p<?= $kunik ?>.graph.graph.setDraggable(costum.editMode);
        });
        p<?= $kunik ?>.graph.graph.setOnDragEnd(() => {
          const positions = {}
          p<?= $kunik ?>.graph.graph.rootG.selectAll("g.divide").each((d) => {
            positions[d.data[0]] = {
              x: d.x,
              y: d.y
            }
          })
          tplCtx = {};
          tplCtx.id = "<?= $blockKey ?>",
          tplCtx.collection = "cms";
          tplCtx.path = "allToRoot";
          tplCtx.value = {
            id: "<?= $blockKey ?>",
            positions: positions
          };
          dataHelper.path2Value( tplCtx, function(params) {
            dyFObj.commonAfterSave(params,function(){
              toastr.success("Position saved");
            });
          });
        });

        setTimeout(() => {
          p<?= $kunik ?>.graph.graph.initZoom();
          var zoomerData = [];
          if(typeof costum.lists !="undefined" && typeof costum.lists.family !="undefined" ){
            zoomerData = Object.keys(costum.lists.family);
          }
          
          d3.select("div#circle-filter-container<?= $kunik ?>")
            .selectAll("button")
            .data(zoomerData)
            .join((enter) => {
                enter.append("xhtml:button")
                  .text(d => d)
                  .classed("btn margin-5 btn-circle-zoomer", true)
                  .on("click", (e,d) => {
                    var thisElement = $(e.target);
                    if(activeCircle<?= $kunik ?> != d){
                      p<?= $kunik ?>.graph.graph.focus(d);
                      activeCircle<?= $kunik ?> = d;
                      $(".btn-circle-zoomer").removeClass("btn-primary");
                      thisElement.addClass("btn-primary");
                      //$("#"+GraphUtils.slugify(d)+" .list-group-item").show();
                    }else{
                      activeCircle<?= $kunik ?> = "";
                      thisElement.removeClass("btn-primary");
                      p<?= $kunik ?>.graph.graph.unfocus().then(() => {
                        mylog.log("UNFOCUSED");
                      });
                    }
                    
                    $("*[href='#"+GraphUtils.slugify(d)+"']").addClass("collapsed");
                    $("*[href='#"+GraphUtils.slugify(d)+"'] > i.fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                    $("#"+GraphUtils.slugify(d)).addClass("in");
                })
                .append("xhtml:span")
                .classed("badge badge-theme-count margin-left-5", true)
                .attr("data-countvalue", d => d)
                .attr("data-countkey", d => d)
                .attr("data-countlock", "false");
            });
            p<?= $kunik ?>.filters.actions.themes.setThemesCounts(p<?= $kunik ?>);
        }, 500);
    }

    setTimeout(() => {
      if($("#graph-container-<?= $kunik ?>").is(":visible")){
        window.initedGraph.push('<?= $kunik ?>');
        initGraph<?= $kunik ?>();
      }

      
    },200)
</script>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    const filtersData = {};
    if(costum && costum.lists){
      for (const key of Object.keys(costum.lists)) {
        filtersData[key] = key;
      }
    }

    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "dataSource" : {
            "label" : "Organisation source",
             "inputType": "finder",
             "initType": ["organizations"],
            "openSearch" :true,
            "values" :  sectionDyf.<?php echo $kunik ?>ParamsData.dataSource
          },
          "isSearch" : {
              "label" : "Ajouter une fonction recherche",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Recherche activé",
                    "offLabel" : "Recherche desactivé",
                    "labelText" : "Ajouter une fonction recherche"
                },
                "checked" : false
          },
          "tags" : {
              "label" : "Les tags à visualiser",
              "inputType": "tags"
          },
          "circlerelations" : {
              "label": "Les relations (utiliser les tags à visualiser)",
              "inputType" : "lists",
              "entries":{
                "source":{
                  "type":"text",
                  "label" : "source",
                  "class":"col-md-5"
                },
                "target":{
                  "label": "target",
                  "type": "text",
                  "class": "col-md-5"
                }
            }
          },
          "isClickAndScrollToZoom" : {
              "label" : "Activer zoom après clique",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Clique puis scroll pour zoomer",
                    "offLabel" : "scroll pour zoomer",
                    "labelText" : "Ajouter une fonction recherche"
                },
                "checked" : true
          },
          "noPreview" : {
              "label" : "Click sur élément du graph: Ne pas passer par prévisualisation",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : trad.yes,
                    "offText" : trad.no,
                    "onLabel" : "Ne pas passer par preview",
                    "offLabel" : "Passer par preview",
                    "labelText" : ""
                },
                "checked" : false
          },
          "height" : {
            "label" : "Hauteur en px ou % ou vh",
            "inputType": "text"
          },
          "relationColor" : {
            "label":"Couleur de ralation",
            "inputType" : "colorpicker",
          },
          "circleColor" : {
            "label":"Couleur du cercle",
            "inputType" : "colorpicker"
          },
          "textZoomIn" : {
            "label" : "Texte sur le bouton Zoom Avant",
            "inputType": "text"
          },
          "textZoomOut" : {
            "label" : "Texte sur le bouton Zoom Arrière",
            "inputType": "text"
          },
          "textZoomReset" : {
            "label" : "Texte sur le bouton Recadrer",
            "inputType": "text"
          },
          "textZoomFullScreen" : {
            "label" : "Texte sur le bouton Plein Ecran",
            "inputType": "text"
          },
          "circlePadding" : {
            "label" : "Padding entre du cercles",
            "inputType": "number"
          },
          "externalCircleMargin" : {
            "label" : "Margin entre les cercles",
            "inputType": "number"
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
            if (k == "dataSource") {
              tplCtx.value[k] = formData.dataSource;
            }
            if (k == "circlerelations"){
              tplCtx.value[k] = formData.circlerelations;
            }
          });
          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');

                  var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                  var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                  var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                  cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                  // urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    $("#graph-cover-<?= $kunik ?>").hide();
        // Toogle scroll zoom on click in graph container
    $("#graph-container-<?= $kunik ?>").on("dblclick", function(){
      if($("#graph-cover-<?= $kunik ?>").is(":visible")){
        $("#graph-cover-<?= $kunik ?>").hide();
      }else{
        $("#graph-cover-<?= $kunik ?>").show();
      }
    })

  });
</script>