<?php
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ];

    $spText = ["titleTop","titleBottom"];

    foreach ($spText as $key) {
        if( !(is_array($blockCms[$key]))){
            ${$key} = ["fr" => $blockCms[$key]];
        } else {
            ${$key} = $blockCms[$key];
        }
    };

?>
<style id="progressCircle<?= $kunik ?>">

</style>
<div class="chartviz <?= $kunik ?>">
    <div class="sp-text text-center titleBottom<?= $blockKey ?>" data-field="titleTop" data-id="<?= $blockKey ?>"></div>
    <?php if($blockCms["textRight"]!= false){ ?>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
    <?php } ?>
            <div id="arc<?= $kunik ?>" class="text-center"></div>
    <?php if($blockCms["textRight"]!= false){ ?>
            </div>
            <div class="tvr<?= $kunik ?>" class="col-md-7">
                <div style="font-size:15pt" data-field="textValueRight" data-id="<?= $blockKey ?>"><?php echo implode(',',$blockCms['answerValue']) ?></div>
            </div>
        </div>
    <?php } ?>
    <div class="sp-text text-center titleBottom<?= $blockKey ?>" data-field="titleBottom" data-id="<?= $blockKey ?>"></div>
</div>
<table id="tableData<?=$kunik?>" class="table table-striped table-bordered tableviz margin-top-10">
    <tr><td>Valeur :</td><td class="arcValue<?= $kunik; ?>"></td></tr>
</table>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var countValue = 0;
        var allCount = 1;
        var dataAnswers = <?php echo json_encode( $blockCms["dataAnswers"] ) ?>;
        if(dataAnswers && dataAnswers != "" && dataAnswers != "undefined")
        {
            countValue = dataAnswers.value;
            allCount = dataAnswers.allCount;
        }
        $("#tableData<?=$kunik?>").hide();;
        var progressCircle<?= $kunik ?> = {
            width: 120,
            height: 120,
            pi : 2 * Math.PI,
            data: {id:"<?php echo $kunik ?>", percent: Math.round((countValue*100)/allCount)||0},
            arc : null,
            parent:null,
            init: function(procir){
                procir.arc = d3.arc()
                    .innerRadius(40)
                    .outerRadius(60) // arc width
                    .startAngle(0);

                procir.parent = d3.select("#arc<?= $kunik ?>").append("svg")
                    .attr("width", procir.width)
                    .attr("height", procir.height);

                procir.draw(procir, procir.data);
            },
            draw: function(procir, data){
                var d3Ring = procir.parent  
                .append("g")
                .attr("transform", "translate("+ procir.width/2 +"," +60+")")
                .attr("id",data.id);
                // Background
                d3Ring
                .append("path")
                .datum({endAngle: procir.pi})
                .attr("class", "emptyColor")
                .attr("d", procir.arc);
                // Foreground
                var foreground = d3Ring
                .append("path")
                .datum({endAngle: 0})
                    .style("stroke", "none")
                    .style("stroke-width", "0px")
                    .style("opacity", 1)
                .attr("d", procir.arc)
                .attr("class", "completeColor");
                // Text
                d3Ring
                .append("text")
                .attr("x", -20)
                .attr("y", 8) 
                .attr("class", "percentColor")
                .style("font-size", 25)
                .style("font-family", "Georgia, Arial, sans-serif")
                .style("font-weight", "bolder")
                .text(data.percent + "%");
                $(".arcValue<?= $kunik; ?>").text(data.percent + "%");
                var angle = procir.helpers.convertPercentToAngle(procir);
                // Animation
                foreground
                .transition()
                  .duration(1500)
                        .delay(500)
                  .call(procir.arcTween, procir, angle);
            },
            arcTween: function(transition, procir, newAngle){
                transition.attrTween("d", function(d) {  
                    var interpolate = d3.interpolate(d.endAngle, newAngle);
                    return function(t) {
                        d.endAngle = interpolate(t);
                        return procir.arc(d);
                    };
                });
            },
            helpers:{
                convertPercentToAngle: function(procir){
                    return ( procir.data.percent / 100 ) * procir.pi
                }
            }
        }
        progressCircle<?= $kunik ?>.init(progressCircle<?= $kunik ?>);
    });
</script>
<script type="text/javascript">

    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    //console.log("str : ", str);
    $("#progressCircle<?= $kunik ?>").append(str);

    if(costum.editMode)
    {
        var coformOptions = [];
        var answerPathData = [];
        var answerValueData = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined") {
            coformOptions = $.map( costum["dashboardGlobalConfig"]["coformList"], function( key, val ) {
                return {value: val,label: key}
            })
        }
        cmsConstructor.sp_params["<?= $blockKey ?>"] = <?php echo json_encode( $blockCms ) ?>;
        if(cmsConstructor.sp_params["<?= $blockKey ?>"].coform != ""){
            answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockKey ?>"].coform);
        }
        if(cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath != ""){
            answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockKey ?>"].coform, cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath);
        }
        var progressCircle = {
            configTabs : {
                general: {
                    inputsConfig: [
                        {
                            type:"groupButtons",
                            options: {
                                name:"textRight",
                                label: tradCms.textrightofcircle,
                                options:[
                                    {
                                        value:true,
                                        label: trad.yes
                                    },
                                    {
                                        value:false,
                                        label: trad.no
                                    },
                                ],
                                defaultValue: cmsConstructor.sp_params["<?= $blockKey ?>"].textRight
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "coform",
                                label: tradCms.selectaform,
                                options: coformOptions
                            }
                        },
                        {
                            type: "select",
                            options: {
                                name: "answerPath",
                                isSelect2: true,
                                label: tradCms.whichQuestionCorrespondstotheGraph,
                                options: answerPathData
                            }
                        },
                        {
                            type: "selectMultiple",
                            options: {
                                name: "answerValue",
                                label: tradCms.answervalue,
                                options: answerValueData
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        {
                            type: "section",
                            options: {
                                name: "percentColor",
                                label: tradCms.colorofpercentagetext,
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "emptyColor",
                                label: tradCms.colorofemptyarc,
                                inputs:[
                                    "fill"
                                ]
                            }
                        },
                        {
                            type: "section",
                            options: {
                                name: "completeColor",
                                label: tradCms.filledarccolor,
                                inputs:[
                                    "fill"
                                ]
                            }
                        }
                    ]
                },
                advanced : {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                if(name == "coform"){
                    answerPathData = getAnswerPath(cmsConstructor.sp_params["<?= $blockKey ?>"].coform);
                    cmsConstructor.blocks.progressCircle<?= $blockKey ?>.configTabs.general.inputsConfig[2].options.options = answerPathData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                if(name == "answerPath"){
                    answerValueData = getAnswerValue(cmsConstructor.sp_params["<?= $blockKey ?>"].coform, cmsConstructor.sp_params["<?= $blockKey ?>"].answerPath);
                    cmsConstructor.blocks.progressCircle<?= $blockKey ?>.configTabs.general.inputsConfig[3].options.options = answerValueData;
                    $(".cmsbuilder-block[data-kunik=<?= $kunik ?>]").trigger("click");
                }
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.progressCircle<?= $blockKey ?> = progressCircle;
    }


    //sp-params and text append 

	appendTextLangBased(".titleTop<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleTop) ?>,"<?= $blockKey ?>");
	appendTextLangBased(".titleBottom<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($titleBottom) ?>,"<?= $blockKey ?>");

    
    function getAnswerPath(value) {
        var childForm = (typeof costum["dashboardGlobalConfig"]!="undefined")?costum["dashboardGlobalConfig"]["coformInputs"]:{};
        var children = [];
        for(const stepKey in childForm[value] ){
            for(const inputKey in childForm[value][stepKey]){
                var input = childForm[value][stepKey][inputKey];
                
                if(input["type"].includes(".multiCheckboxPlus")){
                    children.push({value: stepKey+'.multiCheckboxPlus'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".multiRadio")){
                    children.push({value: stepKey+'.multiRadio'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radiocplx")){
                    children.push({value: stepKey+'.radiocplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxcplx")){
                    children.push({value: stepKey+'.checkboxcplx'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".checkboxNew")){
                    children.push({value: stepKey+'.checkboxNew'+inputKey, label: input["label"]});
                }
                if(input["type"].includes(".radioNew")){
                    children.push({value: stepKey+'.radioNew'+inputKey, label: input["label"]});
                }
                if(input["type"]=="text"){
                    children.push({value: stepKey+'.'+inputKey, label: input["label"]});
                }
            }
        }
        return children;
    }
    function getAnswerValue(coformId, value) {
        var coform = [];
        if(typeof costum["dashboardGlobalConfig"] !="undefined" && costum["dashboardGlobalConfig"]["formTL"]){
            coform = costum["dashboardGlobalConfig"]["formTL"];
        }
        if(typeof coform[coformId] != "undefined" ){
            coform = coform[coformId];
        }
        var input = value.split(".")[1];
        var answerValue = [];
        if(input.includes("multiRadio") || input.includes("multiCheckboxPlus") || input.includes("radiocplx") || input.includes("checkboxcplx")){
            if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["global"]){
                for(const paramValue of coform["params"][input]["global"]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        //console.log("coform : ", coform);
        if(input.includes("checkboxNew") || input.includes("radioNew")){
            if(typeof coform["params"] != "undefined" && typeof coform["params"][input] != "undefined" && coform["params"][input]["list"]){
                for(const paramValue of coform["params"][input]["list"]){
                    answerValue.push({value: paramValue, label: paramValue})
                }
            }
        }
        return answerValue;
    }
</script>