  <?php
  $keyTpl ="actualiteCarousel";
  $paramsData = [ 
    "title" => "Actualités",
    "background"=>"#e84d4c"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  } 
  $where = array("source.key"=> $costum["contextSlug"]);


  if (!empty($filters)) {
    $where["thematique"] = $filters;
  }
  $actualite = PHDB::findAndLimitAndIndex(Poi::COLLECTION,$where,3);
//var_dump($actualite);exit();
  ?>


<style type="text/css">
  .content_<?= $kunik?>{
    /*background :linear-gradient(180deg, <?= $paramsData["background"]?> 0%, #fff 100%);*/
  
  }
  .logoActualite{
    width :250px;
    height : 50px;
  }
  #services_<?= $kunik?>{
    margin-top: 0;
  }
  #services_<?= $kunik?> h4{
    font-size: 25px;
    margin-top: 8%;
    margin-bottom: 5%;
    color: black;

  }
  #services_<?= $kunik?> h5{
    font-size: 16px;
    line-height: 22px;0;
  }
  #services_<?= $kunik?> .btn-<?= $kunik?>{
    display: none;
    z-index: 9999;
  }
  #services_<?= $kunik?>:hover .btn-<?= $kunik?>{
    display: block;
    position: absolute;
    top:40%;
    left: 50%;
    transform: translate(-50%,-50%);
  }

.carousel-control { 
  width:  4%; 
  position: absolute !important;
}
.carousel-control.right { 
  right: -40px;
}
.carousel-control.left,.carousel-control.right {
  margin-left:-40px;
  background-image:none;
}
@media (max-width: 767px) {
  .carousel-inner .active.left { left: -100%; }
  .carousel-inner .next        { left:  100%; }
  .carousel-inner .prev    { left: -100%; }
  .active > div { display:none; }
  .active > div:first-child { display:block; }

}
@media (min-width: 767px) and (max-width: 992px ) {
  .carousel-inner .active.left { left: -50%; }
  .carousel-inner .next        { left:  50%; }
  .carousel-inner .prev    { left: -50%; }
  .active > div { display:none; }
  .active > div:first-child { display:block; }
  .active > div:first-child + div { display:block; }
}
@media (min-width: 992px ) {
  .carousel-inner .active.left { left: -16.7%; }
  .carousel-inner .next        { left:  16.7%; }
  .carousel-inner .prev    { left: -16.7%; }
  .items {width: 40%;}  
}
#containActuality_<?= $kunik?> .item .items{
  width: 20%;
} 
  #services_<?= $kunik?> .actu{
   padding: 10px;
   background-color: <?php echo (isset($costum["css"]["color"]["bg-color"])) ? $costum["css"]["color"]["bg-color"] : $paramsData["background"]; ?>;
   color: white;
   font-size: 25px;
   font-weight: bold; 
 }


#services_<?= $kunik?> a {
  color: currentColor;
  text-decoration: none;
}

#services_<?= $kunik?> p {
  font-size: 13px;
}

#services_<?= $kunik?> a:hover .card-outmore_<?= $kunik?> {
  background: #2C3E50;
  color: #fff;
}

#services_<?= $kunik?> a:hover .thecard_<?= $kunik?> {
  box-shadow: 0 10px 50px rgba(0,0,0,.6);
}
  .thecard_<?= $kunik?> {
  /*width: 300px;*/
  margin: 5% auto;
  box-shadow: 0 1px 3px 0px rgba(0,0,0,.4);
  display: block;
  background-color: #fff;
  border-radius: 4px;
  transition: 400ms ease;
}
.card-img_<?= $kunik?> {
  height: 225px;
}
.card-img_<?= $kunik?> img {
  width:100%;
  height: 240px;
  border-radius: 4px 4px 0px 0px;
}
.card-caption_<?= $kunik?> {
  position: relative;
  background: #ffffff;
  padding: 15px 25px 5px 25px;
  border-radius: 0px 0px 4px 4px;
}
.card-outmore_<?= $kunik?> {
  padding: 10px 25px 10px 25px;
  border-radius: 0px 0px 4px 4px;
  border-top: 1px solid #e0e0e0;
  background: #efefef;
  color: #222;
  display: inline-table;
  width: 100%;
  box-sizing: border-box;
  transition: 400ms ease;
}
.card-outmore_<?= $kunik?> h5 {
  float: left;
}
.card-outmore_<?= $kunik?> i {
  float: right;
}

#calendar-btn {
  font-size: 18px;
  background: #f3b23e;
  color: #fff;
  padding: 13px 15px;
  border-radius: 50em;
  position: absolute;
  right: 20px;
  top: -22px;
  box-shadow: 0 2px 1px rgba(0,0,0,.2);
  transition: 400ms ease;
}
/*
.block-container-<?= $kunik?> {
  background-color: #e7e5eb;
  margin-bottom: 70px !important
}*/

.content_<?= $kunik?> {
  background-color: #e7e5eb;
  margin-bottom: 70px !important
}
</style>

<div id="services_<?= $kunik?>" class="services_<?= $kunik?> content_<?= $kunik?>">
  <div class="btn-<?= $kunik?> text-center">
 
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
      <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('poi')">Ajouter une article</button>
    <?php } ?>
  </div>
  <div>  
    <div class="actu" style="" >
      <font class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $paramsData["title"]?></font>
    </div>
    <div class="col-md-10 col-md-offset-1">
      <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="false" id="myCarousel">
        <div id="containActuality" class="carousel-inner">

        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
      </div>
    </div>
  </div>
</div>


 <script type="text/javascript">

      sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
      jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            
            "properties" : {
              "background" : {
                "inputType" : "colorpicker",
                "label" : "<?php echo Yii::t('cms', 'Background color of the title')?>",
                "markdown" : true,
                values :  sectionDyf.<?php echo $kunik ?>ParamsData.background
              }
            },
            beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
            save : function () {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent") {
                  tplCtx.value[k] = formData.parent;
                }
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
               else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouté");
                          $("#ajax-modal").modal('hide');
                          var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                          var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                          var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                          cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                          // urlCtrl.loadByHash(location.hash);
                        });
                      } );
          }
            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        params = {
          "source" : contextSlug,
          "type"  : "article"
        };
        ajaxPost( 
          null,
           baseUrl+"/costum/costumgenerique/getpoi",
          params,
          function(data){
            mylog.log("success", data);
            var str = `           
          <div class="item active"></div>
          `;
            if(data.result == true)
            {
              var i = 0;
              var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
              $(data.element).each(function(key,value){
                if (i <= 2) {
                  i++;
                  var img = (typeof value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;
                  var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "Aucune description";
                 
                  str +=
                    `
                    <div class="" >
                      <a href="#page.type.poi.id.`+value.id+`" class="lbh-preview-element">
                        <div class="items col-sm-6 col-xs-12 no-padding" >
                          <div class="thecard_<?= $kunik?>">
                            <div class="card-img_<?= $kunik?>">
                              <img src="`+img+`">
                            </div>
                            <div class="card-caption_<?= $kunik?>">
                              <i id="calendar-btn" class="fa fa-calendar"></i>
                              <p><b>`+value.name+`</b></p>
                              <p>`+description+`</p>
                            </div>
                            <div class="card-outmore_<?= $kunik?>">
                              
                            </div>
                          </div>
                        </div>
                      </a>
                      </div>
                      `;
                }else{
                }

              });
            }else
            {
              str = "<center style='padding: 80px;'>Il n'éxiste aucune actualité</center>";
            }
            $("#containActuality").html(str);
          }

        )

$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<3;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
      });
 
    </script>