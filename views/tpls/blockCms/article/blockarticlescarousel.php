<?php
$keyTpl = "blockarticlescarousel";

$paramsData = [ 
    "title"         => "block articles en carousel",
    "icon"          => "",
    "nombre"       => "3",
    "color"         => "#000000"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style type="text/css">
    .article_<?= $kunik?> .ArrowLeft{
        width: 3vw;
        margin-top: 7vw;
        margin-left: -11vw;
    }

    .article_<?= $kunik?> #carousel #indactors-blockArticlesCarousel li {
        background-color: grey;
        color: grey;
    }

    .article_<?= $kunik?> .ArrowRight{
        transform: rotate(180deg);
        width: 3vw;
        margin-top: 7vw;
        margin-left: 6vw;
    }
    .article_<?= $kunik?> .desc{
        text-align: initial;
        
    }

    .article_<?= $kunik?> .element{
        background: gray;
        color: white;
        border-radius: 6px;
        padding: 9px;
        margin-top: 2vw;
    }

    .article_<?= $kunik?> #right{
        opacity: 1 !important;
        margin-left: 83vw;
    }

    @media (max-width:978px){
        .article_<?= $kunik?> img{
            min-height: 150px !important;
        }
        .article_<?= $kunik?> h1{
            font-size: 20px;
        }

        #right{
            opacity: 1 !important;
            margin-left: 81vw !important;
        }
        .article_<?= $kunik?> desc p {
            font-size: 16px
        }
    }
    .article_<?= $kunik?> h1{
        color:<?= $paramsData["color"] ?>;
    }
    .article_<?= $kunik?> img{
        min-height: 438px;
    }
    .article_<?= $kunik?> #carousel ol{
        top: 1%;
        bottom: 2%;
    }
    
    .article_<?= $kunik?> #content-results-articles{
        margin-top: 3%;
    }

    </style>
    <div class="article_<?= $kunik?>">
        <h1 class="sp-text img-text-bloc text-center title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
            <i class="fa <?= $paramsData['icon'] ?>"></i> 
            <?= $paramsData["title"] ?> 
        </h1>
         <div class="text-center btn-edit-delete">
            <?php 
                
                if(Authorisation::isInterfaceAdmin()){ ?>
                    <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('poi')">
                        Ajouter un article
                    </button>            
            <?php } ?>
        </div>
        <div id="carousel" class="carousel slide col-xs-12" data-ride="carousel">
            <ol class="carousel-indicators" id="indactors-blockArticlesCarousel"> 

            </ol>

            <div id="content-results-articles" class="carousel-inner text-center">

            </div>
        </div>

      
<script type="text/javascript">
    tplCtx = {};
    sectionDyf = (typeof sectionDyf == "undefined") ? {} : sectionDyf;
    page = <?= json_encode(@$page); ?>;
    type = 'tpls.blockCms.article.<?= $keyTpl ?>';
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    $(document).ready(function(){
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",

                "properties" : {
                   "nombre" : {
                        label : "<?php echo Yii::t('cms', 'Number of items to display')?>",
                        inputType : "number",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.nombre
                    },
                    icon : { 
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : <?= json_encode(Cms::$icones); ?>,
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.icon
                    },
                    color : {
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        inputType : "colorpicker",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.color
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                            // urlCtrl.loadByHash(location.hash);
                        });
                      } );
              }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
        var params = {
            "source" : contextSlug,
            "limit" : "<?= $paramsData["nombre"]?>"
        };

        ajaxPost(
            null,
            baseUrl+"/costum/costumgenerique/getarticlescarousel",
             params,
           function(data){
                mylog.log("success", data);

                var y= 0;

                var str = "<div class='item active'>";
                var ctr = "<li class='active' data-target='#carousel' data-slide-to='"+y+"' >";

                if(data.result == true)
                {
                    var i = 0;
                    var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                    $(data.elt).each(function(key,value){

                        var img = (typeof value.profilMediumImageUrl  != "undefined" && value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;

                            // var tags = (typeof value.tags  != "undefined" && value.tags  != null) ? value.tags : "Aucun tag(s) pour cette article";

                            var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "<?php echo Yii::t('cms', 'No description')?>";

                            if(i >= 1){
                                str += "</div>";
                                y++;
                                ctr += "</li>";
                                i = 0;
                                str += "<div class='item'>";
                                ctr += "<li class='' data-target='#carousel' data-slide-to='"+y+"'>";
                            }

                            i++;

                            str += "<div class='row col-md-12'>";
                            str += "<div class='col-md-6 col-xs-12'>";
                            str += "<img class='img-art img-responsive pull-right' src='"+img+"'>";
                            str += "</div>";
                            str += "<div class='desc col-md-6 col-xs-12'>";
                            str += "<h1>"+value.name+"</h1><br>";
                            str += "<p style='font-size:19px;'>"+description+"</p><br>";
                            str += '<a href="#page.type.poi.id.'+value.id+'" class="element btn btn-lg lbh-preview-element col-xs-3 no-padding text-center hidden-xs"><?php echo Yii::t("cms", "See the article")?></a>';
                            str += '<a style="margin-left: 33vw;margin-bottom: 3vw;" href="#page.type.poi.id.'+value.id+'" class="element lbh-preview-element col-xs-3 no-padding text-center visible-xs"><?php echo Yii::t("cms", "See the article")?></a>';
                            if(notEmpty(value.tags)){
                                value.tags.forEach(v =>
                                    str += "<span style='margin-top:1%;margin-left:2%;' class='pull-left text-danger'>#"+v+"</span> " );
                            }
                            str += "</div>"; 
                            str += "</div>";
                            str += "</div>";      
                        });
                }
                else
                {
                    str += "<?php echo Yii::t('cms', 'There is no article')?>";
                    ctr += "";
                }
                $("#content-results-articles").html(str);
                $("#indactors-blockArticlesCarousel").html(ctr);
            }
        );
    });
</script>

