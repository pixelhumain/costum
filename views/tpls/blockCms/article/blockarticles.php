<?php
$keyTpl = "blockarticles";

$paramsData = [ 
    "titre"         =>  "block articles",
    "nombre"       => "3",
    "icon"          =>  "",   
    "colorTitle"         =>  "#000000",
    "colorNameArticle" => "#18a47c"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

?>
<style type="text/css">
    .articles_<?= $kunik?> #contanArticle a .articles-plus{
        height: 30px;
    }
    .articles_<?= $kunik?> h1{
        color:<?= $paramsData["colorTitle"] ?>;
    }
    #entity_<?= $kunik?> {
        margin-bottom: 15px;
        padding-top: 5px;
        padding-bottom: 5px;
        max-height: 300px;
        min-height: 150px;
    }
    #entity_<?= $kunik?> .container-img img{
        height: 150px;
        width: 100%;
    }
    #entity_<?= $kunik?> .container-info a{
       margin-top: 4%;
    }
    #entity_<?= $kunik?> .container-info a img{
       height: 40px;
    }
    #entity_<?= $kunik?> .container-info h3{
       font-size: 25px;
       text-transform: initial;
       color:<?= $paramsData["colorNameArticle"] ?>;
    }
    .btn-edit-delete{
        display: none;
    }
    .articles_<?= $kunik?>:hover .btn-edit-delete{
        display: block;
        position: absolute;
        top:40%;
        left: 50%;
        transform: translate(-50%,-50%);
      }
    @media (max-width: 978px) {
         #entity_<?= $kunik?> .container-img img{
            height: 80px;
            width: 100%;
        }
         #entity_<?= $kunik?> {
            max-height: 150px;
            min-height: 80px;
        }
         #entity_<?= $kunik?> .container-info h3{
           font-size: 18px;
        }
        #entity_<?= $kunik?> .container-info a{
           margin-top: 2%;
        }
        #entity_<?= $kunik?> .container-info a img{
           height: 26px;
        }
    }
</style>
<div class="articles_<?= $kunik?>">
    <h1  class="sp-text img-text-bloc text-center title" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titre"> 
        <i class="fa <?= $paramsData['icon'] ?>"></i> 
        <?= $paramsData["titre"] ?> 
    </h1>

    <div id="containArticle" class="col-sm-10 col-sm-offset-1 col-xs-12">
        
    </div><br>
    <div class="text-center btn-edit-delete">
        <?php
            if(Authorisation::isInterfaceAdmin()){ ?>
                <button class="btn btn-primary btn-xs" onclick="dyFObj.openForm('poi')">
                    Ajouter une article
                </button>            
        <?php } ?>
    </div>

</div>

<script type="text/javascript">
    sectionDyf = {};
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    $(document).ready(function(){

        var params = {
            "source" : contextSlug
        };

        ajaxPost(
            null,
            baseUrl+"/costum/costumgenerique/getarticles",
            params, 
            function(data){
                mylog.log("success articles : ", data);
                var str = "";
                if(data.result == true){
                    var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";

                    $(data.element).each(function(key,value){
                        var img = (typeof value.profilMediumImageUrl  != "undefined" && value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;

                        var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "Aucune description";
                        
                        str += '<div id="entity_<?= $kunik?>" class="searchEntityContainer searchEntity shadow2 col-xs-12" >';
                        str += '<div class="container-img col-xs-3 col-md-2">';
                        str += '<img src="'+img+'" class="img-responsive"/>';
                        str += '</div>';
                        str += '<div class="container-info col-xs-9 col-xs-10">';
                        str += '<h3 class="text-left col-xs-8">'+value.name+'</h3>';
                        str += '<a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element col-xs-12 no-padding">';
                        str += '<img class="img-responsive article-plus pull-left"  src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/plus-01.svg">';
                        str += '</a>';
                        str += '</div>';
                        str += '</div>';              
                    });
                }
                else
                    str += "<center>Il n'éxiste aucun articles</center>";

                $("#containArticle").html(str);
            }
            );

        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    icon : { 
                        label : "<?php echo Yii::t('cms', 'Icon')?>",
                        inputType : "select",
                        options : {
                            "fa-newspaper-o"    : "Newspapper",
                            "fa-calendar " : "Calendar",
                            "fa-lightbulb-o "  :"Lightbulb"
                        },
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.icon !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.icon:""
                    },
                    colorTitle : {
                        label : "<?php echo Yii::t('cms', 'Title color')?>",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.colorTitle !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.colorTitle:""
                    },
                    colorNameArticle : {
                        label : "<?php echo Yii::t('cms', 'Color of the name of the article')?>",
                        inputType : "colorpicker",
                        values :  (typeof sectionDyf.<?php echo $kunik ?>ParamsData !="undefined" && typeof sectionDyf.<?php echo $kunik ?>ParamsData.colorNameArticle !="undefined")?sectionDyf.<?php echo $kunik ?>ParamsData.colorNameArticle:""
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {  
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                            toastr.success("Élément bien ajouté");
                            $("#ajax-modal").modal('hide');
                            var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                            var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                            var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                            cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                        //   urlCtrl.loadByHash(location.hash);
                        });
                      } );
                    }
                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() { 
            tplCtx = {}; 
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).attr("data-collection");
            tplCtx.path = "allToRoot";
            mylog.log("tplCtx click",tplCtx.path);

            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });
    });
</script>