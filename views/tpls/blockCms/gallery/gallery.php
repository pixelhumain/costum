<!-- Created by Jean  2020-->
<!-- You can use communnecter gallery as BlockCms -->
<?php 
  $keyTpl ="gallery";
  $paramsData = [ 
    "nbImg" => "3",
    "galleryFilters" => [],
    "picBorder" => "#ffffff"
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  } 

  $defaultTexts = [
    "title" => "LA GALERIE",
    "subTitle" => "Ce que je propose"
  ];
  
  foreach ($defaultTexts as $key => $defaultValue) {
    ${$key} = ($blockCms[$key] ?? ["fr" => $defaultValue]);
    ${$key} = is_array(${$key}) ? ${$key} : ["fr" => ${$key}];
    
    $paramsData[$key] = ${$key}[$costum['langCostumActive']] ?? reset(${$key});
  };

 ?>

<?php
     $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
     $folderList = Folder::getSubfoldersByContext($costum["contextId"],$costum["contextType"],"image");
     $options = [];
     foreach ($folderList as $key => $value) {
        $options[(string)$value["_id"]] = $value["name"];
     }
     if(isset($paramsData["galleryFilters"][0]) && $paramsData["galleryFilters"][0] == "")
        $paramsData["galleryFilters"] = [];

      $cssAnsScriptFilesModule = array(
        '/plugins/mixitup/src/jquery.mixitup.js',
      );
      HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
      $cssAnsScriptFilesModule = array(
        '/js/pages-gallery.js',
        '/css/gallery.css'
      );
      HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl . "/assets");
     $cssAnsScriptFilesModule = array(
        '/js/gallery/index.js',
        '/js/gallery/folder.js',
      );
      //HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>
<style>
  .container<?php echo $kunik ?> .btn-edit-delete{
    display: none;
  }
  .container<?php echo $kunik ?>:hover .btn-edit-delete{
    display: block;
    position: absolute;
    top:0%;
    left: 50%;
    transform: translate(-50%,0%);
  }
  .title<?php echo $kunik ?>{
    text-transform: none !important;
  }
  .subTitle<?php echo $kunik ?>{
    text-transform: none !important;
    margin-bottom: 50px;
  }
  .container<?php echo $kunik ?> .menu-gallery,  .container<?php echo $kunik ?>  .menu-galleryy {
    list-style: none
  }
  .container<?php echo $kunik ?> .menu-gallery li,  .container<?php echo $kunik ?> .menu-galleryy li{
    display: inline;
    margin-right: 15px;
    cursor: pointer;
    text-decoration: none
    /*font-size:45px;*/
  }
  .container<?php echo $kunik ?>  .menu-gallery li a:focus{
    text-decoration: none;
    color: <?php echo @$paramsData["blockCmsColorTitle5"] ?> !important;
    border-bottom:2px solid <?php echo @$paramsData["blockCmsColorTitle5"] ?> !important;
    font-weight: bold;
   }
   .container<?php echo $kunik ?>  .menu-gallery .dropdown li a:hover{
      border:none !important;
   }
  .container<?php echo $kunik ?>  .menu-gallery li a:hover{
    text-decoration: none;
    color: <?php echo @$paramsData["blockCmsColorTitle5"] ?> !important;
    border-bottom:2px solid <?php echo @$paramsData["blockCmsColorTitle5"] ?> !important;
  }
  .container<?php echo $kunik ?>  .menu-gallery li a.active{
    text-decoration: none;
    color: <?php echo @$paramsData["blockCmsColorTitle5"] ?> !important;
    border-bottom:2px solid <?php echo @$paramsData["blockCmsColorTitle5"] ?>!important;

  }
  .container<?php echo $kunik ?> .menu-galleryy li:hover{
    border-bottom:2px solid <?php echo @$paramsData["blockCmsColorTitle4"] ?>;
  }

  .container<?php echo $kunik ?> .swiper-container {
    width: 100%;
    height: 368px;
    margin-left: auto;
    margin-right: auto;
  }

  .container<?php echo $kunik ?> .swiper-slide {
    background-size: cover;
    background-position: center;
    /*transform: scale(1.1);*/
    border:2px solid <?php echo $paramsData["picBorder"] ?>;
  }
  .container<?php echo $kunik ?> .swiper-slide:hover .fa-eye{
    display: inline-block;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-eye{
    display: none;
    padding: 0 5px 0 5px;
    background: rgb(0,0,0,0.6);
    color: white;
    border-radius: 0 0 16px 0;
    font-size: 24px;
  }
    .container<?php echo $kunik ?> .swiper-slide:hover .fa-trash,
    .container<?php echo $kunik ?> .swiper-slide:hover .fa-pencil{
    display: inline-block;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-trash{
    display: none;
    padding: 1px 5px 4px 6px;
    background: rgb(0,0,0,0.6);
    color: white;
    /*border-radius: 0 0 0 16px;*/
    font-size: 23px;
    float: right;
  }
  .container<?php echo $kunik ?> .swiper-slide .fa-pencil{
    display: none;
    padding: 1px 5px 4px 6px;
    background: rgb(0,0,0,0.6);
    color: white;
    border-radius: 0 0 0 16px;
    font-size: 23px;
    float: right;
  }
  .container<?php echo $kunik ?> .img-title{
    display: none;
    background: rgb(0,0,0,0.6);
    color: #ffffff;
    padding: 5px 5px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    word-break: break-all;
    width: 90%;
    font-weight: bold;
    text-transform: uppercase;
    text-align: center;
  }
  .container<?php echo $kunik ?> .swiper-slide:hover .img-title{
    display: block;
  }
  .container<?php echo $kunik ?> .dropdown.open .dropdown-menu {
    display: inline-grid;
  }
 </style>
<div class="container<?php echo $kunik ?> col-md-12">
	<h2 class=" title title<?php echo $kunik ?> Oswald sp-text img-text-bloc title<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"></h2>
  <h3 class=" subtitle subTitle<?php echo $kunik ?> ReenieBeanie sp-text img-text-bloc subTitle<?= $blockKey ?>" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subTitle"></h3>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
    <ul class="menu-galleryy Oswald">
      <li onclick="folder.crudFolder('new')" class="title-4"><i class="fa fa-folder"></i> créer un album</li>
      <li class="addPhoto<?=$kunik ?> title-4" ><i class="fa fa-plus"></i> Ajouter une(des) photo(s)</li>
    </ul>
  <?php } ?> 
  <ul class="menu-gallery mg-2 Oswald">
      <?php if(empty($paramsData["galleryFilters"])){
          foreach ($options as $key => $value) { ?>
          <li>
            <a href="javascript:;" class="album-glaz title-5" data-folderid="<?php echo @$key ?>">
              <?php echo $value ?>
            </a>
            <?php if(Authorisation::isInterfaceAdmin()){ ?>
              <div class="dropdown hiddenPreview" style="display: inline">
                  <button class="btn btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                    <li><a href="javascript:;" onclick="folder.crudFolder('update','<?= $key ?>')" ><?php echo Yii::t("common","Rename") ?></a></li>
                    <li><a href="javascript:;" onclick="folder.showPanel('move','folders','<?= $key ?>')"><?php echo Yii::t("common","Move") ?></a></li>
                    <li><a href="javascript:;" onclick="folder.crudFolder('delete','<?= $key ?>')"><?php echo Yii::t("common", "Delete")?></a></li>
                  </ul>
              </div>
            <?php } ?> 
        </li>
      <?php } 
      }?>

      <?php if(!empty($paramsData["galleryFilters"])){
          foreach ($options as $key => $value) { 
            if(in_array($key, $paramsData["galleryFilters"])){ ?>
          <li>
            <a href="javascript:;" class="album-glaz title-5" data-folderid="<?php echo @$key ?>">
              <?php echo $value ?> los
            </a>
        </li>
      <?php } 
      }
    }?>

  </ul>
  <div class="swiper-container">
    <div class="swiper-wrapper"></div>
     <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>

<script type="text/javascript">

if (costum.editMode){
    cmsConstructor.sp_params["<?= $blockKey ?>"] =  <?= json_encode($blockCms) ?>
} 

appendTextLangBased(".title<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($title) ?>,"<?= $blockKey ?>");
appendTextLangBased(".subTitle<?= $blockKey ?>",<?= json_encode($costum["langCostumActive"]) ?>,<?= json_encode($subTitle) ?>,"<?= $blockKey ?>");


sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  var navInFolders = {
    "file": {
      "name": trad.documents
    },
    "album": {
      "name": trad.Album
    }
  };
  var docsList = {};
  var historyNav = [];
  var itemId = costum.contextId;
  var itemType = costum.contextType;
  var docType = "image";
  var folderId ="";
  var contentKey = true;
jQuery(document).ready(function() {
$('.addPhoto<?=$kunik ?>').click(function(){
    var addPhotos = {
        "beforeBuild":{
            "properties":{
                "info":{
                    "html":"<br/>Ajouter des images dans votre gallery<hr>"
                }
            }
         },
        "onload" : {
            "actions" : {
                "setTitle" : "Ajouter des photos",
                "hide" : {
                    /*"folderfolder" : 1,*/
                    "newsCreationcheckboxSimple" : 1
                }
            }
        }
    };

  dyFObj.init.folderUploadEvent = function( type, id, docT, contentK, foldk){
      if(foldk=="")
        name=(docT=="image") ? trad.noalbumselected : trad.nofolderselected;
      else
        name=navInFolders[foldk].name;
      $(".nameFolder-form").text(name);
        endPoint=uploadObj.get(type, id, docT, contentK, foldk);
        $("#imageUploader").fineUploader('setEndpoint',endPoint);
  }
  dyFObj.openForm('addPhoto',null,null,null,addPhotos)

})


  //init gallery onLoad
  initItemCaroussel<?php echo $kunik ?>('.container<?php echo $kunik ?> .swiper-wrapper',$('.container<?php echo $kunik ?> .menu-gallery .album-glaz').data('folderid'));
  sectionDyf.<?php echo $kunik ?>Params = {
    "jsonSchema" : {    
      "title" : "Configurer votre section",
      "description" : "Personnaliser votre gallerie",
      "icon" : "fa-cog",
      
      "properties" : {
          "nbImg" : {
              "inputType" : "text",
              "label" : "Nombre de l'image sur le caroussel",
              "rules" :{
                number :true
              },
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.nbImg
          },
          "picBorder" : {
              "inputType" : "colorpicker",
              "label" : "Bordure de l'image",
              
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.picBorder
          },
          "galleryFilters" : {
              "inputType" : "selectMultiple",
              "label" : "Filtres de la gallery",
              "options" : <?php echo json_encode($options) ?>,
              values : sectionDyf.<?php echo $kunik ?>ParamsData.galleryFilters
          },
          /*"police" : {
            inputType :"select",
            "label" : "Police",
            "options" : fontObj,
            "isPolice" : true,
            noOrder:true
          }*/          
      },
      beforeBuild : function(){
          uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
      },
      save : function (data) {  
        tplCtx.value = {};
        $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
          tplCtx.value[k] = $("#"+k).val();
          if (k == "parent")
            tplCtx.value[k] = formData.parent;
        });
        console.log("save tplCtx",tplCtx);

        if(typeof tplCtx.value == "undefined")
          toastr.error('value cannot be empty!');
        	else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouté");
                $("#ajax-modal").modal('hide');

                var id = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("id");
                var path = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("path");
                var kunik = $("div[data-kunik=<?= $kunik ?>]").parents(".custom-block-cms").data("kunik");
                cmsBuilder.block.loadIntoPage(id, page, path, kunik);
                // urlCtrl.loadByHash(location.hash);
              });
            } );
          }

      }
    }
  };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    
    $('.container<?php echo $kunik ?> .menu-gallery .album-glaz').click(function(){
        var url = baseUrl+"/"+moduleId+"/gallery/index/type/"+itemType+"/id/"+itemId+"/docType/image/tpl/json";
        var folderId = $(this).data('folderid');
        var html = "";
        var params = {
          folderId : folderId,
          contentKey : "slider"
        }
        ajaxPost(
          null,
          url,
          params, function(data){
              mylog.log('gogogogo',data.docs); 
              $.each(data.docs,function(k,v){
                html += `<div class="swiper-slide swiper-slide${v.id}" style="background-image:url(${v.imagePath})">
                    <a href="${v.imagePath}" data-lightbox="all"><i class="fa fa-eye fa-2x"></i></a>
                    ${(v.title != null) ? '<span class="img-title">'+v.title+'</span>' : ''}
                    <?php if(Authorisation::isInterfaceAdmin()){ ?>
                    <a href="javascript:;" class="delete-pic" data-id="${v.id}"><i class="fa fa-trash fa-2x"></i></a>
                    <a href="javascript:;" class="edit-picture" data-id="${v.id}"><i class="fa fa-pencil fa-2x"></i></a>
                    <?php } ?> 
                </div>
                `;
                });
              //<a href="javascript:deleteImg("600b2a8e80721b13008b4569");"><i class="fa fa-trash fa-2x"></i></a>
                $('.container<?php echo $kunik ?> .swiper-wrapper').html(html);
              initCaroussel<?php $kunik ?>(".container<?php echo $kunik ?> .swiper-container");
              deletePic();
              editPic();
           }
        );
    });
    $('.container<?php echo $kunik ?> .menu-gallery .album-glaz').click(function(){
        initItemCaroussel<?php echo $kunik ?>('.container<?php echo $kunik ?> .swiper-wrapper',$(this).data('folderid'))
    });

    $('.container<?php echo $kunik ?> ul.container<?php echo $kunik ?> .menu-gallery li a').click(function(){
      $('.container<?php echo $kunik ?> .menu-gallery li a').removeClass("active");
      $(this).addClass("active");
    });


    function initCaroussel<?php $kunik ?>(carouselClass){
        var swiper = new Swiper(carouselClass, {
          slidesPerView: 1,
          spaceBetween: 0,
          // init: false,
          pagination: {
            el: '.swiper-pagination',
            clickable: true,
          },
          navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
          keyboard: {
            enabled: true,
          },
          breakpoints: {
            640: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
            1024: {
              slidesPerView: <?php echo $paramsData["nbImg"] ?>,
              spaceBetween: 50,
            },
          }
        });
      }

      function initItemCaroussel<?php echo $kunik ?>(carouselClasse,folderId = null){
              var url = baseUrl+"/"+moduleId+"/gallery/index/type/"+itemType+"/id/"+itemId+"/docType/image/tpl/json";
              var html = "<h1 style='text-align:center;margin:100px 0px'><i class='fa fa-spinner fa-spin'></i></h1>";
              $(carouselClasse).html(html);

              $.ajax({
                 url : url,
                 type : 'post',
                 data : {
                    folderId : folderId,
                    contentKey : "slider"
                 },
                 dataType : 'json',
                 success : function(data){
                    mylog.log('galery glaz',data.docs); 
                    html = "";
                    mylog.log("lisitra",data.docs);
                    $.each(data.docs,function(k,v){
                      html += 
                      `<div class="swiper-slide swiper-slide${v.id}" style="background-image:url(${v.imagePath})">
                          <a href="${v.imagePath}" data-lightbox="all"><i class="fa fa-eye fa-2x"></i></a>
                         
                          ${(v.title != null) ? '<span class="img-title">'+v.title+'</span>' : ''}
                          <?php if(Authorisation::isInterfaceAdmin()){ ?>
                          <a href="javascript:;" class="delete-pic" data-id="${v.id}"><i class="fa fa-trash fa-2x"></i></a>
                          <a href="javascript:;" class="edit-picture" data-id="${v.id}" data-title="${(v.title != null) ? v.title : ''}">
                              <i class="fa fa-pencil fa-2x"></i></a>
                          <?php } ?> 
                      </div>`;
                        });
                    $('.container<?php echo $kunik ?> .swiper-wrapper').html(((html=="") ? '<h3 class="text-center">Aucune photos</h3>' : html));
                    initCaroussel<?php $kunik ?>(".container<?php echo $kunik ?> .swiper-container");
                    deletePic();
                    editPic();
                 }
              });
      }
});

function deletePic(){
    $('.delete-pic').off().on('click',function(){
      $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
            var type = "documents";
            var id = $(this).data('id');
            var urlToSend = baseUrl+"/"+moduleId+"/document/delete/id/"+id;
            
            bootbox.confirm("confirm please !!",
            function(result) 
            {
          if (result) {
            ajaxPost(
                  null,
                  urlToSend,
                  null,
                  function(data){ 
                      if ( data && data.result ) {
                      toastr.success("élément effacé");
                      $('.swiper-slide'+id).remove();
                    } else {
                       toastr.error("something went wrong!! please try again.");
                    }
                  }
              );
          }
        });
    })
}
function editPic(){
    $('.edit-picture').off().on('click',function(){
            var id = $(this).data('id');
            var title = $(this).data('title');
            updateDocument(id,title);
    })
}
function updateDocument(id, title) {
  dataUpdate = { docId : id } ;
  if(title != "undefined" && title != '')
    dataUpdate.title = title;
  mylog.log("dataUpdate", dataUpdate);
  dyFObj.openForm ('document','sub', dataUpdate);
}
</script>
