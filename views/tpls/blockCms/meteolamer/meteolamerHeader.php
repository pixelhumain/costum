<?php
    function getSpots($type){
        $spots = [];
        $res = PHDB::findAndSort('--meteolamer-spots',  ["type" => $type], ["label"=>1]);
        foreach($res as $spot){
            $spots[] = $spot;
        }
        return $spots;
    }

    function arrayChunk($array, $chunkSize){
        $res = []; $i=0;
        while($i < sizeof($array)){
            $res[] = array_slice($array, $i, $chunkSize);
            $i += $chunkSize;
        }
        return $res;
    }
?>

<?php
    $BASE_URL = Yii::app()->getRequest()->getBaseUrl(true);
    $COSTUM_HOST = isset($costum["host"])?$costum["host"]:"";
    $COSTUM_BASE_URL = ((parse_url($BASE_URL, PHP_URL_HOST))==$COSTUM_HOST)?$BASE_URL:$BASE_URL."/costum/co/index/slug/meteolamer";

    $selectedSpot = Yii::app()->getRequest()->getQuery('spot');
    $selectedSpot = $selectedSpot ? $selectedSpot:"reunion";

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $ocean_indien_spots = getSpots("region");
    $ile_spots = getSpots("spot");
?>

<style>
    .hero{
        width: 100%;
        height: 350px;
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('<?= $assetsUrl ?>/images/meteolamer/austin-schmid-_rThRCcLV6U-unsplash.jpg');
        background-size: cover;
        border-radius: 20px;
        display: flex;
        justify-content:center;
        align-items: flex-end;
        margin-bottom: 50px;
    }

    .spot-navigation-container-large{
        color: white;
        display: flex;
        justify-content:space-between;
        align-items: flex-end;
    }
    .spot-navigation-container-medium{
        display:none;
        width:100%;
        height:100%;
        flex-direction:column;
        justify-content:center;
        align-items:center;
    }
    .spot-navigation-container-medium select{
        width:250px;
        height:40px;
        border-radius:40px;
    }

    .spot-navigation-group{
        margin-right: 50px;
        border-left: 2px solid white;
    }

    .spot-navigation-group-header{
        padding: 5px 10px;
        border-bottom: 2px solid white;
    }
    .spot-navigation-group-header a{
        text-decoration: none;
        color: white;
        font-size: 16px;
        text-transform: uppercase;
        font-weight: bold;
    }

    .spot-navigation-group-body{
        padding: 10px;
        display: flex;
    }

    .spot-navigation-group-body ul{
        margin: 0px 50px 0px 0px;
        padding: 0;
    }
    .spot-navigation-group-body ul li{
        list-style: none;
    }
    .spot-navigation-group-body ul li a{
        display: block;
        color: white;
        text-decoration: none;
        text-transform: uppercase;
        padding: 2px 0px;
        font-size: 12px;
        white-space: nowrap;
        font-weight:700;
    }

    .spot-type-navigation{
        width: 100%;
        display: flex;
        justify-content: flex-end;
    }

    .spot-type-navigation ul{
        margin: 0;
        padding: 0;
    }
    .spot-type-navigation li{
        list-style: none;
        margin: 10px 0px;
    }
    .spot-type-navigation li a{
        display: flex;
        align-items: center;
        width: 120px;
        text-decoration: none;
        color: white;
        text-transform: uppercase;
        padding: 6px 8px;
        text-align: right;
        border: 1px solid white;
        border-radius: 20px;
        font-size: 14px;
        font-weight: bold;
        transition: .3s;

    }
    .spot-type-navigation li a img{
        height: 25px;
        width: auto;
        margin-right: 10px;
    }
    .spot-type-navigation li a:hover, .spot-type-navigation li a.active{
        background: white;
        color: black;
    }
    .spot-type-navigation li a:hover > img, .spot-type-navigation li a.active > img{
        filter: invert(1);
    }

    .spot-type-navigation-inline{
        width:auto !important;
    }
    .spot-type-navigation-inline ul li{
        display: inline-block;
        margin: 50px 5px;
    }

    @media only screen and (max-width: 1300px){
        .container-<?= $kunik ?>{
            min-height: 0 !important;
        }

        .spot-navigation-container-large {
            display:none !important;
        }
        .spot-navigation-container-medium{
            display:flex;
        }

        .hero{
            height:auto;
            padding: 50px 0px;
            margin-bottom: 20px;
        }

        .spot-type-navigation{
            flex-direction: row;
            justify-content:center;
            margin-top:20px;
        }

        .spot-type-navigation li{
            display: inline-block;
        }

        .spot-type-navigation li a{
            justify-content:center;
            width:70px;
            font-size:12px;
        }

        .spot-type-navigation li a img{
            display: none;
        }

        .spot-type-navigation-inline ul li{
            margin: 0px;
        }
    }
</style>

<div class="hero container-<?= $kunik ?>">
    <div class="container spot-navigation-container-large">
        <?php if($page !== "maree"){ ?>
        <div style="display:flex;">
            <!-- <div class="spot-navigation-group">
                <div class="spot-navigation-group-header">
                    <a href="#">Océan indien</a>
                </div>
                <div class="spot-navigation-group-body" id="spots-ocean-indien">
                    <?php foreach(arrayChunk($ocean_indien_spots,5) as $spots) { ?>
                    <ul>
                        <?php foreach($spots as $spot){ ?>
                        <li><a href="/costum/co/index/slug/meteolamer#<?=($page!=="vent")?"vague":"vent"?>?spot=<?= $spot["name"] ?>" class="spot-navigation"><?= $spot["label"] ?></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </div>
            </div> -->
            <?php if($page !== "vent"){ ?>
            <div class="spot-navigation-group">
                <div class="spot-navigation-group-header">
                    <a href="<?= $COSTUM_BASE_URL ?>#vague?spot=reunion" class="meteolamer-spot-nav" data-spot="reunion" style="font-size: 20px;">Océan indien</a><br>
                    <a href="<?= $COSTUM_BASE_URL ?>#vague?spot=home" class="meteolamer-spot-nav" data-spot="home">Ile de la réunion</a>
                </div>
                <div class="spot-navigation-group-body">
                    <?php foreach(arrayChunk($ile_spots, 5) as $spots){ ?>
                    <ul>
                        <?php foreach($spots as $spot){ ?>
                        <li>
                            <a 
                                href="<?= $COSTUM_BASE_URL ?>#vague?spot=<?= $spot["name"] ?>" 
                                data-spot="<?= $spot["name"] ?>" 
                                class="spot-navigation meteolamer-spot-nav"
                            >
                                <?= $spot["label"] ?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        <div class="spot-type-navigation">
            <ul>
                <li><a href="<?= $COSTUM_BASE_URL ?>#vague" class="<?= $page=="vague"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/wave.png" alt=""> <span>Vagues</span></a></li>
                <li><a href="<?= $COSTUM_BASE_URL ?>#vent" class="<?= $page=="vent"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/wind.png" alt=""> <span>Vent</span></a></li>
                <li><a href="<?= $COSTUM_BASE_URL ?>#maree" class="<?= $page=="maree"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/tide.png" alt=""> <span>Marée</span></a></li>
            </ul>
        </div>
    </div>
    <div class="spot-navigation-container-medium">
        <?php if($page !=="maree"){ ?>
        <div>
            <select id="spot-select-navigation" class="form-control">
                <option value="reunion" <?= ($selectedSpot=="reunion")?"selected":"" ?>>Océan indien</option>
                <option value="home" <?= ($selectedSpot=="home")?"selected":"" ?>>Ile de la réunion</option>
                <?php foreach($ile_spots as $spot){ ?>
                    <option value="<?= $spot["name"] ?>" <?= ($selectedSpot==$spot["name"])?"selected":"" ?>>
                        <?= $spot["label"] ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <?php } ?>
        <div class="spot-type-navigation <?= ($page === "maree")?"spot-type-navigation-inline":"" ?>">
            <ul>
                <li><a href="<?= $COSTUM_BASE_URL ?>#vague" class="<?= $page=="vague"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/wave.png" alt=""> <span>Vagues</span></a></li>
                <li><a href="<?= $COSTUM_BASE_URL ?>#vent" class="<?= $page=="vent"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/wind.png" alt=""> <span>Vent</span></a></li>
                <li><a href="<?= $COSTUM_BASE_URL ?>#maree" class="<?= $page=="maree"?"active":"" ?> spot-navigation"><img src="<?= $assetsUrl ?>/images/meteolamer/tide.png" alt=""> <span>Marée</span></a></li>
            </ul>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(".spot-type-navigation a").click(function(){
            location.href = $(this).attr("href");
            location.reload();
        })
    })
</script>