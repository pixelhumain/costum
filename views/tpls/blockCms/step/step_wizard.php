<?php
    $myCmsId  = $blockCms["_id"]->{'$id'};
    $styleCss = (object) [$kunik => $blockCms["css"] ?? [] ]; 
?>

<style  id="stepwizard<?= $kunik ?>">
    .stepwizard<?= $kunik ?> .explication{
        margin-top:3%;
        margin-left: 10%;
        margin-right: 10%;
    }
    .stepwizard<?= $kunik ?> .explication-title{
        text-align : center;
        border-top: dashed 1.5px #92bf34;
        border-bottom:dashed 1.5px #92bf34;
        color : #92bf34
    }

    .stepwizard<?= $kunik ?> .explication-img {
        text-align: center;
    }

    @media (min-width: 768px) {
        .stepwizard<?= $kunik ?> .explication-img img {
            width: 70%;
        }
    }

    @media (max-width: 767px) {
        .stepwizard<?= $kunik ?> .explication{
            margin-left: 0%;
            margin-right:0%;
        }
        .stepwizard<?= $kunik ?> .explication-title > h1{
            font-size : 3rem;
        }
        .stepwizard<?= $kunik ?> .explication-img > h1 {
            font-size : 2.5rem;
        }
    }
</style>
<div class="stepwizard<?= $kunik ?> <?= $kunik ?>">
    <div class="explication row">
        <div class="explication-title col-xs-12 col-sm-12">
            <h1 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?= $blockCms["title"]?></h1>
        </div>
        <div class="col-xs-12 margin-top-20 poi-m">
            <?php
            $number = array();
            for ($i=1; $i<= $blockCms["stepNumber"]; $i++) {
                $number[] = ''.$i;
            }
            $params = array(
                "poiList"=> $poiList,
                "listSteps" => $number,
                "el" => $el,
                "color1" => "#92bf34",
                "costum" => $costum
            );
            echo $this->renderPartial("survey.views.tpls.wizard", $params); ?>
        </div>
        <div class="explication-img col-xs-12 col-sm-12 no-padding">
            <?php if ($blockCms["imgShow"] == "yes") { ?>
                <center>
                    <img src="<?= ($blockCms["logo"] != "/assets/beb5cfbf/images/thumbnail-default.jpg" && $blockCms["logo"] != "") ? $blockCms["logo"] : Yii::app()->getModule("costum")->assetsUrl.'/images/costumDesCostums/illu.svg' ?>">
                </center>
            <?php } ?>
            <br>
            <h2 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="subtitle"><?= $blockCms["subtitle"]?></h2>
            <br>
            <br>
        </div>
    </div>
</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $blockCms ); ?>;
    var str = "";
    str += cssHelpers.render.generalCssStyle(<?= json_encode($styleCss) ?>);
    $("#stepwizard<?= $kunik ?>").append(str);

    if(costum.editMode)
    {
        cmsConstructor.sp_params["<?= $myCmsId ?>"] = sectionDyf.<?php echo $kunik?>ParamsData;
        var stepWizard = {
            configTabs : {
                general: {
                    inputsConfig: [
                        {
                            type: "inputSimple",
                            options: {
                                name: "stepNumber",
                                label: "<?php echo Yii::t('cms', 'Number of steps')?>"
                            }
                        },
                        {
                            type:"groupButtons",
                            options: {
                                name:"imgShow",
                                label:"<?php echo Yii::t('cms', 'Show image')?>",
                                options:[
                                    {
                                        value: "yes",
                                        label: trad.yes
                                    },
                                    {
                                        value: "no",
                                        label: trad.no
                                    },
                                ]
                            }
                        },
                        {
                            type: "inputFileImage",
                            options: {
                                name: "logo",
                                label: tradCms.logo,
                            }
                        }
                    ]
                },
                style: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                },
                advanced: {
                    inputsConfig: [
                        "addCommonConfig"
                    ]
                }
            },
            afterSave: function(path,valueToSet,name,payload,value) {
                cmsConstructor.helpers.refreshBlock(cmsConstructor.spId, ".cmsbuilder-block[data-id='"+cmsConstructor.spId+"']");
            }
        }
        cmsConstructor.blocks.step_wizard<?= $myCmsId ?> = stepWizard;
    }
</script>